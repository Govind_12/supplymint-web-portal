import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import BraedCrumps from "../components/breadCrumps";
import NewSideBar from "../components/newSidebar";
import CreateCatalogue from "../components/vendorPortal/vendorCatalogue/createCatalogue";
import ManageCatalogue from "../components/vendorPortal/vendorCatalogue/manageCatalogue";
import Footer from "../components/footer";
import Wishlist from "../components/vendorPortal/vendorCatalogue/wishlist";
import CatalogueHistory from "../components/vendorPortal/vendorCatalogue/catalogueHistory";
import LotDetails from "../components/vendorPortal/vendorCatalogue/lotDetails";
import AddProduct from "../components/vendorPortal/vendorCatalogue/addProductCatalogue";
import Items from "../components/vendorPortal/vendorCatalogue/items";
import WishlistEmpty from "../components/vendorPortal/vendorCatalogue/wishlistEmpty";
import EnterpriseWishlist from "../components/vendorPortal/vendorCatalogue/enterpriseWishlist";
import ItemPreview from "../components/vendorPortal/vendorCatalogue/itemPreview";
import LotDetailsPartial from "../components/vendorPortal/vendorCatalogue/lotDetailsPartial";
import LotDetailsRejected from "../components/vendorPortal/vendorCatalogue/lotDetailsRejected";

class ProductCatalogue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // successModal:false
        };
    }

    render() {
        $("input").attr("autocomplete", "off");
        const { search } = this.state;
        var result = _.filter(this.state.data, function (data) {
            return _.startsWith(data.text, search);
        });
        let hash = window.location.hash;

        return (
            <div>
                <div className="container-fluid nc-design pad-l50">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    <div className="container_div m-top-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path d-table n-menu-path ptb5">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps  />
                                </ul>
                            </div>
                        </div>
                    </div>
                    {hash == "#/catalogue/vendor/manage" ? <CreateCatalogue {...this.props}/> : null}
                    {hash == "#/catalogue/vendor/manageCatalogue" ? <ManageCatalogue {...this.props}/> : null}
                    {hash == "#/catalogue/vendor/products/wishlist" ? <Wishlist {...this.props}/> : null}
                    {hash == "#/productCatalogue/emptyWishlist" ? <WishlistEmpty {...this.props}/> : null}
                    {hash == "#/catalogue/vendor/submission/history" ? <CatalogueHistory {...this.props}/> : null}
                    {hash == "#/productCatalogue/catalogueHistory/lotDetails" ? <LotDetails {...this.props}/> : null}
                    {hash == "#/productCatalogue/catalogueHistory/lotDetailsRejected" ? <LotDetailsRejected {...this.props}/> : null}
                    {hash == "#/productCatalogue/catalogueHistory/lotDetailsPartial" ? <LotDetailsPartial {...this.props}/> : null}
                    {hash == "#/productCatalogue/addProducts" ? <AddProduct {...this.props}/> : null}
                    {hash == "#/catalogue/retailer/products/view" ? <Items {...this.props}/> : null}
                    {hash == "#/catalogue/retailer/products/wishlist" ? <EnterpriseWishlist {...this.props}/> : null}
                    {hash == "#/productCatalogue/itemPreview" ? <ItemPreview {...this.props}/> : null}
                </div>
                {/* <Footer /> */}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {productCatalogue: state.productCatalogue,
        vendorProductTypes: state.vendorProductTypes};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductCatalogue);