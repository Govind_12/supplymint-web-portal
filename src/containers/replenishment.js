import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import openRack from "../assets/open-rack.svg"
import RightSideBar from "../components/rightSideBar";
import Footer from '../components/footer';
import BraedCrumps from "../components/breadCrumps";
// import SuccessRequest from "../components/successRequest";
import ReplenishmentAutoConfig from "../components/replenishment/replenishmentAutoConfig";
import ReplenishmentRunOnDemandNew from "../components/replenishment/replenishmentRunOnDemandNew";
import HistoryReplenishment from "../components/replenishment/historyReplenishment";
// import Summary from "../components/replenishment/summary";
import AdHocRequest from "../components/inventoryPlanning/adHocRequest";
import AddNewAdHoc from "../components/inventoryPlanning/addNewAdHoc";
import AdHocModal from "../components/inventoryPlanning/adHocModal";
import ViewActivity from "../components/assortment/viewActivity";
import Assortment from '../components/assortment/assortment';
import NewSummary from "../components/replenishment/newSummary";
import AutoConfig from "../components/replenishment/autoConfigNew";
import CreateConfiguration from "../components/replenishment/createConfiguration";
import ManageRuleEngine from "../components/replenishment/manageRuleEngine";
import NewSideBar from "../components/newSidebar";
import PoWithUploadHistory from "../components/piOrPoHistory/poWithUploadHistory";
import SaleContribution from "../components/inventoryPlanning/reports/saleContribution";
import SalesComparision from "../components/inventoryPlanning/reports/lastMonthReport";
import SellThruPerformance from "../components/inventoryPlanning/reports/topBottomReports";
import CatSizeInventory from "../components/inventoryPlanning/reports/catSizeReport";
import InventoryMovement from "../components/inventoryPlanning/reports/storeToStoreReport";
import TopMovingItems from "../components/inventoryPlanning/reports/topMovingReport";
import Exceptions from "../components/inventoryPlanning/reports/exceptionReport";

class Replenishment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      successModal: false
    };
  }
  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }
  openSuccessModal(e) {

    this.setState({
      successModal: !this.state.successModal
    });
  }

  // componentWillMount() {
  //   if (sessionStorage.getItem('token') == null) {
  //     this.props.history.push('/');
  //   }
  // }

  componentWillReceiveProps(nextProps) {
    // if(nextProps.sample.sample.isSuccess){
    //   this.setState({
    //     successModal: true
    //   })
    // }
  }

  handler(e) {
    e.preventDefault();
    this.props.sampleRequest();
    // this.openSuccessModal();
  }

  render() {

    $("input").attr("autocomplete", "off");
    const hash = window.location.hash.split("/")[2];
    return (
      <div>
        <div className="container-fluid nc-design">
          <NewSideBar {...this.props} />
          <SideBar {...this.props} />
          {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
          {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
          <div className="container_div m-top-75 pad-l60" id="">
            <div className="col-md-12 col-sm-12 border-btm">
              <div className="menu_path d-table n-menu-path">
                <ul className="list-inline width_100 pad20 nmp-inner">
                  <BraedCrumps {...this.props} />
                </ul>
              </div>
            </div>
          </div>
          {/* {hash == "autoConfiguration" ? <ReplenishmentAutoConfig {...this.props} /> : null} */}
          {hash == "inventoryAutoConfig" ? <AutoConfig {...this.props} /> : null}
          {/* {hash == "runOnDemand" ? <ReplenishmentRunOnDemandNew {...this.props} /> : null} */}
          {/* {hash == "history" ? <HistoryReplenishment {...this.props} /> : null} */}
          {hash == "history" ? <PoWithUploadHistory {...this.props} /> : null}
          {/* {hash == "summary" ? <Summary {...this.props} /> : null} */}
          {hash == "summary" ? <NewSummary {...this.props} /> : null}
          {hash == "configuration" ? <CreateConfiguration {...this.props} /> : null}
          {hash == "manageRuleEngine" ? <ManageRuleEngine {...this.props} /> : null}
          {hash == "manageAd-Hoc" ? ( <AdHocRequest {...this.props} /> ) : null}
          {hash == "viewActivity" ? ( <ViewActivity {...this.props} /> ) : null}
          {hash == "Ad-Hoc" ? ( <AddNewAdHoc {...this.props} /> ) : null}
          {hash == "adHocModal" ? ( <AdHocModal {...this.props} /> ) : null}
          {hash == "assortment" ? ( <Assortment {...this.props} /> ) : null}
          {hash == "saleContribution" ? ( <SaleContribution {...this.props} /> ) : null}
          {hash == "lastMonthReport" ? ( <SalesComparision {...this.props} /> ) : null}
          {hash == "topBottomReports" ? ( <SellThruPerformance {...this.props} /> ) : null}
          {hash == "catSizeReport" ? ( <CatSizeInventory {...this.props} /> ) : null}
          {hash == "storeToStoreReport" ? ( <InventoryMovement {...this.props} /> ) : null}
          {hash == "topMovingReport" ? ( <TopMovingItems {...this.props} /> ) : null}
          {hash == "exceptionReport" ? ( <Exceptions {...this.props} /> ) : null}
          </div>
          <Footer />
          {/* <button onClick={(e)=> this.handler(e)}> Success</button> 
        {this.state.successModal ? <SuccessRequest closeSuccessModal={(e) => this.openSuccessModal(e)} /> : null} */}
      </div>

    );

  }

}

export function mapStateToProps(state) {
  return {
    replenishment: state.replenishment,
    inventoryManagement: state.inventoryManagement,
    seasonPlanning: state.seasonPlanning
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Replenishment);
