import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import RightSideBar from "../components/rightSideBar";
import PI from "../components/pI";
import PiHistory from "../components/piOrPoHistory/piHistory"
import PiHistoryNew from "../components/piOrPoHistory/piHistoryNew"
import openRack from "../assets/open-rack.svg"
import Footer from '../components/footer'
import BraedCrumps from "../components/breadCrumps";
import PO from "../components/pO"
import UdfSetting from "../components/purchaseOrder/udfSetting";
import ItemUdf from "../components/purchaseOrder/itemUdf";
import ItemUdfMapping from "../components/purchaseOrder/itemUdfMapping";
import DeptSizeMapping from "../components/purchaseOrder/DepartmentSizeMapping";
import FilterLoader from "../components/loaders/filterLoader";
import PoHistory from "../components/piOrPoHistory/poHistory";
import UdfMapping from "../components/purchaseOrder/udfMapping";
import PoWithUpload from "../components/purchaseOrder/poWithUpload";
import PoWithUploadHistory from "../components/piOrPoHistory/poWithUploadHistory";
import PoHistoryNew from "../components/piOrPoHistory/poHistoryNew"
import NewSettings from '../assets/new-settings.svg';
import Help from '../assets/help.svg';
import NewSideBar from "../components/newSidebar";
import moment from "moment";
import ProcurementDashboard from "../components/pDashboard/procurementDashboard";
import Settings from "../components/purchaseOrder/procurementSetting";
import OtbReport from "../components/purchaseOrder/otbReports";

class purchaseIndent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      rightbar: false,
      openRightBar: false,
      lastSaveDateTime: '',
      orderNumber: '',
      indentNumber: '',
    };
  }

  // componentWillMount() {
  //   if (sessionStorage.getItem('token') == null) {
  //     this.props.history.push('/');
  //   }
  // }

  componentWillReceiveProps(nextProps) {
    if (nextProps.purchaseIndent.udfSetting.isSuccess) {

      this.setState({

        loader: false
      })
      this.props.createUdfSettingRequest();


    } else if (nextProps.purchaseIndent.udfSetting.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.udfSetting.message.error == undefined ? undefined : nextProps.purchaseIndent.udfSetting.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.udfSetting.message.error == undefined ? undefined : nextProps.purchaseIndent.udfSetting.message.error.errorCode,
        code: nextProps.purchaseIndent.udfSetting.message.status,
        alert: true,

        loader: false

      })
      this.props.createUdfSettingRequest();
    } else if (!nextProps.purchaseIndent.udfSetting.isLoading) {
      this.setState({
        loader: false
      })
    }

    // create header config
    if (nextProps.replenishment.createHeaderConfig.isSuccess) {
      this.setState({
          successMessage: nextProps.replenishment.createHeaderConfig.data.message,
          loader: false,
          success: true,
      })
      this.props.createHeaderConfigClear()
  } else if (nextProps.replenishment.createHeaderConfig.isError) {
      this.setState({
          loader: false,
          alert: true,
          code: nextProps.replenishment.createHeaderConfig.message.status,
          errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
          errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
      })
      this.props.createHeaderConfigClear()
  }

    if (nextProps.purchaseIndent.udfSetting.isLoading) {
      this.setState({
        loader: true
      })
    }
    if (nextProps.purchaseIndent.gen_PurchaseOrder.isSuccess) {
        this.setState({
          lastSaveDateTime: "",
        })
    }
      if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {
          this.setState({
            lastSaveDateTime: "",
          })
    }
    if (nextProps.purchaseIndent.gen_PurchaseIndent.isSuccess) {
        this.setState({
          lastSaveDateTime: "",
        })
    }
    if (nextProps.purchaseIndent.save_edited_pi.isSuccess) {
        this.setState({
          lastSaveDateTime: "",
        })
  }
    
    if (nextProps.purchaseIndent.get_draft_data.isSuccess) {
      if (nextProps.purchaseIndent.get_draft_data.data.resource != null) {
        this.setState({
          lastSaveDateTime: nextProps.purchaseIndent.get_draft_data.data.resource.updationTime
        })
      }
    }
    if (nextProps.purchaseIndent.get_draft_data == "" && nextProps.purchaseIndent.save_draft_pi == "") {
      this.setState({
        lastSaveDateTime: ""
      })
    }
    if (nextProps.purchaseIndent.get_draft_data == "" && nextProps.purchaseIndent.save_draft_po == "") {
      this.setState({
        lastSaveDateTime: ""
      })
    }
    if (nextProps.purchaseIndent.save_draft_pi.isSuccess || nextProps.purchaseIndent.save_draft_po.isSuccess) {
      // data = nextProps.purchaseIndent.save_draft_pi.data.resource ? nextProps.purchaseIndent.save_draft_pi.data.resource : nextProps.purchaseIndent.save_draft_po.data.resource
      // if(nextProps.purchaseIndent.save_draft_pi.data.resource != null) {
      this.setState({
        lastSaveDateTime: new Date()
      }, () => { })
      // }
    }
    
    if(this.props.history.location.pathname == "/purchase/purchaseIndentHistory" || this.props.history.location.pathname == "/purchase/purchaseOrderHistory"){
      this.setState({
        lastSaveDateTime: ""
      })
    }
    // else if(!nextProps.purchaseIndent.save_draft_pi.isSuccess || !nextProps.purchaseIndent.save_draft_po.isSuccess) {

    //   this.setState({
    //     lastSaveDateTime: ""
    //   })
    // }
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      this.setState({
          loader: false
      })
      this.props.getHeaderConfigClear()
  } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
          loader: false,
          alert: true,
          code: nextProps.replenishment.getHeaderConfig.message.status,
          errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
          errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
      this.props.getHeaderConfigClear()
  }
    if (nextProps.purchaseIndent.piDashBottom.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.piDashBottomClear();
    } else if (nextProps.purchaseIndent.piDashBottom.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.piDashBottom.message.error == undefined ? undefined : nextProps.purchaseIndent.piDashBottom.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.piDashBottom.message.error == undefined ? undefined : nextProps.purchaseIndent.piDashBottom.message.error.errorCode,
        code: nextProps.purchaseIndent.piDashBottom.message.status,
        alert: true,
        loader: false
      })
      this.props.piDashBottomClear();
    }
    if (nextProps.purchaseIndent.save_draft_pi.isSuccess) {
      this.setState({
        indentNumber: nextProps.purchaseIndent.save_draft_pi.data.resource.indentNo
      })
    }
    else if (nextProps.purchaseIndent.save_draft_po.isSuccess) {
      this.setState({
        orderNumber: nextProps.purchaseIndent.save_draft_po.data.resource.orderNo
      })
    }

    if (nextProps.purchaseIndent.piDashHierarchy.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.piDashHierarchyClear();
    } else if (nextProps.purchaseIndent.piDashHierarchy.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.piDashHierarchy.message.error == undefined ? undefined : nextProps.purchaseIndent.piDashHierarchy.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.piDashHierarchy.message.error == undefined ? undefined : nextProps.purchaseIndent.piDashHierarchy.message.error.errorCode,
        code: nextProps.purchaseIndent.piDashHierarchy.message.status,
        alert: true,
        loader: false
      })
      this.props.piDashHierarchyClear();
    }

            //create main header
            if (nextProps.replenishment.createMainHeaderConfig.isSuccess) {
              this.setState({
                  successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                  loader: false,
                  success: true,
              })
              this.props.createMainHeaderConfigClear()
          } else if (nextProps.replenishment.createMainHeaderConfig.isError) {
              this.setState({
                  loader: false,
                  alert: true,
                  code: nextProps.replenishment.createMainHeaderConfig.message.status,
                  errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                  errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
              })
              this.props.createMainHeaderConfigClear()
          }
          //create set header
          if (nextProps.replenishment.createSetHeaderConfig.isSuccess) {
              this.setState({
                  successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                  loader: false,
                  success: true,
              })
              this.props.createSetHeaderConfigClear()
          } else if (nextProps.replenishment.createSetHeaderConfig.isError) {
              this.setState({
                  loader: false,
                  alert: true,
                  code: nextProps.replenishment.createSetHeaderConfig.message.status,
                  errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode,
                  errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage
              })
              this.props.createSetHeaderConfigClear()
          }
          //create  item header
          if (nextProps.replenishment.createItemHeaderConfig.isSuccess) {
              this.setState({
                  successMessage: nextProps.replenishment.createItemHeaderConfig.data.message,
                  loader: false,
                  success: true,
              })
              this.props.createItemHeaderConfigClear()
          } else if (nextProps.replenishment.createItemHeaderConfig.isError) {
              this.setState({
                  loader: false,
                  alert: true,
                  code: nextProps.replenishment.createItemHeaderConfig.message.status,
                  errorCode: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorCode,
                  errorMessage: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorMessage
              })
              this.props.createItemHeaderConfigClear()
          }
          if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
              this.setState({
                  loader: false
              })
              this.props.getMainHeaderConfigClear()
          } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
              this.setState({
                  loader: false,
                  alert: true,
                  code: nextProps.replenishment.getMainHeaderConfig.message.status,
                  errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                  errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
              })
              this.props.getMainHeaderConfigClear()
          }
          if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
              this.setState({
                  loader: false
              })
              this.props.getSetHeaderConfigClear()
          } else if (nextProps.replenishment.getSetHeaderConfig.isError) {
              this.setState({
                  loader: false,
                  alert: true,
                  code: nextProps.replenishment.getSetHeaderConfig.message.status,
                  errorCode: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorCode,
                  errorMessage: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorMessage
              })
              this.props.getSetHeaderConfigClear()
          }
          if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
              this.setState({
                  loader: false
              })
              this.props.getItemHeaderConfigClear()
          } else if (nextProps.replenishment.getItemHeaderConfig.isError) {
              this.setState({
                  loader: false,
                  alert: true,
                  code: nextProps.replenishment.getItemHeaderConfig.message.status,
                  errorCode: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorCode,
                  errorMessage: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorMessage
              })
              this.props.getItemHeaderConfigClear()
          }
          if (nextProps.purchaseIndent.analyticsOtb.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.analyticsOtbClear()
        } else if (nextProps.purchaseIndent.analyticsOtb.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.analyticsOtb.message.status,
                errorCode: nextProps.purchaseIndent.analyticsOtb.message.error == undefined ? undefined : nextProps.purchaseIndent.analyticsOtb.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.analyticsOtb.message.error == undefined ? undefined : nextProps.purchaseIndent.analyticsOtb.message.error.errorMessage
            })
            this.props.analyticsOtbClear()
        }
    if (nextProps.purchaseIndent.piImageUrl.isSuccess || nextProps.purchaseIndent.piImageUrl.isError) {
        this.setState({
            loader: false
        })
    }
    if (nextProps.purchaseIndent.piDashHierarchy.isLoading ||
            nextProps.purchaseIndent.analyticsOtb.isLoading ||
            nextProps.purchaseIndent.piDashBottom.isLoading ||
            nextProps.replenishment.createMainHeaderConfig.isLoading || 
            nextProps.replenishment.createSetHeaderConfig.isLoading || 
            nextProps.replenishment.createItemHeaderConfig.isLoading || 
            nextProps.replenishment.getMainHeaderConfig.isLoading || 
            nextProps.replenishment.getSetHeaderConfig.isLoading || 
            nextProps.replenishment.getItemHeaderConfig.isLoading ||
            nextProps.purchaseIndent.piImageUrl.isLoading) {
      this.setState({ loader: true })
    }
    
  }
  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  render() {

    const hash = window.location.hash;
    return (
      <div>
        <div className="container-fluid nc-design">
          <NewSideBar {...this.props} />
          <SideBar {...this.props} />
          {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
          {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
          <div className="container_div m-top-100 m-t-75 pad-l50" id="">
            <div className="col-md-12 col-sm-12 border-btm">
              <div className="menu_path n-menu-path">
                <ul className="list-inline width_100 pad20 nmp-inner">
                  <BraedCrumps  {...this.props} />
                </ul>
                <div className="nmp-right">
                  {/* <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button> */}
                  {/* <button className="nmp-help">Need Help <img src={Help}></img></button> */}

                  {this.state.lastSaveDateTime != "" && this.state.lastSaveDateTime != null && <span className="breadcum-text">Last Save : <span className="breadcum-text-bold">{moment(this.state.lastSaveDateTime).format("hh:mm A")} / {moment(this.state.lastSaveDateTime).format("DD-MMMM-YYYY")} </span></span>}
                  {/* <button className="nmp-help">
                      <svg xmlns="http://www.w3.org/2000/svg" id="prefix__icon" width="18" height="18" viewBox="0 0 23.807 23.29">
                          <path fill="#3a5074" id="prefix__Path_410" d="M4.155 14.608a2.133 2.133 0 0 0-1.6 2.551l.418 1.83a2.13 2.13 0 0 0 2.551 1.6l.989-.225a9.366 9.366 0 0 0 1.3 1.627l-.433.916a2.13 2.13 0 0 0 1 2.843l1.692.813a2.128 2.128 0 0 0 2.841-1l.428-.921a9.479 9.479 0 0 0 2.1 0l.451.926a2.13 2.13 0 0 0 2.843 1l1.69-.813a2.133 2.133 0 0 0 1-2.843l-.441-.919a9.439 9.439 0 0 0 1.3-1.624l1 .225a2.05 2.05 0 0 0 .473.055 2.135 2.135 0 0 0 2.077-1.657l.418-1.83a2.135 2.135 0 0 0-1.632-2.558l-.971-.225a9.108 9.108 0 0 0-.466-2.065l.778-.618a2.133 2.133 0 0 0 .338-3l-1.172-1.469a2.133 2.133 0 0 0-3-.338l-.771.616a9.3 9.3 0 0 0-1.9-.919V5.62a2.133 2.133 0 0 0-2.13-2.13h-1.88a2.133 2.133 0 0 0-2.123 2.13v.98a9.151 9.151 0 0 0-1.9.919L8.645 6.9a2.133 2.133 0 0 0-3 .335L4.473 8.706a2.128 2.128 0 0 0 .335 3l.776.621a9.261 9.261 0 0 0-.458 2.05zm1.927 1.252a.823.823 0 0 0 .638-.766 7.622 7.622 0 0 1 .611-2.716.821.821 0 0 0-.25-.964l-1.251-1a.486.486 0 0 1-.078-.688l1.169-1.468a.5.5 0 0 1 .708-.075l1.251 1a.818.818 0 0 0 1 .02 7.619 7.619 0 0 1 2.5-1.2.823.823 0 0 0 .6-.791V5.62a.5.5 0 0 1 .5-.5h1.877a.5.5 0 0 1 .5.5v1.587a.818.818 0 0 0 .6.791 7.607 7.607 0 0 1 2.5 1.2.818.818 0 0 0 1-.02l1.251-1a.5.5 0 0 1 .688.078l1.169 1.467a.483.483 0 0 1 .105.36.5.5 0 0 1-.183.33l-1.251 1a.818.818 0 0 0-.25.964 7.647 7.647 0 0 1 .608 2.7.818.818 0 0 0 .638.766l1.564.358a.5.5 0 0 1 .368.588l-.476 1.84a.5.5 0 0 1-.568.371l-1.577-.368a.821.821 0 0 0-.9.405 7.7 7.7 0 0 1-1.715 2.14.821.821 0 0 0-.2.974l.7 1.462a.5.5 0 0 1-.228.656l-1.69.813a.5.5 0 0 1-.656-.228l-.706-1.467a.821.821 0 0 0-.886-.451 7.737 7.737 0 0 1-2.753 0 .823.823 0 0 0-.881.451l-.7 1.462a.5.5 0 0 1-.653.23l-1.689-.809a.486.486 0 0 1-.25-.28.5.5 0 0 1 .023-.375l.7-1.462a.821.821 0 0 0-.2-.971 7.644 7.644 0 0 1-1.732-2.14.823.823 0 0 0-.721-.428.751.751 0 0 0-.183.02l-1.577.36a.5.5 0 0 1-.368-.065.471.471 0 0 1-.213-.293l-.415-1.843a.5.5 0 0 1 .368-.586zm4.948 2.678a4.756 4.756 0 1 1 8.112-3.361 4.948 4.948 0 0 1-.043.641.819.819 0 1 1-1.624-.218 3.4 3.4 0 0 0 .028-.423 3.114 3.114 0 1 0-3.116 3.116 3.217 3.217 0 0 0 .5-.04.819.819 0 0 1 .25 1.619 4.638 4.638 0 0 1-.751.06 4.723 4.723 0 0 1-3.356-1.394z" data-name="Path 410" transform="translate(-2.497 -3.49)" />
                      </svg>
                    </button>
                    <button className="nmp-help">
                      <svg xmlns="http://www.w3.org/2000/svg" id="prefix__help" width="18" height="18" viewBox="0 0 22.334 22.334">
                        <path fill="#3a5074" id="prefix__Path_406" d="M11.167 1.563a9.6 9.6 0 1 1-9.6 9.6 9.6 9.6 0 0 1 9.6-9.6m0-1.563a11.167 11.167 0 1 0 11.167 11.167A11.167 11.167 0 0 0 11.167 0z" className="prefix__cls-1" data-name="Path 406"/>
                        <path fill="#3a5074" id="prefix__Path_407" d="M38.532 25a10.164 10.164 0 0 1 1.181.194 2.7 2.7 0 0 1 1.987 2.553 2.323 2.323 0 0 1-.94 2.122c-.317.246-.65.469-.969.71a1.577 1.577 0 0 0-.71 1.407.893.893 0 0 1-.643.893.987.987 0 0 1-1.157-.346.936.936 0 0 1-.132-.462 3.1 3.1 0 0 1 1.273-2.6 9.279 9.279 0 0 0 1.012-.893 1.117 1.117 0 0 0 .261-.628.966.966 0 0 0-1.009-1.081 1.953 1.953 0 0 0-1.921.853.936.936 0 0 1-.972.447.857.857 0 0 1-.683-1.284 2.868 2.868 0 0 1 1.4-1.385 4.849 4.849 0 0 1 2.022-.5z" className="prefix__cls-1" data-name="Path 407" transform="translate(-27.189 -19.416)"/>
                        <path fill="#3a5074" id="prefix__Path_408" d="M45.866 65.917a1.023 1.023 0 0 1-1.1 1.144 1.046 1.046 0 0 1-1.175-1.012c0-.833.373-1.248 1.117-1.271a1.034 1.034 0 0 1 1.158 1.139z" className="prefix__cls-1" data-name="Path 408" transform="translate(-33.855 -50.31)"/>
                        </svg>
                      </button> */}
                </div>
              </div>
            </div>
          </div>
          {hash == "#/purchase/purchaseIndent" || hash == "#/purchase/purchaseIndents" ? (
            <PI {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/dashboard" ? (
            <ProcurementDashboard {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}

          {hash == "#/purchase/purchaseIndentHistory" ? (
            <PiHistoryNew {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/purchaseOrderHistory" ? (
            <PoHistoryNew {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}

          {hash == "#/purchase/purchaseOrder" || hash == "#/purchase/purchaseOrders" ? (
            <PO {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}

          {hash == "#/purchase/udfSetting" ? (
            <UdfSetting  {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/itemUdfSetting" ? (
            <ItemUdf {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/udfMapping" ? (
            <UdfMapping {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/itemUdfMapping" ? (
            <ItemUdfMapping {...this.props} {...this.state} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/departmentSizeMapping" ? (
            <DeptSizeMapping {...this.props} rightSideBar={() => this.onRightSideBar()} />


          ) : null}
          {hash == "#/purchase/purchaseOrderWithUpload" ? (
            <PoWithUpload {...this.props} rightSideBar={() => this.onRightSideBar()} />

          ) : null}
          {/* {hash == "#/purchase/uploadHistory" ? (
            <PoWithUploadHistory {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null} */}
          {hash == "#/purchase/catDescUdfSetting" ? (
            <Settings {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}
          {hash == "#/analytics/otbReport" ? (
            <OtbReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}

          {this.state.loader ? <FilterLoader /> : null}
        </div>
        <Footer />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    purchaseIndent: state.purchaseIndent,
    replenishment: state.replenishment
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(purchaseIndent);
