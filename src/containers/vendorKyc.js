import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import * as actions from "../redux/actions";
import VendorKyc from '../components/vendor/newKyc'
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';

class VendorKycContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
        }
    }

    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
      componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }
    escFun = (e) =>{
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
            this.setState({ loader: false, success: false, alert: false, })
            this.props.history.push('/home')
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.vendor.saveKycDetails.isSuccess) {
            this.setState({
                successMessage: nextProps.vendor.saveKycDetails.data.resource.response,
                loader: false,
                success: true,
            })
            this.props.saveKycDetailsClear()
        }  else if (nextProps.vendor.saveKycDetails.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.vendor.saveKycDetails.message.status,
                errorCode: nextProps.vendor.saveKycDetails.message.error == undefined ? undefined : nextProps.vendor.saveKycDetails.message.error.errorCode,
                errorMessage: nextProps.vendor.saveKycDetails.message.error == undefined ? undefined : nextProps.vendor.saveKycDetails.message.error.errorMessage
            })
            this.props.saveKycDetailsClear()
        }
        if (nextProps.vendor.saveKycDetails.isLoading ) {
            this.setState({
                loader: true
            })
        }
    }
 
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest =(e)=> {
        e.preventDefault();
        this.setState({
            success: false,
        },()=> this.props.history.push('/home'));
    }

    render () {
        const hash = window.location.hash.split("/")[2];
        return (
            <div className="container-fluid nc-design">
                <header className="header header2">
                    <div className="col-md-12 col-sm-12 pad-0">
                        <div className="col-md-6 col-xs-6 col-sm-6 m-top-10">
                            <ul className="list-inline">
                                <li ><Link to="/home"><img className="logo_img new-logo-img" src={require('../assets/vendorLogo.svg')} /></Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>
                {hash == "newKyc" ? (<VendorKyc {...this.props} />) : null}

                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        vendorKyc: state.vendorKyc,
        vendor: state.vendor,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VendorKycContainer);