import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";

class Reporting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    $("input").attr("autocomplete", "off");
    return (
      <div className="container-fluid">
        <SideBar {...this.props} />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reporting);
