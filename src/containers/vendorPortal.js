import React from 'react';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';
import RightSideBar from "../components/rightSideBar";
import DigiVendSetting from '../components/vendorPortal/configuration/digivendSetting';
import NewSideBar from '../components/newSidebar';
import SearchComment from '../components/vendorPortal/searchComment';
import Configuration from '../components/vendorPortal/configuration/configuration';
import CustomDataUpload from '../components/vendorPortal/configuration/vendorCustomData';
import RetailerSalesReport from '../components/vendorPortal/configuration/retailerSalesReport';
import RetailerStockReport from '../components/vendorPortal/configuration/retailerStockReport';
import RetailerLadgerReport from '../components/vendorPortal/configuration/retailerLedgerReport';
import RetailerPoReport from '../components/vendorPortal/configuration/retailerPoReport';
import RetailerLrReport from '../components/vendorPortal/configuration/retailerLrReport';
import RetailerAsnReport from '../components/vendorPortal/configuration/retailerAsnReport';
import VendorSalesReport from '../components/vendorPortal/vendor/reports/vendorSalesReport';
import VendorStockReport from '../components/vendorPortal/vendor/reports/vendorStockReport';
import VendorLedgerReport from '../components/vendorPortal/vendor/reports/vendorLedgerReport';
import RetailerOutstandingReport from '../components/vendorPortal/configuration/retailerOutstandingReport';
import VendorPoReport from '../components/vendorPortal/vendor/reports/vendorPoReport';
import VendorLrReport from '../components/vendorPortal/vendor/reports/vendorLrReport';
import VendorAsnReport from '../components/vendorPortal/vendor/reports/vendorAsnReport';
import VendorInspectionReport from '../components/vendorPortal/vendor/reports/vendorInspectionReport';
import RetailerInspectionReport from '../components/vendorPortal/configuration/retailerInspectionReport';
import VendorSearchComment from '../components/vendorPortal/vendorSearchComment';
import VendorActivityReport from '../components/vendorPortal/vendor/reports/activityReport';
import VendorLoginLogoutReport from '../components/vendorPortal/vendor/reports/loginlogoutReport';
import OrderRequest from '../components/vendorPortal/vendor/orders/orderRequest';
import LogisticsInventoryReport from '../components/vendorPortal/configuration/logisticsInventoryReport';
import LogisticsPaymentReconciliation from '../components/vendorPortal/configuration/logisticsPaymentRoconciliation';

class VendorPortal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            expandDetails: [],
            approvedPO: [],
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
        };
    }

    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
            this.setState({ loader: false, success: false, alert: false })
        }
    }

    componentWillReceiveProps(nextProps) {
        //custom column setting heaader
         if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getMainHeaderConfigClear()
        } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
            })
            this.props.getMainHeaderConfigClear()
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getSetHeaderConfigClear()
        } else if (nextProps.replenishment.getSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorMessage
            })
            this.props.getSetHeaderConfigClear()
        }
        if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getItemHeaderConfigClear()
        } else if (nextProps.replenishment.getItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorMessage
            })
            this.props.getItemHeaderConfigClear()
        }
        //create main header
        if (nextProps.updateHeader.updateHeaderData.isSuccess) {
            this.setState({
                successMessage: nextProps.updateHeader.updateHeaderData.data.message,
                loader: false,
                success: true,
            })
            this.props.updateHeaderClear()
        } else if (nextProps.updateHeader.updateHeaderData.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.updateHeader.updateHeaderData.message.status,
                errorCode: nextProps.updateHeader.updateHeaderData.message.error == undefined ? undefined : nextProps.updateHeader.updateHeaderData.message.error.errorCode,
                errorMessage: nextProps.updateHeader.updateHeaderData.message.error == undefined ? undefined : nextProps.updateHeader.updateHeaderData.message.error.errorMessage
            })
            this.props.updateHeaderClear()
        }
        
        if (nextProps.orders.EnterpriseApprovedPo.isSuccess) {
            this.setState({loader: false })
        }else if (nextProps.orders.EnterpriseApprovedPo.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.EnterpriseApprovedPo.message.status,
                errorCode: nextProps.orders.EnterpriseApprovedPo.message.error == undefined ? undefined : nextProps.orders.EnterpriseApprovedPo.message.error.errorCode,
                errorMessage: nextProps.orders.EnterpriseApprovedPo.message.error == undefined ? undefined : nextProps.orders.EnterpriseApprovedPo.message.error.errorMessage
            })
            this.props.EnterpriseApprovedPoClear();
        }
        if(nextProps.replenishment.createMainHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createMainHeaderConfigClear();
        }else if (nextProps.replenishment.createMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
            })
            this.props.createMainHeaderConfigClear();
        }
        if(nextProps.shipment.getShipment.isSuccess) {
            this.setState({loader: false })
        }else if (nextProps.shipment.getShipment.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.getShipment.message.status,
                errorCode: nextProps.shipment.getShipment.message.error == undefined ? undefined : nextProps.shipment.getShipment.message.error.errorCode,
                errorMessage: nextProps.shipment.getShipment.message.error == undefined ? undefined : nextProps.shipment.getShipment.message.error.errorMessage
            })
            this.props.createMainHeaderConfigClear();
        }
        if(nextProps.logistic.getAllVendorTransitNDeliver.isSuccess) {
            this.setState({loader: false })
        }else if (nextProps.logistic.getAllVendorTransitNDeliver.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.logistic.getAllVendorTransitNDeliver.message.status,
                errorCode: nextProps.logistic.getAllVendorTransitNDeliver.message.error == undefined ? undefined : nextProps.logistic.getAllVendorTransitNDeliver.message.error.errorCode,
                errorMessage: nextProps.logistic.getAllVendorTransitNDeliver.message.error == undefined ? undefined : nextProps.logistic.getAllVendorTransitNDeliver.message.error.errorMessage
            })
            this.props.getAllVendorTransitNDeliverClear();
        }

        if (nextProps.administration.dataSync.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.administration.dataSync.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.dataSync.message.status,
                errorCode: nextProps.administration.dataSync.message.error == undefined ? undefined : nextProps.administration.dataSync.message.error.errorCode,
                errorMessage: nextProps.administration.dataSync.message.error == undefined ? undefined : nextProps.administration.dataSync.message.error.errorMessage
            })
            this.props.dataSyncClear();
        }

        if (nextProps.administration.getTemplate.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.administration.getTemplate.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.getTemplate.message.status,
                errorCode: nextProps.administration.getTemplate.message.error == undefined ? undefined : nextProps.administration.getTemplate.message.error.errorCode,
                errorMessage: nextProps.administration.getTemplate.message.error == undefined ? undefined : nextProps.administration.getTemplate.message.error.errorMessage
            })
            this.props.getTemplateClear();
        }

        if (nextProps.orders.getAllComments.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getAllComments.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getAllComments.message.status,
                errorCode: nextProps.orders.getAllComments.message.error == undefined ? undefined : nextProps.orders.getAllComments.message.error.errorCode,
                errorMessage: nextProps.orders.getAllComments.message.error == undefined ? undefined : nextProps.orders.getAllComments.message.error.errorMessage
            })
            this.props.getAllCommentsClear();
        }
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.shipment.getAllComment.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.getAllComment.message.status,
                errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
                errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage
            })
            this.props.getAllCommentClear();
        }

        if (nextProps.shipment.deleteUploads.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.shipment.deleteUploads.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.deleteUploads.message.status,
                errorCode: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorCode,
                errorMessage: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorMessage
            })
            this.props.deleteUploadsClear();
        }

        if (nextProps.orders.multipleDocumentDownload.isSuccess) {
            this.setState({
                successMessage: nextProps.orders.multipleDocumentDownload.data.message,
                loader: false,
                success: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
            },()=>{
                if( nextProps.orders.multipleDocumentDownload.data.resource !== undefined && nextProps.orders.multipleDocumentDownload.data.resource != null )
                   window.open(`${nextProps.orders.multipleDocumentDownload.data.resource}`)       
            })
            this.props.multipleDocumentDownloadClear()
        }
        else if (nextProps.orders.multipleDocumentDownload.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.multipleDocumentDownload.message.status,
                errorCode: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorCode,
                errorMessage: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorMessage
            })
            this.props.multipleDocumentDownloadClear()
        }

        if (nextProps.shipment.getAllShipmentVendor.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.shipment.getAllShipmentVendor.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.getAllShipmentVendor.message.status,
                errorCode: nextProps.shipment.getAllShipmentVendor.message.error == undefined ? undefined : nextProps.shipment.getAllShipmentVendor.message.error.errorCode,
                errorMessage: nextProps.shipment.getAllShipmentVendor.message.error == undefined ? undefined : nextProps.shipment.getAllShipmentVendor.message.error.errorMessage
            })
            this.props.getAllShipmentVendorClear();
        }
        if (nextProps.orders.emailTranscript.isSuccess) {
            this.setState({ loader: false })
        } else if (nextProps.orders.emailTranscript.isError) {
            this.setState({loader: false})
        }
        if (nextProps.orders.getAllLedgerReport.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getAllLedgerReport.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getAllLedgerReport.message.status,
                errorCode: nextProps.orders.getAllLedgerReport.message.error == undefined ? undefined : nextProps.orders.getAllLedgerReport.message.error.errorCode,
                errorMessage: nextProps.orders.getAllLedgerReport.message.error == undefined ? undefined : nextProps.orders.getAllLedgerReport.message.error.errorMessage
            })
            this.props.getAllLedgerReportClear();
        }
        if (nextProps.orders.getAllSaleReport.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getAllSaleReport.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getAllSaleReport.message.status,
                errorCode: nextProps.orders.getAllSaleReport.message.error == undefined ? undefined : nextProps.orders.getAllSaleReport.message.error.errorCode,
                errorMessage: nextProps.orders.getAllSaleReport.message.error == undefined ? undefined : nextProps.orders.getAllSaleReport.message.error.errorMessage
            })
            this.props.getAllSaleReportClear();
        }
        if (nextProps.orders.getAllStockReport.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getAllStockReport.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getAllStockReport.message.status,
                errorCode: nextProps.orders.getAllStockReport.message.error == undefined ? undefined : nextProps.orders.getAllStockReport.message.error.errorCode,
                errorMessage: nextProps.orders.getAllStockReport.message.error == undefined ? undefined : nextProps.orders.getAllStockReport.message.error.errorMessage
            })
            this.props.getAllStockReportClear();
        }
        if (nextProps.orders.getAllOutStandingReport.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getAllOutStandingReport.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getAllOutStandingReport.message.status,
                errorCode: nextProps.orders.getAllOutStandingReport.message.error == undefined ? undefined : nextProps.orders.getAllOutStandingReport.message.error.errorCode,
                errorMessage: nextProps.orders.getAllOutStandingReport.message.error == undefined ? undefined : nextProps.orders.getAllOutStandingReport.message.error.errorMessage
            })
            this.props.getAllOutStandingReportClear();
        }
        if (nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage
            })
            this.props.getArsGenericFiltersClear();
        }
        if (nextProps.vendor.getAllManageVendor.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.vendor.getAllManageVendor.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.vendor.getAllManageVendor.message.status,
                errorCode: nextProps.vendor.getAllManageVendor.message.error == undefined ? undefined : nextProps.vendor.getAllManageVendor.message.error.errorCode,
                errorMessage: nextProps.vendor.getAllManageVendor.message.error == undefined ? undefined : nextProps.vendor.getAllManageVendor.message.error.errorMessage
            })
            this.props.getAllManageVendorClear();
        }
        if (nextProps.orders.getLedgerFilterReport.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getLedgerFilterReport.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getLedgerFilterReport.message.status,
                errorCode: nextProps.orders.getLedgerFilterReport.message.error == undefined ? undefined : nextProps.orders.getLedgerFilterReport.message.error.errorCode,
                errorMessage: nextProps.orders.getLedgerFilterReport.message.error == undefined ? undefined : nextProps.orders.getLedgerFilterReport.message.error.errorMessage
            })
            this.props.getLedgerFilterReportClear();
        }
        if( nextProps.changeSetting.invoiceApprovalBasedLR.isSuccess){
            this.setState({
                successMessage: nextProps.changeSetting.invoiceApprovalBasedLR.data.message,
                loader: false,
                success: true,
            })
        }
        else if (nextProps.changeSetting.invoiceApprovalBasedLR.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.invoiceApprovalBasedLR.message.error == undefined ? undefined : nextProps.changeSetting.invoiceApprovalBasedLR.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.invoiceApprovalBasedLR.message.status,
                errorCode: nextProps.changeSetting.invoiceApprovalBasedLR.message.error == undefined ? undefined : nextProps.changeSetting.invoiceApprovalBasedLR.message.error.errorCode
            })
            this.props.invoiceApprovalBasedLRClear()
        }
        if (nextProps.changeSetting.createAsnFormat.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.createAsnFormat.data.message,
                loader: false,
                success: true,
            })
        } 
        else if (nextProps.changeSetting.createAsnFormat.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.createAsnFormat.message.error == undefined ? undefined : nextProps.changeSetting.createAsnFormat.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.createAsnFormat.message.status,
                errorCode: nextProps.changeSetting.createAsnFormat.message.error == undefined ? undefined : nextProps.changeSetting.createAsnFormat.message.error.errorCode
            })
            this.props.createAsnFormatClear()
        }
        if (nextProps.changeSetting.getAsnFormat.isSuccess) {
            this.setState({ loader: false})
        }
        else if (nextProps.changeSetting.getAsnFormat.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getAsnFormat.message.error == undefined ? undefined : nextProps.changeSetting.getAsnFormat.message.error.errorMessage,
                errorCode: nextProps.changeSetting.getAsnFormat.message.error == undefined ? undefined : nextProps.changeSetting.getAsnFormat.message.error.errorCode,
                code: nextProps.changeSetting.getAsnFormat.message.status,
                alert: true,
                loader: false
            })
            this.props.getAsnFormatClear()
        }
        if (nextProps.logistic.getButtonActiveConfig.isSuccess) {
            this.setState({ loader: false})
        }
        else if (nextProps.logistic.getButtonActiveConfig.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getButtonActiveConfig.message.error == undefined ? undefined : nextProps.logistic.getButtonActiveConfig.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.logistic.getButtonActiveConfig.message.status,
                errorCode: nextProps.logistic.getButtonActiveConfig.message.error == undefined ? undefined : nextProps.logistic.getButtonActiveConfig.message.error.errorCode
            })
            this.props.getStatusButtonActiveClear()
        }
        if (nextProps.replenishment.createSetHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createSetHeaderConfigClear()
        } 
        else if (nextProps.replenishment.createSetHeaderConfig.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.createSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode
            })
            this.props.createSetHeaderConfigClear()
        }
        if (nextProps.replenishment.createItemHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createItemHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createItemHeaderConfigClear()
        } 
        else if (nextProps.replenishment.createItemHeaderConfig.isError) {
            this.setState({
                errorMessage: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.replenishment.createItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorCode
            })
            this.props.createItemHeaderConfigClear()
        }
        if (nextProps.orders.getVendorActivity.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getVendorActivity.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getVendorActivity.message.status,
                errorCode: nextProps.orders.getVendorActivity.message.error == undefined ? undefined : nextProps.orders.getVendorActivity.message.error.errorCode,
                errorMessage: nextProps.orders.getVendorActivity.message.error == undefined ? undefined : nextProps.orders.getVendorActivity.message.error.errorMessage
            })
            this.props.getVendorActivityClear();
        }
        if (nextProps.orders.getVendorLogs.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.orders.getVendorLogs.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getVendorLogs.message.status,
                errorCode: nextProps.orders.getVendorLogs.message.error == undefined ? undefined : nextProps.orders.getVendorLogs.message.error.errorCode,
                errorMessage: nextProps.orders.getVendorLogs.message.error == undefined ? undefined : nextProps.orders.getVendorLogs.message.error.errorMessage
            })
            this.props.getVendorLogsClear();
        }
        if (nextProps.logistic.getAllLogTransPayReport.isSuccess) {
            this.setState({ loader: false})
        }
        else if (nextProps.logistic.getAllLogTransPayReport.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllLogTransPayReport.message.error == undefined ? undefined : nextProps.logistic.getAllLogTransPayReport.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.logistic.getAllLogTransPayReport.message.status,
                errorCode: nextProps.logistic.getAllLogTransPayReport.message.error == undefined ? undefined : nextProps.logistic.getAllLogTransPayReport.message.error.errorCode
            })
            this.props.getAllLogTransPayReportClear()
        }
        if (nextProps.logistic.getAllLogWhStockReport.isSuccess) {
            this.setState({ loader: false})
        }
        else if (nextProps.logistic.getAllLogWhStockReport.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllLogWhStockReport.message.error == undefined ? undefined : nextProps.logistic.getAllLogWhStockReport.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.logistic.getAllLogWhStockReport.message.status,
                errorCode: nextProps.logistic.getAllLogWhStockReport.message.error == undefined ? undefined : nextProps.logistic.getAllLogWhStockReport.message.error.errorCode
            })
            this.props.getAllLogWhStockReportClear()
        }
        if (nextProps.orders.getPoStockReport.isSuccess) {
            this.setState({
                loader: false,
            });
        } else if (nextProps.orders.getPoStockReport.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getPoStockReport.message.status,
                errorCode: nextProps.orders.getPoStockReport.message.error == undefined ? undefined : nextProps.orders.getPoStockReport.message.error.errorCode,
                errorMessage: nextProps.orders.getPoStockReport.message.error == undefined ? undefined : nextProps.orders.getPoStockReport.message.error.errorMessage
            });
            this.props.getPoStockReportClear();
        }
        if (nextProps.orders.getPoColorMap.isSuccess) {
            this.setState({
                loader: false,
            });
        } else if (nextProps.orders.getPoColorMap.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.getPoColorMap.message.status,
                errorCode: nextProps.orders.getPoColorMap.message.error == undefined ? undefined : nextProps.orders.getPoColorMap.message.error.errorCode,
                errorMessage: nextProps.orders.getPoColorMap.message.error == undefined ? undefined : nextProps.orders.getPoColorMap.message.error.errorMessage
            });
            this.props.getPoColorMapClear();
        }
        if (nextProps.orders.createPoArticle.isSuccess) {
            this.setState({
                successMessage: nextProps.orders.createPoArticle.data.message,
                loader: false,
                success: true,
            });
        } 
        else if (nextProps.orders.createPoArticle.isError) {
            this.setState({
                errorMessage: nextProps.orders.createPoArticle.message.error == undefined ? undefined : nextProps.orders.createPoArticle.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.orders.createPoArticle.message.status,
                errorCode: nextProps.orders.createPoArticle.message.error == undefined ? undefined : nextProps.orders.createPoArticle.message.error.errorCode
            });
            this.props.createPoArticleClear();
        }
        // if (nextProps.replenishment.quickFilterGet.isSuccess) {
        //     this.setState({
        //         loader: false,
        //     });
        //     this.props.quickFilterGetClear();
        // } else if (nextProps.replenishment.quickFilterGet.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.replenishment.quickFilterGet.message.status,
        //         errorCode: nextProps.replenishment.quickFilterGet.message.error == undefined ? undefined : nextProps.replenishment.quickFilterGet.message.error.errorCode,
        //         errorMessage: nextProps.replenishment.quickFilterGet.message.error == undefined ? undefined : nextProps.replenishment.quickFilterGet.message.error.errorMessage
        //     })
        //     this.props.quickFilterGetClear();
        // }
        // if (nextProps.replenishment.quickFilterSave.isSuccess) {
        //     this.setState({
        //         successMessage: nextProps.replenishment.quickFilterSave.data.message,
        //         loader: false,
        //         success: true,
        //     })
        // }
        // else if (nextProps.replenishment.quickFilterSave.isError) {
        //     this.setState({
        //         loader: false,
        //         alert: true,
        //         code: nextProps.replenishment.quickFilterSave.message.status,
        //         errorCode: nextProps.replenishment.quickFilterSave.message.error == undefined ? undefined : nextProps.replenishment.quickFilterSave.message.error.errorCode,
        //         errorMessage: nextProps.replenishment.quickFilterSave.message.error == undefined ? undefined : nextProps.replenishment.quickFilterSave.message.error.errorMessage
        //     });
        //     this.props.quickFilterSaveClear();
        // }
        if (nextProps.replenishment.getMainHeaderConfig.isLoading || 
            nextProps.replenishment.getSetHeaderConfig.isLoading || 
            nextProps.replenishment.getItemHeaderConfig.isLoading 
            || nextProps.updateHeader.updateHeaderData.isLoading || nextProps.orders.EnterpriseApprovedPo.isLoading
            || nextProps.replenishment.createMainHeaderConfig.isLoading || nextProps.shipment.getShipment.isLoading
            || nextProps.logistic.getAllVendorTransitNDeliver.isLoading || nextProps.administration.dataSync.isLoading
            || nextProps.administration.getTemplate.isLoading || nextProps.orders.getAllComments.isLoading
            || nextProps.shipment.getAllComment.isLoading || nextProps.shipment.deleteUploads.isLoading
            || nextProps.orders.multipleDocumentDownload.isLoading || nextProps.shipment.getAllShipmentVendor.isLoading
            || nextProps.orders.emailTranscript.isLoading || nextProps.orders.getAllLedgerReport.isLoading
            || nextProps.orders.getAllSaleReport.isLoading || nextProps.orders.getAllStockReport.isLoading 
            || nextProps.orders.getAllOutStandingReport.isLoading || nextProps.seasonPlanning.getArsGenericFilters.isLoading
            || nextProps.vendor.getAllManageVendor.isLoading || nextProps.orders.getLedgerFilterReport.isLoading
            || nextProps.changeSetting.invoiceApprovalBasedLR.isLoading || nextProps.changeSetting.createAsnFormat.isLoading
            || nextProps.changeSetting.getAsnFormat.isLoading || nextProps.logistic.getButtonActiveConfig.isLoading
            || nextProps.replenishment.createSetHeaderConfig.isLoading || nextProps.replenishment.createItemHeaderConfig.isLoading
            || nextProps.orders.getVendorActivity.isLoading || nextProps.orders.getVendorLogs.isLoading
            || nextProps.logistic.getAllLogTransPayReport.isLoading || nextProps.logistic.getAllLogWhStockReport.isLoading
            || nextProps.orders.getPoStockReport.isLoading || nextProps.orders.getPoColorMap.isLoading
            || nextProps.orders.createPoArticle.isLoading
            // || nextProps.replenishment.quickFilterGet.isLoading || nextProps.replenishment.quickFilterSave.isLoading
            || nextProps.logistic.getAllLogTransPayReport.isLoading || nextProps.logistic.getAllLogWhStockReport.isLoading) {
            this.setState({
                loader: true
            });
        }

    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    render() {
        const hash = window.location.hash.split("/")[2];
        return (
            <div>
                <div className="container-fluid nc-design pad-l60">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-t-75" id="">
                        <div className="col-md-12 col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                                {/* <div className="nmp-right">
                                    <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button>
                                    <button className="nmp-help">Need Help <img src={Help}></img></button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    {hash == "generalSetting" ? (
                        <DigiVendSetting {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : null}
                    {hash == "searchComment" ? (
                        <SearchComment {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorSearchComment" ? (
                        <VendorSearchComment {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "configuration" ? (
                        <Configuration {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "customData" ? (
                        <CustomDataUpload {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerSalesReport" ? (
                        <RetailerSalesReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerStockReport" ? (
                        <RetailerStockReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerLedgerReport" ? (
                        <RetailerLadgerReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerPoReport" ? (
                        <RetailerPoReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerLrReport" ? (
                        <RetailerLrReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerAsnReport" ? (
                        <RetailerAsnReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorSalesReport" ? (
                        <VendorSalesReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorStockReport" ? (
                        <VendorStockReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorLedgerReport" ? (
                        <VendorLedgerReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerOutstandingReport" ? (
                        <RetailerOutstandingReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorPoReport" ? (
                        <VendorPoReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorLrReport" ? (
                        <VendorLrReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorAsnReport" ? (
                        <VendorAsnReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "vendorInspectionReport" ? (
                        <VendorInspectionReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "retailerInspectionReport" ? (
                        <RetailerInspectionReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "activityReport" ? (
                        <VendorActivityReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "loginLogoutReport" ? (
                        <VendorLoginLogoutReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "orderRequest" ? (
                        <OrderRequest {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "logisticsInventoryReport" ? (
                        <LogisticsInventoryReport {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {hash == "logisticsPaymentReconciliation" ? (
                        <LogisticsPaymentReconciliation {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) :null}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

                </div>
                <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        updateHeader:state.orders,
        changeSetting: state.changeSetting,
        logistic: state.logistic,
        replenishment: state.replenishment,
        orders: state.orders,
        shipment: state.shipment,
        administration: state.administration,
        seasonPlanning: state.seasonPlanning,
        vendor: state.vendor,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VendorPortal);