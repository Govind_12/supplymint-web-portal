import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ManageVendorKyc from "../components/vendor/manageVendorKyc";
import * as actions from "../redux/actions";

import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';

const SideBar = React.lazy(() => import("../components/sidebar"));
const ManageVendor = React.lazy(() => import("../components/vendor/manageVendor"));
const Contracts = React.lazy(() => import("../components/vendor/contracts"));
const RightSideBar = React.lazy(() => import("../components/rightSideBar"));
const Footer = React.lazy(() => import('../components/footer'));
const openRack = React.lazy(() => import("../assets/open-rack.svg"));
const openRackRight = React.lazy(() => import("../assets/rack-open-right.svg"));
const BraedCrumps = React.lazy(() => import("../components/breadCrumps"));
const NewSideBar = React.lazy(() => import("../components/newSidebar"));

class Vendor extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        rightbar: false,
        openRightBar: false,
        loader: false,
        errorMessage: "",
        errorCode: "",
        code: "",
        successMessage: "",
        success: false,
        alert: false,
      };
  }

  componentDidMount() {
    document.addEventListener("keydown", this.escFun, false);
    document.addEventListener("click", this.escFun, false);
  }

    componentWillUnmount() {
      document.removeEventListener("keydown", this.escFun, false);
      document.removeEventListener("click", this.escFun, false);
  }
  escFun = (e) =>{
      if( e.keyCode == 27 || (e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
          this.setState({ loader: false, success: false, alert: false, })
      }
  }

  componentWillReceiveProps(nextProps) {
      if (nextProps.vendor.getKycDetails.isSuccess) {
          this.setState({
              loader: false,
          })
          //this.props.getKycDetailsClear()
      }  else if (nextProps.vendor.getKycDetails.isError) {
          this.setState({
              loader: false,
              alert: true,
              code: nextProps.vendor.getKycDetails.message.status,
              errorCode: nextProps.vendor.getKycDetails.message.error == undefined ? undefined : nextProps.vendor.getKycDetails.message.error.errorCode,
              errorMessage: nextProps.vendor.getKycDetails.message.error == undefined ? undefined : nextProps.vendor.getKycDetails.message.error.errorMessage
          })
          this.props.getKycDetailsClear()
      }
      if (nextProps.vendor.getKycDetails.isLoading ) {
          this.setState({
              loader: true
          })
      }
  }

  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  onError(e) {
    e.preventDefault();
    this.setState({
        alert: false
    });
    document.onkeydown = function (t) {
        if (t.which) {
            return true;
        }
    }
  }
  onRequest(e) {
      e.preventDefault();
      this.setState({
          success: false,
      });
  }

  render() {
    $("input").attr("autocomplete", "off");
    const hash = window.location.hash.split("/")[2];
    return (
      <div className="container-fluid nc-design pad-l50">
        <NewSideBar {...this.props} />
        <SideBar {...this.props} />
        <div className="container_div m-top-75 " id="">
          <div className="col-md-12 col-sm-12 border-btm">
            <div className="menu_path d-table n-menu-path">
              <ul className="list-inline width_100 pad20 nmp-inner">
                <BraedCrumps {...this.props} />
              </ul>
            </div>
          </div>
        </div>
        {hash == "manageVendors" ? (
          <ManageVendor {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : hash == "contracts" ? (
          <Contracts {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : hash == "manageKyc" ? (
          <ManageVendorKyc {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null}
        <Footer />
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    vendor: state.vendor,
    replenishment: state.replenishment,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Vendor);
