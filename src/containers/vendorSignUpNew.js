import React from 'react';
import back from "../assets/backArrow.svg";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import * as actions from "../redux/actions";
import { parseJwt } from "../helper/index"
import logo from "../assets/supplylogo.svg";
import vendorPortal from "../assets/vendorPortall.png"
import FilterLoader from "../components/loaders/filterLoader";
import RequestError from '../components/loaders/requestError'
// export const VendorSignUp = () => {
class VendorSignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vendorName: "",
            vendorCode: "",
            contactName: "",
            contactNumber: "",
            email: "",
            newUserName: "",
            newPassword: "",
            userNameerr: "",
            passworderr: "",
            userId: "",
            firstName: "",
            lastName: "",
            workPhone: "",
            orgId: "",
            accessMode: "",
            status: "Active",
            address: "",
            partnerEnterpriseId: "",
            partnerEnterpriseNameE: "",
            userNameE: "",
            selected: [],
            uType: "",

            password: "",

            success: false,
            alert: false,
            loader: false,
            errorMessage: "",
            successMessage: "",


            errorCode: "",
            code: "",
            type: 'password',



        }
    }
    componentWillMount() {
        let location = window.location.href

        let keyValue = location.split("vendorSignUp?")[1]
        var search = new URLSearchParams(keyValue)
        let token = ""
        for (var key of search.values()) {
            token = key
        }
        let decode = token != "" ? parseJwt(token) : ""
        sessionStorage.setItem('token', token)
        if (decode != "") {
            this.setState({
                contactName: decode.aud != undefined ? decode.aud : "",
                partnerEnterpriseId: decode.eid != undefined ? decode.eid : "",
                email: decode.eml != undefined ? decode.eml : "",
                contactNumber: decode.mob != undefined ? decode.mob : "",
                orgId: decode.orgId != undefined ? decode.orgId : "",
                userNameE: decode.prn != undefined ? decode.prn : "",
                password: decode.pwd != undefined ? decode.pwd : "",
                uType: decode.uType != undefined ? decode.uType : "",
                partnerEnterpriseNameE: decode.enm != undefined ? decode.enm : "",
                vendorCode: decode.slCode != undefined ? decode.slCode : "",
                vendorName: decode.slName != undefined ? decode.slName : "",
                userId: decode.jti != undefined ? decode.jti : 0
            })

        }


    }
    handleClick = () => this.setState(({type}) => ({
        type: type === 'password' ? 'text' : 'password'
    }))

    onChange(e) {
        e.preventDefault();
        if (e.target.id == "userName") {
            this.setState({
                newUserName: e.target.value
            }, () => {
                this.userName();
            })
        } else if (e.target.id == "password") {
            this.setState({
                newPassword: e.target.value
            }, () => {
                this.password();
            })
        }
    }
    userName() {
        if (this.state.newUserName == "" || this.state.newUserName.trim() == "" || this.state.newUserName.split(" ").length > 1 || !/^[a-z_\-]+$/.test(this.state.newUserName)) {
            this.setState({
                userNameerr: true
            });
        } else {
            this.setState({
                userNameerr: false
            });
        }
    }
    password() {
        if (this.state.newPassword == "" || !this.state.newPassword.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/)) {
            this.setState({
                passworderr: true
            });
        } else {
            this.setState({
                passworderr: false
            });
        }
    }

    onsubmit() {

        this.userName();
        this.password();
        setTimeout(() => {
            const { newUserName, newPassword, unameerr, passworderr } = this.state;
            if (!unameerr && !passworderr) {

                let loginData = {
                    userId: this.state.userId,
                    firstNameE: this.state.firstName,
                    middleNameE: this.state.middleName,
                    lastNameE: this.state.lastName,
                    emailE: "",
                    mobileNumber: "",
                    workPhone: this.state.workPhone,
                    accessMode: this.state.accessMode,
                    active: this.state.status,
                    address: this.state.address,
                    partnerEnterpriseId: this.state.partnerEnterpriseId,
                    partnerEnterpriseNameE: this.state.partnerEnterpriseNameE,
                    userNameE: this.state.userNameE,
                    password: this.state.password,
                    uType: this.state.uType,
                    newUserName: this.state.newUserName,
                    newPassword: this.state.newPassword,
                    orgId: this.state.orgId,
                    slCode: this.state.vendorCode,
                    slName: this.state.vendorName,


                }

                console.log("Vendor SignUp ---- "+loginData);

                this.props.editUserRequest(loginData);


            }



        }, 100)

    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.editUser.isLoading) {
            this.setState({
                loader: true,
            })

        }
        if (nextProps.administration.editUser.isSuccess) {
            this.setState({

                loader: false,

            })
            this.props.editUserClear();
            sessionStorage.removeItem('token')
            this.props.history.push('/');

        }
        else if (nextProps.administration.editUser.isError) {
            this.setState({
                errorMessage: nextProps.administration.editUser.message.error == undefined ? undefined : nextProps.administration.editUser.message.error.errorMessage,
                code: nextProps.administration.editUser.message.status,
                alert: true,
                success: false,
                errorCode: nextProps.administration.editUser.message.error == undefined ? undefined : nextProps.administration.editUser.message.error.errorCode,
                loader: false
            })
            this.props.editUserClear();
        }

    }
    render() {
        const { vendorCode, vendorName, contactName, contactNumber, email, newUserName, newPassword, userNameerr, passworderr } = this.state
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-90">
                    <div className="vendor-registration-header">
                        <div className="vrh-left">
                            <div className="vrh-logo">
                                <img src={require('../assets/enterpriseLogo.svg')} />
                            </div>
                            <div className="vrh-privacy">
                                <a href="https://www.supplymint.com/#/privacy-policy">Privacy</a>
                                <a href="https://www.supplymint.com/#/terms-of-use">Terms</a>
                            </div>
                        </div>
                        <div className="vrh-right">
                            <button type="button" onClick={() => this.props.history.push('/')}>Sign in</button>
                        </div>
                    </div>
                </div>
                <div className="col-lg-7 col-md-7 col-sm-12 pad-0">
                    <form onSubmit={(e) => this.onsubmit(e)} autoComplete="off" className="m0">
                        <div className="vendor-sign-up">
                            <div className="vsu-body">
                                <div className="vsub-head">
                                    <h3>Vendor Registration</h3>
                                    <p>Welcome <span>Tanvi Creations Pvt Ltd</span><br /> We'll made registration easy for you! just select your desired username and password and go ahead. </p>
                                </div>
                                <div className="vsub-form m-top-10">
                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0 border-bottom">
                                        <div className="col-md-6 col-sm-6 pad-lft-0">
                                            <div className="vsubf-fields">
                                                <h5>Vendor Name</h5>
                                                <p className="">{this.state.vendorName}</p>
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 pad-rgt-0">
                                            <div className="vsubf-fields">
                                                <h5>Vendor Code</h5>
                                                <p>{this.state.vendorCode}</p>
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 pad-lft-0">
                                            <div className="vsubf-fields">
                                                <h5>Contact Name</h5>
                                                <p>{this.state.contactName}</p>
                                            </div>
                                        </div>
                                        {/* <div className="col-md-6 col-sm-6 pad-rgt-0">
                                            <div className="vsubf-input">
                                                <h5>Contact Number</h5>
                                                <p>{this.state.contactNumber}</p>
                                            </div>
                                        </div> */}
                                        <div className="col-md-6 col-sm-6 pad-rgt-0">
                                            <div className="vsubf-fields">
                                                <h5>Account Email</h5>
                                                <p>{this.state.email}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                        <div className="col-md-6 col-sm-6 pad-lft-0">
                                            <div className="vsubf-input">
                                                <label>Username</label>
                                                <input type="text" autoComplete="off" value={newUserName} id="userName" placeholder="Username or Email" onChange={(e) => this.onChange(e)} />
                                                <span className="show-hide-pass-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="13.886" height="13.886" viewBox="0 0 13.886 13.886">
                                                        <path fill="#b4c5d6" d="M13.886 6.943a.542.542 0 1 1-1.085 0A5.858 5.858 0 1 0 6.943 12.8a5.793 5.793 0 0 0 2.68-.648.542.542 0 1 1 .5.964 6.944 6.944 0 1 1 3.765-6.175zm-.655 1.974a.542.542 0 0 1 .015.767 2.291 2.291 0 0 1-3.676-.516 3.442 3.442 0 1 1 .818-2.225V8.1a1.207 1.207 0 0 0 2.077.836.542.542 0 0 1 .766-.019zM9.3 6.943A2.359 2.359 0 1 0 6.943 9.3 2.362 2.362 0 0 0 9.3 6.943z"/>
                                                    </svg>
                                                </span>
                                                {userNameerr ? (
                                                    <span className="error">
                                                        Username must only contain small letters<br />
                                                        {/* (must contain at least 1 lowercase alphabet, at least 1 uppercase alphabet, at least 1 numeric character ,at least one special character and must be eight characters or longer) */}
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 pad-rgt-0">
                                            <div className="vsubf-input">
                                                <label>Password</label>
                                                <input type={this.state.type} autoComplete="off" value={newPassword} placeholder="Min 8 alphanumeric letters" id="password" onChange={(e) => this.onChange(e)} />
                                                <span className="show-hide-pass-btn" onClick={this.handleClick}>{this.state.type === "password" ? <img src={require('../assets/eye-open.svg')} /> :<img src={require('../assets/hide.svg')} />} </span>
                                                {passworderr ? (
                                                    <span className="error">
                                                        Enter valid password<br />
                                                        (must contain at least 1 lowercase alphabet, at least 1 uppercase alphabet, at least 1 numeric character ,at least one special character and must be eight characters or longer)
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="vsubf-footer m-top-30">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" />
                                                <span className="checkmark1"></span>
                                                You are agreeing to the <a href="https://www.supplymint.com/#/terms-of-use">Terms of services</a> and <a href="https://www.supplymint.com/#/privacy-policy">Privacy Policy</a>
                                            </label>
                                            <button type="button" onClick={(e) => this.onsubmit(e)}>Register Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-lg-5 col-md-5 col-sm-12 pad-0">
                    <div className="vendor-sign-up-right">
                        <div className="vsur-inner">
                            <div className="vsuri-body">
                                <div className="vsur-img">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="add_2_" width="50.665" height="50.665" viewBox="0 0 55.665 55.665">
                                        <path fill="#fff" id="Path_1114" d="M22.034 44.749H5.8A5.805 5.805 0 0 1 0 38.95V8.8A5.805 5.805 0 0 1 5.8 3h4.639a1.16 1.16 0 0 1 0 2.319H5.8A3.483 3.483 0 0 0 2.319 8.8v30.15A3.483 3.483 0 0 0 5.8 42.429h16.234a1.16 1.16 0 1 1 0 2.319z" class="cls-1" transform="translate(0 3.958)"/>
                                        <path fill="#fff" id="Path_1115" d="M22.437 16.916a1.16 1.16 0 0 1-1.16-1.16V8.8A3.483 3.483 0 0 0 17.8 5.319h-4.64a1.16 1.16 0 1 1 0-2.319h4.64a5.805 5.805 0 0 1 5.8 5.8v6.958a1.16 1.16 0 0 1-1.163 1.158z" class="cls-1" transform="translate(15.833 3.958)"/>
                                        <path fill="#fff" id="Path_1116" d="M21.4 13.916H7.479A3.483 3.483 0 0 1 4 10.437V5.8a1.16 1.16 0 0 1 1.16-1.16h3.6a5.8 5.8 0 0 1 11.365 0h3.6a1.16 1.16 0 0 1 1.16 1.16v4.639a3.483 3.483 0 0 1-3.485 3.477zM6.319 6.958v3.479a1.162 1.162 0 0 0 1.16 1.16H21.4a1.162 1.162 0 0 0 1.16-1.16V6.958h-3.484a1.16 1.16 0 0 1-1.16-1.16 3.479 3.479 0 0 0-6.958 0A1.16 1.16 0 0 1 9.8 6.958z" class="cls-1" transform="translate(5.278)"/>
                                        <path fill="#fff" id="Path_1117" d="M26.076 41.152a15.076 15.076 0 1 1 15.076-15.076 15.093 15.093 0 0 1-15.076 15.076zm0-27.833a12.757 12.757 0 1 0 12.757 12.757 12.772 12.772 0 0 0-12.757-12.757z" class="cls-1" transform="translate(14.513 14.513)"/>
                                        <path fill="#fff" id="Path_1118" d="M18.16 30.236a1.16 1.16 0 0 1-1.16-1.16V15.16a1.16 1.16 0 1 1 2.319 0v13.916a1.16 1.16 0 0 1-1.159 1.16z" class="cls-1" transform="translate(22.429 18.471)"/>
                                        <path fill="#fff" id="Path_1119" d="M29.076 19.319H15.16a1.16 1.16 0 1 1 0-2.319h13.916a1.16 1.16 0 0 1 0 2.319z" class="cls-1" transform="translate(18.471 22.429)"/>
                                        <path fill="#fff" id="Path_1120" d="M27.354 10.319H4.16A1.16 1.16 0 0 1 4.16 8h23.194a1.16 1.16 0 0 1 0 2.319z" class="cls-1" transform="translate(3.958 10.555)"/>
                                        <path fill="#fff" id="Path_1121" d="M20.4 13.319H4.16a1.16 1.16 0 1 1 0-2.319H20.4a1.16 1.16 0 1 1 0 2.319z" class="cls-1" transform="translate(3.958 14.513)"/>
                                        <path fill="#fff" id="Path_1122" d="M15.757 16.319H4.16a1.16 1.16 0 1 1 0-2.319h11.6a1.16 1.16 0 1 1 0 2.319z" class="cls-1" transform="translate(3.958 18.471)"/>
                                    </svg>
                                </div>
                                <div className="vsur-body">
                                    <p>Welcome to</p>
                                    <h1>Vendor Registration <br />Portal</h1>
                                    <span>Once you signup you can edit your <br /> profile and reserve your <br /> spaces</span>
                                    <button type="button">Know More</button>
                                </div>
                            </div>
                            <div className="vsuri-footer">
                                <a href="https://www.facebook.com/supplymint1/">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.379" height="18.757" viewBox="0 0 9.379 18.757">
                                        <path fill="#fff" d="M13.854 3.114h1.712V.132A22.112 22.112 0 0 0 13.072 0C10.6 0 8.911 1.553 8.911 4.407v2.627H6.187v3.334h2.724v8.389h3.34v-8.388h2.614l.415-3.334h-3.03v-2.3c0-.964.26-1.623 1.6-1.623z" transform="translate(-6.187)"/>
                                    </svg>
                                </a>
                                <a href="https://twitter.com/supplymint">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18.148" height="15.558" viewBox="0 0 19.148 15.558">
                                        <g>
                                            <g>
                                                <path fill="#fff" d="M19.148 49.842a8.184 8.184 0 0 1-2.262.62 3.9 3.9 0 0 0 1.727-2.17 7.845 7.845 0 0 1-2.489.95 3.925 3.925 0 0 0-6.79 2.684 4.042 4.042 0 0 0 .091.9 11.111 11.111 0 0 1-8.091-4.106 3.927 3.927 0 0 0 1.206 5.247 3.877 3.877 0 0 1-1.774-.483v.043a3.944 3.944 0 0 0 3.145 3.857 3.918 3.918 0 0 1-1.029.129 3.471 3.471 0 0 1-.743-.067 3.963 3.963 0 0 0 3.668 2.735A7.888 7.888 0 0 1 .939 61.85 7.352 7.352 0 0 1 0 61.8a11.051 11.051 0 0 0 6.022 1.762 11.1 11.1 0 0 0 11.173-11.17c0-.174-.006-.341-.014-.507a7.831 7.831 0 0 0 1.967-2.043z" transform="translate(0 -48)"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <a href="https://www.linkedin.com/showcase/supplymint">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="linkedin_2_" width="18.208" height="18.988" viewBox="0 0 19.208 19.988">
                                        <path fill="#fff" id="Path_1108" d="M17.813 161.387h4.395v13.688h-4.395z" class="cls-1" transform="translate(-17.508 -155.086)"/>
                                        <path fill="#fff" id="Path_1109" d="M12.5 0A2.5 2.5 0 1 0 15 2.5 2.505 2.505 0 0 0 12.5 0z" class="cls-1" transform="translate(-10)"/>
                                        <path fill="#fff" id="Path_1110" d="M191.415 155.26a4.678 4.678 0 0 0-4.754-4.549 4.749 4.749 0 0 0-3.684 1.735v-1.318H178.8v13.688h4.395V156.9a1.912 1.912 0 1 1 3.824 0v7.912h4.395z" class="cls-1" transform="translate(-172.209 -144.827)"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <p className="copywrite-text">&copy; 2020 Supplymint</p>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div >
        )
    }
}
export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VendorSignUp);