import React from 'react';
import bokeh from "../assets/bokeh.svg"
class OnBoarding extends React.Component {
    render() {
        
        return (
            <div className="container-fluid pad-0">
                <div className="colorBackground">
                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                        <div className="col-md-4 col-sm-4 col-xs-12 pad-0">
                            <div className="onboardingLeft">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-100">
                                    <div className="imgBokhDiv">
                                        <img src={bokeh} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-50">
                                    <div className="contentBokh">
                                        <h2>
                                            Congratulations
                                    </h2>
                                        <p>
                                            You have Successfully Registered with Supplymint !
                                    </p>
                                    </div>
                                </div>
                            </div>
                            <div className="footer FooterOnboarding text-right width_100">
                                <span>
                                    Copyright@supplymint
                                </span>
                            </div>
                        </div>
                        <div className="col-md-8 col-sm-8 col-xs-12 pad-0">
                            <div className="onboardingRight">
                                <div className="col-md-12 col-sm-12 pad-0">
                                    <div className="WelcomeContent">
                                        <h2>
                                            Welcome
                                    </h2>
                                        <span>
                                            Suman Dobriyal
                                    </span>
                                    </div>
                                </div>

                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="cardOnboarding">
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <div className="col-md-8 pad-0">
                                                <div className="col-md-12 col-sm-12 pad-0">
                                                    <label className="headingForm">
                                                        Organisation
                                                </label>
                                                    <label className="subheadForm">
                                                        Turningcloud Solutions
                                                </label>
                                                </div>
                                                <div className="col-md-12 col-sm-12 pad-0 m-top-70">
                                                    <label className="headingForm">
                                                        Last Name
                                                </label>
                                                    <label className="subheadForm">
                                                        Dobriyal
                                                </label>
                                                </div>
                                            </div>
                                            <div className="col-md-4 pad-0">
                                                <div className="col-md-12 col-sm-12 pad-0">
                                                    <label className="headingForm">
                                                        First Name
                                                </label>
                                                    <label className="subheadForm">
                                                        Suman
                                                </label>
                                                </div>
                                                <div className="col-md-12 col-sm-12 pad-0 m-top-70">
                                                    <label className="headingForm">
                                                        Username
                                                </label>
                                                    <label className="subheadForm">
                                                        GS1384736ZZ
                                                </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-100">
                                    <button className="ProcessBtn" onClick={() => this.props.history.push("/home")}>
                                        PROCEED TO DASHBOARD
                                </button>
                                </div>
                                <div className="footer FooterOnboarding">
                                    <p>
                                        Note - Please refer help section for any kind of information and portal related queries.
                                    contact at <a><span>support@supplymint.com </span></a> case you are not satisfied with solution.
                                </p>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default OnBoarding;