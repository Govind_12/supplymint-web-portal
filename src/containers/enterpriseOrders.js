import React from 'react';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';
import RightSideBar from "../components/rightSideBar";
import { vendorRequest } from '../redux/vendor/action/index';
import EnterpeiseOpenPO from '../components/vendorPortal/enterprise/orders/enterpriseOpenPO';
import EnterpriseCancelledOrders from '../components/vendorPortal/enterprise/orders/enterpriseCancelledOrders';
import EnterprisePendingDelivery from '../components/vendorPortal/enterprise/orders/enterprisePendingDelivery';
import NewSettings from '../assets/new-settings.svg';
import Help from '../assets/help.svg';
import EnterpriseClosedPO from '../components/vendorPortal/enterprise/orders/enterpriseClosedPO';
import NewSideBar from '../components/newSidebar';

class EnterpriseOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            expandDetails: [],
            approvedPO: [],
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            isDownloadPage: false,
        };
    }
    componentWillMount() {
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }
    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
      componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
            this.setState({ loader: false, success: false, alert: false, isDownloadPage: false })
      }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.EnterpriseGetPoDetailsClear()
        } else if (nextProps.orders.EnterpriseGetPoDetails.isError) {
            this.setState({
                errorMessage: nextProps.orders.EnterpriseGetPoDetails.message.error == undefined ? undefined : nextProps.orders.EnterpriseGetPoDetails.message.error.errorMessage,
                errorCode: nextProps.orders.EnterpriseGetPoDetails.message.error == undefined ? undefined : nextProps.orders.EnterpriseGetPoDetails.message.error.errorCode,
                code: nextProps.orders.EnterpriseGetPoDetails.message.status,
                alert: true,
                loader: false
            })
            this.props.EnterpriseGetPoDetailsClear()
        }
        if (nextProps.orders.EnterpriseApprovedPo.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.EnterpriseApprovedPoClear()
        } else if (nextProps.orders.EnterpriseApprovedPo.isError) {
            this.setState({
                errorMessage: nextProps.orders.EnterpriseApprovedPo.message.error == undefined ? undefined : nextProps.orders.EnterpriseApprovedPo.message.error.errorMessage,
                errorCode: nextProps.orders.EnterpriseApprovedPo.message.error == undefined ? undefined : nextProps.orders.EnterpriseApprovedPo.message.error.errorCode,
                code: nextProps.orders.EnterpriseApprovedPo.message.status,
                alert: true,
                loader: false
            })
            this.props.EnterpriseApprovedPoClear()
        }

        if (nextProps.orders.EnterpriseCancelPo.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.orders.EnterpriseCancelPo.data.message
            })
            this.props.EnterpriseCancelPoClear()
        } else if (nextProps.orders.EnterpriseCancelPo.isError) {
            this.setState({
                errorMessage: nextProps.orders.EnterpriseCancelPo.message.error == undefined ? undefined : nextProps.orders.EnterpriseCancelPo.message.error.errorMessage,
                errorCode: nextProps.orders.EnterpriseCancelPo.message.error == undefined ? undefined : nextProps.orders.EnterpriseCancelPo.message.error.errorCode,
                code: nextProps.orders.EnterpriseCancelPo.message.status,
                alert: true,
                loader: false
            })
            this.props.EnterpriseCancelPoClear()
        }
        if (nextProps.orders.vendorCreateSr.isSuccess) {
            this.setState({
                loader: false
            })
            let payload = {
                no: 1,
                type: 1,
                search: "",
                status: "APPROVED",
                poDate: "",
                createdOn: "",
                poNumber: "",
                vendorName: "",
                userType: "vendorpo",
            }
            this.props.EnterpriseApprovedPoRequest(payload)
            this.props.vendorCreateSrClear()
        } else if (nextProps.orders.vendorCreateSr.isError) {
            this.setState({
                errorMessage: nextProps.orders.vendorCreateSr.message.error == undefined ? undefined : nextProps.orders.vendorCreateSr.message.error.errorMessage,
                errorCode: nextProps.orders.vendorCreateSr.message.error == undefined ? undefined : nextProps.orders.vendorCreateSr.message.error.errorCode,
                code: nextProps.orders.vendorCreateSr.message.status,
                alert: true,
                loader: false
            })
            this.props.vendorCreateSrClear()
        }

        //po closer
        if (nextProps.orders.poCloser.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.poCloserClear()
        } else if (nextProps.orders.poCloser.isError) {
            this.setState({
                errorMessage: nextProps.orders.poCloser.message.error == undefined ? undefined : nextProps.orders.poCloser.message.error.errorMessage,
                errorCode: nextProps.orders.poCloser.message.error == undefined ? undefined : nextProps.orders.poCloser.message.error.errorCode,
                code: nextProps.orders.poCloser.message.status,
                alert: true,
                loader: false
            })
            this.props.poCloserClear()
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getHeaderConfigClear()
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            })
            this.props.getHeaderConfigClear()
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createHeaderConfigClear()
        } else if (nextProps.replenishment.createHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            })
            this.props.createHeaderConfigClear()
        }
        //po closer End

        // find shipment asn

        if (nextProps.orders.findShipmentAdvice.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.findShipmentAdviceClear()
        } else if (nextProps.orders.findShipmentAdvice.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.findShipmentAdvice.message.status,
                errorCode: nextProps.orders.findShipmentAdvice.message.error == undefined ? undefined : nextProps.orders.findShipmentAdvice.message.error.errorCode,
                errorMessage: nextProps.orders.findShipmentAdvice.message.error == undefined ? undefined : nextProps.orders.findShipmentAdvice.message.error.errorMessage
            })
            this.props.findShipmentAdviceClear()
        }
        // --------------------
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({ loader: false })
            this.props.getAllCommentClear()
        } else if (nextProps.shipment.getAllComment.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.getAllComment.message.status,
                errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
                errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage
            })
            this.props.getAllCommentClear()
        }

        if (nextProps.shipment.qcAddComment.isSuccess) {
            this.setState({ loader: false })
            this.props.qcAddCommentClear()
        }
        else if (nextProps.shipment.qcAddComment.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.qcAddComment.message.status,
                errorCode: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorCode,
                errorMessage: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorMessage
            })
            this.props.qcAddCommentClear()
        }
        if (nextProps.orders.multipleDocumentDownload.isSuccess) {
            this.setState({
                successMessage: nextProps.orders.multipleDocumentDownload.data.message,
                loader: false,
                success: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
                isDownloadPage: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
            },()=>{
                if( nextProps.orders.multipleDocumentDownload.data.resource !== undefined && nextProps.orders.multipleDocumentDownload.data.resource != null )
                   window.open(`${nextProps.orders.multipleDocumentDownload.data.resource}`)       
            })
            this.props.multipleDocumentDownloadClear()
        }
        else if (nextProps.orders.multipleDocumentDownload.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.multipleDocumentDownload.message.status,
                errorCode: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorCode,
                errorMessage: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorMessage
            })
            this.props.multipleDocumentDownloadClear()
        }
        //create main header
        if (nextProps.replenishment.createMainHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createMainHeaderConfigClear()
        } else if (nextProps.replenishment.createMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
            })
            this.props.createMainHeaderConfigClear()
        }
        //create set header
        if (nextProps.replenishment.createSetHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createSetHeaderConfigClear()
        } else if (nextProps.replenishment.createSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage
            })
            this.props.createSetHeaderConfigClear()
        }
        //create dynamic  item header
        if (nextProps.replenishment.createItemHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createItemHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createItemHeaderConfigClear()
        } else if (nextProps.replenishment.createItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorMessage
            })
            this.props.createItemHeaderConfigClear()
        }

         //custom column setting heaader
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getMainHeaderConfigClear()
        } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
            })
            this.props.getMainHeaderConfigClear()
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getSetHeaderConfigClear()
        } else if (nextProps.replenishment.getSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorMessage
            })
            this.props.getSetHeaderConfigClear()
        }
        if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getItemHeaderConfigClear()
        } else if (nextProps.replenishment.getItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorMessage
            })
            this.props.getItemHeaderConfigClear()
        }
        if (nextProps.orders.cancelAndRejectReason.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (nextProps.orders.cancelAndRejectReason.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.cancelAndRejectReason.message.status,
                errorCode: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorCode,
                errorMessage: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorMessage
            })
            this.props.getCancelAndRejectReasonClear()
        }
        //loading api response
        if (nextProps.orders.multipleDocumentDownload.isLoading || 
            nextProps.orders.findShipmentAdvice.isLoading || 
            nextProps.replenishment.createHeaderConfig.isLoading || 
            nextProps.replenishment.getHeaderConfig.isLoading || 
            nextProps.orders.poCloser.isLoading || 
            nextProps.orders.EnterpriseApprovedPo.isLoading || 
            nextProps.orders.EnterpriseCancelPo.isLoading || 
            nextProps.orders.EnterpriseGetPoDetails.isLoading || 
            nextProps.orders.vendorCreateSr.isLoading ||
            nextProps.replenishment.getMainHeaderConfig.isLoading || 
            nextProps.replenishment.getSetHeaderConfig.isLoading || 
            nextProps.replenishment.getItemHeaderConfig.isLoading ||
            nextProps.replenishment.createMainHeaderConfig.isLoading || 
             nextProps.replenishment.createSetHeaderConfig.isLoading || 
             nextProps.replenishment.createItemHeaderConfig.isLoading ||
             nextProps.orders.cancelAndRejectReason.isLoading) {
            this.setState({
                loader: true
            })
        }
        //loading api response end
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false,
            isDownloadPage: false,
        });
    }
    render() {
        const hash = window.location.hash.split("/")[3];
        return (
            <div>
                <div className="container-fluid nc-design pad-l50">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-t-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                                {/* <div className="nmp-right">
                                    <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button>
                                    <button className="nmp-help">Need Help <img src={Help}></img></button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    {hash == "pendingOrders" ? (
                        <EnterpeiseOpenPO {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : hash == "cancelledOrders" ? (
                        <EnterpriseCancelledOrders {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : hash == "processedOrders" ? (
                        <EnterprisePendingDelivery {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : hash == "closedPo" ? (
                        <EnterpriseClosedPO {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : null}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
                <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        orders: state.orders,
        replenishment: state.replenishment,
        shipment: state.shipment
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EnterpriseOrders);