import React from 'react';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';
import * as actions from '../redux/actions';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from '../components/loaders/requestSuccess';
import RequestError from '../components/loaders/requestError';
import RightSideBar from "../components/rightSideBar";
import EnterpriseDashboard from '../components/vendorPortal/enterpriseDashboard';
import VendorDashboard from '../components/vendorPortal/vendorDashboard';
import NewSideBar from '../components/newSidebar';

class PortalDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            loader: false,
            successMessage: "",
            success: false,
            alert: false,
            errorMessage: "",
            errorCode: "",
            code: "",
        };
    }
    // componentWillMount() {
    //     if (sessionStorage.getItem('token') == null) {
    //         this.props.history.push('/');
    //     }
    // }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.dashboardBottom.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.dashboardBottomClear()
        }
        else if (nextProps.orders.dashboardBottom.isError) {
            this.setState({
                errorMessage: nextProps.orders.dashboardBottom.message.error == undefined ? undefined : nextProps.orders.dashboardBottom.message.error.errorMessage,
                errorCode: nextProps.orders.dashboardBottom.message.error == undefined ? undefined : nextProps.orders.dashboardBottom.message.error.errorCode,
                code: nextProps.orders.dashboardBottom.message.status,
                alert: true,
                loader: false
            })
            this.props.dashboardBottomClear()
        }

        if (nextProps.orders.dashboardBottom.isLoading) {
            this.setState({ loader: true })
        }
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    render() {
        console.log("in portal dashboard")
        const hash = window.location.hash.split("/")[2];
        return (
            <div className="container-fluid nc-design pad-l60">
                <NewSideBar {...this.props} />
                <SideBar {...this.props} />
                {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                <div className="container_div m-top-75 m-t-75" id="">
                    <div className="col-md-12 col-sm-12 border-btm">
                        <div className="menu_path n-menu-path">
                            <ul className="list-inline width_100 pad20 nmp-inner">
                                <BraedCrumps {...this.props} />
                            </ul>
                        </div>
                    </div>
                </div>
                {hash == "enterpriseDashboard" ? (
                    <EnterpriseDashboard {...this.props} rightSideBar={() => this.onRightSideBar()} />
                ) : hash == "vendorDashboard" ? (
                    <VendorDashboard {...this.props} rightSideBar={() => this.onRightSideBar()} />
                ) : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

                <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        orders: state.orders,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PortalDashboard);