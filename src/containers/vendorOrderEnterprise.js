import React from 'react';
import ConfirmedPo from '../components/vendorPortal/vendorOrderEnterprise/confirmedPo';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';


export default class EnterpriseOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false
        };
    }
    // componentWillMount(){
    //     if(sessionStorage.getItem('token') == null){
    //       this.props.history.push('/');
    //     }
    //   }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    render() {
        const hash = window.location.hash.split("/")[2];
        return (
            <div className="container-fluid">
                <SideBar {...this.props} />
                <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                <div className="container_div m-top-100 " id="">
                    <div className="col-md-12 col-sm-12">
                        <div className="menu_path">
                            <ul className="list-inline width_100 pad20">
                                <BraedCrumps {...this.props} />
                            </ul>
                        </div>
                    </div>
                </div>
                {hash == "confirmedPo" ? (
                    <ConfirmedPo rightSideBar={() => this.onRightSideBar()} />
                ) : null}
                <Footer />
            </div>
        )
    }
}