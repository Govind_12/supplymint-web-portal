import React from 'react';
import SideBar from '../components/sidebar';
import RightSideBar from '../components/rightSideBar';
import BraedCrumps from '../components/breadCrumps';
import openRack from "../assets/open-rack.svg"
import UserTabVendor from '../components/vendorPortal/vendorAdminScreens/userManageTab';
import ManageSubscriptionTab from '../components/vendorPortal/vendorAdminScreens/manageSubscriptionTab';
import Footer from '../components/footer';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../redux/actions';
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from '../components/loaders/requestSuccess';
import RequestError from '../components/loaders/requestError';
import NewSideBar from '../components/newSidebar';


class VendorAdmin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            openRightBar: false,
            tab: "user",
            loader: false,
            success: false,
            errorMessage: "",
            successMessage: "",
            alert: false,
            errorCode: "",
            code: ""
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.allRetailer.isSuccess) {
            this.setState({ loader: false })
            // this.props.allRetailerClear()
        }
        else if (nextProps.administration.allRetailer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.allRetailer.message.status,
                errorCode: nextProps.administration.allRetailer.message.error == undefined ? undefined : nextProps.administration.allRetailer.message.error.errorCode,
                errorMessage: nextProps.administration.allRetailer.message.error == undefined ? undefined : nextProps.administration.allRetailer.message.error.errorMessage
            })
            this.props.allRetailerClear()
        }
        if (nextProps.administration.vendorUser.isSuccess) {
        }
        if (nextProps.administration.addUser.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.addUser.data.message,
                success: true,
                loader: false,
                alert: false,
              })
        }
        else if (nextProps.administration.addUser.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.addUser.message.status,
                errorCode: nextProps.administration.addUser.message.error == undefined ? undefined : nextProps.administration.addUser.message.error.errorCode,
                errorMessage: nextProps.administration.addUser.message.error == undefined ? undefined : nextProps.administration.addUser.message.error.errorMessage
            })
            this.props.addUserClear()
        }
        if (nextProps.administration.vendorUser.isSuccess) {
            this.setState({ loader: false })
        }
        else if (nextProps.administration.vendorUser.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.vendorUser.message.status,
                errorCode: nextProps.administration.vendorUser.message.error == undefined ? undefined : nextProps.administration.vendorUser.message.error.errorCode,
                errorMessage: nextProps.administration.vendorUser.message.error == undefined ? undefined : nextProps.administration.vendorUser.message.error.errorMessage
            })
            this.props.vendorUserClear()
        }
        if (nextProps.administration.editUser.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.editUser.data.message,
                success: true,
                loader: false,
                alert: false,
              })
        }
        else if (nextProps.administration.editUser.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.editUser.message.status,
                errorCode: nextProps.administration.editUser.message.error == undefined ? undefined : nextProps.administration.editUser.message.error.errorCode,
                errorMessage: nextProps.administration.editUser.message.error == undefined ? undefined : nextProps.administration.editUser.message.error.errorMessage
            })
            this.props.editUserClear();
        }
        if (nextProps.administration.userStatus.isSuccess) {
            this.setState({
              successMessage: nextProps.administration.userStatus.data.message,
              success: true,
              loader: false,
              alert: false,
            })
            this.props.userStatusClear();
      
        }
        else if (nextProps.administration.userStatus.isError) {
            this.setState({
              errorMessage: nextProps.administration.userStatus.message.error == undefined ? undefined : nextProps.administration.userStatus.message.error.errorMessage,
              code: nextProps.administration.userStatus.message.status,
              alert: true,
              success: false,
              errorCode:  nextProps.administration.userStatus.message.error == undefined ? undefined : nextProps.administration.userStatus.message.error.errorCode,
              loader: false
            })
            this.props.userStatusClear();
        }
        if (nextProps.administration.getSubscription.isSuccess) {
            this.setState({
             loader: false,
            })
            this.props.getSubscriptionClear();
        }
        else if (nextProps.administration.getSubscription.isError) {
            this.setState({
              errorMessage: nextProps.administration.getSubscription.message.error == undefined ? undefined : nextProps.administration.getSubscription.message.error.errorMessage,
              code: nextProps.administration.getSubscription.message.status,
              alert: true,
              success: false,
              errorCode:  nextProps.administration.getSubscription.message.error == undefined ? undefined : nextProps.administration.getSubscription.message.error.errorCode,
              loader: false
            })
            this.props.getSubscriptionClear();
        }
        if (nextProps.administration.getTransaction.isSuccess) {
            this.setState({
             loader: false,
            })
            this.props.getTransactionClear();
        }
        else if (nextProps.administration.getTransaction.isError) {
            this.setState({
              errorMessage: nextProps.administration.getTransaction.message.error == undefined ? undefined : nextProps.administration.getTransaction.message.error.errorMessage,
              code: nextProps.administration.getTransaction.message.status,
              alert: true,
              success: false,
              errorCode:  nextProps.administration.getTransaction.message.error == undefined ? undefined : nextProps.administration.getTransaction.message.error.errorCode,
              loader: false
            })
            this.props.getTransactionClear();
        } 
        if (nextProps.administration.allRetailer.isLoading || nextProps.administration.addUser.isLoading || nextProps.administration.vendorUser.isLoading 
            || nextProps.administration.editUser.isLoading || nextProps.administration.userStatus.isLoading
            || nextProps.administration.getSubscription.isLoading || nextProps.administration.getTransaction.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
          success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
          alert: false
        });
        document.onkeydown = function (t) {
          if (t.which) {
            return true;
          }
        }
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    changeTabCS(tab) {
        this.setState({
            tab
        })
    }
    render() {
        return (
            <div>
                <div className="container-fluid nc-design">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                    <div className="container_div m-top-75 pad-l60" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path">
                                {/* <ul className="list-inline width_100 pad20">
                                    <BraedCrumps {...this.props} />
                                </ul> */}
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid pad-0 pad-l50">
                        <div className="container_div" id="home">
                            <div className="container-fluid pad-0">
                                <div className="container_div" id="">
                                    <div className="container-fluid pad-0">
                                        <div className="replenishment_container vendor-admin-user">
                                            <div className="col-md-12 col-sm-12 col-xs-12 itemUdfSet pad-0 changeSettingContain">
                                                <div className="switchTabs p-lr-47">
                                                    <ul className="list_style m0">
                                                        <li onClick={() => this.changeTabCS('user')} className={this.state.tab == "user" ? "displayPointer activeTab m-rgt-25" : "m-rgt-25 displayPointer"}>
                                                            <label className="displayPointer">User</label>
                                                        </li>
                                                        <li onClick={() => this.changeTabCS('subscription')} className={this.state.tab == "subscription" ? "displayPointer activeTab m-rgt-25" : "m-rgt-25 displayPointer"}>
                                                            <label className="displayPointer">Subscription</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                {this.state.tab == "user" ? <UserTabVendor {...this.props} {...this.state}/> :
                                                    this.state.tab == "subscription" ? <ManageSubscriptionTab {...this.props} {...this.state}/> : ""}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
                <Footer />
            </div>
        )
    }
}
export function mapStateToProps(state) {
    return {
        administration: state.administration
    }
}

function mapDisapatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}
export default connect(mapStateToProps, mapDisapatchToProps)(VendorAdmin)