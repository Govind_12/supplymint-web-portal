import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import StoreProfile from "../components/analytics/storeProfile";
import SideBar from "../components/sidebar";
import Footer from "../components/footer";
import BraedCrumps from "../components/breadCrumps";
import ChooseStoreProfile from "../components/analytics/chooseStoreProfile";
import FilterLoader from "../components/loaders/filterLoader";
import RequestError from "../components/loaders/requestError";
import RequestSuccess from "../components/loaders/requestSuccess";
import { SellThru } from "../components/analytics/sellThru";
import NewSideBar from "../components/newSidebar";
import PlannerWorkQueue2 from "../components/analytics/plannerWorkQueue2";
import PlannerWorkSheet from "../components/analytics/plannerWorkSheet";
import InventoryClassification from "../components/analytics/inventoryClassification";


class Analytics extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rightbar: false,
      openRightBar: false,
      sideBar: true,
      loader: false,
      errorMessage: "",
      errorCode: "",
      success: false,
      alert: false,
      successMessage: "",
    };
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }



  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  componentDidMount() {
    document.addEventListener("keydown", this.escFun, false);
    document.addEventListener("click", this.escFun, false);
  }

    componentWillUnmount() {
      document.removeEventListener("keydown", this.escFun, false);
      document.removeEventListener("click", this.escFun, false);
  }
  escFun = (e) =>{
    if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
      this.setState({ loader: false, success: false, alert: false})
    }
  }

  componentWillMount() {
    if (window.location.hash == "#/analytics") {
      if (sessionStorage.getItem("storeCode") != null) {
        this.props.history.push("/analytics/storeProfile")
      }
      else {
        this.props.history.push("/analytics/chooseStoreProfile")
      }
    } else if (window.location.hash == "#/analytics/storeProfile" && sessionStorage.getItem("storeCode") == null) {
      this.props.history.push("/analytics/chooseStoreProfile")
    }

  }
  componentWillReceiveProps(nextProps) {

    if (nextProps.analytics.getStoreProfileCode.isSuccess) {
      if (nextProps.analytics.getStoreProfileCode.data.resource != null) {
        sessionStorage.setItem("storeCode", JSON.stringify(nextProps.analytics.getStoreProfileCode.data.resource))
      } else {
        sessionStorage.removeItem("storeCode")
      }
      this.refs.child != null ? this.refs.child.updateStoreData() : null;
      this.props.getStoreProfileCodeClear();

    } else if (nextProps.analytics.getStoreProfileCode.isError) {
      this.setState({
        errorMessage: nextProps.analytics.getStoreProfileCode.message.error == undefined ? undefined : nextProps.analytics.getStoreProfileCode.message.error.errorMessage,

        errorCode: nextProps.analytics.getStoreProfileCode.message.error == undefined ? undefined : nextProps.analytics.getStoreProfileCode.message.error.errorCode,
        code: nextProps.analytics.getStoreProfileCode.message.status,
        alert: true,
      })
      this.props.getStoreProfileCodeClear()

    } else if (!nextProps.analytics.getStoreProfileCode.isLoading) {
      this.setState({
        loader: false
      })
    }

    if (nextProps.analytics.getStoreData.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.getStoreDataClear()

    } else if (nextProps.analytics.getStoreData.isError) {
      this.setState({
        errorMessage: nextProps.analytics.getStoreData.message.error == undefined ? undefined : nextProps.analytics.getStoreData.message.error.errorMessage,
        errorCode: nextProps.analytics.getStoreData.message.error == undefined ? undefined : nextProps.analytics.getStoreData.message.error.errorCode,
        code: nextProps.analytics.getStoreData.message.status,
        alert: true,
      })
      this.props.getStoreDataClear()

    } else if (!nextProps.analytics.getStoreData.isLoading) {
      this.setState({
        loader: false
      })
    }

    if (nextProps.analytics.getStoreData.isLoading || nextProps.analytics.getStoreProfileCode.isLoading) {
      this.setState({
        loader: true
      })
    }
    if (nextProps.analytics.getStoreProfileCode.isLoading) {
      sessionStorage.removeItem("storeCode")
    }

    if (nextProps.analytics.getSellThru.isError) {
      this.setState({
        errorMessage: nextProps.analytics.getSellThru.message.error == undefined ? undefined : nextProps.analytics.getSellThru.message.error.errorMessage,
        errorCode: nextProps.analytics.getSellThru.message.error == undefined ? undefined : nextProps.analytics.getSellThru.message.error.errorCode,
        code: nextProps.analytics.getSellThru.message.status,
        alert: true,
      })
      this.props.getSellThruClear()

    }

    if (nextProps.analytics.getTopArticle.isError) {
      this.setState({
        errorMessage: nextProps.analytics.getTopArticle.message.error == undefined ? undefined : nextProps.analytics.getTopArticle.message.error.errorMessage,
        errorCode: nextProps.analytics.getTopArticle.message.error == undefined ? undefined : nextProps.analytics.getTopArticle.message.error.errorCode,
        code: nextProps.analytics.getTopArticle.message.status,
        alert: true,
      })
      this.props.getTopArticleClear()

    }
    if (nextProps.analytics.getSalesTrend.isError) {
      this.setState({
        errorMessage: nextProps.analytics.getSalesTrend.message.error == undefined ? undefined : nextProps.analytics.getSalesTrend.message.error.errorMessage,
        errorCode: nextProps.analytics.getSalesTrend.message.error == undefined ? undefined : nextProps.analytics.getSalesTrend.message.error.errorCode,
        code: nextProps.analytics.getSalesTrend.message.status,
        alert: true,
      })
      this.props.getSalesTrendClear()

    }

    if (nextProps.analytics.reviewReason.isSuccess) {
      this.setState({
          loader: false
      })
      this.props.getReviewReasonClear()
    } else if (nextProps.analytics.reviewReason.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.reviewReason.message.status,
            errorCode: nextProps.analytics.reviewReason.message.error == undefined ? undefined : nextProps.analytics.reviewReason.message.error.errorCode,
            errorMessage: nextProps.analytics.reviewReason.message.error == undefined ? undefined : nextProps.analytics.reviewReason.message.error.errorMessage
        })
        this.props.getReviewReasonClear()
    }
    if (nextProps.analytics.arsPwqData.isSuccess) {
      this.setState({
          loader: false
      })
      this.props.getDataWithoutFilterClear()
    } else if (nextProps.analytics.arsPwqData.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.arsPwqData.message.status,
            errorCode: nextProps.analytics.arsPwqData.message.error == undefined ? undefined : nextProps.analytics.arsPwqData.message.error.errorCode,
            errorMessage: nextProps.analytics.arsPwqData.message.error == undefined ? undefined : nextProps.analytics.arsPwqData.message.error.errorMessage
        })
        this.props.getDataWithoutFilterClear()
    }
    if (nextProps.analytics.arsPwqFilters.isSuccess) {
      this.setState({
          loader: false
      })
      this.props.getfiltersArsDataClear()
    } else if (nextProps.analytics.arsPwqFilters.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.arsFilter.message.status,
            errorCode: nextProps.analytics.arsFilter.message.error == undefined ? undefined : nextProps.analytics.arsFilter.message.error.errorCode,
            errorMessage: nextProps.analytics.arsFilter.message.error == undefined ? undefined : nextProps.analytics.arsFilter.message.error.errorMessage
        })
        this.props.getfiltersArsDataClear()
    }
    if (nextProps.analytics.assortmentDetails.isSuccess) {
      this.setState({
          loader: false
      })
      // this.props.getAssortmentDetailsClear()
    } else if (nextProps.analytics.assortmentDetails.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.assortmentDetails.message.status,
            errorCode: nextProps.analytics.assortmentDetails.message.error == undefined ? undefined : nextProps.analytics.assortmentDetails.message.error.errorCode,
            errorMessage: nextProps.analytics.assortmentDetails.message.error == undefined ? undefined : nextProps.analytics.assortmentDetails.message.error.errorMessage
        })
        this.props.getAssortmentDetailsClear()
    }
    if (nextProps.replenishment.createMainHeaderConfig.isSuccess) {
      this.setState({
          successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
          loader: false,
          success: true,
      })
      this.props.createMainHeaderConfigClear()
    } else if (nextProps.replenishment.createMainHeaderConfig.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.replenishment.createMainHeaderConfig.message.status,
            errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
            errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
        })
        this.props.createMainHeaderConfigClear()
    }
    if (nextProps.analytics.allocationData.isSuccess) {
      this.setState({
          loader: false,
      })
    } else if (nextProps.analytics.allocationData.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.allocationData.message.status,
            errorCode: nextProps.analytics.allocationData.message.error == undefined ? undefined : nextProps.analytics.allocationData.message.error.errorCode,
            errorMessage: nextProps.analytics.allocationData.message.error == undefined ? undefined : nextProps.analytics.allocationData.message.error.errorMessage
        })
        this.props.getAllocationDataClear()
    }
    if (nextProps.analytics.assortmentTimePhased.isSuccess) {
      this.setState({
          loader: false,
      })
    } else if (nextProps.analytics.assortmentTimePhased.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.assortmentTimePhased.message.status,
            errorCode: nextProps.analytics.assortmentTimePhased.message.error == undefined ? undefined : nextProps.analytics.assortmentTimePhased.message.error.errorCode,
            errorMessage: nextProps.analytics.assortmentTimePhased.message.error == undefined ? undefined : nextProps.analytics.assortmentTimePhased.message.error.errorMessage
        })
        this.props.getAssortmentTimePhasedClear()
    }
    if (nextProps.analytics.assortmentItems.isSuccess) {
      this.setState({
          loader: false,
      })
    } else if (nextProps.analytics.assortmentItems.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.analytics.assortmentItems.message.status,
            errorCode: nextProps.analytics.assortmentItems.message.error == undefined ? undefined : nextProps.analytics.assortmentItems.message.error.errorCode,
            errorMessage: nextProps.analytics.assortmentItems.message.error == undefined ? undefined : nextProps.analytics.assortmentItems.message.error.errorMessage
        })
        this.props.getAssortmentItemsClear()
    }
    if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
      this.setState({
          loader: false,
      })
    } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
        this.setState({
            loader: false,
            alert: true,
            code: nextProps.replenishment.getMainHeaderConfig.message.status,
            errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
            errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
        })
        this.props.getMainHeaderConfigClear()
    }
    if (nextProps.analytics.reviewReason.isLoading || nextProps.analytics.arsPwqFilters.isLoading
        || nextProps.analytics.arsPwqData.isLoading || nextProps.analytics.assortmentDetails.isLoading
        || nextProps.replenishment.createMainHeaderConfig.isLoading || nextProps.analytics.allocationData.isLoading
        || nextProps.analytics.assortmentTimePhased.isLoading || nextProps.analytics.assortmentItems.isLoading
        || nextProps.replenishment.getMainHeaderConfig.isLoading ) {
        this.setState({
          loader: true
        })
    }

  }

  render() {
    let hash = window.location.hash;
    return (
      <div>
        <div className="container-fluid nc-design pad-l60">
          <NewSideBar {...this.props} />
          <SideBar sideBar={this.state.sideBar} {...this.props} />
          {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
            {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
          <div className="container_div d-table m-top-75 " id="">
            <div className="col-md-12 col-sm-12 border-btm">
              <div className="menu_path d-table n-menu-path">
                <ul className="list-inline width_100 pad20 nmp-inner">
                  <BraedCrumps {...this.props} />
                </ul>
              </div>
            </div>
          </div>
          {/* <StoreProfile /> */}
          {hash == "#/analytics/storeProfile" ? (
            <StoreProfile {...this.props} rightSideBar={() => this.onRightSideBar()} ref="child" />
          ) : null}
          {hash == "#/analytics/chooseStoreProfile" ? (
            <ChooseStoreProfile {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}
          {hash == "#/analytics/sellThru" ? (
            <SellThru />
          ) : null}
          {/* {hash == "#/analytics/plannerWorkQueue" ? (
            <PlannerWorkQueue {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null} */}
          {hash == "#/analytics/plannerWorkQueue2" ? (
            <PlannerWorkQueue2 {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}
          {hash == "#/analytics/plannerWorkSheet" ? (
            <PlannerWorkSheet {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}
          {/* {hash == "#/analytics/plannerWorkSheet2" ? (
            <plannerWorkSheet2 {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null} */}
          {hash == "#/analytics/inventory-classfication" ? (
            <InventoryClassification {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : null}

          {this.state.loader ? <FilterLoader /> : null}
          {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
          {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
        </div>
        <Footer />
      </div>


    );
  }

}

export function mapStateToProps(state) {
  return {
    analytics: state.analytics,
    replenishment: state.replenishment,
    seasonPlanning: state.seasonPlanning
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Analytics);