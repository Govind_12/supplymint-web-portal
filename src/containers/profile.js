import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import RightSideBar from "../components/rightSideBar";
import Footer from '../components/footer';
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from "../components/loaders/requestSuccess";
import RequestError from "../components/loaders/requestError";
import ProfileUser from '../components/profile/profile';
import EditProfile from '../components/profile/editProfile';
import ChangePassword from "../components/profile/changePassword";
import BraedCrumps from "../components/breadCrumps";
import openRack from "../assets/open-rack.svg";
import NewSideBar from "../components/newSidebar";
import ToastLoader from '../components/loaders/toastLoader';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rightbar: false,
      openRightBar: false,
      profileUser: true,
      editUser: false,
      ChangePassword: false,
      alert: false,
      success: false,
      successMessage: "",
      errorCode: "",
      errorMessage: "",
      loader: false,
      code: "",
      toastMsg: "",
      toastLoader: false,
    };
  }
  componentWillMount() {
    // if (sessionStorage.getItem('token') == null) {
    //   this.props.history.push('/');
    // }

    if (!this.props.auth.getUserName.isSuccess) {
      // this.props.getUserNameRequest(sessionStorage.getItem('userName'));
    } else {
      // this.props.getUserNameRequest(sessionStorage.getItem('userName'));
      this.setState({
        loader: false
      })
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.changePassword.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.auth.changePassword.data.message
      })
      this.props.changePasswordClear();
    } else if (nextProps.auth.changePassword.isError) {
      this.setState({
        alert: true,
        code: nextProps.auth.changePassword.message.status,
        errorMessage: nextProps.auth.changePassword.message.error == undefined ? undefined : nextProps.auth.changePassword.message.error.message,
        errorCode: nextProps.auth.changePassword.message.error == undefined ? undefined : nextProps.auth.changePassword.message.error.code,
        loader: false
      })
      this.props.changePasswordClear();
    }

    if (nextProps.auth.getUserName.isSuccess) {
      this.setState({
        // success: true,
        loader: false,
        // successMessage: nextProps.auth.getUserName.data.message,
        userNameState: nextProps.auth.getUserName.data.resource,
      })
      this.props.getUserNameRequest();
    } else if (nextProps.auth.getUserName.isError) {


      this.setState({
        alert: true,
        code: nextProps.auth.getUserName.message.status,
        errorMessage: nextProps.auth.getUserName.message.error == undefined ? undefined : nextProps.auth.getUserName.message.error.errorMessage,
        errorCode: nextProps.auth.getUserName.message.error == undefined ? undefined : nextProps.auth.getUserName.message.error.errorCode,
        loader: false
      })
      this.props.getUserNameRequest();
    }

    if (nextProps.auth.updateProfile.isSuccess) {
      this.setState({
        successMessage: nextProps.auth.updateProfile.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.updateProfileRequest();
      this.props.history.push("/profile")

    } else if (nextProps.auth.updateProfile.isError) {
      this.setState({
        alert: true,
        code: nextProps.auth.updateProfile.message.status,
        errorMessage: nextProps.auth.updateProfile.message.error == undefined ? undefined : nextProps.auth.updateProfile.message.error.message,
        errorCode: nextProps.auth.updateProfile.message.error == undefined ? undefined : nextProps.auth.updateProfile.message.error.code,
        loader: false,
        success: false
      })
      this.props.updateProfileRequest();
    }


    if (nextProps.auth.updateUserDetails.isSuccess) {
      this.setState({
        successMessage: nextProps.auth.updateUserDetails.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.history.push("/profile")
      this.props.updateUserDetailsClear()
    } else if (nextProps.auth.updateUserDetails.isError) {
      this.setState({
        alert: true,
        code: nextProps.auth.updateUserDetails.message.status,
        errorMessage: nextProps.auth.updateUserDetails.message.error == undefined ? undefined : nextProps.auth.updateUserDetails.message.error.message,
        errorCode: nextProps.auth.updateUserDetails.message.error == undefined ? undefined : nextProps.auth.updateUserDetails.message.error.code,
        loader: false,
        success: false
      })
      this.props.updateUserDetailsClear()
    }

    if (nextProps.auth.getProfileDetails.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.getProfileDetailsClear()
    } else if (nextProps.auth.getProfileDetails.isError) {
      this.setState({
        alert: true,
        code: nextProps.auth.getProfileDetails.message.status,
        errorMessage: nextProps.auth.getProfileDetails.message.error == undefined ? undefined : nextProps.auth.getProfileDetails.message.error.message,
        errorCode: nextProps.auth.getProfileDetails.message.error == undefined ? undefined : nextProps.auth.getProfileDetails.message.error.code,
        loader: false,
        success: false
      })
      this.props.getProfileDetailsClear()
    }
    if (!nextProps.auth.changePassword.isLoading && !nextProps.auth.updateProfile.isLoading) {
      this.setState({
        loader: false
      })
    }

    if (nextProps.auth.getSupportAccess.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.getSupportAccessClear()
    } else if (nextProps.auth.getSupportAccess.isError) {
      this.setState({
        alert: true,
        code: nextProps.auth.getSupportAccess.message.status,
        errorMessage: nextProps.auth.getSupportAccess.message.error == undefined ? undefined : nextProps.auth.getSupportAccess.message.error.message,
        errorCode: nextProps.auth.getSupportAccess.message.error == undefined ? undefined : nextProps.auth.getSupportAccess.message.error.code,
        loader: false,
        success: false
      })
      this.props.getSupportAccessClear()
    }

    if (nextProps.auth.createSupportAccess.isSuccess) {
      if (nextProps.auth.createSupportAccess.data.resource.is_success == 0) {
        this.setState({
          toastMsg: nextProps.auth.createSupportAccess.data.resource.resp_msg,
          toastLoader: true,
          loader: false
      })
      setTimeout(() => {
          this.setState({
              toastLoader: false
          })
      }, 3000);
      }
      else {
        this.setState({
          loader: false
        })
      }
      this.props.createSupportAccessClear()
    } else if (nextProps.auth.createSupportAccess.isError) {
      this.setState({
        alert: true,
        code: nextProps.auth.createSupportAccess.message.status,
        errorMessage: nextProps.auth.createSupportAccess.message.error == undefined ? undefined : nextProps.auth.createSupportAccess.message.error.message,
        errorCode: nextProps.auth.createSupportAccess.message.error == undefined ? undefined : nextProps.auth.createSupportAccess.message.error.code,
        loader: false,
        success: false
      })
      this.props.createSupportAccessClear()
    }

    if (nextProps.auth.getProfileDetails.isLoading || nextProps.auth.updateUserDetails.isLoading || nextProps.auth.changePassword.isLoading || nextProps.auth.updateProfile.isLoading || nextProps.auth.getSupportAccess.isLoading || nextProps.auth.createSupportAccess.isLoading) {
      this.setState({
        loader: true
      })
    }
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }


  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  onEditUser() {
    this.setState({
      profileUser: false,
      editUser: true
    });
  }
  onUserSave() {
    this.setState({
      profileUser: true,
      editUser: false
    });
  }
  onChangePassword() {
    this.setState({
      ChangePassword: true,
      editUser: false
    });

  }
  render() {
    $("input").attr("autocomplete", "off");
    const { hash } = window.location;
    return (
      <div className="container-fluid profile-container pad-l65 m-top-75">
        <NewSideBar {...this.props} />
        <SideBar {...this.props} />
        {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
        {/* <div className="container-fluid pad-0">
          <div className="container_div m-top-75 " id="">
            <div className="col-md-12 col-sm-12 border-btm">
              <div className="menu_path d-table n-menu-path">
                <ul className="list-inline width_100 pad20 nmp-inner">
                  <BraedCrumps {...this.props} />
                </ul>
              </div>
            </div>
          </div>
        </div> */}
        {hash == "#/profile" ? (
          <ProfileUser {...this.props} userNameState={this.state.userNameState} rightSideBar={() => this.onRightSideBar()} editUser={() => this.onEditUser()} changePassword={() => this.onChangePassword()} profileDetails={this.props.auth.getProfileDetails.data.resource || ""} supportAccessDetails={this.props.auth.getSupportAccess.data.resource || ""} createSupportAccess={this.props.auth.createSupportAccess.data.resource || ""}/>
        ) : hash == "#/profile/edit" ? (
          <EditProfile {...this.props} userNameState={this.state.userNameState} rightSideBar={() => this.onRightSideBar()} saveUser={() => this.onUserSave()} />
        ) : hash == "#/profile/changePassword" ? (
          <ChangePassword {...this.props} rightSideBar={() => this.onRightSideBar()}/>
        ) : null
        }



        {/* <Footer /> */}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
