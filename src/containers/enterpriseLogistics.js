import React, { Component } from 'react'
import BraedCrumps from '../components/breadCrumps';
import SideBar from '../components/sidebar';
import RightSideBar from '../components/rightSideBar';
import Footer from '../components/footer';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import openRack from '../assets/open-rack.svg';
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';
import EnterpriseLrProcessing from '../components/vendorPortal/enterprise/logistics/enterpriseLrProcessing';
import EnterpriseGateEntry from '../components/vendorPortal/enterprise/logistics/enterpriseGateEntry';
import EnterpriseGoodsIntransit from '../components/vendorPortal/enterprise/logistics/enterpriseGoodsIntransit';
import GrcStatus from '../components/vendorPortal/enterprise/logistics/enterpriseGrcStatus';
import NewSideBar from '../components/newSidebar';

class EnterpriseLogistics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            isDownloadPage: false,
        }
        this.child = React.createRef();
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }

    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false,
            isDownloadPage: false,
        });
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
      componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
            this.setState({ loader: false, success: false, alert: false,isDownloadPage: false, })
      }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.logistic.getAllVendorShipmentShipped.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllVendorShipmentShippedClear()
        } else if (nextProps.logistic.getAllVendorShipmentShipped.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllVendorShipmentShipped.message.error == undefined ? undefined : nextProps.logistic.getAllVendorShipmentShipped.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllVendorShipmentShipped.message.error == undefined ? undefined : nextProps.logistic.getAllVendorShipmentShipped.message.error.errorCode,
                code: nextProps.logistic.getAllVendorShipmentShipped.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllVendorShipmentShippedClear()
        }
        if (nextProps.logistic.getAllVendorSsdetail.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllVendorSsdetailClear()
        } else if (nextProps.logistic.getAllVendorSsdetail.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllVendorSsdetail.message.error == undefined ? undefined : nextProps.logistic.getAllVendorSsdetail.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllVendorSsdetail.message.error == undefined ? undefined : nextProps.logistic.getAllVendorSsdetail.message.error.errorCode,
                code: nextProps.logistic.getAllVendorSsdetail.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllVendorSsdetailClear()
        }
        if (nextProps.logistic.getAllVendorTransitNDeliver.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllVendorTransitNDeliverClear()
        } else if (nextProps.logistic.getAllVendorTransitNDeliver.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllVendorTransitNDeliver.message.error == undefined ? undefined : nextProps.logistic.getAllVendorTransitNDeliver.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllVendorTransitNDeliver.message.error == undefined ? undefined : nextProps.logistic.getAllVendorTransitNDeliver.message.error.errorCode,
                code: nextProps.logistic.getAllVendorTransitNDeliver.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllVendorTransitNDeliverClear()
        }

        if (nextProps.logistic.getAllGateEntry.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllGateEntryClear()
        } else if (nextProps.logistic.getAllGateEntry.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllGateEntry.message.error == undefined ? undefined : nextProps.logistic.getAllGateEntry.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllGateEntry.message.error == undefined ? undefined : nextProps.logistic.getAllGateEntry.message.error.errorCode,
                code: nextProps.logistic.getAllGateEntry.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllGateEntryClear()
        }
        if (nextProps.logistic.getAllVendorSiDetail.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllVendorSiDetailClear()
        } else if (nextProps.logistic.getAllVendorSiDetail.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllVendorSiDetail.message.error == undefined ? undefined : nextProps.logistic.getAllVendorSiDetail.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllVendorSiDetail.message.error == undefined ? undefined : nextProps.logistic.getAllVendorSiDetail.message.error.errorCode,
                code: nextProps.logistic.getAllVendorSiDetail.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllVendorSiDetailClear()
        }
        if (nextProps.logistic.getAllVendorTransporter.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllVendorTransporterClear()
        } else if (nextProps.logistic.getAllVendorTransporter.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllVendorTransporter.message.error == undefined ? undefined : nextProps.logistic.getAllVendorTransporter.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllVendorTransporter.message.error == undefined ? undefined : nextProps.logistic.getAllVendorTransporter.message.error.errorCode,
                code: nextProps.logistic.getAllVendorTransporter.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllVendorTransporterClear()
        }
        if (nextProps.logistic.lrCreate.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.logistic.lrCreate.data.message
            })
            this.props.lrCreateClear()
        } else if (nextProps.logistic.lrCreate.isError) {
            this.setState({
                errorMessage: nextProps.logistic.lrCreate.message.error == undefined ? undefined : nextProps.logistic.lrCreate.message.error.errorMessage,
                errorCode: nextProps.logistic.lrCreate.message.error == undefined ? undefined : nextProps.logistic.lrCreate.message.error.errorCode,
                code: nextProps.logistic.lrCreate.message.status,
                alert: true,
                loader: false
            })
            this.props.lrCreateClear()
        }

        if (nextProps.logistic.updateDelivery.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.logistic.updateDelivery.data.message

            })
            this.props.updateDeliveryClear()
        } else if (nextProps.logistic.updateDelivery.isError) {
            this.setState({
                errorMessage: nextProps.logistic.updateDelivery.message.error == undefined ? undefined : nextProps.logistic.updateDelivery.message.error.errorMessage,
                errorCode: nextProps.logistic.updateDelivery.message.error == undefined ? undefined : nextProps.logistic.updateDelivery.message.error.errorCode,
                code: nextProps.logistic.updateDelivery.message.status,
                alert: true,
                loader: false
            })
            this.props.updateDeliveryClear()
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getHeaderConfigClear()
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            })
            this.props.getHeaderConfigClear()
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createHeaderConfigClear()
        } else if (nextProps.replenishment.createHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            })
            this.props.createHeaderConfigClear()
        }
        if (nextProps.shipment.addMultipleCommentQc.isSuccess) {
            this.setState({
                successMessage: nextProps.shipment.addMultipleCommentQc.data.message,
                loader: false,
                success: true,
            })
            this.props.addMultipleCommentQcClear()
        } else if (nextProps.shipment.addMultipleCommentQc.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.addMultipleCommentQc.message.status,
                errorCode: nextProps.shipment.addMultipleCommentQc.message.error == undefined ? undefined : nextProps.shipment.addMultipleCommentQc.message.error.errorCode,
                errorMessage: nextProps.shipment.addMultipleCommentQc.message.error == undefined ? undefined : nextProps.shipment.addMultipleCommentQc.message.error.errorMessage
            })
            this.props.addMultipleCommentQcClear()
        }
        if (nextProps.shipment.addMultipleCommentQc.isLoading || nextProps.replenishment.createHeaderConfig.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.shipment.shipmentConfirmCancel.isLoading || nextProps.logistic.getAllVendorShipmentShipped.isLoading || nextProps.logistic.getAllVendorSsdetail.isLoading || nextProps.logistic.getAllVendorTransitNDeliver.isLoading
            || nextProps.logistic.getAllVendorSiDetail.isLoading || nextProps.logistic.getAllVendorTransporter.isLoading || nextProps.logistic.lrCreate.isLoading || nextProps.logistic.updateDelivery.isLoading) {
            this.setState({
                loader: true
            })
        }

        if (nextProps.shipment.shipmentConfirmCancel.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.shipment.shipmentConfirmCancel.data.message
            })
            this.props.shipmentConfirmCancelClear()
        } else if (nextProps.shipment.shipmentConfirmCancel.isError) {
            this.setState({
                errorMessage: nextProps.shipment.shipmentConfirmCancel.message.error == undefined ? undefined : nextProps.shipment.shipmentConfirmCancel.message.error.errorMessage,
                errorCode: nextProps.shipment.shipmentConfirmCancel.message.error == undefined ? undefined : nextProps.shipment.shipmentConfirmCancel.message.error.errorCode,
                code: nextProps.shipment.shipmentConfirmCancel.message.status,
                alert: true,
                loader: false
            })
            this.props.shipmentConfirmCancelClear()
        }

        //    if (nextProps.shipment.getAllComment.isSuccess) {
        //             this.setState({
        //                 loader: false,
        //             })
        //             this.props.getAllCommentClear()
        //         } else if (nextProps.shipment.getAllComment.isError) {
        //             this.setState({
        //                 errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage,
        //                 errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
        //                 code: nextProps.shipment.getAllComment.message.status,
        //                 alert: true,
        //                 loader: false
        //             })
        //             this.props.getAllCommentClear()
        //         }
        if (nextProps.shipment.qcAddComment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.qcAddCommentClear()
        } else if (nextProps.shipment.qcAddComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorMessage,
                errorCode: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorCode,
                code: nextProps.shipment.qcAddComment.message.status,
                alert: true,
                loader: false
            })
            this.props.qcAddCommentClear()
        };
        if (nextProps.shipment.deleteUploads.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.deleteUploadsClear()
        } else if (nextProps.shipment.deleteUploads.isError) {
            this.setState({
                errorMessage: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorMessage,
                errorCode: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorCode,
                code: nextProps.shipment.deleteUploads.message.status,
                alert: true,
                loader: false
            })
            this.props.deleteUploadsClear()
        }
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getAllCommentClear()
        } else if (nextProps.shipment.getAllComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage,
                errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
                code: nextProps.shipment.getAllComment.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllCommentClear()
        }
        if (nextProps.orders.multipleDocumentDownload.isSuccess) {
            this.setState({
                successMessage: nextProps.orders.multipleDocumentDownload.data.message,
                loader: false,
                success: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
                isDownloadPage: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
            },()=>{
                if( nextProps.orders.multipleDocumentDownload.data.resource !== undefined && nextProps.orders.multipleDocumentDownload.data.resource != null )
                   window.open(`${nextProps.orders.multipleDocumentDownload.data.resource}`)       
            })
            this.props.multipleDocumentDownloadClear()
        }
        else if (nextProps.orders.multipleDocumentDownload.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.multipleDocumentDownload.message.status,
                errorCode: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorCode,
                errorMessage: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorMessage
            })
            this.props.multipleDocumentDownloadClear()
        }
        if (nextProps.logistic.updateExtensionDate.isSuccess) {
            this.setState({
                loader: false, success: true,
                successMessage: nextProps.logistic.updateExtensionDate.data.data.message
            })
            //this.child.current.handledateExtensionModal();
            this.props.updateExtensionDateClear()
        }
        else if (nextProps.logistic.updateExtensionDate.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.logistic.updateExtensionDate.message.status,
                errorCode: nextProps.logistic.updateExtensionDate.message.error == undefined ? undefined : nextProps.logistic.updateExtensionDate.message.error.errorCode,
                errorMessage: nextProps.logistic.updateExtensionDate.data.data.message.error == undefined ? undefined : nextProps.logistic.updateExtensionDate.data.data.message.error.errorMessage
            })
            this.props.updateExtensionDateClear()
        }
        if (nextProps.logistic.getGrcItem.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getGrcItemClear()
        } else if (nextProps.logistic.getGrcItem.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getGrcItem.message.error == undefined ? undefined : nextProps.logistic.getGrcItem.message.error.errorMessage,
                errorCode: nextProps.logistic.getGrcItem.message.error == undefined ? undefined : nextProps.logistic.getGrcItem.message.error.errorCode,
                code: nextProps.logistic.getGrcItem.message.status,
                alert: true,
                loader: false
            })
            this.props.getGrcItemClear()
        }

        if (nextProps.logistic.createGrc.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.logistic.createGrc.data.message
            })
            // this.confirmModal.handleConfirmGrc()
            this.confirmModal.onRefresh()
            this.props.createGrcClear()
        } else if (nextProps.logistic.createGrc.isError) {
            this.setState({
                errorMessage: nextProps.logistic.createGrc.message.error == undefined ? undefined : nextProps.logistic.createGrc.message.error.errorMessage,
                errorCode: nextProps.logistic.createGrc.message.error == undefined ? undefined : nextProps.logistic.createGrc.message.error.errorCode,
                code: nextProps.logistic.createGrc.message.status,
                alert: true,
                loader: false
            })
            this.props.createGrcClear()
        }


        if (nextProps.logistic.getAllGrc.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getAllGrcClear()
        } else if (nextProps.logistic.getAllGrc.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getAllGrc.message.error == undefined ? undefined : nextProps.logistic.getAllGrc.message.error.errorMessage,
                errorCode: nextProps.logistic.getAllGrc.message.error == undefined ? undefined : nextProps.logistic.getAllGrc.message.error.errorCode,
                code: nextProps.logistic.getAllGrc.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllGrcClear()
        }
        if (nextProps.logistic.getGrcDetails.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getGrcDetailsClear()
        } else if (nextProps.logistic.getGrcDetails.isError) {
            this.setState({
                errorMessage: nextProps.logistic.getGrcDetails.message.error == undefined ? undefined : nextProps.logistic.getGrcDetails.message.error.errorMessage,
                errorCode: nextProps.logistic.getGrcDetails.message.error == undefined ? undefined : nextProps.logistic.getGrcDetails.message.error.errorCode,
                code: nextProps.logistic.getGrcDetails.message.status,
                alert: true,
                loader: false
            })
            this.props.getGrcDetailsClear()
        }
        //custom column setting heaader
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getMainHeaderConfigClear()
        } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
            })
            this.props.getMainHeaderConfigClear()
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getSetHeaderConfigClear()
        } else if (nextProps.replenishment.getSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorMessage
            })
            this.props.getSetHeaderConfigClear()
        }
        if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getItemHeaderConfigClear()
        } else if (nextProps.replenishment.getItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorMessage
            })
            this.props.getItemHeaderConfigClear()
        }
        //create main header
        if (nextProps.replenishment.createMainHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createMainHeaderConfigClear()
        } else if (nextProps.replenishment.createMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
            })
            this.props.createMainHeaderConfigClear()
        }
        //create set header
        if (nextProps.replenishment.createSetHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createSetHeaderConfigClear()
        } else if (nextProps.replenishment.createSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage
            })
            this.props.createSetHeaderConfigClear()
        }
        //create  item header
        if (nextProps.replenishment.createItemHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createItemHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createItemHeaderConfigClear()
        } else if (nextProps.replenishment.createItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorMessage
            })
            this.props.createItemHeaderConfigClear()
        }
        if (nextProps.orders.cancelAndRejectReason.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (nextProps.orders.cancelAndRejectReason.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.cancelAndRejectReason.message.status,
                errorCode: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorCode,
                errorMessage: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorMessage
            })
            this.props.getCancelAndRejectReasonClear()
        }
        if (nextProps.logistic.getAllGateEntry.isLoading || 
            nextProps.logistic.getGrcDetails.isLoading || 
            nextProps.logistic.getAllGrc.isLoading || 
            nextProps.logistic.createGrc.isLoading || 
            nextProps.logistic.getGrcItem.isLoading || 
            nextProps.orders.multipleDocumentDownload.isLoading || 
            nextProps.shipment.deleteUploads.isLoading||
            nextProps.replenishment.getMainHeaderConfig.isLoading || 
            nextProps.replenishment.getSetHeaderConfig.isLoading || 
            nextProps.replenishment.getItemHeaderConfig.isLoading ||
            nextProps.replenishment.createMainHeaderConfig.isLoading || 
             nextProps.replenishment.createSetHeaderConfig.isLoading || 
             nextProps.replenishment.createItemHeaderConfig.isLoading ||
             nextProps.orders.cancelAndRejectReason.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    shipmentTrackingMethod = _ => {
        this.props.shipmentTrackingRequest({
            shipmentId: _.id,
            orderId: _.orderId,
            asnNo: _.shipmentAdviceCode || _.shipmentAdviceNo,
            orderNo: _.orderNumber || _.poNumber,
            documentNumber: _.documentNumber
        })
        this.logisticChild.shipmentTrackModal()
    }
    // shipmentTrackModal = () => {
    //     this.setState({ shipmentModal: !this.state.shipmentModal })
    // }
    render() {
        const hash = window.location.hash.split("/")[3];
        return (
            <div>
                <div className="container-fluid nc-design pad-l50">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-t-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                                {/* <div className="nmp-right">
                                    <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button>
                                    <button className="nmp-help">Need Help <img src={Help}></img></button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    
                {hash == "lrProcessing" ? (
                    // <EnterpriseLrProcessing ref={this.child} {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingMethod={this.shipmentTrackingMethod} />
                    <EnterpriseLrProcessing ref={this.child} {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.logisticChild = ref)}/>
                ) : hash == "goodsIntransit" ? (
                    // <EnterpriseGoodsIntransit {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingMethod={this.shipmentTrackingMethod} />
                    <EnterpriseGoodsIntransit {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.logisticChild = ref)}/>
                ) : hash == "goodsDelivered" ? (
                    // <EnterpriseGateEntry {...this.state} {...this.props} ref={node => this.confirmModal = node} rightSideBar={() => this.onRightSideBar()} shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingMethod={this.shipmentTrackingMethod} />
                    <EnterpriseGateEntry {...this.state} {...this.props} ref={node => this.confirmModal = node} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.logisticChild = ref)}/>
                ) : hash == "grcStatus" ? (
                    // <GrcStatus {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingMethod={this.shipmentTrackingMethod} />
                    <GrcStatus {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.logisticChild = ref)}/>
                ) : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
                <Footer />
            </div>
        )
    }
}
export function mapStateToProps(state) {
    return {
        orders: state.orders,
        logistic: state.logistic,
        shipment: state.shipment,
        replenishment: state.replenishment,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EnterpriseLogistics);
