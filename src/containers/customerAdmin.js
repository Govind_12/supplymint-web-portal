import React from 'react';
import SideBar from '../components/sidebar';
import RightSideBar from '../components/rightSideBar';
import BraedCrumps from '../components/breadCrumps';
import openRack from "../assets/open-rack.svg"
import UserTabCustomer from '../components/customerPortal/customerAdminScreens/userManageTab';
import ManageSubscriptionTab from '../components/customerPortal/customerAdminScreens/manageSubscriptionTab';
import Footer from '../components/footer';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../redux/actions';
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from '../components/loaders/requestSuccess';
import RequestError from '../components/loaders/requestError';
import NewSideBar from '../components/newSidebar';


class CustomerAdmin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            openRightBar: false,
            tab: "user",
            loader: false,
            success: false,
            errorMessage: "",
            successMessage: "",
            alert: false,
            errorCode: "",
            code: ""
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.allRetailerCustomer.isSuccess) {
            this.setState({ loader: false })
            // this.props.allRetailerCustomerClear()
        }
        else if (nextProps.administration.allRetailerCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.allRetailerCustomer.message.status,
                errorCode: nextProps.administration.allRetailerCustomer.message.error == undefined ? undefined : nextProps.administration.allRetailerCustomer.message.error.errorCode,
                errorMessage: nextProps.administration.allRetailerCustomer.message.error == undefined ? undefined : nextProps.administration.allRetailerCustomer.message.error.errorMessage
            })
            this.props.allRetailerCustomerClear()
        }
        if (nextProps.administration.addUserCustomer.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.addUserCustomer.data.message,
                success: true,
                loader: false,
                alert: false,
              })
        }
        else if (nextProps.administration.addUserCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.addUserCustomer.message.status,
                errorCode: nextProps.administration.addUserCustomer.message.error == undefined ? undefined : nextProps.administration.addUserCustomer.message.error.errorCode,
                errorMessage: nextProps.administration.addUserCustomer.message.error == undefined ? undefined : nextProps.administration.addUserCustomer.message.error.errorMessage
            })
            this.props.addUserCustomerClear()
        }
        if (nextProps.administration.customerUser.isSuccess) {
            this.setState({ loader: false })
        }
        else if (nextProps.administration.customerUser.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customerUser.message.status,
                errorCode: nextProps.administration.customerUser.message.error == undefined ? undefined : nextProps.administration.customerUser.message.error.errorCode,
                errorMessage: nextProps.administration.customerUser.message.error == undefined ? undefined : nextProps.administration.customerUser.message.error.errorMessage
            })
            this.props.customerUserClear()
        }
        if (nextProps.administration.editUserCustomer.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.editUserCustomer.data.message,
                success: true,
                loader: false,
                alert: false,
              })
        }
        else if (nextProps.administration.editUserCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.editUserCustomer.message.status,
                errorCode: nextProps.administration.editUserCustomer.message.error == undefined ? undefined : nextProps.administration.editUserCustomer.message.error.errorCode,
                errorMessage: nextProps.administration.editUserCustomer.message.error == undefined ? undefined : nextProps.administration.editUserCustomer.message.error.errorMessage
            })
            this.props.editUserCustomerClear();
        }
        if (nextProps.administration.userStatusCustomer.isSuccess) {
            this.setState({
              successMessage: nextProps.administration.userStatusCustomer.data.message,
              success: true,
              loader: false,
              alert: false,
            })
            this.props.userStatusCustomerClear();
      
        }
        else if (nextProps.administration.userStatusCustomer.isError) {
            this.setState({
              errorMessage: nextProps.administration.userStatusCustomer.message.error == undefined ? undefined : nextProps.administration.userStatusCustomer.message.error.errorMessage,
              code: nextProps.administration.userStatusCustomer.message.status,
              alert: true,
              success: false,
              errorCode:  nextProps.administration.userStatusCustomer.message.error == undefined ? undefined : nextProps.administration.userStatusCustomer.message.error.errorCode,
              loader: false
            })
            this.props.userStatusCustomerClear();
        }
        if (nextProps.administration.getSubscriptionCustomer.isSuccess) {
            this.setState({
             loader: false,
            })
            this.props.getSubscriptionCustomerClear();
        }
        else if (nextProps.administration.getSubscriptionCustomer.isError) {
            this.setState({
              errorMessage: nextProps.administration.getSubscriptionCustomer.message.error == undefined ? undefined : nextProps.administration.getSubscriptionCustomer.message.error.errorMessage,
              code: nextProps.administration.getSubscriptionCustomer.message.status,
              alert: true,
              success: false,
              errorCode:  nextProps.administration.getSubscriptionCustomer.message.error == undefined ? undefined : nextProps.administration.getSubscriptionCustomer.message.error.errorCode,
              loader: false
            })
            this.props.getSubscriptionCustomerClear();
        }
        if (nextProps.administration.getTransactionCustomer.isSuccess) {
            this.setState({
             loader: false,
            })
            this.props.getTransactionCustomerClear();
        }
        else if (nextProps.administration.getTransactionCustomer.isError) {
            this.setState({
              errorMessage: nextProps.administration.getTransactionCustomer.message.error == undefined ? undefined : nextProps.administration.getTransactionCustomer.message.error.errorMessage,
              code: nextProps.administration.getTransactionCustomer.message.status,
              alert: true,
              success: false,
              errorCode:  nextProps.administration.getTransactionCustomer.message.error == undefined ? undefined : nextProps.administration.getTransactionCustomer.message.error.errorCode,
              loader: false
            })
            this.props.getTransactionCustomerClear();
        } 
        if (nextProps.administration.allRetailerCustomer.isLoading || nextProps.administration.addUserCustomer.isLoading || nextProps.administration.customerUser.isLoading 
            || nextProps.administration.editUserCustomer.isLoading || nextProps.administration.userStatusCustomer.isLoading
            || nextProps.administration.getSubscriptionCustomer.isLoading || nextProps.administration.getTransactionCustomer.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
          success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
          alert: false
        });
        document.onkeydown = function (t) {
          if (t.which) {
            return true;
          }
        }
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    changeTabCS(tab) {
        this.setState({
            tab
        })
    }
    render() {
        console.log(this.state);
        return (
            <div>
                <div className="container-fluid nc-design">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                    <div className="container_div m-top-75 pad-l60" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path">
                                {/* <ul className="list-inline width_100 pad20">
                                    <BraedCrumps {...this.props} />
                                </ul> */}
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid pad-0 pad-l50">
                        <div className="container_div" id="home">
                            <div className="container-fluid pad-0">
                                <div className="container_div" id="">
                                    <div className="container-fluid pad-0">
                                        <div className="replenishment_container vendor-admin-user">
                                            <div className="col-md-12 col-sm-12 col-xs-12 itemUdfSet pad-0 changeSettingContain">
                                                <div className="switchTabs p-lr-47">
                                                    <ul className="list_style m0">
                                                        <li onClick={() => this.changeTabCS('user')} className={this.state.tab == "user" ? "displayPointer activeTab m-rgt-25" : "m-rgt-25 displayPointer"}>
                                                            <label className="displayPointer">User</label>
                                                        </li>
                                                        <li onClick={() => this.changeTabCS('subscription')} className={this.state.tab == "subscription" ? "displayPointer activeTab m-rgt-25" : "m-rgt-25 displayPointer"}>
                                                            <label className="displayPointer">Subscription</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                {this.state.tab == "user" ? <UserTabCustomer {...this.props} {...this.state}/> :
                                                    this.state.tab == "subscription" ? <ManageSubscriptionTab {...this.props} {...this.state}/> : ""}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
                <Footer />
            </div>
        )
    }
}
export function mapStateToProps(state) {
    return {
        administration: state.administration
    }
}

function mapDisapatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}
export default connect(mapStateToProps, mapDisapatchToProps)(CustomerAdmin)