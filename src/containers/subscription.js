import React, { Component } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';
import PricingPlans from '../components/vendorPortal/vendorSubscription/pricingPlans';
import InvoiceDetails from '../components/vendorPortal/vendorSubscription/invoiceDetails';
import InvoiceEdit from '../components/vendorPortal/vendorSubscription/invoiceEdit';
import MultiEnterprisesAaGridView from '../components/vendorPortal/vendorSubscription/multienterprisesAaGridView';
import MultiEnterprisesAaListView from '../components/vendorPortal/vendorSubscription/multienterprisesAaListView';
import SubscriptionRenew from '../components/vendorPortal/vendorSubscription/subscriptionRenew';
import TransactionFailed from '../components/vendorPortal/vendorSubscription/transactionFailed';
import TransactionStatus from '../components/vendorPortal/vendorSubscription/transactionStatus';
import CurrentplanDetails from '../components/vendorPortal/vendorSubscription/currentplanDetails';
import Json from '../components/administration/jsonFile.json';
import SideBar from '../components/sidebar';
import { CONFIG } from './../config';

class Subscription extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enterprisedId: sessionStorage.getItem("eid"),
            orgId: sessionStorage.getItem("oid"),
            rightbar: false,
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            fullName:(sessionStorage.getItem("firstName") == null ?  "" : sessionStorage.getItem("firstName")+" "+sessionStorage.getItem("lastName")== null ?  "" : sessionStorage.getItem("lastName")).trim(),
            email:sessionStorage.getItem("email"),
            plans: [],
            oneYearPlan: [],
            threeYearPlan: [],
            selectedPlan: "12",
            noOfUsers: "",
            getPlanSummary: [],
            planId: "",
            states: [],
            allLocations: {},
            city: [],
            orderId: "",
            razorpay_payment_id:"",
            razorpay_subscription_id:"",
            razorpay_signature:"",
            requireFields: ["companyName", "fullName", "email", "contactNumber", "selectedState", "selectedCity", "postalCode", "gstNumber", "billingAddress"]
        }
    }
    componentDidMount() {
        this.setState({ states: Object.keys(Json[0].states), allLocations: Json[0].states })
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.getPaymentStatus.isSuccess) {
            return {
                loader: false
             }
        }
        if (nextProps.orders.getAllPlans.isSuccess) {
            return {
                plans: nextProps.orders.getAllPlans.data.resource == null ? [] : nextProps.orders.getAllPlans.data.resource,
                oneYearPlan: nextProps.orders.getAllPlans.data.resource.find(item => item.duration == 12),
                threeYearPlan: nextProps.orders.getAllPlans.data.resource.find(item => item.duration == 36)
            }
        }
        if (nextProps.orders.getPaymentSummary.isSuccess) {
            return {
                getPlanSummary: nextProps.orders.getPaymentSummary.data.resource,
                loader: false
            }
        }
        if (nextProps.orders.getOrderId.isSuccess) {
            return {
                orderId: nextProps.orders.getOrderId.data.resource.orderId,
                loader: false
            }
        }
        if (nextProps.orders.getCapturePayment.isSuccess) {
            return {
                // orderId: nextProps.orders.getOrderId.data.resource.orderId,
                loader: false
            }
        }
        if (nextProps.orders.getPaymentStatus.isLoading || nextProps.orders.getCapturePayment.isLoading || nextProps.orders.getPaymentSummary.isLoading || nextProps.orders.getOrderId.isLoading) {
            return { loader: true }
        }
        return null
    }

    componentDidUpdate() {
        if (this.props.orders.getPaymentSummary.isSuccess) {
            this.props.history.push('/subscription/invoiceDetails')
            this.props.getPaymentSummaryClear()
        }
        if (this.props.orders.getAllPlans.isSuccess) {
            this.props.getAllPlansClear()
        }
        if (this.props.orders.getOrderId.isSuccess) {
            var data  = this.props.orders.getOrderId.data.resource == null ? {} : this.props.orders.getOrderId.data.resource
            this.makePayment(data)
            this.props.getOrderIdClear()
        }
        if (this.props.orders.getCapturePayment.isSuccess) {
            this.props.history.push('/subscription/transactionSuccessful')
            this.props.getCapturePaymentClear()
        }
    }

    selectPlan = (event) => {
        this.setState({ selectedPlan: this.state.selectedPlan == "12" ? "36" : "12" })
    }

    handleChange = (event) => {
        var name = event.target.name
        var value = event.target.value
        let validate = event.target.dataset.validate
        console.log(validate)
        this.setState({ [name]: value }, () => {
            let numberPattern = /^[0-9]+$/;
            var stateName = name + "Err"
            if(name == "contactNumber"){
                if(validate && Number(this.state.contactNumber.length) == 10 && numberPattern.test(this.state.contactNumber)){
                    this.setState({
                        contactNumberErr: false
                    })
                }else{
                    this.setState({
                        contactNumberErr: true
                    })
                }
            }
            if (validate && value == "") {
                this.setState({ [stateName]: true })
            } else {
                this.setState({ [stateName]: false })
            }
        })
    }

    selectedPlanId = () => {
        let plan = [];
        plan = this.state.plans.filter((data) => data.duration == this.state.selectedPlan)
        this.setState({ planId: plan[0].planId })
        return (plan[0].planId)
    }
    findCities = (event) => {
        document.getElementById('cityId').value = ""
        this.setState({ city: this.state.allLocations[event.target.value], selectedState: event.target.value },()=>{

        })
    }
    handleRequiredFields = () => {
        this.state.requireFields.map((data) => {
            var stateName = data + "Err"
            // console.log(stateName , data ,this.state[data])
            if (this.state[data] == "" || this.state[data] == undefined) {
                this.setState({ [stateName]: true })
            } else {
                this.setState({ [stateName]: "" })
            }
        })
    }
    proceedToPayment = () => {
        this.handleRequiredFields()
        console.log(this.state.companyNameErr, this.state.fullNameErr, this.state.selectedStateErr)
        setTimeout(() => {
            if (this.state.companyNameErr == false && this.state.fullNameErr == false && this.state.emailErr == false
                && this.state.contactNumberErr == false && this.state.selectedStateErr == false && this.state.selectedCityErr == false
                && this.state.postalCodeErr == false && this.state.gstNumberErr == false && this.state.billingAddressErr == false) {
                let payload = {
                    companyName: this.state.companyName,
                    userName: this.state.fullName,
                    emailId: this.state.email,
                    contactNo: this.state.contactNumber,
                    state: this.state.selectedState,
                    city: this.state.selectedCity,
                    postalCode: this.state.postalCode,
                    gstNumber: this.state.gstNumber,
                    address: this.state.billingAddress,
                    planCode:this.state.plans[0].planCode,
                    noOfUsersPaid:sessionStorage.getItem("billing-user"),
                    startsAt:this.state.plans[0].billingStartsAt,
                    enterpriseId:this.state.enterprisedId,
                    organisationId:this.state.orgId,
                    
                }
                this.props.getOrderIdRequest(payload)
            }
        }, 1000)
    }
    makePayment = (data) => {
        data.handler = async (response) => {
            try {
              if (typeof response.razorpay_payment_id == 'undefined' ||  response.razorpay_payment_id < 1) {
                this.window.location.href = '/#/subscription/transactionFailed';
              } else {
                //const paymentId = response.razorpay_payment_id;
                //console.log("response from razorpay---"+JSON.stringify(response))
                //response from razorpay---{"razorpay_payment_id":"pay_Fa4Nr6j7QXbsHx","razorpay_subscription_id":"sub_Fa4NTkfKcDBU7V","razorpay_signature":"5c0e21844ac1df6a08e72cabe6dd5a5e03cfe6a44e6104122667fe8830cc9e31"}
                this.setState({
                    razorpay_payment_id:response.razorpay_payment_id,
                    razorpay_subscription_id:response.razorpay_subscription_id,
                    razorpay_signature:response.razorpay_signature
                })
                window.location.href = '/#/subscription/transactionStatus';
              }
            } catch (err) {
              console.log(err);
            }
          }
        //data.callback_url = `${CONFIG.BASE_URL}/subscription/transactionStatus`;
        //data.callback_url = "http://localhost:5000/subscription/transactionStatus";
        //console.log(data)
        var options = data
        let rzp1 = new window.Razorpay(options);
        rzp1.open();
        //var rzp1 = new Razorpay(options);
        //rzp1.open();
        //e.preventDefault();
    }

    render() {
        const hash = window.location.hash.split("/")[2];
        return (
            <div className="container-fluid p-lr-0 min-h-100vh bbg-fff">
                 {/* <SideBar {...this.props} /> */}
                {/* <imghome onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" /> */}
                {/* {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                {/* <div className="container_div m-top-30 " id=""> */}
                    {/* <div className="col-md-12 col-sm-12">
                        <div className="menu_path">
                            <ul className="list-inline width_100 pad20">
                                <BraedCrumps />
                            </ul>
                        </div>
                    </div> */}
                {/* </div> */}
                {hash == "pricingPlans" ? (
                    <PricingPlans {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "invoiceDetails" ? (
                    <InvoiceDetails {...this.props} proceedToPayment={this.proceedToPayment} handleChange={this.handleChange} findCities={this.findCities} {...this.state} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "invoiceEdit" ? (
                    <InvoiceEdit {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "multienterprisesAaGridView" ? (
                    <MultiEnterprisesAaGridView {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "multienterprisesAaListView" ? (
                    <MultiEnterprisesAaListView {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "subscriptionRenew" ? (
                    <SubscriptionRenew {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "transactionFailed" ? (
                    <TransactionFailed {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "transactionStatus" ? (
                    <TransactionStatus {...this.props}{...this.state} razorpay_payment_id={this.state.razorpay_payment_id} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {hash == "currentplanDetails" ? (
                    <CurrentplanDetails {...this.props} rightSideBar={() => this.onRightSideBar()} />)
                    : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

                {/* <Footer /> */}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        orders: state.orders,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Subscription);
