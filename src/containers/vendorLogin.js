import React from 'react';
// import ProcessIcon from "../assets/process.png";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";

import errorIcon from "../assets/error_icon.svg"
import FilterLoader from "../components/loaders/filterLoader";
import RequestError from '../components/loaders/requestError'
class VendorLogin extends React.Component {

         constructor(props) {
        super(props);
        this.state = {
            uname: "",
            password: "",
            errorMessage: "",
            unameerr: false,
            passworderr: false,
            loader: false,
            errorCode: "",
            code: ""
           
        };
    }


        onChange(e) {
        e.preventDefault();
        if (e.target.id == "uname") {
            this.setState({
                uname: e.target.value
            }, () => {
                this.uname();
            })
        } else if (e.target.id == "password") {
            this.setState({
                password: e.target.value
            }, () => {
                this.password();
            })
        }
    }
    uname() {
        if (this.state.uname == "" || this.state.uname.trim() == "") {
            this.setState({
                unameerr: true
            });
        } else {
            this.setState({
                unameerr: false
            });
        }
    }
    password() {
        if (this.state.password == "" || !this.state.password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/)) {
            this.setState({
                passworderr: true
            });
        } else {
            this.setState({
                passworderr: false
            });
        }
    }
    onSubmit(e) {
        e.preventDefault();
        this.uname();
        this.password();
        const t = this;
        setTimeout(function () {
            const { uname, password, unameerr, passworderr } = t.state;
            if (!unameerr && !passworderr) {
                // t.props.history.push('/home')
                let loginData = {
                    username: uname,
                    password: password,
                }
                t.props.loginRequest(loginData);
            // window.location.reload()

            }



        }, 100)
    }

        componentWillReceiveProps(nextProps) {
        if (nextProps.auth.login.isLoading) {
            this.setState({
                loader: true,
                alert: false
            });
        }
        else if (nextProps.auth.login.isSuccess) {
            this.setState({
                loader: false,
                alert: false
            });
            // window.location.reload()

            this.props.history.push('/home');
        } else if (nextProps.auth.login.isError) {
            this.setState({
                loader: false,
              
                alert: true,
                code: nextProps.auth.login.message.status,
                errorMessage: nextProps.auth.login.message.error == undefined ? undefined : nextProps.auth.login.message.error.message,
                errorCode: nextProps.auth.login.message.error == undefined ? undefined :  nextProps.auth.login.message.error.code
            })
        }


    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }


       render() {
                   const { uname, password, unameerr, passworderr } = this.state;
    return (
        <div className="container-fluid pad-0">
            <div className="col-md-12 pad-0 vendorLogin">
                <div className="col-md-7 pad-0">
                    <div className="leftContainer">
                        <div className="topContent">
                            <svg className="logo_img" xmlns="http://www.w3.org/2000/svg" width="145" height="42" viewBox="0 0 195 42">
                                <g fill="none" fillRule="evenodd">
                                    <path fill="#000" d="M56.675 29.65c-1.3 0-2.546-.208-3.737-.625-1.192-.417-2.113-.967-2.763-1.65l.725-1.425c.633.633 1.475 1.146 2.525 1.538a9.226 9.226 0 0 0 3.25.587c1.567 0 2.742-.287 3.525-.863.783-.575 1.175-1.32 1.175-2.237 0-.7-.212-1.258-.638-1.675a4.304 4.304 0 0 0-1.562-.963c-.617-.225-1.475-.47-2.575-.737-1.317-.333-2.367-.654-3.15-.963a5.172 5.172 0 0 1-2.013-1.412c-.558-.633-.837-1.492-.837-2.575 0-.883.233-1.687.7-2.412.467-.726 1.183-1.305 2.15-1.738.967-.433 2.167-.65 3.6-.65 1 0 1.98.137 2.938.412.958.276 1.787.655 2.487 1.138l-.625 1.475a8.602 8.602 0 0 0-2.35-1.088 8.771 8.771 0 0 0-2.45-.362c-1.533 0-2.687.296-3.462.887-.776.592-1.163 1.355-1.163 2.288 0 .7.212 1.262.638 1.688.425.425.958.75 1.6.974.641.226 1.504.471 2.587.738 1.283.317 2.32.63 3.112.938a5.109 5.109 0 0 1 2.013 1.4c.55.625.825 1.47.825 2.537 0 .883-.237 1.687-.713 2.413-.475.725-1.204 1.3-2.187 1.724-.983.426-2.192.638-3.625.638zM78.7 16.35V29.5H77v-2.4a4.868 4.868 0 0 1-1.925 1.862c-.817.442-1.75.663-2.8.663-1.717 0-3.07-.48-4.063-1.438-.991-.958-1.487-2.362-1.487-4.212V16.35H68.5v7.45c0 1.383.342 2.433 1.025 3.15.683.717 1.658 1.075 2.925 1.075 1.383 0 2.475-.42 3.275-1.262.8-.842 1.2-2.013 1.2-3.513v-6.9H78.7zm11.75-.1c1.233 0 2.35.28 3.35.837a5.989 5.989 0 0 1 2.35 2.363c.567 1.017.85 2.175.85 3.475 0 1.317-.283 2.483-.85 3.5a6.013 6.013 0 0 1-2.338 2.363c-.991.558-2.112.837-3.362.837-1.067 0-2.03-.22-2.888-.663a5.472 5.472 0 0 1-2.112-1.937v7.325h-1.775v-18h1.7v2.6a5.468 5.468 0 0 1 2.112-2c.876-.467 1.863-.7 2.963-.7zm-.125 11.8c.917 0 1.75-.212 2.5-.637a4.503 4.503 0 0 0 1.763-1.813c.425-.783.637-1.675.637-2.675 0-1-.212-1.887-.637-2.663a4.61 4.61 0 0 0-1.763-1.812c-.75-.433-1.583-.65-2.5-.65-.933 0-1.77.217-2.513.65a4.636 4.636 0 0 0-1.75 1.813c-.425.775-.637 1.662-.637 2.662s.212 1.892.638 2.675a4.527 4.527 0 0 0 1.75 1.813c.741.425 1.579.637 2.512.637zm17.075-11.8c1.233 0 2.35.28 3.35.837a5.989 5.989 0 0 1 2.35 2.363c.567 1.017.85 2.175.85 3.475 0 1.317-.283 2.483-.85 3.5a6.013 6.013 0 0 1-2.337 2.363c-.992.558-2.113.837-3.363.837-1.067 0-2.03-.22-2.888-.663a5.472 5.472 0 0 1-2.112-1.937v7.325h-1.775v-18h1.7v2.6a5.468 5.468 0 0 1 2.112-2c.876-.467 1.863-.7 2.963-.7zm-.125 11.8c.917 0 1.75-.212 2.5-.637a4.503 4.503 0 0 0 1.762-1.813c.426-.783.638-1.675.638-2.675 0-1-.212-1.887-.638-2.663a4.61 4.61 0 0 0-1.762-1.812c-.75-.433-1.583-.65-2.5-.65-.933 0-1.77.217-2.513.65a4.636 4.636 0 0 0-1.75 1.813c-.425.775-.637 1.662-.637 2.662s.212 1.892.638 2.675a4.527 4.527 0 0 0 1.75 1.813c.741.425 1.579.637 2.512.637zm10.3-17.1h1.775V29.5h-1.775V10.95zm17.775 5.4l-6.575 14.725c-.533 1.233-1.15 2.108-1.85 2.625-.7.517-1.542.775-2.525.775-.633 0-1.225-.1-1.775-.3-.55-.2-1.025-.5-1.425-.9l.825-1.325c.667.667 1.467 1 2.4 1 .6 0 1.112-.167 1.538-.5.425-.333.82-.9 1.187-1.7l.575-1.275-5.875-13.125h1.85l4.95 11.175 4.95-11.175h1.75zm18.9-.1c1.65 0 2.946.475 3.887 1.425.942.95 1.413 2.35 1.413 4.2V29.5h-1.775v-7.45c0-1.367-.33-2.408-.988-3.125-.658-.717-1.587-1.075-2.787-1.075-1.367 0-2.442.42-3.225 1.262-.783.842-1.175 2.005-1.175 3.488v6.9h-1.775v-7.45c0-1.367-.33-2.408-.987-3.125-.659-.717-1.596-1.075-2.813-1.075-1.35 0-2.42.42-3.213 1.262-.791.842-1.187 2.005-1.187 3.488v6.9h-1.775V16.35h1.7v2.4a4.743 4.743 0 0 1 1.95-1.85c.833-.433 1.792-.65 2.875-.65 1.1 0 2.054.233 2.863.7.808.467 1.412 1.158 1.812 2.075.483-.867 1.18-1.546 2.087-2.037.909-.492 1.946-.738 3.113-.738zm10.125.1h1.775V29.5h-1.775V16.35zm.9-2.875c-.367 0-.675-.125-.925-.375s-.375-.55-.375-.9a1.2 1.2 0 0 1 .375-.875c.25-.25.558-.375.925-.375s.675.12.925.363c.25.241.375.529.375.862 0 .367-.125.675-.375.925s-.558.375-.925.375zM177.8 16.25c1.65 0 2.962.48 3.938 1.438.975.958 1.462 2.354 1.462 4.187V29.5h-1.775v-7.45c0-1.367-.342-2.408-1.025-3.125-.683-.717-1.658-1.075-2.925-1.075-1.417 0-2.537.42-3.362 1.262-.826.842-1.238 2.005-1.238 3.488v6.9H171.1V16.35h1.7v2.425a4.938 4.938 0 0 1 2.012-1.862c.859-.442 1.855-.663 2.988-.663zm17.15 12.45c-.333.3-.746.53-1.237.688a4.985 4.985 0 0 1-1.538.237c-1.233 0-2.183-.333-2.85-1-.667-.667-1-1.608-1-2.825v-7.95h-2.35v-1.5h2.35v-2.875h1.775v2.875h4v1.5h-4v7.85c0 .783.196 1.38.588 1.788.391.408.954.612 1.687.612.367 0 .72-.058 1.063-.175.341-.117.637-.283.887-.5l.625 1.275z" />
                                    <g fillRule="nonzero">
                                        <rect width="41.5" height="42" fill="#3B3B98" rx="9" />
                                        <g fill="#FFF" transform="rotate(2 -139.047 175.047)">
                                            <ellipse cx="15.75" cy="2.662" rx="2.662" ry="2.662" />
                                            <ellipse cx="15.75" cy="15.526" rx="2.662" ry="2.662" />
                                            <ellipse cx="2.662" cy="15.305" rx="2.662" ry="2.662" />
                                            <ellipse cx="7.986" cy="26.838" rx="2.662" ry="2.662" />
                                            <ellipse cx="23.292" cy="26.838" rx="2.662" ry="2.662" />
                                            <ellipse cx="28.838" cy="15.305" rx="2.662" ry="2.662" />
                                            <path d="M14.641 5.102h2.44v8.429h-2.44z" />
                                            <path d="M13.516 3.147l1.695 1.755L3.563 16.15 1.87 14.395zM14.244 15.309l1.974 1.434-7.17 9.87-1.974-1.435z" />
                                            <path d="M4.338 15.943l-2.212 1.031 4.406 9.448 2.211-1.03z" />
                                            <path d="M4.474 14.603l-1.257 2.092 18.497 11.114 1.256-2.092z" />
                                            <path d="M26.997 14.5l1.257 2.092L9.757 27.706 8.5 25.614z" />
                                            <path d="M26.94 15.943l2.212 1.03-4.406 9.45-2.212-1.032z" />
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <h1 className="m-top-30">Vendor Portal</h1>
                            <h4>Welcome to supplymint vendor portal</h4>
                        </div>
                        <div className="processImg"></div>
                        {/* <img className="processImg" src={ProcessIcon} /> */}
                    </div>
                </div>
                <div className="col-md-5 pad-0">
                          <form onSubmit={(e) => this.onSubmit(e)} autoComplete="off">
                    <div className="rightContainer">
                        <h4 className="m-top-30">Account signin</h4>
                        <p className="subTitle">Login to your account using your registered email and password</p>
                        <div className="vendorloginForm">
                            <div className="col-md-12 pad-0 m-top-20">
                                <label className="inputHead">Username</label>
                                <input type="text" value={uname} autoComplete="off" onChange={(e) => this.onChange(e)} id="uname" className={unameerr ?"errorBorder":""} placeholder="Username" />
                                    {unameerr ? (
                                                                <span className="error">
                                                                    Enter valid Username
                                                                </span>
                                                            ) : null}
                            </div>
                            <div className="col-md-12 pad-0 m-top-20">
                                <label className="inputHead">Password</label>
                                <input type="password" autoComplete="off" onChange={(e) => this.onChange(e)} id="password" className={passworderr ?"errorBorder":""} placeholder="Password" />
                                {passworderr ? (
                                                                <span className="error">
                                                                    Enter valid Password<br />
                                                                    {/* (must contain at least 1 lowercase alphabet, at least 1 uppercase alphabet, at least 1 numeric character ,at least one special character and must be eight characters or longer) */}
                                                               </span>
                                                            ) : null}
                                <div className="forgerPass textRight m-top-10">
                                    <label>Trouble in login ? <span>Reset password</span></label>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0 m-top-30">
                                <button  className="loginBtn">Login</button>
                                {/*<p className="notRegister m-top-10">Not Registered ? <span>Click here</span></p>*/}
                            </div>
                            
                        </div>
                    </div>
                        </form>
                    
                </div>
            </div>
       {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div >
    )
}}

export function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VendorLogin);