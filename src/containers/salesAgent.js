import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
const SideBar = React.lazy(() => import("../components/sidebar"));
const ManageSalesAgent = React.lazy(() => import("../components/salesAgent/manageSalesAgent"));
//const Contracts = React.lazy(() => import("../components/salesAgent/contracts"));
const RightSideBar = React.lazy(() => import("../components/rightSideBar"));
const Footer = React.lazy(() => import('../components/footer'));
const openRack = React.lazy(() => import("../assets/open-rack.svg"));
const openRackRight = React.lazy(() => import("../assets/rack-open-right.svg"));
const BraedCrumps = React.lazy(() => import("../components/breadCrumps"));
const NewSideBar = React.lazy(() => import("../components/newSidebar"));

class SalesAgent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rightbar: false,
      openRightBar: false
    };
  }

  // componentWillMount() {
  //   if (sessionStorage.getItem('token') == null) {
  //     this.props.history.push('/');
  //   }
  // }

  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }
  render() {
    $("input").attr("autocomplete", "off");
    const hash = window.location.hash.split("/")[2];
    return (
      <div className="container-fluid nc-design pad-l50">
        <NewSideBar {...this.props} />
        <SideBar {...this.props} />
        {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
        <div className="container_div m-top-75 " id="">
          <div className="col-md-12 col-sm-12 border-btm">
            <div className="menu_path d-table n-menu-path">
              <ul className="list-inline width_100 pad20 nmp-inner">
                <BraedCrumps {...this.props} />
              </ul>
            </div>
          </div>
        </div>
        {hash == "manageSalesAgents" ? (
          <ManageSalesAgent {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : 
        // hash == "contracts" ? (
        //   <Contracts {...this.props} rightSideBar={() => this.onRightSideBar()} />
        // ) : 
        null}
        <Footer />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    vendor: state.vendor,
    replenishment: state.replenishment,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SalesAgent);
