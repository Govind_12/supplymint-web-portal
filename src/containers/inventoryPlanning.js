import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import openRack from "../assets/open-rack.svg"
import RightSideBar from "../components/rightSideBar";
// import Footer from '../../footer'
import BraedCrumps from "../components/breadCrumps";
import AdHocRequest from "../components/inventoryPlanning/adHocRequest";
import AddNewAdHoc from "../components/inventoryPlanning/addNewAdHoc";
import AdHocModal from "../components/inventoryPlanning/adHocModal";
import ViewActivity from "../components/assortment/viewActivity";
import NewSideBar from "../components/newSidebar";
import PlanningParameter from "../components/inventoryPlanning/planningParameter";
import ManageAdhocRequest from "../components/inventoryPlanning/manageAdhocRequest";
import MrpRangePlanning from "../components/inventoryPlanning/mrpRangePlanning";
import Footer from "../components/footer";
import AssortmentPlanning from "../components/inventoryPlanning/assortmentPlanning";
import SiteMappingPlanning from "../components/inventoryPlanning/siteMappingPlanning";
import EventMappingPlanning from "../components/inventoryPlanning/eventMappingPlanning";
import SeasonMappingPlanning from "../components/inventoryPlanning/seasonMappingPlanning";
import AutoConfigNew from "../components/replenishment/autoConfigNew";
import RunOnDemand from "../components/replenishment/runOnDemand";
import CreateEventMappingPlanning from "../components/inventoryPlanning/createEvent";
import CreateSeasonMappingPlanning from "../components/inventoryPlanning/createSeason";
import CreateSiteMappingPlanning from "../components/inventoryPlanning/createSite";
import InventoryConfiguration from "../components/inventoryPlanning/inventoryConfiguration";
import CoreConfiguration from "../components/inventoryPlanning/coreConfiguration";
import CreateAdhocRequest from "../components/inventoryPlanning/createAdhocRequest";
import CustomDataUpload from "../components/inventoryPlanning/arsCustomData";
import ArsInventoryReport from "../components/inventoryPlanning/arsInventoryReport";
import ArsSalesInventoryReport from "../components/inventoryPlanning/arsSalesInventoryReport";
import ArsDashboard from "../components/dashboard/arsDashboard";

class InventoryPlanning extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      successModal: false,
      
    };
  }
  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }
  openSuccessModal(e) {

    this.setState({
      successModal: !this.state.successModal
    });
  }

  // componentWillMount(){
  //   if(sessionStorage.getItem('token') == null){
  //     this.props.history.push('/');
  //   }
  // }

  componentWillReceiveProps(nextProps) {
    
  }
  
  handler(e) {
    e.preventDefault();
    this.props.sampleRequest();
    // this.openSuccessModal();
  }
  render() {
    $("input").attr("autocomplete", "off");
    const { search } = this.state;
    var result = _.filter(this.state.data, function (data) {
      return _.startsWith(data.text, search);
    });
    let hash = window.location.hash;

    return (
      <div>
        <div className="container-fluid nc-design">
          <NewSideBar {...this.props} />
          <SideBar {...this.props} />
          {/* <img onClick={() => this.onRightSideBar()}  src={openRack} className="right_sidemenu rightstick" />
          {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
          <div className="container_div m-top-75 pad-l50" id="">
            <div className="col-md-12 col-sm-12 border-btm">
              <div className="menu_path d-table n-menu-path">
                <ul className="list-inline width_100 pad20 nmp-inner">
                  <BraedCrumps {...this.props} />
                </ul>
              </div>
            </div>
          </div>
          {/* {hash == "#/inventoryPlanning/autoConfiguration" ? <ReplenishmentAutoConfig {...this.props}/> : null} */}
          {/* {hash == "#/inventoryPlanning/runOnDemand" ? <ReplenishmentRunOnDemandNew {...this.props} /> : null} */}
          {hash == "#/inventoryPlanning/history" ? <HistoryReplenishment {...this.props}/> : null}
          {hash == "#/inventoryPlanning/summary" ? <NewSummary {...this.props}/> : null}
          { hash == "#/inventoryPlanning/manageAd-Hoc" ? (
          <AdHocRequest {...this.props}  />
        ) : null}
        { hash == "#/inventoryPlanning/viewActivity" ? (
          <ViewActivity {...this.props}  />
        ) : null}
        { hash == "#/inventoryPlanning/Ad-Hoc" ? (
          <AddNewAdHoc {...this.props}  />
        ) : null}
        { hash == "#/inventoryPlanning/adHocModal" ? (
          <AdHocModal {...this.props}  />
        ) : null}
        { hash == "#/inventoryPlanning/planningParameter" ? (
          <PlanningParameter {...this.props}  />
        ) : null}
        { hash == "#/inventoryPlanning/manageAdhocRequest" ? ( <ManageAdhocRequest {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/mrp-range" ? ( <MrpRangePlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/assortment" ? ( <AssortmentPlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/siteMapping" ? ( <SiteMappingPlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/event-mapping" ? ( <EventMappingPlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/season-mapping" ? ( <SeasonMappingPlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/inventoryAutoConfig" ? ( <AutoConfigNew {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/runOnDemand" ? ( <RunOnDemand {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/createEvent" ? ( <CreateEventMappingPlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/createSeason" ? ( <CreateSeasonMappingPlanning {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/createSite" ? ( <CreateSiteMappingPlanning {...this.props} /> ) :null}
        { hash == "#/configuration/inventoryConfiguration" ? ( <InventoryConfiguration {...this.props} /> ) :null}
        { hash == "#/configuration/coreConfiguration" ? ( <CoreConfiguration {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/createAdhocRequest" ? ( <CreateAdhocRequest {...this.props} /> ) :null}
        { hash == "#/inventoryPlanning/customDataUpload" ? ( <CustomDataUpload {...this.props} /> ) :null}
        { hash == "#/digiars/inventoryPlanningReport" ? ( <ArsInventoryReport {...this.props} /> ) :null}
        { hash == "#/digiars/salesInventoryReport" ? ( <ArsSalesInventoryReport {...this.props} /> ) :null}
        { hash == "#/ars-dashboard" ? ( <ArsDashboard {...this.props} /> ) :null}
        </div>
        <Footer />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return { 
    administration: state.administration,
    replenishment: state.replenishment,
    seasonPlanning: state.seasonPlanning,
    inventoryManagement: state.inventoryManagement };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InventoryPlanning);
