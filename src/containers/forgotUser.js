import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import illustrations from "../assets/illustrations.png";
import back from "../assets/back.svg";
import logo from "../assets/supplylogo.svg";
import pathdotted from "../assets/path-dotted.svg";
import errorIcon from "../assets/error_icon.svg"
import startCommas from "../assets/start.svg";
import endCommas from "../assets/endcommas.svg";
import FilterLoader from '../components/loaders/filterLoader'
import RequestError from '../components/loaders/requestError'
import exclamation from "../assets/exclamation.svg";
import RequestSuccess from "../components/loaders/requestSuccess";
import _ from 'lodash';

class ForgotUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            emailerr: false,
            alert: false,
            loader: false,
            success: false,
            errorMessage: "",
            errorCode: "",
            successMessage:"",
            code: "",
            totalSlide: ["digiars"],
            slideCounter: 1,
            currentSlide: "digiars",
            moduleName: ["digiars", "digivend", "digiproc", "digicat", "digiplan" ],
        };
    }

    nextButton() {
        var addStep =_.cloneDeep(this.state.totalSlide);
            if(this.state.slideCounter < 5){
            this.setState({
                slideCounter: this.state.slideCounter + 1
            }, () => {
                    if(this.state.slideCounter == 2) {
                        addStep.push("digivend")
                    }
                    if(this.state.slideCounter == 3) {
                        addStep.push("digiproc")
                    }
                    if(this.state.slideCounter == 4) {
                        addStep.push("digicat")
                    }
                    if(this.state.slideCounter == 5) {
                        addStep.push("digiplan")
                    }
                    if(addStep[addStep.length-1] == "digiars") {
                    this.setState({
                        currentSlide: "digiars",
                        totalSlide: addStep
                    })
                }
                else if(addStep[addStep.length-1] == "digivend"){
                    this.setState({
                        currentSlide: "digivend",
                        totalSlide: addStep
                    })
                }
                else if(addStep[addStep.length-1] == "digiproc"){
                    this.setState({
                        currentSlide: "digiproc",
                        totalSlide: addStep
                    })
                }
                else if(addStep[addStep.length-1] == "digicat"){
                    this.setState({
                        currentSlide: "digicat",
                        totalSlide: addStep
                    })
                }
                else if(addStep[addStep.length-1] == "digiplan"){
                    this.setState({
                        currentSlide: "digiplan",
                        totalSlide: addStep
                    })
                }
            })
        }
    }
    prevButton(){
        var addStep =_.cloneDeep(this.state.totalSlide);
        addStep.pop()
        if(addStep.length >= 1){
            this.setState({
                slideCounter: this.state.slideCounter - 1
            })
        }

        if(addStep[addStep.length-1] == "digiars") {
            this.setState({
                currentSlide: "digiars",
                totalSlide: addStep
            })
        }
        else if(addStep[addStep.length-1] == "digivend"){
            this.setState({
                currentSlide: "digivend",
                totalSlide: addStep
            })
        }
        else if(addStep[addStep.length-1] == "digiproc"){
            this.setState({
                currentSlide: "digiproc",
                totalSlide: addStep
            })
        }
        else if(addStep[addStep.length-1] == "digicat"){
            this.setState({
                currentSlide: "digicat",
                totalSlide: addStep
            })
        }
        else if(addStep[addStep.length-1] == "digiplan"){
            this.setState({
                currentSlide: "digiplan",
                totalSlide: addStep
            })
        }
    }
    componentDidMount() {
        var item = this.state.moduleName[Math.floor(Math.random() * this.state.moduleName.length)];
        this.setState ({
            currentSlide: item
        })
    }

    componentWillReceiveProps(nextprops){
        if(nextprops.auth.forgotUser.isSuccess){
            // this.props.history.push('/');
            this.setState({
                success: true,
                successMessage: nextprops.auth.forgotUser.data.message,
                loader: false
            })
        }else if(nextprops.auth.forgotUser.isError){
            this.setState({
                loader: false,
                alert: true,
                code: nextprops.auth.forgotUser.message.status,
                errorMessage: nextprops.auth.forgotUser.message.error == undefined ? undefined : nextprops.auth.forgotUser.message.error.message,
                errorCode: nextprops.auth.forgotUser.message.error == undefined ? undefined : nextprops.auth.forgotUser.message.error.code
            })
        }else if(nextprops.auth.forgotUser.isLoading){
            this.setState({
                loader: true,
                alert: false
            })
        }
    }

    email() {
        if (
            this.state.email !== "" &&
            !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }
    onChange(e) {
        e.preventDefault();
        this.setState({
            email: e.target.value
        }, () => {
            this.email();
        })
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
          success: false
        });
    }

    onSubmit(e) {
        e.preventDefault();
        this.email();
        const t = this;
        setTimeout(function () {
            const { emailerr, email } = t.state;
            if (!emailerr) {
                t.props.forgotUserRequest(email);
            }
        }, 100)
    }
    onError(e){
        e.preventDefault();
        this.setState({
            alert: false
        });
    }
    render() {
    
        const { email, emailerr } = this.state;
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-5 col-md-5 col-sm-12 pad-0">
                    <form className="main-signin-form" onSubmit={(e) => this.onSubmit(e)} autoComplete="off">
                        <div className="main-signin-page">
                            <div className="msp-head">
                                <a href="/#">
                                    <span className="msph-">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="38" height="20" viewBox="0 0 23.253 16.347">
                                            <path fill="#1e201d" d="M7.585 4.922a.824.824 0 0 1 1.172 1.16l-5.942 5.941H22.42a.825.825 0 0 1 .832.82.835.835 0 0 1-.832.832H2.815l5.941 5.93a.841.841 0 0 1 0 1.172.821.821 0 0 1-1.172 0L.237 13.43a.827.827 0 0 1 0-1.16z" transform="translate(.001 -4.676)"/>
                                        </svg>
                                    </span>
                                </a>
                                <div className="msph-logo">
                                    <img src={require('../assets/enterpriseLogo.svg')} />
                                </div>
                            </div>
                            <div className="msp-body">
                                <div className="mspb-top">
                                    <h3>Forgot Username</h3>
                                </div>
                                <div className="mspb-bottom m-top-60">
                                    <div className="mspbb-row">
                                        <label>Email</label>
                                        <input value={email} autoComplete="off" onChange={(e) => this.onChange(e)} className={emailerr ? "errorBorder" : ""} type="text" />
                                        {emailerr ? (<span className="error posRelative" >Enter valid email</span>) : null}
                                        <img src={exclamation} className="exclamation_img" />
                                        <p className="mspbb-para">Enter your registered email id to know your username.</p>
                                    </div>
                                    <div className="mspbb-row m-top-80">
                                       <p className="mspbb-para">You are agreeing to the <a href="https://www.supplymint.com/#/terms-of-use" target="_blank">Terms of services</a> and <a href="https://www.supplymint.com/#/privacy-policy" target="_blank">Privacy Policy</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="mspb-footer">
                                <button className="mspb-signin" type="submit">RESET</button>
                                <p>&copy; 2021 Supplymint</p>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-lg-7 col-md-7 col-sm-12 pad-0">
                    <div className="main-page-right">
                        <div className="mpr-content">
                            <div className="mpr-inner">
                                <div className="mpri-bgtop-img">
                                    <img src={require('../assets/bookeh-top.svg')} />
                                </div>
                                <div className="mpri-bgbottom-img">
                                    <img src={require('../assets/bookeh-bottom.svg')} />
                                </div>
                                <div className="mpr-head">
                                    <h3>At Supplymint</h3>
                                    <p><span>We modernize </span> your SCM Business Processes. <br />Our system recommends the following decisions based on<br /> your own data.</p>
                                </div>
                                <div className="mpr-body">
                                    <div className="mprb-slides">
                                        {this.state.currentSlide == "digiars" ?
                                        <div className="mprb-inner mprb-inner-one">
                                            <div className="mprbi-content">
                                                <div className="mprbi-left">
                                                    <img src={require('../assets/hp-ars.svg')} />
                                                </div>
                                                <div className="mprbi-right">
                                                    <h2>DigiARS</h2>
                                                    <p>Automated Replenishment - The platform that ensures the right place for all your products with an aim to eliminate wastage. DigiARS assists you in organising store-wise inventory (automated replenishment) and regulates a path for smart allocation.</p>
                                                    <a href="https://www.supplymint.com/#/inventory-planning" target="_blank" className="mprbi-know-more">Know More</a>
                                                </div>
                                            </div>
                                        </div>: null}
                                        {this.state.currentSlide == "digivend" ?
                                        <div className="mprb-inner mprb-inner-two">
                                            <div className="mprbi-content">
                                                <div className="mprbi-left">
                                                    <img src={require('../assets/digi-vendor-icon.svg')} />
                                                </div>
                                                <div className="mprbi-right">
                                                    <h2>DigiVend</h2>
                                                    <p>Placing, managing, checking, and tracking orders from multiple vendors is now quick and easy with DigiVend. Saving time and investment, retailers can leverage full control over multiple vendors from this one stop digital solution.</p>
                                                    <a href="https://www.supplymint.com/#/vendor-management" target="_blank" className="mprbi-know-more">Know More</a>
                                                </div>
                                            </div>
                                        </div>: null}
                                        {this.state.currentSlide == "digiproc" ?
                                        <div className="mprb-inner mprb-inner-three">
                                            <div className="mprbi-content">
                                                <div className="mprbi-left">
                                                    <img src={require('../assets/digi-proc-icon.svg')} />
                                                </div>
                                                <div className="mprbi-right">
                                                    <h2>DigiProc</h2>
                                                    <p>Procurement - The digital solution for your complex procurement needs. DigiProc makes the process of procurement smooth for retailers in just 3 segments - Creating a product indent, creating a product order, and a user interface to view everything from one place.</p>
                                                    <a href="https://www.supplymint.com/#/procurement" target="_blank" className="mprbi-know-more">Know More</a>
                                                </div>
                                            </div>
                                        </div>: null}
                                        {this.state.currentSlide == "digicat" ?
                                        <div className="mprb-inner mprb-inner-four">
                                            <div className="mprbi-content">
                                                <div className="mprbi-left">
                                                    <img src={require('../assets/ent-catalogue-icon.svg')} />
                                                </div>
                                                <div className="mprbi-right">
                                                    <h2>DigiCatalogue</h2>
                                                    <p>Catalogue - A digital solution for product discovery, DigiCatalogue lets you choose between a diverse range of products from multiple vendors. You can now negotiate your terms with vendors and add products listings digitally with absolute convenience.</p>
                                                    <a type="button" className="mprbi-know-more">Know More</a>
                                                </div>
                                            </div>
                                        </div>: null}
                                        {this.state.currentSlide == "digiplan" ?
                                        <div className="mprb-inner mprb-inner-four">
                                            <div className="mprbi-content">
                                                <div className="mprbi-left">
                                                    <img src={require('../assets/digiPlanIcon.svg')} />
                                                </div>
                                                <div className="mprbi-right">
                                                    <h2>DigiPlan</h2>
                                                    <p>Bringing forward the exact antidote for possible human errors in demand forecasting and planning, DigiPlan is an AI-powered software that pays attention to data and calculates purchasing budgets for future inventory orders and assortment strategies. With a structured OTB plan, you can now improve your financial performance effectively.</p>
                                                    <a href="https://www.supplymint.com/#/demand-planning" target="_blank" className="mprbi-know-more">Know More</a>
                                                </div>
                                            </div>
                                        </div>: null}
                                    </div>
                                    <div className="mprb-padination m-top-80">
                                        <button type="button" className="" onClick={() => this.prevButton()}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23.124" height="18.937" viewBox="0 0 25.124 20.937">
                                                <path fill={this.state.totalSlide.indexOf("digivend") > -1 ? "#3a5074" : "#b4c5d6"} d="M32.077 21.421H11.193l8-7.617a1.047 1.047 0 1 0-1.444-1.516l-9.133 8.7a2.093 2.093 0 0 0 .018 2.979l9.115 8.681a1.047 1.047 0 1 0 1.444-1.516l-8.03-7.617h20.914a1.047 1.047 0 1 0 0-2.094z" transform="translate(-8 -12)"/>
                                            </svg>
                                        </button>
                                        <button type="button" className="" onClick = {() => this.nextButton()}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23.124" height="18.936" viewBox="0 0 25.124 20.936">
                                                <path fill={this.state.totalSlide.indexOf("digiplan") > -1 ? "#b4c5d6": "#3a5074"} d="M9.047 21.421h20.884l-8-7.616a1.047 1.047 0 1 1 1.444-1.516l9.133 8.7a2.093 2.093 0 0 1-.018 2.979l-9.115 8.681a1.047 1.047 0 1 1-1.444-1.516l8.03-7.616H9.047a1.047 1.047 0 1 1 0-2.094z" transform="translate(-8 -12)"/>
                                            </svg>
                                        </button>
                                    </div>
                                    <div className="mprbp-filled">
                                        <span className={this.state.currentSlide == "digiars" ? "mprbpf-unfill mprbpf-filled" : "mprbpf-unfill"}></span>
                                        <span className={this.state.currentSlide == "digivend" ? "mprbpf-unfill mprbpf-filled" : "mprbpf-unfill"}></span>
                                        <span className={this.state.currentSlide == "digiproc" ? "mprbpf-unfill mprbpf-filled" : "mprbpf-unfill"}></span>
                                        <span className={this.state.currentSlide == "digicat" ? "mprbpf-unfill mprbpf-filled" : "mprbpf-unfill"}></span>
                                        <span className={this.state.currentSlide == "digiplan" ? "mprbpf-unfill mprbpf-filled" : "mprbpf-unfill"}></span>
                                    </div>
                                </div>
                                <div className="mpr-footer">
                                    <a href="https://www.facebook.com/supplymint1/" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.379" height="18.757" viewBox="0 0 9.379 18.757">
                                            <path fill="#fff" d="M13.854 3.114h1.712V.132A22.112 22.112 0 0 0 13.072 0C10.6 0 8.911 1.553 8.911 4.407v2.627H6.187v3.334h2.724v8.389h3.34v-8.388h2.614l.415-3.334h-3.03v-2.3c0-.964.26-1.623 1.6-1.623z" transform="translate(-6.187)"/>
                                        </svg>
                                    </a>
                                    <a href="https://twitter.com/supplymint" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18.148" height="15.558" viewBox="0 0 19.148 15.558">
                                            <g>
                                                <g>
                                                    <path fill="#fff" d="M19.148 49.842a8.184 8.184 0 0 1-2.262.62 3.9 3.9 0 0 0 1.727-2.17 7.845 7.845 0 0 1-2.489.95 3.925 3.925 0 0 0-6.79 2.684 4.042 4.042 0 0 0 .091.9 11.111 11.111 0 0 1-8.091-4.106 3.927 3.927 0 0 0 1.206 5.247 3.877 3.877 0 0 1-1.774-.483v.043a3.944 3.944 0 0 0 3.145 3.857 3.918 3.918 0 0 1-1.029.129 3.471 3.471 0 0 1-.743-.067 3.963 3.963 0 0 0 3.668 2.735A7.888 7.888 0 0 1 .939 61.85 7.352 7.352 0 0 1 0 61.8a11.051 11.051 0 0 0 6.022 1.762 11.1 11.1 0 0 0 11.173-11.17c0-.174-.006-.341-.014-.507a7.831 7.831 0 0 0 1.967-2.043z" transform="translate(0 -48)"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                    <a href="https://www.linkedin.com/showcase/supplymint" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" id="linkedin_2_" width="18.208" height="18.988" viewBox="0 0 19.208 19.988">
                                            <path fill="#fff" id="Path_1108" d="M17.813 161.387h4.395v13.688h-4.395z" class="cls-1" transform="translate(-17.508 -155.086)"/>
                                            <path fill="#fff" id="Path_1109" d="M12.5 0A2.5 2.5 0 1 0 15 2.5 2.505 2.505 0 0 0 12.5 0z" class="cls-1" transform="translate(-10)"/>
                                            <path fill="#fff" id="Path_1110" d="M191.415 155.26a4.678 4.678 0 0 0-4.754-4.549 4.749 4.749 0 0 0-3.684 1.735v-1.318H178.8v13.688h4.395V156.9a1.912 1.912 0 1 1 3.824 0v7.912h4.395z" class="cls-1" transform="translate(-172.209 -144.827)"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <div className="noise_background signInMain">
                    <div className="col-md-12 col-sm-12 pad-0">
                        <div className="col-md-3 col-sm-12 pad-0 height100">
                            <div className="left_side_container">
                                <div className="col-md-12 col-sm-12 pad-0">
                                    <div className="top_container">
                                        <div className="col-md-12 col-sm-12 pad-0">
                                        <ul className="list-inline m-lft-15">
                                            <a href="/#">
                                                <li>
                                                    <img src={back} className="back_icon" />
                                                </li>
                                                <li>
                                                    <label className="back_home displayPointer">
                                                        Back
                                            </label>
                                                </li>
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-xs-6 m-top-50 pad-0">
                                        <form onSubmit={(e) => this.onSubmit(e)} autoComplete="off">
                                            <div className="middle_container">
                                                <div className="col-md-12 col-sm-12 pad-0 m-top-60">
                                                    <ul className="list-inline m-top-20">
                                                        <li>
                                                            <h4 className="signin">
                                                                FORGOT USERNAME
                                                    </h4>
                                                        </li>

                                                        <li>
                                                            <input value={email} autoComplete="off" onChange={(e) => this.onChange(e)} className={emailerr ? "input_box m-top-20 errorBorder" : "input_box m-top-20"} type="text" placeholder="Email" />
                                                            {emailerr ? <img src={errorIcon} className="error_icon_img" /> : null}
                                                            {emailerr ? (
                                                                <span className="error" >
                                                                    Enter valid email
                                                            </span>
                                                            ) : null}
                                                        </li>
                                                        <li>
                                                            <img src={exclamation} className="exclamation_img" />
                                                            <p className="forgot_pass_text">
                                                                Enter your registered email id to
                                                                know your username.
                                                    </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="col-md-12 col-sm-12 pad-0 m-top-35">
                                                    <div className="oval">
                                                        <i className="fa fa-angle-left oval_icon"></i>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 col-sm-12 pad-0 m-top-100">
                                                    <button className="signin_btn" type="submit">
                                                        RESET
                                            </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="bottom_position">
                                            <ul className="pad-0">
                                                <li>
                                                    <p>
                                                    &copy; Supplymint
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        SUPPLYMINT
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="col-md-9 col-sm-12 pad-0">
                            <div className="right_side_container">
                                <div className="col-md-12 colsm-12 pad-0 m-top-35">
                                    <div className="supply_mint_logo_div">
                                        <div className="supply_mint_logo">
                                            <img src={logo} className="mint_logo" />

                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 pad-0">
                                    <div className="path_img">
                                        <img src={pathdotted} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 m-top-5">
                                    <div className="content_sigin text-center">
                                        <ul className="list-inline text-center">
                                            <li>
                                            <p>
                                                <img src={startCommas} />Your Supply Chain Solution<img src={endCommas} className="signCommaEnd" />
                                                </p>
                                            </li>
                                            <li>
                                            <p>
                                                We modernize your SCM Business Processes.<br />
                                                Our system recommends the following decisions based on your own data.
                                                 </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="flow_diagram_div">
                                        <img src={illustrations} className="illustration_img" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> */}
                {this.state.loader ? <FilterLoader/> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} />  : null}
            </div>
        );
    }
}

export function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ForgotUser);
