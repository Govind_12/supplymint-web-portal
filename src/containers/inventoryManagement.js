import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import BraedCrumps from "../components/breadCrumps";
import openRack from "../assets/open-rack.svg";
import Footer from '../components/footer'
import Assortment from '../components/assortment/assortment'

class InventoryManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openRightBar: false,
      rightbar: false
    };
  }
  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  // componentWillMount(){
  //   if(sessionStorage.getItem('token') == null){
  //     this.props.history.push('/');
  //   }
  // }

  componentWillReceiveProps(nextProps) {

  }

  render() {
    $("input").attr("autocomplete", "off");
    let hash = window.location.hash;
    return (
      <div className="container-fluid">
        <div className="container_div" id="home"></div>
        <SideBar {...this.props} />
        <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}

        <div className="container_div m-top-100 " id="">
          <div className="col-md-12 col-sm-12">
            <div className="menu_path">
              <ul className="list-inline width_100 pad20">
                <BraedCrumps {...this.props} />
              </ul>
            </div>
          </div>
        </div>
        {hash == "#/inventoryManagement/assortment" ? <Assortment {...this.props} /> : null}
        <Footer />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    inventoryManagement: state.inventoryManagement
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InventoryManagement);
