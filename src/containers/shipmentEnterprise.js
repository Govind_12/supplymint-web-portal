import React from 'react';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';
import * as actions from '../redux/actions';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from '../components/loaders/requestSuccess';
import RequestError from '../components/loaders/requestError';
import RightSideBar from "../components/rightSideBar";
import AsnUnderApprovalEnterprise from '../components/vendorPortal/enterprise/shipment/asnUnderApprovalEnterprise';
import ApprovedAsnEnterprise from '../components/vendorPortal/enterprise/shipment/approvedAsnEnterprise';
import RejectedAsnEnterprise from '../components/vendorPortal/enterprise/shipment/RejectedAsnEnterprise';
import NewSettings from '../assets/new-settings.svg';
import Help from '../assets/help.svg';
import NewSideBar from '../components/newSidebar';

class ShipmentEnterprise extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            loader: false,
            successMessage: "",
            success: false,
            alert: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            resources:"",
            isDownloadPage: false,
        };
    }
    // componentWillMount() {
    //     if (sessionStorage.getItem('token') == null) {
    //         this.props.history.push('/');
    //     }
    // }
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
      componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
            this.setState({ loader: false, success: false, alert: false, isDownloadPage: false, })
      }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.shipment.getShipment.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getShipmentClear()
        } else if (nextProps.shipment.getShipment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getShipment.message.error == undefined ? undefined : nextProps.shipment.getShipment.message.error.errorMessage,
                errorCode: nextProps.shipment.getShipment.message.error == undefined ? undefined : nextProps.shipment.getShipment.message.error.errorCode,
                code: nextProps.shipment.getShipment.message.status,
                alert: true,
                loader: false
            })
            this.props.getShipmentClear()
        }
        if (nextProps.shipment.shipmentConfirmCancel.isSuccess) {
            this.setState({
                successMessage: nextProps.shipment.shipmentConfirmCancel.data.message,
                loader: false,
                success: true,
            })
            this.props.shipmentConfirmCancelClear()
        } else if (nextProps.shipment.shipmentConfirmCancel.isError) {
            this.setState({
                errorMessage: nextProps.shipment.shipmentConfirmCancel.message.error == undefined ? undefined : nextProps.shipment.shipmentConfirmCancel.message.error.errorMessage,
                errorCode: nextProps.shipment.shipmentConfirmCancel.message.error == undefined ? undefined : nextProps.shipment.shipmentConfirmCancel.message.error.errorCode,
                code: nextProps.shipment.shipmentConfirmCancel.message.status,
                alert: true,
                loader: false
            })
            this.props.shipmentConfirmCancelClear()
        }
        if (nextProps.shipment.getCompleteDetailShipment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getCompleteDetailShipmentClear()
        } else if (nextProps.shipment.getCompleteDetailShipment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getCompleteDetailShipment.message.error == undefined ? undefined : nextProps.shipment.getCompleteDetailShipment.message.error.errorMessage,
                errorCode: nextProps.shipment.getCompleteDetailShipment.message.error == undefined ? undefined : nextProps.shipment.getCompleteDetailShipment.message.error.errorCode,
                code: nextProps.shipment.getCompleteDetailShipment.message.status,
                alert: true,
                loader: false
            })
            this.props.getCompleteDetailShipmentClear()
        }
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getAllCommentClear()
        } else if (nextProps.shipment.getAllComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage,
                errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
                code: nextProps.shipment.getAllComment.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllCommentClear()
        }
        if (nextProps.shipment.qcAddComment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.qcAddCommentClear()
        } else if (nextProps.shipment.qcAddComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorMessage,
                errorCode: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorCode,
                code: nextProps.shipment.qcAddComment.message.status,
                alert: true,
                loader: false
            })
            this.props.qcAddCommentClear()
        }
        if (nextProps.shipment.deleteUploads.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.shipment.deleteUploads.isError) {
            this.setState({
                errorMessage: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorMessage,
                errorCode: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorCode,
                code: nextProps.shipment.deleteUploads.message.status,
                alert: true,
                loader: false
            })
            this.props.deleteUploadsClear()
        }
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.shipment.getAllComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage,
                errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
                code: nextProps.shipment.getAllComment.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllCommentClear()
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getHeaderConfigClear()
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            })
            this.props.getHeaderConfigClear()
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createHeaderConfigClear()
        } else if (nextProps.replenishment.createHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            })
            this.props.createHeaderConfigClear()
        }
        //create main header
        if (nextProps.replenishment.createMainHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createMainHeaderConfigClear()
        } else if (nextProps.replenishment.createMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
            })
            this.props.createMainHeaderConfigClear()
        }
        //create set header
        if (nextProps.replenishment.createSetHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createSetHeaderConfigClear()
        } else if (nextProps.replenishment.createSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage
            })
            this.props.createSetHeaderConfigClear()
        }
        //create  item header
        if (nextProps.replenishment.createItemHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createItemHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createItemHeaderConfigClear()
        } else if (nextProps.replenishment.createItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorMessage
            })
            this.props.createItemHeaderConfigClear()
        }
        //custom column setting heaader
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getMainHeaderConfigClear()
        } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
            })
            this.props.getMainHeaderConfigClear()
        }
        if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getSetHeaderConfigClear()
        } else if (nextProps.replenishment.getSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorMessage
            })
            this.props.getSetHeaderConfigClear()
        }
        if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getItemHeaderConfigClear()
        } else if (nextProps.replenishment.getItemHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getItemHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorMessage
            })
            this.props.getItemHeaderConfigClear()
        }
        // add multiple comment qc
        if (nextProps.shipment.addMultipleCommentQc.isSuccess) {
            this.setState({
                successMessage: nextProps.shipment.addMultipleCommentQc.data.message,
                loader: false,
                success: true,
            })
            this.props.addMultipleCommentQcClear()
        } else if (nextProps.shipment.addMultipleCommentQc.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.shipment.addMultipleCommentQc.message.status,
                errorCode: nextProps.shipment.addMultipleCommentQc.message.error == undefined ? undefined : nextProps.shipment.addMultipleCommentQc.message.error.errorCode,
                errorMessage: nextProps.shipment.addMultipleCommentQc.message.error == undefined ? undefined : nextProps.shipment.addMultipleCommentQc.message.error.errorMessage
            })
            this.props.addMultipleCommentQcClear()
        }
        // --------------------------
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({
                loader: false,
            })
        }
        if (nextProps.orders.multipleDocumentDownload.isSuccess) {
            this.setState({
                successMessage: nextProps.orders.multipleDocumentDownload.data.message,
                resources:nextProps.orders.multipleDocumentDownload.data.resource,
                loader: false,
                success: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
                isDownloadPage: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
            },()=>{
                if( this.state.resources !== undefined && this.state.resources != null )
                   window.open(`${this.state.resources}`)
                // window.location.href = this.state.resources;
            
            })
            // window.open(`${this.state.resources}`)
            // window.location.href = this.state.resources;
              
            this.props.multipleDocumentDownloadClear()
        }
        else if (nextProps.orders.multipleDocumentDownload.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.multipleDocumentDownload.message.status,
                errorCode: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorCode,
                errorMessage: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorMessage
            })
            this.props.multipleDocumentDownloadClear()
        }
        if (nextProps.orders.cancelAndRejectReason.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (nextProps.orders.cancelAndRejectReason.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.cancelAndRejectReason.message.status,
                errorCode: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorCode,
                errorMessage: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorMessage
            })
            this.props.getCancelAndRejectReasonClear()
        }
        if (nextProps.orders.multipleDocumentDownload.isLoading ||
             nextProps.shipment.getAllComment.isLoading && 
             nextProps.shipment.getAllComment.data.refresh == "whole"
              || nextProps.shipment.addMultipleCommentQc.isLoading || 
              nextProps.replenishment.createHeaderConfig.isLoading || 
              nextProps.replenishment.getHeaderConfig.isLoading || 
              nextProps.shipment.getShipment.isLoading || 
              nextProps.shipment.shipmentConfirmCancel.isLoading || 
              nextProps.shipment.getCompleteDetailShipment.isLoading ||
            nextProps.shipment.deleteUploads.isLoading||
            nextProps.replenishment.createMainHeaderConfig.isLoading || 
             nextProps.replenishment.createSetHeaderConfig.isLoading || 
             nextProps.replenishment.createItemHeaderConfig.isLoading || 
             nextProps.replenishment.getMainHeaderConfig.isLoading || 
            nextProps.replenishment.getSetHeaderConfig.isLoading || 
            nextProps.replenishment.getItemHeaderConfig.isLoading ||
            nextProps.orders.cancelAndRejectReason.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false,
            isDownloadPage: false,
        });
    }
    shipmentTrackingMethod = _ => {
        this.props.shipmentTrackingRequest({
            shipmentId: _.id,
            orderId: _.orderId,
            asnNo: _.shipmentAdviceCode,
            orderNo: _.orderNumber,
            documentNumber: _.documentNumber
        })
        this.enterpriseChild.shipmentTrackModal()
    }
    // shipmentTrackModal = () => {
    //     this.setState({ shipmentModal: !this.state.shipmentModal })
    // }
    render() {
        const hash = window.location.hash.split("/")[3];
        return (
            <div>
                <div className="container-fluid nc-design pad-l50">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-top-75 m-t-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                                {/* <div className="nmp-right">
                                    <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button>
                                    <button className="nmp-help">Need Help <img src={Help}></img></button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    {hash == "asnUnderApproval" ? (
                        // <AsnUnderApprovalEnterprise {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingMethod={this.shipmentTrackingMethod} />
                        <AsnUnderApprovalEnterprise {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.enterpriseChild = ref)}/>
                    ) : hash == "approvedAsn" ? (
                        // <ApprovedAsnEnterprise {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackModal={this.shipmentTrackModal} shipmentTrackingMethod={this.shipmentTrackingMethod} />
                        <ApprovedAsnEnterprise {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.enterpriseChild = ref)}/>
                    ) : hash == "cancelledAsn" ? (
                        <RejectedAsnEnterprise {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : null}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    </div>
                    <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        shipment: state.shipment,
        replenishment: state.replenishment,
        orders: state.orders,
        logistic: state.logistic,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShipmentEnterprise);