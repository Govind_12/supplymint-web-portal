import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import BraedCrumps from '../components/breadCrumps';
import Footer from '../components/footer';
import NewSideBar from '../components/newSidebar';
import SideBar from '../components/sidebar';
import AccountConfiguration from '../components/vendorPortal/invoiceManagement/accountConfiguration';
import CreateCustomer from '../components/vendorPortal/invoiceManagement/createCustomer';
import CreateInvoice from '../components/vendorPortal/invoiceManagement/createInvoice';
import CreateItems from '../components/vendorPortal/invoiceManagement/createItems';
import Customer from '../components/vendorPortal/invoiceManagement/customers';
import DeliveryChallan from '../components/vendorPortal/invoiceManagement/deliveryChallan';
import Estimate from '../components/vendorPortal/invoiceManagement/estimate';
import Invoices from '../components/vendorPortal/invoiceManagement/invoices';
import Items from '../components/vendorPortal/invoiceManagement/items';
import * as actions from "../redux/actions";
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';

class InvoiceManagement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
        };
    }

    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
            this.setState({ loader: false, success: false, alert: false })
        }
    }

    componentWillReceiveProps(nextProps) {
         if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
            })
            this.props.getMainHeaderConfigClear()
        }
        if(nextProps.replenishment.createMainHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                loader: false,
                success: true,
            })
        }else if (nextProps.replenishment.createMainHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createMainHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
            })
            this.props.createMainHeaderConfigClear();
        }

        if(nextProps.invoiceManagement.getInvoiceManagementItem.isSuccess) {
            this.setState({
                loader: false 
            })
        }else if (nextProps.invoiceManagement.getInvoiceManagementItem.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.getInvoiceManagementItem.message.status,
                errorCode: nextProps.invoiceManagement.getInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.getInvoiceManagementItem.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.getInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.getInvoiceManagementItem.message.error.errorMessage
            })
            this.props.getInvoiceManagementItemClear();
        }

        if(nextProps.invoiceManagement.deleteInvoiceManagementItem.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.deleteInvoiceManagementItem.data.resource.response,
                loader: false,
                success: true,
            })
        }else if (nextProps.invoiceManagement.deleteInvoiceManagementItem.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.deleteInvoiceManagementItem.message.status,
                errorCode: nextProps.invoiceManagement.deleteInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.deleteInvoiceManagementItem.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.deleteInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.deleteInvoiceManagementItem.message.error.errorMessage
            })
            this.props.deleteInvoiceManagementItemClear();
        }

        if(nextProps.invoiceManagement.saveInvoiceManagementItem.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.saveInvoiceManagementItem.data.resource.response,
                loader: false,
                success: true,
            })
            this.props.saveInvoiceManagementItemClear();
            this.props.history.push("/invoiceManagement/items")
        }else if (nextProps.invoiceManagement.saveInvoiceManagementItem.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.saveInvoiceManagementItem.message.status,
                errorCode: nextProps.invoiceManagement.saveInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.saveInvoiceManagementItem.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.saveInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.saveInvoiceManagementItem.message.error.errorMessage
            })
            this.props.saveInvoiceManagementItemClear();
        }

        if(nextProps.invoiceManagement.updateInvoiceManagementItem.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.updateInvoiceManagementItem.data.resource.response,
                loader: false,
                success: true,
            })
            this.props.updateInvoiceManagementItemClear();
            this.props.history.push("/invoiceManagement/items")
        }else if (nextProps.invoiceManagement.updateInvoiceManagementItem.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.updateInvoiceManagementItem.message.status,
                errorCode: nextProps.invoiceManagement.updateInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.updateInvoiceManagementItem.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.updateInvoiceManagementItem.message.error == undefined ? undefined : nextProps.invoiceManagement.updateInvoiceManagementItem.message.error.errorMessage
            })
            this.props.updateInvoiceManagementItemClear();
        }

        if(nextProps.invoiceManagement.getInvoiceManagementCustomer.isSuccess) {
            this.setState({
                loader: false 
            })
        }else if (nextProps.invoiceManagement.getInvoiceManagementCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.getInvoiceManagementCustomer.message.status,
                errorCode: nextProps.invoiceManagement.getInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.getInvoiceManagementCustomer.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.getInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.getInvoiceManagementCustomer.message.error.errorMessage
            })
            this.props.getInvoiceManagementCustomerClear();
        }

        if(nextProps.invoiceManagement.deleteInvoiceManagementCustomer.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.deleteInvoiceManagementCustomer.data.resource.response,
                loader: false,
                success: true,
            })
        }else if (nextProps.invoiceManagement.deleteInvoiceManagementCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.deleteInvoiceManagementCustomer.message.status,
                errorCode: nextProps.invoiceManagement.deleteInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.deleteInvoiceManagementCustomer.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.deleteInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.deleteInvoiceManagementCustomer.message.error.errorMessage
            })
            this.props.deleteInvoiceManagementCustomerClear();
        }

        if(nextProps.invoiceManagement.saveInvoiceManagementCustomer.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.saveInvoiceManagementCustomer.data.resource.response,
                loader: false,
                success: true,
            })
            this.props.saveInvoiceManagementCustomerClear();
            this.props.history.push("/invoiceManagement/customers")
        }else if (nextProps.invoiceManagement.saveInvoiceManagementCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.saveInvoiceManagementCustomer.message.status,
                errorCode: nextProps.invoiceManagement.saveInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.saveInvoiceManagementCustomer.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.saveInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.saveInvoiceManagementCustomer.message.error.errorMessage
            })
            this.props.saveInvoiceManagementCustomerClear();
        }

        if(nextProps.invoiceManagement.updateInvoiceManagementCustomer.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.updateInvoiceManagementCustomer.data.resource.response,
                loader: false,
                success: true,
            })
            this.props.updateInvoiceManagementCustomerClear();
            this.props.history.push("/invoiceManagement/customers")
        }else if (nextProps.invoiceManagement.updateInvoiceManagementCustomer.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.updateInvoiceManagementCustomer.message.status,
                errorCode: nextProps.invoiceManagement.updateInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.updateInvoiceManagementCustomer.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.updateInvoiceManagementCustomer.message.error == undefined ? undefined : nextProps.invoiceManagement.updateInvoiceManagementCustomer.message.error.errorMessage
            })
            this.props.updateInvoiceManagementCustomerClear();
        }

        if(nextProps.invoiceManagement.getInvoiceManagementGeneric.isSuccess) {
            this.setState({
                loader: false 
            })
        }else if (nextProps.invoiceManagement.getInvoiceManagementGeneric.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.getInvoiceManagementGeneric.message.status,
                errorCode: nextProps.invoiceManagement.getInvoiceManagementGeneric.message.error == undefined ? undefined : nextProps.invoiceManagement.getInvoiceManagementGeneric.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.getInvoiceManagementGeneric.message.error == undefined ? undefined : nextProps.invoiceManagement.getInvoiceManagementGeneric.message.error.errorMessage
            })
            this.props.getInvoiceManagementGenericClear();
        }
        if(nextProps.invoiceManagement.expandInvoiceManagementGeneric.isSuccess) {
            this.setState({
                loader: false 
            })
        }else if (nextProps.invoiceManagement.expandInvoiceManagementGeneric.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.expandInvoiceManagementGeneric.message.status,
                errorCode: nextProps.invoiceManagement.expandInvoiceManagementGeneric.message.error == undefined ? undefined : nextProps.invoiceManagement.expandInvoiceManagementGeneric.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.expandInvoiceManagementGeneric.message.error == undefined ? undefined : nextProps.invoiceManagement.expandInvoiceManagementGeneric.message.error.errorMessage
            })
            this.props.expandInvoiceManagementGenericClear();
        }
        if(nextProps.invoiceManagement.deleteInvoiceManagementGeneric.isSuccess) {
            this.setState({
                successMessage: nextProps.invoiceManagement.deleteInvoiceManagementGeneric.data.resource.message,
                loader: false,
                success: true,
            })
        }else if (nextProps.invoiceManagement.deleteInvoiceManagementGeneric.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.deleteInvoiceManagementGeneric.message.status,
                errorCode: nextProps.invoiceManagement.deleteInvoiceManagementGeneric.message.error == undefined ? undefined : nextProps.invoiceManagement.deleteInvoiceManagementGeneric.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.deleteInvoiceManagementGeneric.message.error == undefined ? undefined : nextProps.invoiceManagement.deleteInvoiceManagementGeneric.message.error.errorMessage
            })
            this.props.deleteInvoiceManagementGenericClear();
        }
        if(nextProps.replenishment.createSetHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                loader: false,
                success: true,
            })
        }else if (nextProps.replenishment.createSetHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createSetHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage
            })
            this.props.createSetHeaderConfigClear();
        }

        if(nextProps.invoiceManagement.getCoreDropdown.isSuccess) {
            this.setState({
                loader: false
            });
            //this.props.getCoreDropdownClear();
        }
        else if (nextProps.invoiceManagement.getCoreDropdown.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.getCoreDropdown.message.status,
                errorCode: nextProps.invoiceManagement.getCoreDropdown.message.error == undefined ? undefined : nextProps.invoiceManagement.getCoreDropdown.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.getCoreDropdown.message.error == undefined ? undefined : nextProps.invoiceManagement.getCoreDropdown.message.error.errorMessage
            })
            this.props.getCoreDropdownClear();
        }

        if(nextProps.invoiceManagement.searchInvoiceData.isSuccess) {
            this.setState({
                loader: false
            });
            this.props.searchInvoiceDataClear();
        }
        else if (nextProps.invoiceManagement.searchInvoiceData.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.searchInvoiceData.message.status,
                errorCode: nextProps.invoiceManagement.searchInvoiceData.message.error == undefined ? undefined : nextProps.invoiceManagement.searchInvoiceData.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.searchInvoiceData.message.error == undefined ? undefined : nextProps.invoiceManagement.searchInvoiceData.message.error.errorMessage
            })
            this.props.searchInvoiceDataClear();
        }

        if(nextProps.invoiceManagement.createInvoice.isSuccess) {
            this.setState({
                loader: false
            });
            this.props.createInvoiceClear();
        }
        else if (nextProps.invoiceManagement.createInvoice.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.invoiceManagement.createInvoice.message.status,
                errorCode: nextProps.invoiceManagement.createInvoice.message.error == undefined ? undefined : nextProps.invoiceManagement.createInvoice.message.error.errorCode,
                errorMessage: nextProps.invoiceManagement.createInvoice.message.error == undefined ? undefined : nextProps.invoiceManagement.createInvoice.message.error.errorMessage
            })
            this.props.createInvoiceClear();
        }

        if (nextProps.invoiceManagement.getInvoiceManagementItem.isLoading || nextProps.replenishment.getMainHeaderConfig.isLoading
            || nextProps.replenishment.createMainHeaderConfig.isLoading || nextProps.invoiceManagement.deleteInvoiceManagementItem.isLoading 
            || nextProps.invoiceManagement.saveInvoiceManagementItem.isLoading || nextProps.invoiceManagement.updateInvoiceManagementItem.isLoading
            || nextProps.invoiceManagement.getInvoiceManagementCustomer.isLoading || nextProps.invoiceManagement.deleteInvoiceManagementCustomer.isLoading
            || nextProps.invoiceManagement.saveInvoiceManagementCustomer.isLoading || nextProps.invoiceManagement.updateInvoiceManagementCustomer.isLoading
            || nextProps.invoiceManagement.getInvoiceManagementGeneric.isLoading || nextProps.invoiceManagement.deleteInvoiceManagementGeneric.isLoading
            || nextProps.invoiceManagement.expandInvoiceManagementGeneric.isLoading || nextProps.replenishment.createSetHeaderConfig.isLoading
            || nextProps.invoiceManagement.getCoreDropdown.isLoading || nextProps.invoiceManagement.searchInvoiceData.isLoading
            || nextProps.invoiceManagement.createInvoice.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    render() {
        $("input").attr("autocomplete", "off");
        let hash = window.location.hash;
        return (
            <div>
                 <div className="container-fluid nc-design pad-l50">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    <div className="container_div m-top-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path d-table n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div>
                    {hash == "#/invoiceManagement/configuration" ? ( <AccountConfiguration {...this.props} />) :null}
                    {hash == "#/invoiceManagement/customers" ? ( <Customer {...this.props} /> ) :null}
                    {hash == "#/invoiceManagement/createCustomer" ? ( <CreateCustomer {...this.props} />) :null}
                    {hash == "#/invoiceManagement/items" ? ( <Items {...this.props} />) :null}
                    {hash == "#/invoiceManagement/createItem" ? ( <CreateItems {...this.props} />) :null}
                    {hash == "#/invoiceManagement/invoices" ? ( <Invoices {...this.props} />) :null}
                    {hash == "#/invoiceManagement/createInvoice?type=invoice" ? ( <CreateInvoice {...this.props} />) :null}
                    {hash == "#/invoiceManagement/createInvoice?type=drnote" ? ( <CreateInvoice {...this.props} />) :null}
                    {hash == "#/invoiceManagement/createInvoice?type=crnote" ? ( <CreateInvoice {...this.props} />) :null}
                    {hash == "#/invoiceManagement/createInvoice?type=estimate" ? ( <CreateInvoice {...this.props} />) :null}
                    {hash == "#/invoiceManagement/createInvoice?type=challan" ? ( <CreateInvoice {...this.props} />) :null}
                    {hash == "#/invoiceManagement/estimate" ? ( <Estimate {...this.props} />) :null}
                    {hash == "#/invoiceManagement/deliveryChallan" ? ( <DeliveryChallan {...this.props} />) :null}
                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return { 
        replenishment: state.replenishment,
        invoiceManagement: state.invoiceManagement,
    };
}
  
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InvoiceManagement);