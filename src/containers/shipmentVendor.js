import React from 'react';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';
import * as actions from '../redux/actions';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from '../components/loaders/requestSuccess';
import RequestError from '../components/loaders/requestError';
import RightSideBar from "../components/rightSideBar";
import AsnUnderApprovalVendor from '../components/vendorPortal/vendor/shipment/asnUnderApprovalVendor';
import ApprovedAsnVendor from '../components/vendorPortal/vendor/shipment/approvedAsnVendor';
import CancelledAsnVendor from '../components/vendorPortal/vendor/shipment/cancelledAsnVendor';
import NewSettings from '../assets/new-settings.svg';
import Help from '../assets/help.svg';
import NewSideBar from '../components/newSidebar';

class ShipmentVendor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            loader: false,
            successMessage: "",
            success: false,
            alert: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            shipmentModal: false,
            isDownloadPage: false,
        };
    }
    // componentWillMount() {
    //     if (sessionStorage.getItem('token') == null) {
    //         this.props.history.push('/');
    //     }
    // }
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
      componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    escFun = (e) =>{
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop") || e.target.className.includes("alertPopUp")))){
            this.setState({ loader: false, success: false, alert: false, isDownloadPage: false, })
      }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.getAddressData.isSuccess) {
            this.setState({
              loader: false
            })
            this.props.getAddressRequest()
          } else if (nextProps.orders.getAddressData.isError) {
            this.setState({
              errorMessage: nextProps.orders.getAddressData.message.error == undefined ? undefined : nextProps.orders.getAddressData.message.error.errorMessage,
      
              errorCode: nextProps.orders.getAddressData.message.error == undefined ? undefined : nextProps.orders.getAddressData.message.error.errorCode,
              code: nextProps.orders.getAddressData.message.status,
              alert: true,
              loader: false
      
            })
            this.props.getAddressRequest()
          }else if (!nextProps.orders.getAddressData.isLoading) {
            this.setState({
              loader: false,
      
            })
          }
          if (nextProps.orders.getLocationData.isSuccess) {
            this.setState({
              loader: false
            })
            this.props.getLocationRequest()
          } else if (nextProps.orders.getLocationData.isError) {
            this.setState({
              errorMessage: nextProps.orders.getLocationData.message.error == undefined ? undefined : nextProps.orders.getLocationData.message.error.errorMessage,
      
              errorCode: nextProps.orders.getLocationData.message.error == undefined ? undefined : nextProps.orders.getLocationData.message.error.errorCode,
              code: nextProps.orders.getLocationData.message.status,
              alert: true,
              loader: false
      
            })
            this.props.getLocationRequest()
          }
          else if (!nextProps.orders.getLocationData.isLoading) {
            this.setState({
              loader: false,
      
            })
          }
        if (nextProps.shipment.getAllShipmentVendor.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getAllShipmentVendorClear()
        } else if (nextProps.shipment.getAllShipmentVendor.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getAllShipmentVendor.message.error == undefined ? undefined : nextProps.shipment.getAllShipmentVendor.message.error.errorMessage,
                errorCode: nextProps.shipment.getAllShipmentVendor.message.error == undefined ? undefined : nextProps.shipment.getAllShipmentVendor.message.error.errorCode,
                code: nextProps.shipment.getAllShipmentVendor.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllShipmentVendorClear()
        }

        if (nextProps.shipment.updateVendor.isSuccess) {
            this.setState({
                successMessage: nextProps.shipment.updateVendor.data.message,
                loader: false,
                success: true,
            })
            this.props.updateVendorClear()
        } else if (nextProps.shipment.updateVendor.isError) {
            this.setState({
                errorMessage: nextProps.shipment.updateVendor.message.error == undefined ? undefined : nextProps.shipment.updateVendor.message.error.errorMessage,
                errorCode: nextProps.shipment.updateVendor.message.error == undefined ? undefined : nextProps.shipment.updateVendor.message.error.errorCode,
                code: nextProps.shipment.updateVendor.message.status,
                alert: true,
                loader: false
            })
            this.props.updateVendorClear()
        }

        if (nextProps.shipment.getShipmentDetails.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getShipmentDetailsClear()
        } else if (nextProps.shipment.getShipmentDetails.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getShipmentDetails.message.error == undefined ? undefined : nextProps.shipment.getShipmentDetails.message.error.errorMessage,
                errorCode: nextProps.shipment.getShipmentDetails.message.error == undefined ? undefined : nextProps.shipment.getShipmentDetails.message.error.errorCode,
                code: nextProps.shipment.getShipmentDetails.message.status,
                alert: true,
                loader: false
            })
            this.props.getShipmentDetailsClear()
        }
        if (nextProps.shipment.deleteUploads.isSuccess) {
            this.setState({
                successMessage: nextProps.shipment.deleteUploads.data.message,
                loader: false,
                success: true,
            })
        } else if (nextProps.shipment.deleteUploads.isError) {
            this.setState({
                errorMessage: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorMessage,
                errorCode: nextProps.shipment.deleteUploads.message.error == undefined ? undefined : nextProps.shipment.deleteUploads.message.error.errorCode,
                code: nextProps.shipment.deleteUploads.message.status,
                alert: true,
                loader: false
            })
            this.props.deleteUploadsClear()
        }
        if (nextProps.shipment.getAllComment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.getAllCommentClear()
        } else if (nextProps.shipment.getAllComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorMessage,
                errorCode: nextProps.shipment.getAllComment.message.error == undefined ? undefined : nextProps.shipment.getAllComment.message.error.errorCode,
                code: nextProps.shipment.getAllComment.message.status,
                alert: true,
                loader: false
            })
            this.props.getAllCommentClear()
        }
        if (nextProps.shipment.qcAddComment.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.qcAddCommentClear()
        } else if (nextProps.shipment.qcAddComment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorMessage,
                errorCode: nextProps.shipment.qcAddComment.message.error == undefined ? undefined : nextProps.shipment.qcAddComment.message.error.errorCode,
                code: nextProps.shipment.qcAddComment.message.status,
                alert: true,
                loader: false
            })
            this.props.qcAddCommentClear()
        }
        if (nextProps.shipment.updateVendorShipment.isSuccess) {
            this.setState({
                loader: false,
                successMessage: nextProps.shipment.updateVendorShipment.data.message,
                success: true,
            })
            this.props.updateVendorShipmentClear()
        } else if (nextProps.shipment.updateVendorShipment.isError) {
            this.setState({
                errorMessage: nextProps.shipment.updateVendorShipment.message.error == undefined ? undefined : nextProps.shipment.updateVendorShipment.message.error.errorMessage,
                errorCode: nextProps.shipment.updateVendorShipment.message.error == undefined ? undefined : nextProps.shipment.updateVendorShipment.message.error.errorCode,
                code: nextProps.shipment.updateVendorShipment.message.status,
                alert: true,
                loader: false
            })
            this.props.updateVendorShipmentClear()
        }
        if (nextProps.shipment.updateInvoice.isSuccess) {
            this.setState({
                loader: false,
                successMessage: nextProps.shipment.updateInvoice.data.message,
                success: true,
            })
            this.props.updateInvoiceClear()
        } else if (nextProps.shipment.updateInvoice.isError) {
            this.setState({
                errorMessage: nextProps.shipment.updateInvoice.message.error == undefined ? undefined : nextProps.shipment.updateInvoice.message.error.errorMessage,
                errorCode: nextProps.shipment.updateInvoice.message.error == undefined ? undefined : nextProps.shipment.updateInvoice.message.error.errorCode,
                code: nextProps.shipment.updateInvoice.message.status,
                alert: true,
                loader: false
            })
            this.props.updateInvoiceClear()
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.getHeaderConfigClear()
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            })
            this.props.getHeaderConfigClear()
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
            this.setState({
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
                loader: false,
                success: true,
            })
            this.props.createHeaderConfigClear()
        } else if (nextProps.replenishment.createHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            })
            this.props.createHeaderConfigClear()
        }
        if (nextProps.orders.shipmentTracking.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.shipmentTrackingClear()
        } else if (nextProps.orders.shipmentTracking.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.shipmentTracking.message.status,
                errorCode: nextProps.orders.shipmentTracking.message.error == undefined ? undefined : nextProps.orders.shipmentTracking.message.error.errorCode,
                errorMessage: nextProps.orders.shipmentTracking.message.error == undefined ? undefined : nextProps.orders.shipmentTracking.message.error.errorMessage
            })
            this.props.createHeaderConfigClear()
        }
        if (nextProps.orders.multipleDocumentDownload.isSuccess) {
            this.setState({
                successMessage: nextProps.orders.multipleDocumentDownload.data.message,
                loader: false,
                success: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
                isDownloadPage: nextProps.orders.multipleDocumentDownload.data.resource == undefined || nextProps.orders.multipleDocumentDownload.data.resource == null ? true : false,
            },()=>{
                if( nextProps.orders.multipleDocumentDownload.data.resource !== undefined && nextProps.orders.multipleDocumentDownload.data.resource != null )
                   window.open(`${nextProps.orders.multipleDocumentDownload.data.resource}`)       
            })
            this.props.multipleDocumentDownloadClear()
        }
        else if (nextProps.orders.multipleDocumentDownload.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.orders.multipleDocumentDownload.message.status,
                errorCode: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorCode,
                errorMessage: nextProps.orders.multipleDocumentDownload.message.error == undefined ? undefined : nextProps.orders.multipleDocumentDownload.message.error.errorMessage
            })
            this.props.multipleDocumentDownloadClear()
        }

                //custom column setting heaader
                if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
                    this.setState({
                        loader: false
                    })
                    this.props.getMainHeaderConfigClear()
                } else if (nextProps.replenishment.getMainHeaderConfig.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.replenishment.getMainHeaderConfig.message.status,
                        errorCode: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorCode,
                        errorMessage: nextProps.replenishment.getMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getMainHeaderConfig.message.error.errorMessage
                    })
                    this.props.getMainHeaderConfigClear()
                }
                if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
                    this.setState({
                        loader: false
                    })
                    this.props.getSetHeaderConfigClear()
                } else if (nextProps.replenishment.getSetHeaderConfig.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.replenishment.getSetHeaderConfig.message.status,
                        errorCode: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorCode,
                        errorMessage: nextProps.replenishment.getSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getSetHeaderConfig.message.error.errorMessage
                    })
                    this.props.getSetHeaderConfigClear()
                }
                if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
                    this.setState({
                        loader: false
                    })
                    this.props.getItemHeaderConfigClear()
                } else if (nextProps.replenishment.getItemHeaderConfig.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.replenishment.getItemHeaderConfig.message.status,
                        errorCode: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorCode,
                        errorMessage: nextProps.replenishment.getItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getItemHeaderConfig.message.error.errorMessage
                    })
                    this.props.getItemHeaderConfigClear()
                }
        
                //create main header
                if (nextProps.replenishment.createMainHeaderConfig.isSuccess) {
                    this.setState({
                        successMessage: nextProps.replenishment.createMainHeaderConfig.data.message,
                        loader: false,
                        success: true,
                    })
                    this.props.createMainHeaderConfigClear()
                } else if (nextProps.replenishment.createMainHeaderConfig.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.replenishment.createMainHeaderConfig.message.status,
                        errorCode: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorCode,
                        errorMessage: nextProps.replenishment.createMainHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createMainHeaderConfig.message.error.errorMessage
                    })
                    this.props.createMainHeaderConfigClear()
                }
                //create set header
                if (nextProps.replenishment.createSetHeaderConfig.isSuccess) {
                    this.setState({
                        successMessage: nextProps.replenishment.createSetHeaderConfig.data.message,
                        loader: false,
                        success: true,
                    })
                    this.props.createSetHeaderConfigClear()
                } else if (nextProps.replenishment.createSetHeaderConfig.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.replenishment.createSetHeaderConfig.message.status,
                        errorCode: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorCode,
                        errorMessage: nextProps.replenishment.createSetHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createSetHeaderConfig.message.error.errorMessage
                    })
                    this.props.createSetHeaderConfigClear()
                }
                //create  item header
                if (nextProps.replenishment.createItemHeaderConfig.isSuccess) {
                    this.setState({
                        successMessage: nextProps.replenishment.createItemHeaderConfig.data.message,
                        loader: false,
                        success: true,
                    })
                    this.props.createItemHeaderConfigClear()
                } else if (nextProps.replenishment.createItemHeaderConfig.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.replenishment.createItemHeaderConfig.message.status,
                        errorCode: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorCode,
                        errorMessage: nextProps.replenishment.createItemHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createItemHeaderConfig.message.error.errorMessage
                    })
                    this.props.createItemHeaderConfigClear()
                }
                if (nextProps.orders.cancelAndRejectReason.isSuccess) {
                    this.setState({
                        loader: false
                    })
                } else if (nextProps.orders.cancelAndRejectReason.isError) {
                    this.setState({
                        loader: false,
                        alert: true,
                        code: nextProps.orders.cancelAndRejectReason.message.status,
                        errorCode: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorCode,
                        errorMessage: nextProps.orders.cancelAndRejectReason.message.error == undefined ? undefined : nextProps.orders.cancelAndRejectReason.message.error.errorMessage
                    })
                    this.props.getCancelAndRejectReasonClear()
                }
        if (nextProps.orders.multipleDocumentDownload.isLoading || nextProps.orders.shipmentTracking.isLoading || nextProps.shipment.getAllComment.isLoading && nextProps.shipment.getAllComment.data.refresh == "fullPage" || nextProps.replenishment.createHeaderConfig.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.shipment.getAllShipmentVendor.isLoading || nextProps.shipment.updateInvoice.isLoading || nextProps.shipment.updateVendorShipment.isLoading || nextProps.shipment.updateVendor.isLoading || nextProps.shipment.getShipmentDetails.isLoading
         || nextProps.shipment.deleteUploads.isLoading ||
         nextProps.replenishment.getMainHeaderConfig.isLoading || 
            nextProps.replenishment.getSetHeaderConfig.isLoading || 
            nextProps.replenishment.getItemHeaderConfig.isLoading ||
            nextProps.replenishment.createMainHeaderConfig.isLoading || 
             nextProps.replenishment.createSetHeaderConfig.isLoading || 
             nextProps.replenishment.createItemHeaderConfig.isLoading ||
             nextProps.orders.cancelAndRejectReason.isLoading
        ) {
            this.setState({
                loader: true
            })
        }
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false,
            isDownloadPage: false,
        });
    }
    shipmentTrackingMethod = _ => {
        this.props.shipmentTrackingRequest({
            shipmentId: _.id,
            orderId: _.orderId,
            asnNo: _.shipmentAdviceCode,
            orderNo: _.orderNumber,
            documentNumber: _.documentNumber
        })
        this.shipmentVendorChild.shipmentTrackModal()
    }
    // shipmentTrackModal = () => {
    //     this.setState({ shipmentModal: !this.state.shipmentModal })
    // }
    render() {
        
        const hash = window.location.hash.split("/")[3];
        return (
            <div>
                <div className="container-fluid nc-design pad-l50">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-top-75 m-t-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                                {/* <div className="nmp-right">
                                    <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button>
                                    <button className="nmp-help">Need Help <img src={Help}></img></button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    {hash == "asnUnderApproval" ? (
                        <AsnUnderApprovalVendor {...this.state} {...this.props} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.shipmentVendorChild = ref)}/>
                    ) : hash == "approvedAsn" ? (
                        <ApprovedAsnVendor {...this.props} {...this.state} rightSideBar={() => this.onRightSideBar()} shipmentTrackingMethod={this.shipmentTrackingMethod} onReff={ref => (this.shipmentVendorChild = ref)}/>
                    ) : hash == "cancelledAsn" ? (
                        <CancelledAsnVendor {...this.props} rightSideBar={() => this.onRightSideBar()} />
                    ) : null}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
                <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        shipment: state.shipment,
        replenishment: state.replenishment,
        orders: state.orders,
        logistic: state.logistic,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShipmentVendor);