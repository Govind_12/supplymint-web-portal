import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import RightSideBar from "../components/rightSideBar";
import FilterLoader from '../components/loaders/filterLoader';
import RequestSuccess from '../components/loaders/requestSuccess';
import RequestError from '../components/loaders/requestError';
import openRack from "../assets/open-rack.svg"
import Footer from '../components/footer'
import BraedCrumps from "../components/breadCrumps";
import GeneralSetting from '../components/changeSetting/generalSetting';
import NotificationSetting from '../components/changeSetting/notificationSetting';
import ConfigurationSetting from '../components/changeSetting/configurationSetting';
import OtbSetting from '../components/changeSetting/otbSetting';
import ProcurementSetting from '../components/changeSetting/procurementSetting';
import NewSideBar from '../components/newSidebar';

class ChangeSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: "general",
            loader: true,
            errorCode: "",
            success: false,
            successMessage: "",
            alert: false,
            notifyWrite: false,
            code: "",
            errorMessage: "",
            numberFormat: "",
            decimalPlaces: "",
            currency: "",
            timeFormat: "",
            dateFormat: "",
            currentNumberFormat: "",
            currentDecimalPlaces: "",
            currentCurrency: "",
            currentTimeFormat: "",
            currentDateFormat: "",
            symbol: false,
            seperator: false,
            module: [],
            selectedModule: "",
            subModule: [],
            selectedSubModule: "",
            emailStatus: "",
            property: [],
            selectedProperty: "",
            to: [],
            cc: [],
            addto: "",
            addcc: "",
            generalSave: false,
            // defaultFestival: [],
            // customFestival: [],
            selected: "",
            saveFest: false,
            otbSettingData: [],
            otbSettingKeys: [],
            checkedData: [],

            otbSave: false,
            startDate: "",
            endDate: "",
            dateSelect: false,
            startDateErr: false,
            endDateErr: false,
            prefix: "",
            year: "",
            randomNo: "",
            asnSave: false,
            valueStr: "",
            shipTrackMenu: "asnFormat",
            needUpdate: false,
        }
    }
    checkedDatafun(data) {

        let checkedData = this.state.checkedData
        if (checkedData.includes(data)) {

            for (let i = 0; i < checkedData.length; i++) {
                if (checkedData[i] == data) {
                    checkedData.splice(i, 1)
                }
            }
            this.setState({
                otbSave: true,
                checkedData: checkedData,
            })
        } else {

            checkedData = checkedData.concat(data)

            this.setState({
                otbSave: true,
                checkedData: checkedData,

            })
        }
    }
    onClearOtb() {
        this.setState({
            checkedData: []
        })
    }
    onSubmit() {
        let otbSettingKeys = this.state.otbSettingKeys
        let payload = {}

        for (let i = 0; i < otbSettingKeys.length; i++) {
            if (this.state.checkedData.includes(this.state.otbSettingData[otbSettingKeys[i]])) {
                payload[otbSettingKeys[i]] = this.state.otbSettingData[otbSettingKeys[i]]
            }
        }
        this.setState({
            otbSave: false
        })
        this.props.settingAssortOtbRequest(payload)
        this.onClearOtb()
    }
    componentWillMount() {
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }
        this.props.getSettingRequest('data');
        // this.props.getAllOtbRequest()
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.changeSetting.createSetting.isLoading && !nextProps.changeSetting.createSetting.isSuccess && !nextProps.changeSetting.createSetting.isError) {
            this.setState({
                loader: false
            })
        }

        if (!nextProps.changeSetting.getSetting.isLoading && !nextProps.changeSetting.getSetting.isSuccess && !nextProps.changeSetting.getSetting.isError) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.changeSetting.getAllOtb.isSuccess) {
            if (nextProps.changeSetting.getAllOtb.data.resource != null) {
                var checkedData = nextProps.changeSetting.getAllOtb.data.resource
                checkedData = Object.values(checkedData)
                this.setState({
                    checkedData
                })
            }
            // this.props.getAllOtbClear();
            this.setState({
                loader: false
            })
        } else if (nextProps.changeSetting.getAllOtb.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getAllOtb.message.error == undefined ? undefined : nextProps.changeSetting.getAllOtb.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getAllOtb.message.status,
                errorCode: nextProps.changeSetting.getAllOtb.message.error == undefined ? undefined : nextProps.changeSetting.getAllOtb.message.error.errorCode
            })
            // this.props.getAllOtbClear();
        }
        if (nextProps.changeSetting.settingAssortOtb.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.settingAssortOtb.data.message,
                loader: false,
                success: true,
            })
            this.props.settingAssortOtbRequest();
        } else if (nextProps.changeSetting.settingAssortOtb.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.settingAssortOtb.message.error == undefined ? undefined : nextProps.changeSetting.settingAssortOtb.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.settingAssortOtb.message.status,
                errorCode: nextProps.changeSetting.settingAssortOtb.message.error == undefined ? undefined : nextProps.changeSetting.settingAssortOtb.message.error.errorCode
            })
            this.props.settingAssortOtbRequest();
        }

        if (nextProps.changeSetting.otbGetDefault.isSuccess) {
            if (nextProps.changeSetting.otbGetDefault.data.resource != null) {
                this.setState({
                    otbSettingData: nextProps.changeSetting.otbGetDefault.data.resource,
                    otbSettingKeys: Object.keys(nextProps.changeSetting.otbGetDefault.data.resource)
                })
            }
            this.props.otbGetDefaultClear();
            this.setState({
                loader: false
            })
        } else if (nextProps.changeSetting.otbGetDefault.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.otbGetDefault.message.error == undefined ? undefined : nextProps.changeSetting.otbGetDefault.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.otbGetDefault.message.status,
                errorCode: nextProps.changeSetting.otbGetDefault.message.error == undefined ? undefined : nextProps.changeSetting.otbGetDefault.message.error.errorCode
            })
            this.props.otbGetDefaultClear();
        }
        if (nextProps.changeSetting.otbGetHlevel.isSuccess) {

            this.setState({

                loader: false,
            })

        } else if (nextProps.changeSetting.otbGetHlevel.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.otbGetHlevel.message.error == undefined ? undefined : nextProps.changeSetting.otbGetHlevel.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.otbGetHlevel.message.status,
                errorCode: nextProps.changeSetting.otbGetHlevel.message.error == undefined ? undefined : nextProps.changeSetting.otbGetHlevel.message.error.errorCode
            })
            this.props.otbGetHlevelClear();
        }

        if (nextProps.changeSetting.settingOtbCreate.isSuccess) {

            this.setState({

                loader: false,
                success: true,
                successMessage: nextProps.changeSetting.settingOtbCreate.data.message,
            })
            this.props.settingOtbCreateRequest();
        } else if (nextProps.changeSetting.settingOtbCreate.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.settingOtbCreate.message.error == undefined ? undefined : nextProps.changeSetting.settingOtbCreate.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.settingOtbCreate.message.status,
                errorCode: nextProps.changeSetting.settingOtbCreate.message.error == undefined ? undefined : nextProps.changeSetting.settingOtbCreate.message.error.errorCode
            })
            this.props.settingOtbCreateRequest();
        }
        if (nextProps.changeSetting.getEmailStatus.isSuccess) {
            if (nextProps.changeSetting.getEmailStatus.data.resource != null) {
                this.setState({
                    emailStatus: nextProps.changeSetting.getEmailStatus.data.resource.status,
                    loader: false,
                })
            }
        } else if (nextProps.changeSetting.getEmailStatus.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getEmailStatus.message.error == undefined ? undefined : nextProps.changeSetting.getEmailStatus.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getEmailStatus.message.status,
                errorCode: nextProps.changeSetting.getEmailStatus.message.error == undefined ? undefined : nextProps.changeSetting.getEmailStatus.message.error.errorCode
            })
            this.props.getEmailStatusClear()
        }

        if (nextProps.changeSetting.getModule.isSuccess) {
            if (nextProps.changeSetting.getModule.data.resource != null) {
                let response = [];
                let data = nextProps.changeSetting.getModule.data.resource;
                data.forEach(d => {
                    let key = Object.keys(d)[0];
                    let val = Object.values(d)[0];
                    response.push({ key: key, value: val })
                })
                this.setState({
                    loader: false,
                    module: response
                })
            }
        } else if (nextProps.changeSetting.getModule.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getModule.message.error == undefined ? undefined : nextProps.changeSetting.getModule.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getModule.message.status,
                errorCode: nextProps.changeSetting.getModule.message.error == undefined ? undefined : nextProps.changeSetting.getModule.message.error.errorCode
            })
            this.props.getModuleClear()
        }

        if (nextProps.changeSetting.updateEmailStatus.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.updateEmailStatus.data.message,
                loader: false,
                success: true,
            })
            this.props.updateEmailStatusRequest();
        } else if (nextProps.changeSetting.updateEmailStatus.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.updateEmailStatus.message.error == undefined ? undefined : nextProps.changeSetting.updateEmailStatus.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.updateEmailStatus.message.status,
                errorCode: nextProps.changeSetting.updateEmailStatus.message.error == undefined ? undefined : nextProps.changeSetting.updateEmailStatus.message.error.errorCode
            })
            this.props.updateEmailStatusRequest();
        }

        if (nextProps.changeSetting.getProperty.isSuccess) {
            if (nextProps.changeSetting.getProperty.data.resource != null) {
                let response = [];
                let data = nextProps.changeSetting.getProperty.data.resource;
                data.forEach(d => {
                    let key = Object.keys(d)[0];
                    let val = Object.values(d)[0];
                    response.push({ key: key, value: val })
                })
                this.setState({
                    loader: false,
                    property: response
                })
            }
        } else if (nextProps.changeSetting.getProperty.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getProperty.message.error == undefined ? undefined : nextProps.changeSetting.getProperty.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getProperty.message.status,
                errorCode: nextProps.changeSetting.getProperty.message.error == undefined ? undefined : nextProps.changeSetting.getProperty.message.error.errorCode
            })
        }

        if (nextProps.changeSetting.getSubModule.isSuccess) {
            if (nextProps.changeSetting.getSubModule.data.resource != null) {
                let response = [];
                let data = nextProps.changeSetting.getSubModule.data.resource;
                data.forEach(d => {
                    let key = Object.keys(d)[0];
                    let val = Object.values(d)[0];
                    response.push({ key: key, value: val })
                })
                this.setState({
                    loader: false,
                    subModule: response
                })
            }
        } else if (nextProps.changeSetting.getSubModule.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getSubModule.message.error == undefined ? undefined : nextProps.changeSetting.getSubModule.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getSubModule.message.status,
                errorCode: nextProps.changeSetting.getSubModule.message.error == undefined ? undefined : nextProps.changeSetting.getSubModule.message.error.errorCode
            })
        }

        if (nextProps.changeSetting.createSetting.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.createSetting.data.message,
                loader: false,
                success: true,
                numberFormat: "",
                decimalPlaces: "",
                currency: "",
                timeFormat: "",
                dateFormat: "",
                generalSave: false,
                symbol: false,
                seperator: false
            })
            this.props.createSettingRequest();
        } else if (nextProps.changeSetting.createSetting.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.createSetting.message.error == undefined ? undefined : nextProps.changeSetting.createSetting.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.createSetting.message.status,
                errorCode: nextProps.changeSetting.createSetting.message.error == undefined ? undefined : nextProps.changeSetting.createSetting.message.error.errorCode
            })
            this.props.createSettingRequest();
        }

        if (nextProps.changeSetting.getSetting.isSuccess) {
            if (nextProps.changeSetting.getSetting.data.resource != null) {
                this.setState({
                    loader: false,
                    numberFormat: nextProps.changeSetting.getSetting.data.resource.numberFormat,
                    decimalPlaces: nextProps.changeSetting.getSetting.data.resource.numberFormatDecimalValue,
                    currency: nextProps.changeSetting.getSetting.data.resource.currency,
                    timeFormat: nextProps.changeSetting.getSetting.data.resource.timeFormat,
                    dateFormat: nextProps.changeSetting.getSetting.data.resource.dateFormat,

                    currentNumberFormat: nextProps.changeSetting.getSetting.data.resource.numberFormat,
                    currentDecimalPlaces: nextProps.changeSetting.getSetting.data.resource.numberFormatDecimalValue,
                    currentCurrency: nextProps.changeSetting.getSetting.data.resource.currency,
                    currentTimeFormat: nextProps.changeSetting.getSetting.data.resource.timeFormat,
                    currentDateFormat: nextProps.changeSetting.getSetting.data.resource.dateFormat,

                    symbol: nextProps.changeSetting.getSetting.data.resource.currencyUseSymbol == "false" ? false : true,
                    seperator: nextProps.changeSetting.getSetting.data.resource.numberFormatUseSeperator == "false" ? false : true,
                })
            }
            this.props.getSettingRequest();
        } else if (nextProps.changeSetting.getSetting.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getSetting.message.error == undefined ? undefined : nextProps.changeSetting.getSetting.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getSetting.message.status,
                errorCode: nextProps.changeSetting.getSetting.message.error == undefined ? undefined : nextProps.changeSetting.getSetting.message.error.errorCode
            })
            this.props.getSettingRequest();
        }

        if (nextProps.changeSetting.getToCc.isSuccess) {
            this.setState({
                loader: false,
            })
            if (nextProps.changeSetting.getToCc.data.resource != null) {
                this.setState({
                    to: nextProps.changeSetting.getToCc.data.resource.to,
                    cc: nextProps.changeSetting.getToCc.data.resource.cc,
                })
                if( nextProps.changeSetting.getToCc.data.resource.to.length > 0){
                    this.setState({ needUpdate: true, notifyWrite: true,})
                }else{
                    this.setState({ needUpdate: false, notifyWrite: false})
                }
            }
            this.props.getToCcRequest();
        } else if (nextProps.changeSetting.getToCc.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getToCc.message.error == undefined ? undefined : nextProps.changeSetting.getToCc.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getToCc.message.status,
                errorCode: nextProps.changeSetting.getToCc.message.error == undefined ? undefined : nextProps.changeSetting.getToCc.message.error.errorCode
            })
            this.props.getToCcRequest();
        }

        if (nextProps.changeSetting.saveToCc.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.saveToCc.data.message,
                loader: false,
                success: true,
                to: [],
                cc: [],
                selectedModule: "",
                subModule: [],
                selectedSubModule: "",
                property: [],
                addcc: "",
                addto: "",
                selectedProperty: "",
                notifyWrite: false
            })
            this.props.saveToCcClear();
        } else if (nextProps.changeSetting.saveToCc.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.saveToCc.message.error == undefined ? undefined : nextProps.changeSetting.saveToCc.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.saveToCc.message.status,
                errorCode: nextProps.changeSetting.saveToCc.message.error == undefined ? undefined : nextProps.changeSetting.saveToCc.message.error.errorCode
            })
            this.props.saveToCcClear();
        }
        if (nextProps.changeSetting.updateToCc.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.updateToCc.data.message,
                loader: false,
                success: true,
                to: [],
                cc: [],
                selectedModule: "",
                subModule: [],
                selectedSubModule: "",
                property: [],
                addcc: "",
                addto: "",
                selectedProperty: "",
                notifyWrite: false
            })
            this.props.updateToCcClear();
        } else if (nextProps.changeSetting.updateToCc.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.updateToCc.message.error == undefined ? undefined : nextProps.changeSetting.updateToCc.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.updateToCc.message.status,
                errorCode: nextProps.changeSetting.updateToCc.message.error == undefined ? undefined : nextProps.changeSetting.updateToCc.message.error.errorCode
            })
            this.props.updateToCcClear();
        }
        // _______________________________GET ALL CONFIG __________________________________

        if (nextProps.changeSetting.saveToCc.isLoading || nextProps.changeSetting.getAsnFormat.isLoading || nextProps.changeSetting.createAsnFormat.isLoading || nextProps.changeSetting.getToCc.isLoading || nextProps.changeSetting.updateEmailStatus.isLoading || nextProps.changeSetting.getEmailStatus.isLoading || nextProps.changeSetting.getProperty.isLoading || nextProps.changeSetting.getSubModule.isLoading || nextProps.changeSetting.getModule.isLoading || nextProps.changeSetting.createSetting.isLoading
            || nextProps.changeSetting.otbGetDefault.isLoading || nextProps.changeSetting.getSetting.isLoading || nextProps.changeSetting.otbGetHlevel.isLoading || nextProps.changeSetting.settingOtbCreate.isLoading) {
            this.setState({
                loader: true
            })
        }

        // -----------------------------------------purchaseOrderDateRange-----------------------------


    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    onEmailStatus(e) {
        this.props.updateEmailStatusRequest(this.state.emailStatus == "Active" ? "Inactive" : "Active")
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    handleChnage(e) {
        if (e.target.id == "numberFormat") {
            this.setState({
                numberFormat: e.target.value,
                generalSave: true
            })
        } else if (e.target.id == "decimalPlaces") {
            this.setState({
                decimalPlaces: e.target.value,
                generalSave: true
            })
        } else if (e.target.id == "currency") {
            this.setState({
                currency: e.target.value,
                generalSave: true
            })
        } else if (e.target.id == "timeFormat") {
            this.setState({
                timeFormat: e.target.value,
                generalSave: true
            })
        } else if (e.target.id == "dateFormat") {
            this.setState({
                dateFormat: e.target.value,
                generalSave: true
            })
        } else if (e.target.id == "symbol") {
            this.setState({
                symbol: !this.state.symbol,
                generalSave: true
            })
        } else if (e.target.id == "seperator") {
            this.setState({
                seperator: !this.state.seperator,
                generalSave: true
            })
        } else if (e.target.id == "module") {
            this.setState({
                selectedModule: e.target.value,
                subModule: [],
                selectedSubModule: "",
                property: [],
                selectedProperty: "",
                to: [],
                cc: []
            })
            this.props.getPropertyRequest();
            this.props.getSubModuleRequest(e.target.value);
        } else if (e.target.id == "subModule") {
            this.setState({
                selectedSubModule: e.target.value,
                property: [],
                selectedProperty: "",
                to: [],
                cc: []
            })
            if (e.target.value != "") {
                this.props.getPropertyRequest(e.target.value);
            }
        } else if (e.target.id == "property") {
            this.setState({
                selectedProperty: e.target.value,
                to: [],
                cc: []
            })
            let data = {
                moduleName: this.state.selectedModule,
                subModuleName: this.state.selectedSubModule,
                configurationProperty: e.target.value
            }
            this.props.getToCcRequest(data);
        } else if (e.target.id == "too") {
            if (this.state.selectedProperty != "") {
                this.setState({
                    addto: e.target.value
                })
            }
        } else if (e.target.id == "ccc") {
            if (this.state.selectedProperty != "") {
                this.setState({
                    addcc: e.target.value
                })
            }
        }
    }

    tabPressed(e) {
        if (e.target.id == "too") {
            if (e.keyCode === 9 || e.keyCode === 13) {
                e.preventDefault();
                this.addToCc(e)
            }
        } else if (e.target.id == "ccc") {
            if (e.keyCode === 9 || e.keyCode === 13) {
                e.preventDefault();
                this.addToCc(e)
            }
        }
    }

    onSave(e) {
        e.preventDefault();
        if (this.state.tab == "general") {
            let data = {
                numberFormat: this.state.numberFormat,
                decimalPlaces: this.state.decimalPlaces,
                useSeperator: this.state.seperator,
                currency: this.state.currency,
                useSymbol: this.state.symbol,
                timeFormat: this.state.timeFormat,
                dateFormat: this.state.dateFormat,
            }
            this.props.createSettingRequest(data);
        } else {
            let data = {
                to: this.state.to,
                cc: this.state.cc,
                selectedModule: this.state.selectedModule,
                selectedSubModule: this.state.selectedSubModule,
                selectedProperty: this.state.selectedProperty,
            }
            if( this.state.needUpdate )
              this.props.updateToCcRequest(data)
            else  
              this.props.saveToCcRequest(data);
        }
    }

    removeTo(email) {
        let to = this.state.to;
        for (let i = 0; i < to.length; i++) {
            if (to[i] == email) {
                to.splice(i, 1);
            }
        }
        this.setState({
            to,
            notifyWrite: true
        })
    }

    removeCc(email) {
        let cc = this.state.cc;
        for (let i = 0; i < cc.length; i++) {
            if (cc[i] == email) {
                cc.splice(i, 1);
            }
        }
        this.setState({
            cc,
            notifyWrite: true
        })
    }

    addToCc(e) {
        e.preventDefault();
        if (e.target.id == 'too') {
            if (this.state.addto == "" || !this.state.addto.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )) {
                this.setState({
                    notifyWrite: false,
                })

            } else {
                let to = this.state.to;
                e.target.value = ""
                to.push(this.state.addto);
                this.setState({
                    to,
                    addto: "",
                    notifyWrite: true
                })
            }
        } else {
            if (this.state.addcc == "" || !this.state.addcc.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )) {

            } else {
                let cc = this.state.cc;
                cc.push(this.state.addcc);
                this.setState({
                    cc,
                    addcc: "",
                    notifyWrite: true
                })
            }
        }
    }

    onClear(e) {
        this.setState({
            selectedModule: "",
            subModule: [],
            selectedSubModule: "",
            property: [],
            selectedProperty: "",
            generalSave: false,
            to: [],
            cc: [],
            addto: "",
            addcc: "",
            otbSettingData: [],
            otbSettingKeys: [],
            checkedData: []
        })
    }

    // onClickActive(name) {
    //     let customData = this.state.customFestival;
    //     for (let i = 0; i < customData.length; i++) {
    //         customData[i].name == name ? customData[i].isChecked = customData[i].isChecked == "false" ? "true" : "false" : null
    //     }
    //     let selected = 0;
    //     for (let i = 0; i < customData.length; i++) {
    //         if (customData[i].isChecked == "true") {
    //             selected = selected + 1;
    //         }
    //     }
    //     this.setState({
    //         customFestival: customData,
    //         selected,
    //         saveFest: true
    //     })
    // }



    // onClearSelection(e) {
    //     e.preventDefault();
    //     let customData = this.state.customFestival;
    //     for (let i = 0; i < customData.length; i++) {
    //         if (customData[i].isChecked == "true") {
    //             customData[i].isChecked = "false";
    //         }
    //     }
    //     let selected = 0;
    //     for (let i = 0; i < customData.length; i++) {
    //         if (customData[i].isChecked == "true") {
    //             selected = selected + 1;
    //         }
    //     }
    //     this.setState({
    //         customFestival: customData,
    //         selected,
    //         saveFest: true
    //     })
    // }

    changeTabCS(tab) {
        this.setState({
            tab,
            shipTrackMenu: "asnFormat"
        })
        if (tab == 'notify') {
            this.setState({
                notifyWrite: false,
                module: [],
                selectedModule: "",
                subModule: [],
                selectedSubModule: "",
                emailStatus: "",
                property: [],
                selectedProperty: "",
                to: [],
                cc: [],
                addto: "",
                addcc: "",
            })
            this.props.getModuleRequest();
            this.props.getEmailStatusRequest();
        } else if (tab == 'general') {
            this.setState({
                generalSave: false,
                numberFormat: "",
                decimalPlaces: "",
                currency: "",
                timeFormat: "",
                dateFormat: "",
                currentNumberFormat: "",
                currentDecimalPlaces: "",
                currentCurrency: "",
                currentTimeFormat: "",
                currentDateFormat: "",
                symbol: false,
                seperator: false,
            })
            this.props.getSettingRequest('data');
        } else if (tab == 'otb') {
            this.props.otbGetDefaultRequest()
            this.props.getAllOtbRequest()
        } else if (tab == 'configuration') {
            // this.props.otbGetHlevelRequest();
            // this.props.getAllConfigRequest();
        } else if (tab == 'catdescudf') {
            this.setState({
                notifyWrite: false,
                module: [],
                selectedModule: "",
                subModule: [],
                selectedSubModule: "",
                emailStatus: "",
                property: [],
                selectedProperty: "",
                to: [],
                cc: [],
                addto: "",
                addcc: "",
            })
            this.props.getSettingRequest('data');
        }

    }
    proStartDateErr() {
        if (this.state.dateSelect == "restriction") {
            this.setState({
                startDateErr: this.state.startDate == "" ? true : false,
            })
        }
    }
    proEndDateErr() {
        if (this.state.dateSelect == "restriction") {
            this.setState({
                endDateErr: this.state.endDate == "" ? true : false
            })
        }
    }
    dateHandle = (e) => {
        if (e.target.id == "startDate") {
            this.setState({
                startDate: e.target.value,
                endDate: "",
                startDateErr: false
            }, () => {
                this.proStartDateErr()
            })
        } else if (e.target.id == "endDate") {
            this.setState({
                endDate: e.target.value,
                endDateErr: false
            }, () => {
                this.proEndDateErr()
            })
        }
    }
    onClearProcurement() {
        this.setState({
            startDate: "",
            endDate: "",
            dateSelect: "",
            startDateErr: false,
            endDateErr: false
        })
    }
    handleRestriction = (e) => {
        if (e.target.id == "noRestrict") {
            this.setState({
                startDate: "",
                endDate: "",
                startDateErr: false,
                endDateErr: false,
                dateSelect: "noRestriction"
            })
        } else if (e.target.id == "restriction") {
            this.setState({
                dateSelect: "restriction"
            })
        }
    }
    saveProcurement() {
        this.proStartDateErr();
        this.proEndDateErr();
        setTimeout(() => {
            const { startDateErr, endDateErr } = this.state

            if (!startDateErr && !endDateErr) {
                let data = {
                    startDate: this.state.dateSelect == "noRestrict" ? "" : this.state.startDate,
                    endDate: this.state.dateSelect == "noRestrict" ? "" : this.state.endDate
                }
                this.props.purchaseOrderDateRangeRequest(data)
                this.setState({
                    dateSelect: "",
                    startDate: "",
                    endDate: "",
                    startDateErr: false,
                    endDateErr: false
                })
            }
        }, 100);
    }
    render() {
        // var GeneralTab = JSON.parse(sessionStorage.getItem('General Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('General Setting')).crud)
        // var NotificationTab = JSON.parse(sessionStorage.getItem('Notification Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Notification Setting')).crud)
        // var ItemConfigurationTab = JSON.parse(sessionStorage.getItem('Item Configuration Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Configuration Setting')).crud)
        // var OTBTab = JSON.parse(sessionStorage.getItem('OTB Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('OTB Setting')).crud)
        // var ProcurementTab = JSON.parse(sessionStorage.getItem('Procurement Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Procurement Setting')).crud)
        var GeneralTab = 1, NotificationTab = 1, ItemConfigurationTab = 1, OTBTab = 1, ProcurementTab = 1

        return (
            <div>
                <div className="container-fluid nc-design pad-l65">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    {/* <div className="container_div m-top-75 " id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path d-table n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div> */}
                    <div className="container-fluid m-top-80 pad-0">
                        <div className="col-lg-12 pad-0 ">
                            <div className="subscription-tab procurement-setting-tab">
                                <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                                    <li className="nav-item active">
                                        <a className="nav-link st-btn" href="#generalSetting" role="tab" data-toggle="tab" onClick={() => this.changeTabCS('general')}>General</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link st-btn" href="#notification" role="tab" data-toggle="tab" onClick={() => this.changeTabCS('notify')}>Notification</a>
                                    </li>
                                    <li className="pst-button">
                                        <div className="pstb-inner">
                                            {this.state.tab == "notify" ? this.state.notifyWrite ? <span><button type="button" className="pst-save" onClick={(e) => this.onSave(e)} >Save</button><button className="gen-clear" type="button" onClick={(e) => this.onClear(e)}>Cancel</button></span> : <span><button type="button" className="pst-save btnDisabled" >Save</button><button className="gen-clear btnDisabled" type="button" disabled>Cancel</button></span> : null}
                                            {this.state.tab == "general" ? this.state.generalSave ? <span><button type="button" className="pst-save" onClick={(e) => this.onSave(e)} >Save</button><button className="gen-clear" type="button" onClick={(e) => this.onClear(e)}>Cancel</button></span> : <span><button type="button" className="pst-save btnDisabled" >Save</button><button className="gen-clear btnDisabled" type="button">Cancel</button></span> : null}
                                            {this.state.tab == "procurement" ? <span><button className={this.state.dateSelect == "" ? "clearBtnProcurement textDisable" : "clearBtnProcurement"} type="button" onClick={(e) => this.state.dateSelect == "" ? null : this.onClearProcurement(e)}>Clear</button><button type="button" className={this.state.dateSelect == "" ? "btnDisabled" : "pst-save"} onClick={(e) => this.state.dateSelect == "" ? null : this.saveProcurement(e)} >Save</button></span> : null}
                                            {this.state.tab == "asnSetting" ? <span><button type="button" className={this.state.asnSave ? "pst-save" : "btnDisabled"} onClick={(e) => this.state.asnSave ? this.saveAsn(e) : null} >Save</button></span> : null}
                                            {this.state.tab == "otb" ? this.state.otbSave ? <span><button className="gen-clear" type="button" onClick={(e) => this.onClearOtb(e)}>Clear</button>
                                                <button type="button" className="pst-save" onClick={(e) => this.onSubmit(e)} >Save</button>
                                                </span> : <span><button className="gen-clear btnDisabled" type="button" disabled>Clear</button>
                                                    <button type="button" className="btnDisabled" disabled >Save</button>
                                                </span> : null}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div className="tab-content">
                                <div className="tab-pane m-top-10 fade in active" id="generalSetting" role="tabpanel">
                                    {this.state.tab == "general" && <GeneralSetting {...this.props} {...this.state} handleChange={(e) => this.handleChnage(e)} />}
                                </div>
                                <div className="tab-pane fade" id="notification" role="tabpanel">
                                    {this.state.tab == "notify" && <NotificationSetting addToCc={(e) => this.addToCc(e)} removeCc={(e) => this.removeCc(e)} removeTo={(e) => this.removeTo(e)} onEmailStatus={(e) => this.onEmailStatus(e)} {...this.props} {...this.state} handleChange={(e) => this.handleChnage(e)} tabPressed={(e) => this.tabPressed(e)} />}
                                </div>
                            </div>
                        </div>
                        {/* <div className="change-setting-page">
                            <div className="col-md-12 col-sm-12 col-xs-12 itemUdfSet pad-0 changeSettingContain">
                                <div className="switchTabs p-lr-47">
                                    <ul className="list_style m0">
                                        {GeneralTab ? <li onClick={() => this.changeTabCS('general')} className={this.state.tab == "general" ? "activeTab" : ""}>
                                            <label>General</label>
                                        </li> : null}
                                        {NotificationTab ? <li onClick={() => this.changeTabCS('notify')} className={this.state.tab == "notify" ? "activeTab" : ""}>
                                            <label>Notification</label>
                                        </li> : null}
                                        {CatDescUdfTab ? <li onClick={() => this.changeTabCS('catdescudf')} className={this.state.tab == "catdescudf" ? "displayPointer activeTab" : "displayPointer"}>
                                            <label className="displayPointer">Cat/Desc & UDF</label>
                                        </li> : null}
                                        <li onClick={() => this.changeTabCS('festival')} className={this.state.tab == "festival" ? "displayPointer m-rgt-25 activeTab" : "m-rgt-25 displayPointer"}>
                                            <label className="displayPointer">Festival</label>
                                        </li>
                                        {ItemConfigurationTab ? <li onClick={() => this.changeTabCS('configuration')} className={this.state.tab == "configuration" ? "displayPointer m-rgt-25 activeTab" : "m-rgt-25 displayPointer"}>
                                            <label className="displayPointer">Item Configuration</label>
                                        </li> : null}
                                        {OTBTab ? <li onClick={() => this.changeTabCS('otb')} className={this.state.tab == "otb" ? "displayPointer m-rgt-25 activeTab" : "m-rgt-25 displayPointer"}>
                                            <label className="displayPointer">OTB</label>
                                        </li> : null}
                                        {ProcurementTab ? <li onClick={() => this.changeTabCS('procurement')} className={this.state.tab == "procurement" ? "displayPointer m-rgt-25 activeTab" : "m-rgt-25 displayPointer"}>
                                            <label className="displayPointer">Procurement</label>
                                        </li> : null}
                                        {ShipmentTrackingTab ? <li onClick={() => this.changeTabCS('asnSetting')} className={this.state.tab == "asnSetting" ? "displayPointer m-rgt-25 activeTab" : "m-rgt-25 displayPointer"}>
                                            <label className="displayPointer">Shipment Tracking</label>
                                        </li> : null}
                                    </ul>
                                </div>
                                {this.state.tab == "general" ? <GeneralSetting {...this.props} {...this.state} handleChange={(e) => this.handleChnage(e)} />
                                    : this.state.tab == "notify" ? <NotificationSetting addToCc={(e) => this.addToCc(e)} removeCc={(e) => this.removeCc(e)} removeTo={(e) => this.removeTo(e)} onEmailStatus={(e) => this.onEmailStatus(e)} {...this.props} {...this.state} handleChange={(e) => this.handleChnage(e)} tabPressed={(e) => this.tabPressed(e)} />
                                        : this.state.tab == "catdescudf" ? <CatDescUdfTab />
                                        : this.state.tab == "festival" ? <FestivalSetting {...this.props} {...this.state} />
                                            : this.state.tab == "configuration" ? <ConfigurationSetting  {...this.props} {...this.state} />
                                            : this.state.tab == "otb" ? <OtbSetting {...this.props} {...this.state} checkedData={this.state.checkedData} checkedDatafun={(e) => this.checkedDatafun(e)} otbSettingData={this.state.otbSettingData} otbSettingKeys={this.state.otbSettingKeys} />
                                                : this.state.tab == "procurement" ? <ProcurementSetting {...this.props} {...this.state} dateHandle={this.dateHandle} handleRestriction={this.handleRestriction} />
                                                    : null}
                            </div>
                            <div className="footerDivForm height4per m-top-30">
                                <ul className="list-inline m-lft-0 m-top-10">
                                    {this.state.tab == "notify" && this.state.selectedModule != "" && this.state.selectedSubModule != "" && this.state.selectedProperty != "" ? <li>
                                    </li> : null}
                                    {this.state.tab == "notify" ? this.state.notifyWrite ? <li><button className="clear_button_vendor" type="button" onClick={(e) => this.onClear(e)}>Clear</button><button type="button" className="save_button_vendor" onClick={(e) => this.onSave(e)} >Save</button></li> : <li><button className="clear_button_vendor textDisable" type="button" disabled>Clear</button><button type="button" className="btnDisabled save_button_vendor pointerNone" >Save</button></li> : null}
                                    {this.state.tab == "general" ? this.state.generalSave ? <li><button type="button" className="save_button_vendor" onClick={(e) => this.onSave(e)} >Save</button></li> : <li><button type="button" className="btnDisabled save_button_vendor pointerNone" >Save</button></li> : null}
                                    {this.state.tab == "procurement" ? <li><button className={this.state.dateSelect == "" ? "clearBtnProcurement textDisable" : "clearBtnProcurement"} type="button" onClick={(e) => this.state.dateSelect == "" ? null : this.onClearProcurement(e)}>Clear</button><button type="button" className={this.state.dateSelect == "" ? "save_button_vendor btnDisabled" : "save_button_vendor"} onClick={(e) => this.state.dateSelect == "" ? null : this.saveProcurement(e)} >Save</button></li> : null}
                                    : <li><button className="clear_button_vendor textDisable" type="button" disabled>Clear</button><button type="button" className="btnDisabled save_button_vendor pointerNone" >Save</button></li> : null}
                                    {this.state.tab == "asnSetting" ? <li><button type="button" className={this.state.asnSave ? "save_button_vendor" : "save_button_vendor btnDisabled"} onClick={(e) => this.state.asnSave ? this.saveAsn(e) : null} >Save</button></li> : null}
                                    {this.state.tab == "otb" ? this.state.otbSave ? <li><button className="clear_button_vendor" type="button" onClick={(e) => this.onClearOtb(e)}>Clear</button>
                                        <button type="button" className=" save_button_vendor" onClick={(e) => this.onSubmit(e)} >Save</button>
                                    </li> : <li><button className="clear_button_vendor btnDisabled" type="button" disabled>Clear</button>
                                            <button type="button" className=" save_button_vendor btnDisabled" disabled >Save</button>
                                        </li> : null}
                                </ul>
                            </div>
                        </div> */}
                    </div>
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
                <Footer />

            </div>
        );
    }
}

export function mapStateToProps(state) {
    return {
        changeSetting: state.changeSetting
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangeSetting);