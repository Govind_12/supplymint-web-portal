import React from 'react';
import SideBar from '../components/sidebar';
import openRack from '../assets/open-rack.svg';
import BraedCrumps from "../components/breadCrumps";
import Footer from '../components/footer';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import FilterLoader from '../components/loaders/filterLoader';
import RequestError from '../components/loaders/requestError';
import RequestSuccess from '../components/loaders/requestSuccess';
import RightSideBar from "../components/rightSideBar";
import DigiProcSetting from '../components/purchaseIndent/digiProcSetting';
import NewSideBar from '../components/newSidebar';
import ExcelUpload from '../components/purchaseIndent/excelUpload';
import ExcelUploadHistory from '../components/purchaseIndent/excelUploadHistory';

class DigiProc extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            expandDetails: [],
            approvedPO: [],
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
        };
    }
    componentWillReceiveProps(nextProps) {


    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    render() {
        const hash = window.location.hash.split("/")[2];
        return (
            <div>
                <div className="container-fluid nc-design pad-l60">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-t-75" id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                                {/* <div className="nmp-right">
                                    <button className="nmp-settings">Configuration Setting <img src={NewSettings} /></button>
                                    <button className="nmp-help">Need Help <img src={Help}></img></button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    {hash == "generalSetting" && (<DigiProcSetting {...this.props} rightSideBar={() => this.onRightSideBar()} />)}
                    {hash == "excelUpload" && (<ExcelUpload {...this.props} rightSideBar={() => this.onRightSideBar()} />)}
                    {hash == "excelUploadHistory" && (<ExcelUploadHistory {...this.props} rightSideBar={() => this.onRightSideBar()} />)}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess {...this.props} {...this.state} successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

                </div>
                <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        changeSetting: state.changeSetting,
        replenishment: state.replenishment
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DigiProc);