import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";

import RightSideBar from "../components/rightSideBar";

import openRack from "../assets/open-rack.svg"
import Footer from '../components/footer'
import BraedCrumps from "../components/breadCrumps";
// import ForecastOnDemand from"../components/demandPlanning/forecastOnDemand";
// import Weekly from "../components/demandPlanning/weekly";
// import Monthly from "../components/demandPlanning/monthly";
// import BudgetedSales from "../components/budgetedSales/budgetedSales";
// import BudgetedSalesHistory from "../components/budgetedSales/budgetedSalesHistory";
// import WeeklyHistory from "../components/demandPlanning/weeklyHistory";
// import MonthlyHistory from "../components/demandPlanning/monthlyHistory";
// import OpenToBuy from "../components/demandPlanning/openToBuy";
// import NoPlanExist from "../components/demandPlanning/noPlanExist";
// import ForecastHistory from "../components/demandPlanning/forecastHistory";
// import OpenToBuy fro../components/archiveFiles/openToBuyBuy";
import ForecastAutoConfig from "../components/demandPlanning/forecastAutoConfig";
class DemandPlanning extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      rightbar: false,
      openRightBar: false
    };
  }

  // componentWillMount() {
  //   if (sessionStorage.getItem('token') == null) {
  //     this.props.history.push('/');
  //   }
  // }

  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }
  render() {
    $("input").attr("autocomplete", "off");
    let hash = window.location.hash;

    return (
      <div className="container-fluid">
        <SideBar {...this.props} />
        <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />

        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
        <div className="container_div m-top-75 " id="">
          <div className="col-md-12 col-sm-12">
            <div className="menu_path">
              <ul className="list-inline width_100 pad20">
                <BraedCrumps {...this.props} />
              </ul>
            </div>
          </div>
        </div>
        {/* { hash == "#/demandPlanning/forecastOnDemand" ? (
        <ForecastOnDemand {...this.props} rightSideBar={() => this.onRightSideBar()} />
      ) : null} */}
        {/* {hash == "#/demandPlanning/weekly" ? (
          <Weekly {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {/* {hash == "#/demandPlanning/monthly" ? (
          <Monthly {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {/* {hash == "#/demandPlanning/budgetedSales" ? (
          <BudgetedSales {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null}
        {hash == "#/demandPlanning/budgetedSalesHistory" ? (
          <BudgetedSalesHistory {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {/* {hash == "#/demandPlanning/weeklyHistory" ? (
          <WeeklyHistory {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {/* {hash == "#/demandPlanning/monthlyHistory" ? (
          <MonthlyHistory {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {/* {hash == "#/demandPlanning/openToBuy" ? (
          <OpenToBuy {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {/* {hash == "#/demandPlanning/forecastHistory" ? (
          <ForecastHistory {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null} */}
        {hash == "#/demandPlanning/forecastAutoConfiguration" ? (
          <ForecastAutoConfig {...this.props} rightSideBar={() => this.onRightSideBar()} />
        ) : null}
        <Footer />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    demandPlanning: state.demandPlanning,
    inventoryManagement: state.inventoryManagement
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DemandPlanning);
