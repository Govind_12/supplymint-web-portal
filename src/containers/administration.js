import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import Organization from "../components/administration/organization";
import Roles from "../components/administration/roles";
import Users from "../components/administration/users";
import Site from "../components/administration/site";
// import SiteMapping from "../components/archiveFiles/siteMapping";
import Datasync from "../components/administration/datasync";
import Custom from "../components/administration/custom";
import RightSideBar from "../components/rightSideBar";
import openRack from "../assets/open-rack.svg"
import Footer from '../components/footer'
import BraedCrumps from "../components/breadCrumps";
// import PromotionalEvents from "../components/administration/changeSetting/promotionalEvents";
// import FestivalSetting from "../components/administration/changeSetting/festivalSetting";
import RolesMaster from "../components/administration/roleMaster"
import NewSideBar from "../components/newSidebar";
import ApiLogs from "../components/administration/apiLogs";
import SystemMails from "../components/administration/systemMails";
class Administration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rightbar: false,
      openRightBar: false,
      sideBar: true
    };
  }
  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  // componentWillMount() {
  //   if (sessionStorage.getItem('token') == null) {
  //     this.props.history.push('/');
  //   }
  // }

  render() {

    const hash = window.location.hash.split("/")[2];
    const bhash = window.location.hash.split("/")[3];
    return (
      <div>
        <div className="container-fluid nc-design pad-l60">
          <NewSideBar {...this.props} />
          <SideBar sideBar={this.state.sideBar} {...this.props} />
          {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
          {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
          <div className="container_div m-top-75 " id="">
            <div className="col-md-12 col-sm-12 border-btm">
              <div className="menu_path d-table n-menu-path">
                <ul className="list-inline width_100 pad20 nmp-inner">
                  <BraedCrumps {...this.props} />
                </ul>
              </div>
            </div>
          </div>
          {hash == "custom" ? (
            <Custom {...this.props} rightSideBar={() => this.onRightSideBar()} />
          ) : hash == "dataSync" ? (
            <Datasync {...this.props} />
          )
              // : hash == "siteMapping" ? (
              //   <SiteMapping {...this.props} rightSideBar={() => this.onRightSideBar()} />
              // ) 
              : hash == "roles" ? (
                <Roles {...this.props} rightSideBar={() => this.onRightSideBar()} />
              ) : hash == "users" ? (
                <Users {...this.props} rightSideBar={() => this.onRightSideBar()} />
              ) : hash == "site" ? (
                <Site {...this.props} rightSideBar={() => this.onRightSideBar()} />
              ) : hash == "apilogs" ? (
                <ApiLogs {...this.props} rightSideBar={() => this.onRightSideBar()} />
              ) : hash == "systemMails" ? (
                <SystemMails {...this.props} rightSideBar={() => this.onRightSideBar()} />
              ) : hash == "organisation" ? (
                <Organization {...this.props} rightSideBar={() => this.onRightSideBar()} />
              )
                      // : hash == "festivalSetting" ? (
                      //   <FestivalSetting {...this.props} rightSideBar={() => this.onRightSideBar()} />
                      // )
                      //  : hash == "promotionalEvents" ? (
                      //   <PromotionalEvents {...this.props} rightSideBar={() => this.onRightSideBar()} />
                      // )
                      : bhash == "addRoles" ? (
                        <RolesMaster {...this.props} rightSideBar={() => this.onRightSideBar()} />

                      ) : bhash == "manageRoles" ? (
                        <RolesMaster {...this.props} rightSideBar={() => this.onRightSideBar()} />
                      ) : bhash == "addManageRoles" ? (
                        <RolesMaster {...this.props} rightSideBar={() => this.onRightSideBar()} />
                      ) : null}
          </div>
        <Footer /> 
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    administration: state.administration,
    replenishment: state.replenishment,
    seasonPlanning: state.seasonPlanning
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Administration);
