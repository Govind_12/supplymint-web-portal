import React from "react";
import { connect } from "react-redux";
import axios from "axios";
import { bindActionCreators } from "redux";
import * as actions from "../redux/actions";
import SideBar from "../components/sidebar";
import RightSideBar from "../components/rightSideBar";
import { isUserAlreadyLoggedIn } from "../generic/"
import openRack from "../assets/open-rack.svg"
import BraedCrumps from "../components/breadCrumps";

import FilterLoader from "../components/loaders/filterLoader";
import RequestError from "../components/loaders/requestError";
import Footer from '../components/footer';
import { CONFIG } from "../config/index";
import SalesTrends from "../components/dashboard/salesTrend";
import TopDiv from "../components/dashboard/topDiv";
import TopStore from "../components/dashboard/topStorenItems";
import card from "../assets/card.svg";
import card1 from "../assets/card1.svg";
import card2 from "../assets/card2.svg";
import card3 from "../assets/card3.svg";
import ProductConsistency from "../components/dashboard/productConsistency";
import OldDashboard from "../components/dashboard/oldDashboard";
import { setToken } from '../helper';
import NewSideBar from "../components/newSidebar";


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      rightbar: false,
      openRightBar: false,
      alert: false,
      code: "",
      errorMessage: "",
      errorCode: "",

      successMessage: "",
      success: false,

      data: [
        { "title": "TOTAL SALES", "subTitle": "( LAST ONE YEAR )", "symbol": "₹", "symbolTool": "Rs", "image": card, value: "", "display": false, date: "" },
        { "title": "TOTAL UNITS SALE", "subTitle": "( LAST ONE YEAR )", "symbol": "", "symbolTool": "", "image": card1, value: "", "display": false, date: "" },
        { "title": "AVG SALES PER UNIT", "subTitle": "( LAST WEEK )", "symbol": "", "symbolTool": "Rs", "image": card2, value: "", "display": false, date: "" },
        { "title": <span>AVG SALES <br /> PER ASSORTMENT</span>, "subTitle": "( LAST ONE MONTH )", "symbol": "", "symbolTool": "Rs", "image": card3, value: "", "display": false, date: "" },
        { "title": "TOTAL SALES", "subTitle": "( CURRENT FINANCIAL YEAR )", "symbol": "₹", "symbolTool": "Rs", "image": card, value: "", "display": false, date: "" },
        { "title": "TOTAL SALES", "subTitle": "( PREVIOUS FINANCIAL YEAR )", "symbol": "₹", "symbolTool": "", "image": card1, value: "", "display": false, date: "" },
        { "title": "TOTAL SALES", "subTitle": "( CURRENT MONTH )", "symbol": "₹", "symbolTool": "Rs", "image": card2, value: "", "display": false, date: "" },
        { "title": "TOTAL SALES", "subTitle": "( CURRENT MONTH LAST YEAR)", "symbol": "₹", "symbolTool": "Rs", "image": card3, value: "", "display": false, date: "" }
      ],
      salesTrend: {
        unitSales: [],
        salesValue: []
      },
      salesTrendTime: "currentmonth",
      salesTrendToggle: "unitSales",
      storesArticles: [],
      storesArticlesTab: "topStores",
      filterProduct: "topThree",
      filterDay: "thirty",
      productTab: "fastMoving",
      slowFast: { "slowMoving": { "thirty": { "topFive": [], "topTen": [], "topThree": [] }, "fourty": { "topFive": [], "topTen": [], "topThree": [] }, "ninety": { "topFive": [], "topTen": [], "topThree": [] } }, "fastMoving": { "thirty": { "topFive": [], "topTen": [], "topThree": [] }, "fourty": { "topFive": [], "topTen": [], "topThree": [] }, "ninety": { "topFive": [], "topTen": [], "topThree": [] } } },
      dashShow: "old"
    };
  }
  updateTopDiv(data) {
    this.setState({
      data: data
    })
  }
  onError(e) {
    e.preventDefault();
    console.log("error")
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  componentDidMount() {
    sessionStorage.setItem('currentPage', "")
    sessionStorage.setItem('currentPageName', "")
    if (sessionStorage.getItem('login_redirect_pathParam') !== undefined && sessionStorage.getItem('login_redirect_pathParam') !== null) {
      var login_redirect = sessionStorage.getItem('login_redirect_pathParam')
      sessionStorage.removeItem('login_redirect_pathParam')
      sessionStorage.removeItem('redirect_url', '')
      this.props.history.push(login_redirect)
    }
    // else{this.props.history.push('/home')}
    // if (!isUserAlreadyLoggedIn()) {
    //   this.props.history.push('/')
    // }
    // if (sessionStorage.getItem("profile") == null) {
    //   if (!this.props.home.profileImage.isSuccess) {
    //     let data = {
    //       userName: sessionStorage.getItem("userName")
    //     }
    //     this.props.profileImageRequest(data);
    //   } else {
    //     let data = {
    //       userName: sessionStorage.getItem("userName")
    //     }
    //     this.props.profileImageRequest(data);
    //     this.setState({
    //       loader: false
    //     })
    //   }
    // }

    if (!this.props.home.dashboardTiles.isSuccess) {
      this.props.dashboardTilesRequest('data');
    } else if (this.props.home.dashboardTiles.data.resource != null) {
      this.setState({
        data: [
          { "title": "TOTAL SALES", "subTitle": "( LAST ONE YEAR )", "symbol": "₹", "symbolTool": "Rs", "image": card, value: this.props.home.dashboardTiles.data.resource.totalSales.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.totalSales, date: this.props.home.dashboardTiles.data.resource.totalSales.Duration },
          { "title": "TOTAL UNITS SALE", "subTitle": "( LAST ONE YEAR )", "symbol": "", "symbolTool": "", "image": card1, value: this.props.home.dashboardTiles.data.resource.totalUnits.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.totalUnits, date: this.props.home.dashboardTiles.data.resource.totalUnits.Duration },
          { "title": "AVG SALES PER UNIT", "subTitle": "( LAST WEEK )", "symbol": "", "symbolTool": "Rs", "image": card2, value: this.props.home.dashboardTiles.data.resource.avgSalesUnit.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.avgSalesUnit, date: this.props.home.dashboardTiles.data.resource.avgSalesUnit.Duration },
          { "title": <span>AVG SALES <br /> PER ASSORTMENT</span>, "subTitle": "( LAST ONE MONTH )", "symbol": "", "symbolTool": "Rs", "image": card3, value: this.props.home.dashboardTiles.data.resource.avgAssortmentUnit.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.avgAssortmentUnit, date: this.props.home.dashboardTiles.data.resource.avgAssortmentUnit.Duration },
          { "title": "TOTAL SALES", "subTitle": "( CURRENT FINANCIAL YEAR )", "symbol": "₹", "symbolTool": "Rs", "image": card, value: this.props.home.dashboardTiles.data.resource.totalSaleCurrentFinYear.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.totalSaleCurrentFinYear, date: this.props.home.dashboardTiles.data.resource.totalSaleCurrentFinYear.Duration },
          { "title": "TOTAL SALES", "subTitle": "( PREVIOUS FINANCIAL YEAR )", "symbol": "₹", "symbolTool": "", "image": card1, value: this.props.home.dashboardTiles.data.resource.totalSalePreviousFinYear.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.totalSalePreviousFinYear, date: this.props.home.dashboardTiles.data.resource.totalSalePreviousFinYear.Duration },
          { "title": "TOTAL SALES", "subTitle": "( CURRENT MONTH )", "symbol": "₹", "symbolTool": "Rs", "image": card2, value: this.props.home.dashboardTiles.data.resource.totalSaleCurrentMonth.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.totalSaleCurrentMonth, date: this.props.home.dashboardTiles.data.resource.totalSaleCurrentMonth.Duration },
          { "title": "TOTAL SALES", "subTitle": "( CURRENT MONTH LAST YEAR)", "symbol": "₹", "symbolTool": "Rs", "image": card3, value: this.props.home.dashboardTiles.data.resource.totalSaleCurrentMonthPreviousYear.value, "display": this.props.home.dashboardTiles.data.resource.dashboardKey.totalSaleCurrentMonthPreviousYear, date: this.props.home.dashboardTiles.data.resource.totalSaleCurrentMonthPreviousYear.Duration }

        ],
      })
    }
    if (this.props.home.slowFastArticle.isSuccess) {
      if (this.props.home.slowFastArticle.data.resource != null) {
        this.setState({
          storesArticles: this.props.home.storesArticles.data.resource,
        })
      }
    }
    if (this.props.home.storesArticles.isSuccess) {
      if (this.props.home.storesArticles.data.resource != null) {
        this.setState({
          slowFast: this.props.home.slowFastArticle.data.resource
        })
      }
    }
    if (this.props.home.salesTrendGraph.isSuccess) {
      if (this.props.home.salesTrendGraph.data.resource != null) {
        this.setState({
          salesTrend: this.props.home.salesTrendGraph.data.resource.salesTrend
        })
      }
    }
    //   if (sessionStorage.getItem('partnerEnterpriseCode') === null) {
    //     let headers = {
    //       'X-Auth-Token': sessionStorage.getItem('token'),
    //       'Content-Type': 'multipart/form-data'
    //     }
    // axios.get(`${CONFIG.BASE_URL}/core/enterprise/get/${sessionStorage.getItem('partnerEnterpriseId')}`, { headers: headers })
    //       .then(res => {
    //         this.setState({
    //           loader: false
    //         })
    //         sessionStorage.setItem('partnerEnterpriseCode', res.data.data.resource.code);
    //         sessionStorage.setItem('gstin', res.data.data.resource.gstin)
    //       }).catch((error) => {
    //         this.setState({
    //           loader: true
    //         })
    //       })

    //      `` 
    //   } else {
    //     this.setState({
    //       loader: false
    //     })
    //   }
    //WHY ARE WE CHECKING FOR partnerEnterpriseCode RATHER THAN loginTime ?
    if (sessionStorage.getItem('loginTime') == null) {
      let payload = {
        userName: sessionStorage.getItem('userName'),
        emailId: sessionStorage.getItem('email'),
        token: sessionStorage.getItem('token'),
        type: "LOGGEDIN",
        keepMeSignIn: "false",
      }
      this.props.userSessionCreateRequest(payload);
    }
  }

  onRightSideBar() {
    this.setState({
      openRightBar: true,
      rightbar: !this.state.rightbar
    });
  }

  changeStoArtTab(d) {
    this.setState({
      storesArticlesTab: d
    })
  }

  changeSalTreTab(d) {
    this.setState({
      salesTrendToggle: d
    })
  }

  changeTime(t) {
    this.setState({
      salesTrend: {
        unitSales: [],
        salesValue: []
      },
      salesTrendTime: t
    })
    this.props.salesTrendGraphRequest(t)
  }

  filterProducts(e) {
    this.setState({
      filterProduct: e
    })
  }

  filterDays(e) {
    this.setState({
      filterDay: e
    })
  }

  productTabs(e) {
    this.setState({
      productTab: e
    })
  }

  exportData(type) {
    let headers = {
      'X-Auth-Token': sessionStorage.getItem('token'),
      'Content-Type': 'application/json'
    }
    this.setState({
      loader: true
    })
    axios.get(`${CONFIG.BASE_URL}/download/module/${type}/DashBoard`, { headers: headers })
      .then(res => {
        this.setState({
          loader: false
        })
        window.open(`${res.data.data.resource}`);
      }).catch((error) => {
        this.setState({
          loader: false
        })
      });
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.home.userSession.isSuccess && sessionStorage.getItem('loginTime') == null) {
      sessionStorage.setItem('loginTime', nextProps.home.userSession.data.resource.lastTimeOut);

    }

    if (nextProps.home.dashboardTiles.isSuccess) {
      if (nextProps.home.dashboardTiles.data.resource == null) {
        this.setState({

          dashShow: 'old',
          loader: false
        })

      } else {
        this.setState({

          loader: false,
          data: [

            { "title": "TOTAL SALES", "subTitle": "( LAST ONE YEAR )", "symbol": "₹", "symbolTool": "Rs", "image": card, value: nextProps.home.dashboardTiles.data.resource.totalSales.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.totalSales, date: nextProps.home.dashboardTiles.data.resource.totalSales.Duration },
            { "title": "TOTAL UNITS SALE", "subTitle": "( LAST ONE YEAR )", "symbol": "", "symbolTool": "", "image": card1, value: nextProps.home.dashboardTiles.data.resource.totalUnits.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.totalUnits, date: nextProps.home.dashboardTiles.data.resource.totalUnits.Duration },
            { "title": "AVG SALES PER UNIT", "subTitle": "( LAST WEEK )", "symbol": "", "symbolTool": "Rs", "image": card2, value: nextProps.home.dashboardTiles.data.resource.avgSalesUnit.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.avgSalesUnit, date: nextProps.home.dashboardTiles.data.resource.avgSalesUnit.Duration },
            { "title": <span>AVG SALES <br /> PER ASSORTMENT</span>, "subTitle": "( LAST ONE MONTH )", "symbol": "", "symbolTool": "Rs", "image": card3, value: nextProps.home.dashboardTiles.data.resource.avgAssortmentUnit.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.avgAssortmentUnit, date: nextProps.home.dashboardTiles.data.resource.avgAssortmentUnit.Duration },
            { "title": "TOTAL SALES", "subTitle": "( CURRENT FINANCIAL YEAR )", "symbol": "₹", "symbolTool": "Rs", "image": card, value: nextProps.home.dashboardTiles.data.resource.totalSaleCurrentFinYear.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.totalSaleCurrentFinYear, date: nextProps.home.dashboardTiles.data.resource.totalSaleCurrentFinYear.Duration },
            { "title": "TOTAL SALES", "subTitle": "( PREVIOUS FINANCIAL YEAR )", "symbol": "₹", "symbolTool": "", "image": card1, value: nextProps.home.dashboardTiles.data.resource.totalSalePreviousFinYear.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.totalSalePreviousFinYear, date: nextProps.home.dashboardTiles.data.resource.totalSalePreviousFinYear.Duration },
            { "title": "TOTAL SALES", "subTitle": "( CURRENT MONTH )", "symbol": "₹", "symbolTool": "Rs", "image": card2, value: nextProps.home.dashboardTiles.data.resource.totalSaleCurrentMonth.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.totalSaleCurrentMonth, date: nextProps.home.dashboardTiles.data.resource.totalSaleCurrentMonth.Duration },
            { "title": "TOTAL SALES", "subTitle": "( CURRENT MONTH LAST YEAR)", "symbol": "₹", "symbolTool": "Rs", "image": card3, value: nextProps.home.dashboardTiles.data.resource.totalSaleCurrentMonthPreviousYear.value, "display": nextProps.home.dashboardTiles.data.resource.dashboardKey.totalSaleCurrentMonthPreviousYear, date: nextProps.home.dashboardTiles.data.resource.totalSaleCurrentMonthPreviousYear.Duration }


          ], dashShow: "new"

        })
        this.props.slowFastArticleRequest('data');
        this.props.salesTrendGraphRequest('currentmonth');
        this.props.storesArticlesRequest('data');
      }
      this.props.dashboardTilesClear();
    } else if (nextProps.home.dashboardTiles.isError) {
      this.setState({

        loader: false
      })
      this.props.dashboardTilesClear();
    }



    if (nextProps.home.storesArticles.isSuccess) {
      if (nextProps.home.storesArticles.data.resource == null) {
        this.setState({


          loader: false
        })

      } else {
        this.setState({


          loader: false,
          storesArticles: nextProps.home.storesArticles.data.resource
        })
      }
      this.props.storesArticlesClear();
    } else if (nextProps.home.storesArticles.isError) {
      this.setState({

        loader: false
      })
      this.props.storesArticlesClear();
    }

    if (nextProps.home.salesTrendGraph.isSuccess) {

      if (nextProps.home.salesTrendGraph.data.resource == null) {

        this.setState({

          loader: false
        })


      } else {

        this.setState({

          loader: false,
          salesTrend: nextProps.home.salesTrendGraph.data.resource.salesTrend
        })

      }
      this.props.salesTrendGraphClear();
    } else if (nextProps.home.salesTrendGraph.isError) {
      this.setState({

        loader: false
      })
      this.props.salesTrendGraphClear();
    }
    if (nextProps.home.slowFastArticle.isSuccess) {

      if (nextProps.home.slowFastArticle.data.resource == null) {

        this.setState({
          loader: false
        })
      } else {

        this.setState({

          loader: false, slowFast: nextProps.home.slowFastArticle.data.resource
        })
      }
      this.props.slowFastArticleClear();
    } else if (nextProps.home.slowFastArticle.isError) {
      this.setState({
        loader: false
      })
      this.props.slowFastArticleClear();
    }


    if (nextProps.home.switchEnt.isSuccess) {
      this.setState({

        loader: false,
      })




    } else if (nextProps.home.switchEnt.isError) {
      this.setState({

        errorMessage: nextProps.home.switchEnt.message.error == undefined ? undefined : nextProps.home.switchEnt.message.error.errorMessage,
        errorCode: nextProps.home.switchEnt.message.error == undefined ? undefined : nextProps.home.switchEnt.message.error.errorCode,
        code: nextProps.home.switchEnt.message.status,
        alert: true,

        loader: false
      })

    }


    if (nextProps.home.switchEnt.isLoading || nextProps.home.dashboardTiles.isLoading || nextProps.home.storesArticles.isLoading || nextProps.home.salesTrendGraph.isLoading || nextProps.home.slowFastArticle.isLoading) {
      this.setState({

        loader: true
      })
    }




    if (nextProps.home.switchEnt.isError) {
      this.props.switchEntClear()
    }
    if (nextProps.home.switchEnt.isSuccess) {

      setToken(nextProps.home.switchEnt.data.resource)

      this.props.switchEntClear();

    }

  }
  render() {
    console.log("in home")
    return (
      <div className="container-fluid pad-l65 nc-design heigh100vh">
        <NewSideBar {...this.props} />
        <SideBar {...this.props} />
        {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
        <div className="container_div m-top-75" id="">
          <div className="col-md-12 col-sm-12">
            <div className="menu_path">
              <ul className="list-inline width_100 pad20 m-top-bot7">
                <BraedCrumps />
              </ul>
            </div>
          </div>
        </div> */}
        {this.state.dashShow == "old" ? <OldDashboard /> :
          <div className="container-fluid">
            <div className="container_div">
              <TopDiv {...this.state} {...this.props} updateTopDiv={(e) => this.updateTopDiv(e)} clickRightSideBar={() => this.onRightSideBar()} />
              {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
              <div className="container-fluid">
                <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                  <TopStore {...this.props} {...this.state} exportData={(e) => this.exportData(e)} changeStoArtTab={(e) => this.changeStoArtTab(e)} />
                  <SalesTrends {...this.props} {...this.state} changeTime={(e) => this.changeTime(e)} changeSalTreTab={(e) => this.changeSalTreTab(e)} />
                </div>
                <div className="col-md-12 col-sm-12 col-xs-6 pad-0 pad-lft-0 m-top-35">
                  {/* <TopStock /> */}
                  <ProductConsistency {...this.props} {...this.state} productTabs={(e) => this.productTabs(e)} filterDays={(e) => this.filterDays(e)} filterProducts={(e) => this.filterProducts(e)} />
                </div>
                {/* <Dashboard /> */}
              </div>
            </div>
          </div>}
        <div className="col-md-12 col-sm-12 col-xs-12">
          <div className="page_footer bg-transparent">
            <p>&copy; 2021 TurningCloud Solutions </p>
            <span className="footer-links"><a href="http://support.supplymint.com/" target="_blank">Help & Support</a> | <a href="https://www.supplymint.com/#/privacy-policy" target="_blank">Privacy</a> | <a href="https://www.supplymint.com/#/terms-of-use" target="_blank">Terms & Conditions</a></span>
          </div>
        </div>
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.alert ? <RequestError {...this.props} {...this.state} code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}


      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    home: state.home
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
