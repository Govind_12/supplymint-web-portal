import React, { Suspense } from 'react'
import { createHashHistory } from 'history';
import FilterLoader from '../components/loaders/filterLoader';
import { HashRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
const Error = React.lazy(() => import('../containers/error'));
const Home = React.lazy(() => import('../containers/home'));
const Assortment = React.lazy(() => import('../components/assortment/assortment'));
const WarehouseManagement = React.lazy(() => import('../containers/warehouseManagement'));
const Replenishment = React.lazy(() => import('../containers/replenishment'));
const DemandPlanning = React.lazy(() => import('../containers/demandPlanning'));
const Vendor = React.lazy(() => import('../containers/vendor'));
const Transporter = React.lazy(() => import('../containers/transporter'));
const Customer = React.lazy(() => import('../containers/customer'));
const GenericItemMaster = React.lazy(() => import('../containers/genericItemMaster'));
const SalesAgent = React.lazy(() => import('../containers/salesAgent'));
const Reporting = React.lazy(() => import('../containers/reporting'));
const SignIn = React.lazy(() => import('../containers/signIn'));
const EmailActivation = React.lazy(() => import('../containers/emailActivation'));
const EmailVerification = React.lazy(() => import('../containers/emailVerification'));
const ResettingPassword = React.lazy(() => import('../containers/resettingPassword'));
const VendorSignUpNew = React.lazy(() => import('../containers/vendorSignUpNew'));
const VendorLogin = React.lazy(() => import('../containers/vendorLogin'));
const ForgotPass = React.lazy(() => import('../containers/forgotPassword'));
const ForgotUser = React.lazy(() => import('../containers/forgotUser'));
const ResetPassword = React.lazy(() => import('../containers/resetPassword'));
const PageNotFound = React.lazy(() => import('../components/errorPage/pageNotFound'));
const InternetError = React.lazy(() => import('../components/errorPage/internetError'));
const InternalServerError = React.lazy(() => import('../components/errorPage/internalServerError'));
const UnauthorisedAccess = React.lazy(() => import('../components/errorPage/unauthorisedAccess'));
const Summary = React.lazy(() => import('../components/replenishment/summary'));
const BudgetedSales = React.lazy(() => import("../components/archiveFiles/budgetedSales"));
const BudgetedSalesHistory = React.lazy(() => import('../components/archiveFiles/budgetedSales/budgetedSalesHistory'));
const UdfMapping = React.lazy(() => import('../components/purchaseOrder/udfMapping'));
const DeptSizeMapping = React.lazy(() => import('../components/purchaseOrder/DepartmentSizeMapping'));
const ItemUdf = React.lazy(() => import('../components/purchaseOrder/itemUdf'));
const ViewActivity = React.lazy(() => import('../components/assortment/viewActivity'));
const NoPlanExist = React.lazy(() => import('../components/demandPlanning/otb/noPlanExist'));
const Analytics = React.lazy(() => import('../containers/analytics'));
// import FestivalSetting from '../components/changeSetting/festivalSetting';
// import FestivalAdmin from '../components/changeSetting/festivalSetting';
import MasterNysaaSTR from '../components/customForNysaa/masterNysaaSTR';
import DataSync from '../components/dataSync/dataSync';
import { VendorSignUp } from '../components/vendorPortal/vedorSignUp';
const CreateConfiguration = React.lazy(() => import('../components/replenishment/createConfiguration'));
const ManageRuleEngine = React.lazy(() => import('../components/replenishment/manageRuleEngine'));


// {commented before
// import  InventoryAutoConfig  from '../components/replenishment/inventoryAutoConfig';
// import EnterpriseOrders from '../containers/vendorOrderEnterprise';
// import MasterNysaaSTR from '../components/customForNysaa/masterNysaaSTR';
// import  InventoryAutoConfig  from '../components/replenishment/inventoryAutoConfig';
// import { VendorLogin } from '../components/vendorPortal/VendorLogin';
//}
import createStore from '../store/createStore';
const Logistics = React.lazy(() => import('../containers/logistics'))
const Administration = React.lazy(() => import('../containers/administration'))
const InventoryPlanning = React.lazy(() => import('../containers/inventoryPlanning'))
const PiInvoicePdfModal = React.lazy(() => import('../components/piOrPoHistory/piInvoicePdfModal'))
const ProfileModal = React.lazy(() => import('../components/profile/profileModal'))
const ChangeSetting = React.lazy(() => import('../containers/changeSetting'))
const AddVendor = React.lazy(() => import('../components/vendor/Managevendor/addVendor'))
const AddCustomer = React.lazy(() => import('../components/customer/Managecustomer/addCustomer'))
const AddGenericItemMaster = React.lazy(() => import('../components/genericItemMaster/Manageitem/addItem'))
const AddSalesAgent = React.lazy(() => import('../components/salesAgent/ManagesalesAgent/addSalesAgent'))
const AddTransporter = React.lazy(() => import('../components/transporter/Managetransporter/addTransporter'))
const MasterSketchersRetail = React.lazy(() => import('../components/sketchers/masterSketchesRetail'))
const MasterSketchersMBO = React.lazy(() => import('../components/sketchers/masterSkechersMBO'))
const GlobalDataSync = React.lazy(() => import('../components/sketchers/globalDataSync'))
const PiHistory = React.lazy(() => import('../containers/purchaseIndent'))
const PoHistory = React.lazy(() => import('../containers/purchaseIndent'))
const AddOrganization = React.lazy(() => import('../components/administration/organization/addOrganization'))
const AddRoles = React.lazy(() => import('../components/administration/roles/addRoles'))
const AddUser = React.lazy(() => import('../components/administration/user/addUser'))
const SiteCreate = React.lazy(() => import('../components/administration/site/siteCreate'))
// const AddSiteMapping = React.lazy(() => import('../components/archiveFiles/addSiteMapping'))
const Item = React.lazy(() => import('../containers/item'))
const Profile = React.lazy(() => import('../containers/profile'))
const purchaseIndent = React.lazy(() => import('../containers/purchaseIndent'))
const PurchaseOrder = React.lazy(() => import('../containers/purchaseIndent'))
const OnBoarding = React.lazy(() => import('../containers/onBoarding'))
const DragAndDrop = React.lazy(() => import('../components/dragAndDropModal/dragAndDrop'))
const DataMappingExcel = React.lazy(() => import('../components/dragAndDropModal/dataMappingExcel'))
const SuccessStatus = React.lazy(() => import('../components/dragAndDropModal/successStatus'))
const ErrorStatus = React.lazy(() => import('../components/dragAndDropModal/errorStatus'))
const MasterEvents = React.lazy(() => import('../components/sketchers/masterEvents'))
const EnterpriseOrders = React.lazy(() => import('../containers/enterpriseOrders'))
const ShipmentVendor = React.lazy(() => import('../containers/shipmentVendor'))
const shipmentEnterprise = React.lazy(() => import('../containers/shipmentEnterprise'))
const VendorOrders = React.lazy(() => import('../containers/vendorOrders'))
const VendorLogistics = React.lazy(() => import('../containers/vendorLogistics'))

const fmcgMaster = React.lazy(() => import('../components/fmcgCustom/fmcgMaster'))
const Subscription = React.lazy(() => import('../containers/subscription'))
const PortalDashboard = React.lazy(() => import('../containers/portalDashboard'))
const VendorAdmin = React.lazy(() => import('../containers/vendorAdmin'))
const TransactionSuccessfull = React.lazy(() => import('../components/mergeUser/transactionSucessfull'))
const TransactionFailed = React.lazy(() => import('../components/mergeUser/transationFailed'))

const ProcurementDashboard = React.lazy(() => import('../components/pDashboard/procurementDashboard'))
// const PlannerWorkQueue = React.lazy(() => import('../components/analytics/plannerWorkQueue'))
// const PlannerWorkQueue2 = React.lazy(() => import('../components/analytics/plannerWorkQueue2'))
// const PlannerWorkSheet = React.lazy(() => import('../components/analytics/plannerWorkSheet'))
// const PlannerWorkSheet2 = React.lazy(() => import('../components/analytics/plannerWorkSheet2'))
const EnterpriseLogistics = React.lazy(() => import('../containers/enterpriseLogistics'));

const ArsDashboard = React.lazy(() => import('../components/dashboard/arsDashboard'));
const InventoryClassification = React.lazy(() => import('../components/analytics/inventoryClassification'));

// const AssortmentPlanning = React.lazy(() => import('../components/inventoryPlanning/assortmentPlanning'));
const ManageAdhocRequest = React.lazy(() => import('../components/inventoryPlanning/manageAdhocRequest'));
const ProductCatalogue = React.lazy(() => import('../containers/productCatalogue'));
const QualityCheck = React.lazy(() => import('../containers/qualityCheck'));
const VendorPortal = React.lazy(() => import('../containers/vendorPortal'));
const DigiProc = React.lazy(() => import('../containers/digiProc'));
const VendorKycContainer = React.lazy(() => import('../containers/vendorKyc'));
const InvoiceManagement = React.lazy(() => import('../containers/invoiceManagement'))
const CustomerAdmin = React.lazy(() => import('../containers/customerAdmin'))

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: true
    }
  }

  shouldComponentUpdate() {
    return false
  }
  render() {

    $(document).ready(function () {

      $('body').on('click', '.right_sidemenu', function () {
        $('.right_sidebar').addClass('activeRightSide')
        $("#custom").hide();
      })
      $(document).click(function (e) {
        var right_side = $(".right_sidebar, .right_sidemenu");
        if (!right_side.is(e.target) && right_side.has(e.target).length == 0) {
          right_side.removeClass('activeRightSide');
          $("#custom").hide();

        }
      });
      $('body').on('click', '.right_sidemenu1', function () {
        $('.right_sidebar').removeClass('activeRightSide')
        $("#custom").hide();
      })

      $(document).keydown(function (e) {
        if (e.keyCode === 27) {
          var sidebar = $(".sidemenu, #toggle_sidemenu");
          var humburger = $(".humburgerIcon, #toggle_sidemenu");
          var drop = $(".dropdownSketchFilter, .runOndemandDrop");
          $('.right_sidebar, .right_sidemenu').removeClass('activeRightSide');
          $(".notification_dropdown, .bell_div").removeClass('displayBlock');
          sidebar.removeClass('active');
          humburger.removeClass('openSideBar');
          drop.removeClass('displayBlock');
          $("#custom").hide();
        }
      })

      // NOTIFICATION
      $('body').on('click', '.bell_div', function () {
        $('.notification_dropdown').toggleClass('displayBlock');
      })
      $(document).click(function (e) {
        var bellIcon = $(".notification_dropdown, .bell_div");
        if (!bellIcon.is(e.target) && bellIcon.has(e.target).length == 0) {
          bellIcon.removeClass('displayBlock');
          $("#custom").hide();
        }
      })


      // skechers dropdown    

      $('body').on('click', '.runOndemandDrop', function () {
        $('.dropdownSketchFilter').toggleClass('displayBlock');
      })
      $(document).click(function (e) {
        var drop = $(".dropdownSketchFilter, .runOndemandDrop");
        if (!drop.is(e.target) && drop.has(e.target).length == 0) {
          drop.removeClass('displayBlock');
        }
      })
      $('body').on('click', '.rondZone', function () {
        $('.zoneli').toggleClass('display_block');
      })
      $(document).click(function (e) {
        var drop = $(".zoneli,.rondZone");
        if (!drop.is(e.target) && drop.has(e.target).length == 0) {

          drop.removeClass('display_block');
        }
      })

      $('body').on('click', '.rondGrade', function () {
        $('.gradeli').toggleClass('display_block');
      })
      $(document).click(function (e) {
        var drop = $(".gradeli,.rondGrade");
        if (!drop.is(e.target) && drop.has(e.target).length == 0) {

          drop.removeClass('display_block');
        }
      })



      // SIDEBAR

      $('body').on('click', '#toggle_sidemenu', function () {

        $('.humburgerIcon').toggleClass('openSideBar');


        $('.sidemenu').toggleClass('active');



      })
      $(document).click(function (e) {


        var sidebar = $(".sidemenu, #toggle_sidemenu");
        var humburger = $(".humburgerIcon, #toggle_sidemenu");
        var menuSubItem = $(".menuSubItem");
        if (menuSubItem.is(e.target)) {
          sidebar.removeClass('active');
          humburger.removeClass('openSideBar');
        }
        $(".notification_dropdown").hide();
        // if("#cus")
        // $("#tttt").trigger("click");
        if ($("#custom").hasClass("in")) { $("#leftDrop").trigger("click") }
        var sidebar = $(".sidemenu, #toggle_sidemenu");
        var humburger = $(".humburgerIcon, #toggle_sidemenu");
        if (!sidebar.is(e.target) && sidebar.has(e.target).length == 0 && (!humburger.is(e.target) && humburger.has(e.target).length == 0)) {
          sidebar.removeClass('active');
          humburger.removeClass('openSideBar');
        }
      });
    });


    const store = createStore();
    return (
      <Provider store={store} >
        <Suspense fallback={<FilterLoader />}>
          <HashRouter>
            <Switch >
              <Route exact path="/" component={SignIn} />
              <Route exact path="/home" component={Home} />
              <Route exact path="/vendorLogin" component={VendorLogin} />
              <Route exact path="/VendorSignUp" component={VendorSignUpNew} />
              <Route exact path="/inventoryPlanning" component={InventoryPlanning} />
              <Route exact path="/warehouseManagement" component={WarehouseManagement} />
              <Route exact path="/administration/organisation" component={Administration} />
              {/* <Route exact path="/administration/organisation/emptyBox" component={Administration} /> */}
              <Route exact path="/administration/organisation/addOrganisation" component={AddOrganization} />
              {/* <Route exact path="/administration/organisation/manageOrganisation" component={Administration} /> */}
              <Route exact path="/administration/roles/" component={Administration} />
              <Route exact path="/administration/roles/addRoles" component={AddRoles} />
              {/* <Route exact path="/administration/roles/viewRoles" component={Administration} /> */}
              <Route exact path="/administration/users" component={Administration} />
              <Route exact path="/administration/apilogs" component={Administration} />
              <Route exact path="/administration/systemMails" component={Administration} />
              <Route exact path="/administration/users/addUser" component={AddUser} />
              {/* <Route exact path="/administration/user/userManage" component={Administration} /> */}
              <Route exact path="/administration/site" component={Administration} />
              <Route exact path="/administration/site/siteCreate" component={SiteCreate} />
              {/* <Route exact path="/administration/site/siteView" component={Administration} /> */}
              {/* <Route exact path="/administration/siteMapping" component={Administration} /> */}
              {/* <Route exact path="/administration/siteMapping/addSiteMapping" component={AddSiteMapping} /> */}
              {/* <Route exact path="/administration/siteMapping/viewSiteMapping" component={Administration} /> */}
              <Route exact path="/administration/dataSync" component={Administration} />

              {/* <Route exact path="/administration/festivalAdmin" component={Administration} /> */}

              <Route exact path="/reporting" component={Reporting} />
              <Route exact path="/logistics" component={Logistics} />
              {/* <Route exact path="/vendor/manageVendors/viewVendor" component={Vendor} /> */}
              <Route exact path="/vendor/manageVendors" component={Vendor} />
              <Route exact path="/vendor/manageVendors/addVendor" component={AddVendor} />
              {/* <Route exact path="/vendor/contracts/emptyBox" component={Vendor} /> */}
              {/* <Route exact path="/vendor/contracts/addVendor" component={Vendor} /> */}
              {/* <Route exact path="/vendor/contracts/viewVendor" component={Vendor} /> */}
              <Route exact path="/mdm/manageTransaporters" component={Transporter} />
              <Route exact path="/mdm/manageTransporters/addTransporter" component={AddTransporter} />
              <Route exact path="/mdm/manageCustomers" component={Customer} />
              <Route exact path="/mdm/manageCustomers/addCustomer" component={AddCustomer} />
              <Route exact path="/mdm/manageItems" component={GenericItemMaster} />
              <Route exact path="/mdm/manageItems/addItem" component={AddGenericItemMaster} />
              <Route exact path="/mdm/manageSalesAgents" component={SalesAgent} />
              <Route exact path="/mdm/manageSalesAgents/addSalesAgent" component={AddSalesAgent} />
              <Route exact path="/demandPlanning" component={DemandPlanning} />
              <Route exact path="/forgotPassword" component={ForgotPass} />
              <Route exact path="/forgotUser" component={ForgotUser} />
              <Route exact path="/resetPassword" component={ResetPassword} />
              <Route exact path="/resettingPassword/:id" component={ResettingPassword} />
              <Route exact path="/emailActivation/:id" component={EmailActivation} />
              <Route exact path="/emailVerification/:id" component={EmailVerification} />
              <Route exact path="/item/itemMapping" component={Item} />
              <Route exact path="/item/itemConfigurationTable" component={Item} />
              <Route exact path="/item/itemConfiguration" component={Item} />
              <Route exact path="/item/itemRoute" component={Item} />
              <Route exact path="/profile" component={Profile} />
              <Route exact path="/profile/edit" component={Profile} />
              <Route exact path="/profile/changePassword" component={Profile} />
              <Route exact path="/purchase/purchaseIndent" component={purchaseIndent} />
              <Route exact path="/purchase/purchaseOrder" component={PurchaseOrder} />
              <Route exact path="/purchase/purchaseIndents" component={purchaseIndent} />
              <Route exact path="/purchase/purchaseOrders" component={PurchaseOrder} />
              <Route exact path="/onBoarding" component={OnBoarding} />
              <Route exact path="/dragAndDrop" component={DragAndDrop} />
              <Route exact path="/dataMappingExcel" component={DataMappingExcel} />
              <Route exact path="/successStatus" component={SuccessStatus} />
              <Route exact path="/errorStatus" component={ErrorStatus} />
              <Route exact path="/purchase/purchaseIndentHistory" component={purchaseIndent} />
              <Route exact path="/purchase/purchaseOrderHistory" component={purchaseIndent} />
              <Route exact path="/pageNotFound" component={PageNotFound} />
              <Route exact path="/internetError" component={InternetError} />
              <Route exact path="/internalServerError" component={InternalServerError} />
              <Route exact path="/unauthorisedAccess" component={UnauthorisedAccess} />
              {/* <Route exact path="/inventoryPlanning/runOnDemand" component={Replenishment} /> */}
              <Route exact path="/inventoryPlanning/summary" component={Replenishment} />
              {/* <Route exact path="/inventoryPlanning/autoConfiguration" component={Replenishment} /> */}
              <Route exact path="/inventoryPlanning/history" component={Replenishment} />
              {/* <Route exact path="/inventoryPlanning/assortment" component={Replenishment} /> */}
              <Route exact path="/administration/custom/masterSkechersRetail" component={MasterSketchersRetail} />
              <Route exact path="/administration/custom/masterSkechersMBO" component={MasterSketchersMBO} />
              <Route exact path="/administration/custom/globalDataSync" component={GlobalDataSync} />
              {/* <Route exact path="/demandPlanning/forecastOnDemand" component={DemandPlanning} /> */}
              {/* <Route exact path="/demandPlanning/weekly" component={DemandPlanning} />
              <Route exact path="/demandPlanning/weeklyHistory" component={DemandPlanning} /> */}
              {/* <Route exact path="/demandPlanning/monthlyHistory" component={DemandPlanning} />
              <Route exact path="/demandPlanning/monthly" component={DemandPlanning} /> */}
              <Route exact path="/inventoryPlanning/manageAd-Hoc" component={Replenishment} />
              <Route exact path="/inventoryPlanning/Ad-Hoc" component={Replenishment} />
              {/* <Route exact path="/demandPlanning/budgetedSales" component={DemandPlanning} />
              <Route exact path="/demandPlanning/budgetedSalesHistory" component={DemandPlanning} /> */}
              <Route exact path="/piOrPoHistory/piInvoicePdfModal" component={PiInvoicePdfModal} />
              <Route exact path="/profile/profileModal" component={ProfileModal} />
              <Route exact path="/changeSetting" component={ChangeSetting} />
              <Route exact path="/purchase/udfSetting" component={purchaseIndent} />
              <Route exact path="/purchase/itemUdfSetting" component={purchaseIndent} />
              <Route exact path="/purchase/departmentSizeMapping" component={purchaseIndent} />
              <Route exact path="/purchase/udfMapping" component={purchaseIndent} />
              <Route exact path="/purchase/setUdfMapping" component={purchaseIndent} />
              <Route exact path="/purchase/catDescUdfSetting" component={purchaseIndent} />
              {/* <Route exact path="/demandPlanning/openToBuy" component={DemandPlanning} /> */}

              {/* <Route exact path="/demandPlanning/noPlanExist" component={DemandPlanning} /> */}
              {/* <Route exact path="/purchase/itemUdfMapping" component={purchaseIndent} /> */}
              {/* <Route exact path="/demandPlanning/forecastHistory" component={DemandPlanning} /> */}
              {/* <Route exact path="/purchase/udfMapping" component={purchaseIndent} /> */}
              <Route exact path="/purchase/itemUdfMapping" component={purchaseIndent} />
              <Route exact path="/inventoryPlanning/viewActivity" component={Replenishment} />
              <Route exact path="/demandPlanning/otb/noPlanExist" component={NoPlanExist} />
              <Route exact path="/analytics/storeProfile" component={Analytics} />
              <Route exact path="/analytics/chooseStoreProfile" component={Analytics} />
              <Route exact path="/analytics" component={Analytics} />
              <Route exact path="/demandPlanning/forecastAutoConfiguration" component={DemandPlanning} />
              {/* <Route exact path="/administration/festivalSetting" component={Administration} /> */}
              {/* <Route exact path="/administration/promotionalEvents" component={Administration} /> */}
              <Route exact path="/administration/rolesMaster/addRoles" component={Administration} />
              <Route exact path="/administration/rolesMaster/manageRoles" component={Administration} />
              {/*<Route exact path="/administration/rolesMaster/addManageRoles" component={Administration} />*/}
              <Route exact path="/administration/custom/uploadCustomMaster" component={fmcgMaster} />
              <Route exact path="/replenishment/inventoryAutoConfig" component={Replenishment} />
              <Route exact path="/dataSync" component={DataSync} />
              {/*<Route exact path="/administration/custom/masterNysaaSTR" component={MasterNysaaSTR} /> */}
              {/* <Route exact path="/inventoryPlanning/inventoryAutoConfig" component={Replenishment} /> */}
              <Route exact path="/vendorLogin" component={VendorLogin} />
              <Route exact path="/vendorSignUp" component={VendorSignUp} />
              <Route exact path="/inventoryPlanning/configuration" component={Replenishment} />
              <Route exact path="/inventoryPlanning/manageRuleEngine" component={Replenishment} />
              <Route exact path="/analytics/sellThru" component={Analytics} />
              <Route exact path="/purchaseOrder/confirmedPo" component={EnterpriseOrders} />


              <Route exact path="/purchase/purchaseOrderWithUpload" component={PurchaseOrder} />
              <Route exact path="/purchase/uploadHistory" component={PurchaseOrder} />
              <Route exact path="/administration/custom/masterEvents" component={MasterEvents} />

              <Route exact path="/purchase/purchaseOrderWithUpload" component={PurchaseOrder} />
              <Route exact path="/purchase/uploadHistory" component={PurchaseOrder} />

              {/* __________________________________ORDERS ENTERPRISE________________________________ */}
              <Route exact path="/enterprise/purchaseOrder/cancelledOrders" component={EnterpriseOrders} />
              <Route exact path="/enterprise/purchaseOrder/pendingOrders" component={EnterpriseOrders} />
              <Route exact path="/enterprise/purchaseOrder/processedOrders" component={EnterpriseOrders} />
              <Route exact path="/enterprise/purchaseOrder/closedPo" component={EnterpriseOrders} />

              <Route exact path="/enterprise/qualityCheck/pendingQc" component={QualityCheck} />
              <Route exact path="/enterprise/qualityCheck/cancelledInspection" component={QualityCheck} />
              <Route exact path="/vendor/purchaseOrder/cancelledOrders" component={VendorOrders} />
              <Route exact path="/vendor/purchaseOrder/pendingOrders" component={VendorOrders} />
              <Route exact path="/vendor/logistics/lrProcessing" component={VendorLogistics} />
              <Route exact path="/vendor/purchaseOrder/processedOrders" component={VendorOrders} />
              <Route exact path="/vendor/logistics/goodsIntransit" component={VendorLogistics} />
              <Route exact path="/vendor/logistics/goodsDelivered" component={VendorLogistics} />
              <Route exact path="/vendor/logistics/grcStatus" component={VendorLogistics} />


              <Route exact path="/enterprise/logistics/lrProcessing" component={EnterpriseLogistics} />
              <Route exact path="/enterprise/logistics/goodsIntransit" component={EnterpriseLogistics} />
              <Route exact path="/enterprise/logistics/goodsDelivered" component={EnterpriseLogistics} />
              <Route exact path="/enterprise/logistics/grcStatus" component={EnterpriseLogistics} />



              {/* ___________________________SHIPMENT ENTERPRISE_______________________ */}

              <Route exact path="/enterprise/shipment/asnUnderApproval" component={shipmentEnterprise} />
              <Route exact path="/enterprise/shipment/approvedAsn" component={shipmentEnterprise} />
              <Route exact path="/enterprise/shipment/cancelledAsn" component={shipmentEnterprise} />

              {/* _______________________________SHIPMENT VENDOR _____________________________ */}

              <Route exact path="/vendor/shipment/asnUnderApproval" component={ShipmentVendor} />
              <Route exact path="/vendor/shipment/approvedAsn" component={ShipmentVendor} />
              <Route exact path="/vendor/shipment/cancelledAsn" component={ShipmentVendor} />



              {/* _____________________________________Subscription ______________________________ */}
              <Route exact path="/subscription/pricingPlans" component={Subscription} />
              <Route exact path="/subscription/invoiceDetails" component={Subscription} />
              <Route exact path="/subscription/invoiceEdit" component={Subscription} />
              <Route exact path="/subscription/multienterprisesAaGridView" component={Subscription} />
              <Route exact path="/subscription/multienterprisesAaListView" component={Subscription} />
              <Route exact path="/subscription/subscriptionRenew" component={Subscription} />
              <Route exact path="/subscription/transactionFailed" component={Subscription} />
              <Route exact path="/subscription/transactionStatus" component={Subscription} />
              <Route exact path="/subscription/currentplanDetails" component={Subscription} />



              {/* ____________________________________Dashboard___________________________ */}
              <Route exact path="/vendor/vendorDashboard" component={PortalDashboard} />
              <Route exact path="/enterprise/enterpriseDashboard" component={PortalDashboard} />
              <Route exact path="/vendorAdmin" component={VendorAdmin} />

              {/* ________________________________Manage Subscription______________________ */}
              {/* ________________________________Procurement Dashboard______________________________ */}
              <Route exact path="/purchase/dashboard" component={purchaseIndent} />

              {/* ________________________________ARS Planner Work Queue______________________________ */}
              <Route exact path="/analytics/plannerWorkQueue" component={Analytics} />
              <Route exact path="/analytics/plannerWorkQueue2" component={Analytics} />
              <Route exact path="/analytics/plannerWorkSheet" component={Analytics} />
              <Route exact path="/analytics/plannerWorkSheet2" component={Analytics} />
              <Route exact path="/analytics/inventory-classfication" component={Analytics} />

              {/* ________________________________ARS DASHBOARD______________________________ */}
              <Route exact path="/ars-dashboard" component={InventoryPlanning} />

              {/* ________________________________ASSORTMENT______________________________ */}
              <Route exact path="/inventoryPlanning/assortment" component={InventoryPlanning} />

              {/* ________________________________AD-HOC REQUEST______________________________ */}
              <Route exact path="/inventoryPlanning/manageAdhocRequest" component={InventoryPlanning} />

              {/* ________________________________PLANNING SETTING______________________________ */}
              <Route exact path="/inventoryPlanning/planningParameter" component={InventoryPlanning} />
              <Route exact path="/inventoryPlanning/mrp-range" component={InventoryPlanning} />
              <Route exact path="/inventoryPlanning/siteMapping" component={InventoryPlanning} />
              <Route exact path="/inventoryPlanning/event-mapping" component={InventoryPlanning} />
              <Route exact path="/inventoryPlanning/season-mapping" component={InventoryPlanning} />
              <Route exaxt path="/inventoryPlanning/customDataUpload" component={InventoryPlanning} />

              {/* __________________________________Merge User_____________________________ */}
              <Route exact path="/transationSucessfull" component={TransactionSuccessfull} />
              <Route exact path="/transationFailed" component={TransactionFailed} />
              <Route exact path="/digivend/generalSetting" component={VendorPortal} />
              <Route exact path="/digiproc/generalSetting" component={DigiProc} />

              {/* _________________________________Product Catalogue_________________________ */}
              <Route exact path="/catalogue/vendor/manage" component={ProductCatalogue} />
              <Route exact path="/catalogue/vendor/manageCatalogue" component={ProductCatalogue} />
              <Route exact path="/catalogue/vendor/products/wishlist" component={ProductCatalogue} />
              <Route exact path="/productCatalogue/emptyWishlist" component={ProductCatalogue} />
              <Route exact path="/catalogue/vendor/submission/history" component={ProductCatalogue} />
              <Route exact path="/productCatalogue/catalogueHistory/lotDetails" component={ProductCatalogue} />
              <Route exact path="/productCatalogue/catalogueHistory/lotDetailsRejected" component={ProductCatalogue} />
              <Route exact path="/productCatalogue/catalogueHistory/lotDetailsPartial" component={ProductCatalogue} />
              <Route exact path="/productCatalogue/addProducts" component={ProductCatalogue} />
              <Route exact path="/catalogue/retailer/products/view" component={ProductCatalogue} />
              <Route exact path="/catalogue/retailer/products/wishlist" component={ProductCatalogue} />
              <Route exact path="/productCatalogue/itemPreview" component={ProductCatalogue} />

              {/* ___________________________________Comment Page_________________________________ */}
              <Route exaxt path="/digivend/searchComment" component={VendorPortal} />
              <Route exaxt path="/vendor/vendorSearchComment" component={VendorPortal} />
              <Route exaxt path="/digivend/configuration" component={VendorPortal} />
              <Route exaxt path="/digivend/customData" component={VendorPortal} />

              {/* _________________________________Procurement Otb Report_________________________ */}
              <Route exaxt path="/analytics/otbReport" component={purchaseIndent} />

              {/* _________________________________Vendor Portal Reports_________________________ */}
              <Route exaxt path="/analytics/retailerSalesReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerStockReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerLedgerReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerPoReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerLrReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerAsnReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerOutstandingReport" component={VendorPortal} />
              <Route exaxt path="/analytics/retailerInspectionReport" component={VendorPortal} />

              <Route exaxt path="/analytics/vendorSalesReport" component={VendorPortal} />
              <Route exaxt path="/analytics/vendorStockReport" component={VendorPortal} />
              <Route exaxt path="/analytics/vendorLedgerReport" component={VendorPortal} />
              <Route exaxt path="/analytics/vendorPoReport" component={VendorPortal} />
              <Route exaxt path="/analytics/vendorLrReport" component={VendorPortal} />
              <Route exaxt path="/analytics/vendorAsnReport" component={VendorPortal} />
              <Route exaxt path="/analytics/logisticsInventoryReport" component={VendorPortal} />
              <Route exaxt path="/analytics/logisticsPaymentReconciliation" component={VendorPortal} />
              {/* _________________________________Replenishment New Pages_________________________ */}
              <Route exaxt path="/inventoryPlanning/inventoryAutoConfig" component={InventoryPlanning} />
              <Route exaxt path="/inventoryPlanning/runOnDemand" component={InventoryPlanning} />
              <Route exaxt path="/inventoryPlanning/createEvent" component={InventoryPlanning} />
              <Route exaxt path="/inventoryPlanning/createSeason" component={InventoryPlanning} />
              <Route exaxt path="/inventoryPlanning/createSite" component={InventoryPlanning} />
              <Route exaxt path="/configuration/inventoryConfiguration" component={InventoryPlanning} />
              <Route exaxt path="/configuration/coreConfiguration" component={InventoryPlanning} />
              <Route exaxt path="/inventoryPlanning/createAdhocRequest" component={InventoryPlanning} />


              <Route exaxt path="/analytics/vendorInspectionReport" component={VendorPortal} />
              
              {/* _______________________________ Vendor Kyc_______________________________ */}
              <Route exaxt path="/other/newKyc" component={VendorKycContainer} />
              <Route exaxt path="/other/manageKyc" component={Vendor} />

              {/* _________________________________Ars Reports_________________________ */}
              <Route exaxt path="/digiars/inventoryPlanningReport" component={InventoryPlanning} />
              <Route exaxt path="/digiars/salesInventoryReport" component={InventoryPlanning} />
              {/* _________________________________Procurement Excel Upload____________________________ */}
              <Route exaxt path="/digiproc/excelUpload" component={DigiProc} />
              <Route exaxt path="/digiproc/excelUploadHistory" component={DigiProc} />

              {/* _______________________________Invoice Management_________________________ */}
              <Route exaxt path="/invoiceManagement/configuration" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/customers" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/createCustomer" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/items" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/createItem" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/invoices" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/createInvoice" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/estimate" component={InvoiceManagement} />
              <Route exaxt path="/invoiceManagement/deliveryChallan" component={InvoiceManagement} />
              <Route exaxt path="/inventoryPlanning/saleContribution" component={Replenishment} />
              <Route exaxt path="/inventoryPlanning/lastMonthReport" component={Replenishment} />
              <Route exaxt path="/inventoryPlanning/topBottomReports" component={Replenishment} />
              <Route exaxt path="/inventoryPlanning/catSizeReport" component={Replenishment} />
              <Route exaxt path="/inventoryPlanning/storeToStoreReport" component={Replenishment} />
              <Route exaxt path="/inventoryPlanning/topMovingReport" component={Replenishment} />
              <Route exaxt path="/inventoryPlanning/exceptionReport" component={Replenishment} />

              {/* _______________________________Vendor Reports_____________________________ */}
              <Route exaxt path="/vendor/activityReport" component={VendorPortal} />
              <Route exaxt path="/vendor/loginLogoutReport" component={VendorPortal} />
              {/* _________________________________Order Request___________________________ */}
              <Route exaxt path="/vendor/orderRequest" component={VendorPortal} />

              {/* _____________________________CUSTOMER_PORTAL_____________________________ */}
              <Route exact path="/customerAdmin" component={CustomerAdmin} />

              <Route component={Error} />
            </Switch>
          </HashRouter>
        </Suspense >
      </Provider >
    )
  }
}

export default App
