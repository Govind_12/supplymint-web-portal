var config = {
    apiKey: "AIzaSyBGTx6KDKAqq5nHq20qs89ovOQlsH1neHs",
    authDomain: "supplymint-c77d0.firebaseapp.com",
    databaseURL: "https://supplymint-c77d0.firebaseio.com",
    projectId: "supplymint-c77d0",
    storageBucket: "supplymint-c77d0.appspot.com",
    messagingSenderId: "441454815200",
    appId: "1:441454815200:web:3638e5479ec2701259ed85",
    measurementId: "G-SML7Z17PS5"
};

firebase.initializeApp(config);
export const messaging = firebase.messaging();

console.log(":after firebase messaging", ("serviceWorker" in navigator))
if ("serviceWorker" in navigator) {
    console.log("in service worker navigator", navigator.serviceWorker)
    navigator.serviceWorker.register("/firebase-messaging-sw.js", { scope: './' })
        .then(function (registration) {
            console.log("Registration successful, scope is:", registration.scope);
            messaging.useServiceWorker(registration);
            // Request for permission
            messaging.requestPermission()
                .then(function () {
                    console.log('Notification permission granted.');
                    // TODO(developer): Retrieve an Instance ID token for use with FCM.
                    messaging.getToken()
                        .then(function (currentToken) {
                            if (currentToken) {
                                console.log('Token: ' + currentToken)
                                sendTokenToServer(currentToken);
                            } else {
                                console.log('No Instance ID token available. Request permission to generate one.');
                                //setTokenSentToServer(false);
                            }
                        })
                        .catch(function (err) {
                            console.log('An error occurred while retrieving token. ', err);
                            //setTokenSentToServer(false);
                        });
                })
                .catch(function (err) {
                    console.log('Unable to get permission to notify.', err);
                });
        })
        .catch(function (err) {
            console.log("Service worker registration failed, error:", err);
        });
}

// Handle incoming messages
//messaging.onMessage(function (payload) {
  //  console.log("Notification received: ", payload);
    // alert(payload.notification.body, payload.notification.title)
    // toastr["info"](payload.notification.body, payload.notification.title);
//});

// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function () {
    messaging.getToken()
        .then(function (refreshedToken) {
            console.log('Token refreshed.');
            // Indicate that the new Instance ID token has not yet been sent 
            // to the app server.
            sendTokenToServer(refreshedToken);
        })
        .catch(function (err) {
            console.log('Unable to retrieve refreshed token ', err);
        });
});

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
    console.log('Setting new token to local storage...');
    localStorage.setItem('firebase-token',currentToken);
}