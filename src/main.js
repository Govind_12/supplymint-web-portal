import React from "react";
import ReactDOM from "react-dom";
import createStore from "./store/createStore";
import "./styles/main.scss";
import "./firebase-script.js";


const store = createStore(window.__INITIAL_STATE__);

const MOUNT_NODE = document.getElementById("app");

let render = () => {


  const App = require("./routes/index").default;

  const Loading = require("./components/loading").default;

  ReactDOM.render(<Loading />, MOUNT_NODE);

  setTimeout(() => {
    ReactDOM.render(<App store={store} />, MOUNT_NODE);
    
    //initializeFirebase();
    // ServiceWorker.register()
  }, 2000)
};

if (__DEV__) {
  if (module.hot) {
    const renderApp = render;
    const renderError = error => {
      const RedBox = require("redbox-react").default;

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    render = () => {
      try {
        renderApp();
      } catch (e) {
        console.error(e);
        renderError(e);
      }
    };

    module.hot.accept(["./routes/index"], () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE);
        render();
      })
    );
  }
}

if (__DEVELOP__) {
  if (module.hot) {
    const renderApp = render;
    const renderError = error => {
      const RedBox = require("redbox-react").default;

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    render = () => {
      try {
        renderApp();
      } catch (e) {
        console.error(e);
        renderError(e);
      }
    };

    module.hot.accept(["./routes/index"], () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE);
        render();
      })
    );
  }

}

if (__QUALITY__) {
  if (module.hot) {
    const renderApp = render;
    const renderError = error => {
      const RedBox = require("redbox-react").default;

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    render = () => {
      try {
        renderApp();
      } catch (e) {
        console.error(e);
        renderError(e);
      }
    };

    module.hot.accept(["./routes/index"], () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE);
        render();
      })
    );
  }
}
if (!__TEST__) render();
