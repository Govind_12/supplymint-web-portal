
export const OganisationIdName = () => {
    let orgIdGlobal = ""
    let orgNameGlobal = ""
    let orgCodeGlobal=""
    if (sessionStorage.getItem('orgId-name') != null) {
        let organization = JSON.parse(sessionStorage.getItem('orgId-name'))
  
        for (let i = 0; i < organization.length; i++) {
            if (organization[i].active == "TRUE") {
                orgIdGlobal = organization[i].orgId
                orgNameGlobal = organization[i].orgName
                orgCodeGlobal= organization[i].orgCode

            }
        }
        
      
    }
      return {orgIdGlobal , orgNameGlobal,orgCodeGlobal}
} 