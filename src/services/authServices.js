import axios from 'axios';
// import _ from 'lodash';

export default function loginAjax(method, URL, data, api) {
  if (method === 'POST') {
    let config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return axios.post(URL, data, config);
  } else if (method === 'GET') {
    let config = {
      headers: {
        "Content-Type": "application/json"
      }
    }
    return axios.get(URL, config);
  }
}