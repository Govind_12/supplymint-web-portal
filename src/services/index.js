import axios from 'axios';
// import _ from 'lodash';
export default function fireAjax(method, URL, data, api) {
  if (method === 'POST') {
    let config = {
      headers: {
        'Content-Type': 'application/json',
        //'X-TENANT-ID': "d16c149919433dbcd4ba5c3255ae8fe1c906a99b967f5193f19ecd4bd4b51901",
        'X-Auth-Token': sessionStorage.getItem('token'),
        'Page-Link': window.location.hash
        // 'X-Auth-Token' : "eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJEZW1vIG1haW4iLCJlaWQiOjEsImVtbCI6ImluZm9AdHVybmluZ2Nsb3VkLmNvbSIsIlgtVEVOQU5ULUlEIjoiZDE2YzE0OTkxOTQzM2RiY2Q0YmE1YzMyNTVhZThmZTFjOTA2YTk5Yjk2N2Y1MTkzZjE5ZWNkNGJkNGI1MTkwMSIsIkJVQ0tFVCI6InRjbG91ZC1zdXBwbHltaW50LWRldmVsb3AtY29tIiwiaXNzIjoiU3VwcGx5TWludCIsImlwYSI6IklQQSIsImV4cCI6MTU1OTIyNjQyOSwicHJuIjoic3VtZWV0YSIsImp0aSI6M30._q5klrasadrML4Wo3SdmPEdlVAluH_hZjmlHgAWgbaWFdOFHNW0vOkm1RE4SKr9JoUZSy5kvhkvff1ic88tLrg"

      }
    }
    return axios.post(encodeURI(URL), data, config);
  } else if (method === 'GET') {
    let config = {
      headers: {
        "Content-Type": "application/json",
        //'X-TENANT-ID': "d16c149919433dbcd4ba5c3255ae8fe1c906a99b967f5193f19ecd4bd4b51901",
        'X-Auth-Token': sessionStorage.getItem('token'),
        'Page-Link': window.location.hash
        // 'X-Auth-Token' : "eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJEZW1vIG1haW4iLCJlaWQiOjEsImVtbCI6ImluZm9AdHVybmluZ2Nsb3VkLmNvbSIsIlgtVEVOQU5ULUlEIjoiZDE2YzE0OTkxOTQzM2RiY2Q0YmE1YzMyNTVhZThmZTFjOTA2YTk5Yjk2N2Y1MTkzZjE5ZWNkNGJkNGI1MTkwMSIsIkJVQ0tFVCI6InRjbG91ZC1zdXBwbHltaW50LWRldmVsb3AtY29tIiwiaXNzIjoiU3VwcGx5TWludCIsImlwYSI6IklQQSIsImV4cCI6MTU1OTIyNjQyOSwicHJuIjoic3VtZWV0YSIsImp0aSI6M30._q5klrasadrML4Wo3SdmPEdlVAluH_hZjmlHgAWgbaWFdOFHNW0vOkm1RE4SKr9JoUZSy5kvhkvff1ic88tLrg"
      }
    }
    return axios.get(encodeURI(URL), config);
  }
}