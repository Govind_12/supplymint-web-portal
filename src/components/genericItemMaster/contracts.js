import React from "react";
import AddItem from "./Manageitem/addItem";
import EmptyBox from "./Manageitem/emptyBox";
import ViewItem from "./Manageitem/viewItem";
import ItemData from "../../json/itemData.json";

class Contracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemState: [],
      // addItem: false
    };
  }

  onAddItem() {
    // this.setState({
    //   addItem: true
    // });
    this.props.history.push('/vendor/contracts/addVendor')

  }

  onSaveItem() {
    // this.setState({
      // addItem: false,
      // itemState: ItemData
    // });
    this.props.history.push('/vendor/contracts/viewVendor')
  }

  render() {
    
    const hash = window.location.hash.split("/")[3];
    
    return (
      <div className="container-fluid">
         { hash == "emptyBox" ?
             <EmptyBox
             {...this.props}
             itemState = {this.state.itemState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addItem={() => this.onAddItem()}
            />
           : hash == "viewItem" ? 
            <ViewItem
              ItemData={ItemData}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            
         : hash == "addItem" ?
          <AddItem
            {...this.props}
            saveItem={() => this.onSaveItem()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />: null} 
      </div>
    );
  }
}

export default Contracts;
