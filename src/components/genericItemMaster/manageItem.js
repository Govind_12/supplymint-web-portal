import React from "react";
import AddItem from "./Manageitem/addItem";
import EmptyBox from "./Manageitem/emptyBox";
import ViewItem from "./Manageitem/viewItem";
import ItemData from "../../json/itemData.json";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

class ManageItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemState: [],
      // addVendor: false
      page: 1,
      success: false,
      alert: false,
      loader: false,
      errorMessage: "",
      successMessage: "",
      emptyBox: false,
      viewItem: false,

      errorCode: "",
      code: "",
    };
  }


  // componentWillMount() {
  //   if (!this.props.vendor.getItem.isSuccess) {
  //     // this.props.vendorRequest(this.state.page);
  //   } else {
  //     this.setState({
  //       loader: false
  //     })
  //     if (this.props.vendor.getItem.data.resource == null) {
  //       this.setState({
  //         emptyBox: true,
  //         viewItem: false
  //       })
  //     } else {
  //       this.setState({
  //         itemState: this.props.vendor.getItem.data.resource,
  //         emptyBox: false,
  //         viewItem: true
  //       })
  //     }
  //   }
  // }
  componentWillReceiveProps(nextProps) {
    // if (!nextProps.vendor.editItem.isLoading && !nextProps.vendor.editItem.isSuccess && !nextProps.vendor.editItem.isError) {
    //   this.setState({
    //     loader: false,

    //   })
    // }
    if (nextProps.vendor.getAllManageItem.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getAllManageItemClear()
      if (nextProps.vendor.getAllManageItem.data.resource == null) {
        this.setState({
          emptyBox: true
        })
      } else {
        this.setState({
          itemState: nextProps.vendor.getAllManageItem.data.resource,
          viewItem: true
        })
      }
    } else if (nextProps.vendor.getAllManageItem.isError) {
      this.setState({
        alert: true,
        success: false,
        errorCode: nextProps.vendor.getAllManageItem.message.error == undefined ? undefined : nextProps.vendor.getAllManageItem.message.error.errorCode,
        errorMessage: nextProps.vendor.getAllManageItem.message.error == undefined ? undefined : nextProps.vendor.getAllManageItem.message.error.errorMessage,
        loader: false,

        code: nextProps.vendor.getAllManageItem.message.status,
      })
      this.props.getAllManageItemClear()
    } else if (nextProps.vendor.getAllManageItem.isLoading) {
      this.setState({
        loader: true
      })
    }

    // if (nextProps.vendor.deleteItem.isLoading) {
    //   this.setState({
    //     loader: true
    //   })
    // }
    // else if (nextProps.vendor.deleteItem.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.deleteItem.data.message,
    //     alert: false,
    //   })
    //   this.props.deleteItemRequest();
    // }
    // else if (nextProps.vendor.deleteItem.isError) {
    //   this.setState({
    //     alert: true,
    //     errorMessage: nextProps.vendor.deleteItem.error.errorMessage,
    //     success: false,
    //     loader: false
    //   })
    //   this.props.deleteItemRequest();
    // }

    // //edit  organization modal
    // if (nextProps.vendor.editItem.isLoading) {
    //   this.setState({
    //     loader: true
    //   })

    // }
    // else if (nextProps.vendor.editItem.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.editItem.data.message,
    //     alert: false,
    //   })
    //   this.props.editVendorRequest();

    // }
    // else if (nextProps.vendor.editItem.isError) {
    //   this.setState({
    //     alert: true,
    //     success: false,
    //     errorCode: nextProps.vendor.editItem.message.error == undefined ? undefined : nextProps.vendor.editItem.message.error.errorCode,
    //     errorMessage: nextProps.vendor.editItem.message.error == undefined ? undefined : nextProps.vendor.editItem.message.error.errorMessage,
    //     loader: false,

    //     code: nextProps.vendor.editItem.message.status,
    //   })
    //   this.props.editVendorRequest();
    // }

    // dynamic header
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getHeaderConfigClear()
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
      this.props.getHeaderConfigClear()
    }

    // if (nextProps.vendor.accessItemPortal.isSuccess) {
    //   this.setState({
    //     loader: false
    //   })
    //   // this.props.accessItemPortalClear()
    // } else if (nextProps.vendor.accessItemPortal.isError) {
    //   this.setState({
    //     loader: false,
    //     alert: true,
    //     code: nextProps.vendor.accessItemPortal.message.status,
    //     errorCode: nextProps.vendor.accessItemPortal.message.error == undefined ? "NA" : nextProps.vendor.accessItemPortal.message.error.code,
    //     errorMessage: nextProps.vendor.accessItemPortal.message.error == undefined ? "NA" : nextProps.vendor.accessItemPortal.message.error.message
    //   })
    //   //this.props.accessItemPortalClear()
    // }

    if (//nextProps.vendor.createItem.isLoading || 
      nextProps.replenishment.getHeaderConfig.isLoading
        //|| nextProps.vendor.accessItemPortal.isLoading
        ) {
      this.setState({
        loader: true
      })
    }
  }
  onAddItem() {
    // this.setState({
    //   addVendor: true
    // });
    this.props.history.push('/mdm/manageItems/addItem')
  }

  onSaveItem() {
    this.setState({
      // addVendor: false,
      itemState: ItemData
    });
    // this.props.history.push('/vendor/manageVendors/viewVendor')
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  render() {
    const hash = window.location.hash.split("/")[3];
    return (
      <div className="container-fluid pad-0">
        {/*{ this.state.emptyBox  ?
             <EmptyBox
             {...this.props}
             vendorState = {this.state.vendorState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addVendor={() => this.onAddVendor()}
            />
           : null}*/}

        {/*{ this.state.viewVendor ? */}
        <ViewItem
          ItemData={this.state.itemState}
          {...this.props}
          clickRightSideBar={() => this.props.rightSideBar()}
        />
        {/*: null}*/}

        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default ManageItem;
