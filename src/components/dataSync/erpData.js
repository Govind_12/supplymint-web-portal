import React from "react";

class ErpData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSyncErp: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            fileName:"",
            status:"",
            bucketPath:"",
            eventEnd:"",
            eventStart:"",
            dataCount:"",
            key:""
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dataSync.dataSyncErp.isSuccess) {
            this.setState({
                loader: false,
                dataSyncErp: nextProps.dataSync.dataSyncErp.data.resource == null ? [] : nextProps.dataSync.dataSyncErp.data.resource,
                prev: nextProps.dataSync.dataSyncErp.data.prePage == null ? 0 : nextProps.dataSync.dataSyncErp.data.prePage,
                current: nextProps.dataSync.dataSyncErp.data.currPage == null ? 0 : nextProps.dataSync.dataSyncErp.data.currPage,
                next: nextProps.dataSync.dataSyncErp.data.currPage + 1 == null ? 0 : nextProps.dataSync.dataSyncErp.data.currPage + 1,
                maxPage: nextProps.dataSync.dataSyncErp.data.maxPage == null ? 0 : nextProps.dataSync.dataSyncErp.data.maxPage,
            })
        }
    }
    // _____________________________PAGINATION FOR CUSTOMTAB__________________________________

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.dataSync.dataSyncErp.data.prePage,
                current: this.props.dataSync.dataSyncErp.data.currPage,
                next: this.props.dataSync.dataSyncErp.data.currPage + 1,
                maxPage: this.props.dataSync.dataSyncErp.data.maxPage,
            })
            if (this.props.dataSync.dataSyncErp.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.dataSync.dataSyncErp.data.currPage - 1,
                    search: this.state.search,
                    fileName: this.state.fileName,
                    status: this.state.status,
                    bucketPath: this.state.bucketPath,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount,
                    key: this.state.key
                }
                this.props.dataSyncErpRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.dataSync.dataSyncErp.data.prePage,
                current: this.props.dataSync.dataSyncErp.data.currPage,
                next: this.props.dataSync.dataSyncErp.data.currPage + 1,
                maxPage: this.props.dataSync.dataSyncErp.data.maxPage,
            })
            if (this.props.dataSync.dataSyncErp.data.currPage != this.props.dataSync.dataSyncErp.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.dataSync.dataSyncErp.data.currPage + 1,
                    search: this.state.search,
                    fileName: this.state.fileName,
                    status: this.state.status,
                    bucketPath: this.state.bucketPath,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount,
                    key: this.state.key
                }
                this.props.dataSyncErpRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.dataSync.dataSyncErp.data.prePage,
                current: this.props.dataSync.dataSyncErp.data.currPage,
                next: this.props.dataSync.dataSyncErp.data.currPage + 1,
                maxPage: this.props.dataSync.dataSyncErp.data.maxPage,
            })
            if (this.props.dataSync.dataSyncErp.data.currPage <= this.props.dataSync.dataSyncErp.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.search,
                    fileName: this.state.fileName,
                    status: this.state.status,
                    bucketPath: this.state.bucketPath,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount,
                    key: this.state.key
                }
                this.props.dataSyncErpRequest(data)
            }

        }
    }

    render() {
        return (
            <div>

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 bordere3e7f3 tableGeneric">
                    <div className="zui-wrapper">
                        <div className="scrollableTableFixed table-scroll zui-scroller dataSyncTable scrollableOrgansation orgScroll" id="table-scroll">

                            <table className="table scrollTable main-table zui-table">
                                <thead>
                                    <tr>
                                        <th>
                                            <label>Name</label>
                                        </th>

                                        <th>
                                            <label>Data Count</label>
                                        </th>
                                        <th>
                                            <label>Event Start</label>
                                        </th>
                                        <th>
                                            <label>Event End</label>
                                        </th>
                                        <th>
                                            <label>Key</label>
                                        </th>
                                        <th>
                                            <label>Status</label>
                                        </th>

                                    </tr>
                                </thead>

                                <tbody>
                                    {this.state.dataSyncErp.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.dataSyncErp.map((data, key) => (
                                        <tr key={key}>
                                            <td>
                                                <label>
                                                    {data.name}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.dataCount}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.eventStart}
                                                </label>
                                            </td>
                                            <td>
                                                <label>{data.eventEnd}</label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.key}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.status}
                                                </label>
                                            </td>

                                        </tr>
                                    ))}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="pagerDiv">
                    <ul className="list-inline pagination">
                        <li >
                            <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                First
                                     </button>
                        </li>

                        <li>
                            <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                Prev
                  </button>
                        </li>
                        <li>
                            <button className="PageFirstBtn pointerNone">
                                <span>{this.state.current}/{this.state.maxPage}</span>
                            </button>
                        </li>
                        {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                Next
                  </button>
                        </li> : <li >
                                <button className="PageFirstBtn borderNone" disabled>
                                    Next
                  </button>
                            </li>}


                    </ul>
                </div>
            </div>
        )
    }
}

export default ErpData;