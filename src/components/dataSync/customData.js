import React from "react";

class CustomData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            dataSyncCustom: [],
            fileName:"",
            status:"",
            bucketPath:"",
            eventEnd:"",
            eventStart:"",
            dataCount:"",
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.dataSync.getAllDataSync.isSuccess) {
            this.setState({
                loader: false,
                dataSyncCustom: nextProps.dataSync.getAllDataSync.data.resource == null ? [] : nextProps.dataSync.getAllDataSync.data.resource,
                prev: nextProps.dataSync.getAllDataSync.data.prePage == null ? 0 : nextProps.dataSync.getAllDataSync.data.prePage,
                current: nextProps.dataSync.getAllDataSync.data.currPage == null ? 0 : nextProps.dataSync.getAllDataSync.data.currPage,
                next: nextProps.dataSync.getAllDataSync.data.currPage + 1 == null ? 0 : nextProps.dataSync.getAllDataSync.data.currPage + 1,
                maxPage: nextProps.dataSync.getAllDataSync.data.maxPage == null ? 0 : nextProps.dataSync.getAllDataSync.data.maxPage,
            })
        }

    }

    // _____________________________PAGINATION FOR CUSTOMTAB__________________________________

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.dataSync.getAllDataSync.data.prePage,
                current: this.props.dataSync.getAllDataSync.data.currPage,
                next: this.props.dataSync.getAllDataSync.data.currPage + 1,
                maxPage: this.props.dataSync.getAllDataSync.data.maxPage,
            })
            if (this.props.dataSync.getAllDataSync.data.currPage != 0) {
                let data = {
                    type: this.props.type,
                    no: this.props.dataSync.getAllDataSync.data.currPage - 1,
                    search: this.state.search,
                    fileName: this.state.fileName,
                    status: this.state.status,
                    bucketPath: this.state.bucketPath,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount
                }
                this.props.getAllDataSyncRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.dataSync.getAllDataSync.data.prePage,
                current: this.props.dataSync.getAllDataSync.data.currPage,
                next: this.props.dataSync.getAllDataSync.data.currPage + 1,
                maxPage: this.props.dataSync.getAllDataSync.data.maxPage,
            })
            if (this.props.dataSync.getAllDataSync.data.currPage != this.props.dataSync.getAllDataSync.data.maxPage) {
                let data = {
                    type: this.props.type,
                    no: this.props.dataSync.getAllDataSync.data.currPage + 1,
                    search: this.state.search,
                    fileName: this.state.fileName,
                    status: this.state.status,
                    bucketPath: this.state.bucketPath,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount
                }
                this.props.getAllDataSyncRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.dataSync.getAllDataSync.data.prePage,
                current: this.props.dataSync.getAllDataSync.data.currPage,
                next: this.props.dataSync.getAllDataSync.data.currPage + 1,
                maxPage: this.props.dataSync.getAllDataSync.data.maxPage,
            })
            if (this.props.dataSync.getAllDataSync.data.currPage <= this.props.dataSync.getAllDataSync.data.maxPage) {
                let data = {
                    type: this.props.type,
                    no: 1,
                    search: this.state.search,
                    fileName: this.state.fileName,
                    status: this.state.status,
                    bucketPath: this.state.bucketPath,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount
                }
                this.props.getAllDataSyncRequest(data)
            }

        }
    }

    render() {
        return (
            <div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 bordere3e7f3 tableGeneric">
                    <div className="zui-wrapper">
                        <div className="scrollableTableFixed table-scroll zui-scroller dataSyncTable scrollableOrgansation orgScroll" id="table-scroll">
                            <table className="table scrollTable main-table zui-table">
                                <thead>
                                    <tr>
                                        <th>
                                            <label>Name</label>
                                        </th>

                                        <th>
                                            <label>Data Count</label>
                                        </th>
                                        <th>
                                            <label>Event Start</label>
                                        </th>
                                        <th>
                                            <label>Event End</label>
                                        </th>
                                        <th>
                                            <label>Bucket Path</label>
                                        </th>
                                        <th>
                                            <label>
                                                File Name
                                                        </label>
                                        </th>
                                        <th>
                                            <label>Created By</label>
                                        </th>
                                        <th>
                                            <label>
                                                Created On
                                                        </label>
                                        </th>
                                        <th>
                                            <label>Status</label>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {this.state.dataSyncCustom.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.dataSyncCustom.map((data, key) => (
                                        <tr key={key}>
                                            {/* <td className="fixed-side"><label className="checkBoxLabelOrg"><input type="checkBox" /><span className="checkmarkOrg"></span></label>
                                                    <button onClick={() => this.organisationModalOpen(`${data.orgID}`)} >
                                                        <img src={editIcon} />
                                                    </button>
                                                    <div className="topToolTip">
                                                        <button onClick={() => this.onDeleteOrganization(`${data.orgID}`)}>
                                                            <img src={deleteIcon} /></button>
                                                        <span className="topToolTipText">Delete Record</span>
                                                    </div>
                                                </td> */}
                                            <td>
                                                <label>
                                                    {data.templateName}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.dataCount}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.eventStart}
                                                </label>
                                            </td>
                                            <td>
                                                <label>{data.eventEnd}</label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.bucketPath}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.fileName}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.createdBy}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.createdOn}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.status}
                                                </label>
                                            </td>

                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="pagerDiv">
                    <ul className="list-inline pagination">
                        <li >
                            <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                First
                                     </button>
                        </li>

                        <li>
                            <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                Prev
                  </button>
                        </li>
                        <li>
                            <button className="PageFirstBtn pointerNone">
                                <span>{this.state.current}/{this.state.maxPage}</span>
                            </button>
                        </li>
                        {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                Next
                  </button>
                        </li> : <li >
                                <button className="PageFirstBtn borderNone" disabled>
                                    Next
                  </button>
                            </li>}

                    </ul>
                </div>
            </div>

        )

    }
}
export default CustomData;