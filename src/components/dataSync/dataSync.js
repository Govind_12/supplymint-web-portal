import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import SideBar from '../sidebar';
import Footer from '../footer';
import RightSideBar from '../rightSideBar';
import BraedCrumps from '../breadCrumps';
import openRack from "../../assets/open-rack.svg"
import CreateTemplateModal from './createTemplateModal';
import dataSyncIcon from "../../assets/data-sync.svg";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from '../loaders/requestSuccess';
import RequestError from '../loaders/requestError';
import ErpData from './erpData';
import CustomData from './customData';
import FilterDataModal from './filterDataModal';

class DataSync extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            success: false,
            alert: false,
            errorMessage: "",
            successMessage: "",
            errorCode: "",
            rightbar: false,
            openRightBar: false,
            createTemplateModal: false,
            erpData: true,
            customData: false,
            no: 1,
            type: 1,
            search: "",
            urlLink: "",
            filter: false,
            dataFilter: false,
            // )______________________________createtemplate states______________________________
            templateName: "",
            templateNameerr: false,
            headerName: "",
            headerNameState: [],
            tabupload: true,
            tabcreate: false,
            createTemplate: false,
            uploadTemplate: false,
            templateSelect: "",
            templateSelecterr: false,
            generateTemplate: false,
            createSave: false,
            trueState: true,
            tempNameData: [],
            headersName: [],
            fileData: {},
            fileName: "",
            errorCode: "",
            alert: false,
            errorMessage: "",
            Activeupload: true,
            specialCharacter: false,
            Activecreate: false,
        }
    }
    componentWillMount() {
        let data = {
            type: 1,
            no: 1,
            search: "",
        }
        this.props.dataSyncErpRequest(data);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.dataSync.uploadTemplate.isSuccess) {
            this.setState({
                tempNameData: nextProps.dataSync.uploadTemplate.data.resource["Display Name"] == null ? this.state.tempNameData : nextProps.dataSync.uploadTemplate.data.resource["Display Name"],
                headersName: nextProps.dataSync.uploadTemplate.data.resource["Display Name"] == undefined ? nextProps.dataSync.uploadTemplate.data.resource : [],
                loader: false
            })
            this.props.uploadTemplateClear();
        } else if (nextProps.dataSync.uploadTemplate.isError) {

            if (nextProps.dataSync.uploadTemplate.message != "error occurs") {
                this.setState({
                    loader: false,
                    alert: true,
                    success: false,
                    code: nextProps.dataSync.uploadTemplate.message.status,
                    errorMessage: nextProps.dataSync.uploadTemplate.message.data.message == undefined ? undefined : nextProps.dataSync.uploadTemplate.message.data.message,
                })
            } else {
                this.setState({
                    loader: false,
                    alert: true,
                    success: false,
                    code: nextProps.dataSync.uploadTemplate.message.status,
                    errorMessage: nextProps.dataSync.uploadTemplate.message.error == undefined ? undefined : nextProps.dataSync.uploadTemplate.message.error.errorMessage,
                    errorCode: nextProps.dataSync.uploadTemplate.message.error == undefined ? undefined : nextProps.dataSync.uploadTemplate.message.error.errorCode
                })
            }
            this.props.uploadTemplateClear();
        }
        if (nextProps.dataSync.getAllDataSync.isSuccess || nextProps.dataSync.dataSyncErp.isSuccess) {
            this.setState({
                loader: false,
            })
        } else if (nextProps.dataSync.getAllDataSync.isError) {
            this.setState({
                loader: false,
                alert: true,
                success: false,
                code: nextProps.dataSync.getAllDataSync.message.status,
                errorMessage: nextProps.dataSync.getAllDataSync.message.error == undefined ? undefined : nextProps.dataSync.getAllDataSync.message.error.errorMessage,
                errorCode: nextProps.dataSync.getAllDataSync.message.error == undefined ? undefined : nextProps.dataSync.getAllDataSync.message.error.errorCode
            })
            this.props.getAllDataSyncClear();
        } else if (nextProps.dataSync.dataSyncErp.isError) {
            this.setState({
                loader: false,
                alert: true,
                success: false,
                code: nextProps.dataSync.dataSyncErp.message.status,
                errorMessage: nextProps.dataSync.dataSyncErp.message.error == undefined ? undefined : nextProps.dataSync.dataSyncErp.message.error.errorMessage,
                errorCode: nextProps.dataSync.dataSyncErp.message.error == undefined ? undefined : nextProps.dataSync.dataSyncErp.message.error.errorCode
            })
            this.props.getAllDataSyncClear();
        }
        if (nextProps.dataSync.createDataSyncTemplate.isSuccess) {
            this.setState({
                createSave: true,
                createTemplate: false,
                loader: false,
                success: true,
                successMessage: nextProps.dataSync.createDataSyncTemplate.data.message,
                urlLink: nextProps.dataSync.createDataSyncTemplate.data.resource.url
            })
            this.props.createDataSyncTemplateClear()
        } else if (nextProps.dataSync.createDataSyncTemplate.isError) {
            if (nextProps.dataSync.createDataSyncTemplate.message != "error occurs") {
                this.setState({
                    createSave: false,
                    createTemplate: true,
                    loader: false,
                    alert: true,
                    success: false,
                    code: nextProps.dataSync.createDataSyncTemplate.message.status,
                    errorMessage: nextProps.dataSync.createDataSyncTemplate.message.data.message == undefined ? undefined : nextProps.dataSync.createDataSyncTemplate.message.data.message,
                })
            } else {
                this.setState({
                    createSave: false,
                    createTemplate: true,
                    loader: false,
                    alert: true,
                    success: false,
                    code: nextProps.dataSync.createDataSyncTemplate.message.status,
                    errorMessage: nextProps.dataSync.createDataSyncTemplate.message.error == undefined ? undefined : nextProps.dataSync.createDataSyncTemplate.message.error.errorMessage,
                    errorCode: nextProps.dataSync.createDataSyncTemplate.message.error == undefined ? undefined : nextProps.dataSync.createDataSyncTemplate.message.error.errorCode
                })
            }
            this.props.createDataSyncTemplateClear()
        }
        if (nextProps.dataSync.dataSyncErp.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.dataSyncErpClear()
        }
        if (nextProps.dataSync.uploadTemplate.isLoading || nextProps.dataSync.getAllDataSync.isLoading || nextProps.dataSync.dataSyncErp.isLoading ||
            nextProps.dataSync.createDataSyncTemplate.isLoading || nextProps.dataSync.dataSyncErp.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    errorUplaod(res) {
        if (res.data.status == "4000") {
            this.setState({
                alert: true,
                code: res.data.status,
                errorMessage: res.data.data.message,
            })
        } else if (res.data.status == "2000") {
            this.setState({
                success: true,
                successMessage: res.data.data.message,
            })
        }
    }
    // __________________________SEARCH HANDLE CHANGE AND API CALL_____________________________
    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    onSearch() {
        this.setState({
            type: 3,
            no: 1
        })
        if (this.state.customData) {
            let data = {
                type: 3,
                search: this.state.search,
                no: 1,

            }
            this.props.getAllDataSyncRequest(data)
        } else if (this.state.erpData) {
            let data = {
                type: 3,
                no: 1,
                search: this.state.search
            }
            this.props.dataSyncErpRequest(data);
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }

    onClearSearch(e) {
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        if (this.state.customData) {
            let data = {
                type: 1,
                search: "",
                no: 1,
            }
            this.props.getAllDataSyncRequest(data)
        } else if (this.state.erpData) {
            let data = {
                type: 1,
                search: "",
                no: 1,
            }
            this.props.dataSyncErpRequest(data);
        }
    }
    // ________________________TAB CHANGES ______________________________
    changeTab(tab) {
        if (tab == "erpData") {
            this.setState({
                erpData: true,
                customData: false
            })
            let data = {
                type: 1,
                no: 1,
                search: "",
            }
            this.props.dataSyncErpRequest(data);
        } else if (tab == "customData") {
            this.setState({
                customData: true,
                erpData: false
            })
            let data = {
                no: 1,
                type: 1,
                search: "",
            }
            this.props.getAllDataSyncRequest(data)
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    openCreateModal(e) {
        if (this.state.customData) {
            this.setState({
                createTemplateModal: true
            })
        } else if (this.state.erpData) {
            this.setState({
                createTemplateModal: false
            })
            let data = {
                type: 1,
                no: 1,
                search: "",
            }
            this.props.dataSyncErpRequest(data);
        }
    }
    closeModal() {
        this.setState({
            Activeupload: true,
            Activecreate: false,
            tabupload: true,
            tabcreate: false,
            createTemplateModal: false
        })
    }

    // _________________-filter__________________________

    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: true,
            dataFilter: !this.state.dataFilter
        });
    }
    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            search: "",
            name: data.name,
            key: data.key,
            status: data.status,
            eventStart: data.eventStart,
            eventEnd: data.eventEnd,
            dataCount: data.dataCount,

        })
    }

    onClearFilter(e) {
        this.setState({
            type: 1,
            no: 1,
            search: "",
            name: "",
            key: "",
            status: "",
            eventStart: "",
            eventEnd: "",
            dataCount: "",

        })
        if (this.state.erpData) {
            let data = {
                type: 1,
                no: 1,
                search: ""
            }
            this.props.dataSyncErpRequest(data);
        } else if (this.state.customData) {
            let data = {
                type: 1,
                no: 1,
                search: "",
            }
            this.props.getAllDataSyncRequest(data);
        }
    }

    // _____________________________________-CREATE TEMPLATE CODE START_________________________________

    handleChange(e) {
        if (e.target.id == "templateSelect") {
            this.setState({
                templateSelect: e.target.value
            }, () => {
                this.templateSelectError();
            })
            let data = {
                tempName: e.target.value,
                isSelected: true
            }
            this.props.uploadTemplateRequest(data)
        } else if (e.target.id == "templateName") {

            this.setState({
                templateName: e.target.value
            }, () => {
                this.templateNameError();
            })
        } else if (e.target.id == "headerName") {
            this.setState({
                headerName: e.target.value
            }, () => {
                this.headerNameError();
            })
        }
    }
    // _____________________________UPLOAD TEMPLATE___________________________
    templateSelectError() {
        if (this.state.templateSelect == "") {
            this.setState({
                templateSelecterr: true
            })
        } else {
            this.setState({
                templateSelecterr: false
            })
        }
    }
    headerNameError() {
        if (this.state.headerName == "" || !this.state.headerName.match(/^[a-zA-Z0-9 ]+$/)) {
            this.setState({
                specialCharacter: true
            })
        } else {
            this.setState({
                specialCharacter: false
            })
        }
    }

    fileUpload(e) {
        let values = e.target.files
        // if (values.size < 51200) {
        // if (values[0].type == "text/csv") {
        this.setState({
            fileData: values[0],
            fileName: values[0].name,
        })
        // }
        // }
    }
    uploadSync(e) {
        let orgIdGlobal = ""
        if (sessionStorage.getItem("orgId-name") != undefined || sessionStorage.getItem("orgId-name") != null) {
            let organization = JSON.parse(sessionStorage.getItem("orgId-name"))
            for (let i = 0; i < organization.length; i++) {
                if (organization[i].active == "TRUE") {
                    orgIdGlobal = organization[i].orgId
                }
            }
        }
        const formData = new FormData();
        this.setState({
            generateTemplate: true
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        formData.append('file', this.state.fileData);
        formData.append('orgId', orgIdGlobal)
        axios.post(`${CONFIG.BASE_URL}${CONFIG.DATASYNC}/upload/data`, formData, { headers: headers })
            .then(res => {
                if (res.data.status == "2000") {
                    this.props.errorUplaod(res)
                    this.setState({
                        trueState: false,
                        generateTemplate: false,
                        fileName: ""
                    })
                    let data = {
                        type: 1,
                        no: 1,
                        search: "",
                    }
                    this.props.getAllDataSyncRequest(data)
                } else if (res.data.status == "4000") {
                    this.props.errorUplaod(res)
                    this.setState({
                        trueState: true,
                        generateTemplate: false,
                        fileName: ""
                    })
                }
            }).catch((error) => {
                this.setState({
                    generateTemplate: false,
                    fileName: ""
                })
            })
    }

    // ______________________________CREATE NEW TEMPLATE_________________________________________

    getFocus() {
        document.getElementById("headerName").focus()
    }
    tabPressed(e) {
        if (e.target.id == "headerName") {
            if (this.state.headerName != "") {
                if (e.keyCode === 9 || e.keyCode === 13) {
                    e.preventDefault();
                    this.addHeader(e)
                }
            }
        }
    }
    addHeader(e) {
        if (e.target.id == 'headerName') {
            let headerNameState = this.state.headerNameState;
            e.target.value = ""
            headerNameState.push(this.state.headerName);
            this.setState({
                headerNameState,
                headerName: "",
            })
        }
    }
    templateNameError() {
        if (this.state.templateName == "" ||
            !this.state.templateName.match(/^[a-zA-Z0-9 ]+$/)) {
            this.setState({
                templateNameerr: true
            })
        } else {
            this.setState({
                templateNameerr: false
            })
        }
    }
    createTemplateState() {
        this.templateNameError();
        setTimeout(() => {
            const { templateNameerr } = this.state
            if (!templateNameerr) {
                let headers = {}
                let headerNameState = this.state.headerNameState
                for (let i = 0; i < headerNameState.length; i++) {
                    headers["hn" + [i + 1]] = headerNameState[i]
                }
                let createData = {
                    templateName: this.state.templateName,
                    headers
                }
                this.props.createDataSyncTemplateRequest(createData)

                this.setState({
                    // createSave: true,
                    // createTemplate: false,
                    headerNameState: [],
                    templateName: "",
                    headerName: "",
                })
            }
        }, 10);
    }
    removeHeader(name) {
        let headerNameState = this.state.headerNameState;
        for (let i = 0; i < headerNameState.length; i++) {
            if (headerNameState[i] == name) {
                headerNameState.splice(i, 1);
            }
        }
        this.setState({
            headerNameState
        })
    }

    // ______________________________TAB SWICHING_______________________________

    changeTabCreate(tab) {
        if (tab == "upload") {
            this.setState({
                tabupload: true,
                tabcreate: false,
                Activeupload: true,
                Activecreate: false
            })
        } else if (tab == "create") {
            this.setState({
                tabupload: false,
                tabcreate: true,
                Activeupload: false,
                Activecreate: true
            })
        }
    }
    uploadCreateTemplate() {
        // this.setState({
        //     Activeupload: this.state.tabupload ? true : false,
        //     Activecreate: this.state.tabcreate ? true : false,
        //     uploadTemplate: this.state.tabupload || !this.state.tabcreate ? true : false,
        //     createTemplate: this.state.tabcreate || !this.state.tabupload ? true : false,
        //     tabupload: false,
        //     tabcreate: false,
        // })
        if (this.state.tabupload) {

            this.setState({
                Activeupload: true,
                Activecreate: false,
                uploadTemplate: true,
                createTemplate: false,
                tabupload: false,
                tabcreate: false,
            })
            let data = {
                tempName: this.state.templateSelect,
                isSelected: false
            }
            this.props.uploadTemplateRequest(data)
        } else if (this.state.tabcreate) {

            this.setState({
                Activeupload: false,
                Activecreate: true,
                uploadTemplate: false,
                createTemplate: true,
                tabupload: false,
                tabcreate: false,
            })
        }
    }
    cancelUpload() {
        this.setState({
            uploadTemplate: false,
            Activeupload: true,
            Activecreate: false,
            tabupload: true,
            tabcreate: false,
            createSave: false,
            createTemplate: false,
            fileName: "",
            templateSelect: "",
            headerNameState: []

        })
    }
    cancelCreate() {

        this.setState({
            createTemplate: false,
            Activeupload: false,
            Activecreate: true,
            tabcreate: true,
            tabupload: false,
            templateName: "",
            headerName: "",
            headerNameState: []
        })
    }
    render() {
        return (
            <div className="container-fluid">
                <SideBar {...this.props} />
                <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                <div className="container_div m-top-100 " id="">
                    <div className="col-md-12 col-sm-12">
                        <div className="menu_path">
                            <ul className="list-inline width_100 pad20 m-top-bot7">
                                <BraedCrumps {...this.props} />
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <div className="container_content heightAuto">
                            <div className="col-md-6 col-sm-12 pad-0">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            Data Sync
                                        </label>
                                    </li>
                                    <li className="m-top-30">
                                        {/* <label className="export_data_div">
                                                    Export Data
                                            </label> */}
                                        <ul className="list-inline m-top-10 alignMiddle">
                                            {/* <li>
                                            <button className="button_home">
                                                CSV
                                            </button>
                                        </li> */}
                                            {/* <li>
                                            <button className="button_home">
                                                XLS
                                            </button>
                                        </li> */}
                                            <li className="newFilterBtn">
                                                <button className="filter_button" onClick={e => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                                    FILTER
                                                    <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                        <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                    </svg>
                                                </button>
                                            </li>
                                            {/* <li className="refresh-table">
                                                <img src={refreshIcon} onClick={() => this.onRefresh()} />
                                                <p className="tooltiptext topToolTipGeneric">Refresh</p>
                                            </li> */}
                                            <li>
                                                <button type="button" className="dataSyncBtn" onClick={(e) => this.openCreateModal(e)}>Data Sync <img src={dataSyncIcon} /></button>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                {this.state.type != 2 ? null : <span className="clearFilterBtnData" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                            </div>

                            <div className="col-md-6 col-sm-12 pad-0 searchDataSync">
                                <ul className="list-inline search_list manageSearch alignMiddle width100 justifyEnd">
                                    <li>
                                        <div className="col-md-12 toggleBtnDash toggleSquare alignMiddle pad-0">
                                            <div className="col-md-6">
                                                <div className="saleToggleBtn">
                                                    <div className={this.state.erpData ? "unitSales activeUnit" : "unitSales"} onClick={() => this.changeTab('erpData')}>ERP Data</div>
                                                    <div className={this.state.customData ? "unitSales activeUnit" : "unitSales"} onClick={() => this.changeTab('customData')}>Custom Data</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="paginationWidth50">
                                        {/* <form className="newSearch m0" onSubmit={(e) => this.onSearch(e)}> */}
                                        <input onKeyPress={this._handleKeyPress} onChange={(e) => this.handleSearch(e)} value={this.state.search} type="text" placeholder="Search..." className="search-box width100" />
                                        {/* </form> */}
                                    </li>

                                </ul>
                                {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="clearSearchFilter">
                                    Clear Search Filter
                                </span> : null}
                            </div>
                            {this.state.erpData ? <ErpData {...this.state} {...this.props} /> : null}
                            {this.state.customData ? <CustomData {...this.state} {...this.props} /> : null}
                        </div>
                    </div>
                </div>
                {this.state.filter ? <FilterDataModal {...this.state} {...this.props} keys={this.state.key} updateFilter={(e) => this.updateFilter(e)} closeFilter={(e) => this.openFilter(e)} /> : null}
                {this.state.createTemplateModal ? <CreateTemplateModal ref={(node) => { this.saveRef = node }} {...this.props} {...this.state} closeModal={(e) => this.closeModal(e)} errorUplaod={(e) => this.errorUplaod(e)} handleChange={(e) => this.handleChange(e)} templateSelectError={(e) => this.templateSelectError(e)}
                    headerNameError={(e) => this.headerNameError(e)} fileUpload={(e) => this.fileUpload(e)} uploadSync={(e) => this.uploadSync(e)} getFocus={(e) => this.getFocus(e)} tabPressed={(e) => this.tabPressed(e)} addHeader={(e) => this.addHeader(e)} templateNameError={(e) => this.templateNameError(e)} createTemplateState={(e) => this.createTemplateState(e)} removeHeader={(e) => this.removeHeader(e)}
                    changeTabCreate={(e) => this.changeTabCreate(e)} uploadCreateTemplate={(e) => this.uploadCreateTemplate(e)} cancelUpload={(e) => this.cancelUpload(e)} cancelCreate={(e) => this.cancelCreate(e)}
                /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

                <Footer />
            </div>
        )
    }

}
export function mapStateToProps(state) {
    return {
        dataSync: state.dataSync
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DataSync);