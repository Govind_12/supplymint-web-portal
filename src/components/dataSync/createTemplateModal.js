import React from 'react'
import download from '../../assets/download (1).svg';
import close from "../../assets/close.svg";
import doneIcon from "../../assets/done.svg";
import oval from "../../assets/oval-empty.svg";
import TemplateLoader from '../loaders/generateTemplateLoader';
import axios, { post } from "axios";
import { CONFIG } from "../../config";
class CreateTemplateModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    
    render() {
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalRight dataSyncRightModal">
                        <div className="col-md-1 leftCol pad-0">
                            <div className="top m-top-75">
                                <img src={doneIcon} />
                                {/* <img src={oval} /> */}
                            </div>
                            <div className="bottom m-top-25">
                                {this.props.uploadTemplate || this.props.createSave || this.props.createTemplate ? <img src={doneIcon} />
                                    : <img src={oval} />}
                            </div>
                        </div>
                        <div className="col-md-11 pad-0">
                            <div className="modal-Header borderNoneAll displayInherit">
                                <h2>Custom Data Sync</h2>
                                <p>Sync data with existing template and newly created template</p>
                            </div>
                            <div className={this.props.uploadTemplate || this.props.createTemplate || this.props.createSave ? "col-md-12 rightModalTop opacity m-top-15" : "col-md-12 rightModalTop m-top-15"}>
                                <div className="col-md-12">
                                    <div className={this.props.Activeupload ? "active alignMiddle justifyCenter uploadTemp upCrTmp" : "alignMiddle justifyCenter uploadTemp upCrTmp"} onClick={() => this.props.uploadTemplate || this.props.createTemplate || this.props.createSave ? null : this.props.changeTabCreate('upload')}><label>Upload Template</label></div>
                                    <div className={this.props.Activecreate ? "alignMiddle justifyCenter createTemp upCrTmp active" : "alignMiddle justifyCenter createTemp upCrTmp"} onClick={() => this.props.createTemplate || this.props.uploadTemplate || this.props.createSave ? null : this.props.changeTabCreate('create')}><label>Create New Template</label></div>
                                </div>
                                <div className="footerRight">
                                    <button type="button" className="cancel" onClick={(e) => this.props.uploadTemplate || this.props.createTemplate || this.props.createSave ? null : this.props.closeModal(e)}>Close</button>
                                    <button type="button" className="next" onClick={(e) => this.props.uploadTemplate || this.props.createTemplate || this.props.createSave ? null : this.props.uploadCreateTemplate(e)} >Next</button>
                                </div>
                            </div>
                            <div className="col-md-12 rightModalBottom displayTable m-top-25 pad-0">
                                {this.props.tabupload ? <label>Upload Template & Sync</label> : null}
                                {this.props.tabcreate ? <label>Create Template and Sync</label> : null}
                                {/* ____________________________UPLOAD TEMPLATE_____________________ */}
                                {this.props.uploadTemplate || this.props.createSave ? <div className="uploadTemlateDiv templateCreatedSuccessfully">
                                    {this.props.createSave ? <div className="top displayInline width100">
                                        <div className="col-md-12">
                                            <h6>Template created successfully</h6>
                                            <p>Do you Want to upload template now ?</p>
                                            <div className="col-md-12 pad-0">
                                                <a href={this.props.urlLink} download><button type="button" className="downTempBtn displayBlock m-top-30"><span>Download Template</span><img src={download} className="imgHeight" /></button></a>
                                            </div>
                                        </div>
                                    </div> : null}
                                    {this.props.uploadTemplate ? <div className="top displayInline width100">
                                        <div className="col-md-12">
                                            <h6>Choose Template</h6>
                                            <select className="templateSelect" value={this.props.templateSelect} id="templateSelect" onChange={(e) => this.props.handleChange(e)}>
                                                <option value="">Choose Template</option>
                                                {this.props.tempNameData.length != 0 ? this.props.tempNameData.map((data, key) => (
                                                    <option value={data} key={key}>{data}</option>
                                                )) : null}
                                            </select>
                                            {this.props.templateSelecterr ? <span className="error">
                                                Choose template
                                            </span> : null}
                                            <p>Note : Expected coloum headers in selected template</p>
                                            {this.props.headersName.length == 0 || this.props.code == "4000" ? <button type="button" className="downTempBtn displayBlock btnDisabled m-top-30"><span>Download Template</span><img src={download} className="imgHeight" /></button> :
                                                <div className="col-md-12 pad-0"> <span>{this.props.headersName[0].Headers.join(',')}</span>
                                                    <a href={this.props.headersName[0].url} download><button type="button" className="downTempBtn displayBlock m-top-30"><span>Download Template</span><img src={download} className="imgHeight" /></button></a>
                                                </div>}
                                        </div>
                                    </div> : null}
                                    <div className="bottom displayInGrid">
                                        <div className="col-md-12 pad-0">
                                            <div className="piImageModalMain">
                                                <div className="col-md-5 pad-0">
                                                    <div className={this.props.templateSelect == "" || !this.props.createSave ? "btnDisabled chooseFileBtn width_88" : "chooseFileBtn width_88"} id="chooseImage">
                                                        <div>
                                                            {this.props.templateSelect != "" || this.props.createSave ? <div><input type="file" className="imageChooser" id="fileUpload" onChange={(e) => this.props.fileUpload(e)} />
                                                                <label className="chosenImage" >
                                                                    Choose File
                                                        </label>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="20" viewBox="0 0 17 20">
                                                                    <path fill="#6629DB" fillRule="evenodd" d="M10.502.472a5.736 5.736 0 0 0-4.485 2.817L.986 11.861a.667.667 0 1 0 1.15.675l5.031-8.572c1.216-2.071 3.824-2.787 5.864-1.628 2.04 1.158 2.707 3.713 1.491 5.784l-5.03 8.572-.378.643a2.747 2.747 0 0 1-3.74.982c-1.322-.751-1.757-2.358-.998-3.651l4.533-7.723.944-1.61a1.226 1.226 0 0 1 1.675-.437c.598.34.787 1.04.446 1.621l-5.1 8.688a.667.667 0 1 0 1.15.675l5.1-8.688c.706-1.203.278-2.765-.936-3.455-1.215-.69-2.78-.28-3.485.922l-.945 1.609-4.532 7.723c-1.125 1.916-.45 4.384 1.49 5.485 1.94 1.102 4.424.45 5.548-1.466l.378-.644 5.03-8.572c1.573-2.678.69-6.1-1.982-7.618a5.535 5.535 0 0 0-3.188-.704z" />
                                                                </svg> </div> : <div>
                                                                    <input type="file" className="btnDisabled imageChooser" disabled />
                                                                    <label className="chosenImage disableClr" >
                                                                        Choose File
                                                        </label>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="20" viewBox="0 0 17 20">
                                                                        <path fill="#d8d3d3" fillRule="evenodd" d="M10.502.472a5.736 5.736 0 0 0-4.485 2.817L.986 11.861a.667.667 0 1 0 1.15.675l5.031-8.572c1.216-2.071 3.824-2.787 5.864-1.628 2.04 1.158 2.707 3.713 1.491 5.784l-5.03 8.572-.378.643a2.747 2.747 0 0 1-3.74.982c-1.322-.751-1.757-2.358-.998-3.651l4.533-7.723.944-1.61a1.226 1.226 0 0 1 1.675-.437c.598.34.787 1.04.446 1.621l-5.1 8.688a.667.667 0 1 0 1.15.675l5.1-8.688c.706-1.203.278-2.765-.936-3.455-1.215-.69-2.78-.28-3.485.922l-.945 1.609-4.532 7.723c-1.125 1.916-.45 4.384 1.49 5.485 1.94 1.102 4.424.45 5.548-1.466l.378-.644 5.03-8.572c1.573-2.678.69-6.1-1.982-7.618a5.535 5.535 0 0 0-3.188-.704z" />
                                                                    </svg> </div>}

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-7 pad-0 ">
                                                    <div className="uploadFile displayInline">
                                                        <label className="m0">{this.props.fileName}</label>
                                                        <p>Note : Max upload size allowed <span>50mb</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="footerRight m-top-20">
                                                {this.props.createSave || !this.props.trueState ? <button type="button" className="cancel" onClick={(e) => this.props.cancelUpload(e)}>Upload Later</button> : <button type="button" className="cancel" onClick={(e) => this.props.cancelUpload(e)}>Cancel</button>}
                                                <button type="button" className={this.props.fileName == "" ? "opacity next" : "next"} onClick={(e) => this.props.fileName == "" ? null : this.props.uploadSync(e)}>Upload & Sync</button>
                                            </div>
                                        </div>
                                    </div>
                                    {this.props.generateTemplate ? <TemplateLoader /> : null}
                                </div> : null}
                                {/* ____________________________END UPLOAD TEMPLATE___________________________ */}

                                {/* _________________________CREATENEW TEMPLATE_____________________________ */}

                                {this.props.createTemplate ? <div className="createNewTemplate">
                                    <div className="col-md-12 addEmails">
                                        <h4>Template Name</h4>
                                        <div className="errorMainDiv">
                                            <input type="text" placeholder="Enter Template Name" id="templateName" value={this.props.templateName} onChange={(e) => this.props.handleChange(e)} autoComplete="off" />
                                            {this.props.templateNameerr ? <span className="error posAbsolute m0">
                                                special characters not allowed
                                        </span> : null}
                                        </div>
                                        <h4 className="m-top-15">Headers Name</h4>
                                        <div className="errorMainDiv">
                                            <div className="emailBox">
                                                <ul className="pad-0">
                                                    <div className="emails" onClick={(e) => this.props.getFocus(e)}>
                                                        {this.props.headerNameState.length == 0 ? null : this.props.headerNameState.map((data, key) => (<li key={key}>{data}<span><img src={close} onClick={(e) => this.props.removeHeader(`${data}`)} /></span></li>))}
                                                        <li className="displayInline"><input type="text" value={this.props.headerName} id="headerName" onChange={(e) => this.props.handleChange(e)} onKeyDown={(e) => { this.props.tabPressed(e) }} placeholder="Type here..." />

                                                        </li>
                                                    </div>
                                                </ul>
                                            </div>
                                            {this.props.specialCharacter ? <span className="error posAbsolute m0">
                                                special characters not allowed
                                        </span> : null}
                                        </div>
                                    </div>
                                    <div className="footerRight pad-lft-0 padRightNone">
                                        <button type="button" className="cancel" onClick={(e) => this.props.cancelCreate(e)}>Cancel</button>
                                        <button type="button" className={this.props.headerNameState.length == 0 || this.props.templateNameerr || this.props.specialCharacter || this.props.templateName == "" ? "next btnDisabled" : "next"} onClick={(e) => this.props.headerNameState.length == 0 || this.props.templateNameerr || this.props.specialCharacter || this.props.templateName == "" ? null : this.props.createTemplateState(e)}>Save</button>
                                    </div>
                                </div> : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default CreateTemplateModal;