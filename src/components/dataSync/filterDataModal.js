import React from "react";

class FilterDataModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            type: "",
            no: "",
            eventStart: "",
            dataCountstatus: "",
            dataCountCustom: "",
            eventEnd: "",
            key: "",
            dataCount: "",
            templateName: "",
            status: ""
        };
    }

    handleChange(e) {
        if (e.target.id == "name") {
            this.setState({
                name: e.target.value
            });
        } else if (e.target.id == "eventStart") {
            e.target.placeholder = e.target.value;
            this.setState({
                eventStart: e.target.value
            });
        } else if (e.target.id == "eventEnd") {
            e.target.placeholder = e.target.value;
            this.setState({
                eventEnd: e.target.value
            });
        } else if (e.target.id == "key") {
            this.setState({
                key: e.target.value
            });
        } else if (e.target.id == "dataCount") {
            this.setState({
                dataCount: e.target.value
            });
        } else if (e.target.id == "dataCountstatus") {
            this.setState({
                dataCountstatus: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            })
        } else if (e.target.id == "templateName") {
            this.setState({
                templateName: e.target.value
            });
        } else if (e.target.dataCountCustom == "dataCountCustom") {
            this.setState({
                dataCountCustom: e.target.value
            })
        }
    }

    clearFilter(e) {
        this.setState({
            orgName: "",
            type: "",
            no: "",
            name: "",
            key: "",
            dataCount: "",
            dataCountstatus: "",
            dataCountCustom: ""
        })
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.props.erpData) {
            let data = {
                no: 1,
                type: 2,
                name: this.state.name,
                key: this.state.key,
                status: this.state.dataCountstatus,
                eventStart: this.state.eventStart,
                eventEnd: this.state.eventEnd,
                dataCount: this.state.dataCount,
                serach: ""
            }
            this.props.dataSyncErpRequest(data);
            this.props.updateFilter(data)
        } else if (this.props.customData) {
            let data = {
                no: 1,
                type: 2,
                templateName: this.state.templateName,
                status: this.state.status,
                dataCount: this.state.dataCount,
                serach: ""
            }
            this.props.getAllDataSyncRequest(data);
            this.props.updateFilter(data)
        }
        this.props.closeFilter(e);
    }

    render() {
        let count = 0;
        if (this.state.name != "") {
            count++;
        }
        if (this.state.eventEnd != "") {
            count++;
        }
        if (this.state.eventStart != "") {
            count++;
        }
        if (this.state.key != "" && this.state.key != undefined) {
            count++;
        } if (this.state.dataCount != "") {
            count++;
        }
        if (this.state.templateName != "") {
            count++;
        }
        if (this.state.status != "") {
            count++;
        }
        return (

            <div className={this.props.dataFilter ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.dataFilter ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.dataFilter ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0 dataSyncFilterItem">
                            <div className="container_modal">

                                {this.props.erpData ? <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="name" value={this.state.name} placeholder={this.state.name == "" ? "Name" : this.state.name} className="organistionFilterModal" />                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} value={this.state.key} className="organistionFilterModal" id="key" placeholder={this.state.key == "" ? "Key" : this.state.key} />
                                    </li>
                                    <li>
                                        <input type="date" placeholder={this.state.eventStart == "" ? "Event start" : this.state.eventStart} onChange={(e) => this.handleChange(e)} value={this.state.eventStart} id="eventStart" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" placeholder={this.state.eventEnd == "" ? "Event end" : this.state.eventEnd} onChange={(e) => this.handleChange(e)} value={this.state.eventEnd} id="eventEnd" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" id="dataCount" value={this.state.dataCount} onChange={(e) => this.handleChange(e)} className="organistionFilterModal" placeholder={this.state.dataCount == "" ? "Data count" : this.state.dataCount} />
                                    </li>
                                </ul> : this.props.customData ? <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="templateName" value={this.state.templateName} placeholder={this.state.templateName == "" ? "Template name" : this.state.templateName} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="status" value={this.state.status} placeholder={this.state.status == "" ? "Status" : this.state.status} className="organistionFilterModal" />
                                    </li>

                                    <li>
                                        <input type="text" id="dataCountCustom" value={this.state.dataCountCustom} onChange={(e) => this.handleChange(e)} className="organistionFilterModal" placeholder={this.state.dataCountCustom == "" ? "Data count" : this.state.dataCountCustom} />
                                    </li>
                                </ul> : null}
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    {this.state.name == "" && this.state.eventEnd == "" && this.state.eventStart == "" && this.state.key == "" && this.state.dataCount == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                        : <button onClick={(e) => this.clearFilter(e)} type="button" className="modal_clear_btn">
                                            CLEAR FILTER
                                        </button>}
                                    <li>
                                        {this.state.name != "" || this.state.templateName != "" || this.state.key != "" || this.state.eventEnd != "" || this.state.eventStart != "" || this.state.dataCount != "" || this.state.status != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default FilterDataModal;
