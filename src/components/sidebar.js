import React from "react";
import Header from "./header";
import { Link } from "react-router-dom";
import profile from "../assets/avatar.svg";
import SessionTimeOut from './timeOutModals/sessionExpireTimerModal';
import IdleTimer from 'react-idle-timer'
import SessionExpired from './timeOutModals/sessionExpiredModal';

class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      over: false,
      firstShow: false,
      inactivityPopupS: false,
      expired: false,
      modules: [],
    };
    this.idleTimer = null
    this.onAction = this._onAction.bind(this)
    this.onActive = this._onActive.bind(this)
    this.onIdle = this._onIdle.bind(this)
  }

  componentWillMount() {
    if (sessionStorage.getItem('token') == null) {
      sessionStorage.setItem('redirect_url', window.location.hash)
      if (sessionStorage.getItem('redirect_url') !== null) {
        console.log("in ffffffff")
        sessionStorage.setItem('login_redirect', sessionStorage.getItem('redirect_url').replace('#', ''))
        let queryString = sessionStorage.getItem('redirect_url').replace('#', '')
        console.log("queryString", queryString, `/?redirect_url=${queryString}`)
        this.props.history.push(`/?redirect_url=${queryString}`)
      } else {
        if (sessionStorage.getItem('logout') == "true") {
          sessionStorage.clear();
          this.props.history.push('/');
        }
        if (sessionStorage.getItem('token') == null) {
          this.props.history.push('/');
        }
      }
    }
  }
  // componentWillMount() {
  //   if (sessionStorage.getItem('token') == null) {
  //     sessionStorage.setItem('redirect_url', window.location.hash)
  //     if (sessionStorage.getItem('redirect_url') !== null) {
  //       sessionStorage.setItem('login_redirect', sessionStorage.getItem('redirect_url').replace('#', ''))
  //       let queryString = sessionStorage.getItem('redirect_url').replace('#', '')

  //       this.props.history.push(`/?redirect_url=${queryString}`)


  //     } else {
  //       if (sessionStorage.getItem('logout') == "true") {
  //         sessionStorage.clear();
  //         this.props.history.push('/');
  //       }
  //       if (sessionStorage.getItem('token') == null) {
  //         this.props.history.push('/');
  //       }
  //     }
  //   }
  // }
  // sessionStorage.setItem('redirect_url', window.location.hash)
  //  console.log(window.location.hash);
  //    if (sessionStorage.getItem('token') == null) {
  //   sessionStorage.setItem('redirect_url', window.location.hash)

  //   if (sessionStorage.getItem('redirect_url') !== null) {
  //     var a=sessionStorage.getItem('redirect_url');
  //     var b=a.replace('#','').split('?');
  //     var pathParam=b[1];
  //     var queryParam=b.splice(2);
  //     sessionStorage.setItem('redirect_url_pathParam',pathParam);
  //     sessionStorage.setItem('redirect_url_queryParam',queryParam);
  //     sessionStorage.setItem('login_redirect', sessionStorage.getItem('redirect_url').replace('#', ''));
  //     let queryString = sessionStorage.getItem('redirect_url').replace('#', '');
  //     this.props.history.push(`/?redirect_url=${queryString}`)
  //   } else {
  //     if (sessionStorage.getItem('logout') == "true") {
  //       sessionStorage.clear();
  //       this.props.history.push('/');
  //     }
  //     if (sessionStorage.getItem('token') == null) {
  //       this.props.history.push('/');
  //     }
  //   }
  // }}
  // console.log("in will mount sidebar", sessionStorage.getItem('token') == null , sessionStorage.getItem('token'))
  // if (sessionStorage.getItem('token') !== null) {
  // sessionStorage.setItem('redirect_url', window.location.hash)
  // var pathParams = window.location.hash.replace("#","").split("/")
  // var queryParams;
  // console.log("pathParams", pathParams);
  // if(pathParams[pathParams.length-1].includes("?")){
  //   var lastElem = pathParams[pathParams.length-1].split("?");
  //   pathParams[pathParams.length-1] = lastElem[0];
  //   queryParams = lastElem[1].split("&");
  //   console.log("pathParams", pathParams, "queryParams", queryParams);
  // }
  // if (sessionStorage.getItem('redirect_url') !== null) {
  //   sessionStorage.setItem('login_redirect', sessionStorage.getItem('redirect_url').replace('#', ''))
  //   let queryString = sessionStorage.getItem('redirect_url').replace('#', '')
  //   this.props.history.push(`/?redirect_url=${queryString}`)
  //     console.log("window.location.hash", window.location.hash)

  //       sessionStorage.setItem('redirect_url', window.location.hash)
  //       var a=window.location.hash;
  //       /* var array= '?logisticNo=LR-290620/162546733?3u874y7y4?6t3t36t3'; */
  //      var b =a.replace('#/','').split('?');
  //       console.log(b);
  //       console.log(b[b.length-1]);
  //       var c=b[1];
  //       console.log(c);
  //       var pathParam=c;
  //       // var pathParam=c.replace("redirect_url=/",'');
  //       sessionStorage.setItem("Redirect_url_path_params",pathParam);
  //       var queryParam=b.splice(2);
  //       sessionStorage.setItem("Redirect_url_query_params",queryParam);

  //       if (sessionStorage.getItem('redirect_url') !== null) {
  //         // let query =sessionStorage.getItem('Redirect_url_path_params');
  //         // this.props.history.push(`/?redirect_url=${query}`)
  //         sessionStorage.setItem('login_redirect', sessionStorage.getItem('redirect_url').replace('#', ''))
  //         let queryString = sessionStorage.getItem('redirect_url').replace('#', '')
  //         this.props.history.push(`/?redirect_url=${queryString}`)
  //       }
  //      else {
  //       if (sessionStorage.getItem('logout') == "true") {
  //         sessionStorage.clear();
  //         this.props.history.push('/');
  //       }
  //       if (sessionStorage.getItem('token') == null) {
  //         this.props.history.push('/');
  //       }
  //     }
  //   }
  // }

  componentDidMount() {
    if (sessionStorage.getItem('logout') == "true") {
      sessionStorage.clear();
      this.props.history.push('/');
    }
    // if (sessionStorage.getItem('token') == null) {
    //   this.props.history.push('/');
    // }
    this.setState({ modules: JSON.parse(sessionStorage.getItem('modules')) })
    document.addEventListener("keydown", this.escFunction, false);

    let token = sessionStorage.getItem('chatToken');
        console.log("chat token ---- "+token);
        if(token!=null && token!='' && token!=undefined) {
            $('body').loadChatIcon(token);
          }
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.escFunction, false);
  }

  escFunction = (event) => {
    if (event.keyCode === 27) {
      this.setState({ show: false, firstShow: false })
    }
  }

  componentWillReceiveProps(nextProps) {
    // if (sessionStorage.getItem('logout') == "true") {
    //   sessionStorage.clear();
    //   this.props.history.push('/');
    // }
    // if (sessionStorage.getItem('token') == null) {
    //   this.props.history.push('/');
    // }
  }

  _onAction() {

  }

  _onActive() {

  }

  _onIdle() {
    if (!this.state.expired) {
      this.inactivityPopupOpen();
    }
  }


  inactivityPopupOpen() {
    this.setState({
      inactivityPopup: true
    })
  }
  inactivityPopupClose(type) {
    if (type == 'auto') {
      sessionStorage.setItem('logout', true)
      this.setState({
        inactivityPopup: false,
        expired: true
      })
    } else {
      this.setState({
        inactivityPopup: false
      })
    }
  }

  isActiveSubLink(subLink) {
    return window.location.hash.split("/")[2] == subLink;
  }

  isActiveLink(menuLink) {
    return window.location.hash.split("/")[1] == `${menuLink}`;
  }
  isCustomActive(customLink) {
    return window.location.hash.split("/")[3] == `${customLink}`;
  }

  onShowHide() {
    this.setState({
      show: !this.state.show,
      firstShow: !this.state.firstShow
    });
  }

  renderShit(root) {
    if (Array.isArray(root) && root.length > 0 && root.length != undefined) {
      return (
        <ul className="collapse list" id={root[0].parentCode}>
          {
            root.map(node => (
              <React.Fragment>
                {sessionStorage.getItem('currentPage') == node.code && sessionStorage.setItem('parentName', node.name)}
                <li data-toggle="collapse" data-target={`#${node.code}`}><span className="colorBlue">{node.subModules != undefined && node.name}</span>{node.subModules != undefined && <i className="fa fa-angle-down arrow_down_icon" />}{node.subModules == undefined ? this.renderShit(node) : this.renderShit(node.subModules)}</li>
              </React.Fragment>
            ))
          }
        </ ul>
      );
    }
    else {
      return <span className="mli-inner-item" data-parent={root.parentCode} onClick={() => this.closeSideBar(root.parentCode, root.pageUrl.replace('#', ''))}>{root.name}</span>;
    }
  }
  closeSideBar(pageId, url) {
    if (url != window.location.hash.replace('#', '')) {
      this.props.history.push(url)
      sessionStorage.setItem('currentPage', pageId)
    }
    if (document.getElementById('sidebar').classList) {
      document.getElementById('sidebar').classList.remove('active')
    }
  }

  render() {
    let miid = JSON.parse(sessionStorage.getItem('mid'));
    let role = "";
    let data = JSON.parse(sessionStorage.getItem('roles'));
    if (data != null) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].id == miid) {
          role = data[i].name;
        }
      }
    }

    var Change_Setting = JSON.parse(sessionStorage.getItem('Change Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Change Setting')).crud);
    var Ad_Hoc_Request = JSON.parse(sessionStorage.getItem('Ad-Hoc Request')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Ad-Hoc Request')).crud);
    var Administration = JSON.parse(sessionStorage.getItem('Administration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Administration')).crud);
    var ARS = JSON.parse(sessionStorage.getItem('ARS')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ARS')).crud);
    var Add_New_Vendors = JSON.parse(sessionStorage.getItem('Add New Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Add New Vendors')).crud);
    var Allocation_Report = JSON.parse(sessionStorage.getItem('Allocation Report')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Allocation Report')).crud);
    var Analytics = JSON.parse(sessionStorage.getItem('Analytics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Analytics')).crud);
    var Assortment = JSON.parse(sessionStorage.getItem('Assortment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Assortment')).crud);
    var Custom_Masters = JSON.parse(sessionStorage.getItem('Custom Masters')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Custom Masters')).crud);
    var Data_Sync = JSON.parse(sessionStorage.getItem('Data Sync')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Data Sync')).crud);
    var Demand_Planning = JSON.parse(sessionStorage.getItem('Demand Planning')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Demand Planning')).crud);
    var Fast_Moving_Articles = JSON.parse(sessionStorage.getItem('Fast Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Fast Moving Articles')).crud);
    var History = JSON.parse(sessionStorage.getItem('History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('History')).crud);
    var Inventory_Classification = JSON.parse(sessionStorage.getItem('Inventory Classification')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Inventory Classification')).crud);
    var Inventory_Planning = JSON.parse(sessionStorage.getItem('Inventory Planning')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Inventory Planning')).crud);
    var Item = JSON.parse(sessionStorage.getItem('Item')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item')).crud);
    var Item_Configuration = JSON.parse(sessionStorage.getItem('Item Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Configuration')).crud);
    var Item_Mapping = JSON.parse(sessionStorage.getItem('Item Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Mapping')).crud);
    var Item_route = JSON.parse(sessionStorage.getItem('Item route')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item route')).crud);
    var Manage_Users = JSON.parse(sessionStorage.getItem('Manage Users')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Users')).crud);
    var Manage_Vendors = JSON.parse(sessionStorage.getItem('Manage Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Vendors')).crud);
    var Moderate_Moving_Articles = JSON.parse(sessionStorage.getItem('Moderate Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Moderate Moving Articles')).crud);
    var Monthly = JSON.parse(sessionStorage.getItem('Monthly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly')).crud);
    var New_Articles = JSON.parse(sessionStorage.getItem('New Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('New Articles')).crud);
    var Organization = JSON.parse(sessionStorage.getItem('Organization')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Organization')).crud);

    var Priority_List = JSON.parse(sessionStorage.getItem('Priority List')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Priority List')).crud);

    var Replenishment = JSON.parse(sessionStorage.getItem('Replenishment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Replenishment')).crud);
    var Roles = JSON.parse(sessionStorage.getItem('Roles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Roles')).crud);
    var Sales_Achieved = JSON.parse(sessionStorage.getItem('Sales Achieved')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Sales Achieved')).crud);
    var Schedulers = JSON.parse(sessionStorage.getItem('Schedulers')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Schedulers')).crud);
    var Sell_Throughs = JSON.parse(sessionStorage.getItem('Sell Throughs')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Sell Throughs')).crud);
    var Site = JSON.parse(sessionStorage.getItem('Site')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site')).crud);
    var Site_Mapping = JSON.parse(sessionStorage.getItem('Site Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site Mapping')).crud);
    var Stale_Moving_Articles = JSON.parse(sessionStorage.getItem('Stale Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Stale Moving Articles')).crud);
    var Store_Profiling = JSON.parse(sessionStorage.getItem('Store Profiling')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Store Profiling')).crud);
    var Store_Ranking = JSON.parse(sessionStorage.getItem('Store Ranking')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Store Ranking')).crud);
    var Summary = JSON.parse(sessionStorage.getItem('Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Summary')).crud);
    var Vendor_Management = JSON.parse(sessionStorage.getItem('Vendor Management')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Vendor Management')).crud);
    var Auto_Configuration = JSON.parse(sessionStorage.getItem('Auto Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Auto Configuration')).crud);
    var Run_on_Demand = JSON.parse(sessionStorage.getItem('Run on Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Run on Demand')).crud);
    var Weekly = JSON.parse(sessionStorage.getItem('Weekly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly')).crud);
    var Retail = JSON.parse(sessionStorage.getItem('Retail Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Retail Master')).crud);
    var MBO = JSON.parse(sessionStorage.getItem('MBO Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('MBO Master')).crud);
    var Budgeted_Sales = JSON.parse(sessionStorage.getItem('Budgeted Sales')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales')).crud);
    var Budgeted_Sales_History = JSON.parse(sessionStorage.getItem('Budgeted Sales History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales History')).crud);
    var Forecast_On_Demand = JSON.parse(sessionStorage.getItem('Forecast On Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast On Demand')).crud);
    var Weekly_History = JSON.parse(sessionStorage.getItem('Weekly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly History')).crud);
    var Monthly_History = JSON.parse(sessionStorage.getItem('Monthly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly History')).crud);
    var Forecast_History = JSON.parse(sessionStorage.getItem('Forecast History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast History')).crud);
    //procurement
    var Procurement = JSON.parse(sessionStorage.getItem('Procurement')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Procurement')).crud);
    var Purchase_Indent = JSON.parse(sessionStorage.getItem('Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Indent')).crud);
    var Purchase_Order = JSON.parse(sessionStorage.getItem('Purchase Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order')).crud);
    var Purchase_Order_With_Upload = JSON.parse(sessionStorage.getItem('Purchase Order File Upload')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order File Upload')).crud);
    var PI_History = JSON.parse(sessionStorage.getItem('PI History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PI History')).crud);
    var PO_History = JSON.parse(sessionStorage.getItem('PO History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PO History')).crud);
    var Item_UDF_Mapping = JSON.parse(sessionStorage.getItem('Item UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Mapping')).crud)
    var Item_UDF_Setting = JSON.parse(sessionStorage.getItem('Item UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Setting')).crud)
    var UDF_Mapping = JSON.parse(sessionStorage.getItem('UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Mapping')).crud)
    var UDF_Setting = JSON.parse(sessionStorage.getItem('UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Setting')).crud)
    var Dept_Size_Mapping = JSON.parse(sessionStorage.getItem('Dept Size Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Dept Size Mapping')).crud)
    var Configuration = JSON.parse(sessionStorage.getItem('Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Configuration')).crud)
    var Manage_Rule_Engine = JSON.parse(sessionStorage.getItem('Manage Rule Engine')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Rule Engine')).crud)

    var Rule_Engine_Configuration = JSON.parse(sessionStorage.getItem('Rule Engine Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Rule Engine Configuration')).crud)
    var Role_Master = JSON.parse(sessionStorage.getItem('Role Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Role Master')).crud)
    var Add_Role = JSON.parse(sessionStorage.getItem('Add Role')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Add Role')).crud)
    var Manage_Role = JSON.parse(sessionStorage.getItem('Manage Role')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Role')).crud)
    var File_Upload_History = JSON.parse(sessionStorage.getItem('File Upload History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('File Upload History')).crud)
    // ANALYTICS
    var FMCG_Master = JSON.parse(sessionStorage.getItem('FMCG Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('FMCG Master')).crud)
    var UPLOAD_MASTER = JSON.parse(sessionStorage.getItem('Upload Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Upload Master')).crud)
    var manage_rule = JSON.parse(sessionStorage.getItem('Manage Rule Engines')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Rule Engines')).crud)
    var Event_Master = JSON.parse(sessionStorage.getItem('Event Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Event Master')).crud)

    var Purchase_Orders = JSON.parse(sessionStorage.getItem('Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Orders')).crud)
    var Purchase_Orders_History = JSON.parse(sessionStorage.getItem('Purchase Order History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order History')).crud)

    var Cancelled_Order = JSON.parse(sessionStorage.getItem('Cancelled Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled Orders')).crud)
    var Pending_Orders = JSON.parse(sessionStorage.getItem('Pending Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Pending Orders')).crud)

    var Asn_Under_Approval = JSON.parse(sessionStorage.getItem('ASN Under Approval')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ASN Under Approval')).crud)
    var Approved_Asn = JSON.parse(sessionStorage.getItem('Approved ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Approved ASN')).crud)
    var Cancelled_Asn = JSON.parse(sessionStorage.getItem('Cancelled ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled ASN')).crud)
    var Lr_Processing = JSON.parse(sessionStorage.getItem('LR Processing')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('LR Processing')).crud)
    var Goods_Intransit = JSON.parse(sessionStorage.getItem('Goods Intransit')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Intransit')).crud)
    var Goods_Delivered = JSON.parse(sessionStorage.getItem('Goods Delivered')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Delivered')).crud)
    var Logistics = JSON.parse(sessionStorage.getItem('Logistics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Logistics')).crud)
    var Order = JSON.parse(sessionStorage.getItem('Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Order')).crud)
    var Shipment = JSON.parse(sessionStorage.getItem('Shipment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment')).crud)
    var Shipment_Tracking = JSON.parse(sessionStorage.getItem('Shipment Tracking')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment Tracking')).crud)
    var Processed_Orders = JSON.parse(sessionStorage.getItem('Processed Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Processed Orders')).crud)
    var Generics_Purchase_Orders = JSON.parse(sessionStorage.getItem('Generics Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Generics Purchase Orders')).crud)
    var Generics_Purchase_Indent = JSON.parse(sessionStorage.getItem('Generics Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Generics Purchase Indent')).crud)



    return (
      < div >

        <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={1000 * 10800} />
        {this.state.inactivityPopup ? <SessionTimeOut inactivityPopupClose={(e) => this.inactivityPopupClose(e)} /> : null}
        {sessionStorage.getItem('logout') == "true" ? <SessionExpired {...this.props} /> : null}
        <Header {...this.props} show={this.state.show} showHide={() => this.onShowHide()} />
        {/* { */}
        {/* // !this.state.firstShow ? null : */}
        <nav
          className="sidemenu newSidebar2 sidebarHover"
          // {
          //   this.state.show ? "sidemenu  menuShow" : "sidemenu "
          // }
          id="sidebar"
        >
          <div className="sidemenu_header sideBarMenuList ">
            <ul>
              <li>
                {/* <div className="img_border sideBarProfileImg">
                  {sessionStorage.getItem("profile") != null ? <img className="profile_img" src={sessionStorage.getItem("profile")} /> : <img className="profile_img" src={profile} />}
                </div> */}
                <div className="userImage userImgEditProfile sidebarImg marginAuto">
                  <span className="firstLetter">
                    {sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName').charAt(0)}
                  </span>
                </div>
              </li>
              <li>
                <p>{sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName')} {sessionStorage.getItem('lastName') == null ? "" : sessionStorage.getItem('lastName')}</p>
              </li>
              <li>
                <p>{sessionStorage.getItem('email')}</p>
              </li>
              <li>
                <p>{role}</p>
              </li>
            </ul>
          </div>
          <ul className="generic-sidebar-container sidemenu_list m-p-0 sideBarMenuList">
            <li className="menu-list-item">
              <span id="home_toggle" className="menu-item-list-inner">
                <span className="mli-icon textCenter">
                  <svg
                    className="icons"
                    xmlns="http://www.w3.org/2000/svg"
                    width="22"
                    height="20"
                    viewBox="0 0 22 20"
                  >
                    <g fill="#6D6DC9" fillRule="nonzero">
                      <path d="M21.972 8.719a1.71 1.71 0 0 0-.646-1.148L12.342.514a2.181 2.181 0 0 0-2.7 0L.656 7.57a1.723 1.723 0 0 0-.29 2.417c.586.746 1.67.876 2.417.29l7.414-5.824a1.282 1.282 0 0 1 1.587 0l7.415 5.824a1.72 1.72 0 0 0 2.772-1.559" />
                      <path d="M18.575 10.843L11.83 5.584a1.363 1.363 0 0 0-1.678 0l-6.744 5.26a.45.45 0 0 0-.174.355V17.2c0 1.077.876 1.953 1.953 1.953h11.608a1.955 1.955 0 0 0 1.952-1.953V11.2a.45.45 0 0 0-.173-.356" />
                    </g>
                  </svg>
                </span>
                <span onClick={() => this.props.history.push('#/home')} className="mli-icon">Home</span></span>
            </li>
            {this.state.modules[sessionStorage.getItem('mid')] != undefined && this.state.modules[sessionStorage.getItem('mid')].map((_) => (
              <li className="menu-list-item"><li className="menu-item-list-inner" data-toggle="collapse" data-target={`#${_.code}`}><span className="mli-icon">{_.iconUrl === null ? <img src={require(`../assets/07.svg`)} /> : <img src={require(`../assets/${_.iconUrl}`)} />}</span><span className="mli-name">{_.name}{_.subModules != undefined && <i className="fa fa-angle-down arrow_down_icon" />}</span></li>
                {Array.isArray(_.subModules) && this.renderShit(_.subModules)}
              </li>
            ))}
          </ul>
          <footer>
            <div className="nav_footer sideBarMenu">
              <label>Supplymint version 1.0</label>
            </div>

          </footer>
        </nav>
        {/* } */}


      </div >
    );
  }
}

export default SideBar;
