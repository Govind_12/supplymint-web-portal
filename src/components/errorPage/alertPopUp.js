import React, { Component } from 'react';
import warningIcon from '../../assets/warning.svg';
import closePopUp from '../../assets/close-pop-up.svg';

export default class AlertPopUp extends Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
    }
    componentDidMount() {

        this.textInput.current.focus();
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.props.closeErrorRequest()
        }
    }
    render() {
        console.log("Hey there");
        return (
            <div className="alertPopUp">
                <div className="modal-main">
                    <div className="swing">

                        <img src={closePopUp} className="closeimg" onKeyPress={this._handleKeyPress} onClick={(e) => this.props.closeErrorRequest(e)} />
                    </div>
                    <div className="modal error">
                        <div className="heading displayFlex">
                            <input type="hidden" ref={this.textInput} />
                            <h3 className="displayInline">Alert Notification</h3><img src={warningIcon} />
                        </div>
                        <p>{this.props.errorMessage}</p>
                        {/*<label>wait time : <span>02 mins</span></label>*/}
                    </div>
                </div>
            </div>
        )
    }
}
