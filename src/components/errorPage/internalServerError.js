import React from 'react';
import server from "../../assets/server.svg";

class InternalServerError extends React.Component {

    render() {
        return (
            <div className="container-fluid pad-0 internalServerErrorMain">
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                    <div className="errorPage">
                    <button  type="button" onClick={(e)=>this.props.closeErrorRequest(e)}  className="backButtonErrorpage">Back</button>

                        <div className="errorInternal">
                            <h1>
                                500
                            </h1>

                        </div>
                        <div className="contentError">
                            <h2>
                                INTERNAL SERVER ERROR
                            </h2>
                            <p className="internalError">{this.props.errorCode}</p>
                            <p className="internalError">{this.props.errorMessage}</p>
                            <p className="topEror">
                                The server encountered an internal error or misconfiguration and was <br />
                                unable to complete your request
                            </p>
                            <p className="bottonEror">
                                Please Contact the server administrator <a>help@supplymint.com</a>  to inform them of the time this error occurred and <br />
                                the actions your performed just before this error
                            </p>
                        </div>
                        <div className="footImg">
                            <img src={server} />
                            <div className="errorHeader0">
                                {/* <button className="backbuttonError">
                                    BACK TO HOME
                                </button> */}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        );
    }
}

export default InternalServerError;