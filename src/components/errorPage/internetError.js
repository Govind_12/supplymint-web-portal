import React from 'react';
import graphicImg from "../../assets/graphic.svg";
import errorBackground from "../../assets/background@3x.jpg";
class InternetError extends React.Component {

    render() {
        return (

            <div className="container-fluid pad-0 noInternetError">
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                    <div className="errorPage">
                    <button  type="button" onClick={(e)=>this.props.closeErrorRequest(e)}  className="backButtonErrorpage">Back</button>
                        <div className="errorhead">
                            <h2>
                             Your connection was interrupted
                            </h2>
                        </div>

                        <div className="contentErorConnection">
                            <h3>
                                Unable to connect to the server or check your internet connection
                            </h3>
                            <p>
                                Try checking the server or network cable or router or reconnecting to wifi
                            </p>
                        </div>
                        <div className="footerConnection">
                            {/* <div className="connectionImg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                    <g fill="#4A4A4A" fillRule="evenodd">
                                        <path fillRule="nonzero" d="M12.429 24.857C5.575 24.857 0 19.282 0 12.43 0 5.575 5.575 0 12.429 0c6.853 0 12.428 5.575 12.428 12.429 0 6.853-5.575 12.428-12.428 12.428zm0-23.821c-6.282 0-11.393 5.111-11.393 11.393 0 6.28 5.111 11.392 11.393 11.392 6.28 0 11.392-5.112 11.392-11.392 0-6.282-5.112-11.393-11.392-11.393z" />
                                        <path d="M12.429 20.196a.519.519 0 0 1 0-1.035 6.74 6.74 0 0 0 6.732-6.732 6.74 6.74 0 0 0-6.732-6.733 6.74 6.74 0 0 0-6.733 6.733.518.518 0 0 1-1.035 0c0-4.284 3.484-7.768 7.768-7.768 4.283 0 7.767 3.484 7.767 7.768 0 4.283-3.484 7.767-7.767 7.767z" />
                                        <path d="M5.179 13.982a.516.516 0 0 1-.365-.15l-2.092-2.071a.519.519 0 0 1 .73-.737l1.727 1.711 1.73-1.71a.517.517 0 1 1 .727.736l-2.093 2.071a.516.516 0 0 1-.364.15z" />
                                    </g>
                                </svg>
                                <p>
                                    REFRESH
                                </p>
                            </div> */}
                        </div>
                        <div className="errroContectionImg">

                            <img src={graphicImg} className="firstImg" />
                            <img src={errorBackground} className="footerBackgroundImg" />
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default InternetError;