import React from 'react';
import server from "../../assets/server.svg";

class UnauthorisedAccess extends React.Component {

    render() {
        return (
            <div className="container-fluid pad-0 noInternetError">
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                    <div className="errorPage">
                    <button className="backButtonErrorpage"  type="button" onClick={(e)=>this.props.closeErrorRequest(e)}  >Back</button>

                        <div className="errorInternal">
                            <h1>
                                401
                            </h1>

                        </div>
                        <p>{this.props.errorCode}</p>
                            <p>{this.props.errorMessage}</p>
                        <div className="contentError">
                            <h2>
                                UNAUTHORISED ACCESS
                            </h2>
                            <p className="topEror">
                                Access is denied due to invalid credentials
                            </p>
                            <p className="bottonEror">
                                You do not have permission to view this directory or page using the credentials that you supplied
                            </p>
                        </div>
                        <div className="footImg">
                            <img src={server} />
                            <div className="errorHeader0">
                                {/* <button className="backbuttonError">
                                    BACK TO HOME
                                </button> */}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        );
    }
}

export default UnauthorisedAccess;