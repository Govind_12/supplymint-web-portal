import React, { Component } from 'react';

export default class SearchPages extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className="gsmi-pages-list">
                <ul className="gsmipl-inner">
                    {this.props.searchPages.map(_ =>
                        <li id={_.url} onClick={() => this.props.history.push(_.url)}>
                            {/* <Link> */}
                            <div className="gsmipli-item">
                                <span className="gsmip-list">{_.name}</span>
                                <span className="gsmip-module-name">{_.path}</span>
                                {/*<img src={require('../../assets/rightArrow2.svg')} /><span className="gsmip-module-name">Configuration</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-page-name">General Settings</span>*/}
                            </div>
                            {/* </Link> */}
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}