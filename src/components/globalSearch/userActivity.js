import React, { Component } from 'react';

export default class UserActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activities: []
        }
    }
    // componentDidMount() {
    //     let payload = {
    //         "pageNo": "1",
    //         "type": "1",
    //         "search": "",
    //         "dataValue": "",
    //         "message": "",
    //         "userId": "",
    //         "userName": ""
    //     }
    //     this.props.userActivityRequest(payload)
    // }
    // static getDerivedStateFromProps(props, state) {
    //     if (props.home.userActivity.isSuccess) {
    //         return {
    //             activities: props.home.userActivity.data.resource
    //         }
    //     }
    // }
    render() {
        console.log(this.props.userActivities);
        return (
            <div className="gsmi-user-activities">
                <ul className="gsmiua-inner">
                    {this.props.userActivities.map(_ => (
                        <li className="dot">
                            <div className="gsmiuai-item">
                                <div className="gsmiuai-left">
                                    <span className="gsmiuai-action">{_.message}</span>
                                    <span className="gsmiuai-createdby">Created by {_.firstName} {_.lastName}</span>
                                    <span className="gsmiuai-mailid">{_.email}</span>
                                </div>
                                <div className="gsmiuai-right">
                                    <span className="gsmiuai-time">{_.actionTimeValue}</span>
                                    <button type="button" className="gsmiuair-btn">{_.module}</button>
                                </div>
                            </div>
                        </li>
                    ))}
                    {/* <li>
                        <div className="gsmiuai-item">
                            <div className="gsmiuai-left">
                                <span className="gsmiuail-img"><img src={require('../../assets/new-purchase-order.svg')} /></span>
                                <span className="gsmiuai-action">New Purchase Order Created</span>
                                <span className="gsmiuai-createdby">Created by Anish Rana</span>
                                <span className="gsmiuai-mailid">anish@turingcloud.com</span>
                            </div>
                            <div className="gsmiuai-right">
                                <span className="gsmiuai-time">2 hour ago</span>
                                <button type="button" className="gsmiuair-btn">Open PO</button>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="gsmiuai-item">
                            <div className="gsmiuai-left">
                                <span className="gsmiuail-img"><img src={require('../../assets/new-purchase-order-po.svg')} /></span>
                                <span className="gsmiuai-action">#PO000609-0620BSP	Order has been cancelled by Anish Rana</span>
                                <span className="gsmiuai-createdby">Cancelled by Anish Rana</span>
                                <span className="gsmiuai-mailid">anish@turingcloud.com</span>
                            </div>
                            <div className="gsmiuai-right">
                                <span className="gsmiuai-time">2 hour ago</span>
                                <button type="button" className="gsmiuair-btn">Open PO</button>
                            </div>
                        </div>
                    </li>
                    <li className="dot">
                        <div className="gsmiuai-item">
                            <div className="gsmiuai-left">
                                <span className="gsmiuai-action">UDF setting updated</span>
                                <span className="gsmiuai-createdby">Cancelled by Anish Rana</span>
                                <span className="gsmiuai-mailid">anish@turingcloud.com</span>
                            </div>
                            <div className="gsmiuai-right">
                                <span className="gsmiuai-time">2 hour ago</span>
                                <div className="gsmiuair-button">
                                    <button type="button" className="gsmiuair-mod-btn">Digiproc</button>
                                    <button type="button" className="gsmiuair-btn">UDF Setting</button>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li className="dot">
                        <div className="gsmiuai-item">
                            <div className="gsmiuai-left">
                                <span className="gsmiuai-action">Purchase Order Created</span>
                                <span className="gsmiuai-createdby">Cancelled by Anish Rana</span>
                                <span className="gsmiuai-mailid">anish@turingcloud.com</span>
                            </div>
                            <div className="gsmiuai-right">
                                <span className="gsmiuai-time">2 hour ago</span>
                                <div className="gsmiuair-button">
                                    <button type="button" className="gsmiuair-mod-btn">Digivend</button>
                                    <button type="button" className="gsmiuair-btn">Open PO</button>
                                </div>
                            </div>
                        </div>
                    </li> */}
                </ul>
            </div>
        )
    }
}