import React, { Component } from 'react';

export default class SearchPages extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDownloadItem: false
        };
    }

    openDownloadItem(e) {
        e.preventDefault();
        this.setState({
            showDownloadItem: !this.state.showDownloadItem
        }, () => document.addEventListener('click', this.closeDownloadItem));
    }
    closeDownloadItem = () => {
        this.setState({ showDownloadItem: false }, () => {
            document.removeEventListener('click', this.closeDownloadItem);
        });
    }

    render() {
        return (
            <React.Fragment>
                {!this.props.showFilters ?
                <div className="gsmi-pages-list">
                    <ul className="gsmipl-inner gsmipl-data-tab">
                        <li className="active">
                            <div className="gsmipli-data">
                                <span className="gsmip-list">ASN/67826378/20-21</span>
                                <span className="gsmip-module-name">Digivend</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Shipment</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Pending ASN Approval</span>
                            </div>
                            <div className="gsmipli-right">
                                <div className="gsmiplir-buttons gsmiplirb-bright">
                                    <button className="gsmiplirb-viewcomments">All Comments</button>
                                </div>
                                <div className="gsmiplir-buttons">
                                    <button className="gsmiplirb-downitems" onClick={(e) => this.openDownloadItem(e)}>Download Files <img src={require('../../assets/downArrowNew.svg')} /></button>
                                    {this.state.showDownloadItem && 
                                    <div className="gsmihs-dropdown">
                                        <ul className="gsmihsdd-inner">
                                            <li>Invoice</li>
                                            <li>Barcode</li>
                                            <li>Packaging List</li>
                                            <li>PO Pdf</li>
                                            <li>QC</li>
                                        </ul>
                                    </div>}
                                </div>
                                <div className="gsmiplir-buttons gsmiplirb-bleft">
                                    <button className="gsmiplirb-tracker"><img src={require('../../assets/tracking.svg')} />Track Shipment</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="gsmipli-data">
                                <span className="gsmip-list">ASN/67826378/20-21</span>
                                <span className="gsmip-module-name">Digivend</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Shipment</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Pending ASN Approval</span>
                            </div>
                            <div className="gsmipli-right">
                                <div className="gsmiplir-buttons gsmiplirb-bright">
                                    <button className="gsmiplirb-viewcomments">All Comments</button>
                                </div>
                                <div className="gsmiplir-buttons">
                                    <button className="gsmiplirb-downitems">Download Files <img src={require('../../assets/downArrowNew.svg')} /></button>
                                </div>
                                <div className="gsmiplir-buttons gsmiplirb-bleft">
                                    <button className="gsmiplirb-tracker"><img src={require('../../assets/tracking.svg')} />Track Shipment</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="gsmipli-data">
                                <span className="gsmip-list">ASN/67826378/20-21</span>
                                <span className="gsmip-module-name">Digivend</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Shipment</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Pending ASN Approval</span>
                            </div>
                            <div className="gsmipli-right">
                                <div className="gsmiplir-buttons gsmiplirb-bright">
                                    <button className="gsmiplirb-viewcomments">All Comments</button>
                                </div>
                                <div className="gsmiplir-buttons">
                                    <button className="gsmiplirb-downitems">Download Files <img src={require('../../assets/downArrowNew.svg')} /></button>
                                </div>
                                <div className="gsmiplir-buttons gsmiplirb-bleft">
                                    <button className="gsmiplirb-tracker"><img src={require('../../assets/tracking.svg')} />Track Shipment</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="gsmipli-data">
                                <span className="gsmip-list">ASN/67826378/20-21</span>
                                <span className="gsmip-module-name">Digivend</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Shipment</span><img src={require('../../assets/rightArrow2.svg')} />
                                <span className="gsmip-module-name">Pending ASN Approval</span>
                            </div>
                            <div className="gsmipli-right">
                                <div className="gsmiplir-buttons gsmiplirb-bright">
                                    <button className="gsmiplirb-viewcomments">All Comments</button>
                                </div>
                                <div className="gsmiplir-buttons">
                                    <button className="gsmiplirb-downitems">Download Files <img src={require('../../assets/downArrowNew.svg')} /></button>
                                </div>
                                <div className="gsmiplir-buttons gsmiplirb-bleft">
                                    <button className="gsmiplirb-tracker"><img src={require('../../assets/tracking.svg')} />Track Shipment</button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div> :
                <div className="gsmi-filters">
                    <div className="gsmif-applyied-filter">
                        <h3>Add Filters</h3>
                        <div className="show-applied-filter">
                            <button className="saf-clear-all">Clear Filter</button>
                            <button className="saf-btn">PO Number <img src={require('../../assets/clearSearch.svg')} /></button>
                            <button className="saf-btn">PO Qty <img src={require('../../assets/clearSearch.svg')} /></button>
                            <button className="saf-btn">PO Amount <img src={require('../../assets/clearSearch.svg')} /></button>
                        </div>
                    </div>
                    <div className="gsmif-data">
                        <div className="gsmifd-col">
                            <label>Sample Label</label>
                            <input type="text" value="Sample Data" />
                        </div>
                        <div className="gsmifd-col">
                            <label>Sample Label</label>
                            <input type="text" />
                        </div>
                    </div>
                    <div className="gsmif-apply-filter-button">
                        <button type="button" className="gsmifafb-apply">Apply Filter</button>
                        <button type="button" className="gsmifafb-cancel">Cancel</button>
                    </div>
                </div>}
            </React.Fragment>
        )
    }
}