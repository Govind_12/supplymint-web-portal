import React from 'react';
import timer from '../../assets/timer.svg';

class SessionExpiredModal extends React.Component {
  
    clearFun(){
        sessionStorage.clear();
        this.props.history.push('/')
    }
    render() {
        
        return (
            <div className="modal  display_block sessionZindex" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain sessionExpireModalMain pad-0">
                    <div className="col-md-12 pad-0 m-top-30">
                            <div className="col-md-12 textCenter">
                                    <img className="timerImg" src={timer} />
                                    <h3>Session Expired</h3>
                            </div>  
                            <div className="col-md-12 continueSession textCenter m-top-15">
                                <h4>You were signed out of your account. Please click 'Reload' to sign in to Supplymint again.</h4>
                                <button onClick={(e) => this.clearFun()} type="button">Reload</button>
                            </div>                      
                    </div>   
                </div>  
      
            </div>
        </div>

            )
        }
    }
    
export default SessionExpiredModal;