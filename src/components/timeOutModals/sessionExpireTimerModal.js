import React from 'react';
import timer from '../../assets/timer.svg';

class SessionExpireTimeModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          show: 59,
        };
    }

    componentDidMount(){
        this.count = setInterval(() => {
            this.setState({
                show: this.state.show - 1
            })
            if(this.state.show == 0){
                this.onClose('auto')
            }
        },1000)
    }

    onClose(type){
        if(type == 'manual'){
            clearInterval(this.count);
            this.props.inactivityPopupClose('manual');
        }else{
            clearInterval(this.count);
            this.props.inactivityPopupClose('auto');
        }
    }


    render() {  
        
        return (
            <div className="modal  display_block sessionZindex" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain sessionExpireModalMain pad-0">
                    <div className="col-md-12 pad-0 m-top-30">                                                
                            <div className="col-md-12 textCenter">
                                    <img className="timerImg" src={timer} />
                                    <h3>Your Session is going to expire in</h3>
                                    <div className="timeRemain m-top-15">00:{this.state.show}<span>min </span></div>
                            </div>  
                            <div className="col-md-12 continueSession textCenter m-top-15">
                                <h4>Please Click Continue To Keep Working</h4>
                                <button onClick={(e) => this.onClose('manual')} type="button">Continue</button>
                            </div> 
                    </div> 
                </div>  
            </div>
        </div>

            )
        }
    }
    
export default SessionExpireTimeModal;