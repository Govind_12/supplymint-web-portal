import React from "react";
import AddSalesAgent from "./ManagesalesAgent/addSalesAgent";
import EmptyBox from "./ManagesalesAgent/emptyBox";
import ViewSalesAgent from "./ManagesalesAgent/viewSalesAgent";
import SalesAgentData from "../../json/salesAgentData.json";

class Contracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      salesAgentState: [],
      // addSalesAgent: false
    };
  }

  onAddSalesAgent() {
    // this.setState({
    //   addSalesAgent: true
    // });
    this.props.history.push('/vendor/contracts/addVendor')

  }

  onSaveSalesAgent() {
    // this.setState({
      // addSalesAgent: false,
      // salesAgentState: SalesAgentData
    // });
    this.props.history.push('/vendor/contracts/viewVendor')
  }

  render() {
    
    const hash = window.location.hash.split("/")[3];
    
    return (
      <div className="container-fluid">
         { hash == "emptyBox" ?
             <EmptyBox
             {...this.props}
             salesAgentState = {this.state.salesAgentState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addSalesAgent={() => this.onAddSalesAgent()}
            />
           : hash == "viewSalesAgent" ? 
            <ViewSalesAgent
              SalesAgentData={SalesAgentData}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            
         : hash == "addSalesAgent" ?
          <AddSalesAgent
            {...this.props}
            saveSalesAgent={() => this.onSaveSalesAgent()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />: null} 
      </div>
    );
  }
}

export default Contracts;
