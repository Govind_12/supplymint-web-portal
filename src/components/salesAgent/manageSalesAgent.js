import React from "react";
import AddSalesAgent from "./ManagesalesAgent/addSalesAgent";
import EmptyBox from "./ManagesalesAgent/emptyBox";
import ViewSalesAgent from "./ManagesalesAgent/viewSalesAgent";
import SalesAgentData from "../../json/salesAgentData.json";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

class ManageSalesAgent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      salesAgentState: [],
      // addVendor: false
      page: 1,
      success: false,
      alert: false,
      loader: false,
      errorMessage: "",
      successMessage: "",
      emptyBox: false,
      viewSalesAgent: false,

      errorCode: "",
      code: "",
    };
  }


  // componentWillMount() {
  //   if (!this.props.vendor.getSalesAgent.isSuccess) {
  //     // this.props.vendorRequest(this.state.page);
  //   } else {
  //     this.setState({
  //       loader: false
  //     })
  //     if (this.props.vendor.getSalesAgent.data.resource == null) {
  //       this.setState({
  //         emptyBox: true,
  //         viewSalesAgent: false
  //       })
  //     } else {
  //       this.setState({
  //         salesAgentState: this.props.vendor.getSalesAgent.data.resource,
  //         emptyBox: false,
  //         viewSalesAgent: true
  //       })
  //     }
  //   }
  // }
  componentWillReceiveProps(nextProps) {
    // if (!nextProps.vendor.editSalesAgent.isLoading && !nextProps.vendor.editSalesAgent.isSuccess && !nextProps.vendor.editSalesAgent.isError) {
    //   this.setState({
    //     loader: false,

    //   })
    // }
    if (nextProps.vendor.getAllManageSalesAgent.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getAllManageSalesAgentClear()
      if (nextProps.vendor.getAllManageSalesAgent.data.resource == null) {
        this.setState({
          emptyBox: true
        })
      } else {
        this.setState({
          salesAgentState: nextProps.vendor.getAllManageSalesAgent.data.resource,
          viewSalesAgent: true
        })
      }
    } else if (nextProps.vendor.getAllManageSalesAgent.isError) {
      this.setState({
        alert: true,
        success: false,
        errorCode: nextProps.vendor.getAllManageSalesAgent.message.error == undefined ? undefined : nextProps.vendor.getAllManageSalesAgent.message.error.errorCode,
        errorMessage: nextProps.vendor.getAllManageSalesAgent.message.error == undefined ? undefined : nextProps.vendor.getAllManageSalesAgent.message.error.errorMessage,
        loader: false,

        code: nextProps.vendor.getAllManageSalesAgent.message.status,
      })
      this.props.getAllManageSalesAgentClear()
    } else if (nextProps.vendor.getAllManageSalesAgent.isLoading) {
      this.setState({
        loader: true
      })
    }

    // if (nextProps.vendor.deleteSalesAgent.isLoading) {
    //   this.setState({
    //     loader: true
    //   })
    // }
    // else if (nextProps.vendor.deleteSalesAgent.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.deleteSalesAgent.data.message,
    //     alert: false,
    //   })
    //   this.props.deleteSalesAgentRequest();
    // }
    // else if (nextProps.vendor.deleteSalesAgent.isError) {
    //   this.setState({
    //     alert: true,
    //     errorMessage: nextProps.vendor.deleteSalesAgent.error.errorMessage,
    //     success: false,
    //     loader: false
    //   })
    //   this.props.deleteSalesAgentRequest();
    // }

    // //edit  organization modal
    // if (nextProps.vendor.editSalesAgent.isLoading) {
    //   this.setState({
    //     loader: true
    //   })

    // }
    // else if (nextProps.vendor.editSalesAgent.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.editSalesAgent.data.message,
    //     alert: false,
    //   })
    //   this.props.editVendorRequest();

    // }
    // else if (nextProps.vendor.editSalesAgent.isError) {
    //   this.setState({
    //     alert: true,
    //     success: false,
    //     errorCode: nextProps.vendor.editSalesAgent.message.error == undefined ? undefined : nextProps.vendor.editSalesAgent.message.error.errorCode,
    //     errorMessage: nextProps.vendor.editSalesAgent.message.error == undefined ? undefined : nextProps.vendor.editSalesAgent.message.error.errorMessage,
    //     loader: false,

    //     code: nextProps.vendor.editSalesAgent.message.status,
    //   })
    //   this.props.editVendorRequest();
    // }

    // dynamic header
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getHeaderConfigClear()
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
      this.props.getHeaderConfigClear()
    }

    // if (nextProps.vendor.accessSalesAgentPortal.isSuccess) {
    //   this.setState({
    //     loader: false
    //   })
    //   // this.props.accessSalesAgentPortalClear()
    // } else if (nextProps.vendor.accessSalesAgentPortal.isError) {
    //   this.setState({
    //     loader: false,
    //     alert: true,
    //     code: nextProps.vendor.accessSalesAgentPortal.message.status,
    //     errorCode: nextProps.vendor.accessSalesAgentPortal.message.error == undefined ? "NA" : nextProps.vendor.accessSalesAgentPortal.message.error.code,
    //     errorMessage: nextProps.vendor.accessSalesAgentPortal.message.error == undefined ? "NA" : nextProps.vendor.accessSalesAgentPortal.message.error.message
    //   })
    //   //this.props.accessSalesAgentPortalClear()
    // }

    if (//nextProps.vendor.createSalesAgent.isLoading || 
      nextProps.replenishment.getHeaderConfig.isLoading
        //|| nextProps.vendor.accessSalesAgentPortal.isLoading
        ) {
      this.setState({
        loader: true
      })
    }
  }
  onAddSalesAgent() {
    // this.setState({
    //   addVendor: true
    // });
    this.props.history.push('/mdm/manageSalesAgents/addSalesAgent')
  }

  onSaveSalesAgent() {
    this.setState({
      // addVendor: false,
      salesAgentState: SalesAgentData
    });
    // this.props.history.push('/vendor/manageVendors/viewVendor')
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  render() {
    const hash = window.location.hash.split("/")[3];
    return (
      <div className="container-fluid pad-0">
        {/*{ this.state.emptyBox  ?
             <EmptyBox
             {...this.props}
             vendorState = {this.state.vendorState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addVendor={() => this.onAddVendor()}
            />
           : null}*/}

        {/*{ this.state.viewVendor ? */}
        <ViewSalesAgent
          SalesAgentData={this.state.salesAgentState}
          {...this.props}
          clickRightSideBar={() => this.props.rightSideBar()}
        />
        {/*: null}*/}

        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default ManageSalesAgent;
