import React from "react";
import SalesAgentFilter from "./salesAgentFilter";
import EditModal from "./editSalesAgentModal";
import refreshIcon from "../../../assets/refresh.svg";
import ToastLoader from '../../loaders/toastLoader';
import eyeIcon from "../../../assets/eyeIcon.svg";
import filterIcon from '../../../assets/headerFilter.svg';
import searchIcon from '../../../assets/clearSearch.svg';
import SearchImage from '../../../assets/searchicon.svg';
import Reload from '../../../assets/refresh-block.svg';
import AlreadyUser from '../../../assets/alreadyUser.svg';
import ColoumSetting from "../../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../../replenishment/confirmationReset";
import Pagination from "../../pagination";
import { CONFIG } from "../../../config/index";

export default class ViewSalesAgent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: false,
      filterBar: false,
      editModal: false,
      salesAgentData: [],
      edit: "",
      id: "",
      slCode: "",
      slCityName: "",
      slName: "",
      firstName: "",
      lastName: "",
      middleName: "",
      contactNumber: "",
      email: "",
      slAddr: "",
      search: "",
      toastLoader: false,
      toastMsg: "",
      gstNo: "",

      // Dyanmic Header
      coloumSetting: false,
      getHeaderConfig: [],
      fixedHeader: [],
      customHeaders: {},
      headerConfigState: {},
      headerConfigDataState: {},
      fixedHeaderData: [],
      customHeadersState: [],
      headerState: {},
      headerSummary: [],
      defaultHeaderMap: [],
      confirmModal: false,
      headerMsg: "",
      paraMsg: "",
      headerCondition: false,
      tableCustomHeader: [],
      tableGetHeaderConfig: [],
      saveState: [],
      // Header Filter
      filterKey: "",
      filterType: "",
      prevFilter: "",
      filterItems: {},
      checkedFilters: [],
      inputBoxEnable: false,
      tagState: false,
      filterValueForTag: [],
      filteredValue: [],
      // fromSlCode: "",
      // toSlCode : "",
      fromDueDays : "",
      toDueDays : "",
      fromDeliveryBuffDays : "",
      toDeliveryBuffDays : "",
      active: 0,

      //----- PAGINATION -----
      prev: '',
      current: 1,
      next: '',
      maxPage: 0,
      totalItems: 0,
      jumpPage: 1,
    };
  }
  componentDidMount() {
    let payload = {
      no: 1,
      type: 1,
      search: ""
    }
    this.props.getAllManageSalesAgentRequest(payload)

    let data = {
      enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
      attributeType: "TABLE HEADER",
      displayName: "MANAGE_SALESAGENTS_ALL",
      basedOn: "ALL"
    }
    this.props.getHeaderConfigRequest(data)
    document.addEventListener("keydown", this.escFunction, false);

    sessionStorage.setItem('currentPage', "MDMMAIN")
    sessionStorage.setItem('currentPageName', "Manage SalesAgents")
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.escFunction, false);
  }

  openEditModal(id) {
    let salesAgentData = this.state.salesAgentData
    for (let i = 0; i < salesAgentData.length; i++) {
      if (salesAgentData[i].id == id) {
        this.setState({
          firstName: salesAgentData[i].firstName,
          // lastName: salesAgentData[i].lastName,
          // middleName: salesAgentData[i].middleName,
          slCityName: salesAgentData[i].city,
          email: salesAgentData[i].email,
          contactNumber: salesAgentData[i].mobile,
          slCode: salesAgentData[i].slCode,
          slName: salesAgentData[i].slName,
          slAddr: salesAgentData[i].slAddr,
          gstNo: salesAgentData[i].gstInNo,
          active: salesAgentData[i].isActive
        })
      }
    }
    this.setState({
      editId: id,
      editModal: !this.state.editModal
    });
  }
  close() {
    this.setState({
      editModal: false
    })
  }
  openFilter(e) {
    e.preventDefault();
    this.setState({
      filter: true,
      filterBar: !this.state.filterBar
    });
  }
  onClearSearch(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: ""
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.salesAgentRequest(data);
  }


  onSearch = (e) => {
    if (e.target.value.trim().length) {
      if (e.target.value != "" && e.key == "Enter") {
        let payload = {
          no: this.state.current,
          type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
          search: e.target.value,
          status: "SHIPMENT_SHIPPED",
          filter: this.state.filteredValue,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
        }
        this.props.getAllManageSalesAgentRequest(payload)
        this.setState({
          type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true, filterBar: false,
          filter: false, editModal: false, coloumSetting: false
        })
      }
    }
    this.setState({ search: e.target.value }, () => {
      if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
        let payload = {
          no: this.state.current,
          type: this.state.type == 4 ? 2 : 1,
          search: "",
          filter: this.state.filteredValue,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
        }
        this.props.getAllManageSalesAgentRequest(payload)
        this.setState({
          search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false,
          filterBar: false, filter: false, editModal: false, coloumSetting: false
        })
      }
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.vendor.getAllManageSalesAgent.isSuccess) {
      this.setState({
        salesAgentData: nextProps.vendor.getAllManageSalesAgent.data.resource.genericitemMasterList,
        prev: nextProps.vendor.getAllManageSalesAgent.data.resource.prePage,
        current: nextProps.vendor.getAllManageSalesAgent.data.resource.currPage,
        next: nextProps.vendor.getAllManageSalesAgent.data.resource.currPage + 1,
        maxPage: nextProps.vendor.getAllManageSalesAgent.data.resource.maxPage,
        jumpPage: nextProps.vendor.getAllManageSalesAgent.data.resource.currPage,
        totalItems: nextProps.vendor.getAllManageSalesAgent.data.resource.totalRecord
      })
    } else if (nextProps.vendor.getAllManageSalesAgent.isErrror) {
      this.setState({
        salesAgentData: null
      })

    }
    // if (nextProps.vendor.editSalesAgent.isSuccess) {
    //   let payload = {
    //     type: 1,
    //     no: 1,
    //     search: ""
    //   }
    //   this.props.salesAgentRequest(payload)
    // }
    // Dynamic Header 
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "ALL") {
        let getHeaderConfig = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"])
        let fixedHeader = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"])
        let customHeadersState = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"])
        this.setState({
          filterItems: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
          loader: false,
          customHeaders: this.state.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
          getHeaderConfig,
          fixedHeader,
          customHeadersState,
          headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
          fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"],
          headerConfigDataState: { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] },
          headerSummary: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
          defaultHeaderMap: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
          mandateHeaderMain: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
        })
        setTimeout(() => {
          this.setState({
            headerCondition: this.state.customHeadersState.length == 0 ? true : false,
          })
        }, 1000);
      }
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
    }

    if (nextProps.replenishment.createHeaderConfig.isSuccess) {
      this.setState({
        successMessage: nextProps.replenishment.createHeaderConfig.data.message,
        loader: false,
        success: true,
      })
      if (nextProps.replenishment.createHeaderConfig.data.basedOn == "ALL") {
        let payload = {
          enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
          attributeType: "TABLE HEADER",
          displayName: "MANAGE_VENDOR_ALL",
          basedOn: "ALL"
        }
        this.props.getHeaderConfigRequest(payload)
      }
      this.props.createHeaderConfigClear()
    } else if (nextProps.replenishment.createHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.createHeaderConfig.message.status,
        errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
      })
      this.props.createHeaderConfigClear()
    }
    // if (nextProps.vendor.accessSalesAgentPortal.isSuccess) {
    //     if(nextProps.vendor.accessSalesAgentPortal.data.resource !== null && nextProps.vendor.accessSalesAgentPortal.data.resource.token !== null && 
    //       nextProps.vendor.accessSalesAgentPortal.data.resource.token !== undefined && nextProps.vendor.accessSalesAgentPortal.data.resource.token !== "")
    //     {
    //       this.close();
    //       let url = "/#/?accessSalesAgentPortalToken="+ nextProps.vendor.accessSalesAgentPortal.data.resource.token
    //       window.open(url);
    //     }
    //     this.props.accessSalesAgentPortalClear()
    // }
    // else if (nextProps.vendor.accessSalesAgentPortal.isError) {
    //   this.close();
    //   this.props.accessSalesAgentPortalClear()
    // }
  }

  page = (e) => {
    if (e.target.id == "prev") {
      if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
      } else {
        this.setState({
          prev: this.props.vendor.getAllManageSalesAgent.data.prePage,
          current: this.props.vendor.getAllManageSalesAgent.data.currPage,
          next: this.props.vendor.getAllManageSalesAgent.data.currPage + 1,
          maxPage: this.props.vendor.getAllManageSalesAgent.data.maxPage,
        })
        if (this.props.vendor.getAllManageSalesAgent.data.currPage != 0) {
          let data = {
            type: this.state.type,
            no: this.props.vendor.getAllManageSalesAgent.data.currPage - 1,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: this.state.filteredValue
          }
          this.props.getAllManageSalesAgentRequest(data);
        }
      }
    } else if (e.target.id == "next") {
      this.setState({
        prev: this.props.vendor.getAllManageSalesAgent.data.prePage,
        current: this.props.vendor.getAllManageSalesAgent.data.currPage,
        next: this.props.vendor.getAllManageSalesAgent.data.currPage + 1,
        maxPage: this.props.vendor.getAllManageSalesAgent.data.maxPage,
      })
      if (this.props.vendor.getAllManageSalesAgent.data.currPage != this.props.vendor.getAllManageSalesAgent.data.maxPage) {
        let data = {
          type: this.state.type,
          no: this.props.vendor.getAllManageSalesAgent.data.currPage + 1,
          search: this.state.search,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
          filter: this.state.filteredValue
        }
        this.props.getAllManageSalesAgentRequest(data)
      }
    }
    else if (e.target.id == "first") {
      if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
      }
      else {
        this.setState({
          prev: this.props.vendor.getAllManageSalesAgent.data.prePage,
          current: this.props.vendor.getAllManageSalesAgent.data.currPage,
          next: this.props.vendor.getAllManageSalesAgent.data.currPage + 1,
          maxPage: this.props.vendor.getAllManageSalesAgent.data.maxPage,
        })
        if (this.props.vendor.getAllManageSalesAgent.data.currPage <= this.props.vendor.getAllManageSalesAgent.data.maxPage) {
          let data = {
            type: this.state.type,
            no: 1,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: this.state.filteredValue
          }
          this.props.getAllManageSalesAgentRequest(data)
        }
      }
    } else if (e.target.id == "last") {
      if (this.state.current == this.state.maxPage || this.state.current == undefined) {
      }
      else {
        this.setState({
          prev: this.props.vendor.getAllManageSalesAgent.data.prePage,
          current: this.props.vendor.getAllManageSalesAgent.data.currPage,
          next: this.props.vendor.getAllManageSalesAgent.data.currPage + 1,
          maxPage: this.props.vendor.getAllManageSalesAgent.data.maxPage,
        })
        if (this.props.vendor.getAllManageSalesAgent.data.currPage <= this.props.vendor.getAllManageSalesAgent.data.maxPage) {
          let data = {
            type: this.state.type,
            no: this.props.vendor.getAllManageSalesAgent.data.maxPage,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: this.state.filteredValue
          }
          this.props.getAllManageSalesAgentRequest(data)
        }
      }
    }
  }

  getAnyPage = _ => {
    if (_.target.validity.valid) {
      this.setState({ jumpPage: _.target.value })
      if (_.key == "Enter" && _.target.value != this.state.current) {
        if (_.target.value != "") {
          let payload = {
            no: _.target.value,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.filteredValue,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
          }
          this.props.getAllManageSalesAgentRequest(payload)
        }
        else {
          this.setState({
            toastMsg: "Page number can not be empty!",
            toastLoader: true
          })
          setTimeout(() => {
            this.setState({
                toastLoader: false
            })
          }, 3000);
        }
      }
    }
  }

  onClearSearch(e) {
    if (this.state.type == 3 || this.state.type == 4) {
      let payload = {
        no: 1,
        type: this.state.type == 4 ? 2 : 1,
        search: "",
        filter: this.state.filteredValue,
        sortedBy: this.state.filterKey,
        sortedIn: this.state.filterType,
      }
      this.props.getAllManageSalesAgentRequest(payload)
      this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
    } else {
      this.setState({ search: "" })
    }
  }
  onRefresh() {
    let payload = {
      no: 1,
      type: 1,
      search: ""
    }
    this.props.getAllManageSalesAgentRequest(payload)
  }

  openColoumSetting(data) {
    if (this.state.customHeadersState.length == 0) {
      this.setState({
        headerCondition: true
      }, () => document.addEventListener('click', this.closeColumnSetting))
    }
    if (data == "true") {
      this.setState({
        coloumSetting: true
      }, () => document.addEventListener('click', this.closeColumnSetting))
    } else {
      this.setState({
        coloumSetting: false
      }, () => document.removeEventListener('click', this.closeColumnSetting))
    }
  }

  closeColumnSetting = (e) => {
    if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
      this.setState({ coloumSetting: false }, () =>
        document.removeEventListener('click', this.closeColumnSetting))
    }
  }

  pushColumnData(data) {
    let getHeaderConfig = this.state.getHeaderConfig
    let customHeadersState = this.state.customHeadersState
    let headerConfigDataState = this.state.headerConfigDataState
    let customHeaders = this.state.customHeaders
    let saveState = this.state.saveState
    if (this.state.headerCondition) {
      if (!data.includes(getHeaderConfig) || this.state.getHeaderConfig.length == 0) {
        getHeaderConfig.push(data)
        if (!getHeaderConfig.includes(headerConfigDataState)) {
          let keyget = (_.invert(headerConfigDataState))[data];
          Object.assign(customHeaders, { [keyget]: data })
          saveState.push(keyget)
        }
        if (!Object.keys(customHeaders).includes(this.state.defaultHeaderMap)) {
          let keygetvalue = (_.invert(headerConfigDataState))[data];
          this.state.defaultHeaderMap.push(keygetvalue)
        }
      }
    } else {
      if (!data.includes(customHeadersState) || this.state.customHeadersState.length == 0) {
        customHeadersState.push(data)
        if (!customHeadersState.includes(headerConfigDataState)) {
          let keyget = (_.invert(headerConfigDataState))[data];
          Object.assign(customHeaders, { [keyget]: data })
          saveState.push(keyget)
        }
        if (!Object.keys(customHeaders).includes(this.state.headerSummary)) {
          let keygetvalue = (_.invert(headerConfigDataState))[data];
          this.state.headerSummary.push(keygetvalue)
        }
      }
    }
    this.setState({
      getHeaderConfig,
      customHeadersState,
      customHeaders,
      saveState
    })
  }
  closeColumn(data) {
    let getHeaderConfig = this.state.getHeaderConfig
    let headerConfigState = this.state.headerConfigState
    let customHeaders = []
    let customHeadersState = this.state.customHeadersState
    if (!this.state.headerCondition) {
      for (let j = 0; j < customHeadersState.length; j++) {
        if (data == customHeadersState[j]) {
          customHeadersState.splice(j, 1)
        }
      }
      for (var key in headerConfigState) {
        if (!customHeadersState.includes(headerConfigState[key])) {
          customHeaders.push(key)
        }
      }
      if (this.state.customHeadersState.length == 0) {
        this.setState({
          headerCondition: false
        })
      }
    } else {
      for (var i = 0; i < getHeaderConfig.length; i++) {
        if (data == getHeaderConfig[i]) {
          getHeaderConfig.splice(i, 1)
        }
      }
      for (var key in headerConfigState) {
        if (!getHeaderConfig.includes(headerConfigState[key])) {
          customHeaders.push(key)
        }
      }
    }
    customHeaders.forEach(e => delete headerConfigState[e]);
    this.setState({
      getHeaderConfig,
      customHeaders: headerConfigState,
      customHeadersState,
    })
    setTimeout(() => {
      let keygetvalue = (_.invert(this.state.headerConfigDataState))[data];
      let saveState = this.state.saveState
      saveState.push(keygetvalue)
      let headerSummary = this.state.headerSummary
      let defaultHeaderMap = this.state.defaultHeaderMap
      if (!this.state.headerCondition) {
        for (let j = 0; j < headerSummary.length; j++) {
          if (keygetvalue == headerSummary[j]) {
            headerSummary.splice(j, 1)
          }
        }
      } else {
        for (let i = 0; i < defaultHeaderMap.length; i++) {
          if (keygetvalue == defaultHeaderMap[i]) {
            defaultHeaderMap.splice(i, 1)
          }
        }
      }
      this.setState({
        headerSummary,
        defaultHeaderMap,
        saveState
      })
    }, 100);
  }
  saveColumnSetting(e) {
    this.setState({
      coloumSetting: false,
      headerCondition: false,
      saveState: []
    })
    let payload = {
      module: "MANAGE_VENDOR",
      subModule: "VENDOR",
      section: "MANAGE-VENDOR",
      source: "WEB-APP",
      typeConfig: "PORTAL",
      attributeType: "TABLE HEADER",
      displayName: "MANAGE_VENDOR_ALL",
      basedOn: "ALL",
      fixedHeaders: this.state.fixedHeaderData,
      defaultHeaders: this.state.headerConfigDataState,
      customHeaders: this.state.customHeaders
    }
    this.props.createHeaderConfigRequest(payload)
  }
  resetColumnConfirmation() {
    this.setState({
      headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
      confirmModal: true,
    })
  }
  resetColumn() {
    let payload = {
      module: "MANAGE_VENDOR",
      subModule: "VENDOR",
      section: "MANAGE-VENDOR",
      source: "WEB-APP",
      typeConfig: "PORTAL",
      attributeType: "TABLE HEADER",
      displayName: "MANAGE_VENDOR_ALL",
      basedOn: "ALL",
      fixedHeaders: this.state.fixedHeaderData,
      defaultHeaders: this.state.headerConfigDataState,
      customHeaders: this.state.headerConfigDataState
    }
    this.props.createHeaderConfigRequest(payload)
    this.setState({
      headerCondition: true,
      coloumSetting: false,
      saveState: []
    })
  }
  closeConfirmModal(e) {
    this.setState({
      confirmModal: !this.state.confirmModal,
    })
  }

  filterHeader = (event) => {
    var data = event.target.dataset.key
    if (event.target.closest("th").classList.contains("rotate180"))
      event.target.closest("th").classList.remove("rotate180")
    else
      event.target.closest("th").classList.add("rotate180")

    var def = { ...this.state.headerConfigDataState };
    var filterKey = ""
    Object.keys(def).some(function (k) {
      if (def[k] == data) {
        filterKey = k
      }
    })
    if (this.state.prevFilter == data) {
      this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
    } else {
      this.setState({ filterKey, filterType: "ASC" })
    }
    this.setState({ prevFilter: data }, () => {
      let payload = {
        no: this.state.current,
        type: this.state.type,
        search: this.state.search,
        sortedBy: this.state.filterKey,
        sortedIn: this.state.filterType,
        filter: this.state.filteredValue,
      }
      this.props.getAllManageSalesAgentRequest(payload)
    })
  }

  openFilter(e) {
    e.preventDefault();
    this.setState({
      filter: !this.state.filter,
      filterBar: !this.state.filterBar
    });
  }

  closeFilter(e) {
    this.setState({
      filter: false,
      filterBar: false
    });
  }

  handleInputBoxEnable = (e, data) => {
    this.setState({ inputBoxEnable: true })
    this.handleCheckedItems(e, this.state.filterItems[data])
  }

  handleCheckedItems = (e, data) => {
    let array = [...this.state.checkedFilters]
    let len = Object.values(this.state.filterValueForTag).length > 0;
    if (this.state.checkedFilters.some((item) => item == data)) {
      array = array.filter((item) => item != data)
      delete this.state.filterValueForTag[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)]
      this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = "";
      this.setState({ [data]: "" })
      let flag = array.some(data => this.state[data] == "" || this.state[data] == undefined)
      if (!flag && len) {
        this.state.checkedFilters.some((item, index) => {
          if (item == data) {
            this.clearTag(e, index)
          }
        })
      }

    } else {
      array.push(data)
    }
    var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
    this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
  }

  handleInput = (event, filterName) => {
    if (event != null) {
      let id = event.target.id;
      let value = event.target.value;
      // if (event.target.id === "fromslCode"){
      //   this.setState({ fromSlCode: value }, () => {
      //       document.getElementById(id).setAttribute("value", this.state.fromSlCode);
      //       this.handleFromAndToValue(event)
      //   })
      // }
      // else if (event.target.id === "toslCode"){
      //   this.setState({ toSlCode: value }, () => {
      //       document.getElementById(id).setAttribute("value", this.state.toSlCode);
      //       this.handleFromAndToValue(event)
      //   })
      // }
      if (event.target.id === "fromdueDays") {
        this.setState({ fromDueDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.fromDueDays);
          this.handleFromAndToValue(event)
        })
      }
      else if (event.target.id === "todueDays") {
        this.setState({ toDueDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.toDueDays);
          this.handleFromAndToValue(event)
        })
      }
      else if (event.target.id === "fromdeliveryBuffDays") {
        this.setState({ fromDeliveryBuffDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.fromDeliveryBuffDays);
          this.handleFromAndToValue(event)
        })
      }
      else if (event.target.id === "todeliveryBuffDays") {
        this.setState({ toDeliveryBuffDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.toDeliveryBuffDays);
          this.handleFromAndToValue(event)
        })
      }
      else
        this.handleFromAndToValue(event);
    }
  }

  handleFromAndToValue = () => {
    var value = "";
    var name = event.target.dataset.value;

    // if (event.target.id === "fromslCode" || event.target.id === "toslCode")
    //     value = this.state.fromSlCode + " | " + this.state.toSlCode
    if (event.target.id === "fromdueDays" || event.target.id === "todueDays")
      value = this.state.fromDueDays + " | " + this.state.toDueDays
    else if (event.target.id === "fromdeliveryBuffDays" || event.target.id === "todeliveryBuffDays")
      value = this.state.fromDeliveryBuffDays + " | " + this.state.toDeliveryBuffDays
    else
      value = event.target.value

    if (/^\s/g.test(value)) {
      value = value.replace(/^\s+/, '');
    }

    this.setState({ [name]: value, applyFilter: true }, () => {
      if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
        this.setState({ applyFilter: false })
      } else {
        this.setState({ applyFilter: true })
      }
      this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
    })
  }

  submitFilter = () => {
    let payload = {}
    let filtervalues = {}
    this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))

    //reset min and max value field that are not checked::
    // if( payload.slCode === undefined ){
    //   this.setState({fromSlCode: "", toSlCode: "" })
    // }
    if (payload.dueDays === undefined) {
      this.setState({ fromDueDays: "", toDueDays: "" })
    }
    if (payload.deliveryBuffDays === undefined) {
      this.setState({ fromDeliveryBuffDays: "", toDeliveryBuffDays: "" })
    }

    Object.keys(payload).map((data) => (data.includes("dueDays") || data.includes("deliveryBuffDays"))
      && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() }))

    this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
    Object.keys(filtervalues).map((data) => (data.includes("dueDays") || data.includes("deliveryBuffDays"))
      && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim() }))

    let data = {
      no: this.state.current == 0 ? 1 : this.state.current,
      type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
      search: this.state.search,
      filter: payload,
      sortedBy: this.state.filterKey,
      sortedIn: this.state.filterType,
    }
    this.props.getAllManageSalesAgentRequest(data)

    this.setState({
      filter: false,
      filteredValue: payload,
      filterValueForTag: filtervalues,
      type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
      tagState: true
    })
  }

  clearFilter = () => {
    if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
      let payload = {
        no: this.state.current == 0 ? 1 : this.state.current,
        type: this.state.type == 4 ? 3 : 1,
        search: this.state.search,
        sortedBy: this.state.filterKey,
        sortedIn: this.state.filterType,
      }
      this.props.getAllManageSalesAgentRequest(payload)
    }
    this.setState({
      filteredValue: [],
      type: this.state.type == 4 ? 3 : 1,
      selectAll: false,
      filterValueForTag: [],
      // fromSlCode: "",
      // toSlCode: "",
      fromDueDays: "",
      toDueDays: "",
      fromDeliveryBuffDays: "",
      toDeliveryBuffDays: "",
    })
    this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
  }

  clearTag = (e, index) => {
    let deleteItem = this.state.checkedFilters;
    let deletedItem = this.state.checkedFilters[index];
    deleteItem.splice(index, 1)
    this.setState({
      checkedFilters: deleteItem,
      [deletedItem]: "",
    }, () => {
      if (this.state.checkedFilters.length == 0)
        this.clearFilter();
      else
        this.submitFilter();
    })
  }

  clearAllTag = (e) => {
    this.setState({
      checkedFilters: [],
    }, () => {
      this.clearFilter();
      this.clearFilterOutside();
    })
  }

  clearFilterOutside = () => {
    this.setState({
      filteredValue: [],
      selectAll: false,
      checkedFilters: []
    })
  }

  addSalesAgent = () => {
    this.props.history.push('/mdm/manageSalesAgents/addSalesAgent')
    this.props.updateCreatedSalesAgentRequest({})
  }
  updateSalesAgent = (salesAgentDetails) => {
    this.props.history.push('/mdm/manageSalesAgents/addSalesAgent')
    this.props.updateCreatedSalesAgentRequest(vendorDetails)
  }
  escFunction = (event) => {
    if (event.keyCode === 27) {
      this.setState({ filter: false, editModal: false, coloumSetting: false })
    }
  }

  //For Address Tooltip::
  small = (str) => {
    if (str != null) {
      if (str.length <= 30) {
        return false;
      }
      return true;
    }
  }

  render() {
    return (

      <div className="container-fluid pad-0">
        <div className="col-lg-12 pad-0 ">
          <div className="gen-vendor-potal-design p-lr-47">
            <div className="col-lg-6 pad-0">
              <div className="gvpd-left">
                <div className="gvpd-search">
                  <input type="search" onChange={this.onSearch} onKeyDown={this.onSearch} value={this.state.search} placeholder={this.state.search == "" ? "Type to Search" : this.state.search} className="search_bar" />
                  <img className="search-image" src={SearchImage} />
                  {this.state.search != "" && <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></span>}
                </div>
              </div>
            </div>
            <div className="col-lg-6 pad-0">
              <div className="gvpd-right">
                <div className="tooltip" onClick={this.addSalesAgent}>
                  <span className="add-btn"><img src={require('../../../assets/add-green.svg')} />
                    <span className="generic-tooltip">Add Sales Agent</span>
                  </span>
                </div>
                <div className="gvpd-filter">
                  <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                      <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                    </svg>
                    <span className="generic-tooltip">Filter</span>
                    {/* {this.state.filterCount != 0 && <p className="noOfFilter">{this.state.filterCount}</p>} */}
                  </button>
                  {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null}
                  {this.state.filter && <SalesAgentFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-lg-12 p-lr-47">
          {this.state.tagState ? this.state.checkedFilters.map((keys, index) => (
            <div className="show-applied-filter">
              {(Object.values(this.state.filterValueForTag)[index]) !== undefined && index === 0 ? <button type="button" className="saf-clear-all" onClick={(e) => this.clearAllTag(e)}>Clear All</button> : null}
              {(Object.values(this.state.filterValueForTag)[index]) !== undefined && <button type="button" className="saf-btn">{keys}
                <img onClick={(e) => this.clearTag(e, index)} src={require('../../../assets/clearSearch.svg')} />
                <span className="generic-tooltip">{typeof (Object.values(this.state.filterValueForTag)[index]) == 'object' ? Object.values(Object.values(this.state.filterValueForTag)[index]).join(',') : Object.values(this.state.filterValueForTag)[index]}</span>
              </button>}
            </div>)) : ''}
        </div>

        <div className="col-md-12 pad-0 p-lr-47">
          <div className="vendor-gen-table" >
            <div className="manage-table">
              {/* <ColoumSetting {...this.state} {...this.props} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} /> */}
              <table className="table gen-main-table">
                <thead>
                  <tr>
                    <th className="fix-action-btn">
                      <ul className="rab-refresh">
                        <li className="rab-rinner">
                          <span><img onClick={() => this.onRefresh()} src={Reload} /></span>
                        </li>
                      </ul>
                    </th>
                    {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                      <th key={key} data-key={data} onClick={this.filterHeader}>
                        <label data-key={data}>{data}</label>
                        <img src={filterIcon} className="imgHead" data-key={data} />
                      </th>
                    )) : this.state.customHeadersState.map((data, key) => (
                      <th key={key} data-key={data} onClick={this.filterHeader}>
                        <label data-key={data}>{data}</label>
                        <img src={filterIcon} className="imgHead" data-key={data} />
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {this.state.salesAgentData != null ? this.state.salesAgentData.map((data, key) => (
                    <tr key={key}>
                      <td className="fix-action-btn">
                        <ul className="table-item-list">
                          <li className="til-inner til-eye-btn">
                            <img src={eyeIcon} className="eye" onClick={(e) => this.openEditModal(data.id)} />
                            {/* <span className="generic-tooltip">Edit</span> */}
                          </li>
                          {/*<li className="til-inner til-already-user">
                              <img src={AlreadyUser} />
                              <span className="generic-tooltip">Already a user</span>
                            </li>*/}
                          <li className="til-inner til-edit-btn" onClick={() => this.updateSalesAgent(data)}>
                            {/* <img src={require("../../../assets/edit.svg")} className="height15" /> */}
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                              <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                              <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                              <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                              <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                            </svg>
                            <span className="generic-tooltip">Edit</span>
                          </li>
                        </ul>
                      </td>
                      {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => (
                        <td key={key} >
                          {<label >{data[hdata]}</label>}
                          {hdata === "slName" ? (data.isActive == "0" ? null :
                            <span className="td-already-user">
                              <img src={AlreadyUser} />
                              <span className="generic-tooltip">Already a user</span>
                            </span>
                          ) : null}
                          {(hdata === "shippedAddress" || hdata === "slAddress") && this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                        </td>
                      )) : this.state.headerSummary.map((sdata, keyy) => (
                        <td key={keyy} >
                          {<label className="table-td-text">{data[sdata]}</label>}
                          {sdata === "slName" ? (data.isActive == "0" ? null :
                            <span className="td-already-user">
                              <img src={AlreadyUser} />
                              <span className="generic-tooltip">Already a user</span>
                            </span>
                          ) : null}
                          {(sdata === "shippedAddress" || sdata === "slAddress") && this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                        </td>
                      ))}
                    </tr>
                  )) : <tr className="tableNoData"><td colSpan="100%"> <span className={"float_Left"}>NO DATA FOUND</span> </td></tr>}
                </tbody>
              </table>
            </div>
          </div>
          <div className="col-md-12 pad-0" >
            <div className="new-gen-pagination">
              <div className="ngp-left">
                <div className="table-page-no">
                  <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                  <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                </div>
              </div>
              <div className="ngp-right">
                <div className="nt-btn">
                  <Pagination {...this.state} {...this.props} page={this.page}
                    prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                </div>
              </div>
            </div>
          </div>
        </div>

        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
        {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
        {this.state.editModal ? <EditModal {...this.props} {...this.state} firstName={this.state.firstName} slAddr={this.state.slAddr} slCityName={this.state.slCityName} slCode={this.state.slCode} slName={this.state.slName} contactNumber={this.state.contactNumber} email={this.state.email} id={this.state.editId} closeEdit={(e) => this.openEditModal(e)} close={(e) => this.close(e)} /> : null}
      </div>
    )
  }
}