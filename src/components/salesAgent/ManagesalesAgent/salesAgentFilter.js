import React, { Component } from 'react';
import Close from '../../../assets/close-red.svg';
import Search from '../../../assets/searchicon.svg';
import Checkbox from '../../../assets/checkbox.svg';
import Recent from '../../../assets/recent.svg';
import Check from '../../../assets/check.svg';
import Delete from '../../../assets/delete.svg';
import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;
import moment from "moment";
export default class SalesAgentFilter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            recentAdded: false,
            searchPushedPo: "",
            updateFilterItem: this.props.filterItems,
            showRecentlyAddedButton: true,
        }
    }
    handleSearch = (event) => {
        this.setState({ searchPushedPo: event.target.value, updateFilterItem: "" })
        var filterSearch = [];

        Object.keys(this.props.filterItems).map(key => {
            var filterName = this.props.filterItems[key].toString();
            var searchValue = event.target.value.toString();
            if (filterName.toLowerCase().includes(searchValue.toLowerCase())) {
                filterSearch[key] = filterName;
            }
        });
        this.setState({ updateFilterItem: filterSearch });
    }
    closeRecentlyAdd = () => {
        this.setState({ recentAdded: false })
    }

    handleRecentlyAddedButton = (e) => {
        if (this.props.checkedFilters.length == 1) {
            this.setState({
                showRecentlyAddedButton: true
            })
        }
    }

    render() {
        const { searchPushedPo, showRecentlyAddedButton } = this.state
        return (
            <div className="asn-filter textLeft">
                <div className="backdrop-transparent"></div>
                <div className="asn-filter-inner">
                    <div className="asnf-head">
                        <div className="asnfh-top">
                            <span>Filter</span>
                            <span className="asnfht-input"><input type="text" value={searchPushedPo} data-value="searchPushedPo" onChange={this.handleSearch} placeholder="Type To Search"></input></span>
                            {/* <span className="asn-filter-search"><img src={Search}></img></span> */}
                            <span className="asn-filter-close displayPointer"><img src={Close} onClick={(e) => this.props.closeFilter(e)}></img></span>
                        </div>
                        {/* <div className="asnfh-bottom">
                            {showRecentlyAddedButton && <span onClick={() => this.props.checkedFilters.length != 0 && this.setState({ recentAdded: !this.state.recentAdded, showRecentlyAddedButton: false })}>Recently Added <img src={Recent}></img></span>}
                        </div> */}
                    </div>
                    <div className="asnf-body">
                        <div className="asnfb-head">
                            <span className="asnfb-left">Header</span><span className="asnfb-right">Value</span>
                        </div>
                        <div className="asnfb-body">

                            {Object.keys(this.state.updateFilterItem).map((data, key) => (<div key={key} className="asnfb-item">
                                {/* {console.log(data, this.props[this.props.filterItems[data]] == "", this.props[this.props.filterItems[data]])} */}
                                <span className="asnfbb-left">
                                    <span className="asn-check">
                                        <div className="checkSeperate checkBoxBorder">
                                            <label className="checkBoxLabel0"> <input type="checkBox" checked={this.props.checkedFilters.some((item) => item == this.props.filterItems[data])} onChange={(e) => this.props.handleCheckedItems(e, this.props.filterItems[data])} />{this.props.filterItems[data]}<span className="checkmark1"></span> </label>
                                            {/* <label className="checkBoxLabel0"> <input type="checkBox"/>{this.props.filterItems[data]}<span className="checkmark1"></span> </label> */}
                                        </div>
                                    </span>
                                </span>


                                {this.props.checkedFilters.includes(this.props.filterItems[data]) && this.props.inputBoxEnable
                                    ?
                                    data.includes("updatedOnDate") || data.includes("createdTime")
                                    ?
                                    <span className="asnfbb-right">
                                        {/* <DatePicker onChange={this.props.handleInput}/> */}
                                        <RangePicker format="DD-MM-YYY" showTime={{format:"HH:mm",defaultValue: [moment('00:00', 'HH:mm'), moment('00:00', 'HH:mm')] }} placeholder={[this.props[this.props.filterItems[data]] !== undefined && this.props[this.props.filterItems[data]] !== "" ? (this.props[this.props.filterItems[data]].split("|")[0].trim() !== "" ? moment(new Date(this.props[this.props.filterItems[data]].split("|")[0].trim())).format("DD-MM-YYYY") : "start date") : "start date", this.props[this.props.filterItems[data]] !== undefined  && this.props[this.props.filterItems[data]] !== "" ? (this.props[this.props.filterItems[data]].split("|")[1].trim() !== "" ? moment(new Date(this.props[this.props.filterItems[data]].split("|")[1].trim())).format("DD-MM-YYYY") : "end date") : "end date"]} onChange={(event) => this.props.handleInput(event, this.props.filterItems[data])} />
                                        {/* <input type="date" onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props[this.props.filterItems[data]] == undefined ? "" : this.props[this.props.filterItems[data]]} placeholder={this.props[this.props.filterItems[data]] == undefined || this.props[this.props.filterItems[data]] == "" ? "Select Date" : this.props[this.props.filterItems[data]]} /> */}
                                    </span>
                                    :
                                    (data.includes("createdTime") || data.includes("Date") || data.includes("date") || data.includes("validFrom") || data.includes("validTo")
                                        ? data.includes("Date") || data.includes("date")
                                            ? <span className="asnfbb-right">
                                                {/* <DatePicker onChange={this.props.handleInput}/> */}
                                                <RangePicker format="DD-MM-YYY" placeholder={[this.props[this.props.filterItems[data]] !== undefined && this.props[this.props.filterItems[data]] !== "" ? (this.props[this.props.filterItems[data]].split("|")[0].trim() !== "" ? moment(new Date(this.props[this.props.filterItems[data]].split("|")[0].trim())).format("DD-MM-YYYY") : "start date") : "start date", this.props[this.props.filterItems[data]] !== undefined  && this.props[this.props.filterItems[data]] !== "" ? (this.props[this.props.filterItems[data]].split("|")[1].trim() !== "" ? moment(new Date(this.props[this.props.filterItems[data]].split("|")[1].trim())).format("DD-MM-YYYY") : "end date") : "end date"]} onChange={(event) => this.props.handleInput(event, this.props.filterItems[data])} />
                                                {/* <input type="date" onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props[this.props.filterItems[data]] == undefined ? "" : this.props[this.props.filterItems[data]]} placeholder={this.props[this.props.filterItems[data]] == undefined || this.props[this.props.filterItems[data]] == "" ? "Select Date" : this.props[this.props.filterItems[data]]} /> */}
                                            </span>
                                            : <span className="asnfbb-right">
                                                {/* <input type="date" id="fromCreation" onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromCreationDate} placeholder={this.props.fromCreationDate == undefined || this.props.fromCreationDate == "" ? "Select Start Date" : this.props.fromCreationDate}/> */}
                                                <RangePicker format="DD-MM-YYY" placeholder={[this.props[this.props.filterItems[data]] !== undefined && this.props[this.props.filterItems[data]] !== "" ? (this.props[this.props.filterItems[data]].split("|")[0].trim() !== "" ? moment(new Date(this.props[this.props.filterItems[data]].split("|")[0].trim())).format("DD-MM-YYYY") : "start date") : "start date", this.props[this.props.filterItems[data]] !== undefined  && this.props[this.props.filterItems[data]] !== "" ? (this.props[this.props.filterItems[data]].split("|")[1].trim() !== "" ? moment(new Date(this.props[this.props.filterItems[data]].split("|")[1].trim())).format("DD-MM-YYYY") : "end date") : "end date"]} onChange={(event) => this.props.handleInput(event, this.props.filterItems[data])} />
                                            </span>
                                        : (data.includes("poQuantity") || data.includes("poAmount")
                                            ? <span className="asnfbb-right">
                                                <input min="0" type="number" id={data.includes("poQuantity") ? "fromPIQuantity" : "fromPIAmount"} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={data.includes("poQuantity") ? this.props.fromPIQuantityValue : this.props.fromPIAmountValue} placeholder="Enter From Value" />
                                                <input  className="asnfbbr-poquantity" min={data.includes("poQuantity") ? this.props.fromPIQuantityValue : this.props.fromPIAmountValue} type="number" id={data.includes("poQuantity") ? "toPIQuantity" : "toPIAmount"} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={data.includes("poQuantity") ? this.props.toPIQuantityValue : this.props.toPIAmountValue} placeholder="Enter To Value" />
                                            </span>
                                            : data.includes("Qty") ?
                                                data.includes("totalCancelledQty") ? 
                                                <span className="asnfbb-right">
                                                    <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromtotalCancelledQty} placeholder="Enter From Value" />
                                                    <input className="asnfbbr-poquantity"  min={this.props.fromtotalCancelledQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.tototalCancelledQty} placeholder="Enter To Value" />
                                                </span> :
                                                data.includes("totalPendingQty") ? 
                                                <span className="asnfbb-right">
                                                    <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromtotalPendingQty} placeholder="Enter From Value" />
                                                    <input className="asnfbbr-poquantity"  min={this.props.fromtotalPendingQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.tototalPendingQty} placeholder="Enter To Value" />
                                                </span> :
                                                data.includes("totalRequestedQty") ? 
                                                <span className="asnfbb-right">
                                                    <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromtotalRequestedQty} placeholder="Enter From Value" />
                                                    <input className="asnfbbr-poquantity"  min={this.props.fromtotalRequestedQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.tototalRequestedQty} placeholder="Enter To Value" />
                                                </span> :
                                                 data.includes("orderQty") ? 
                                                 <span className="asnfbb-right">
                                                     <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromorderQty} placeholder="Enter From Value" />
                                                     <input className="asnfbbr-poquantity"  min={this.props.fromorderQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toorderQty} placeholder="Enter To Value" />
                                                 </span> :
                                                  data.includes("poQty") ? 
                                                  <span className="asnfbb-right">
                                                      <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.frompoQty} placeholder="Enter From Value" />
                                                      <input className="asnfbbr-poquantity"  min={this.props.frompoQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.topoQty} placeholder="Enter To Value" />
                                                  </span> :
                                                  data.includes("cancelQty") ? 
                                                  <span className="asnfbb-right">
                                                      <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromcancelQty} placeholder="Enter From Value" />
                                                      <input className="asnfbbr-poquantity"  min={this.props.fromcancelQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.tocancelQty} placeholder="Enter To Value" />
                                                  </span> :

                                                    // These filters for pending Order
                                                    data.includes("shipmentCancelledQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromshipmentCancelledQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity"  min={this.props.fromshipmentCancelledQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toshipmentCancelledQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("shipmentConfirmedQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromshipmentConfirmedQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity"  min={this.props.fromshipmentConfirmedQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toshipmentConfirmedQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("shipmentDeliveredQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromshipmentDeliveredQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity"  min={this.props.fromshipmentDeliveredQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toshipmentDeliveredQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("shipmentIntransitQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromshipmentIntransitQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity"  min={this.props.fromshipmentIntransitQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toshipmentIntransitQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("shipmentRequestedQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromshipmentRequestedQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity"  min={this.props.fromshipmentRequestedQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toshipmentRequestedQty} placeholder="Enter To Value" />
                                                    </span> :

                                                    //There filters for grc status::
                                                    data.includes("actualQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromactualQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity" min={this.props.fromactualQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toactualQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("inwardQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.frominwardQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity" min={this.props.frominwardQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toinwardQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("pendingQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.frompendingQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity" min={this.props.frompendingQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.topendingQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("rejectedQty") ?
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromrejectedQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity" min={this.props.fromrejectedQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.torejectedQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("prevInwardQty") ? 
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromprevInwardQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity" min={this.props.fromprevInwardQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toprevInwardQty} placeholder="Enter To Value" />
                                                    </span> :
                                                    data.includes("prevRejectQty") &&
                                                    <span className="asnfbb-right">
                                                        <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromprevRejectQty} placeholder="Enter From Value" />
                                                        <input className="asnfbbr-poquantity" min={this.props.fromprevRejectQty} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toprevRejectQty} placeholder="Enter To Value" />
                                                    </span> 
                                                    

                                              //manage vendor filter fields::
                                            : data.includes("dueDays") ? 
                                            <span className="asnfbb-right">
                                                <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromDueDays} placeholder="Enter From Value" />
                                                <input className="asnfbbr-poquantity" min={this.props.fromDueDays} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toDueDays} placeholder="Enter To Value" />
                                            </span> :
                                            data.includes("deliveryBuffDays") ? 
                                            <span className="asnfbb-right">
                                                <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromDeliveryBuffDays} placeholder="Enter From Value" />
                                                <input className="asnfbbr-poquantity" min={this.props.fromDeliveryBuffDays} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toDeliveryBuffDays} placeholder="Enter To Value" />
                                            </span> :

                                            //lr processing Invoice Amount::
                                            data.includes("invoiceAmount") ? 
                                            <span className="asnfbb-right">
                                                <input  min="0" type="number" id={"from"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.fromInvoiceAmount} placeholder="Enter From Value" />
                                                <input className="asnfbbr-poquantity" min={this.props.fromInvoiceAmount} type="number" id={"to"+data} onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.toInvoiceAmount} placeholder="Enter To Value" />
                                            </span> :

                                            //single input field:::
                                            <span className="asnfbb-right">
                                                <input type="text" onChange={this.props.handleInput} data-value={this.props.filterItems[data]} value={this.props.filteredValue[data]} placeholder="Enter Value" />
                                            </span>))
                                    : <span className="asnfbb-right not-checked" onClick={(e) => this.props.handleInputBoxEnable(e, data)}></span>}

                            </div>))}

                            {/* {this.state.recentAdded && this.props.checkedFilters.length != 0 && <div className="recentAdded">
                                <div className="recentAdded-inner">
                                    {this.props.checkedFilters.map((data, key) => <ul key={key}>
                                        <li><div className="checkSeperate checkBoxBorder">
                                            <label className="checkBoxLabel0"><input type="checkBox" onChange={(e) => { this.props.handleCheckedItems(e, data), this.handleRecentlyAddedButton(e) }} />{data}<span className="checkmark1"></span> </label>
                                          
                                        </div></li>
                                     
                                    </ul>)}
                                </div>
                                <div className="asnfra-bottom">
                                    <span className="asnfrb-rmall" onClick={() => { this.props.clearFilter(), this.setState({ showRecentlyAddedButton: false }) }}><span className="asnfrb-img"><img src={Delete}></img></span> Remove all</span>
                                    <span className="asnfrb-done" onClick={() => this.setState({ recentAdded: false, showRecentlyAddedButton: true })}>Done</span>
                                </div>
                            </div>} */}
                        </div>
                    </div>
                    <div className="asnf-bottom">
                        {this.props.applyFilter == true && this.props.checkedFilters.length != 0 ? <a className="m-r-10" onClick={this.props.submitFilter}>Apply Filter</a> : <a className="m-r-10 btnDisabled" >Apply Filter</a>}<a onClick={this.props.clearFilter}>Clear</a>
                    </div>
                </div>
            </div>
        )
    }
}