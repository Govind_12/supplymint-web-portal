import React from "react";
import downArrow from '../../assets/downArrow.svg';
import upArrow from '../../assets/upArrow.svg';
import infoIcon from '../../assets/info-dark.svg';

class TopStore extends React.Component {
    numDifferentiation(val) {
        if (val >= 10000000) val = (val / 10000000).toFixed(2) + ' Cr';
        else if (val >= 100000) val = (val / 100000).toFixed(2) + ' Lac';
        else if (val >= 1000) val = (val / 1000).toFixed(2) + ' K';
        return val;
    }
    render() {
        return (
            <div>
                <div className="col-md-6 col-sm-6 pad-lft-0">
                    <div className="cards_dashboard height-48">
                        <div className="col-md-10 col-sm-12 pad-0">
                            <ul className="list_style">
                                <li>
                                    <label className="contribution_mart">
                                        TOP STORES AND ARTICLE SALES
                        </label>
                                </li>
                                <li>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-2 col-sm-2 pad-0">
                            <ul className="list-inline m-0 text_align_right">
                                <li>
                                    <div className="dropdown">
                                        <svg className="dropdown_img dropdown-toggle" data-toggle="dropdown" xmlns="http://www.w3.org/2000/svg" width="7" height="27" viewBox="0 0 7 27">
                                            <g fill="#6D6DC9" fillRule="nonzero" stroke="#6D6DC9" transform="translate(1 1)">
                                                <circle cx="2.5" cy="2.5" r="2.5" />
                                                <circle cx="2.5" cy="12.643" r="2.5" />
                                                <circle cx="2.5" cy="22.786" r="2.5" />
                                            </g>
                                        </svg>
                                        <ul className="zIndex2 dropdown-menu dropdown_table first_child list-inline">
                                            <li>
                                                <span>
                                                    EXPORT DATA
                                      </span>
                                            </li>
                                            <li onClick={(e) => this.props.exportData('xls')}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 20 21">
                                                    <g fill="#4A4A4A" fillRule="nonzero">
                                                        <path d="M2.889 20.747H17.11A2.889 2.889 0 0 0 20 17.857V10.97a1.111 1.111 0 0 0-2.222 0v6.889a.667.667 0 0 1-.667.666H2.89a.667.667 0 0 1-.667-.666v-6.89a1.111 1.111 0 0 0-2.222 0v6.89a2.889 2.889 0 0 0 2.889 2.889z" />
                                                        <path d="M9.778.142c-.614 0-1.111.498-1.111 1.111v9.145L5.849 7.58a1.111 1.111 0 0 0-1.571 1.571l4.713 4.713c.05.049.103.093.16.132.029.017.058.03.087.046a.644.644 0 0 0 .222.091c.031 0 .062.023.093.03.147.03.298.03.445 0 .033 0 .062-.019.093-.03a.644.644 0 0 0 .222-.09c.03-.016.06-.03.087-.047.057-.04.11-.083.16-.132l4.718-4.713a1.111 1.111 0 0 0-1.571-1.571l-2.818 2.818V1.253c0-.613-.498-1.11-1.111-1.11z" />
                                                    </g>
                                                </svg>
                                                <span>
                                                    XLS
                                        </span>
                                            </li>
                                            <li onClick={(e) => this.props.exportData('csv')}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 20 21">
                                                    <g fill="#4A4A4A" fillRule="nonzero">
                                                        <path d="M2.889 20.747H17.11A2.889 2.889 0 0 0 20 17.857V10.97a1.111 1.111 0 0 0-2.222 0v6.889a.667.667 0 0 1-.667.666H2.89a.667.667 0 0 1-.667-.666v-6.89a1.111 1.111 0 0 0-2.222 0v6.89a2.889 2.889 0 0 0 2.889 2.889z" />
                                                        <path d="M9.778.142c-.614 0-1.111.498-1.111 1.111v9.145L5.849 7.58a1.111 1.111 0 0 0-1.571 1.571l4.713 4.713c.05.049.103.093.16.132.029.017.058.03.087.046a.644.644 0 0 0 .222.091c.031 0 .062.023.093.03.147.03.298.03.445 0 .033 0 .062-.019.093-.03a.644.644 0 0 0 .222-.09c.03-.016.06-.03.087-.047.057-.04.11-.083.16-.132l4.718-4.713a1.111 1.111 0 0 0-1.571-1.571l-2.818 2.818V1.253c0-.613-.498-1.11-1.111-1.11z" />
                                                    </g>
                                                </svg>
                                                <span>
                                                    CSV
                                        </span>
                                            </li>
                                            <li onClick={(e) => this.props.exportData('pdf')}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 20 21">
                                                    <g fill="#4A4A4A" fillRule="nonzero">
                                                        <path d="M2.889 20.747H17.11A2.889 2.889 0 0 0 20 17.857V10.97a1.111 1.111 0 0 0-2.222 0v6.889a.667.667 0 0 1-.667.666H2.89a.667.667 0 0 1-.667-.666v-6.89a1.111 1.111 0 0 0-2.222 0v6.89a2.889 2.889 0 0 0 2.889 2.889z" />
                                                        <path d="M9.778.142c-.614 0-1.111.498-1.111 1.111v9.145L5.849 7.58a1.111 1.111 0 0 0-1.571 1.571l4.713 4.713c.05.049.103.093.16.132.029.017.058.03.087.046a.644.644 0 0 0 .222.091c.031 0 .062.023.093.03.147.03.298.03.445 0 .033 0 .062-.019.093-.03a.644.644 0 0 0 .222-.09c.03-.016.06-.03.087-.047.057-.04.11-.083.16-.132l4.718-4.713a1.111 1.111 0 0 0-1.571-1.571l-2.818 2.818V1.253c0-.613-.498-1.11-1.111-1.11z" />
                                                    </g>
                                                </svg>
                                                <span>
                                                    PDF
                                        </span>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>

                        </div>
                        <div className="col-md-12 col-sm-12 pad-0 dashTabs">
                            <ul className="nav nav-tabs dashboard-tab">
                                <li className={this.props.storesArticlesTab == "topStores" ? "active" : ""}>
                                    <a onClick={(e) => this.props.changeStoArtTab('topStores')}>TOP STORES</a>
                                </li>
                                <li className={this.props.storesArticlesTab == "bottomStores" ? "active" : ""}>
                                    <a onClick={(e) => this.props.changeStoArtTab('bottomStores')}>BOTTOM STORES</a>
                                </li>
                                <li className={this.props.storesArticlesTab == "topArticles" ? "active" : ""}>
                                    <a onClick={(e) => this.props.changeStoArtTab('topArticles')} >TOP ARTICLE</a>
                                </li>
                                <li className={this.props.storesArticlesTab == "bottomArticles" ? "active" : ""}>
                                    <a onClick={(e) => this.props.changeStoArtTab('bottomArticles')}>BOTTOM ARTICLE</a>
                                </li>
                            </ul>
                        </div>
                        <div className="tab-content">
                            <div className="container_table_content tab-pane fade in active " id="top_store">
                                <div className="col-md-12 col-sm-12 pad-0 m-top-20 tabsTableMain">
                                    <div className="table_container">
                                        <table className="table dashboard_table rankTableDash">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <label>
                                                            RANK
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            {this.props.storesArticlesTab == "topStores" || this.props.storesArticlesTab == "bottomStores" ? "STORE NAME" : "ARTICLE NAME"}
                                                        </label>

                                                    </th>
                                                    <th>
                                                        <label>
                                                            LAST YEAR
                                                            <div className="bootToolTip tooltipBot fixToolTip    displayInline">
                                                                <img src={infoIcon} className="height13 m-lft-5" />
                                                                <div className="tooltiptext">
                                                                    <label>Period</label>
                                                                    <div className="date"><span>{this.props.storesArticles.lastYear}</span></div>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            THIS YEAR 
                                                            <div className="bootToolTip tooltipBot fixToolTip displayInline">
                                                                <img src={infoIcon} className="height13 m-lft-5" />
                                                                <div className="tooltiptext">
                                                                    <label>Period</label>
                                                                    <div className="date"><span>{this.props.storesArticles.thisYear}</span></div>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            LAST MONTH 
                                                            <div className="bootToolTip tooltipBot fixToolTip displayInline">
                                                                <img src={infoIcon} className="height13 m-lft-5" />
                                                                <div className="tooltiptext">
                                                                    <label>Period</label>
                                                                    <div className="date"><span>{this.props.storesArticles.lastMonth}</span></div>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            PREV YEAR LAST MONTH 
                                                        </label>
                                                            <div className="bootToolTip fixToolTip displayInline">
                                                                <img src={infoIcon} className="height13 m-lft-5" />
                                                                <div className="tooltiptext">
                                                                    <label>Period</label>
                                                                    <div className="date"><span>{this.props.storesArticles.preLastMonth}</span></div>
                                                                </div>
                                                            </div>
                                                    </th>
                                                </tr>

                                            </thead>
                                            <tbody>
                                                {this.props.storesArticles.length == 0 ? <tr><td colSpan="100%"><label>No Data Found</label></td></tr> : this.props.storesArticles[this.props.storesArticlesTab].map((data, key) => (<tr key={key}>
                                                    <td>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 16 25">
                                                            <g fill="none" fillRule="evenodd">
                                                                <path fill="#38A169" d="M0 0h16v4H0z" />
                                                                <path fill="#38A169" fillRule="nonzero" d="M0 6v13.762L8.016 25 16 19.762V6z" />
                                                                <text fill="#FFF" fontFamily="ProximaNova-Bold, Proxima Nova" fontSize="12" fontWeight="bold">
                                                                    <tspan x="5" y="18">{key+1}</tspan>
                                                                </text>
                                                            </g>
                                                        </svg>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {this.props.storesArticlesTab == "topStores" || this.props.storesArticlesTab == "bottomStores" ? data.storeName : data.articleName}

                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {this.numDifferentiation(data.lastYear)}
                                                        </label>
                                                        {data.lastYearSign == "+" ? <img src={upArrow} className="float_Right" /> : <img src={downArrow} className="float_Right" />}
                                                    </td>
                                                    <td>
                                                        <label>
                                                            {this.numDifferentiation(data.thisYear)}
                                                        </label>
                                                        {data.thisYearSign == "+" ? <img src={upArrow} className="float_Right" /> : <img src={downArrow} className="float_Right" />}

                                                    </td>
                                                    <td>
                                                        <label>
                                                            {this.numDifferentiation(data.lastMonth)}
                                                        </label>
                                                        {data.lastMonthSign == "+" ? <img src={upArrow} className="float_Right" /> : <img src={downArrow} className="float_Right" />}

                                                    </td>
                                                    <td>
                                                        <label>
                                                            {this.numDifferentiation(data.prevYearLastMonth)}
                                                        </label>
                                                        {data.prevYearLastMonthSign == "+" ? <img src={upArrow} className="float_Right" /> : <img src={downArrow} className="float_Right" />}

                                                    </td>
                                                </tr>))}
                                                {/* <tr>
                                                    <td>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 16 25">
                                                            <g fill="none" fill-rule="evenodd">
                                                                <path fill="#38A169" d="M0 0h16v4H0z" />
                                                                <path fill="#38A169" fill-rule="nonzero" d="M0 6v13.762L8.016 25 16 19.762V6z" />
                                                                <text fill="#FFF" font-family="ProximaNova-Bold, Proxima Nova" font-size="12" font-weight="bold">
                                                                    <tspan x="5" y="18">1</tspan>
                                                                </text>
                                                            </g>
                                                        </svg>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            data.lastYear
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            data.lastYear
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            data.thisYear
                                                        </label>

                                                    </td>
                                                    <td>
                                                        <label>
                                                            data.lastMonth
                                                        </label>

                                                    </td>
                                                    <td>
                                                        <label>
                                                            data.prevYearLastMonth
                                                        </label>
                                                    </td>
                                                </tr> */}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="container_table_content tab-pane fade" id="bottom_store">
                                
                            <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                    <div className="table_container">
                                        <table className="table dashboard_table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <label>
                                                            STORES
                                                        </label>
                                                        
                                                    </th>
                                                    <th>
                                                        <label>
                                                            LAST YEAR
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            THIS YEAR
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            LAST MONTH
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            PREV YEAR LAST MONTH
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            NEXT THREE MONTH
                                                        </label>
                                                    </th>
                                                </tr>

                                            </thead>
                                            <tbody>
                                                    <tr>
                                                        <td>
                                                            <label>
                                                                Store one 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                567.657
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                            445.243
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                            445.243
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                Store Two 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                567.657
                                                            </label>
                                                        </td>
                                                    </tr>
                                            

                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopStore;
