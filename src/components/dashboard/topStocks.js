import React from "react";
import openRack from '../../assets/open-rack.svg'
import MapStock from '../../assets/map.svg'
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const data = [
    { name: 'Page A', pv: 2400, amt: 2400 },
    { name: 'Page B', pv: 6398, amt: 2210 },
    { name: 'Page C', pv: 7800, amt: 2290 },
    { name: 'Page D', pv: 5908, amt: 2000 },
    { name: 'Page E', pv: 6800, amt: 2181 },
];
const COLORS = ['#e9e9e9', '#6d6dc9'];

class TopStock extends React.Component {
    render() {
        return (
            <div>
                {/* <div className="col-md-12 col-sm-12 col-xs-6 pad-0 m-top-10"> */}
                <div className="col-md-4 col-xs-12 pad-lft-0">
                    <div className="cards_dashboard height-50">
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list_style">
                                <li>
                                    <label className="contribution_mart">
                                        TOP 5 STOCKS
                                </label>
                                </li>
                                <li>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-12 co-sm-12 col-xs-6 pad-0">
                            <div className="mapDiv dashBarChart">
                                {/* <img src={MapStock} /> */}
                                {/* <BarChart width={430} height={240} data={data}
                                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip cursor={{fill: 'transparent'}} />
                                    <Bar dataKey="pv" fill="#8884d8">
                                        {
                                            data.map((entry, index) => {
                                                const color = index % 2 > 0 ? COLORS[0] : COLORS[1];
                                                return <Cell key={index} fill={color} />;
                                            })
                                        }
                                    </Bar>
                                </BarChart> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default TopStock;
