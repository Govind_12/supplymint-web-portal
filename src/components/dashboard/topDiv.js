import React from "react";
class TopDiv extends React.Component {

    changeValue(title) {

        let data = this.props.data

        for (let i = 0; i < data.length; i++) {
            if (data[i].title == title) {

                data[i].display = !data[i].display
                //    if(type=="in"){
                //    data[].display = true
                //    }else{
                //         data.display = true
                //    }
            }
        }
        this.props.updateTopDiv(data)




    }
    commaSeparator(val) {
        var nf = new Intl.NumberFormat();
        return nf.format(val)
    }
    numDifferentiation(val) {
        if (val >= 10000000) val = (val / 10000000).toFixed(2) + ' Cr';
        else if (val >= 100000) val = (val / 100000).toFixed(2) + ' Lac';
        else if (val >= 1000) val = (val / 1000).toFixed(2) + ' K';
        return val;
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="col-md-12 col-sm-12 col-xs-6 pad-0 m-top-5 zIndex2">
                    <div className="cards_dashboard">
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list_style">
                                <li>
                                    <label className="contribution_mart">
                                        DASHBOARD
                                </label>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0 m-top-20 dashCard">
                            {this.props.data.map((data, key) => (
                               data.display? <div key={key} className="col-md-3 col-sm-3 pad-lft-0 cardMain">
                                <div className="cards">
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="col-md-4 pad-0">
                                            <div className="cards_lft">
                                                <img src={data.image} className="cards_img" />
                                            </div>
                                        </div>
                                        <div className="col-md-8 col-sm-8 pad-0">
                                            <ul className="list-inline card_list m-0">
                                                <li className="width100">
                                                    <div className="container_list_card">
                                                        <p className="cards_content">
                                                            {data.title}
                                                        </p>
                                                        <span>
                                                            {data.subTitle}
                                                        </span>
                                                    </div>
                                                </li>
                                                <li >
                                                    <div className="amout_content">
                                                        <span>
                                                            {data.symbol}
                                                        </span>
                                                        <div className="bootToolTip tooltipBot displayInline">
                                                            {this.numDifferentiation(data.value)}
                                                            {/* {data.value >= 1000? <p className="tooltiptext">{this.commaSeparator(data.value)}</p>:null} */}
                                                            <div className="tooltiptext">
                                                                <h4 className="m0">{data.symbolTool}   {this.commaSeparator(data.value)}</h4>
                                                                <label>Period</label>
                                                                <div className="date"><span>{data.date}</span></div>                                                                
                                                            </div>
                                                            {/* {data.display?data.value:this.numDifferentiation(data.value)} */}
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>:null))}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopDiv;
