import React from 'react';

class ArsDashboard extends React.Component {
    home(){
        if(sessionStorage.getItem('token') == null){
            window.location.hash = "#/"
        }else{
            window.location.hash = "#/home"
        }
    }
    render() {
        return (
            <div className="container-fluid pad-l50">
                <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                    <div className="ars-dash-msg">
                        <div className="adm-inner">
                            <img src={require('../../assets/under_constructions.svg')} />
                            <h3>Page Under Development</h3>
                            <p>We will live this page soon</p>
                            <button type="button" onClick={() => this.home()}>Go Back</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ArsDashboard;