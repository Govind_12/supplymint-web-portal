import React from "react";
import downArrow from '../../assets/downArrow.svg';
import upArrow from '../../assets/upArrow.svg';

export default class ProductConsistency extends React.Component {
    render() {
     
        return (
            <div className="col-md-12 col-xs-12 pad-0">
                <div className="cards_dashboard height-50 productConsistency">
                    <div className="col-md-12 col-sm-12 pad-0">
                        <ul className="list_style">
                            <li>
                                <label className="contribution_mart">
                                    PRODUCT CONSISTENCY
                                </label>
                            </li>
                            <li>
                            </li>
                        </ul>
                    </div>
                    <div className="col-md-9 col-sm-8 col-xs-12 pad-0">
                        <div className="col-md-12 pad-0">
                            <ul className="nav nav-tabs dashboard-tab">
                                <li onClick={(e) => this.props.productTabs('fastMoving')} className={this.props.productTab == "fastMoving" ? "active" : ""}>
                                    <a data-toggle="tab" href="#fast_moving_item">FAST MOVING ITEMS</a>
                                </li>
                                <li onClick={(e) => this.props.productTabs('slowMoving')} className={this.props.productTab == "slowMoving" ? "active" : ""}>
                                    <a data-toggle="tab" href="#slow_moving_item">SLOW MOVING ITEMS</a>
                                </li>
                            </ul>
                        </div>
                        <div className="tab-content">
                            <div className="container_table_content tab-pane fade in active" id="fast_moving_item">
                                <div className="col-md-12 pad-0 m-top-20">
                                    <div className="table_container">
                                        <div className="table_heading">
                                            {/* <label>
                                                DENIM SHIRT
                                            </label> */}
                                        </div>
                                        <div className="tableHeadFix">
                                            <div className="tableHeadFixHeight">
                                                <table className="table dashboard_table border-seperate">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <label>
                                                                    ARTICLE CODE
                                                        </label>

                                                            </th>
                                                            <th>
                                                                <label>
                                                                    ARTICLE NAME
                                                        </label>
                                                            </th>
                                                            <th>
                                                                <label>
                                                                    VOLUME
                                                        </label>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.props.slowFast[this.props.productTab][this.props.filterDay][this.props.filterProduct].length==0?<tr><td colSpan="3"><label>No Data Found</label></td></tr>:this.props.slowFast[this.props.productTab][this.props.filterDay][this.props.filterProduct].map((data, key) => (
                                                            <tr key={key}>
                                                                <td>
                                                                    <label>
                                                                        {data.articleCode}
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        {data.articleName}
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        {data.days}
                                                                    </label>
                                                                    {data.daysSign == "+" ? <img src={upArrow} className="float_Right" /> : <img src={downArrow} className="float_Right" />}
                                                                </td>
                                                            </tr>))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-4 pad-rgt-0 m-top-35">
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="table_heading">
                                <label>
                                    FILTER PRODUCTS
                                </label>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                            <ul className="list-inline products_list">
                                <li>
                                    <div className="box">
                                        <ul className="pad-lft-0">
                                            <li>
                                                <label className="containerPurchaseRadio">
                                                    <input checked={this.props.filterProduct == "topThree"} onChange={(e) => this.props.filterProducts("topThree")} type="radio" name="topProducts" />
                                                    <span className="checkmark"></span>Top 3 fast Moving products
                                                </label>
                                            </li>
                                            <li>
                                                <label className="containerPurchaseRadio">
                                                    <input checked={this.props.filterProduct == "topFive"} onChange={(e) => this.props.filterProducts("topFive")} type="radio" name="topProducts" />
                                                    <span className="checkmark"></span>Top 5 fast Moving products
                                                </label>
                                            </li>
                                            <li>
                                                <label className="containerPurchaseRadio">
                                                    <input checked={this.props.filterProduct == "topTen"} onChange={(e) => this.props.filterProducts("topTen")} type="radio" name="topProducts" />
                                                    <span className="checkmark"></span>Top 10 fast Moving products
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <ul className="list-inline m-top-10 products_list">
                                <li>
                                    <label className="frequency_list">
                                        Sort by Sold Frequency
                                    </label>
                                </li>
                                <li>
                                    <div className="box">
                                        <ul className="pad-lft-0">
                                            <li>
                                                <label className="containerPurchaseRadio">
                                                    <input checked={this.props.filterDay == "thirty"} onChange={(e) => this.props.filterDays("thirty")} type="radio" name="radio" />
                                                    <span className="checkmark"></span>30 Days
                                                </label>
                                            </li>
                                            <li>
                                                <label className="containerPurchaseRadio">
                                                    <input checked={this.props.filterDay == "fourty"} onChange={(e) => this.props.filterDays("fourty")} type="radio" name="radio" />
                                                    <span className="checkmark"></span>45 Days
                                                </label>
                                            </li>
                                            <li>
                                                <label className="containerPurchaseRadio">
                                                    <input checked={this.props.filterDay == "ninety"} onChange={(e) => this.props.filterDays("ninety")} type="radio" name="radio" />
                                                    <span className="checkmark"></span>90 Days
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div >


        );
    }
}
