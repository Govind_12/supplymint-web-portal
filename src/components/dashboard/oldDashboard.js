import React from 'react';
import dashbordImage from "../../assets/group-2.png";
import { Link } from "react-router-dom";
import moment from "moment";
import { render } from 'react-dom';

export default class OldDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentDate: Date().toLocaleString()
        }
    }
    componentDidMount() {
        this.getHour()
    }

    getHour = () => {
        const date = new Date();
        const hour = date.getHours()
        this.setState({
            hour
        });
    }

    render() {
        const { hour } = this.state;
        return (
            <div className="container-fluid m-top-75 pad-0">
                {/* <div className="col-md-12 col-sm-12 col-xs-6 pad-0 m-top-5 mainDashboard">
                    <div className="cards_dashboard">
                        <div className="col-md-6">
                            <div className="dashboardContent">
                                <h2>Welcome to Supplymint</h2>
                                <h4>Power to modernize your supplychain solutions</h4>
                                <p>Supplymint is designed to track and manage all IT and non-IT Assets of organizations. 
                                Each company wants to track the status and condition of their all assets to improve their
                                productivity and lower the maintenance cost. This solution will help in achieving the same.
                                </p>

                            </div>
                        </div>
                        <div className="col-md-6">
                            <img src={dashbordImage} />
                        </div>
                    </div>
                </div> */}
                <div className="home-page-main-layer">
                    <div className="hpml-inner">
                        <div className="hpmli-div">
                            <div className="hpmli-top">
                                <div className="hpmlit-message">
                                    <p>{hour < 12 ? <img src={require('../../assets/morning.svg')} /> : hour < 15 ? <img src={require('../../assets/afternoon.svg')} /> : <img src={require('../../assets/evening.svg')} />}   Hi! <span>{hour < 12 ? "Good Morning" : hour < 15 ? "Good Afternoon" : "Good evening"}</span></p>
                                </div>
                                <span className="hpmlit-time"><span className="bold">{moment(this.state.currentDate).format("dddd")}</span>, {moment(this.state.currentDate).format("DD MMMM YYYY")} </span>
                                <div className="hpmlit-wlcm-text">
                                    <h3>Welcome To Supplymint</h3>
                                    <p>AI Powered Digital Planning & Solutions</p>
                                </div>
                            </div>

                            {(sessionStorage.getItem('uType') === "VENDOR") ?
                                <div className="hpmli-body">
                                    <div className="hpmlib-box">
                                        <div className="hpmlibb-icon">
                                            <span className="hpmlibbi-bg">
                                                <span className="hpmlibbibg-inner">
                                                    <img src={require('../../assets/digi-vendor-icon.svg')} />
                                                </span>
                                            </span>
                                        </div>
                                        <div className="hpmlibb-content">
                                            <h3>DigiVend</h3>
                                            <p>Supplymint manage & track all your vendor orders from one place & never miss out on a delay. </p>
                                            {sessionStorage.getItem('moduleList').includes("VDIGIVEND") ? <Link to="/vendor/vendorDashboard"><button type="button">Go to Dashboard
                                            <span className="hpmlibb-right-arrow">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="19.601" height="11.432" viewBox="0 0 23.601 11.432">
                                                        <g id="arrow_4_" data-name="arrow (4)" transform="translate(0 -132)">
                                                            <g id="Group_2930" data-name="Group 2930" transform="translate(0 132)">
                                                                <path id="Path_891" fill="#fff" d="M23.331 137.063l-4.817-4.794a.922.922 0 0 0-1.3 1.307l3.234 3.219H.922a.922.922 0 0 0 0 1.844h19.524l-3.234 3.219a.922.922 0 0 0 1.3 1.307l4.817-4.794a.923.923 0 0 0 .002-1.308z" data-name="Path 891" transform="translate(0 -132)" />
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </button></Link> : <span className="hpmlibb-button">
                                                    <button type="button">Go to Dashboard</button>
                                                </span>}
                                        </div>
                                    </div>
                                </div> :
                                <div className="hpmli-body">
                                    <div className="hpmlib-box">
                                        <div className="hpmlibb-icon">
                                            <span className="hpmlibbi-bg">
                                                <span className="hpmlibbibg-inner">
                                                    <img src={require('../../assets/digi-proc-icon.svg')} />
                                                </span>
                                            </span>
                                        </div>
                                        <div className="hpmlibb-content">
                                            <h3>DigiProc</h3>
                                            <p>Supplymint enables ease of work in procurement for our enterprises extending its support up-to end users. </p>
                                            {sessionStorage.getItem('moduleList').includes("DIGIPROC") ? <Link to="/purchase/dashboard"><button type="button">Go to Dashboard
                                            <span className="hpmlibb-right-arrow">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="19.601" height="11.432" viewBox="0 0 23.601 11.432">
                                                        <g id="arrow_4_" data-name="arrow (4)" transform="translate(0 -132)">
                                                            <g id="Group_2930" data-name="Group 2930" transform="translate(0 132)">
                                                                <path id="Path_891" fill="#fff" d="M23.331 137.063l-4.817-4.794a.922.922 0 0 0-1.3 1.307l3.234 3.219H.922a.922.922 0 0 0 0 1.844h19.524l-3.234 3.219a.922.922 0 0 0 1.3 1.307l4.817-4.794a.923.923 0 0 0 .002-1.308z" data-name="Path 891" transform="translate(0 -132)" />
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </button></Link> : <span className="hpmlibb-button">
                                                    <button type="button">Go to Dashboard</button>
                                                </span>}
                                        </div>
                                    </div>
                                    <div className="hpmlib-box hpmlib-blr">
                                        <div className="hpmlibb-icon">
                                            <span className="hpmlibbi-bg">
                                                <span className="hpmlibbibg-inner">
                                                    <img src={require('../../assets/digi-vendor-icon.svg')} />
                                                </span>
                                            </span>
                                        </div>
                                        <div className="hpmlibb-content">
                                            <h3>DigiVend</h3>
                                            <p>Supplymint manage & track all your vendor orders from one place & never miss out on a delay. </p>
                                            {sessionStorage.getItem('moduleList').includes("DIGIVEND") ? <Link to="/enterprise/enterpriseDashboard"><button type="button">Go to Dashboard
                                            <span className="hpmlibb-right-arrow">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="19.601" height="11.432" viewBox="0 0 23.601 11.432">
                                                            <g id="arrow_4_" data-name="arrow (4)" transform="translate(0 -132)">
                                                                <g id="Group_2930" data-name="Group 2930" transform="translate(0 132)">
                                                                    <path id="Path_891" fill="#fff" d="M23.331 137.063l-4.817-4.794a.922.922 0 0 0-1.3 1.307l3.234 3.219H.922a.922.922 0 0 0 0 1.844h19.524l-3.234 3.219a.922.922 0 0 0 1.3 1.307l4.817-4.794a.923.923 0 0 0 .002-1.308z" data-name="Path 891" transform="translate(0 -132)" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </button></Link> : <span className="hpmlibb-button">
                                                    <button type="button">Go to Dashboard</button>
                                                </span>}
                                        </div>
                                    </div>
                                    <div className="hpmlib-box">
                                        <div className="hpmlibb-icon">
                                            <span className="hpmlibbi-bg">
                                                <span className="hpmlibbibg-inner">
                                                    <img src={require('../../assets/digi-ars-icon.svg')} />
                                                </span>
                                            </span>
                                        </div>
                                        <div className="hpmlibb-content">
                                            <h3>DigiARS</h3>
                                            <p>Our predictive analysis helps in smart inventory planning and replenishment minimizing human intervention </p>
                                            {sessionStorage.getItem('moduleList').includes("DIGIARS") ? <Link to="/ars-dashboard"><button type="button">Go to Dashboard
                                            <span className="hpmlibb-right-arrow">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="19.601" height="11.432" viewBox="0 0 23.601 11.432">
                                                            <g id="arrow_4_" data-name="arrow (4)" transform="translate(0 -132)">
                                                                <g id="Group_2930" data-name="Group 2930" transform="translate(0 132)">
                                                                    <path id="Path_891" fill="#fff" d="M23.331 137.063l-4.817-4.794a.922.922 0 0 0-1.3 1.307l3.234 3.219H.922a.922.922 0 0 0 0 1.844h19.524l-3.234 3.219a.922.922 0 0 0 1.3 1.307l4.817-4.794a.923.923 0 0 0 .002-1.308z" data-name="Path 891" transform="translate(0 -132)" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </button></Link> :
                                                <span className="hpmlibb-button">
                                                    <button type="button">Go to Dashboard</button>
                                                </span>}
                                        </div>
                                    </div>
                                </div>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}