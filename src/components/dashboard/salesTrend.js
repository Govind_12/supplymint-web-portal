import React from "react";
import { Label, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

class SalesTrends extends React.Component {
    Fun() {
        var element = document.getElementById('scrollMe');
        var positionInfo = element.getBoundingClientRect();
        var width = positionInfo.width;
        var elmnt = document.getElementById("scrollMe");
        elmnt.scrollLeft = elmnt.scrollLeft + width;
        elmnt.style.transition = "5s";
    }
    FunRight() {
        var element = document.getElementById('scrollMe');

        var positionInfo = element.getBoundingClientRect();
        var width = positionInfo.width;
        var elmnt = document.getElementById("scrollMe");
        elmnt.scrollLeft = elmnt.scrollLeft - width;
    }
    numDifferentiation(val) {
        if (val >= 10000000) val = (val / 10000000).toFixed(2) + ' Cr';
        else if (val >= 100000) val = (val / 100000).toFixed(2) + ' Lac';
        else if (val >= 1000) val = (val / 1000).toFixed(2) + ' K';
        return val;
    }
    render() {
        return (
            <div>
                <div className="col-md-6 col-sm-6 pad-rgt-0 salesTrendGraph">
                    <div className="cards_dashboard height-48 ">
                        <div className="col-md-3 col-sm-12 pad-0 saleTrendHeading">
                            <ul className="list_style">
                                <li>
                                    <label className="contribution_mart">
                                        SALES TREND GRAPH
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-9 col-sm-8 pad-rgt-0 pad-lft-0">
                            <div className="months_list">
                                <div className="scrollMonths" id="scrollMe">
                                    <span className="glyphicon glyphicon-chevron-left" onClick={() => this.FunRight()}></span>
                                    <span className="glyphicon glyphicon-chevron-right" onClick={() => this.Fun()}></span>
                                    <ul className="list-inline" id="totalWidth">
                                        <li>
                                            <button className={this.props.salesTrendTime == "currentmonth" ? "btnActive" : ""} onClick={(e) => this.props.changeTime('currentmonth')}>
                                                CURRENT MONTH
                            </button>
                                        </li>
                                        <li>
                                            <button className={this.props.salesTrendTime == "lastmonth" ? "btnActive" : ""} onClick={(e) => this.props.changeTime('lastmonth')}>
                                                LAST MONTH
                            </button>
                                        </li>
                                        <li>
                                            <button className={this.props.salesTrendTime == "lastthreemonth" ? "btnActive" : ""} onClick={(e) => this.props.changeTime('lastthreemonth')}>
                                                LAST 3 MONTH
                            </button>
                                        </li>
                                        <li>
                                            <button className={this.props.salesTrendTime == "lastsixmonth" ? "btnActive" : ""} onClick={(e) => this.props.changeTime('lastsixmonth')}>
                                                LAST 6 MONTH
                            </button>
                                        </li>
                                        <li>
                                            <button className={this.props.salesTrendTime == "lasttwelfthmonth" ? "btnActive" : ""} onClick={(e) => this.props.changeTime('lasttwelfthmonth')}>
                                                LAST 12 MONTH
                            </button>
                                        </li>
                                        <li>
                                            <button className={this.props.salesTrendTime == "lastfinancialyear" ? "btnActive" : ""} onClick={(e) => this.props.changeTime('lastfinancialyear')}>
                                                LAST FINANCIAL YEAR
                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 toggleBtnDash alignMiddle">
                            <div className="col-md-6 m-top-15">
                                <div className="saleToggleBtn">
                                    <div className={this.props.salesTrendToggle == "unitSales" ? "unitSales activeUnit " : "unitSales displayPointer"} onClick={(e) => this.props.changeSalTreTab('unitSales')}>Unit Sales</div>
                                    <div className={this.props.salesTrendToggle == "salesValue" ? "unitSales activeSales" : "unitSales displayPointer"} onClick={(e) => this.props.changeSalTreTab('salesValue')}>Sales Value</div>
                                </div>
                            </div>
                            <div className="col-md-6 m-top-20 textRight">
                                <label>{this.props.salesTrend.years}</label>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0 ">
                            <div className="graphDiv dashLineGraph ">
                                <LineChart width={600} height={240} data={this.props.salesTrend[this.props.salesTrendToggle]}
                                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                                    {/* <XAxis interval={this.props.salesTrendTime == "lastsixmonth" ? 2 : this.props.salesTrend[this.props.salesTrendToggle].length > 12 ? 3 : null} dataKey="day" />
                                    <YAxis /> */}
                                    <XAxis dataKey="billDate" interval={this.props.salesTrendTime == "lastsixmonth" ? 2 : this.props.salesTrend[this.props.salesTrendToggle].length > 12 ? 3 : null} dataKey="day" >
                                        <Label value={this.props.salesTrendTime == "currentmonth" ? "Days" : this.props.salesTrendTime == "lastmonth" ? "Days" : this.props.salesTrendTime == "lastthreemonth" ? "Weeks" : this.props.salesTrendTime == "lastsixmonth" ? "Weeks" : this.props.salesTrendTime == "lasttwelfthmonth" ? "Months" : this.props.salesTrendTime == "lastfinancialyear" ? "Months" : null} position="insideBottom" offset={-3} />
                                    </XAxis>
                                    <YAxis label={{ value: "Unit Sales", position: 'insideTop', dy: -20, dx: 5 }} />
                                    <CartesianGrid strokeDasharray="1 1" />
                                    <Tooltip />
                                    <Legend />
                                    <Line type="linear" dataKey="value" stroke="#6d6dc9" activeDot={{ r: 4 }} dot={{ stroke: '#6d6dc9', strokeWidth: 6 }} />
                                </LineChart>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default SalesTrends;
