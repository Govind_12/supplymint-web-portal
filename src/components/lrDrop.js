import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper/index'

class LrDrop extends React.Component {

    render() {
        var Lr_Processing = JSON.parse(sessionStorage.getItem('LR Processing')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('LR Processing')).crud)

        var Goods_Intransit = JSON.parse(sessionStorage.getItem('Goods Intransit')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Intransit')).crud)
        var Goods_Delivered = JSON.parse(sessionStorage.getItem('Goods Delivered')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Delivered')).crud)

        var Logistics = JSON.parse(sessionStorage.getItem('Logistics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Logistics')).crud)

        return (
            <label>
                <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
                    <button className="dropbtn home_link">Logistic
                    <i className="fa fa-chevron-down"></i>
                    </button>
                    {/* {sessionStorage.getItem("uType") == "ENT" && Logistics > 0 ? <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
                        {Lr_Processing > 0 ? <Link to="/enterprise/logistics/lrProcessing" onClick={(e) => closeMyDrop(e)}>LR Processing</Link> : null}
                        {Goods_Intransit > 0 ? <Link to="/enterprise/logistics/goodsIntransit" onClick={(e) => closeMyDrop(e)}>Goods Intransit</Link> : null}
                        {Goods_Delivered > 0 ? <Link to="/enterprise/logistics/goodsDelivered" onClick={(e) => closeMyDrop(e)}>Gate Entry</Link> : null}
                        <Link to="/enterprise/logistics/grcStatus" onClick={(e) => closeMyDrop(e)}>GRC Status</Link>
                    </div> : null}
                    {sessionStorage.getItem("uType") == "VENDOR" && Logistics > 0 ? <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
                        {Lr_Processing > 0 ? <Link to="/vendor/logistics/lrProcessing" onClick={(e) => closeMyDrop(e)}>LR Processing</Link> : null}
                        {Goods_Intransit > 0 ? <Link to="/vendor/logistics/goodsIntransit" onClick={(e) => closeMyDrop(e)}>Goods Intransit</Link> : null}
                        {Goods_Delivered > 0 ? <Link to="/vendor/logistics/goodsDelivered" onClick={(e) => closeMyDrop(e)}>Goods Delivered</Link> : null}
                    </div> : null} */}
                </div>
            </label>
        );
    }
}

export default LrDrop;