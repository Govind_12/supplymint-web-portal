import React, { Component } from 'react'
import { FilterStore } from '../../helper';

export default class AllStoresModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filteredData: props.selectedStores,
            search: ""
        }
    }
    handleFilter = (e) => {
        this.setState({
            search: e.target.value,
            filteredData: FilterStore(e.target.value, this.props.selectedStores)
        })
    }
    render() {
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block newPoModal">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content masterEvenEdit allStoreModal modalShow pad-0">
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="modal_Color">
                                    <div className="modal-top alignMiddle">
                                        <div className="col-md-6 pad-0">
                                            <h2 className="select_name-content m0">All Stores</h2>
                                        </div>
                                        <div className="col-md-6 pad-0 text-right">
                                            <button type="button" id="allStore" className="discardBtns m-rgt-10" onClick={this.props.closeAllStore}>Close</button>
                                            {/* <button type="button" id="saveButton" className="saveBtnBlue" onClick={(e) => this.onSave(e)}>Save</button> */}
                                        </div>
                                    </div>
                                    <div className="col-md-12 newSearch m-top-20">
                                    <div className="col-md-6"></div>
                                        <div className="col-md-6">
                                            <input type="search" className="inputSearch float_Right width_70" placeholder="Search..." value={this.state.search} onChange={this.handleFilter} />
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 displayInline modalMidPad">
                                        <div className="modal-content modalMid pad-0">
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-10 bordere3e7f3 tableGeneric masterEventTable eventmodalTableScroll">
                                                <table className="table scrollTable main-table zui-table tableBotBorder">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Store</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.filteredData.length!=0? this.state.filteredData.map((data, key) => (<tr key={key}>
                                                            <td ><label>{data}</label></td>
                                                        </tr>)):<tr><label>No Data Found</label></tr>}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
