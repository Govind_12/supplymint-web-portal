import React from 'react';
import RightSideBar from "../rightSideBar";
import Footer from '../footer'
import BraedCrumps from "../breadCrumps";
import SideBar from "../sidebar";
// import Footer from '../../footer' 
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import openRack from "../../assets/open-rack.svg";
import shapeError from "../../assets/shape-error.svg";
import cloudIcon from "../../assets/cloud-icon.svg";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import axios, { post } from "axios";
import { CONFIG } from "../../config";
import SmallLoader from '../loaders/smallLoader';
import CheckGreen from "../../assets/greencheck.png";
import refreshIcon from "../../assets/refresh.svg";
import { OganisationIdName } from '../../organisationIdName.js';

class GlobalDataSync extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prev: 1,
            current: 1,
            next: "",
            maxPage: 1,
            fromDate: "",
            toDate: "",
            roleData: [{
                "id": 0,
                "channel": "RETAIL",
                "storeCode": "MFN0086",
                "orgCode": "RETAIL",
                "storeName": "UK MULTICORP - GRAND VENICE - G NOIDA",
                "zone": "NORTH",
                "storeGrade": "B FO",
                "storeRank": "217",
                "partner": "FEBO"
            }, {
                "id": 0,
                "channel": "RETAIL",
                "storeCode": "MFN0205",
                "orgCode": "RETAIL",
                "storeName": "MONCHER TRENDS LLP - CC2",
                "zone": "EAST",
                "storeGrade": "B+",
                "storeRank": "68",
                "partner": "FEBO"
            }, {
                "id": 0,
                "channel": "RETAIL",
                "storeCode": "MFN0155",
                "orgCode": "RETAIL",
                "storeName": "V S CORP - NELLORE",
                "zone": "SOUTH",
                "storeGrade": "B",
                "storeRank": "126",
                "partner": "FEBO"
            }, {
                "id": 0,
                "channel": "RETAIL",
                "storeCode": "MFN0219",
                "orgCode": "RETAIL",
                "storeName": "GAURIK FASHIONS - CHENNAI FORUM MALL",
                "zone": "SOUTH",
                "storeGrade": "B",
                "storeRank": "186",
                "partner": "FEBO"
            }, {
                "id": 0,
                "channel": "RETAIL",
                "storeCode": "MFN0220",
                "orgCode": "RETAIL",
                "storeName": "FIT & STYLE - KHANNA",
                "zone": "NORTH",
                "storeGrade": "C",
                "storeRank": "238",
                "partner": "FEBO"
            }],
            selected: [], search: "",
            orgCodeGlobal: "",
            openRightBar: false,
            rightbar: false,
            file: "",
            uploadButton: false,
            channel: "",
            template: "",
            // channelerr: false,
            templateerr: false,
            filePath: "",
            fileerr: false,
            loader: false,
            success: false,
            alert: false,
            errorMessage: "",
            successMessage: "",
            errorCode: "",
            templateData: "",
            downloadLink: null,
            code: "",
            status: "",
            fileData: {},
            fname: "",
            smallLoader: false,
            dataSyncTable: [],
            headers: ""
        }
    }
    Change(e) {
        this.setState({
            search: e.target.value
        })
    }


    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onnChange(id) {

        let match = _.find(this.state.roleData, function (o) {
            return o.storeCode == id;
        })
        this.setState({
            selected: this.state.selected.concat(match)
        });
        let idd = match.storeCode;
        var array = this.state.roleData;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            roleData: array,
            selectederr: false,
            filter: true,
            remove: false
        })

    }
    onDelete(e) {
        let match = _.find(this.state.selected, function (o) {
            return o.storeCode == e;
        })
        this.setState({
            roleData: this.state.roleData.concat(match)
        });


    }
    handleChange(e) {
        if (e.target.id == "fromDate") {
            this.setState({
                fromDate: e.target.value
            })
        } else if (e.target.id == "toDate") {
            this.setState({
                toDate: e.target.value
            })
        }
    }


    render() {
        const { search, roleData, selectederr } = this.state;
        var result = _.filter(roleData, function (data) {
            return _.startsWith(data.storeName.toLowerCase(), search.toLowerCase());
        });
        let { channelerr, templateerr, fileerr } = this.state;
        return (
            <div className="container-fluid">
                <div className="container_div" id="home">
                    <SideBar {...this.props} />
                    <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}

                    <div className="container_div m-top-75 " id="">
                        <div className="col-md-12 col-sm-12">
                            <div className="menu_path">
                                <ul className="list-inline width_100 pad20">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid">
                        <div className="container_div" id="">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <div className="container-fluid">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="replenishment_container">
                                            <div className="col-md-12 col-sm-12 pad-0">
                                                <ul className="list_style">
                                                    <li>
                                                        <label className="contribution_mart">
                                                            GLOBAL DATA SYNC
                                            </label>
                                                    </li>
                                                    <li>
                                                        <p className="master_para">Sync data to MFTP server</p>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div className="col-md-12 col-sm-12 pad-0">
                                                <div className="chooseFileSketches">
                                                    <ul className="pad-0 ">
                                                        <li><h6>From Date</h6>
                                                            <div className="form-group">
                                                                <input type="date" className="form-control" id="fromDate" placeholder={this.state.fromDate} onChange={(e) => this.handleChange(e)} />
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h6>To Date</h6>
                                                            <div className="form-group">
                                                                <input type="date" className="form-control" id="toDate" placeholder={this.state.toDate} onChange={(e) => this.handleChange(e)} />
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0">


                                                <div className="sketchesFormDiv  width50">
                                                    <label className="sketchesFormInput" >
                                                        Choose Stores
                                                            <span> ( you can choose multiple stores at a time )</span>
                                                    </label>
                                                    <div className="purchaseSelectBox runOndemandDrop width60per">
                                                        <label className="SketchBox">{this.state.selected.length} Stores selected</label>
                                                        <span className="spanSketch">
                                                            ▾
                                                                        </span>
                                                    </div>

                                                    <div className="dropdownSketchFilter skechersDrop dropDownSkech">
                                                        <div className="SketchFilter">
                                                            <p>Choose Stores from below list :</p>
                                                            <input type="text" placeholder="Type to Search…" value={search} onChange={(e) => this.Change(e)} />
                                                        </div>
                                                        <div className="sketchStoreDrop">
                                                            <div className="lftStoreDiv m-rgt-20">
                                                                <h2>Choose Stores</h2>
                                                                <div className="storeChoose">
                                                                    <ul className="list-inline">
                                                                        {result.length == 0 ? null : result.map((data, i) => <li key={i} onClick={(e) => this.onnChange(`${data.storeCode}`)} >
                                                                            {/* <p className="para_purchase">
                                                                                            <input type="text" className="purchaseCheck" type="checkbox" id="c8" name="cb" />
                                                                                            <label htmlFor="c8" className="purchaseLabel"></label>
                                                                                        </p> */}
                                                                            <span className="onHover"></span>
                                                                            <p className="contentStore pad-5">{data.storeName}</p>

                                                                        </li>)}

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="lftStoreDiv">
                                                                <h2>Selected Stores</h2>
                                                                <div className="storeChoose pad-10">
                                                                    <ul className="list-inline">
                                                                        <li>
                                                                            {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.storeCode} className="">
                                                                                <p className="contentStore">{data.storeName} </p>
                                                                                <span onClick={(e) => this.onDelete(data.storeCode)} value={data.storeCode} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                            </div>)}
                                                                        </li>
                                                                    </ul>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 ">

                                                <ul className="list-inline  pad-0 m-top-15">
                                                    {/*<li>
                                                            <button type="button" className="clear_button_vendor" onClick={() => this.onClear()}>Clear</button>
                                                        </li>*/}
                                                    <li>
                                                        <button className="save_button_vendor" type="submit">Sync Data</button>
                                                    </li>
                                                </ul>

                                            </div>

                                            <div className="col-md-12 col-sm-12 pad-0 m-top-50">

                                                <ul className="list_style alignMiddle">
                                                    <li className="m-rgt-10 displayInline">
                                                        <label className="contribution_mart m-bot-0">
                                                            Data Sync
                                                    </label>
                                                    </li>
                                                    <li className="refresh-table displayInline">
                                                        <img src={refreshIcon} />
                                                        <p className="tooltiptext topToolTipGeneric">
                                                            Refresh
                                                </p>
                                                    </li>
                                                </ul>
                                                {/* <div className="sketchesEmpty"></div> */}
                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 bordere3e7f3 tableGeneric ">
                                                    <div className="zui-wrapper">
                                                        <div className="scrollableTableFixed table-scroll zui-scroller dataSyncTable scrollableOrgansation orgScroll" id="table-scroll">

                                                            <table className="table scrollTable main-table zui-table">
                                                                <thead>
                                                                    <tr>

                                                                        {/* <th>
                                                                            <label>ORGANISATION NAME</label>
                                                                        </th> */}
                                                                        <th>
                                                                            <label>Sno</label>
                                                                        </th>

                                                                        <th>
                                                                            <label>From Date</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>To Date</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Stores </label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Status</label>
                                                                        </th>


                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                    <tr >
                                                                        <td>
                                                                            <label>
                                                                                1
                                                                                </label>
                                                                        </td>
                                                                        <td>
                                                                            <label>
                                                                                2019-08-23
                                                                                </label>
                                                                        </td>
                                                                        <td>
                                                                            <label>
                                                                                2019-09-23
                                                                                </label>
                                                                        </td>
                                                                        <td>
                                                                            <label>GAURIK FASHIONS - CHENNAI FORUM MALL,FIT & STYLE - KHANNA</label>
                                                                        </td>
                                                                        <td>
                                                                            <label>
                                                                                Pending
                                                                                </label>
                                                                        </td>



                                                                    </tr>
                                                                    <tr >
                                                                        <td>
                                                                            <label>
                                                                                2
                                                                                </label>
                                                                        </td>
                                                                        <td>
                                                                            <label>
                                                                                2019-08-23
                                                                                </label>
                                                                        </td>
                                                                        <td>
                                                                            <label>
                                                                                2019-09-23
                                                                                </label>
                                                                        </td>
                                                                        <td>
                                                                            <label>GAURIK FASHIONS - CHENNAI FORUM MALL,FIT & STYLE - KHANNA</label>
                                                                        </td>
                                                                        <td>
                                                                            <label>
                                                                                Pending
                                                                                </label>
                                                                        </td>



                                                                    </tr>


                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="pagerDiv">
                                                    <ul className="list-inline pagination">
                                                        <li>
                                                            <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                                First
                                        </button>
                                                        </li>

                                                        <li>
                                                            <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                                                Prev
                  </button>
                                                        </li>

                                                        {/* {this.state.prev != 0 ? <li >
                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                        Prev
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" disabled>
                                            Prev
                  </button>
                                    </li>} */}
                                                        <li>
                                                            <button className="PageFirstBtn pointerNone">
                                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                                            </button>
                                                        </li>
                                                        {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage ? <li >
                                                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                                Next
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn borderNone" disabled>
                                                                    Next
                  </button>
                                                            </li>}


                                                        {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} closeErrorRequest={(e) => this.onError(e)} errorMessage={this.state.errorMessage} /> : null}
                {this.state.success ? <RequestSuccess closeRequest={(e) => this.onRequest(e)} successMessage={this.state.successMessage} /> : null}

            </div>

        );
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GlobalDataSync);