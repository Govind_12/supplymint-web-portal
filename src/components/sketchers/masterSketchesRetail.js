import React from 'react';
import RightSideBar from "../rightSideBar";
import Footer from '../footer'
import BraedCrumps from "../breadCrumps";
import SideBar from "../sidebar";
// import Footer from '../../footer' 
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import openRack from "../../assets/open-rack.svg";
import shapeError from "../../assets/shape-error.svg";
import cloudIcon from "../../assets/cloud-icon.svg";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import axios, { post } from "axios";
import { CONFIG } from "../../config";
import SmallLoader from '../loaders/smallLoader';
import CheckGreen from "../../assets/greencheck.png";
import refreshIcon from "../../assets/refresh.svg";
import { OganisationIdName } from '../../organisationIdName.js';

class MasterSketches extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgCodeGlobal: "",
            openRightBar: false,
            rightbar: false,
            file: "",
            uploadButton: false,
            channel: "",
            template: "",
            // channelerr: false,
            templateerr: false,
            filePath: "",
            fileerr: false,
            loader: false,
            success: false,
            alert: false,
            errorMessage: "",
            successMessage: "",
            errorCode: "",
            templateData: "",
            downloadLink: null,
            code: "",
            status: "",
            fileData: {},
            fname: "",
            smallLoader: false,
            dataSyncTable: [],
            headers: ""
        }
    }

    fileChange(e) {
        // const formData = new FormData();
        // let name = e.target.files[0];
        this.setState({
            fileData: e.target.files[0],
            fname: e.target.files[0].name,
            downloadLink: null,
            status: "",
            filePath: ""
            // status: "File Uploading"
        })
        // let headers = {
        //     'X-Auth-Token': sessionStorage.getItem('token'),
        //     'Content-Type': 'multipart/form-data'
        // }
        // formData.append('file', name);
        // formData.append('template', this.state.template);
        // axios.post(`${CONFIG.BASE_URL}/aws/s3bucket/upload`, formData, { headers: headers })
        //     .then(res => {
        //         this.setState({
        //             filePath: res.data.data.resource.filePath,
        //             status: "File Uploaded Successfully"
        //         })
        //     }).catch((error) => {
        //         this.setState({
        //             status: "Failed To Upload File"
        //         })
        //     })
    }



    // }
    // upload(e) {
    //     // var imagefile = document.querySelector('#file');
    //     // let a = {"file": imagefile.files[0]}
    // //     const reader = new FileReader();

    // // reader.readAsDataURL(files[0]);
    //     var fileUpload = document.getElementById("fileUpload");
    //     // var reader = new FileReader();
    //     // let data=[];
    //     // reader.onload = function (fileUpload) {

    //     //     data = fileUpload.files
    //     // }        

    //     // let data = {
    //     //     'file': fileUpload
    //     // }

    //     formData.append('file', fileUpload.files);
    //     axios.post('http://192.168.1.136:5000/admin/custom/upload', formData, {
    //         headers: {
    //             'X-Auth-Token': sessionStorage.getItem('token')
    //         }
    //     }).then(res => {
    //         console.log(res);
    //     }).catch((error) => {
    //         console.log(error);

    //     })



    // }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    handleChnage(e) {
        // if (e.target.id == "channel") {
        //     this.setState({
        //         channel: e.target.value,
        //         template: "",
        //         templateData: "",
        //         loader: true,
        //         fname: "",
        //         fileData: {},
        //         downloadLink: null,
        //         status: "",
        //         filePath: ""
        //     },
        //         () => {
        //             this.channel();
        //         })
        //     this.props.getTemplateRequest(e.target.value);
        // } else 
        if (e.target.id == "template") {
            this.setState({
                template: e.target.value,
                fname: "",
                fileData: {},
                downloadLink: null,
                status: "",
                filePath: ""
            },
                () => {
                    this.template();
                })
            let templateData = this.state.templateData
            for (let i = 0; i < templateData.length; i++) {
                if (templateData[i].fullFileName == e.target.value) {
                    this.setState({
                        headers: templateData[i].header
                    })
                }
            }
            this.props.downloadRequest(e.target.value)
        }
    }

    template() {
        if (
            this.state.template == "" || this.state.template.trim() == "") {
            this.setState({
                templateerr: true
            });
        } else {
            this.setState({
                templateerr: false
            });
        }
    }
    // channel() {
    //     if (
    //         this.state.channel == "" || this.state.channel.trim() == "") {
    //         this.setState({
    //             channelerr: true
    //         });
    //     } else {
    //         this.setState({
    //             channelerr: false
    //         });
    //     }
    // }
    file() {
        if (
            this.state.filePath == "" || this.state.filePath.trim() == "") {
            this.setState({
                fileerr: true
            });
        } else {
            this.setState({
                fileerr: false
            });
        }
    }

    onSubmit(e) {
        e.preventDefault();
        // this.channel();
        this.template();
        this.file();
        let t = this;
        setTimeout(function (e) {
            const { channelerr, templateerr, fileerr } = t.state;
            if (!channelerr && !templateerr && !fileerr) {
                let data = {
                    channel: t.state.channel,
                    template: t.state.template,
                    file: t.state.filePath
                }
                t.props.saveSketchersRequest(data)
            }
        }, 100)
    }

    onUploadFile(e) {
        e.preventDefault();
        const formData = new FormData();
        // let name = e.target.files[0];
        this.setState({
            smallLoader: true,
            status: "File Uploading"
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        formData.append('file', this.state.fileData);
        formData.append('template', this.state.template);
        axios.post(`${CONFIG.BASE_URL}/aws/s3bucket/upload`, formData, { headers: headers })
            .then(res => {
                this.setState({
                    smallLoader: false,
                    filePath: res.data.data.resource.filePath,
                    status: "File Uploaded Successfully"
                })
            }).catch((error) => {
                this.setState({
                    smallLoader: false,
                    status: "Failed To Upload File"
                })
            })
    }

    componentWillMount() {
        var { orgCodeGlobal } = OganisationIdName()
        let data = {
            type: 1,
            no: 1,
            search: "",
            channel: orgCodeGlobal
        }
        this.setState({
            channel: orgCodeGlobal,
            orgCodeGlobal: orgCodeGlobal
        })
        this.props.dataSyncRequest(data);

        this.props.getTemplateRequest(orgCodeGlobal);
    }

    componentWillReceiveProps(nextProps) {

        if (!nextProps.administration.saveSketchers.isLoading && !nextProps.administration.saveSketchers.isError && !nextProps.administration.saveSketchers.isSuccess) {

            this.setState({
                loader: false
            })

        }
        if (nextProps.administration.saveSketchers.isSuccess) {
            this.setState({
                loader: false,
                alert: false,
                success: true,
                successMessage: nextProps.administration.saveSketchers.data.message,
                file: "",
                uploadButton: false,
                // channel: "",
                template: "",
                downloadLink: null,
                status: "",
                templateData: "",
                fname: "",
                filePath: "",
                fileData: {},
                smallLoader: false,
            })
            this.props.saveSketchersRequest();
        } else if (nextProps.administration.saveSketchers.isError) {
            this.setState({
                loader: false,
                alert: true,
                success: false,
                code: nextProps.administration.saveSketchers.message.status,
                errorMessage: nextProps.administration.saveSketchers.message.error == undefined ? undefined : nextProps.administration.saveSketchers.message.error.errorMessage,
                errorCode: nextProps.administration.saveSketchers.message.error == undefined ? undefined : nextProps.administration.saveSketchers.message.error.errorCode
            })
            this.props.saveSketchersRequest();
        }
        if (nextProps.administration.getTemplate.isSuccess) {
            this.setState({
                templateData: nextProps.administration.getTemplate.data.resource,
                loader: false
            })
        }
        if (nextProps.administration.download.isSuccess) {
            this.setState({
                loader: false,
                downloadLink: nextProps.administration.download.data.resource
            })
        }

        if (nextProps.administration.dataSync.isSuccess) {
            this.setState({
                loader: false,
                dataSyncTable: nextProps.administration.dataSync.data.resource == null ? [] : nextProps.administration.dataSync.data.resource.slice(0, 5)
            })
        } else if (nextProps.administration.dataSync.isError) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.administration.dataSync.isLoading || nextProps.administration.download.isLoading || nextProps.administration.saveSketchers.isLoading || nextProps.administration.getTemplate.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    onClear() {
        this.setState({
            template: "",
            fname: "",
            fileData: {},
            downloadLink: null,
            status: "",
            filePath: ""
        })
    }

    onRefresh() {

        let data = {
            type: 1,
            no: 1,
            search: "",
            channel: this.state.orgCodeGlobal
        }
        this.props.dataSyncRequest(data);
    }

    render() {

        let { channelerr, templateerr, fileerr } = this.state;
        return (
            <div className="container-fluid">
                <div className="container_div" id="home">
                    <SideBar {...this.props} />
                    <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}

                    <div className="container_div m-top-75 " id="">
                        <div className="col-md-12 col-sm-12">
                            <div className="menu_path">
                                <ul className="list-inline width_100 pad20">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid">
                        <div className="container_div" id="">
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <div className="container-fluid">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="replenishment_container">
                                            <div className="col-md-12 col-sm-12 pad-0">
                                                <ul className="list_style">
                                                    <li>
                                                        <label className="contribution_mart">
                                                            RETAIL MASTER
                                            </label>
                                                    </li>
                                                    <li>
                                                        <p className="master_para">Add Master data for Skechers</p>
                                                    </li>
                                                </ul>
                                            </div>
                                            {this.state.downloadLink == null ? null : this.state.template == "" ? null : <div className="col-md-12 col-sm-12 pad-0">
                                                <div className="sketchesDownload m-top-15">
                                                    <img src={shapeError} />
                                                    <span>Previously Uploaded File Found <a href={this.state.downloadLink.url} download><button type="button">Download Now</button></a></span>
                                                </div>
                                                <div className="fileDetails">
                                                    <p> {this.state.downloadLink.fileName}</p>
                                                    <p>Last Modified: {this.state.downloadLink.modifiedOn}</p>
                                                </div>
                                            </div>}
                                            {/* {this.state.downloadLink == null ? "No Data Found" : 
                                                <a href={this.state.downloadLink} download> Download Link</a>}
                                            </div> : null : null} */}
                                            <div className="col-md-12 col-sm-12 pad-0">
                                                <div className="chooseFileSketches">
                                                    <ul className="pad-0 m-top-30">
                                                        {/* <li><h6>Data Channel</h6> */}
                                                        <div className="form-group">
                                                            {/* <select value={this.state.channel} onChange={(e) => this.handleChnage(e)} id="channel" className={channelerr ? "errorBorder form-control" : "form-control"} >
                                                                    <option value="">Select channel</option>
                                                                    <option value="Common">Common</option>
                                                                    <option value="Retail">Retail</option>
                                                                    <option value="MBO">MBO</option>

                                                                </select>
                                                                {channelerr ? (
                                                                    <span className="error">
                                                                        Select Channel
                                                                    </span>
                                                                ) : null} */}
                                                        </div>
                                                        {/* </li> */}
                                                        <li>
                                                            <h6>Data Template</h6>
                                                            <div className="form-group">
                                                                <select value={this.state.template} onChange={(e) => this.handleChnage(e)} id="template" className={templateerr ? "errorBorder form-control" : "form-control"}>
                                                                    <option value="">Select template</option>
                                                                    {this.state.templateData == "" ? null : this.state.templateData == undefined ? null : this.state.templateData.map((data, key) => (<option key={key} value={data.fullFileName}>{data.fullFileName}</option>))}

                                                                </select>
                                                                {templateerr ? (
                                                                    <span className="error">
                                                                        Select Template
                                                                    </span>
                                                                ) : null}
                                                            </div>
                                                        </li>
                                                        <li>
                                                            {this.state.template != "" ? <h6> </h6> : null}
                                                            {this.state.template != "" ? <div className="sketchesChooseFile">
                                                                <label className="displayPointer">
                                                                    <input className={fileerr ? "errorBorder form-control" : "form-control"} type="file" id="fileUpload" onChange={(e) => this.fileChange(e)} />
                                                                    Choose File  <img src={cloudIcon} />
                                                                </label>
                                                                {this.state.template == "" ? null :
                                                                    <span className="displayBlock uploadStatusMs">{this.state.fname}
                                                                    </span>}
                                                                {/* {this.state.uploadButton ? <button onClick={(e) => this.upload(e)} className="save_button_vendor" type="button">Upload</button> : null} */}
                                                                {fileerr ? (
                                                                    <span className="error">
                                                                        Select File
                                                                    </span>
                                                                ) : null}
                                                            </div> : null}
                                                        </li>
                                                        {this.state.fname != "" || this.state.smallLoader ?
                                                            this.state.smallLoader ?
                                                                <li>
                                                                    <SmallLoader />
                                                                    {this.state.status == "" ? null : <span className="displayBlock uploadStatusMs loaderTextSke">{this.state.status}
                                                                    </span>}
                                                                </li>
                                                                :
                                                                <li>
                                                                    {this.state.status == "File Uploaded Successfully" ? null : <button className="save_button_vendor" onClick={(e) => this.onUploadFile(e)} type="button">Upload</button>}
                                                                    {this.state.status == "" ? null : this.state.status == "Failed To Upload File" ? <span className="displayBlock uploadStatusMs uploadStatusSkc">{this.state.status}
                                                                    </span> : <div className="uploadSuccessIcon"> <img src={CheckGreen} /> <span className="displayBlock uploadStatusMs uploadStatusSkc">{this.state.status}
                                                                    </span> </div>}
                                                                </li> : null}
                                                        {/* {this.state.status == "" ? null : <span className="displayBlock uploadStatusMs">{this.state.status}
                                                        </span>} */}
                                                    </ul>

                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0">
                                                <div className="sketchesNote m-top-25">
                                                    {this.state.headers != "" ? <h5>Note : Expected Column Headers - <span>{this.state.headers}</span></h5> : null}

                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-50">

                                                <ul className="list_style alignMiddle">
                                                    <li className="m-rgt-10 displayInline">
                                                        <label className="contribution_mart m-bot-0">
                                                            Data Sync
                                                    </label>
                                                    </li>
                                                    <li className="refresh-table displayInline">
                                                        <img onClick={() => this.onRefresh()} src={refreshIcon} />
                                                        <p className="tooltiptext topToolTipGeneric">
                                                            Refresh
                                                </p>
                                                    </li>
                                                </ul>
                                                {/* <div className="sketchesEmpty"></div> */}
                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 bordere3e7f3 tableGeneric ">
                                                    <div className="zui-wrapper">
                                                        <div className="scrollableTableFixed table-scroll zui-scroller dataSyncTable scrollableOrgansation orgScroll" id="table-scroll">

                                                            <table className="table scrollTable main-table zui-table">
                                                                <thead>
                                                                    <tr>

                                                                        {/* <th>
                                                                            <label>ORGANISATION NAME</label>
                                                                        </th> */}
                                                                        <th>
                                                                            <label>Name</label>
                                                                        </th>

                                                                        <th>
                                                                            <label>Data Count</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Event Start</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Event End</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Key</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Status</label>
                                                                        </th>

                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    {this.state.dataSyncTable == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.dataSyncTable.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.dataSyncTable.map((data, key) => (
                                                                        <tr key={key}>
                                                                            <td>
                                                                                <label>
                                                                                    {data.name}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.dataCount}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.eventStart}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.eventEnd}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.key}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.status}
                                                                                </label>
                                                                            </td>

                                                                        </tr>
                                                                    ))}

                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 ">
                                                <div className="footerDivForm">
                                                    <ul className="list-inline m-lft-0 pad-0 m-top-15">
                                                        {this.state.template == "" ? null : <li>
                                                            <button type="button" className="clear_button_vendor" onClick={() => this.onClear()}>Clear</button>
                                                        </li>}
                                                        <li>
                                                            {this.state.filePath == "" ? null : <button className="save_button_vendor" type="submit">Save</button>}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} closeErrorRequest={(e) => this.onError(e)} errorMessage={this.state.errorMessage} /> : null}
                {this.state.success ? <RequestSuccess closeRequest={(e) => this.onRequest(e)} successMessage={this.state.successMessage} /> : null}

            </div>

        );
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MasterSketches);