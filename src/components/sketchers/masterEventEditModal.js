import React, { Component } from 'react'

export default class MasterEventEditModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            channel: this.props.editPayload.channel,
            partner: this.props.editPayload.partner,
            zone: this.props.editPayload.zone,
            grade: this.props.editPayload.grade,
            storeCode: this.props.editPayload.storeCode,
            eventName: this.props.editPayload.eventName,
            startDate: this.props.editPayload.startDate,
            endDate: this.props.editPayload.endDate,
            multiplier: this.props.editPayload.multiplierROS,
            stockDays: this.props.editPayload.stockDays,
            id: this.props.editPayload.id,
            eventNameerr: false,
            startDateerr: false,
            endDateerr: false,
            stockDayserr: false,
            multipliererr: false,

        }
    }
    componentWillMount() {
        this.setState({
            channel: this.props.editPayload.channel,
            partner: this.props.editPayload.partner,
            zone: this.props.editPayload.zone,
            grade: this.props.editPayload.grade,
            storeCode: this.props.editPayload.storeCode,
            eventName: this.props.editPayload.eventName,
            startDate: this.props.editPayload.startDate,
            endDate: this.props.editPayload.endDate,
            multiplier: this.props.editPayload.multiplierROS,
            stockDays: this.props.editPayload.stockDays,
            id: this.props.editPayload.id,
        })

    }
    handleChange(e) {
        if (e.target.id == "eventName") {
            this.setState({
                eventName: e.target.value
            }, () => {
                this.eventName();
            }
            );

        } else if (e.target.id == "startDate") {
            this.setState({
                startDate: e.target.value

            }, () => {
                this.startDate();
            }
            );
        } else if (e.target.id == "endDate") {
            this.setState({
                endDate: e.target.value
            }, () => {
                this.endDate();
            }
            );
        } else if (e.target.id == "stockDays") {
            if (e.target.validity.valid) {
                this.setState({
                    stockDays: e.target.value
                }, () => {
                    this.stockDays();
                }
                );
            }
        }
        else if (e.target.id == "multiplier") {
            if (e.target.validity.valid) {
                this.setState({
                    multiplier: e.target.value
                }, () => {
                    this.multiplier();
                }
                );
            }
        }

    }
    onDisCard() {
        this.setState({
            channel: this.props.editPayload.channel,
            partner: this.props.editPayload.partner,
            zone: this.props.editPayload.zone,
            grade: this.props.editPayload.grade,
            storeCode: this.props.editPayload.storeCode,
            eventName: this.props.editPayload.eventName,
            startDate: this.props.editPayload.startDate,
            endDate: this.props.editPayload.endDate,
            multiplier: this.props.editPayload.multiplierROS,
            stockDays: this.props.editPayload.stockDays,
            id: this.props.editPayload.id,
        })
    }

    eventName() {
        if (
            this.state.eventName == "") {
            this.setState({
                eventNameerr: true
            });
        } else {
            this.setState({
                eventNameerr: false
            });
        }
    }

    startDate() {
        if (this.state.startDate == "") {
            this.setState({
                startDateerr: true
            })
        } else {
            this.setState({
                startDateerr: false
            })
        }
    }
    endDate() {
        if (this.state.endDate == "") {
            this.setState({
                endDateerr: true
            })
        } else {
            this.setState({
                endDateerr: false
            })
        }
    }
    stockDays() {
        if (this.state.stockDays == "") {
            this.setState({
                stockDayserr: true
            })
        } else {
            this.setState({
                stockDayserr: false
            })
        }
    }
    multiplier() {
        if (this.state.multiplier == "") {
            this.setState({
                multipliererr: true
            })
        } else {
            this.setState({
                multipliererr: false
            })
        }
    }
    onSave() {
        this.eventName()
        this.startDate()
        this.endDate()
        this.multiplier()
        this.stockDays()
        setTimeout(() => {
            const { endDateerr, eventNameerr, startDateerr, stockDayserr, multipliererr } = this.state
            if (!endDateerr && !eventNameerr && !startDateerr && !stockDayserr && !multipliererr) {
                let payload = {
                    channel: this.state.channel,
                    partner: this.state.partner,
                    zone: this.state.zone,
                    grade: this.state.grade,
                    storeCode: this.state.storeCode,
                    eventName: this.state.eventName,
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    id: this.state.id,
                    stockDays: this.state.stockDays,
                    multiRos: this.state.multiplier,
                    isUpdate: "true",
                    isEventNameChanged:this.props.editPayload.eventName == this.state.eventName?"false":"true"
                }
                this.props.customMasterSkechersRequest(payload)
                this.props.closeEditModal()

            }


        }, 10)
    }
    render() {
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block newPoModal">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content masterEvenEdit modalShow pad-0">
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="modal_Color">
                                    <div className="modal-top alignMiddle">
                                        <div className="col-md-6 pad-0">
                                            <h2 className="select_name-content m0">Edit Event Master</h2>
                                        </div>
                                        <div className="col-md-6 pad-0 text-right">
                                            <button type="button" id="closeButton" className="discardBtns m-rgt-10" onClick={(e) => this.onDisCard(e)}>Discard</button>
                                            <button type="button" id="closeButton" className="discardBtns m-rgt-10" onClick={(e) => this.props.closeEditModal(e)}>Close</button>

                                            <button type="button" id="saveButton" className="saveBtnBlue" onClick={(e) => this.onSave(e)}>Save</button>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 displayInline modalMidPad">
                                        <div className="modal-content modalMid pad-0">

                                            <form className="m-bot-0 m-top-15">
                                                <div className="col-md-12 pad-0">

                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Channel</label>
                                                        <input type="text" className="width_90 inputGenericEvent" value={this.state.channel} autoComplete="off" disabled />
                                                    </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Partner</label>
                                                        <input type="text" className="width_90 inputGenericEvent" value={this.state.partner} autoComplete="off" disabled />
                                                    </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Zone</label>
                                                        <input type="text" className="width_90 inputGenericEvent" value={this.state.zone} autoComplete="off" disabled />                                                  </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Grade</label>
                                                        <input type="text" className="width_90 inputGenericEvent" value={this.state.grade} autoComplete="off" disabled />                                                    </div>
                                                </div>
                                                <div className="col-md-12 pad-0 m-top-25">
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Store</label>
                                                        <input type="text" className="width_90 inputGenericEvent text-ellipse" value={this.state.storeCode} autoComplete="off" disabled />
                                                    </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Event Name</label>
                                                        <input type="text" className="width_90 inputGenericEvent" placeholder="Enter Event Name" id="eventName" onChange={(e) => this.handleChange(e)} value={this.state.eventName} autoComplete="off" />
                                                        {this.state.eventNameerr ? (
                                                            <span className="error" >
                                                                Enter Event Name
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Start Date</label>
                                                        <input type="date" className="search-box width_90 inputGenericEvent"  placeholder={this.state.startDate!=""?this.state.startDate:"Select Start Date"} id="startDate" onChange={(e) => this.handleChange(e)} value={this.state.startDate} />
                                                        {this.state.startDateerr ? (
                                                            <span className="error" >
                                                                Select Start Date
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">End Date</label>
                                                        <input type="date" className="search-box width_90 inputGenericEvent" id="endDate" placeholder={this.state.endDate!=""?this.state.endDate:"Select End Date"} min={this.state.startDate} onChange={(e) => this.handleChange(e)} value={this.state.endDate} />
                                                        {this.state.endDateerr ? (
                                                            <span className="error" >
                                                                Select End Date
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="col-md-12 pad-0 m-top-25">
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">No. of days additional stock cover</label>
                                                        <input type="text" className="width_90 inputGenericEvent" placeholder="Enter Stock Days" id="stockDays" onChange={(e) => this.handleChange(e)} value={this.state.stockDays} autoComplete="off" />
                                                        {this.state.stockDayserr ? (
                                                            <span className="error" >
                                                                Enter Stock Days
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                    <div className="col-md-3">
                                                        <label className="labelEvent">Multiplier for ROS</label>
                                                        <input type="text" className="width_90 inputGenericEvent" placeholder="Enter Multiplier" id="multiplier" onChange={(e) => this.handleChange(e)} value={this.state.multiplier} autoComplete="off" />
                                                        {this.state.multipliererr ? (
                                                            <span className="error" >
                                                                Enter Multiplier
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}
