import React, { Component } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import BraedCrumps from '../breadCrumps';
import RightSideBar from '../rightSideBar';
import Footer from '../footer'
import SideBar from '../sidebar';
import openRack from "../../assets/open-rack.svg";
import arrowIcon from "../../assets/selectIcon.svg";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import { OganisationIdName } from '../../organisationIdName.js';
import MasterEventEditModal from './masterEventEditModal';
import MasterEventFilter from './masterEventFilter';
import delIcon from '../../assets/new-delete.svg';
import editIcon from '../../assets/edit.svg';
import moment from 'moment';
import AllStoresModal from './allStoresModal';
import ovalIcon from '../../assets/oval-loader.svg';
import uploadedIcon from '../../assets/cloud-white.svg';
import PoError from '../loaders/poError';
import axios, { post } from "axios";
import { CONFIG } from "../../config";
import refreshIcon from "../../assets/refresh.svg";



class MasterEvents extends Component {

    constructor(props) {
        super(props);
        this.state = {
            editPayload: {},
            editModal: false,
            getAllEvents: [],
            filterBar: false,
            filterModal: false,
            getAllSearch: "",
            orgNameGlobal: "",
            orgIdGlobal: "",
            orgCodeGlobal: "",
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            roleData: [],
            zone: [],
            zoneData: [],
            channel: "",
            channelCode: "",
            gradeData: [],
            partner: "",
            partnerData: [],
            grade: [],
            selected: [],
            search: "",
            selectederr: "",
            filter: true,
            remove: false,

            errorCode: "",
            succeeded: false,
            code: "",
            modalState: "",
            multiplier: "",
            filterStores: "",
            eventName: "",
            startDate: "",
            endDate: "",
            noOfDays: "",
            eventNameerr: false,
            startDateerr: false,
            endDateerr: false,
            stockDayserr: false,
            multipliererr: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            filterStartDate: "",
            filterEndDate: "",
            filterMultiplierROS: "",
            filterStockDays: "",
            allStoreModal: false,
            selectedStores: [],
            manualExcelUpload: "manual",
            file: [],
            errorMassage: "",
            customError: false
        }
    }
    closeErrorRequest() {
        this.setState({
            customError: false
        })
    }
    eventName() {
        if (
            this.state.eventName == "") {
            this.setState({
                eventNameerr: true
            });
        } else {
            this.setState({
                eventNameerr: false
            });
        }
    }

    startDate() {
        if (this.state.startDate == "") {
            this.setState({
                startDateerr: true
            })
        } else {
            this.setState({
                startDateerr: false
            })
        }
    }
    endDate() {
        if (this.state.endDate == "") {
            this.setState({
                endDateerr: true
            })
        } else {
            this.setState({
                endDateerr: false
            })
        }
    }
    stockDays() {
        if (this.state.noOfDays == "") {
            this.setState({
                stockDayserr: true
            })
        } else {
            this.setState({
                stockDayserr: false
            })
        }
    }
    multiplier() {
        if (this.state.multiplier == "") {
            this.setState({
                multipliererr: true
            })
        } else {
            this.setState({
                multipliererr: false
            })
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    componentWillMount() {

        var { orgIdGlobal, orgNameGlobal, orgCodeGlobal } = OganisationIdName()
        this.setState({
            orgNameGlobal: orgNameGlobal,
            orgIdGlobal: orgIdGlobal,
            channel: orgNameGlobal,
            orgCodeGlobal: orgCodeGlobal

        })
        let data = {
            channel: orgCodeGlobal,
        }
        let zonePayload = {
            channel: orgCodeGlobal,
            partner: ""
        }
        let gradeData = {
            channel: orgCodeGlobal,
            partner: "",
            zone: []
        }
        let storedata = {
            channel: orgCodeGlobal,
            partner: "",
            zone: [],
            grade: []
        }


        this.props.customGetPartnerRequest(data)
        this.props.customGetZoneRequest(zonePayload)
        this.props.customGetGradeRequest(gradeData)
        this.props.customGetStoreCodeRequest(storedata)
        let payload = {
            no: 1,
            type: 1,
            search: "",
            eventName: "",
            startDate: "",
            endDate: "",
            multiplierROS: "",
            stockDays: "",
            grade: "",
            partner: "",
            zone: "",
            storeCode: ""
        }
        this.props.customGetEventRequest(payload)

    }

    UNSAFE_componentWillReceiveProps(nextProps) {

        if (nextProps.administration.customGetPartner.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customGetPartner.isSuccess) {
            if (nextProps.administration.customGetPartner.data.resource != null) {
                this.setState({
                    loader: false,
                    partnerData: nextProps.administration.customGetPartner.data.resource.partner,
                    roleData: nextProps.administration.customGetPartner.data.resource.store
                })
            }
            this.props.customGetPartnerClear();
        } else if (nextProps.administration.customGetPartner.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customGetPartner.message.status,
                errorMessage: nextProps.administration.customGetPartner.message.error == undefined ? undefined : nextProps.administration.customGetPartner.message.error.errorMessage,
                errorCode: nextProps.administration.customGetPartner.message.error == undefined ? undefined : nextProps.administration.customGetPartner.message.error.errorCode
            })
            this.props.customGetPartnerClear();
        }
        //zone
        if (nextProps.administration.customGetZone.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customGetZone.isSuccess) {
            if (nextProps.administration.customGetZone.data.resource != null) {
                this.setState({
                    loader: false,
                    zoneData: nextProps.administration.customGetZone.data.resource.zone,
                    roleData: nextProps.administration.customGetZone.data.resource.store
                })
            }
            this.props.customGetZoneClear();
        } else if (nextProps.administration.customGetZone.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customGetZone.message.status,
                errorMessage: nextProps.administration.customGetZone.message.error == undefined ? undefined : nextProps.administration.customGetZone.message.error.errorMessage,
                errorCode: nextProps.administration.customGetZone.message.error == undefined ? undefined : nextProps.administration.customGetZone.message.error.errorCode
            })
            this.props.customGetZoneClear();
        }
        //grade
        if (nextProps.administration.customGetGrade.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customGetGrade.isSuccess) {

            if (nextProps.administration.customGetGrade.data.resource != null) {
                this.setState({
                    loader: false,
                    gradeData: nextProps.administration.customGetGrade.data.resource.grade,
                    roleData: nextProps.administration.customGetGrade.data.resource.store
                })
            }
            this.props.customGetGradeClear();
        } else if (nextProps.administration.customGetGrade.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customGetGrade.message.status,
                errorMessage: nextProps.administration.customGetGrade.message.error == undefined ? undefined : nextProps.administration.customGetGrade.message.error.errorMessage,
                errorCode: nextProps.administration.customGetGrade.message.error == undefined ? undefined : nextProps.administration.customGetGrade.message.error.errorCode
            })
            this.props.customGetGradeClear();
        }
        //store
        if (nextProps.administration.customGetStoreCode.isSuccess) {
            if (nextProps.administration.customGetStoreCode.data.resource != null) {
                this.setState({
                    loader: false,
                    roleData: nextProps.administration.customGetStoreCode.data.resource.store
                })
            }
            this.props.customGetStoreCodeClear();
        } else if (nextProps.administration.customGetStoreCode.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customGetStoreCode.message.status,
                errorMessage: nextProps.administration.customGetStoreCode.message.error == undefined ? undefined : nextProps.administration.customGetStoreCode.message.error.errorMessage,
                errorCode: nextProps.administration.customGetStoreCode.message.error == undefined ? undefined : nextProps.administration.customGetStoreCode.message.error.errorCode
            })
            this.props.customGetStoreCodeClear();
        }
        // if (nextProps.administration.customGetZone.isLoading || nextProps.administration.customGetPartner.isLoading || nextProps.administration.customGetGrade.isLoading || nextProps.administration.customGetStoreCode.isLoading) {
        //     this.setState({
        //         loader: true
        //     })
        // }
        if (nextProps.administration.customApplyFilter.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customApplyFilter.isSuccess) {
            this.setState({
                loader: false,

                filterStores: nextProps.administration.customApplyFilter.data.resource == null ? "" : nextProps.administration.customApplyFilter.data.resource.store
            })


            setTimeout(() => {

                this.finalSave()
            }, 10)
            this.props.customApplyFilterClear();
        } else if (nextProps.administration.customApplyFilter.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customApplyFilter.message.status,
                errorMessage: nextProps.administration.customApplyFilter.message.error == undefined ? undefined : nextProps.administration.customApplyFilter.message.error.errorMessage,
                errorCode: nextProps.administration.customApplyFilter.message.error == undefined ? undefined : nextProps.administration.customApplyFilter.message.error.errorCode
            })
            this.props.customApplyFilterClear();
        }
        if (nextProps.administration.customMasterSkechers.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customMasterSkechers.isSuccess) {
            this.setState({
                loader: false,
                alert: false,
                success: true,
                successMessage: nextProps.administration.customMasterSkechers.data.message,

            })
            this.onClear();
            let payload = {
                no: 1,
                type: 1,
                search: "",
                eventName: "",
                startDate: "",
                endDate: "",
                multiplierROS: "",
                stockDays: "",
                grade: "",
                partner: "",
                zone: "",
                storeCode: ""
            }
            this.props.customGetEventRequest(payload)
            this.props.customMasterSkechersClear();

        } else if (nextProps.administration.customMasterSkechers.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customMasterSkechers.message.status,
                errorMessage: nextProps.administration.customMasterSkechers.message.error == undefined ? undefined : nextProps.administration.customMasterSkechers.message.error.errorMessage,
                errorCode: nextProps.administration.customMasterSkechers.message.error == undefined ? undefined : nextProps.administration.customMasterSkechers.message.error.errorCode
            })
            this.props.customMasterSkechersClear();
        }


        if (nextProps.administration.customGetEvent.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customGetEvent.isSuccess) {

            this.setState({
                loader: false,
                alert: false,



            })
            if (nextProps.administration.customGetEvent.data.resource != null) {
                this.setState({
                    getAllEvents: nextProps.administration.customGetEvent.data.resource,
                    prev: nextProps.administration.customGetEvent.data.prePage,
                    current: nextProps.administration.customGetEvent.data.currPage,
                    next: nextProps.administration.customGetEvent.data.currPage + 1,
                    maxPage: nextProps.administration.customGetEvent.data.maxPage,

                })
            } else {
                this.setState({
                    getAllEvents: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0

                })
            }
            this.props.customGetEventClear();
        }
        else if (nextProps.administration.customGetEvent.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customGetEvent.message.status,
                errorMessage: nextProps.administration.customGetEvent.message.error == undefined ? undefined : nextProps.administration.customGetEvent.message.error.errorMessage,
                errorCode: nextProps.administration.customGetEvent.message.error == undefined ? undefined : nextProps.administration.customGetEvent.message.error.errorCode
            })
            this.props.customGetEventClear();
        }

        if (nextProps.administration.customDeleteEvent.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.customDeleteEvent.isSuccess) {
            this.setState({
                loader: false,
                alert: false,
                success: true,
                successMessage: nextProps.administration.customDeleteEvent.data.message,

            })
            let payload = {
                no: 1,
                type: 1,
                search: "",
                eventName: "",
                startDate: "",
                endDate: "",
                multiplierROS: "",
                stockDays: "",
                grade: "",
                partner: "",
                zone: "",
                storeCode: ""
            }
            this.props.customGetEventRequest(payload)
            this.props.customDeleteEventClear();

        } else if (nextProps.administration.customDeleteEvent.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.customDeleteEvent.message.status,
                errorMessage: nextProps.administration.customDeleteEvent.message.error == undefined ? undefined : nextProps.administration.customDeleteEvent.message.error.errorMessage,
                errorCode: nextProps.administration.customDeleteEvent.message.error == undefined ? undefined : nextProps.administration.customDeleteEvent.message.error.errorCode
            })
            this.props.customDeleteEventClear();
        }

    }
    Change(e) {
        this.setState({
            search: e.target.value
        })
    }
    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true
            });
        } else {
            this.setState({
                selectederr: false
            })
        }
    }
    onnChange(id) {

        let match = _.find(this.state.roleData, function (o) {
            return o.storeCode == id;
        })
        this.setState({
            selected: this.state.selected.concat(match)
        });
        let idd = match.storeCode;
        var array = this.state.roleData;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            roleData: array,
            selectederr: false,
            filter: true,
            remove: false
        })

    }
    onDelete(e) {
        let match = _.find(this.state.selected, function (o) {
            return o.storeCode == e;
        })
        this.setState({
            roleData: this.state.roleData.concat(match)
        });

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].storeCode === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            filter: true,
            remove: false
        })
        if (this.props.administration.customGetZone.isSuccess && !this.props.administration.customGetGrade.isSuccess && !this.props.administration.customGetStoreCode.isSuccess) {
            this.props.administration.customGetZone.data.resource.store = this.state.roleData.concat(match)
        } else if (this.props.administration.customGetGrade.isSuccess && !this.props.administration.customGetStoreCode.isSuccess) {
            this.props.administration.customGetGrade.data.resource.store = this.state.roleData.concat(match)
        } else if (this.props.administration.customGetStoreCode.isSuccess) {
            this.props.administration.customGetStoreCode.data.resource.store = this.state.roleData.concat(match);
        }

    }
    handleChange(e) {
        if (e.target.id == "channel") {
            this.setState({
                channel: e.target.value,
                partner: "",
                partnerData: [],
                zone: [],
                grade: [],
                zoneData: [],
                gradeData: [],
                selected: [],
                filter: true,
                remove: false,
            })
            let data = {
                channel: e.target.value
            }
            let zonePayload = {
                channel: e.target.value,
                partner: ""
            }
            let gradeData = {
                channel: e.target.value,
                partner: "",
                zone: []
            }
            let storedata = {
                channel: e.target.value,
                partner: "",
                zone: [],
                grade: []
            }
            this.props.customGetPartnerRequest(data);
            this.props.customGetZoneRequest(zonePayload);
            this.props.customGetGradeRequest(gradeData);
            this.props.customGetStoreCodeRequest(storedata);
        } else if (e.target.id == "partner") {
            this.setState({
                partner: e.target.value,
                zone: [],
                grade: [],
                zoneData: [],
                gradeData: [],
                selected: [],
                filter: true,
                remove: false,
            })
            let data = {
                channel: this.state.orgCodeGlobal,
                partner: e.target.value
            }
            let gradeData = {
                channel: this.state.orgCodeGlobal,
                partner: e.target.value,
                zone: []
            }
            let storedata = {
                channel: this.state.orgCodeGlobal,
                partner: e.target.value,
                zone: [],
                grade: []
            }
            this.props.customGetZoneRequest(data);
            this.props.customGetGradeRequest(gradeData);
            this.props.customGetStoreCodeRequest(storedata);
        }
        // else if (e.target.id == "zone") {
        //     this.setState({
        //         zone: e.target.value,
        //         grade: "",
        //         gradeData: [],
        //         selected: [],
        //         filter: true,
        //         remove: false,
        //     })
        //     let data = {
        //         channel: this.state.orgCodeGlobal,
        //         partner: this.state.partner,
        //         zone: e.target.value
        //     }
        //     let storedata = {
        //         channel: this.state.orgCodeGlobal,
        //         partner: this.state.partner,
        //         zone: e.target.value,
        //         grade: ""
        //     }
        //     this.props.customGetGradeRequest(data);
        //     this.props.customGetStoreCodeRequest(storedata);
        // } else if (e.target.id == "grade") {
        //     this.setState({
        //         grade: e.target.value,
        //         selected: [],
        //         filter: true,
        //         remove: false,
        //     })
        //     let data = {
        //         channel: this.state.orgCodeGlobal,
        //         partner: this.state.partner,
        //         zone: this.state.zone,
        //         grade: e.target.value
        //     }
        //     this.props.customGetStoreCodeRequest(data);
        // } 
        else if (e.target.id == "eventName") {
            this.setState({
                eventName: e.target.value
            }, () => {
                this.eventName();
            }
            );

        } else if (e.target.id == "startDate") {
            this.setState({
                startDate: e.target.value

            }, () => {
                this.startDate();
            }
            );
        } else if (e.target.id == "endDate") {
            this.setState({
                endDate: e.target.value
            }, () => {
                this.endDate();
            }

            );
        } else if (e.target.id == "noOfDays") {
            if (e.target.validity.valid) {
                this.setState({
                    noOfDays: e.target.value
                },
                    () => {
                        this.stockDays();
                    }

                );

            }
        }
        else if (e.target.id == "multiplier") {
            if (e.target.validity.valid) {
                this.setState({
                    multiplier: e.target.value
                },
                    () => {
                        this.multiplier();
                    }
                );
            }




        }

    }
    onSubmit(e) {
        e.preventDefault()
        this.endDate()
        this.startDate()
        this.eventName()
        this.stockDays()
        this.multiplier()

        setTimeout(() => {

            const { eventNameerr, startDateerr, endDateerr, stockDayserr, multipliererr } = this.state;
            if (!eventNameerr && !startDateerr && !endDateerr && !stockDayserr && !multipliererr) {
                if (this.state.selected.length == 0) {

                    let data = {
                        channel: this.state.orgCodeGlobal,
                        zone: this.state.zone.length == 0 ? [] : this.state.zone,
                        grade: this.state.grade.length == 0 ? [] : this.state.grade,
                        partner: this.state.partner == "" ? "" : this.state.partner,
                        storeCode: "NA"
                    }
                    this.props.customApplyFilterRequest(data);
                } else {
                    let abc = "";
                    for (let i = 0; i < this.state.selected.length; i++) {
                        let a = this.state.selected[i].storeCode;
                        if (this.state.selected.length > 1) {
                            abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
                        } else {
                            abc = a;
                        }
                    }
                    let data = {
                        channel: this.state.orgCodeGlobal,
                        zone: this.state.zone.length == 0 ? [] : this.state.zone,
                        grade: this.state.grade.length == 0 ? [] : this.state.grade,
                        partner: this.state.partner == "" ? "" : this.state.partner,
                        storeCode: abc
                    }
                    this.props.customApplyFilterRequest(data);
                }

            }

        }, 10)


    }
    finalSave() {

        let payload = {
            channel: this.state.orgCodeGlobal,
            partner: this.state.partner == "" ? "NA" : this.state.partner,
            zone: this.state.zone.length == 0 ? "NA" : this.state.zone.join('|'),
            grade: this.state.grade.length == 0 ? "NA" : this.state.grade.join('|'),
            storeCode: this.state.filterStores == "" ? "NA" : this.state.filterStores,
            eventName: this.state.eventName,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            id: "NA",
            stockDays: this.state.noOfDays,
            multiRos: this.state.multiplier,
            isUpdate: "false",
            isEventNameChanged: "false"
        }
        this.props.customMasterSkechersRequest(payload)


    }
    onClear() {
        this.setState({
            roleData: [],
            zone: [],
            zoneData: [],

            gradeData: [],
            partner: "",
            partnerData: [],
            grade: [],
            selected: [],
            filterStores: "",
            eventName: "",
            startDate: "",
            endDate: "",
            noOfDays: "",
            multiplier: "",
            eventNameerr: false,
            startDateerr: false,
            endDateerr: false,
            stockDayserr: false,
            multipliererr: false

        })
        let data = {
            channel: this.state.orgCodeGlobal,
        }
        let zonePayload = {
            channel: this.state.orgCodeGlobal,
            partner: ""
        }
        let gradeData = {
            channel: this.state.orgCodeGlobal,
            partner: "",
            zone: []
        }
        let storedata = {
            channel: this.state.orgCodeGlobal,
            partner: "",
            zone: [],
            grade: []
        }


        this.props.customGetPartnerRequest(data)
        this.props.customGetZoneRequest(zonePayload)
        this.props.customGetGradeRequest(gradeData)
        this.props.customGetStoreCodeRequest(storedata)


    }


    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {

                this.setState({
                    prev: this.props.administration.customGetEvent.data.prePage,
                    current: this.props.administration.customGetEvent.data.currPage,
                    next: this.props.administration.customGetEvent.data.currPage + 1,
                    maxPage: this.props.administration.customGetEvent.data.maxPage,
                })
                if (this.props.administration.customGetEvent.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.administration.customGetEvent.data.currPage - 1,

                        search: this.state.getAllSearch,
                        eventName: "",
                        startDate: this.state.filterStartDate,
                        endDate: this.state.filterEndDate,
                        multiplierROS: this.state.filterMultiplierROS,
                        stockDays: this.state.filterStockDays,
                        grade: "",
                        partner: "",
                        zone: "",
                        storeCode: ""
                    }
                    this.props.customGetEventRequest(data);
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.customGetEvent.data.prePage,
                current: this.props.administration.customGetEvent.data.currPage,
                next: this.props.administration.customGetEvent.data.currPage + 1,
                maxPage: this.props.administration.customGetEvent.data.maxPage,
            })
            if (this.props.administration.customGetEvent.data.currPage != this.props.administration.customGetEvent.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.administration.customGetEvent.data.currPage + 1,
                    search: this.state.getAllSearch,
                    eventName: "",
                    startDate: this.state.filterStartDate,
                    endDate: this.state.filterEndDate,
                    multiplierROS: this.state.filterMultiplierROS,
                    stockDays: this.state.filterStockDays,
                    grade: "",
                    partner: "",
                    zone: "",
                    storeCode: ""
                }
                this.props.customGetEventRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.administration.customGetEvent.data.prePage,
                    current: this.props.administration.customGetEvent.data.currPage,
                    next: this.props.administration.customGetEvent.data.currPage + 1,
                    maxPage: this.props.administration.customGetEvent.data.maxPage,
                })
                if (this.props.administration.customGetEvent.data.currPage <= this.props.administration.customGetEvent.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.getAllSearch,
                        eventName: "",
                        startDate: this.state.filterStartDate,
                        endDate: this.state.filterEndDate,
                        multiplierROS: this.state.filterMultiplierROS,
                        stockDays: this.state.filterStockDays,
                        grade: "",
                        partner: "",
                        zone: "",
                        storeCode: ""
                    }
                    this.props.customGetEventRequest(data)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.administration.customGetEvent.data.prePage,
                    current: this.props.administration.customGetEvent.data.currPage,
                    next: this.props.administration.customGetEvent.data.currPage + 1,
                    maxPage: this.props.administration.customGetEvent.data.maxPage,
                })
                if (this.props.administration.customGetEvent.data.currPage <= this.props.administration.customGetEvent.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.administration.customGetEvent.data.maxPage,
                        search: this.state.getAllSearch,
                        eventName: "",
                        startDate: this.state.filterStartDate,
                        endDate: this.state.filterEndDate,
                        multiplierROS: this.state.filterMultiplierROS,
                        stockDays: this.state.filterStockDays,
                        grade: "",
                        partner: "",
                        zone: "",
                        storeCode: ""
                    }
                    this.props.customGetEventRequest(data)
                }

            }

        }
    }

    handleFilter = () => {
        this.setState({
            filterModal: !this.state.filterModal,
            filterBar: !this.state.filterBar
        })
    }
    closeFilter() {
        this.setState({
            filterModal: false,
            filterBar: false
        })
    }
    delRequest(eventName) {

        let payload = {
            "eventName": eventName
        }
        this.props.customDeleteEventRequest(payload)

    }

    clearFilter(e) {
        this.setState({
            filterStartDate: "",
            filterEndDate: "",
            filterMultiplierROS: "",
            filterStockDays: ""
        })
    }

    handleChangeFilter(e) {
        if (e.target.id == "filterStartDate") {
            this.setState({
                filterStartDate: e.target.value
            });
        } else if (e.target.id == "filterEndDate") {
            this.setState({
                filterEndDate: e.target.value
            });
        } else if (e.target.id == "filterMultiplierROS") {
            if (e.target.validity.valid) {
                this.setState({
                    filterMultiplierROS: e.target.value
                });
            }
        } else if (e.target.id == "filterStockDays") {
            if (e.target.validity.valid) {
                this.setState({
                    filterStockDays: e.target.value
                });
            }
        }
    }
    onFilterSubmit() {
        this.closeFilter()
        this.setState({
            getAllSearch: "",
            type: 2
        })
        let payload = {
            no: 1,
            type: 2,
            search: "",
            eventName: "",
            startDate: this.state.filterStartDate,
            endDate: this.state.filterEndDate,
            multiplierROS: this.state.filterMultiplierROS,
            stockDays: this.state.filterStockDays,
            grade: "",
            partner: "",
            zone: "",
            storeCode: ""
        }
        this.props.customGetEventRequest(payload)

    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    getAllSearchChange(e) {
        this.setState({
            getAllSearch: e.target.value
        })

    }
    onSearch(e) {
        this.setState({
            type: 3
        })

        let payload = {
            no: 1,
            type: 3,
            search: this.state.getAllSearch,
            eventName: "",
            startDate: this.state.filterStartDate,
            endDate: this.state.filterEndDate,
            multiplierROS: this.state.filterMultiplierROS,
            stockDays: this.state.filterStockDays,
            grade: "",
            partner: "",
            zone: "",
            storeCode: ""
        }
        this.props.customGetEventRequest(payload)

    }
    onClearSearch() {
        this.setState({
            type: 1,
            getAllSearch: "",
        })

        let payload = {
            no: 1,
            type: 1,
            search: "",
            eventName: "",
            startDate: this.state.filterStartDate,
            endDate: this.state.filterEndDate,
            multiplierROS: this.state.filterMultiplierROS,
            stockDays: this.state.filterStockDays,
            grade: "",
            partner: "",
            zone: "",
            storeCode: ""
        }
        this.props.customGetEventRequest(payload)
    }
    onFilterClear() {

        this.setState({
            type: 1,
            getAllSearch: "",
            filterStartDate: "",
            filterEndDate: "",
            filterMultiplierROS: "",
            filterStockDays: ""
        })

        let payload = {
            no: 1,
            type: 1,
            search: "",
            eventName: "",
            startDate: "",
            endDate: "",
            multiplierROS: "",
            stockDays: "",
            grade: "",
            partner: "",
            zone: "",
            storeCode: ""
        }
        this.props.customGetEventRequest(payload)

    }

    onOpenEditModal(id) {
        let getAllEvents = this.state.getAllEvents
        let editPayload = {}
        for (let i = 0; i < getAllEvents.length; i++) {
            if (getAllEvents[i].id == id) {
                editPayload.eventName = getAllEvents[i].eventName
                editPayload.grade = getAllEvents[i].grade
                editPayload.id = id
                editPayload.zone = getAllEvents[i].zone
                editPayload.storeCode = getAllEvents[i].storeCode
                editPayload.stockDays = getAllEvents[i].stockDays
                editPayload.channel = getAllEvents[i].channel
                editPayload.endDate = moment(getAllEvents[i].endDate).format("YYYY-MM-DD")
                editPayload.startDate = moment(getAllEvents[i].startDate).format("YYYY-MM-DD")
                editPayload.grade = getAllEvents[i].grade
                editPayload.multiplierROS = getAllEvents[i].multiplierROS
                editPayload.partner = getAllEvents[i].partner



            }
        }

        this.setState({
            editModal: true,
            editPayload: editPayload
        })

    }
    closeEditModal() {
        this.setState({
            editModal: false
        })
    }
    handleModal = (e) => {
        if (e.target.dataset.stores != undefined) {
            var storeArray = e.target.dataset.stores.split('|')
        }
        if (e.target.id == "allStore") {
            this.setState({
                allStoreModal: !this.state.allStoreModal,
                selectedStores: storeArray
            })
        }
    }
    handleMasterUpload = (e) => {
        if (e.target.id == "manual") {
            this.setState({
                manualExcelUpload: "manual"
            })
        } else if (e.target.id == "excel") {
            this.setState({
                manualExcelUpload: "excel"
            })
        }
    }
    ZoneChange(e) {
        let data = e.target.value
        let zone = []
        if (e.target.value != "") {
            zone.push(data)


        }
        this.setState({
            zone,


            grade: [],
            gradeData: [],
            selected: [],
            filter: true,
            remove: false,
        }, () => {
            let data = {
                channel: this.state.orgCodeGlobal,
                partner: this.state.partner,
                zone: this.state.zone
            }
            let storedata = {
                channel: this.state.orgCodeGlobal,
                partner: this.state.partner,
                zone: this.state.zone,
                grade: this.state.grade
            }

            this.props.customGetGradeRequest(data);
            this.props.customGetStoreCodeRequest(storedata);
        })

    }

    gradeChange(e) {
        let gradeData = e.target.value
        let grade = []
        if (e.target.value != "") {
            grade.push(gradeData)
        }

        this.setState({
            grade,

            selected: [],
            filter: true,
            remove: false,
        }, () => {

            let data = {
                channel: this.state.orgCodeGlobal,
                partner: this.state.partner,
                zone: this.state.zone,
                grade: this.state.grade
            }
            this.props.customGetStoreCodeRequest(data);
        })


    }
    fileChange(e) {

        let values = Object.values(e.target.files)

        if (values[0].type == "text/csv" || values[0].type == "application/vnd.ms-excel") {
            e.target.value = ""
            this.setState({
                file: values
            })
        } else {
            e.target.value = ""
            this.setState({
                customError: true,
                errorMassage: "Please upload only CSV"
            })
        }
    }
    uploadCsv() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }

        if (this.state.file.length == 0) {
            this.setState({
                errorMassage: "Select File",
                customError: true
            })

        }
        else {
            this.setState({
                loader: true

            })
            const fd = new FormData();
            let count = 0
            let files = this.state.file


            for (var i = 0; i < files.length; i++) {

                count++

                fd.append("fileData", files[i]);
                fd.append('templateName', "EVENT_MASTER")

            }
            let fileData = this.state.file

            if (count > 0) {
                axios.post(`${CONFIG.BASE_URL}/upload/data/sync`, fd, { headers: headers })
                    .then(res => {


                        if (res.data.status == "2000") {

                            this.setState({
                                file: [],
                                success: true,

                                successMessage: "File uploaded succesfully ,Please refresh table in a while",
                                loader: false

                            })
                            let payload = {
                                no: 1,
                                type: 1,
                                search: "",
                                eventName: "",
                                startDate: "",
                                endDate: "",
                                multiplierROS: "",
                                stockDays: "",
                                grade: "",
                                partner: "",
                                zone: "",
                                storeCode: ""
                            }
                            this.props.customGetEventRequest(payload)
                        } else {

                            this.setState({
                                errorMessage: res.data.error == undefined ? undefined : res.data.error.errorMessage,
                                errorCode: res.data.error == undefined ? undefined : res.data.error.errorCode,
                                code: res.data.status,
                                alert: true,
                                loader: false
                            })

                        }



                    }).catch((error) => {
                        this.setState({
                            loader: false
                        })
                    })

            }
        }
    }
    onRefresh() {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            eventName: "",
            startDate: "",
            endDate: "",
            multiplierROS: "",
            stockDays: "",
            grade: "",
            partner: "",
            zone: "",
            storeCode: ""
        }
        this.props.customGetEventRequest(payload)
    }

    render() {

        const { search, roleData, selectederr, eventNameerr, startDateerr, endDateerr, multipliererr, stockDayserr, manualExcelUpload } = this.state;
        var result = _.filter(roleData, function (data) {
            return _.startsWith(data.storeName.toLowerCase(), search.toLowerCase());
        });
        return (
            <div className="container-fluid">
                <div className="container_div" id="home">
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}

                    <div className="container_div m-top-75 " id="">
                        <div className="col-md-12 col-sm-12">
                            <div className="menu_path">
                                <ul className="list-inline width_100 pad20">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid">
                        <div className="container_div" id="">
                            <div className="container-fluid">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="administration_container width100 pad-0 masterEventContainer">
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <div className="container-top displayInline width100">
                                                <div className="col-md-12 pad-0 alignMiddle">
                                                    <div className="col-md-2 pad-0">
                                                        <ul className="list_style m0">
                                                            <li>
                                                                <label className="contribution_mart">
                                                                    MASTER <span>( EVENTS )</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="col-md-10 pad-0 procurementSetting">
                                                        <div className="card-content newRadioBtns pad-0">
                                                            <ul className="pad-0">
                                                                <li className="m-rgt-30">
                                                                    <label className="select_modalRadio">
                                                                        <input type="radio" name="frequency" id="manual" checked={this.state.manualExcelUpload == "manual" ? true : false} onChange={this.handleMasterUpload} /><span className="text">Manual Entry</span>
                                                                        <span className="checkradio-select select_all positionCheckbox"></span>
                                                                    </label>
                                                                </li>
                                                                <li className="widthAuto float_initial">
                                                                    <label className="select_modalRadio">
                                                                        <input type="radio" name="frequency" id="excel" onChange={this.handleMasterUpload} checked={this.state.manualExcelUpload == "excel" ? true : false} /><span className="text">CSV Upload</span>
                                                                        <span className="checkradio-select select_all positionCheckbox"></span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                {manualExcelUpload == "manual" ? <div> <div className="col-md-12 pad-0 m-top-20 masterFromInput">
                                                    <div className="fieldWid pad-0">
                                                        <label>Channel</label>
                                                        <div className="dropdown">

                                                            <input type="text" value={this.state.channel} className="inputGenericEvent" id="channel" placeholder="channel" disabled />

                                                        </div>
                                                    </div>
                                                    <div className="fieldWid pad-0">
                                                        <label>Partner</label>
                                                        <div className="dropdown">

                                                            <select onChange={(e) => this.handleChange(e)} value={this.state.partner} id="partner" className="selectGenericEvent width_90">
                                                                <option value="">
                                                                    Select Partner
                                                                        </option>
                                                                {this.state.partnerData.length == 0 ? null : this.state.partnerData == null ? null : this.state.partnerData.map((data, key) => (<option key={key} value={data}>
                                                                    {data}
                                                                </option>))}
                                                            </select>


                                                        </div>
                                                    </div>
                                                    <div className="fieldWid pad-0">
                                                        <label>Zone</label>
                                                        <div className="dropdown">

                                                            <select value={this.state.zone} onChange={(e) => this.ZoneChange(e)} id="zone" className="selectGenericEvent width_90">
                                                                <option value="">
                                                                    Select Zone
                                                                        </option>
                                                                {this.state.zoneData.length != 0 || this.state.zoneData != null ? this.state.zoneData.map((data, key) => (<option key={key} value={data} >
                                                                    {data}
                                                                </option>)) : null}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="fieldWid pad-0">
                                                        <label>Grade</label>
                                                        <div className="dropdown">

                                                            <select value={this.state.grade} id="grade" className="selectGenericEvent width_90" onChange={(e) => this.gradeChange(e)}>
                                                                <option value="">
                                                                    Select Store Grade
                                                                </option>
                                                                {this.state.gradeData.length != 0 || this.state.gradeData != null ? this.state.gradeData.map((data, key) => (<option key={key} value={data} >
                                                                    {data}
                                                                </option>)) : null}
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div className="float_Left width_25">
                                                        <label className="sketchesFormInput" >
                                                            Choose Stores
                                                            <span className="m-lft-2"> ( you can choose multiple stores at a time )</span>
                                                        </label>
                                                        <div className="purchaseSelectBox runOndemandDrop selectGenericEvent width_61">
                                                            <label className="SketchBox m-top-2">{this.state.selected.length} Stores selected</label>
                                                            {/* <span className="spanSketch">
                                                                ▾
                                                                        </span> */}
                                                        </div>

                                                        <div className="dropdownSketchFilter skechersDrop dropDownSkech">
                                                            <div className="SketchFilter">
                                                                <p>Choose Stores from below list :</p>
                                                                <input type="text" placeholder="Type to Search…" value={search} onChange={(e) => this.Change(e)} />
                                                            </div>
                                                            <div className="sketchStoreDrop">
                                                                <div className="lftStoreDiv m-rgt-20">
                                                                    <h2>Choose Stores</h2>
                                                                    <div className="storeChoose">
                                                                        <ul className="list-inline">
                                                                            {result.length == 0 ? null : result.map((data, i) => <li key={i} onClick={(e) => this.onnChange(`${data.storeCode}`)} >
                                                                                {/* <p className="para_purchase">
                                                                                            <input type="text" className="purchaseCheck" type="checkbox" id="c8" name="cb" />
                                                                                            <label htmlFor="c8" className="purchaseLabel"></label>
                                                                                        </p> */}
                                                                                <span className="onHover"></span>
                                                                                <p className="contentStore pad-5">{data.storeName}</p>

                                                                            </li>)}

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div className="lftStoreDiv">
                                                                    <h2>Selected Stores</h2>
                                                                    <div className="storeChoose pad-10">
                                                                        <ul className="list-inline">
                                                                            <li>
                                                                                {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.storeCode} className="">
                                                                                    <p className="contentStore">{data.storeName} </p>
                                                                                    <span onClick={(e) => this.onDelete(data.storeCode)} value={data.storeCode} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                                </div>)}
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <div className="col-md-12 pad-0 m-top-25 masterFromInput">
                                                        <div className="fieldWid pad-0">
                                                            <label>Event Name</label>
                                                            <input type="text" className="inputGenericEvent" id="eventName" placeholder="Enter Event Name" value={this.state.eventName} onChange={(e) => this.handleChange(e)} />
                                                            {this.state.eventNameerr ? (
                                                                <span className="error" >
                                                                    Enter Event Name
                                                        </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="fieldWid pad-0">
                                                            <label>Start Date</label>
                                                            <input type="date" id="startDate" className="inputGenericEvent width_90" value={this.state.startDate} placeholder={this.state.startDate != "" ? this.state.startDate : "Select Start Date"} onChange={(e) => this.handleChange(e)} />
                                                            {this.state.startDateerr ? (
                                                                <span className="error" >
                                                                    Select Start Date
                                                        </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="fieldWid pad-0">
                                                            <label>End Date</label>
                                                            <input type="date" id="endDate" className="inputGenericEvent width_90" value={this.state.endDate} min={this.state.startDate} placeholder={this.state.endDate != "" ? this.state.endDate : "Select End Date"} onChange={(e) => this.handleChange(e)} />
                                                            {this.state.endDateerr ? (
                                                                <span className="error" >
                                                                    Select End Date
                                                        </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="fieldWid pad-0">
                                                            <label>No. of days additional stock cover</label>
                                                            <input type="text" className="inputGenericEvent width_90" id="noOfDays" value={this.state.noOfDays} placeholder="Enter Stock Days" maxLength="2" pattern="[0-9]*" onChange={(e) => this.handleChange(e)} />
                                                            {this.state.stockDayserr ? (
                                                                <span className="error" >
                                                                    Enter Stock Days
                                                        </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="fieldWid pad-0">
                                                            <label>Multiplier for ROS</label>
                                                            <input type="text" className="inputGenericEvent" id="multiplier" value={this.state.multiplier} placeholder="Enter Multiplier" pattern="[0-9]*" onChange={(e) => this.handleChange(e)} />
                                                            {this.state.multipliererr ? (
                                                                <span className="error" >
                                                                    Enter Multiplier
                                                        </span>
                                                            ) : null}
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3 pad-lft-0 m-top-30">
                                                        {this.state.zone == "" && this.state.partner == "" && this.state.grade == "" && this.state.selected.length == 0 && this.state.eventName == "" && this.state.startDate == "" && this.state.endDate == "" && this.state.multiplier == "" && this.state.noOfDays == "" ?
                                                            <button type="button" className="discardBtns m-rgt-15 textDisable" >Clear</button>
                                                            : <button type="button" className="discardBtns m-rgt-15" onClick={(e) => this.onClear(e)}>Clear</button>}
                                                        <button type="button" className="saveBtnNew" onClick={(e) => this.onSubmit(e)}>Save</button>
                                                    </div>
                                                </div> : manualExcelUpload == "excel" &&
                                                    <div className="rightModal m-top-40 displayInline excelUpload width100">
                                                        <div className="alignMiddle">
                                                            <div className="poUploadModal displayInline">
                                                                <div className="content">
                                                                    <label className="file m-bot-0" >
                                                                        <input type="file" onChange={(e) => this.fileChange(e)} />
                                                                        <span >Choose file</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                                                            <path fill="#000" fillRule="evenodd" d="M3.867.069c-.943 0-1.89.379-2.63 1.135C-.286 2.76-.375 5.017 1.03 6.453l9.677 9.886c1.02 1.044 2.37 1.616 3.81 1.592 1.422-.018 2.771-.604 3.796-1.651 1.028-1.05 1.614-2.399 1.651-3.797a5.241 5.241 0 0 0-1.522-3.827L10.505.618l-.812.837L17.63 9.49c1.624 1.66 1.568 4.221-.132 5.957-.81.829-1.874 1.292-2.994 1.306a4.06 4.06 0 0 1-2.981-1.247L1.846 5.62a2.27 2.27 0 0 1-.649-1.739c.038-.665.342-1.32.855-1.845C3.062 1.005 4.628.99 5.62 2l8.059 8.236c.174.177.265.39.264.615a.902.902 0 0 1-.272.622.946.946 0 0 1-.642.297.795.795 0 0 1-.606-.25L6.706 5.68l-.815.834 5.716 5.839c.391.4.9.616 1.457.595a2.083 2.083 0 0 0 1.422-.642c.39-.399.607-.913.61-1.449a2.056 2.056 0 0 0-.602-1.454L6.435 1.167A3.58 3.58 0 0 0 3.867.07z" />
                                                                        </svg>
                                                                    </label>

                                                                </div>
                                                                {this.state.file.length > 0 ? <span>{this.state.file[0].name}</span> : null}
                                                            </div>
                                                            <button type="button" className="uploadBtn" onClick={(e) => this.uploadCsv(e)}>Upload
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="17" viewBox="0 0 18 17">
                                                                    <path fill="#FFF" fillRule="nonzero" d="M18 10v7H0v-7h.906v6.054h16.188V10H18zM9.95.398L9.53 0l-.42.429L5 4.626l.63.643 3.42-3.492V14h.9V1.746l3.42 3.493.63-.644L9.95.398z" />
                                                                </svg>
                                                            </button>
                                                            {/* <button type="button" className="uplodaingBtn">Uploading <img src={ovalIcon} className="loading"/></button>
                                                            <button type="button" className="uploadedBtn">Uploaded <img src={uploadedIcon} /></button> */}
                                                        </div>
                                                        <p className="noteText m-top-40"><span>Note : </span> Valid file should be .csv only</p>
                                                    </div>}
                                            </div>
                                            <div className="container-bottom displayInline width100 m-top-25">
                                                <div className="col-md-12 pad-0">
                                                    <div className="col-md-1 pad-0">
                                                        <button className="filter_button filterNew" onClick={this.handleFilter}>Filter
                                                     <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                                <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                            </svg>
                                                        </button>
                                                        {this.state.type == 2 ? <span className="clearFilterBtn displayBlock posInitial" onClick={(e) => this.onFilterClear(e)} >Clear Filter</span> : null}

                                                    </div>
                                                    <div className="col-md-5 pad-0">
                                                        <div className="refresh-table">
                                                            <img onClick={() => this.onRefresh()} src={refreshIcon} className="m-left-20 " />

                                                            <p className="tooltiptext topToolTipGeneric">
                                                                Refresh
                                                </p>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6 newSearch pad-right-0">
                                                        <input type="Search" className="inputSearch float_Right" value={this.state.getAllSearch} onKeyPress={this._handleKeyPress} onChange={(e) => this.getAllSearchChange(e)} placeholder="Search…" />
                                                        {this.state.type == 3 ? <span className="clearSearchFilter width_54 m0" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                                                    </div>

                                                </div>
                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 bordere3e7f3 tableGeneric masterEventTable">
                                                    <div className="zui-wrapper">
                                                        <div className="scrollableTableFixed table-scroll zui-scroller scrollableOrgansation orgScroll" id="table-scroll">
                                                            <table className="table scrollTable main-table zui-table tableBotBorder">
                                                                <thead>
                                                                    <tr className="trBg">
                                                                        <th className="fixed-side pad-10">
                                                                            <label>Action </label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Event Name</label>
                                                                        </th>

                                                                        <th>
                                                                            <label>Start Date</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>End Date</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Stock Days</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Multiplier for ROS</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Grade</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Partner</label>
                                                                        </th>
                                                                        <th>
                                                                            <label>Zone</label>
                                                                        </th>

                                                                        <th>
                                                                            <label>Store</label>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.getAllEvents.length != 0 ? this.state.getAllEvents.map((data, key) => (

                                                                        <tr key={key}>
                                                                            <td className="fixed-side alignMiddle">
                                                                                <img src={delIcon} className="height16 displayPointer m-lr-10" onClick={(e) => this.delRequest(data.eventName)} />
                                                                                <img src={editIcon} className="height15 displayPointer m-lr-10" onClick={(e) => this.onOpenEditModal(data.id)} />
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.eventName}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.startDate}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label> {data.endDate}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.stockDays}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.multiplierROS}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.grade}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.partner}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label>{data.zone}</label>
                                                                            </td>
                                                                            <td>
                                                                                <label className="text-ellipse">{data.storeCode}</label>
                                                                                <button type="button" className="viewStoreBtn" id="allStore" data-stores={data.storeCode} onClick={this.handleModal}>View All Store</button>
                                                                            </td>

                                                                        </tr>
                                                                    )) : <tr className="tableNoData"><td className="border-bot0" colSpan="100%"><span>No Data found</span></td></tr>}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="pagerDiv newPagination">
                                                    <ul className="list-inline pagination">
                                                        <li >
                                                            <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                                First
                                                              </button>
                                                        </li>

                                                        <li>
                                                            <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                                                Prev
                                                             </button>
                                                        </li>

                                                        <li>
                                                            <button className="PageFirstBtn pointerNone">
                                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                                            </button>
                                                        </li>
                                                        {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                                Next
                                                           </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn borderNone" disabled>
                                                                    Next
                                                                  </button>
                                                            </li>}
                                                        {this.state.current != 0 && this.state.next - 1 != this.state.maxPage && this.state.current != undefined && this.state.maxPage != this.state.current ? <li >
                                                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="last">
                                                                Last
                                                           </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn borderNone" disabled>
                                                                    Last
                                                                  </button>
                                                            </li>}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
                {this.state.allStoreModal ? <AllStoresModal {...this.state} closeAllStore={this.handleModal} /> : null}
                {this.state.editModal ? <MasterEventEditModal {...this.props} closeEditModal={(e) => this.closeEditModal(e)} editPayload={this.state.editPayload} /> : null}
                {this.state.filterModal ? <MasterEventFilter clearFilter={(e) => this.clearFilter(e)} closeFilter={this.handleFilter} filterBar={this.state.filterBar} onFilterSubmit={(e) => this.onFilterSubmit(e)} filterStartDate={this.state.filterStartDate} filterEndDate={this.state.filterEndDate} filterMultiplierROS={this.state.filterMultiplierROS} filterStockDays={this.state.filterStockDays} handleChangeFilter={(e) => this.handleChangeFilter(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} closeErrorRequest={(e) => this.onError(e)} errorMessage={this.state.errorMessage} /> : null}
                {this.state.success ? <RequestSuccess closeRequest={(e) => this.onRequest(e)} successMessage={this.state.successMessage} /> : null}
                {this.state.customError ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MasterEvents);