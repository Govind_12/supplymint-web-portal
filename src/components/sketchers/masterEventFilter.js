import React from "react";

class MasterEventFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            filterStartDate: this.props.filterStartDate,
            filterEndDate: this.props.filterEndDate,
            filterMultiplierROS: this.props.filterMultiplierROS,
            filterStockDays: this.props.filterStockDays
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            filterStartDate: nextProps.filterStartDate,     
            filterEndDate: nextProps.filterEndDate,
            filterMultiplierROS: nextProps.filterMultiplierROS,
            filterStockDays: nextProps.filterStockDays
        })
    }




    onSubmit(e) {
        e.preventDefault();
      this.props.onFilterSubmit()
    }

    render() {

        let count = 0;
        if (this.props.filterStartDate != "") {
            count++;
        }
        if (this.props.filterEndDate != "") {
            count++;
        } if (this.props.filterMultiplierROS != "") {
            count++;
        }
        if (this.props.filterStockDays != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="date" onChange={(e) => this.props.handleChangeFilter(e)} id="filterStartDate" value={this.props.filterStartDate} placeholder={this.props.filterStartDate==""?"Start Date":this.props.filterStartDate} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.props.handleChangeFilter(e)} id="filterEndDate" min={this.props.filterStartDate} value={this.props.filterEndDate} placeholder={this.props.filterEndDate==""?"End Date":this.props.filterEndDate} className="organistionFilterModal" />
                                    </li>

                                    <li>
                                        <input type="text" onChange={(e) => this.props.handleChangeFilter(e)} id="filterStockDays" pattern="[0-9]*" maxLength="2" value={this.props.filterStockDays} placeholder="Stock Days" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.props.handleChangeFilter(e)} id="filterMultiplierROS" pattern="[0-9]*" value={this.props.filterMultiplierROS} placeholder="Multiplier" className="organistionFilterModal" />
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.props.filterEndDate == "" && this.props.filterStartDate == "" && this.props.filterMultiplierROS == "" && this.props.filterStockDays == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.props.clearFilter(e)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.props.filterEndDate != "" || this.props.filterStartDate != "" || this.props.filterMultiplierROS != "" || this.props.filterStockDays != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default MasterEventFilter;
