import React from "react";
import PurchaseIndentForm from "./purchaseIndent/purchaseIndentFrom";
import GenericPurchaseIndent from "./purchaseOrder/genericPurchaseIndent"
import FilterLoader from './loaders/filterLoader';
import RequestSuccess from "./loaders/requestSuccess";
import RequestError from "./loaders/requestError";
import ConfirmModal from "./loaders/confirmModal";

class PI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contactSubmit: false,
      loader: false,
      success: false,
      alert: false,
      piHistoryState: [],
      errorMessage: "",
      errorCode: "",
      code: "",
      successMessage: "",
      sessionRemove: "",
      confirmModal: false,
      headerMsg: "",
      paraMsg: "",
      rowDelete: "",
      rowsId: "",
      pi: "",
      moduleError: "procurement"

    };
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
    if(this.state.contactSubmit){
      console.log('reload')
      window.location.reload('false');
    }
  }
  getRows(rowData) {
    this.setState({
      sessionRemove: rowData
    })
  }
  closeConfirmModal(e) {
    this.setState({
      confirmModal: !this.state.confirmModal,
    })
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  confirmModal(idx) {
    this.setState({
      confirmModal: true,
      headerMsg: "Are you sure you want to delete row?",
      paraMsg: "Click confirm to continue.",
      rowDelete: "row Delete",
      rowsId: idx
    })
  }

  onResetData() {
    this.setState({
      confirmModal: true,
      headerMsg: "Are you sure to reset the form?",
      paraMsg: "Click confirm to continue.",
      pi: "pi reset"
    })
  }
  resetData() {
    this.refs.child != null ? this.refs.child.resetData() : null
  }

  deleteRows(id) {
    this.refs.child != null ? this.refs.child.deleteRows(id) : null
  }
  componentDidMount() {
    this.props.getDraftClear()
    this.props.piSaveDraftClear()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.purchaseIndent.getAgentName.isSuccess) {
      this.setState({
        loader: false
      })
    } else if (nextProps.purchaseIndent.getAgentName.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getAgentName.message.error == undefined ? undefined : nextProps.purchaseIndent.getAgentName.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.getAgentName.message.error == undefined ? undefined : nextProps.purchaseIndent.getAgentName.message.error.errorCode,
        code: nextProps.purchaseIndent.getAgentName.message.status,
        alert: true,
        loader: false
      })
    }

    if (nextProps.purchaseIndent.gen_IndentBased.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.gen_IndentBased.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_IndentBased.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.gen_IndentBased.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_IndentBased.message.error.errorCode,
        code: nextProps.purchaseIndent.gen_IndentBased.message.status,
        alert: true,
        loader: false
      })
      this.props.gen_IndentBasedClear()
    }


    if (nextProps.purchaseIndent.getTransporter.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getTransporterRequest()
    } else if (nextProps.purchaseIndent.getTransporter.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getTransporter.message.error == undefined ? undefined : nextProps.purchaseIndent.getTransporter.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getTransporter.message.error == undefined ? undefined : nextProps.purchaseIndent.getTransporter.message.error.errorCode,
        code: nextProps.purchaseIndent.getTransporter.message.status,
        alert: true,
        loader: false

      })
      this.props.getTransporterRequest()
    }
    else if (!nextProps.purchaseIndent.getTransporter.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.supplier.isSuccess) {
      this.setState({
        loader: false,
        alert: false
      })

      this.props.supplierRequest();
    } else if (nextProps.purchaseIndent.supplier.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.supplier.message.error == undefined ? undefined : nextProps.purchaseIndent.supplier.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.supplier.message.error == undefined ? undefined : nextProps.purchaseIndent.supplier.message.error.errorCode,
        code: nextProps.purchaseIndent.supplier.message.status,
        alert: true,

        loader: false

      })
      this.props.supplierRequest();
    }
    else if (!nextProps.purchaseIndent.supplier.isLoading) {
      this.setState({
        loader: false
      })
    }
    if (nextProps.purchaseIndent.divisionData.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.divisionSectionDepartmentRequest();


    } else if (nextProps.purchaseIndent.divisionData.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorCode,
        code: nextProps.purchaseIndent.divisionData.message.status,
        alert: true,

        loader: false

      })
      this.props.divisionSectionDepartmentRequest();
    }
    if (nextProps.purchaseIndent.leadTime.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.leadTime.message.error == undefined ? undefined : nextProps.purchaseIndent.leadTime.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.leadTime.message.error == undefined ? undefined : nextProps.purchaseIndent.leadTime.message.error.errorCode,
        code: nextProps.purchaseIndent.leadTime.message.status,
        alert: true,
        loader: false
      })
      this.props.leadTimeClear();



    }
    if (nextProps.purchaseIndent.getAllCity.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.getAllCityClear();
    }

    else if (nextProps.purchaseIndent.getAllCity.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.getAllCity.message.error == undefined ? undefined : nextProps.purchaseIndent.getAllCity.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.getAllCity.message.error == undefined ? undefined : nextProps.purchaseIndent.getAllCity.message.error.errorCode,
        code: nextProps.purchaseIndent.getAllCity.message.status,
        alert: true,
        loader: false
      })
      this.props.getAllCityClear();



    }

    if (nextProps.purchaseIndent.marginRule.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.marginRuleClear();
    }

    if (nextProps.purchaseIndent.marginRule.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.marginRule.message.error == undefined ? undefined : nextProps.purchaseIndent.marginRule.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.marginRule.message.error == undefined ? undefined : nextProps.purchaseIndent.marginRule.message.error.errorCode,
        code: nextProps.purchaseIndent.marginRule.message.status,
        alert: true,
        loader: false
      })
      this.props.marginRuleClear();



    }
    else if (nextProps.purchaseIndent.leadTime.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.leadTimeClear();
    }


    if (nextProps.purchaseIndent.vendorMrp.isError) {

      this.setState({

        errorMessage: nextProps.purchaseIndent.vendorMrp.message.error == undefined ? undefined : nextProps.purchaseIndent.vendorMrp.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.vendorMrp.message.error == undefined ? undefined : nextProps.purchaseIndent.vendorMrp.message.error.errorCode,
        code: nextProps.purchaseIndent.vendorMrp.message.status,

        alert: true,

        loader: false
      })

      this.props.vendorMrpRequest();


    } else if (nextProps.purchaseIndent.vendorMrp.isSuccess) {
      this.setState({
        vendorMrpState: nextProps.purchaseIndent.vendorMrp.data.resource,
        loader: false

      })

      this.props.vendorMrpRequest();
    }
    else if (!nextProps.purchaseIndent.vendorMrp.isLoading) {
      this.setState({
        loader: false

      })
    }
    if (nextProps.purchaseIndent.purchaseTerm.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.purchaseTerm.message.error == undefined ? undefined : nextProps.purchaseIndent.purchaseTerm.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.purchaseTerm.message.error == undefined ? undefined : nextProps.purchaseIndent.purchaseTerm.message.error.errorCode,
        code: nextProps.purchaseIndent.purchaseTerm.message.status,
        alert: true,

        loader: false
      })
      this.props.purchaseTermClear()
    } else if (nextProps.purchaseIndent.purchaseTerm.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.purchaseTermClear()
    }
    if (nextProps.purchaseIndent.size.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.size.message.error == undefined ? undefined : nextProps.purchaseIndent.size.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.size.message.error == undefined ? undefined : nextProps.purchaseIndent.size.message.error.errorCode,
        code: nextProps.purchaseIndent.size.message.status,
        alert: true,
        loader: false
      })
      this.props.sizeRequest();

    } else if (nextProps.purchaseIndent.size.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.sizeRequest();

    }
    else if (!nextProps.purchaseIndent.size.isLoading) {
      this.setState({
        loader: false,
      })
    }

    if (nextProps.purchaseIndent.multipleLineItem.isSuccess) {
      this.setState({


        // loader: false
      })
      this.props.multipleLineItemClear();

    } else if (nextProps.purchaseIndent.multipleLineItem.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.multipleLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.multipleLineItem.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.multipleLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.multipleLineItem.message.error.errorCode,
        code: nextProps.purchaseIndent.multipleLineItem.message.status,
        alert: true,

        loader: false

      })
      this.props.multipleLineItemClear();

    } else if (!nextProps.purchaseIndent.multipleLineItem.isLoading) {
      this.setState({
        loader: false
      })

    }
    if (nextProps.purchaseIndent.getMultipleMargin.isSuccess) {
      this.setState({


        loader: false
      })
      this.props.getMultipleMarginClear();

    } else if (nextProps.purchaseIndent.getMultipleMargin.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getMultipleMargin.message.error == undefined ? undefined : nextProps.purchaseIndent.getMultipleMargin.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getMultipleMargin.message.error == undefined ? undefined : nextProps.purchaseIndent.getMultipleMargin.message.error.errorCode,
        code: nextProps.purchaseIndent.getMultipleMargin.message.status,
        alert: true,

        loader: false

      })
      this.props.getMultipleMarginClear();

    } else if (!nextProps.purchaseIndent.getMultipleMargin.isLoading) {
      this.setState({
        loader: false
      })

    }

    if (nextProps.purchaseIndent.color.isError) {
      this.setState({

        errorMessage: nextProps.purchaseIndent.color.message.error == undefined ? undefined : nextProps.purchaseIndent.color.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.color.message.error == undefined ? undefined : nextProps.purchaseIndent.color.message.error.errorCode,
        code: nextProps.purchaseIndent.color.message.status,
        alert: true,
        loader: false
      })

      this.props.colorRequest();
    } else if (nextProps.purchaseIndent.color.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.colorRequest();
    }
    else if (!nextProps.purchaseIndent.color.isLoading) {
      this.setState({
        loader: false,

      })
    }
    if (nextProps.purchaseIndent.poItemcode.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.poItemcode.message.error == undefined ? undefined : nextProps.purchaseIndent.poItemcode.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.poItemcode.message.error == undefined ? undefined : nextProps.purchaseIndent.poItemcode.message.error.errorCode,
        code: nextProps.purchaseIndent.poItemcode.message.status,
        alert: true,

        loader: false
      })
      this.props.poItemcodeClear();
    } else if (nextProps.purchaseIndent.poItemcode.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.poItemcodeClear();
    }

    if (nextProps.purchaseIndent.lineItem.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.lineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.lineItem.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.lineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.lineItem.message.error.errorCode,
        code: nextProps.purchaseIndent.lineItem.message.status,
        alert: true,

        loader: false
      })
      this.props.lineItemRequest();
    } else if (nextProps.purchaseIndent.lineItem.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.lineItemRequest();
    }


    if (nextProps.purchaseIndent.markUp.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.markUpRequest();
    }
    else if (nextProps.purchaseIndent.markUp.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.markUp.message.error == undefined ? undefined : nextProps.purchaseIndent.markUp.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.markUp.message.error == undefined ? undefined : nextProps.purchaseIndent.markUp.message.error.errorCode,
        code: nextProps.purchaseIndent.markUp.message.status,
        alert: true,

        loader: false
      })
      this.props.markUpRequest();
    }
    else if (!nextProps.purchaseIndent.markUp.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.quantity.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.quantityRequest();
    }
    else if (nextProps.purchaseIndent.quantity.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.quantity.message.error == undefined ? undefined : nextProps.purchaseIndent.quantity.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.quantity.message.error == undefined ? undefined : nextProps.purchaseIndent.quantity.message.error.errorCode,
        code: nextProps.purchaseIndent.quantity.message.status,
        alert: true,

        loader: false
      })
      this.props.quantityRequest();
    }
    else if (!nextProps.purchaseIndent.quantity.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.transporter.isSuccess) {
      this.setState({
        loader: false
      })
    } else if (nextProps.purchaseIndent.transporter.isError) {
      this.setState(
        {
          loader: false,
          errorMessage: nextProps.purchaseIndent.transporter.message.error == undefined ? undefined : nextProps.purchaseIndent.transporter.message.error.errorMessage,
          errorCode: nextProps.purchaseIndent.transporter.message.error == undefined ? undefined : nextProps.purchaseIndent.transporter.message.error.errorCode,
          code: nextProps.purchaseIndent.transporter.message.status,
          alert: true
        }
      )
    }

    if (nextProps.purchaseIndent.procurementSite.isSuccess) {
      this.setState({
        loader: false
      })
    } else if (nextProps.purchaseIndent.procurementSite.isError) {
      this.setState(
        {
          loader: false,
          errorMessage: nextProps.purchaseIndent.procurementSite.message.error == undefined ? undefined : nextProps.purchaseIndent.procurementSite.message.error.errorMessage,
          errorCode: nextProps.purchaseIndent.procurementSite.message.error == undefined ? undefined : nextProps.purchaseIndent.procurementSite.message.error.errorCode,
          code: nextProps.purchaseIndent.procurementSite.message.status,
          alert: true
        }
      )
    }

    if (nextProps.purchaseIndent.hsnCode.isSuccess) {

      this.setState({
        loader: false
      })
      this.props.hsnCodeClear();

    } else if (nextProps.purchaseIndent.hsnCode.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.hsnCode.message.error == undefined ? undefined : nextProps.purchaseIndent.hsnCode.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.hsnCode.message.error == undefined ? undefined : nextProps.purchaseIndent.hsnCode.message.error.errorCode,
        code: nextProps.purchaseIndent.hsnCode.message.status,
        alert: true,

        loader: false

      })

      this.props.hsnCodeClear();
    }
    if (nextProps.purchaseIndent.piAddNew.isSuccess) {
      this.setState({
        loader: false
      })

    } else if (nextProps.purchaseIndent.piAddNew.isError) {
      this.setState(
        {
          loader: false,
          errorMessage: nextProps.purchaseIndent.piAddNew.message.error == undefined ? undefined : nextProps.purchaseIndent.piAddNew.message.error.errorMessage,
          errorCode: nextProps.purchaseIndent.piAddNew.message.error == undefined ? undefined : nextProps.purchaseIndent.piAddNew.message.error.errorCode,
          code: nextProps.purchaseIndent.piAddNew.message.status,
          alert: true
        }
      )
      this.props.piAddNewRequest();
    } else if (!nextProps.purchaseIndent.piAddNew.isLoading) {
      this.setState({
        loader: false,

      })
    }
    if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.getCatDescUdfRequest();

    } else if (nextProps.purchaseIndent.getCatDescUdf.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorCode,
        code: nextProps.purchaseIndent.getCatDescUdf.message.status,
        alert: true,

        loader: false

      })
      this.props.getCatDescUdfRequest();
    } else if (!nextProps.purchaseIndent.getCatDescUdf.isLoading) {
      this.setState({
        loader: false
      })

    }
    if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getUdfMappingClear();

    } else if (nextProps.purchaseIndent.getUdfMapping.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorCode,
        code: nextProps.purchaseIndent.getUdfMapping.message.status,
        alert: true,

        loader: false

      })
      this.props.getUdfMappingClear();

    } else if (!nextProps.purchaseIndent.getUdfMapping.isLoading) {
      this.setState({
        loader: false
      })

    }

    if (nextProps.purchaseIndent.itemUdfMapping.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.itemUdfMappingRequest();

    } else if (nextProps.purchaseIndent.itemUdfMapping.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.itemUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.itemUdfMapping.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.itemUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.itemUdfMapping.message.error.errorCode,
        code: nextProps.purchaseIndent.itemUdfMapping.message.status,
        alert: true,

        loader: false

      })
      this.props.itemUdfMappingRequest();
    } else if (!nextProps.purchaseIndent.itemUdfMapping.isLoading) {
      this.setState({
        loader: false
      })

    }
    if (nextProps.purchaseIndent.getItemDetailsValue.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getItemDetailsValueRequest();

    } else if (nextProps.purchaseIndent.getItemDetailsValue.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getItemDetailsValue.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetailsValue.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getItemDetailsValue.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetailsValue.message.error.errorCode,
        code: nextProps.purchaseIndent.getItemDetailsValue.message.status,
        alert: true,

        loader: false

      })
      this.props.getItemDetailsValueRequest();
    }


    if (nextProps.purchaseIndent.getItemDetails.isSuccess) {

      this.setState({
        loader: false
      })
      this.props.getItemDetailsRequest();

    } else if (nextProps.purchaseIndent.getItemDetails.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.getItemDetails.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetails.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.getItemDetails.message.error == undefined ? undefined : nextProps.purchaseIndent.getItemDetails.message.error.errorCode,
        code: nextProps.purchaseIndent.getItemDetails.message.status,
        alert: true,

        loader: false

      })
      this.props.getItemDetailsRequest();
    }


    if (nextProps.purchaseIndent.udfType.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.udfTypeRequest();

    } else if (nextProps.purchaseIndent.udfType.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.udfType.message.error == undefined ? undefined : nextProps.purchaseIndent.udfType.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.udfType.message.error == undefined ? undefined : nextProps.purchaseIndent.udfType.message.error.errorCode,
        code: nextProps.purchaseIndent.udfType.message.status,
        alert: true,

        loader: false

      })
      this.props.udfTypeRequest();
    }



    if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.poRadioValidationClear();
    }

    else if (nextProps.purchaseIndent.poRadioValidation.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.poRadioValidation.message.error == undefined ? undefined : nextProps.purchaseIndent.poRadioValidation.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.poRadioValidation.message.error == undefined ? undefined : nextProps.purchaseIndent.poRadioValidation.message.error.errorCode,
        code: nextProps.purchaseIndent.poRadioValidation.message.status,
        alert: true,
        loader: false
      })
      this.props.poRadioValidationClear();



    } else if (!nextProps.purchaseIndent.poRadioValidation.isLoading) {

      this.setState({
        loader: false,

      })
    }
    if (nextProps.purchaseIndent.otb.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.otb.message.error == undefined ? undefined : nextProps.purchaseIndent.otb.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.otb.message.error == undefined ? undefined : nextProps.purchaseIndent.otb.message.error.errorCode,
        code: nextProps.purchaseIndent.otb.message.status,
        alert: true,

        loader: false

      })
      this.props.otbClear();
    }
    if (nextProps.purchaseIndent.discount.isSuccess) {

      this.setState({


        loader: false
      })
      this.props.discountClear();

    } else if (nextProps.purchaseIndent.discount.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.discount.message.error == undefined ? undefined : nextProps.purchaseIndent.discount.message.error.errorMessage,

        errorCode: nextProps.purchaseIndent.discount.message.error == undefined ? undefined : nextProps.purchaseIndent.discount.message.error.errorCode,
        code: nextProps.purchaseIndent.discount.message.status,
        alert: true,

        loader: false

      })

      this.props.discountClear();
    } else if (!nextProps.purchaseIndent.discount.isLoading) {
      this.setState({
        loader: false,

      })
    }
    if (nextProps.purchaseIndent.piCreate.isError) {
      this.setState(
        {
          loader: false,
          errorMessage: nextProps.purchaseIndent.piCreate.message.error == undefined ? undefined : nextProps.purchaseIndent.piCreate.message.error.errorMessage,
          errorCode: nextProps.purchaseIndent.piCreate.message.error == undefined ? undefined : nextProps.purchaseIndent.piCreate.message.error.errorCode,
          code: nextProps.purchaseIndent.piCreate.message.status,
          alert: true
        }
      )
      this.props.piCreateRequest();
    } else if (nextProps.purchaseIndent.piCreate.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        // confirmModal: false,

        successMessage: nextProps.purchaseIndent.piCreate.data.message,
        // itemNotFound: false,

      })
      if (sessionStorage.getItem(this.state.sessionRemove + "-" + "CAT1") != null || sessionStorage.getItem(this.state.sessionRemove + "-" + "DESC2") != null
        || sessionStorage.getItem(this.state.sessionRemove + "-" + "CAT2") != null || sessionStorage.getItem(this.state.sessionRemove + "-" + "DESC3") != null
        || sessionStorage.getItem(this.state.sessionRemove + "-" + "CAT3") != null || sessionStorage.getItem(this.state.sessionRemove + "-" + "DESC4") != null
        || sessionStorage.getItem(this.state.sessionRemove + "-" + "CAT4") != null || sessionStorage.getItem(this.state.sessionRemove + "-" + "DESC5") != null
        || sessionStorage.getItem(this.state.sessionRemove + "-" + "size") != null || sessionStorage.getItem(this.state.sessionRemove + "-" + "color") != null) {
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "CAT1");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "CAT2");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "CAT3");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "CAT4");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "DESC2");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "DESC3");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "DESC4");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "DESC5")
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "size");
        sessionStorage.removeItem(this.state.sessionRemove + "-" + "color");
        sessionStorage.removeItem("divisionSection")

      }

      this.props.piCreateRequest();
    }


    // ___________________________________________

    if (nextProps.purchaseIndent.piArticle.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.piArticleClear();
    }

    else if (nextProps.purchaseIndent.piArticle.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.piArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.piArticle.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.piArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.piArticle.message.error.errorCode,
        code: nextProps.purchaseIndent.piArticle.message.status,
        alert: true,
        loader: false
      })
      this.props.piArticleClear();
    }


    if (nextProps.purchaseIndent.gen_PurchaseIndent.isSuccess) {
      this.setState({
        contactSubmit: true,
        success: true,
        loader: false,


        successMessage: nextProps.purchaseIndent.gen_PurchaseIndent.data.message,
      })
    }

    else if (nextProps.purchaseIndent.gen_PurchaseIndent.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.gen_PurchaseIndent.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_PurchaseIndent.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.gen_PurchaseIndent.message.error == undefined ? undefined : nextProps.purchaseIndent.gen_PurchaseIndent.message.error.errorCode,
        code: nextProps.purchaseIndent.gen_PurchaseIndent.message.status,
        alert: true,
        loader: false
      })



    } else if (!nextProps.purchaseIndent.gen_PurchaseIndent.isLoading) {

      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.save_edited_pi.isSuccess) {

      this.setState({
        contactSubmit: true,
        success: true,
        loader: false,


        successMessage: nextProps.purchaseIndent.save_edited_pi.data.message,
      })
    }

    else if (nextProps.purchaseIndent.save_edited_pi.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.save_edited_pi.message.error == undefined ? undefined : nextProps.purchaseIndent.save_edited_pi.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.save_edited_pi.message.error == undefined ? undefined : nextProps.purchaseIndent.save_edited_pi.message.error.errorCode,
        code: nextProps.purchaseIndent.save_edited_pi.message.status,
        alert: true,
        loader: false
      })



    } else if (!nextProps.purchaseIndent.save_edited_pi.isLoading) {

      this.setState({
        loader: false,

      })
    }

    if (nextProps.purchaseIndent.poArticle.isSuccess) {
      this.setState({

        loader: false
      })
      this.props.poArticleClear();
    }

    else if (nextProps.purchaseIndent.poArticle.isError) {

      this.setState({
        errorMessage: nextProps.purchaseIndent.poArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.poArticle.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.poArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.poArticle.message.error.errorCode,
        code: nextProps.purchaseIndent.poArticle.message.status,
        alert: true,
        loader: false
      })
      this.props.poArticleClear();



    } else if (!nextProps.purchaseIndent.poArticle.isLoading) {
      this.setState({
        loader: false,

      })
    }

    if (!nextProps.purchaseIndent.pi_edit_data.isLoading) {
      this.setState({

        loader: false
      })
      // this.props.piEditClear()
    }

    if (nextProps.purchaseIndent.pi_edit_data.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.pi_edit_data.message.error == undefined ? undefined : nextProps.purchaseIndent.pi_edit_data.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.pi_edit_data.message.error == undefined ? undefined : nextProps.purchaseIndent.pi_edit_data.message.error.errorCode,
        code: nextProps.purchaseIndent.pi_edit_data.message.status,
        alert: true,
        loader: false
      })
      // this.props.piEditClear()
    }
    if (!nextProps.purchaseIndent.get_draft_data.isLoading) {
      this.setState({

        loader: false
      })
      // this.props.getDraftClear()
    }
    if (nextProps.purchaseIndent.get_draft_data.isError) {
      this.setState({
        errorMessage: nextProps.purchaseIndent.get_draft_data.message.error == undefined ? undefined : nextProps.purchaseIndent.get_draft_data.message.error.errorMessage,
        errorCode: nextProps.purchaseIndent.get_draft_data.message.error == undefined ? undefined : nextProps.purchaseIndent.get_draft_data.message.error.errorCode,
        code: nextProps.purchaseIndent.get_draft_data.message.status,
        alert: true,
        loader: false
      })
      // this.props.getDraftClear()
    }
    if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {

      this.setState({
        successMessage: nextProps.purchaseIndent.discardPoPiData.data.message,
        loader: false,
        success: true,
      })
      this.props.discardPoPiDataClear()
    } else if (nextProps.purchaseIndent.discardPoPiData.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.purchaseIndent.discardPoPiData.message.status,
        errorCode: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorCode,
        errorMessage: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorMessage
      })
      this.props.discardPoPiDataClear()
    }
    // _____________________________________getudfmapping_________________________________
    if (nextProps.purchaseIndent.save_draft_pi.isSuccess) {
      if (nextProps.purchaseIndent.save_draft_pi.data.modal == "modal") {

        this.setState({
          successMessage: `Your Draft No.  ${nextProps.purchaseIndent.save_draft_pi.data.resource.indentNo} has been saved`,
          loader: false,
          success: true,
        })
      } else {
        this.setState({ loader: false })
      }
      this.props.piSaveDraftClear()
    } else if (nextProps.purchaseIndent.save_draft_pi.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.purchaseIndent.save_draft_pi.message.status,
        errorCode: nextProps.purchaseIndent.save_draft_pi.message.error == undefined ? undefined : nextProps.purchaseIndent.save_draft_pi.message.error.errorCode,
        errorMessage: nextProps.purchaseIndent.save_draft_pi.message.error == undefined ? undefined : nextProps.purchaseIndent.save_draft_pi.message.error.errorMessage
      })
      this.props.piSaveDraftClear()
    }
    //console.log(nextProps.purchaseIndent.save_draft_pi)

    if (nextProps.purchaseIndent.discardPoPiData.isLoading || nextProps.purchaseIndent.poArticle.isLoading || nextProps.purchaseIndent.gen_PurchaseIndent.isLoading || nextProps.purchaseIndent.save_edited_pi.isLoading || nextProps.purchaseIndent.purchaseTerm.isLoading || nextProps.purchaseIndent.procurementSite.isLoading || nextProps.purchaseIndent.getAllCity.isLoading ||
      nextProps.purchaseIndent.getTransporter.isLoading || nextProps.purchaseIndent.discount.isLoading ||
      nextProps.purchaseIndent.color.isLoading || nextProps.purchaseIndent.size.isLoading || nextProps.purchaseIndent.poItemcode.isLoading ||

      nextProps.purchaseIndent.vendorMrp.isLoading || nextProps.purchaseIndent.poRadioValidation.isLoading ||
      nextProps.purchaseIndent.supplier.isLoading || nextProps.purchaseIndent.multipleLineItem.isLoading || nextProps.purchaseIndent.getMultipleMargin.isLoading ||
      nextProps.purchaseIndent.leadTime.isLoading || nextProps.purchaseIndent.divisionData.isLoading || nextProps.purchaseIndent.hsnCode.isLoading
      || nextProps.purchaseIndent.piAddNew.isLoading || nextProps.purchaseIndent.getCatDescUdf.isLoading || nextProps.purchaseIndent.getAgentName.isLoading
      || nextProps.purchaseIndent.itemUdfMapping.isLoading || nextProps.purchaseIndent.getUdfMapping.isLoading || nextProps.purchaseIndent.getItemDetailsValue.isLoading
      || nextProps.purchaseIndent.getItemDetails.isLoading || nextProps.purchaseIndent.udfType.isLoading || nextProps.purchaseIndent.piCreate.isLoading || nextProps.purchaseIndent.piArticle.isLoading || nextProps.purchaseIndent.pi_edit_data.isLoading || nextProps.purchaseIndent.get_draft_data.isLoading) {
      this.setState({
        loader: true

      })
    }
  }
  render() {
    console.log('contact', this.state.contactSubmit)
    const hash = window.location.hash;

    return (

      <div className="container-fluid pad-0 pad-l50">
        {hash == "#/purchase/purchaseIndent" ? (

          <PurchaseIndentForm ref="child" {...this.props} getRows={(e) => this.getRows(e)} confirmModal={(e) => this.confirmModal(e)} onResetData={(e) => this.onResetData(e)} />
        ) : null}
        {hash == "#/purchase/purchaseIndents" ? (

          <GenericPurchaseIndent ref="child" {...this.props} confirmModal={(e) => this.confirmModal(e)} onResetData={(e) => this.onResetData(e)} successMessage={this.state.success} />
        ) : null}

        {this.state.confirmModal ? <ConfirmModal {...this.props} {...this.state} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} pi={this.state.pi} rowsId={this.state.rowsId} rowDelete={this.state.rowDelete} deleteRows={(e) => this.deleteRows(e)} resetData={(e) => this.resetData(e)} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} moduleError={this.state.moduleError} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>

    );
  }
}

export default PI;
