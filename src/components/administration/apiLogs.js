import React from 'react';
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ManageApiLogs from "./apiLogs/manageApiLogs";
class ApiLogs extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            moduleApiLogState: [],
          loader: false,
          emptyBox: false,
          manageApiLogs: false,
          errorMessage: "",
          page: 1,
          errorCode: "",
          success: false,
          successMessage: "",
          alert: false,
          code: "",
        };
      }
      onRequest(e) {
        e.preventDefault();
        this.setState({
          success: false
        })
      }
      onError(e) {
        e.preventDefault();
        let flag = false
        this.setState({
          alert: flag
        })
    
        document.onkeydown = function (t) {
          if (t.which) {
            return true;
          }
        }
      }
      componentDidMount() {
        if (!this.props.administration.getModuleApiLogsData.isSuccess) {
            let data = {
                pageNo: 1,
                type: 1,
                search:"" ,
                sortedBy:"" ,
                sortedIn: "",
                filter: {}
            }
            this.props.getModuleApiLogsRequest(data)
        } else {
            let data = {
                pageNo: 1,
                type: 1,
                search:"" ,
                sortedBy:"" ,
                sortedIn: "",
                filter: {}
            }
            this.props.getModuleApiLogsRequest(data)
            this.setState({
                loader: false
            })
    
          this.setState({
            moduleApiLogState: this.props.administration.getModuleApiLogsData.data.resource,
            emptyBox: false,
            manageApiLogs: true
          })
    
        }
      }
      static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.administration.getModuleApiLogsData.isSuccess) {
          return {
            loader: false,
            moduleApiLogState: nextProps.administration.getModuleApiLogsData.data.resource,
            manageApiLogs: true,
            emptyBox: false
          }
    
    
        } else if (nextProps.administration.getModuleApiLogsData.isError) {
            console.log(nextProps.administration.getModuleApiLogsData,"error")
          return {
            loader: false,
            errorMessage: nextProps.administration.getModuleApiLogsData.message.error == undefined ? undefined : 
            nextProps.administration.organization.message.error &&
            nextProps.administration.organization.message.error.errorMessage ?
            nextProps.administration.organization.message.error.errorMessage : "",
            errorCode: nextProps.administration.getModuleApiLogsData.message.error == undefined ? undefined : 
            nextProps.administration.organization.message.error &&
            nextProps.administration.organization.message.error.errorCode ?
            nextProps.administration.organization.message.error.errorCode : "",
            code: nextProps.administration.getModuleApiLogsData.message.status,
            alert: true,
            manageApiLogs: true
          }
        }
       if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            return {
              loader: false,
          }
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
          return {
            loader: false,
            alert: true,
            code: nextProps.replenishment.getHeaderConfig.message.status,
            errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
            errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
             }
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
          return {
            successMessage: nextProps.replenishment.createHeaderConfig.data.message,
            loader: false,
            success: true,
          }
    
        } else if (nextProps.replenishment.createHeaderConfig.isError) {
          return {
            loader: false,
            alert: true,
            code: nextProps.replenishment.createHeaderConfig.message.status,
            errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
            errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
          }
    
        }
    
        if (nextProps.administration.getModuleApiLogsData.isLoading ||
             nextProps.replenishment.getHeaderConfig.isLoading || 
             nextProps.replenishment.createHeaderConfig.isLoading) {
          return {
            loader: true
          }
        }
        return {}
      }
      componentDidUpdate(previousProps, previousState) {
        if (this.props.administration.getModuleApiLogsData.isError || this.props.administration.getModuleApiLogsData.isSuccess) {
          this.props.getModuleApiLogsClear()
        }
        if (this.props.replenishment.getHeaderConfig.isError || this.props.replenishment.getHeaderConfig.isSuccess) {
          this.props.getHeaderConfigClear()
        }
        if (this.props.replenishment.createHeaderConfig.isSuccess) {
            let payload = {
                displayName: "ADM_API_LOG_HISTORY",
                attributeType: "TABLE HEADER",
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
              }
              this.props.getHeaderConfigRequest(payload)
          this.props.createHeaderConfigClear()
        } else if (this.props.replenishment.createHeaderConfig.isError) {
          this.props.createHeaderConfigClear()
        }
      }
    
    
    render(){
        return(
            <div className="container-fluid pad-0">
            {this.state.manageApiLogs ?
            <ManageApiLogs
            moduleApiLogState={this.state.moduleApiLogState}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            : null}
            {this.state.loader ? <FilterLoader /> : null}
            {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
            {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default ApiLogs;