import React from "react";

import EmptyBox from "./roles/emptyBox";
import siteData from "../../json/siteData.json";
import ViewRoles from "./roles/viewRoles";
import AddRoles from "./roles/addRoles";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
class Roles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rolesState: [],
      emptyBox: false,
      viewRoles: false,
      loader: true,
      success: false,
      errorMessage: "",
      successMessage: "",
      alert: false,
      errorCode: "",
      code: ""
      // siteCreate :false
    };
  }

  componentWillMount() {
    if (!this.props.administration.roles.isSuccess && !this.props.administration.allRole.isSuccess) {
      let data = {
        type: 1,
        no: 1
      }
      this.props.rolesRequest(data);
      this.props.allRoleRequest();
    } else {
      let data = {
        type: 1,
        no: 1
      }
      this.props.rolesRequest(data);
      this.props.allRoleRequest();
      this.setState({
        loader: false,
        rolesState: this.props.administration.roles.data.resource,
        emptyBox: false,
        viewRoles: true
      })
    }



  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.roles.isSuccess) {
      this.setState({
        loader: false,
        rolesState: nextProps.administration.roles.data.resource,
        viewRoles: true,
        emptyBox: false
      })
    } else if (nextProps.administration.roles.isLoading) {
      this.setState({
        loader: true
      })
    }

    if (nextProps.administration.roles.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.roles.message.error == undefined ? undefined : nextProps.administration.roles.message.error.errorMessage,
        errorCode: nextProps.administration.roles.message.error == undefined ? undefined : nextProps.administration.roles.message.error.errorCode,
        code: nextProps.administration.roles.message.status,
        alert: true,
        viewRoles: true,
      })
    }
    //edit  roles modal
    if (nextProps.administration.editRoles.isLoading) {
      this.setState({
        rolesState: [],
        loader: true,
      })

    }
    else if (nextProps.administration.editRoles.isSuccess && nextProps.administration.roles.isSuccess) {
      this.setState({
        rolesState: nextProps.administration.roles.data.resource,
        success: true,
        loader: false,
        successMessage: nextProps.administration.editRoles.data.message,
        alert: false,
      })
      this.props.editRolesRequest();
    }
    else if (nextProps.administration.editRoles.isError) {
      this.setState({
        errorMessage: nextProps.administration.editRoles.message.error == undefined ? undefined : nextProps.administration.editRoles.message.error.errorMessage,
        alert: true,
        success: false,
        loader: false,
        code: nextProps.administration.editRoles.message.status,
        errorCode: nextProps.administration.editRoles.message.error == undefined ? undefined : nextProps.administration.editRoles.message.error.errorCode
      })
      this.props.editRolesRequest();
    }

  }

  onAddRoles() {
    this.setState({
      // siteCreate:true,
      // rolesState:siteData
    });
    this.props.history.push('/administration/roles/addRoles')

  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
  }
  onSaveRoles() {
    this.setState({
      // siteMapState: siteMapdata,
      // // addSiteMap: false
    });
    this.props.history.push('/administration/roles/viewRoles')

  }


  render() {

    // const hash = window.location.hash.split("/")[3];
    // let element = document.getElementById('app');
    // this.state.loader ?element.classList.add('blurContent'): element.classList.remove('blurContent');

    return (
      <div className="container-fluid">

        {this.state.emptyBox ?
          <EmptyBox {...this.props} clickRightSideBar={() => this.props.rightSideBar()} onAddRoles={() => this.onAddRoles()} />
          : null}{this.state.viewRoles ?
            <ViewRoles

              {...this.props}
              rolesState={this.state.rolesState}
              onAddRoles={() => this.onAddRoles()}
              clickRightSideBar={() => this.props.rightSideBar()} />
            : null}
        {/* // hash == "addRoles" ?
    //   <AddRoles {...this.props} clickRightSideBar={() => this.props.rightSideBar()} saveRoles={() => this.onSaveRoles()} />
    //  : null} */}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

      </div>);
  }
}

export default Roles;
