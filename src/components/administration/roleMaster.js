import React from "react";
import AddRoles from "./rolesMaster/addRole";
import ManageRoles from "./rolesMaster/manageRoles"
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import AddManageRole from "./rolesMaster/addManageRole";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";

class RoleMaster extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roleUpdate: true,
      loader: false,
      success: false,
      errorMessage: "",
      successMessage: "",
      alert: false,
      errorCode: "",
      code: ""
      // siteCreate :false
    };
  }

  // componentDidMount() {
  //    this.setState({ loader: true})
  //    this.props.moduleGetDataRequest('data')
  // }

  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.moduleGetData.isSuccess) {
      this.setState({
        loader: false,
      })
      // this.props.moduleGetDataClear()
    } else if (nextProps.administration.moduleGetData.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.moduleGetData.message.error == undefined ? undefined : nextProps.administration.moduleGetData.message.error.errorMessage,
        errorCode: nextProps.administration.moduleGetData.message.error == undefined ? undefined : nextProps.administration.moduleGetData.message.error.errorCode,
        code: nextProps.administration.moduleGetData.message.status,
        alert: true,

      })
      this.props.moduleGetDataClear()
    }

    if (nextProps.administration.roleUpdateData.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.roleUpdateData.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.roleUpdateDataClear()

    } else if (nextProps.administration.roleUpdateData.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.roleUpdateData.message.error == undefined ? undefined : nextProps.administration.roleUpdateData.message.error.errorMessage,
        errorCode: nextProps.administration.roleUpdateData.message.error == undefined ? undefined : nextProps.administration.roleUpdateData.message.error.errorCode,
        code: nextProps.administration.roleUpdateData.message.status,
        alert: true,

      })
      this.props.roleUpdateDataClear()
    }

    if (nextProps.administration.getSubModuleByRoleName.isSuccess) {
      this.setState({

        loader: false,
        alert: false,
      })
      this.props.getSubModuleByRoleNameClear()

    } else if (nextProps.administration.getSubModuleByRoleName.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.getSubModuleByRoleName.message.error == undefined ? undefined : nextProps.administration.updateRole.message.error.errorMessage,
        errorCode: nextProps.administration.getSubModuleByRoleName.message.error == undefined ? undefined : nextProps.administration.updateRole.message.error.errorCode,
        code: nextProps.administration.getSubModuleByRoleName.message.status,
        alert: true,

      })
      this.props.getSubModuleByRoleNameClear()
    }


    if (nextProps.administration.moduleGetRole.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.moduleGetRoleClear()

    } else if (nextProps.administration.moduleGetRole.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.moduleGetRole.message.error == undefined ? undefined : nextProps.administration.moduleGetRole.message.error.errorMessage,
        errorCode: nextProps.administration.moduleGetRole.message.error == undefined ? undefined : nextProps.administration.moduleGetRole.message.error.errorCode,
        code: nextProps.administration.moduleGetRole.message.status,
        alert: true,

      })
      this.props.moduleGetRoleClear()
    }
    if (nextProps.administration.getSubModuleData.isSuccess) {
      this.setState({

        loader: false,

      })
      this.props.getSubModuleDataClear()

    } else if (nextProps.administration.getSubModuleData.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.getSubModuleData.message.error == undefined ? undefined : nextProps.administration.getSubModuleData.message.error.errorMessage,
        errorCode: nextProps.administration.getSubModuleData.message.error == undefined ? undefined : nextProps.administration.getSubModuleData.message.error.errorCode,
        code: nextProps.administration.getSubModuleData.message.status,
        alert: true,

      })
      this.props.getSubModuleDataClear()
    }

    if (nextProps.administration.roleDelete.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.roleDelete.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      //this.props.roleDeleteClear()

    } else if (nextProps.administration.roleDelete.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.roleDelete.message.error == undefined ? undefined : nextProps.administration.roleDelete.message.error.errorMessage,
        errorCode: nextProps.administration.roleDelete.message.error == undefined ? undefined : nextProps.administration.roleDelete.message.error.errorCode,
        code: nextProps.administration.roleDelete.message.status,
        alert: true,

      })
      this.props.roleDeleteClear()
    }
    if (nextProps.administration.roleSubmitData.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.roleSubmitData.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.roleSubmitClear()

    } else if (nextProps.administration.roleSubmitData.isError) {
      this.setState({
        loader: false,
        errorMessage: nextProps.administration.roleSubmitData.message.error == undefined ? undefined : nextProps.administration.roleSubmitData.message.error.errorMessage,
        errorCode: nextProps.administration.roleSubmitData.message.error == undefined ? undefined : nextProps.administration.roleSubmitData.message.error.errorCode,
        code: nextProps.administration.roleSubmitData.message.status,
        alert: true,

      })
      this.props.roleSubmitClear()
    }


    if (nextProps.administration.roleDelete.isLoading || nextProps.administration.moduleGetData.isLoading || nextProps.administration.updateRole.isLoading
      || nextProps.administration.moduleGetRole.isLoading || nextProps.administration.getSubModuleData.isLoading || nextProps.administration.getSubModuleByRoleName.isLoading ||
      nextProps.administration.roleUpdateData.isLoading || nextProps.administration.roleSubmitData.isLoading) {
      this.setState({
        loader: true
      })
    }
  }

  onRequest(e) {
    console.log("success Close")
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  render() {

    const hash = window.location.hash.split("/")[3];
    // let element = document.getElementById('app');
    // this.state.loader ?element.classList.add('blurContent'): element.classList.remove('blurContent');
    return (
      <div className="container-fluid pad-0">
        {hash == "addRoles" ?
          <AddRoles {...this.props} {...this.state} clickRightSideBar={() => this.props.rightSideBar()} /> : null}
        {hash == "manageRoles" ?
          <ManageRoles {...this.props} {...this.state} clickRightSideBar={() => this.props.rightSideBar()} />
          : null}
        {hash == "addManageRoles" ?
          <AddManageRole {...this.props} clickRightSideBar={() => this.props.rightSideBar()} />
          : null}
        {/*: null} */}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

      </div>);
  }
}

export function mapStateToProps(state) {
  return {
    administration: state.administration,
    VendorOrders: state.VendorOrders,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoleMaster);
