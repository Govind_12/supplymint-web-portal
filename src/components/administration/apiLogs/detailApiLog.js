import React from 'react';

class DetailApiLog extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            apiId:this.props.apiId,
            getModuleApiDisplayData:this.props.administration.getModuleApiLogsData.data.resource.filter((item)=>item.id === this.props.apiId)
        }
    }
    
    render(){
        return(
            this.state.getModuleApiDisplayData.length != 0 ?
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content manage-api-log-modal">
                    <div className="malm-head">
                        <h3>API Name</h3>
                        <span>{this.state.getModuleApiDisplayData[0].apiName}</span>
                        <button type="button" className="malm-close-btn" onClick={this.props.CloseManageApiLog}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="malm-body">
                        <div className="malmb-inner">
                            <h3>API URL</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <span className="code">{this.state.getModuleApiDisplayData[0].apiURL}</span>
                            </div>
                        </div>
                        <div className="malmb-inner">
                            <h3>Headers</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <pre>
                                    <code>
                                    {this.state.getModuleApiDisplayData[0].headers}
                                    </code>
                                </pre>
                            </div>
                        </div>
                        <div className="malmb-inner">
                            <h3>Request Payload</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <pre>
                                    <code>
                                    {this.state.getModuleApiDisplayData[0].requestPayload}
                                    </code>
                                </pre>
                            </div>
                        </div>
                        <div className="malmb-inner">
                            <h3>Response Payload</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <pre>
                                    <code>
                                    {this.state.getModuleApiDisplayData[0].responsePayload}
                                    </code>
                                </pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            :null
        )
    }
}

export default DetailApiLog;