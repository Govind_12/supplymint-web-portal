import React from 'react';
import Pagination from '../../pagination';
import DetailApiLog from './detailApiLog';
import ToastLoader from "../../loaders/toastLoader";
import FilterLoader from "../../loaders/filterLoader";
import VendorFilter from "../../vendorPortal/vendorFilter";
import filterIcon from '../../../assets/headerFilter.svg';
import refreshIcon from "../../../assets/refresh-block.svg";
import ColoumSetting from "../../replenishment/coloumSetting";
import ApiAccess from './apiAccess';
class ManageApiLogs extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            getModuleApiLogsData: this.props.moduleApiLogState,
            loader: false,
            filter: false,
            filterBar: false,
            showDownloadDrop: false,
            exportToExcel: false,
            showManageApiLog: false,
            toastLoader: false,
            toastMsg: "",
            type: 1,
            search:'',
            filterItems:{},
            checkedFilters:[],
            filteredValue: [],
            applyFilter: false,
            apiId:null,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            displayName: "",
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],
            searchBy: "contains",
            apiAccess: false,
            apiAccessData: ""
        }
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
    }
    componentDidMount() {
        if (this.props.administration.getModuleApiLogsData.isSuccess) {
            this.setState({
                prev: this.props.administration.getModuleApiLogsData.data.prePage,
                current: this.props.administration.getModuleApiLogsData.data.currPage,
                next: this.props.administration.getModuleApiLogsData.data.currPage + 1,
                maxPage: this.props.administration.getModuleApiLogsData.data.maxPage,
            })
        }
        if (!this.props.replenishment.getHeaderConfig.isSuccess) {
            let payload = {
                displayName: "ADM_API_LOG_HISTORY",
                attributeType: "TABLE HEADER",
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
              }
              this.props.getHeaderConfigRequest(payload)
        }
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
        
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }
    
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createHeaderConfig.isSuccess && this.props.replenishment.createHeaderConfig.data.basedOn == "ALL") {
            let payload = {

                displayName: "ADM_API_LOG_HISTORY",
                attributeType: "TABLE HEADER",
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
              }
              this.props.getHeaderConfigRequest(payload)
        }

        if (this.props.administration.getApiAccessDetails.isSuccess) {
            this.props.getApiAccessDetailsClear();
        }

    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null) {
                let getHeaderConfig = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"])
                let fixedHeader = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"])
                let customHeadersState = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"])
                return {
                    filterItems: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
                    loader: false,
                    customHeaders: prevState.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
                    getHeaderConfig,
                    fixedHeader,
                    customHeadersState,
                    // tableCustomHeader: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
                    // tableGetHeaderConfig: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
                    headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
                    fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"],
                    headerConfigDataState: { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] },
                    headerSummary: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
                    defaultHeaderMap: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
                    headerCondition: customHeadersState.length == 0 ? true : false,
                }
            }
        }
        if (nextProps.administration.getModuleApiLogsData.isSuccess) {
            if (nextProps.administration.getModuleApiLogsData.data.resource != null) {
                return {
                    getModuleApiLogsData: nextProps.administration.getModuleApiLogsData.data.resource,
                    prev: nextProps.administration.getModuleApiLogsData.data.prePage,
                    current: nextProps.administration.getModuleApiLogsData.data.currPage,
                    next: nextProps.administration.getModuleApiLogsData.data.currPage + 1,
                    maxPage: nextProps.administration.getModuleApiLogsData.data.maxPage,
                }
            } else {
                return {
                    getModuleApiLogsData: [],
                    prev: "",
                    current: "",
                    next: "",
                    maxPage: "",
                }
            }
        }
        if (nextProps.administration.getApiAccessDetails.isSuccess) {
            if (nextProps.administration.getApiAccessDetails.data.resource != null) {
                return {
                    apiAccess: true,
                    apiAccessData: nextProps.administration.getApiAccessDetails.data.resource,
                    loader: false
                }
            } else {
                return {
                    apiAccess: true,
                    apiAccessData: {},
                    loader: false
                }
            }
        }
        return {}

    }
    
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {

                this.setState({
                    prev: this.props.administration.getModuleApiLogsData.data.prePage,
                    current: this.props.administration.getModuleApiLogsData.data.currPage,
                    next: this.props.administration.getModuleApiLogsData.data.currPage + 1,
                    maxPage: this.props.administration.getModuleApiLogsData.data.maxPage,
                })
                if (this.props.administration.getModuleApiLogsData.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        pageNo: this.props.administration.getModuleApiLogsData.data.currPage - 1,
                        sortedBy:"" ,
                        sortedIn: "",
                        filter: {},
                        search: this.state.search,
                    }
                    this.props.getModuleApiLogsRequest(data)
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.getModuleApiLogsData.data.prePage,
                current: this.props.administration.getModuleApiLogsData.data.currPage,
                next: this.props.administration.getModuleApiLogsData.data.currPage + 1,
                maxPage: this.props.administration.getModuleApiLogsData.data.maxPage,
            })
            if (this.props.administration.getModuleApiLogsData.data.currPage != this.props.administration.getModuleApiLogsData.data.maxPage) {
                let data = {
                        type: this.state.type,
                        sortedBy:"" ,
                        sortedIn: "",
                        filter: {},
                        search: this.state.search,
                        pageNo: this.props.administration.getModuleApiLogsData.data.currPage + 1,
                    }
                    this.props.getModuleApiLogsRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.administration.getModuleApiLogsData.data.prePage,
                    current: this.props.administration.getModuleApiLogsData.data.currPage,
                    next: this.props.administration.getModuleApiLogsData.data.currPage + 1,
                    maxPage: this.props.administration.getModuleApiLogsData.data.maxPage,
                })
                if (this.props.administration.getModuleApiLogsData.data.currPage <= this.props.administration.getModuleApiLogsData.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        pageNo: 1,
                        sortedBy:"" ,
                        sortedIn: "",
                        filter: {},
                        search: this.state.search,
                    }
                    this.props.getModuleApiLogsRequest(data)
                }
            }

        }
    }
    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 3000);
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                pageNo: 1,
                sortedBy:"" ,
                sortedIn: "",
                filter: {},
                search: this.state.search,
            }
            this.props.getModuleApiLogsRequest(data)
        }

    }
    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            search:""
        })
        let data = {
            type: 1,
            pageNo: 1,
            sortedBy:"" ,
            sortedIn: "",
            filter: {},
            search: "",
        }
        this.props.getModuleApiLogsRequest(data)
    }
    openManageApiLog(e,id) {
        e.preventDefault();
        this.setState({
            apiId:id,
            showManageApiLog: !this.state.showManageApiLog
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    CloseManageApiLog = () => {
        this.setState({ showManageApiLog: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    openApiAccess(e) {
        e.preventDefault();
        this.props.getApiAccessDetailsRequest('');
        
        this.setState({
            loader: true,
            //apiAccess: !this.state.apiAccess
        });
    }
    CloseApiAccess = () => {
        this.setState({ apiAccess: false
        });
    }
    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }

    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        }
        //  () =>  document.addEventListener('click', this.closeFilter)
         );
    }
    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            this.setState({ [data]: "" })
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }
    handleInput = (event,filterName) => {
        if( event != undefined && event.length != undefined ){
            this.setState({ fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                            filterNameForDate: filterName}, ()=>this.handleFromAndToValue(event))
            this.setState({ toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                            filterNameForDate: filterName }, ()=>this.handleFromAndToValue(event)) 
        }
        else if( event != null ){
           
            this.handleFromAndToValue(event);  
    }

        // var value = event.target.value
        // var name = event.target.dataset.value
        // if (/^\s/g.test(value)) {
        //     value = value.replace(/^\s+/, '');
        // }
        // this.setState({ [name]: value, applyFilter: true }, () => {
        //     if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
        //         this.setState({ applyFilter: false })
        //     } else {
        //         this.setState({ applyFilter: true })
        //     }
        // })
    }
    handleFromAndToValue=()=>{
        var value = event.target.value;
        var name = event.target.dataset.value;
        if( name == undefined ){ 
            value = this.state.fromCreationDate+" | "+this.state.toCreationDate
            name = this.state.filterNameForDate;
         }
         else     
        value = event.target.value
         if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
          }
        
        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                this.setState({ applyFilter: true })
            }
        })
    }
    closeFilter = (e) => {
        // e.preventDefault();
        if( e == undefined || e.target.parentElement.className.baseVal == "" || ( e.target.parentElement !== null && !e.target.parentElement.className.includes("asn") && 
            !e.target.parentElement.className.includes("check"))){
            this.setState({
                filter: false,
                filterBar: false
            }, () => document.removeEventListener('click', this.closeFilter));
        }
    }
    clearTag=(e,index)=>{
        let deleteItem = this.state.checkedFilters;
        deleteItem.splice(index,1)
        this.setState({
           checkedFilters:deleteItem
        },()=>{
            this.submitFilter();
            console.log(this.state.checkedFilters)
        })
    }

    clearAllTag=(e)=>{
   
        this.setState({
            checkedFilters:[]
        
        },()=>{
            this.submitFilter();
            this.clearFilterOutside();
        })
     }
    clearFilterOutside=()=>{
        this.setState({
            filteredValue:[],
            selectAll:false,
            checkedFilters:[]
        })
    }
    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }
    onRefresh = () =>{
        
        let data = {
            type:this.state.type,
            pageNo: this.state.curren,
            sortedBy:"" ,
            sortedIn: "",
            filter: {},
            status: this.state.status,
            search: this.state.search,
        }
        this.props.getModuleApiLogsRequest(data)
    }
    submitFilter = () => {
        console.log(this.state.checkedFilters)
        let payload = {}
        let filtervalues =  {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        // Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data]  + "T00:00+05:30")))
        Object.keys(payload).map((data)=>(data.includes("poDate")|| data.includes("validFromDate") || data.includes("validToDate")|| data.includes("qcFromDate") || data.includes("qcToDate"))
        && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim() + "T00:00+05:30", to: payload[data].split("|")[1].trim() + "T00:00+05:30" }))
        
        //for handling to and from value on UI level::    
        this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(filtervalues).map((data) => ((data.includes("poDate") || data.includes("validFromDate") || data.includes("validToDate")|| data.includes("qcToDate") || data.includes("qcFromDate"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim() })))
        let data = {
            type: this.state.checkedFilters.length === 0 ? 1 : 2,
            pageNo: 1,
            search: this.state.search,
            filter: payload,
            sortedBy: "",
            sortedIn: "",
        }
        this.props.getModuleApiLogsRequest(data)
        this.setState({
            type:2,
            filter: false,
            filteredValue: filtervalues,
            tagState:true
        })
    }
    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.getModuleApiLogsRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        this.child.closeRecentlyAdd()
    }
    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
          this.setState({ showManageApiLog: false, })
        }
    }
    render(){
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                <form onSubmit={(e) => this.onSearch(e)}>
                                    <input onChange={(e) => this.handleSearch(e)} value={this.state.search} type="search"  placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {this.state.type == 3 ?<span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={require('../../../assets/clearSearch.svg')}/></span>: null}
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <button type="button" className="gen-approved" onClick={(e) => this.openApiAccess(e)}>API Access Details</button>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Export Excel</span>
                                    </button>
                                    {this.state.exportToExcel &&
                                    <ul className="pi-history-download">
                                        <li>
                                            <button className="export-excel" type="button">
                                                <span className="pi-export-svg">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                        <g id="prefix__files" transform="translate(0 2)">
                                                            <g id="prefix__Group_2456" data-name="Group 2456">
                                                                <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                    <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                    <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                        <tspan x="0" y="0">XLS</tspan>
                                                                    </text>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </span>
                                                Export to Excel
                                            </button>
                                        </li>
                                        <li>
                                            <button className="export-excel" type="button">
                                                <span className="pi-export-svg">
                                                    <img src={require('../../../assets/downloadAll.svg')} />
                                                </span>
                                            Download All</button>
                                        </li>
                                    </ul>}
                                </div>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Documents</span>
                                    </button>
                                    {/* {this.state.showDownloadDrop ? (
                                        <MultipleDownload  {...this.props} drop="entPendingOrder" />
                                    ) : (null)} */}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                </div>
                                {/* <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                     {this.state.filter && <VendorFilter />} 
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState?this.state.checkedFilters.map((keys,index)=>(
                    <div className="show-applied-filter">
                            {index===0 ?<button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>:null}
                        <button type="button" className="saf-btn">{keys}
                            <img onClick={(e)=>this.clearTag(e,index)} src={require('../../../assets/clearSearch.svg')} />
                    {/* <span className="generic-tooltip">{Object.values(this.state.filteredValue)[index]}</span> */}
                    <span className="generic-tooltip">{typeof (Object.values(this.state.filteredValue)[index]) == 'object' ? Object.values(Object.values(this.state.filteredValue)[index]).join(',') : Object.values(this.state.filteredValue)[index]}</span>
                        </button>
                    </div>)):''}
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            {/* <ColoumSetting {...this.props} {...this.state} coloumSetting={this.state.coloumSetting} getHeaderConfig={this.state.getHeaderConfig} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} /> */}
                                <table className="table gen-main-table">
                                    <thead >
                                        <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img onClick={() => this.onRefresh()} src={refreshIcon} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                                <img src={filterIcon} className="imgHead" />
                                            </th>
                                        )) : this.state.customHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                                <img src={filterIcon} className="imgHead" />
                                            </th>
                                        ))}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.getModuleApiLogsData.length != 0 ? this.state.getModuleApiLogsData.map((data, key) => (
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkBox" />
                                                            <span className="checkmark1"></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="border-lft displayPointer" onClick={(e) => this.openManageApiLog(e,data.id)}><label className="bold displayPointer">{data.apiName}</label></td>
                                            <td>
                                                <label>{data.apiURL}</label>
                                            </td>
                                            <td>
                                                <label>{data.headers}</label>
                                            </td>
                                            <td>
                                            <label>{data.requestPayload}</label>
                                            </td>
                                            <td>
                                            <label>{data.responsePayload}</label>
                                            </td>

                                            <td>
                                                <label>{data.responseCode}</label>
                                            </td>
                                            <td>
                                            <label>{data.status}</label>
                                            </td>
                                            <td>
                                            <label>{data.createdTime}</label>
                                            </td>
                                            <td>
                                            <label>{data.apiType}</label>
                                            </td>
                                        </tr>)) : <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr>}
                                      </tbody>


                                      
                                </table>
                            </div>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <div className="pagination-inner">
                                            <ul className="pagination-item">
                                                {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ?
                                                <li>
                                                    <button type="button">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> :
                                                <li>
                                                    <button onClick={(e) => this.page(e)} id="first" >
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                {this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ?
                                                <li>
                                                    <button className="" onClick={(e) => this.page(e)} id="prev">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> :
                                                <li>
                                                    <button className="">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                <li>
                                                    <button className="pi-number-btn">
                                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                                    </button>
                                                </li>
                                                {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage ? <li >
                                                    <button className="" onClick={(e) => this.page(e)} id="next">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> : <li >
                                                        <button className="" disabled>
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                    </button>
                                                    </li>}
                                                    <li>
                                                        <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>

                                                {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                                                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                                                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                {this.state.loader && <FilterLoader />}
                {this.state.apiAccess && <ApiAccess {...this.props} {...this.state} data={this.state.apiAccessData} CloseApiAccess={this.CloseApiAccess} />}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.showManageApiLog && <DetailApiLog {...this.props} {...this.state} CloseManageApiLog={this.CloseManageApiLog} />}
            </div>
        )
    }
}

export default ManageApiLogs;