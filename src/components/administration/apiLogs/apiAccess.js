import React from 'react';


         
class ApiAccess extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            edit: [],
            id:[],
            apiName:'',
            apiUrl:'',
            check:[],
        
            apiData: this.props.data.table != undefined ? this.props.data.table : []
            

        }
           
    }
   handleChange=(e)=>{
    if( e.target.id == "apiName"){
        this.setState({
          apiName: e.target.value
        })
    } 
    if( e.target.id == "apiUrl"){
        this.setState({
          apiUrl: e.target.value,
          
        }
        );
    } 
    
    } 
   

    onSubmit=(e)=> {
        e.preventDefault();
    this.setState
        setTimeout(()=> {
        let payload = {
            api_name : this.state.apiName,
            api_url :this.state.apiUrl,
            is_enable :"TRUE",
             operation : 'INSERT' 
        }
                this.props.getInsertRequest(payload); 

            }
        , 10)
 
     }
    
     onSelect=(e)=> {
        e.preventDefault();
    this.setState
        setTimeout(()=> {
        let payload = {
            api_name : this.state.apiName,
            api_url :this.state.apiUrl,
            is_enable :"TRUE",
             operation : 'UPDATE'
              
        }
                this.props.saveInsertRequest(payload); 

            }
        , 10)
 
     }

    
   
    handleEdit =(e, data)=> {
        e.preventDefault();
        let apiData = this.state.apiData;
        apiData.map( item => data.api_url == item.api_url ? data.api_url = e.target.value : data.api_url = data.api_url)
        this.setState({apiData });
    }
   
    

    
    handleClose(e, key) {
        e.preventDefault();
        const {edit, check} = this.state;
        edit[key] = false
        check[key] = !this.state.check[key]
        this.setState({
            edit: this.state.edit,
            check: this.state.check,
        });
        console.log("close")
    }

    handleSubmit(e, key) {
        e.preventDefault();
        const {edit, check} = this.state;
        edit[key] = !this.state.edit[key]
        this.setState({
            edit: this.state.edit,
        });
        console.log("submit")
    }

    handleClick(e, key){
        e.preventDefault();
        const {check, edit} = this.state;
        check[key] = !this.state.check[key]
        edit[key] = !this.state.edit[key]
        this.setState({
            check: this.state.check,
            edit: this.state.edit,
        }); 
        
    }
    componentDidMount(){
        
        var edit = []
       for(  let i=0; i<this.props.data.table.length;i++) {
        edit[i] = false
        this.setState({
            edit: edit
        })
       }
       var check = []
       for(  let i=0; i<this.props.data.table.length;i++) {
        check[i] = false
        this.setState({
            check: check
        })
       }
   
        
    };
   
   
    closeFilter = () => {
        this.setState({
            edit: true,
        });
    }
    render() {
        const{apiData} = this.state;
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content manage-api-log-modal api-access-details-modal">
                    <div className="malm-head">
                        <h3>API Access Details</h3>
                        <button type="button" className="" onClick={this.props.CloseApiAccess}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="10.047" height="10.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-32" role="tablist">
                            <li className="nav-item active">
                                <a className="nav-link st-btn" href="#inbound" role="tab" data-toggle="tab">Inbound</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#outbound" role="tab" data-toggle="tab">Outbound</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="inbound" role="tabpanel">
                            <div className="malm-body">
                                <div className="malmb-inner">
                                    <label>URL</label>
                                    <div className="malmb-item">
                                        <span className="code">{this.props.data.url == undefined ? "" : this.props.data.url}</span>
                                    </div>
                                </div>
                                <div className="malmb-inner">
                                    <label>Token</label>
                                    <div className="malmb-item">
                                        <span className="code">{this.props.data.erpToken == undefined ? "" : this.props.data.erpToken}</span>
                                    </div>
                                </div>
                                <div className="malmb-inner">
                                    <label>Stage</label>
                                    <div className="malmb-item">
                                        <span className="code">{this.props.data.stage == undefined ? "" : this.props.data.stage}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade in" id="outbound" role="tabpanel">
                            <div className="aadm-body">
                                <div className="aadmb-top m-top-10">
                                    <div className="aadmbt-field">
                                        <label>Auth Token</label>
                                        <input type="text" value={this.props.data.token} disabled="true" />
                                    </div>
                                    <button type="button">Submit</button>
                                </div>
                                <h2>Add New</h2>
                                <div className="aadmb-top m-top-10">
                                    <div className="aadmbt-field">
                                        <label>API Name</label>
                                        {/* <select>
                                        <option value="">Select name</option>
                                        {Object.keys(this.props.data.apiNames).length != 0 ? Object.keys(this.props.data.apiNames).map((data, key) => (
                                         <option value={data}>{this.props.data.apiNames[data]}</option>
                                         )) : <option></option>}
                                                
                                        </select> */}

                                            <select value={this.state.apiName} id="apiName" onChange={this.handleChange}>
                                                <option value="">Select</option>
                                                <option value="abc">abc</option>
                                                <option value="bhg">bhg</option>
                                                <option value="cdf">cdf</option>
                                            </select>
                                    </div>
                                    <div className="aadmbt-field">
                                        <label>API URL</label>
                                        <input type="text" id="apiUrl" value={this.state.apiUrl} onChange={this.handleChange} />
                                    </div>
                                    <button type="button" onClick={(e) => this.onSubmit(e)}  >Submit</button>
                                </div>
                                <h2>Manage</h2>
                                <div className="vendor-gen-table">
                                    <div className="manage-table">
                                        <table className="table gen-main-table">
                                            <thead>
                                                <tr>
                                                    <th><label></label></th>
                                                    <th><label>API Name</label></th>
                                                    <th><label>API URL</label></th>
                                                </tr>
                                            </thead>
                                           
                                            <tbody>
                                            
                                            {apiData.length != 0 ? apiData.map((data, key) => (
                                                <tr>
                                                
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                 <div className="nph-switch-btn">
                                                                    <label className="tg-switch">
                                                                        <input type="checkbox"  />
                                                                        <span className="tg-slider tg-round"></span>
                                                                    </label>
                                                                </div> 
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td><label>{data.api_name}</label></td>
                                                    <td>
                                                        {this.state.edit[key] === false  ?  
                                                        <label className="editModalLabel">{data.api_url}</label>:
                                                        <input type="text" value={data.api_url}  onChange={(e)=>this.handleEdit(e, data)}/>
                                                       
                                                         }
                                                        {this.state.edit[key] === false && this.state.check[key] === false?
                                                         <span className="access-edit-btn"  onClick={(e)=>{this.handleClick(e,key);this.handleEdit(e,key)}}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>  
                                                         </span>
                                                         :<span></span>
                                                         }
                                                            
                                                          
                                                           {this.state.check[key] === true?
                                                             <span className="access-edit"  onClick={(e) =>{ this.onSelect(e); this.handleSubmit(e,key)}}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" id="1" width="16" height="16" fill="currentColor" className="bi bi-check-square" viewBox="0 0 16 16">
                                                                   <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                                   <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
                                                                </svg></span>
                                                                :<span></span>
                                                                }
                                                            {this.state.check[key] === true?
                                                             <span className="access-edit-btn" onClick={(e)=>this.handleClose(e,key)}>
                                                                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-square" viewBox="0 0 16 16">
                                                                    <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                                                    <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                                 </svg></span>:<span></span>
                                                            }
                                                             
                                                    </td>
                                                </tr>
                                                )) : <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr>}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ApiAccess;