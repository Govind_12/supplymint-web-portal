import React from "react";
import multideleteIcon from "../../assets/multidelete.svg";
import DataSyncFilter from "./dataSyncFilter";
import RequestError from "../loaders/requestError";
import FilterLoader from "../loaders/filterLoader";
import refreshIcon from '../../assets/refresh-block.svg';
import searchIcon from '../../assets/clearSearch.svg';
import SearchImage from '../../assets/searchicon.svg';
import ToastLoader from "../loaders/toastLoader";
import dataSyncIcon from "../../assets/data-sync.svg";

import { OganisationIdName } from '../../organisationIdName.js';

class Datasync extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSyncState: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            filter: false,
            filterBar: false,
            dataFilter: false,
            type: 1,
            no: 1,
            search: "",
            loader: true,
            name: "",
            key: "",
            status: "",
            eventStart: "",
            eventEnd: "",
            dataCount: "",
            alert: false,
            code: "",
            errorMessage: "",
            errorCode: "",
            toastLoader:false,
            toastMsg:"",
            channel:"",
            orgCodeGlobal:""
        };
    }

    componentWillMount() {
     var { orgCodeGlobal } = OganisationIdName()
        let data = {
            type: 1,
            no: 1,
            search: "",
            channel:orgCodeGlobal
            
        }
        this.setState({
            orgCodeGlobal:orgCodeGlobal
        })
        this.props.dataSyncRequest(data);
    }

    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            search: "",
            name: data.name,
            key: data.key,
            status: data.status,
            eventStart: data.eventStart,
            eventEnd: data.eventEnd,
            dataCount: data.dataCount,
        })
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
          document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.dataSync.isSuccess) {
            this.setState({
                loader: false,
                alert: false,
                dataSyncState: nextProps.administration.dataSync.data.resource,
                prev: nextProps.administration.dataSync.data.prePage,
                current: nextProps.administration.dataSync.data.currPage,
                next: nextProps.administration.dataSync.data.currPage + 1,
                maxPage: nextProps.administration.dataSync.data.maxPage,
            })
        } else if (nextProps.administration.dataSync.isLoading) {
            this.setState({
                loader: true,
                alert: false
            })
        } else if (nextProps.administration.dataSync.isError) {
            this.setState({
                loader: false,
                alert: true,
                errorMessage: nextProps.administration.dataSync.message.error == undefined ? undefined : nextProps.administration.dataSync.message.error.errorMessage,
                errorCode: nextProps.administration.dataSync.message.error == undefined ? undefined : nextProps.administration.dataSync.message.error.errorCode,
                code: nextProps.administration.dataSync.message.status
            })
        }

    }

    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.administration.dataSync.data.prePage,
                    current: this.props.administration.dataSync.data.currPage,
                    next: this.props.administration.dataSync.data.currPage + 1,
                    maxPage: this.props.administration.dataSync.data.maxPage,
                })
                if (this.props.administration.dataSync.data.currPage != 0) {
                 
                    let data = {
                        type: this.state.type,
                        no: this.props.administration.dataSync.data.currPage - 1,
                        name: this.state.name,
                        key: this.state.key,
                        status: this.state.status,
                        eventStart: this.state.eventStart,
                        eventEnd: this.state.eventEnd,
                        dataCount: this.state.dataCount,
                        search: this.state.search,
                        channel:this.state.orgCodeGlobal
                    }
                    this.props.dataSyncRequest(data);
                }

            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.dataSync.data.prePage,
                current: this.props.administration.dataSync.data.currPage,
                next: this.props.administration.dataSync.data.currPage + 1,
                maxPage: this.props.administration.dataSync.data.maxPage,
            })
            if (this.props.administration.dataSync.data.currPage != this.props.administration.dataSync.data.maxPage) {
                
                let data = {
                    type: this.state.type,
                    no: this.props.administration.dataSync.data.currPage + 1,
                    name: this.state.name,
                    key: this.state.key,
                    status: this.state.status,
                    eventStart: this.state.eventStart,
                    eventEnd: this.state.eventEnd,
                    dataCount: this.state.dataCount,
                    search: this.state.search,
                    channel:this.state.orgCodeGlobal
                }
                this.props.dataSyncRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {

                this.setState({
                    prev: this.props.administration.dataSync.data.prePage,
                    current: this.props.administration.dataSync.data.currPage,
                    next: this.props.administration.dataSync.data.currPage + 1,
                    maxPage: this.props.administration.dataSync.data.maxPage,
                })
                if (this.props.administration.dataSync.data.currPage <= this.props.administration.dataSync.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        name: this.state.name,
                        key: this.state.key,
                        status: this.state.status,
                        eventStart: this.state.eventStart,
                        eventEnd: this.state.eventEnd,
                        dataCount: this.state.dataCount,
                        search: this.state.search,
                        channel:this.state.orgCodeGlobal
                    }
                    this.props.dataSyncRequest(data)
                }
            }

        }
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        });
    }
    closeFilter(e) {
        // e.preventDefault();
        this.setState({
            filter: false,
            filterBar: false
        });
    }

    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                no: 1
            })
            let data = {
                type: 3,
                search: this.state.search,
                no: 1,
                channel:this.state.orgCodeGlobal
            }
            this.props.dataSyncRequest(data)
        }
    }

    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        let data = {
            type: 1,
            no: 1,
            search: "",
            channel:this.state.orgCodeGlobal
        }
        this.props.dataSyncRequest(data);
    }

    onClearFilter(e) {
        this.setState({
            type: 1,
            no: 1,
            search: "",
            name: "",
            key: "",
            status: "",
            eventStart: "",
            eventEnd: "",
            dataCount: "",
        })
        let data = {
            type: 1,
            no: 1,
            search: "",
            channel:this.state.orgCodeGlobal

        }
        this.props.dataSyncRequest(data);
    }

    onRefresh() {
        let data = {
            type: this.state.type,
            no: this.state.current,
            name: this.state.name,
            key: this.state.key,
            status: this.state.status,
            eventStart: this.state.eventStart,
            eventEnd: this.state.eventEnd,
            dataCount: this.state.dataCount,
            search: this.state.search,
            channel:this.state.orgCodeGlobal
        }
        this.props.dataSyncRequest(data)
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <form className="m0" onSubmit={(e) => this.onSearch(e)}>
                                        <input onChange={(e) => this.handleSearch(e)} value={this.state.search} type="search" placeholder="Type to Search..." className="search_bar" />
                                        <img className="search-image" src={SearchImage} />
                                        {this.state.type == 3 ? <span onClick={(e) => this.onClearSearch(e)} className="closeSearch"><img src={searchIcon} /></span> : null}
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="gvpd-filter">
                                    <button className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={e => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {this.state.filter && <VendorFilter closeFilter={(e) => this.closeFilter(e)} />}
                                    {this.state.type != 2 ? null : <span className="clearFilterBtnData" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 pad-0 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table" id="table-scroll">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn width40px">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={refreshIcon} onClick={() => this.onRefresh()} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        <th>
                                            <label>Name</label><img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Data Count</label><img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Event Start</label><img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Event End</label><img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Key</label><img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Status</label><img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.dataSyncState == null ? <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr> : this.state.dataSyncState.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.dataSyncState.map((data, key) => (
                                        <tr key={key}>
                                            {/* <td className="fixed-side"><label className="checkBoxLabelOrg"><input type="checkBox" /><span className="checkmarkOrg"></span></label>
                                            <button onClick={() => this.organisationModalOpen(`${data.orgID}`)} >
                                                <img src={editIcon} />
                                            </button>
                                            <div className="topToolTip">
                                                <button onClick={() => this.onDeleteOrganization(`${data.orgID}`)}>
                                                    <img src={deleteIcon} /></button>
                                                <span className="topToolTipText">Delete Record</span>
                                            </div>
                                        </td> */}
                                            <td className="fix-action-btn width40px">
                                                <ul className="table-item-list">
                                                    <li className="til-inner"></li>
                                                </ul>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.name}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.dataCount}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.eventStart}
                                                </label>
                                            </td>
                                            <td>
                                                <label>{data.eventEnd}</label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.key}
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    {data.status}
                                                </label>
                                            </td>

                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" min="1"  value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <div className="pagination-inner">
                                            <ul className="pagination-item">
                                            {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ?
                                                <li >
                                                    <button>
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> :
                                                <li >
                                                    <button onClick={(e) => this.page(e)} id="first" >
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                {this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ?
                                                <li>
                                                    <button onClick={(e) => this.page(e)} id="prev">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> :
                                                <li>
                                                    <button>
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                <li>
                                                    <button className="pi-number-btn">
                                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                                    </button>
                                                </li>
                                                {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                                    <button className="" onClick={(e) => this.page(e)} id="next">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> : <li >
                                                        <button className="" disabled>
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                </li>}
                                                <li>
                                                    <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* {this.state.modalOpen ? <DataSyncFilter {...this.state} {...this.props} orgId={this.state.orgId} orgModalAnimation={this.state.orgModalAnimation} modalOpen={(e) => this.organisationModalOpen(e)} /> : null} */}

                    </div>
                
                
                {/* {this.state.filter ? <DataSyncFilter {...this.state} keys={this.state.key} updateFilter={(e) => this.updateFilter(e)} {...this.props} dataFilter={this.state.dataFilter} closeFilter={(e) => this.openFilter(e)} /> : null} */}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>

            </div>
        );
    }
}

export default Datasync;
