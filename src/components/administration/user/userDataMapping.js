import React from 'react';
import MappingData from './mappingData';

class UserDataMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataMapping: false,
            currentTab: "item",
            data: {...this.props.userDataMappingData},
            genericDataModalShow: false,
            genericSearchData: {},
            selectedKey: "",
            selectedCode: "",
            selectedItems: [],
            selectedCodes: [],
            search: "",
            dataChange: false
        }
    }

    componentDidMount() {
        this.handleTabChange("item");
        // if (this.state.data.userData !== undefined && this.state.data.userData.length !== 0) {
        //     let itemData = this.state.data.userData.filter((item) => item.key === "ITEM_MAPPING");
        //     console.log(itemData);
        //     if (itemData.length !== 0) {console.log("inner if");
        //         var selectedKey = itemData[0].column_key;
        //         var selectedCode = itemData[0].column_key;
        //         var selectedItems = JSON.parse(itemData[0].search_value);
        //         var selectedCodes = itemData[0].value === undefined ? [] : JSON.parse(itemData[0].value);
        //         var selectedIndex = 0;
        //         JSON.parse(this.state.data.item).forEach((item, index) => {
        //             if (item.key == selectedKey || (item.code !== undefined && item.code == selectedCode)) {
        //                 selectedIndex = index + 1;
        //                 selectedKey = item.key;
        //                 selectedCode = item.code === undefined ? "" : item.code;
        //             }
        //         });
        //         this.setState({
        //             selectedKey,
        //             selectedCode,
        //             selectedItems,
        //             selectedCodes
        //         });
        //         document.getElementById("itemSelect").selectedIndex = selectedIndex;
        //     }
        // }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
            this.props.getArsGenericFiltersClear();
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getArsGenericFiltersClear();
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isLoading) {
            console.log("loading");
            this.setState({
            });
        }
    }

    siteData = (e, entity, key, code, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, code, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, code, search, pageNo) => {
        //if (this.state.genericDataModalShowRow != key) this.closeGenericDataModal();
        if (this.state.selectedKey !== "") {
            this.props.getArsGenericFiltersRequest({
                entity: entity,
                key: key,
                code: code,
                search: search,
                pageNo: pageNo
            });
            this.setState({genericSearchData: {payload: {entity: entity, key: key, code: this.state.selectedCode, search: search}}, genericDataModalShowRow: key});
        }
    }

    closeGenericDataModal = () => {
        this.setState({
            genericDataModalShow: false,
            search: ""
        });
    }

    selectItems = (entity, key, items, itemCodes) => {
        this.setState({
            selectedItems: items,
            selectedCodes: itemCodes,
            dataChange: true
        });
    }

    handleTabChange = (tab) => {
        var selectedKey = "";
        var selectedCode = "";
        var selectedItems = [];
        var selectedCodes = [];
        var selectedIndex = 0;
        if (this.state.data.userData !== undefined && this.state.data.userData.length !== 0) {
            if (tab === "item") {
                let itemData = this.state.data.userData.filter((item) => item.key === "ITEM_MAPPING");
                if (itemData.length !== 0) {
                    var selectedKey = itemData[0].column_key;
                    var selectedCode = itemData[0].column_key;
                    var selectedItems = JSON.parse(itemData[0].search_value);
                    var selectedCodes = itemData[0].value === undefined ? [] : JSON.parse(itemData[0].value);
                    JSON.parse(this.state.data.item).forEach((item, index) => {
                        if (item.key == selectedKey || (item.code !== undefined && item.code == selectedCode)) {
                            selectedIndex = index + 1;
                            selectedKey = item.key;
                            selectedCode = item.code === undefined ? "" : item.code;
                        }
                    });
                }
            }
            else if (tab === "site") {
                let itemData = this.state.data.userData.filter((item) => item.key === "SITE_MAPPING");
                if (itemData.length !== 0) {
                    var selectedKey = itemData[0].column_key;
                    var selectedCode = itemData[0].column_key;
                    var selectedItems = JSON.parse(itemData[0].search_value);
                    var selectedCodes = itemData[0].value === undefined ? [] : JSON.parse(itemData[0].value);
                    JSON.parse(this.state.data.site).forEach((item, index) => {
                        if (item.key == selectedKey || (item.code !== undefined && item.code == selectedCode)) {
                            selectedIndex = index + 1;
                            selectedKey = item.key;
                            selectedCode = item.code === undefined ? "" : item.code;
                        }
                    });
                }
            }
            else if (tab === "vendor") {
                let itemData = this.state.data.userData.filter((item) => item.key === "VENDOR_MAPPING");
                if (itemData.length !== 0) {
                    var selectedKey = itemData[0].column_key;
                    var selectedCode = itemData[0].column_key;
                    var selectedItems = JSON.parse(itemData[0].search_value);
                    var selectedCodes = itemData[0].value === undefined ? [] : JSON.parse(itemData[0].value);
                    JSON.parse(this.state.data.vendor).forEach((item, index) => {
                        if (item.key == selectedKey || (item.code !== undefined && item.code == selectedCode)) {
                            selectedIndex = index + 1;
                            selectedKey = item.key;
                            selectedCode = item.code === undefined ? "" : item.code;
                        }
                    });
                }
            }
        }
        document.getElementById(tab + "Select").selectedIndex = selectedIndex;
        this.setState({
            currentTab: tab,
            genericDataModalShow: false,
            genericSearchData: {},
            selectedKey,
            selectedCode,
            selectedItems,
            selectedCodes,
            search: "",
            dataChange: false
        });
    }

    handleAttrChange = (e) => {
        this.setState({
            selectedKey: JSON.parse(e.target.value).key === undefined ? e.target.value : JSON.parse(e.target.value).key,
            selectedCode: JSON.parse(e.target.value).code === undefined ? "" : JSON.parse(e.target.value).code,
            selectedItems: [],
            selectedCodes: [],
            search: "",
            dataChange: true
        });
    }

    handleSubmit = () => {
        if (this.state.currentTab !== "manager" && this.state.selectedKey !== "" && this.state.selectedItems.length !== 0) {
            this.props.updateUserDataMappingRequest({
                userId: this.props.userDataMappingData.id,
                key: this.state.currentTab === "item" ? "ITEM_MAPPING" : this.state.currentTab === "site" ? "SITE_MAPPING" : "VENDOR_MAPPING",
                column: this.state.selectedCode == undefined || this.state.selectedCode == "" ? this.state.selectedKey : this.state.selectedCode,
                value: this.state.selectedCode == undefined || this.state.selectedCode == "" ? null : this.state.selectedCodes,
                search_value: this.state.selectedItems
            });
            this.setState({
                dataChange: false
            });
        }
    }

    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content user-data-mapping-modal">
                    <div className="udmm-head">
                        <h3>User Data Mapping</h3>
                        <button type="button" onClick={this.props.closeUserDataMapping}><img src={require('../../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-32" role="tablist">
                            <li className="nav-item active">
                                <a className="nav-link st-btn" href="#Itemdata" role="tab" data-toggle="tab" onClick={() => this.handleTabChange("item")}>Item Data</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#Sitedata" role="tab" data-toggle="tab" onClick={() => this.handleTabChange("site")}>Site Data</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#Vendordata" role="tab" data-toggle="tab" onClick={() => this.handleTabChange("vendor")}>Vendor Data</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#Addmanager" role="tab" data-toggle="tab" onClick={() => this.handleTabChange("manager")}>Add Manager</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="Itemdata" role="tabpanel">
                            <div className="udmm-body">
                                <h3>Add New Item Data</h3>
                                <div className="pi-new-layout">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label className="pnl-purchase-label">Attributes</label>
                                        <select id="itemSelect" className="pnl-purchase-select onFocus" onChange={(e) => this.handleAttrChange(e)}>
                                            <option value="">Select Value</option>{
                                            this.state.data.item !== undefined && this.state.data.item.length !== 0 ?
                                            JSON.parse(this.state.data.item).map((item) =>
                                                <option value={`{"key": "${item.key}", "code": "${item.code}"}`}>{item.value}</option>
                                            ) :
                                            <option></option>
                                        }</select>
                                    </div>
                                    <div className="col-lg-5 pad-lft-0 disFlexEnd">
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input"
                                                id="itemFocus"
                                                placeholder={this.state.selectedItems.length === 0 ? "Search data for mapping" : this.state.selectedItems.length === 1 ? this.state.selectedItems[0] + " selected" : this.state.selectedItems[0] + " + " + (this.state.selectedItems.length - 1) + " selected"}
                                                value={this.state.search} onChange={(e) => this.setState({search: e.target.value})}
                                                onFocus={(e) => {if (this.state.genericDataModalShow === false) this.openGenericDataModal("item", this.state.selectedKey, this.state.selectedCode, e.target.value, 1)}}
                                                //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                onKeyDown={(e) => this.siteData(e, "item", this.state.selectedKey, this.state.selectedCode, e.target.value, 1)}
                                            />
                                            <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("item", this.state.selectedKey, this.state.selectedCode, this.state.search, 1)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.genericDataModalShow ? <MappingData data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems} selectedCodes={this.state.selectedCodes} /> : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade in" id="Sitedata" role="tabpanel">
                            <div className="udmm-body">
                                <h3>Add New Site Data</h3>
                                <div className="pi-new-layout">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label className="pnl-purchase-label">Attributes</label>
                                        <select id="siteSelect" className="pnl-purchase-select onFocus" onChange={(e) => this.handleAttrChange(e)}>
                                            <option value="">Select Value</option>{
                                            this.state.data.site !== undefined && this.state.data.site.length !== 0 ?
                                            JSON.parse(this.state.data.site).map((site) =>
                                                <option value={`{"key": "${site.key}"}`}>{site.value}</option>
                                            ) :
                                            <option></option>
                                        }</select>
                                    </div>
                                    <div className="col-lg-5 pad-lft-0 disFlexEnd">
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input"
                                                id="siteFocus"
                                                placeholder={this.state.selectedItems.length === 0 ? "Search data for mapping" : this.state.selectedItems.length === 1 ? this.state.selectedItems[0] + " selected" : this.state.selectedItems[0] + " + " + (this.state.selectedItems.length - 1) + " selected"}
                                                value={this.state.search} onChange={(e) => this.setState({search: e.target.value})}
                                                onFocus={(e) => {if (this.state.genericDataModalShow === false) this.openGenericDataModal("site", this.state.selectedKey, this.state.selectedCode, e.target.value, 1)}}
                                                //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                onKeyDown={(e) => this.siteData(e, "site", this.state.selectedKey, this.state.selectedCode, e.target.value, 1)}
                                            />
                                            <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("site", this.state.selectedKey, this.state.selectedCode, this.state.search, 1)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.genericDataModalShow ? <MappingData data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems} /> : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade in" id="Vendordata" role="tabpanel">
                            <div className="udmm-body">
                                <h3>Add New Vendor Data</h3>
                                <div className="pi-new-layout">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label className="pnl-purchase-label">Attributes</label>
                                        <select id="vendorSelect" className="pnl-purchase-select onFocus" onChange={(e) => this.handleAttrChange(e)}>
                                            <option value="">Select Value</option>{
                                            this.state.data.vendor !== undefined && this.state.data.vendor.length !== 0 ?
                                            JSON.parse(this.state.data.vendor).map((vendor) =>
                                                <option value={`{"key": "${vendor.key}"}`}>{vendor.value}</option>
                                            ) :
                                            <option></option>
                                        }</select>
                                    </div>
                                    <div className="col-lg-5 pad-lft-0 disFlexEnd">
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input"
                                                id="vendorFocus"
                                                placeholder={this.state.selectedItems.length === 0 ? "Search data for mapping" : this.state.selectedItems.length === 1 ? this.state.selectedItems[0] + " selected" : this.state.selectedItems[0] + " + " + (this.state.selectedItems.length - 1) + " selected"}
                                                value={this.state.search} onChange={(e) => this.setState({search: e.target.value})}
                                                onFocus={(e) => {if (this.state.genericDataModalShow === false) this.openGenericDataModal("vendor", this.state.selectedKey, this.state.selectedCode, e.target.value, 1)}}
                                                //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                onKeyDown={(e) => this.siteData(e, "vendor", this.state.selectedKey, this.state.selectedCode, e.target.value, 1)}
                                            />
                                            <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("vendor", this.state.selectedKey, this.state.selectedCode, this.state.search, 1)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.genericDataModalShow ? <MappingData data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems} /> : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade in" id="Addmanager" role="tabpanel">
                            <div className="udmm-body">
                                <h3>Add New Manager</h3>
                                <div className="pi-new-layout">
                                    <div className="col-lg-4 pad-lft-0">
                                        <label className="pnl-purchase-label">Level 1</label>
                                        <select className="pnl-purchase-select onFocus">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label className="pnl-purchase-label">Level 2</label>
                                        <select className="pnl-purchase-select onFocus">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <label className="pnl-purchase-label">Level 3</label>
                                        <select className="pnl-purchase-select onFocus">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="udmm-footer">
                        <button type="button" className={this.state.dataChange && this.state.selectedKey !== "" && this.state.selectedItems.length !== 0 ? "" : "btnDisabled"} disabled={this.state.dataChange && this.state.selectedKey !== "" && this.state.selectedItems.length !== 0 ? "" : "disabled"} onClick={this.handleSubmit}>Submit</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserDataMapping;