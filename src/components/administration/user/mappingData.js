import React from 'react';

class MappingData extends React.Component {
    constructor(props) {
        console.log("constr");
        super(props);
        this.state = {
            sites: [...this.props.data.data.resource.searchResultNew],
            prev: this.props.data.data.resource.previousPage,
            current: this.props.data.data.resource.currPage,
            next: this.props.data.data.resource.currPage + 1,
            maxPage: this.props.data.data.resource.maxPage,
            totalItems: this.props.data.data.resource.totalCount,
            checkedItems: this.props.selectedItems == undefined ? [] : [...this.props.selectedItems],
            checkedCodes: this.props.selectedCodes == undefined ? [] : [...this.props.selectedCodes]
        }
    }

    componentDidMount() {
        document.addEventListener("click", this.closeModal);
        document.addEventListener("keydown", this.closeModalEsc);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            sites: [...nextProps.data.data.resource.searchResultNew],
            prev: nextProps.data.data.resource.previousPage,
            current: nextProps.data.data.resource.currPage,
            next: nextProps.data.data.resource.currPage + 1,
            maxPage: nextProps.data.data.resource.maxPage,
            totalItems: nextProps.data.data.resource.totalCount,
            checkedItems: nextProps.selectedItems == undefined ? [] : [...nextProps.selectedItems],
            checkedCodes: nextProps.selectedCodes == undefined ? [] : [...nextProps.selectedCodes]
        });
    }

    componentWillUnmount() {
        // this.props.select(this.props.data.payload.entity, this.props.data.payload.key, this.state.checkedItems);
        document.removeEventListener("click", this.closeModal);
        document.removeEventListener("keydown", this.closeModalEsc);
    }

    closeModal = (e) => {
        if (!(e.path.some((element) => element.id === "genericDataModal")) && e.target.id !== this.props.data.payload.entity + "Focus") {
            console.log(e);
            this.props.close();
        }
    }

    closeModalEsc = (e) => {
        if(e.keyCode == 27) {
            this.props.close();
        }
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.data.data.resource.previousPage,
                    current: this.props.data.data.resource.currPage,
                    next: this.props.data.data.resource.currPage + 1,
                    maxPage: this.props.data.data.resource.maxPage
                })
                if (this.props.data.data.resource.previousPage != 0) {
                    this.props.action({
                        entity: this.props.data.payload.entity,
                        key: this.props.data.payload.key,
                        search: this.props.data.payload.search,
                        pageNo: this.props.data.data.resource.previousPage
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.data.data.resource.previousPage,
                    current: this.props.data.data.resource.currPage,
                    next: this.props.data.data.resource.currPage + 1,
                    maxPage: this.props.data.data.resource.maxPage
                })
            if (this.props.data.data.resource.currPage != this.props.data.data.resource.maxPage) {
                this.props.action({
                    entity: this.props.data.payload.entity,
                    key: this.props.data.payload.key,
                    search: this.props.data.payload.search,
                    pageNo: this.props.data.data.resource.currPage + 1
                });
            }
        }
    }

    handleChange = (item, code) => {
        let checkedItems = [...this.state.checkedItems];
        let checkedCodes = [...this.state.checkedCodes];
        let index = checkedItems.indexOf(item);
        if (index === -1) {
            checkedItems.push(item);
        }
        else {
            checkedItems.splice(index, 1);
        }
        if (code != undefined) {
            let indexCode = checkedCodes.indexOf(code);
            if (indexCode === -1) {
                checkedCodes.push(code);
            }
            else {
                checkedCodes.splice(indexCode, 1);
            }
        }
        this.setState({checkedItems, checkedCodes});
        this.props.select(this.props.data.payload.entity, this.props.data.payload.key, checkedItems, checkedCodes);
    }

    render() {
        console.log("render props:", this.props);
        console.log("render state:", this.state);
        return (
            <div className="dropdown-menu-city1 dropdown-menu-vendor zi999" id="genericDataModal">
                <ul className="dropdown-menu-city-item">
                {
                    this.state.checkedItems.length === 0 ? null :
                    <div className="dmci-checked">
                    {
                        this.state.checkedItems.map((item) =>
                            <li>
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" checked={this.state.checkedItems.indexOf(item) !== -1} onChange={() => this.handleChange(item, this.state.sites.filter((data) => data[this.props.data.payload.key] == item)[0][this.props.data.payload.code])} />
                                    <span className="checkmark1"></span>
                                </label>
                                <span className="vendor-details displayBlock">
                                    <span className="vd-name div-col-1">{item}</span>
                                </span>
                            </li>
                        )
                    }
                    </div>
                }
                {
                    this.state.sites.length == 0 ?
                    <li>
                        <span className="vendor-details displayBlock">
                            <span className="vd-name div-col-1">No search data found!</span>
                        </span>
                    </li> :
                    this.state.sites.map((item) =>
                        this.state.checkedItems.indexOf(item[this.props.data.payload.key]) !== -1 ?
                        null :
                        <li>
                            <label className="checkBoxLabel0">
                                <input type="checkBox" checked={this.state.checkedItems.indexOf(item[this.props.data.payload.key]) !== -1} onChange={() => this.handleChange(item[this.props.data.payload.key], item[this.props.data.payload.code])} />
                                <span className="checkmark1"></span>
                            </label>
                            <span className="vendor-details displayBlock">
                                <span className="vd-name div-col-1">{item[this.props.data.payload.code] === undefined || item[this.props.data.payload.code] === "" ? item[this.props.data.payload.key] : item[this.props.data.payload.code] + " - " + item[this.props.data.payload.key]}</span>
                            </span>
                        </li>
                    )
                }
                </ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={this.props.close}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        <button className="pnpb-prev" type="button" id="prev" onClick={(e) => this.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                        <button className="pnpb-no" type="button" disabled>{this.state.current}/{this.state.maxPage}</button>
                        <button className="pnpb-next" type="button" id="next" onClick={(e) => this.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default MappingData;