import React from 'react';

class UserFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // data: [
            //     { "id": 0, "text": "Administartion" },
            //     { "id": 1, "text": "Manage Role" },
            //     { "id": 2, "text": "Admin" },
            //     { "id": 3, "text": "Manage Site" },
            //     { "id": 4, "text": "Site Mapping" },
            //     { "id": 5, "text": "Site" }
            // ],
            firstName: "",
            middleName: "",
            lastName: "",
            partnerEnterpriseName: "",
            userName: "",
            email: "",
            status: ""
        };
    }
    clearFilter(e) {
        e.preventDefault
        this.setState({
            firstName: "",
            middleName: "",
            lastName: "",
            partnerEnterpriseName: "",
            userName: "",
            email: "",
            status: ""
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            firstName: nextProps.firstName,
            middleName: nextProps.middleName,
            lastName: nextProps.lastName,
            partnerEnterpriseName: nextProps.partnerEnterpriseName,
            userName: nextProps.userName,
            email: nextProps.email,
            status: nextProps.status
        })
    }

    handleChange(e) {
        if (e.target.id == "firstName") {
            this.setState({
                firstName: e.target.value
            });
        } else if (e.target.id == "lastName") {
            this.setState({
                lastName: e.target.value
            });
        } else if (e.target.id == "middleName") {
            this.setState({
                middleName: e.target.value
            });
        } else if (e.target.id == "partnerEnterpriseName") {
            this.setState({
                partnerEnterpriseName: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        } else if (e.target.id == "email") {
            this.setState({
                email: e.target.value
            });
        } else if (e.target.id == "userName") {
            this.setState({
                userName: e.target.value
            });
        }
    }

    onSubmit(e) {
        e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            search: this.state.search,
            firstName: this.state.firstName,
            middleName: this.state.middleName,
            lastName: this.state.lastName,
            partnerEnterpriseName: this.state.partnerEnterpriseName,
            userName: this.state.userName,
            email: this.state.email,
            status: this.state.status
        }
        this.props.userRequest(data);
        this.props.filter(e);
        this.props.updateFilter(data);
    }
    render() {

        let count = 0;
        if (this.state.firstName != "") {
            count++;
        }
        if (this.state.middleName != "") {
            count++;
        }
        if (this.state.lastName != "") {
            count++;
        }
        if (this.state.partnerEnterpriseName != "") {
            count++;
        }
        if (this.state.userName != "") {
            count++;
        }
        if (this.state.email != "") {
            count++;
        }
        if (this.state.status != "") {
            count++;
        }
        const { search } = this.state;
        var result = _.filter(this.state.data, function (data) {
            return _.startsWith(data.text, search);
        });

        return (
            <div>
                <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myModal">
                    <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                    <form onSubmit={(e) => this.onSubmit(e)}>

                        <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                            <button type="button" onClick={(e) => this.props.filter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <div className="col-md-12 col-sm-12 ">
                                <ul className="list-inline">
                                    <li>
                                        <label className="filter_modal">
                                            FILTERS

                                </label>
                                    </li>
                                    <li>
                                        <label className="filter_text">
                                            {count} Filters applied
                                </label>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-md-12 col-sm-12">
                                <div className="container_modal">
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0 filterFirstRow m-top-10">
                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <input onChange={(e) => this.handleChange(e)} id="partnerEnterpriseName" value={this.state.partnerEnterpriseName} type="text" placeholder="Enterprise Name" className="userModalInput" />
                                        </div>
                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <input onChange={(e) => this.handleChange(e)} id="firstName" value={this.state.firstName} type="text" placeholder="First Name" className="userModalInput" />
                                        </div>
                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <input onChange={(e) => this.handleChange(e)} id="middleName" value={this.state.middleName} placeholder="Middle Name" type="text" className="userModalInput" />
                                        </div>
                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <input onChange={(e) => this.handleChange(e)} id="lastName" value={this.state.lastName} type="text" placeholder="Last Name" className="userModalInput" />
                                        </div>
                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <input onChange={(e) => this.handleChange(e)} id="userName" value={this.state.userName} type="text" placeholder="User Name" className="userModalInput" />
                                        </div>
                                        {/* <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                    <input  onChange={(e) => this.handleChange(e)}   id="email" value={this.state.email}  type="text" placeholder="Email" className="userModalInput" />
                                </div>
                            
                                <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                        <select  onChange={(e) => this.handleChange(e)}   id="status" value={this.state.status}  className="userModalSelect">
                                            <option value="">
                                                Status
                                            </option>
                                            <option value="Active">
                                                Active
                                                </option>
                                                <option value="Inactive">
                                                Inactive
                                                </option>
                                        </select>
                                </div> */}
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <input onChange={(e) => this.handleChange(e)} id="email" value={this.state.email} type="text" placeholder="Email" className="userModalInput" />
                                        </div>

                                        <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                            <select onChange={(e) => this.handleChange(e)} id="status" value={this.state.status} className="userModalSelect">
                                                <option value="">
                                                    Status
                                            </option>
                                                <option value="Active">
                                                    Active
                                                </option>
                                                <option value="Inactive">
                                                    Inactive
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="col-md-6 float_right pad-0 m-top-20">
                                    <ul className="list-inline text_align_right">
                                        <li>
                                            {this.state.firstName == "" && this.state.middleName == "" && this.state.lastName == "" && this.state.partnerEnterpriseName == "" && this.state.userName == "" && this.state.email == "" && this.state.status == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                                : <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                                    CLEAR FILTER
                                    </button>}
                                        </li>
                                        <li>
                                            {this.state.firstName != "" || this.state.lastName != "" || this.state.middleName != "" || this.state.partnerEnterpriseName != "" || this.state.userName != "" || this.state.email != "" || this.state.status != "" ? <button type="submit" className="modal_Apply_btn">
                                                APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                    APPLY
                                        </button>}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        )
    }
}

export default UserFilter;