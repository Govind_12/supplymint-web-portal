import React from 'react';
import RightSideBar from "../../rightSideBar";
import Footer from '../../footer'
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";
import NewSideBar from "../../../components/newSidebar";

class AddUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            errorMessage: "",
            successMessage: "",
            first: "",
            middle: "",
            last: "",
            user: "",
            email: "",
            mobile: "",
            active: "Active",
            phone: "",
            access: "UI",
            address: "",
            firsterr: false,
            errorCode: "",
            // middleerr: false,
            lasterr: false,
            usererr: false,
            emailerr: false,
            mobileerr: false,
            activeerr: false,
            phoneerr: false,
            // accesserr: false,
            // addresserr: false,
            data: [
                { "id": 0, "text": "Administartion" },
                { "id": 1, "text": "Manage Role" },
                { "id": 2, "text": "Admin" },
                { "id": 3, "text": "Manage Site" },
                { "id": 4, "text": "Site Mapping" },
                { "id": 5, "text": "Site" }
            ],
            roleData: [],
            selected: [],
            selectederr: "",
            current: "",
            open: false,
            search: "",
            loader: false,
            success: false,
            alert: false,
            rightbar: false,
            openRightBar: false,
            code: ""
        };
    }
    componentDidMount() {
        this.props.allRoleRequest();
        sessionStorage.setItem('currentPage', "RADMMAIN")
        sessionStorage.setItem('currentPageName', "Users")
    }
    componentWillReceiveProps(nextProps) {


        if (nextProps.administration.allRole.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.allRole.isSuccess) {
            let x = this.state.roleData.concat(this.state.selected)
            if (nextProps.administration.allRole.data.resource.length <= x.length) {
                // nextProps.administration.allRole.data.resource = x
                this.setState({
                    loader: false,
                    // roleData: nextProps.administration.allRole.data.resource
                })
            }
            else {
                this.setState({
                    loader: false,
                    roleData: nextProps.administration.allRole.data.resource
                })
            }
        }
        if (nextProps.administration.addUser.isSuccess) {
            this.setState({
                loader: false,
                successMessage: nextProps.administration.addUser.data.message,
                success: true
            })   
            this.onClear();
        } else if (nextProps.administration.addUser.isError) {
            this.setState({
                errorCode: nextProps.administration.addUser.message.error == undefined ? undefined : nextProps.administration.addUser.message.error.errorCode,
                loader: false,
                code: nextProps.administration.addUser.message.status,
                errorMessage: nextProps.administration.addUser.message.error == undefined ? undefined : nextProps.administration.addUser.message.error.errorMessage,
                alert: true,
                roleData: this.state.roleData
            })
            // this.props.addUserRequest();
            this.props.addUserClear();
        } else if (nextProps.administration.addUser.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    onClear() {
        this.setState({
            first: "",
            search: "",
            middle: "",
            last: "",
            user: "",
            email: "",
            mobile: "",
            active: "",
            phone: "",
            access: "UI",
            address: "",
            selected: [],
            roleData: this.state.roleData.concat(this.state.selected)
        })
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        },()=>{
            this.props.addUserClear();
            this.props.history.push("/administration/users")
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    onDelete =(e)=> {
        let match = this.state.selected.filter( data => data.id == e )
        this.setState(preState=>({
            roleData: preState.roleData.concat(match)
        }));

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            //open: false,
        })
    }
    Change(e) {
        this.setState({
            search: e.target.value
        })
    }

    onnChange =(e)=> {
        e.preventDefault();
        this.setState({
            current: e.target.value,
        })
        let match = this.state.roleData.filter( data =>
            data.id == e.target.value
        )

        this.setState({
            selected: this.state.selected.concat(match)
        });
        let idd = this.state.roleData.filter( data => data.id == e.target.value)[0] !== undefined ? this.state.roleData.filter( data => data.id == e.target.value)[0].id
                  : "";
        var array = this.state.roleData;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            roleData: array,
            selectederr: false,
            //open: false,
        })

    }

    handleChange(e) {
        e.preventDefault();
        if (e.target.id == "first") {
            this.setState(
                {
                    first: e.target.value
                },
                () => {
                    this.first();
                }
            );
        } else if (e.target.id == "last") {
            this.setState(
                {
                    last: e.target.value
                },
                () => {
                    this.last();
                }
            );
        } 
        // else if (e.target.id == "middle") {
        //     this.setState(
        //         {
        //             middle: e.target.value
        //         },
        //         () => {
        //             this.middle();
        //         }
        //     );
        // }
         else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );
        } else if (e.target.id == "mobile") {
            this.setState(
                {
                    mobile: e.target.value
                },
                () => {
                    this.mobile();
                }
            );
        } 
        // else if (e.target.id == "phone") {
        //     this.setState(
        //         {
        //             phone: e.target.value
        //         },
        //         () => {
        //             this.phone();
        //         }
        //     );
        // } 
        else if (e.target.id == "access") {
            this.setState(
                {
                    access: e.target.value
                },
                () => {
                    // this.access();
                }
            );
        } 
        // else if (e.target.id == "address") {
        //     this.setState(
        //         {
        //             address: e.target.value
        //         },
        //         () => {
        //             // this.address();
        //         }
        //     );
        // } 
        else if (e.target.id == "active") {
            this.setState(
                {
                    active: e.target.value
                },
                () => {
                    this.active();
                }
            );
        } else if (e.target.id == "user") {
            this.setState(
                {
                    user: e.target.value
                },
                () => {
                    this.user(e);
                }
            );
        }
    }
    first() {
        if (
            this.state.first == "" || this.state.first.trim() == "" || !this.state.first.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                firsterr: true
            });
        } else {
            this.setState({
                firsterr: false
            });
        }
    }
    // middle() {
    //     if (this.state.middle == "") {
    //         this.setState({
    //             middleerr: false
    //         })
    //     }
    //     else if (!this.state.middle.match(/^[a-zA-Z ]+$/)) {
    //         this.setState({
    //             middleerr: true
    //         });
    //     }
    // }
    active() {
        if (
            this.state.active == "" || this.state.active.trim() == "") {
            this.setState({
                activeerr: true
            });
        } else {
            this.setState({
                activeerr: false
            });
        }
    }
    access() {
        if (
            this.state.access == "" || this.state.access.trim() == "") {
            this.setState({
                accesserr: true
            });
        } else {
            this.setState({
                accesserr: false
            });
        }
    }
    email() {
        if (
            this.state.email == "" || !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }

    // phone() {
    //     if (this.state.phone == "" || this.state.phone.trim() == "") {
    //         this.setState({
    //             phoneerr: false
    //         })
    //     }
    //     else if (this.state.phone.length < 10) {
    //         this.setState({
    //             phoneerr: true
    //         });
    //     }
    //     else {
    //         this.setState({
    //             phoneerr: false
    //         });
    //     }
    // }
    mobile() {
        if (
            this.state.mobile == "" || !this.state.mobile.match(/^\d{10}$/) ||
            !this.state.mobile.match(/^[1-9]\d+$/) ||
            !this.state.mobile.match(/^.{10}$/)) {
            this.setState({
                mobileerr: true
            });
        } else {
            this.setState({
                mobileerr: false
            });
        }
    }
    // address() {
    //     if (
    //         this.state.address == "" || this.state.address.trim() == "") {
    //         this.setState({
    //             addresserr: true
    //         });
    //     } else {
    //         this.setState({
    //             addresserr: false
    //         });
    //     }
    // }
    user(e) {
        if (
            this.state.user.includes(" ") || this.state.user == "" || this.state.user.trim() == "" || !this.state.user.match(/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/)) {
            this.setState({
                usererr: true
            });
        } else {
            this.setState({
                usererr: false
            });
        }
    }
    last() {
        if (
            this.state.last == "" || this.state.last.trim() == "" || !this.state.last.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                lasterr: true
            });
        } else {
            this.setState({
                lasterr: false
            });
        }
    }
    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true
            });
        } else {
            this.setState({
                selectederr: false
            })
        }
    }
    onFormSubmit(e) {
        e.preventDefault();
        this.first();
        this.last();
        //this.middle();
        this.email();
        //this.phone();
        this.mobile();
        // this.access();
        this.active();
        // this.address();
        this.selected();
        this.user();
        const t = this;
        var uType = sessionStorage.getItem('uType')
        setTimeout(function () {
            const { selected, first, firsterr, last, lasterr, email, emailerr, mobile, mobileerr, access, accesserr, active, activeerr, user, usererr, selectederr } = t.state;
            if (!selectederr && !firsterr && !lasterr && !emailerr && !mobileerr && !activeerr && !usererr) {
                let data = {
                    first: first,
                    //middle: middle,
                    last: last,
                    email: email,
                    mobile: mobile,
                   // phone: phone,
                    access: access == "" ? "NA" : access,
                    active: active,
                   // address: address == "" ? "NA" : address,
                    user: user,
                    selected: selected,
                    uType,
                    userAsAdmin: false
                }
                t.props.addUserRequest(data); 

            }
        }, 100)
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    openClose(e) {
        e.preventDefault();
        this.setState({
            open: !this.state.open
        },()=>{
            if(this.state.open)
             document.addEventListener("click", this.closeRoleModal)
            else
              document.removeEventListener("click", this.closeRoleModal)
        })
    }

    closeRoleModal =(e)=>{
       if( e !== null && e.target !== undefined && e.target.parentElement !== null && !e.target.parentElement.className.includes("dropdownDiv")){
           this.setState({ open: false},()=>{
              document.removeEventListener("click", this.closeRoleModal)
           })
       }
    }

    render() {

        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        $("body").on("keyup", ".numbersOnlywithhighpn", function () {
            if (this.value != this.value.replace(/[^0-9-]/g, '')) {
                this.value = this.value.replace(/[^0-9-]/g, '');
            }
        });
        const { search, roleData, first, firsterr, last, lasterr, email, emailerr, mobile, mobileerr, access, accesserr, active, activeerr, user, usererr, selectederr } = this.state;

        // var result = roleData.filter( data => {
        //     data.name.toLowerCase().includes(search.toLowerCase());
        // });

        return (
            <div>
                <div className="container-fluid nc-design pad-l60">
                    <div className="container_div" id="home">
                        <NewSideBar {...this.props} />
                        <SideBar {...this.props} />
                        <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}

                        <div className="container_div m-top-75 " id="">
                            <div className="col-md-12 col-sm-12 border-btm">
                                <div className="menu_path n-menu-path">
                                    <ul className="list-inline width_100 pad20 nmp-inner">
                                        <BraedCrumps {...this.props} />
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="container-fluid pad-0">
                            <div className="container_div" id="">
                                <div className="container-fluid pad-0">
                                    <form onSubmit={(e) => this.onFormSubmit(e)}>
                                        <div className="col-lg-12 pad-0 ">
                                            <div className="gen-vendor-potal-design p-lr-47">
                                                <div className="col-lg-6 pad-0">
                                                    <div className="gvpd-left">
                                                        <button className="gvpd-back" onClick={() => this.props.history.push("/administration/users")} type="reset">
                                                            <span className="back-arrow">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                                                    <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                                                </svg>
                                                            </span>
                                                            Back
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 pad-0">
                                                    <div className="gvpd-right">
                                                        <button className="gen-clear" onClick={() => this.onClear()} type="reset">Clear</button>
                                                        <button className="gen-save" type="submit">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="add-vendor-page-layout">
                                            <div className="user-sticky-div">
                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="turning">Retailer Name</label>
                                                        <input type="text" disabled className="form-box" id="turning" placeholder={`${sessionStorage.getItem('partnerEnterpriseName')}`} />
                                                    </div>
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="first">First Name</label>
                                                        <input type="text" maxlength="80" className={firsterr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} value={first} id="first" placeholder="First Name" />
                                                        {/* {firsterr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                        {firsterr ? (
                                                            <span className="error">
                                                                Enter first name
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                    {/* <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="middle"></label>
                                                        <input type="text" className={middleerr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} id="middle" value={middle} placeholder="Middle Name" />
                                                        {/* {middleerr ? <img src={errorIcon} className="error_icon" /> : null} 
                                                        {middleerr ? (
                                                            <span className="error">
                                                                Enter middle name
                                                            </span>
                                                        ) : null}
                                                    </div> */}
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="last">Last Name</label>
                                                        <input type="text" maxlength="80" className={lasterr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} id="last" value={last} placeholder="Last Name" />
                                                        {/* {lasterr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                        {lasterr ? (
                                                            <span className="error">
                                                                Enter last name
                                                            </span>
                                                        ) : null}
                                                    </div>

                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="user">Username</label>
                                                        <input type="text" maxlength="80" className={usererr ? "errorBorder form-box" : "form-box"} id="user" onChange={(e) => this.handleChange(e)} value={user} placeholder="Username" />
                                                        {/* {usererr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                        {usererr ? (
                                                            <span className="error">
                                                                Enter username
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 ">
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="email">Email</label>
                                                        <input type="email" maxlength="200" className={emailerr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} id="email" value={email} placeholder="Email" />
                                                        {/* {emailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                        {emailerr ? (
                                                            <span className="error">
                                                                Enter valid email
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="mobile">Mobile Number</label>
                                                        <input type="text" maxLength="10" className={mobileerr ? "errorBorder form-box numbersOnly" : "form-box numbersOnly"} onChange={(e) => this.handleChange(e)} id="mobile" value={mobile} placeholder="Mobile Number" />
                                                        {/* {mobileerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                        {mobileerr ? (
                                                            <span className="error">
                                                                Enter valid mobile number
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="active">Status</label>
                                                        <select className={activeerr ? "errorBorder form-box displayPointer" : "form-box displayPointer"} onChange={(e) => this.handleChange(e)} value={active} id="active" placeholder="Active">
                                                            <option value="">Status</option>
                                                            <option value="Active" >Active</option>
                                                            <option value="Inactive" >Inactive</option>
                                                        </select>
                                                        {/* {activeerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                        {activeerr ? (
                                                            <span className="error">
                                                                Select active
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                    {/* <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="phone"></label>
                                                        <input type="text" maxLength="15" className={phoneerr ? "errorBorder form-box numbersOnlywithhighpn" : "form-box numbersOnlywithhighpn"} onChange={(e) => this.handleChange(e)} value={phone} id="phone" placeholder="Work Phone" />
                                                        {/* {phoneerr ? <img src={errorIcon} className="error_icon" /> : null} 
                                                        {phoneerr ? (
                                                            <span className="error">
                                                                Enter valid phone number
                                                            </span>
                                                        ) : null}
                                                    </div> */}
                                                    <div className="col-md-2 col-sm-2 pad-lft-0">
                                                        <label htmlFor="access" >Access Mode</label>
                                                        <select className={accesserr ? "errorBorder form-box displayPointer" : "form-box displayPointer"} placeholder="Access Mode" id="access" onChange={(e) => this.handleChange(e)} value={access} aria-placeholder="Accessmode" >

                                                            <option value="UI">UI</option>
                                                            <option value="API">API</option>
                                                            {/* {accesserr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                            {accesserr ? (
                                                                <span className="error">
                                                                    Select access mode
                                                                </span>
                                                            ) : null}
                                                        </select>
                                                    </div>
                                                </div>
                                                {/* <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 ">
                                                    <div className="col-md-4 col-sm-4 pad-lft-0">
                                                        <label htmlFor="address"></label>
                                                        <textarea className={addresserr ? "errorBorder formtextarea" : "formtextarea"} onChange={(e) => this.handleChange(e)} value={address} id="address" placeholder="Address..." ></textarea>
                                                        {/* {addresserr ? <img src={errorIcon} className="user_error_icon-txtarea" /> : null} 
                                                        {addresserr ? (
                                                            <span className="error">
                                                                Enter valid address
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                </div> */}
                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 ">
                                                    <div className="ao-billing-details">
                                                        <div className="aobd-inner">
                                                            <h4>Add Roles</h4>
                                                            <p className="aobd-para">You can add multiple roles</p>
                                                        </div>
                                                    </div>
                                                    {/* <ul className="list_style ">
                                                        <li><p className="para-content">You can add multiple roles</p></li>
                                                    </ul> */}
                                                    <div className="col-md-12 col-sm-12 pad-0">
                                                        <ul className="list-inline m-top-10 width_100 m-bot-10">
                                                            <li>
                                                                {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.id} className="role-user-name">
                                                                    <span type="button" className="role-user-name-inner">{data.name} </span>
                                                                    <span onClick={(e) => this.onDelete(data.id)} value={data.id} className="roleun-close" aria-hidden="true">&times;</span>
                                                                </div>)}

                                                            </li>

                                                        </ul>
                                                    </div>

                                                    <div className="col-md-2 col-xs-12 pad-0 m-rgt-20">

                                                        <div className={selectederr ? "errorBorder userModalSelect displayPointer" : "userModalSelect displayPointer"} onClick={(e) => this.openClose(e)}>

                                                            <label className="displayPointer">Choose Role
                                                        {/* <span>
                                                                    ▾
                                                        </span> */}
                                                            </label>
                                                            {/* {selectederr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                            {selectederr ? (
                                                                <span className="error">
                                                                    Select Role
                                                        </span>
                                                            ) : null}

                                                        </div>
                                                        {this.state.open ? <div className="dropdownDiv m-top-10">

                                                            {/* <input type="search" className="searchFilterModal" value={search} onChange={(e) => this.Change(e)} /> */}
                                                            <ul className="dropdownFilterOption">
                                                                {roleData.map((data, i) => <li value={data.id} key={data.id} onClick={(e) => this.onnChange(e)} >
                                                                    {data.name}
                                                                </li>)}
                                                            </ul>

                                                        </div> : null}
                                                    </div>
                                                    {/* <AsyncSelect isMulti cacheOptions defaultOptions loadOptions={promiseOptions} />   */}
                                                </div>
                                            </div>

                                            {/* <div className="col-md-12 col-sm-12 pad-0 ">
                                                <div className="footerDivForm">
                                                    <ul className="list-inline m-lft-0 pad-0 m-top-15">
                                                        <li>
                                                            <button className="clear_button_vendor" onClick={() => this.onClear()} type="reset">Clear</button>
                                                        </li>
                                                        <li>
                                                            <button className="save_button_vendor" type="submit">Save</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> */}
                                        </div>
                                    </form>

                                </div>

                            </div>
                            {this.state.loader ? <FilterLoader /> : null}
                            {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                            {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddUser);