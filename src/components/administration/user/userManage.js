import React from 'react';
import UserFilter from './userFilter';
import UserModal from './userModal';
import UserEditModal from './userEditModal';
import searchIcon from '../../../assets/clearSearch.svg';
import SearchImage from '../../../assets/searchicon.svg';
import refreshIcon from "../../../assets/refresh-block.svg";
import ToastLoader from '../../loaders/toastLoader';
import Pagination from '../../pagination';
import UserDataMapping from './userDataMapping';
import FilterLoader from "../../loaders/filterLoader";
import RequestError from "../../loaders/requestError";
import RequestSuccess from "../../loaders/requestSuccess";

class UserManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            success: false,
            successMessage: "",
            error: false,
            errorCode: "",
            errorMessage: "",
            userFilter: false,
            userModal: false,
            filter: false,
            filterBar: false,
            userEditModal: false,
            userId: "",
            userState: this.props.userState,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            no: 1,
            type: 1,
            search: "",
            firstName: "",
            middleName: "",
            lastName: "",
            partnerEnterpriseName: "",
            userName: "",
            email: "",
            status: "",
            summaryModalAnimation: false,
            toastLoader: false,
            toastMsg: "",
            showDownloadDrop: false,
            roleData:[],
            editModalOnRoleSuccess: false,
            jumpPage: 1,
            mobile: "",
            userDataMapping: false,
            userDataMappingData: {}
        }
    }

    openUserDataMapping(e, id) {
        e.preventDefault();
        this.setState({
            userDataMappingData: {
                id: id
            }
        }, () => this.props.getUserDataMappingRequest({userId: id}));
    }
    closeUserDataMapping = () => {
        this.setState({ userDataMapping: false
        });
    }

    componentDidMount(){
        // this.props.allRoleRequest();
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    componentWillMount() {
        if (this.props.administration.user.isSuccess) {
            this.setState({
                loading: false,
                prev: this.props.administration.user.data.prePage,
                current: this.props.administration.user.data.currPage,
                next: this.props.administration.user.data.currPage + 1,
                maxPage: this.props.administration.user.data.maxPage,
                jumpPage: this.props.administration.user.data.currPage,
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.user.isSuccess) {
            this.setState({
                loading: false,
                userState: nextProps.administration.user.data.resource,
                prev: nextProps.administration.user.data.prePage,
                current: nextProps.administration.user.data.currPage,
                next: nextProps.administration.user.data.currPage + 1,
                maxPage: nextProps.administration.user.data.maxPage,
                jumpPage: nextProps.administration.user.data.currPage,
            })

        }
        if (nextProps.administration.editUser.isSuccess) {
            let data = {
                loading: false,
                type: 1,
                no: 1,
                firstName: "",
                middleName: "",
                lastName: "",
                partnerEnterpriseName: "",
                userName: "",
                email: "",
                status: "",
                search: "",
            }
            this.props.userRequest(data)
        }
        if (nextProps.administration.allRole.isSuccess) {
            this.setState({
                loading: false,
                roleData: nextProps.administration.allRole.data.resource,
                userEditModal: this.state.editModalOnRoleSuccess ? true : false,
                userModalAnimation: this.state.editModalOnRoleSuccess ? true : false
            })
            this.props.allRoleClear()
        }

        if (nextProps.administration.getUserDataMapping.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                userDataMappingData: nextProps.administration.getUserDataMapping.data.resource !== null && Object.keys(nextProps.administration.getUserDataMapping.data.resource).length !== 0 ? {...this.state.userDataMappingData, ...nextProps.administration.getUserDataMapping.data.resource} : {},
                userDataMapping: true
            });
            this.props.getUserDataMappingClear();
        }
        else if (nextProps.administration.getUserDataMapping.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.administration.getUserDataMapping.message.status,
                errorCode: nextProps.administration.getUserDataMapping.message.error == undefined ? undefined : nextProps.administration.getUserDataMapping.message.error.errorCode,
                errorMessage: nextProps.administration.getUserDataMapping.message.error == undefined ? undefined : nextProps.administration.getUserDataMapping.message.error.errorMessage,
                userDataMappingData: {}
            });
            this.props.getUserDataMappingClear();
        }

        if (nextProps.administration.updateUserDataMapping.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                error: false,
                successMessage: nextProps.administration.updateUserDataMapping.data.message
            });
            this.props.updateUserDataMappingClear();
        }
        else if (nextProps.administration.updateUserDataMapping.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.administration.updateUserDataMapping.message.status,
                errorCode: nextProps.administration.updateUserDataMapping.message.error == undefined ? undefined : nextProps.administration.updateUserDataMapping.message.error.errorCode,
                errorMessage: nextProps.administration.updateUserDataMapping.message.error == undefined ? undefined : nextProps.administration.updateUserDataMapping.message.error.errorMessage
            });
            this.props.updateUserDataMappingClear();
        }

        if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getArsGenericFiltersClear();
        }

        if (nextProps.administration.getUserDataMapping.isLoading || nextProps.seasonPlanning.getArsGenericFilters.isLoading || nextProps.administration.updateUserDataMapping.isLoading) {
            this.setState({
                loading: true
            });
        }
    }

    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                firstName: "",
                middleName: "",
                lastName: "",
                partnerEnterpriseName: "",
                userName: "",
                email: "",
                status: "",
                userModalAnimation: false
            })
            let data = {
                type: 3,
                no: 1,
                // firstName: "",
                // middleName: "",
                // lastName: "",
                // partnerEnterpriseName: "",
                // userName: "",
                // email: "",
                // status: "",
                search: this.state.search,
            }
            this.props.userRequest(data)
        }
    }

    updateFilter(data) {
        this.setState({
            no: data.no,
            type: data.type,
            search: data.search,
            firstName: data.firstName,
            middleName: data.middleName,
            lastName: data.lastName,
            partnerEnterpriseName: data.partnerEnterpriseName,
            userName: data.userName,
            email: data.email,
            status: data.status
        })
    }
    onModal(e) {
        e.preventDefault();
        this.setState({
            userModal: !this.state.userModal,
            summaryModalAnimation: !this.state.summaryModalAnimation

        })
    }
    onFilter(e) {
        e.preventDefault();
        this.setState({
            userFilter: true,
            filterBar: !this.state.filterBar
        })
    }
    onUserEditModal =(data)=> {
        this.setState({ editModalOnRoleSuccess : !this.state.editModalOnRoleSuccess}, ()=>{
            if(!this.state.userEditModal){
                this.props.allRoleRequest();
                this.setState({ 
                    userId: data.id,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    mobile: data.mobileNumber,          
                })
            }else{
                this.setState({
                    userEditModal: !this.state.userEditModal,
                    userModalAnimation: !this.state.userModalAnimation
                })
            }
        })   
    }

    deleteUser(id) {
        // const idd = id;
        let data = {
            idd: id,
            no: this.state.current,
            type: this.state.type,
            search: this.state.search,
            // firstName: this.state.firstName,
            // middleName: this.state.middleName,
            // lastName: this.state.lastName,
            // partnerEnterpriseName: this.state.partnerEnterpriseName,
            // userName: this.state.userName,
            // email: this.state.email,
            // status: this.state.status
        }
        this.props.deleteUserRequest(data);
    }
    page =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.administration.user.data.prePage,
                    current: this.props.administration.user.data.currPage,
                    next: this.props.administration.user.data.currPage + 1,
                    maxPage: this.props.administration.user.data.maxPage,
                })
                if (this.props.administration.user.data.currPage != 0) {
                    let data = {
                        no: this.props.administration.user.data.currPage - 1,
                        type: this.state.type,
                        search: this.state.search,
                        firstName: this.state.firstName,
                        middleName: this.state.middleName,
                        lastName: this.state.lastName,
                        partnerEnterpriseName: this.state.partnerEnterpriseName,
                        userName: this.state.userName,
                        email: this.state.email,
                        status: this.state.status
                    }
                    this.props.userRequest(data)
                }

            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.user.data.prePage,
                current: this.props.administration.user.data.currPage,
                next: this.props.administration.user.data.currPage + 1,
                maxPage: this.props.administration.user.data.maxPage,
            })
            if (this.props.administration.user.data.currPage != this.props.administration.user.data.maxPage) {
                let data = {
                    no: this.props.administration.user.data.currPage + 1,
                    type: this.state.type,
                    search: this.state.search,
                    firstName: this.state.firstName,
                    middleName: this.state.middleName,
                    lastName: this.state.lastName,
                    partnerEnterpriseName: this.state.partnerEnterpriseName,
                    userName: this.state.userName,
                    email: this.state.email,
                    status: this.state.status
                }
                this.props.userRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.administration.user.data.prePage,
                    current: this.props.administration.user.data.currPage,
                    next: this.props.administration.user.data.currPage + 1,
                    maxPage: this.props.administration.user.data.maxPage,
                })
                if (this.props.administration.user.data.currPage <= this.props.administration.user.data.maxPage) {
                    let data = {
                        no: 1,
                        type: this.state.type,
                        search: this.state.search,
                        firstName: this.state.firstName,
                        middleName: this.state.middleName,
                        lastName: this.state.lastName,
                        partnerEnterpriseName: this.state.partnerEnterpriseName,
                        userName: this.state.userName,
                        email: this.state.email,
                        status: this.state.status
                    }
                    this.props.userRequest(data)
                }
            }
        }
    }

    onActive = (id) => {
        let user = this.state.userState;
        for (let i = 0; i < user.length; i++) {
            if (user[i].id == id) {
                user[i].status = user[i].status == "Active" ? "Inactive" : "Active";
                let data = {
                    userName: user[i].userName,
                    active: user[i].status == "Active" ? "true" : "false",
                    userId: user[i].id,
                    uType: 'ENT'
                }
                this.props.userStatusRequest(data);
                //this.props.editUserRequest(data); 
            }
        }
    }

    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.userRequest(data);
    }

    onClearFilter(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: "",
            firstName: "",
            middleName: "",
            lastName: "",
            partnerEnterpriseName: "",
            userName: "",
            email: "",
            status: "",
        })
        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.userRequest(data);
    }

    onRefresh() {
        let data = {
            no: this.state.current,
            type: this.state.type,
            search: this.state.search,
            firstName: this.state.firstName,
            middleName: this.state.middleName,
            lastName: this.state.lastName,
            partnerEnterpriseName: this.state.partnerEnterpriseName,
            userName: this.state.userName,
            email: this.state.email,
            status: this.state.status
        }
        this.props.userRequest(data)
    }
    handleCreateUser() {
        // if (this.props.user == "vendor") {
        //     this.props.handleAddUserModal()
        // } else {

        //     this.props.history.push('/administration/users/addUser')
        // }
        this.props.history.push('/administration/users/addUser')
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let data = {
                        no: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        firstName: this.state.firstName,
                        middleName: this.state.middleName,
                        lastName: this.state.lastName,
                        partnerEnterpriseName: this.state.partnerEnterpriseName,
                        userName: this.state.userName,
                        email: this.state.email,
                        status: this.state.status
                    }
                    this.props.userRequest(data)
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    sendEmail =(e)=>{
        this.setState({
            userEditModal: false,
            editModalOnRoleSuccess: false
        })
        let data = {
            first: this.state.firstName,
            last: this.state.lastName,
            email: this.state.email,
            mobile: this.state.mobile,
            uType: 'ENT',
            additionalUrl: "/send/email"
        }
        this.props.addUserRequest(data); 
    }
    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
          this.setState({ userEditModal: false })
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className={this.props.user == "vendor" ? "col-lg-12 pad-0" : "col-lg-12 pad-0"} >
                    <div className={this.props.user == "vendor" ? "gen-vendor-potal-design p-lr-47" : "gen-vendor-potal-design p-lr-47"}>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <form onSubmit={(e) => this.onSearch(e)}>
                                        <input type="search" onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                        <img className="search-image" src={SearchImage} onClick={(e) => this.onSearch(e)} />
                                        {this.state.type != 3 ? null : <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></span>}
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="tooltip" onClick={() => this.handleCreateUser()}>
                                    <span className="add-btn"><img src={require('../../../assets/add-green.svg')} />
                                        <span className="generic-tooltip">Add User</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="col-md-12 pad-0 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            <table className="table gen-main-table">
                                {/* UserManageTable */}
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn user-fix-action-btn">
                                            <ul className="rab-refresh">
                                                {/* <li className="rab-rinner">
                                                    <label className="width65">Usage Summary</label>
                                                </li> */}
                                                <li className="rab-rinner">
                                                    <span><img onClick={() => this.onRefresh()} src={refreshIcon} /></span>
                                                </li>
                                            </ul>
                                            {/* <label className="width65">Usage Summary</label>
                                            <label className="width65 pad-lft-5">Action</label> */}
                                        </th>

                                        {/* <th className="fixed-side-user"> 
                                        <label className="width65">ACTION</label>
                                    </th> */}
                                        {/* <th className="positionRelative">
                                            <label>Enterprise</label>
                                        </th> */}
                                        <th className="positionRelative">
                                            <label>Name</label><img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th className="positionRelative">
                                            <label>User Name</label><img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th className="positionRelative">
                                            <label>Email</label><img src={require('../../../assets/headerFilter.svg')} />
                                        </th>

                                        <th className="positionRelative">
                                            <label> Mobile</label><img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th className="positionRelative">
                                            <label> Access Mode</label><img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th className="positionRelative">
                                            <label>Status</label><img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        {/* <th>
                                        <label>USAGE SUMMARY</label>
                                    </th>

                                    <th>
                                        <label>ACTION</label>
                                    </th> */}

                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.userState == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.userState.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.userState.map((data, key) => (
                                        <tr key={key}>
                                            <td className="fix-action-btn user-fix-action-btn">
                                                <ul className="table-item-list">
                                                    {/* <li className="til-inner">
                                                        <button onClick={(e) => this.onModal(e)} className="btnSummary">
                                                            View Summary
                                                    </button>
                                                    </li> */}
                                                    <li className="til-inner til-edit-btn" onClick={() => this.onUserEditModal(data)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Edit</span>
                                                    </li>
                                                    <li className="til-inner" onClick={(e) => this.openUserDataMapping(e, data.id)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17.72" height="19.67" viewBox="0 0 15.72 19.67">
                                                            <g id="data_mapping" data-name="data mapping" transform="translate(-44.7 -9.791)">
                                                                <g id="data" transform="translate(46.375 9.791)">
                                                                <path id="Path_1535" data-name="Path 1535" d="M10.6,0C7.508,0,4.38.913,4.38,2.612V13.828c0,1.7,3.128,2.612,6.22,2.612s6.22-.913,6.22-2.612V2.612C16.819.913,13.691,0,10.6,0Zm0,11.22c-3.2,0-5.206-.95-5.206-1.6v-2a6.046,6.046,0,0,0,.973.457c.306.105.653.205,1.023.3a14.814,14.814,0,0,0,3.2.347,14.814,14.814,0,0,0,3.2-.347c.37-.091.717-.192,1.027-.3a5.48,5.48,0,0,0,.968-.457v2C15.806,10.27,13.8,11.22,10.6,11.22ZM13.7,7.361a10.845,10.845,0,0,1-1.763.288,14.7,14.7,0,0,1-2.69,0,10.38,10.38,0,0,1-1.758-.292c-1.443-.361-2.1-.913-2.1-1.238V4.11a4.74,4.74,0,0,0,.977.457,8.777,8.777,0,0,0,1.018.306,15.677,15.677,0,0,0,6.421,0,8.777,8.777,0,0,0,1.018-.306,4.74,4.74,0,0,0,.977-.457V6.119c0,.342-.639.877-2.1,1.242Zm0-3.512a10.115,10.115,0,0,1-1.767.292,14.453,14.453,0,0,1-2.667,0A10.243,10.243,0,0,1,7.5,3.85c-1.466-.361-2.105-.9-2.105-1.238,0-.653,2.009-1.6,5.206-1.6s5.206.945,5.206,1.6c0,.342-.639.877-2.105,1.238ZM5.394,11.12a7.06,7.06,0,0,0,2.283.827,15.261,15.261,0,0,0,5.863,0,7.06,7.06,0,0,0,2.283-.827v2.708c0,.653-2.028,1.6-5.206,1.6s-5.224-.945-5.224-1.6Z" transform="translate(-4.38 0)" fill="#a4b9dd"/>
                                                                </g>
                                                                <path id="mapping" d="M7.93,23.93V21.965h2.948V20h1.965v1.965H15.79V23.93Zm8.843-1.965V23.93H19.72V21.965Zm-9.825,0H4V23.93H6.948Z" transform="translate(40.7 5.531)" fill="#a4b9dd"/>
                                                            </g>
                                                        </svg>
                                                    </li>
                                                    <li className={data.status == "Active" ? "addToggleSwitch mainToggle" : "mainToggle"}>
                                                        {/* <button type="button" onClick={(e) => this.onActive(`${data.userId}`)} className={ data.status == "Active" ? "btnSummary" : "active_button"}>
                                                        {data.status}
                                                    </button> */}
                                                        <label className="switchToggle">
                                                            <input onChange={(e) => this.onActive(`${data.id}`)} type="checkbox" id="myDiv" />
                                                                <span className="sliderToggle round">
                                                            </span>
                                                        </label>
                                                        {/* <p className="onActive">Active</p>
                                                        <p className="onInActive">Inactive</p> */}
                                                    </li>
                                                </ul>

                                                {/* <ul className="list-inline m-0 float_None alignMiddle">
                                                    <li >
                                                        <button type="button" onClick={(e) => this.onActive(`${data.userId}`)} className={ data.status == "Active" ? "btnSummary" : "active_button"}>
                                                        {data.status}
                                                    </button>
                                                        <label className="switchToggle">
                                                            <input onClick={(e) => this.onActive(`${data.userId}`)} type="checkbox" id="myDiv" />
                                                            <span className="sliderToggle round">
                                                            </span>
                                                        </label>
                                                        <p className="onActive">Active</p>
                                                        <p className="onInActive">Inactive</p>
                                                    </li>
                                                </ul> */}
                                            </td>
                                            {/* <td>
                                                <label>
                                                    {data.partnerEnterpriseName}
                                                </label>
                                            </td> */}
                                            <td>
                                                <label>
                                                    {data.firstName} {data.lastName}
                                                </label>
                                            </td>
                                            <td>
                                                <label>{data.userName}</label>
                                            </td>
                                            <td>
                                                <label className="maxwidth200">{data.email}</label>
                                            </td>
                                            <td>
                                                <label>{data.mobileNumber}</label>
                                            </td>
                                            <td>
                                                <label>{data.accessMode}</label>
                                            </td>
                                            <td>
                                                <label>{data.status}</label>
                                            </td>
                                        </tr>))}
                                </tbody>
                            </table>
                        </div>

                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.userDataMapping && <UserDataMapping userDataMappingData={this.state.userDataMappingData} closeUserDataMapping={this.closeUserDataMapping} {...this.props} />}
                {this.state.userFilter ? <UserFilter
                    firstName={this.state.firstName}
                    middleName={this.state.middleName}
                    lastName={this.state.lastName}
                    partnerEnterpriseName={this.state.partnerEnterpriseName}
                    userName={this.state.userName}
                    email={this.state.email}
                    status={this.state.status}
                    updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} filter={(e) => this.onFilter(e)} /> : null}
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {/* {this.state.userModal ? <UserModal summaryModalAnimation={this.state.summaryModalAnimation} modal={(e) => this.onModal(e)} /> : null} */}
                {this.state.userEditModal ? <UserEditModal {...this.state} {...this.props} userId={this.state.userId} userModalAnimation={this.state.userModalAnimation} userEditModal={(e) => this.onUserEditModal(e)} roleData={this.state.roleData} sendEmail={this.sendEmail}/> : null}
            </div>



        )
    }
}

export default UserManage;