import React from "react";
import openRack from "../../../assets/open-rack.svg";
import warning2 from "../../../assets/warning-2.svg";
import BraedCrumps from "../../breadCrumps";
class EmptyBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="container_div" id="">
        {/* <div className="col-md-12 col-sm-12">
          <div className="menu_path">
            <ul className="list-inline width_100"> */}
              {/* <li>
                <label className="home_link">Home > User > Vendors</label>
              </li> */}
              {/* <BraedCrumps />
              <li
                className="float_right"
                onClick={() => this.props.clickRightSideBar()}
              >
                <img src={openRack} className="right_sidemenu" />
              </li> */}
            {/* </ul>
          </div>
        </div> */}

        <div className="col-md-12 col-sm-12 col-xs-12">
          <div className="container_content height-70vh">
            {/* <div className="col-md-12 col-sm-12 pad-0">
              <ul className="list_style">
                <li>
                  <label className="contribution_mart">
                    MASTER V-MART <span>(MRP-CONTRIBUTION)</span>
                  </label>
                </li>
                <li>
                  <p className="master_para">Add Master data</p>
                </li>
              </ul>
            </div> */}
            <div className="col-md-8 col-sm-12 col-xs-12 col-md-offset-3 pad-0">
              <div className="records_div">
                <ul className="record_list m-top-50">
                  <li>
                    <img src={warning2} />
                  </li>
                  <li>
                    <p className="record_notification">
                      You have No Records added in your List !
                    </p>
                  </li>
                  <li>
                    <p className="record_para">
                      Create one by clicking below button
                    </p>
                  </li>
                  <li>
                    <button
                      onClick={() => this.props.addUser()}
                      className="record_btn"
                    >
                      ADD NOW
                    </button>
                  </li>
                </ul>
                <div className="col-md-12 col-sm-12 m-top-10">
                  <div className="dividerDiv">
                    <svg xmlns="http://www.w3.org/2000/svg" width="500" height="10" viewBox="0 0 520 10">
                        <g fill="none" fillRule="evenodd">
                            <path fillRule="nonzero" stroke="#E6E6F5" strokeLinecap="square" d="M.5 5.5H233M286.5 6.25H519"/>
                            <path fill="#4A4A4A" d="M257.052 9.12c-.856 0-1.628-.186-2.316-.558a4.132 4.132 0 0 1-1.62-1.548 4.253 4.253 0 0 1-.588-2.214c0-.816.196-1.554.588-2.214a4.132 4.132 0 0 1 1.62-1.548c.688-.372 1.46-.558 2.316-.558.856 0 1.628.186 2.316.558a4.146 4.146 0 0 1 1.62 1.542c.392.656.588 1.396.588 2.22 0 .824-.196 1.564-.588 2.22a4.146 4.146 0 0 1-1.62 1.542c-.688.372-1.46.558-2.316.558zm0-1.368a3.03 3.03 0 0 0 1.512-.378c.448-.252.8-.604 1.056-1.056.256-.452.384-.958.384-1.518s-.128-1.066-.384-1.518a2.734 2.734 0 0 0-1.056-1.056 3.03 3.03 0 0 0-1.512-.378 3.03 3.03 0 0 0-1.512.378c-.448.252-.8.604-1.056 1.056A3.026 3.026 0 0 0 254.1 4.8c0 .56.128 1.066.384 1.518.256.452.608.804 1.056 1.056a3.03 3.03 0 0 0 1.512.378zM268.728 9l-1.716-2.46c-.072.008-.18.012-.324.012h-1.896V9h-1.56V.6h3.456c.728 0 1.362.12 1.902.36.54.24.954.584 1.242 1.032.288.448.432.98.432 1.596 0 .632-.154 1.176-.462 1.632-.308.456-.75.796-1.326 1.02L270.408 9h-1.68zm-.036-5.412c0-.536-.176-.948-.528-1.236-.352-.288-.868-.432-1.548-.432h-1.824v3.348h1.824c.68 0 1.196-.146 1.548-.438.352-.292.528-.706.528-1.242z"/>
                        </g>
                    </svg>
                  </div>
                </div>
                <ul className="list-inline record_list1">
                    <li>
                      <button className="excelBtnEmptyBox">
                        <label className="excelLabel">
                         <svg className="excelImg" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                              <g fill="none" fillRule="nonzero">
                                  <circle cx="23.52" cy="23.52" r="23.52" fill="#E6E6F5"/>
                                  <path fill="#6D6DC9" d="M24.255 13.23c-1.944 0-3.9.76-5.382 2.277-1.102 1.129-1.77 2.529-2.054 3.99-2.877.57-5.059 3.143-5.059 6.248 0 3.523 2.807 6.396 6.248 6.396h2.987a.811.811 0 0 0 .716-.413.852.852 0 0 0 0-.842.811.811 0 0 0-.716-.413h-2.987c-2.56 0-4.618-2.107-4.618-4.728 0-2.475 1.838-4.492 4.193-4.71a.824.824 0 0 0 .73-.73 6.167 6.167 0 0 1 1.715-3.625 5.87 5.87 0 0 1 4.227-1.781c1.532 0 3.048.583 4.219 1.781a6.211 6.211 0 0 1 1.714 5.067.848.848 0 0 0 .197.655c.154.18.376.283.61.284h.322c2.121 0 3.803 1.722 3.803 3.893 0 2.172-1.682 3.894-3.803 3.894h-3.802a.811.811 0 0 0-.716.413.852.852 0 0 0 0 .842c.148.26.422.418.716.413h3.802c2.996 0 5.433-2.495 5.433-5.562 0-2.894-2.178-5.25-4.94-5.51.012-2.013-.686-4.03-2.182-5.562a7.49 7.49 0 0 0-5.373-2.277zm0 8.9a.721.721 0 0 0-.552.217l-2.988 2.78a.856.856 0 0 0-.05 1.183c.287.322.838.345 1.154.052l1.621-1.512v8.126c0 .46.365.834.815.834.45 0 .815-.374.815-.834V24.85l1.621 1.512c.316.293.85.253 1.155-.052.32-.322.262-.886-.051-1.182l-2.988-2.781c-.162-.152-.28-.218-.552-.218z"/>
                              </g>
                          </svg>
                          Upload Excel
                        
                          <span>
                            max upload size 5mb
                          </span>
                          </label>
                      <input type="file" className="excelFile"/>
                        
                         

                      </button>
                    </li>
                    <li>
                      <label className="formateExcel">
                        Supported Format XLS and CSV
                      </label>
                      </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EmptyBox;
