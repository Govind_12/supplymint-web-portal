import React from 'react';

class UserModal extends React.Component {
    render() {

        return (
            <div className={this.props.summaryModalAnimation ? "modal  display_block" : "display_none"} id="modalSummary">
            <div className={this.props.summaryModalAnimation ? "backdrop display_block" : "display_none"}></div>

            <div className={this.props.summaryModalAnimation ? " display_block" : "display_none"}>
                <div className={this.props.summaryModalAnimation ? "modal-content summaryContent modalShow" : "modalHide"}>


                    <div className="col-md-12 col-sm-12 pad-0">
                        <ul className="list-inline summaryList">
                            <li>
                                <label className="summaryHeading">
                                    USAGE SUMMARY
                            </label>
                            </li>
                            <li>
                                <button className="summaryBtn">
                                    View Recent Activity
                            </button>
                                <button className="summaryBtn m-lft-10">
                                    Last 10 days Activity
                            </button>
                            </li>
                        </ul>
                    </div>
                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                        <div className="contentContainerSummary">
                            <div className="inlineListContent">
                                <ul className="list-inline summaryListInline">
                                    <li>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16" viewBox="0 0 15 16">
                                            <path fill="#6D6DC9" fillRule="nonzero" d="M14.861 10.833c0 .997-.81 1.806-1.808 1.806H1.947a1.807 1.807 0 0 1-.141-3.606v-3.2a5.694 5.694 0 1 1 11.389 0v3.2c.93.071 1.666.85 1.666 1.8zm-1.806-.416c-.69 0-1.25-.559-1.25-1.25V5.832a4.305 4.305 0 1 0-8.61 0v3.333c0 .69-.56 1.25-1.25 1.25a.418.418 0 0 0-.417.417c0 .23.187.417.42.417h11.105c.231 0 .42-.188.42-.417a.418.418 0 0 0-.418-.416zm-8.194 2.916H6.25a1.25 1.25 0 1 0 2.5 0h1.389a2.639 2.639 0 0 1-5.278 0z"/>
                                        </svg>

                                    </li>
                                    <li>
                                        <label className="summaryModalContent">
                                            New Sales Report Generated
                                    </label>
                                        <ul className="list-inline width_100">
                                            <li>
                                                <span className="timingNotification">
                                                    23 July 2018
                                            </span>
                                            </li>
                                            <li>
                                                <span className="timingNotification">
                                                    9:30 pm
                                            </span>
                                            </li>
                                        </ul>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div className="footerSummary">
                            <button onClick={(e) => this.props.modal(e)} className="footerBtnSummary">
                                OK
                        </button>
                        </div>
                    </div>
                </div>

            </div>
            </div>
        )
    }
}

export default UserModal;