import React from 'react';
import ToastLoader from '../../loaders/toastLoader';
class ProxyStoreModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            proxyStoreValue: [],
            searchMrp: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
            proxyStore: this.props.proxyStore
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.administration.proxyStore.isSuccess) {
            if (nextProps.administration.proxyStore.data.resource != null) {
                this.setState({
                    proxyStoreValue: nextProps.administration.proxyStore.data.resource,
                    prev: nextProps.administration.proxyStore.data.prePage,
                    current: nextProps.administration.proxyStore.data.currPage,
                    next: nextProps.administration.proxyStore.data.currPage + 1,
                    maxPage: nextProps.administration.proxyStore.data.maxPage,
                })
            } else {
                this.setState({
                    proxyStoreValue: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.administration.proxyStore.data.prePage,
                current: this.props.administration.proxyStore.data.currPage,
                next: this.props.administration.proxyStore.data.currPage + 1,
                maxPage: this.props.administration.proxyStore.data.maxPage,
            })
            if (this.props.administration.proxyStore.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.administration.proxyStore.data.currPage - 1,

                    search: this.state.search
                }
                this.props.proxyStoreRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.proxyStore.data.prePage,
                current: this.props.administration.proxyStore.data.currPage,
                next: this.props.administration.proxyStore.data.currPage + 1,
                maxPage: this.props.administration.proxyStore.data.maxPage,
            })
            if (this.props.administration.proxyStore.data.currPage != this.props.administration.proxyStore.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.administration.proxyStore.data.currPage + 1,
                    search: this.state.search
                }
                this.props.proxyStoreRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.administration.proxyStore.data.prePage,
                current: this.props.administration.proxyStore.data.currPage,
                next: this.props.administration.proxyStore.data.currPage + 1,
                maxPage: this.props.administration.proxyStore.data.maxPage,
            })
            if (this.props.administration.proxyStore.data.currPage <= this.props.administration.proxyStore.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.search
                }
                this.props.proxyStoreRequest(data)
            }
        }
    }

    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                type: "",
                no: 1
            })
        if (this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                search: "",
            }
            this.props.proxyStoreRequest(data)
        }
    }

    handleChange(e) {
        if (e.target.id == "searchMrp") {
            this.setState({
                searchMrp: e.target.value
            })
        }
    }

    selectedData(e) {
        let c = this.state.proxyStoreValue;
        for (let i = 0; i < c.length; i++) {
            if (c[i].id == e) {
                this.setState({
                    proxyStore: c[i].id
                })
            }
        }
        this.setState({
            proxyStoreValue: c
        })
    }

    onDone() {
        let proxyStoreValue = this.state.proxyStoreValue
        let data = {}
        for (var i = 0; i < proxyStoreValue.length; i++) {
            
            if (this.state.proxyStore != "") {
                if (proxyStoreValue[i].id == this.state.proxyStore) {
                    data = {
                        proxyStore: proxyStoreValue[i].name,
                    }
                    
                }
            }
        }
        setTimeout(() => {
            this.props.updateProxyStore(data)
        }, 10);
        for (var i = 0; i < proxyStoreValue.length; i++) {
            if (this.state.proxyStore != "") {
                if (proxyStoreValue[i].id == this.state.proxyStore) {
                    this.setState(
                        {
                            searchMrp: "",
                        })

                } this.props.closeProxyModal()
            } else {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1500)
            }
        }
    }
    onSearch(e) {
        this.setState({
            type: 3,
        })
        let data = {
            type: 3,
            no: 1,
            search: this.state.searchMrp,
        }
        this.props.proxyStoreRequest(data)
    }

    render() {
        
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal proxyModal">
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Size">
                                <div className="modal-top">
                                    <ul className="list-inline width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT PROXY STORE</label>
                                        </li>
                                        <li className="float_right">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onClick={() => this.onDone()}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" onClick={(e) => this.props.closeProxyModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul className="list-inline width_100 m-top-10">
                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    <input type="search" className="search-box" onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={this.state.searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className={this.state.searchMrp == "" ? "btnDisabled findButton" : "findButton"} onClick={(e) => this.state.searchMrp == "" ? null : this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                    <label>
                                                        <button type="button" className={this.state.type != 3 ? "btnDisabled clearbutton m-lft-15" : "clearbutton m-lft-15"} onClick={(e) => this.state.type != 3 ? null : this.onsearchClear(e)}>CLEAR</button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>

                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover selectVendorMain">   
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {this.state.proxyStoreValue.map((data, key) => (
                                                    <tr key={key}>
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={data.id} type="radio" name="transporter" checked={`${data.id}` == this.state.proxyStore} onChange={() => this.selectedData(`${data.id}`)} />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>{data.code}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.name}</label>
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div className="pagerDiv pagerWidth75 m0 modalPagination sizeModalPagination">
                                    <ul className="list-inline pagination paginationWidth40">
                                        {this.state.current == 1 || this.state.current == 0 ? <li >
                                            <button className="PageFirstBtn pointerNone" type="button">
                                                First
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn " type="button" onClick={(e) => this.page(e)} id="first" >
                                                    First
                  </button>
                                            </li>}
                                        {this.state.prev != 0 ? <li >
                                            <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                Prev
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn" disabled>
                                                    Prev
                  </button>
                                            </li>}
                                        <li>
                                            <button className="PageFirstBtn pointerNone" type="button">
                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                            </button>
                                        </li>
                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                            <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                Next
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn borderNone" type="button" disabled>
                                                    Next
                  </button>
                                            </li> : <li >
                                                <button className="PageFirstBtn borderNone" type="button" disabled>
                                                    Next
                  </button>
                                            </li>}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default ProxyStoreModal;