import React from "react";
import moment from 'moment';
import RightSideBar from "../../rightSideBar";
import Footer from '../../footer'
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";
import Json from "../jsonFile.json";
import ProxyStoreModal from "./proxyStoreModal";
import NewSideBar from "../../newSidebar";
//import { openSync } from "fs";
class SiteCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            successMessage: "",
            isShipping: false,
            errorCode: "",
            isBilling: false,
            isOnlineStore: false,
            name: "",
            nameerr: false,
            code: "",
            codeerr: false,
            country: "",
            countryerr: false,
            state: "",
            stateerr: false,
            city: "",
            cityerr: false,
            zipcode: "",
            zipcodeerr: false,
            gstin: "",
            gstinerr: false,
            timezone: "",
            timezoneerr: false,
            displayName: "",
            displayNameerr: false,
            partner: "", //until group, made similar to latitude, no errors handled
            zone: "",
            region: "",
            group: "",
            latitude: "",
            latittudeerr: false,
            longitude: "",
            longitudeeer: false,
            status: "Active",
            statuserr: false,
            type: "",
            typeerr: false,
            additional: "",
            area: "",
            areaerr: false,
            unit: "",
            uniterr: false,
            numberOfFloors: "",
            numberOfFloorserr: false,
            udf1: "",
            udf2: "",
            udf3: "",
            udf4: "",
            udf5: "",
            opStartDate: "",
            opStartDateerr: false,
            opEndDate: "",
            opEndDateerr: false,
            isCheckbox: "",
            isCheckboxerr: false,
            contactName: "",
            contactNameerr: false,
            contactNumber: "",
            contactNumbererr: false,
            email: "",
            emailerr: false,
            mobile: "",
            mobileerr: false,
            Web: "",
            Websiteerr: false,
            Address: "",
            Addresserr: false,
            billingName: "",
            billingNameerr: false,
            billingPhone: "",
            billingPhoneerr: false,
            billingEmail: "",
            billingEmailerr: false,
            billingMobile: "",
            billingMobilrerr: false,
            shippingName: "",
            shippingerr: false,
            shippingPhone: "",
            shippingPhoneerr: false,
            shippingEmail: "",
            shippingEmailerr: false,
            shippingMobile: "",
            shippingMobilrerr: false,
            receiverName: "",
            receiverNameerr: false,
            receiverPhone: "",
            receiverPhoneerr: false,
            receiverEmail: "",
            receiverEmailerr: false,
            receiverMobile: "",
            receiverMobilrerr: false,
            loader: false,
            success: false,
            alert: false,
            rightbar: false,
            openRightBar: false,
            code: "",
            cityStateData: Json,
            stateData: [],
            cityData: [],
            startDate: "",
            startDaterr: false,
            endDate: "",
            endDaterr: false,
            proxyStore: "",
            proxyStorerr: false,
            minDate: "",
            maxDate: "",
            proxyStoreModal: false,
            isProxyStore: false,
            isProxyStoreerr: false
        }
    }
    componentWillMount() {
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }

        // let crud = JSON.parse(sessionStorage.getItem('Site'))
        let Create = 1;
        if (Create <= 0) {
            this.props.history.push('/administration/site');
        }
        var cityData = this.state.cityStateData[0].states;

        this.setState({
            stateData: Object.keys(cityData),
        })
    }
    componentWillReceiveProps(nextProps) {

        if (!nextProps.administration.addSite.isLoading && !nextProps.administration.addSite.isSuccess && !nextProps.administration.addSite.isError) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.administration.addSite.isLoading || nextProps.administration.proxyStore.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.addSite.isError) {
            this.setState({
                errorMessage: nextProps.administration.addSite.message.error == undefined ? undefined : nextProps.administration.addSite.message.error.errorMessage,
                loader: false,
                code: nextProps.administration.addSite.message.status,
                errorCode: nextProps.administration.addSite.message.error == undefined ? undefined : nextProps.administration.addSite.message.error.errorCode,
                error: true
            })
            this.props.addSiteRequest();
        } else if (nextProps.administration.addSite.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.addSite.data.message,
                loader: false,
                success: true
            })
            this.props.addSiteRequest();
            this.onClear();
        }
        if (nextProps.administration.proxyStore.isError) {
            this.setState({
                errorMessage: nextProps.administration.proxyStore.message.error == undefined ? undefined : nextProps.administration.proxyStore.message.error.errorMessage,
                loader: false,
                code: nextProps.administration.proxyStore.message.status,
                errorCode: nextProps.administration.proxyStore.message.error == undefined ? undefined : nextProps.administration.proxyStore.message.error.errorCode,
                error: true
            })
            this.props.proxyStoreClear();
        } else if (nextProps.administration.proxyStore.isSuccess) {
            this.setState({
                loader: false,
            })
            this.props.proxyStoreClear();
        }
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onClear() {
        // e.preventDefault();
        document.getElementById('timezone').placeholder = "Timezone";
        document.getElementById('opStartDate').placeholder = "Operation Start Date";
        this.setState({
            name: "",
            code: "",
            country: "",
            state: "",
            city: "",
            zipcode: "",
            gstin: "",
            timezone: "",
            displayName: "",
            partner: "",
            zone: "",
            region: "",
            group: "",
            latitude: "",
            longitude: "",
            status: "",
            type: "",
            additional: "",
            area: "",
            unit: "",
            numberOfFloors: "",
            udf1: "",
            udf2: "",
            udf3: "",
            udf4: "",
            udf5: "",
            opStartDate: "",
            opEndDate: "",
            isCheckbox: "",
            contactName: "",
            contactNumber: "",
            email: "",
            mobile: "",
            Web: "",
            Address: "",
            billingName: "",
            billingPhone: "",
            billingEmail: "",
            billingMobile: "",
            isShipping: false,
            isOnlineStore: false,
            shippingName: "",
            shippingPhone: "",
            shippingEmail: "",
            shippingMobile: "",
            receiverName: "",
            receiverPhone: "",
            receiverEmail: "",
            receiverMobile: "",
        })
    }

    //name
    name() {
        if (
            this.state.name == ""
        ) {
            this.setState({
                nameerr: true
            });
        } else {
            this.setState({
                nameerr: false
            });
        }
    }

    //code
    code() {
        if (
            this.state.code == "") {
            this.setState({
                codeerr: true
            });
        } else {
            this.setState({
                codeerr: false
            });
        }
    }
    //country
    country() {
        if (
            this.state.country == "") {
            this.setState({
                countryerr: true
            });
        } else {
            this.setState({
                countryerr: false
            });
        }
    }
    //state
    statee() {
        if (
            this.state.state == "") {
            this.setState({
                stateerr: true
            });
        } else {
            this.setState({
                stateerr: false
            });
        }

    }

    //city

    city() {
        if (
            this.state.city == "") {
            this.setState({
                cityerr: true
            });
        } else {
            this.setState({
                cityerr: false
            });
        }


    }
    //Zipcode

    zipcode() {

        if (
            this.state.zipcode == "" ||
            !this.state.zipcode.match(/(^\d{6}$)|(^\d{6}-\d{4}$)/) ||
            !this.state.zipcode.match(/^.{6}$/)
        ) {
            this.setState({
                zipcodeerr: true
            });
        } else {
            this.setState({
                zipcodeerr: false
            });
        }
    }

    //GSTIN
    gstin() {
        if (
            this.state.gstin == "") {
            this.setState({
                gstinerr: true
            });
        } else {
            this.setState({
                gstinerr: false
            });
        }


    }

    //timezone

    timezone() {
        if (
            this.state.timezone == "") {
            this.setState({
                timezoneerr: true
            });
        } else {
            this.setState({
                timezoneerr: false
            });
        }

    }
    //displayName
    displayName() {
        if (this.state.displayName == "" || !this.state.displayName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                displayNameerr: true
            });
        } else {

            this.setState({
                displayNameerr: false
            });
        }
    }
    //latitude
    latitude() {
        if (this.state.latitude == "") {
            this.setState({
                latittudeerr: true
            });
        } else {

            this.setState({
                latittudeerr: false
            });
        }

    }

    //longitude
    longitude() {

        if (this.state.longitude == "") {
            this.setState({
                longitudeeer: true
            });
        } else {

            this.setState({
                longitudeeer: false
            });
        }
    }

    //status
    status() {
        if (this.state.status == "") {
            this.setState({
                statuserr: true
            });
        } else {

            this.setState({
                statuserr: false
            });
        }
    }
    //type
    type() {
        if (this.state.type == "") {
            this.setState({
                typeerr: true
            });
        } else {

            this.setState({
                typeerr: false
            });
        }


    }
    //area
    area() {
        if (this.state.area == "" ||
            !this.state.area.match(/^[1-9]\d+$/)) {
            this.setState({
                areaerr: true
            });

        } else {
            this.setState({
                areaerr: false
            });
        }
    }
    //unit
    unit() {
        if (this.state.unit == "") {
            this.setState({
                uniterr: true
            });

        } else {
            this.setState({
                uniterr: false
            });
        }
    }
    //numberOfFloors
    numberOfFloors() {
        if (this.state.numberOfFloors == "" || this.state.numberOfFloors <= 0) {
            this.setState({
                numberOfFloorserr: true
            });

        } else {
            this.setState({
                numberOfFloorserr: false
            });
        }
    }
    //opStartDate
    opStartDate() {
        if (this.state.opStartDate == "") {
            this.setState({
                opStartDateerr: true
            });

        } else {
            this.setState({
                opStartDateerr: false
            });
        }
    }
    //opEndDate

    opEndDate() {
        if (this.state.opEndDate == "") {
            this.setState({
                opEndDateerr: true
            });

        } else {
            this.setState({
                opEndDateerr: false
            });
        }
    }

    //checkbox
    isCheckbox() {
        if (this.state.isCheckbox == "") {
            this.setState({
                isCheckboxerr: true
            });
        } else {
            this.setState({
                isCheckboxerr: false
            });
        }

    }

    //contactName
    contactName() {
        if (this.state.contactName == "" || !this.state.contactName.match(/^[a-zA-Z ]+$/)) {


            this.setState({
                contactNameerr: true
            });
        } else {
            this.setState({
                contactNameerr: false
            });
        }

    }
    //contactNumber
    contactNumber() {
        if (this.state.contactNumber == "" || this.state.contactNumber.trim() == "") {
            this.setState({
                contactNumbererr: false
            })
        }
        else if (this.state.contactNumber.length < 10) {
            this.setState({
                contactNumbererr: true
            });
        }
        else {
            this.setState({
                contactNumbererr: false
            });
        }

    }
    //email
    email() {

        if (
            this.state.email == "" ||
            !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }

    //mobile
    mobile() {
        if (this.state.mobile == "" ||
            !this.state.mobile.match(/^\d{10}$/) ||
            !this.state.mobile.match(/^[1-9]\d+$/) ||
            !this.state.mobile.match(/^.{10}$/)) {
            this.setState({
                mobileerr: true
            });
        } else {
            this.setState({
                mobileerr: false
            });
        }
    }

    //Website
    Web() {
        if (
            this.state.Web == "" ||
            !this.state.Web.match(
                /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
            )
        ) {
            this.setState({
                Websiteerr: true
            });
        } else {
            this.setState({
                Websiteerr: false
            });
        }
    }
    //
    Address() {
        if (this.state.Address == "") {
            this.setState({
                Addresserr: true
            });
        } else {
            this.setState({
                Addresserr: false
            });
        }

    }
    //billingName
    billingName() {
        if (this.state.billingName == "" || !this.state.billingName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                billingNameerr: true
            });
        } else {
            this.setState({
                billingNameerr: false
            });
        }

    }
    //billingPhone
    billingPhone() {
        if (this.state.billingPhone == "" || this.state.billingPhone.trim() == "") {
            this.setState({
                billingPhoneerr: false
            })
        }
        else if (this.state.billingPhone.length < 10) {
            this.setState({
                billingPhoneerr: true
            });
        }
        else {
            this.setState({
                billingPhoneerr: false
            });
        }
    }
    //billingEmail
    billingEmail() {

        if (
            this.state.billingEmail == "" ||
            !this.state.billingEmail.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                billingEmailerr: true
            });
        } else {
            this.setState({
                billingEmailerr: false
            });
        }
    }
    //billingMobile
    billingMobile() {
        if (this.state.billingMobile == "" || !this.state.billingMobile.match(/^\d{10}$/) ||
            !this.state.billingMobile.match(/^[1-9]\d+$/) ||
            !this.state.billingMobile.match(/^.{10}$/)) {
            this.setState({
                billingMobileerr: true
            });
        } else {
            this.setState({
                billingMobileerr: false
            });
        }
    }
    //shippingName

    shippingName() {
        if (this.state.shippingName == "" || !this.state.shippingName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                shippingNameerr: true
            });
        } else {
            this.setState({
                shippingNameerr: false
            });
        }

    }
    //shippingPhone
    shippingPhone() {
        if (this.state.shippingPhone == "" || this.state.shippingPhone.trim() == "") {
            this.setState({
                shippingPhoneerr: false
            })
        }
        else if (this.state.shippingPhone.length < 10) {
            this.setState({
                shippingPhoneerr: true
            });
        }
        else {
            this.setState({
                shippingPhoneerr: false
            });
        }


    }
    //billingEmail
    shippingEmail() {

        if (
            this.state.shippingEmail == "" ||
            !this.state.shippingEmail.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                shippingEmailerr: true
            });
        } else {
            this.setState({
                shippingEmailerr: false
            });
        }
    }
    //shippingMobile
    shippingMobile() {
        if (this.state.shippingMobile == "" || !this.state.shippingMobile.match(/^\d{10}$/) ||
            !this.state.shippingMobile.match(/^[1-9]\d+$/) ||
            !this.state.shippingMobile.match(/^.{10}$/)) {
            this.setState({
                shippingMobileerr: true
            });
        } else {
            this.setState({
                shippingMobileerr: false
            });
        }
    }
    //receiverName


    receiverName() {
        if (this.state.receiverName == "" || !this.state.receiverName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                receiverNameerr: true
            });
        } else {
            this.setState({
                receiverNameerr: false
            });
        }

    }
    //receiverPhone
    receiverPhone() {
        if (this.state.receiverPhone == "" || this.state.receiverPhone.trim() == "") {
            this.setState({
                receiverPhoneerr: false
            })
        }
        else if (this.state.receiverPhone.length < 10) {
            this.setState({
                receiverPhoneerr: true
            });
        }
        else {
            this.setState({
                receiverPhoneerr: false
            });
        }

    }
    //receiverEmail
    receiverEmail() {

        if (
            this.state.receiverEmail == "" ||
            !this.state.receiverEmail.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                receiverEmailerr: true
            });
        } else {
            this.setState({
                receiverEmailerr: false
            });
        }
    }
    //receiverMobile
    receiverMobile() {
        if (this.state.receiverMobile == "" || !this.state.receiverMobile.match(/^\d{10}$/) ||
            !this.state.receiverMobile.match(/^[1-9]\d+$/) ||
            !this.state.receiverMobile.match(/^.{10}$/)) {
            this.setState({
                receiverMobileerr: true
            });
        } else {
            this.setState({
                receiverMobileerr: false
            });
        }
    }

    // __________________________NEW FIELDS VALIDATION CODE HERE_________________________

    startDateError() {
        if (this.state.startDate == "") {
            this.setState({
                startDaterr: true
            })
        } else {
            this.setState({
                startDaterr: false
            })
        }
    }

    endDateError() {
        if (this.state.endDate == "") {
            this.setState({
                endDaterr: true
            })
        } else {
            this.setState({
                endDaterr: false
            })
        }
    }

    handleChange(e) {
        if (e.target.id == "name") {
            this.setState(
                {
                    name: e.target.value
                },
                () => {
                    this.name();
                }
            );
        }
        else if (e.target.id == "code") {
            this.setState(
                {
                    code: e.target.value
                },
                () => {
                    this.code();
                }
            );
        }
        else if (e.target.id == "country") {
            this.setState(
                {
                    country: e.target.value
                },
                () => {
                    this.country();
                }
            );

        }
        else if (e.target.id == "state") {
            this.setState(
                {
                    state: e.target.value
                },
                () => {
                    this.statee();
                }
            );
            if (e.target.value != "") {
                const t = this
                setTimeout(function () {

                    t.setState({
                        cityData: t.state.cityStateData[0].states[t.state.state]
                    })
                }, 100)
            }

        }
        else if (e.target.id == "city") {
            this.setState(
                {
                    city: e.target.value
                },
                () => {
                    this.city();
                }
            );

        } else if (e.target.id == "zipcode") {
            this.setState(
                {
                    zipcode: e.target.value
                },
                () => {
                    this.zipcode();
                }
            );
        } else if (e.target.id == "gstin") {
            this.setState(
                {
                    gstin: e.target.value
                },
                () => {
                    this.gstin();
                }
            );
        } else if (e.target.id == "timezone") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    timezone: e.target.value
                },
                () => {
                    this.timezone();
                }
            );
        }
        else if (e.target.id == "displayName") {
            this.setState(
                {
                    displayName: e.target.value
                },
                () => {
                    this.displayName();
                }
            );

        } 
        else if (e.target.id == "partner") {
            this.setState({partner: e.target.value});
        }
        else if (e.target.id == "zone") {
            this.setState({zone: e.target.value});
        }
        else if (e.target.id == "region") {
            this.setState({region: e.target.value});
        }
        else if (e.target.id == "group") {
            this.setState({group: e.target.value});
        }
        else if (e.target.id == "latitude") {
            this.setState(
                {
                    latitude: e.target.value
                },
                // () => {
                //     this.latitude();
                // }
            );
        } else if (e.target.id == "longitude") {
            this.setState(
                {
                    longitude: e.target.value
                },
                // () => {
                //     this.longitude();
                // }
            );
        }
        else if (e.target.id == "status") {
            this.setState(
                {
                    status: e.target.value
                },
                () => {
                    this.status();
                }
            );

        } else if (e.target.id == "type") {
            this.setState(
                {
                    type: e.target.value
                },
                () => {
                    this.type();
                }
            );
        } else if (e.target.id == "area") {
            this.setState(
                {
                    area: e.target.value
                },
                () => {
                    this.area();
                }
            );
        }
        else if (e.target.id == "additional") {
            this.setState({additional: e.target.value});
        }
        else if (e.target.id == "unit") {
            this.setState(
                {
                    unit: e.target.value
                },
                () => {
                    this.unit();
                }
            );

        } else if (e.target.id == "numberOfFloors") {
            this.setState(
                {
                    numberOfFloors: e.target.value
                },
                () => {
                    this.numberOfFloors();
                }
            );
        } 
        else if (e.target.id == "udf1") {
            this.setState({udf1: e.target.value});
        }
        else if (e.target.id == "udf2") {
            this.setState({udf2: e.target.value});
        }
        else if (e.target.id == "udf3") {
            this.setState({udf3: e.target.value});
        }
        else if (e.target.id == "udf4") {
            this.setState({udf4: e.target.value});
        }
        else if (e.target.id == "udf5") {
            this.setState({udf5: e.target.value});
        }
        else if (e.target.id == "opStartDate") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    opStartDate: e.target.value
                },
                () => {
                    this.opStartDate();
                }
            );
        } else if (e.target.id == "opEndDate") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    opEndDate: e.target.value
                },
                () => {
                    this.opEndDate();
                }
            );
        }

        else if (e.target.id == "isOnlineStore" || e.target.id == "isBilling" || e.target.id == "isShipping") {
            if (e.target.id == "isOnlineStore") {
                this.setState({
                    isOnlineStore: !this.state.isOnlineStore
                })
            } else if (e.target.id == "isBilling") {
                this.setState({
                    isBilling: !this.state.isBilling
                })
            } else if (e.target.id == "isShipping") {
                this.setState({
                    isShipping: !this.state.isShipping
                })
            }
        }

        else if (e.target.id == "contactName") {
            this.setState(
                {
                    contactName: e.target.value
                },
                () => {
                    this.contactName();
                }
            );

        } else if (e.target.id == "contactNumber") {
            this.setState(
                {
                    contactNumber: e.target.value
                },
                () => {
                    this.contactNumber();
                }
            );
        } else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );
        } else if (e.target.id == "mobile") {
            this.setState(
                {
                    mobile: e.target.value
                },
                () => {
                    this.mobile();
                }
            );
        }

        else if (e.target.id == "web") {
            this.setState(
                {
                    Web: e.target.value
                },
                () => {
                    this.Web();
                }
            );
        }

        else if (e.target.id == "address") {
            this.setState(
                {
                    Address: e.target.value
                },
                () => {
                    this.Address();
                }
            );
        } else if (e.target.id == "billingName") {
            this.setState(
                {
                    billingName: e.target.value
                },
                () => {
                    this.billingName();
                }
            );
        } else if (e.target.id == "billingPhone") {
            this.setState(
                {
                    billingPhone: e.target.value
                },
                () => {
                    this.billingPhone();
                }
            );
        } else if (e.target.id == "billingEmail") {
            this.setState(
                {
                    billingEmail: e.target.value
                },
                () => {
                    this.billingEmail();
                }
            );
        }
        else if (e.target.id == "billingMobile") {
            this.setState(
                {
                    billingMobile: e.target.value
                },
                () => {
                    this.billingMobile();
                }
            );
        } else if (e.target.id == "shippingName") {
            this.setState(
                {
                    shippingName: e.target.value
                },
                () => {
                    this.shippingName();
                }
            );
        } else if (e.target.id == "shippingPhone") {
            this.setState(
                {
                    shippingPhone: e.target.value
                },
                () => {
                    this.shippingPhone();
                }
            );
        } else if (e.target.id == "shippingEmail") {
            this.setState(
                {
                    shippingEmail: e.target.value
                },
                () => {
                    this.shippingEmail();
                }
            );
        }
        else if (e.target.id == "shippingMobile") {
            this.setState(
                {
                    shippingMobile: e.target.value
                },
                () => {
                    this.shippingMobile();
                }
            );
        }
        else if (e.target.id == "receiverName") {
            this.setState(
                {
                    receiverName: e.target.value
                },
                () => {
                    this.receiverName();
                }
            );
        } else if (e.target.id == "receiverPhone") {
            this.setState(
                {
                    receiverPhone: e.target.value
                },
                () => {
                    this.receiverPhone();
                }
            );
        } else if (e.target.id == "receiverEmail") {
            this.setState(
                {
                    receiverEmail: e.target.value
                },
                () => {
                    this.receiverEmail();
                }
            );
        }
        else if (e.target.id == "receiverMobile") {
            this.setState(
                {
                    receiverMobile: e.target.value
                },
                () => {
                    this.receiverMobile();
                }
            );
        } else if (e.target.id == "startDate") {
            this.setState({
                startDate: e.target.value
            }, () => {
                this.startDateError()
            })
        } else if (e.target.id == "endDate") {
            this.setState({
                endDate: e.target.value
            }, () => {
                this.endDateError()
            })
        } else if (e.target.id == "isProxyStore") {
            this.setState({
                isProxyStore: !this.state.isProxyStore
            }, () => {
                this.isProxyStoreError()
            })
        }
    }

    isProxyStoreError() {
        if (this.state.isProxyStore == "") {
            this.setState({
                isProxyStoreerr: true
            })
        } else {
            this.setState({
                isProxyStoreerr: false
            })
        }
    }

    contactSubmit(e) {
        e.preventDefault();
        this.name();
        this.code();
        this.country();
        this.statee();
        this.city();
        this.zipcode();
        this.gstin();
        this.timezone();
        this.displayName();
        this.status();
        this.startDateError();
        this.endDateError();
        //this.isProxyStoreError();
        const t = this;
        setTimeout(function () {
            const { nameerr, codeerr, countryerr, stateerr, cityerr, zipcodeerr, gstinerr, timezoneerr, displayNameerr, latittudeerr,
                longitudeeer, statuserr, startDaterr, endDaterr, proxyStorerr } = t.state;
            if (!nameerr && !codeerr && !countryerr && !stateerr && !cityerr && !zipcodeerr && !gstinerr && !timezoneerr && !displayNameerr) {
                let siteData = {
                    name: t.state.name,
                    code: t.state.code,
                    country: t.state.country,
                    state: t.state.state,
                    city: t.state.city,
                    zipCode: t.state.zipcode,
                    gstin: t.state.gstin,
                    timeZone: t.state.timezone,
                    displayName: t.state.displayName,
                    partner: t.state.partner,
                    zone: t.state.zone,
                    region: t.state.region,
                    groupName: t.state.group,
                    latitude: t.state.latitude,
                    longitude: t.state.longitude,
                    //status: t.state.status,
                    type: t.state.type,
                    additional: t.state.additional,
                    area: t.state.area,
                    areaUnit: t.state.unit,
                    numberFloors: t.state.numberOfFloors,
                    udf1: t.state.udf1,
                    udf2: t.state.udf2,
                    udf3: t.state.udf3,
                    udf4: t.state.udf4,
                    udf5: t.state.udf5,
                    optStoreSDate: t.state.opStartDate,
                    optStoreEDate: t.state.opEndDate,
                    // isBilling: t.state.isBilling,
                    // isShipping: t.state.isShipping,
                    // isOnlineStore: t.state.isOnlineStore,
                    // contactName: t.state.contactName,
                    // contactNumber: t.state.contactNumber,
                    // email: t.state.email,
                    // mobile: t.state.mobile,
                    // website: t.state.Web,
                    address: t.state.Address,
                    // billingName: t.state.billingName,
                    // billingPhone: t.state.billingPhone,
                    // billingEmail: t.state.billingEmail,
                    // billingMobile: t.state.billingMobile,
                    // shippingName: t.state.shippingName,
                    // shippingPhone: t.state.shippingPhone,
                    // shippingEmail: t.state.shippingEmail,
                    // shippingMobile: t.state.shippingMobile,
                    // receiverName: t.state.receiverName,
                    // receiverPhone: t.state.receiverPhone,
                    // receiverEmail: t.state.receiverEmail,
                    // receiverMobile: t.state.receiverMobile,
                    // // _______________________proxy____________________________
                    // startDate: t.state.startDate,
                    // endDate: t.state.endDate,
                    // isProxyStore: t.state.isProxyStore,
                    // proxyStore: t.state.proxyStore,
                    // orgId: null,
                    isProxyStore: t.state.isProxyStore,
                    proxyStore: t.state.proxyStore
                }
                t.props.addSiteRequest(siteData);
            }
        }, 100)
    }
    openproxyStore(e) {
        let data = {
            no: 1,
            type: 1,
            search: ""
        }

        this.props.proxyStoreRequest(data)
        this.setState({
            proxyStoreModal: true
        })
    }
    updateProxyStore(data) {

        this.setState({
            proxyStore: data.proxyStore
        })
    }
    closeProxyModal() {
        this.setState({
            proxyStoreModal: !this.state.proxyStoreModal
        })
    }

    render() {

        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        $("body").on("keyup", ".numbersOnlywithhighpn", function () {
            if (this.value != this.value.replace(/[^0-9-]/g, '')) {
                this.value = this.value.replace(/[^0-9-]/g, '');
            }
        });
        const
            { isProxyStore, name, nameerr, code, codeerr, country, countryerr, state, stateerr, city, cityerr, zipcode, zipcodeerr, gstin, gstinerr, timezone, timezoneerr, displayName, displayNameerr, partner, zone, region, group, latitude, latittudeerr, longitude, longitudeeer, status, statuserr, type, typeerr, additional, area, areaerr, unit, uniterr, numberOfFloors, numberOfFloorserr, udf1, udf2, udf3, udf4, udf5, opStartDate, opStartDateerr, opEndDate, opEndDateerr, isCheckbox, isBilling, isOnlineStore, isShipping, isCheckboxerr, contactName, contactNameerr, contactNumber, contactNumbererr, email, emailerr, mobile, mobileerr, Web, Websiteerr, Address, Addresserr, billingName, billingNameerr, billingPhone, billingPhoneerr, billingEmail, billingEmailerr, billingMobile, billingMobileerr, shippingName, shippingNameerr, shippingPhone, shippingPhoneerr, shippingEmail, shippingEmailerr, shippingMobile, shippingMobileerr, receiverName, receiverNameerr, receiverPhone, receiverPhoneerr, receiverEmail, receiverEmailerr, receiverMobile, receiverMobileerr, endDaterr, startDaterr, proxyStorerr
            } = this.state;

        return (
            <div className="container-fluid nc-design pad-l60">
                <div className="container_div " id="home">
                    <NewSideBar {...this.props} />
                    <SideBar {...this.props} />
                    {/* <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null} */}
                    <div className="container_div m-top-75 " id="">
                        <div className="col-md-12 col-sm-12 border-btm">
                            <div className="menu_path n-menu-path">
                                <ul className="list-inline width_100 pad20 nmp-inner">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid pad-0">
                        <form name="siteForm" className="organizationForm" onSubmit={(e) => this.contactSubmit((e))}>
                            <div className="col-lg-12 pad-0 ">
                                <div className="gen-vendor-potal-design p-lr-47">
                                    <div className="col-lg-6 pad-0">
                                        <div className="gvpd-left">
                                            {/* <div className="add-organization">
                                                <span>Site</span>
                                                <p>Add Location Details</p>
                                            </div> */}
                                            <button className="gvpd-back" onClick={() => this.props.history.push("/administration/site")} type="reset">
                                                <span className="back-arrow">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                                        <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                                    </svg>
                                                </span>
                                                Back
                                            </button>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 pad-0">
                                        <div className="gvpd-right">
                                            <button type="reset" className="gen-clear" onClick={() => this.onClear()}>Clear</button>
                                            <button type="submit" className="gen-save">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="add-vendor-page-layout">
                                    <div className="manage-sticky-div">
                                        <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                            <div className="ao-billing-details">
                                                <h4>Basic Details</h4>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-15">
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="code">Site Code</label>
                                                <input type="text" className={codeerr ? "form-box errorBorder" : "form-box"} id="code" name="code" value={code} onChange={e => this.handleChange(e)} />
                                                {/* {codeerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {codeerr ? (
                                                    <span className="error">
                                                        Enter  Code
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="name">Site Name</label>
                                                <input type="text" className={nameerr ? "form-box errorBorder" : "form-box"} name="Site name" id="name" value={name} onChange={e => this.handleChange(e)} />
                                                {/* {nameerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {nameerr ? (
                                                    <span className="error">
                                                        Enter  name
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="name">Display Name</label>
                                                <input type="text" className={displayNameerr ? "form-box errorBorder" : "form-box"} name="displayName" id="displayName" value={displayName} onChange={e => this.handleChange(e)} />
                                                {/* {displayNameerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {displayNameerr ? (
                                                    <span className="error">
                                                        Enter Display Name
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="country">Site Type</label>
                                                <select className={countryerr ? "form-box errorBorder" : "form-box"} ref="country" id="country" name="country" value={country} onChange={(e) => this.handleChange(e)}>
                                                    <option value="" >Select Type</option>
                                                </select>
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="country">Select Country</label>
                                                <select className={countryerr ? "form-box errorBorder" : "form-box"} ref="country" id="country" name="country" value={country} onChange={(e) => this.handleChange(e)}>
                                                    <option value="" >Select Country</option>
                                                    <option value="India">India</option>
                                                </select>
                                                {/* {countryerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {countryerr ? (
                                                    <span className="error">
                                                        Select Country
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="state">Select State</label>
                                                <select className={stateerr ? "form-box errorBorder" : "form-box"} id="state" name="state" value={state} onChange={e => this.handleChange(e)} >
                                                    <option value="">Select State</option>
                                                    {this.state.stateData.map((data, i) => <option value={data} key={i}>{data}</option>

                                                    )}
                                                </select>
                                                {/* {stateerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {stateerr ? (
                                                    <span className="error">
                                                        Select  State
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="city">Select City</label>
                                                <select className={cityerr ? "form-box errorBorder" : "form-box"} id="city" name="city" value={city} onChange={e => this.handleChange(e)}>
                                                    <option value="">City</option>
                                                    {this.state.cityData != undefined ? this.state.cityData.map((data, i) =>
                                                        <option value={data} key={i}>{data}</option>) : null}
                                                </select>
                                                {/* {cityerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {cityerr ? (
                                                    <span className="error">
                                                        Enter City
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="zipcode">Zipcode</label>
                                                <input type="text" className={zipcodeerr ? "errorBorder form-box numbersOnly" : "form-box numbersOnly"} id="zipcode" name="zipcode" maxLength="6" value={zipcode} onChange={e => this.handleChange(e)} />
                                                {/* {zipcodeerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                {zipcodeerr ? (
                                                    <span className="error">
                                                        Enter valid Zipcode
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="GSTIN">GSTIN</label>
                                                <input type="text" className={gstinerr ? "errorBorder form-box" : "form-box"} id="gstin" name="gstin" value={gstin} onChange={e => this.handleChange(e)} />
                                                {gstinerr ? (
                                                    <span className="error">
                                                        Enter GSTIN
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="col-md-4 col-sm-4 pad-lft-0">
                                            <label>Address</label>
                                                <textarea className={Addresserr ? "formtextarea errorBorder" : "formtextarea"} value={Address} id="address" onChange={e => this.handleChange(e)}></textarea>
                                                {/* {Addresserr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                                                {Addresserr ? (
                                                    <span className="error" >
                                                        Enter Address
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        {/* <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30" >
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="status">Select Status</label>
                                                <select className={statuserr ? "form-box errorBorder" : "form-box"} id="status" name="status" value={status} onChange={e => this.handleChange(e)}>
                                                    <option value=" ">Select Status</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Inactive">Inactive</option>
                                                </select>
                                                {statuserr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {statuserr ? (
                                                    <span className="error">
                                                        Select Status
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="type">Select Site</label>
                                                <select className={typeerr ? "errorBorder form-box" : "form-box"} id="type" name="type" value={type} onChange={e => this.handleChange(e)} >
                                                    <option value=" ">Select Site Type</option>
                                                    <option value="Plant">Plant</option>
                                                    <option value="Warehouse">Warehouse</option>
                                                    <option value="Store">Store</option>
                                                </select>
                                                {typeerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {typeerr ? (
                                                    <span className="error">
                                                        Select Site Type
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="area">Area Coverage</label>
                                                <input type="text" className={areaerr ? "form-box errorBorder numbersOnly" : "form-box numbersOnly"} id="area" name="area" value={area} onChange={e => this.handleChange(e)} />
                                                {areaerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {areaerr ? (
                                                    <span className="error">
                                                        Enter Area Coverage
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="start">Store Operation Start Date</label>
                                                <input type="date" className={opStartDateerr ? "form-box errorBorder" : "form-box"} name="opStartDate" id="opStartDate" value={opStartDate} onChange={e => this.handleChange(e)} />
                                                {opStartDateerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {opStartDateerr ? (
                                                    <span className="error">
                                                        Enter Start Date
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="row" >
                                            <div className="col-md-2 col-sm-2 ">
                                                <ul className="list-inline width_100">
                                                    <div className="siteCheckBox">
                                                        <label className="checkBoxLabel0" htmlFor="isBilling"><input type="checkBox" checked={isBilling} name="isBilling" value={isBilling} id="isBilling" onChange={(e) => this.handleChange(e)} />IsBilling <span className="checkmark1"></span> </label>
                                                    </div>
                                                </ul>
                                            </div>
                                            <div className="col-md-2 col-sm-2 ">
                                                <ul className="list-inline width_100">
                                                    <div className="siteCheckBox">
                                                        <label className="checkBoxLabel0" htmlFor="isShipping"><input type="checkBox" checked={isShipping} id="isShipping" name="isShipping" value={isShipping} onChange={e => this.handleChange(e)} />IsShipping <span className="checkmark1"></span> </label>
                                                    </div>

                                                </ul>
                                            </div>
                                            <div className="col-md-2 col-sm-2">
                                                <ul className="list-inline width_100">
                                                    <div className="siteCheckBox">
                                                        <label className="checkBoxLabel0" htmlFor="isOnlineStore"><input type="checkBox" checked={isOnlineStore} id="isOnlineStore" name="isOnlineStore" value={isOnlineStore} onChange={e => this.handleChange(e)} />IsOnlineStore <span className="checkmark1"></span> </label>
                                                    </div>

                                                </ul>
                                            </div>
                                        </div> */}
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="ao-billing-details">
                                                <h4>Additional Details</h4>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-15">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Partner</label>
                                                    <input type="text" className="form-box" id="partner" name="partner" value={partner} onChange={e => this.handleChange(e)} />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Zone</label>
                                                    <input type="text" className="form-box" id="zone" name="zone" value={zone} onChange={e => this.handleChange(e)} />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Region</label>
                                                    <input type="text" className="form-box" id="region" name="region" value={region} onChange={e => this.handleChange(e)} />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Group</label>
                                                    <input type="text" className="form-box" id="group" name="group" value={group} onChange={e => this.handleChange(e)} />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="latitude">Latitude</label>
                                                    <input type="text" className={latittudeerr ? "form-box errorBorder" : "form-box"} id="latitude" name="latitude" value={latitude} onChange={e => this.handleChange(e)} />
                                                    {/* {latittudeerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {latittudeerr ? (
                                                        <span className="error">
                                                            Enter Latitude
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="longitude">Longitude</label>
                                                    <input type="text" className={longitudeeer ? "form-box errorBorder" : "form-box"} id="longitude" name="longitude" value={longitude} onChange={e => this.handleChange(e)} />
                                                    {/* {longitudeeer ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {longitudeeer ? (
                                                        <span className="error">
                                                            Enter Longitude
                                                        </span>
                                                    ) : null}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Site Operation Start Date</label>
                                                    <input type="date" className="form-box" value={opStartDate} id="opStartDate" name="opStartDate" />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Site Operation End Date</label>
                                                    <input type="date" className="form-box" value={opEndDate} id="opEndDate" name="opEndDate" />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Additional</label>
                                                    <input type="text" className="form-box" id="additional" name="additional" value={additional} onChange={e => this.handleChange(e)} />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Area</label>
                                                    <input type="text" className="form-box" id="area" name="area" value={area} onChange={e => this.handleChange(e)} />
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="unit">Area Unit</label>
                                                    <select className={uniterr ? "errorBorder form-box" : "form-box"} id="unit" name="unit" value={unit} onChange={e => this.handleChange(e)} >
                                                        {/* {uniterr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                        <option value="">Select Area unit</option>
                                                        <option value="sq/yd">sq/yd</option>
                                                        <option value="sq/mtr">sq/mtr</option>
                                                        <option value="sq/ft">sq/ft</option>
                                                        <option value="Acre">Acre</option>
                                                        <option value="Hectare">Hectare</option>
                                                        <option value="Decare">Decare</option>
                                                        <option value="sq/km">sq/km</option>
                                                        <option value="sq/cm">sq/cm</option>
                                                        <option value="sq/mm">sq/mm</option>
                                                        <option value="sq/in">sq/in</option>
                                                        {uniterr ? (
                                                            <span className="error">
                                                                Enter Area Unit
                                                            </span>
                                                        ) : null}
                                                    </select>
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="number">No. Of Floors</label>
                                                    <input type="text" maxLength="2" className={numberOfFloorserr ? "form-box numbersOnly errorBorder" : "form-box numbersOnly"} id="numberOfFloors" name="numberOfFloors" value={numberOfFloors} onChange={e => this.handleChange(e)} />
                                                    {/* {numberOfFloorserr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {numberOfFloorserr ? (
                                                        <span className="error">
                                                            Enter Number of Floors
                                                        </span>
                                                    ) : null}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="time">Select Timezone</label>
                                                    <select className={timezoneerr ? "form-box errorBorder" : "form-box"} onChange={(e) => this.handleChange(e)} value={timezone} id="timezone" title="Timezone" name="DropDownTimezone" >
                                                        <option value="">Select Timezone</option>
                                                        <option value="(GMT +5:30) Bombay, Calcutta, Madras, New Delhi">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                                        {timezoneerr ? (
                                                            <span className="error">
                                                                Enter Timezone
                                                            </span>
                                                        ) : null}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            {/* <div className="add-organization">
                                                <span>Add proxy store</span>
                                            </div> */}
                                            <div className="col-md-12 pad-0">
                                                <div className="col-md-3 pad-0">
                                                    <div className="siteCheckBox">
                                                        <label className="checkBoxLabel0" htmlFor="isProxyStore"><input type="checkBox" checked={isProxyStore} name="isProxyStore" value={isProxyStore} id="isProxyStore" onChange={(e) => this.handleChange(e)} />isProxyStoreApplicable <span className="checkmark1"></span> </label>
                                                    </div>
                                                    {this.state.isProxyStoreerr ? <span className="error">
                                                        check proxystore
                                                    </span> : null}
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-15">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <div className="pi-new-layout">
                                                        <label htmlFor="proxyStore">Proxy Store</label>
                                                        <div className="inputTextKeyFucMain" onClick={(e) => this.openproxyStore(e)} >
                                                            <input type="text" className={proxyStorerr ? "errorBorder pnl-purchase-input" : "pnl-purchase-input onFocus"} />
                                                            <span className="modal-search-btn">
                                                                <svg xmlns="http://www.w3.org/2000/svg" id="openSite" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        {proxyStorerr ? (
                                                            <span className="error">
                                                                Select Store
                                                            </span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label>UDF1</label>
                                                <input type="text" className="form-box" id="udf1" name="udf1" value={udf1} onChange={e => this.handleChange(e)} />
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label>UDF2</label>
                                                <input type="text" className="form-box" id="udf2" name="udf2" value={udf2} onChange={e => this.handleChange(e)} />
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label>UDF3</label>
                                                <input type="text" className="form-box" id="udf3" name="udf3" value={udf3} onChange={e => this.handleChange(e)} />
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label>UDF4</label>
                                                <input type="text" className="form-box" id="udf4" name="udf4" value={udf4} onChange={e => this.handleChange(e)} />
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label>UDF5</label>
                                                <input type="text" className="form-box" id="udf5" name="udf5" value={udf5} onChange={e => this.handleChange(e)} />
                                            </div>
                                        </div>


                                        {/* <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="name">Name</label>
                                                <input type="text" className={contactNameerr ? "errorBorder form-box" : "form-box"} name="contactName" id="contactName" value={contactName} onChange={e => this.handleChange(e)} />
                                                {contactNameerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {contactNameerr ? (
                                                    <span className="error">
                                                        Enter Contact Name
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="number">Phone Number</label>
                                                <input type="text" maxLength="15" className={contactNumbererr ? "errorBorder form-box numbersOnlywithhighpn" : "form-box numbersOnlywithhighpn"} id="contactNumber" name="contactNumber" value={contactNumber} onChange={e => this.handleChange(e)} />
                                                {contactNumbererr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {contactNumbererr ? (
                                                    <span className="error">
                                                        Enter Valid Phone Number
                                                    </span>
                                                ) : null}

                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="email">Email</label>
                                                <input type="email" className={emailerr ? "form-box errorBorder" : "form-box"} id="email" name="email" value={email} onChange={e => this.handleChange(e)} />


                                                {emailerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {emailerr ? (
                                                    <span className="error">
                                                        Enter Valid Email Id
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="mobile">Mobile Number</label>
                                                <input type="text" maxLength="10" className={mobileerr ? "form-box errorBorder numbersOnly" : "form-box numbersOnly"} id="mobile" name="mobile" value={mobile} onChange={e => this.handleChange(e)} />

                                                {mobileerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {mobileerr ? (
                                                    <span className="error">
                                                        Enter Valid Mobile Number
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0">
                                                <label htmlFor="web">Website</label>
                                                <input type="text" className={Websiteerr ? "form-box errorBorder" : "form-box"} id="web" value={Web} onChange={e => this.handleChange(e)} />
                                                {Websiteerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {Websiteerr ? (
                                                    <span className="error">
                                                        Enter Valid Website
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-4 col-sm-4 pad-lft-0">
                                            <label>Address</label>
                                                <textarea className={Addresserr ? "formtextarea errorBorder" : "formtextarea"} value={Address} id="address" onChange={e => this.handleChange(e)}></textarea>
                                                {Addresserr ? <img src={errorIcon} className="error_icon-txtarea" /> : null}
                                                {Addresserr ? (
                                                    <span className="error" >
                                                        Enter Address
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="ao-billing-details">
                                                <h4>BILLING DETAILS</h4>
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="name">Name</label>
                                                <input type="text" className={billingNameerr ? "form-box errorBorder" : "form-box"} name="billingName" value={billingName} id="billingName" onChange={e => this.handleChange(e)} />
                                                {billingNameerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {billingNameerr ? (
                                                    <span className="error">
                                                        Enter Name
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="phone">Phone Number</label>
                                                <input type="text" maxLength="15" className={billingPhoneerr ? "errorBorder form-box numbersOnlywithhighpn" : "form-box numbersOnlywithhighpn"} name="billingPhone" id="billingPhone" value={billingPhone} onChange={e => this.handleChange(e)} />
                                                {billingPhoneerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {billingPhoneerr ? (
                                                    <span className="error">
                                                        Enter valid Phone Number
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="email">Email</label>
                                                <input type="email" className={billingEmailerr ? "form-box errorBorder" : "form-box"} name="billingEmail" id="billingEmail" value={billingEmail} onChange={e => this.handleChange(e)} />
                                                {billingEmailerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {billingEmailerr ? (
                                                    <span className="error">
                                                        Enter valid Email
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="mobile">Mobile Number</label>
                                                <input type="text" maxLength="10" className={billingMobileerr ? "form-box errorBorder numbersOnly" : "form-box numbersOnly"} name="billingMobile" id="billingMobile" value={billingMobile} onChange={e => this.handleChange(e)} />
                                                {billingMobileerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {billingMobileerr ? (
                                                    <span className="error">
                                                        Enter valid Mobile Number
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="ao-billing-details">
                                                <h4>SHIPPING DETAILS</h4>
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="name">Name</label>
                                                <input type="text" className={shippingNameerr ? "form-box errorBorder" : "form-box"} name="shippingName" id="shippingName" value={shippingName} onChange={e => this.handleChange(e)} />
                                                {shippingNameerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {shippingNameerr ? (
                                                    <span className="error">
                                                        Enter Name
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="phone">Phone Number</label>
                                                <input type="text" maxLength="15" className={shippingPhoneerr ? "errorBorder form-box numbersOnlywithhighpn" : "form-box numbersOnlywithhighpn"} name="shippingPhone" id="shippingPhone" value={shippingPhone} onChange={e => this.handleChange(e)} />
                                                {shippingPhoneerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {shippingPhoneerr ? (
                                                    <span className="error">
                                                        Enter Valid Phone Number
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="email">Email</label>
                                                <input type="email" className={shippingEmailerr ? "form-box errorBorder" : "form-box"} name="shippingEmail" id="shippingEmail" value={shippingEmail} onChange={e => this.handleChange(e)} />
                                                {shippingEmailerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {shippingEmailerr ? (
                                                    <span className="error">
                                                        Enter Valid Email
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="mobile">Mobile Number</label>
                                                <input type="text" maxLength="10" className={shippingMobileerr ? "form-box errorBorder numbersOnly" : "form-box numbersOnly"} name="shippingMobile" id="shippingMobile" value={shippingMobile} onChange={e => this.handleChange(e)} />
                                                {shippingMobileerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {shippingMobileerr ? (
                                                    <span className="error">
                                                        Enter Valid Mobile Number
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                            <div className="ao-billing-details">
                                                <h4>RECEIVER DETAILS</h4>
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="name">Name</label>
                                                <input type="text" className={receiverNameerr ? "errorBorder form-box" : "form-box"} name="receiverName" id="receiverName" value={receiverName} onChange={e => this.handleChange(e)} />
                                                {receiverNameerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {receiverNameerr ? (
                                                    <span className="error">
                                                        Enter Valid Name
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="phone">Phone Number</label>
                                                <input type="text" maxLength="15" className={receiverPhoneerr ? "form-box errorBorder numbersOnlywithhighpn" : "form-box numbersOnlywithhighpn"} name="receiverPhone" id="receiverPhone" value={receiverPhone} onChange={e => this.handleChange(e)} />
                                                {receiverPhoneerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {receiverPhoneerr ? (
                                                    <span className="error">
                                                        Enter Valid Phone Number
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="email">Email</label>
                                                <input type="email" className={receiverEmailerr ? "form-box errorBorder" : "form-box"} name="receiverEmail" id="receiverEmail" value={receiverEmail} onChange={e => this.handleChange(e)} />
                                                {receiverEmailerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {receiverEmailerr ? (
                                                    <span className="error">
                                                        Enter Valid Email
                                                    </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-2 col-sm-2 pad-lft-0 m-top-15">
                                                <label htmlFor="mobile">Mobile Number</label>
                                                <input type="text" maxLength="10" className={receiverMobileerr ? "form-box numbersOnly errorBorder" : "form-box numbersOnly"} name="receiverMobile" id="receiverMobile" value={receiverMobile} onChange={e => this.handleChange(e)} />
                                                {receiverMobileerr ? <img src={errorIcon} className="error_icon right50" /> : null}
                                                {receiverMobileerr ? (
                                                    <span className="error">
                                                        Enter Valid Mobile Number
                                                    </span>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="startDate">Start Date</label>
                                                    <input type="date" className={startDaterr ? "form-box errorBorder" : "form-box"} id="startDate" value={this.state.startDate} min={this.state.minDate} max={this.state.maxDate} onChange={(e) => this.handleChange(e)} />
                                                    {startDaterr ?
                                                        <span className="error">
                                                            Enter start date
                                                    </span> : null}
                                                </div>

                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="endDate">End Date</label>
                                                    <input type="date" className={endDaterr ? "form-box errorBorder" : "form-box"} id="endDate" value={this.state.endDate} min={this.state.startDate} max={this.state.maxDate} onChange={(e) => this.handleChange(e)} />
                                                    {endDaterr ?
                                                        <span className="error">
                                                            Enter end date
                                                    </span> : null}
                                                </div>
                                            </div>
                                        </div> */}


                                    </div>
                                    {/* <div className="col-md-12 col-sm-12">
                                        <div className="footerbutton">
                                            <ul className="list-inline m-lft-0 m-top-20">
                                                <li>
                                                    <button type="reset" className="clearbtnOrganisation" onClick={() => this.onClear()}>Clear</button>
                                                </li>
                                                <li >
                                                    <button type="submit" className="save-changes">Save</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> */}


                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.proxyStoreModal ? <ProxyStoreModal {...this.state} {...this.props} closeProxyModal={(e) => this.closeProxyModal(e)} updateProxyStore={(e) => this.updateProxyStore(e)} /> : null}
            </div>
        );
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SiteCreate);
