import React from 'react';
import Json from "../jsonFile.json";
// import errorIcon from "../../../assets/error_icon.svg";
class SiteEditModal extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            id: "",
            // contactId: "",
            // shippingId: "",
            // billingId: "",
            // receiverId: "",
            name: "",
            nameerr: false,
            code: "",
            codeerr: false,
            country: "",
            countryerr: false,
            state: "",
            stateerr: false,
            city: "",
            cityerr: false,
            zipcode: "",
            zipcodeerr: false,
            timezone: "",
            timezoneerr: false,
            displayName: "",
            displayNameerr: false,
            latitude: "",
            latittudeerr: false,
            longitude: "",
            longitudeeer: false,
            status: "",
            statuserr: false,
            type: "",
            typeerr: false,
            area: "",
            areaerr: false,
            unit: "",
            uniterr: false,
            numberOfFloors: "",
            numberOfFloorserr: false,
            opStartDate: "",
            opStartDateerr: false,
            opEndDate: "",
            opEndDateerr: false,
            isShipping: false,
            isBilling: false,
            isOnlineStore: false,
            contactName: "",
            contactNameerr: false,
            contactNumber: "",
            contactNumbererr: false,
            email: "",
            emailerr: false,
            mobile: "",
            mobileerr: false,
            Web: "",
            Websiteerr: false,
            Address: "",
            Addresserr: false,
            billingName: "",
            billingNameerr: false,
            billingPhone: "",
            billingPhoneerr: false,
            billingEmail: "",
            billingEmailerr: false,
            billingMobile: "",
            billingMobilrerr: false,
            shippingName: "",
            shippingerr: false,
            shippingPhone: "",
            shippingPhoneerr: false,
            shippingEmail: "",
            shippingEmailerr: false,
            shippingMobile: "",
            shippingMobilrerr: false,
            receiverName: "",
            receiverNameerr: false,
            receiverPhone: "",
            receiverPhoneerr: false,
            receiverEmail: "",
            receiverEmailerr: false,
            receiverMobile: "",
            receiverMobilrerr: false,
            btnDisable: true,
            cityStateData: Json,
            stateData: [],
            cityData:[],
            gstin: "",
            gstinerr: false,
            partner: "",
            partnererr: false,
            zone: "",
            zoneerr: false,
            region: "",
            regionerr: false,
            groupName: "",
            groupNameerr: false,
            udf1: "",
            udf2: "",
            udf3: "",
            udf4: "",
            udf5: "",
            additional: ""
        }
    }


    onClear(e) {
        document.getElementById('timezone').placeholder = "Timezone";
        document.getElementById('opStartDate').placeholder = "Operation Start Date";
        e.preventDefault();
        this.setState({
            name: "",
            code: "",
            country: "",
            state: "",
            city: "",
            zipcode: "",
            timezone: "",
            displayName: "",
            latitude: "",
            longitude: "",
            status: "",
            type: "",
            area: "",
            unit: "",
            numberOfFloors: "",
            opStartDate: "",
            opEndDate: "",
            contactName: "",
            contactNumber: "",
            email: "",
            mobile: "",
            Web: "",
            Address: "",
            billingName: "",
            billingPhone: "",
            billingEmail: "",
            billingMobile: "",
            shippingName: "",
            shippingPhone: "",
            shippingEmail: "",
            shippingMobile: "",
            receiverName: "",
            receiverPhone: "",
            receiverEmail: "",
            receiverMobile: "",
            gstin: "",
            partner: "",
            zone: "",
            region: "",
            groupName: "",
            udf1: "",
            udf2: "",
            udf3: "",
            udf4: "",
            udf5: "",
            additional: ""
        })
    }

    //name
    name() {
        if (
            this.state.name == ""
        ) {
            this.setState({
                nameerr: true
            });
        } else {
            this.setState({
                nameerr: false
            });
        }
    }

    //code
    code() {
        if (
            this.state.code == "") {
            this.setState({
                codeerr: true
            });
        } else {
            this.setState({
                codeerr: false
            });
        }
    }
    //country
    country() {
        if (
            this.state.country == "") {
            this.setState({
                countryerr: true
            });
        } else {
            this.setState({
                countryerr: false
            });
        }
    }
    //state
    statee() {
        if (
            this.state.state == "") {
            this.setState({
                stateerr: true
            });
        } else {
            this.setState({
                stateerr: false
            });
        }

    }

    //city

    city() {
        if (
            this.state.city == "") {
            this.setState({
                cityerr: true
            });
        } else {
            this.setState({
                cityerr: false
            });
        }


    }
    //Zipcode

    zipcode() {

        if (
            this.state.zipcode == "" ||
            !this.state.zipcode.match(/(^\d{6}$)|(^\d{6}-\d{4}$)/) ||
            !this.state.zipcode.match(/^.{6}$/)
        ) {
            this.setState({
                zipcodeerr: true
            });
        } else {
            this.setState({
                zipcodeerr: false
            });
        }
    }
    //timezone

    timezone() {
        if (
            this.state.timezone == "") {
            this.setState({
                timezoneerr: true
            });
        } else {
            this.setState({
                timezoneerr: false
            });
        }

    }
    //displayName
    displayName() {
        if (this.state.displayName == "" || !this.state.displayName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                displayNameerr: true
            });
        } else {

            this.setState({
                displayNameerr: false
            });
        }
    }
    //latitude
    latitude() {
        if (this.state.latitude == "") {
            this.setState({
                latittudeerr: true
            });
        } else {

            this.setState({
                latittudeerr: false
            });
        }

    }

    //longitude
    longitude() {

        if (this.state.longitude == "") {
            this.setState({
                longitudeeer: true
            });
        } else {

            this.setState({
                longitudeeer: false
            });
        }
    }

    //status
    status() {
        if (this.state.status == "") {
            this.setState({
                statuserr: true
            });
        } else {

            this.setState({
                statuserr: false
            });
        }
    }
    //type
    type() {
        if (this.state.type == "") {
            this.setState({
                typeerr: true
            });
        } else {

            this.setState({
                typeerr: false
            });
        }


    }
    //area
    area() {
        if (this.state.area == "" ||
        !this.state.area.match(/^[1-9]\d+$/)) {
            this.setState({
                areaerr: true
            });

        } else {
            this.setState({
                areaerr: false
            });
        }
    }
    //unit
    unit() {
        if (this.state.unit == "") {
            this.setState({
                uniterr: true
            });

        } else {
            this.setState({
                uniterr: false
            });
        }
    }
    //numberOfFloors
    numberOfFloors() {
        if (this.state.numberOfFloors == "") {
            this.setState({
                numberOfFloorserr: true
            });

        } else {
            this.setState({
                numberOfFloorserr: false
            });
        }
    }
    //opStartDate
    opStartDate() {
        if (this.state.opStartDate == "") {
            this.setState({
                opStartDateerr: true
            });

        } else {
            this.setState({
                opStartDateerr: false
            });
        }
    }
    //opEndDate

    opEndDate() {
        if (this.state.opEndDate == "") {
            this.setState({
                opEndDateerr: false
            });

        } else {
            this.setState({
                opEndDateerr: false
            });
        }
    }

    //checkbox

    //contactName
    contactName() {
        if (this.state.contactName == "" || !this.state.contactName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                contactNameerr: true
            });
        } else {
            this.setState({
                contactNameerr: false
            });
        }

    }
    //contactNumber
    contactNumber() {
    
        if(this.state.contactNumber == "" || this.state.contactNumber.trim()==""){
            this.setState({
                contactNumbererr: false
            })
        }
        else if (this.state.contactNumber.length < 10){
            this.setState({
                contactNumbererr: true
            });
        }
        else {
            this.setState({
                contactNumbererr: false
            });
        }

    }
    //email
    email() {

        if (
            this.state.email == "" ||
            !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }

    //mobile
    mobile() {
        if (this.state.mobile == "" ||
            !this.state.mobile.match(/^\d{10}$/) ||
            !this.state.mobile.match(/^[1-9]\d+$/) ||
            !this.state.mobile.match(/^.{10}$/)) {
            this.setState({
                mobileerr: true
            });
        } else {
            this.setState({
                mobileerr: false
            });
        }
    }

    //Website
    Web() {
        if (
            this.state.Web == "" ||
            !this.state.Web.match(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
            
            )
        ) {
            this.setState({
                Websiteerr: true
            });
        } else {
            this.setState({
                Websiteerr: false
            });
        }
    }
    //
    Address() {
        if (this.state.Address == "") {
            this.setState({
                Addresserr: true
            });
        } else {
            this.setState({
                Addresserr: false
            });
        }

    }
    //billingName
    billingName() {
        if (this.state.billingName == "" || !this.state.billingName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                billingNameerr: true
            });
        } else {
            this.setState({
                billingNameerr: false
            });
        }

    }
    //billingPhone
    billingPhone() {
    
        if(this.state.billingPhone == "" || this.state.billingPhone.trim()==""){
            this.setState({
                billingPhoneerr: false
            })
        }
        else if (this.state.billingPhone.length < 10) {
            this.setState({
                billingPhoneerr: true
            });
        }
        else {
            this.setState({
                billingPhoneerr: false
            });
        }

    }
    //billingEmail
    billingEmail() {

        if (
            this.state.billingEmail == "" ||
            !this.state.billingEmail.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                billingEmailerr: true
            });
        } else {
            this.setState({
                billingEmailerr: false
            });
        }
    }
    //billingMobile
    billingMobile() {
        if (this.state.billingMobile == "" || !this.state.billingMobile.match(/^\d{10}$/) ||
            !this.state.billingMobile.match(/^[1-9]\d+$/) ||
            !this.state.billingMobile.match(/^.{10}$/)) {
            this.setState({
                billingMobileerr: true
            });
        } else {
            this.setState({
                billingMobileerr: false
            });
        }
    }
    //shippingName

    shippingName() {
        if (this.state.shippingName == "" || !this.state.shippingName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                shippingNameerr: true
            });
        } else {
            this.setState({
                shippingNameerr: false
            });
        }

    }
    //shippingPhone
    shippingPhone() {

        if(this.state.shippingPhone == "" || this.state.shippingPhone.trim()==""){
            this.setState({
                shippingPhoneerr: false
            })
        }
        else if (this.state.shippingPhone.length < 10){
            this.setState({
                shippingPhoneerr: true
            });
        }
        else {
            this.setState({
                shippingPhoneerr: false
            });
        }
    }
    //billingEmail
    shippingEmail() {

        if (
            this.state.shippingEmail == "" ||
            !this.state.shippingEmail.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                shippingEmailerr: true
            });
        } else {
            this.setState({
                shippingEmailerr: false
            });
        }
    }
    //shippingMobile
    shippingMobile() {
        if (this.state.shippingMobile == "" || !this.state.shippingMobile.match(/^\d{10}$/) ||
            !this.state.shippingMobile.match(/^[1-9]\d+$/) ||
            !this.state.shippingMobile.match(/^.{10}$/)) {
            this.setState({
                shippingMobileerr: true
            });
        } else {
            this.setState({
                shippingMobileerr: false
            });
        }
    }
    //receiverName


    receiverName() {
        if (this.state.receiverName == "" || !this.state.receiverName.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                receiverNameerr: true
            });
        } else {
            this.setState({
                receiverNameerr: false
            });
        }

    }
    //receiverPhone
    receiverPhone() {
     
        if(this.state.receiverPhone == "" || this.state.receiverPhone.trim()==""){
            this.setState({
                receiverPhoneerr: false
            })
        }
        else if (this.state.receiverPhone.length < 10) {
            this.setState({
                receiverPhoneerr: true
            });
        }
        else {
            this.setState({
                receiverPhoneerr: false
            });
        }
    }
    //receiverEmail
    receiverEmail() {

        if (
            this.state.receiverEmail == "" ||
            !this.state.receiverEmail.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                receiverEmailerr: true
            });
        } else {
            this.setState({
                receiverEmailerr: false
            });
        }
    }
    //receiverMobile
    receiverMobile() {
        if (this.state.receiverMobile == "" || !this.state.receiverMobile.match(/^\d{10}$/) ||
            !this.state.receiverMobile.match(/^[1-9]\d+$/) ||
            !this.state.receiverMobile.match(/^.{10}$/)) {
            this.setState({
                receiverMobileerr: true
            });
        } else {
            this.setState({
                receiverMobileerr: false
            });
        }
    }

    //GSTIN
    gstin() {
        if(this.state.gstin == "") {
            this.setState({gstinerr: true});
        }
        else {
            this.setState({gstinerr: false});
        }
    }





    handleChange(e) {
        this.setState({
            btnDisable: false
        })
        if (e.target.id == "name") {
            this.setState(
                {
                    name: e.target.value
                },
                () => {
                    this.name();
                }
            );
        }
        else if (e.target.id == "code") {
            this.setState(
                {
                    code: e.target.value
                },
                () => {
                    this.code();
                }
            );
        }
        else if (e.target.id == "country") {
            this.setState(
                {
                    country: e.target.value
                },
                () => {
                    this.country();
                }
            );

        }
        else if (e.target.id == "state") {
            this.setState(
                {
                    state: e.target.value
                },
                () => {
                    this.statee();
                }
            );
            if( e.target.value!= ""){
                const t=this
                setTimeout(function(){

                    t.setState({
                        cityData:t.state.cityStateData[0].states[t.state.state]
                    })
                },100)
            }

        }
        else if (e.target.id == "city") {
            this.setState(
                {
                    city: e.target.value
                },
                () => {
                    this.city();
                }
            );

        } else if (e.target.id == "zipcode") {
            this.setState(
                {
                    zipcode: e.target.value
                },
                () => {
                    this.zipcode();
                }
            );




        } else if (e.target.id == "timezone") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    timezone: e.target.value
                },
                () => {
                    this.timezone();
                }
            );


        }


        else if (e.target.id == "displayName") {
            this.setState(
                {
                    displayName: e.target.value
                },
                () => {
                    this.displayName();
                }
            );

        } else if (e.target.id == "latitude") {
            this.setState(
                {
                    latitude: e.target.value
                },
                // () => {
                //     this.latitude();
                // }
            );




        } else if (e.target.id == "longitude") {
            this.setState(
                {
                    longitude: e.target.value
                },
                // () => {
                //     this.longitude();
                // }
            );


        }


        else if (e.target.id == "status") {
            this.setState(
                {
                    status: e.target.value
                },
                () => {
                    this.status();
                }
            );

        } else if (e.target.id == "type") {
            this.setState(
                {
                    type: e.target.value
                },
                () => {
                    this.type();
                }
            );




        } else if (e.target.id == "area") {
            this.setState(
                {
                    area: e.target.value
                },
                () => {
                    this.area();
                }
            );


        }


        else if (e.target.id == "unit") {
            this.setState(
                {
                    unit: e.target.value
                },
                () => {
                    this.unit();
                }
            );

        } else if (e.target.id == "numberOfFloors") {
            this.setState(
                {
                    numberOfFloors: e.target.value
                },
                () => {
                    this.numberOfFloors();
                }
            );




        } else if (e.target.id == "opStartDate") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    opStartDate: e.target.value
                },
                () => {
                    this.opStartDate();
                }
            );


        } else if (e.target.id == "opEndDate") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    opEndDate: e.target.value
                },
                () => {
                    // this.opEndDate();
                }
            );


        }

        else if (e.target.id == "isOnlineStore" || e.target.id == "isBilling" || e.target.id == "isShipping") {
            
            if(e.target.id == "isOnlineStore"){
                this.setState({
                    isOnlineStore: !this.state.isOnlineStore
                })
            }else if(e.target.id == "isBilling"){
                this.setState({
                    isBilling: !this.state.isBilling
                })
            }else if(e.target.id == "isShipping"){
                this.setState({
                    isShipping: !this.state.isShipping
                })
            }
            

        }

        else if (e.target.id == "contactName") {
            this.setState(
                {
                    contactName: e.target.value
                },
                () => {
                    this.contactName();
                }
            );

        } else if (e.target.id == "contactNumber") {
            this.setState(
                {
                    contactNumber: e.target.value
                },
                () => {
                    this.contactNumber();
                }
            );




        } else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );


        } else if (e.target.id == "mobile") {
            this.setState(
                {
                    mobile: e.target.value
                },
                () => {
                    this.mobile();
                }
            );


        }

        else if (e.target.id == "web") {
            this.setState(
                {
                    Web: e.target.value
                },
                () => {
                    this.Web();
                }
            );


        }

        else if (e.target.id == "address") {
            this.setState(
                {
                    Address: e.target.value
                },
                () => {
                    this.Address();
                }
            );


        } else if (e.target.id == "billingName") {
            this.setState(
                {
                    billingName: e.target.value
                },
                () => {
                    this.billingName();
                }
            );




        } else if (e.target.id == "billingPhone") {
            this.setState(
                {
                    billingPhone: e.target.value
                },
                () => {
                    this.billingPhone();
                }
            );


        } else if (e.target.id == "billingEmail") {
            this.setState(
                {
                    billingEmail: e.target.value
                },
                () => {
                    this.billingEmail();
                }
            );


        }

        else if (e.target.id == "billingMobile") {
            this.setState(
                {
                    billingMobile: e.target.value
                },
                () => {
                    this.billingMobile();
                }
            );



        } else if (e.target.id == "shippingName") {
            this.setState(
                {
                    shippingName: e.target.value
                },
                () => {
                    this.shippingName();
                }
            );




        } else if (e.target.id == "shippingPhone") {
            this.setState(
                {
                    shippingPhone: e.target.value
                },
                () => {
                    this.shippingPhone();
                }
            );


        } else if (e.target.id == "shippingEmail") {
            this.setState(
                {
                    shippingEmail: e.target.value
                },
                () => {
                    this.shippingEmail();
                }
            );


        }

        else if (e.target.id == "shippingMobile") {
            this.setState(
                {
                    shippingMobile: e.target.value
                },
                () => {
                    this.shippingMobile();
                }
            );


        }
        else if (e.target.id == "receiverName") {
            this.setState(
                {
                    receiverName: e.target.value
                },
                () => {
                    this.receiverName();
                }
            );




        } else if (e.target.id == "receiverPhone") {
            this.setState(
                {
                    receiverPhone: e.target.value
                },
                () => {
                    this.receiverPhone();
                }
            );


        } else if (e.target.id == "receiverEmail") {
            this.setState(
                {
                    receiverEmail: e.target.value
                },
                () => {
                    this.receiverEmail();
                }
            );


        }

        else if (e.target.id == "receiverMobile") {
            this.setState(
                {
                    receiverMobile: e.target.value
                },
                () => {
                    this.receiverMobile();
                }
            );


        }
        else if (e.target.id == "gstin") {
            this.setState({gstin: e.target.value}, () => this.gstin());
        }
        else if (e.target.id == "partner") {
            this.setState({partner: e.target.value});
        }
        else if (e.target.id == "zone") {
            this.setState({zone: e.target.value});
        }
        else if (e.target.id == "region") {
            this.setState({region: e.target.value});
        }
        else if (e.target.id == "groupName") {
            this.setState({groupName: e.target.value});
        }
        else if (e.target.id == "udf1") {
            this.setState({udf1: e.target.value});
        }
        else if (e.target.id == "udf2") {
            this.setState({udf2: e.target.value});
        }
        else if (e.target.id == "udf3") {
            this.setState({udf3: e.target.value});
        }
        else if (e.target.id == "udf4") {
            this.setState({udf4: e.target.value});
        }
        else if (e.target.id == "udf5") {
            this.setState({udf5: e.target.value});
        }
        else if (e.target.id == "additional") {
            this.setState({additional: e.target.value});
        }
    }



    contactSubmit(e) {
        e.preventDefault();
        this.name();
        this.code();
        this.country();
        this.statee();
        this.city();
        this.email();
        this.zipcode();
        this.timezone();
        this.displayName();
        // this.latitude();
        // this.longitude();
        // this.status();
        this.type();
        this.area();
        this.unit();
        this.numberOfFloors();
        this.opStartDate();
        // this.opEndDate();
        // this.contactName();
        // this.contactNumber();
        // this.email();
        // this.mobile();
        // this.billingMobile();
        // this.shippingName();
        // this.shippingPhone();
        // this.shippingEmail();
        // this.Web();
        this.Address();
        // this.billingName();
        // this.billingPhone();
        // this.billingEmail();
        // this.shippingMobile();
        // this.receiverName();
        // this.receiverPhone();
        // this.receiverEmail();
        // this.receiverMobile();
        this.gstin();

        const t = this;
        setTimeout(function () {
            
            const {/* contactId, receiverId, shippingId, billingId, */ id, nameerr, codeerr, countryerr, stateerr, cityerr, zipcodeerr, timezoneerr, displayNameerr, latittudeerr,
                longitudeeer, statuserr, typeerr, areaerr, uniterr, numberOfFloorserr, opStartDateerr, opEndDateerr, contactNameerr,
                contactNumbererr, emailerr, mobileerr, Websiteerr, Addresserr, billingNameerr, billingPhoneerr, billingEmailerr, billingMobileerr, shippingNameerr, shippingPhoneerr, shippingEmailerr, shippingMobileerr
                , receiverNameerr, receiverPhoneerr, receiverEmailerr, receiverMobileerr, gstinerr } = t.state;
            console.log(nameerr , !codeerr , countryerr , stateerr , cityerr , zipcodeerr , timezoneerr , displayNameerr,
                typeerr , areaerr , uniterr , numberOfFloorserr , opStartDateerr , Addresserr , gstinerr);
            if (!nameerr && !codeerr && !countryerr && !stateerr && !cityerr && !zipcodeerr && !timezoneerr && !displayNameerr &&
                    !areaerr && !uniterr && !numberOfFloorserr && !Addresserr && !gstinerr) {

                let siteData = {
                    // // shippingId: shippingId,
                    // // billingId: billingId,
                    // // receiverId: receiverId,
                    // // contactId: contactId,
                    // name: t.state.name,
                    // code: t.state.code,
                    // // id: t.state.id,
                    // country: t.state.country,
                    // gstin: t.state.gstin,
                    // partner: t.state.partner,
                    // state: t.state.state,
                    // city: t.state.city,
                    // zipCode: t.state.zipcode,
                    // timezone: t.state.timezone,
                    // displayName: t.state.displayName,
                    // latitude: t.state.latitude,
                    // longitude: t.state.longitude,
                    // //statusE: t.state.status,
                    // type: t.state.type,
                    // area: t.state.area,
                    // areaUnit: t.state.unit,
                    // numberFloors: t.state.numberOfFloors,
                    // optStoreSDate: t.state.opStartDate,
                    // optStoreEDate: t.state.opEndDate,
                    // partner: ,
                    // zone: ,
                    // region: ,
                    // groupName: ,

                    // type : t.props.type,
                    // // search: t.props.search,
                    // // no: t.props.no,
                    // siteName: t.props.name,
                    // code: t.props.code,
                    // displayName: t.props.displayName,
                    // state: t.props.state,
                    // country: t.props.country,
                    // zipCode: t.props.zipCode,
                    // status: t.props.status,
                    code: t.state.code,
                    name: t.state.name,
                    country: t.state.country,
                    gstin: t.state.gstin,
                    area: t.state.area,
                    areaUnit: t.state.unit,
                    city: t.state.city,
                    displayName: t.state.displayName,
                    state: t.state.state,
                    zipCode: t.state.zipcode,
                    partner: t.state.partner,
                    zone: t.state.zone,
                    region: t.state.region,
                    latitude: t.state.latitude,
                    longitude: t.state.longitude,
                    numberFloors: t.state.numberOfFloors,
                    optStoreSDate: t.state.opStartDate,
                    optStoreEDate: t.state.opEndDate,
                    address: t.state.Address,
                    groupName: t.state.groupName,
                    udf1: t.state.udf1,
                    udf2: t.state.udf2,
                    udf3: t.state.udf3,
                    udf4: t.state.udf4,
                    udf5: t.state.udf5,
                    timeZone: t.state.timezone,
                    type: t.state.type,
                    additional: t.state.additional
                }
                t.props.editSiteRequest(siteData);


            }
        }, 100)

    }



    componentWillMount() {
        console.log(this.props.administration.site.data.resource);
        var state=""
        for (let i = 0; i < this.props.administration.site.data.resource.length; i++) {

            if (this.props.administration.site.data.resource[i].id == this.props.id) {
                state =this.props.administration.site.data.resource[i].state
                this.setState({
                    id: this.props.administration.site.data.resource[i].id,
                    // contactId: this.props.administration.site.data.resource[i].contact.id,
                    // shippingId: this.props.administration.site.data.resource[i].shipping.id,
                    // billingId: this.props.administration.site.data.resource[i].billing.id,
                    // receiverId: this.props.administration.site.data.resource[i].receiver.id,
                    name: this.props.administration.site.data.resource[i].name,
                    code: this.props.administration.site.data.resource[i].code,
                    country: this.props.administration.site.data.resource[i].country,
                    state: this.props.administration.site.data.resource[i].state,
                    city: this.props.administration.site.data.resource[i].city,
                    zipcode: this.props.administration.site.data.resource[i].zipCode,
                    timezone: this.props.administration.site.data.resource[i].timeZone,
                    displayName: this.props.administration.site.data.resource[i].displayName,
                    latitude: this.props.administration.site.data.resource[i].latitude,
                    longitude: this.props.administration.site.data.resource[i].longitude,
                    status: this.props.administration.site.data.resource[i].status,
                    type: this.props.administration.site.data.resource[i].type,
                    area: this.props.administration.site.data.resource[i].area,
                    unit: this.props.administration.site.data.resource[i].areaUnit,
                    numberOfFloors: this.props.administration.site.data.resource[i].numberFloors,
                    opStartDate: this.props.administration.site.data.resource[i].optStoreSDate == null ? "" : this.props.administration.site.data.resource[i].optStoreSDate,
                    opEndDate: this.props.administration.site.data.resource[i].optStoreEDate == null ? "" : this.props.administration.site.data.resource[i].optStoreEDate,
                    // isShipping: this.props.administration.site.data.resource[i].isShipping  == "true" ? true : false,
                    // isOnlineStore: this.props.administration.site.data.resource[i].isOnlineStore  == "true" ? true : false,
                    // isBilling: this.props.administration.site.data.resource[i].isBilling == "true" ? true : false,
                    // contactName: this.props.administration.site.data.resource[i].contact.name,
                    // contactNumber: this.props.administration.site.data.resource[i].contact.phone,
                    // email: this.props.administration.site.data.resource[i].contact.email,
                    // mobile: this.props.administration.site.data.resource[i].contact.mobile,
                    // Web: this.props.administration.site.data.resource[i].contact.website,
                    Address: this.props.administration.site.data.resource[i].address,
                    // billingName: this.props.administration.site.data.resource[i].billing.name,
                    // billingPhone: this.props.administration.site.data.resource[i].billing.phone,
                    // billingEmail: this.props.administration.site.data.resource[i].billing.email,
                    // billingMobile: this.props.administration.site.data.resource[i].billing.mobile,
                    // shippingName: this.props.administration.site.data.resource[i].shipping.name,
                    // shippingPhone: this.props.administration.site.data.resource[i].shipping.phone,
                    // shippingEmail: this.props.administration.site.data.resource[i].shipping.email,
                    // shippingMobile: this.props.administration.site.data.resource[i].shipping.mobile,
                    // receiverName: this.props.administration.site.data.resource[i].receiver.name,
                    // receiverPhone: this.props.administration.site.data.resource[i].receiver.phone,
                    // receiverEmail: this.props.administration.site.data.resource[i].receiver.email,
                    // receiverMobile: this.props.administration.site.data.resource[i].receiver.mobile,
                    gstin: this.props.administration.site.data.resource[i].gstin,
                    partner: this.props.administration.site.data.resource[i].partner,
                    zone: this.props.administration.site.data.resource[i].zone,
                    region: this.props.administration.site.data.resource[i].region,
                    groupName: this.props.administration.site.data.resource[i].groupName,
                    udf1: this.props.administration.site.data.resource[i].udf1,
                    udf2: this.props.administration.site.data.resource[i].udf2,
                    udf3: this.props.administration.site.data.resource[i].udf3,
                    udf4: this.props.administration.site.data.resource[i].udf4,
                    udf5: this.props.administration.site.data.resource[i].udf5,
                    additional: this.props.administration.site.data.resource[i].additional,
                })
            }
        }
        var cityData = this.state.cityStateData[0].states;
 
        this.setState({
            stateData: Object.keys(cityData),
            cityData:this.state.cityStateData[0].states[state]
        })
    }



    componentWillReceiveProps(nextProps){
        if(nextProps.administration.editSite.isSuccess){
      
           this.props.onSiteEdit()
       }
   }
    render() {

        $("body").on("keyup",".numbersOnly",function(){
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
               this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        $("body").on("keyup", ".numbersOnlywithhighpn", function () {
            if (this.value != this.value.replace(/[^0-9-]/g, '')) {
                this.value = this.value.replace(/[^0-9-]/g, '');
            }
        });
        const
        { name,
            nameerr,
            code,
            codeerr,
            country,
            countryerr,
            state,
            stateerr,
            city,
            cityerr,
            // email, 
            // emailerr, 
            zipcode,
            zipcodeerr,
            timezone,
            timezoneerr,
            displayName,
            displayNameerr,
            latitude,
            latittudeerr,
            longitude,
            longitudeeer,
            status,
            statuserr,
            type,
            typeerr,
            area,
            areaerr,
            unit,
            uniterr,
            numberOfFloors,
            numberOfFloorserr,
            opStartDate,
            opStartDateerr,
            opEndDate,
            opEndDateerr,
            isBilling,
            isShipping,
            isOnlineStore,
            contactName,
                contactNameerr,
                contactNumber,
                contactNumbererr,
                email,
                emailerr,
                mobile,
                mobileerr,
                Web,
                Websiteerr,
                Address,
                Addresserr,
                billingName,
                billingNameerr,
                billingPhone,
                billingPhoneerr,
                billingEmail,
                billingEmailerr,
                billingMobile,
                billingMobileerr,
                shippingName,
                shippingNameerr,
                shippingPhone,
                shippingPhoneerr,
                shippingEmail,
                shippingEmailerr,
                shippingMobile,
                shippingMobileerr,
                receiverName,
                receiverNameerr,
                receiverPhone,
                receiverPhoneerr,
                receiverEmail,
                receiverEmailerr,
                receiverMobile,
                receiverMobileerr,

                gstin,
                gstinerr,
                partner,
                zone,
                region,
                groupName,
                udf1,
                udf2,
                udf3,
                udf4,
                udf5,
                additional
                
            } = this.state;
            return (
                <div className="modal">
                    <div className="backdrop modal-backdrop-new"></div>
                    <div className="modal-content master-data-view-modal">
                        <form name="siteForm" onSubmit={(e) => this.contactSubmit((e)) } >
                            <div className="mdvm-head">
                                <div className="mdvmh-left">
                                    <h3>Manage Site</h3>
                                </div>
                                <div className="mdvmh-right">
                                    {this.state.btnDisable ? <button className="mdvmh-enable btnDisabled" type="button" disabled>Save</button> : <button className="mdvmh-enable" type="submit">Save</button>}
                                    <button type="button" onClick={(e)=>this.props.onSiteEdit()}>Cancel</button>
                                </div>
                            </div>
                            <div className="mdvm-body">
                                <div className="col-md-12 m-top-10">
                                    <h3>Basic Details</h3>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Site Code</label>
                                        <input className={codeerr ? "InputBoxEdit errorBorder":"InputBoxEdit"} type="text" id="code" name="code" value={code} onChange={e => this.handleChange(e)} />
                                        {codeerr ? (<span className="error">Enter  Code</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Site Name</label>
                                        <input type="text" className={nameerr ? "errorBorder InputBoxEdit":"InputBoxEdit"} name="name" id="name" value={name} onChange={e => this.handleChange(e)} placeholder="Name" />
                                        {nameerr ? (<span className="error">Enter  name</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Display Name</label>
                                        <input type="text" className={displayNameerr ? "InputBoxEdit errorBorder" : "InputBoxEdit"} name="displayName" id="displayName" value={displayName} placeholder="Display Name" onChange={e => this.handleChange(e)} />
                                        {displayNameerr ? (<span className="error">Enter Display Name</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Country</label>
                                        <select className={countryerr ? "errorBorder":""} id="country" name="country" value={country} onChange={(e) => this.handleChange(e)}>
                                            <option value="" >Country</option>
                                            <option value="India">India</option>
                                        </select>
                                        {countryerr ? (<span className="error">Select Country</span>) : null}
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>State</label>
                                        <select className={stateerr ? "errorBorder":""} id="state" name="state" value={state} onChange={e => this.handleChange(e)}>
                                        <option value="">State</option>
                                        {this.state.stateData.map((data, i) =>  <option value={data} key={i}>{data}</option>
                                        )}
                                        </select>
                                        {stateerr ? (<span className="error">Select State</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>City</label>
                                        <select className={cityerr ? "errorBorder":""} id="city" name="city" value={city} onChange={e => this.handleChange(e)}>
                                            <option value="">city</option>
                                        {this.state.cityData != undefined ? this.state.cityData.map((data,i)=>
                                            <option value={data} key={i}>{data}</option>):null}
                                        </select>
                                        {cityerr ? (<span className="error">Enter City</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Zipcode</label>
                                        <input type="text" className={zipcodeerr ? "InputBoxEdit numbersOnly errorBorder":"InputBoxEdit numbersOnly"} id="zipcode" name="zipcode" maxLength="6" value={zipcode} placeholder="Zipcode" onChange={e => this.handleChange(e)} />
                                        {zipcodeerr ? (<span className="error">Enter valid Zipcode</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>GSTIN</label>
                                        <input type="text" className={gstinerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} id="gstin" name="gstin" value={gstin} onChange={e => this.handleChange(e)} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-12 col-sm-12 col-xs-12">
                                        <label>Address</label>
                                        <textarea className={Addresserr ? "textAreaEditModal errorBorder":"textAreaEditModal"} rows="5" id="address" value={Address} placeholder="Address" onChange={e => this.handleChange(e)}>
                                        </textarea>
                                        {Addresserr ? (<span className="error" >Enter Address</span>) : null}
                                    </div>
                                </div>
                                <div className="col-md-12 headModal m-top-30">
                                    <h3>Additional Details</h3>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Partner</label>
                                        <input type="text" className="InputBoxEdit" id="partner" name="partner" value={partner} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Zone</label>
                                        <input type="text" className="InputBoxEdit" id="zone" name="zone" value={zone} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Region</label>
                                        <input type="text" className="InputBoxEdit" id="region" name="region" value={region} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Group</label>
                                        <input type="text" className="InputBoxEdit" id="groupName" name="groupName" value={groupName} onChange={e => this.handleChange(e)} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Latitude</label>
                                        <input type="text" className={latittudeerr ? "InputBoxEdit errorBorder":"InputBoxEdit"} id="latitude" name="latitude" value={latitude} placeholder="Latitude" onChange={e => this.handleChange(e)} />
                                        {latittudeerr ? (<span className="error">Enter Latitude</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Longitude</label>
                                        <input type="text" className={longitudeeer ? "InputBoxEdit errorBorder":"InputBoxEdit"} id="longitude" name="longitude" value={longitude} placeholder="Longitude" onChange={e => this.handleChange(e)} />
                                        {longitudeeer ? (<span className="error">Enter Longitude</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Site Operation Start Date</label>
                                        <input type="date" className="purchaseOrderTextbox" value={opStartDate} id="opStartDate" name="opStartDate" onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Site Operation End Date</label>
                                        <input type="date" className="purchaseOrderTextbox" value={opEndDate} id="opEndDate" name="opEndDate" onChange={e => this.handleChange(e)} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Additional</label>
                                        <input type="text" className="InputBoxEdit" id="additional" name="additional" value={additional} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Area</label>
                                        <input type="text" className={areaerr ? "InputBoxEdit errorBorder numbersOnly":"InputBoxEdit numbersOnly"} id="area" name="area" value={area} placeholder="Area Coverage" onChange={e => this.handleChange(e)} />
                                        {areaerr ? (<span className="error">Enter Area Coverage</span>) : null}
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Area Unit</label>
                                        <select  className={uniterr ? "errorBorder":""} id="unit" name="unit" value={unit} placeholder="Area Unit" onChange={e => this.handleChange(e)} >
                                        <option value="">Select Area unit</option>
                                        <option value="sq/yd">sq/yd</option>
                                        <option value="sq/mtr">sq/mtr</option>
                                        <option value="sq/ft">sq/ft</option>
                                        <option value="Acre">Acre</option>
                                        <option value="Hectare">Hectare</option>
                                        <option value="Decare">Decare</option>
                                        <option value="sq/km">sq/km</option>
                                        <option value="sq/cm">sq/cm</option>
                                        <option value="sq/mm">sq/mm</option>  
                                        <option value="sq/in">sq/in</option>
                                        {uniterr ? (<span className="error">Enter Area Unit</span>) : null}
                                        </select>
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>Number Of Floor</label>
                                        <input type="text" className={numberOfFloorserr ? "InputBoxEdit numbersOnly errorBorder":"InputBoxEdit numbersOnly"}  maxLength ="2" id="numberOfFloors" name="numberOfFloors" value={numberOfFloors} placeholder="Number of Floors" onChange={e => this.handleChange(e)} />
                                        {numberOfFloorserr ? (<span className="error">Enter Number of Floors</span>) : null}
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>TimeZone</label>
                                        <select  className= {timezoneerr ? "errorBorder":""} placeholder={this.state.timezone} id="timezone" name="timezone" value={timezone} placeholder={timezone} onChange={e => this.handleChange(e)} >
                                        <option value="">Select Timezone</option>
                                        <option value="(GMT +5:30) Bombay, Calcutta, Madras, New Delhi">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                        {timezoneerr ? (<span className="error">Enter Timezone</span>) : null}
                                        </select>
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>UDF1</label>
                                        <input type="text" className="InputBoxEdit" id="udf1" name="udf1" value={udf1} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>UDF2</label>
                                        <input type="text" className="InputBoxEdit" id="udf2" name="udf2" value={udf2} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>UDF3</label>
                                        <input type="text" className="InputBoxEdit" id="udf3" name="udf3" value={udf3} onChange={e => this.handleChange(e)} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>UDF4</label>
                                        <input type="text" className="InputBoxEdit" id="udf4" name="udf4" value={udf4} onChange={e => this.handleChange(e)} />
                                    </div>
                                    <div className="col-md-3 col-sm-2 col-xs-3">
                                        <label>UDF5</label>
                                        <input type="text" className="InputBoxEdit" id="udf5" name="udf5" value={udf5} onChange={e => this.handleChange(e)} />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        )
    }
}

export default SiteEditModal;