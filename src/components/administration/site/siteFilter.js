import React from 'react';
import Json from "../jsonFile.json";
class SiteFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            siteName: this.props.siteName,
            code: this.props.code,
            displayName: this.props.displayName,
            country: this.props.country,
            state: this.props.state,
            zipCode: this.props.zipCode,
            status: this.props.status,
            cityStateData: Json,
            stateData: [],
            cityData:[]
        }
    }

    clearFilter(e) {
        this.setState({
            siteName: "",
            code: "",
            displayName: "",
            country: "",
            state: "",
            zipCode: "",
            status: ""
        })
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            siteName: nextProps.siteName,
            code: nextProps.code,
            displayName: nextProps.displayName,
            country: nextProps.country,
            state: nextProps.state,
            zipCode: nextProps.zipCode,
            status: nextProps.status,
        })
    }

    
    componentWillMount(){
        var cityData = this.state.cityStateData[0].states;
 
            this.setState({
                stateData: Object.keys(cityData),
                // cityData: this.state.cityStateData[0].states
            })
            
      
        
    }
    handleChange(e) {
        if (e.target.id == "siteName") {
            this.setState({
                siteName: e.target.value
            });
        } else if (e.target.id == "code") {
            this.setState({
                code: e.target.value
            });
        } else if (e.target.id == "state") {
            this.setState({
                state: e.target.value
            });
            if( e.target.value!= ""){
                const t=this
                setTimeout(function(){

                    t.setState({
                        cityData:t.state.cityStateData[0].states[t.state.state]
                    })
                },100)
            }
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        } else if (e.target.id == "country") {
            this.setState({
                country: e.target.value
            });
        } else if (e.target.id == "displayName") {
            this.setState({
                displayName: e.target.value
            });
        } else if (e.target.id == "zipCode") {
            this.setState({
                zipCode: e.target.value
            });
        }
    }
    onSubmit(e) {
        e.preventDefault();
        let data = {
            no: 1,
            type : 2,
            search: this.state.search,
            siteName: this.state.siteName,
            code: this.state.code,
            displayName: this.state.displayName,
            state: this.state.state,
            country: this.state.country,
            zipCode: this.state.zipCode,
            status: this.state.status,
        }
        this.props.siteRequest(data);
        this.props.closeFilter(e);
        this.props.updateFilter(data)
    }
    render() {
    
        let count = 0;
        if(this.state.siteName != ""){
            count ++;
        }
        if(this.state.code != ""){
            count ++;
        }
        if(this.state.displayName != ""){
            count ++;
        }
        if(this.state.status != ""){
            count ++;
        }
        if(this.state.country != ""){
            count ++;
        }
        if(this.state.zipCode != ""){
            count ++;
        }
        if(this.state.state != ""){
            count ++;
        }
        return (
            <div>
                <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myModal">
                    <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                            <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>

                            <div className="col-md-12 col-sm-12 pad-0">
                                <ul className="list-inline">
                                    <li>
                                        <label className="filter_modal">
                                            FILTERS

                     </label>
                                    </li>
                                    <li>
                                        <label className="filter_text">
                                            {count} Filters applied
                     </label>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="container_modal">

                                    <ul className="list-inline m-top-20">

                                        <li>
                                            <input type="text" onChange={(e) => this.handleChange(e)} id="siteName" value={this.state.siteName} placeholder="Site Name" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={(e) => this.handleChange(e)} value={this.state.code} id="code" placeholder="Code" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={(e) => this.handleChange(e)} value={this.state.displayName} id="displayName" placeholder="Display Name" className="organistionFilterModal" />
                                        </li>

                                        <li>
                                            <select id="country" onChange={(e) => this.handleChange(e)} value={this.state.country} className="RoleFilterSelect filterBtnWidth">
                                                <option value="">
                                                    Country
                                       </option>
                                                <option value="india">India</option>
                                            </select>
                                        </li>
                                        <li>
                                            <select id="state" onChange={(e) => this.handleChange(e)} value={this.state.state} className="RoleFilterSelect filterBtnWidth">
                                                <option value="">
                                                    State
                                       </option>
                                       {this.state.stateData.map((data, i) =>  <option value={data} key={i}>{data}</option>
                                            
                                            )}
                                            </select>
                                        </li>
                                        {/* <li>
                                            <input type="text" id="zipCode" onChange={(e) => this.handleChange(e)} value={this.state.zipCode} placeholder="Zip Code" className="organistionFilterModal" />
                                        </li> */}
                                        <li>
                                            <select id="status" onChange={(e) => this.handleChange(e)} value={this.state.status} className="RoleFilterSelect filterBtnWidth">
                                                <option value="">
                                                    Status
                                       </option>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="col-md-6 float_right pad-0 m-top-20">
                                    <ul className="list-inline text_align_right">
                                        <li>
                                            {this.state.siteName == "" && this.state.code == "" && this.state.displayName == "" && this.state.status == "" && this.state.country == "" && this.state.zipCode == "" && this.state.state == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            :<button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                                CLEAR FILTER
                                            </button>}
                                        </li>
                                        <li>
                                        {this.state.siteName != "" || this.state.code != "" || this.state.displayName != "" || this.state.status != "" || this.state.country != "" || this.state.zipCode!="" || this.state.state !="" ?  <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button>: <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                            APPLY
                                        </button>}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default SiteFilter;