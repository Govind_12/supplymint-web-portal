import React from 'react';
class CreateNewFestivalModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameNew: "",
            dateNew: "",
            dateNewerr: false,
            nameNewerr: false,
            minDate: this.props.yearValue + '-' + '04-01'
        }
    }

    handleChangeNew(e) {
        if (e.target.id == "nameNew") {
            this.setState({
                nameNew: e.target.value
            }, () => {
                this.nameNewError()
            })
        } else if (e.target.id == "dateNew") {
            this.setState({
                dateNew: e.target.value
            }, () => {
                this.dateNewError()
            })
        }
    }

    dateNewError() {
        if (this.state.dateNew == "") {
            this.setState({
                dateNewerr: true
            })
        } else {
            this.setState({
                dateNewerr: false
            })
        }
    }
    nameNewError() {
        if (this.state.nameNew == "") {
            this.setState({
                nameNewerr: true
            })
        } else {
            this.setState({
                nameNewerr: false
            })
        }
    }

    createNewFestival(e) {
        this.dateNewError();
        this.nameNewError();
        setTimeout(() => {
            const { nameNewerr, dateNewerr } = this.state;
            if (!nameNewerr && !dateNewerr) {
                let lastArrayChecked = this.props.lastArrayChecked
                let customFest = this.props.customFest
                let dataArray = []
                let dataConcat = []
                let customAdded = []
                let data = {
                    festivalName: this.state.nameNew,
                    festivalDate: this.state.dateNew,
                    isChecked: "false",
                    afterImpact: {
                        startDate: "",
                        endDate: ""
                    },
                    beforeImpact: {
                        startDate: "",
                        endDate: ""
                    },
                    festivalType: "custom"
                }
                dataArray.push(data)

                if (customFest.length != 0) {
                    for (let j = 0; j < customFest.length; j++) {
                        let data = {
                            festivalName: customFest[j].festivalName,
                            festivalDate: customFest[j].festivalDate,
                            afterImpact: {
                                startDate: customFest[j].afterImpact.startDate,
                                endDate: customFest[j].afterImpact.endDate
                            },
                            beforeImpact: {
                                startDate: customFest[j].beforeImpact.startDate,
                                endDate: customFest[j].beforeImpact.endDate
                            },
                            isChecked: customFest[j].isChecked,
                            festivalType: customFest[j].festivalType
                        }
                        customAdded.push(data)

                        dataConcat = customAdded.concat(dataArray)
                    }
                }
                let payload = {
                    lastAddFestival: data,
                    customAddFestival: this.props.customFest == "" ? dataArray : dataConcat,
                    updateFestival: lastArrayChecked.length == 0 ? null : lastArrayChecked,
                    fiscalYear: this.props.financialYear
                }

                this.props.createFestivalRequest(payload)
                this.setState({
                    configuredName: [],
                    lastFestivalArray: [],
                    lastArrayChecked: [],
                    lastAddedArray: [],
                    customTrue: [],
                    falseState: []

                })
                this.props.closeModal()

            }
        }, 10)
    }

    render() {
        const { nameNewerr, dateNewerr } = this.state
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>

                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal createNewFestival">
                        <h3>Create New Festival</h3>
                        <div className="modalAction m-top-30">
                            <input type="text" className="m-top-15" id="nameNew" value={this.state.nameNew} placeholder="Enter Festival Title" onChange={(e) => this.handleChangeNew(e)} autoComplete="off" />
                            {nameNewerr ? <span className="error">
                                    select festival name
                            </span> : null}
                            <input type="date" className="m-top-15" id="dateNew" min={this.state.minDate} placeholder={this.state.dateNew == "" ? "Festival Date" : this.state.dateNew} onChange={(e) => this.handleChangeNew(e)} />
                            {dateNewerr ? <span className="error">
                                select festival date
                            </span> : null}
                        </div>
                        <div className="footer">
                            <button type="button" className="cancel" onClick={(e) => this.props.closeModal(e)} >Cancel</button>
                            <button type="button" className="done" onClick={(e) => this.createNewFestival(e)}>Save</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default CreateNewFestivalModal;