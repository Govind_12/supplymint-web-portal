import React from 'react';
import editIcon from '../../../assets/edit-2.svg';
import delIcon from '../../../assets/new-delete.svg';
import closeSearch from "../../../assets/close-recently.svg";
import PromotionalEventModal from './promotionalEventModal';
import FilterLoader from '../../loaders/filterLoader';
import RequestSuccess from '../../loaders/requestSuccess';
import RequestError from '../../loaders/requestError';
import ConfirmationModalFestival from './confirmationModalFestival';
import addEvent from "../../../assets/add-event.svg";
import noEvent from "../../../assets/noEvents.svg";
import moment from 'moment';
export default class PromotionalEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createEvent: false,
            loader: false,
            errorMessage: "",
            errorCode: "",
            success: false,
            successMessage: "",
            alert: false,
            code: "",
            PromotionalEvent: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            no: 1,
            idRow: "",
            name: "",
            resetAndDelete: false,
            headerMsg: "",
            paraMsg: "",
            type: 1,
            search: ""
        }
    }
    manageModel(e, id) {
        if (e == "open") {
            this.setState({
                createEvent: true,
                tableEditId: id,
            })
        } else if (e == "close") {
            this.setState({
                createEvent: false
            })
        }
    }
    componentDidMount() {
        let data = {
            no: 1,
            search: "",
            type: 1,
        }
        this.props.getPromotionalEventRequest(data)
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.administration.promotional.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.promotional.data.message,
                loader: false,
                success: true,
            })
            let data = {
                no: 1,
                search: this.state.search,
                type: 1,
            }
            this.props.getPromotionalEventRequest(data)
            this.props.promotionalClear()
        } else if (nextProps.administration.promotional.isError) {
            this.setState({
                errorMessage: nextProps.administration.promotional.message.error == undefined ? undefined : nextProps.administration.promotional.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.promotional.message.status,
                errorCode: nextProps.administration.promotional.message.error == undefined ? undefined : nextProps.administration.promotional.message.error.errorCode
            })
            this.props.promotionalClear();
        }
        if (nextProps.administration.updatePromotionalEvent.isSuccess) {
            if (nextProps.administration.updatePromotionalEvent.data.resource != null) {
                this.setState({
                    successMessage: nextProps.administration.updatePromotionalEvent.data.message,
                    loader: false,
                    success: true,
                })
            }
            let data = {
                no: 1,
                search: this.state.search,
                type: 1,
            }
            this.props.getPromotionalEventRequest(data)
            this.props.updatePromotionalEventClear()
        } else if (nextProps.administration.updatePromotionalEvent.isError) {
            this.setState({
                errorMessage: nextProps.administration.updatePromotionalEvent.message.error == undefined ? undefined : nextProps.administration.updatePromotionalEvent.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.updatePromotionalEvent.message.status,
                errorCode: nextProps.administration.updatePromotionalEvent.message.error == undefined ? undefined : nextProps.administration.updatePromotionalEvent.message.error.errorCode
            })
            this.props.updatePromotionalEventClear();
        }
        if (nextProps.administration.getPromotionalEvent.isSuccess) {
            if (nextProps.administration.getPromotionalEvent.data.resource != null) {
                this.setState({
                    loader: false,
                    PromotionalEvent: nextProps.administration.getPromotionalEvent.data.resource,
                    prev: nextProps.administration.getPromotionalEvent.data.prePage,
                    current: nextProps.administration.getPromotionalEvent.data.currPage,
                    next: nextProps.administration.getPromotionalEvent.data.currPage + 1,
                    maxPage: nextProps.administration.getPromotionalEvent.data.maxPage,
                })
            } else {
                this.setState({
                    loader: false,
                    PromotionalEvent: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            this.props.getPromotionalEventClear();
        } else if (nextProps.administration.getPromotionalEvent.isError) {
            this.setState({
                errorMessage: nextProps.administration.getPromotionalEvent.message.error == undefined ? undefined : nextProps.administration.getPromotionalEvent.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.getPromotionalEvent.message.status,
                errorCode: nextProps.administration.getPromotionalEvent.message.error == undefined ? undefined : nextProps.administration.getPromotionalEvent.message.error.errorCode
            })
            this.props.getPromotionalEventClear();
        }

        if (nextProps.administration.disablePromotionalEvent.isSuccess) {
        

            this.setState({
                successMessage: nextProps.administration.disablePromotionalEvent.data.message,
                loader: false,
                success: true,
            })
            let data = {
                no: 1,
                search: this.state.search,
                type: 1
            }
            this.props.getPromotionalEventRequest(data)
            this.props.disablePromotionalEventClear();
        } else if (nextProps.administration.disablePromotionalEvent.isError) {
            this.setState({
                errorMessage: nextProps.administration.disablePromotionalEvent.message.error == undefined ? undefined : nextProps.administration.disablePromotionalEvent.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.disablePromotionalEvent.message.status,
                errorCode: nextProps.administration.disablePromotionalEvent.message.error == undefined ? undefined : nextProps.administration.disablePromotionalEvent.message.error.errorCode
            })
            this.props.disablePromotionalEventClear();
        }

        if (nextProps.administration.deactivePromotionalEvent.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.deactivePromotionalEvent.data.message,
                loader: false,
                success: true,
            })

            let data = {
                no: 1,
                search: this.state.search,
                type: 1,
            }
            this.props.getPromotionalEventRequest(data)
            this.props.deactivePromotionalEventClear();
        } else if (nextProps.administration.deactivePromotionalEvent.isError) {
            this.setState({
                errorMessage: nextProps.administration.deactivePromotionalEvent.message.error == undefined ? undefined : nextProps.administration.deactivePromotionalEvent.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.deactivePromotionalEvent.message.status,
                errorCode: nextProps.administration.deactivePromotionalEvent.message.error == undefined ? undefined : nextProps.administration.deactivePromotionalEvent.message.error.errorCode
            })
            this.props.deactivePromotionalEventClear();
        }
        if (nextProps.administration.promotional.isLoading || nextProps.administration.getPromotionalEvent.isLoading
            || nextProps.administration.updatePromotionalEvent.isLoading || nextProps.administration.deactivePromotionalEvent.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.administration.getPromotionalEvent.data.prePage,
                current: this.props.administration.getPromotionalEvent.data.currPage,
                next: this.props.administration.getPromotionalEvent.data.currPage + 1,
                maxPage: this.props.administration.getPromotionalEvent.data.maxPage,
            })
            if (this.props.administration.getPromotionalEvent.data.currPage != 0) {
                let data = {
                    no: this.props.administration.getPromotionalEvent.data.currPage - 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.getPromotionalEventRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.getPromotionalEvent.data.prePage,
                current: this.props.administration.getPromotionalEvent.data.currPage,
                next: this.props.administration.getPromotionalEvent.data.currPage + 1,
                maxPage: this.props.administration.getPromotionalEvent.data.maxPage,
            })
            if (this.props.administration.getPromotionalEvent.data.currPage != this.props.administration.getPromotionalEvent.data.maxPage) {

                let data = {
                    no: this.props.administration.getPromotionalEvent.data.currPage + 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.getPromotionalEventRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.administration.getPromotionalEvent.data.prePage,
                current: this.props.administration.getPromotionalEvent.data.currPage,
                next: this.props.administration.getPromotionalEvent.data.currPage + 1,
                maxPage: this.props.administration.getPromotionalEvent.data.maxPage,
            })
            if (this.props.administration.getPromotionalEvent.data.currPage <= this.props.administration.getPromotionalEvent.data.maxPage) {
                let data = {
                    no: 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.getPromotionalEventRequest(data)
            }

        }
    }

    deleteRowConfirmation(id) {
        this.setState({
            resetAndDelete: true,
            headerMsg: "Are you sure to delete promotional event?",
            paraMsg: "Click confirm to continue.",
            name: "",
            idRow: id
        })
    }
    closeResetDeleteModal() {
        this.setState({
            resetAndDelete: !this.state.resetAndDelete,
        })
    }

    deleteRow(id) {
        let PromotionalEvent = this.state.PromotionalEvent
        for (let i = 0; i < PromotionalEvent.length; i++) {
            if (PromotionalEvent[i].id == id) {
                let data = {
                    id: PromotionalEvent[i].id,
                    userName: sessionStorage.getItem('userName')
                }
                this.props.deactivePromotionalEventRequest(data)
            }
        }
        this.setState({
            PromotionalEvent
        })

    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
          document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    enableDisable(id) {
        let PromotionalEvent = this.state.PromotionalEvent;
        for (let i = 0; i < PromotionalEvent.length; i++) {
            if (PromotionalEvent[i].id == id) {
                PromotionalEvent[i].isEnable = PromotionalEvent[i].isEnable == "TRUE" ? "FALSE" : "TRUE";
                let data = {
                    id: PromotionalEvent[i].id,
                    isEnable: PromotionalEvent[i].isEnable,
                }
                this.props.disablePromotionalEventRequest(data)
            }
        }
    }
    handleChange(e) {
        if (e.target.id == "search") {
            this.setState({
                search: e.target.value
            })
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    onSearch() {
        this.setState({
            type: 3,
        })
        let data = {
            type: 3,
            search: this.state.search,
            no: 1
        }
        this.props.getPromotionalEventRequest(data)
    }

    onClearSearch() {
        this.setState({
            search: "",
            type: 1,
            no: 1,
        })
        var data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.getPromotionalEventRequest(data)
    }

    render() {
        
        return (
            <div>
                <div className="container_div container-fluid" id="">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <div className="container_content heightAuto promotionalEvents pad-0">
                            <div className="colHeader">
                                <h4>Promotional Events</h4>
                            </div>
                            <div className="pageContent displayInline width100">

                                { this.state.PromotionalEvent.length == 0 && this.state.type == 1? <div className="col-md-12">
                                    <div className="col-md-2"></div>
                                    <div className="col-md-8">
                                        <div className="currentlyNoEvent">
                                            <div className="modalImg">
                                                <button type="button" className="marginAuto" onClick={() => this.manageModel("open")}><img className="width-17" src={addEvent} />Create New Event</button>
                                            </div>
                                            <div className="noEventFound textCenter">
                                                <h5 className="displayInline"><img src={noEvent} />No Events are available </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div> : <div>
                                        <div className="promotionalHeader modalImg lineAfter displayInline Width100 alignMiddle">
                                            <div className="col-md-6">
                                                <button type="button" onClick={() => this.manageModel("open")}><img className="width-17" src={addEvent} />Create New Event</button>
                                            </div>
                                            <div className="col-md-6 col-sm-6 pad-0 chooseDataModal" >
                                                <ul className="list-inline width50 float_Right">
                                                    <li className="textRight width100">
                                                        <div className="newSearch">
                                                            <input type="search" id="search" onKeyPress={this._handleKeyPress} value={this.state.search} onChange={(e) => this.handleChange(e)} className="search-box width100 " placeholder="Search..." autoComplete="off" />
                                                            {this.state.type == 3 ? <span onClick={() => this.onClearSearch()} className="closeSearch">
                                                                <img src={closeSearch} />
                                                            </span> : null}
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="promotionalEventTable m-top-20">
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3">
                                                <div className="Zui-wrapper">
                                                    <div className="scrollableOrgansation zui-scrollerHistory table-scroll scrollableTableFixed heightAuto">
                                                        <table className="table scrollTable zui-table UserManageTable manageUserTable border-bot-table">
                                                            <thead>
                                                                <tr>
                                                                    <th className="fixed-side-user fixed-side1">
                                                                        <label className="width65 pad-lft-5">Action</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>Event Title</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>Start Date</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>End Date</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>Description</label>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {this.state.PromotionalEvent.length != 0 ? this.state.PromotionalEvent.map((data, key) => (
                                                                    <tr key={key} id={data.id}>
                                                                        <td className="fixed-side-user fixed-side1 ">
                                                                            <ul className="list-inline m-0 float_None alignMiddle">
                                                                                <li>
                                                                                    <button className="edit_button heightAuto" onClick={() => this.manageModel('open', data.id)}><img src={editIcon} /></button>
                                                                                    <button className="edit_button heightAuto" onClick={() => this.deleteRowConfirmation(data.id)}><img src={delIcon} /></button>
                                                                                </li>
                                                                                <li className={data.isEnable == "TRUE" ? "addToggleSwitch mainToggle" : "mainToggle"}>

                                                                                    <label className="switchToggle">
                                                                                        <input onClick={(e) => this.enableDisable(`${data.id}`)} type="checkbox" id="myDiv" />
                                                                                        <span className="sliderToggle round">
                                                                                        </span>
                                                                                    </label>
                                                                                    <p className="onActive">Enable</p>
                                                                                    <p className="onInActive">Disable</p>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td><label>{data.name}</label></td>
                                                                        <td><label>  {moment(data.startDate).format("YYYY-MM-DD")} </label></td>
                                                                        <td><label>  {moment(data.endDate).format("YYYY-MM-DD")}</label></td>
                                                                        <td><label className="maxwidth355"> {data.description} </label></td>
                                                                    </tr>)) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                            <div className="pagerDiv">
                                                <ul className="list-inline pagination ">
                                                    {this.state.current == 1 || this.state.current == 0 ? <li >
                                                        <button className="PageFirstBtn pointerNone" type="button">
                                                            First
                  </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn " type="button" onClick={(e) => this.page(e)} id="first" >
                                                                First
                  </button>
                                                        </li>}
                                                    {this.state.prev != 0 ? <li >
                                                        <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                            Prev
                  </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn" disabled>
                                                                Prev
                  </button>
                                                        </li>}
                                                    <li>
                                                        <button className="PageFirstBtn pointerNone" type="button">
                                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                                        </button>
                                                    </li>
                                                    {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                        <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                            Next
                  </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                Next
                  </button>
                                                        </li> : <li >
                                                            <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                Next
                  </button>
                                                        </li>}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>}
                            </div>
                        </div>

                    </div>
                </div>
                {this.state.createEvent ? <PromotionalEventModal {...this.state} {...this.props} manageModel={(e) => this.manageModel(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.resetAndDelete ? <ConfirmationModalFestival {...this.props} {...this.state} deleteRow={(e) => this.deleteRow(e)} closeResetDeleteModal={(e) => this.closeResetDeleteModal(e)} /> : null}

            </div>
        )
    }
}