import React from 'react';
class PromotionalEventModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventName: "",
            startDate: "",
            endDate: "",
            description: "",
            minDate: "",
            maxDate: "",
            startDaterr: false,
            endDaterr: false,
            eventNamerr: false,
            disableBtn:false
        }
    }

    componentWillMount() {
        if (this.props.administration.getPromotionalEvent.data.resource != null) {
            for (let i = 0; i < this.props.administration.getPromotionalEvent.data.resource.length; i++) {
                if (this.props.administration.getPromotionalEvent.data.resource[i].id == this.props.tableEditId) {
                    this.setState({
                        eventName: this.props.administration.getPromotionalEvent.data.resource[i].name,
                        startDate: this.props.administration.getPromotionalEvent.data.resource[i].startDate,
                        endDate: this.props.administration.getPromotionalEvent.data.resource[i].endDate,
                        description: this.props.administration.getPromotionalEvent.data.resource[i].description,

                    });

                }
            }
        }
    }

    handleChange(e) {
        this.setState({
            disableBtn:true
        })
        if (e.target.id == "eventName") {
            this.setState({
                eventName: e.target.value
            }, () => {
                this.eventNameError();
            })
        } else if (e.target.id == "startDate") {
            this.setState({
                startDate: e.target.value
            }, () => {
                this.startDateError();
            })
        } else if (e.target.id == "endDate") {
            this.setState({
                endDate: e.target.value
            }, () => {
                this.endDateRError();
            })
        } else if (e.target.id == "description") {
            this.setState({
                description: e.target.value
            })
        }
    }

    startDateError() {
        if (this.state.startDate == "") {
            this.setState({
                startDaterr: true
            })
        } else {
            this.setState({
                startDaterr: false
            })
        }
    }
    endDateRError() {
        if (this.state.endDate == "") {
            this.setState({
                endDaterr: true
            })
        } else {
            this.setState({
                endDaterr: false
            })
        }
    }

    eventNameError() {
        if (this.state.eventName == "") {
            this.setState({
                eventNamerr: true
            })
        } else {
            this.setState({
                eventNamerr: false
            })
        }
    }


    savePromotional() {
        this.startDateError();
        this.endDateRError();
        this.eventNameError();

        setTimeout(() => {
            const { startDaterr, endDaterr, eventNamerr } = this.state;
            if (!startDaterr && !endDaterr && !eventNamerr) {
                // let sessionId = []
                // sessionId.push(JSON.parse(sessionStorage.getItem('Organization')))
                if (this.props.tableEditId == "" || this.props.tableEditId == undefined) {
                    let payload = {
                        name: this.state.eventName,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        description: this.state.description,
                        orgId: null
                    }
                    this.props.promotionalRequest(payload)
                    this.props.manageModel('close')
                } else {
                    let payload = {
                        id: this.props.tableEditId,
                        name: this.state.eventName,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        description: this.state.description,
                        orgId: null
                    }
                    this.props.updatePromotionalEventRequest(payload)
                    this.props.manageModel('close')
                }
            }
        }, 10);
    }

    render() {
        
        const { startDaterr, endDaterr, eventNamerr, } = this.state
        return (
            <div className="modal  display_block" id="editVendorModal">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content vendorEditModalContent modalShow adHocModal createNewFestival createNewEvent">
                        <div className="modalAction">
                            <input type="text" className="m-top-15" id="eventName" value={this.state.eventName} placeholder={this.state.eventName == "" ? "Enter Event Title" : this.state.eventName} onChange={(e) => this.handleChange(e)} />
                            {eventNamerr ?
                                <span className="error">
                                    Enter event name
                                    </span> : null}
                            <div className="col-md-6 pad-lft-0">
                                <input type="date" className="m-top-15 width100" id="startDate" value={this.state.startDate} min={this.state.minDate} max={this.state.maxDate} placeholder={this.state.startDate == "" ? "Start Date" : this.state.startDate} onChange={(e) => this.handleChange(e)} />
                                {startDaterr ?
                                    <span className="error">
                                        Enter start date
                                    </span> : null}
                            </div>
                            <div className="col-md-6">
                                <input type="date" className="m-top-15 width100" id="endDate" value={this.state.endDate} min={this.state.startDate} max={this.state.maxDate} placeholder={this.state.endDate == "" ? "End Date" : this.state.endDate} onChange={(e) => this.handleChange(e)} />
                                {endDaterr ?
                                    <span className="error">
                                        Enter end date
                                    </span> : null}
                            </div>
                            <div className="col-md-12 pad-lft-0 m-top-15">
                                <textarea className="width100" rows="5" id="description" value={this.state.description} placeholder={this.state.description == "" ? "Description" : this.state.description} onChange={(e) => this.handleChange(e)} />

                            </div>
                        </div>
                        <div className="footer">
                            <button type="button" className="cancel" onClick={() => this.props.manageModel('close')}>Cancel</button>
                            <button type="button" className={!this.state.disableBtn ? "btnDisabled done" : "done" }onClick={() =>!this.state.disableBtn ? null : this.savePromotional()}>Save</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default PromotionalEventModal;