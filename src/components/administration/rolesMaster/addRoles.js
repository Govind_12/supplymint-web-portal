import React from 'react';
import addIcon from "../../../assets/add-white.svg";
import rightArrow from "../../../assets/arrow-right.svg";
import addModule from "../../../assets/addModule.svg";
import ShowAllPermission from './viewAllPermissionModal';
import PoError from "../../loaders/poError";
export default class AddRolesOld extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleGetRoleData: [],
            roleNameArray: [],
            allPermissionModal: false,
            addRole: false,
            editRoleName: true,
            editModule: false,
            editPermission: false,
            addRoleName: "",
            addRoleNameerr: false,
            moduleData: [],
            moduleNames: [],
            levelTwo: false,
            levelThree: false,
            levelFour: false,
            levelTwoData: [],
            levelThreeData: [],
            levelFourData: [],
            levelTwoModuleName: "",
            levelThreeModuleName: "",
            levelFourModuleName: "",
            moduleNamesOnly: [],
            poErrorMsg: false,
            errorMassage: "",
            permissionData: [],

            selectedModuleCode: "",
            selectedChk: false,
            roleName: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            no: 1,
            type: 1,
            search: ""

        }
    }
    componentDidMount() {
        let payload = {
            moduleCode: 'first'
        }
        this.props.moduleGetDataRequest(payload)
        this.props.moduleGetRoleRequest('data')
    }
    manageShowModal(e, data) {
        if (e == "open") {
            this.setState({
                allPermissionModal: true,
                roleName: data
            })
            let payload = {
                roleId: data
            }
            this.props.getSubModuleByRoleNameRequest(payload)
        } else if (e == "close") {
            this.setState({
                allPermissionModal: false
            })
        }
    }
    AddRoles() {
        this.setState({
            addRole: !this.state.addRole,
        })
    }
    editRoleName() {
        this.setState({
            editRoleName: !this.state.editRoleName,
            editPermission: false
        })
    }
    editModule() {
        this.setState({
            editModule: !this.state.editModule,
            editPermission: false

        })
    }
    permissionBack() {
        this.setState({
            editPermission: !this.state.editPermission,
            editModule: true
        })
    }
    moduleBack() {
        this.setState({
            editModule: !this.state.editModule,
            editRoleName: true
        })
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }

    handleChange(e) {
        if (e.target.id == "addRoleName") {
            this.setState({
                addRoleName: e.target.value
            }, () => {
                this.roleName();
            })
        }
    }
    roleName() {
        if (
            this.state.addRoleName == ""
        ) {
            this.setState({
                addRoleNameerr: true
            });
        } else {
            this.setState({
                addRoleNameerr: false
            });
        }
    }
    nextRoleName() {
        this.roleName()
        setTimeout(() => {
            if (!this.state.addRoleNameerr) {
                this.setState({
                    editModule: true,
                    editRoleName: false

                })
            }
        }, 10)
    }
    permissionNext() {
        let moduleData = this.state.moduleData
        let moduleNamesOnly = this.state.moduleNamesOnly
        let moduleNames = this.state.moduleNames
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].isChecked == "TRUE" && moduleData[i].subModule.length == 0) {
                if (!moduleNamesOnly.includes(moduleData[i].moduleName)) {
                    moduleNamesOnly.push(moduleData[i].moduleName)
                    let modulee = {
                        name: moduleData[i].moduleName,
                        create: "0",
                        read: "0",
                        update: "0",
                        delete: "0"
                    }
                    moduleNames.push(modulee)
                }

            } else if (moduleData[i].isChecked == "FALSE" && moduleData[i].subModule.length == 0) {
                for (let p = 0; p < moduleNamesOnly.length; p++) {
                    if (moduleNamesOnly[p] == moduleData[i].moduleName) {
                        moduleNamesOnly.splice(p, 1)
                    }
                }
                for (let a = 0; a < moduleNames.length; a++) {
                    if (moduleNames[a].name == moduleData[i].moduleName)
                        moduleNames.splice(a, 1)
                }
            }
            if (moduleData[i].subModule.length != 0) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].isChecked == "TRUE" && moduleData[i].subModule[j].subModule.length == 0) {
                        if (!moduleNamesOnly.includes(moduleData[i].subModule[j].Name)) {
                            moduleNamesOnly.push(moduleData[i].subModule[j].Name)
                            let modulee = {
                                name: moduleData[i].subModule[j].Name,
                                create: "0",
                                read: "0",
                                update: "0",
                                delete: "0"
                            }
                            moduleNames.push(modulee)
                        }
                    } else if (moduleData[i].subModule[j].isChecked == "FALSE" && moduleData[i].subModule[j].subModule.length == 0) {
                        // if (moduleNamesOnly.includes(moduleData[i].subModule[j].Name)) { //     moduleNamesOnly.splice(j, 1)// }
                        for (let p = 0; p < moduleNamesOnly.length; p++) {
                            if (moduleNamesOnly[p] == moduleData[i].subModule[j].Name) {
                                moduleNamesOnly.splice(p, 1)
                            }
                        }
                        for (let a = 0; a < moduleNames.length; a++) {
                            if (moduleNames[a].name == moduleData[i].subModule[j].Name)
                                moduleNames.splice(a, 1)
                        }
                    }
                    if (moduleData[i].subModule[j].subModule.length != 0) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].isChecked == "TRUE" && moduleData[i].subModule[j].subModule[k].subModule.length == 0) {
                                if (!moduleNamesOnly.includes(moduleData[i].subModule[j].subModule[k].Name)) {
                                    moduleNamesOnly.push(moduleData[i].subModule[j].subModule[k].Name)
                                    let modulee = {
                                        name: moduleData[i].subModule[j].subModule[k].Name,
                                        create: "0",
                                        read: "0",
                                        update: "0",
                                        delete: "0"
                                    }
                                    moduleNames.push(modulee)
                                }
                            } else if (moduleData[i].subModule[j].subModule[k].isChecked == "FALSE" && moduleData[i].subModule[j].subModule[k].subModule.length == 0) {
                                // if (moduleNamesOnly.includes(moduleData[i].subModule[j].subModule[k].Name)) { moduleNamesOnly.splice(k, 1) }
                                for (let p = 0; p < moduleNamesOnly.length; p++) {
                                    if (moduleNamesOnly[p] == moduleData[i].subModule[j].subModule[k].Name) {
                                        moduleNamesOnly.splice(p, 1)
                                    }
                                }
                                for (let a = 0; a < moduleNames.length; a++) {
                                    if (moduleNames[a].name == moduleData[i].subModule[j].subModule[k].Name)
                                        moduleNames.splice(a, 1)
                                }
                            }
                            if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                    if (moduleData[i].subModule[j].subModule[k].subModule[l].isChecked == "TRUE" && moduleData[i].subModule[j].subModule[k].subModule[l].subModule.length == 0) {
                                        if (!moduleNamesOnly.includes(moduleData[i].subModule[j].subModule[k].subModule[l].Name)) {
                                            moduleNamesOnly.push(moduleData[i].subModule[j].subModule[k].subModule[l].Name)
                                            let modulee = {
                                                name: moduleData[i].subModule[j].subModule[k].subModule[l].Name,
                                                create: "0",
                                                read: "0",
                                                update: "0",
                                                delete: "0"
                                            }
                                            moduleNames.push(modulee)
                                        }
                                    } else if (moduleData[i].subModule[j].subModule[k].subModule[l].isChecked == "FALSE" && moduleData[i].subModule[j].subModule[k].subModule[l].subModule.length == 0) {
                                        // if (moduleNamesOnly.includes(moduleData[i].subModule[j].subModule[k].subModule[l].Name)) {moduleNamesOnly.splice(l, 1)}
                                        for (let p = 0; p < moduleNamesOnly.length; p++) {
                                            if (moduleNamesOnly[p] == moduleData[i].subModule[j].subModule[k].subModule[l].Name) {
                                                moduleNamesOnly.splice(p, 1)
                                            }
                                        }
                                        for (let a = 0; a < moduleNames.length; a++) {
                                            if (moduleNames[a].name == moduleData[i].subModule[j].subModule[k].subModule[l].Name)
                                                moduleNames.splice(a, 1)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            editPermission: true,
            moduleNamesOnly: moduleNamesOnly,
            moduleNames: moduleNames,
            editModule: false
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.moduleGetData.isSuccess) {
            let moduleName = ""
            let check = false
            if (nextProps.administration.moduleGetData.data.resource != null) {
                if (this.state.selectedModuleCode != "") {

                    let moduleData = this.state.moduleData
                    for (let i = 0; i < moduleData.length; i++) {
                        if (moduleData[i].moduleCode == this.state.selectedModuleCode) {
                            moduleName = moduleData[i].moduleName
                            check = moduleData[i].isChecked
                            moduleData[i].subModule = nextProps.administration.moduleGetData.data.resource

                        }
                    }
                    this.setState({
                        moduleData: moduleData
                    })
                    setTimeout(() => {
                        if (this.state.selectedChk == false) {
                            this.levelTwo(moduleName)
                        } else {
                            this.levelOneCheck(moduleName, check)
                            this.setState({
                                selectedChk: false
                            })
                        }
                    }, 10)
                }
                else {
                    this.setState({
                        moduleData: nextProps.administration.moduleGetData.data.resource
                    })


                }
            } else {
                this.setState({
                    moduleData: []
                })
            }
        }
        if (nextProps.administration.moduleGetRole.isSuccess) {
            if (nextProps.administration.moduleGetRole.data.resource != null) {
                this.setState({
                    moduleGetRoleData: nextProps.administration.moduleGetRole.data.resource,
                    roleNameArray: Object.keys(nextProps.administration.moduleGetRole.data.resource),
                    prev: nextProps.administration.moduleGetRole.data.prePage,
                    current: nextProps.administration.moduleGetRole.data.currPage,
                    next: nextProps.administration.moduleGetRole.data.currPage + 1,
                    maxPage: nextProps.administration.moduleGetRole.data.maxPage,
                })
            } else {
                this.setState({
                    moduleGetRoleData: [],
                    roleNameArray: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.administration.updateRole.isSuccess) {
            this.setState({
                allPermissionModal: false,
                addRole: false,
                editRoleName: true,
                editModule: false,
                editPermission: false,
                addRoleName: "",
                addRoleNameerr: false,
                moduleData: [],
                moduleNames: [],
                levelTwo: false,
                levelThree: false,
                levelFour: false,
                levelTwoData: [],
                levelThreeData: [],
                levelFourData: [],
                levelTwoModuleName: "",
                levelThreeModuleName: "",
                levelFourModuleName: "",
                moduleNamesOnly: [],
            })
            let payload = {
                moduleCode: 'first'
            }
            this.props.moduleGetDataRequest(payload)
        }
    }
    levelTwo(moduleName) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == moduleName) {

                if (moduleData[i].subModule.length != 0) {
                    this.setState({
                        levelTwo: true,
                        levelTwoModuleName: moduleName,
                        levelThree: false,
                        levelFour: false,
                        levelThreeModuleName: "",
                        levelFourModuleName: ""
                    })
                    if (moduleData[i].isExpand == "FALSE") {
                        moduleData[i].isExpand = "TRUE"
                    }
                } else {
                    this.setState({
                        selectedModuleCode: moduleData[i].moduleCode
                    })
                    let payload = {
                        moduleCode: moduleData[i].moduleCode
                    }
                    this.props.moduleGetDataRequest(payload)
                }
            }
            // else { moduleData[i].isChecked = "FALSE"this.setState({levelTwo: false,levelThree: false,levelFour: false,levelTwoModuleName: "",levelThreeModuleName: "",levelFourModuleName: ""})}

            else if (moduleData[i].moduleName != moduleName) {
                moduleData[i].isExpand = "FALSE"
                if (moduleData[i].subModule.length != 0) {
                    for (let j = 0; j < moduleData[i].subModule.length; j++) {
                        moduleData[i].subModule[j].isExpand = "FALSE"
                        if (moduleData[i].subModule[j].subModule.length != 0) {
                            for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                moduleData[i].subModule[j].subModule[k].isExpand = "FALSE"
                                if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                    for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                        moduleData[i].subModule[j].subModule[k].subModule[l].isExpand = "FALSE"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    levelThree(moduleName) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == moduleName) {
                        if (moduleData[i].subModule[j].isExpand == "FALSE") {
                            moduleData[i].subModule[j].isExpand = "TRUE"
                            if (moduleData[i].subModule[j].subModule.length != 0) {
                                this.setState({
                                    levelThree: true,
                                    levelThreeModuleName: moduleName,
                                    levelFour: false,
                                    levelFourModuleName: ""
                                })
                            }
                        }
                    }
                    else if (moduleData[i].subModule[j].Name != moduleName) {
                        moduleData[i].subModule[j].isExpand = "FALSE"
                    }
                }
            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    levelOneCheck(moduleName, check) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == moduleName) {
                if (moduleData[i].subModule.length != 0) {
                    if (moduleData[i].isChecked == "FALSE") {
                        moduleData[i].isChecked = "TRUE"
                        if (moduleData[i].subModule.length != 0) {
                            let level2 = moduleData[i].subModule
                            for (let j = 0; j < level2.length; j++) {
                                level2[j].isChecked = "TRUE"
                                if (level2[j].subModule.length != 0) {
                                    let level3 = level2[j].subModule
                                    for (let l = 0; l < level3.length; l++) {
                                        level3[l].isChecked = "TRUE"
                                        if (level3[l].subModule.length != 0) {
                                            let level4 = level3[l].subModule
                                            for (let k = 0; k < level4.length; k++) {
                                                level4[k].isChecked = "TRUE"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        moduleData[i].isChecked = "FALSE"
                        if (moduleData[i].subModule.length != 0) {
                            let level2 = moduleData[i].subModule
                            for (let j = 0; j < level2.length; j++) {
                                level2[j].isChecked = "FALSE"
                                if (level2[j].subModule.length != 0) {
                                    let level3 = level2[j].subModule
                                    for (let l = 0; l < level3.length; l++) {
                                        level3[l].isChecked = "FALSE"
                                        if (level3[l].subModule.length != 0) {
                                            let level4 = level3[l].subModule
                                            for (let k = 0; k < level4.length; k++) {
                                                level4[k].isChecked = "FALSE"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    this.setState({
                        selectedChk: true,
                        selectedModuleCode: moduleData[i].moduleCode
                    })
                    let payload = {
                        moduleCode: moduleData[i].moduleCode
                    }
                    this.props.moduleGetDataRequest(payload)
                }


            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    levelFour(name) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name == name) {
                                if (moduleData[i].subModule[j].subModule[k].isExpand == "FALSE") {
                                    moduleData[i].subModule[j].subModule[k].isExpand = "TRUE"
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                        this.setState({
                                            levelFour: true,
                                            levelFourModuleName: name
                                        })
                                    }
                                }
                            } else if (moduleData[i].subModule[j].subModule[k].Name != name) {
                                moduleData[i].subModule[j].subModule[k].isExpand = "FALSE"
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    levelTwoCheck(modulee) {
        let moduleData = this.state.moduleData
        let levelTwoFlag = false
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                if (moduleData[i].subModule.length != 0) {
                    for (let j = 0; j < moduleData[i].subModule.length; j++) {
                        if (moduleData[i].subModule[j].Name == modulee) {
                            if (moduleData[i].subModule[j].isChecked == "FALSE") {
                                moduleData[i].subModule[j].isChecked = "TRUE"
                                moduleData[i].isChecked = "TRUE"
                                if (moduleData[i].subModule[j].subModule.length != 0) {
                                    for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                        moduleData[i].subModule[j].subModule[k].isChecked = "TRUE"
                                        if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                            for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                                moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "TRUE"
                                            }
                                        }
                                    }
                                }
                            } else {
                                moduleData[i].subModule[j].isChecked = "FALSE"
                                if (moduleData[i].subModule[j].subModule.length != 0) {
                                    for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                        moduleData[i].subModule[j].subModule[k].isChecked = "FALSE"
                                        if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                            for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                                moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "FALSE"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (let k = 0; k < moduleData[i].subModule.length; k++) {
                        if (moduleData[i].subModule[k].isChecked == "TRUE") {
                            levelTwoFlag = true
                            break
                        }

                    }
                }
                if (levelTwoFlag == true) {
                    moduleData[i].isChecked = "TRUE"
                } else {
                    moduleData[i].isChecked = "FALSE"

                }
            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    levelThreeCheck(mname) {
        let moduleData = this.state.moduleData
        let levelTwoFlag = false
        let levelThreeFlag = false
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {

                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name == mname) {
                                if (moduleData[i].subModule[j].subModule[k].isChecked == "FALSE") {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "TRUE"
                                    moduleData[i].isChecked = "TRUE"
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                        for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "TRUE"
                                        }
                                    }
                                } else {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "FALSE"
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                        for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "FALSE"
                                        }
                                    }
                                }
                            }
                        }

                        for (let f = 0; f < moduleData[i].subModule[j].subModule.length; f++) {
                            if (moduleData[i].subModule[j].subModule[f].isChecked == "TRUE") {
                                levelThreeFlag = true
                                break
                            }
                        }
                        if (levelThreeFlag == true) {
                            moduleData[i].subModule[j].isChecked = "TRUE"
                        } else {
                            moduleData[i].subModule[j].isChecked = "FALSE"

                        }
                    }
                }
                for (let k = 0; k < moduleData[i].subModule.length; k++) {
                    if (moduleData[i].subModule[k].isChecked == "TRUE") {
                        levelTwoFlag = true
                        break
                    }

                }
                if (levelTwoFlag == true) {

                    moduleData[i].isChecked = "TRUE"
                } else {

                    moduleData[i].checked = "FALSE"
                }
            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    levelFourCheck(name) {
        let moduleData = this.state.moduleData
        let levelTwoFlag = false
        let levelThreeFlag = false
        let levelFourFlag = false
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name == this.state.levelFourModuleName) {
                                for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                    if (moduleData[i].subModule[j].subModule[k].subModule[l].Name == name) {
                                        if (moduleData[i].subModule[j].subModule[k].subModule[l].isChecked == "FALSE") {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "TRUE"
                                            moduleData[i].isChecked = "TRUE"
                                        } else {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "FALSE"
                                        }
                                    }
                                }
                                for (let g = 0; g < moduleData[i].subModule[j].subModule.length; g++) {
                                    if (moduleData[i].subModule[j].subModule[k].subModule[g].isChecked == "TRUE") {
                                        levelFourFlag = true
                                        break
                                    }
                                }
                                if (levelFourFlag == true) {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "TRUE"
                                } else {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "FALSE"

                                }

                            }
                        }
                        for (let f = 0; f < moduleData[i].subModule[j].subModule.length; f++) {
                            if (moduleData[i].subModule[j].subModule[f].isChecked == "TRUE") {
                                levelThreeFlag = true
                                break
                            }
                        }
                        if (levelThreeFlag == true) {
                            moduleData[i].subModule[j].isChecked = "TRUE"
                        } else {
                            moduleData[i].subModule[j].isChecked = "FALSE"

                        }
                    }
                }
                for (let k = 0; k < moduleData[i].subModule.length; k++) {
                    if (moduleData[i].subModule[k].isChecked == "TRUE") {
                        levelTwoFlag = true
                        break
                    }

                }
                if (levelTwoFlag == true) {

                    moduleData[i].isChecked = "TRUE"
                } else {

                    moduleData[i].checked = "FALSE"
                }
            }
        }
        this.setState({
            moduleData: moduleData
        })
    }
    crudOperation(name, crud) {
        let moduleNames = this.state.moduleNames
        for (let i = 0; i < moduleNames.length; i++) {
            if (moduleNames[i].name == name) {
                if (crud == "create") {
                    if (moduleNames[i].create == "1") {
                        moduleNames[i].create = "0"
                    } else {
                        moduleNames[i].create = "1"
                        moduleNames[i].read = "1"
                    }
                } else if (crud == "read") {
                    if (moduleNames[i].read == "1") {
                        moduleNames[i].read = "0"
                    } else {
                        moduleNames[i].read = "1"
                    }
                } else if (crud == "update") {
                    if (moduleNames[i].update == "1") {
                        moduleNames[i].update = "0"
                    } else {
                        moduleNames[i].update = "1"
                        moduleNames[i].read = "1"
                    }
                } else if (crud == "delete") {
                    if (moduleNames[i].delete == "1") {
                        moduleNames[i].delete = "0"
                    } else {
                        moduleNames[i].delete = "1"
                        moduleNames[i].read = "1"
                    }
                }
            }
        }
        this.setState({
            moduleNames: moduleNames
        })
    }
    saveRoles() {
        let moduleData = this.state.moduleData
        let moduleNames = this.state.moduleNames
        let checkedModule = []
        let flag = false
        for (let p = 0; p < moduleNames.length; p++) {
            if (moduleNames[p].read == "1" || moduleNames[p].create == "1" || moduleNames[p].update == "1" || moduleNames[p].delete == "1") {
                flag = true
                break
            }
        }
        if (flag) {
            for (let i = 0; i < moduleData.length; i++) {
                for (let j = 0; j < moduleNames.length; j++) {
                    if (moduleData[i].isChecked == "TRUE") {
                        moduleData[i].crud = "1111"
                        if (moduleData[i].subModule.length == 0) {
                            if (moduleData[i].moduleName == moduleNames[j].name) {
                                moduleData[i].crud = moduleNames[j].create + moduleNames[j].read + moduleNames[j].update + moduleNames[j].delete
                            }
                            // if (moduleData[i].moduleName != moduleNames[j].name) { //     moduleData[i].crud= "0000" // }
                        } else {
                            for (let k = 0; k < moduleData[i].subModule.length; k++) {
                                for (let l = 0; l < moduleNames.length; l++) {
                                    if (moduleData[i].subModule[k].subModule.length == 0) {
                                        if (moduleData[i].subModule[k].Name == moduleNames[l].name) {
                                            moduleData[i].subModule[k].crud = moduleNames[l].create + moduleNames[l].read + moduleNames[l].update + moduleNames[l].delete
                                        }
                                        // if(moduleData[i].subModule[k].Name != moduleNames[l].name){ //     moduleData[i].subModule[k].crud="0000" // }
                                    } else {
                                        for (let m = 0; m < moduleData[i].subModule[k].subModule.length; m++) {
                                            for (let n = 0; n < moduleNames.length; n++) {
                                                if (moduleData[i].subModule[k].subModule[m].subModule.length == 0) {
                                                    if (moduleData[i].subModule[k].subModule[m].Name == moduleNames[n].name) {
                                                        moduleData[i].subModule[k].subModule[m].crud = moduleNames[n].create + moduleNames[n].read + moduleNames[n].update + moduleNames[n].delete
                                                    }
                                                    // if (moduleData[i].subModule[k].subModule[m].Name != moduleNames[n].name) { //      moduleData[i].subModule[k].subModule[m].crud="0000" // }
                                                } else {
                                                    for (let a = 0; a < moduleData[i].subModule[k].subModule[m].subModule.length; a++) {
                                                        for (let b = 0; b < moduleNames.length; b++) {
                                                            if (moduleData[i].subModule[k].subModule[m].subModule[a].subModule.length == 0) {
                                                                if (moduleData[i].subModule[k].subModule[m].subModule[a].Name == moduleNames[b].name) {
                                                                    moduleData[i].subModule[k].subModule[m].subModule[a].crud = moduleNames[b].create + moduleNames[b].read + moduleNames[b].update + moduleNames[b].delete
                                                                }
                                                                //    if(moduleData[i].subModule[k].subModule[m].subModule[a].Name!=moduleNames[b].name){//        moduleData[i].subModule[k].subModule[m].subModule[a].crud ="0000"//    }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (let i = 0; i < moduleData.length; i++) {
                if (moduleData[i].isChecked == "TRUE") {
                    checkedModule.push(moduleData[i])
                }
            }
            console.log(checkedModule)
            this.setState({
                moduleData: moduleData
            })
            let payload = {
                isUpdated: "FALSE",
                roleName: this.state.addRoleName,
                module: checkedModule
            }
            this.props.updateRoleRequest(payload)
        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Atleast select one"
            })

        }
    }


    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.administration.moduleGetRole.data.prePage,
                current: this.props.administration.moduleGetRole.data.currPage,
                next: this.props.administration.moduleGetRole.data.currPage + 1,
                maxPage: this.props.administration.moduleGetRole.data.maxPage,
            })
            if (this.props.administration.moduleGetRole.data.currPage != 0) {
                let data = {
                    no: this.props.administration.moduleGetRole.data.currPage - 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.moduleGetRoleRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.moduleGetRole.data.prePage,
                current: this.props.administration.moduleGetRole.data.currPage,
                next: this.props.administration.moduleGetRole.data.currPage + 1,
                maxPage: this.props.administration.moduleGetRole.data.maxPage,
            })
            if (this.props.administration.moduleGetRole.data.currPage != this.props.administration.moduleGetRole.data.maxPage) {

                let data = {
                    no: this.props.administration.moduleGetRole.data.currPage + 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.moduleGetRoleRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.administration.moduleGetRole.data.prePage,
                current: this.props.administration.moduleGetRole.data.currPage,
                next: this.props.administration.moduleGetRole.data.currPage + 1,
                maxPage: this.props.administration.moduleGetRole.data.maxPage,
            })
            if (this.props.administration.moduleGetRole.data.currPage <= this.props.administration.moduleGetRole.data.maxPage) {
                let data = {
                    no: 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.moduleGetRoleRequest(data)
            }

        }
    }

    render() {
        return (
            <div className="container-fluid pad-0" id="">
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                    <div className="container_content height-70vh promotionalEvents addRoleMain add-vendor-page">
                        <ul className="list_style m-top-3">
                            <li>
                                <label className="contribution_mart">
                                    Add Roles
                            </label>
                            </li>
                        </ul>
                        {!this.state.addRole ? <div className="col-md-12 pad-0 m-top-10">
                            <div className="addNewRole">
                                <h5 onClick={(e) => this.AddRoles(e)}><img src={addIcon} /><label>Add New Role</label></h5>
                            </div>
                        </div> : null}
                        {this.state.addRole ? <div className="col-md-12 pad-0">
                            {!this.state.editRoleName ? <div className="roleInfo addNewRole alignMiddle width100 removeroleEdit">
                                <div className="col-md-6 pad-0">
                                    <h3>Role Name</h3>
                                    <h4>{this.state.addRoleName}</h4>
                                </div>
                                <div className="col-md-6 pad-0 textRight">
                                    <button type="button" className="edit" onClick={(e) => this.editRoleName(e)}>Edit</button>
                                </div>
                            </div> : null}
                            {this.state.editRoleName ? <div className="roleInfo addNewRole width100">
                                <h3>Role Name</h3>
                                <input type="text" placeholder="Enter Role Name" id="addRoleName" value={this.state.addRoleName} onChange={(e) => this.handleChange(e)} />
                                <button type="button" className="cancel" onClick={(e) => this.editRoleName(e)} >Cancel</button>
                                <button type="button" className="next" onClick={(e) => this.nextRoleName(e)}>Next</button>
                                {this.state.addRoleNameerr ? (
                                    <span className="error">
                                        Enter  Role Name
                                    </span>
                                ) : null}
                            </div> : null}
                        </div> : null}
                        {this.state.addRole ? <div className="col-md-12 pad-0 m-top-20">
                            {!this.state.editModule ? <div className="roleInfo addNewRole alignMiddle width100 removeroleEdit">
                                <div className="col-md-6 pad-0">
                                    <h3>Modules</h3>
                                    <ul className="list-inline selectedModule m-top-5">
                                        {this.state.moduleNamesOnly.map((name, k) => (
                                            <li key={k}>{name}</li>
                                        ))}
                                    </ul>
                                </div>
                                <div className="col-md-6 pad-0 textRight">
                                    <button type="button" className="edit" onClick={(e) => this.editModule(e)}>Edit</button>
                                </div>
                            </div> : null}
                            {this.state.editModule ? <div className="roleInfo addNewRole width100 posRelative moduleSelect displayInline ">
                                <h3>Modules</h3>
                                <div className="col-md-10 pad-0">
                                    <div className="col-md-3 pad-0 m-top-9">
                                        {this.state.moduleData.map((data, key) => (
                                            <div className="col-md-12 pad-0 select" onClick={(e) => this.levelTwo(data.moduleName)} key={key}>
                                                <div className="circularCheckBox">
                                                    <ul className="list-inline width_100">
                                                        <div className="siteCheckBox">
                                                            <label className="checkBoxLabel0"><input type="checkBox" checked={data.isChecked == "TRUE"} onClick={(e) => this.levelOneCheck(data.moduleName, data.isChecked)} /><span className="checkmark1"></span> </label>
                                                        </div>
                                                    </ul>
                                                </div>
                                                <label>{data.moduleName}{data.subModule.length != 0 ? data.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                            </div>
                                        ))}
                                    </div>
                                    {this.state.levelTwo ? <div className="col-md-3 pad-0 m-top-9">
                                        {this.state.moduleData.map((data, l1) => (data.moduleName == this.state.levelTwoModuleName ? data.subModule.map((levelTwo, keyy) => (
                                            <div className="col-md-12 pad-0 Select" key={keyy} onClick={levelTwo.subModule.length != 0 ? (e) => this.levelThree(levelTwo.Name) : null}>
                                                <div className="circularCheckBox">
                                                    <ul className="list-inline width_100">
                                                        <div className="siteCheckBox">
                                                            <label className="checkBoxLabel0"><input type="checkBox" checked={levelTwo.isChecked == "TRUE"} onClick={(e) => this.levelTwoCheck(levelTwo.Name)} /><span className="checkmark1"></span> </label>
                                                        </div>
                                                    </ul>
                                                </div>
                                                <label>{levelTwo.Name} {levelTwo.subModule.length != 0 ? levelTwo.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                            </div>)) : null))}
                                    </div> : null}
                                    {this.state.levelThree ? <div className="col-md-3 pad-0 m-top-9">
                                        {this.state.moduleData.map((data, ll1) => (data.moduleName == this.state.levelTwoModuleName ? data.subModule.map((levelTwo, l2) => (
                                            levelTwo.Name == this.state.levelThreeModuleName ? levelTwo.subModule.map((levelThree, keyyy) => (
                                                <div className="col-md-12 pad-0 Select" key={keyyy} onClick={levelThree.subModule.length != 0 ? (e) => this.levelFour(levelThree.Name) : null} >
                                                    <div className="circularCheckBox">
                                                        <ul className="list-inline width_100">
                                                            <div className="siteCheckBox">
                                                                <label className="checkBoxLabel0"><input type="checkBox" checked={levelThree.isChecked == "TRUE"} onClick={(e) => this.levelThreeCheck(levelThree.Name)} /><span className="checkmark1"></span> </label>
                                                            </div>
                                                        </ul>
                                                    </div>
                                                    <label>{levelThree.Name}  {levelThree.subModule.length != 0 ? levelThree.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                                </div>)) : null)) : null))}
                                    </div> : null}
                                    {this.state.levelFour ? <div className="col-md-3 pad-0 m-top-9">
                                        {this.state.moduleData.map((data, lll1) => (data.moduleName == this.state.levelTwoModuleName ? data.subModule.map((levelTwo, ll2) => (
                                            levelTwo.Name == this.state.levelThreeModuleName ? levelTwo.subModule.map((levelThree, l3) => (
                                                levelThree.Name == this.state.levelFourModuleName ? levelThree.subModule.map((levelFour, keyyyy) => (
                                                    <div className="col-md-12 pad-0 select" key={keyyyy}>
                                                        <div className="circularCheckBox">
                                                            <ul className="list-inline width_100">
                                                                <div className="siteCheckBox">
                                                                    <label className="checkBoxLabel0"><input type="checkBox" checked={levelFour.isChecked == "TRUE"} onClick={(e) => this.levelFourCheck(levelFour.Name)} /><span className="checkmark1"></span> </label>
                                                                </div>
                                                            </ul>
                                                        </div>
                                                        <label>{levelFour.Name} {levelFour.subModule.length != 0 ? levelFour.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                                    </div>)) : null)) : null)) : null))}
                                    </div> : null}
                                </div>
                                <div className="handleOperation">
                                    <button type="button" className="cancel" onClick={(e) => this.moduleBack(e)}>Back</button>
                                    <button type="button" className="next" onClick={(e) => this.permissionNext(e)} >Next</button>
                                </div>
                            </div> : null}
                        </div> : null}

                        <div className="col-md-12 pad-0 m-top-20 permission">
                            {!this.state.addRole ? <div className="roleInfo addNewRole alignMiddle width100 removeroleEdit">
                                <div className="col-md-6 pad-0">
                                    <h3>Permissions</h3>
                                </div>
                            </div> : null}
                            {this.state.addRole ? this.state.editPermission ? <div className="roleInfo addNewRole width100 displayInline ">
                                <div className="col-md-6 pad-0">
                                    <h3>Permissions</h3>
                                </div>
                                <div className="col-md-6 textRight pad-0">
                                    <button type="button" className="cancel" onClick={(e) => this.permissionBack(e)}>Back</button>
                                    <button type="button" className="next" onClick={(e) => this.saveRoles(e)}>Save</button>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3 userRestrict noStrips m-top-20">
                                    <div className="scrollableOrgansation zui-scrollerHistory max-wid-100 scrollableTableFixed heightAuto">
                                        <table className="table scrollTable zui-table border-bot-table">
                                            <thead>
                                                <tr>
                                                    <th><label>Module</label></th>
                                                    <th><label>Create</label></th>
                                                    <th><label>Read</label></th>
                                                    <th><label>Update</label></th>
                                                    <th><label>Delete</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.moduleNames.length != 0 ? this.state.moduleNames.map((data, key) => (<tr key={key}>
                                                    <td><label>{data.name}</label></td>
                                                    <td><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" checked={data.create == "1" ? true : false} onClick={(e) => this.crudOperation(data.name, "create")} /> <span className="checkmark1"></span> </label></td>
                                                    <td><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" checked={data.read == "1" ? true : false} onClick={(e) => this.crudOperation(data.name, "read")} /> <span className="checkmark1"></span> </label></td>
                                                    <td><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" checked={data.update == "1" ? true : false} onClick={(e) => this.crudOperation(data.name, "update")} /> <span className="checkmark1"></span> </label></td>
                                                    <td><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" checked={data.delete == "1" ? true : false} onClick={(e) => this.crudOperation(data.name, "delete")} /> <span className="checkmark1"></span> </label></td>
                                                </tr>)) : null}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> : null : null}
                        </div>
                        {!this.state.addRole ? <div className="col-md-12 pad-0 m-top-40">
                            <div className="vendor-gen-table">
                                <div className="manage-table">
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th><label>Role Name</label></th>
                                                <th><label>Module</label></th>
                                                <th><label>Permission</label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.roleNameArray.length != 0 ? this.state.roleNameArray.map((data, key) => (
                                                <tr key={key}>
                                                    <td><label>{data}</label></td>
                                                    <td>{this.state.moduleGetRoleData[data].map((dataa, keyy) => (<label key={keyy}>{dataa.MODULE_NAME},</label>))}</td>
                                                    <td><label className="displayPointer" onClick={(e) => this.manageShowModal("open", data)}>Show all Permission</label></td>
                                                </tr>)) : <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> : null}
                        {!this.state.addRole ?
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder" min="1" value="1" />
                                            {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <div className="pad-right-15 wthauto">
                                                <ul className="list-inline pagination width100 ">
                                                    {this.state.current == 1 || this.state.current == 0 ? <li >
                                                        <button className="PageFirstBtn pointerNone" type="button">
                                                            First
                                                    </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn " type="button" onClick={(e) => this.page(e)} id="first" >
                                                                First
                                                        </button>
                                                        </li>}
                                                    {this.state.prev != 0 ? <li >
                                                        <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                            Prev
                                                    </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn" disabled>
                                                                Prev
                                                        </button>
                                                        </li>}
                                                    <li>
                                                        <button className="PageFirstBtn pointerNone pagination-no" type="button">
                                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                                        </button>
                                                    </li>
                                                    {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                        <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                            Next
                                                    </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                Next
                                                        </button>
                                                        </li> : <li >
                                                            <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                Next
                                                        </button>
                                                        </li>}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> : null}
                    </div>
                </div>
                {this.state.allPermissionModal ? <ShowAllPermission  {...this.props} roleName={this.state.roleName} permissionData={this.state.permissionData} manageShowModal={(e) => this.manageShowModal(e)} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}