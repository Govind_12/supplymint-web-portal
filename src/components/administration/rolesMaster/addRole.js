import React from 'react';
import addIcon from "../../../assets/add-white.svg";
import rightArrow from "../../../assets/arrow-right.svg";
import addModule from "../../../assets/addModule.svg";
import ShowAllPermission from './viewAllPermissionModal';
import PoError from "../../loaders/poError";
import Pagination from '../../pagination';
import axios from 'axios';
import { CONFIG } from '../../../config/index';
import { message } from 'antd';
import searchIcon from '../../../assets/clearSearch.svg';

//This is Create Role page

export default class AddRoles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            getModuleData: [],
            roleNameAvailablityStatus: null,
            newArray: [],
            roleName: null,
            parseData: [],
            success: false,
            search: "",
        }
    }
    componentDidMount() {
        this.props.moduleGetDataRequest('data')
        sessionStorage.setItem('currentPage', "RADMMAIN")
        sessionStorage.setItem('currentPageName', "Manage Role")

        // if (this.props.administration.moduleGetData.data.resource) {
        //     let data = this.props.administration.moduleGetData.data.resource
        //     for (let i = 0; i < data.length; i++) {
        //         if (Array.isArray(data[i].modules) && data[i].modules.length) {
        //             let topModule = data[i].moduleName
        //             this.parseSubmodules(data[i].modules, topModule, "")
        //         }
        //     }

        // }
        // this.setState({
        //     success: false,
        //     getModuleData: this.props.administration &&
        //         this.props.administration.moduleGetData &&
        //         this.props.administration.moduleGetData.data &&
        //         this.props.administration.moduleGetData.data.resource ?
        //         this.props.administration.moduleGetData.data.resource : [],
        // })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.moduleGetData.isSuccess) {
            if (nextProps.administration.moduleGetData.data.resource != null) {
                let data = nextProps.administration.moduleGetData.data.resource
                for (let i = 0; i < data.length; i++) {
                    if (Array.isArray(data[i].modules) && data[i].modules.length) {
                        let topModule = data[i].moduleName
                        this.parseSubmodules(data[i].modules, topModule, "")
                    }
                }
                this.setState({
                    getModuleData: nextProps.administration.moduleGetData.data.resource,
                })
            } else {
                this.setState({
                    getModuleData: [],
                })
            }
            this.props.moduleGetDataClear()
        }
        if (nextProps.administration.checkRoleNameData.isSuccess) {
            if (nextProps.administration.checkRoleNameData.data.resource != null) {

                this.setState({
                    roleNameAvailablityStatus: nextProps.administration.checkRoleNameData.data.resource.available === 1 ? true : false,

                })
            } else {
                this.setState({
                    roleNameAvailablityStatus: null,
                })
            }
        }
        if (nextProps.administration.checkRoleNameData.isError) {
            this.setState({ roleNameAvailablityStatus: false, })
        }
    }
    checkAll = (moduleId) => {
        let data = [...this.state.parseData]
        let index = data.findIndex((_) => _.moduleId == moduleId)
        if (data[index].crud === "1111") {
            data[index].crud = "0000"
        } else {
            data[index].crud = "1111"
        }
        this.setState({ parseData: data })
    }
    onPermissionCheckboxClick = (e, moduleId, status, pos) => {
        let data = [...this.state.parseData]
        let index = data.findIndex((_) => _.moduleId == moduleId)
        let crud = data[index].crud
        crud = crud.split('');
        crud[pos] = parseInt(crud[pos]) === 1 ? 0 : 1;
        if(parseInt(crud[0])===1 || parseInt(crud[2])===1 || parseInt(crud[3])===1) {
            crud[1]=1;
        }
        data[index].crud = crud.join('')
        this.setState({
            parseData: data
        })
    }
    handleRoleName = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "") {
                this.setState({
                    roleName: e.target.value,
                })  
            }
        } else {
            this.setState({
                roleName: null,
                roleNameAvailablityStatus: null,
            })
        }
        if(e.keyCode == 13){
          e.target.blur(e);  
        }

    }

    onBlurRoleNameChange =(e) =>{
        if(this.state.roleName !== null || this.state.roleName !== ""){
            let payload = {
                roleName: this.state.roleName
            }
            this.props.checkRoleNameRequest(payload)
        }

    }

    onClear = () => {
        let parseData = this.state.parseData.map((_) => ({ ..._, crud: "0000" }))
        this.setState({
            roleName: null,
            roleNameAvailablityStatus: null,
            parseData
        })
    }
    onSubmit = () => {
        let roles = this.state.parseData.filter((_) => _.crud !== "0000").map(__ => ({ moduleId: __.moduleId, crud: __.crud }))
        if (roles.length && this.state.roleName) {
            let payload = {
                roleName: this.state.roleName,
                roleAccess: roles
            }
            this.props.roleSubmitRequest(payload)
            this.props.history.push("/administration/rolesMaster/manageRoles")
        }
        else {
            message.error("Please check role name and atleast one access is required")
        }
    }
    parseSubmodules = (subModules, topModule, parentNode) => {
        for (let i = 0; i < subModules.length; i++) {
            if (Array.isArray(subModules[i].modules) && subModules.modules !== null) {
                this.parseSubmodules(subModules[i].modules, topModule, subModules[i].moduleName)
            } else {
                let page = { moduleId: subModules[i].moduleId, module: topModule, parentNode: parentNode == "" ? topModule : parentNode, page: subModules[i].moduleName, crud: "0000" }
                this.setState(prevState => ({ parseData: [...prevState.parseData, page] }))
            }
        }
    }

    onRefresh =()=> {
        let data = {
            no: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
        }
        this.props.moduleGetDataRequest(data)
        this.setState({ search: ""})
    }

    onSearch = (e) => {
        let value = e.target.value;
        this.setState({ search: e.target.value},()=>{
            let data = {
                no: 1,
                type: value.length ? 3 : 1,
                search: value,
                sortedBy: "",
                sortedIn: "",
            }
            this.props.moduleGetDataRequest(data)
        })   
    }

    render() {
        return (
            <div className="container-fluid pad-0" id="">
                <form id="myForm">
                    <div className="col-lg-12 pad-0">
                        <div className="new-gen-head p-lr-47">
                            <div className="col-lg-7 pad-0">
                                <div className="new-gen-left">
                                    {/* <div className="ngl-search">
                                       <input type="search" value={this.state.search} onChange={this.onSearch} placeholder="Type To Search" name="search" autoComplete="off"/>
                                       <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                       {this.state.search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.onRefresh} /></span> : null}
                                    </div> */}
                                    <button className="ngl-back" onClick={() => this.props.history.push("/administration/rolesMaster/manageRoles")} type="reset">
                                        <span className="back-arrow">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                                <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                            </svg>
                                        </span>
                                        Back
                                    </button>
                                </div>
                            </div>
                            <div className="col-lg-5 pad-0">
                                <div className="new-gen-right">
                                    <button type="button" onClick={this.onClear}>Clear</button>
                                    <button type="button" onClick={this.onSubmit} className="get-details">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 p-lr-47">
                        <div className="ar-name">
                            <p>Role Name</p>
                            <div className="arn-buttons">
                                <input type="text" value={this.state.roleName == null ? "" : this.state.roleName} onChange={this.handleRoleName} onBlur={this.onBlurRoleNameChange} onKeyDown={this.handleRoleName}/>
                                {'\u00A0'}{'\u00A0'}{this.state.roleNameAvailablityStatus == null ? null : (this.state.roleNameAvailablityStatus === true ? (this.state.roleName !== null && <img src={require('../../../assets/tick1.svg')} />) : <img src={require('../../../assets/cross.svg')} />)}

                                {/*                             <button type="button" className="arnb-check">Check Availability </button> */}
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 p-lr-47">
                        <div className="vendor-gen-table add-role-table">
                            <div className="manage-table">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                <table className="table gen-main-table">
                                    <thead>
                                        <tr>
                                            <th><label>Module Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            <th><label>Parent Modules</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            <th><label>Page Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            <th><label>Permission</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        {this.state.parseData.length && this.state.parseData.map((_, key) => (
                                            <tr key={key}>
                                                <td><label className="bold">{_.module}</label></td>
                                                <td><label>{_.parentNode}</label></td>
                                                <td><label>{_.page}</label></td>
                                                <td>
                                                    <ul className="manage-role-check">
                                                        <li>
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" id={_.moduleId + "create"} checked={_.crud[0] == 1} onChange={(e) => this.onPermissionCheckboxClick(e, _.moduleId, "create", 0)} name="selectEach" />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                            <p>Create</p>
                                                        </li>
                                                        <li>
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" name="selectEach1" checked={_.crud[1] == 1} id={_.moduleId + "read"} onChange={(e) => this.onPermissionCheckboxClick(e, _.moduleId, "read", 1)} />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                            <p>Read</p>
                                                        </li>
                                                        <li>
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" name="selectEach2" id={_.moduleId + "update"} checked={_.crud[2] == 1} onChange={(e) => this.onPermissionCheckboxClick(e, _.moduleId, "update", 2)} />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                            <p>Update</p>
                                                        </li>

                                                        <li>
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" name="selectEach3" id={_.moduleId + "delete"} checked={_.crud[3] == 1} onChange={(e) => this.onPermissionCheckboxClick(e, _.moduleId, "delete", 3)} />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                            <p>Delete</p>
                                                        </li>
                                                        <li>
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkBox" name="checkAll" id={_.moduleId + "checkAll"} checked={_.crud == "1111"} onChange={() => this.checkAll(_.moduleId)}/>
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                            <p>Check All</p>
                                                            {/* <p onClick={() => this.checkAll(_.moduleId)}>Check All</p> */}
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {/*<div className="new-gen-pagination">
                            <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder" value={1} />
                                    { <span className="ngp-total-item">Total Items </span> <span className="bold">1045</span> }
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination />
                                </div>
                            </div>
                        </div>*/}
                    </div>
                </form>
            </div>
        )
    }
}
