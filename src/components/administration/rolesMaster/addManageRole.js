import React from 'react';
import Pagination from '../../pagination';

export default class AddManageRole extends React.Component {
    render() {
        return (
            <div className="container-fluid pad-0" id="">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-7 pad-0">
                            <div className="new-gen-left">
                                <div className="ngl-search">
                                    <input type="search" placeholder="Type To Search" name="search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="get-details">Save</button>
                                <button type="button">Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="ar-name">
                        <p>Role Name</p>
                        <div className="arn-buttons">
                            <button type="button" className="">System Administrator <img src={require('../../../assets/cross.svg')} /></button>
                            <button type="button" className="arnb-check">Check Availability </button>
                            <button type="button" className="arnb-error">Role name already exist . Please choose different one </button>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table add-role-table">
                        <div className="manage-table">
                            <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th><label>Module Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Parent Modules</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Permission</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>

                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">AUTO CONFIGURATION</label></td>
                                        <td><label>Replenishment</label><img src={require('../../../assets/rightArrow2.svg')} /><label>Scheduler</label></td>
                                        <td>
                                            <ul className="manage-role-check">
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Read</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Create</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Update</p>
                                                </li>
                                                <li>
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                    <p>Delete</p>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="new-gen-pagination">
                        <div className="ngp-left">
                            <div className="table-page-no">
                                <span>Page :</span><input type="number" className="paginationBorder" value={1} />
                                {/* <span className="ngp-total-item">Total Items </span> <span className="bold">1045</span> */}
                            </div>
                        </div>
                        <div className="ngp-right">
                            <div className="nt-btn">
                                <Pagination />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}