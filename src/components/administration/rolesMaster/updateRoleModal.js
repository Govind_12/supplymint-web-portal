import React from 'react';
import rightArrow from "../../../assets/arrow-right.svg";
import addModule from "../../../assets/addModule.svg";
import _ from 'lodash'
export default class UpdateRoleModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleData: [],
            read: "0",
            create: "0",
            update: "0",
            deleteCrud: "0",
            fosusedvalue: false,
            moduleDataStatic: [],
            unchecked: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.getSubModuleData.isSuccess) {
            if (nextProps.administration.getSubModuleData.data.resource != null) {
                if (!this.state.unchecked) {
                    this.setState({
                        moduleData: nextProps.administration.getSubModuleData.data.resource,
                    })
                } else if (this.state.unchecked) {
                    this.setState({
                        moduleDataStatic: nextProps.administration.getSubModuleData.data.resource
                    })
                    this.updatedPayload()
                }
            } else {
                this.setState({
                    moduleData: [],
                    moduleDataStatic: []
                })
            }
            this.props.getSubModuleDataClear()
        }
        if (nextProps.administration.updateRole.isSuccess) {
            let data = {
                roleName: this.props.roleName
            }
            this.props.getSubModuleDataRequest(data)
            this.props.updateRoleClear()
        }
    }
    levelTwo(moduleName) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == moduleName) {
                if (moduleData[i].isExpand == "FALSE") {
                    moduleData[i].isExpand = "TRUE"
                    if (moduleData[i].subModule.length != 0) {
                        this.setState({
                            levelTwo: true,
                            levelTwoModuleName: moduleName,
                            levelThree: false,
                            levelFour: false,
                            levelThreeModuleName: "",
                            levelFourModuleName: ""
                        })
                    }
                }
            }
            else if (moduleData[i].moduleName != moduleName) {
                moduleData[i].isExpand = "FALSE"
                moduleData[i].isFocused = "FALSE"
                if (moduleData[i].subModule.length != 0) {
                    for (let j = 0; j < moduleData[i].subModule.length; j++) {
                        moduleData[i].subModule[j].isExpand = "FALSE"
                        moduleData[i].subModule[j].isFocused = "FALSE"
                        if (moduleData[i].subModule[j].subModule.length != 0) {
                            for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                moduleData[i].subModule[j].subModule[k].isExpand = "FALSE"
                                moduleData[i].subModule[j].subModule[k].isFocused = "FALSE"
                                if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                    for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                        moduleData[i].subModule[j].subModule[k].subModule[l].isExpand = "FALSE"
                                        moduleData[i].subModule[j].subModule[k].subModule[l].isFocused = "FALSE"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            moduleData,
        })
    }
    levelThree(moduleName) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == moduleName) {
                        if (moduleData[i].subModule[j].isExpand == "FALSE") {
                            moduleData[i].subModule[j].isExpand = "TRUE"
                            if (moduleData[i].subModule[j].subModule.length != 0) {
                                this.setState({
                                    levelThree: true,
                                    levelThreeModuleName: moduleName,
                                    levelFour: false,
                                    levelFourModuleName: ""
                                })
                            }
                        }
                    }
                    else if (moduleData[i].subModule[j].Name != moduleName) {
                        moduleData[i].subModule[j].isExpand = "FALSE"
                        moduleData[i].subModule[j].isFocused = "FALSE"
                    }
                }
            }
        }
        this.setState({
            moduleData,
        })
    }
    levelOneCheck(moduleName, check) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == moduleName) {
                if (moduleData[i].isChecked == "FALSE") {
                    moduleData[i].isChecked = "TRUE"
                    this.state.create = moduleData[i].crud.charAt(0)
                    this.state.read = moduleData[i].crud.charAt(1)
                    this.state.update = moduleData[i].crud.charAt(2)
                    this.state.deleteCrud = moduleData[i].crud.charAt(3)
                    if (moduleData[i].subModule.length != 0) {
                        let level2 = moduleData[i].subModule
                        for (let j = 0; j < level2.length; j++) {
                            level2[j].isChecked = "TRUE"
                            if (level2[j].subModule.length != 0) {
                                let level3 = level2[j].subModule
                                for (let l = 0; l < level3.length; l++) {
                                    level3[l].isChecked = "TRUE"
                                    if (level3[l].subModule.length != 0) {
                                        let level4 = level3[l].subModule
                                        for (let k = 0; k < level4.length; k++) {
                                            level4[k].isChecked = "TRUE"
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    moduleData[i].isChecked = "FALSE"
                    if (moduleData[i].subModule.length != 0) {
                        let level2 = moduleData[i].subModule
                        for (let j = 0; j < level2.length; j++) {
                            level2[j].isChecked = "FALSE"
                            if (level2[j].subModule.length != 0) {
                                let level3 = level2[j].subModule
                                for (let l = 0; l < level3.length; l++) {
                                    level3[l].isChecked = "FALSE"
                                    if (level3[l].subModule.length != 0) {
                                        let level4 = level3[l].subModule
                                        for (let k = 0; k < level4.length; k++) {
                                            level4[k].isChecked = "FALSE"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            moduleData,
            create: this.state.create,
            read: this.state.read,
            update: this.state.update,
            deleteCrud: this.state.deleteCrud,
            fosusedvalue: false
        })
    }
    levelFour(name) {
     
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name == name) {
                                if (moduleData[i].subModule[j].subModule[k].isExpand == "FALSE") {
                                    moduleData[i].subModule[j].subModule[k].isExpand = "TRUE"
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                       
                                        this.setState({
                                            levelFour: true,
                                            levelFourModuleName: name
                                        })
                                    }
                                }
                            } else if (moduleData[i].subModule[j].subModule[k].Name != name) {
                                moduleData[i].subModule[j].subModule[k].isExpand = "FALSE"
                                moduleData[i].subModule[j].subModule[k].isFocused = "FALSE"
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            moduleData,
        })
    }

    levelTwoCheck(modulee) {
        let moduleData = this.state.moduleData
        let levelTwoFlag = false
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                if (moduleData[i].subModule.length != 0) {
                    for (let j = 0; j < moduleData[i].subModule.length; j++) {
                        if (moduleData[i].subModule[j].Name == modulee) {
                            if (moduleData[i].subModule[j].isChecked == "FALSE") {
                                moduleData[i].subModule[j].isChecked = "TRUE"
                                if (moduleData[i].subModule[j].subModule.length == 0) {
                                    moduleData[i].subModule[j].isFocused = "TRUE"
                                    this.state.create = moduleData[i].subModule[j].crud.charAt(0)
                                    this.state.read = moduleData[i].subModule[j].crud.charAt(1)
                                    this.state.update = moduleData[i].subModule[j].crud.charAt(2)
                                    this.state.deleteCrud = moduleData[i].subModule[j].crud.charAt(3)
                                }
                                if (moduleData[i].subModule[j].subModule.length != 0) {
                                    for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                        moduleData[i].subModule[j].subModule[k].isChecked = "TRUE"
                                        if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                            for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                                moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "TRUE"
                                            }
                                        }
                                    }
                                }
                            } else {
                                moduleData[i].subModule[j].isChecked = "FALSE"
                                if (moduleData[i].subModule[j].subModule.length != 0) {
                                    for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                        moduleData[i].subModule[j].subModule[k].isChecked = "FALSE"
                                        if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                            for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                                moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "FALSE"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (let k = 0; k < moduleData[i].subModule.length; k++) {
                        if (moduleData[i].subModule[k].isChecked == "TRUE") {
                            levelTwoFlag = true
                            break
                        }

                    }
                }
                if (levelTwoFlag == true) {
                    moduleData[i].isChecked = "TRUE"
                } else {
                    moduleData[i].isChecked = "FALSE"

                }
            }
        }
        this.setState({
            moduleData: moduleData,
            create: this.state.create,
            read: this.state.read,
            update: this.state.update,
            deleteCrud: this.state.deleteCrud,
            fosusedvalue: true,
        })
        this.unfocused(modulee)
    }
    levelThreeCheck(mname) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name == mname) {
                                if (moduleData[i].subModule[j].subModule[k].isChecked == "FALSE") {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "TRUE"
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length == 0) {
                                        moduleData[i].subModule[j].subModule[k].isFocused = "TRUE"
                                        this.state.create = moduleData[i].subModule[j].subModule[k].crud.charAt(0)
                                        this.state.read = moduleData[i].subModule[j].subModule[k].crud.charAt(1)
                                        this.state.update = moduleData[i].subModule[j].subModule[k].crud.charAt(2)
                                        this.state.deleteCrud = moduleData[i].subModule[j].subModule[k].crud.charAt(3)
                                    }
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                        for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "TRUE"
                                        }
                                    }
                                } else {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "FALSE"
                                    if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                        for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "FALSE"
                                        }
                                    }
                                }


                            }
                        }
                        for (let f = 0; f < moduleData[i].subModule[j].subModule.length; f++) {
                            if (moduleData[i].subModule[j].subModule[f].isChecked == "TRUE") {
                                levelThreeFlag = true
                                break
                            }
                        }
                        if (levelThreeFlag == true) {
                            moduleData[i].subModule[j].isChecked = "TRUE"
                        } else {
                            moduleData[i].subModule[j].isChecked = "FALSE"

                        }

                    }

                }
                for (let k = 0; k < moduleData[i].subModule.length; k++) {
                    if (moduleData[i].subModule[k].isChecked == "TRUE") {
                        levelTwoFlag = true
                        break
                    }

                }
                if (levelTwoFlag == true) {

                    moduleData[i].isChecked = "TRUE"
                } else {

                    moduleData[i].checked = "FALSE"
                }
            }
        }
        this.setState({
            moduleData: moduleData,
            create: this.state.create,
            read: this.state.read,
            update: this.state.update,
            deleteCrud: this.state.deleteCrud,
            fosusedvalue: true,
        })
        this.unfocused(mname)
    }
    levelFourCheck(name) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name == this.state.levelFourModuleName) {
                                for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                    if (moduleData[i].subModule[j].subModule[k].subModule[l].Name == name) {
                                        if (moduleData[i].subModule[j].subModule[k].subModule[l].isChecked == "FALSE") {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "TRUE"
                                            if (moduleData[i].subModule[j].subModule[k].subModule[l].subModule.length == 0) {
                                                moduleData[i].subModule[j].subModule[k].subModule[l].isFocused = "TRUE"
                                                this.state.create = moduleData[i].subModule[j].subModule[k].subModule[l].crud.charAt(0)
                                                this.state.read = moduleData[i].subModule[j].subModule[k].subModule[l].crud.charAt(1)
                                                this.state.update = moduleData[i].subModule[j].subModule[k].subModule[l].crud.charAt(2)
                                                this.state.deleteCrud = moduleData[i].subModule[j].subModule[k].subModule[l].crud.charAt(3)
                                            }
                                        } else {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isChecked = "FALSE"
                                            moduleData[i].subModule[j].subModule[k].subModule[l].isFocused = "TRUE"
                                        }
                                    }
                                }
                                 for (let g = 0; g < moduleData[i].subModule[j].subModule.length; g++) {
                                    if (moduleData[i].subModule[j].subModule[k].subModule[g].isChecked == "TRUE") {
                                        levelFourFlag = true
                                        break
                                    }
                                }
                                if (levelFourFlag == true) {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "TRUE"
                                } else {
                                    moduleData[i].subModule[j].subModule[k].isChecked = "FALSE"

                                }
                            }
                        }
                                 for (let f = 0; f < moduleData[i].subModule[j].subModule.length; f++) {
                            if (moduleData[i].subModule[j].subModule[f].isChecked == "TRUE") {
                                levelThreeFlag = true
                                break
                            }
                        }
                        if (levelThreeFlag == true) {
                            moduleData[i].subModule[j].isChecked = "TRUE"
                        } else {
                            moduleData[i].subModule[j].isChecked = "FALSE"

                        }

                    }
                }
                    for (let k = 0; k < moduleData[i].subModule.length; k++) {
                    if (moduleData[i].subModule[k].isChecked == "TRUE") {
                        levelTwoFlag = true
                        break
                    }

                }
                if (levelTwoFlag == true) {

                    moduleData[i].isChecked = "TRUE"
                } else {

                    moduleData[i].checked = "FALSE"
                }
            }
        }
        this.setState({
            moduleData: moduleData,
            create: this.state.create,
            read: this.state.read,
            update: this.state.update,
            deleteCrud: this.state.deleteCrud,
            fosusedvalue: true,

        })
        this.unfocused(name)
    }
    updatePermission(crud) {
        if (crud == "create") {
            if (this.state.create == "1") {
                this.setState({
                    create: "0"
                })
            } else {
                this.setState({
                    create: "1"
                })
            }
        } else if (crud == "read") {
            if (this.state.read == "1") {
                this.setState({
                    read: "0"
                })
            } else {
                this.setState({
                    read: "1"
                })
            }
        } else if (crud == "update") {
            if (this.state.update == "1") {
                this.setState({
                    update: "0"
                })
            } else {
                this.setState({
                    update: "1"
                })
            }
        } else if (crud == "deleteCrud") {
            if (this.state.deleteCrud == "1") {
                this.setState({
                    deleteCrud: "0"
                })
            } else {
                this.setState({
                    deleteCrud: "1"
                })
            }
        }
        setTimeout(() => {
            let moduleData = this.state.moduleData
            for (let i = 0; i < moduleData.length; i++) {
                if (moduleData[i].isFocused == "TRUE") {
                    moduleData[i].crud = this.state.create + this.state.read + this.state.update + this.state.deleteCrud
                }
                if (moduleData[i].subModule.length != 0) {
                    for (let j = 0; j < moduleData[i].subModule.length; j++) {
                        if (moduleData[i].subModule[j].isFocused == "TRUE") {
                            moduleData[i].subModule[j].crud = this.state.create + this.state.read + this.state.update + this.state.deleteCrud
                        }
                        if (moduleData[i].subModule[j].subModule.length != 0) {
                            for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                if (moduleData[i].subModule[j].subModule[k].isFocused == "TRUE") {
                                    moduleData[i].subModule[j].subModule[k].crud = this.state.create + this.state.read + this.state.update + this.state.deleteCrud
                                }
                                if (moduleData[i].subModule[j].subModule[k].subModule.length != 0) {
                                    for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                        if (moduleData[i].subModule[j].subModule[k].subModule[l].isFocused == "TRUE") {
                                            moduleData[i].subModule[j].subModule[k].subModule[l].crud = this.state.create + this.state.read + this.state.update + this.state.deleteCrud
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.setState({
                moduleData,
                create: this.state.create,
                read: this.state.read,
                update: this.state.update,
                deleteCrud: this.state.deleteCrud
            })
        }, 10);
    }
    unfocused(moduleName) {
        let moduleData = this.state.moduleData
        for (let i = 0; i < moduleData.length; i++) {
            if (moduleData[i].moduleName == this.state.levelTwoModuleName) {
                if (moduleData[i].moduleName != moduleName) {
                    moduleData[i].isFocused = "FALSE"
                }
                for (let j = 0; j < moduleData[i].subModule.length; j++) {
                    if (moduleData[i].subModule[j].Name != moduleName) {
                        moduleData[i].subModule[j].isFocused = "FALSE"
                    }
                    if (moduleData[i].subModule[j].Name == this.state.levelThreeModuleName) {
                        for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                            if (moduleData[i].subModule[j].subModule[k].Name != moduleName) {
                                moduleData[i].subModule[j].subModule[k].isFocused = "FALSE"
                            }
                            if (moduleData[i].subModule[j].subModule[k].Name == this.state.levelFourModuleName) {
                                for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                    if (moduleData[i].subModule[j].subModule[k].subModule[l].Name != moduleName) {
                                        moduleData[i].subModule[j].subModule[k].subModule[l].isFocused = "FALSE"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.setState({
            moduleData
        })
    }
    saveRolesModal() {
        this.setState({
            unchecked: true
        })
        let data = {
            roleName: this.props.roleName
        }
        this.props.getSubModuleDataRequest(data)
    }
    updatedPayload() {
        let moduleData = this.state.moduleData
        let moduleDataStatic = this.state.moduleDataStatic
        let checkedModule=[]
        for (let i = 0; i < moduleData.length; i++) {
            for (let a = 0; a < moduleDataStatic.length; a++) {
                if (moduleData[i].subModule.length != 0 && moduleDataStatic[a].subModule.length != 0) {
                    for (let j = 0; j < moduleData[i].subModule.length; j++) {
                        for (let b = 0; b < moduleDataStatic[a].subModule.length; b++) {
                            if (moduleData[i].subModule[j].isChecked == moduleDataStatic[a].subModule[b].isChecked && moduleData[i].subModule[j].subModule.length == 0) {
                                moduleData[i].subModule[j].crud = moduleDataStatic[a].subModule[b].crud
                            }
                            if (moduleData[i].subModule[j].subModule.length != 0 && moduleDataStatic[a].subModule[b].subModule.length != 0) {
                                for (let k = 0; k < moduleData[i].subModule[j].subModule.length; k++) {
                                    for (let c = 0; c < moduleDataStatic[a].subModule[b].subModule.length; c++) {
                                        if (moduleData[i].subModule[j].subModule[k].isChecked == moduleDataStatic[a].subModule[b].subModule[c] && moduleData[i].subModule[j].subModule[k].subModule.length == 0) {
                                            moduleData[i].subModule[j].subModule[k].crud = moduleDataStatic[a].subModule[b].subModule[c].crud
                                        }
                                        if (moduleData[i].subModule[j].subModule[k].subModule.length != 0 && moduleDataStatic[a].subModule[b].subModule[c].subModule.length != 0) {
                                            for (let l = 0; l < moduleData[i].subModule[j].subModule[k].subModule.length; l++) {
                                                for (let d = 0; d < moduleDataStatic[a].subModule[b].subModule[c].subModule.length; d++) {
                                                    if (moduleData[i].subModule[j].subModule[k].subModule[l].isChecked == moduleDataStatic[a].subModule[b].subModule[c].subModule[d] && moduleData[i].subModule[j].subModule[k].subModule[l].subModule.length == 0) {
                                                        moduleData[i].subModule[j].subModule[k].subModule[l].crud = moduleDataStatic[a].subModule[b].subModule[c].subModule[d].crud
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
         for (let i = 0; i < moduleData.length; i++) {
                if (moduleData[i].isChecked == "TRUE") {
                    moduleData[i].crud = "1111"
                    checkedModule.push(moduleData[i])
                }
            }
          
        this.setState({
            moduleData,
            // unchecked:false
        })
        setTimeout(() => {
            let payload = {
                isUpdated: "TRUE",
                module: checkedModule,
                roleName: this.props.roleName,
            }
            this.props.updateRoleRequest(payload)
            this.setState({
                unchecked: false
            })
        }, 10);
    }

    render() {
        return (
            <div className="modal display_block">
                <div className="backdrop display_block"></div>
                <div className="modal_Indent display_block updateRoleModal">
                    <div className="col-md-12 col-sm-12 modalpoColor modal-content modalShow pad-0 viewRolesTable heightAuto">
                        <div className="modal_Color selectVendorPopUp">
                            <div className="modalTop alignMiddle">
                                <div className="col-md-6 pad-0">
                                    <h2>Manage Roles</h2>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="col-md-12 col-sm-12 modalTableNew">
                                <div className="table tableModal moduleSelect border-bot-table customTable">
                                    <div className="topHeader">
                                        <div><span>Role Name</span></div>
                                        <div><span>Modules</span></div>
                                        <div><span>Permissions</span></div>
                                    </div>
                                    <div className="bottomTable">
                                        <div>{this.props.roleName}</div>
                                        <div>
                                            <div className="pad-0">
                                                <div className="col-md-3 pad-0 m-top-9">
                                                    {this.state.moduleData.map((data, key) => (
                                                        <div className="col-md-12 pad-0 select" onClick={data.subModule.length != 0 ? (e) => this.levelTwo(data.moduleName) : null} key={key}>
                                                            <div className="circularCheckBox">
                                                                <ul className="list-inline width_100">
                                                                    <div className="siteCheckBox">
                                                                        <label className="checkBoxLabel0" ><input type="checkBox" checked={data.isChecked == "TRUE"} onClick={(e) => this.levelOneCheck(data.moduleName, data.isChecked)} /><span className="checkmark1"></span> </label>
                                                                    </div>
                                                                </ul>
                                                            </div>
                                                            <label className={data.isFocused == "TRUE" ? "onFocusLabel" : ""} >{data.moduleName}{data.subModule.length != 0 ? data.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                                        </div>
                                                    ))}
                                                </div>
                                                {this.state.levelTwo ? <div className="col-md-3 pad-0 m-top-9">
                                                    {this.state.moduleData.map((data, l1) => (data.moduleName == this.state.levelTwoModuleName ? data.subModule.map((levelTwo, keyy) => (
                                                        <div className="col-md-12 pad-0 Select" key={keyy} onClick={levelTwo.subModule.length != 0 ? (e) => this.levelThree(levelTwo.Name) : null}>
                                                            <div className="circularCheckBox">
                                                                <ul className="list-inline width_100">
                                                                    <div className="siteCheckBox">
                                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={levelTwo.isChecked == "TRUE"} onClick={(e) => this.levelTwoCheck(levelTwo.Name)} /><span className="checkmark1"></span> </label>
                                                                    </div>
                                                                </ul>
                                                            </div>
                                                            <label className={levelTwo.isFocused == "TRUE" ? "onFocusLabel" : ""}>{levelTwo.Name} {levelTwo.subModule.length != 0 ? levelTwo.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                                        </div>)) : null))}

                                                </div> : null}
                                                {this.state.levelThree ? <div className="col-md-3 pad-0 m-top-9">
                                                    {this.state.moduleData.map((data, ll1) => (data.moduleName == this.state.levelTwoModuleName ? data.subModule.map((levelTwo, l2) => (
                                                        levelTwo.Name == this.state.levelThreeModuleName ? levelTwo.subModule.map((levelThree, keyyy) => (
                                                            <div className="col-md-12 pad-0 Select" key={keyyy} onClick={levelThree.subModule.length != 0 ? (e) => this.levelFour(levelThree.Name) : null} >
                                                                <div className="circularCheckBox">
                                                                    <ul className="list-inline width_100">
                                                                        <div className="siteCheckBox">
                                                                            <label className="checkBoxLabel0"><input type="checkBox" checked={levelThree.isChecked == "TRUE"} onClick={(e) => this.levelThreeCheck(levelThree.Name)} /><span className="checkmark1"></span> </label>
                                                                        </div>
                                                                    </ul>
                                                                </div>
                                                                <label className={levelThree.isFocused == "TRUE" ? "onFocusLabel" : ""}>{levelThree.Name}  {levelThree.subModule.length != 0 ? levelThree.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                                            </div>)) : null)) : null))}
                                                </div> : null}
                                                {this.state.levelFour ? <div className="col-md-3 pad-0 m-top-9">
                                                    {this.state.moduleData.map((data, lll1) => (
                                                        data.moduleName == this.state.levelTwoModuleName ? data.subModule.map((levelTwo, ll2) => (
                                                            levelTwo.Name == this.state.levelThreeModuleName ? levelTwo.subModule.map((levelThree, l3) => (
                                                                levelThree.Name == this.state.levelFourModuleName ? levelThree.subModule.map((levelFour, keyyyy) => (
                                                                    <div className="col-md-12 pad-0 select" key={keyyyy} >
                                                                        <div className="circularCheckBox">
                                                                            <ul className="list-inline width_100">
                                                                                <div className="siteCheckBox">
                                                                                    <label className="checkBoxLabel0"><input type="checkBox" checked={levelFour.isChecked == "TRUE"} onClick={(e) => this.levelFourCheck(levelFour.Name)} /><span className="checkmark1"></span> </label>
                                                                                </div>
                                                                            </ul>
                                                                        </div>
                                                                        <label className={levelFour.isFocused == "TRUE" ? "onFocusLabel" : ""}>{levelFour.Name} {levelFour.subModule.length != 0 ? levelFour.isExpand == "FALSE" ? <img src={addModule} /> : <img src={rightArrow} /> : null}</label>
                                                                    </div>)) : null)) : null)) : null))}
                                                </div> : null}
                                            </div>
                                        </div>
                                        <div className="verticalTop">{this.state.fosusedvalue ? <ul className="pad-0 posRelative">
                                            <li><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" id="createId" checked={this.state.create == "1"} onChange={(e) => this.updatePermission("create")} /> <span className="checkmark1"></span> </label><label className="checkBoxName" htmlFor="create">Create</label></li>
                                            <li><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" id="readId" checked={this.state.read == "1"} onChange={(e) => this.updatePermission("read")} /> <span className="checkmark1"></span> </label><label className="checkBoxName" htmlFor="read">Read</label></li>
                                            <li><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" id="updateId" checked={this.state.update == "1"} onChange={(e) => this.updatePermission("update")} /> <span className="checkmark1"></span> </label><label className="checkBoxName" htmlFor="update">Update</label></li>
                                            <li><label className="checkBoxLabel0 displayPointer pad-0"><input type="checkBox" id="deleteId" checked={this.state.deleteCrud == "1"} onChange={(e) => this.updatePermission("deleteCrud")} /> <span className="checkmark1"></span> </label><label className="checkBoxName" htmlFor="deleteCrud">Delete</label></li>
                                        </ul> : null}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="footer displayInline">
                            <button type="button" className="cancel" onClick={() => this.props.manageModal("close")}>Cancel</button>
                            <button type="button" className="save" onClick={(e) => this.saveRolesModal(e)}>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}