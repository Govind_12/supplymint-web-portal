import React from 'react';
import Pagination from '../../pagination';

class ViewPermission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            permissionData: [],
        }
    }
    
    componentDidMount(){
        this.setState({
            permissionData: this.props.administration && 
            this.props.administration.getSubModuleByRoleName && 
            this.props.administration.getSubModuleByRoleName.data && 
            this.props.administration.getSubModuleByRoleName.data.resource &&
            this.props.administration.getSubModuleByRoleName.data.resource.resource ? 
            this.props.administration.getSubModuleByRoleName.data.resource.resource:[],

        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.getSubModuleByRoleName.isSuccess) {
            if (nextProps.administration.getSubModuleByRoleName.data.resource != null) {

                this.setState({
                    permissionData: nextProps.administration.getSubModuleByRoleName.data.resource.resource,

                })
            } else {
                this.setState({
                    permissionData: [],

                })
            }
        }
    }
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content manage-role-view">
                    <div className="mrv-head">
                        <div className="mrvh-left">
                            <p>Role</p>
                            <h3>{this.props.roleName}</h3>
                        </div>
                        <div className="mrvh-right">
                            <button className="mrvh-close" onClick={() => this.props.manageShowModal("close")}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                    <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="mrv-body">
                        <div className="mrvb-search">
                            <input type="search" placeholder="Type To Search" name="search" />
                            <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                        </div>
                        <div className="vendor-gen-table">
                            <div className="manage-table">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                <table className="table gen-main-table">
                                    <thead>
                                        <tr>
                                            <th><label>Module Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            <th><label>Parent Modules</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            <th><label>Page Names</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            <th><label>Permission</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.permissionData.length != 0 ? this.state.permissionData.map((data, key) => {
                                        return data.modules == null ?
                                        <tr key={key}>
                                                    <td><label className="bold">{data.moduleName}</label></td>
                                                    <td><label>{data.moduleName}</label></td>
                                                    <td><label>{data.moduleName}</label></td>
                                                    <td className="rcud">
                                                        {
                                                            data.crud.split("")[0] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Create</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Create</label>
                                                        }
                                                        {
                                                            data.crud.split("")[1] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Read</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Read</label>
                                                        }
                                                        {
                                                            data.crud.split("")[2] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Update</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Update</label>
                                                        }
                                                        {
                                                            data.crud.split("")[3] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Delete</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Delete</label>
                                                        }
                                                    </td>
                                                </tr>
                                                : 
                                        data.modules.map((data1) => {
                                           return data1.modules == null ?
                                                <tr key={key}>
                                                    <td><label className="bold">{data.moduleName}</label></td>
                                                    <td><label>{data1.moduleName}</label></td>
                                                    <td><label>{data1.moduleName}</label></td>
                                                    <td className="rcud">
                                                        {
                                                            data1.crud.split("")[0] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Create</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Create</label>
                                                        }
                                                        {
                                                            data1.crud.split("")[1] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Read</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Read</label>
                                                        }
                                                        {
                                                            data1.crud.split("")[2] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Update</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Update</label>
                                                        }
                                                        {
                                                            data1.crud.split("")[3] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Delete</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Delete</label>
                                                        }
                                                    </td>
                                                </tr> :
                                                data1.modules.map((data2) => {
                                                    return <tr key={key}>
                                                        <td><label className="bold">{data.moduleName}</label></td>
                                                        <td><label>{data1.moduleName}</label></td>
                                                        <td><label>{data2.moduleName}</label></td>
                                                        <td className="rcud">
                                                        {
                                                            data2.crud.split("")[0] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Create</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Create</label>
                                                        }
                                                        {
                                                            data2.crud.split("")[1] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Read</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Read</label>
                                                        }
                                                        {
                                                            data2.crud.split("")[2] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Update</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Update</label>
                                                        }
                                                        {
                                                            data2.crud.split("")[3] == "1" ?
                                                            <label><img src={require('../../../assets/tick1.svg')} /> Delete</label> :
                                                            <label><img src={require('../../../assets/cross.svg')} /> Delete</label>
                                                        }
                                                        </td>
                                                    </tr>
                                                })
                                        })
                                    }) : <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr>
                                    }
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="new-gen-pagination">
                            <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder"  value={1} />
                                    {/* <span className="ngp-total-item">Total Items </span> <span className="bold">1045</span> */}
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ViewPermission;