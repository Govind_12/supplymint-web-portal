import React from 'react';

export default class ShowAllPermission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            permissionData: [],


        }
    }
    componentDidMount(){
        console.log(this.state.permissionData,"test",this.props.administration.getSubModuleByRoleName);
        // this.setState({
        //     getModuleData: this.props.administration && 
        //     this.props.administration.moduleGetData && 
        //     this.props.administration.moduleGetData.data && 
        //     this.props.administration.moduleGetData.data.resource ? 
        //     this.props.administration.moduleGetData.data.resource:[],

        // })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.getSubModuleByRoleName.isSuccess) {
            if (nextProps.administration.getSubModuleByRoleName.data.resource != null) {
                this.setState({
                    permissionData: nextProps.administration.getSubModuleByRoleName.data.resource
                })


            }
        }
    }

    render() {
        
        return (
            <div className="modal display_block">
                <div className="backdrop display_block"></div>
                <div className="modal_Indent display_block">
                    <div className="col-md-12 col-sm-12 modalpoColor modal-content modalShow pad-0 viewRolesTable heightAuto">
                        <div className="modal_Color selectVendorPopUp">
                            <div className="modalTop alignMiddle">
                                <div className="col-md-6 pad-0">
                                    <h2>View All Permission</h2>
                                </div>
                            </div>
                        </div>
                        <div className="modalContentMid m-top-10">
                            <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                <table className="table tableModal">
                                    <thead>
                                        <tr>
                                            <th>Role Name</th>
                                            <th>Modules</th>
                                            <th>Permissions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.permissionData.length > 0 ?
                                            this.state.permissionData.map((data, key) => (

                                                <tr key={key}>
                                                    {key == 0 ? <td rowSpan={this.state.permissionData.length}>{this.props.roleName}</td> : null}
                                                    <td>{data.Name}</td>
                                                    <td><ul className="list-inline">{data.crud.charAt(0) == "1" ? <li>Create</li> : null}{data.crud.charAt(1) == "1" ? <li>Read</li> : null}{data.crud.charAt(2) == "1" ? <li>Update</li> : null}{data.crud.charAt(3) == "1" ? <li>Delete</li> : null}</ul></td>
                                                </tr>
                                            )) :
                                            <tr>
                                                <td rowSpan={this.state.permissionData.length}>{this.props.roleName}</td>
                                                <td>Module</td>
                                                <td><ul className="list-inline"><li>Create</li><li>Read</li><li>Update</li><li>Delete</li></ul></td>
                                            </tr>
                                        }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="footer">
                            <button type="button" onClick={() => this.props.manageShowModal("close")}>Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}