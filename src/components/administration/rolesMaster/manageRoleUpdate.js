import React from 'react';
import Pagination from '../../pagination';
import axios from 'axios'
import { CONFIG } from "../../../config/index";
import { message } from 'antd';
import searchIcon from '../../../assets/clearSearch.svg';

//This is Update Role page

class UpdateManageRole extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            getSubModuleData: [],
            roleNameAvailablityStatus: null,
            newArray: [],
            roleName: null,
            parseData: [],
            search: "",
        }
    }
    componentDidMount() {
        this.setState({
            getSubModuleData: this.props.administration &&
                this.props.administration.getSubModuleByRoleName &&
                this.props.administration.getSubModuleByRoleName.data &&
                this.props.administration.getSubModuleByRoleName.data.resource &&
                this.props.administration.getSubModuleByRoleName.data.resource.resource ?
                this.props.administration.getSubModuleByRoleName.data.resource.resource : [],

        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.getSubModuleByRoleName.isSuccess) {
            if (nextProps.administration.getSubModuleByRoleName.data.resource != null) {
                let data = nextProps.administration.getSubModuleByRoleName.data.resource.resource
                for (let i = 0; i < data.length; i++) {
                    if (Array.isArray(data[i].modules) && data[i].modules.length) {
                        let topModule = data[i].moduleName
                        this.parseSubmodules(data[i].modules, topModule, "")
                    }
                }

                this.setState({
                    getSubModuleData: nextProps.administration.getSubModuleByRoleName.data.resource.resource,

                }, () => {

                })

            } else {
                this.setState({
                    getSubModuleData: [],

                })
            }
        }
    }

    checkAll = (moduleId) => {
        let data = [...this.state.parseData]
        let index = data.findIndex((_) => _.moduleId == moduleId)
        console.log(moduleId)
        if (data[index].crud === "1111") {
            data[index].crud = "0000"
        } else {
            data[index].crud = "1111"
        }
        this.setState({ parseData: data })
    }
    parseSubmodules = (subModules, topModule, parentNode) => {
        for (let i = 0; i < subModules.length; i++) {
            if (Array.isArray(subModules[i].modules) && subModules.modules !== null) {
                this.parseSubmodules(subModules[i].modules, topModule, subModules[i].moduleName)
            } else {
                let page = { moduleId: subModules[i].moduleId, module: topModule, parentNode: parentNode == "" ? topModule : parentNode, page: subModules[i].moduleName, crud: subModules[i].crud }
                this.setState(prevState => ({ parseData: [...prevState.parseData, page] }))
            }
        }
    }
    onPermissionCheckboxChange = (e, moduleId, status, pos) => {
        let data = [...this.state.parseData]
        let index = data.findIndex((_) => _.moduleId == moduleId)
        let crud = data[index].crud
        crud = crud.split('');
        crud[pos] = parseInt(crud[pos]) === 1 ? 0 : 1;
        if(parseInt(crud[0])===1 || parseInt(crud[2])===1 || parseInt(crud[3])===1) {
            crud[1]=1;
        }
        data[index].crud = crud.join('')
        this.setState({
            parseData: data
        })
    }
    onClear = () => {
        let parseData = this.state.parseData.map((_) => ({ ..._, crud: "0000" }))
        this.setState({ parseData })
    }
    onSubmit = () => {
        let roles = this.state.parseData.filter((_) => _.crud !== "0000").map(__ => ({ moduleId: __.moduleId, crud: __.crud }))
        if (roles.length) {
            let payload = {
                roleId: this.props.roleId,
                roleName: this.props.roleName,
                roleAccess: roles
            }
            this.props.roleUpdateDataRequest(payload)
            //this.props.history.push("/administration/rolesMaster/manageRoles")
            this.props.manageModal("close")
        } else {
            message.error("Please check atleast one access is required")
        }
    }

    onSearch = (e) => {
        let value = e.target.value;
        this.setState({ search: e.target.value},()=>{
            let data = {
                no: 1,
                type: value.length ? 3 : 1,
                search: value,
                roleId: this.props.roleId,
                sortedBy: "",
                sortedIn: "",
            }
            this.props.getSubModuleByRoleNameRequest(data)
        })
    }

    onRefresh =(e)=>{
        let data = {
            no: 1,
            type: 1,
            search: "",
            roleId: this.props.roleId,
            sortedBy: "",
            sortedIn: "",
        }
        this.props.getSubModuleByRoleNameRequest(data)
        this.setState({ search: ""})
    }

    render() {
        return (
            <div className="modal">
                <form id="myForm">
                    <div className="backdrop modal-backdrop-new"></div>
                    <div className="modal-content manage-role-view">
                        <div className="mrv-head">
                            <div className="mrvh-left">
                                <p>Role</p>
                                <h3>{this.props.roleName}</h3>
                            </div>
                            <div className="mrvh-right">
                                <button type="button" onClick={this.onSubmit} className="mrvhb-save">Save</button>
                                <button type="button" onClick={this.onClear} className="mrvhb-clear">Clear</button>
                                <button className="mrvh-close" onClick={() => this.props.manageModal("close")}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                        <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div className="mrv-body">
                            <div className="mrvb-search">
                                {/* <input type="search" value={this.state.search} onChange={this.onSearch} placeholder="Type To Search" name="search" autoComplete="off"/>
                                <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                {this.state.search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.onRefresh} /></span> : null} */}
                            </div>
                            <div className="vendor-gen-table">
                                <div className="manage-table">
                                    {/* <div className="columnFilterGeneric">
                                        <span className="glyphicon glyphicon-menu-left"></span>
                                    </div> */}
                                    <table className="table gen-main-table">
                                        <thead>
                                            <tr>
                                                <th><label>Module Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                                <th><label>Parent Modules</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                                <th><label>Page Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                                <th><label>Permission</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            {this.state.parseData.length != 0 && this.state.parseData.map((data, key) => (
                                                <tr key={key}>
                                                    <td><label className="bold">{data.module}</label></td>
                                                    <td><label>{data.parentNode}</label></td>
                                                    <td><label>{data.page}</label></td>
                                                    <td>
                                                        <ul className="manage-role-check">
                                                            <li>
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" id={data.moduleId + "create"} checked={data.crud[0] == "1"} onChange={(e) => this.onPermissionCheckboxChange(e, data.moduleId, "create", 0)} name="selectEach" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                                <p>Create</p>
                                                            </li>
                                                            <li>
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach1" id={data.moduleId + "read"} checked={data.crud[1] == "1"} onChange={(e) => this.onPermissionCheckboxChange(e, data.moduleId, "read", 1)} />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                                <p>Read</p>
                                                            </li>
                                                            <li>
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach2" id={data.moduleId + "update"} checked={data.crud[2] == "1"} onChange={(e) => this.onPermissionCheckboxChange(e, data.moduleId, "update", 2)} />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                                <p>Update</p>
                                                            </li>
                                                            <li>
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach3" checked={data.crud[3] == "1"} id={data.moduleId + "delete"} onChange={(e) => this.onPermissionCheckboxChange(e, data.moduleId, "delete", 3)} />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                                <p>Delete</p>
                                                            </li>
                                                            <li>
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" name="selectEach4" checked={data.crud == "1111"} id={data.moduleId + "checkAll"} onClick={() => this.checkAll(data.moduleId)}/>
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                                <p>Check All</p>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>))}

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/*<div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value={1} />
                                         <span className="ngp-total-item">Total Items </span> <span className="bold">1045</span> 
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
export default UpdateManageRole;