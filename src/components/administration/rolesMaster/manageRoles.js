import React from 'react';
import UpdateRoleModal from './updateRoleModal';
import ShowAllPermission from './viewAllPermissionModal';
import ViewPermission from './manageRoleViewPermission';
import UpdateManageRole from './manageRoleUpdate';
import { NavLink } from "react-router-dom";
import NoPlanExist from '../../demandPlanning/otb/noPlanExist';
import Pagination from '../../pagination';
import Reload from '../../../assets/refresh-block.svg';
import searchIcon from '../../../assets/clearSearch.svg';
//import UpdateSuccessModal from '../../vendorPortal/modals/updateSuccessModal';
//This is Master Role Page 

export default class ManageRoles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            updateRole: false,
            getModuleRoles: [],
            roleNameArray: [],
            allPermissionModal: false,
            permissionData: [],
            roleId: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            no: 1,
            type: 1,
            search: "",
            roleName: null,
        }
    }

    componentWillMount() {
        //  this.props.moduleGetDataRequest('data')
        let data = {
            no: 1,
            type: 1,
            search: ""
        }
        this.props.moduleGetRoleRequest(data)
        sessionStorage.setItem('currentPage', "RADMMAIN")
        sessionStorage.setItem('currentPageName', "Manage Role")
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.moduleGetRole.isSuccess) {
            if (nextProps.administration.moduleGetRole.data.resource != null) {

                this.setState({
                    getModuleRoles: nextProps.administration.moduleGetRole.data.resource.data,
                    roleNameArray: nextProps.administration.moduleGetRole.data.resource.data.map(el => {
                        return el.name
                    })
                    ,
                    prev: nextProps.administration.moduleGetRole.data.prePage,
                    current: nextProps.administration.moduleGetRole.data.currPage,
                    next: nextProps.administration.moduleGetRole.data.currPage + 1,
                    maxPage: nextProps.administration.moduleGetRole.data.maxPage,
                })
            } else {
                this.setState({
                    getModuleRoles: [],
                    roleNameArray: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.administration.roleDelete.isSuccess) {
            let data = {
                no: 1,
                type: 1,
                search: ""
            }
            this.props.moduleGetRoleRequest(data)
            this.props.roleDeleteClear()
        }
        // if (nextProps.administration.roleUpdateData.isSuccess) {
        //     this.setState({
        //         updateRole: false,
        //     })
        //     this.props.updateRoleClear()
        // }

    }
    manageModal =(e, pdata)=> {
        if (e == "open") {
            this.setState({
                updateRole: true,
                roleId: pdata.id,
                roleName: pdata.name
            })
            let data = {
                roleId: pdata.id
            }
            this.props.getSubModuleByRoleNameRequest(data)
        } else if (e == "close") {
            this.setState({
                updateRole: false
            })
        }
    }
    // manageModal(e, name) {
    //     if (e == "open") {
    //         this.setState({
    //             updateRole: true,
    //             roleName: name
    //         })
    //         let data = {
    //             roleName: name
    //         }
    //         this.props.getSubModuleDataRequest(data)
    //     } else if (e == "close") {
    //         this.setState({
    //             updateRole: false
    //         })
    //     }
    // }
    manageShowModal(e, pdata) {
        if (e == "open") {
            this.setState({
                allPermissionModal: true,
                roleId: pdata.id,
                roleName: pdata.name
            })
            let payload = {
                roleId: pdata.id
            }
            this.props.getSubModuleByRoleNameRequest(payload)



        } else if (e == "close") {
            this.setState({
                allPermissionModal: false
            })
        }

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.administration.moduleGetRole.data.prePage,
                current: this.props.administration.moduleGetRole.data.currPage,
                next: this.props.administration.moduleGetRole.data.currPage + 1,
                maxPage: this.props.administration.moduleGetRole.data.maxPage,
            })
            if (this.props.administration.moduleGetRole.data.currPage != 0) {
                let data = {
                    no: this.props.administration.moduleGetRole.data.currPage - 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.moduleGetRoleRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.moduleGetRole.data.prePage,
                current: this.props.administration.moduleGetRole.data.currPage,
                next: this.props.administration.moduleGetRole.data.currPage + 1,
                maxPage: this.props.administration.moduleGetRole.data.maxPage,
            })
            if (this.props.administration.moduleGetRole.data.currPage != this.props.administration.moduleGetRole.data.maxPage) {

                let data = {
                    no: this.props.administration.moduleGetRole.data.currPage + 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.moduleGetRoleRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.administration.moduleGetRole.data.prePage,
                current: this.props.administration.moduleGetRole.data.currPage,
                next: this.props.administration.moduleGetRole.data.currPage + 1,
                maxPage: this.props.administration.moduleGetRole.data.maxPage,
            })
            if (this.props.administration.moduleGetRole.data.currPage <= this.props.administration.moduleGetRole.data.maxPage) {
                let data = {
                    no: 1,
                    search: this.state.search,
                    type: this.state.type,
                }
                this.props.moduleGetRoleRequest(data)
            }

        }
    }
    deleteRole = (id) => {
        let payload = {
            roleId: id
        }
        this.props.roleDeleteRequest(payload)
    }
    onRefresh =()=> {
        let data = {
            no: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
        }
        this.props.moduleGetRoleRequest(data)
        this.setState({ search: ""})
    }

    onSearch = (e) => {
        let value = e.target.value;
        this.setState({ search: e.target.value},()=>{
            let data = {
                no: 1,
                type: value.length ? 3 : 1,
                search: value,
                sortedBy: "",
                sortedIn: "",
            }
            this.props.moduleGetRoleRequest(data)
        })   
    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }
    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
          this.setState({ updateRole: false, allPermissionModal: false, })
        }
    }

    render() {

        return (
            <div className="container-fluid pad-0" id="vendor_manage">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-7 pad-0">
                            <div className="new-gen-left">
                                <div className="ngl-search">
                                    <input type="search" value={this.state.search} onChange={this.onSearch} placeholder="Type To Search" name="search" autoComplete="off"/>
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {this.state.search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.onRefresh} /></span> : null}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 pad-0">
                            <div className="new-gen-right">
                                <NavLink to="/administration/rolesMaster/addRoles"><span className="add-btn"><img src={require('../../../assets/add-green.svg')} />
                                    <span className="generic-tooltip">Create Role</span>
                                </span>
                                </NavLink>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            {/* <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div> */}
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn manage-role-fix">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img onClick={() => this.onRefresh()} src={Reload} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        <th><label>Role Name</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Role Type </label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                        <th><label>Permissions</label><img src={require('../../../assets/headerFilter.svg')} /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.getModuleRoles.length != 0 ? this.state.getModuleRoles.map((data, key) => (
                                        <tr key={key}>
                                            <td className="fix-action-btn manage-role-fix">
                                                <ul className="table-item-list">
                                                    <li className={data.description === "1" ? "disableSvg til-inner til-edit-btn" : "til-inner til-edit-btn"} onClick={data.description === "1" ? undefined : () => this.manageModal("open", data)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        {data.description === "1" ? <span className="generic-tooltip">Edit Disabled</span> : <span className="generic-tooltip">Edit</span>}
                                                    </li>
                                                    {data.description === "1" ?
                                                        <li className="disableSvg til-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 15.691 18.83">
                                                                <path fill="#d8d3d3" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Delete Disabled</span>
                                                        </li> :
                                                        <li className="til-inner til-delete-btn" onClick={() => this.deleteRole(data.id)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 15.691 18.83">
                                                                <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Delete</span>
                                                        </li>}

                                                </ul>
                                            </td>
                                            <td><label className="bold">{data.name}</label></td>
                                            {/* <td>{this.state.getModuleRoles.map((dataa, keyy) => (<label key={keyy}>{dataa.MODULE_NAME},</label>))}</td> */}
                                            <td>{data.description === "1" ? <label>System Defined</label> : <label>User Defined</label>}</td>
                                            <td className="mr-vap"><label className="displayPointer" onClick={(e) => this.manageShowModal("open", data)}>View all Permissions</label></td>
                                        </tr>)) : <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr>}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="new-gen-pagination">
                        <div className="ngp-left">
                            <div className="table-page-no">
                                <span>Page :</span><input type="number" className="paginationBorder" min="1" value="1" />
                                {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                            </div>
                        </div>
                        <div className="ngp-right">
                            <div className="nt-btn">
                                <div className="pagination-inner">
                                    <ul className="pagination-item">
                                        {this.state.current == 1 || this.state.current == 0 ? <li >
                                            <button className="" type="button">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className=" " type="button" onClick={(e) => this.page(e)} id="first" >
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        {this.state.prev != 0 ? <li >
                                            <button className="" type="button" onClick={(e) => this.page(e)} id="prev">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        <li>
                                            <button className="pi-number-btn" type="button">
                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                            </button>
                                        </li>
                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                            <button className="" type="button" onClick={(e) => this.page(e)} id="next">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li>
                                                <button className="" type="button" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li> : <li>
                                                <button className="" type="button" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        <li>
                                            <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.updateRole ? <UpdateManageRole {...this.state}{...this.props} manageModal={(e) => this.manageModal(e)} /> : null}
                {this.state.allPermissionModal ? <ViewPermission {...this.state} {...this.props} manageShowModal={(e) => this.manageShowModal(e)} /> : null}
                {/* {this.state.updateRole ? <UpdateRoleModal {...this.props} {...this.state} manageModal={(e) => this.manageModal(e)} /> : null} */}
                {/* {this.state.allPermissionModal ? <ShowAllPermission  {...this.props} permissionData={this.state.permissionData} roleName={this.state.roleName} manageShowModal={(e) => this.manageShowModal(e)} /> : null} */}
            </div>
        )
    }
} 