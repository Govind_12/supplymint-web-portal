import React from 'react';

class ViewSystemEmail extends React.Component {

    componentDidMount() {
        let frame = document.getElementById("emailBody"); 
        frame.onload = function() { 
            frame.style.height = frame.contentWindow.document.body.scrollHeight + 10 + 'px';
            frame.style.width = "100%";
            frame.style.pointerEvents = "none";
        } 
    }

    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content manage-api-log-modal view-system-email-modal">
                    <div className="malm-head">
                        <h3>View System Email</h3>
                        <button type="button" className="" onClick={this.props.CloseSystemEmail}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="10.047" height="10.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="malm-body">
                        <div className="malmb-inner">
                            <h3>Subject</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <span className="code vsem-sub">{this.props.data.subject == undefined ? "" : this.props.data.subject}</span>
                            </div>
                        </div>
                        <div className="malmb-inner">
                            <h3>To</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <span className="code vsem-to">{this.props.data.to == undefined ? "" : this.props.data.to}</span>
                            </div>
                        </div>
                        <div className="malmb-inner">
                            <h3>Cc</h3>
                            <div className="malmb-item">
                                <span className="malmb-copy-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.994" height="19" viewBox="0 0 15.994 19">
                                        <path id="prefix__copy" fill="#9eb5de" d="M10.057 19H2.969A2.972 2.972 0 0 1 0 16.031V5.975a2.972 2.972 0 0 1 2.969-2.969h7.088a2.972 2.972 0 0 1 2.969 2.969v10.056A2.972 2.972 0 0 1 10.057 19zM2.969 4.49a1.486 1.486 0 0 0-1.485 1.485v10.056a1.486 1.486 0 0 0 1.484 1.484h7.088a1.486 1.486 0 0 0 1.484-1.484V5.975a1.486 1.486 0 0 0-1.483-1.485zm13.025 9.686V2.969A2.972 2.972 0 0 0 13.025 0H4.787a.742.742 0 0 0 0 1.484h8.238a1.486 1.486 0 0 1 1.485 1.485v11.207a.742.742 0 0 0 1.484 0zm0 0"/>
                                    </svg>
                                </span>
                                <span className="code vsem-cc">{this.props.data.cc == undefined ? "" : this.props.data.cc}</span>
                            </div>
                        </div>
                        <div className="malmb-inner">
                            <h3>Body</h3>
                            <div className="malmb-item">
                                <iframe id="emailBody" srcDoc={this.props.data.template} frameBorder="false" ></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewSystemEmail;