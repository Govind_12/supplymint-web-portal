import React from "react";
import EmptyBox from "./organization/emptyBox";
import AddOrganization from "./organization/addOrganization";
import ManageOrganization from "./organization/manageOrganization";
import organizationData from "../../json/organisationData.json";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import { isUserAlreadyLoggedIn } from '../../generic/index';
class Organization extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      organizationState: [],
      addOrganization: false,
      loader: false,
      emptyBox: false,
      manageOrganisation: false,
      errorMessage: "",
      page: 1,
      errorCode: "",
      success: false,
      successMessage: "",
      alert: false,
      code: "",
    };
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    })
  }
  onError(e) {
    console.log("errrrrrrrrrrrrrrrsgrsfx")
    e.preventDefault();
    console.log("e3")
    let flag = false
    this.setState({
      alert: flag
    })
    console.log("3242")

    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  componentDidMount() {
    if (!this.props.administration.organization.isSuccess) {
      let data = {
        type: 1,
        no: 1
      }
      this.props.organizationRequest(data);
    } else {
      let data = {
        type: 1,
        no: 1
      }
      this.props.organizationRequest(data);
      this.setState({
        loader: false
      })

      this.setState({
        organizationState: this.props.administration.organization.data.resource,
        emptyBox: false,
        manageOrganisation: true
      })

    }
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.administration.organization.isSuccess) {
      return {
        loader: false,
        organizationState: nextProps.administration.organization.data.resource,
        manageOrganisation: true,
        emptyBox: false
      }


    } else if (nextProps.administration.organization.isError) {
      console.log("orgg errryrtyr")
      return {
        loader: false,
        errorMessage: nextProps.administration.organization.message.error == undefined ? undefined : nextProps.administration.organization.message.error.errorMessage,
        errorCode: nextProps.administration.organization.message.error == undefined ? undefined : nextProps.administration.organization.message.error.errorCode,
        code: nextProps.administration.organization.message.status,
        alert: true,
        manageOrganisation: true
      }
    }
    if (nextProps.administration.deleteOrganization.isSuccess) {
      return {
        success: true,
        loader: false,
        successMessage: nextProps.administration.deleteOrganization.data.message,
        alert: false,
      }

    }
    else if (nextProps.administration.deleteOrganization.isError) {
      return {
        errorMessage: nextProps.administration.deleteOrganization.message.error == undefined ? undefined : nextProps.administration.deleteOrganization.message.error.errorMessage,
        alert: true,
        errorCode: nextProps.administration.deleteOrganization.message.error == undefined ? undefined : nextProps.administration.deleteOrganization.message.error.errorCode,
        code: nextProps.administration.deleteOrganization.message.status,
        success: false,
        loader: false,
      }

    }
    if (nextProps.administration.editOrganization.isSuccess) {
      return {
        success: true,
        loader: false,
        successMessage: nextProps.administration.editOrganization.data.message,
        alert: false,
      }
    }
    else if (nextProps.administration.editOrganization.isError) {
      return {
        errorMessage: nextProps.administration.editOrganization.message.error == undefined ? undefined : nextProps.administration.editOrganization.message.error.errorMessage,
        alert: true,
        errorCode: nextProps.administration.editOrganization.message.error == undefined ? undefined : nextProps.administration.editOrganization.message.error.errorCode,
        code: nextProps.administration.editOrganization.message.status,
        success: false,
        loader: false,
      }

    }
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {


        return {

          loader: false,

        


      }
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      return {
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage


      }
    }
    if (nextProps.replenishment.createHeaderConfig.isSuccess) {
      return {
        successMessage: nextProps.replenishment.createHeaderConfig.data.message,
        loader: false,
        success: true,
      }

    } else if (nextProps.replenishment.createHeaderConfig.isError) {
      return {
        loader: false,
        alert: true,
        code: nextProps.replenishment.createHeaderConfig.message.status,
        errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
      }

    }

    if (nextProps.administration.organization.isLoading || nextProps.administration.editOrganization.isLoading || nextProps.administration.deleteOrganization.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.replenishment.createHeaderConfig.isLoading) {
      return {
        loader: true
      }
    }
    return {}
  }

  componentDidUpdate(previousProps, previousState) {
    if (this.props.administration.editOrganization.isError || this.props.administration.editOrganization.isSuccess) {
      this.props.editOrganizationClear()
    }
    if (this.props.administration.deleteOrganization.isError || this.props.administration.deleteOrganization.isSuccess) {
      this.props.deleteOrganizationClear()
    }
    if (this.props.administration.organization.isError || this.props.administration.organization.isSuccess) {
      this.props.organizationClear()
    }
    if (this.props.replenishment.getHeaderConfig.isError || this.props.replenishment.getHeaderConfig.isSuccess) {
      this.props.getHeaderConfigClear()
    }
    if (this.props.replenishment.createHeaderConfig.isSuccess) {
      let payload = {
        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
        displayName: "ORG_TABLE_HEADER",
        attributeType: "TABLE HEADER",
      }
      this.props.getHeaderConfigRequest(payload)
      this.props.createHeaderConfigClear()
    } else if (this.props.replenishment.createHeaderConfig.isError) {
      this.props.createHeaderConfigClear()
    }
  }

  onAddOrganization() {
    this.props.history.push('/administration/organisation/addOrganisation')
  }

  onSaveOrganization() {
    this.setState({
      organizationState: organizationData,
    });
    this.props.history.push('/administration/organisation/manageOrganisation')
  }

  render() {
    console.log(this.state.alert)
    // let element = document.getElementById('app');
    // this.state.loader ?element.classList.add('blurContent'): element.classList.remove('blurContent');

    return (
      // <div className="container-fluid">
      <div className="container-fluid pad-0">
        {this.state.emptyBox ?
          <EmptyBox
            {...this.props}
            organizationState={this.state.organizationState}
            addOrganization={() => this.onAddOrganization()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />
          : null} {this.state.manageOrganisation ?
            <ManageOrganization
              organizationState={this.state.organizationState}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            //     : hash == "addOrganisation" ?
            // <AddOrganization
            //   {...this.props}
            //   saveOrganization={() => this.onSaveOrganization()}
            //   clickRightSideBar={() => this.props.rightSideBar()}
            // />
            : null}
        {this.state.loader ? <FilterLoader /> : null}

        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default Organization;
