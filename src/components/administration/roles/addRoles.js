import React from "react";

import RightSideBar from "../../rightSideBar";
import Footer from '../../footer'
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";

class AddRoles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            role: "",
            usernameerr: false,
            roleerr: false,
            loader: false,
            alert: false,
            errorMessage: "",
            successMessage: "",
            success: false,
            userId: "",
            errorCode: "",
            roleData: [],
            userData: [],
            code: ""
        };
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    componentWillMount() {

        // if(sessionStorage.getItem('token') == null){
        //     this.props.history.push('/');
        // }

        if (this.props.administration.allRole.isSuccess && this.props.administration.allUser.isSuccess) {
            this.setState({
                roleData: this.props.administration.allRole.data.resource,
                userData: this.props.administration.allUser.data.resource
            })
        } else {
            this.props.allRoleRequest();
            this.props.allUserRequest();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.allRole.isSuccess && nextProps.administration.allUser.isSuccess) {
            this.setState({
                loader: false,
                roleData: nextProps.administration.allRole.data.resource,
                userData: nextProps.administration.allUser.data.resource
            })
        } else if (nextProps.administration.allRole.isLoading || nextProps.administration.allUser.isLoading) {
            this.setState({
                loader: true,
            })
        }
        if (nextProps.administration.addRoles.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.addRoles.data.message,
                success: true,
                loader: false
            })
            this.props.addRolesRequest();
            this.onClear();
        } else if (nextProps.administration.addRoles.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.addRoles.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.addRoles.message.status,
                errorCode: nextProps.administration.addRoles.message.error == undefined ? undefined : nextProps.administration.addRoles.message.error.errorCode,
                errorMessage: nextProps.administration.addRoles.message.error == undefined ? undefined : nextProps.administration.addRoles.message.error.errorMessage,
            })
            this.props.addRolesRequest();
        }
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    handleChange(e) {
        e.preventDefault();
        if (e.target.id == "role") {
            this.setState(
                {
                    role: e.target.value
                },
                () => {
                    this.role();
                }
            );
        } else if (e.target.id == "username") {
            for (let i = 0; i < this.state.userData.length; i++) {
                if (this.state.userData[i].userName == e.target.value) {
                    this.setState({
                        userId: this.state.userData[i].userId
                    })
                }
            }
            this.setState(
                {
                    username: e.target.value
                },
                () => {
                    this.username();
                }
            );
        }
    }

    username() {
        if (
            this.state.username == "" || this.state.username.trim() == "" || !this.state.username.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                usernameerr: true
            });
        } else {
            this.setState({
                usernameerr: false
            });
        }
    }
    role() {
        if (
            this.state.role == "" || this.state.role.trim() == "") {
            this.setState({
                roleerr: true
            });
        } else {
            this.setState({
                roleerr: false
            });
        }
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.role();
        this.username();
        const t = this;
        setTimeout(function () {
            const { role, username, usernameerr, userId, roleerr } = t.state;
            if (!usernameerr && !roleerr) {
                let data = {
                    userId: userId,
                    role: role,
                    username: username
                }
                t.props.addRolesRequest(data);
            }
        }, 100)
    }

    onClear() {
        // e.preventDefault();
        this.setState({
            username: "",
            role: "",
        })
    }

    render() {

        const { username, usernameerr, role, roleerr } = this.state;

        return (
            <div className="container-fluid">
                <SideBar {...this.props} />
                <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                <div className="container_div m-top-75 " id="">
                    <div className="col-md-12 col-sm-12">
                        <div className="menu_path">
                            <ul className="list-inline width_100 pad20">
                                <BraedCrumps {...this.props} />
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="container_div">

                        <div className="container-fluid">
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                <div className="container_content">
                                    <div className="col-md-6 col-sm-12 pad-0">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    ASSIGN ROLES
                            </label>
                                            </li>
                                            <li>
                                                {/* <p className="master_para">lorem ipsum doler immet</p> */}
                                            </li>
                                        </ul>
                                    </div>
                                    <form onSubmit={(e) => this.onFormSubmit(e)}>
                                        <div className="StickyDiv">
                                            <div className="col-md-12 col-sm-12 col-xs-3 pad-0 m-top-20">
                                                <div className="col-md-2 pad-lft-0">
                                                    {this.state.userData.length == 0 ? null : <select value={username} onChange={(e) => this.handleChange(e)} id="username" className={usernameerr ? "vendor_select_box errorBorder" : "vendor_select_box"}>
                                                        <option value="">Select UserName</option>
                                                        {this.state.userData.map((data, key) => (
                                                            <option key={key} value={data.userName}>{data.userName}</option>
                                                        ))}
                                                    </select>}
                                                    {/* {usernameerr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                    {usernameerr ? (
                                                        <span className="error">
                                                            Enter valid username
                          </span>
                                                    ) : null}
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    {this.state.roleData.length == 0 ? null : <select value={role} onChange={(e) => this.handleChange(e)} id="role" className={roleerr ? "vendor_select_box errorBorder" : "vendor_select_box"}>
                                                        <option value="">Select Role</option>
                                                        {this.state.roleData.map((data, key) => (<option key={key} value={data.name}>{data.name}</option>))}
                                                    </select>}
                                                    {/* {roleerr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                    {roleerr ? (
                                                        <span className="error">
                                                            Enter valid Role
                          </span>
                                                    ) : null}

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <div className="footerDivForm">
                                                <ul className="list-inline m-lft-0 m-top-10">
                                                    <li>
                                                        <button type="button" onClick={() => this.onClear()} className="clear_button_vendor" type="reset">
                                                            Clear
                                            </button>
                                                    </li>
                                                    <li>
                                                        <button className="save_button_vendor">Save</button>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>


        );
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddRoles);
