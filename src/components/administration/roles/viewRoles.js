import React from "react";
import Reassign from './reassign';
import Filter from './filter';
import openRack from '../../../assets/open-rack.svg'
import { CONFIG } from "../../../config/index";
import axios from 'axios';
import BraedCrumps from "../../breadCrumps";
import refreshIcon from "../../../assets/refresh.svg";
import ToastLoader from "../../loaders/toastLoader";

class ViewRoles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: false,
      filterBar: false,
      reassign: false,
      rolesState: this.props.rolesState,
      id: "",
      prev: "",
      current: "",
      next: "",
      maxPage: "",
      type: 1,
      no: 0,
      search: "",
      userName: "",
      name: "",
      createdBy: "",
      status: "",
      reassignModalAnimation: false,
      toastLoader:false,
      toastMsg:""
    };
  }

  handleSearch(e) {
    this.setState({
      search: e.target.value
    })
  }
  onSearch(e) {
    e.preventDefault();
    if (this.state.search == "") {
      this.setState({
        toastMsg:"Enter text on search input ",
        toastLoader: true
    })
    setTimeout(() => {
       this.setState({
        toastLoader:false
       })
    }, 1500);
    } else {
      this.setState({
        type: 3,
        userName: "",
        name: "",
        createdBy: "",
        status: ""
      })
      let data = {
        type: 3,
        no: 1,
        userName: "",
        name: "",
        createdBy: "",
        status: "",
        search: this.state.search,
      }
      this.props.rolesRequest(data)
    }
  }

  updateFilter(data) {
    this.setState({
      type: data.type,
      no: data.no,
      userName: data.userName,
      search: data.search,
      name: data.name,
      createdBy: data.createdBy,
      status: data.status
    })
  }

  onFilterClick() {
    this.setState({
      filter: !this.state.filter,
      filterBar: !this.state.filterBar
    })
  }
  onReassignClick(e) {
    this.setState({
      reassign: !this.state.reassign,
      id: e,
      reassignModalAnimation: !this.state.reassignModalAnimation
    })
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.roles.isSuccess) {
      this.setState({
        rolesState: nextProps.administration.roles.data.resource,
        prev: nextProps.administration.roles.data.prePage,
        current: nextProps.administration.roles.data.currPage,
        next: nextProps.administration.roles.data.currPage + 1,
        maxPage: nextProps.administration.roles.data.maxPage,
      })
    }
  }

  page(e) {
    if (e.target.id == "prev") {
      if(this.state.current == "" || this.state.current == undefined ||  this.state.current == 1){
                
      }else{
        this.setState({
          prev: this.props.administration.roles.data.prePage,
          current: this.props.administration.roles.data.currPage,
          next: this.props.administration.roles.data.currPage + 1,
          maxPage: this.props.administration.roles.data.maxPage,
        })
        if (this.props.administration.roles.data.currPage != 0) {
          let data = {
            no: this.props.administration.roles.data.currPage - 1,
            type: this.state.type,
            search: this.state.search,
            userName: this.state.userName,
            name: this.state.name,
            createdBy: this.state.createdBy,
            status: this.state.status
          }
          this.props.rolesRequest(data);
        }

      }
    } else if (e.target.id == "next") {
      this.setState({
        prev: this.props.administration.roles.data.prePage,
        current: this.props.administration.roles.data.currPage,
        next: this.props.administration.roles.data.currPage + 1,
        maxPage: this.props.administration.roles.data.maxPage,
      })
      if (this.props.administration.roles.data.currPage != this.props.administration.roles.data.maxPage) {
        let data = {
          no: this.props.administration.roles.data.currPage + 1,
          type: this.state.type,
          search: this.state.search,
          userName: this.state.userName,
          name: this.state.name,
          createdBy: this.state.createdBy,
          status: this.state.status
        }
        this.props.rolesRequest(data)
      }
    }
    else if (e.target.id == "first") {
      if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

      } else {
        this.setState({
          prev: this.props.administration.roles.data.prePage,
          current: this.props.administration.roles.data.currPage,
          next: this.props.administration.roles.data.currPage + 1,
          maxPage: this.props.administration.roles.data.maxPage,
        })
        if (this.props.administration.roles.data.currPage <= this.props.administration.roles.data.maxPage) {
          let data = {
            no: 1,
            type: this.state.type,
            search: this.state.search,
            userName: this.state.userName,
            name: this.state.name,
            createdBy: this.state.createdBy,
            status: this.state.status
          }
          this.props.rolesRequest(data)
        }
      }
    }
  }

  componentWillMount() {
    if (this.props.administration.roles.isSuccess) {
      this.setState({

        prev: this.props.administration.roles.data.prePage,
        current: this.props.administration.roles.data.currPage,
        next: this.props.administration.roles.data.currPage + 1,
        maxPage: this.props.administration.roles.data.maxPage,
      })
    }
  }

  onClearSearch(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: ""
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.rolesRequest(data);
  }

  onClearFilter(e) {
    this.setState({
      type: 1,
      no: 1,
      search: "",
      userName: "",
      name: "",
      createdBy: "",
      status: "",
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.rolesRequest(data);
  }

  onDeleteRoles(id) {
    const idd = id;
    this.props.deleteRolesRequest(idd);
  }
  xlscsv(type) {
    let headers = {
      'X-Auth-Token': sessionStorage.getItem('token'),
      'Content-Type': 'application/json'
    }
    axios.get(`${CONFIG.BASE_URL}/download/module/${type}/UserRole`, { headers: headers })
      .then(res => {
        window.open(`${res.data.data.resource}`)
      }).catch((error) => {
      });

  }

  truncate = (str, no_words) => {
    if (str.length <= 70) {
      return str;

    } else {
      let semi = str.split(",").splice(0, no_words).join(",");
      return `${semi}...`
    }
  }

  small = (str) => {
    if (str.length <= 70) {
      return false;
    }
    return true;
  }

  onRefresh() {
    let data = {
      no: this.state.current,
      type: this.state.type,
      search: this.state.search,
      userName: this.state.userName,
      name: this.state.name,
      createdBy: this.state.createdBy,
      status: this.state.status
    }
    this.props.rolesRequest(data)
  }

  render() {
    $("input").attr("autocomplete", "off");
    return (
      <div className="container-fluid">
        <div className="container_div " id="vendor_manage">
          {/* <div className="col-md-12 col-sm-12 pad-0">
            <div className="menu_path">
              <ul className="list-inline width_100"> */}
          {/* <li>
                  <label className="home_link">Home > Manage User Roles</label>
                </li> */}
          {/* <BraedCrumps/> */}
          {/* <li className="float_right">
                  <img src={openRack} onClick={() => this.props.rightSideBar()} className="right_sidemenu" />
                </li> */}
          {/* </ul>
            </div>
          </div> */}
          <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
            <div className="container_content heightAuto">
              <div className="col-md-6 col-xs-12 pad-0">
                <ul className="list_style">
                  <li>
                    <label className="contribution_mart">
                      MANAGE USER ROLES
                            </label>
                  </li>
                  <li>
                    {/* <p className="master_para">lorem ipsum doler immet</p> */}
                  </li>

                  <li className="m-top-20">
                    <label className="export_data_div">
                      Export Data
                            </label>
                    <ul className="list-inline m-top-10">
                      <li>{this.state.rolesState!=null ?
                        <button type="button" onClick={(e) => this.xlscsv("CSV")} className="button_home">
                          CSV
                        </button>:<button type="button" className="button_home btnDisabled">
                          CSV
                        </button>}
                      </li>
                      <li>
                        {this.state.rolesState!=null?
                        <button type="button" onClick={(e) => this.xlscsv("XLS")} className="button_home">
                          XLS
                                    </button>:
                                    <button type="button"  className="button_home btnDisabled">
                          XLS
                                    </button>
                                    
                                    }
                      </li>
                      <li>
                        <button className="filter_button" onClick={() => this.onFilterClick()} data-toggle="modal" data-target="#myRoleModal">
                          FILTER

                        <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                            <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                          </svg>
                        </button>
                      </li>
                      <li className="refresh-table">
                        <img onClick={() => this.onRefresh()} src={refreshIcon} />
                        <p className="tooltiptext topToolTipGeneric">
                            Refresh                                                  
                        </p>   
                      </li>
                    </ul>
                  </li>
                </ul>
                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
              </div>

              <div className="col-md-6 col-sm-12 pad-0">
                {/* <ul className="list-inline circle_list">
                  <li>
                    <div className="tooltip" onClick={(e) => this.props.onAddRoles(e)}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                        <g fill="none" fillRule="evenodd">
                          <circle cx="17.5" cy="17.5" r="17.5" fill="#6D6DC9" />
                          <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7" />
                        </g>
                      </svg>

                      <p className="tooltiptext tooltip-left">
                        Add Master <p>Please click on add icon to add new master data
                                        </p>
                      </p>
                    </div>

                  </li>
                </ul> */}
                <ul className="list-inline search_list manageSearch m-top-60">
                  <li>
                    <form onSubmit={(e) => this.onSearch(e)}>
                      <input onChange={(e) => this.handleSearch(e)} value={this.state.search} type="search" placeholder="Type to Search..." className="search_bar" />
                      <button type="submit" className="searchWithBar">Search
                      <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                          <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                          </path></svg>
                      </button>
                    </form>
                  </li>
                </ul>
                {this.state.type != 3 ? null : <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>}

              </div>

              <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3 mainRoleTable">
                <div className="zui-wrapper">
                  <div className="scrollableOrgansation scrollableTableFixed roleTableHeight">
                    <table className="table zui-table roleTable border-bot-table">
                      {/* roleTable */}
                      <thead>
                        <tr>
                          <th className="fixed-side-role fixed-side1">
                            <label> Action</label>
                          </th>
                          <th>
                            <label> User Name</label>
                          </th>
                          <th>
                            <label>Assigned Role</label>
                          </th>
                          <th>
                            <label>Created By</label>
                          </th>
                          <th>
                            <label>Created On</label>
                          </th>
                          <th>
                            <label>Status</label>
                          </th>

                          {/* <th className="width120">
                          <label> ACTION</label>
                        </th> */}

                        </tr>
                      </thead>

                      <tbody>
                        {this.state.rolesState == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.rolesState.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.rolesState.map((data, key) => (
                          <tr key={key}>
                            <td className="fixed-side-role fixed-side1">
                              <ul className="list-inline">
                                <li>
                                  <button className="reassignBtn" onClick={() => this.onReassignClick(`${data.id}`)} >
                                    {/* REASSIGN */}
                                    UPDATE
                                  </button>
                                </li>
                                {/* <li>
                                <button className="revokeBtn" >
                                REVOKE ACCESS
                                  </button>
                              </li> */}
                              </ul>
                            </td>
                            <td>
                              <label>
                                {data.userName}
                              </label>
                            </td>
                            <td>
                              <label>

                                <div className="topToolTip toolTipSupplier limitTextToolTip">

                                  <label className="chooseSupplyWidth widthpx200">
                                    {this.truncate(`${data.name}`, 4)}
                                  </label>
                                  {this.small(`${data.name}`) ? <span className="topToolTipText" id="hideToolTip">{data.name}</span> : null}

                                </div>
                              </label>
                            </td>
                            <td>
                              <label>
                                {data.createdBy}
                              </label>
                            </td>
                            <td>
                              <label>
                                {data.createdTime}
                              </label>
                            </td>
                            <td>
                              <label>
                                {data.status}
                              </label>
                            </td>
                            {/* <td className="width120">
                            <ul className="list-inline">
                              <li>
                                <button className="reassignBtn" onClick={() => this.onReassignClick(`${data.id}`)} >
                                  REASSIGN
                                  </button>
                              </li>
                              <li>
                                <button className="revokeBtn" >
                                REVOKE ACCESS
                                  </button>
                              </li>
                            </ul>
                          </td> */}

                          </tr>
                        ))}


                      </tbody>

                    </table>
                  </div>
                </div>
              </div>
              <div className="pagerDiv">
                <ul className="list-inline pagination">
                  <li >
                    <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                      First
                  </button>
                  </li>

                  <li>
                    <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                      Prev
                    </button>
                  </li>
                  {/* {this.state.prev != 0 ? <li >
                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                      Prev
                  </button>
                  </li> : <li >
                      <button className="PageFirstBtn" disabled>
                        Prev
                  </button>
                    </li>} */}
                  <li>
                    <button className="PageFirstBtn pointerNone">
                      <span>{this.state.current}/{this.state.maxPage}</span>
                    </button>
                  </li>
                  {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage  ? <li >
                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                      Next
                  </button>
                  </li> : <li >
                      <button className="PageFirstBtn borderNone" disabled>
                        Next
                  </button>
                    </li>}


                  {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                </ul>
              </div>


            </div>
          </div>

          <br />
          <br />

        </div>


{this.state.filter ? <Filter updateFilter={(e) => this.updateFilter(e)} createdBy={this.state.createdBy} userName={this.state.userName} name={this.state.name} status={this.state.status} {...this.props} filterBar={this.state.filterBar} onFilter={() => this.onFilterClick()} /> : null}      
{this.state.reassign ? <Reassign {...this.state} {...this.props} id={this.state.id} reassignModalAnimation={this.state.reassignModalAnimation} onReassign={(e) => this.onReassignClick(e)} /> : null}
{this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} />:null }
      </div>
    )
  }
}

export default ViewRoles;
