import React from 'react';

class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.administration.allRole.data.resource,
            // current: "",
            open: false,
            status: this.props.status,
            userName: this.props.userName,
            name: this.props.name,
            createdBy: this.props.createdBy
            // search: ""
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            status: nextProps.status,
            userName: nextProps.userName,
            name: nextProps.name,
            createdBy: nextProps.createdBy
        })
    }

    handleChange(e) {
        if (e.target.id == "userName") {
            this.setState({
                userName: e.target.value
            });
        } else if (e.target.id == "name") {
            this.setState({
                name: e.target.value
            });
        } else if (e.target.id == "createdBy") {
            this.setState({
                createdBy: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        }
    }

    clearFilter(e) {
        this.setState({
            open: false,
            status: "",
            userName: "",
            name: "",
            createdBy: ""
        })
    }

    onSubmit(e) {
        e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            search: this.state.search,
            userName: this.state.userName,
            name: this.state.name,
            createdBy: this.state.createdBy,
            status: this.state.status
        }
        this.props.rolesRequest(data);
        this.props.onFilter(e);
        this.props.updateFilter(data);
    }

    render() {
        let count = 0;
        if (this.state.userName != "") {
            count++;
        }
        if (this.state.name != "") {
            count++;
        }
        if (this.state.createdBy != "") {
            count++;
        }
        if (this.state.status != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal modalRoleWise display_block" : "display_none"} id="myRoleModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <form onSubmit={(e) => this.onSubmit(e)}>
                    <div className={this.props.filterBar ? "modal-content modalRole vendorFilterShow" : " vendorFilterHide"}>
                        <button type="button" onClick={() => this.props.onFilter()} className="close closeBtn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 ">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                            </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                            </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_confirmation">
                                {/* <div className="col-md-12 col-sm-12 pad-0"> */}
                                {/* <ul className="list-inline m-top-10 width_100 m-bot-10">
                                    <li>
                                        {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.id} className="selectectbuttondiv">
                                            <button className="modal_btn">{data.text}</button>
                                            <span onClick={(e) => this.onDelete(data.id)} value={data.id} className="close_btn" aria-hidden="true">&times;</span>
                                        </div>)}

                                    </li>

                                </ul> */}

                                {/* </div> */}
                                <div className="col-md-12 col-sm-12 pad-0">
                                    <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="userName" value={this.state.userName} placeholder="User Name" className="userModalInput" />
                                    </div>

                                    <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                        <select onChange={(e) => this.handleChange(e)} id="name" value={this.state.name} className="RoleFilterSelect">
                                            <option value="">
                                                Role
                                            </option>
                                            {this.state.data.map((data, key) => (<option key={key} value={data.name}>{data.name}</option>))}
                                        </select>
                                    </div>
                                    <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="createdBy" value={this.state.createdBy} placeholder="Created By" className="userModalInput" />
                                    </div>

                                    <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                                        <select onChange={(e) => this.handleChange(e)} id="status" value={this.state.status} className="RoleFilterSelect">
                                            <option value="">
                                                Status
                                            </option>
                                            <option value="Active">
                                                Active
                                                </option>
                                            <option value="Inactive">
                                                Inactive
                                                </option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.userName == "" && this.state.name == "" && this.state.createdBy == "" && this.state.status == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button onClick={(e) => this.clearFilter(e)} type="button" className="modal_clear_btn">
                                                CLEAR FILTER
                                        </button>}
                                    </li>
                                    <li>
                                    {this.state.userName != "" || this.state.createdBy != "" || this.state.name != "" || this.state.status != "" ?  <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button>: <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                            APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default Filter;