import React from 'react';
import errorIcon from "../../../assets/error_icon.svg";
class Reassign extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            role: "",
            roleerr: false,
            userName: "",
            userId: "",
            roleData: [],
            btnDisable: true,
            search: "",
            selectederr: false,
            selected: [],
        }
    }
    handleChange(e) {
        e.preventDefault();
        this.setState({
            btnDisable: false
        })
        if (e.target.id == "role") {
            this.setState(
                {
                    role: e.target.value
                },
                () => {
                    this.role();
                }
            );
        }
    }

    role() {
        if (
            this.state.role == "" || this.state.role.trim() == "") {
            this.setState({
                roleerr: true
            });
        } else {
            this.setState({
                roleerr: false
            });
        }
    }



    onFormSubmit(e) {
        e.preventDefault();
        // this.role();
        this.selected();

        const t = this;
        setTimeout(function () {
            const { userId, id, userName, role, selectederr } = t.state;
            if (!selectederr) {
                let final = [];
                for (let i = 0; i < t.props.administration.allRole.data.resource.length; i++) {
                    for (let j = 0; j < t.state.selected.length; j++) {
                        if (t.props.administration.allRole.data.resource[i].name == t.state.selected[j]) {
                            final.push(t.props.administration.allRole.data.resource[i])
                        }
                    }
                }
                let abc = "";
                for (let i = 0; i < final.length; i++) {
                    let a = final[i].id;
                    if (final.length > 1) {
                        abc = abc == "" ? "".concat(a) : abc.concat('|').concat(a);
                    } else {
                        abc = a;
                    }
                }

                let data = {
                    'userId': userId,
                    'id': id,
                    'userNameE': userName,
                    'roles': abc,
                    'type': t.props.type,
                    'no': t.props.no,
                    'userRoles': t.state.selected,
                    'userName': t.props.userName,
                    'name': t.props.name,
                    'createdBy': t.props.createdBy,
                    'status': t.props.status,
                    'search': t.props.search,
                }
                t.props.editRolesRequest(data);
            }
        }, 100)
    }


    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true
            });
        } else {
            this.setState({
                selectederr: false
            })
        }
    }
    onnChange(id) {
        this.setState({
            current: id,
            btnDisable: false
        })
        let match =[];
        match.push(_.find(this.state.roleData, function (o) {
            return o == id;
        }))


        const t =this
        setTimeout(function(){
            t.setState({
                selected: t.state.selected==null? match:t.state.selected.concat(match)
            });
        },10)
       
        let idd = match[0];
        var array = this.state.roleData;
        for (var i = 0; i < array.length; i++) {
            if (array[i] === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            roleData: array,
            selectederr: false
        })
    }
    onDelete(e) {
        let match = _.find(this.state.selected, function (o) {
            return o == e;
        })
        this.setState({
            roleData: this.state.roleData.concat(match)
        });

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i] === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            btnDisable: false
        })
        // if (this.props.replenishment.getZone.isSuccess && !this.props.replenishment.getGrade.isSuccess && !this.props.replenishment.getStoreCode.isSuccess) {
        //     this.props.replenishment.getZone.data.resource.store = this.state.roleData.concat(match)
        // } else if (this.props.replenishment.getGrade.isSuccess && !this.props.replenishment.getStoreCode.isSuccess) {
        //     this.props.replenishment.getGrade.data.resource.store = this.state.roleData.concat(match)
        // } else if (this.props.replenishment.getStoreCode.isSuccess) {
        //     this.props.replenishment.getStoreCode.data.resource.store = this.state.roleData.concat(match);
        // }

    }
    Change(e) {
        this.setState({
            search: e.target.value
        })
    }

    onClear(e) {
        e.preventDefault();
        this.setState({

            role: "",
        })
    }
    componentWillMount() {
        for (let i = 0; i < this.props.administration.roles.data.resource.length; i++) {
            if (this.props.administration.roles.data.resource[i].id == this.props.id) {
                this.setState({
                    id: this.props.id,
                    userName: this.props.administration.roles.data.resource[i].userName,
                    userId: this.props.administration.roles.data.resource[i].userId,
                    selected: this.props.administration.roles.data.resource[i].userRoles
                });

            }
        }
        let rolesData = [];
        if(this.props.administration.allRole.data.resource!=null){
        for (let i = 0; i < this.props.administration.allRole.data.resource.length; i++) {
            rolesData.push(this.props.administration.allRole.data.resource[i].name)
        }}
        for (let i = 0; i < this.props.administration.roles.data.resource.length; i++) {
            if (this.props.administration.roles.data.resource[i].id == this.props.id) {
                if(this.props.administration.roles.data.resource[i].userRoles!=null){
                for (let k = 0; k < this.props.administration.roles.data.resource[i].userRoles.length; k++) {
                    
                    for (let j = 0; j < rolesData.length; j++) {
                       if (this.props.administration.roles.data.resource[i].userRoles[k].toLowerCase().trim() == rolesData[j].toLowerCase().trim()) {
                            rolesData.splice(j,1);
                            break;
                        }
                    }}
                }
                break;
            }
        }
        this.setState({
            roleData: rolesData
        })



        // this.setState({
        //     roleData: this.props.administration.allRole.data.resource
        // })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.roles.isSuccess) {
            this.props.onReassign()
        }
    }
    render() {
        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        }); 

        const { search, roleData, selectederr } = this.state;
        var result = _.filter(roleData, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        });
        const { role, roleerr } = this.state;
        return (
            <div className={this.props.reassignModalAnimation ? "modal modalReassignContainer  display_block" : "display_none"} id="editVendorModal">
                <div className={this.props.reassignModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.reassignModalAnimation ? " display_block" : "display_none"}>
                    <div className={this.props.reassignModalAnimation ? "modal-content modalReassign modalShow" : "modalHide"}>

                        {/* <div className="modal modalReassignContainer" id="reassignModal"> */}

                        <form onSubmit={(e) => this.onFormSubmit(e)}>

                            {/* <div className="backdropReassign"></div>
                    <div className="modal-content modalReassign"> */}

                            <div className="col-md-12 col-sm-12 m-top-10">
                                <div className="modal_confirmation reassignModal">
                                    <div className="col-md-10 col-sm-12 pad-0">
                                        <ul className="list-inline modalReassignContainer">
                                            <li>
                                                <label className="headingRoleReassgin">
                                                    REASSIGN ROLES TO USER :
                                    </label>
                                            </li>
                                            <li>
                                                <label className="filter_text">
                                                    {this.state.userName}
                                                </label>
                                            </li>
                                        </ul>
                                        </div>
                                        <div className="col-md-2 col-xs-3 pad-0">
                                        <div className="closeBtnReassignDiv">
                                            <button className="closeBtnReassign" onClick={() => this.props.onReassign()} data-dismiss="modal" aria-label="Close">
                                                Close
                                             </button>
                                        </div>
                                    </div>

                                        {/* <ul className="list-inline m-top-20">
                                <li>
                                    <button className="reassignModalBtn">
                                        Administrator
                                    </button>
                                    <span className="close_btn" aria-hidden="true">&times;</span>
                                </li>
                                <li>
                                    <button className="reassignModalBtn">
                                        Project Manager
                                    </button>
                                    <span className="close_btn" aria-hidden="true">&times;</span>
                                </li>
                            </ul> */}
                                        {/* <ul className="list-inline m-top-20 modalReassignContainer">

                                        <li>
                                            <select className={roleerr ? "selectBoxModAL errorBorder":"selectBoxModAL"} value={role} onChange={(e) => this.handleChange(e)} id="role">
                                                <option value="">
                                                    Select Roles
                                           </option>
                                                {this.state.roleData.map((data, key) => (
                                                    <option key={key} value={data.name}>{data.name}</option>
                                                ))}
                                            </select> */}
                                        {/* {roleerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                        {/* {roleerr ? (
                                                <span className="error">
                                                    Enter valid Role
                            </span>
                                            ) : null}
                                        </li>
                                    </ul> */}
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="dropdownSketchFilter skechersDrop rolesUpdate">
                                            <div className="SketchFilter">
                                                <p>Choose Roles from below list :</p>
                                                <input type="text" placeholder="Type to Search…"
                                                    value={search}
                                                    onChange={(e) => this.Change(e)} />
                                            </div>
                                            <div className="sketchStoreDrop">
                                                <div className="lftStoreDiv m-rgt-20">
                                                    <h2>Choose Roles</h2>
                                                    <div className="storeChoose">
                                                        <ul className="list-inline">
                                                            {result.length == 0 ? null : result.map((data, i) => <li key={i} onClick={(e) => this.onnChange(`${data}`)} >
                                                                <p className="para_purchase">
                                                                    <input type="text" className="purchaseCheck" type="checkbox" id="c8" name="cb" />
                                                                    {/* <label htmlFor="c8" className="purchaseLabel"></label> */}
                                                                </p>
                                                                <span className="onHover"></span>
                                                                <p className="contentStore pad-5">{data}</p>

                                                            </li>)}

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="lftStoreDiv">
                                                    <h2>Selected Roles</h2>
                                                    <div className={this.state.selectederr ? "storeChoose pad-10 borderRed" : "storeChoose pad-10"}>
                                                        <ul className="list-inline">
                                                            <li>
                                                                {this.state.selected == null ? null : this.state.selected.map((data, i) => <div key={data} className="">
                                                                    <p className="contentStore pointerNone">{data} </p>
                                                                    <span onClick={(e) => this.onDelete(data)} value={data} className="close_btn_demand" aria-hidden="true">&times;</span>
                                                                </div>)}
                                                            </li>
                                                        </ul>

                                                    </div>
                                                    {this.state.selectederr ? <span className="errorReassign">Select atleast one role</span> : false}
                                                </div>

                                            </div>
                                        </div>
                                    



                                    </div>
                                    {/* <div className="col-md-2 col-xs-3 pad-0">
                                        <div className="closeBtnReassignDiv">
                                            <button className="closeBtnReassign" onClick={() => this.props.onReassign()} data-dismiss="modal" aria-label="Close">
                                                Close
                                             </button>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                            <div className="modal-footer modalReassignfooter">
                                <div className="col-md-12 col-sm-12 col-xs-12">
                                    <div className="divFooter">
                                        <ul className="list-inline m-0 text-center width_100">
                                            {/* <li>
                                                <button className="clearModalBtn" type="reset" onClick={(e) => this.onClear(e)} >
                                                    CLEAR
                                </button>
                                            </li> */}
                                            <li>
                                                {this.state.btnDisable ? <li><button type="button" className="btnDisabled saveModalBtn" >Save</button></li> : <button className="saveModalBtn" type="submit">
                                                    SAVE CHANGES
                                </button>}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Reassign;