import React from "react";
import SiteCreate from "./site/siteCreate";
import EmptyBox from "./site/emptyBox";
import siteData from "../../json/siteData.json";
import SiteView from "./site/siteView";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
class Site extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteState: [],
      loader: true,
      emptyBox: false,
      siteView: false,
      errorMessage: "",
      successMessage: "",
      type: 1,
      success: false,
      errorCode: "",
      alert: false,
      code: ""
      // siteCreate :false
    };
  }
  componentDidMount() {
    if (!this.props.administration.site.isSuccess) {
      let data = {
        type: 1,
        no: 1
      }
      this.props.siteRequest(data);
    } else {
      let data = {
        type: 1,
        no: 1
      }
      this.props.siteRequest(data);
      this.setState({
        loader: false,
        siteState: this.props.administration.site.data.resource,
        emptyBox: false,
        siteView: true
      })
    }
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.administration.site.isSuccess) {
      return {
        loader: false,
        siteView: true,
        emptyBox: false,
        siteState: nextProps.administration.site.data.resource,
      }

    } else if (nextProps.administration.site.isError) {
      return {
        loader: false,
        code: nextProps.administration.site.message.status,
        errorCode: nextProps.administration.site.message.error == undefined ? undefined : nextProps.administration.site.message.error.errorCode,
        errorMessage: nextProps.administration.site.message.error == undefined ? undefined : nextProps.administration.site.message.error.errorMessage,
        alert: true,
        siteView: true
      }
    }
    if (nextProps.administration.editSite.isSuccess) {
      return {
        successMessage: nextProps.administration.editSite.data.message,
        success: true,
        loader: false,
        alert: false,
      }

    }
    else if (nextProps.administration.editSite.isError) {
      return {
        errorMessage: nextProps.administration.editSite.message.error == undefined ? undefined : nextProps.administration.editSite.message.error.errorMessage,
        alert: true,
        code: nextProps.administration.editSite.message.status,
        success: false,
        loader: false,
        errorCode: nextProps.administration.editSite.message.error == undefined ? undefined : nextProps.administration.editSite.message.error.errorCode
      }

    }
    if (nextProps.administration.deleteSite.isSuccess) {
      return {
        successMessage: nextProps.administration.deleteSite.data.message,
        success: true,
        loader: false,
        alert: false,
      }

    }
    else if (nextProps.administration.deleteSite.isError) {
      return {
        errorMessage: nextProps.administration.deleteSite.message.error == undefined ? undefined : nextProps.administration.deleteSite.message.error.errorMessage,
        alert: true,
        success: false,
        loader: false,
        code: nextProps.administration.deleteSite.message.status,
        errorCode: nextProps.administration.deleteSite.message.error == undefined ? undefined : nextProps.administration.deleteSite.message.error.errorCode
      }

    }
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {


      return {

        loader: false,




      }
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      return {
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage


      }
    }
    if (nextProps.replenishment.createHeaderConfig.isSuccess) {
      return {
        successMessage: nextProps.replenishment.createHeaderConfig.data.message,
        loader: false,
        success: true,
      }

    } else if (nextProps.replenishment.createHeaderConfig.isError) {
      return {
        loader: false,
        alert: true,
        code: nextProps.replenishment.createHeaderConfig.message.status,
        errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
      }

    }
    if ( nextProps.administration.site.isLoading || nextProps.administration.editSite.isLoading || nextProps.administration.deleteSite.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.replenishment.createHeaderConfig.isLoading) {
      return {
        loader: true
      }
    }
    return {}
  }


  componentDidUpdate(previousProps, previousState) {
    if (this.props.administration.editSite.isError || this.props.administration.editSite.isSuccess) {
      this.props.editSiteClear()
    }
    if (this.props.administration.deleteSite.isError || this.props.administration.deleteSite.isSuccess) {
      this.props.deleteSiteClear()
    }
    if (this.props.administration.site.isError || this.props.administration.site.isSuccess) {
      this.props.siteClear()
    }
    if (this.props.replenishment.getHeaderConfig.isError || this.props.replenishment.getHeaderConfig.isSuccess) {
      this.props.getHeaderConfigClear()
    }
    if (this.props.replenishment.createHeaderConfig.isSuccess) {
      let payload = {
        enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
        displayName: "SITE_TABLE_HEADER",
        attributeType: "TABLE HEADER",
      }
      this.props.getHeaderConfigRequest(payload)
      this.props.createHeaderConfigClear()
    } else if (this.props.replenishment.createHeaderConfig.isError) {
      this.props.createHeaderConfigClear()
    }
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  onAddSite() {
    this.setState({
      // siteCreate:true,
      // siteState:siteData
    });
    this.props.history.push('/administration/site/siteCreate')

  }


  onSaveSite() {
    this.setState({
      siteMapState: siteMapdata,
      // addSiteMap: false
    });
    this.props.history.push('/administration/site/siteView')

  }


  render() {
    // const hash = window.location.hash.split("/")[3];
    // let element = document.getElementById('app');
    // this.state.loader ?element.classList.add('blurContent'): element.classList.remove('blurContent');

    return (
      <div className="container-fluid pad-0">

        {this.state.emptyBox ?
          <EmptyBox {...this.props} siteState={this.state.siteState} clickRightSideBar={() => this.props.rightSideBar()} addSite={() => this.onAddSite()} />
          : null}{this.state.siteView ?
            <SiteView

              {...this.props}
              siteState={this.state.siteState}
              clickRightSideBar={() => this.props.rightSideBar()} />
            // : hash == "siteCreate" ?
            //   <SiteCreate {...this.props} clickRightSideBar={() => this.props.rightSideBar()} saveSite={() => this.onSaveSite()} />
            : null}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>);
  }
}

export default Site;
