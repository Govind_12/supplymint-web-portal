import React from "react";
import UserManage from './user/userManage';
import EmptyBox from './user/emptyBox';
import AddUser from './user/addUser';
import userData from "../../json/userData.json";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userState: [],
      emptyBox: false,
      loader: true,
      userManage: false,
      errorMessage: "",
      success: false,
      alert: false,
      errorCode: "",
      successMessage: "",
      code: ""
    };
  }

  componentWillMount() {
    if (!this.props.administration.user.isSuccess) {
      let data = {
        type: 1,
        no: 1
      }
      this.props.userRequest(data);
    } else {
      let data = {
        type: 1,
        no: 1
      }
      this.props.userRequest(data);
      this.setState({
        loader: false,
          userState: this.props.administration.user.data.resource,
          emptyBox: false,
          userManage: true
        })
    }
    sessionStorage.setItem('currentPage', "RADMMAIN")
    sessionStorage.setItem('currentPageName', "Users")
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.user.isSuccess) {
      this.setState({
        loader: false,
          userState: nextProps.administration.user.data.resource,
          userManage: true,
          emptyBox: false
        })
    }
    if (nextProps.administration.user.isLoading) {
      this.setState({
       
        loader: true
      })
    }else if (nextProps.administration.user.isError) {
      this.setState({
       errorCode: nextProps.administration.user.message.error == undefined ? undefined : nextProps.administration.user.message.error.errorCode,
       errorMessage: nextProps.administration.user.message.error == undefined ? undefined : nextProps.administration.user.message.error.errorMessage,
       code: nextProps.administration.user.message.status,
        loader: false,
        userManage: true,
        alert: true
      })
    }
    if (nextProps.administration.editUser.isLoading) {
      this.setState({
        loader: true,
      })

    }
    if (nextProps.administration.editUser.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.editUser.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.editUserClear();

    }
    else if (nextProps.administration.editUser.isError) {
      this.setState({
        errorMessage: nextProps.administration.editUser.message.error == undefined ? undefined : nextProps.administration.editUser.message.error.errorMessage,
        code: nextProps.administration.editUser.message.status,
        alert: true,
        success: false,
        errorCode:  nextProps.administration.editUser.message.error == undefined ? undefined : nextProps.administration.editUser.message.error.errorCode,
        loader: false
      })
      this.props.editUserClear();
    }

    if (nextProps.administration.userStatus.isLoading) {
      this.setState({
        loader: true,
      })

    }
    if (nextProps.administration.userStatus.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.userStatus.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.userStatusClear();

    }
    else if (nextProps.administration.userStatus.isError) {
      this.setState({
        errorMessage: nextProps.administration.userStatus.message.error == undefined ? undefined : nextProps.administration.userStatus.message.error.errorMessage,
        code: nextProps.administration.userStatus.message.status,
        alert: true,
        success: false,
        errorCode:  nextProps.administration.userStatus.message.error == undefined ? undefined : nextProps.administration.userStatus.message.error.errorCode,
        loader: false
      })
      this.props.userStatusClear();
    }


    if (nextProps.administration.deleteUser.isLoading) {
      this.setState({
        loader: true,
      })

    }
    else if (nextProps.administration.deleteUser.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.deleteUser.data.message,
        success: true,
        loader: false,
        alert: false,
      })
      this.props.deleteUserRequest();

    }
    else if (nextProps.administration.deleteUser.isError) {
      this.setState({
        errorMessage: nextProps.administration.deleteUser.message.error == undefined ? undefined :  nextProps.administration.deleteUser.message.error.errorMessage,
        alert: true,
        success: false,
        loader: false,
        code: nextProps.administration.deleteUser.message.status,
        errorCode: nextProps.administration.deleteUser.message.error == undefined ? undefined : nextProps.administration.deleteUser.message.error.errorCode,
      })
      this.props.deleteUserRequest();
    }
    if (nextProps.administration.allRole.isSuccess) {
      this.setState({ loader: false })
    }
    if (nextProps.administration.addUser.isSuccess) {
      this.setState({
          loader: false,
          successMessage: nextProps.administration.addUser.data.message,
          success: true
      }) 
      this.props.addUserClear();  
    } else if (nextProps.administration.addUser.isError) {
        this.setState({
            errorCode: nextProps.administration.addUser.message.error == undefined ? undefined : nextProps.administration.addUser.message.error.errorCode,
            loader: false,
            code: nextProps.administration.addUser.message.status,
            errorMessage: nextProps.administration.addUser.message.error == undefined ? undefined : nextProps.administration.addUser.message.error.errorMessage,
            alert: true,
        })
        this.props.addUserClear();
    }
    if (nextProps.administration.allRole.isLoading || nextProps.administration.addUser.isLoading) {
      this.setState({ loader: true })
    }

  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  onAddUser() {
    this.props.history.push('/administration/users/addUser')
  }

  onSaveUser() {
    this.props.history.push('/administration/user/userManage')
  }


  render() {
    // let element = document.getElementById('app');
    // this.state.loader ?element.classList.add('blurContent'): element.classList.remove('blurContent');

    return (
      <div className="container-fluid pad-0">

        {this.state.emptyBox ?
          <EmptyBox

            {...this.props}
            addUser={() => this.onAddUser()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />
          : null}{this.state.userManage ?
            <UserManage
              userState={this.state.userState}

              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            // <AddUser
            //   {...this.props}
            //   saveUser={() => this.onSaveUser()}
            //   clickRightSideBar={() => this.props.rightSideBar()}
            // /> 
            : null}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>


    );
  }
}

export default Users;
