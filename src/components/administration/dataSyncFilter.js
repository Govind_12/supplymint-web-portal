import React from "react";

import { OganisationIdName } from '../../organisationIdName.js';
class DataSyncFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            type: "",
            no: "",
            eventStart: "",
            dataCountstatus: "",
            eventEnd: "",
            key: "",
            dataCount: ""
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            eventStart: nextProps.eventStart,
            dataCountstatus: nextProps.dataCountstatus,
            eventEnd: nextProps.eventEnd,
            key: nextProps.keys,
            dataCount: nextProps.dataCount,
            name: nextProps.name
        })
        if (nextProps.eventStart == "") {
            document.getElementById('eventStart').placeholder = "Event Start";
        } else {
            document.getElementById('eventStart').placeholder = nextProps.eventStart;
        }
        if (nextProps.eventEnd == "") {
            document.getElementById('eventEnd').placeholder = "Event End";
        } else {
            document.getElementById('eventEnd').placeholder = nextProps.eventEnd;
        }
    }

    componentWillMount() {
        this.setState({
            eventStart: this.props.eventStart,
            dataCountstatus: this.props.dataCountstatus,
            eventEnd: this.props.eventEnd,
            key: this.props.keys,
            dataCount: this.props.dataCount,
            name: this.props.name
        })
    }

    handleChange(e) {
        if (e.target.id == "name") {
            this.setState({
                name: e.target.value
            });
        } else if (e.target.id == "eventStart") {
            e.target.placeholder = e.target.value;
            this.setState({
                eventStart: e.target.value
            });
        } else if (e.target.id == "eventEnd") {
            e.target.placeholder = e.target.value;
            this.setState({
                eventEnd: e.target.value
            });
        } else if (e.target.id == "key") {
            this.setState({
                key: e.target.value
            });
        } else if (e.target.id == "dataCount") {
            this.setState({
                dataCount: e.target.value
            });
        } else if (e.target.id == "dataCountstatus") {
            this.setState({
                dataCountstatus: e.target.value
            });
        }
    }

    clearFilter(e) {
        this.setState({
            orgName: "",
            type: "",
            no: "",
            name: "",
            key: "",
            dataCount: "",
            dataCountstatus: "",
        })
        document.getElementById('eventEnd').placeholder = "Event End";
        document.getElementById('eventStart').placeholder = "Event Start";
    }

    onSubmit(e) {
        e.preventDefault();
         var { orgCodeGlobal } = OganisationIdName()
        let data = {
            no: 1,
            type: 2,
            name: this.state.name,
            key: this.state.key,
            status: this.state.dataCountstatus,
            eventStart: this.state.eventStart,
            eventEnd: this.state.eventEnd,
            dataCount: this.state.dataCount,
            serach: "",
            channel:orgCodeGlobal
        }
        this.props.dataSyncRequest(data);
        this.props.updateFilter(data)
        this.props.closeFilter(e);

    }

    render() {
        let count = 0;
        if (this.state.name != "") {
            count++;
        }
        if (this.state.eventEnd != "") {
            count++;
        }
        if (this.state.eventStart != "") {
            count++;
        }
        if (this.state.key != "" && this.state.key != undefined) {
            count++;
        } if (this.state.dataCount != "") {
            count++;
        }
        return (

            <div className={this.props.dataFilter ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.dataFilter ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.dataFilter ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0 dataSyncFilterItem">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="name" value={this.state.name} placeholder="Name" className="organistionFilterModal" />                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} value={this.state.key} className="organistionFilterModal" id="key" placeholder="Key" />
                                    </li>
                                    <li>
                                        <input type="date" placeholder="Event Start" onChange={(e) => this.handleChange(e)} value={this.state.eventStart} id="eventStart" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" placeholder="Event End" onChange={(e) => this.handleChange(e)} value={this.state.eventEnd} id="eventEnd" className="organistionFilterModal" />
                                    </li>

                                    {/* <li> */}
                                    {/* <select id="dataCountstatus" onChange={(e) => this.handleChange(e)} value={this.state.dataCountstatus} className="organistionFilterModal">
                                            <option value="">
                                                Status
                                                </option>
                                            <option value="PROCESSED">PROCESSED</option>
                                            <option value="FAILED">FAILED</option>
                                            <option value="PROCESSING">PROCESSING</option>
                                        </select> */}
                                    {/* <input type="text" placeholder="s" className="organistionFilterModal" /> */}
                                    {/* </li> */}
                                    <li>
                                        <input type="text" id="dataCount" value={this.state.dataCount} onChange={(e) => this.handleChange(e)} className="organistionFilterModal" placeholder="Data Count" />
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    {this.state.name == "" && this.state.eventEnd == "" && this.state.eventStart == "" && this.state.key == "" && this.state.dataCount == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                        : <button onClick={(e) => this.clearFilter(e)} type="button" className="modal_clear_btn">
                                            CLEAR FILTER
                                        </button>}
                                    <li>
                                        {this.state.name != "" || this.state.key != "" || this.state.eventEnd != "" || this.state.eventStart != "" || this.state.dataCount != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default DataSyncFilter;
