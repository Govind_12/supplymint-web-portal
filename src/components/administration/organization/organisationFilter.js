import React from "react";

class OrganizationFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgName: this.props.orgName,
            type: "",
            no: "",
            displayName: this.props.displayName,
            status: this.props.status,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            orgName: nextProps.orgName,
            displayName: nextProps.displayName,
            status: nextProps.status,
        })
    }

    handleChange(e) {
        if (e.target.id == "orgName") {
            this.setState({
                orgName: e.target.value
            });
        } else if (e.target.id == "displayName") {
            this.setState({
                displayName: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        }
    }

    clearFilter(e) {
        this.setState({
            orgName: "",
            type: "",
            no: "",
            displayName: "",
            status: "",
        })
    }

    onSubmit(e) {
        e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            orgName: this.state.orgName,
            displayName: this.state.displayName,
            status: this.state.status,
            serach: ""
        }
        this.props.organizationRequest(data);
        this.props.closeFilter(e);
        this.props.updateFilter(data)
    }

    render() {

        let count = 0;
        if (this.state.orgName != "") {
            count++;
        }
        if (this.state.displayName != "") {
            count++;
        } if (this.state.status != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="orgName" value={this.state.orgName} placeholder="Organisation Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="displayName" value={this.state.displayName} placeholder="Display Name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <select id="status" onChange={(e) => this.handleChange(e)} value={this.state.status} className="organistionFilterModal">
                                            <option value="">
                                                Status
                            </option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        {/* <input type="text" placeholder="s" className="organistionFilterModal" /> */}
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.state.orgName == "" && this.state.displayName == "" && this.state.status == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.state.orgName != "" || this.state.displayName != "" || this.state.status != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default OrganizationFilter;
