import React from "react";
import openRack from "../../../assets/open-rack.svg";
import searchIcon from '../../../assets/clearSearch.svg';
import OrganizationFilter from "./organisationFilter";
import BraedCrumps from "../../breadCrumps";
import OrganisationModal from "./organisationModal";
import editIcon from "../../../assets/edit.svg";
import deleteIcon from "../../../assets/delete.svg";
import SearchImage from '../../../assets/searchicon.svg';
import filterIcon from '../../../assets/headerFilter.svg';
import { CONFIG } from "../../../config/index";
import axios from 'axios';
import multideleteIcon from "../../../assets/multidelete.svg";
import refreshIcon from "../../../assets/refresh-block.svg";
import ToastLoader from "../../loaders/toastLoader";
import ColoumSetting from "../../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../../replenishment/confirmationReset";
import VendorFilter from "../../vendorPortal/vendorFilter";

class ManageOrganization extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            organizationState: this.props.organizationState,
            filter: false,
            filterBar: false,
            modalOpen: false,
            orgId: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: 1,
            no: 1,
            displayName: "",

            status: "",
            orgName: "",
            search: "",
            orgModalAnimation: false,
            toastLoader: false,
            toastMsg: "",
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],
            searchBy: "contains",
            showDownloadDrop: false,
            filterItems:{},
            checkedFilters:[],
            filteredValue: [],
            applyFilter: false,
        };
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
    }

    componentDidMount() {
        console.log(this.state.organizationState);
        if (this.props.administration.organization.isSuccess) {
            this.setState({

                prev: this.props.administration.organization.data.prePage,
                current: this.props.administration.organization.data.currPage,
                next: this.props.administration.organization.data.currPage + 1,
                maxPage: this.props.administration.organization.data.maxPage,
            })
        }
        if (!this.props.replenishment.getHeaderConfig.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                displayName: "ORG_TABLE_HEADER",
                attributeType: "TABLE HEADER",
            }
            this.props.getHeaderConfigRequest(payload)
        }
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
        
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

        // componentWillReceiveProps(nextProps) {
    //     if (nextProps.administration.organization.isSuccess) {
    //         this.setState({
    //             organizationState: nextProps.administration.organization.data.resource,
    //             prev: nextProps.administration.organization.data.prePage,
    //             current: nextProps.administration.organization.data.currPage,
    //             next: nextProps.administration.organization.data.currPage + 1,
    //             maxPage: nextProps.administration.organization.data.maxPage,
    //         })
    //     }
    // }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createHeaderConfig.isSuccess && this.props.replenishment.createHeaderConfig.data.basedOn == "ALL") {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                displayName: "ORG_TABLE_HEADER",
                attributeType: "TABLE HEADER",
            }
            this.props.getHeaderConfigRequest(payload)
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null) {
                let getHeaderConfig = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"])
                let fixedHeader = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"])
                let customHeadersState = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"])
                return {
                    filterItems: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
                    loader: false,
                    customHeaders: prevState.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
                    getHeaderConfig,
                    fixedHeader,
                    customHeadersState,
                    // tableCustomHeader: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
                    // tableGetHeaderConfig: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
                    headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
                    fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"],
                    headerConfigDataState: { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] },
                    headerSummary: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
                    defaultHeaderMap: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
                    headerCondition: customHeadersState.length == 0 ? true : false,
                }


            }
        }
        if (nextProps.administration.organization.isSuccess) {
            if (nextProps.administration.organization.data.resource != null) {
                return {
                    organizationState: nextProps.administration.organization.data.resource,
                    prev: nextProps.administration.organization.data.prePage,
                    current: nextProps.administration.organization.data.currPage,
                    next: nextProps.administration.organization.data.currPage + 1,
                    maxPage: nextProps.administration.organization.data.maxPage,
                }
            } else {
                return {
                    organizationState: [],
                    prev: "",
                    current: "",
                    next: "",
                    maxPage: "",
                }
            }
        }
        return {}

    }

    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }

    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }

    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                displayName: "",
                status: "",
                orgName: ""
            })
            let data = {
                type: 3,
                no: 1,
                displayName: "",
                status: "",
                orgName: "",
                search: this.state.search,
                searchBy: this.state.searchBy
            }
            this.props.organizationRequest(data)
        }

    }
    handleFromAndToValue=()=>{
        var value = event.target.value;
        var name = event.target.dataset.value;
        if( name == undefined ){ 
            value = this.state.fromCreationDate+" | "+this.state.toCreationDate
            name = this.state.filterNameForDate;
         }
         else     
        value = event.target.value
         if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
          }
        
        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                this.setState({ applyFilter: true })
            }
        })
    }
    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            displayName: data.displayName,
            status: data.status,
            orgName: data.orgName,
            search: data.search,
        });
    }
    submitFilter = () => {
        let payload = {}
        let filtervalues =  {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        // Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data]  + "T00:00+05:30")))
        Object.keys(payload).map((data)=>(data.includes("poDate")|| data.includes("validFromDate") || data.includes("validToDate")|| data.includes("qcFromDate") || data.includes("qcToDate"))
        && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim() + "T00:00+05:30", to: payload[data].split("|")[1].trim() + "T00:00+05:30" }))
        
        //for handling to and from value on UI level::    
        this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(filtervalues).map((data) => ((data.includes("poDate") || data.includes("validFromDate") || data.includes("validToDate")|| data.includes("qcToDate") || data.includes("qcFromDate"))
            && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim() })))
        let data = {
            type: 2,
            no: 1,
            displayName: this.state.displayName,
            status: this.state.status,
            orgName: this.state.orgName,
            search: this.state.search,
            searchBy: this.state.searchBy,
            filter: payload,
            sortedIn: this.state.filterType,
        }
        console.log(payload,"payload")
        this.props.organizationRequest(data)
        this.setState({
            filter: false,
            filteredValue: filtervalues,
            type: 2,
            tagState:true
        })
    }
    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            this.setState({ [data]: "" })
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }
    handleInput = (event,filterName) => {
        if( event != undefined && event.length != undefined ){
            this.setState({ fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                            filterNameForDate: filterName}, ()=>this.handleFromAndToValue(event))
            this.setState({ toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                            filterNameForDate: filterName }, ()=>this.handleFromAndToValue(event)) 
        }
        else if( event != null ){
           
            this.handleFromAndToValue(event);  
    }

        // var value = event.target.value
        // var name = event.target.dataset.value
        // if (/^\s/g.test(value)) {
        //     value = value.replace(/^\s+/, '');
        // }
        // this.setState({ [name]: value, applyFilter: true }, () => {
        //     if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
        //         this.setState({ applyFilter: false })
        //     } else {
        //         this.setState({ applyFilter: true })
        //     }
        // })
    }
    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                status: "SHIPMENT_REQUESTED",
                vendorName: "",
                poNumber: "",
                poDate: "",
                createdOn: "",
                userType: "entship",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.getShipmentRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        this.child.closeRecentlyAdd()
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        }
        //  () =>  document.addEventListener('click', this.closeFilter)
         );
    }
    closeFilter = (e) => {
        // e.preventDefault();
        if( e == undefined || e.target.parentElement.className.baseVal == "" || ( e.target.parentElement !== null && !e.target.parentElement.className.includes("asn") && 
            !e.target.parentElement.className.includes("check"))){
            this.setState({
                filter: false,
                filterBar: false
            }, () => document.removeEventListener('click', this.closeFilter));
        }
    }

    clearTag=(e,index)=>{
        let deleteItem = this.state.checkedFilters;
        deleteItem.splice(index,1)
        this.setState({
           checkedFilters:deleteItem
        },()=>{
            this.submitFilter();
            console.log(this.state.checkedFilters)
        })
    }

    clearAllTag=(e)=>{
   
        this.setState({
            checkedFilters:[]
        
        },()=>{
            this.submitFilter();
            this.clearFilterOutside();
        })
     }
    clearFilterOutside=()=>{
        this.setState({
            filteredValue:[],
            selectAll:false,
            checkedFilters:[]
        })
    }
    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }
    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                status: "SHIPMENT_REQUESTED",
                vendorName: "",
                poNumber: "",
                poDate: "",
                createdOn: "",
                userType: "entship",
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.getShipmentRequest(payload)
        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
        this.child.closeRecentlyAdd()
    }
    onDeleteOrganization(id) {
        let data = {
            iid: id,
            type: this.state.type,
            no: this.props.administration.organization.data.currPage,
            displayName: this.state.displayName,
            status: this.state.status,
            orgName: this.state.orgName,
            search: this.state.search,
        }
        this.props.deleteOrganizationRequest(data);
    }
    organisationModalOpen(e) {
        this.setState({
            modalOpen: !this.state.modalOpen,
            orgId: e,
            orgModalAnimation: !this.state.orgModalAnimation

        })
    }

    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {

                this.setState({
                    prev: this.props.administration.organization.data.prePage,
                    current: this.props.administration.organization.data.currPage,
                    next: this.props.administration.organization.data.currPage + 1,
                    maxPage: this.props.administration.organization.data.maxPage,
                })
                if (this.props.administration.organization.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.administration.organization.data.currPage - 1,
                        displayName: this.state.displayName,
                        status: this.state.status,
                        orgName: this.state.orgName,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.organizationRequest(data);
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.organization.data.prePage,
                current: this.props.administration.organization.data.currPage,
                next: this.props.administration.organization.data.currPage + 1,
                maxPage: this.props.administration.organization.data.maxPage,
            })
            if (this.props.administration.organization.data.currPage != this.props.administration.organization.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.administration.organization.data.currPage + 1,
                    displayName: this.state.displayName,
                    status: this.state.status,
                    orgName: this.state.orgName,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.organizationRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            }
            else {
                this.setState({
                    prev: this.props.administration.organization.data.prePage,
                    current: this.props.administration.organization.data.currPage,
                    next: this.props.administration.organization.data.currPage + 1,
                    maxPage: this.props.administration.organization.data.maxPage,
                })
                if (this.props.administration.organization.data.currPage <= this.props.administration.organization.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        displayName: this.state.displayName,
                        status: this.state.status,
                        orgName: this.state.orgName,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.organizationRequest(data)
                }
            }

        }
    }



    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        let data = {
            type: 1,
            no: 1,
            search: "",
            searchBy: this.state.searchBy
        }
        this.props.organizationRequest(data);
    }

    onClearFilter(e) {
        this.setState({
            type: 1,
            no: 1,
            search: "",
            displayName: "",
            status: "",
            orgName: "",
        })
        let data = {
            type: 1,
            no: 1,
            search: "",
            searchBy: this.state.searchBy
        }
        this.props.organizationRequest(data);
    }

    xlscsv(type) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/download/module/${type}/Organisation`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
            });
    }

    getAllData(type) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/download/module/${type}/Organisation`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
            });
    }

    onRefresh = () =>{
        let data = {
            type: this.state.type,
            no: this.state.current,
            displayName: this.state.displayName,
            status: this.state.status,
            orgName: this.state.orgName,
            search: this.state.search,
            searchBy: this.state.searchBy
        }
        this.props.organizationRequest(data)
    }

    openColoumSetting(data) {
        if (this.state.customHeadersState.length == 0) {
            this.setState({
                headerCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                coloumSetting: true
            })
        } else {
            this.setState({
                coloumSetting: false
            })
        }
    }
    pushColumnData(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let customHeadersState = this.state.customHeadersState
        let headerConfigDataState = this.state.headerConfigDataState
        let customHeaders = this.state.customHeaders
        let saveState = this.state.saveState
        if (this.state.headerCondition) {
            if (!data.includes(getHeaderConfig) || this.state.getHeaderConfig.length == 0) {
                getHeaderConfig.push(data)
                if (!getHeaderConfig.includes(headerConfigDataState)) {
                    let keyget = (_.invert(headerConfigDataState))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(this.state.defaultHeaderMap)) {
                    let keygetvalue = (_.invert(headerConfigDataState))[data];
                    this.state.defaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(customHeadersState) || this.state.customHeadersState.length == 0) {
                customHeadersState.push(data)
                if (!customHeadersState.includes(headerConfigDataState)) {
                    let keyget = (_.invert(headerConfigDataState))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(this.state.headerSummary)) {
                    let keygetvalue = (_.invert(headerConfigDataState))[data];
                    this.state.headerSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            getHeaderConfig,
            customHeadersState,
            customHeaders,
            saveState
        })
    }
    closeColumn(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let headerConfigState = this.state.headerConfigState
        let customHeaders = []
        let customHeadersState = this.state.customHeadersState
        if (!this.state.headerCondition) {
            for (let j = 0; j < customHeadersState.length; j++) {
                if (data == customHeadersState[j]) {
                    customHeadersState.splice(j, 1)
                }
            }
            for (var key in headerConfigState) {
                if (!customHeadersState.includes(headerConfigState[key])) {
                    customHeaders.push(key)
                }
            }
            if (this.state.customHeadersState.length == 0) {
                this.setState({
                    headerCondition: false
                })
            }
        } else {
            for (var i = 0; i < getHeaderConfig.length; i++) {
                if (data == getHeaderConfig[i]) {
                    getHeaderConfig.splice(i, 1)
                }
            }
            for (var key in headerConfigState) {
                if (!getHeaderConfig.includes(headerConfigState[key])) {
                    customHeaders.push(key)
                }
            }
        }
        customHeaders.forEach(e => delete headerConfigState[e]);
        this.setState({
            getHeaderConfig,
            customHeaders: headerConfigState,
            customHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.headerConfigDataState))[data];
            let saveState = this.state.saveState
            saveState.push(keygetvalue)
            let headerSummary = this.state.headerSummary
            let defaultHeaderMap = this.state.defaultHeaderMap
            if (!this.state.headerCondition) {
                for (let j = 0; j < headerSummary.length; j++) {
                    if (keygetvalue == headerSummary[j]) {
                        headerSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < defaultHeaderMap.length; i++) {
                    if (keygetvalue == defaultHeaderMap[i]) {
                        defaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                headerSummary,
                defaultHeaderMap,
                saveState
            })
        }, 100);
    }
    saveColumnSetting(e) {
        this.setState({
            coloumSetting: false,
            headerCondition: false,
            saveState: []
        })
        let payload = {
            module: "ADMINISTRATION",
            subModule: "ORGANISATION",
            section: "MANAGEORGANISATION",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            displayName: "ORG_TABLE_HEADER",
            attributeType: "TABLE HEADER",
            fixedHeaders: this.state.fixedHeaderData,
            defaultHeaders: this.state.headerConfigDataState,
            customHeaders: this.state.customHeaders
        }
        this.props.createHeaderConfigRequest(payload)
    }
    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // paraMsg: "Click confirm to continue.",
            confirmModal: true,
        })
    }
    resetColumn() {
        let payload = {
            module: "ADMINISTRATION",
            subModule: "ORGANISATION",
            section: "MANAGEORGANISATION",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            displayName: "ORG_TABLE_HEADER",
            attributeType: "TABLE HEADER",
            fixedHeaders: this.state.fixedHeaderData,
            defaultHeaders: this.state.headerConfigDataState,
            customHeaders: this.state.headerConfigDataState
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            headerCondition: true,
            coloumSetting: false,
            saveState: []
        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }
    handleChange(e) {
        if (e.target.id == "searchByOrg") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        displayName: this.state.displayName,
                        status: this.state.status,
                        orgName: this.state.orgName,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.organizationRequest(data)
                }
            })

        }

    }
    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
            this.setState({ modalOpen: false, orgModalAnimation:false})
        }
    }
    render() {
        return (
            <div className="container-fluid pad-0" id="vendor_manage">
                <div className="col-lg-12 pad-0">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <form onSubmit={(e) => this.onSearch(e)}>
                                        <input onChange={(e) => this.handleSearch(e)} value={this.state.search} type="search" placeholder="Type to Search..." className="search_bar" />
                                        <img className="search-image" src={SearchImage} />
                                        {this.state.type == 3 ? <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></span> : null}
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                {/* <select id="searchByOrg" name="searchByOrg" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                    <option value="contains">Contains</option>
                                    <option value="startWith"> Start with</option>
                                    <option value="endWith">End with</option>
                                </select> */}
                                {/* <div className="tooltip" onClick={() => this.props.history.push('/administration/organisation/addOrganisation')}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                                        <g fill="none" fillRule="evenodd">
                                            <circle cx="17.5" cy="17.5" r="17.5" fill="#8b77fa" />
                                            <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7" />
                                        </g>
                                    </svg>
                                    <p className="tooltiptext tooltip-left">
                                        Add Organisation <span>Please click on add icon to add new organisation
                                        </span>
                                    </p>
                                </div> */}
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Export Excel</span>
                                    </button>
                                    {this.state.showDownloadDrop ? (
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={(e) => this.xlscsv("XLS")}>
                                                    {/* <img src={ExportExcel} /> */}
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                            <li>
                                                <button className="export-excel" type="button" onClick={()=>this.getAllData("XLS")}>
                                                    <span className="pi-export-svg">
                                                        <img src={require('../../../assets/downloadAll.svg')} />
                                                    </span>
                                                Download All</button>
                                            </li>
                                        </ul>
                                    ) : (null)}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                </div>
                                {/* <div className="gvpd-filter">
                                    <button className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={e => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.filter && <VendorFilter closeFilter={(e) => this.closeFilter(e)} />}
                                    {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState?this.state.checkedFilters.map((keys,index)=>(
                    <div className="show-applied-filter">
                            {index===0 ?<button type="button" className="saf-clear-all" onClick={(e)=>this.clearAllTag(e)}>Clear All</button>:null}
                        <button type="button" className="saf-btn">{keys}
                            <img onClick={(e)=>this.clearTag(e,index)} src={require('../../../assets/clearSearch.svg')} />
                    {/* <span className="generic-tooltip">{Object.values(this.state.filteredValue)[index]}</span> */}
                    <span className="generic-tooltip">{typeof (Object.values(this.state.filteredValue)[index]) == 'object' ? Object.values(Object.values(this.state.filteredValue)[index]).join(',') : Object.values(this.state.filteredValue)[index]}</span>
                        </button>
                    </div>)):''}
                </div>
                <div className="col-md-12 pad-0 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table" id="table-scroll">
                            {/* <ColoumSetting {...this.props} {...this.state} coloumSetting={this.state.coloumSetting} getHeaderConfig={this.state.getHeaderConfig} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} /> */}
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img onClick={() => this.onRefresh()} src={refreshIcon} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                                <img src={filterIcon} className="imgHead" />
                                            </th>
                                        )) : this.state.customHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                                <img src={filterIcon} className="imgHead" />
                                            </th>
                                        ))}
                                    </tr>
                                </thead>

                                <tbody>
                                    {this.state.organizationState == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.organizationState.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.organizationState.map((data, key) => (
                                        <tr key={key}>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner til-edit-btn" onClick={() => this.organisationModalOpen(`${data.id}`)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Edit</span>
                                                    </li>
                                                </ul>
                                            </td>
                                            {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => (
                                                <td key={key} >
                                                    <label>{data[hdata]}</label>
                                                </td>
                                            )) : this.state.headerSummary.map((sdata, keyy) => (
                                                <td key={keyy} >
                                                    <label>{data[sdata]}</label>
                                                </td>
                                            ))}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" min="1" value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <div className="pagination-inner">
                                            <ul className="pagination-item">
                                            {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ?
                                                <li>
                                                    <button>
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> :
                                                <li>
                                                    <button onClick={(e) => this.page(e)} id="first" >
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                {this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ?
                                                <li>
                                                    <button onClick={(e) => this.page(e)} id="prev">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> :
                                                <li>
                                                    <button>
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                <li>
                                                    <button className="pi-number-btn">
                                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                                    </button>
                                                </li>
                                                {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage ? <li >
                                                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li> : <li >
                                                        <button className="" disabled>
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>}
                                                <li>
                                                    <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                        <span className="page-item-btn-inner">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                        {/* {this.state.filter ? <OrganizationFilter displayName={this.state.displayName} status={this.state.status} orgName={this.state.orgName} updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} /> : null} */}
                        {this.state.modalOpen ? <OrganisationModal {...this.state} {...this.props} orgId={this.state.orgId} orgModalAnimation={this.state.orgModalAnimation} modalOpen={(e) => this.organisationModalOpen(e)} /> : null}
                    </div>
                </div>

            </div>

        );
    }
}

export default ManageOrganization;
