import React from "react";
import RightSideBar from "../../rightSideBar";
import Footer from '../../footer'
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";
import NewSideBar from "../../../components/newSidebar";

class AddOrganization extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDefault: "FALSE",
            name: "",
            nameerr: false,
            displayname: "",
            displaynameerr: false,
            description: "",
            // descriptionerr: false,
            contactPerson: "",
            contactPersonerr: false,
            successMessage: "",
            errorMessage: "",
            contactNumber: "",
            contactNumbererr: false,
            email: "",
            emailerr: false,
            status: "Active",
            statuserr: false,
            billtoAddress: "",
            billtoAddresserr: false,
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            rightbar: false,
            errorCode: "",
            code: "",
            openRightBar: false
        };
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    // componentWillMount() {
    //     if (sessionStorage.getItem('token') == null) {
    //         this.props.history.push('/');
    //     }
    // }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.administration.addOrganization.isSuccess && !nextProps.administration.addOrganization.isError && !nextProps.administration.addOrganization.isLoading) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.administration.addOrganization.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.addOrganization.data.message,
                loader: false,
                success: true
            })
            this.props.addOrganizationClear();
            this.onClear();
        } else if (nextProps.administration.addOrganization.isError) {
            this.setState({
                errorMessage: nextProps.administration.addOrganization.message.error == undefined ? undefined : nextProps.administration.addOrganization.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.addOrganization.message.status,
                errorCode: nextProps.administration.addOrganization.message.error == undefined ? undefined : nextProps.administration.addOrganization.message.error.errorCode
            })
            this.props.addOrganizationClear();
        } else if (nextProps.administration.addOrganization.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    onClear() {
        // e.preventDefault();
        this.setState({
            name: "",
            displayname: "",
            description: "",
            contactPerson: "",
            contactNumber: "",
            email: "",
            status: "",
            billtoAddress: "",
        })
    }
    //organization  name
    organisationName() {
        if (
            this.state.name == "" ||
            !this.state.name.match(/^[a-zA-Z ]+$/)
        ) {
            this.setState({
                nameerr: true
            });
        } else {
            this.setState({
                nameerr: false
            });
        }
    }
    displayName() {
        if (
            this.state.displayname == "" ||
            !this.state.displayname.match(/^[a-zA-Z ]+$/)
        ) {
            this.setState({
                displaynameerr: true
            });
        } else {
            this.setState({
                displaynameerr: false
            });
        }
    }
    description() {
        if (
            this.state.description == "" || this.state.description.trim() == "") {
            this.setState({
                descriptionerr: true
            });
        } else {
            this.setState({
                descriptionerr: false
            });
        }
    }
    contactPerson() {
        if (
            this.state.contactPerson == "" || !this.state.contactPerson.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                contactPersonerr: true
            });
        } else {
            this.setState({
                contactPersonerr: false
            });
        }
    }
    contactNumber() {
        if (
            this.state.contactNumber == "" ||
            !this.state.contactNumber.match(/^\d{10}$/) ||
            !this.state.contactNumber.match(/^[1-9]\d+$/) ||
            !this.state.contactNumber.match(/^.{10}$/)) {
            this.setState({
                contactNumbererr: true
            });
        } else {
            this.setState({
                contactNumbererr: false
            });
        }
    }
    email() {

        if (
            this.state.email == "" ||
            !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }
    status() {
        if (
            this.state.status == "") {
            this.setState({
                statuserr: true
            });
        } else {
            this.setState({
                statuserr: false
            });
        }
    }
    billtoAddress() {
        if (
            this.state.billtoAddress == "" || this.state.billtoAddress.trim() == "") {
            this.setState({
                billtoAddresserr: true
            });
        } else {
            this.setState({
                billtoAddresserr: false
            });
        }
    }


    contactSubmit(e) {
        e.preventDefault();
        this.organisationName();
        this.displayName();
        // this.description();
        this.contactPerson();
        this.contactNumber();
        this.email();
        this.status();
        this.billtoAddress();

        const t = this;
        setTimeout(function (e) {
            const { isDefault, name, displayname, description, contactPerson, contactNumber, email, status, billtoAddress, nameerr, displaynameerr, descriptionerr, contactPersonerr, contactNumbererr, emailerr, statuserr, billtoAddresserr } = t.state;
            if (!nameerr && !displaynameerr && !contactPersonerr && !contactNumbererr && !emailerr && !statuserr && !billtoAddresserr) {
                t.setState({
                    loader: true,
                    alert: false,
                    success: false
                })
                let organizationData = {

                    orgName: name,
                    displayName: displayname,
                    description: description == "" ? "NA" : description,
                    contPerson: contactPerson,
                    contNumber: contactNumber,
                    contEmail: email,
                    status: status,
                    billToAddress: billtoAddress,
                    isDefault: isDefault


                }
                t.props.addOrganizationRequest(organizationData);

            }
        }, 100)

    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    handleChange(e) {
        console.log(e)
        console.log(e.target)
        if (e.target.id == "name") {
            this.setState(
                {
                    name: e.target.value
                },
                () => {
                    this.organisationName();
                }
            );
        } else if (e.target.id == "displayname") {
            this.setState(
                {
                    displayname: e.target.value
                },
                () => {
                    this.displayName();
                }
            );
        } else if (e.target.id == "description") {
            this.setState(
                {
                    description: e.target.value
                },
                () => {
                    this.description();
                }
            );
        } else if (e.target.id == "contactPerson") {
            this.setState({
                contactPerson: e.target.value
            },
                () => {
                    this.contactPerson();
                });
        }
        else if (e.target.id == "contactNumber") {
            this.setState({
                contactNumber: e.target.value
            }, () => {
                this.contactNumber();
            });
        }
        else if (e.target.id == "email") {
            this.setState({
                email: e.target.value
            }, () => {
                this.email();
            });
        }
        else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            }, () => {
                this.status();
            });
        }
        else if (e.target.id == "billtoAddress") {
            this.setState({
                billtoAddress: e.target.value
            }, () => {
                this.billtoAddress();
            });
        }
    }

    render() {

        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        const {
            name,
            nameerr,
            displayname,
            displaynameerr,
            description,
            descriptionerr,
            contactPerson,
            contactPersonerr,
            contactNumber,
            contactNumbererr,
            email,
            emailerr,
            status,
            statuserr,
            billtoAddress,
            billtoAddresserr
        } = this.state;
        return (

            <div className="container-fluid nc-design pad-l60">
                <NewSideBar {...this.props} />
                <SideBar {...this.props} />
                <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                <div className="container_div m-top-75 " id="">
                    <div className="col-md-12 col-sm-12 border-btm">
                        <div className="menu_path">
                            <ul className="list-inline width_100 pad20">
                                <label className="home_link"><BraedCrumps /></label>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="container-fluid pad-0">
                    <div className="container_div" id="">
                        <div className="container-fluid pad-0">
                            <form name="organizationForm" className="organizationForm" onSubmit={(e) => this.contactSubmit((e)) } autoComplete="off" > 
                                <div className="col-lg-12 pad-0 ">
                                    <div className="gen-vendor-potal-design p-lr-37">
                                        <div className="col-lg-6 pad-0">
                                            <div className="gvpd-left">
                                                <div className="add-organization">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="16" viewBox="0 0 20.4 17">
                                                        <path d="M19.55 7.65H2.593l6.493-6.185A.85.85 0 1 0 7.913.234L.5 7.3a1.7 1.7 0 0 0 .015 2.419l7.4 7.049a.85.85 0 1 0 1.173-1.231l-6.52-6.185H19.55a.85.85 0 0 0 0-1.7z" data-name="arrow (2)"/>
                                                    </svg>
                                                    <span>Add Organization</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 pad-0">
                                            <div className="gvpd-right">
                                                <button className="gen-save" type="submit">Save</button>
                                                <button type="button" className="gen-clear" onClick={() => this.onClear()} type="reset">Clear</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="macHeight add-organisation-page">
                                        <div className="StickyDiv manage-sticky-div">
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-10">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Organisation Name</label>
                                                    <input type="text" ref="name"
                                                        name="name"
                                                        id="name" autoComplete="off" value={name} onChange={e => this.handleChange(e)} placeholder="Name" className={nameerr ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} />
                                                    {/* {nameerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {nameerr ? (
                                                        <span className="error">
                                                            Enter  name
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Display Name</label>
                                                    <input type="text" autoComplete="off" ref="displayname" name="displayname" id="displayname"
                                                        value={displayname} onChange={e => this.handleChange(e)} placeholder="Display Name" className={displaynameerr ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} />
                                                    {/* {displaynameerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {displaynameerr ? (
                                                        <span className="error">
                                                            Enter display name
                                                        </span>
                                                    ) : null}

                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Description</label>
                                                    <input type="text" onChange={e => this.handleChange(e)} ref="description" name="description" id="description" value={description} placeholder="Description" className="orgnisationTextbox"></input>

                                                    {/* {descriptionerr ? <img src={errorIcon} className="error_icon-txtarea" /> : null}
                                                    {descriptionerr ? (
                                                        <span className="error">
                                                            Enter Description
                                                        </span>
                                                    ) : null} */}
                                                </div>

                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Contact Person</label>
                                                    <input type="text" autoComplete="off" onChange={e => this.handleChange(e)} name="contactPerson" id="contactPerson" value={contactPerson} placeholder="Contact Person" className={contactPersonerr ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} />
                                                    {/* {contactPersonerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {contactPersonerr ? (
                                                        <span className="error">
                                                            Enter contact person
                                                        </span>
                                                    ) : null}

                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Contact Number</label>
                                                    <input type="text" autoComplete="off" maxLength="10" onChange={e => this.handleChange(e)} name="contactNumber" id="contactNumber" value={contactNumber} placeholder="Contact Number" className={contactNumbererr ? "errorBorder orgnisationTextbox numbersOnly" : "orgnisationTextbox numbersOnly"} />
                                                    {/* {contactNumbererr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {contactNumbererr ? (
                                                        <span className="error">
                                                            Enter valid contact number
                                                        </span>
                                                    ) : null}

                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Contact Email</label>
                                                    <input type="text" autoComplete="off" onChange={e => this.handleChange(e)} name="email" id="email" value={email} placeholder="Email" className={emailerr ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} />
                                                    {/* {emailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {emailerr ? (
                                                        <span className="error">
                                                            Enter valid email address
                                                        </span>
                                                    ) : null}

                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label>Status</label>
                                                    <select
                                                        ref="status"
                                                        name="status"
                                                        id="status"
                                                        className={statuserr ? "errorBorder orgnisationTextbox displayPointer" : "orgnisationTextbox displayPointer"}
                                                        onChange={e => this.handleChange(e)}
                                                        value={status}
                                                    >
                                                        <option value="">Status</option>
                                                        <option value="Active">Active</option>
                                                        <option value="Inactive">Inactive</option>
                                                    </select>
                                                    {/* {statuserr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {statuserr ? (
                                                        <span className="error" >
                                                            Select Status
                                                        </span>
                                                    ) : null}
                                                </div>

                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                                <div className="col-md-4 col-sm-4 pad-lft-0">
                                                    <label>Address</label>
                                                    <textarea ref="billtoAddress" onChange={e => this.handleChange(e)} name="billtoAddress" id="billtoAddress" value={billtoAddress} className={billtoAddresserr ? "errorBorder orgnisationTextarea" : "orgnisationTextarea"} placeholder="Address"></textarea>
                                                    {/* {billtoAddresserr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                                                    {billtoAddresserr ? (
                                                        <span className="error" >
                                                            Address
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-30">
                                                <div className="ao-billing-details">
                                                    <h4>Billing Details</h4>
                                                    <div className="aobd-shipping">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkBox" />
                                                            <span className="checkmark1"></span>
                                                        </label>
                                                        <p>Shipping details same as billing details</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>GSTIN</label>
                                                    <input type="text" className="orgnisationTextbox" />
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>Country</label>
                                                    <select>
                                                        <option>Select Country</option>
                                                    </select>
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>State</label>
                                                    <select>
                                                        <option>Select State</option>
                                                    </select>
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>City</label>
                                                    <select>
                                                        <option>Select City</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <div className="col-md-4 pad-lft-0">
                                                    <label>Address</label>
                                                    <textarea type="text" className="orgnisationTextarea"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-30">
                                                <div className="ao-billing-details">
                                                    <h4>Shipping</h4>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>GSTIN</label>
                                                    <input type="text" className="orgnisationTextbox" />
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>Country</label>
                                                    <select>
                                                        <option>Select Country</option>
                                                    </select>
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>State</label>
                                                    <select>
                                                        <option>Select State</option>
                                                    </select>
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>City</label>
                                                    <select>
                                                        <option>Select City</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <div className="col-md-4 pad-lft-0">
                                                    <label>Address</label>
                                                    <textarea type="text" className="orgnisationTextarea"></textarea>
                                                </div>
                                            </div>
                                            {/* <div className="col-md-12 pad-0">
                                                <div className="col-md-2 pad-lft-0">
                                                    <div className="piImageModalMain m-top-20 addFile">
                                                        <label className="displayBlock">Upload Logo</label>
                                                        <div className="chooseFileBtn" id="chooseImage">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 29 29">
                                                                    <path fill="#50E3C2" fill-rule="nonzero" d="M14.5 0C6.492 0 0 6.492 0 14.5S6.492 29 14.5 29 29 22.508 29 14.5 22.508 0 14.5 0zm6.844 15.467h-5.877v5.877a.967.967 0 1 1-1.934 0v-5.877H7.656a.967.967 0 0 1 0-1.934h5.877V7.656a.967.967 0 0 1 1.934 0v5.877h5.877a.967.967 0 1 1 0 1.934z" />
                                                                </svg>
                                                                <input type="file" className="imageChooser" onChange={(e) => this.fileChange(e)} />
                                                                <label className="chosenImage" >
                                                                    Add File
                                                      </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> */}
                                            {/* <div className="assortmentCheck displayInline m-top-20">
                                                <label className="checkBoxLabel0"><input type="checkBox" id="itemName" />Set as default logo <span className="checkmark1"></span> </label>
                                            </div> */}
                                        </div>
                                        {/* <div className="col-md-12 col-sm-12 pad-0">
                                            <div className="footerDivForm">
                                                <ul className="list-inline m-top-10">
                                                    <li>
                                                        <button type="button" className="clearbtnOrganisation" onClick={() => this.onClear()} type="reset">
                                                            Clear
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button className="savebtnOrganisation" type="submit">Save</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> */}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}


            </div>

        );
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddOrganization);
