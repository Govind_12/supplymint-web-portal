import React from 'react';
import errorIcon from "../../../assets/error_icon.svg";
import Json from '../../administration/jsonFile.json';
import { getStoreDataClear } from '../../../redux/actions';
import axios from 'axios';
import { CONFIG } from '../../../config/index';
const allState = Object.keys(Json[0].states)
const allLocations = Json[0].states

class OrganisationModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      nameerr: false,
      displayname: "",
      displaynameerr: false,
      // description: "",
      // descriptionerr: false,
      contactPerson: "",
      contactPersonerr: false,
      contactNumber: "",
      contactNumbererr: false,
      email: "",
      emailerr: false,
      status: "",
      statuserr: false,
      billtoAddress: "",
      billtoAddresserr: false,
      editData: [],
      btnDisable: true,
      cinNumber:"",
      termsName:"",
      logoName:"",

      billingGSTIN: "",
      billingGSTINErr: false,
      billingCountry: "",
      billingCountryErr: false,
      billingState: "",
      billingStateErr: false,
      billingCity: "",
      billingCityErr: false,
      billingAddress: "",
      billingAddressErr: false,

      shippingGSTIN: "",
      shippingGSTINErr: false,
      shippingCountry: "",
      shippingCountryErr: false,
      shippingState: "",
      shippingStateErr: false,
      shippingCity: "",
      shippingCityErr: false,
      shippingAddress: "",
      shippingAddressErr: false,

      billingCities: [],
      shippingCities: [],
      selectedFile:null,
      fileUrl:null,
      filePath:null,
    };
  }
  componentWillMount() {
    console.log(this.props.administration.organization);
    for (let i = 0; i < this.props.administration.organization.data.resource.length; i++) {
      if (this.props.administration.organization.data.resource[i].id == this.props.orgId) {
        console.log(this.props.administration.organization.data.resource[i])
        this.setState({

          orgId: this.props.administration.organization.data.resource[i].id,
          name: this.props.administration.organization.data.resource[i].name,
          billtoAddress: this.props.administration.organization.data.resource[i].billToAddress,
          displayname: this.props.administration.organization.data.resource[i].displayName,
          // description: this.props.administration.organization.data.resource[i].description,
          contactPerson: this.props.administration.organization.data.resource[i].contPerson,
          contactNumber: this.props.administration.organization.data.resource[i].contNumber,
          status: this.props.administration.organization.data.resource[i].status,
          email: this.props.administration.organization.data.resource[i].contEmail,
          cinNumber:this.props.administration.organization.data.resource[i].cin,
          logoName:this.props.administration.organization.data.resource[i].logo,
          termsName:this.props.administration.organization.data.resource[i].terms,
          fileUrl:this.props.administration.organization.data.resource[i].imageUrl,
          billingGSTIN: this.props.administration.organization.data.resource[i].gstIn,
          billingCountry: this.props.administration.organization.data.resource[i].country,
          billingState: this.props.administration.organization.data.resource[i].state,
          billingCity: this.props.administration.organization.data.resource[i].city,
          billingAddress: this.props.administration.organization.data.resource[i].fullAddress,

          shippingGSTIN: this.props.administration.organization.data.resource[i].shipGSTIN,
          shippingCountry: this.props.administration.organization.data.resource[i].shipCountry,
          shippingState: this.props.administration.organization.data.resource[i].shipState,
          shippingCity: this.props.administration.organization.data.resource[i].shipCity,
          shippingAddress: this.props.administration.organization.data.resource[i].shipAddress,

          billingCities: allLocations[this.props.administration.organization.data.resource[i].state],
          shippingCities: allLocations[this.props.administration.organization.data.resource[i].shipState],

        });

      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.editOrganization.isSuccess) {
               
      this.props.modalOpen()
    }
  }

  onClear(e) {
    e.preventDefault();
    this.setState({
      name: "",
      displayname: "",
      // description: "",
      contactPerson: "",
      contactNumber: "",
      email: "",
      status: "",
      billtoAddress: "",
      cinNumber:"",
      termsName:"",
      logoName:"",

      billingGSTIN: "",
      billingCountry: "",
      billingState: "",
      billingCity: "",
      billingAddress: "",

      shippingGSTIN: "",
      shippingCountry: "",
      shippingState: "",
      shippingCity: "",
      shippingAddress: "",
    })
  }

  //organization  name
  organisationName() {
    if (
      this.state.name == undefined || 
      this.state.name == "" ||
      !this.state.name.match(/^[a-zA-Z ]+$/)
    ) {
      this.setState({
        nameerr: true
      });
    } else {
      this.setState({
        nameerr: false
      });
    }
  }
  displayName() {
    if (
      this.state.displayname == undefined || 
      this.state.displayname == "")
    // ||
    // !this.state.displayname.match(/^[a-zA-Z ]+$/)
    {
      this.setState({
        displaynameerr: true
      });
    } else {
      this.setState({
        displaynameerr: false
      });
    }
  }
  // description() {
  //   if (
  //     this.state.description == undefined || 
  //     this.state.description == "" || this.state.description.trim() == "") {
  //     this.setState({
  //       descriptionerr: true
  //     });
  //   } else {
  //     this.setState({
  //       descriptionerr: false
  //     });
  //   }
  // }
  contactPerson() {
    if (
      this.state.contactPerson == undefined || 
      this.state.contactPerson == "" || !this.state.contactPerson.match(/^[a-zA-Z ]+$/)) {
      this.setState({
        contactPersonerr: true
      });
    } else {
      this.setState({
        contactPersonerr: false
      });
    }
  }
  contactNumber() {
    if (
      this.state.contactNumber == undefined || 
      this.state.contactNumber == "" ||
      !this.state.contactNumber.match(/^\d{10}$/) ||
      !this.state.contactNumber.match(/^[1-9]\d+$/) ||
      !this.state.contactNumber.match(/^.{10}$/)) {
      this.setState({
        contactNumbererr: true
      });
    } else {
      this.setState({
        contactNumbererr: false
      });
    }
  }
  email() {

    if (
      this.state.email == undefined || 
      this.state.email == "" ||
      !this.state.email.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      this.setState({
        emailerr: true
      });
    } else {
      this.setState({
        emailerr: false
      });
    }
  }
  status() {
    if (
      this.state.status == undefined || 
      this.state.status == "") {
      this.setState({
        statuserr: true
      });
    } else {
      this.setState({
        statuserr: false
      });
    }
  }
  billtoAddress() {
    if (
      this.state.billtoAddress == undefined ||
      this.state.billtoAddress == "" || this.state.billtoAddress.trim() == "") {
      this.setState({
        billtoAddresserr: true
      });
    } else {
      this.setState({
        billtoAddresserr: false
      });
    }
  }

  billingGSTIN() {
    if (
      this.state.billingGSTIN == undefined || 
      this.state.billingGSTIN == "" )
      {
      this.setState({
        billingGSTINErr: true
      });
    } else {
      this.setState({
        billingGSTINErr: false
      });
    }
  }
  billingCountry() {
    if (
      this.state.billingCountry == undefined || 
      this.state.billingCountry == "")
    {
      this.setState({
        billingCountryErr: true
      });
    } else {
      this.setState({
        billingCountryErr: false
      });
    }
  }
  billingState() {
    if (
      this.state.billingState == undefined || 
      this.state.billingState == "")
    {
      this.setState({
        billingStateErr: true
      });
    } else {
      this.setState({
        billingStateErr: false
      });
    }
  }
  billingCity() {
    if (
      this.state.billingCity == undefined || 
      this.state.billingCity == "")
    {
      this.setState({
        billingCityErr: true
      });
    } else {
      this.setState({
        billingCityErr: false
      });
    }
  }
  billingAddress() {
    if (
      this.state.billingAddress == undefined || 
      this.state.billingAddress == "")
    {
      this.setState({
        billingAddressErr: true
      });
    } else {
      this.setState({
        billingAddressErr: false
      });
    }
  }

  shippingGSTIN() {
    if (
      this.state.shippingGSTIN == undefined || 
      this.state.shippingGSTIN == "" )
      {
      this.setState({
        shippingGSTINErr: true
      });
    } else {
      this.setState({
        shippingGSTINErr: false
      });
    }
  }
  shippingCountry() {
    if (
      this.state.shippingCountry == undefined || 
      this.state.shippingCountry == "")
    {
      this.setState({
        shippingCountryErr: true
      });
    } else {
      this.setState({
        shippingCountryErr: false
      });
    }
  }
  shippingState() {
    if (
      this.state.shippingState == undefined || 
      this.state.shippingState == "")
    {
      this.setState({
        shippingStateErr: true
      });
    } else {
      this.setState({
        shippingStateErr: false
      });
    }
  }
  shippingCity() {
    if (
      this.state.shippingCity == undefined || 
      this.state.shippingCity == "")
    {
      this.setState({
        shippingCityErr: true
      });
    } else {
      this.setState({
        shippingCityErr: false
      });
    }
  }
  shippingAddress() {
    if (
      this.state.shippingAddress == undefined || 
      this.state.shippingAddress == "")
    {
      this.setState({
        shippingAddressErr: true
      });
    } else {
      this.setState({
        shippingAddressErr: false
      });
    }
  }

  //submit
  contactSubmit(e) {
    e.preventDefault();
    this.organisationName();
    this.displayName();
    // this.description();
    this.contactPerson();
    this.contactNumber();
    this.email();
    this.status();
    this.billtoAddress();

    this.billingGSTIN();
    this.billingCountry();
    this.billingState();
    this.billingCity();
    this.billingAddress();

    this.shippingGSTIN();
    this.shippingCountry();
    this.shippingState();
    this.shippingCity();
    this.shippingAddress();

    const t = this;
    setTimeout(function () {
      const { orgId, name, displayname, contactPerson, contactNumber, email, status, billtoAddress, cinNumber,termsName,logoName,
              fileUrl,
              nameerr, displaynameerr, contactPersonerr, contactNumbererr, emailerr, statuserr, 
              billtoAddresserr, billingGSTIN, billingGSTINErr, billingCountry, billingCountryErr, billingState, billingStateErr,
              billingCity, billingCityErr, billingAddress, billingAddressErr, shippingGSTIN, shippingGSTINErr, shippingCountry, 
              shippingCountryErr, shippingState, shippingStateErr, shippingCity, shippingCityErr, shippingAddress, shippingAddressErr } = t.state;
      if (!nameerr && !displaynameerr && !contactPersonerr && !contactNumbererr && !emailerr && !statuserr && !billtoAddresserr
          && !billingGSTINErr && !billingCountryErr && !billingStateErr && !billingCityErr && !billingAddressErr
          && !shippingGSTINErr && !shippingCountryErr && !shippingStateErr && !shippingCityErr && !shippingAddressErr) {

        let editOrganizationData = {
          type: t.props.type,
          no: t.props.no,
          displayName: t.props.displayName,
          status: t.props.status,
          orgName: t.props.orgName,
          search: t.props.search,

          orgId: orgId,
          orgNameE: name,
          displayNameE: displayname,
          // description: description,
          contPerson: contactPerson,
          contNumber: contactNumber,
          contEmail: email,
          statusE: status,
          billToAddress: billtoAddress,
          cin:cinNumber,
          terms:termsName,
          logo:logoName,
         
          billingGSTIN: billingGSTIN,
          billingCountry: billingCountry,
          billingState: billingState,
          billingCity: billingCity,
          billingAddress: billingAddress,

          shippingGSTIN: shippingGSTIN,
          shippingCountry: shippingCountry,
          shippingState: shippingState,
          shippingCity: shippingCity,
          shippingAddress: shippingAddress,

        }
        t.props.editOrganizationRequest(editOrganizationData);
        //    onClear(e);
        t.props.modalOpen();
      }
    }, 100)

  }



  //handleChange
  handleChange(e) {
    this.setState({
      btnDisable: false
    })
    if (e.target.id == "name") {
      this.setState(
        {
          name: e.target.value
        },
        () => {
          this.organisationName();
        }
      );
    } else if (e.target.id == "displayname") {
      this.setState(
        {
          displayname: e.target.value
        },
        () => {
          this.displayName();
        }
      );
    } 
    // else if (e.target.id == "description") {
    //   this.setState(
    //     {
    //       description: e.target.value
    //     },
    //     () => {
    //       //   this.description();
    //     }
    //   );
    //} 
    else if (e.target.id == "contactPerson") {
      this.setState({
        contactPerson: e.target.value
      },
        () => {
          this.contactPerson();
        });
    }
    else if (e.target.id == "contactNumber") {
      this.setState({
        contactNumber: e.target.value
      }, () => {
        this.contactNumber();
      });
    }
    else if (e.target.id == "email") {
      this.setState({
        email: e.target.value
      }, () => {
        this.email();
      });
    }
    else if (e.target.id == "status") {
      this.setState({
        status: e.target.value
      }, () => {
        this.status();
      });
    }
    else if (e.target.id == "billtoAddress") {
      this.setState({
        billtoAddress: e.target.value
      }, () => {
        this.billtoAddress();
      });
    }

    else if (e.target.id == "billingGSTIN") {
      this.setState({
        billingGSTIN: e.target.value
      }, () => {
        this.billingGSTIN();
      });
    }
    else if (e.target.id == "billingCountry") {
      this.setState({
        billingCountry: e.target.value
      }, () => {
        this.billingCountry();
      });
    }
    else if (e.target.id == "billingCity") {
      this.setState({
        billingCity: e.target.value
      }, () => {
        this.billingCity();
      });
    }
    else if (e.target.id == "billingState") {
      this.setState({
        billingState: e.target.value,
        billingCities : allLocations[e.target.value]
      }, () => {
        this.billingState();
    })
    }
    else if (e.target.id == "billingAddress") {
      this.setState({
        billingAddress: e.target.value
      }, () => {
        this.billingAddress();
      });
    }

    else if (e.target.id == "shippingGSTIN") {
      this.setState({
        shippingGSTIN: e.target.value
      }, () => {
        this.shippingGSTIN();
      });
    }
    else if (e.target.id == "shippingCountry") {
      this.setState({
        shippingCountry: e.target.value
      }, () => {
        this.shippingCountry();
      });
    }
    else if (e.target.id == "shippingCity") {
      this.setState({
        shippingCity: e.target.value
      }, () => {
        this.shippingCity();
      });
    }
    else if (e.target.id == "shippingState") {
      this.setState({
        shippingState: e.target.value,
        shippingCities : allLocations[e.target.value]
      }, () => {
        this.shippingState();
      });
    }
    else if (e.target.id == "shippingAddress") {
      this.setState({
        shippingAddress: e.target.value
      }, () => {
        this.shippingAddress();
      });
    }
    else if(e.target.id == 'cin'){
      let value=e.target.value;
      // console.log(value);
      this.setState({
        cinNumber:value
      });
      // console.log(cinNumber);
    }
    else if(e.target.id == 'terms'){
      let value=e.target.value;
      console.log(value);
      this.setState({
    
        termsName:e.target.value
      });
    } else if(e.target.id == 'logo'){
      this.setState({
        logoName:e.target.value
      });
    }else if(e.target.id == "fileUpload"){
      const formData = new FormData()
      formData.append(
        'file',
        e.target.files[0]
      )
      axios.post(`${CONFIG.BASE_URL}/admin/custom/multiple/file/upload`,
       formData,{headers: {
        'Content-Type': 'multipart/form-data',
        'X-Auth-Token': sessionStorage.getItem('token')
        }}).then((res)=>{
          console.log(res,"res")
        this.setState({
          fileUrl:res.data.data.resource[0].url,
          logoName:res.data.data.resource[0].filePath,
        })
      })
    }
  }
  render() {
    // console.log(cinNumber,termsName,logoName);

    $("body").on("keyup", ".numbersOnly", function () {
      if (this.value != this.value.replace(/[^0-9]/g, '')) {
        this.value = this.value.replace(/[^0-9]/g, '');
      }
    });
    const {
      name,
      nameerr,
      displayname,
      displaynameerr,
      // description,
      // descriptionerr,
      contactPerson,
      contactPersonerr,
      contactNumber,
      contactNumbererr,
      email,
      emailerr,
      status,
      statuserr,
      billtoAddress,
      billtoAddresserr,
      cinNumber,
      logoName,
      termsName

    } = this.state;
    return (
      <div className={this.props.orgModalAnimation ? "modal  display_block" : "display_none"} id="editVendorModal">
        <div className={this.props.orgModalAnimation ? "backdrop modal-backdrop-new" : "display_none"}></div>

        <div className={this.props.orgModalAnimation ? " display_block" : "display_none"}>
          <div className={this.props.orgModalAnimation ? "modal-content vendorEditModalContent modalShow edit-organisation" : "modalHide"}>

            {/* <div className="modal" id="editVendorModal">
            <div className="backdrop"></div>
            <div className="modal-content vendorEditModalContent"> */}
            <form
              name="organizationForm"
              className="organizationForm"
              onSubmit={(e) => this.contactSubmit((e))
              }
            >
              <div className="edm-head">
                <div className="edmh-left">
                  <label className="contribution_mart">Organisation</label>
                </div>
                <div className="edmh-right">
                    {this.state.btnDisable ? <button type="button" className="btnDisabled vendorEditBtn1" >Save</button> : <button  className="vendorEditBtn1" type="submit">Save</button>}
                    <button onClick={(e) => this.props.modalOpen(e)} className="vendorEditBtn2" type="button">Discard</button>
                </div>
              </div>
              
                <div className="col-md-12 col-sm-12 m-top-20 pad-0">
                     <div className="scrollableEditModal1 eom-body">
                        {/* <div className="col-md-12 headModal m-top-20 pad-0">
                            <label>
                                Basic Details
                            </label>
                        </div> */}
                        
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                       
                            <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                                <label className="editModalLabel">
                                     Organization Name
                                </label>
                      <input type="text" name="name"
                        id="name" value={name} onChange={e => this.handleChange(e)} className={nameerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="Name" />
                      {/* {nameerr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                      {nameerr ? (
                        <span className="error">
                          Enter  name
                          </span>
                      ) : null}
                    </div>

                    <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                      <label className="editModalLabel">
                        Display name
                                </label>
                      <input type="text" name="displayname" id="displayname"
                        value={displayname} onChange={e => this.handleChange(e)} className={displaynameerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="display name" />
                      {/* {displaynameerr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                      {displaynameerr ? (
                        <span className="error">
                          Enter display name
                          </span>
                        ) : null}
                            </div>
                            {/* <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                                <label className="editModalLabel">
                                    Description
                                </label>
                                <input onChange={e => this.handleChange(e)} ref="description" name="description" id="description" value={description} className={descriptionerr ? "errorBorder InputBoxEdit" : "InputBoxEdit" } rows="5" placeholder="Description" />
                                
                             
                              {descriptionerr ? (
                                <span className="error">
                                  Enter Description
                                </span>
                              ) : null}
                            </div> */}
                             <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                                <label className="editModalLabel">
                                   Discription
                                </label>
                                <input type="text"  name="discription" id="discription" className={contactPersonerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} />
                            </div>
                            <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                                <label className="editModalLabel">
                                   Contact Person
                                </label>
                                <input type="text" onChange={e => this.handleChange(e)} name="contactPerson" id="contactPerson" value={contactPerson} className={contactPersonerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="Contact Person" />
                                {/* {contactPersonerr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                                {contactPersonerr ? (
                                  <span className="error">
                                    Enter Contact Person
                                    </span>
                                  ) : null}
                            </div>
                      <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-10">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                          <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                            <label className="editModalLabel">
                                Contact Number
                            </label>
                            <input type="text" maxLength="10" onChange={e => this.handleChange(e)}  name="contactNumber" id="contactNumber" value={contactNumber} className={contactNumbererr ? "errorBorder InputBoxEdit numbersOnly" : "InputBoxEdit numbersOnly" } placeholder="contact number"/>

                            {/* {contactNumbererr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                          {contactNumbererr ? (
                            <span className="error">
                              Enter valid contact number
                            </span>
                          ) : null}
                            </div>
                            <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                                <label className="editModalLabel">
                                  Contact Email
                                </label>
                      <input type="text" onChange={e => this.handleChange(e)} name="email" id="email" value={email} className={emailerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="email" />
                      {/* {emailerr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                      {emailerr ? (
                        <span className="error">
                          Enter valid email address
                          </span>
                      ) : null}

                    </div>
                    {/* <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                      <label className="editModalLabel">
                        Status
                                </label>
                      <select name="status"
                        id="status" onChange={e => this.handleChange(e)}
                        value={status} className={statuserr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"} >
                        <option value="">Status</option>
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                      </select>
                      {statuserr ? (
                        <span className="error" >
                          Select Status
                          </span>
                      ) : null}
                    </div> */}

                  </div>

                  {/* <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">

                    <div className="col-md-12 col-sm-2 col-xs-3 pad-lft-0">
                      <label className="editModalLabel">
                        Address
                      </label>
                      <textarea onChange={e => this.handleChange(e)} name="billtoAddress" id="billtoAddress" value={billtoAddress} className={billtoAddresserr ? "errorBorder textAreaEditModal" : "textAreaEditModal"} rows="5" placeholder="Description">
                      </textarea>
                      {billtoAddresserr ? (
                        <span className="error" >
                          Enter bill address
                          </span>
                      ) : null}
                    </div>
                    </div> */}
                        <div className="col-md-12 pad-0 m-top-20">
                          <div className="ao-billing-details">
                            <h4>Billing Details</h4>
                          </div>
                        </div>
                        <div className="col-md-11 pad-0 m-top-10">
                          <div className="col-md-3 pad-lft-0">
                              <label>GSTIN</label>
                              {/* <input type="text" className="orgnisationTextbox" /> */}
                              <input type="text" onChange={e => this.handleChange(e)} name="billingGSTIN" id="billingGSTIN" value={this.state.billingGSTIN} className={this.state.billingGSTINErr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="enter GST no." />
                              {this.state.billingGSTINErr ? ( <span className="error">Enter GST no </span>) : null}
                          </div>
                          <div className="col-md-3 pad-lft-0">
                              <label>Country</label>
                              <select onChange={e => this.handleChange(e)} name="billingCountry" id="billingCountry" value={this.state.billingCountry} className={this.state.billingCountryErr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"}>
                                  <option value="">Select Country</option>
                                  <option value="India">India</option>
                              </select>
                              {this.state.billingCountryErr ? ( <span className="error">Select country </span>) : null}
                          </div>
                          <div className="col-md-3 pad-lft-0">
                              <label>State</label>
                              <select onChange={e => this.handleChange(e)} name="billingState" id="billingState" value={this.state.billingState} className={this.state.billingStateErr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"}>
                                  <option>Select State</option>
                                  {allState.map((_, key) => (
                                    <option value={_} key={key}>{_}</option>
                                  ))}
                              </select>
                              {this.state.billingStateErr ? ( <span className="error">Select state </span>) : null}
                          </div>
                          <div className="col-md-3 pad-lft-0">
                              <label>City</label>
                              <select onChange={e => this.handleChange(e)} name="billingCity" id="billingCity" value={this.state.billingCity} className={this.state.billingCityErr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"}>
                                  <option>Select City</option>
                                  {this.state.billingState !== null && this.state.billingCities.map((_, k) => (
                                      <option value={_} key={k}>{_}</option>
                                  ))}
                              </select>
                              {this.state.billingCityErr ? ( <span className="error">Select city </span>) : null}
                          </div>
                      </div>
                      <div className="col-md-1 pad-lft-0 m-top-10">
                        <label>Pincode</label>
                        <input type="text" name="pincode" id="pincode" className="InputBoxEdit" />
                      </div>

                      <div className="col-md-12 pad-0 m-top-20">
                          <div className="col-md-12 pad-lft-0">
                              <label>Address</label>
                              {/* <textarea type="text" className="orgnisationTextarea"></textarea> */}
                              <textarea onChange={e => this.handleChange(e)} name="billingAddress" id="billingAddress" value={this.state.billingAddress} className={this.state.billingAddressErr ? "errorBorder textAreaEditModal" : "textAreaEditModal"} rows="5" placeholder="Description">
                              </textarea>
                              {this.state.billingAddressErr ? ( <span className="error">Enter address </span>) : null}
                          </div>
                      </div>
                      <div className="col-md-12 pad-0 m-top-30">
                          <div className="ao-billing-details">
                              <h4>Shipping Details</h4>
                          </div>
                      </div>
                      <div className="col-md-11 pad-0 m-top-10">
                          <div className="col-md-3 pad-lft-0">
                              <label>GSTIN</label>
                              <input type="text" onChange={e => this.handleChange(e)} name="shippingGSTIN" id="shippingGSTIN" value={this.state.shippingGSTIN} className={this.state.shippingGSTINErr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="enter GST no." />
                              {this.state.shippingGSTINErr ? ( <span className="error">Enter GST no </span>) : null}
                          </div>
                          <div className="col-md-3 pad-lft-0">
                              <label>Country</label>
                              <select onChange={e => this.handleChange(e)} name="shippingCountry" id="shippingCountry" value={this.state.shippingCountry} className={this.state.shippingCountryErr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"}>
                                  <option value="">Select Country</option>
                                  <option value="India">India</option>
                              </select>
                              {this.state.shippingCountryErr ? ( <span className="error">Select country </span>) : null}
                          </div>
                          <div className="col-md-3 pad-lft-0">
                              <label>State</label>
                              <select onChange={e => this.handleChange(e)} name="shippingState" id="shippingState" value={this.state.shippingState} className={this.state.shippingStateErr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"}>
                                  <option>Select State</option>
                                  {allState.map((_, key) => (
                                    <option value={_} key={key}>{_}</option>
                                  ))}
                              </select>
                              {this.state.shippingStateErr ? ( <span className="error">Select state </span>) : null}
                          </div>
                          <div className="col-md-3 pad-lft-0">
                              <label>City</label>
                              <select onChange={e => this.handleChange(e)} name="shippingCity" id="shippingCity" value={this.state.shippingCity} className={this.state.shippingCityErr ? "errorBorder SelectBoxEdit" : "SelectBoxEdit"}>
                                  <option>Select City</option>
                                  {this.state.shippingState !== null && this.state.shippingCities.map((_, k) => (
                                      <option value={_} key={k}>{_}</option>
                                  ))}
                              </select>
                              {this.state.shippingCityErr ? ( <span className="error">Select city </span>) : null}
                          </div>
                      </div>
                      <div className="col-md-1 pad-lft-0 m-top-10">
                        <label>Pincode</label>
                        <input type="text" name="pincode" id="pincode" className="InputBoxEdit" />
                      </div>
                      <div className="col-md-12 pad-0 m-top-20">
                          <div className="col-md-12 pad-lft-0">
                              <label>Address</label>
                              <textarea onChange={e => this.handleChange(e)} name="shippingAddress" id="shippingAddress" value={this.state.shippingAddress} className={this.state.shippingAddressErr ? "errorBorder textAreaEditModal" : "textAreaEditModal"} rows="5" placeholder="Description">
                              </textarea>
                              {this.state.shippingAddressErr ? ( <span className="error">Enter address </span>) : null}
                          </div>
                      </div>
                      <div className="col-md-12 pad-0 m-top-20">
                        <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                            <label className="editModalLabel">
                                Cin
                            </label>
                          <input type="text" onChange={e => this.handleChange(e)}  id="cin" value={cinNumber} className={contactPersonerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} placeholder="enter cin" />
                          {/* {contactPersonerr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                          {contactPersonerr ? (
                            <span className="error">
                              Enter cin Number
                              </span>
                            ) : null}
                        </div>

                        <div className="col-md-8 col-sm-2 col-xs-3 pad-lft-0 eom-logo-upload">
                            <label className="editModalLabel">
                                Logo
                            </label>
                            <div className="bottomToolTip">
                                <label className="customInput">
                                    <input type="file" id="fileUpload" onChange={e => this.handleChange(e)} multiple="multiple" />
                                    <span >Choose image</span>
                                    <img src={require('./../../../assets/uploadNew.svg')} />
                                </label>
                                {/* <div className="fileName">
                                    <label>Bulk_data_Mar_2020.xls</label><label className="widthAuto"><img src={Close} /></label>
                                </div>
                                <div className="fileName" id="fileName"></div> */}
                            </div>
                            <div className="eomlo-logo">
                              {this.state.fileUrl ?<img src={this.state.fileUrl} /> : null}
                            </div>
                        </div>
                      </div>
                      <div className="col-md-12 pad-0 m-top-20">
                        <div className="col-md-12 col-sm-2 col-xs-3 pad-lft-0">
                          <label className="editModalLabel">
                              Terms
                          </label>
                          <textarea type="text" onChange={e => this.handleChange(e)} id="Terms" value={termsName} className={contactPersonerr ? "errorBorder textAreaEditModal" : "textAreaEditModal"} placeholder="Enter Terms" />
                          {/* {contactPersonerr ? <img src={errorIcon} className="error_icon_purchase"/>:null} */}
                          {contactPersonerr ? (
                            <span className="error">
                              Enter Terms
                              </span>
                            ) : null}
                          </div>
                      </div>
                    </div>
                </div>
              </div>
              </div>
            </form>
          </div>


        </div>
      </div>
    )
  }
}

export default OrganisationModal;