import React from 'react';
import Pagination from '../pagination';
import ViewSystemEmail from './viewSystemEmail';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import Confirm from "../loaders/arsConfirm";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import eyeIcon from "../../assets/eyeIcon.svg";

class SystemMails extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            exportToExcel: false,
            loading: true,
            confirmDelete: false,
            submit: false,
            headerMsg: '',
            paraMsg: '',
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",

            logData: [],
            type: 1,
            prev: '',
            current: 1,
            next: '',
            maxPage: 0,
            totalItems: 0,
            jumpPage: 1,
            toastMsg: "",
            toastLoader: false,
            selectedItems: [],

            filter: false,
            filterBar: false,
            showDownloadDrop: false,
            viewSystemEmail: false,
            viewSystemEmailData: {}
        }
    }

    componentDidMount() {
        this.props.getEmailNotificationLogRequest({
            pageNo: 1,
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "",
            filter: {}
        });
        //this.props.resendEmailNotificationRequest('aryabhatta123456@turningcloud.com');
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.administration.getEmailNotificationLog.isSuccess) {
            if (nextProps.administration.getEmailNotificationLog.data.resource != null && nextProps.administration.getEmailNotificationLog.data.resource.length != 0) {
                return {
                    loading: false,
                    error: false,
                    success: false,
                    logData: nextProps.administration.getEmailNotificationLog.data.resource,
                    prev: nextProps.administration.getEmailNotificationLog.data.prePage,
                    current: nextProps.administration.getEmailNotificationLog.data.currPage,
                    next: nextProps.administration.getEmailNotificationLog.data.currPage + 1,
                    maxPage: nextProps.administration.getEmailNotificationLog.data.maxPage,
                    totalItems: nextProps.administration.getEmailNotificationLog.data.resultedDataCount,
                    jumpPage: nextProps.administration.getEmailNotificationLog.data.currPage,
                    selectedItems: []
                }
            }
        }
        else if (nextProps.administration.getEmailNotificationLog.isError) {
            return {
                loading: false,
                error: true,
                success: false,
                code: nextProps.administration.getEmailNotificationLog.message.status,
                errorCode: nextProps.administration.getEmailNotificationLog.message.error == undefined ? undefined : nextProps.administration.getEmailNotificationLog.message.error.errorCode,
                errorMessage: nextProps.administration.getEmailNotificationLog.message.error == undefined ? undefined : nextProps.administration.getEmailNotificationLog.message.error.errorMessage,
                logData: [],
                prev: '',
                current: '',
                next: '',
                maxPage: '',
                jumpPage: ''
            }
        }

        if (nextProps.administration.resendEmailNotification.isSuccess) {
            return {
                loading: false,
                error: false,
                success: true,
                successMessage: nextProps.administration.resendEmailNotification.data.message
            }
        }
        else if (nextProps.administration.resendEmailNotification.isError) {
            return {
                loading: false,
                error: true,
                success: false,
                code: nextProps.administration.resendEmailNotification.message.status,
                errorCode: nextProps.administration.resendEmailNotification.message.error == undefined ? undefined : nextProps.administration.resendEmailNotification.message.error.errorCode,
                errorMessage: nextProps.administration.resendEmailNotification.message.error == undefined ? undefined : nextProps.administration.resendEmailNotification.message.error.errorMessage
            }
        }

        if (nextProps.administration.getEmailBody.isSuccess) {
            return {
                loading: false,
                error: false,
                success: false,
                viewSystemEmailData: nextProps.administration.getEmailBody.data.resource,
                viewSystemEmail: true
            }
        }
        else if (nextProps.administration.getEmailBody.isError) {
            return {
                loading: false,
                error: true,
                success: false,
                code: nextProps.administration.getEmailBody.message.status,
                errorCode: nextProps.administration.getEmailBody.message.error == undefined ? undefined : nextProps.administration.getEmailBody.message.error.errorCode,
                errorMessage: nextProps.administration.getEmailBody.message.error == undefined ? undefined : nextProps.administration.getEmailBody.message.error.errorMessage
            }
        }

        if (nextProps.administration.getEmailNotificationLog.isLoading || nextProps.administration.resendEmailNotification.isLoading || nextProps.administration.getEmailBody.isLoading) {
            return {
                loading: true,
                error: false,
                success: false
            }
        }

        return {}
    }

    componentDidUpdate() {
        if (this.props.administration.getEmailNotificationLog.isSuccess || this.props.administration.getEmailNotificationLog.isError)
            this.props.getEmailNotificationLogClear();
        if (this.props.administration.resendEmailNotification.isSuccess || this.props.administration.resendEmailNotification.isError)
            this.props.resendEmailNotificationClear();
        if (this.props.administration.getEmailBody.isSuccess || this.props.administration.getEmailBody.isError)
            this.props.getEmailBodyClear();
    }

    showDownloadDrop(e) {
        e.preventDefault();
        this.setState({
            showDownloadDrop: !this.state.showDownloadDrop
        }, () => document.addEventListener('click', this.closeDownloadDrop));
    }
    closeDownloadDrop = () => {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        });
    }
    openSystemEmail(e, data) {
        e.preventDefault();
        this.setState({
            viewSystemEmailData: data,
            viewSystemEmail: !this.state.viewSystemEmail
        });
    }
    CloseSystemEmail = () => {
        this.setState({
            viewSystemEmailData: {},
            viewSystemEmail: false
        });
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.administration.getEmailNotificationLog.data.prePage,
                    current: this.props.administration.getEmailNotificationLog.data.currPage,
                    next: this.props.administration.getEmailNotificationLog.data.currPage + 1,
                    maxPage: this.props.administration.getEmailNotificationLog.data.maxPage
                })
                if (this.props.administration.getEmailNotificationLog.data.prePage != 0) {
                    this.props.getEmailNotificationLogRequest({
                        pageNo: this.props.administration.getEmailNotificationLog.data.prePage,
                        type: this.state.type,
                        search: "",
                        sortedBy: "",
                        sortedIn: "",
                        filter: {}
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.administration.getEmailNotificationLog.data.prePage,
                    current: this.props.administration.getEmailNotificationLog.data.currPage,
                    next: this.props.administration.getEmailNotificationLog.data.currPage + 1,
                    maxPage: this.props.administration.getEmailNotificationLog.data.maxPage
                })
            if (this.props.administration.getEmailNotificationLog.data.currPage != this.props.administration.getEmailNotificationLog.data.maxPage) {
                this.props.getEmailNotificationLogRequest({
                    pageNo: this.props.administration.getEmailNotificationLog.data.currPage + 1,
                    type: this.state.type,
                    search: "",
                    sortedBy: "",
                    sortedIn: "",
                    filter: {}
                });
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.administration.getEmailNotificationLog.data.prePage,
                    current: this.props.administration.getEmailNotificationLog.data.currPage,
                    next: this.props.administration.getEmailNotificationLog.data.currPage + 1,
                    maxPage: this.props.administration.getEmailNotificationLog.data.maxPage
                })
                if (this.props.administration.getEmailNotificationLog.data.currPage <= this.props.administration.getEmailNotificationLog.data.maxPage) {
                    this.props.getEmailNotificationLogRequest({
                        pageNo: 1,
                        type: this.state.type,
                        search: "",
                        sortedBy: "",
                        sortedIn: "",
                        filter: {}
                    });
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.administration.getEmailNotificationLog.data.prePage,
                    current: this.props.administration.getEmailNotificationLog.data.currPage,
                    next: this.props.administration.getEmailNotificationLog.data.currPage + 1,
                    maxPage: this.props.administration.getEmailNotificationLog.data.maxPage
                })
                if (this.props.administration.getEmailNotificationLog.data.currPage <= this.props.administration.getEmailNotificationLog.data.maxPage) {
                    this.props.getEmailNotificationLogRequest({
                        pageNo: this.props.administration.getEmailNotificationLog.data.maxPage,
                        type: this.state.type,
                        search: "",
                        sortedBy: "",
                        sortedIn: "",
                        filter: {}
                    });
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    this.props.getEmailNotificationLogRequest({
                        pageNo: _.target.value,
                        type: this.state.type,
                        search: "",
                        sortedBy: "",
                        sortedIn: "",
                        filter: {}
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false
        }, () => {

        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 45) {
                return false;
            }
            return true;
        }
    }

    render () {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                <form>
                                    <input type="search"  placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                    <span className="closeSearch" ><img src={require('../../assets/clearSearch.svg')}/></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Documents</span>
                                    </button>
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table">
                        <div className="manage-table">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={require('../../assets/reload.svg')} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        <th><label>Email Subject</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>To</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Cc</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Sent Time</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Status</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                    </tr>
                                </thead>
                                <tbody>{
                                    this.state.logData.length == 0 ? 
                                    <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                    this.state.logData.map((item) => (
                                        <tr key={item.id}>
                                            <td className="fix-action-btn width80">
                                                <ul className="table-item-list">
                                                    <li className="til-inner">
                                                        {/* <label className="checkBoxLabel0">
                                                            <input type="checkBox" />
                                                            <span className="checkmark1"></span>
                                                        </label> */}
                                                        <img src={eyeIcon} className="eye" onClick={() => this.props.getEmailBodyRequest(item.id)} />
                                                    </li>
                                                    <li className="til-inner til-resend-mail" onClick={() => this.props.resendEmailNotificationRequest(item.id)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18.691" height="19.177" viewBox="0 0 18.691 19.177">
                                                            <g id="exchange" transform="translate(-.324)">
                                                                <path fill="#000" id="Path_1533" d="M17.683 232.494a2 2 0 0 1-2 2H2.606l2.637-2.637-.944-.944-3.776 3.78a.668.668 0 0 0 0 .944l3.777 3.776.944-.944-2.637-2.637H15.68a3.342 3.342 0 0 0 3.338-3.338v-2.67h-1.335zm0 0" class="cls-1" transform="translate(-.003 -220.236)"/>
                                                                <path fill="#000" id="Path_1534" d="M1.659 6.918a2 2 0 0 1 2-2h13.077L14.1 7.553l.944.944L18.82 4.72a.668.668 0 0 0 0-.944L15.044 0 14.1.944l2.637 2.637H3.662A3.342 3.342 0 0 0 .324 6.918v2.67h1.335zm0 0" class="cls-1"/>
                                                            </g>
                                                        </svg>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td><label className="bold">{item.subject}</label></td>
                                            <td><label>{item.to}</label>
                                            {this.small && <div className="table-tooltip"><label>{item.to}</label></div>}
                                            </td>
                                            <td><label>{item.cc}</label></td>
                                            <td><label>{item.createdOn}</label></td>
                                            <td><label className={item.status == "SUCCEEDED" ? "vgt-status" : "vgt-status vgt-status-fail"}>{item.status}</label></td>
                                        </tr>
                                    ))
                                }
                                    {/* <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path fill="#eeeeee" d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path fill="#eeeeee" d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status">Succeed</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path fill="#eeeeee" d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path fill="#eeeeee" d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status">Succeed</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path fill="#eeeeee" d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path fill="#eeeeee" d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status">Succeed</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path fill="#eeeeee" d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path fill="#eeeeee" d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status">Succeed</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path fill="#eeeeee" d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path fill="#eeeeee" d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status">Succeed</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path fill="#eeeeee" d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path fill="#eeeeee" d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status">Succeed</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19.121" viewBox="0 0 24 19.121">
                                                        <g transform="translate(-183 -458.219)">
                                                            <path d="M19.609 61H1.89A1.893 1.893 0 0 0 0 62.889v12.6a1.893 1.893 0 0 0 1.89 1.889h17.719a1.893 1.893 0 0 0 1.891-1.892v-12.6A1.893 1.893 0 0 0 19.609 61zm-.261 1.26l-8.559 8.559-8.632-8.559zM1.26 75.225V63.144l6.067 6.014zm.891.891l6.07-6.07 2.127 2.109a.63.63 0 0 0 .889 0l2.074-2.074 6.037 6.037zm18.088-.891L14.2 69.188l6.037-6.037z" transform="translate(183 397.219)"/>
                                                            <circle cx="5.5" cy="5.5" r="5.5" fill="#fff" transform="translate(196 466)"/>
                                                            <g>
                                                                <g>
                                                                    <path d="M54.666 6.019a4.294 4.294 0 0 1 4.212-4.789V.076a.083.083 0 0 1 .133-.06l2.375 1.745a.073.073 0 0 1 0 .117l-2.373 1.746a.082.082 0 0 1-.133-.06V2.412a3.109 3.109 0 0 0-2.812 1.905 3.071 3.071 0 0 0 .432 3.131.593.593 0 1 1-.932.733 4.235 4.235 0 0 1-.902-2.162zM61.523 3.8a3.071 3.071 0 0 1 .437 3.131 3.105 3.105 0 0 1-2.812 1.905v-1.15a.083.083 0 0 0-.133-.06L56.64 9.372a.073.073 0 0 0 0 .117l2.373 1.745a.082.082 0 0 0 .133-.06V10.02a4.294 4.294 0 0 0 4.212-4.789 4.213 4.213 0 0 0-.9-2.162.592.592 0 1 0-.93.733z" transform="translate(196.886 466.09) translate(-54.637)"/>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label onClick={(e) => this.openSystemEmail(e)} className="bold">Vendor Activation Email</label></td>
                                        <td><label>Akash@example.com, rohit@example.com</label></td>
                                        <td><label>Ankit1991@example.com, suresh@example.com</label></td>
                                        <td><label>20 Nov 2020</label></td>
                                        <td><label className="vgt-status vgt-status-fail">Failed</label></td>
                                    </tr> */}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-12 pad-0" >
                        <div className="new-gen-pagination">
                            <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination {...this.state} {...this.props} page={this.page}
                                        prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.confirmDelete ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeConfirmDelete} action={this.props.deleteSeasonPlanningRequest} payload={this.state.selectedItems} afterConfirm={this.afterConfirmDelete} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.viewSystemEmail && <ViewSystemEmail data={this.state.viewSystemEmailData} CloseSystemEmail={this.CloseSystemEmail} />}
            </div>
        )
    }
}
export default SystemMails;