import React from 'react';
import RightSideBar from "../../rightSideBar";
import Footer from '../../footer'
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";

class AddSiteMapping extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            errorMessage: "",
            successMessage: "",
            dateChange: false,
            from: "",
            to: "",
            fromId: "",
            toId: "",
            fromDate: "",
            toDate: "",
            transportationMode: "",
            transportationLeadTime: "",
            active: "Active",
            fromerr: false,
            toerr: false,
            fromDateerr: false,
            errorCode: "",
            toDateerr: false,
            transportationModeerr: false,
            transportationLeadTimeerr: false,
            activeerr: false,
            loader: false,
            success: false,
            alert: false,
            rightbar: false,
            openRightBar: false,
            sites: [],
            code: ""
        }
    }
    componentWillMount() {
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }

        let crud = JSON.parse(sessionStorage.getItem('Site Mapping'))
        let Create = crud.crud.charAt(0);
        if (Create <= 0) {
            this.props.history.push('/administration/siteMapping');
        }
        this.props.fromToRequest();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.fromTo.isSuccess) {
            this.setState({
                loader: false,
                sites: nextProps.administration.fromTo.data.resource
            })
        } else if (nextProps.administration.fromTo.isLoading) {
            this.setState({
                loader: true
            })
        } else if (nextProps.administration.fromTo.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.administration.fromTo.message.status,
                errorCode: nextProps.administration.fromTo.message.error == undefined ? undefined : nextProps.administration.fromTo.message.error.errorCode,
                errorMessage: nextProps.administration.fromTo.message.error == undefined ? undefined : nextProps.administration.fromTo.message.error.errorMessage,
            })
        }
        if (nextProps.administration.addSiteMapping.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.addSiteMapping.data.message,
                loader: false,
                success: true
            })
            this.props.addSiteMappingRequest();
            this.onClear();
        } else if (nextProps.administration.addSiteMapping.isLoading) {
            this.setState({
                loader: true,
            })
        } else if (nextProps.administration.addSiteMapping.isError) {
            this.setState({
                loader: false,
                code: nextProps.administration.addSiteMapping.message.status,
                errorCode: nextProps.administration.addSiteMapping.message.error == undefined ? undefined : nextProps.administration.addSiteMapping.message.error.errorCode,
                errorMessage: nextProps.administration.addSiteMapping.message.error == undefined ? undefined : nextProps.administration.addSiteMapping.message.error.errorMessage,
            })
            this.props.addSiteMappingRequest();
        }
    }

    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    from() {
        if (
            this.state.from == "" || this.state.from.trim() == "") {
            this.setState({
                fromerr: true
            });
        } else {
            this.setState({
                fromerr: false
            });
        }
    }
    to() {
        if (
            this.state.to == "" || this.state.to.trim() == "") {
            this.setState({
                toerr: true
            });
        } else {
            this.setState({
                toerr: false
            });
        }
    }
    // toDate() {
    //     if (
    //         this.state.toDate == "" || this.state.toDate.trim() == "") {
    //         this.setState({
    //             toDateerr: true
    //         });
    //     } else {
    //         this.setState({
    //             toDateerr: false
    //         });
    //     }
    // }
    fromDate() {
        if (
            this.state.fromDate == "" || this.state.fromDate.trim() == "") {
            this.setState({
                fromDateerr: true
            });
        } else {
            this.setState({
                fromDateerr: false
            });
        }
    }
    transportationLeadTime() {
        if (
            this.state.transportationLeadTime == "" || this.state.transportationLeadTime.trim() == "") {
            this.setState({
                transportationLeadTimeerr: true
            });
        } else {
            this.setState({
                transportationLeadTimeerr: false
            });
        }
    }
    transportationMode() {
        if (
            this.state.transportationMode == "" || this.state.transportationMode.trim() == "") {
            this.setState({
                transportationModeerr: true
            });
        } else {
            this.setState({
                transportationModeerr: false
            });
        }
    }
    active() {
        if (
            this.state.active == "" || this.state.active.trim() == "") {
            this.setState({
                activeerr: true
            });
        } else {
            this.setState({
                activeerr: false
            });
        }
    }

    handleChange(e) {
        e.preventDefault();
        if (e.target.id == "from") {
            for (let i = 0; i < this.state.sites.length; i++) {
                if (this.state.sites[i].id == e.target.value) {
                    this.setState({
                        fromId: this.state.sites[i].name
                    })
                }
            }
            this.setState(
                {
                    from: e.target.value
                },
                () => {
                    this.from();
                }
            );
        } else if (e.target.id == "to") {
            for (let i = 0; i < this.state.sites.length; i++) {
                if (this.state.sites[i].id == e.target.value) {
                    this.setState({
                        toId: this.state.sites[i].name
                    })
                }
            }
            this.setState(
                {
                    to: e.target.value
                },
                () => {
                    this.to();
                }
            );
        } else if (e.target.id == "fromDate") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    fromDate: e.target.value
                },
                () => {
                    this.fromDate();
                }
            );
        } else if (e.target.id == "toDate") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    toDate: e.target.value,



                },
                // () => {
                //     this.toDate();

                // }
            );
        } else if (e.target.id == "transportationMode") {
            this.setState(
                {
                    transportationMode: e.target.value
                },
                () => {
                    this.transportationMode();
                }
            );
        } else if (e.target.id == "transportationLeadTime") {
            this.setState(
                {
                    transportationLeadTime: e.target.value
                },
                () => {
                    this.transportationLeadTime();
                }
            );
        } else if (e.target.id == "active") {
            this.setState(
                {
                    active: e.target.value
                },
                () => {
                    this.active();
                }
            );
        }
    }
    onDateChange(e) {
        e.target.placeholder = e.target.value
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.from();
        this.to();
        this.fromDate();
        // this.toDate();
        this.transportationLeadTime();
        // this.transportationMode();
        this.active();
        const t = this;
        setTimeout(function () {
            const { from, to, fromId, toId, fromDate, toDate, transportationLeadTime, transportationMode, active, fromerr, toerr, fromDateerr, toDateerr, transportationLeadTimeerr, transportationModeerr, activeerr } = t.state;
            if (!fromerr && !toerr && !fromDateerr
                && !transportationLeadTimeerr
                //  && !transportationModeerr 
                && !activeerr) {
                let data = {
                    from: from,
                    to: to,
                    fromId: fromId,
                    toId: toId,
                    fromDate: fromDate,
                    toDate: toDate == "" ? "NA" : toDate,
                    transportationLeadTime: transportationLeadTime,
                    transportationMode: transportationMode,
                    active: active
                }
                t.props.addSiteMappingRequest(data);

            }
        }, 100)
    }
    onClear() {
        // e.preventDefault();
        document.getElementById('fromDate').placeholder = "Start Date";
        document.getElementById('toDate').placeholder = "End Date";
        document.getElementById('transportationLeadTime').placeholder = "Transportation Lead Time";
        this.setState({
            from: "",
            to: "",
            fromDate: "",
            toDate: "",
            transportationMode: "",
            transportationLeadTime: "",
            active: ""
        })
    }

    render() {

        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        const { from, to, fromDate, toDate, transportationLeadTime, transportationMode, active, fromerr, toerr, fromDateerr, toDateerr, transportationLeadTimeerr, transportationModeerr, activeerr } = this.state;
        return (
            <div className="container-fluid">
                <div className="container_div" id="home">
                    <SideBar {...this.props} />
                    <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                    {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}
                    <div className="container_div m-top-75 " id="">
                        <div className="col-md-12 col-sm-12">
                            <div className="menu_path">
                                <ul className="list-inline width_100 pad20">
                                    <BraedCrumps {...this.props} />
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid">
                        <div className="container_div" id="">

                            <div className="container-fluid">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="container_admin macHeight inputBoxesGeneric">
                                        <div className="container_card">
                                            <ul className="list_style">
                                                <li>
                                                    <label className="site-content">SITE MAPPING</label>
                                                </li>
                                                <li>
                                                    <label className="para-content">Add Stock Movement Mapping</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="Slider_Div">
                                            <ul className="list_style">
                                                <li>
                                                    <label className="details-content">BASIC DETAILS</label>
                                                </li>
                                                <li>
                                                    <label className="para-content">Add your Basic details here</label>
                                                </li>
                                            </ul>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20" onSubmit={(e) => this.onFormSubmit(e)}>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="from"></label>
                                                    <select value={from} className={fromerr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} id="from">
                                                        <option value="" >From Site</option>
                                                        {this.state.sites == null ? null : this.state.sites.map((data, key) => (<option key={key} value={data.id} data="abc" >{data.name}</option>))}
                                                    </select>
                                                    {/* {fromerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {fromerr ? (
                                                        <span className="error">
                                                            Enter valid from site
                          </span>
                                                    ) : null} </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="to"></label>
                                                    <select value={to} className={toerr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} id="to">
                                                        <option value="" >To Site</option>
                                                        {this.state.sites == null ? null : this.state.sites.map((data, key) => (<option key={key} value={data.id} >{data.name}</option>))}
                                                    </select>
                                                    {/* {toerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {toerr ? (
                                                        <span className="error">
                                                            Enter valid to site
                          </span>
                                                    ) : null}</div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="start"></label>
                                                    <input type="date" value={fromDate} onChange={(e) => this.handleChange(e)} className={fromDateerr ? "errorBorder form-box" : "form-box"} id="fromDate" placeholder="Start Date" />

                                                    {/* {fromDateerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {fromDateerr ? (
                                                        <span className="error">
                                                            Enter valid start date
                          </span>
                                                    ) : null}</div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="end"></label>
                                                    <input type="date" value={toDate} onChange={(e) => this.handleChange(e)} className={toDateerr ? "errorBorder form-box" : "form-box"} id="toDate" placeholder="End Date" />

                                                    {/* {toDateerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {toDateerr ? (
                                                        <span className="error">
                                                            Enter valid end date
                          </span>
                                                    ) : null}
                                                </div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="mode"></label>
                                                    <input type="text" value={transportationMode} onChange={(e) => this.handleChange(e)} className={transportationModeerr ? "errorBorder form-box" : "form-box"} id="transportationMode" placeholder="Transportation Mode" />
                                                    {/* {transportationModeerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {transportationModeerr ? (
                                                        <span className="error">
                                                            Enter valid transportation mode
                          </span>
                                                    ) : null}</div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="time"></label>
                                                    <input maxLength={4} type="text" value={transportationLeadTime} onChange={(e) => this.handleChange(e)} className={transportationLeadTimeerr ? "errorBorder form-box numbersOnly" : "form-box numbers numbersOnly"} id="transportationLeadTime" placeholder="Transportation Lead Time(in days)" />
                                                    {/* {transportationLeadTimeerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {transportationLeadTimeerr ? (
                                                        <span className="error">
                                                            Enter valid transportation lead time
                          </span>
                                                    ) : null}</div>
                                                <div className="col-md-2 col-sm-2 pad-lft-0">
                                                    <label htmlFor="active"></label>
                                                    <select className={activeerr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} value={active} id="active">
                                                        <option value="" >Status</option>
                                                        <option value="Active" >Active</option>
                                                        <option value="Inactive" >Inactive</option>
                                                    </select>
                                                    {/* {activeerr ? <img src={errorIcon} className="error_icon right50" /> : null} */}
                                                    {activeerr ? (
                                                        <span className="error">
                                                            Enter valid active
                          </span>
                                                    ) : null} </div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 ">
                                            <div className="footerbutton">
                                                <ul className="list-inline m-lft-0 m-top-15">
                                                    <li>
                                                        <button type="button" className="clearbtnOrganisation" onClick={() => this.onClear()} type="reset">Clear</button>
                                                    </li>
                                                    <li>
                                                        <button className="savebtnOrganisation" onClick={(e) => this.onFormSubmit(e)} type="submit" >Save</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>

        )
    }
}

export function mapStateToProps(state) {
    return {
        administration: state.administration
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddSiteMapping);