import React from 'react';

class SiteMapFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fromSiteName: "",
            toSiteName: "",
            startDate: "",
            tptLeadTime: "",
            status: ""
        }
    }

    componentWillMount(){
        this.setState({
            fromSiteName: this.props.fromSiteName,
            toSiteName: this.props.toSiteName,
            startDate: this.props.startDate,
            tptLeadTime: this.props.tptLeadTime,
            status: this.props.status
        })
    }

    componentDidMount(){
        document.getElementById('startDate').placeholder = this.props.startDate == "" ? "Start Date" : this.props.startDate;
    }

    componentWillReceiveProps(nextProps){
        document.getElementById('startDate').placeholder = nextProps.startDate == "" ? "Start Date" : nextProps.startDate;
        this.setState({
            fromSiteName: nextProps.fromSiteName,
            toSiteName: nextProps.toSiteName,
            startDate: nextProps.startDate,
            tptLeadTime: nextProps.tptLeadTime,
            status: nextProps.status
        })
    }

    clearFilter(e) {
        this.setState({
            fromSiteName: "",
            toSiteName: "",
            startDate: "",
            tptLeadTime: "",
            status: ""
        })
        document.getElementById('startDate').placeholder = "Start Date"
    }

    handleChange(e) {
        if (e.target.id == "fromSiteName") {
            this.setState({
                fromSiteName: e.target.value
            });
        } else if (e.target.id == "toSiteName") {
            this.setState({
                toSiteName: e.target.value
            });
        } else if (e.target.id == "startDate") {
            e.target.placeholder = e.target.value
            this.setState({
                startDate: e.target.value
            });
        } else if (e.target.id == "tptLeadTime") {
            this.setState({
                tptLeadTime: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        }
    }

    onSubmit(e) {
        e.preventDefault();
        let data = {
            no: 1,
            type: 2,
            search: this.state.search,
            tptLeadTime: this.state.tptLeadTime,
            startDate: this.state.startDate,
            toSiteName: this.state.toSiteName,
            fromSiteName: this.state.fromSiteName,
            status: this.state.status
        }
        this.props.siteMappingRequest(data);
        this.props.closeFilter(e)
        this.props.updateFilter(data)
    }

    render() {
        let count = 0;
        if(this.state.tptLeadTime != ""){
            count ++;
        }
        if(this.state.startDate != ""){
            count ++;
        }
        if(this.state.toSiteName != ""){
            count ++;
        }
        if(this.state.fromSiteName != ""){
            count ++;
        }
        if(this.state.status != ""){
            count ++;
        }
        return (
            <div>
                <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myModal">
                    <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                            <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <div className="col-md-12 col-sm-12 pad-0">
                                <ul className="list-inline">
                                    <li>
                                        <label className="filter_modal">
                                            FILTERS
        
                     </label>
                                    </li>
                                    <li>
                                        <label className="filter_text">
                                            {count} Filters applied
                     </label>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="container_modal">

                                    <ul className="list-inline m-top-20">

                                        <li>
                                            <input type="text"  onChange={(e) => this.handleChange(e)}   id="fromSiteName" value={this.state.fromSiteName}  placeholder="From Site" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text"  onChange={(e) => this.handleChange(e)}   id="toSiteName" value={this.state.toSiteName}  placeholder="To Site" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="date"  onChange={(e) => this.handleChange(e)}   id="startDate" value={this.state.startDate}  placeholder="Start Date" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text"  onChange={(e) => this.handleChange(e)}  id="tptLeadTime" value={this.state.tptLeadTime} placeholder="Lead Time(In Days)" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <select onChange={(e) => this.handleChange(e)} id="status" value={this.state.status} className="RoleFilterSelect filterBtnWidth">
                                                <option value="">
                                                    Status
                                                </option>
                                                <option value="Active">
                                                Active
                                                </option>
                                                <option value="Inactive">
                                                Inactive
                                                </option>
                                            </select>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="col-md-6 float_right pad-0 m-top-20">
                                    <ul className="list-inline text_align_right">
                                        <li>
                                            {this.state.tptLeadTime == "" && this.state.startDate == "" && this.state.toSiteName == "" && this.state.fromSiteName == "" && this.state.status == "" ? <button type="button" className="modal_clear_btn pointerNone textDisable">CLEAR FILTER</button>
                                            :<button onClick={(e) => this.clearFilter(e)} type="button" className="modal_clear_btn">
                                                CLEAR FILTER
                                            </button>}
                                        </li>
                                        <li>
                                        {this.state.tptLeadTime != "" || this.state.startDate != "" || this.state.toSiteName != "" || this.state.fromSiteName != "" || this.state.status != "" ?  <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button>: <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                            APPLY
                                        </button>}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default SiteMapFilter;