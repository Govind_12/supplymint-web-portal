import React from 'react';
import openRack from "../../../assets/open-rack.svg";
import searchIcon from "../../../assets/search.svg";
import SiteMapFilter from "./siteMapFilter";
import BraedCrumps from '../../breadCrumps';
import editIcon from '../../../assets/edit.svg'
import deleteIcon from '../../../assets/delete.svg'
import {CONFIG} from "../../../config/index";
import axios from 'axios';
import refreshIcon from '../../../assets/refresh.svg';
import ToastLoader from '../../loaders/toastLoader';

class ViewSiteMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: false,
            filterBar: true,
            siteMapState: this.props.siteMapState,
            siteMapModal:false,
            id:"",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            no: 1,
            type: 1,
            search: "",
            tptLeadTime: "",
            startDate: "",
            toSiteName: "",
            fromSiteName: "",
            status: "",
            sitemappingModalAnimation: false,
            toastLoader:false,
            toastMsg:""
        };
    }
openSiteMapModal(e){
  
    this.setState({
        siteMapModal: !this.state.siteMapModal,
        id:e,
        sitemappingModalAnimation:!this.state.sitemappingModalAnimation
    })
 
}

handleSearch(e){
    this.setState({
        search: e.target.value
    })
}
onSearch(e){
    e.preventDefault();
        if(this.state.search == ""){
            this.setState({
                toastMsg:"Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
               this.setState({
                toastLoader:false
               })
            }, 1500);
        }else{
    this.setState({
        type: 3,
        tptLeadTime: "",
        startDate: "",
        toSiteName: "",
        fromSiteName: "",
        status: ""
    })
    let data = {
        type: 3,
        no: 1,
        tptLeadTime: "",
        startDate: "",
        toSiteName: "",
        fromSiteName: "",
        status: "",
        search: this.state.search,
    }
    this.props.siteMappingRequest(data)
}
}

updateFilter(data){
    this.setState({
        no: data.no,
        type: data.type,
        search: data.search,
        tptLeadTime: data.tptLeadTime,
        startDate: data.startDate,
        toSiteName: data.toSiteName,
        fromSiteName: data.fromSiteName,
        status: data.status
    })
}


deleteSiteMapping(id){
    let data = {
        no: this.props.administration.siteMapping.data.currPage,
        idd: id,
        type: this.state.type,
        search: this.state.search,
        tptLeadTime: this.state.tptLeadTime,
        startDate: this.state.startDate,
        toSiteName: this.state.toSiteName,
        fromSiteName: this.state.fromSiteName,
        status: this.state.status
    }
    this.props.deleteSiteMappingRequest(data);

}
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: true,
            filterBar: !this.state.filterBar
        });
    }

    componentWillMount(){
        if(this.props.administration.siteMapping.isSuccess){
             this.setState({
                 prev: this.props.administration.siteMapping.data.prePage,
                 current: this.props.administration.siteMapping.data.currPage,
                 next: this.props.administration.siteMapping.data.currPage + 1,
                 maxPage: this.props.administration.siteMapping.data.maxPage,
             })}
     }

    componentWillReceiveProps(nextProps){
        if(nextProps.administration.siteMapping.isSuccess){
            this.setState({
                siteMapState: nextProps.administration.siteMapping.data.resource,
                prev: nextProps.administration.siteMapping.data.prePage,
                current: nextProps.administration.siteMapping.data.currPage,
                next: nextProps.administration.siteMapping.data.currPage + 1,
                maxPage: nextProps.administration.siteMapping.data.maxPage,
            })
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            if(this.state.current == "" || this.state.current == undefined ||  this.state.current == 1){
                
            }else{
                this.setState({
                    prev: this.props.administration.siteMapping.data.prePage,
                    current: this.props.administration.siteMapping.data.currPage,
                    next:this.props.administration.siteMapping.data.currPage + 1,
                    maxPage:this.props.administration.siteMapping.data.maxPage,
                })
                if(this.props.administration.siteMapping.data.currPage != 0){
                    let data = {
                        no: this.props.administration.siteMapping.data.currPage -1 ,
                        type: this.state.type,
                        search: this.state.search,
                        tptLeadTime: this.state.tptLeadTime,
                        startDate: this.state.startDate,
                        toSiteName: this.state.toSiteName,
                        fromSiteName: this.state.fromSiteName,
                        status: this.state.status
                    }
                    this.props.siteMappingRequest(data);
                }

            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.administration.siteMapping.data.prePage,
                current: this.props.administration.siteMapping.data.currPage,
                next:this.props.administration.siteMapping.data.currPage + 1,
                maxPage:this.props.administration.siteMapping.data.maxPage,
            })
            if(this.props.administration.siteMapping.data.currPage !=this.props.administration.siteMapping.data.maxPage){
                let data = {
                    no: this.props.administration.siteMapping.data.currPage + 1 ,
                    type: this.state.type,
                    search: this.state.search,
                    tptLeadTime: this.state.tptLeadTime,
                    startDate: this.state.startDate,
                    toSiteName: this.state.toSiteName,
                    fromSiteName: this.state.fromSiteName,
                    status: this.state.status
                }
                this.props.siteMappingRequest(data);
            }
        }
        else if (e.target.id == "first") {
            if(this.state.current == 1 || this.state.current == "" || this.state.current == undefined){

            }else{
                this.setState({
                prev:this.props.administration.siteMapping.data.prePage,
                current: this.props.administration.siteMapping.data.currPage,
                next: this.props.administration.siteMapping.data.currPage + 1,
                maxPage: this.props.administration.siteMapping.data.maxPage,
                })
                if (this.props.administration.siteMapping.data.currPage <= this.props.administration.siteMapping.data.maxPage) {
                    let data = {
                        no: 1 ,
                        type: this.state.type,
                        search: this.state.search,
                        tptLeadTime: this.state.tptLeadTime,
                        startDate: this.state.startDate,
                        toSiteName: this.state.toSiteName,
                        fromSiteName: this.state.fromSiteName,
                        status: this.state.status
                    }
                this.props.siteMappingRequest(data);
                }
            }
        }
    }

    onClearSearch(e){
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.siteMappingRequest(data);
    }

    onClearFilter(e){
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: "",
            tptLeadTime: "",
            startDate: "",
            toSiteName: "",
            fromSiteName: "",
            status: "",
        })
        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.siteMappingRequest(data);
    }

    xlscsv(type) {
        let headers = {
          'X-Auth-Token': sessionStorage.getItem('token'),
          'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/download/module/${type}/SiteMap`, { headers: headers })
          .then(res => {
            window.open(`${res.data.data.resource}`)
          }).catch((error) => {
            console.log(error);
          });
    
      }

    onRefresh(){
        let data = {
            no: this.state.current,
            type: this.state.type,
            search: this.state.search,
            tptLeadTime: this.state.tptLeadTime,
            startDate: this.state.startDate,
            toSiteName: this.state.toSiteName,
            fromSiteName: this.state.fromSiteName,
            status: this.state.status
        }
    this.props.siteMappingRequest(data);
    }

    render() {
        let crud = JSON.parse(sessionStorage.getItem('Site Mapping'))
        let Create = crud.crud.charAt(0);
        let Read = crud.crud.charAt(1);
        let Update = crud.crud.charAt(2);
        let Delete = crud.crud.charAt(3);
        return (
<div className="container-fluid">
            <div className="container_div">

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                    <div className="container_content heightAuto">
              <div className="col-md-6 col-sm-12 pad-0">
                <ul className="list_style">
                  <li>
                    <label className="contribution_mart">
                      MANAGE SITE MAPPING
                            </label>
                  </li>
                  <li>
                    {/* <p className="master_para">lorem ipsum doler immet</p> */}
                  </li>

                  <li className="m-top-30">
                    <label className="export_data_div">
                      Export Data
                            </label>
                    <ul className="list-inline m-top-10">
                      <li>
                        <button  type="button" onClick={(e) => this.xlscsv("CSV")} className="button_home">
                          CSV
                                    </button>
                      </li>
                      <li>
                        <button  type="button" onClick={(e) => this.xlscsv("XLS")} className="button_home">
                          XLS
                                    </button>
                      </li>
                      <li>
                        <button className="filter_button" onClick={(e) => this.openFilter(e)} data-toggle="modal" data-target="#myRoleModal">
                          FILTER
                                    
                        <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                          <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                        </svg>
                        </button>

                      </li>
                      <li  className="refresh-table">
                        <img src={refreshIcon} onClick={() => this.onRefresh()}/> 
                        <p className="tooltiptext topToolTipGeneric">
                                Refresh                                                  
                            </p>
                      </li>
                    </ul>
                  </li>
                </ul>
                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
              </div>

              <div className="col-md-6 col-sm-12 pad-0">
               {Create > 0 ?  <ul className="list-inline circle_list">
                        <li>
                             <div className="tooltip" onClick={() => this.props.history.push('/administration/siteMapping/addSiteMapping')}> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                                    <g fill="none" fillRule="evenodd">
                                        <circle cx="17.5" cy="17.5" r="17.5" fill="#6D6DC9"/>
                                        <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7"/>
                                    </g>
                                </svg>

                                <p className="tooltiptext tooltip-left">
                                     Add Site <span>Please click on add icons to Add New Site
                                        </span>
                                </p>
                             </div> 
                    
                        </li>
                </ul> : null}
                <ul className="list-inline search_list manageSearch">
                  <li>
                      <form onSubmit={(e) => this.onSearch(e)}>
                    <input type="search" onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                    {/* <img src="../imgs/search.svg" className="search_img" /> */}
                    <button type="submit" className="searchWithBar">Search
                    <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                        <path fill="#ffffff" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z"/>
                    </svg>
                    </button>
                    </form>
                  </li>
       
                </ul>
                {this.state.type != 3 ? null : <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>}

              </div>
                        <div className={ Update > 0 ?  "col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric siteMapppingMain" : "col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric siteMapppingMain userRestrict"}>
                        <div className="zui-wrapper bordere3e7f3">
                        <div className="scrollableTableFixed table-scroll zui-scroller scrollableOrgansation orgScroll" id="table-scroll">
                            <table className="table zui-table sitemappingTable border-bot-table">
                                <thead >
                                    <tr>
                                         {Update > 0 ? <th className="fixed-side-siteMap fixed-side1"><label>Action</label></th> : null}
                                        <th><label>From Site</label></th>
                                        <th><label>To Site</label></th>
                                        <th><label>Start Date</label></th>
                                        <th><label>Transportation Lead Time(In Days)</label></th>
                                        <th><label>Status</label></th>                                        
                                    </tr>
                                </thead>

                                <tbody>
                           { this.state.siteMapState == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.siteMapState.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> :this.state.siteMapState.map((data, key) => (
                           <tr key={key}>
                           { Update > 0 ?<td className="fixed-side-siteMap fixed-side1">
                        
                         <ul className="list-inline">
                                                <li>
                                                    <button onClick={() => this.openSiteMapModal(`${data.id}`)}  className="edit_button" >
                                                        {/* EDIT */}
                                                        <img src={editIcon} />
                           </button>
                                                </li>
                                                {/* DELETE */}
                                                {/* <li>
                                                    <button className="delete_button" onClick={()=>this.deleteSiteMapping(`${data.id}`)} data-toggle="modal" data-target="#confirmModal" >
                                                        
                                                        <img src={deleteIcon} />
                                                    </button>
                                                </li> */}

                        </ul> 
                        </td>: null}
                                <td><label>{data.fromSite}</label></td>
                                <td><label>{data.toSite}</label></td>
                                <td><label>{data.startDate}</label></td>
                                <td><label>{data.tptLeadTime}</label></td>
                                <td><label>{data.status}</label></td>
                                
                           </tr>))}
                        </tbody>

                            </table>
                        </div>
                        </div>  
                        </div>
                        <div className="pagerDiv">
                            <ul className="list-inline pagination">
                                <li >
                                    <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                        First
                  </button>
                                </li>
                                <li>
                                        <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                            Prev
                  </button>
                                    </li>
                                {/* {this.state.prev != 0 ? <li >
                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                        Prev
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" disabled>
                                            Prev
                  </button>
                                    </li>} */}
                                <li>
                                    <button className="PageFirstBtn pointerNone">
                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                    </button>
                                </li>
                                {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage  ? <li >
                                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                        Next
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn borderNone" disabled>
                                            Next
                  </button>
                                    </li>}

                             </ul>
                        </div>

                    </div>
                </div>
               {this.state.filter ? <SiteMapFilter 
               fromSiteName={this.state.fromSiteName}
               toSiteName={this.state.toSiteName}
               startDate={this.state.startDate}
               tptLeadTime={this.state.tptLeadTime}
               status={this.state.status}
               updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)}/> : null}
               {this.state.toastLoader ? <ToastLoader toastMsg ={this.state.toastMsg}  /> : null}
                
            </div>
            </div>
        )
    }
}

export default ViewSiteMapping;