import React from "react";
import EmptyBox from './emptyBox';
import AddSiteMapping from './siteMapping/addSiteMapping';
import ViewSiteMapping from './viewSiteMapping';
import siteMapdata from '../../json/siteMapData.json'
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";
class SiteMapping extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      siteMapState: [],
      addSiteMap: false,
      loader: true,
      successMessage: "",
      errorMessage: "",
      emptyBox: false,
      viewSiteMapping: false,
      success: false,
      errorCode: "",
      alert: false,
      code: ""
    };
  }
  componentWillMount() {

    if (!this.props.administration.siteMapping.isSuccess) {
      let data = {
        type: 1,
        no: 1
      }
      this.props.siteMappingRequest(data);
      this.props.fromToRequest();
    } else {
      let data = {
        type: 1,
        no: 1
      }
      this.props.siteMappingRequest(data);
      this.props.fromToRequest();
      this.setState({
        siteMapState: this.props.administration.siteMapping.data.resource,
        loader: false
      })
      this.setState({
        emptyBox: false,
        viewSiteMapping: true
      })
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.siteMapping.isSuccess) {

      this.setState({
        loader: false,
        viewSiteMapping: true,
        emptyBox: false,
        siteMapState: nextProps.administration.siteMapping.data.resource,
      })

    } else if (nextProps.administration.siteMapping.isLoading) {
      this.setState({
        loader: true
      })
    }

    if (nextProps.administration.siteMapping.isError) {
      this.setState({
        viewSiteMapping: true,
        loader: false,
        errorCode: nextProps.administration.siteMapping.message.error == undefined ? undefined : nextProps.administration.siteMapping.message.error.errorCode,
        errorMessage: nextProps.administration.siteMapping.message.error == undefined ? undefined : nextProps.administration.siteMapping.message.error.errorMessage,
        code: nextProps.administration.siteMapping.message.status,
        alert: true,
      })
    }
    if (nextProps.administration.editSiteMapping.isLoading) {
      this.setState({
        loader: true
      })

    }
    else if (nextProps.administration.editSiteMapping.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.administration.editSiteMapping.data.message,
        alert: false,
      })
      this.props.editSiteMappingRequest();
    }
    else if (nextProps.administration.editSiteMapping.isError) {
      this.setState({
        errorMessage: nextProps.administration.editSiteMapping.message.error == undefined ? undefined : nextProps.administration.editSiteMapping.message.error.errorMessage,
        alert: true,
        success: false,
        code: nextProps.administration.editSiteMapping.message.status,
        errorCode: nextProps.administration.editSiteMapping.message.error == undefined ? undefined : nextProps.administration.editSiteMapping.message.error.errorCode,
        loader: false
      })
      this.props.editSiteMappingRequest();
    }
    else if (nextProps.administration.deleteSiteMapping.isLoading) {
      this.setState({
        loader: true
      })

    }
    else if (nextProps.administration.deleteSiteMapping.isSuccess) {
      this.setState({
        successMessage: nextProps.administration.deleteSiteMapping.data.message,
        success: true,
        loader: false,
        alert: false
      })
      this.props.deleteSiteMappingRequest();
    }
    else if (nextProps.administration.deleteSiteMapping.isError) {
      this.setState({
        errorMessage: nextProps.administration.deleteSiteMapping.message.error == undefined ? undefined : nextProps.administration.deleteSiteMapping.message.error.errorMessage,
        alert: true,
        success: false,
        code: nextProps.administration.deleteSiteMapping.message.status,
        errorCode: nextProps.administration.deleteSiteMapping.message.error == undefined ? undefined : nextProps.administration.deleteSiteMapping.message.error.errorCode,
        loader: false
      })
      this.props.deleteSiteMappingRequest();
    }


  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }

  }

  onAddSiteMap() {
    this.props.history.push('/administration/siteMapping/addSiteMapping')
  }

  onSaveSiteMap() {
    this.setState({
      siteMapState: siteMapdata,
    });
    this.props.history.push('/administration/siteMapping/viewSiteMapping')

  }

  render() {
    // let element = document.getElementById('app');
    // this.state.loader ?element.classList.add('blurContent'): element.classList.remove('blurContent');

    return (
      <div className="container-fluid" >
        {this.state.emptyBox ?
          <EmptyBox {...this.props} siteMapState={this.state.siteMapState}
            clickRightSideBar={() => this.props.rightSideBar()} addSiteMap={() => this.onAddSiteMap()} />
          : null} {this.state.viewSiteMapping ?
            <ViewSiteMapping
              {...this.props}
              siteMapState={this.state.siteMapState}
              addAnotherSiteMap={() => this.onAddAnotherSiteMap()}
              clickRightSideBar={() => this.props.rightSideBar()} />
            // : hash == "addSiteMapping"?
            // <AddSiteMapping {...this.props} clickRightSideBar={() => this.props.rightSideBar()} saveSiteMap={() => this.onSaveSiteMap()} />
            : null}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default SiteMapping;
