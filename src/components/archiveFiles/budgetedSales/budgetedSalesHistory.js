import React from 'react';
import FilterLoader from '../../loaders/filterLoader';
import RequestSuccess from '../../loaders/requestSuccess';
import RequestError from '../../loaders/requestError';
import { call, put } from 'redux-saga/effects';
import * as actions from '../../../redux/actions';
import axios from 'axios';
import CONFIG from '../../../config/development';
import ToastLoader from '../../loaders/toastLoader';

class BudgetedSalesHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            type: 1,
            search: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            no: 1,
            uploadedDate: "",
            fileName: "",
            totalData: "",
            valid: "",
            invalid: "",
            loader: true,
            errorCode: "",
            success: false,
            successMessage: "",
            alert: false,
            code: "",
            errorMessage: "",
            tableData: [],
            toastLoader: false,
            toastMsg: ""
        }
    }

    componentWillMount() {
        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.bsHistoryRequest(data);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.demandPlanning.bsHistory.isSuccess) {
            this.setState({
                loader: false,
                tableData: nextProps.demandPlanning.bsHistory.data.resource == null ? [] : nextProps.demandPlanning.bsHistory.data.resource,
                prev: nextProps.demandPlanning.bsHistory.data.prePage,
                current: nextProps.demandPlanning.bsHistory.data.currPage,
                next: nextProps.demandPlanning.bsHistory.data.currPage + 1,
                maxPage: nextProps.demandPlanning.bsHistory.data.maxPage,
            })
            this.props.bsHistoryRequest();
        } else if (nextProps.demandPlanning.bsHistory.isError) {
            this.setState({
                loader: false,
                alert: true,
                errorMessage: nextProps.demandPlanning.bsHistory.message.error == undefined ? undefined : nextProps.demandPlanning.bsHistory.message.error.errorMessage,
                errorCode: nextProps.demandPlanning.bsHistory.message.error == undefined ? undefined : nextProps.demandPlanning.bsHistory.message.error.errorCode,
                code: nextProps.demandPlanning.bsHistory.message.status,
            })
            this.props.bsHistoryRequest();
        }

        if (nextProps.demandPlanning.bsHistory.isLoading) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.demandPlanning.bsHistory.data.prePage,
                    current: this.props.demandPlanning.bsHistory.data.currPage,
                    next: this.props.demandPlanning.bsHistory.data.currPage + 1,
                    maxPage: this.props.demandPlanning.bsHistory.data.maxPage,
                })
                if (this.props.demandPlanning.bsHistory.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.demandPlanning.bsHistory.data.currPage - 1,
                        search: this.state.search,
                    }
                    this.props.bsHistoryRequest(data);
                }

            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.demandPlanning.bsHistory.data.prePage,
                current: this.props.demandPlanning.bsHistory.data.currPage,
                next: this.props.demandPlanning.bsHistory.data.currPage + 1,
                maxPage: this.props.demandPlanning.bsHistory.data.maxPage,
            })
            if (this.props.demandPlanning.bsHistory.data.currPage != this.props.demandPlanning.bsHistory.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.demandPlanning.bsHistory.data.currPage + 1,
                    search: this.state.search,
                }
                this.props.bsHistoryRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.demandPlanning.bsHistory.data.prePage,
                    current: this.props.demandPlanning.bsHistory.data.currPage,
                    next: this.props.demandPlanning.bsHistory.data.currPage + 1,
                    maxPage: this.props.demandPlanning.bsHistory.data.maxPage,
                })
                if (this.props.demandPlanning.bsHistory.data.currPage <= this.props.demandPlanning.bsHistory.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.search,
                    }
                    this.props.bsHistoryRequest(data)
                }
            }
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: ""
        })
        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.bsHistoryRequest(data);
    }

    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.search,
            }
            this.props.bsHistoryRequest(data)
        }
    }

    download(id) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        this.setState({
            loader: true
        })
        axios.get(`${CONFIG.BASE_URL}${CONFIG.BUDGETED}/history/url?uuid=${id}`, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                window.open(`${res.data.data.resource}`);
            }).catch((error) => {
                this.setState({
                    loader: false
                })
                console.log(error);
            });
    }

    render() {
        return (

            <div className="container-fluid">
                <div className="container_div">

                    <div className="col-md-12 col-sm-12 col-xs-12 udfMappingMain">
                        <div className="container_content heightAuto">
                            <div className="col-md-6 col-sm-12 pad-0">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            BUDGETED SALES HISTORY
                                </label>
                                    </li>
                                    <li>
                                        {/* <p className="master_para">lorem ipsum doler immet</p> */}
                                    </li>


                                </ul>
                                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                            </div>

                            <div className="col-md-6 col-sm-12 pad-0">

                                <ul className="list-inline search_list manageSearch">
                                    <li>
                                        <form onSubmit={(e) => this.onSearch(e)}>
                                            <input type="search" onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                            {/* <img src="../imgs/search.svg" className="search_img" /> */}
                                            <button type="submit" className="searchWithBar">Search
                    <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                    <path fill="#ffffff" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                </svg>
                                            </button>
                                        </form>
                                    </li>

                                </ul>
                                {this.state.type != 3 ? null : <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>}

                            </div>
                            <div className="col-md-12 col-sm-12">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3 budgetedTable">
                                    <div className="zui-wrapper">
                                        <div className="scrollableOrgansation scrollableTableFixed roleTableHeight">
                                            <table className="table zui-table roleTable border-bot-table">

                                                <thead>
                                                    <tr>
                                                        <th className="fixed-side-role fixed-side1">
                                                            <label> Action</label>
                                                        </th>
                                                        <th>
                                                            <label> Uploaded Date</label>
                                                        </th>
                                                        <th>
                                                            <label>Filename</label>
                                                        </th>
                                                        <th>
                                                            <label>No. Of Assortment</label>
                                                        </th>
                                                        <th>
                                                            <label>Matched Records</label>
                                                        </th>
                                                        <th>
                                                            <label>Mismatched Records</label>
                                                        </th>
                                                        <th>
                                                            <label>Uploaded By</label>
                                                        </th>



                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {this.state.tableData == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.tableData.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.tableData.map((data, key) => (<tr key={key}>
                                                        <td className="fixed-side-role fixed-side1">
                                                            <ul className="list-inline">
                                                                <li>
                                                                    {data.url != null ? <button type="button" onClick={(e) => this.download(`${data.id}`)} className="reassignBtn downloadBtn" >
                                                                        DOWNLOAD
                                                                    </button> :
                                                                        <button type="button" className="btnDisabled downloadBtnBudgeted" >DOWNLOAD</button>
                                                                    }
                                                                </li>

                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.uploadedDate}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.fileName}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.totalRecord}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.valid}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.invalid}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.uploadedBy}
                                                            </label>
                                                        </td>

                                                    </tr>))}


                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="pagerDiv">
                                <ul className="list-inline pagination">
                                    <li >
                                        <button className={this.state.current == 1 || this.state.current == "" || this.state.current == undefined ? "PageFirstBtn pointerNone" : "PageFirstBtn "} onClick={(e) => this.page(e)} id="first" >
                                            First
                  </button>
                                    </li>
                                    <li>
                                        <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                                            Prev
                  </button>
                                    </li>
                                    {/* {this.state.prev != 0 ? <li >
                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                            Prev
                  </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn" disabled>
                                                Prev
                  </button>
                                        </li>} */}
                                    <li>
                                        <button className="PageFirstBtn pointerNone">
                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                        </button>
                                    </li>
                                    {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage ? <li >
                                        <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                            Next
                  </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn borderNone" disabled>
                                                Next
                  </button>
                                        </li>}

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default BudgetedSalesHistory;