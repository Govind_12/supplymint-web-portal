import React from 'react';
import FilterLoader from '../loaders/filterLoader';
import downloadIconbug from '../../assets/download-icon.svg';
import circle1 from '../../assets/circle-1.svg';
import circle2 from '../../assets/circle-2.svg';
import circle5 from '../../assets/circle-5.svg';
import circleFail from '../../assets/failed.svg';
import statusError from '../../assets/shape-err-fill.svg';
import { CONFIG } from '../../config';
import axios from 'axios';
import RequestError from '../loaders/requestError';
import processComplete from '../../assets/shape-process.svg';
import fileIcon from '../../assets/attachment.svg';
import lockIcon from "../../assets/lock.svg";
import downloadBtnIcon from "../../assets/download-icon-white.svg";
import exclamationGreen from "../../assets/shape-green.svg";
import { call, put } from 'redux-saga/effects';
import * as actions from '../../redux/actions';

class BudgetedSales extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            upFrequency: "",
            upFileData: "",
            upFileName: "",
            upFrequencyerr: false,
            upFileNameerr: false,
            uploadLoad: false,
            uploadSuccess: false,
            uploadFailed: false,
            processLoad: false,
            processSuccess: false,
            processFailed: false,
            verifyLoad: false,
            verifySuccess: false,
            verifyFailed: false,
            successLoad: false,
            successSuccess: false,
            successFailed: false,
            uploadFileRes: "",
            successData: "",
            success: false,
            alert: false,
            loader: true,
            code: "",
            errorCode: "",
            errorMessage: "",
            downFrequency: "",
            downTo: "",
            downFrom: "",
            downFrequencyerr: false,
            downToerr: false,
            downFromerr: false,
            apiUpFrequency: "",
            apiUploadFileRes: "",
            statusApi: false,
            checkFailed: "",
            failedState: "",
            uploadStatus: ""
        }
    }

    handleChange(freq) {
        this.setState({
            upFrequency: freq
        }, () => {
            this.freq()
        })
    }

    fileChange(e) {
        this.setState({
            upFileData: e.target.files[0],
            upFileName: e.target.files[0].name,
        }, () => {
            this.file()
        })
    }

    file() {
        if (
            this.state.upFileName == "") {
            this.setState({
                upFileNameerr: true
            });
        } else {
            this.setState({
                upFileNameerr: false
            });
        }
    }

    freq() {
        if (
            this.state.upFrequency == "") {
            this.setState({
                upFrequencyerr: true
            });
        } else {
            this.setState({
                upFrequencyerr: false
            });
        }
    }

    onUploadFile(e) {
        e.preventDefault();
        this.file();
        this.freq();
        setTimeout(() => {
            if (!this.state.upFileNameerr && !this.state.upFrequencyerr) {
                const formData = new FormData();
                sessionStorage.setItem('fileUpload', true);
                this.setState({
                    uploadLoad: true,
                    uploadSuccess: false,
                    processLoad: false,
                    processSuccess: false,
                    statusApi: false,
                    verifyLoad: false,
                    verifySuccess: false,
                    successLoad: false,
                    successSuccess: false,
                    failedState: "",
                })
                let headers = {
                    'X-Auth-Token': sessionStorage.getItem('token'),
                    'Content-Type': 'multipart/form-data'
                }
                formData.append('fileData', this.state.upFileData);
                formData.append('type', this.state.upFrequency);
                formData.append('userName', sessionStorage.getItem('userName'));
                axios.post(`${CONFIG.BASE_URL}${CONFIG.BUDGETED}/upload/data`, formData, { headers: headers })
                    .then(res => {
                        if (res.data.status == "2000") {
                            this.setState({
                                uploadLoad: false,
                                uploadSuccess: true,
                                uploadFileRes: res.data.data.resource.fileName,
                                processLoad: true
                            })
                            this.setInt();
                            // this.props.processingBSRequest(res.data.data.resource.fileName);
                        } else if (res.data.status == "4000") {
                            this.setState({
                                alert: true,
                                code: res.data.status,
                                errorCode: res.data.error.errorCode,
                                errorMessage: res.data.error.errorMessage,
                                uploadLoad: false,
                                uploadSuccess: false,
                                processLoad: false,
                                processSuccess: false,
                                statusApi: false,
                                verifyLoad: false,
                                verifySuccess: false,
                                successLoad: false,
                                successSuccess: false,
                            })
                        }
                    }).catch((error) => {
                        // this.setState({
                        //     loader: false
                        // })
                    })
            }
        }, 100)
    }

    componentWillMount() {
        this.setInt();
    }

    setInt() {
        this.props.bsStatusRequest('data');

    }

    componentWillReceiveProps(nextProps) {


        // if (nextProps.demandPlanning.processingBS.isSuccess) {
        //     if (nextProps.demandPlanning.processingBS.data.resource == "Success") {
        //         this.setState({
        //             processLoad: false,
        //             processSuccess: true,
        //             verifyLoad: true,
        //         })
        //         if (!this.state.statusApi) {
        //             this.props.verifyBSRequest(this.state.upFrequency);
        //         } else {
        //             this.props.verifyBSRequest(this.state.apiUpFrequency);
        //         }
        //         this.props.processingBSRequest();
        //     } else {
        //         setTimeout(() => {
        //             if (!this.state.statusApi) {
        //                 this.props.processingBSRequest(this.state.uploadFileRes);
        //             } else {
        //                 this.props.processingBSRequest(this.state.apiUploadFileRes);
        //             }
        //         }, 3000)
        //         this.props.processingBSRequest();
        //     }
        //     // function* myfun() {
        //     //     yield put(actions.processingBSClear());
        //     // }
        //     // myfun();
        // } else if (nextProps.demandPlanning.processingBS.isError) {
        //     this.setState({
        //         alert: true,
        //         code: nextProps.demandPlanning.processingBS.message.status,
        //         errorCode: nextProps.demandPlanning.processingBS.message.error == undefined ? undefined : nextProps.demandPlanning.processingBS.message.error.errorCode,
        //         errorMessage: nextProps.demandPlanning.processingBS.message.error == undefined ? undefined : nextProps.demandPlanning.processingBS.message.error.errorMessage
        //     })
        //     setTimeout(() => {
        //         if (!this.state.statusApi) {
        //             this.props.processingBSRequest(this.state.uploadFileRes);
        //         } else {
        //             this.props.processingBSRequest(this.state.apiUploadFileRes);
        //         }
        //     }, 3000)
        //     // function* myfun() {
        //     //     yield put(actions.processingBSClear());
        //     // }
        //     // myfun();
        //     this.props.processingBSRequest();
        // }

        // if (nextProps.demandPlanning.verifyBS.isSuccess) {
        //     if (nextProps.demandPlanning.verifyBS.data.resource == "Success") {

        //         this.setState({
        //             verifyLoad: false,
        //             verifySuccess: true,
        //             successLoad: true,
        //         })
        //         if (!this.state.statusApi) {
        //             this.props.successBSRequest(this.state.upFrequency);
        //         } else {
        //             this.props.successBSRequest(this.state.apiUpFrequency);
        //         }
        //         this.props.verifyBSRequest();
        //     } else {
        //         setTimeout(() => {

        //             if (!this.state.statusApi) {
        //                 this.props.verifyBSRequest(this.state.upFrequency);
        //             } else {
        //                 this.props.verifyBSRequest(this.state.apiUpFrequency);
        //             }
        //         }, 3000)
        //         this.props.verifyBSRequest();
        //     }
        //     // function* myfun() {
        //     //     yield put(actions.verifyBSClear());
        //     // }
        //     // myfun();
        // } else if (nextProps.demandPlanning.verifyBS.isError) {
        //     this.setState({
        //         alert: true,
        //         code: nextProps.demandPlanning.verifyBS.message.status,
        //         errorCode: nextProps.demandPlanning.verifyBS.message.error == undefined ? undefined : nextProps.demandPlanning.verifyBS.message.error.errorCode,
        //         errorMessage: nextProps.demandPlanning.verifyBS.message.error == undefined ? undefined : nextProps.demandPlanning.verifyBS.message.error.errorMessage
        //     })
        //     setTimeout(() => {
        //         if (!this.state.statusApi) {
        //             this.props.verifyBSRequest(this.state.upFrequency);
        //         } else {
        //             this.props.verifyBSRequest(this.state.apiUpFrequency);
        //         }
        //     }, 3000)
        //     // function* myfun() {
        //     //     yield put(actions.verifyBSClear());
        //     // }
        //     // myfun();
        //     this.props.verifyBSRequest();
        // }

        // if (nextProps.demandPlanning.successBS.isSuccess) {
        //     if (sessionStorage.getItem('fileUpload') == 'true') {
        //         this.setState({
        //             successLoad: false,
        //             successSuccess: true,
        //         })
        //     }
        //     this.setState({
        //         successData: nextProps.demandPlanning.successBS.data.resource
        //     })
        //     // function* myfun() {
        //     //     yield put(actions.successBSClear());
        //     // }
        //     // myfun();
        //     this.props.successBSRequest();
        // } else if (nextProps.demandPlanning.successBS.isError) {
        //     this.setState({
        //         alert: true,
        //         code: nextProps.demandPlanning.successBS.message.status,
        //         errorCode: nextProps.demandPlanning.successBS.message.error == undefined ? undefined : nextProps.demandPlanning.successBS.message.error.errorCode,
        //         errorMessage: nextProps.demandPlanning.successBS.message.error == undefined ? undefined : nextProps.demandPlanning.successBS.message.error.errorMessage
        //     })
        //     // function* myfun() {
        //     //     yield put(actions.successBSClear());
        //     // }
        //     // myfun();
        //     this.props.successBSRequest();
        // }

        if (nextProps.demandPlanning.bsStatus.isSuccess) {
            this.setState({
                loader: false
            })
            if (nextProps.demandPlanning.bsStatus.data.resource != null) {
                // console.log(nextProps.demandPlanning.bsStatus.data.resource.lastStatus);
                if (nextProps.demandPlanning.bsStatus.data.resource.status == "SUCCEEDED") {
                    clearInterval(this.bcStatusInterval)
                    sessionStorage.setItem("bsStatus", "SUCCEEDED")
                    if (sessionStorage.getItem('fileUpload') == 'true') {
                        this.setState({
                            uploadLoad: false,
                            uploadSuccess: true,
                            processLoad: false,
                            processSuccess: true,
                            verifyLoad: false,
                            loader: false,
                            verifySuccess: true,
                            successLoad: false,
                            successData: nextProps.demandPlanning.bsStatus.data.resource.data,
                            successSuccess: true,
                            uploadStatus: false
                            // checkFailed : "SUCCEEDED"
                        })
                    } else {
                        this.setState({
                            successData: nextProps.demandPlanning.bsStatus.data.resource.data,
                            apiUpFrequency: nextProps.demandPlanning.bsStatus.data.resource.frequency,
                            apiUploadFileRes: nextProps.demandPlanning.bsStatus.data.resource.fileName,
                            statusApi: true,
                            loader: false,
                        })

                    }

                    this.props.bsStatusRequest();
                    // this.props.successBSRequest(nextProps.demandPlanning.bsStatus.data.resource.frequency);
                } else if (nextProps.demandPlanning.bsStatus.data.resource.status == "VERIFY") {
                    sessionStorage.setItem("bsStatus", "VERIFY")
                    this.setState({
                        apiUpFrequency: nextProps.demandPlanning.bsStatus.data.resource.frequency,
                        apiUploadFileRes: nextProps.demandPlanning.bsStatus.data.resource.fileName,
                        statusApi: true,
                        uploadLoad: false,
                        uploadSuccess: true,
                        processLoad: false,
                        processSuccess: true,
                        verifyLoad: false,
                        loader: false,
                        verifySuccess: true,
                        successLoad: true,
                        successSuccess: false,
                        // checkFailed : "VERIFY"
                    })

                    setTimeout(() => {
                        this.props.bsStatusRequest('data');
                    }, 5000)
                    // this.props.verifyBSRequest(nextProps.demandPlanning.bsStatus.data.resource.frequency);
                } else if (nextProps.demandPlanning.bsStatus.data.resource.status == "PROCESS") {
                    sessionStorage.setItem("bsStatus", "PROCESS")
                    this.setState({
                        apiUpFrequency: nextProps.demandPlanning.bsStatus.data.resource.frequency,
                        apiUploadFileRes: nextProps.demandPlanning.bsStatus.data.resource.fileName,
                        statusApi: true,
                        uploadLoad: false,
                        uploadSuccess: true,
                        processLoad: false,
                        processSuccess: true,
                        verifyLoad: true,
                        verifySuccess: false,
                        successLoad: false,
                        successSuccess: false,
                        loader: false,
                        // checkFailed : "PROCESS"/
                    })
                    setTimeout(() => {
                        this.props.bsStatusRequest('data');
                    }, 5000)
                    // this.props.processingBSRequest(nextProps.demandPlanning.bsStatus.data.resource.fileName);
                } else if (nextProps.demandPlanning.bsStatus.data.resource.status == "UPLOADED") {
                    sessionStorage.setItem("bsStatus", "UPLOADED")
                    this.setState({
                        apiUpFrequency: nextProps.demandPlanning.bsStatus.data.resource.frequency,
                        apiUploadFileRes: nextProps.demandPlanning.bsStatus.data.resource.fileName,
                        statusApi: true,
                        uploadLoad: false,
                        uploadSuccess: true,
                        processLoad: true,
                        processSuccess: false,
                        verifyLoad: false,
                        verifySuccess: false,
                        successLoad: false,
                        successSuccess: false,
                        loader: false,
                        // checkFailed : "UPLOADED"                        
                    })
                    sessionStorage.setItem("checkFailed", "UPLOADED")
                    setTimeout(() => {
                        this.props.bsStatusRequest('data');
                    }, 5000)
                    // this.props.processingBSRequest(nextProps.demandPlanning.bsStatus.data.resource.fileName);
                }

                else if (nextProps.demandPlanning.bsStatus.data.resource.status == "FAILED") {
                    this.setState({
                        uploadStatus: false
                    })
                    if (sessionStorage.getItem("bsStatus") == null) {


                        this.setState({
                            failedState: "",
                            verifySuccess: false,
                            processSuccess: false,
                            uploadSuccess: false,
                        })
                    } else {
                        if (nextProps.demandPlanning.bsStatus.data.resource.lastStatus != null) {
                            console.log(nextProps.demandPlanning.bsStatus.data.resource.lastStatus)

                            if (nextProps.demandPlanning.bsStatus.data.resource.lastStatus == "PROCESS") {
                                this.setState({
                                    failedState: "VERIFY",
                                    processLoad: false,
                                    verifyLoad: false,
                                    processSuccess: true,
                                    uploadSuccess: true,
                                    uploadLoad: false
                                })
                            } else if (nextProps.demandPlanning.bsStatus.data.resource.lastStatus == "VERIFY") {
                                this.setState({
                                    failedState: "SUCCEEDED",
                                    processSuccess: true,
                                    uploadSuccess: true,
                                    verifySuccess: true,
                                    verifyLoad: false,
                                    successLoad: false,
                                    processLoad: false
                                })

                            } else if (nextProps.demandPlanning.bsStatus.data.resource.lastStatus == "UPLOADED") {
                                console.log(nextProps.demandPlanning.bsStatus.data.resource.lastStatus)
                                this.setState({
                                    failedState: "PROCESS",
                                    uploadLoad: false,
                                    processLoad: false,
                                    processSuccess: false,
                                    uploadSuccess: true
                                })
                            }
                            else if (nextProps.demandPlanning.bsStatus.data.resource.lastStatus == "SUCCEEDED") {
                                this.setState({
                                    failedState: "SUCCEEDED",
                                    verifySuccess: true,
                                    processSuccess: true,
                                    uploadSuccess: true,
                                    uploadLoad: false,
                                    successLoad: false
                                })
                            }
                        }
                    }

                    this.props.bsStatusRequest();
                }
            }

        } else if (nextProps.demandPlanning.bsStatus.isError) {
            this.setState({
                loader: false,
                alert: true,
                errorMessage: nextProps.demandPlanning.bsStatus.message.error == undefined ? undefined : nextProps.demandPlanning.bsStatus.message.error.errorMessage,
                errorCode: nextProps.demandPlanning.bsStatus.message.error == undefined ? undefined : nextProps.demandPlanning.bsStatus.message.error.errorCode,
                code: nextProps.demandPlanning.bsStatus.message.status,
            })
            this.props.bsStatusRequest();
        }
    }

    componentWillUnmount() {
        clearInterval(this.bcStatusInterval);
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    xlscsv(type) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        this.setState({
            loader: true
        })
        axios.get(`${CONFIG.BASE_URL}/download/module/xls/${type}`, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                window.open(`${res.data.data.resource}`);
            }).catch((error) => {

                this.setState({
                    loader: false
                })
            });
    }

    handleDownChange(freq) {
        this.setState({
            downFrequency: freq
        }, () => {
            this.downFrequency();
        })
    }

    handleToFrom(e) {
        if (e.target.id == "to") {
            e.target.placeholder = e.target.value;
            this.setState({
                downTo: e.target.value
            }, () => {
                this.downTo();
            })
        } else if (e.target.id == "from") {
            e.target.placeholder = e.target.value;
            this.setState({
                downFrom: e.target.value
            }, () => {
                this.downFrom();
            })

        }
    }

    downFrequency() {
        if (
            this.state.downFrequency == "") {
            this.setState({
                downFrequencyerr: true
            });
        } else {
            this.setState({
                downFrequencyerr: false
            });
        }
    }
    downFrom() {
        if (
            this.state.downFrom == "") {
            this.setState({
                downFromerr: true
            });
        } else {
            this.setState({
                downFromerr: false
            });
        }
    }
    downTo() {
        if (
            this.state.downTo == "") {
            this.setState({
                downToerr: true
            });
        } else {
            this.setState({
                downToerr: false
            });
        }
    }

    downloadFile() {
        this.downFrequency();
        this.downFrom();
        this.downTo();
        setTimeout(() => {
            if (!this.state.downFrequencyerr && !this.state.downFromerr && !this.state.downToerr) {
                let headers = {
                    'X-Auth-Token': sessionStorage.getItem('token'),
                    'Content-Type': 'application/json'
                }
                this.setState({
                    loader: true
                })
                axios.get(`${CONFIG.BASE_URL}${CONFIG.BUDGETED}/download/data?frequency=${this.state.downFrequency}&fromDate=${this.state.downFrom}&toDate=${this.state.downTo}`, { headers: headers })
                    .then(res => {
                        this.setState({
                            downFrequency: "",
                            downFrom: "",
                            downTo: "",
                            loader: false
                        })
                        this.setInt();
                        document.getElementById('to').placeholder = "";
                        document.getElementById('from').placeholder = ""
                        window.open(`${res.data.data.resource}`);
                    }).catch((error) => {
                        this.setState({
                            loader: false
                        })
                    });
            }
        })
    }

    downloadErrorLog() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        this.setState({
            loader: true
        })
        axios.get(`${CONFIG.BASE_URL}/download/module/xls/DemandBudgeted`, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                window.open(`${res.data.data.resource}`);
            }).catch((error) => {
                this.setState({
                    loader: false
                })
            });
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="container_div" id="home">
                    <div className="container-fluid">
                        <div className="container_div" id="">

                            <div className="container-fluid">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="replenishment_container customRunOnDemand budgetedSales">
                                        <div className="col-md-12 col-sm-12 pad-0">
                                            <ul className="list_style">
                                                <li>
                                                    <label className="contribution_mart">
                                                        BUDGETED SALES
                                                </label>
                                                </li>
                                                <li>
                                                    <p className="master_para">Manage budgeted sales</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-20 finalAssortment">

                                                <div className="col-md-9 col-sm-9 budgetSales pad-lft-0">
                                                    <div className="boxShadow">
                                                        <h4>BUDGETED SALES</h4>
                                                        <div className="selectFrequencyCustom m-top-20">
                                                            <ul className="pad-0">
                                                                <li> <label className="select_modalRadio">
                                                                    <input onChange={() => this.handleDownChange('Weekly')} checked={this.state.downFrequency == "Weekly"} type="radio" name="frequency" /><span>Weekly</span>
                                                                    <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                                    {this.state.downFrequencyerr ? <span className="error errorCode">Select Frequency</span> : null}
                                                                </label>
                                                                </li>
                                                                <li><label className="select_modalRadio">
                                                                    <input onChange={() => this.handleDownChange('Monthly')} checked={this.state.downFrequency == "Monthly"} type="radio" name="frequency" /><span>Monthly</span>
                                                                    <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                                </label>
                                                                </li>
                                                                <li>
                                                                    <label>From Date</label>
                                                                    <input type="date" onChange={(e) => this.handleToFrom(e)} id="from" value={this.state.downFrom} className=" purchaseOrderTextbox pad-right-8" placeholder="Start Date" />
                                                                    {this.state.downFromerr ? <span className="error errorCode">Select From Date</span> : null}
                                                                </li>
                                                                <li>
                                                                    <label>To Date</label>
                                                                    <input type="date" min={this.state.downFrom} onChange={(e) => this.handleToFrom(e)} value={this.state.downTo} id="to" className="purchaseOrderTextbox pad-right-8" placeholder="End Date" />
                                                                    {this.state.downToerr ? <span className="error errorCode">Select To Date</span> : null}
                                                                </li>
                                                                <li>
                                                                    <button type="button" onClick={() => this.downloadFile()}>DOWNLOAD FILE</button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-3 col-sm-3">
                                                    <div className="boxShadow">
                                                        <h4>FINAL ASSORTMENT</h4>
                                                        <p>Available for download</p>
                                                        <button onClick={() => this.xlscsv('FinalAssortment')} type="button" className="availDownload">DOWNLOAD FILE <img src={downloadBtnIcon} /></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <div className="col-md-12 col-sm-12 col-xs-12 uploadData pad-lft-0">
                                            {/* --------------------------------Patch For Disable field--------------------------------- */}
                                            {this.state.uploadLoad || this.state.processLoad || this.state.verifyLoad || this.state.successLoad || this.state.uploadStatus ? <div className="toolTipMainHover budgetedSalesPatch">
                                                {console.log(this.state.uploadStatus)}

                                                <div className="noClickDiv">
                                                </div>
                                                <div className="topToolTip ">

                                                    <span className="topToolTipText skechersTooltip">
                                                        {/* You cannot apply filter as the job is already running. */}
                                                        <img src={lockIcon} />
                                                        <h3>Temporary Locked</h3>
                                                        <p>You can’t make any selection as file upload is in progress.  Please wait until the file upload process is completed.</p>
                                                    </span>

                                                </div>
                                            </div> : null}
                                            {/* --------------------------------------patch code end--------------------------------------------*/}
                                            <div className="predectionTime m-top-30 boxShadow displayInline">
                                                <div className="budgetSaleStatus float_Left">
                                                    <label>UPLOAD DATA</label>
                                                    {/* <button type="button" onClick={() => this.xlscsv('ExcelTemplate')} className="downloadExcelTemplate">Download Excel Template<img src={downloadIconbug} /></button> */}
                                                    <div className="selectFrequencyCustom m-top-20">


                                                        <ul className="pad-0">

                                                            <li className="margin-right0">
                                                                <label>CHOOSE FREQUENCY</label>
                                                                <label className="select_modalRadio">
                                                                    <input type="radio" checked={this.state.upFrequency == "Weekly"} onChange={() => this.handleChange('Weekly')} name="frequency1" /><span>Weekly</span>
                                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                                </label>
                                                            </li>
                                                            <li><label className="select_modalRadio m-top-28">
                                                                <input type="radio" checked={this.state.upFrequency == "Monthly"} onChange={() => this.handleChange('Monthly')} name="frequency1" /><span>Monthly</span>
                                                                <span className="checkradio-select select_all positionCheckbox"></span>
                                                            </label></li>
                                                            <li>
                                                                <div className="uploadFile displayInline">
                                                                    <label>UPLOAD FILE</label>
                                                                    <label className="custom-file-upload-budget chooseFileBudgeted">
                                                                        <input onChange={(e) => this.fileChange(e)} type="file" />
                                                                        Choose File<img src={fileIcon} />
                                                                        <p className="tooltiptext topToolTipGeneric">
                                                                            Expected Column Headers-<span>Assortment Code, Bill Date, Budgeted QTY</span>
                                                                        </p>
                                                                    </label>
                                                                    <div className="uploadStatus">
                                                                        <span>
                                                                            {this.state.upFileName}
                                                                        </span>
                                                                    </div>
                                                                    <button onClick={(e) => this.onUploadFile(e)} type="button">UPLOAD FILE</button>
                                                                    <label className="messageBottom">Don’t have Template ? <span onClick={() => this.xlscsv('ExcelTemplate')}>Download Now</span></label>
                                                                    {this.state.upFileNameerr ? <span className="error errorCode">Choose File</span> : null}
                                                                </div>
                                                            </li>
                                                            {this.state.upFrequencyerr ? <span className="error errorCode">Select Frequency</span> : null}
                                                        </ul>
                                                    </div>


                                                    <div className="uploadingProcess posRelative">
                                                        <div className="col-md-12 col-sm-12 pad-0 fourIcons m-top-50">
                                                            <div className="col-md-12 col-md-12 pad-0 verticalTop displayFlex">
                                                                {/* <div className="col-md-3 textLeft pad-lft-0"><img src={this.state.uploadLoad ? circle2 : this.state.uploadSuccess ? circle5 : circle1} /><span className="circleMiddletext"><p className={this.state.uploadLoad ? "blueText" : this.state.uploadSuccess ? "displaynone" : ""}>1</p></span><label className={this.state.uploadLoad ? "fontWeig600" : this.state.uploadSuccess ? "txtBlue fontWeig600" : ""}>Upload</label></div>
                                                                <div className="col-md-3 textLeft pad-lft-0"><img src={this.state.processLoad ? circle2 : this.state.processSuccess ? circle5 : circle1} /><span className="circleMiddletext"><p className={this.state.processLoad ? "blueText" : this.state.processSuccess ? "displaynone" : ""}>2</p></span><label className={this.state.processLoad ? "fontWeig600" : this.state.processSuccess ? "txtBlue fontWeig600" : ""}>Processing</label></div>
                                                                <div className="col-md-3 textLeft pad-lft-0"><img src={this.state.verifyLoad ? circle2 : this.state.verifySuccess ? circle5 : circle1} /><span className="circleMiddletext"><p className={this.state.verifyLoad ? "blueText" : this.state.verifySuccess ? "displaynone" : ""}>3</p></span><label className={this.state.verifyLoad ? "fontWeig600" : this.state.verifySuccess ? "txtBlue fontWeig600" : ""}>Verifying</label></div>
                                                                <div className="col-md-3 textLeft pad-lft-0"><img src={this.state.successLoad ? circle2 : this.state.successSuccess ? circle5 : circle1} /><span className="circleMiddletext"><p className={this.state.successLoad ? "blueText" : this.state.successSuccess ? "displaynone" : ""}>4</p></span><label className={this.state.successLoad ? "fontWeig600" : this.state.successSuccess ? "txtBlue fontWeig600" : ""}>Success</label></div> */}

                                                                <div className="col-md-3 textLeft pad-lft-0">{this.state.uploadLoad ? <img src={circle2} className="budgetesProcessingImg" /> : this.state.uploadSuccess ? <img src={circle5} /> : this.state.failedState == "UPLOADED" ? <img src={circleFail} /> : <img src={circle1} />}<span className="circleMiddletext"><p className={this.state.uploadLoad ? "blueText" : this.state.uploadSuccess ? "displaynone" : ""}>{this.state.failedState == "UPLOADED" ? "" : 1}</p></span><label className={this.state.uploadLoad ? "fontWeig600" : this.state.uploadSuccess ? "txtBlue fontWeig600" : ""}>Upload</label></div>
                                                                <div className="col-md-3 textLeft pad-lft-0">{this.state.processLoad ? <img src={circle2} className="budgetesProcessingImg" /> : this.state.processSuccess ? <img src={circle5} /> : this.state.failedState == "PROCESS" ? <img src={circleFail} /> : <img src={circle1} />}<span className="circleMiddletext"><p className={this.state.processLoad ? "blueText" : this.state.processSuccess ? "displaynone" : ""}>{this.state.failedState == "PROCESS" ? "" : 2}</p></span><label className={this.state.processLoad ? "fontWeig600" : this.state.processSuccess ? "txtBlue fontWeig600" : ""}>Processing</label></div>
                                                                <div className="col-md-3 textLeft pad-lft-0">{this.state.verifyLoad ? <img src={circle2} className="budgetesProcessingImg" /> : this.state.verifySuccess ? <img src={circle5} /> : this.state.failedState == "VERIFY" ? <img src={circleFail} /> : <img src={circle1} />}<span className="circleMiddletext"><p className={this.state.verifyLoad ? "blueText" : this.state.verifySuccess ? "displaynone" : ""}>{this.state.failedState == "VERIFY" ? "" : 3}</p></span><label className={this.state.verifyLoad ? "fontWeig600" : this.state.verifySuccess ? "txtBlue fontWeig600" : ""}>Verifying</label></div>
                                                                <div className="col-md-3 textLeft pad-lft-0">{this.state.successLoad ? <img src={circle2} className="budgetesProcessingImg" /> : this.state.successSuccess ? <img src={circle5} /> : this.state.failedState == "SUCCEEDED" ? <img src={circleFail} /> : <img src={circle1} />}<span className="circleMiddletext"><p className={this.state.successLoad ? "blueText" : this.state.successSuccess ? "displaynone" : ""}>{this.state.failedState == "SUCCEEDED" ? "" : 4}</p></span><label className={this.state.successLoad ? "fontWeig600" : this.state.successSuccess ? "txtBlue fontWeig600" : ""}>Success</label></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {this.state.successData == "" || this.state.successData == undefined ? null :
                                                    <div className="uploadStatusBudget float_Left">
                                                        <label>Upload Status</label>
                                                        <div className="statusIcon m-top-10">
                                                            {this.state.successData.invalid > 0 ? <div>
                                                                <img src={processComplete} />
                                                                <p>Process completed with errors</p>
                                                            </div>
                                                                :
                                                                <div>
                                                                    <img src={exclamationGreen} />
                                                                    <p>Process completed Successfully !</p>
                                                                </div>}
                                                        </div>
                                                        <div className="assortmentTotal">
                                                            <ul className="pad-0 m-top-30">
                                                                <li>
                                                                    <h4>Total Assortment</h4>
                                                                    <p>{this.state.successData.total}</p>
                                                                </li>
                                                                <li>
                                                                    <h4>Mismatched Records</h4>
                                                                    <p>{this.state.successData.invalid}</p>
                                                                </li>
                                                                <li>
                                                                    <h4>Matched Records</h4>
                                                                    <p>{this.state.successData.valid}</p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div className="errorLogsBtn m-top-20">
                                                            {this.state.successData.invalid > 0 ? <button onClick={() => this.downloadErrorLog()} type="button">Download Error Logs</button> : null}
                                                        </div>
                                                    </div>}

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
            </div>

        );
    }
}

export default BudgetedSales;