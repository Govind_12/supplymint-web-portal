import React from "react";
import multideleteIcon from "../../assets/multidelete.svg";
import RequestError from "../loaders/requestError";
import FilterLoader from "../loaders/filterLoader";
import refreshIcon from "../../assets/refresh.svg";
import DemandPlanningFilter from './demandPlanningFilter';
import { getMonthByNumber } from '../../helper'
import axios from 'axios';
import { CONFIG } from "../../config/index";
import ToastLoader from "../loaders/toastLoader";

class ForecastHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            type: 1,
            forcastData: [],
            runId: "NA",
            frequency: "",
            frequencyerr: false,
            chooseYear: "",
            predictPeriod: "",
            startMonth: "",
            toastLoader: false,
            toastMsg: ""

        }
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            demandPlanning: !this.state.demandPlanning,
            filterBar: !this.state.filterBar
        });
    }

    onClearFilter(e) {
        this.setState({
            search: "",
            type: 1,
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        });
        let data = {
            no: 1,
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        }
        this.props.getForcastRequest(data);
    }

    onClearSearch(e) {
        this.setState({
            search: "",
            type: 1
        });
        let data = {
            no: 1,
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        }
        this.props.getForcastRequest(data);

    }
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.demandPlanning.getForcast.data.prePage,
                    current: this.props.demandPlanning.getForcast.data.currPage,
                    next: this.props.demandPlanning.getForcast.data.currPage + 1,
                    maxPage: this.props.demandPlanning.getForcast.data.maxPage,
                })
                if (this.props.demandPlanning.getForcast.data.currPage != 0) {
                    let data = {
                        no: this.props.demandPlanning.getForcast.data.currPage - 1,
                        search: this.state.search,
                        type: this.state.type,
                        demandForecast: this.state.demandForecast,
                        frequency: this.state.frequency,
                        predictPeriod: this.state.predictPeriod,
                        chooseYear: this.state.chooseYear,
                        startMonth: this.state.startMonth,
                        generatedForecast: this.state.generatedForecast,
                        status: this.state.status
                    };
                    this.props.getForcastRequest(data);
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.demandPlanning.getForcast.data.prePage,
                current: this.props.demandPlanning.getForcast.data.currPage,
                next: this.props.demandPlanning.getForcast.data.currPage + 1,
                maxPage: this.props.demandPlanning.getForcast.data.maxPage,
            })
            if (this.props.demandPlanning.getForcast.data.currPage != this.props.demandPlanning.getForcast.data.maxPage) {
                let data = {
                    no: this.props.demandPlanning.getForcast.data.currPage + 1,
                    search: this.state.search,
                    type: this.state.type,
                    demandForecast: this.state.demandForecast,
                    frequency: this.state.frequency,
                    predictPeriod: this.state.predictPeriod,
                    chooseYear: this.state.chooseYear,
                    startMonth: this.state.startMonth,
                    generatedForecast: this.state.generatedForecast,
                    status: this.state.status
                };
                this.props.getForcastRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.demandPlanning.getForcast.data.prePage,
                    current: this.props.demandPlanning.getForcast.data.currPage,
                    next: this.props.demandPlanning.getForcast.data.currPage + 1,
                    maxPage: this.props.demandPlanning.getForcast.data.maxPage,
                })
                if (this.props.demandPlanning.getForcast.data.currPage <= this.props.demandPlanning.getForcast.data.maxPage) {
                    let data = {
                        no: 1,
                        search: this.state.search,
                        type: this.state.type,
                        demandForecast: this.state.demandForecast,
                        frequency: this.state.frequency,
                        predictPeriod: this.state.predictPeriod,
                        chooseYear: this.state.chooseYear,
                        startMonth: this.state.startMonth,
                        generatedForecast: this.state.generatedForecast,
                        status: this.state.status
                    };
                    this.props.getForcastRequest(data)
                }

            }
        }
    }
    componentWillMount() {
        let data = {
            no: 1,
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        }
        this.props.getForcastRequest(data);

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.demandPlanning.getForcast.isSuccess) {
            this.setState({
                loader: false,
                forcastData: nextProps.demandPlanning.getForcast.data.resource == null ? [] : nextProps.demandPlanning.getForcast.data.resource,
                prev: nextProps.demandPlanning.getForcast.data.prePage,
                current: nextProps.demandPlanning.getForcast.data.currPage,
                next: nextProps.demandPlanning.getForcast.data.currPage + 1,
                maxPage: nextProps.demandPlanning.getForcast.data.maxPage,
            })
        } else if (nextProps.demandPlanning.getForcast.isError) {
            this.setState({
                code: nextProps.demandPlanning.getForcast.message.status,
                loader: false,
                errorCode: nextProps.demandPlanning.getForcast.message.error == undefined ? undefined : nextProps.demandPlanning.getForcast.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.getForcast.message.error == undefined ? undefined : nextProps.demandPlanning.getForcast.message.error.errorMessage,
            })
        }

        if (nextProps.demandPlanning.getForcast.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                // demandForecast: this.state.demandForecast,
                demandForecast: "",
                frequency: "",
                predictPeriod: "",
                chooseYear: "",
                startMonth: "",
                generatedForecast: "",
                status: ""
            })
            let data = {
                no: 1,
                search: this.state.search,
                type: 3,
                demandForecast: "",
                frequency: "",
                predictPeriod: "",
                chooseYear: "",
                startMonth: "",
                generatedForecast: "",
                status: ""
            };
            this.props.getForcastRequest(data);
        }
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            demandPlanning: !this.state.demandPlanning,
            filterBar: !this.state.filterBar
        });
    }

    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            demandForecast: data.demandForecast,
            frequency: data.frequency,
            predictPeriod: data.predictPeriod,
            chooseYear: data.chooseYear,
            startMonth: data.startMonth,
            generatedForecast: data.generatedForecast,
            search: data.search,
            status: data.status
        });
    }

    refresh() {
        let data = {
            no: this.state.current,
            type: this.state.type,
            search: this.state.search,
            demandForecast: this.state.demandForecast,
            frequency: this.state.frequency,
            predictPeriod: this.state.predictPeriod,
            chooseYear: this.state.chooseYear,
            startMonth: this.state.startMonth,
            generatedForecast: this.state.generatedForecast,
            status: this.state.status
        }
        this.props.getForcastRequest(data);
    }

    downloadForecast(runId) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/download/history?runId=${runId}`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                console.log(error);
            });
    }

    render() {

        let show = false;
        if (this.state.rangeVal != "" && this.state.frequency != "" && this.state.year != "" && this.state.startMonth != "") {
            show = true
        }

        return (

            <div className="container-fluid pad-0">
                <div className="container_div" id="home">

                    <div className="container-fluid">
                        <div className="container_div" id="">

                            <div className="container-fluid">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="replenishment_container customRunOnDemand forecastHistoryMain">
                                        <div className="col-md-12 pad-0">
                                            <div className="col-md-4 pad-0">
                                                <div className="col-md-12 col-sm-12 pad-0">
                                                    <ul className="list_style">
                                                        <li>
                                                            <label className="contribution_mart">
                                                                FORECAST HISTORY
                                                    </label>
                                                        </li>
                                                        <li>
                                                            {/* <p className="master_para">Schedule and run engine for forecasting</p> */}
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>

                                        </div>

                                        <div className="col-md-6 col-sm-12 pad-0">
                                            <ul className="list_style">


                                                <li className="m-top-50">

                                                    <ul className="list-inline m-top-10">
                                                        {/* <li>
                                                        <button type="button" onClick={() => this.xlscsvDownload('CSV')} className="button_home">
                                                            CSV
                                                    </button>
                                                    </li>
                                                    <li>
                                                        <button type="button" onClick={() => this.xlscsvDownload('XLS')} className="button_home">
                                                            XLS
                                                    </button>
                                                    </li> */}
                                                        <li>
                                                            <button className="filter_button" onClick={e => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                                                FILTER
    
                                                        <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                                    <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                                </svg>
                                                            </button>
                                                        </li>
                                                        <li className="refresh-table">
                                                            <img onClick={() => this.refresh()} src={refreshIcon} />
                                                            <p className="tooltiptext topToolTipGeneric">
                                                                Refresh
                                                        </p>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                                        </div>
                                        <div className="col-md-6 col-sm-12 pad-0">

                                            <ul className="list-inline search_list manageSearch">
                                                <li>
                                                    <form onSubmit={(e) => this.onSearch(e)}>
                                                        <input onChange={(e) => this.setState({ search: e.target.value })} value={this.state.search} type="search" placeholder="Type to Search..." className="search_bar" />
                                                        <button type="submit" className="searchWithBar">Search
                                        <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                                <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                                </path></svg>
                                                        </button>
                                                    </form>
                                                </li>

                                            </ul>
                                            {this.state.type == 3 ? <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                                        </div>


                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric tableBorder forecastOnDemandTable">
                                            <div className="scrollableOrgansation">
                                                <table className="table UserManageTable tableOddColor m-top-20">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <label>Run Id</label>
                                                            </th>
                                                            <th>
                                                                <label>Frequency</label>
                                                            </th>
                                                            <th>
                                                                <label>Year</label>
                                                            </th>

                                                            <th>
                                                                <label>Start Month</label>
                                                            </th>
                                                            <th>
                                                                <label> Forecast Period</label>
                                                            </th>
                                                            <th>
                                                                <label> Forecast On</label>
                                                            </th>
                                                            <th>
                                                                <label>Duration</label>
                                                            </th>
                                                            <th>
                                                                <label>Status</label>
                                                            </th>
                                                            <th>
                                                                <label>Download</label>
                                                            </th>



                                                        </tr>
                                                    </thead>

                                                    <tbody>

                                                        {this.state.forcastData.length == 0 ? <tr className="tableNoData"><td colSpan="9"> NO DATA FOUND </td></tr> : this.state.forcastData.map((data, key) => (<tr key={key}>
                                                            <td>
                                                                <label>
                                                                    {data.runId}
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {data.frequency}
                                                                </label>
                                                            </td>

                                                            {/* <td>
                                                            <label className="maxwidth200">
                                                            {data.totalStore}
                                                            </label>
                                                            </td> */}

                                                            <td>
                                                                <label>
                                                                    {data.chooseYear}
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {getMonthByNumber(data.startMonth)}
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {data.predictPeriod}
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {data.createdOn}
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {data.duration} mins
                                                            </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {data.status}
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label>
                                                                    {/* <a href={data.downloadUrl} download>{data.downloadUrl == null ? null : <img src={downloadIcon} />}</a> */}
                                                                    {data.status == "Succeeded" || data.status == "SUCCEEDED" ? <button className="downloadBtn crvBtn" type="button" onClick={() => this.downloadForecast(`${data.runId}`)} >Download</button> : <button type="button" className="crvBtn btnDisabled" >Download</button>}


                                                                </label>

                                                            </td>


                                                        </tr>))}

                                                    </tbody>

                                                </table>
                                            </div>

                                        </div>

                                        <div className="container-fluid">
                                            <div className="pagerDiv">
                                                <ul className="list-inline pagination">
                                                    <li >
                                                        <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                                            First
              </button>
                                                    </li>
                                                    {this.state.prev != 0 ? <li >
                                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                            Prev
              </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn" disabled>
                                                                Prev
              </button>
                                                        </li>}
                                                    <li>
                                                        <button className="PageFirstBtn pointerNone">
                                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                                        </button>
                                                    </li>
                                                    {this.state.next - 1 != this.state.maxPage ? <li >
                                                        <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                            Next
              </button>
                                                    </li> : <li >
                                                            <button className="PageFirstBtn borderNone" disabled>
                                                                Next
              </button>
                                                        </li>}


                                                    {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
            <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
            {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.demandPlanning ? <DemandPlanningFilter updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} /> : null}
                {/* <Footer /> */}


            </div>
        );
    }
}

export default ForecastHistory;
