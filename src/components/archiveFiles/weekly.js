import React from 'react';
import FilterLoader from '../loaders/filterLoader';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import paramaterIconWeek from "../../assets/change.svg";
import AssortmentModal from "./assortmentModal";
import _ from 'lodash';
import { getForecastStart, getForecastEnd, formatComma } from '../../helper';
import { call, put } from 'redux-saga/effects';
import * as actions from '../../redux/actions';
import downloadIcon from '../../assets/download.svg';
import PreviousAssortmentModal from './previousAssortmentModal';
import GetPreviousAssortmentModal from './getPreviousAssortmenModal';
import moment from 'moment';

class Weekly extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: JSON.parse(sessionStorage.getItem('weeklyGraph')),
            timeAgo: "",

            blue: true,
            green: true,
            purple: true,
            storeData: [],
            assortmentData: [],
            weeklyData: [],
            alert: false,
            loader: false,
            success: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            store: "",
            assortment: "",
            weekly: "",
            storeSearch: "",
            assortmentSearch: "",
            assortmentModal: false,
            assortmentModalAnimation: false,
            // storeerr: false,
            weeklyerr: false,
            assortmenterr: false,
            storeshow: false,
            startDate: "",
            endDate: "",
            startmin: getForecastStart(),
            endmax: getForecastEnd(),
            startDateerr: false,
            endDateerr: false,
            weeklyshow: false,
            chooseStoreerr: false,
            active: "graph",
            tableData: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            no: 1,
            type: 1,
            search: "",
            durationst: "",
            durationend: "",
            assCode: "",
            dev: true,
            downloadUrl: "",
            previousAssort: false,
            selectedAssortment: "",
            getPreviousAssortment: false,
            weeklyMonthly: "WEEKLY",
            lastRunId: ""
        }
    }
    componentWillMount() {
        this.props.dpProgressRequest('WEEKLY');
        // this.statusIntervall = setInterval(() => {
        // this.props.dpProgressRequest('WEEKLY');
        // }, 10000)
    }

    componentWillUnmount() {
        clearInterval(this.statusIntervall);
    }

    componentDidMount() {
        setTimeout(() => {
            var shape = document.getElementsByClassName("mainGraph")[0].childNodes[0].childNodes[0];
            shape.setAttribute("viewBox", "0 0 1100 320");
            shape.setAttribute("height", "400");
        }, 1000)
    }

    componentWillReceiveProps(nextProps) {
        // if (nextProps.demandPlanning.storeDP.isSuccess) {
        //     this.setState({
        //         loader: false,
        //         storeData: nextProps.demandPlanning.storeDP.data.resource
        //     })
        // } else if (nextProps.demandPlanning.storeDP.isError) {
        //     this.setState({
        //         code: nextProps.demandPlanning.storeDP.message.status,
        //         alert: true,
        //         loader: false,
        //         errorCode: nextProps.demandPlanning.storeDP.message.error == undefined ? undefined : nextProps.demandPlanning.storeDP.message.error.errorCode,
        //         errorMessage: nextProps.demandPlanning.storeDP.message.error == undefined ? undefined : nextProps.demandPlanning.storeDP.message.error.errorMessage,
        //     })
        //     // this.props.storeDPRequest();
        // }

        if (nextProps.demandPlanning.assortmentDP.isSuccess) {
            this.setState({
                loader: false,
                assortmentData: nextProps.demandPlanning.assortmentDP.data.resource
            })
        } else if (nextProps.demandPlanning.assortmentDP.isError) {
            this.setState({
                code: nextProps.demandPlanning.assortmentDP.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.demandPlanning.assortmentDP.message.error == undefined ? undefined : nextProps.demandPlanning.assortmentDP.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.assortmentDP.message.error == undefined ? undefined : nextProps.demandPlanning.assortmentDP.message.error.errorMessage,
            })
            myfun();
            function* myfun() {
                yield put(actions.assortmentDPClear());
            }
        }

        if (nextProps.demandPlanning.weeklyDP.isSuccess) {
            this.setState({
                loader: false,
                weeklyData: nextProps.demandPlanning.weeklyDP.data.resource
            })
        } else if (nextProps.demandPlanning.weeklyDP.isError) {
            this.setState({
                code: nextProps.demandPlanning.weeklyDP.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.demandPlanning.weeklyDP.message.error == undefined ? undefined : nextProps.demandPlanning.weeklyDP.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.weeklyDP.message.error == undefined ? undefined : nextProps.demandPlanning.weeklyDP.message.error.errorMessage,
            })
        }

        if (nextProps.demandPlanning.weeklyFor.isSuccess) {
            this.setState({
                loader: false,
                // data: _.sortBy(nextProps.demandPlanning.weeklyFor.data.resource == null ? [] : nextProps.demandPlanning.weeklyFor.data.resource.weeklyGraph, function (node) {
                //     return - (new Date(node.billDate).getTime());
                // }).reverse(),
                // assCode: nextProps.demandPlanning.weeklyFor.data.resource.assortmentCode,
                // durationend: nextProps.demandPlanning.weeklyFor.data.resource.endDate,
                // durationst: nextProps.demandPlanning.weeklyFor.data.resource.startDate
            })
            setTimeout(() => {
                var shape = document.getElementsByClassName("mainGraph")[0].childNodes[0].childNodes[0];;
                shape.setAttribute("viewBox", "0 0 1100 320");
                shape.setAttribute("height", "400");
            }, 10)
            sessionStorage.setItem('weeklyAssortmentCode', nextProps.demandPlanning.weeklyFor.data.resource == null ? "" : nextProps.demandPlanning.weeklyFor.data.resource.assortmentCode)
            sessionStorage.setItem('weeklyStart', nextProps.demandPlanning.weeklyFor.data.resource == null ? "" : nextProps.demandPlanning.weeklyFor.data.resource.startDate)
            sessionStorage.setItem('weeklyEnd', nextProps.demandPlanning.weeklyFor.data.resource == null ? "" : nextProps.demandPlanning.weeklyFor.data.resource.endDate)
            sessionStorage.setItem('weeklyGraph', JSON.stringify(_.sortBy(nextProps.demandPlanning.weeklyFor.data.resource == null ? [] : nextProps.demandPlanning.weeklyFor.data.resource.weeklyGraph, function (node) {
                return - (new Date(node.billDate).getTime());
            }).reverse()))
            sessionStorage.setItem('weeklystatus', nextProps.demandPlanning.weeklyFor.data.resource.status)
            this.props.weeklyForRequest();
            this.props.dpProgressRequest('WEEKLY');
        } else if (nextProps.demandPlanning.weeklyFor.isError) {
            this.setState({
                code: nextProps.demandPlanning.weeklyFor.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.demandPlanning.weeklyFor.message.error == undefined ? undefined : nextProps.demandPlanning.weeklyFor.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.weeklyFor.message.error == undefined ? undefined : nextProps.demandPlanning.weeklyFor.message.error.errorMessage,
            })
            this.props.weeklyForRequest();
        }
        // if (nextProps.demandPlanning.table.isSuccess) {

        //     this.setState({
        //         loader: false,
        //         tableData: nextProps.demandPlanning.table.data.resource == null ? [] : nextProps.demandPlanning.table.data.resource,
        //         prev: nextProps.demandPlanning.table.data.prePage,
        //         current: nextProps.demandPlanning.table.data.currPage,
        //         next: nextProps.demandPlanning.table.data.currPage + 1,
        //         maxPage: nextProps.demandPlanning.table.data.maxPage,
        //     })
        //     this.props.tableRequest();
        // } else if (nextProps.demandPlanning.table.isError) {
        //     this.setState({
        //         code: nextProps.demandPlanning.table.message.status,
        //         alert: true,
        //         loader: false,
        //         errorCode: nextProps.demandPlanning.table.message.error == undefined ? undefined : nextProps.demandPlanning.table.message.error.errorCode,
        //         errorMessage: nextProps.demandPlanning.table.message.error == undefined ? undefined : nextProps.demandPlanning.table.message.error.errorMessage,
        //     })
        //     this.props.tableRequest();
        // }

        if (nextProps.demandPlanning.dpProgress.isSuccess) {
            if (nextProps.demandPlanning.dpProgress.data.resource == null) {
            } else {
                if (nextProps.demandPlanning.dpProgress.data.resource.downloadUrl == "INPROGRESS") {
                    this.setState({
                        downloadUrl: ""
                    })
                    sessionStorage.setItem('weeklyAssortmentCode', nextProps.demandPlanning.dpProgress.data.resource.assortment)
                    sessionStorage.setItem('weeklyStart', nextProps.demandPlanning.dpProgress.data.resource.startDate)
                    sessionStorage.setItem('weeklyEnd', nextProps.demandPlanning.dpProgress.data.resource.endDate)
                    sessionStorage.setItem('weeklyGraph', JSON.stringify(_.sortBy(nextProps.demandPlanning.dpProgress.data.resource.generatedJson, function (node) {
                        return - (new Date(node.billDate).getTime());
                    }).reverse()))
                    sessionStorage.setItem('weeklystatus', nextProps.demandPlanning.dpProgress.data.resource.status)
                    setTimeout(() => {
                        this.props.dpProgressRequest('WEEKLY');

                    }, 5000)

                } else {
                    this.setState({
                        downloadUrl: nextProps.demandPlanning.dpProgress.data.resource.downloadUrl,

                    })
                    if (JSON.parse(sessionStorage.getItem('weeklyGraph')).length == 0) {
                        sessionStorage.setItem('weeklyAssortmentCode', nextProps.demandPlanning.dpProgress.data.resource.assortment)
                        sessionStorage.setItem('weeklyStart', nextProps.demandPlanning.dpProgress.data.resource.startDate)
                        sessionStorage.setItem('weeklyEnd', nextProps.demandPlanning.dpProgress.data.resource.endDate)
                        sessionStorage.setItem('weeklyGraph', JSON.stringify(_.sortBy(nextProps.demandPlanning.dpProgress.data.resource.generatedJson, function (node) {
                            return - (new Date(node.billDate).getTime());
                        }).reverse()))
                        // data: nextProps.demandPlanning.dpProgress.data.resource.generatedJson

                    }
                    sessionStorage.setItem('weeklystatus', nextProps.demandPlanning.dpProgress.data.resource.status)
                    this.props.dpProgressRequest()
                }
            }

        }


        if (nextProps.demandPlanning.getPreviousAssortment.isSuccess) {
            sessionStorage.setItem('weeklyGraph', JSON.stringify(_.sortBy(nextProps.demandPlanning.getPreviousAssortment.data.resource, function (node) {
                return - (new Date(node.billDate).getTime());
            }).reverse()))
            sessionStorage.setItem('weeklystatus', nextProps.demandPlanning.getPreviousAssortment.data.status)
            // sessionStorage.setItem('prevAssortmentResult', JSON.stringify(nextProps.demandPlanning.getPreviousAssortment.data.resource))
            this.setState({
                loader: false
            })
            let payload = {
                frequency: 'WEEKLY',
                startDate: this.state.startDate,
                endDate: this.state.endDate,
                runId: this.state.selectedAssortment
            }
            this.state.assortment != "" ? this.props.downloadPreviousForcastRequest(payload) : null
            this.props.getPreviousAssortmentRequest()

        } else if (nextProps.demandPlanning.getPreviousAssortment.isError) {


            this.setState({
                loader: false,
                alert: true,
                code: nextProps.demandPlanning.getPreviousAssortment.message.status,
                errorCode: nextProps.demandPlanning.getPreviousAssortment.message.error == undefined ? undefined : nextProps.demandPlanning.getPreviousAssortment.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.getPreviousAssortment.message.error == undefined ? undefined : nextProps.demandPlanning.getPreviousAssortment.message.error.errorMessage,
            })
        }

        if (nextProps.demandPlanning.downloadPreviousForcast.isSuccess) {
            this.setState({
                loader: false
            })
            if (nextProps.demandPlanning.downloadPreviousForcast.data.resource == "") {

                this.props.dpProgressRequest('WEEKLY');
            }

        } else if (nextProps.demandPlanning.downloadPreviousForcast.isError) {

            this.setState({
                loader: false,
                alert: true,
                code: nextProps.demandPlanning.downloadPreviousForcast.message.status,
                errorCode: nextProps.demandPlanning.downloadPreviousForcast.message.error == undefined ? undefined : nextProps.demandPlanning.downloadPreviousForcast.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.downloadPreviousForcast.message.error == undefined ? undefined : nextProps.demandPlanning.downloadPreviousForcast.message.error.errorMessage,
            })

        }
        if (nextProps.demandPlanning.getAssortmentHistory.isSuccess) {
            this.setState({ loader: false })
        } else if (nextProps.demandPlanning.getAssortmentHistory.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.demandPlanning.getAssortmentHistory.message.status,
                errorCode: nextProps.demandPlanning.getAssortmentHistory.message.error == undefined ? undefined : nextProps.demandPlanning.getAssortmentHistory.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.getAssortmentHistory.message.error == undefined ? undefined : nextProps.demandPlanning.getAssortmentHistory.message.error.errorMessage,
            })
        }


        if (nextProps.demandPlanning.table.isLoading || nextProps.demandPlanning.assortmentDP.isLoading || nextProps.demandPlanning.weeklyDP.isLoading || nextProps.demandPlanning.downloadPreviousForcast.isLoading
            || nextProps.demandPlanning.weeklyFor.isLoading || nextProps.demandPlanning.getPreviousAssortment.isLoading || nextProps.demandPlanning.getAssortmentHistory.isLoading) {
            this.setState({
                loader: true
            })
        }

    }

    openAssortment(e) {

        if (this.state.selectedAssortment != "" && this.state.selectedAssortment != this.state.lastRunId) {
            let data = {
                no: 1,
                search: "",
                type: 1,
                runId: this.state.selectedAssortment,
                totalCount: 0,
                assortmentCode: "NA",
                startDate: "",
                endDate: ""
            }
            this.setState({
                getPreviousAssortment: !this.state.getPreviousAssortment
            })
            this.props.getPreviousAssortmentRequest(data);
        } else {
            let data = {
                type: 1,
                search: "",
                no: 1,
                totalCount: 0
            }
            this.props.assortmentDPRequest(data);
            this.setState({
                assortmentModal: !this.state.assortmentModal,
                assortmentModalAnimation: !this.state.assortmentModalAnimation
            })
        }
    }
    previousAssortment() {

        let data = {
            type: 1,
            search: "",
            no: 1,
            totalCount: 0,
            year: "CURRENT_FISCAL_YEAR",
            frequency: "WEEKLY",
            startDate: "",
            endDate: "",
            duration: "6_MONTHS"
        }
        this.props.getAssortmentHistoryRequest(data);
        this.setState({
            previousAssort: !this.state.previousAssort,
            assortment: ""
        })
    }

    selectedAssortment(data) {

        this.setState({
            selectedAssortment: data.id,
            startDate: data.startDate,
            endDate: data.endDate,
            previousAssort: !this.state.previousAssort,
            assortment: "",
            lastRunId: data.lastRunId,
            timeAgo: data.timeAgo

        })
        sessionStorage.setItem('lastRunId', data.lastRunId)
    }
    closeAssortment(e) {
        this.setState({
            assortmentModal: !this.state.assortmentModal,
            assortmentModalAnimation: !this.state.assortmentModalAnimation
        })
    }
    closeGetPreviousAssort(e) {
        this.setState({
            getPreviousAssortment: !this.state.getPreviousAssortment,

        })
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    updateAssortment(selected) {
        let e = { target: { id: "" } };
        e.target.id = "assortment";
        this.handleChange(e, selected)
        sessionStorage.setItem('assortmentId', selected)
        if (sessionStorage.getItem('lastRunId') == null) {
            sessionStorage.setItem('lastRunId', "")
        }
    }

    onBtnClick(c) {
        if (c == "blue") {
            this.setState({
                blue: !this.state.blue
            })
        } else if (c == "green") {
            this.setState({
                green: !this.state.green
            })
        } else if (c == "purple") {
            this.setState({
                purple: !this.state.purple
            })
        } else if (c == "dev") {
            this.setState({
                dev: !this.state.dev
            })
        }
    }

    forecast() {


        // this.stores();
        // this.weeks();
        this.startDate();
        this.endDate();
        this.assortment();
        const t = this;


        setTimeout(function () {
            const { assortmenterr, startDateerr, endDateerr } = t.state;

            if (t.state.selectedAssortment == "" || t.state.selectedAssortment == t.state.lastRunId) {

                if (!assortmenterr && !startDateerr && !endDateerr) {
                    let data = {
                        // store: t.state.store,
                        assortment: t.state.assortment,
                        startDate: t.state.startDate,
                        endDate: t.state.endDate
                    }
                    t.setState({
                        downloadUrl: ""
                    })
                    t.props.weeklyForRequest(data);

                }
            } else {
                if (!assortmenterr && !startDateerr && !endDateerr) {
                    let data = {
                        no: 1,
                        search: "",
                        type: 1,
                        runId: t.state.selectedAssortment,
                        totalCount: 0,
                        assortmentCode: t.state.assortment,
                        startDate: t.state.startDate,
                        endDate: t.state.endDate
                    }
                    t.props.getPreviousAssortmentRequest(data)
                }
            }
        }, 1000)


    }

    handleChange(e, data) {
        if (e.target.id == "stores") {
            this.setState({
                store: data,
                chooseStoreerr: false
            },
                () => {
                    this.setState({
                        storeshow: !this.state.storeshow
                    })
                    this.stores();
                }
            )
        } else if (e.target.id == "weeks") {
            this.setState({
                weekly: data
            },
                () => {
                    this.weeks();
                }
            )
        } else if (e.target.id == "assortment") {
            this.setState({
                assortment: data
            },
                () => {
                    this.assortment();
                }
            )
        } else if (e.target.id == "startDate") {
            e.target.placeholder = e.target.value;
            this.setState({
                startDate: e.target.value
            },
                () => {
                    this.startDate();
                }
            )
        } else if (e.target.id == "endDate") {
            e.target.placeholder = e.target.value;
            this.setState({
                endDate: e.target.value
            },
                () => {
                    this.endDate();
                }
            )
        }
    }

    stores() {
        if (
            this.state.store == "" || this.state.store.trim() == "") {
            this.setState({
                storeerr: true
            });
        } else {
            this.setState({
                storeerr: false
            });
        }
    }
    weeks() {
        if (
            this.state.weekly == "" || this.state.weekly.trim() == "") {
            this.setState({
                weeklyerr: true
            });
        } else {
            this.setState({
                weeklyerr: false
            });
        }
    }
    assortment() {
        if (this.state.assortment == "") {

            this.setState({
                assortmenterr: true
            });
        } else {

            this.setState({
                assortmenterr: false
            });
        }
    }
    endDate() {
        if (
            this.state.endDate == "") {
            this.setState({
                endDateerr: true
            });
        } else {
            this.setState({
                endDateerr: false
            });
        }
    }
    startDate() {
        if (this.state.startDate == "") {
            this.setState({
                startDateerr: true
            });
        } else {
            this.setState({
                startDateerr: false
            });
        }
    }

    viewChange(type) {
        // if (type == this.state.active) {


        // } else if (type == "graph") {
        if (type == "graph") {
            setTimeout(() => {
                var shape = document.getElementsByClassName("mainGraph")[0].childNodes[0].childNodes[0];
                shape.setAttribute("viewBox", "0 0 1100 400");
                shape.setAttribute("height", "400");
            }, 10)
        }
        this.setState({
            active: type
        });
        //     if (this.state.store == "" || this.state.assortment == "" || this.state.startDate == "" || this.state.endDate == "") {
        //     } else {
        //         this.stores();
        //         this.startDate();
        //         this.endDate();
        //         this.assortment();
        //         const t = this;
        //         setTimeout(function () {
        //             const { storeerr, assortmenterr, startDateerr, endDateerr } = t.state;
        //             if (!storeerr && !assortmenterr && !startDateerr && !endDateerr) {
        //                 let data = {
        //                     store: t.state.store,
        //                     assortment: t.state.assortment,
        //                     startDate: t.state.startDate,
        //                     endDate: t.state.endDate
        //                 }
        //                 t.setState({
        //                     active: type
        //                 });
        //                 t.props.weeklyForRequest(data);
        //             }
        //         }, 100)
        //     }
        // } else if (type == 'table') {
        //     this.setState({
        //         active: type
        //     });
        //     if (this.state.store == "" || this.state.assortment == "" || this.state.startDate == "" || this.state.endDate == "") {
        //     } else {
        //         let data = {
        //             no: 1,
        //             type: 1,
        //             search: ""
        //         }
        //         this.props.tableRequest(data);
        //     }
        // }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.demandPlanning.table.data.prePage,
                current: this.props.demandPlanning.table.data.currPage,
                next: this.props.demandPlanning.table.data.currPage + 1,
                maxPage: this.props.demandPlanning.table.data.maxPage,
            })
            if (this.props.demandPlanning.table.data.currPage != 0) {
                if (this.state.store == "" || this.state.assortment == "" || this.state.startDate == "" || this.state.endDate == "") {
                } else {
                    let data = {
                        no: this.props.demandPlanning.table.data.currPage - 1,
                        search: this.state.search,
                        type: this.state.type,
                    };
                    this.props.tableRequest(data);
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.demandPlanning.table.data.prePage,
                current: this.props.demandPlanning.table.data.currPage,
                next: this.props.demandPlanning.table.data.currPage + 1,
                maxPage: this.props.demandPlanning.table.data.maxPage,
            })
            if (this.props.demandPlanning.table.data.currPage != this.props.demandPlanning.table.data.maxPage) {
                if (this.state.store == "" || this.state.assortment == "" || this.state.startDate == "" || this.state.endDate == "") {
                } else {
                    let data = {
                        no: this.props.demandPlanning.table.data.currPage + 1,
                        search: this.state.search,
                        type: this.state.type,
                    };
                    this.props.tableRequest(data)
                }
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.demandPlanning.table.data.prePage,
                current: this.props.demandPlanning.table.data.currPage,
                next: this.props.demandPlanning.table.data.currPage + 1,
                maxPage: this.props.demandPlanning.table.data.maxPage,
            })
            if (this.props.demandPlanning.table.data.currPage <= this.props.demandPlanning.table.data.maxPage) {
                if (this.state.store == "" || this.state.assortment == "" || this.state.startDate == "" || this.state.endDate == "") {
                } else {
                    let data = {
                        no: 1,
                        search: this.state.search,
                        type: this.state.type,
                    };
                    this.props.tableRequest(data)
                }
            }

        }
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.store == "" || this.state.assortment == "" || this.state.startDate == "" || this.state.endDate == "") {
        } else {
            this.setState({
                type: 3,
                no: 1
            })
            let data = {
                no: 1,
                search: this.state.search,
                type: 3,
            };
            this.props.tableRequest(data);
        }
    }

    onClearSearch(e) {
        this.setState({
            search: "",
            type: 1,
            no: 1
        });
        let data = {
            no: 1,
            type: 1,
            search: ""
        }
        this.props.tableRequest(data);

    }

    onClear() {
        this.setState({
            assortment: "",
            startDate: "",
            endDate: "",
            selectedAssortment: "",
            lastRunId: ""

        })
        document.getElementById('startDate').placeholder = "Start Date";
        document.getElementById('endDate').placeholder = "End Date";
    }
    onClose() {
        this.setState({
            previousAssort: !this.state.previousAssort,
        })
    }

    render() {

        var { storeSearch, assortmentSearch } = this.state;
        // var resultStore = _.filter(this.state.storeData, function (data) {
        //     return _.startsWith(data.toLowerCase(), storeSearch.toLowerCase());
        // });
        var resultAssortment = _.filter(this.state.assortmentData, function (data) {
            return _.startsWith(data.toLowerCase(), assortmentSearch.toLowerCase());
        });
        return (
            <div className="container-fluid">
                <div className="container_div" id="home">
                    <div className="container-fluid">
                        <div className="container_div" id="">
                            <div className="container-fluid pad-0">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 newContainer">
                                    <div className="replenishment_container weekelyPlanning">
                                        <div className="newHeader">
                                            <div className="col-md-6 pad-0">
                                                <ul className="list_style">
                                                    <li>
                                                        <label className="contribution_mart">
                                                            WEEKLY FORECAST
                                                </label>
                                                    </li>
                                                    <li>
                                                        {/* <p className="master_para">Predicting forecast on weekly basis</p> */}
                                                        <div className="topSubHeading">
                                                            <p>Showing : </p>
                                                            <h4>{this.state.selectedAssortment != "" && this.state.selectedAssortment != this.state.lastRunId ? "Previous Assortment" : "Current Assortment"}{this.state.timeAgo != "" ? "/" + this.state.timeAgo : ""}{this.state.startDate != "" ? "/" + moment(this.state.startDate).format("DD/MMM/YYYY") : ""}{this.state.endDate != "" ? "-" + moment(this.state.endDate).format("DD/MMM/YYYY") : ""}</h4>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="col-md-6 pad-0 weeklyDownload">
                                                <div className="col-md-9 pad-0">
                                                    <h5 className="textRight">Report available for all assortment </h5>
                                                    <p className="textRight fromToData m0">{sessionStorage.getItem('weeklyStart')} <span>to</span> {sessionStorage.getItem('weeklyEnd')}</p>
                                                </div>
                                                <div className="col-md-3 pad-0">
                                                    <div className="textRight m-top-3">
                                                        {this.state.downloadUrl == "" ? <button type="button" className="btnDisabled dowonload-report-btn">Download Report </button> : <button type="button" onClick={() => window.open(`${this.state.downloadUrl}`)} className="dowonload-report-btn">Download Report</button>}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="containerBody">
                                            <div className="weeklyFilters">
                                                <div className="col-md-12 pad-0">
                                                    <div className="col-md-7 col-sm-6 pad-0">
                                                        <div className="previousAssort">
                                                            <label>Looking for previous assortments ?</label><button type="button" onClick={() => this.previousAssortment()}>Click Here</button>
                                                        </div>
                                                        <div className="col-md-12 pad-0 m-top-20">
                                                            <div className="col-md-4 col-xs-12 pad-lft-0">
                                                                <label className="labelGlobal fontWeig600">Assortment</label>
                                                                <div className={this.state.assortmenterr ? "errorBorder weekelyDropdown displayPointer userModalSelect m-top-5" : "displayPointer userModalSelect m-top-5"} onClick={(e) => this.openAssortment(e)}>

                                                                    <label className="limitText100 displayPointer pad-lft-8">{this.state.assortment == "" ? "Choose Assortment" : this.state.assortment}
                                                                    </label>
                                                                    <span className="dropDownIcons">
                                                                        <i className="fa fa-chevron-down"></i>
                                                                    </span>
                                                                </div>
                                                                {this.state.assortmenterr ? <span className="error">Choose Assortment</span> : null}
                                                            </div>
                                                            <div className="col-md-3 col-xs-12 pad-lft-0">
                                                                <label className="labelGlobal fontWeig600">From Date</label>
                                                                <div className="weekelyDropdown m-top-5">
                                                                    <input type="date" min={this.state.startmin} max={this.state.endmax} value={this.state.startDate} id="startDate" onChange={(e) => this.handleChange(e, "xyz")} className={this.state.startDateerr ? "purchaseOrderTextbox errorBorder dateIcon" : "purchaseOrderTextbox dateIcon"} placeholder={this.state.startDate == "" ? "Start Date" : this.state.startDate} />
                                                                </div>
                                                                {this.state.startDateerr ? <span className="error">Choose Start Date</span> : null}
                                                            </div>
                                                            <div className="col-md-3 col-xs-12 pad-lft-0">
                                                                <label className="labelGlobal fontWeig600">To Date</label>
                                                                <div className="weekelyDropdown m-top-5">
                                                                    <input type="date" id="endDate" min={this.state.startDate} max={this.state.endmax} value={this.state.endDate} onChange={(e) => this.handleChange(e, "xyz")} className={this.state.endDateerr ? "purchaseOrderTextbox errorBorder" : "purchaseOrderTextbox"} placeholder={this.state.endDate == "" ? "End Date" : this.state.endDate} />
                                                                </div>
                                                                {this.state.endDateerr ? <span className="error">Choose End Date</span> : null}
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div className="col-md-5 pad-0 weeklyDownload">



                                                    </div>
                                                    {/* <div className="col-md-6 col-sm-6 col-xs-6 pad-0 weekelyDropDowns"> */}
                                                    {/* <div className="col-md-2 col-xs-12 pad-0"> */}

                                                    {/* <div className={this.state.storeerr ? "errorBorder userModalSelect" : "userModalSelect"}>

                                                    <label  onClick={(e) => this.setState({ storeshow: !this.state.storeshow })} className="displayPointer">{this.state.store == "" ? "Choose Store" : this.state.store}
                                                        <span className="dropDownIcons">
                                                            <i className="fa fa-chevron-down"></i>
                                                        </span>
                                                    </label>
                                                    {selectederr ? <img src={errorIcon} className="error_icon_purchase1" /> : null}
                                                    {this.state.storeshow ? <div className="dropdownDiv hiddenDiv">

                                                        <input type="search" className="searchFilterModal" value={this.state.storeSearch} onChange={(e) => this.setState({ storeSearch: e.target.value })} placeholder="Type to Search" />
                                                        <ul className="dropdownFilterOption">
                                                            {resultStore.length == 0 ? null : resultStore.map((data, key) => (<li id="stores" onClick={(e) => this.handleChange(e, `${data}`)} key={key}>
                                                                {data}
                                                            </li>))}
                                                        </ul>

                                                    </div> : null}

                                                </div>
                                                {this.state.storeerr ? <span className="error">Choose Store</span> : null}

                                            </div> */}
                                                    {/* ****************************************************************** */}
                                                    {/* <div className="col-md-2 col-xs-12 pad-0">
                                                    <label className="labelGlobal">Assortment</label>
                                                    <div className={this.state.assortmenterr ? "errorBorder weekelyDropdown displayPointer userModalSelect" : "displayPointer userModalSelect"} onClick={(e) => this.openAssortment(e)}>

                                                        <label className="limitText100 displayPointer pad-lft-8">{this.state.assortment == "" ? "Choose Assortment" : this.state.assortment}
                                                        </label>
                                                        <span className="dropDownIcons">
                                                            <i className="fa fa-chevron-down"></i>
                                                        </span> */}
                                                    {/* ***************************************************************** */}
                                                    {/* {selectederr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}

                                                    {/* <div className="dropdownDiv hiddenDiv">

                                                                    <input type="search" className="searchFilterModal" value={this.state.assortmentSearch} onChange={(e) => this.setState({assortmentSearch: e.target.value})} placeholder="Type to Search" />
                                                                    <ul className="dropdownFilterOption">
                                                                    {resultAssortment.length == 0 ? null : resultAssortment.map((data,key) => (<li  onClick={() => this.setState({assortment: data})}  key={key}>
                                                                {data}
                                                            </li>))}
                                                                    </ul>

                                                                    </div> */}
                                                    {/* *******************************************8 */}
                                                    {/* </div>

                                                    {this.state.assortmenterr ? <span className="error">Choose Assortment</span> : null}
                                                </div> */}
                                                    {/* ****************************************** */}
                                                    {/* <div className="col-md-2 col-xs-12 pad-0 m-rgt-20">

                                                <div onClick={(e) => this.setState({weeklyshow: !this.state.weeklyshow})} className={ this.state.weeklyerr ? "errorBorder userModalSelect chooseWeeks" : "userModalSelect chooseWeeks"}>

                                                    <label className="displayPointer">{this.state.weekly == "" ? "Choose Weeks" : this.state.weekly}
                                                    <span>
                                                            ▾
                                                            </span>
                                                    </label>
                                                    
                                                    {this.state.weeklyshow ? <div className="dropdownDiv hiddenDiv">
                                                        <ul className="dropdownFilterOption">
                                                            {this.state.weeklyData.length == 0 ? null : this.state.weeklyData.map((data, key) => (<li  id="weeks" onClick={(e) => this.handleChange(e, `${data}`)} key={key}>
                                                                {data}
                                                            </li>))}
                                                        </ul>

                                                    </div> : null}
                                                </div>
                                                {this.state.weeklyerr ? <span className="error">Choose Months</span> : null}
                                            </div> */}
                                                    {/* *************************************************** */}
                                                    {/* <div className="col-md-2 col-xs-12 pad-0 ">
                                                    <label className="labelGlobal">From Date</label>
                                                    <div className="weekelyDropdown">
                                                        <input type="date" min={this.state.startmin} max={this.state.endmax} id="startDate" onChange={(e) => this.handleChange(e, "xyz")} className={this.state.startDateerr ? "purchaseOrderTextbox errorBorder dateIcon" : "purchaseOrderTextbox dateIcon"} placeholder="Start Date" />
                                                    </div>
                                                    {this.state.startDateerr ? <span className="error">Choose Start Date</span> : null}
                                                </div>
                                                <div className="col-md-2 col-xs-12 pad-0">
                                                    <label className="labelGlobal">To Date</label>
                                                    <div className="weekelyDropdown">
                                                        <input type="date" id="endDate" min={this.state.startDate} max={this.state.endmax} onChange={(e) => this.handleChange(e, "xyz")} className={this.state.endDateerr ? "purchaseOrderTextbox errorBorder" : "purchaseOrderTextbox"} placeholder="End Date" />
                                                    </div>
                                                    {this.state.endDateerr ? <span className="error">Choose End Date</span> : null}
                                                </div> */}
                                                    {/* ************************************************* */}
                                                    {/* </div> */}
                                                </div>

                                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 tabDemandMain">
                                                    <div className="col-md-4 runForcast blueButton pad-0">
                                                        <button className="m-rgt-20" onClick={() => this.forecast()} type="button">GENERATE REPORT</button>
                                                        {this.state.startDate == "" && this.state.endDate == "" && this.state.assortment == "" ? <label className="pointerNone textDisable resetBtn">CLEAR</label>
                                                            : <label onClick={() => this.onClear()} className="displayPointer resetBtn">CLEAR</label>}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekBottom">
                                                <div className="col-md-12 pad-0">
                                                    <div className="tableFilters">
                                                        <div className="col-md-6 mar-bot-20 pad-lft-0">
                                                            {sessionStorage.getItem('weeklyAssortmentCode') == "" ? null :
                                                                <div>
                                                                    <h3>FORECAST REPORT DETAILS</h3>
                                                                    {/* <p className="displayInline pad-right-8">For Assortment:</p> */}
                                                                    <label className="assortmentDetail m-top-10">{sessionStorage.getItem('weeklyAssortmentCode')}</label>
                                                                </div>}
                                                            {sessionStorage.getItem('weeklyStart') != "" ? <label className="textRight fromToData">{sessionStorage.getItem('weeklyStart')}<span>to</span> {sessionStorage.getItem('weeklyEnd')}</label> : null}

                                                        </div>
                                                    </div>
                                                    <div className="col-md-6 textRight pad-0 generalTabs">
                                                        <button type="button" onClick={() => this.viewChange('graph')} className={this.state.active == "graph" ? "tabBtnClk pointerNone" : "tabBtn  borderNoneAll"}>View Prediction Graph</button>
                                                        <button type="button" onClick={() => this.viewChange('table')} className={this.state.active == "table" ? "tabBtnClk pointerNone" : "tabBtn  borderNoneAll"}>View Tabular data</button>
                                                    </div>
                                                </div>



                                                {this.state.active == "graph" ?
                                                    <div className="graphHeadingMain displayInline">
                                                        {/* <h5>PREDICTION GRAPH</h5> */}

                                                        <div className="tableFilters">
                                                            {/* <div className="col-md-6 mar-bot-20 pad-lft-0">
                                                        {sessionStorage.getItem('weeklyAssortmentCode') == "" ? null :
                                                            <div>
                                                                <h3>FORECAST REPORT DETAILS</h3>
                                                                <p className="displayInline pad-right-8">For Assortment:</p>
                                                                <label className="assortmentDetail">{sessionStorage.getItem('weeklyAssortmentCode')}</label>
                                                            </div>}
                                                    </div> */}
                                                            {/* <div className="col-md-6 textRight m-top-10 mar-bot-20">
                                                        {this.state.downloadUrl == "" ? <button type="button" className="btnDisabled dowonload-report-btn">Download Report</button> : <button type="button" onClick={() => window.open(`${this.state.downloadUrl}`)} className="dowonload-report-btn">Download Report</button>}
                                                    </div> */}
                                                        </div>

                                                        <div className="grapnWeekMonth m-top-10 width100">

                                                            <div className="graphCheckBox m-top-30">
                                                                <ul className="m-rgt-50">
                                                                    {/* <li>
                                                            <div className="checkSeperate checkdev">
                                                                <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.dev} onClick={(e) => this.onBtnClick("dev")} /><p>Deviation</p><span className="checkmark1"></span> </label>
                                                            </div> </li> */}
                                                                    {sessionStorage.getItem('weeklystatus') != "previous" ? <li>
                                                                        <div className="checkSeperate checkGreen">
                                                                            <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.green} onChange={(e) => this.onBtnClick("green")} /><p>Actual Sales</p><span className="checkmark1"></span> </label>
                                                                        </div> </li> : null}
                                                                    <li><div className="checkSeperate checkBlue">
                                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.blue} onChange={(e) => this.onBtnClick("blue")} /><p>Predicted Sales</p><span className="checkmark1"></span> </label>
                                                                    </div></li>
                                                                    {sessionStorage.getItem('weeklystatus') != "previous" ? <li><div className="checkSeperate checkPurple">
                                                                        <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.purple} onChange={(e) => this.onBtnClick("purple")} /><p>Budgeted Sales</p><span className="checkmark1"></span> </label>
                                                                    </div></li> : null}
                                                                </ul>
                                                            </div>
                                                            <div className="col-md-12 demandPlanningGraph">
                                                                {sessionStorage.getItem('weeklystatus') != "previous" ? JSON.parse(sessionStorage.getItem('weeklyGraph')).length == 0 ?
                                                                    <div className="noDataFound"><span>NO FORECAST FOUND FOR THIS RANGE</span></div>
                                                                    :
                                                                    <div className="mainGraph">
                                                                        <LineChart width={1100} height={300} data={JSON.parse(sessionStorage.getItem('weeklyGraph'))}
                                                                            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                                                                        >
                                                                            <XAxis tick={{ angle: 270 }} textAnchor="end" dataKey="billDate" />
                                                                            <YAxis />
                                                                            {/* <CartesianGrid strokeDasharray="3 3" /> */}
                                                                            <Tooltip formatter={(value) => formatComma(value)} />
                                                                            {/* {this.state.dev ? <Line type="linear" dataKey="deviation" stroke="#8884d8" strokeDasharray="2 2"/> : null} */}
                                                                            {this.state.green ? <Line type="linear" dataKey="actual" fill="#7ed321" stroke="#7ed321" activeDot={{ r: 8 }} /> : null}
                                                                            {this.state.blue ? <Line type="linear" dataKey="predicted" shape='square' fill="#009aff" stroke="#009aff" activeDot={{ r: 5 }} /> : null}
                                                                            {this.state.purple ? <Line type="linear" dataKey="budgeted" shape='triangle' fill="#9013fe" stroke="#9013fe" activeDot={{ r: 2 }} /> : null}
                                                                        </LineChart>
                                                                    </div> : JSON.parse(sessionStorage.getItem('weeklyGraph')) == undefined || JSON.parse(sessionStorage.getItem('weeklyGraph')).length == 0 ?
                                                                        <div className="noDataFound"><span>NO FORECAST FOUND FOR THIS RANGE</span></div>
                                                                        :
                                                                        <div className="mainGraph">
                                                                            <LineChart width={1100} height={300} data={JSON.parse(sessionStorage.getItem('weeklyGraph'))}
                                                                                margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                                                                            >
                                                                                <XAxis tick={{ angle: 270 }} textAnchor="end" dataKey="billdate" />
                                                                                <YAxis />
                                                                                {/* <CartesianGrid strokeDasharray="3 3" /> */}
                                                                                <Tooltip formatter={(value) => formatComma(value)} />
                                                                                {/* {this.state.dev ? <Line type="linear" dataKey="deviation" stroke="#8884d8" strokeDasharray="2 2"/> : null} */}
                                                                                {/* {this.state.green ? <Line type="linear" dataKey="actual" fill="#7ed321" stroke="#7ed321" activeDot={{ r: 8 }} /> : null} */}
                                                                                {this.state.blue ? <Line type="linear" dataKey="predicted" shape='square' fill="#009aff" stroke="#009aff" activeDot={{ r: 5 }} /> : null}
                                                                                {/* {this.state.purple ? <Line type="linear" dataKey="budgeted" shape='triangle' fill="#9013fe" stroke="#9013fe" activeDot={{ r: 2 }} /> : null} */}
                                                                            </LineChart>
                                                                        </div>}
                                                                {/* ----------------------------------Scroll Graph Code--------------------------------------                                                 */}
                                                                {/* <div className="yAxisGraph">
                                                    <LineChart width={this.state.data.length*73 > 1100 ? this.state.data.length*73 : 1100} height={300} data={this.state.data}
                                                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                                                        <XAxis dataKey="billDate" />
                                                        <YAxis />
                                                        {this.state.dev ? <Line type="linear" dataKey="deviation" stroke="#8884d8" strokeDasharray="2 2"/> : null}
                                                        {this.state.green ? <Line type="linear" dataKey="actual" fill="#7ed321" stroke="#7ed321" activeDot={{ r: 8 }} /> : null}
                                                            {this.state.blue ? <Line type="linear" dataKey="predicted" shape='square' fill="#009aff" stroke="#009aff" activeDot={{ r: 5 }} /> : null}
                                                            {this.state.purple ? <Line type="linear" dataKey="budgeted" shape='triangle' fill="#9013fe" stroke="#9013fe" activeDot={{ r: 2 }} /> : null}
                                                    </LineChart>
                                                    </div> */}

                                                                {/* --------------------------------------Scroll Graph Code end------------------------------------                                                     */}
                                                                {/* <button type="button" onClick={(e) => this.onBtnClick("blue")}>Blue</button>
                                                    <button type="button" onClick={(e) => this.onBtnClick("green")}>Green</button>
                                                    <button type="button" onClick={(e) => this.onBtnClick("red")}>Red</button> */}
                                                                <br />
                                                                <br />
                                                                <br />
                                                                <br />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                                            <div className="tableFilters">
                                                                {/* <div className="col-md-6 mar-bot-20 pad-lft-0">
                                                            {sessionStorage.getItem('weeklyAssortmentCode') == "" ? null :
                                                                <div>
                                                                    <h3>FORECAST REPORT DETAILS</h3>
                                                                    <p className="displayInline pad-right-8">For Assortment:</p>
                                                                    <label className="assortmentDetail">{sessionStorage.getItem('weeklyAssortmentCode')}</label>
                                                                </div>}
                                                        </div> */}

                                                                {/* <div className="col-md-6 textRight mar-bot-20">
                                                            {this.state.downloadUrl == "" ? <button type="button" className="btnDisabled dowonload-report-btn">Download Report</button> : <button type="button" onClick={() => window.open(`${this.state.downloadUrl}`)} className="dowonload-report-btn">Download Report</button>}
                                                        </div> */}
                                                            </div>





                                                            <div className="scrollableOrgansation tableGeneric tableBorder tableHeadFixed ">
                                                                <table className="table UserManageTable tableOddColor">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <label>Date</label>
                                                                            </th>
                                                                            <th>
                                                                                <label>Actual Sales</label>
                                                                            </th>
                                                                            <th>
                                                                                <label>Predicted Sales</label>
                                                                            </th>
                                                                            <th>
                                                                                <label>Deviation</label>
                                                                            </th>
                                                                            <th>
                                                                                <label>Budgeted Sales</label>
                                                                            </th>

                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>

                                                                        {JSON.parse(sessionStorage.getItem('weeklyGraph')).length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : JSON.parse(sessionStorage.getItem('weeklyGraph')).map((data, key) => (<tr key={key}>
                                                                            <td>
                                                                                <label>
                                                                                    {data.billDate}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.actual}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.predicted}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label className="textRed">
                                                                                    {data.deviation}
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <label>
                                                                                    {data.budgeted}
                                                                                </label>
                                                                            </td>



                                                                        </tr>))}


                                                                    </tbody>

                                                                </table>
                                                            </div>

                                                        </div>



                                                        {/* <div className="container-fluid">
                                                    <div className="pagerDiv">
                                                        <ul className="list-inline pagination">
                                                            <li >
                                                                <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                                                    First
                  </button>
                                                            </li>
                                                            {this.state.prev != 0 ? <li >
                                                                <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                                    Prev
                  </button>
                                                            </li> : <li >
                                                                    <button className="PageFirstBtn" disabled>
                                                                        Prev
                  </button>
                                                                </li>}
                                                            <li>
                                                                <button className="PageFirstBtn pointerNone">
                                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                                </button>
                                                            </li>
                                                            {this.state.next - 1 != this.state.maxPage ? <li >
                                                                <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                                    Next
                  </button>
                                                            </li> : <li >
                                                                    <button className="PageFirstBtn borderNone" disabled>
                                                                        Next
                  </button>
                                                                </li>}


                                                            {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />}
                                                        </ul>
                                                    </div>
                                                </div> */}
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {/* <Footer /> */}
                {this.state.previousAssort ? <PreviousAssortmentModal {...this.state} {...this.props} checkedData={this.state.selectedAssortment} previousAssortment={() => this.previousAssortment()} selectedAssortment={(data) => this.selectedAssortment(data)} weeklyMonthly={this.state.weeklyMonthly} onClose={() => this.onClose()} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.assortmentModal ? <AssortmentModal store={this.state.store} assortment={this.state.assortment} updateAssortment={(e) => this.updateAssortment(e)}  {...this.props} assortmentModalAnimation={this.state.assortmentModalAnimation} openAssortment={(e) => this.closeAssortment(e)} /> : null}
                {this.state.getPreviousAssortment ? <GetPreviousAssortmentModal store={this.state.store} assortment={this.state.assortment} updateAssortment={(e) => this.updateAssortment(e)} {...this.props} openAssortment={(e) => this.closeGetPreviousAssort(e)} selectedAssortment={this.state.selectedAssortment} /> : null}


            </div>

        );
    }
}

export default Weekly;