import React from 'react';
import scale from "../../../assets/scale.svg";
import createIcon from "../../../assets/create.svg";
import addIcon from '../../../assets/noun-plus.svg';
import editIcon from '../../../assets/removeIcon.svg';
import up from '../../../assets/up.svg';
import down from '../../../assets/down.svg';
import CreateNewFestivalModal from '../administration/changeSetting/createNewFestivalModal';
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from '../loaders/requestSuccess';
import RequestError from '../loaders/requestError';
import ConfirmationModalFestival from '../administration/changeSetting/confirmationModalFestival';
export default class FestivalSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            configFestival: [],
            customFestival: [],
            defaultFestival: [],
            signState: [],
            festivalDate: "",
            festivalName: "",
            beforeFromDate: "",
            beforeToDate: "",
            afterFromDate: "",
            afterToDate: "",
            beforeFromDateerr: false,
            beforeToDateerr: false,
            afterFromDateerr: false,
            afterToDateerr: false,
            dateSelect: false,
            toolTip: { "left": 0, "top": 0 },
            minDate: "",
            maxDate: "",
            lastArrayChecked: [],
            configuredName: [],
            lastFestivalArray: [],
            falseState: [],
            defaultFestivalState: [],
            years: [],
            disableYear: false,
            yearValue: "",
            financialYear: "",
            dataArray: [],
            loader: false,
            errorMessage: "",
            errorCode: "",
            success: false,
            successMessage: "",
            alert: false,
            code: "",
            getStartDay: "",
            startMonth: "",
            getEndDay: "",
            endMonth: "",
            lastAddedArray: [],
            festivalDateDay: "",
            festivalDateMonth: "",
            customTrue: [],
            recentlyAdded: [],
            name: "",
            resetAndDelete: false,
            headerMsg: "",
            paraMsg: "",
            clickedId: null,
            dateFestival: "",
            yearsCurrent: "",
            truestate: false
        }
    }
    componentDidMount() {
        var date = new Date, years = this.state.years, year = date.getFullYear(), minDate = this.state.minDate,
            maxDate = this.state.maxDate;
        this.state.yearValue = date.getFullYear(), this.state.yearsCurrent = date.getFullYear()
        var nextYear = date.getFullYear() + 1
        this.state.financialYear = this.state.yearValue + '-' + nextYear
        if (year == this.state.yearValue) {
            minDate = year + '-' + '04-01'
            maxDate = nextYear + '-' + '03-30'
        }
        document.addEventListener('mousedown', this.handleClickOutside);
        for (var i = year; i < year + 6; i++) {
            years.push(i);
            years.sort()
        }
        year--
        for (var i = 0; i < 5; i++) {
            years.push(year--);
            years.sort()
        }
        let data = {
            year: this.state.financialYear
        }
        this.props.allFestivalRequest(data)
        this.setState({
            years,
            minDate,
            maxDate
        })
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.administration.allFestival.isSuccess) {
            if (nextProps.administration.allFestival.data.resource != null) {
                let configFestival = nextProps.administration.allFestival.data.resource["configFestival"]
                let defaultFestival = nextProps.administration.allFestival.data.resource["defaultFestival"]
                let customFest = nextProps.administration.allFestival.data.resource["customFestival"] == null ? [] : nextProps.administration.allFestival.data.resource["customFestival"]
                let lastArrayChecked = this.state.lastArrayChecked
                let lastFestivalArray = this.state.lastFestivalArray
                let falseState = this.state.falseState
                let lastAddedArray = this.state.lastAddedArray
                let customTrue = this.state.customTrue
                let configuredName = this.state.configuredName
                if (configFestival != null) {
                    for (var i = 0; i < configFestival.length; i++) {
                        if (configFestival[i].isChecked == "true" && configFestival[i].festivalType == "default") {
                            for (var j = 0; j < defaultFestival.length; j++) {
                                if (configFestival[i].festivalName == defaultFestival[j].festivalName) {
                                    defaultFestival.splice(j, 1)
                                }
                            }
                            if (!lastFestivalArray.includes(configFestival[i].festivalName)) {
                                lastFestivalArray.push(configFestival[i].festivalName)
                                let data = {
                                    festivalName: configFestival[i].festivalName,
                                    festivalDate: configFestival[i].festivalDate,
                                    afterImpact: {
                                        startDate: configFestival[i].afterImpact.startDate,
                                        endDate: configFestival[i].afterImpact.endDate
                                    },
                                    beforeImpact: {
                                        startDate: configFestival[i].beforeImpact.startDate,
                                        endDate: configFestival[i].beforeImpact.endDate
                                    },
                                    isChecked: configFestival[i].isChecked,
                                    festivalType: configFestival[i].festivalType
                                }
                                configuredName.push(data)
                                lastArrayChecked.push(data)
                            }
                        }
                    }
                    if (customFest != null) {
                        for (var j = 0; j < customFest.length; j++) {
                            if (customFest[j].isChecked == "false") {
                                if (!falseState.includes(customFest[j].festivalName)) {
                                    falseState.push(customFest[j].festivalName)
                                    let data = {
                                        festivalName: customFest[j].festivalName,
                                        festivalDate: customFest[j].festivalDate,
                                        afterImpact: {
                                            startDate: customFest[j].afterImpact.startDate,
                                            endDate: customFest[j].afterImpact.endDate
                                        },
                                        beforeImpact: {
                                            startDate: customFest[j].beforeImpact.startDate,
                                            endDate: customFest[j].beforeImpact.endDate
                                        },
                                        isChecked: customFest[j].isChecked,
                                        festivalType: customFest[j].festivalType
                                    }
                                    lastAddedArray.push(data)
                                }
                            }
                            if (customFest[j].isChecked == "true") {
                                if (!lastFestivalArray.includes(customFest[j].festivalName)) {
                                    lastFestivalArray.push(customFest[j].festivalName)
                                    let data = {
                                        festivalName: customFest[j].festivalName,
                                        festivalDate: customFest[j].festivalDate,
                                        afterImpact: {
                                            startDate: customFest[j].afterImpact.startDate,
                                            endDate: customFest[j].afterImpact.endDate
                                        },
                                        beforeImpact: {
                                            startDate: customFest[j].beforeImpact.startDate,
                                            endDate: customFest[j].beforeImpact.endDate
                                        },
                                        isChecked: customFest[j].isChecked,
                                        festivalType: customFest[j].festivalType
                                    }
                                    configuredName.push(data)
                                    customTrue.push(data)
                                }
                            }
                        }
                    }
                }
                this.setState({
                    customFest,
                    customTrue,
                    loader: false,
                    lastAddedArray,
                    configuredName,
                    lastFestivalArray,
                    falseState,
                    lastArrayChecked,
                    configFestival: nextProps.administration.allFestival.data.resource["configFestival"] == null ? [] : nextProps.administration.allFestival.data.resource["configFestival"],
                    defaultFestival,
                    defaultFestivalState: nextProps.administration.allFestival.data.resource["defaultFestival"] == null ? [] : [...nextProps.administration.allFestival.data.resource["defaultFestival"]],
                    customFestival: nextProps.administration.allFestival.data.resource["customFestival"] == null ? [] : [...nextProps.administration.allFestival.data.resource["customFestival"]]
                })
            }
            this.props.allFestivalClear()
        } else if (nextProps.administration.allFestival.isError) {
            this.setState({
                errorMessage: nextProps.administration.allFestival.message.error == undefined ? undefined : nextProps.administration.allFestival.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.allFestival.message.status,
                errorCode: nextProps.administration.allFestival.message.error == undefined ? undefined : nextProps.administration.allFestival.message.error.errorCode
            })
            this.props.createFestivalClear();
        }
        if (nextProps.administration.createFestival.isSuccess) {
            this.setState({
                successMessage: nextProps.administration.createFestival.data.message,
                loader: false,
                success: true,
            })
            let year = this.state.yearValue + 1
            let bothYear = this.state.yearValue + '-' + year
            let data = {
                year: bothYear
            }
            this.props.allFestivalRequest(data)
            this.props.createFestivalClear()
        } else if (nextProps.administration.createFestival.isError) {
            this.setState({
                errorMessage: nextProps.administration.createFestival.message.error == undefined ? undefined : nextProps.administration.createFestival.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.administration.createFestival.message.status,
                errorCode: nextProps.administration.createFestival.message.error == undefined ? undefined : nextProps.administration.createFestival.message.error.errorCode
            })
            this.props.createFestivalClear();
        }

        if (nextProps.administration.createFestival.isLoading || nextProps.administration.allFestival.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    openCreateNew() {
        this.setState({
            createNew: true
        })
    }
    closeModal() {
        this.setState({
            createNew: false
        })
    }
    dateToolTip(name, festivalDate, e) {
        var id = e.currentTarget.id
        var leftTop = {}
        var img = document.getElementById(id)
        var pos = img.getBoundingClientRect();
        leftTop.top = pos.top + 80;
        leftTop.left = pos.left;
        leftTop.bottom = pos.bottom
        var configFestival = this.state.configFestival
        var defaultFestival = this.state.defaultFestival
        let customFest = this.state.customFest
        for (var i = 0; i < configFestival.length; i++) {
            if (name == configFestival[i].festivalName && festivalDate == configFestival[i].festivalDate) {
                if (configFestival[i].isChecked == "true") {
                    this.deleteArrayConfirmation(name)
                } else {
                    this.setState({
                        signState: configFestival[i].festivalName,
                        beforeFromDate: configFestival[i].beforeImpact.startDate,
                        beforeToDate: configFestival[i].beforeImpact.endDate,
                        afterFromDate: configFestival[i].afterImpact.startDate,
                        afterToDate: configFestival[i].afterImpact.endDate,
                        festivalDate: configFestival[i].festivalDate,
                        dateSelect: !this.state.dateSelect,
                        dateFestival: festivalDate,
                        toolTip: leftTop,
                        festivalName: name,
                        clickedId: id
                    })
                }
            }
        }
        for (var i = 0; i < defaultFestival.length; i++) {
            if (name == defaultFestival[i].festivalName && festivalDate == defaultFestival[i].festivalDate) {
                if (defaultFestival[i].isChecked == "true") {
                    this.deleteArrayConfirmation(name)
                } else {
                    this.setState({
                        signState: defaultFestival[i].festivalName,
                        beforeFromDate: defaultFestival[i].beforeImpact.startDate,
                        beforeToDate: defaultFestival[i].beforeImpact.endDate,
                        afterFromDate: defaultFestival[i].afterImpact.startDate,
                        afterToDate: defaultFestival[i].afterImpact.endDate,
                        festivalDate: defaultFestival[i].festivalDate,
                        dateFestival: festivalDate,
                        dateSelect: !this.state.dateSelect,
                        toolTip: leftTop,
                        festivalName: name,
                        clickedId: id
                    })
                }
            }
        }
        if (customFest != "" || customFest != null) {
            for (var j = 0; j < customFest.length; j++) {
                if (name == customFest[j].festivalName && festivalDate == customFest[j].festivalDate) {
                    if (customFest[j].isChecked == "true") {
                        this.deleteArrayConfirmation(name)
                    } else {
                        this.setState({
                            signState: customFest[j].festivalName,
                            beforeFromDate: customFest[j].beforeImpact.startDate,
                            beforeToDate: customFest[j].beforeImpact.endDate,
                            afterFromDate: customFest[j].afterImpact.startDate,
                            afterToDate: customFest[j].afterImpact.endDate,
                            festivalDate: customFest[j].festivalDate,
                            dateSelect: !this.state.dateSelect,
                            dateFestival: festivalDate,
                            toolTip: leftTop,
                            festivalName: name,
                            clickedId: id

                        })
                    }
                }
            }
        }
        this.setState({
            beforeFromDateerr: false,
            beforeToDateerr: false,
            afterFromDateerr: false,
            afterToDateerr: false,
        })
        var tooltip = document.getElementById("toolTipPos")
        tooltip.style.bottom = leftTop.bottom + 290
        var divHeight = document.getElementById("divHeight").clientHeight
        if (parseInt(tooltip.style.bottom) > divHeight) {
            leftTop.top -= 320
            setTimeout(() => {
                document.getElementById("toolTipPos").classList.add("tooltiparrow")
            }, 10)
        } else {
            document.getElementById("toolTipPos").classList.remove("tooltiparrow")
        }
        document.body.addEventListener('scroll', this.scrollEvent)
    }
    scrollEvent = () => {
        var img = document.getElementById(this.state.clickedId)
        if (document.getElementById(this.state.clickedId) != null) {
            var pos = img.getBoundingClientRect();
            var leftTop = {}
            var tooltip = document.getElementById("toolTipPos")
            var divHeight = document.getElementById("divHeight").clientHeight
            tooltip.style.bottom += 260
            if (parseInt(tooltip.style.bottom) > divHeight) { leftTop.top = pos.top - 270; }
            else { leftTop.top = pos.top + 80 }
            leftTop.left = pos.left;
            this.setState({ toolTip: leftTop })
        }
    }
    // ________________________date set for configuartion festival___________________

    handleChange(e, festivalId) {
        if (e.target.id == "beforeFromDate") {
            let defaultFestival = this.state.defaultFestival
            for (var i = 0; i < defaultFestival.length; i++) {
                if (festivalId == defaultFestival[i].festivalName) {
                    this.setState({
                        beforeFromDate: e.target.value,
                    }, () => {
                        this.beforeFromDateError()
                    })
                }
            }
            let customFest = this.state.customFest
            for (var j = 0; j < customFest.length; j++) {
                if (festivalId == customFest[j].festivalName) {
                    this.setState({
                        beforeFromDate: e.target.value,
                    }, () => {
                        this.beforeFromDateError()
                    })
                }
            }
            let configFestival = this.state.configFestival
            for (var k = 0; k < configFestival.length; k++) {
                if (festivalId == configFestival[k].festivalName) {
                    this.setState({
                        beforeFromDate: e.target.value,
                    }, () => {
                        this.beforeFromDateError()
                    })
                }
            }
        } else if (e.target.id == "beforeToDate") {
            let defaultFestival = this.state.defaultFestival
            for (var i = 0; i < defaultFestival.length; i++) {
                if (festivalId == defaultFestival[i].festivalName) {
                    this.setState({
                        beforeToDate: e.target.value,
                    }, () => {
                        this.beforeToDataError()
                    })
                }
            }
            let customFest = this.state.customFest
            for (var j = 0; j < customFest.length; j++) {
                if (festivalId == customFest[j].festivalName) {
                    this.setState({
                        beforeToDate: e.target.value,
                    }, () => {
                        this.beforeToDataError()
                    })
                }
            }
            let configFestival = this.state.configFestival
            for (var k = 0; k < configFestival.length; k++) {
                if (festivalId == configFestival[k].festivalName) {
                    this.setState({
                        beforeToDate: e.target.value,
                    }, () => {
                        this.beforeToDataError()
                    })
                }
            }
        } else if (e.target.id == "afterFromDate") {
            let defaultFestival = this.state.defaultFestival
            for (var i = 0; i < defaultFestival.length; i++) {
                if (festivalId == defaultFestival[i].festivalName) {
                    this.setState({
                        afterFromDate: e.target.value,
                    }, () => {
                        this.afterFromDateError()
                    })
                }
            }
            let customFest = this.state.customFest
            for (var j = 0; j < customFest.length; j++) {
                if (festivalId == customFest[j].festivalName) {
                    this.setState({
                        afterFromDate: e.target.value,
                    }, () => {
                        this.afterFromDateError()
                    })
                }
            }
            let configFestival = this.state.configFestival
            for (var k = 0; k < configFestival.length; k++) {
                if (festivalId == configFestival[k].festivalName) {
                    this.setState({
                        afterFromDate: e.target.value,
                    }, () => {
                        this.afterFromDateError()
                    })
                }
            }
        } else if (e.target.id == "afterToDate") {
            let defaultFestival = this.state.defaultFestival
            for (var i = 0; i < defaultFestival.length; i++) {
                if (festivalId == defaultFestival[i].festivalName) {
                    this.setState({
                        afterToDate: e.target.value,
                    }, () => {
                        this.afterToDataError()
                    })
                }
            }
            let customFest = this.state.customFest
            for (var j = 0; j < customFest.length; j++) {
                if (festivalId == customFest[j].festivalName) {
                    this.setState({
                        afterToDate: e.target.value,
                    }, () => {
                        this.afterToDataError()
                    })
                }
            }
            let configFestival = this.state.configFestival
            for (var k = 0; k < configFestival.length; k++) {
                if (festivalId == configFestival[k].festivalName) {

                    this.setState({
                        afterToDate: e.target.value,
                    }, () => {
                        this.afterToDataError()
                    })
                }
            }
        }
    }

    beforeFromDateError() {
        if (this.state.beforeFromDate == "") {
            this.setState({
                beforeFromDateerr: true
            })
        } else {
            this.setState({
                beforeFromDateerr: false
            })
        }
    }

    beforeToDataError() {
        if (this.state.beforeToDate == "") {
            this.setState({
                beforeToDateerr: true
            })
        } else {
            this.setState({
                beforeToDateerr: false
            })
        }
    }

    afterFromDateError() {
        if (this.state.afterFromDate == "") {
            this.setState({
                afterFromDateerr: true
            })
        } else {
            this.setState({
                afterFromDateerr: false
            })
        }
    }

    afterToDataError() {
        if (this.state.afterToDate == "") {
            this.setState({
                afterToDateerr: true
            })
        } else {
            this.setState({
                afterToDateerr: false
            })
        }
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
        document.body.removeEventListener('scroll', this.scrollEvent)
    }
    setWrapperRef(node) {
        this.wrapperRef = node;
    }
    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                dateSelect: false,
            })
        }
    }
    saveFestival() {
        this.beforeFromDateError()
        this.beforeToDataError()
        this.afterFromDateError()
        this.afterToDataError()
        if (this.state.beforeFromDate != "") {
            this.beforeFromDateError();
        }
        if (this.state.afterFromDate != "") {
            this.afterFromDateError()
        }
        setTimeout(() => {
            const { beforeToDateerr, beforeFromDateerr, afterFromDateerr, afterToDateerr } = this.state;
            if (!beforeToDateerr && !beforeFromDateerr && !afterFromDateerr && !afterToDateerr) {
                let configFestival = [...this.state.configFestival]
                let festivalName = this.state.festivalName
                let lastArrayChecked = this.state.lastArrayChecked
                let lastFestivalArray = this.state.lastFestivalArray
                let customTrue = this.state.customTrue
                let recentlyAdded = this.state.recentlyAdded
                let lastAddedArray = this.state.lastAddedArray
                for (var i = 0; i < configFestival.length; i++) {
                    if (festivalName == configFestival[i].festivalName && configFestival[i].type == "default" && this.state.dateFestival == configFestival[i].festivalDate) {
                        configFestival[i].afterImpact.startDate = this.state.afterFromDate
                        configFestival[i].afterImpact.endDate = this.state.afterToDate
                        configFestival[i].beforeImpact.startDate = this.state.beforeFromDate
                        configFestival[i].beforeImpact.endDate = this.state.beforeToDate
                        configFestival[i].isChecked = "true"
                        if (!lastFestivalArray.includes(configFestival[i].festivalName)) {
                            lastFestivalArray.push(configFestival[i].festivalName)
                            let data = {
                                festivalName: configFestival[i].festivalName,
                                afterImpact: {
                                    startDate: configFestival[i].afterImpact.startDate,
                                    endDate: configFestival[i].afterImpact.endDate
                                },
                                beforeImpact: {
                                    startDate: configFestival[i].beforeImpect.startDate,
                                    endDate: configFestival[i].beforeImpect.endDate
                                },
                                festivalDate: configFestival[i].festivalDate,
                                isChecked: "true",
                                festivalType: configFestival[i].festivalType
                            }
                            lastArrayChecked.push(data)
                            recentlyAdded.push(data)
                        }
                    }
                }

                let defaultFestival = [...this.state.defaultFestival]
                for (var j = 0; j < defaultFestival.length; j++) {
                    if (festivalName == defaultFestival[j].festivalName && this.state.dateFestival == defaultFestival[j].festivalDate) {
                        defaultFestival[j].afterImpact.startDate = this.state.afterFromDate
                        defaultFestival[j].afterImpact.endDate = this.state.afterToDate
                        defaultFestival[j].beforeImpact.startDate = this.state.beforeFromDate
                        defaultFestival[j].beforeImpact.endDate = this.state.beforeToDate
                        defaultFestival[j].isChecked = "true"
                        if (festivalName.includes(defaultFestival[j].festivalName)) {
                            lastFestivalArray.push(defaultFestival[j].festivalName)
                            let data = {
                                festivalName: defaultFestival[j].festivalName,
                                afterImpact: {
                                    startDate: defaultFestival[j].afterImpact.startDate,
                                    endDate: defaultFestival[j].afterImpact.endDate
                                },
                                beforeImpact: {
                                    startDate: defaultFestival[j].beforeImpact.startDate,
                                    endDate: defaultFestival[j].beforeImpact.endDate
                                },
                                festivalDate: defaultFestival[j].festivalDate,
                                isChecked: "true",
                                festivalType: "default"
                            }
                            lastArrayChecked.push(data)
                            recentlyAdded.push(data)
                        }
                    }
                }
                let customFest = this.state.customFest
                if (customFest.length != 0) {
                    for (var j = 0; j < customFest.length; j++) {
                        if (festivalName == customFest[j].festivalName) {
                            customFest[j].afterImpact.startDate = this.state.afterFromDate
                            customFest[j].afterImpact.endDate = this.state.afterToDate
                            customFest[j].beforeImpact.startDate = this.state.beforeFromDate
                            customFest[j].beforeImpact.endDate = this.state.beforeToDate
                            customFest[j].isChecked = "true"
                            if (!lastFestivalArray.includes(customFest[j].festivalName)) {
                                lastFestivalArray.push(customFest[j].festivalName)
                                let data = {
                                    festivalName: customFest[j].festivalName,
                                    festivalDate: customFest[j].festivalDate,
                                    afterImpact: {
                                        startDate: customFest[j].afterImpact.startDate,
                                        endDate: customFest[j].afterImpact.endDate
                                    },
                                    beforeImpact: {
                                        startDate: customFest[j].beforeImpact.startDate,
                                        endDate: customFest[j].beforeImpact.endDate
                                    },

                                    isChecked: "true",
                                    festivalType: customFest[j].festivalType
                                }
                                customTrue.push(data)
                                recentlyAdded.push(data)
                            }
                        }
                    }
                    for (let k = 0; k < customTrue.length; k++) {
                        for (let l = 0; l < lastAddedArray.length; l++) {
                            if (customTrue[k].festivalName == lastAddedArray[l].festivalName) {
                                lastAddedArray.splice(l, 1)
                            }
                        }
                    }
                }
                this.setState({
                    dateSelect: !this.state.dateSelect,
                    defaultFestival,
                    configFestival,
                    lastArrayChecked,
                    customTrue,
                    customFest,
                    recentlyAdded,
                    lastAddedArray
                })
            }
        }, 10)
    }

    deleteArrayConfirmation(name) {
        this.setState({
            resetAndDelete: true,
            headerMsg: "Are you sure to remove festival impact date?",
            paraMsg: "Click confirm to continue.",
            name: name
        })
    }
    closeResetDeleteModal() {
        this.setState({
            resetAndDelete: !this.state.resetAndDelete,
        })
    }
    deleteArray(name) {
        let configFestival = this.state.configFestival
        let defaultFestival = this.state.defaultFestival
        let lastFestivalArray = this.state.lastFestivalArray
        let lastArrayChecked = this.state.lastArrayChecked
        let customTrue = this.state.customTrue
        let customFest = this.state.customFest
        let lastAddedArray = this.state.lastAddedArray
        let recentlyAdded = this.state.recentlyAdded
        for (let i = 0; i < configFestival.length; i++) {
            if (name == configFestival[i].festivalName && configFestival[i].festivalType == "default") {
                // ____________lastadded name in lastfestivalarray___________
                for (var k = 0; k < lastFestivalArray.length; k++) {
                    if (lastFestivalArray[k] == configFestival[i].festivalName) {
                        recentlyAdded.push(lastFestivalArray[k])
                        lastFestivalArray.splice(k, 1)
                        configFestival[i].afterImpact.startDate = ""
                        configFestival[i].afterImpact.endDate = ""
                        configFestival[i].beforeImpact.startDate = ""
                        configFestival[i].beforeImpact.endDate = ""
                        configFestival[i].isChecked = "false"
                    }
                }
                // ____________isChecked true in lastArrayChecked____________

                for (let l = 0; l < lastArrayChecked.length; l++) {
                    if (lastArrayChecked[l].festivalName == configFestival[i].festivalName) {
                        lastArrayChecked.splice(l, 1)
                        configFestival[i].afterImpact.startDate = ""
                        configFestival[i].afterImpact.endDate = ""
                        configFestival[i].beforeImpact.startDate = ""
                        configFestival[i].beforeImpact.endDate = ""
                        configFestival[i].isChecked = "false"
                    }
                }
            }
        }
        // ____________default festival added recently____________
        for (let j = 0; j < defaultFestival.length; j++) {
            if (name == defaultFestival[j].festivalName) {
                for (var k = 0; k < lastFestivalArray.length; k++) {
                    if (lastFestivalArray[k] == defaultFestival[j].festivalName) {
                        recentlyAdded.push(lastFestivalArray[k])
                        lastFestivalArray.splice(k, 1)
                        defaultFestival[j].afterImpact.startDate = ""
                        defaultFestival[j].afterImpact.endDate = ""
                        defaultFestival[j].beforeImpact.startDate = ""
                        defaultFestival[j].beforeImpact.endDate = ""
                        defaultFestival[j].isChecked = "false"
                    }
                }
                for (let l = 0; l < lastArrayChecked.length; l++) {
                    if (lastArrayChecked[l].festivalName == defaultFestival[j].festivalName) {
                        lastArrayChecked.splice(l, 1)
                        defaultFestival[j].afterImpact.startDate = ""
                        defaultFestival[j].afterImpact.endDate = ""
                        defaultFestival[j].beforeImpact.startDate = ""
                        defaultFestival[j].beforeImpact.endDate = ""
                        defaultFestival[j].isChecked = "false"
                    }
                }
            }
        }
        // ______________custom festival array_________
        for (let m = 0; m < customFest.length; m++) {
            if (name == customFest[m].festivalName) {
                for (var k = 0; k < lastFestivalArray.length; k++) {
                    if (lastFestivalArray[k] == customFest[m].festivalName) {
                        recentlyAdded.push(lastFestivalArray[k])
                        lastFestivalArray.splice(k, 1)
                        customFest[m].afterImpact.startDate = ""
                        customFest[m].afterImpact.endDate = ""
                        customFest[m].beforeImpact.startDate = ""
                        customFest[m].beforeImpact.endDate = ""
                        customFest[m].isChecked = "false"
                    }
                }
                for (let l = 0; l < customTrue.length; l++) {
                    if (customTrue[l].festivalName == customFest[m].festivalName) {
                        customTrue.splice(l, 1)
                        customFest[m].afterImpact.startDate = ""
                        customFest[m].afterImpact.endDate = ""
                        customFest[m].beforeImpact.startDate = ""
                        customFest[m].beforeImpact.endDate = ""
                        customFest[m].isChecked = "false"
                        let data = {
                            festivalName: customFest[m].festivalName,
                            afterImpact: {
                                startDate: "",
                                endDate: ""
                            },
                            beforeImpact: {
                                startDate: "",
                                endDate: ""
                            },
                            festivalDate: customFest[m].festivalDate,
                            isChecked: "false",
                            festivalType: customFest[m].festivalType
                        }
                        lastAddedArray.push(data)
                    }
                }
            }
        }
        this.setState({
            customFest,
            configFestival,
            customTrue,
            lastFestivalArray,
            lastArrayChecked,
            lastAddedArray,

        })
    }

    getYearData(year) {
        let yearsValue = year, nextYear = yearsValue + 1
        this.state.financialYear = yearsValue + '-' + nextYear
        let minDate = this.state.minDate
        let maxDate = this.state.maxDate
        // ____________________________Get All Api_________________________________
        if (this.state.yearsCurrent + 5 == year) {
            this.setState({
                truestate: true
            })
            let data = {
                year: year - 1 + '-' + year
            }
            this.props.allFestivalRequest(data)

        } else {
            this.setState({
                truestate: false
            })
            let data = {
                year: this.state.financialYear
            }
            this.props.allFestivalRequest(data)
        }
        var date = new Date,
            fullyear = date.getFullYear();
        // ____________---financial year date selection__________________
        minDate = year + '-' + '04-01'
        maxDate = nextYear + '-' + '03-30'
        // ___________________class change state set_________________
        if (fullyear > year) {
            this.setState({
                disableYear: true
            })
        } else {
            this.setState({
                disableYear: false
            })
        }
        this.setState({
            yearValue: yearsValue,
            minDate,
            maxDate,
            recentlyAdded: [],
            configuredName: [],
            lastFestivalArray: [],
            lastArrayChecked: [],
            lastAddedArray: [],
            customTrue: [],
            falseState: []
        })
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    lastSaveFestival(e) {
        let lastArrayChecked = this.state.lastArrayChecked
        let customTrue = this.state.customTrue
        let lastAddedArray = this.state.lastAddedArray
        let dataArray = []
        let mainpayload = []
        let customAdded = []
        if (lastArrayChecked.length != 0) {
            lastArrayChecked.forEach(custom => {
                if (custom.isChecked == "true") {
                    let data = {
                        festivalName: custom.festivalName,
                        festivalDate: custom.festivalDate,
                        afterImpact: {
                            startDate: custom.afterImpact.startDate,
                            endDate: custom.afterImpact.endDate
                        },
                        beforeImpact: {
                            startDate: custom.beforeImpact.startDate,
                            endDate: custom.beforeImpact.endDate
                        },
                        isChecked: custom.isChecked,
                        festivalType: custom.festivalType
                    }
                    dataArray.push(data)
                }
            })
        }
        if (lastAddedArray.length != 0 && customTrue.length != 0) {
            for (let i = 0; i < lastAddedArray.length; i++) {
                let data = {
                    festivalName: lastAddedArray[i].festivalName,
                    festivalDate: lastAddedArray[i].festivalDate,
                    afterImpact: {
                        startDate: lastAddedArray[i].afterImpact.startDate,
                        endDate: lastAddedArray[i].afterImpact.endDate
                    },
                    beforeImpact: {
                        startDate: lastAddedArray[i].beforeImpact.startDate,
                        endDate: lastAddedArray[i].beforeImpact.endDate
                    },
                    isChecked: lastAddedArray[i].isChecked,
                    festivalType: lastAddedArray[i].festivalType
                }
                customAdded.push(data)
                mainpayload = customAdded
            }
            for (let j = 0; j < customTrue.length; j++) {
                let data = {
                    festivalName: customTrue[j].festivalName,
                    festivalDate: customTrue[j].festivalDate,
                    afterImpact: {
                        startDate: customTrue[j].afterImpact.startDate,
                        endDate: customTrue[j].afterImpact.endDate
                    },
                    beforeImpact: {
                        startDate: customTrue[j].beforeImpact.startDate,
                        endDate: customTrue[j].beforeImpact.endDate
                    },
                    isChecked: customTrue[j].isChecked,
                    festivalType: customTrue[j].festivalType
                }
                customAdded.push(data)
                mainpayload = customAdded
            }
        } else if (lastAddedArray.length == 0 && customTrue.length != 0) {
            for (let j = 0; j < customTrue.length; j++) {

                let data = {
                    festivalName: customTrue[j].festivalName,
                    festivalDate: customTrue[j].festivalDate,
                    afterImpact: {
                        startDate: customTrue[j].afterImpact.startDate,
                        endDate: customTrue[j].afterImpact.endDate
                    },
                    beforeImpact: {
                        startDate: customTrue[j].beforeImpact.startDate,
                        endDate: customTrue[j].beforeImpact.endDate
                    },
                    isChecked: customTrue[j].isChecked,
                    festivalType: customTrue[j].festivalType
                }
                mainpayload.push(data)
            }
        } else if (customTrue.length == 0 && lastAddedArray.length != 0) {
            for (let i = 0; i < lastAddedArray.length; i++) {

                let data = {
                    festivalName: lastAddedArray[i].festivalName,
                    festivalDate: lastAddedArray[i].festivalDate,
                    afterImpact: {
                        startDate: lastAddedArray[i].afterImpact.startDate,
                        endDate: lastAddedArray[i].afterImpact.endDate
                    },
                    beforeImpact: {
                        startDate: lastAddedArray[i].beforeImpact.startDate,
                        endDate: lastAddedArray[i].beforeImpact.endDate
                    },
                    isChecked: lastAddedArray[i].isChecked,
                    festivalType: lastAddedArray[i].festivalType
                }
                mainpayload.push(data)
            }
        }

        let payload = {
            lastAddFestival: null,
            customAddFestival: mainpayload == "" ? null : mainpayload,
            updateFestival: lastArrayChecked.length == 0 ? null : dataArray,
            fiscalYear: this.state.financialYear
        }

        this.props.createFestivalRequest(payload)

        this.setState({
            recentlyAdded: [],
            configuredName: [],
            lastFestivalArray: [],
            lastArrayChecked: [],
            lastAddedArray: [],
            customTrue: [],
            falseState: []
        })
    }

    FunUp(id) {
        var element = document.getElementsByClassName(id)[0];
        var positionInfo = element.getBoundingClientRect();
        var height = positionInfo.height;
        var elmnt = document.getElementsByClassName(id)[0];
        if (id == "festivalConfigured") {
            height = height - 5;
        }
        elmnt.scrollTop = elmnt.scrollTop - height;
        elmnt.style.transition = "5s";
    }
    FunDown(id) {
        var element = document.getElementsByClassName(id)[0];
        var positionInfo = element.getBoundingClientRect();
        var height = positionInfo.height;
        var elmnt = document.getElementsByClassName(id)[0];
        height = height - 10
        if (id == "festivalConfigured") {
            height = height + 3;
        }
        elmnt.scrollTop = elmnt.scrollTop + height;
    }
    cancel() {
        this.setState({
            dateSelect: false,
        })
    }
    clearData() {
        let data = {
            year: this.state.financialYear
        }
        this.props.allFestivalRequest(data)
        this.setState({
            recentlyAdded: [],
            configuredName: [],
            lastFestivalArray: [],
            lastArrayChecked: [],
            lastAddedArray: [],
            customTrue: [],
            falseState: []
        })
    }
    render() {
        const { beforeFromDateerr, beforeToDateerr, afterFromDateerr, afterToDateerr } = this.state
        return (
            <div className="container-fluid" id="divHeight">
                <div className="container_div festivalSettingAdmin">
                    <div className="container-fluid">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                            <div className="topRow">
                                <div className="col-md-8 pad-lft-0">
                                    <div className="left">
                                        <div className="col-md-12">
                                            <div className="col-md-6 pad-0">
                                                <h5>Past Year’s</h5>
                                            </div>
                                            <div className="col-md-6 pad-0 textRight">
                                                <h5>Upcoming Year’s</h5>
                                            </div>
                                        </div>
                                        <div className="col-md-12 pad-0">
                                            <ul>
                                                {this.state.years.map((date, key) => (
                                                    this.state.truestate ? <li className={this.state.yearValue == date || this.state.years[this.state.years.length - 1] - 1 == date ? "active" : ""} key={key} onClick={() => this.getYearData(date)}>{date}</li> :
                                                        <li className={this.state.yearValue == `${date}` || this.state.yearValue + 1 == `${date}` ? "active" : ""} key={key} onClick={() => this.getYearData(date)}>{date}</li>
                                                    // <li className={this.state.yearValue == `${date}` || this.state.yearValue + 1 == `${date}` ? "active" : ""} key={key} onClick={() => this.getYearData(date)}>{date}</li>
                                                ))}
                                            </ul>
                                            <div className="col-md-12">
                                                <img src={scale} className="width100" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 pad-lft-0">
                                    <div className="right alignMiddle">
                                        <div className="col-md-6 pad-0" >
                                            <button type="button" onClick={(e) => this.state.disableYear ? null : this.openCreateNew(e)} className={this.state.disableYear ? "btnDisabled createbtn alignMiddle" : "createbtn alignMiddle"} > <img src={createIcon} />Create new festival</button>
                                        </div>
                                        <div className="col-md-6 textCenter">
                                            <button type="button" className={this.state.recentlyAdded.length == 0 ? "opacity clearbtn" : "clearbtn"} onClick={() => this.state.recentlyAdded.length == 0 ? null : this.clearData()}>Clear</button>
                                            <button type="button" className={this.state.recentlyAdded.length == 0 ? "btnDisabled savebtn" : "savebtn"} onClick={(e) => this.state.recentlyAdded.length == 0 ? null : this.lastSaveFestival(e)}>Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="bottomRow m-top-15">
                                <div className="col-md-8 pad-lft-0">
                                    <div className="left">
                                        <div className="colHeader alignMiddle">
                                            <div className="col-md-6"><h4>Available Festival</h4></div>
                                            <div className="col-md-6 textRight">
                                                <ul className="list-inline width100">
                                                    <li>Custom Created Festival</li>
                                                    <li>Default Festival</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className=" width100 displayInline">
                                            <div className="innerCustomDiv">
                                                {this.state.defaultFestivalState.length != 0 ? <div id="scrollMe">
                                                    {this.state.configFestival.map((dataa, keyy) => (
                                                        dataa.festivalType == "default" ? <div className="col-md-4 padRightNone eachSelectedHoliday" key={keyy} id={dataa.festivalName} onClick={(e) => this.dateToolTip(dataa.festivalName, dataa.festivalDate, e)}>
                                                            <div className="item active">
                                                                <div className="col-md-12 alignMiddle">
                                                                    {!this.state.disableYear ? <div className="col-md-2 pad-0">{dataa.beforeImpact.startDate == "" ? <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={addIcon} /></div> :
                                                                        <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={editIcon} onClick={() => this.deleteArrayConfirmation(dataa.festivalName)} /></div>}
                                                                    </div> : null}
                                                                    <div className="col-md-10 pad-0"><p>{dataa.festivalName}</p>
                                                                        <span className="festivalDate">({dataa.festivalDate})</span>
                                                                        {dataa.beforeImpact.startDate != "" ? <div className="date"><span className="from">{dataa.beforeImpact.startDate} </span><div className="pad-lf-3 displayInline">-</div><span className="to">{dataa.afterImpact.endDate}</span></div> : null}</div>
                                                                </div>
                                                            </div>
                                                        </div> : null))}
                                                    {this.state.customFestival.length != 0 ? this.state.customFestival.map((data, keyy) => (
                                                        <div className="col-md-4 padRightNone eachSelectedHoliday" key={keyy} id={data.festivalName} onClick={(e) => this.dateToolTip(data.festivalName, data.festivalDate, e)}>
                                                            <div className="item activeBlack">
                                                                <div className="col-md-12 alignMiddle">
                                                                    {!this.state.disableYear ? <div className="col-md-2 pad-0">{data.beforeImpact.startDate == "" ? <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={addIcon} /></div> :
                                                                        <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={editIcon} onClick={() => this.deleteArrayConfirmation(data.festivalName)} /></div>}
                                                                    </div> : null}
                                                                    <div className="col-md-10 pad-0"><p>{data.festivalName}</p>
                                                                        <span className="festivalDate">({data.festivalDate})</span>
                                                                        {data.beforeImpact.startDate != "" ? <div className="date"><span className="from">{data.beforeImpact.startDate}</span><div className="pad-lf-3 displayInline">-</div><span className="to">{data.afterImpact.endDate}</span></div> : null}</div>
                                                                </div>
                                                            </div>
                                                        </div>)) : null}
                                                    {this.state.configFestival.length == 0 ? this.state.defaultFestivalState.map((data, key) => (
                                                        <div className="col-md-4 padRightNone eachSelectedHoliday" key={key} id={data.festivalName} onClick={(e) => this.dateToolTip(data.festivalName, data.festivalDate, e)}>
                                                            <div className="item active">
                                                                <div className="col-md-12 alignMiddle">
                                                                    {!this.state.disableYear ? <div className="col-md-2 pad-0">{data.beforeImpact.startDate == "" || this.state.lastArrayChecked.length == 0 ? <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={addIcon} /></div> :
                                                                        <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={editIcon} onClick={() => this.deleteArrayConfirmation(data.festivalName)} /></div>}
                                                                    </div> : null}
                                                                    <div className="col-md-10 pad-0"><p>{data.festivalName}</p>
                                                                        <span className="festivalDate">({data.festivalDate}) </span>
                                                                        {data.beforeImpact.startDate != "" ? <div className="date"><span className="from"> {data.beforeImpact.startDate} </span><div className="pad-lf-3 displayInline">-</div><span className="to">{data.afterImpact.endDate}</span></div> : null}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )) : this.state.defaultFestival.map((data, key) => (
                                                        <div className="col-md-4 padRightNone eachSelectedHoliday" key={key} id={data.festivalName} onClick={(e) => this.dateToolTip(data.festivalName, data.festivalDate, e)}>
                                                            <div className="item active">
                                                                <div className="col-md-12 alignMiddle">
                                                                    {!this.state.disableYear ? <div className="col-md-2 pad-0">{data.beforeImpact.startDate == "" ? <div className="col-md-12 pad-0 imageBeforeEffect"><img className="imgHeight" src={addIcon} /></div> :
                                                                        <div className="col-md-12 pad-0"><img className="imgHeight" src={editIcon} onClick={() => this.deleteArrayConfirmation(data.festivalName)} /></div>}
                                                                    </div> : null}
                                                                    <div className="col-md-10 pad-0"><p>{data.festivalName}</p>
                                                                        <span className="festivalDate">({data.festivalDate})</span>
                                                                        {data.beforeImpact.startDate != "" ? <div className="date"><span className="from">{data.beforeImpact.startDate} </span><div className="pad-lf-3 displayInline">-</div><span className="to">{data.afterImpact.endDate}</span></div> : null}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>))}

                                                </div> : <p>No data found</p>}
                                            </div>
                                            {this.state.defaultFestivalState.length != 0 ? <div className="col-md-12 textCenter arrow" >
                                                <div className="displayInline" onClick={() => this.FunUp("innerCustomDiv")}><img src={up} /></div>
                                                <div className="displayInline" onClick={() => this.FunDown("innerCustomDiv")}><img src={down} /></div>
                                            </div> : null}
                                        </div>
                                    </div>
                                </div>
                                <div className="pad-lft-0 right">
                                    <div>
                                        <div className="colHeader alignMiddle">
                                            <div className="col-md-6"><h4>Configured Festivals</h4></div>
                                        </div>
                                        <div className="displayInline width100">
                                            <div className="festivalConfigured">
                                                <div className="displayInline width100">
                                                    {this.state.configuredName.length != 0 ? this.state.configuredName.map((d, key) => (
                                                        <div className="m-top-10 configuredDate" key={key}>
                                                            <h5>{d.festivalName}</h5>
                                                            <span>{d.festivalDate}</span>
                                                            <div className="impactDiv">
                                                                <span className="impactDate"> Impact date</span>
                                                                <p>({d.beforeImpact.startDate}) - ({d.afterImpact.endDate})</p>
                                                            </div>
                                                        </div>
                                                    )) : <p className="noDataFound font13">No configured festival found </p>}
                                                    <div id="toolTipPos" ref={(e) => this.setWrapperRef(e)} style={{ top: this.state.toolTip.top, left: this.state.toolTip.left }} className={this.state.dateSelect ? "dateSelectToolTip" : "displayNone"}>
                                                        <div className="col-md-12 pad-0 m-top-5">
                                                            <h4>Before Festival Impact Date</h4>
                                                            <div className="col-md-6 pad-lft-0">
                                                                <label className="displayBlock">From</label>
                                                                <input type="date" id="beforeFromDate" min={this.state.minDate} max={this.state.festivalDate} value={this.state.beforeFromDate} className="purchaseOrderTextbox lineAfterDate organistionFilterModal" placeholder={this.state.beforeFromDate == "" ? "Select date from" : this.state.beforeFromDate} onChange={(e) => this.handleChange(e, this.state.festivalName)} />
                                                                {beforeFromDateerr ? (
                                                                    <span className="error">
                                                                        Select  Date From
                                            </span>
                                                                ) : null}
                                                            </div>
                                                            <div className="col-md-6 pad-right-0">
                                                                <label className="displayBlock">To</label>
                                                                {this.state.beforeFromDate == "" ? <input type="date" id="beforeToDate" placeholder="Select date to" className="organistionFilterModal" disabled /> :
                                                                    <input type="date" id="beforeToDate" min={this.state.beforeFromDate} max={this.state.festivalDate} placeholder={this.state.beforeToDate == "" ? "Select date to" : this.state.beforeToDate} value={this.state.beforeToDate} className="organistionFilterModal" onChange={(e) => this.handleChange(e, this.state.festivalName)} />}
                                                                {beforeToDateerr ? (
                                                                    <span className="error">
                                                                        Select  Date To
                                            </span>
                                                                ) : null}
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 pad-0 m-top-30">
                                                            <h4>After Festival Impact Date</h4>
                                                            <div className="col-md-6 pad-lft-0">
                                                                <label className="displayBlock">From</label>
                                                                <input type="date" id="afterFromDate" min={this.state.festivalDate} max={this.state.maxDate} value={this.state.afterFromDate} className="purchaseOrderTextbox lineAfterDate organistionFilterModal" placeholder={this.state.afterFromDate == "" ? "Select date from" : this.state.afterFromDate} onChange={(e) => this.handleChange(e, this.state.festivalName)} />
                                                                {afterFromDateerr ? (
                                                                    <span className="error">
                                                                        Select  Date From
                                            </span>
                                                                ) : null}
                                                            </div>
                                                            <div className="col-md-6 pad-right-0">
                                                                <label className="displayBlock">To</label>
                                                                {this.state.afterFromDate == "" ? <input type="date" id="afterToDate" placeholder="Select date to" className="organistionFilterModal" disabled /> :
                                                                    <input type="date" id="afterToDate" min={this.state.afterFromDate} max={this.state.maxDate} value={this.state.afterToDate} placeholder={this.state.afterToDate == "" ? "Select date to" : this.state.afterToDate} className="organistionFilterModal" onChange={(e) => this.handleChange(e, this.state.festivalName)} />}
                                                                {afterToDateerr ? (
                                                                    <span className="error">
                                                                        Select  Date To
                                            </span>
                                                                ) : null}
                                                            </div>
                                                        </div>

                                                        <div className="toolTipFooter displayInline m-top-30">
                                                            <button type="button" className="done" onClick={(e) => this.saveFestival(e)}>Done</button>
                                                            <button type="button" className="cancel" onClick={() => this.cancel()}>Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {this.state.configuredName.length != 0 ? <div className="col-md-12 textCenter arrow" >
                                                <div className="displayInline" onClick={() => this.FunUp("festivalConfigured")}><img src={up} /></div>
                                                <div className="displayInline" onClick={() => this.FunDown("festivalConfigured")}><img src={down} /></div>
                                            </div> : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.createNew ? <CreateNewFestivalModal {...this.props} {...this.state} closeModal={(e) => this.closeModal(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.resetAndDelete ? <ConfirmationModalFestival {...this.props} {...this.state} closeResetDeleteModal={(e) => this.closeResetDeleteModal(e)} deleteArray={(e) => this.deleteArray(e)} /> : null}
            </div>
        )
    }
}