import React from 'react';
import { loadavg } from 'os';
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import DemandPlanningFilter from './demandPlanningFilter';
import downloadIcon from '../../assets/download-icon.svg'
import axios from 'axios';
import { CONFIG } from "../../config/index";
import { getMonthByNumber } from '.././../helper'
import runDemandCircle from "../../assets/rundemandcircle.svg";
import circleInside from "../../assets/circleInside.svg";
import rectangleBg from "../../assets/rectangle.svg";
import refreshIcon from "../../assets/refresh.svg";
import ForecastSuccessModal from './forecastSuccessModal';
import CloseStatus from '../../assets/group-4.svg';
import SuccessIcon from '../../assets/success-check.svg';
import AssortRunning from '../../assets/live-job.svg';

class ForecastOnDemand extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            demandPlanning: false,
            frequency: "",
            frequencyerr: false,
            noMonths: "",
            year: "",
            yearerr: false,
            monthly: [{ value: 1, check: false }, { value: 2, check: false }, { value: 3, check: false }, { value: 4, check: false }, { value: 5, check: false }, { value: 6, check: false }, { value: 7, check: false }, { value: 8, check: false }, { value: 9, check: false }, { value: 10, check: false }, { value: 11, check: false }, { value: 12, check: false }, { value: 13, check: false }, { value: 14, check: false }, { value: 15, check: false }, { value: 16, check: false }, { value: 17, check: false }, { value: 18, check: false }, { value: 19, check: false }, { value: 20, check: false }, { value: 21, check: false }, { value: 22, check: false }, { value: 23, check: false }, { value: 24, check: false }, { value: 25, check: false }, { value: 26, check: false }, { value: 27, check: false }, { value: 28, check: false }, { value: 29, check: false }, { value: 30, check: false }, { value: 31, check: false }, { value: 32, check: false }, { value: 33, check: false }, { value: 34, check: false }, { value: 35, check: false }, { value: 36 }],
            weekly: [{ value: 5, check: false }, { value: 6, check: false }, { value: 7, check: false }, { value: 8, check: false }, { value: 9, check: false }, { value: 10, check: false }, { value: 11, check: false }, { value: 12, check: false }, { value: 13, check: false }, { value: 14, check: false }, { value: 15, check: false }, { value: 16, check: false }, { value: 17, check: false }, { value: 18, check: false }, { value: 19, check: false }, { value: 20, check: false }, { value: 21, check: false }, { value: 22, check: false }, { value: 23, check: false }, { value: 24, check: false }, { value: 25, check: false }],
            loader: true,
            alert: false,
            success: false,
            code: "",
            errorMessage: "",
            errorCode: "",
            successMessage: "",
            forcastData: [],
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            startMontherr: false,
            generatedForecast: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            no: 1,
            demandPlanning: false,
            rangeVal: 1,
            status: "",
            runId: "NA",
            runStatus: "NA",
            lastStatus: "NA",
            lastCreatedOn: "NA",
            lastEngineRun: "NA",
            percentage: 0,
            timeTaken: 0,
            sfmodal: false,
            status: "",
            statusMessage: "",

        }
    }

    componentWillMount() {
        let data = {
            no: 1,
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        }
        this.props.lastForecastRequest();
        this.props.getForcastRequest(data);
        this.props.getForecastStatusRequest("NA");
        this.setIntervalDP = setInterval(() => {
            this.props.getForecastStatusRequest(this.state.runId);
        }, 5000)
        this.statusInterval = setInterval(() => {
            this.props.getAssortmentStatusRequest()
        }, 10000)
        this.props.getAssortmentStatusRequest();
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.demandPlanning.forcast.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.demandPlanning.forcast.data.message,
                runId: nextProps.demandPlanning.forcast.data.resource.runId
            })
            this.props.getForecastStatusRequest(nextProps.demandPlanning.forcast.data.resource.runId);
            this.props.forcastRequest();
        } else if (nextProps.demandPlanning.forcast.isError) {
            this.setState({
                code: nextProps.demandPlanning.forcast.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.demandPlanning.forcast.message.error == undefined ? undefined : nextProps.demandPlanning.forcast.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.forcast.message.error == undefined ? undefined : nextProps.demandPlanning.forcast.message.error.errorMessage,
            })
            this.props.forcastRequest();
        }

        if (nextProps.inventoryManagement.getAssortmentStatus.isSuccess) {
            this.setState({
                status: nextProps.inventoryManagement.getAssortmentStatus.data.resource.status,
                statusMessage: nextProps.inventoryManagement.getAssortmentStatus.data.resource.message
            })
        }

        if (nextProps.demandPlanning.lastForecast.isSuccess) {
            if (nextProps.demandPlanning.lastForecast.data.resource != null) {
                this.setState({
                    lastCreatedOn: nextProps.demandPlanning.lastForecast.data.resource.createdOn,
                    lastStatus: nextProps.demandPlanning.lastForecast.data.resource.status
                })
            }
        }

        if (nextProps.demandPlanning.getForcast.isSuccess) {
            this.setState({
                loader: false,
                forcastData: nextProps.demandPlanning.getForcast.data.resource == null ? [] : nextProps.demandPlanning.getForcast.data.resource,
                prev: nextProps.demandPlanning.getForcast.data.prePage,
                current: nextProps.demandPlanning.getForcast.data.currPage,
                next: nextProps.demandPlanning.getForcast.data.currPage + 1,
                maxPage: nextProps.demandPlanning.getForcast.data.maxPage,
            })
        } else if (nextProps.demandPlanning.getForcast.isError) {
            this.setState({
                code: nextProps.demandPlanning.getForcast.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.demandPlanning.getForcast.message.error == undefined ? undefined : nextProps.demandPlanning.getForcast.message.error.errorCode,
                errorMessage: nextProps.demandPlanning.getForcast.message.error == undefined ? undefined : nextProps.demandPlanning.getForcast.message.error.errorMessage,
            })
        }

        if (nextProps.demandPlanning.getForecastStatus.isSuccess) {
            if (nextProps.demandPlanning.getForecastStatus.data.resource != null) {
                this.setState({
                    runId: nextProps.demandPlanning.getForecastStatus.data.resource.runId == null ? "NA" : nextProps.demandPlanning.getForecastStatus.data.resource.runId,
                    runStatus: nextProps.demandPlanning.getForecastStatus.data.resource.status,
                    lastEngineRun: nextProps.demandPlanning.getForecastStatus.data.resource.lastEngineRun + " mins",
                    percentage: nextProps.demandPlanning.getForecastStatus.data.resource.percentage,
                    timeTaken: parseInt(nextProps.demandPlanning.getForecastStatus.data.resource.timeTaken) > 60 ? `${Math.floor(parseInt(nextProps.demandPlanning.getForecastStatus.data.resource.timeTaken) / 60)} Hrs ${parseInt(nextProps.demandPlanning.getForecastStatus.data.resource.timeTaken) % 60} Mins` : nextProps.demandPlanning.getForecastStatus.data.resource.timeTaken + " Mins",
                })
                if (nextProps.demandPlanning.getForecastStatus.data.resource.status == "Failed" || nextProps.demandPlanning.getForecastStatus.data.resource.status == "Succeeded") {
                    this.setState({
                        sfmodal: true,
                    })
                }
            }
        }

        if (nextProps.demandPlanning.getForcast.isLoading || nextProps.demandPlanning.forcast.isLoading) {
            this.setState({
                loader: true
            })
        }
    }

    componentWillUnmount() {
        clearInterval(this.setIntervalDP);
        clearInterval(this.statusInterval);
    }

    forcast(e) {
        e.preventDefault();
        this.year();
        this.frequency();
        this.startMonth();
        const t = this;
        setTimeout(function (e) {
            const { yearerr, startMontherr, frequencyerr } = t.state;
            if (!yearerr && !startMontherr && !frequencyerr) {
                let data = {
                    demandForecast: "",
                    frequency: t.state.frequency,
                    predictPeriod: t.state.rangeVal,
                    chooseYear: t.state.year,
                    startMonth: t.state.startMonth,
                    configuredOn: "",
                    startedOn: "",
                    totalStore: "",
                    totalAssortment: ""
                }

                t.props.forcastRequest(data);

            }
        }, 100)
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.demandPlanning.getForcast.data.prePage,
                current: this.props.demandPlanning.getForcast.data.currPage,
                next: this.props.demandPlanning.getForcast.data.currPage + 1,
                maxPage: this.props.demandPlanning.getForcast.data.maxPage,
            })
            if (this.props.demandPlanning.getForcast.data.currPage != 0) {
                let data = {
                    no: this.props.demandPlanning.getForcast.data.currPage - 1,
                    search: this.state.search,
                    type: this.state.type,
                    demandForecast: this.state.demandForecast,
                    frequency: this.state.frequency,
                    predictPeriod: this.state.predictPeriod,
                    chooseYear: this.state.chooseYear,
                    startMonth: this.state.startMonth,
                    generatedForecast: this.state.generatedForecast,
                    status: this.state.status
                };
                this.props.getForcastRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.demandPlanning.getForcast.data.prePage,
                current: this.props.demandPlanning.getForcast.data.currPage,
                next: this.props.demandPlanning.getForcast.data.currPage + 1,
                maxPage: this.props.demandPlanning.getForcast.data.maxPage,
            })
            if (this.props.demandPlanning.getForcast.data.currPage != this.props.demandPlanning.getForcast.data.maxPage) {
                let data = {
                    no: this.props.demandPlanning.getForcast.data.currPage + 1,
                    search: this.state.search,
                    type: this.state.type,
                    demandForecast: this.state.demandForecast,
                    frequency: this.state.frequency,
                    predictPeriod: this.state.predictPeriod,
                    chooseYear: this.state.chooseYear,
                    startMonth: this.state.startMonth,
                    generatedForecast: this.state.generatedForecast,
                    status: this.state.status
                };
                this.props.getForcastRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.demandPlanning.getForcast.data.prePage,
                    current: this.props.demandPlanning.getForcast.data.currPage,
                    next: this.props.demandPlanning.getForcast.data.currPage + 1,
                    maxPage: this.props.demandPlanning.getForcast.data.maxPage,
                })
                if (this.props.demandPlanning.getForcast.data.currPage <= this.props.demandPlanning.getForcast.data.maxPage) {
                    let data = {
                        no: 1,
                        search: this.state.search,
                        type: this.state.type,
                        demandForecast: this.state.demandForecast,
                        frequency: this.state.frequency,
                        predictPeriod: this.state.predictPeriod,
                        chooseYear: this.state.chooseYear,
                        startMonth: this.state.startMonth,
                        generatedForecast: this.state.generatedForecast,
                        status: this.state.status
                    };
                    this.props.getForcastRequest(data)
                }

            }
        }
    }

    onSearch(e) {
        e.preventDefault();
        this.setState({
            type: 3,
            // demandForecast: this.state.demandForecast,
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        })
        let data = {
            no: 1,
            search: this.state.search,
            type: 3,
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        };
        this.props.getForcastRequest(data);
    }

    onClearSearch(e) {
        this.setState({
            search: "",
            type: 1
        });
        let data = {
            no: 1,
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        }
        this.props.getForcastRequest(data);

    }

    closeSfModal(e) {
        this.setState({
            sfmodal: false,
            runId: "NA",
            runStatus: "Not Started"
        })
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            demandPlanning: !this.state.demandPlanning,
            filterBar: !this.state.filterBar
        });
    }

    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            demandForecast: data.demandForecast,
            frequency: data.frequency,
            predictPeriod: data.predictPeriod,
            chooseYear: data.chooseYear,
            startMonth: data.startMonth,
            generatedForecast: data.generatedForecast,
            search: data.search,
            status: data.status
        });
    }

    onClearFilter(e) {
        this.setState({
            search: "",
            type: 1,
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        });
        let data = {
            no: 1,
            type: 1,
            search: "",
            demandForecast: "",
            frequency: "",
            predictPeriod: "",
            chooseYear: "",
            startMonth: "",
            generatedForecast: "",
            status: ""
        }
        this.props.getForcastRequest(data);
    }

    xlscsvDownload(type) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/download/module/${type}/demandPlanning`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                console.log(error);
            });
    }

    handleChange(e, data) {
        if (e.target.id == "frequency") {
            this.setState({
                frequency: data,
                rangeVal: data == "WEEKLY" ? 4 : 1
            },
                () => {
                    this.frequency();
                }
            )
        } else if (e.target.id == "year") {
            this.setState({
                year: e.target.value
            },
                () => {
                    this.year();
                })
        } else if (e.target.id == "startMonth") {
            this.setState({
                startMonth: e.target.value
            },
                () => {
                    this.startMonth();
                })
        } else if (e.target.id == "myRange") {
            this.setState({
                rangeVal: e.target.value
            })
        }
    }

    startMonth() {
        if (
            this.state.startMonth == "") {
            this.setState({
                startMontherr: true
            });
        } else {
            this.setState({
                startMontherr: false
            });
        }
    }

    year() {
        if (
            this.state.year == "") {
            this.setState({
                yearerr: true
            });
        } else {
            this.setState({
                yearerr: false
            });
        }
    }

    frequency() {
        if (
            this.state.frequency == "") {
            this.setState({
                frequencyerr: true
            });
        } else {
            this.setState({
                frequencyerr: false
            });
        }
    }

    downloadForecast(runId) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}${CONFIG.DEMAND_PLANNING}/download/history?runId=${runId}`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                console.log(error);
            });
    }

    render() {

        let show = false;
        if (this.state.rangeVal != "" && this.state.frequency != "" && this.state.year != "" && this.state.startMonth != "") {
            show = true
        }
        return (
            <div className="container-fluid pad-0">
                <div className="container_div" id="home">

                    <div className="container-fluid">
                        <div className="container_div" id="">

                            <div className="container-fluid">
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="replenishment_container customRunOnDemand forecastOnDemandMain">
                                        <div className="col-md-12 pad-0">
                                            <div className="col-md-4 pad-0">
                                                <div className="col-md-12 col-sm-12 pad-0">
                                                    <ul className="list_style">
                                                        <li>
                                                            <label className="contribution_mart">
                                                                FORECAST ON DEMAND
                                                        </label>
                                                        </li>
                                                        <li>
                                                            <p className="master_para">Schedule and run engine for forecasting</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <form >
                                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                                        <div className="selectFrequencyCustom m-top-20">
                                                            <label>Select Frequency</label>
                                                            <ul className="pad-0 lineAfter m-top-10">
                                                                <li> <label className="select_modalRadio">
                                                                    <input type="radio" id="frequency" onChange={(e) => this.handleChange(e, "WEEKLY")} name="frequency" checked={this.state.frequency == "WEEKLY" ? true : false} /><span>Weekly</span>
                                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                                </label>
                                                                </li>
                                                                <li><label className="select_modalRadio">
                                                                    <input type="radio" id="frequency" onChange={(e) => this.handleChange(e, "MONTHLY")} name="frequency" checked={this.state.frequency == "MONTHLY" ? true : false} /><span>Monthly</span>
                                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                                </label></li>
                                                                {this.state.frequencyerr ? <span className="error m-top-10">Select Frequency</span> : null}
                                                                {/* <li><input type="radio" name="frequency"/><span>Weekely</span></li>
                                                            <li><input type="radio" name="frequency"/><span>Monthly</span></li> */}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                                        <div className="predectionTime m-top-50">
                                                            {this.state.frequency == "" ? null : <div>
                                                                <label>PREDICTION TIMELINE</label>
                                                                <p className="m-top-5">Choose number of {this.state.frequency == "WEEKLY" ? "weeks" : "months"}</p>

                                                                <div className="slideContainer m-top-30">
                                                                    {/* <p id="rangeTooltip" className="rangeTooltip"><span id="demo">{this.state.rangeVal}</span></p> */}
                                                                    <input type="range" onChange={(e) => this.handleChange(e)} min={this.state.frequency == "WEEKLY" ? "4" : "1"} max={this.state.frequency == "WEEKLY" ? "53" : "12"} value={this.state.rangeVal} className="slider" id="myRange" />
                                                                    <p>{this.state.rangeVal}</p> <span>{this.state.frequency == "WEEKLY" ? "Weeks" : "Months"}</span>
                                                                </div>

                                                            </div>}
                                                            <div className="chooseYear m-top-45">
                                                                <ul className="pad-0">
                                                                    <li className="verticalTop">
                                                                        <h5>Choose Year</h5>
                                                                        <select className={this.state.yearerr ? "errorBorder displayPointer" : "displayPointer"} value={this.state.year} id="year" onChange={(e) => this.handleChange(e)} >
                                                                            <option>Select Year</option>
                                                                            <option value="2019">2019</option>
                                                                            <option value="2020">2020</option>
                                                                        </select>
                                                                        {this.state.yearerr ? <span className="error">Select Year</span> : null}
                                                                    </li>
                                                                    <li className="verticalTop">
                                                                        <h5>Select Start Month</h5>
                                                                        <select className={this.state.startMontherr ? "errorBorder displayPointer" : "displayPointer"} value={this.state.startMonth} id="startMonth" onChange={(e) => this.handleChange(e)} >
                                                                            <option>Select Month</option>
                                                                            <option value="01">January</option>
                                                                            <option value="02">February</option>
                                                                            <option value="03">March</option>
                                                                            <option value="04">April</option>
                                                                            <option value="05">May</option>
                                                                            <option value="06">June</option>
                                                                            <option value="07">July</option>
                                                                            <option value="08">August</option>
                                                                            <option value="09">September</option>
                                                                            <option value="10">October</option>
                                                                            <option value="11">November</option>
                                                                            <option value="12">December</option>
                                                                        </select>
                                                                        {this.state.startMontherr ? <span className="error">Select Start Month</span> : null}
                                                                    </li>
                                                                </ul>
                                                                {show ? <h4 className="m-top-35">Note : You are set to forecast on <span>{this.state.frequency == "WEEKLY" ? "Weekly" : "Monthly"}</span> basis for <span>{this.state.rangeVal}</span> {this.state.frequency == "WEEKLY" ? "weeks" : "months"}, from <span>{getMonthByNumber(this.state.startMonth)} {this.state.year}</span></h4> : null}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                                        <div className="runForcast m-top-80">
                                                            {this.state.runStatus == "Processing" || this.state.status == "INPROGRESS" ? <button type="button" className="btnDisabled pointerNone">RUN FORECAST</button> : <button type="button" onClick={(e) => this.forcast(e)}>RUN FORECAST</button>}
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            {/* ----------------------------------------------Middle Timer Div-------------------------------- */}

                                            <div className="col-md-4 col-sm-6 col-xs-12 pad-0 m-top-145 timerForcast">
                                                <div className="runDemandSvg">
                                                    <div className="runDemandBg">
                                                        <img src={runDemandCircle} className="firstSvg" />
                                                        <img src={circleInside} className="midSvg" />
                                                    </div>
                                                    {/* <img src={counterArea} className="lastImg" /> */}
                                                    {/* <div className="timeRunDemandMid">
                                                        <div className="clearfix">
                                                            <div className="c101 p51 big1">
                                                                <div className="slice1">
                                                                    <div className={this.state.percentage < 51 ? `d${this.state.percentage} progress-circle-demand` : `over51 progress-circle-demand d${this.state.percentage}`}>
                                                                        <div className="left-half-clipper-demand">
                                                                            <div className="first50-bar-demand"></div>
                                                                            <div className="value-bar-demand"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div> */}
                                                    <div className="forecastDemandTimer">
                                                        <div className="single-chart">
                                                            <svg viewBox="0 0 36 36" className="circular-chart green">
                                                                <path className="circle-bg" fill="#fff" d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831" />
                                                                <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                                                                    <stop offset="0%" stopColor="#3023ae" />
                                                                    <stop offset="100%" stopColor="#c86dd7" />
                                                                </linearGradient>
                                                                <path className="circle" stroke="url(#gradient)" strokeDasharray={this.state.percentage != 0 ? this.state.percentage + ", 100" : -1 + ", 100"} d="M36 20.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div className="runDemandMiddle">
                                                        <div className="midSvgbg">
                                                            <svg className="lastImg" xmlns="http://www.w3.org/2000/svg" width="193" height="194" viewBox="0 0 193 194">

                                                                <g fill="none" fillRule="evenodd">
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        <div className="autoConfigMid">
                                                            <div className="runDemandTimeElapsed">
                                                                <h5>Time Elapsed</h5>
                                                                <h3>{this.state.timeTaken}</h3>
                                                                {/* <h3>0</h3> */}
                                                                {/* <p>Min</p> */}
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="lastEngineRunTime textCenter posRelative">
                                                    <p>Last Engine Run Time </p><span className="forecastRunTime">{this.state.lastEngineRun}</span>
                                                </div>
                                                <div className="lftSideContent">
                                                    <ul className="list-inline width100 m-top-10">
                                                        <li>
                                                            <h3>Engine Run State : </h3>
                                                            <span className="forecastJobState">{this.state.runStatus}</span>
                                                        </li>


                                                    </ul>
                                                </div>
                                            </div>

                                            {/* ----------------------------------------------Middle Timer Div End-------------------------------- */}

                                            <div className="col-md-4 forcastLastRunDetailMain pad-0">
                                                <div className="forcastLastRunDetail">
                                                    {this.state.status != "SUCCEEDED_VIEWED" ?
                                                        <div className="assortmentStatus m-lft-8">
                                                            <div className="assortFailed">
                                                                {this.state.status == "SUCCEEDED" ? <img src={SuccessIcon} />
                                                                    : this.state.status == "INPROGRESS" ? <img src={AssortRunning} /> : null}
                                                                <label>{this.state.statusMessage}</label>
                                                                {this.state.status == "SUCCEEDED" ? <img className="displayPointer" onClick={(e) => this.props.viewMessageRequest('data')} src={CloseStatus} /> : null}
                                                            </div>
                                                        </div> : null}
                                                    <div className="col-md-10 lastEngineDetailForecast">
                                                        <div className="detailDemand">
                                                            <h2>Last Engine Run Details</h2>
                                                        </div>
                                                        <div className="numberDetail runDemandSuccess col-md-10 col-md-offset-2">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="86" height="85" viewBox="0 0 86 85">
                                                                <g fill="none" fillRule="evenodd" transform="translate(.5)">
                                                                    <circle cx="42.5" cy="42.5" r="42.5" fill="#ECECF6" fillRule="nonzero" />
                                                                    <g fill="#6D6DC9">
                                                                        <path d="M43 61c-10.476 0-19-8.524-19-19s8.524-19 19-19a1.188 1.188 0 0 1 0 2.375c-9.168 0-16.625 7.457-16.625 16.625S33.832 58.625 43 58.625 59.625 51.168 59.625 42A1.187 1.187 0 1 1 62 42c0 10.476-8.524 19-19 19z" />
                                                                        <path d="M43 25.375c-.309 0-.618-.13-.843-.344a1.249 1.249 0 0 1-.344-.843c0-.31.13-.618.344-.844.451-.439 1.235-.439 1.686 0 .214.226.344.535.344.843 0 .31-.13.618-.344.844a1.249 1.249 0 0 1-.843.344z" />
                                                                        <path fillRule="nonzero" d="M59.055 37.69a1.19 1.19 0 0 1 .843-1.45 1.186 1.186 0 0 1 1.46.832 1.202 1.202 0 0 1-.842 1.46 1.37 1.37 0 0 1-.31.036 1.19 1.19 0 0 1-1.151-.879zm-1.662-4.002h.011a1.186 1.186 0 1 1 2.054-1.188c.321.558.13 1.294-.439 1.615-.19.107-.391.166-.593.166-.405 0-.808-.213-1.033-.593zm-2.638-3.444a1.183 1.183 0 0 1 0-1.675 1.183 1.183 0 0 1 1.674 0h.012a1.192 1.192 0 0 1-.01 1.675 1.138 1.138 0 0 1-.832.344c-.308 0-.617-.107-.844-.344zm-3.443-2.648a1.17 1.17 0 0 1-.427-1.615c.32-.57 1.045-.76 1.615-.44a1.2 1.2 0 0 1 .44 1.627c-.226.38-.62.594-1.034.594-.202 0-.404-.06-.594-.166zm-4.001-1.651a1.208 1.208 0 0 1-.844-1.46 1.19 1.19 0 0 1 1.45-.844h.011a1.19 1.19 0 1 1-.309 2.34c-.107 0-.213-.012-.308-.036z" />
                                                                        <path d="M60.813 43.188c-.322 0-.62-.131-.844-.345a1.243 1.243 0 0 1-.344-.843c0-.309.13-.617.344-.843.427-.44 1.235-.44 1.687 0 .213.226.344.534.344.843 0 .309-.13.617-.344.843a1.249 1.249 0 0 1-.843.344zM50.124 47.938c-.226 0-.455-.066-.658-.2l-7.125-4.75a1.189 1.189 0 0 1-.529-.988V30.125a1.188 1.188 0 0 1 2.376 0v11.24l6.596 4.397a1.187 1.187 0 0 1-.66 2.175z" />
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                            <div className="listDemand borderTop">
                                                                <ul className="list-inline">
                                                                    <li>
                                                                        <h5>Last Engine Run</h5>
                                                                        {this.state.smallLoader1 ? <SectionLoader /> : <div><span>{this.state.lastCreatedOn}</span>
                                                                            <span className={this.state.lastStatus == "Succeeded" ? "runStateDemand m-top-10" : "runDemandStop m-top-10"}>{this.state.lastStatus == "NA" ? null : this.state.lastStatus}</span>
                                                                        </div>}
                                                                    </li>
                                                                    <li>
                                                                        {this.state.jobRunState == "NA" ? null : <button type="button" onClick={() => this.props.history.push('/demandPlanning/forecastHistory')} className="launchBtn btnHover" >View Forecast History</button>}
                                                                    </li>
                                                                    <li>
                                                                        {sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? <span>{this.state.nextScheduled}</span> : this.state.smallLoader2 ? <SectionLoader /> : <span>{this.state.nextScheduled}</span>}

                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.sfmodal ? <ForecastSuccessModal {...this.props} {...this.state} closeSfModal={(e) => this.closeSfModal(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.demandPlanning ? <DemandPlanningFilter updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} /> : null}
                {/* <Footer /> */}


            </div>

        );
    }
}

export default ForecastOnDemand;