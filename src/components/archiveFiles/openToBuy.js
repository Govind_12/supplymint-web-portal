import React from "react";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import FilterLoader from "../loaders/filterLoader";
import NoPlanExist from "./otb/noPlanExist";
import Otb from "./otb/otb";
import PoError from "../loaders/poError";
import OtbFailedModal from "./otb/otbFailedModal";
class OpenToBuy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otbState: [],
      loader: false,
      planExist: false,
      openToBuy: false,
      errorMessage: "",
      page: 1,
      errorCode: "",
      success: false,
      successMessage: "",
      alert: false,
      addOtb: false,
      activePlan: [],
      noPlanLoader: false,
      statusFailed: false,
      errorMassage: "",
      poErrorMsg: false,
      planPercentage: "",
      otbFailedModal: true
    };
  }
  componentWillMount() {
    this.props.getActivePlanRequest()
  }
  closeErrorRequest(e) {
    this.setState({
      poErrorMsg: !this.state.poErrorMsg
    })
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.demandPlanning.getOtbStatus.isSuccess) {
      if (nextProps.demandPlanning.getOtbStatus.data.resource.status == "INPROGRESS") {
        this.setState({
          planExist: false,
          noPlanLoader: true,
          otbFailedModal: true,
          // planPercentage: nextProps.demandPlanning.getOtbStatus.data.resource
        })
        setTimeout(() => {
          this.props.getOtbStatusRequest();
        }, 5000);
      } else if (nextProps.demandPlanning.getOtbStatus.data.resource.status == "FAILED") {
        this.setState({
          otbFailedModal: true,
          noPlanLoader: false,
          statusFailed: true,
          errorMassage: "Unable to create plan,try again!!!",
          poErrorMsg: true
        })
      } else if (nextProps.demandPlanning.getOtbStatus.data.resource.status == "SUCCEEDED") {
        this.setState({
          noPlanLoader: false,
          otbFailedModal: true,
        })
      }
    } else if (nextProps.demandPlanning.getOtbStatus.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.getOtbStatus.message.error == undefined ? undefined : nextProps.demandPlanning.getOtbStatus.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.getOtbStatus.message.error == undefined ? undefined : nextProps.demandPlanning.getOtbStatus.message.error.errorCode,
        code: nextProps.demandPlanning.getOtbStatus.message.status,
        alert: true,
        noPlanLoader: false
      })
      this.props.getOtbStatusClear();
    }
    if (nextProps.demandPlanning.getOtbStatus.isLoading) {
      this.setState({
        noPlanLoader: true
      })
    }
    if (!nextProps.demandPlanning.getActivePlan.isLoading) {
      this.setState({
        loader: false
      })
    }
    if (nextProps.demandPlanning.getActivePlan.isSuccess) {
      this.props.getOtbStatusRequest();
      this.setState({
        loader: false
      })
      if (nextProps.demandPlanning.getActivePlan.data.resource == null) {
        this.setState({
          activePlan: nextProps.demandPlanning.getActivePlan.data.resource,
          planExist: true,
          openToBuy: false
        })
      } else {
        this.setState({
          activePlan: nextProps.demandPlanning.getActivePlan.data.resource,
          planExist: false,
          openToBuy: true
        })
      }
      this.props.getActivePlanClear();
    } else if (nextProps.demandPlanning.getActivePlan.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.getActivePlan.message.error == undefined ? undefined : nextProps.demandPlanning.getActivePlan.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.getActivePlan.message.error == undefined ? undefined : nextProps.demandPlanning.getActivePlan.message.error.errorCode,
        code: nextProps.demandPlanning.getActivePlan.message.status,
        alert: true,
        loader: false
      })
      this.props.getActivePlanClear();
    }
    if (nextProps.demandPlanning.createPlan.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.createPlanRequest();
      this.props.getActivePlanRequest();
    }
    else if (nextProps.demandPlanning.createPlan.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.createPlan.message.error == undefined ? undefined : nextProps.demandPlanning.createPlan.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.createPlan.message.error == undefined ? undefined : nextProps.demandPlanning.createPlan.message.error.errorCode,
        code: nextProps.demandPlanning.createPlan.message.status,
        alert: true,
        loader: false
      })
      this.props.createPlanRequest();
    }
    if (nextProps.demandPlanning.updatePlan.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.demandPlanning.updatePlan.data.message,
      })
      this.props.updatePlanRequest();
    }
    else if (nextProps.demandPlanning.updatePlan.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.updatePlan.message.error == undefined ? undefined : nextProps.demandPlanning.updatePlan.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.updatePlan.message.error == undefined ? undefined : nextProps.demandPlanning.updatePlan.message.error.errorCode,
        code: nextProps.demandPlanning.updatePlan.message.status,
        alert: true,
        loader: false
      })
      this.props.updatePlanRequest();
    }
    // _____________________________________REMOVE PLAN __________________________________________
    if (nextProps.demandPlanning.removePlan.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.demandPlanning.removePlan.data.message,
      })
      this.props.removePlanRequest();
    }
    else if (nextProps.demandPlanning.removePlan.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.removePlan.message.error == undefined ? undefined : nextProps.demandPlanning.removePlan.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.removePlan.message.error == undefined ? undefined : nextProps.demandPlanning.removePlan.message.error.errorCode,
        code: nextProps.demandPlanning.removePlan.message.status,
        alert: true,
        loader: false
      })
      this.props.removePlanRequest();
    }
    if (nextProps.demandPlanning.totalOtb.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.totalOtbRequest();
    } else if (nextProps.demandPlanning.totalOtb.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.totalOtb.message.error == undefined ? undefined : nextProps.demandPlanning.totalOtb.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.totalOtb.message.error == undefined ? undefined : nextProps.demandPlanning.totalOtb.message.error.errorCode,
        code: nextProps.demandPlanning.totalOtb.message.status,
        alert: true,
        loader: false
      })
      this.props.totalOtbRequest();
    }
    // __________________________----UPDATE ACTIVE PLAN _____________________________________
    if (nextProps.demandPlanning.updateActivePlan.isSuccess) {
      this.setState({
        loader: false,
      })
      this.props.updateActivePlanRequest();
    }
    else if (nextProps.demandPlanning.updateActivePlan.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.updateActivePlan.message.error == undefined ? undefined : nextProps.demandPlanning.updateActivePlan.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.updateActivePlan.message.error == undefined ? undefined : nextProps.demandPlanning.updateActivePlan.message.error.errorCode,
        code: nextProps.demandPlanning.updateActivePlan.message.status,
        alert: true,
        loader: false
      })
      this.props.updateActivePlanRequest();
    }
    if (nextProps.demandPlanning.planSaleUpdate.isError) {
      this.setState({
        errorMessage: nextProps.demandPlanning.planSaleUpdate.message.error == undefined ? undefined : nextProps.demandPlanning.planSaleUpdate.message.error.errorMessage,
        errorCode: nextProps.demandPlanning.planSaleUpdate.message.error == undefined ? undefined : nextProps.demandPlanning.planSaleUpdate.message.error.errorCode,
        code: nextProps.demandPlanning.planSaleUpdate.message.status,
        alert: true,
      })
      this.props.planSaleUpdateClear();
    }
    if (nextProps.demandPlanning.getActivePlan.isLoading || nextProps.demandPlanning.createPlan.isLoading || nextProps.demandPlanning.updatePlan.isLoading
      || nextProps.demandPlanning.removePlan.isLoading || nextProps.demandPlanning.updateActivePlan.isLoading || nextProps.demandPlanning.totalOtb.isLoading) {
      this.setState({
        loader: true
      })
    }
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }
  onOtbPlan() {
    this.props.history.push('/demandPlanning/otb/noPlanExist')
  }
  closeOtbFailed() {
    this.setState({
      otbFailedModal: false
    })
  }
  render() {
    return (
      <div className="container-fluid">
        {this.state.planExist ? <NoPlanExist  {...this.props} {...this.state} otbState={this.state.otbState} addOtb={() => this.onOtbPlan()} clickRightSideBar={() => this.props.rightSideBar()} /> : null}
        {this.state.openToBuy || this.state.noPlanLoader || this.state.statusFailed ? <Otb {...this.props} {...this.state} statusFailed={this.state.statusFailed} noPlanLoader={this.state.noPlanLoader} activePlan={this.state.activePlan} otbState={this.state.otbState} clickRightSideBar={() => this.props.rightSideBar()} /> : null}
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
        {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
        {this.state.otbFailedModal ? <OtbFailedModal {...this.props} {...this.state} closeOtbFailed={(e) => this.closeOtbFailed(e)} /> : null}
      </div>
    );
  }
}
export default OpenToBuy;
