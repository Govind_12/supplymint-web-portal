import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper/index'

class OrderDrop extends React.Component {

    render() {
        var Order = JSON.parse(sessionStorage.getItem('Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Order')).crud)
        var Cancelled_Order = JSON.parse(sessionStorage.getItem('Cancelled Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled Orders')).crud)
        var Pending_Orders = JSON.parse(sessionStorage.getItem('Pending Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Pending Orders')).crud)
        var Processed_Orders = JSON.parse(sessionStorage.getItem('Processed Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Processed Orders')).crud)

        return (
            <label>
                <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
                    <button className="dropbtn home_link">Order
                    <i className="fa fa-chevron-down"></i>
                    </button>
                    {/* {sessionStorage.getItem("uType") == "ENT" && Order > 0 ? <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
                        {Pending_Orders > 0 ? <Link to="/enterprise/purchaseOrder/pendingOrders" onClick={(e) => closeMyDrop(e)}>Open PO</Link> : null}
                        <Link to="/enterprise/purchaseOrder/closedPo" onClick={(e) => closeMyDrop(e)}>Closed PO</Link>
                        {Processed_Orders > 0 ? <Link to="/enterprise/purchaseOrder/processedOrders" onClick={(e) => closeMyDrop(e)}>Pending Delivery</Link> : null}
                        {Cancelled_Order > 0 ? <Link to="/enterprise/purchaseOrder/cancelledOrders" onClick={(e) => closeMyDrop(e)}>Cancelled Orders</Link> : null}
                    </div> : null}
                    {sessionStorage.getItem("uType") == "VENDOR" && Order > 0 ? <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
                        {Pending_Orders > 0 ? <Link to="/vendor/purchaseOrder/pendingOrders" onClick={(e) => closeMyDrop(e)}>Pending Orders</Link> : null}
                        {Processed_Orders > 0 ? <Link to="/vendor/purchaseOrder/processedOrders" onClick={(e) => closeMyDrop(e)}>Processed Orders</Link> : null}
                        {Cancelled_Order > 0 ? <Link to="/vendor/purchaseOrder/cancelledOrders" onClick={(e) => closeMyDrop(e)}>Cancelled Orders</Link> : null}
                    </div> : null} */}
                </div>
            </label>
        );
    }
}

export default OrderDrop;