import React from 'react';

class PoError extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            error: this.props.code,
            showDiv:false
        };
    }
  
    componentDidMount() {
        this.textInput.current.focus();
     
    }
    onclose(e){
        // console.log(e.key)
        let count = 0 
        if(e.key=="Enter"){
           this.props.closeErrorRequest(e)
        }
    }

    
    onLogout =(e)=> {
        e.preventDefault();
        this.props.dashboardTilesRequest();
        this.props.storesArticlesRequest();
        this.props.salesTrendGraphRequest();
        this.props.slowFastArticleRequest();
        let payload = {
          userName: sessionStorage.getItem('userName'),
          token: sessionStorage.getItem('token'),
          type: "LOGGEDOUT",
          emailId: sessionStorage.getItem('email'),
          keepMeSignIn: "false",
        }
        this.props.userSessionCreateRequest(payload);
        this.props.userSessionCreateRequest();
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('partnerEnterpriseId');
        sessionStorage.removeItem('partnerEnterpriseName');
        sessionStorage.removeItem('firstName');
        sessionStorage.removeItem('lastName');
        sessionStorage.removeItem('roles');
        sessionStorage.removeItem('storeCode');
        document.getElementById("chatIconContainer").style.display='none';
        sessionStorage.clear();
        this.props.history.push('/');
    }
  
    render() {
        // console.log("errrrrasjdfasj" , this.props.poErrorMsg)
        return (
            <div className="requestErrorMain">
                <div className="modal display_block" id="confirmModal"><div className="backdrop"></div>
                    <div className="modal-content modalOrganisationAlert modalUpdate">

                        <div className="col-md-12 col-sm-12 pad-top-10">
                            <div className="modal_confirmation">
                                <ul className="list-inline modalListOrganisation">
                                    <li>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                            <path fill="#c44569" fillRule="evenodd" d="M9 13h2v2H9v-2zm0-8h2v6H9V5zm.99-5C4.47 0 0 4.48 0 10s4.47 10 9.99 10C15.52 20 20 15.52 20 10S15.52 0 9.99 0zM10 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z" />
                                        </svg>
                                    </li>
                                    <li className="float_initial">
                                        <p>
                                            {this.props.errorMassage == "" ? "" : this.props.errorMassage == undefined ? "" : this.props.errorMassage}
                                        </p>
                                       
                                    </li>

                                    <li>
                                        <button type="button" className="success_button"  id="errorClose"  onClick={(e)=>{this.props.errorMessage === "session expired please login again !!" ? this.onLogout(e) : this.props.closeErrorRequest(e)}}>OK</button>
                                 <button type="hidden" id ="hidden" ref={this.textInput} onKeyDown={(e) =>this.onclose(e)} />
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div></div>
            </div>
        );
    }
}

export default PoError;