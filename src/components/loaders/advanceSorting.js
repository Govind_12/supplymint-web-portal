import React from 'react';

class AdvanceSorting extends React.Component {
    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content advance-sorting-modal">
                    <div className="asm-head">
                        <h3>Advance Sorting</h3>
                        <button type="button" onClick={this.props.CloseSorting}><img src={require('../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="asm-body">
                        <div className="asmb-top">
                            <p>You can do column dependent sorting & arrange the column order</p>
                        </div>
                        <div className="asmb-mid">
                            <h3>Sort By</h3>
                            <div className="asmbm-col">
                                <label>Column</label>
                                <select>
                                    <option>Section</option>
                                </select>
                            </div>
                            <div className="asmbm-col">
                                <label>Order</label>
                                <select>
                                    <option>Ascending (A-Z)</option>
                                </select>
                            </div>
                            <button type="button" className="asmbm-drag"></button>
                        </div>
                        <div className="asmb-mid">
                            <h3>Sort Then</h3>
                            <div className="asmbm-col">
                                <label>Column</label>
                                <select>
                                    <option>Department</option>
                                </select>
                            </div>
                            <div className="asmbm-col">
                                <label>Order</label>
                                <select>
                                    <option>Descending (Z-A)</option>
                                </select>
                            </div>
                            <button type="button" className="asmbm-drag"></button>
                        </div>
                        <div className="asmb-foot">
                            <div className="asmb-plus-minus">
                                <button type="button" className="asmb-add"><i class="fa fa-plus"></i></button>
                                <button type="button" className="asmb-minus"><i class="fa fa-minus"></i></button>
                            </div>
                            <button type="button" className="asmb-copy"><i class="fa fa-copy"></i> Copy</button>
                        </div>
                    </div>
                    <div className="asm-footer">
                        <button type="button">Apply</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdvanceSorting;