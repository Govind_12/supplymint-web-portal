import React from 'react';
import alert from '../../assets/alert-2.svg';
import ToastLoader from "./toastLoader";

class ItemFound extends React.Component {
    constructor(props) {
        super(props);
     this.state={
	   piCreateResponse:[],
	   confirmModal:false,
	   toastLoader:false,
	   toastMsg:""

	 } ; 
	}
	
componentWillMount(){
if(this.props.purchaseIndent.piCreate.isSuccess){
	if (this.props.purchaseIndent.piCreate.data.resource!=null){
              this.setState({
                     piCreateResponse:this.props.purchaseIndent.piCreate.data.resource

			  })

	}
}

}


componentWillReceiveProps(nextprops){
	if(nextprops.purchaseIndent.piCreate.isSuccess){
		if (nextprops.purchaseIndent.piCreate.data.resource!=null){
				  this.setState({
						 piCreateResponse:nextprops.purchaseIndent.piCreate.data.resource
	
				  })
	
		}
	}
	
	}
closeConfirmModal()
{
this.setState({
	confirmModal:true
})

}
	notFoundData(){
let cn =this.state.piCreateResponse
this.props.closeItemNotFound();
this.props.piNotMatchRequest(cn);
this.props.piCreateClear();


	}

	foundData(){
		let cn =this.state.piCreateResponse;
	



		if(cn.piKey.fpiDetails.length!=0){
			cn.piKey.invItem= false;
			cn.piKey.foundData= true;
			this.props.piMatchRequest(cn);
			this.props.closeItemNotFound();
			this.props.piCreateClear();
		}else{
			this.setState({
				toastLoader:true,
				toastMsg:"There is No Found Data"
			})
			const t=this
            setTimeout(function(){
             t.setState({
                 toastLoader:false
             })
            },1000)
		}
		this.setState({
			piCreateResponse:cn
		})
	
	}
    render() {
	

        return (
            <div className="warning">
            <div className="warning-two-item">
		<div className="col-md-12 modal-dialog">
			<div className="background-blur"></div>
			<div className="modal-content">
				<div className="modal-data ">
					<div className="modal-header">
					<div className="icon">
                    <img src={alert}/>
					</div>
					<h2>Warning!</h2>
						<p>There is no data found in the database do you want to save ?</p>
						{/* <p><span>Note :</span> On “ NO ”  selection data will be saved without new entries.</p>  */}
					<div className="modal-footer">
						<button className="confirm-btn" type ="button" onClick={(e)=>this.notFoundData(e)}>Yes</button>
						<button className="close-btn" type="button" onClick={(e)=>this.foundData(e)}>No</button>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	{/* {this.state.confirmModal ?<SaveModal closeConfirmModal={()=>this.closeConfirmModal()} piResource={this.props.piResource}/>:null} */}
	{this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} />:null}
	    </div>
    
   
        );
    }
}

export default ItemFound;