import React from 'react';
import success from '../../assets/success.svg';


class RequestSuccess extends React.Component {

    render() {
        return (
            <div className="display_block"  id="requestsuccessModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            
            
        <div className="modal modal-success successModalDiv successRequestModal" role="document">
        <div className="backdrop"></div>
            <div className="modal-content">
                <div className="requestsuccess">
                    {/* <div className="col-md-12 col-sm-12 "> */}
                        {/* <div className=""> */}
                            {/* <button type="button" className="cutting" onClick={(e)=>this.props.closeRequest(e)} aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button> */}
                        {/* </div> */}
                        <div className="success_container">
                            <ul className="list_style pad-0">
                                <li>
                                    {this.props.isDownloadPage !== undefined && this.props.isDownloadPage ? <img src={require('../../assets/alert-2.svg')} className="sc-alert-img" />
                                    : <img src={success} className="success_img"/> }
                                </li>
                            </ul>
                            <ul className="list_style width_100 pad-0 m-top-10">
                                <li>
                                    <label className="success_heading selectText">
                                            {this.props.successMessage}
                                    </label>
                                </li>
                                {/* <li>
                                    <label className="success_text">
                                            Ref #74657874657fc
                                    </label>
                                </li>
                                <li>
                                    <label className="success_text">
                                            Type something
                                    </label>
                                </li> */}
                            </ul>
                            <ul className="list_style width_100 pad-0 m-top-20">
                                <li>
                                    <button type="button" className="success_button" onClick={(e)=>this.props.closeRequest(e)}>OK</button>
                                </li>
                            </ul>
                        </div>
                    {/* </div> */}
                </div>
            </div>
        </div>
    </div>
        
    
   
        );
    }
}

export default RequestSuccess;