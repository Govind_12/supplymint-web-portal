import React from 'react';

class ConfirmationSaveModal extends React.Component {
    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content save-confirmation-modal">
                    <div className="scm-inner">
                        <img src={require('../../assets/question.svg')} />
                        <h3>Are you sure you want to save changes ?</h3>
                        <p>By updating these settings the master configuration will be updated and changes reflects to relavant modules</p>
                        <div className="scmi-btn">
                            <button type="button" className="scmi-confirm" onClick={this.props.confirm}>Confirm</button>
                            <button type="button" className="" onClick={this.props.discard}>Discard</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ConfirmationSaveModal;