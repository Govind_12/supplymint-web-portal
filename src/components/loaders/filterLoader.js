import React from 'react';
class FilterLoader extends React.Component {
    render() {
        return (
            <div>
                <div className="backdroploader"></div>
                <div className="circle-loader filter-loader">
                    <div className="oval-shape">
                    </div>
                    <div className="loader-icon">
                    </div>
                    <div className="loader-txt">
                        <h2>Processing...</h2>
                    </div>
                </div>
            </div>
        );
    }
}

export default FilterLoader;