import React from 'react';
import alertIcon from '../../assets/alert-2.svg';


class SaveData extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		
	
		};
	  }
onSubmit(){
	this.props.piCreateRequest(this.props.savePayload)
	this.props.closeConfirmModal()
}
    render() {

        return (
            <div>
            <div className="save-current-data">
			<div className="col-md-12 modal-dialog">
				<div className="background-blur"></div>
				<div className="modal-content">
					<div className="modal-data ">
						<div className="modal-header">
						<div className="icon">
						<img src={alertIcon}/>
						</div>
						<h2>Are you sure you want to save current Data ?</h2>
						<p>Confirming this action will save your all data. please ensure that you have 
							correctly entered all the data because this cannot be undone.</p>
						<div className="modal-footer">
							<button className="close-btn"type ="button" onClick={(e)=>this.props.closeConfirmModal(e)}>Close</button>
							<button className="confirm-btn" type="button" onClick={(e)=>this.onSubmit(e)}>Confirm</button>
						</div>
					</div>
					</div>
				</div>
			</div>
	</div>
        </div>
    
   
        );
    }
}

export default SaveData;