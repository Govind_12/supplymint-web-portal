import React from 'react';
import alertIcon from '../../assets/alert-2.svg';
import crossIcon from '../../assets/cross.svg';


class ConfirmClearForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            poEdit : false,
        };
    }

    componentDidMount() {
        let poEdit = this.props.poEdit == undefined ? false : this.props.poEdit;
        this.setState({
            poEdit : poEdit
        })
        console.log('poEdit', poEdit, this.props.poEdit)
    }

    onSubmit() {
        this.props.confirmClear()
    }
    closeIt() {
        this.props.closeModal()
    }

    render() {
        return (
            <div>
                <div className="save-current-data modalPos" onClick={() => this.closeIt()}>
                    <div className="col-md-12 modal-dialog m0">
                        <div className="background-blur"></div>
                        <div className="modal-content">
                            <div className="modal-data ">
                                <div className="modal-header">
                                    <div className="icon">
                                        <img src={alertIcon} />
                                    </div>
                                    <div className="icon" style={{ float: "right" }}>
                                        <img src={crossIcon} />
                                    </div>
                                    <h2>Do you want to discard {this.props.poEdit ? 'changes' : 'document'}</h2>
                                    <p></p>
                                    <div className="modal-footer">
                                        <button className="close-btn" type="button" onClick={(e) => this.onSubmit(e)} >Discard {this.props.poEdit ? 'Changes' : 'Document'}</button>
                                        <button className="confirm-btn" type="button" onClick={this.props.cancelClearForm}>{this.state.poEdit ? 'Submit' : 'Save As Draft'}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}

export default ConfirmClearForm;