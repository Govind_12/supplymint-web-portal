import React from 'react';
import alertIcon from '../../assets/alert-2.svg';
class ResetAndDelete extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render() {

		return (
			<div>
				<div className="save-current-data modalPos">
					<div className="col-md-12 modal-dialog m0">
						<div className="background-blur"></div>
						<div className="modal-content">
							<div className="modal-data ">
								<div className="modal-header">
									<div className="icon">
										<img src={alertIcon} />
									</div>
									<h2 style={{fontWeight: '500', fontSize: '12px'}}>{this.props.headerMsg}</h2>
									<p>{this.props.paraMsg}</p>
									<div className="modal-footer">
										<button className="close-btn" type="button" onClick={(e) => this.props.closeModalHandler(e)}>Close</button>
										<button className="confirm-btn" type="button" onClick={(e) => this.props.rightBtnHandler(e)}>{this.props.rightBtnName}</button>
										<button className="confirm-btn" type="button" onClick={(e) => this.props.leftBtnHandler(e)}>{this.props.leftBtnName}</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		);
	}
}

export default ResetAndDelete;