import React from 'react';

class SmallLoader extends React.Component {

    render() {
        return (
            <div className="sm-loader"></div>
        )
    }
}

export default SmallLoader;