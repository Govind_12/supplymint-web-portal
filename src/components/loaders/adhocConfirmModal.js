import React from 'react';
import alertIcon from '../../assets/alert-2.svg';


class AdhocConfirmModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		
	
		};
	  }
onSubmit(){ 
    var data={
        itemData:this.props.itemCode,
        workOrder:this.props.workOrderId
    }
    this.props.deleteItemCodeRequest(data)
	this.props.closeAdhocModal(); 
	this.props.closeConfirmModal()
}
closeAdHocModal(){
	this.props.cancelAdhocRequest(this.props.orderId);
	this.props.closeConfirmModal()
}
componentWillReceiveProps(nextProps){
	
}
 
    render() {

        return (
            <div>
            <div className="save-current-data modalPos">
			<div className="col-md-12 modal-dialog m0">
				<div className="background-blur"></div>
				<div className="modal-content">
					<div className="modal-data ">
						<div className="modal-header">
						<div className="icon">
						<img src={alertIcon}/>
						</div>
						<h2>{this.props.headerMsg}</h2>
						<p>{this.props.paraMsg}</p>
						<div className="modal-footer">
							<button className="close-btn"type ="button" onClick={(e)=>this.props.paraMsg ==this.props.closeConfirmModal(e)}>Close</button>
							<button className="confirm-btn" type="button" onClick={(e)=>this.props.paraMsgAd == "" ?  this.closeAdHocModal(e) : this.onSubmit(e)}>Confirm</button>
						</div>
					</div>
					</div>
				</div>
			</div>
	</div>
        </div>
    
   
        );
    }
}

export default AdhocConfirmModal;