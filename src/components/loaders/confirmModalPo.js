import React from 'react';
import alertIcon from '../../assets/alert-2.svg';


class ConfirmModalPo extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};
	}
	onSubmit() {
		if ((this.props.pi == "" || this.props.pi == undefined)) {
			this.props.onnRadioChange(this.props.radioChange);
		}

		else {
			// this.props.resetData();
		}
		this.props.closeConfirmModal()
	}
	componentWillReceiveProps(nextProps) {

	}

	render() {

		return (
			<div>
				<div className="save-current-data modalPos">
					<div className="col-md-12 modal-dialog m0">
						<div className="background-blur modal-backdrop-new"></div>
						<div className="modal-content">
							<div className="modal-data ">
								<div className="modal-header">
									<div className="icon">
										<img src={alertIcon} />
									</div>
									<h2>{this.props.headerMsg}</h2>
									<p>{this.props.paraMsg}</p>
									<div className="modal-footer">
										<button className="close-btn" type="button" onClick={(e) => this.props.closeConfirmModal(e)}>Close</button>
										<button className="confirm-btn" type="button" onClick={(e) => this.onSubmit(e)}>Confirm</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		);
	}
}

export default ConfirmModalPo;