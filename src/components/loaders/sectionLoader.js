import React from 'react';

class SectionLoader extends React.Component {

    render() {
        return (<div className="small-circle-loader">
                <div className="oval-shape-small">
                </div>
               
               <div className="loader-txt">
                   
                    <h2>Processing...</h2>
                </div>
            </div>
        )
    }
}

export default SectionLoader;