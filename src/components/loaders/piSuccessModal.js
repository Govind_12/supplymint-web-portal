import React from 'react';
import error from '../../assets/error_icon.svg';


class PiSuccessModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            piData: this.props.piData,
            Division: "",
            Section: "",
            Department: ""


        };
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.piCreate.isSuccess) {
            if (nextProps.purchaseIndent.piCreate.data.resource != null) {
                //   let piData = nextProps.purchaseIndent.piCreate.data.resource
                this.setState({
                    Division: nextProps.purchaseIndent.piCreate.data.resource.piKey.hl1Name,
                    Section: nextProps.purchaseIndent.piCreate.data.resource.piKey.hl2Name,
                    Department: nextProps.purchaseIndent.piCreate.data.resource.piKey.hl3Name

                })

            }
        }

    }
    componentWillMount() {

        if (this.props.purchaseIndent.piCreate.isSuccess) {
            if (this.props.purchaseIndent.piCreate.data.resource != null) {
                // let piData = this.props.purchaseIndent.piCreate.data.resource
                this.setState({
                    Division: this.props.purchaseIndent.piCreate.data.resource.piKey.hl1Name,
                    Section: this.props.purchaseIndent.piCreate.data.resource.piKey.hl2Name,
                    Department: this.props.purchaseIndent.piCreate.data.resource.piKey.hl3Name
                })

            }
        }
    }

    render() {

        return (
            <div className="display_block" id="requestsuccessModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">


                <div className="modal modal-success successModalDiv successRequestModal" role="document">
                    <div className="backdrop"></div>
                    <div className="modal-content piSaveSuccessModal">
                        <div className="requestsuccess oflHidden">
                            {/* <div className="col-md-12 col-sm-12 "> */}
                            {/* <div className=""> */}
                            <button type="button" className="cutting" onClick={(e) => this.props.closePiRequest(e)} aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {/* </div> */}
                            <div className="success_container">
                                <ul className="list_style pad-0">
                                    <li>
                                        <img src={error} className="success_img" />
                                        <label className="success_heading pad-lft-20">
                                            {this.props.piSuccessMessage}
                                        </label>
                                    </li>
                                </ul>
                                {/* <ul className="list_style width_100 pad-0 m-top-10"> */}
                                {/* <li>
                                    <label className="success_heading">
                                            {this.props.piSuccessMessage}
                                    </label>
                                </li> */}
                                {/* <li>
                                    <label className="success_text">
                                            Ref #74657874657fc
                                    </label>
                                </li>
                                <li>
                                    <label className="success_text">
                                            Type something
                                    </label>
                                </li> */}
                                {/* </ul> */}

                                <div className="col-md-12 col-sm-12 m-top-10">
                                    <div className="adHocModalTable">
                                        <table>
                                            <thead>
                                                <th>Article Code</th>
                                                <th>Article Name</th>
                                                <th>Division</th>
                                                <th>Section</th>
                                                <th>Department</th>
                                                <th>Unique</th>
                                                <th>Brand</th>
                                                <th>Pattern</th>
                                                <th>Assortment</th>
                                                <th>Size</th>
                                                <th>Color</th>
                                                <th>Vendor Design</th>
                                                <th>Season</th>
                                                <th>Fabric</th>
                                                <th>Darwin</th>
                                                <th>Weaved</th>
                                            </thead>

                                            <tbody>
                                                {this.state.piData.piKey.piDetails.map((data, key) => (<tr key={key}>

                                                    <td>{data.hl4Code}</td>
                                                    <td>{data.hl4Name}</td>
                                                    <td>{this.state.Division}</td>
                                                    <td>{this.state.Section}</td>
                                                    <td>{this.state.Department}</td>
                                                    <td>{data.uniqueName}-{data.uniqueCode}</td>
                                                    <td>{data.brandName}-{data.brandCode}</td>
                                                    <td>{data.patternName}-{data.patternCode}</td>
                                                    <td>{data.assortmentName}-{data.assortmentCode}</td>
                                                    <td>{data.sizeName}-{data.sizeCode}</td>
                                                    <td>{data.colorName}-{data.colorCode}</td>
                                                    <td>{data.design}</td>
                                                    <td>{data.seasonName}-{data.seasonCode}</td>
                                                    <td>{data.fabricName}-{data.fabricCode}</td>
                                                    <td>{data.darwinName}-{data.darwinCode}</td>

                                                    <td>{data.weavedName}-{data.weavedCode}</td>
                                                </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>



                                <ul className="closeModalPi list_style width_100 pad-0 m-top-20 textCenter m-top-30 ">
                                    <li>
                                        {/* <button type="button" className="success_button" onClick={(e)=>this.props.closePiRequest(e)}>OK</button> */}
                                        {/* <button type="button" className="success_button" onClick={(e) => this.props.closePiRequest.push("/purchase/purchaseIndents")}>OK</button> */}
                                        <button type="button" className="success_button" onClick={() => this.props.history.push("/purchase/purchaseIndents")}>OK</button>
                                    </li>
                                </ul>
                            </div>
                            {/* </div> */}
                        </div>
                    </div>
                </div>
            </div>



        );
    }
}

export default PiSuccessModal;