import React from 'react';

class ToastLoader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showToast: false
        }
    }
    render() {
        return (
            <div>
                <div id="snackbar" className="show">
                    <div className="toastImg">
                        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53">
                            <defs>
                                <circle id="b" cx="16.5" cy="16.5" r="16.5" />
                                <filter id="a" width="190.9%" height="190.9%" x="-45.5%" y="-45.5%" filterUnits="objectBoundingBox">
                                    <feMorphology in="SourceAlpha" radius=".5" result="shadowSpreadOuter1" />
                                    <feOffset in="shadowSpreadOuter1" result="shadowOffsetOuter1" />
                                    <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="5.5" />
                                    <feColorMatrix in="shadowBlurOuter1" values="0 0 0 0 0.788318452 0 0 0 0 0.788318452 0 0 0 0 0.788318452 0 0 0 0.5 0" />
                                </filter>
                            </defs>
                            <g fill="none" fillRule="nonzero">
                                <g transform="translate(10 10)">
                                    <use fill="#000" filter="url(#a)" href="#b" />
                                    <use fill="#FFF" href="#b" />
                                </g>
                                <g fill="#6D6DC9">
                                    <path d="M28.674 27.229c0 .808-.703 1.464-1.57 1.464-.866 0-1.568-.656-1.568-1.464v-6.335c0-.81.702-1.465 1.569-1.465.866 0 1.569.656 1.569 1.465v6.335zM28.674 33.062c0 .81-.703 1.465-1.57 1.465-.866 0-1.568-.656-1.568-1.465v-1.767c0-.81.702-1.465 1.569-1.465.866 0 1.569.656 1.569 1.465v1.767z" />
                                </g>
                            </g>
                        </svg>

                    </div>
                    <div className="contentToastDiv">
                        {/* <h2>Record Deleted Successfully</h2> */}

                        <p>
                          {this.props.toastMsg}
                        </p>
                    </div>
                </div>
            </div >

        );
    }
}

export default ToastLoader;