import React from 'react';

class ProcessLoader extends React.Component {

    render() {
    
        return (
            <div>
{/* 
                 <div className="loaderDiv">
                     </div>  */}
                <div className="backdroploader"></div>
                    <div className="circle-loader filter-loader">
                        {/* <div className="oval-shape">
                        </div>
                        <div className="loader-icon">
                        </div> */}
                        <div className="loader-txt">                        
                            <h2>Your OTB plan creation is under progress please wait for sometime!!</h2>
                        </div>
                    </div>
                </div>

        );
    }
}

export default ProcessLoader;