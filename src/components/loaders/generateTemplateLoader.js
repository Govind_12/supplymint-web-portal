import React from 'react'

function TemplateLoader() {
    return <div className="templateLoaderMain">
            <div className="loaderContent">
            <h4>Loading ! please wait…</h4>
            <p>generating template</p>
            </div>
        </div>
  }
  export default TemplateLoader