import React from 'react';
import alertIcon from '../../assets/alert-2.svg';
class ResetAndDelete extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}


	onSubmit() {

		if (this.props.rowDelete != "" ) {
		
			this.props.DeleteRowPo(this.props.rowsId,this.props.otbValueData,this.props.mrp,this.props.mrpValueData)
		}
		else {
			this.props.resetRows();
		}
		this.props.closeResetDeleteModal()
	}


	render() {

		return (
			<div>
				<div className="save-current-data modalPos">
					<div className="col-md-12 modal-dialog m0">
						<div className="background-blur"></div>
						<div className="modal-content">
							<div className="modal-data ">
								<div className="modal-header">
									<div className="icon">
										<img src={alertIcon} />
									</div>
									<h2>{this.props.headerMsg}</h2>
									<p>{this.props.paraMsg}</p>
									<div className="modal-footer">
										<button className="close-btn" type="button" onClick={(e) => this.props.closeResetDeleteModal(e)}>Close</button>
										<button className="confirm-btn" type="button" onClick={(e) => this.onSubmit(e)}>Confirm</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		);
	}
}

export default ResetAndDelete;