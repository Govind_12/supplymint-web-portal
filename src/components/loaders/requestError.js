import React from 'react';
import InternalServerError from "../errorPage/internalServerError";
import InternetError from "../errorPage/internetError";
import PageNotFound from "../errorPage/pageNotFound";
import UnauthorisedAccess from "../errorPage/unauthorisedAccess";
import AlertPopUp from "../errorPage/alertPopUp";


class RequestError extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: this.props.code
        };
    }
    componentWillMount() {
        this.setState({
            error: this.props.code
        })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            error: nextProps.code
        })

    }

    onLogout =(e)=> {
        sessionStorage.clear();
        window.location.href = '/';
        // if( this.props !== undefined && this.props.history !== undefined )
        //    this.props.history.push('/');  
    }

    render() {
        // stopping all the keys commented for once need to check later
        // document.onkeydown = function (t) {
        //     if (t.which) {
        //         return false;
        //     }
        // }
        let erroroccured = 0;
        if (this.state.error === "4000") {
            erroroccured++;
        }
        return (
            <div className={this.props.moduleError == undefined ? "requestErrorMain" : "requestErrorMain posInherit"}>
                {erroroccured > 0 ? <div className="modal display_block" id="confirmModal"><div className="backdrop"></div>
                    <div className="modal-content modalOrganisationAlert modalUpdate">

                        <div className="col-md-12 col-sm-12 pad-top-10">
                            <div className="modal_confirmation">
                                <ul className="list-inline modalListOrganisation">
                                    <li>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                            <path fill="#c44569" fillRule="evenodd" d="M9 13h2v2H9v-2zm0-8h2v6H9V5zm.99-5C4.47 0 0 4.48 0 10s4.47 10 9.99 10C15.52 20 20 15.52 20 10S15.52 0 9.99 0zM10 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z" />
                                        </svg>
                                    </li>
                                    <li className="float_initial">
                                        <p>
                                            {this.props.errorMessage == "" ? "Api not calling to the Server OR Check your Internet Connection" : this.props.errorMessage == undefined ? "No Internet Access" : this.props.errorMessage}
                                        </p>
                                        {/* <span>
                                    Note  -  This process is irreversible. data will be permanently deleted.
                                </span> */}
                                    </li>

                                    <li>
                                        <button type="button" className="success_button" onClick={(e) => {this.props.errorMessage === "session expired please login again !!" ? this.onLogout(e) : this.props.closeErrorRequest(e)}}>OK</button>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div></div> : null}
                {/* {this.props.code == "5000" && this.props.moduleError == undefined ? <InternalServerError errorMessage={this.props.errorMessage} errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} /> : null} */}
                {this.props.code == "5000" && this.props.moduleError == undefined ? <AlertPopUp errorMessage={this.props.errorMessage} errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} /> : null}

                {this.props.code == "5000" && this.props.moduleError == "procurement" ? <AlertPopUp errorMessage={this.props.errorMessage} errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} /> : null}
                {this.props.code == "5001" ? <AlertPopUp errorMessage={this.props.errorMessage} errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} /> : null}
                {this.props.code == undefined && this.props.moduleError == "procurement" ? <AlertPopUp errorMessage="Unable to connect to the server or check your internet connection" errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} />
                    : this.props.code == undefined && <InternetError closeErrorRequest={(e) => this.props.closeErrorRequest(e)} />}
                {this.props.code == "4040" ? <PageNotFound errorMessage={this.props.errorMessage} errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} /> : null}
                {this.props.code == "4010" ? <UnauthorisedAccess errorMessage={this.props.errorMessage} errorCode={this.props.errorCode} closeErrorRequest={(e) => this.props.closeErrorRequest(e)} /> : null}
            </div>
        );
    }
}

export default RequestError;