import React from "react";
import shape from "../assets/shape.svg";

class Notification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clearNotifications: false
    };
  }
  clearNotify(e) {
    e.preventDefault();
    this.setState({
      clearNotifications: true
    });
    this.props.resetNotifyCount();
  }
  render() {

    return (
      <div className="modal">
        <div className="backdrop modal-backdrop-new"></div>
          <div className="modal-content notification-modal">
            <div className="nm-inner">
              <img src={require('../assets/blank-notification-icons.png')} />
              <h3>NO NOTIFICATIONS</h3>
              <p>We will notify you once we have something for you</p>
              <div className="mni-close">
                <span className="nmic-esc">Esc</span>
                  <button type="button" className="nmi-close-btn" onClick={this.props.CloseNotification}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                          <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                      </svg>
                  </button>
              </div>
            </div>
          </div>
      {/*<div className={this.props.notifyShow ? "notification_dropdown notifi-drop-pos" : "notification_dropdown hideNotify"}>
         <div className="col-md-12 col-sm-12 m-top-bottom-10">
          <ul className="list-inline width_100">
            <li className="float_left">
              <h2>Notifications</h2>
            </li>
            {!this.state.clearNotifications ? (
              <li className="float_right">
                <button
                  onClick={(e) => this.clearNotify(e)}
                  className="notification_button"
                >
                  Clear
                </button>
              </li>
            ) : null}
          </ul>
        </div>
        {!this.state.clearNotifications ? (
          <div className="col-md-12 col-sm-12">
            <div className="border_top">
              <div className="col-md-8 col-sm-12 pad-0">
                <ul className="list-inline">
                  <li>
                    <img src={shape} className="noti_img" />
                  </li>
                  <li>
                    <label className="notification_content">
                      Profile updated successfully
                    </label>
                  </li>
                </ul>
              </div>
              <div className="col-md-4 col-sm-12 pad-0">
                <ul className="list-inline text_align_right">
                  <li>
                    <span className="notification_dd">20-july-2018</span>
                    <ul className="list-inline text_align_right">
                      <li>
                        <span className="notification_dd">9:30 am</span>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        ) : (
          <div className="col-md-12 col-sm-12">
            <div className="border_top">
              <div className="col-md-12 col-sm-12 pad-0">
                <ul className="list-inline">
                  <li>
                    <label className="notification_alert">
                      {" "}
                      No Notifications As Of Now{" "}
                    </label>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        )} 
      </div>*/}
    </div>
    );
  }
}

export default Notification;
