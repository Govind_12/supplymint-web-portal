import React from 'react';
import { Link } from 'react-router-dom';
import AdminDrop from './adminDrop';
import VendorDrop from './vendorDrop';
import PurchaseDrop from './purchaseDrop';
import ReplenishmentDrop from './replenishmentDrop';
import DemandPlanningDrop from './demanPlanningDrop';
import InventoryPlanningDrop from './inventoryPlanningDrop';
import OrderDrop from './orderDrop';
import ShipmentDrop from './shipmentDrop';
import LrDrop from "./lrDrop";
import Slash from "../assets/slash-new.svg";
import RightArrow from "../assets/rightArrow.svg";
var array = {}
class BraedCrumps extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modules: [],
            currentPage: "",
            breadCrumbs: []
        }
    }
    componentDidMount() {
        this.setState({ modules: JSON.parse(sessionStorage.getItem('modules')), currentPage: sessionStorage.getItem('currentPage') })
    }
    
    formatData = (root) => {
        if (root.subModules !== undefined) {
            array[root.code] = root.subModules;
            return (
                root.subModules !== null && root.subModules.map(node => {
                    if (node.subModules != undefined) {
                        array[node.code] = node.subModules
                        this.formatData(node.subModules);
                    } else {
                        array[node.code] = root;
                    }
                })
            )
        }
        else {
            root.forEach(subRoot => {
                if (subRoot.subModules != null) {
                    array[subRoot.code] = subRoot.subModules;
                    return (
                        subRoot.subModules !== null && subRoot.subModules.map(node => {
                            if (node.subModules != undefined) {
                                array[node.code] = node.subModules
                                this.formatData(node.subModules);
                            } else {
                                array[node.code] = subRoot;
                            }
                        })
                    )
                }
            })
        }
    }
    renderCurrentPage(data) {
        if (data.subModules !== null) {
            return (
                <li className="breadcrump-sub-menu">
                    {data != undefined && <span className="bsm-head"><span className="sm-head-inner">{data.name}</span><i className="fa fa-chevron-right"></i>
                        <ul className="breadcrump-sub-menu2">
                            {data.subModules.map(node => (
                                <li className="bsm2-item">{node.subModules != undefined && (`${node.name}`)}{node.subModules != undefined && <img className="drop-menu-right-arrow" src={RightArrow} />}{this.renderCurrentPage(node)}</li>
                            ))}</ul>
                    </span>}

                </li>
            );
        }
        else {
            return <span className="bm-inner-item" onClick={() => this.openPage(data)}>{data.name}</span>
        }
    }
    openPage = (root) => {
        this.props.history.push(root.pageUrl.replace('#', ''))
        sessionStorage.setItem('currentPage', root.parentCode)
        sessionStorage.setItem('currentPageName', root.name)
    }
    clearSession = () => {
        sessionStorage.setItem('currentPage', "")
        sessionStorage.setItem('currentPageName', "")
    }

    render() {
        return (
            <div>
                <ul className="list-inline breadcrumbsList breadCrumbsHeight width_100 pad20">
                    <li>
                        <label className="home_link" onClick={this.clearSession}><Link to="/home">Home  </Link> </label>
                    </li>
                    <li className="breadcrumps-menu-item">
                        {/* <label className="home_link">{bhash == "Home" ? null : <span><img className="fa" src={Slash} /></span>}  {bhash == "Administration" ? <AdminDrop /> : hash[0] == "Logistics" ? <LrDrop /> : bhash == "Vendor" && hash != "Shipment" && bhash != "Enterprise" && hash[0] != "Purchase" ? <VendorDrop /> : bhash == "Purchase" ? <PurchaseDrop /> : hash == "Shipment" ? <ShipmentDrop /> : bhash == "Vendor" || bhash == "Enterprise" && hash[0] == "Purchase" ? <OrderDrop /> : bhash[0] == "Demand" ? <DemandPlanningDrop /> : bhash == "Replenishment" ? <ReplenishmentDrop /> : bhash == "Home" ? "" : bhash[0] == "Inventory" ? <InventoryPlanningDrop /> : bhash[0] == "Change" ? "Change Settings" : bhash[0] == "Profile" ? <Link to="../profile">Profile</Link> : bhash} </label> */}
                        {window.location.hash.split('/')[1] !== "home" && <label className="home_link"><span><img className="fa" src={Slash} /></span><div className="dropdown" id="myFav"><button className="dropbtn home_link">{sessionStorage.getItem('parentName') !== "" && <React.Fragment>{sessionStorage.getItem('parentName')}</React.Fragment>} <i className="fa fa-chevron-down"></i></button></div></label>}
                        {sessionStorage.getItem("currentPage") !== "" &&
                            <ul className="breadcrump-menu">
                                {array[sessionStorage.getItem('currentPage')] !== undefined && array[sessionStorage.getItem('currentPage')].map((_) => (
                                    this.renderCurrentPage(_)
                                ))}
                            </ul>
                        }
                    </li>
                    <li className="home_link">{sessionStorage.getItem('currentPageName') !== "" ? <React.Fragment><span><img className="fa" src={Slash} /></span> {sessionStorage.getItem('currentPageName')}</React.Fragment> : ""}</li>
                    {sessionStorage.getItem("currentPage") !== "" && <ul>
                        {this.state.modules[sessionStorage.getItem('mid')] != undefined &&
                            this.state.modules[sessionStorage.getItem('mid')].map((_) => (
                                <li className="menu-list-item"><span className="menu-item-list-inner">
                                    {/* <span className="mli-icon"></span><span className="mli-name">{_.name}{_.subModules != undefined && <i className="fa fa-angle-down arrow_down_icon" />}</span>*/}
                                </span>
                                    {this.formatData(_)}
                                </li>
                            ))}
                    </ul>}

                </ul>
            </div>

        );
    }
}

export default BraedCrumps;