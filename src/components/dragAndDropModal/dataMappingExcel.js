import React from 'react';
import backButtonDrag from "../../assets/back-button.svg";
class DataMappingExcel extends React.Component {
    scrollDiv(e){
        var target = $("#target");
            $("#source").scroll(function () {
                target.prop("scrollTop", this.scrollTop)
                    .prop("scrollLeft", this.scrollLeft);
            });
    }
    render() {
        return (
            <div id="left">
                <div className="modal">
                    <div className="modal-content modalContentDraganddrop">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">


                            <div className="col-md-4 col-sm-12 col-xs-12 pad-0">
                                <div className="modalLeftSideContainer">
                                    <div className="backButton">
                                        <ul className="list-inline">

                                            <li>
                                                <img src={backButtonDrag} className="backbuttonImg" />
                                                <h3>Back</h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="leftSideContent m-top-50">
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Choose File
                                    </h2>
                                            <p>
                                                Step 1 completed with no errors
                                    </p>

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c2" name="cb" />
                                            <label htmlFor="c2" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-8 co-sm-12 col-xs-12 pad-0">

                                <div className="modalRightSideContainer">
                                    <div className="headercontentDragdrop1">
                                        <h2>
                                            Data Mapping
                                        </h2>
                                        <h3>
                                            Map coloumns with respective headers
                                        </h3>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0 mtop5">
                                        <div className="col-md-4 col-xs-6 pad-0">
                                            <div className="dropdownscroll">
                                                <h2>
                                                    DB Coloumns
                                                </h2>
                                                <div className="dbColumDropDown mtop5">
                                                    <ul className="list-inline" onScroll={(e) => this.scrollDiv(e)} id="target">
                                                        <li>
                                                            <span>Organisation</span>
                                                        </li>
                                                        <li>
                                                            <span>Vendor Organisation</span>
                                                        </li>
                                                        <li>
                                                            <span>Vendor Code</span>
                                                        </li>
                                                        <li>
                                                            <span>Description</span>
                                                        </li>
                                                        <li>
                                                            <span>City</span>
                                                        </li>
                                                        <li>
                                                            <span>State</span>
                                                        </li>
                                                        <li>
                                                            <span>Organisation</span>
                                                        </li>
                                                        
                                                        <li>
                                                            <span>Description</span>
                                                        </li>
                                                        <li>
                                                            <span>City</span>
                                                        </li>
                                                        <li>
                                                            <span>State</span>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2 m_top_22">
                                            <div className="svgDividerImg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="53" height="9" viewBox="0 0 53 9">
                                                    <rect width="53" height="9" x="1062" y="557" fill="#6D6DC9" fillRule="nonzero" rx="4.5" transform="translate(-1062 -557)" />
                                                </svg>

                                            </div>
                                        </div>
                                        <div className="col-md-4 col-xs-6 pad-0">
                                            <div className="dropdownscroll">
                                                <h2>
                                                    DB Coloumns
                                                </h2>
                                                <div className="dbColumDropDown1 mtop5">
                                                    <ul className="list-inline " onScroll={(e) => this.scrollDiv(e)} id="source">
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    Organisation
                                                                </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    Vendor
                                                               </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    Vendor Code
                                                               </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    Description
                                                                </option>
                                                            </select>

                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    City
                                                               </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    State
                                                               </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    Vendor Code
                                                               </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    Description
                                                                </option>
                                                            </select>

                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    City
                                                               </option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <select>
                                                                <option>
                                                                    State
                                                               </option>
                                                            </select>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="footercontentDragdrop1">
                                        <ul className="list-inline width_100">

                                            <li>
                                                <button className="footerBtnData">
                                                    NEXT
                                                </button>
                                            </li>
                                            <li>
                                                <span>
                                                    Need Help ?
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default DataMappingExcel;