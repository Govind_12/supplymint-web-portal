import React from 'react';
import backButtonDrag from "../../assets/back-button.svg";
class SuccessStatus extends React.Component {
    render() {

        return (
            <div>

                <div className="modal">
                    <div className="modal-content modalContentDraganddrop">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">


                            <div className="col-md-4 col-sm-12 col-xs-12 pad-0">
                                <div className="modalLeftSideContainer">
                                    <div className="backButton">
                                        <ul className="list-inline">

                                            <li>
                                                <img src={backButtonDrag} className="backbuttonImg" />
                                                <h3>Back</h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="leftSideContent m-top-50">
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Choose File
                                    </h2>
                                            <p>
                                                Step 1 completed with no errors
                                    </p>

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c2" name="cb" />
                                            <label htmlFor="c2" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-7 co-sm-12 col-xs-12 pad-0">

                                <div className="modalRightSideContainer ">
                                    <div className="headercontentDragdrop">
                                        <h2>
                                            Status
                                        </h2>

                                    </div>
                                    <div className="containerSuccessContent">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="184" height="184" viewBox="0 0 184 184">
                                            <g fill="none" fillRule="evenodd">
                                                <circle cx="92" cy="92" r="92" fill="#F8F8F8" fillRule="nonzero" opacity=".467" />
                                                <circle cx="91" cy="94" r="67" fill="#F7F7F7" fillRule="nonzero" />
                                                <path fill="#C44569" d="M117.47 120.47A35.912 35.912 0 0 0 128 95a35.912 35.912 0 0 0-10.53-25.47A35.912 35.912 0 0 0 92 59a35.912 35.912 0 0 0-25.47 10.53A35.912 35.912 0 0 0 56 95a35.912 35.912 0 0 0 10.53 25.47A35.912 35.912 0 0 0 92 131a35.912 35.912 0 0 0 25.47-10.53zm-20.6-11.583a4.86 4.86 0 0 0-4.87-4.87c-2.698 0-4.936 2.171-4.936 4.87 0 2.698 2.238 4.936 4.936 4.936s4.87-2.238 4.87-4.936zM92 99.08c-1.777 0-3.093-1.447-3.29-3.29l-1.646-14.677c-.263-2.566 2.435-4.936 4.936-4.936 2.5 0 5.2 2.37 4.87 4.936L95.29 95.79c-.197 1.843-1.513 3.29-3.29 3.29z" />
                                            </g>
                                        </svg>

                                        <h2>
                                        Excel Upload Failed with errors !
                                        </h2>
                                        {/* <h3>
                                            Lorem Ipsum Doler Immet
                                        </h3> */}
                                        <button className="errorSheetFile">
                                        Download Error Sheet
                                        </button>
                                    </div>
                                </div>
                                <div className="footerSuccessStatus">
                                    <ul className="list-inline width_100">

                                        <li>
                                            <button>
                                                Done
                                            </button>
                                        </li>
                                        <li>
                                            <span>
                                                Need Help ?
                                            </span>
                                        </li>
                                    </ul>
                                </div>u
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default SuccessStatus;