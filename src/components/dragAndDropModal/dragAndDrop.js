import React from 'react';
import backButtonDrag from "../../assets/back-button.svg";
class DragAndDrop extends React.Component {
    render() {
        return (
            <div>
                <div className="modal">
                    <div className="modal-content modalContentDraganddrop">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">


                            <div className="col-md-4 col-sm-12 col-xs-12 pad-0">
                                <div className="modalLeftSideContainer">
                                    <div className="backButton">
                                        <ul className="list-inline">
                                       
                                            <li>
                                            <img src={backButtonDrag} className="backbuttonImg" />
                                                <h3>Back</h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="leftSideContent m-top-50">
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Choose File
                                    </h2>
                                            <p>
                                                Step 1 completed with no errors
                                    </p>

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                        <input className="dragdropCheckbox" type="checkbox" id="c2" name="cb" />
                                            <label htmlFor="c2" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                        <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Status
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-7 co-sm-12 col-xs-12 pad-0">

                                <div className="modalRightSideContainer">
                                    <div className="headercontentDragdrop">
                                        <h2>
                                            Drag & Drop
                                    </h2>
                                        <h3>
                                            your files in below area , or Choose File
                                </h3>
                                    </div>
                                    <div className="containercontentDragdrop">
                                        <div className="imageDrag">
                                            <input type="file" className="dragImageInputFile" />
                                            <svg className="svgImageDrageFile" xmlns="http://www.w3.org/2000/svg" width="250" height="230" viewBox="0 0 380 250">
                                                <g fill="#E6E6F5" fillRule="nonzero">
                                                    <path d="M0 7h6v47H0zM0 6.988v-1a5 5 0 0 1 5-5h47.9v6H0zM374 7h6v47h-6zM327 6.988v-6h47.9a5 5 0 0 1 5 5v1H327zM0 197h6v47H0zM0 245v-1h52.9v6H5a5 5 0 0 1-5-5z" />
                                                    <g>
                                                        <path d="M374 197h6v47h-6zM327 250v-6h52.9v1a5 5 0 0 1-5 5H327z" />
                                                    </g>
                                                    <g>
                                                        <path d="M182.7 121h20.6v-17.1h10.4L193 86.8l-20.7 17.1h10.4zM182.7 124.8h20.6v8.5h-20.6zM182.7 138.2h20.6v4.3h-20.6zM182.7 147.2h20.6v2h-20.6z" />
                                                    </g>
                                                </g>
                                            </svg>

                                        </div>
                                        <p className="selectefFile">
                                        Vendor_list_2018.xls
                                        </p>
                                    </div>
                                    <div className="footercontentDragdrop">
                                        <ul className="list-inline width_100">
                                            <li>
                                                <label>
                                                    <p>
                                                        Supported File Format <span>.xls </span>and <span>.csv</span>
                                                    </p>
                                                </label>
                                            </li>
                                            <li>
                                                <p>
                                                    Max Upload Size is 5mb
                                                </p>
                                            </li>
                                            <li>
                                                <button>
                                                    NEXT
                                                </button>
                                            </li>
                                            <li>
                                                <span>
                                                    Need Help ?
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DragAndDrop;