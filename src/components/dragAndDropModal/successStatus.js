import React from 'react';
import backButtonDrag from "../../assets/back-button.svg";
class SuccessStatus extends React.Component {
    render() {

        return (
            <div>

                <div className="modal">
                    <div className="modal-content modalContentDraganddrop">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">


                            <div className="col-md-4 col-sm-12 col-xs-12 pad-0">
                                <div className="modalLeftSideContainer">
                                    <div className="backButton">
                                        <ul className="list-inline">

                                            <li>
                                                <img src={backButtonDrag} className="backbuttonImg" />
                                                <h3>Back</h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="leftSideContent m-top-50">
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Choose File
                                    </h2>
                                            <p>
                                                Step 1 completed with no errors
                                    </p>

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c2" name="cb" />
                                            <label htmlFor="c2" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                        <div className="LeftSideContentChecked m-top-100">
                                            <input className="dragdropCheckbox" type="checkbox" id="c1" name="cb" />
                                            <label htmlFor="c1" className="dragdrop_text"></label>
                                            <h2>
                                                Data Mapping
                                    </h2>
                                            {/* <p>
                                                Step 1 completed with no errors
                                    </p> */}

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-7 co-sm-12 col-xs-12 pad-0">

                                <div className="modalRightSideContainer ">
                                    <div className="headercontentDragdrop">
                                        <h2>
                                            Status
                                        </h2>

                                    </div>
                                    <div className="containerSuccessContent">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="184" height="184" viewBox="0 0 184 184">
                                            <g fill="none" fillRule="nonzero">
                                                <circle cx="92" cy="92" r="92" fill="#F8F8F8" opacity=".467" />
                                                <circle cx="91" cy="94" r="67" fill="#F7F7F7" />
                                                <path fill="#57A700" d="M124.994 85.365l-1.52-1.034c-1.68-1.113-2.4-3.26-1.84-5.169l.48-1.75c.8-2.703-1.04-5.487-3.838-5.964l-1.84-.318c-2-.318-3.599-1.83-3.919-3.817l-.32-1.83a4.735 4.735 0 0 0-5.998-3.737l-1.759.556c-1.92.557-4.079-.159-5.198-1.75l-1.12-1.59a4.734 4.734 0 0 0-7.038-.716l-1.36 1.273c-1.519 1.352-3.678 1.67-5.518.636l-1.6-.875c-2.478-1.352-5.597-.238-6.717 2.386l-.72 1.75a4.593 4.593 0 0 1-4.638 2.942l-1.84-.08c-2.879-.158-5.198 2.228-4.958 5.011l.16 1.83c.16 1.987-1.04 3.896-2.88 4.691l-1.679.716a4.74 4.74 0 0 0-2.32 6.68l.96 1.59c1.04 1.75.8 3.977-.56 5.488l-1.199 1.352c-1.92 2.147-1.52 5.408.88 6.999l1.52 1.034c1.679 1.113 2.399 3.26 1.839 5.169l-.72 1.829c-.8 2.704 1.04 5.487 3.839 5.964l1.84.319c1.999.318 3.598 1.829 3.918 3.817l.32 1.83a4.735 4.735 0 0 0 5.998 3.737l1.76-.557c1.919-.557 4.078.16 5.198 1.75l1.12 1.51a4.734 4.734 0 0 0 7.037.717l1.36-1.273c1.52-1.352 3.679-1.67 5.518-.636l1.6.875c2.479 1.352 5.598.238 6.717-2.386l.72-1.75a4.593 4.593 0 0 1 4.639-2.942l1.84.08c2.878.158 5.198-2.228 4.958-5.011l-.08-1.83c-.16-1.987 1.04-3.896 2.879-4.691l1.68-.716a4.74 4.74 0 0 0 2.319-6.68l-.96-1.59c-1.04-1.75-.8-3.977.56-5.488l1.2-1.352c1.999-2.068 1.599-5.408-.72-6.999zm-31.91 15.031l-6.558 6.442-6.478-6.521-6.238-6.283 6.557-6.442 6.239 6.283 14.955-14.792 6.478 6.52-14.955 14.793z" />
                                            </g>
                                        </svg>
                                        <h2>
                                            Excel Uploaded successfully !
                                        </h2>
                                        {/* <h3>
                                            Lorem Ipsum Doler Immet
                                        </h3> */}
                                    </div>
                                </div>
                                <div className="footerSuccessStatus">
                                    <ul className="list-inline width_100">

                                        <li>
                                            <button>
                                                Done
                                            </button>
                                        </li>
                                        <li>
                                            <span>
                                                Need Help ?
                                            </span>
                                        </li>
                                    </ul>
                                </div>u
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default SuccessStatus;