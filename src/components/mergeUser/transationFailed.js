import React, { Component } from 'react'


export default class TransationFailed extends Component {
    render(){
        return(
            <div className="container-fluid pad-0 backG-fff">
                <div className="col-lg-12 pad-0">
                    <div className="transation-sucessfull transation-failed">
                        <div className="transation-sucessfull-inner transation-failed-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="93" height="93" viewBox="0 0 93 93">
                                <g id="prefix__success2" transform="translate(233 259)">
                                    <g id="prefix__check_1" data-name="check 1" transform="translate(-209 -209)">
                                        <path fill="#ff6f6f" opacity=".115" id="prefix__Path_9" d="M46.5 0A46.5 46.5 0 1 1 0 46.5 46.5 46.5 0 0 1 46.5 0z" data-name="Path 9" transform="translate(-24 -50)" />
                                        <path fill="#ffe5e5" opacity=".998" id="prefix__Path_10" d="M33.736 0A33.736 33.736 0 1 1 0 33.736 33.736 33.736 0 0 1 33.736 0z" data-name="Path 10" transform="translate(-11 -37)" />
                                        <g id="prefix__cancel" transform="translate(-1 -27)">
                                            <g id="prefix__Group_26" data-name="Group 26">
                                                <circle fill="#eb0008" id="prefix__Ellipse_2" cx="23" cy="23" r="23" data-name="Ellipse 2" transform="translate(1 1)"/>
                                                <path fill="#fff" id="prefix__Path_11" d="M24 48a24 24 0 1 1 24-24 24.027 24.027 0 0 1-24 24zm0-44a20 20 0 1 0 20 20A20.023 20.023 0 0 0 24 4zm8 30a2 2 0 0 1-1.414-.586l-16-16a2 2 0 0 1 2.828-2.828l16 16A2 2 0 0 1 32 34zm0 0a2 2 0 0 1-1.414-.586l-16-16a2 2 0 0 1 2.828-2.828l16 16A2 2 0 0 1 32 34zm-16 0a2 2 0 0 1-1.414-3.414l16-16a2 2 0 1 1 2.828 2.828l-16 16A2 2 0 0 1 16 34z" data-name="Path 11" />
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <h3>Account Merging Failed </h3>
                            <p className="bg-red">Account Merging failed due to unexpected error. Please try again. If problem persist , please contact at <span>Support@supplymint.com</span></p>
                            <button type="button">Back to Login</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}