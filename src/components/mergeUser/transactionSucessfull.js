import React, { Component } from 'react'


export default class TransationSucessfull extends Component {
    render(){
        return(
            <div className="container-fluid pad-0 backG-fff">
                <div className="col-lg-12 pad-0">
                    <div className="transation-sucessfull">
                        <div className="transation-sucessfull-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="93" height="93" viewBox="0 0 93 93">
                                <g id="prefix__success2" transform="translate(233 259)">
                                    <g id="prefix__check_1" data-name="check 1" transform="translate(-209 -209)">
                                        <path fill="#ac6fff" opacity=".115" id="prefix__Path_9" d="M46.5 0A46.5 46.5 0 1 1 0 46.5 46.5 46.5 0 0 1 46.5 0z" data-name="Path 9" transform="translate(-24 -50)"/>
                                        <path  fill="#f0e5ff" opacity=".998" id="prefix__Path_10" d="M33.736 0A33.736 33.736 0 1 1 0 33.736 33.736 33.736 0 0 1 33.736 0z" data-name="Path 10" transform="translate(-11 -37)"/>
                                        <path fill="#853ee5" id="prefix__Path_8" d="M23.842 1a22.842 22.842 0 1 0 22.841 22.842A22.842 22.842 0 0 0 23.842 1zm11.74 16.355L20.37 32.552a1.4 1.4 0 0 1-.822.426 1.685 1.685 0 0 1-.289.03.672.672 0 0 1-.244-.03 1.4 1.4 0 0 1-.822-.426L12.1 26.476a1.517 1.517 0 0 1 0-2.147 1.535 1.535 0 0 1 2.162 0l5.025 5.025L33.42 15.207a1.535 1.535 0 0 1 2.162 0 1.517 1.517 0 0 1 0 2.147z" data-name="Path 8" transform="translate(-1 -27)" />
                                    </g>
                                </g>
                            </svg>
                            <h3>Account Merging successful </h3>
                            <p>Your Account is successfully linked with the Enterprise. You can now start by login your account</p>
                            <button type="button">Continue to login</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}