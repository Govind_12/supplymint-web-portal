import React from 'react';
import TopBottomSale from './topBottomSaleTable';
import { SaleDetailsBasic, TopArticle, ProductInDemand, SaleTrend, StockInHand } from './saleDetailsTab.js';
import { Label, LineChart, Line, BarChart, ResponsiveContainer, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import ChooseStoreModal from './chooseStoreModal';

import loader from '../../assets/Loader.svg';

const data = [
    {
        name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
    },
    {
        name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
    },
    {
        name: 'Page C', uv: 2000, pv: 4000, amt: 2290,
    },
    {
        name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
    },
    {
        name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
    },

];

class StoreProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            topDetailsTab: 'basicInfo',
            topBotTab: 'Barcode',
            basicStoreData: [],
            storeModal: false,
            sellThruData: [],
            topArticleData: [],
            salesTrend: [],
            salesTrendType: "Unit",
            topArticleType: "top",
            totalSaleUnit: "sales",
            totalUnitSales: [],
            stockHandCoverage: [],
        }
    }
    // graphLabel(e) {
    //     var graph = document.getElementById("saleProfit");
    //     var child = graph.children;
    //     var svg1 = child[1].getElementsByTagName("svg")
    //     svg1[0].setAttribute("viewBox", "0 0 500 310")

    //     var graph2 = document.getElementById("saleperSqare");
    //     var child2 = graph2.children;
    //     var svg2 = child2[1].getElementsByTagName("svg")
    //     svg2[0].setAttribute("viewBox", "0 0 500 310")

    //     var graph0 = document.getElementById("totalSaleGraph");
    //     var child0 = graph0.children;
    //     var svg0 = child0[1].getElementsByTagName("svg")
    //     svg0[0].setAttribute("viewBox", "0 0 400 310")
    // }
    componentWillMount() {

        if (sessionStorage.getItem("storeCode") != null) {
            this.setState({
                basicStoreData: JSON.parse(sessionStorage.getItem("storeCode"))
            })
        }
        let payload = {
            sellThruType: "BARCODE",
            storeCode: JSON.parse(sessionStorage.getItem("storeCode")).siteCode

        }
        this.props.getSellThruRequest(payload)
        this.props.generateTotalSalesRequest(payload)

    }

    updateStoreData() {

        if (sessionStorage.getItem("storeCode") != null) {

            this.setState({
                basicStoreData: JSON.parse(sessionStorage.getItem("storeCode"))
            })

        }
        setTimeout(() => {
            let payload = {
                sellThruType: "BARCODE",
                storeCode: this.state.basicStoreData.siteCode
            }
            this.props.getSellThruRequest(payload)
            this.props.generateTotalSalesRequest(payload)
            this.props.stockHandRequest(payload)
        }, 100);



    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.analytics.getSellThru.isSuccess) {
            if (nextProps.analytics.getSellThru.data.resource != null) {
                this.setState({
                    sellThruData: nextProps.analytics.getSellThru.data.resource
                })

            }
            this.props.getSellThruClear()
        }
        if (nextProps.analytics.getTopArticle.isSuccess) {
            if (nextProps.analytics.getTopArticle.data.resource != null) {
                this.setState({
                    topArticleData: nextProps.analytics.getTopArticle.data.resource
                })

            }
            this.props.getTopArticleClear()
        }
        if (nextProps.analytics.getSalesTrend.isSuccess) {
            if (nextProps.analytics.getSalesTrend.data.resource != null) {
                this.setState({
                    salesTrend: nextProps.analytics.getSalesTrend.data.resource
                })

            }
            this.props.getSalesTrendClear()
        }
        if (nextProps.analytics.generateTotalSales.isSuccess) {

            if (nextProps.analytics.generateTotalSales.data.resource != null) {
                this.graphViewBox()
                this.setState({
                    totalUnitSales: nextProps.analytics.generateTotalSales.data.resource
                })

            }
            this.props.generateTotalSalesClear()
        }

        if (nextProps.analytics.stockHand.isSuccess) {
            if (nextProps.analytics.stockHand.data.resource != null) {
                this.setState({
                    stockHandCoverage: nextProps.analytics.stockHand.data.resource
                })

            }
            this.props.stockHandClear()
        }

    }

    numDifferentiation(val) {
        if (val >= 10000000) val = (val / 10000000).toFixed(2) + ' Cr';
        else if (val >= 100000) val = (val / 100000).toFixed(2) + ' Lac';
        else if (val >= 1000) val = (val / 1000).toFixed(2) + ' K';
        return val;
    }

    openStoreModal() {
        this.setState({
            storeModal: true
        })
        this.props.getStoreDataRequest();
    }
    closeStoreModal() {
        this.setState({
            storeModal: false
        })
    }
    sellThruType(type) {
        this.setState({
            topBotTab: type,
            sellThruData: []
        })
        let payload = {
            sellThruType: type.toUpperCase(),
            storeCode: this.state.basicStoreData.siteCode
        }
        this.props.getSellThruRequest(payload)
    }

    basicDetails(type) {
        this.setState({
            topDetailsTab: type
        })
        if (type == 'saleTrend') {
            this.props.getSalesTrendRequest();
        } else if (type == "stockHand") {
            let payload = {
                storeCode: this.state.basicStoreData.siteCode
            }
            this.props.stockHandRequest(payload)
        } else if (type == "topArticle") {
            this.props.getTopArticleRequest();
        } 

    }
    activeTab(type) {
        this.setState({
            salesTrendType: type
        })

    }
    tabActive(type) {
        this.setState({
            totalSaleUnit: type
        })
    }
    activeTopArticle(type) {
        this.setState({
            topArticleType: type
        })
    }
    graphViewBox() {
        setTimeout(() => {
            var svg2 = document.querySelectorAll('.totalSaleProfit')[0].childNodes[1].childNodes[0]
            var svg3 = document.querySelectorAll('.salePerSquare')[0].childNodes[1].childNodes[0]
            svg2.setAttribute("viewBox", "0 0 500 310");
            svg3.setAttribute("viewBox", "0 0 500 310");
        }, 10)
    }
    toolTipLabel(id, tab1, tab2) {
        setTimeout(() => {
            if (id == "totalSaleProfit" && document.querySelectorAll('.totalSaleProfit')[0].childNodes[1].childNodes[1].childNodes[0].childNodes[1].childNodes[1].childNodes[0] != undefined) {
                if (document.querySelectorAll('.totalSaleProfit')[0].childNodes[1].childNodes[1].childNodes[0].childNodes[1].childNodes[0].childNodes[0] != undefined) {
                    document.querySelectorAll('.totalSaleProfit')[0].childNodes[1].childNodes[1].childNodes[0].childNodes[1].childNodes[0].childNodes[0].innerHTML = "Profit"
                }
                document.querySelectorAll('.totalSaleProfit')[0].childNodes[1].childNodes[1].childNodes[0].childNodes[1].childNodes[1].childNodes[0].innerHTML = tab1
            }
            else if (id == "salePerSquare") {
                var graph = document.querySelectorAll('.salePerSquare')[0].childNodes[1].childNodes[1].childNodes[0].childNodes[1].childNodes[0].childNodes[0].innerHTML = tab1
            }
            else if (id == "totalSaleVsUnitSold") {
                var graph = document.querySelectorAll("." + id)[0].childNodes[0].childNodes[1].childNodes[0].childNodes[1].childNodes[0].childNodes[0].innerHTML = this.state.totalSaleUnit == 'sales' ? tab1 : tab2
            } else if (id == "productGraph") {
                var graph = document.querySelectorAll("." + id)[0].childNodes[0].childNodes[1].childNodes[0].childNodes[1].childNodes[0].childNodes[0].innerHTML = this.state.salesTrendType == 'Unit' ? tab1 : tab2
            }
        }, 500)
    }
    prevMonth() {
        var monthJSON = [{ "month": "Jan" }, { "month": "Feb" }, { "month": "Mar" }, { "month": "Apr" }, { "month": "May" }, { "month": "Jun" }, { "month": "Jul" }, { "month": "Aug" }, { "month": "Sept" }, { "month": "Oct" }, { "month": "Nov" }, { "month": "Dec" }]
        var date = new Date()
        var curMonth = date.getMonth();
        var curYear = date.getFullYear();
        if (curMonth == 0) {
            var prevMonth = 11;
            var calMonth = monthJSON[prevMonth].month;
            var calYear = curYear - 1;
            return calMonth.concat(" ", calYear)
        } else {
            var prevMonth = curMonth - 1;
            var calMonth = monthJSON[prevMonth].month;
            return calMonth.concat(" ", curYear)
        }
    }
    render() {
        const { basicStoreData } = this.state
        
        return (
            <div>
                <div className="container-fluid pad-0">
                    <div className="container_div" id="home">
                        <div className="container-fluid">
                            <div className="container_div" id="">
                                <div className="container-fluid">
                                    <div className="replenishment_container">
                                        <div className="storeProfileMain">
                                            <div className="col-md-12 pad-0">
                                                <div className="heading">
                                                    <div className="col-md-6 pad-0">
                                                        <h4>Store Name <span>{basicStoreData.siteName}</span></h4>
                                                        <h4>Rank <span>{basicStoreData.storeRank}</span></h4>
                                                    </div>
                                                    <div className="col-md-6 pad-0 textRight">
                                                        <div className="dropStore" onClick={() => this.openStoreModal()}>
                                                            <span>{basicStoreData.siteCode}</span>
                                                            <i className="fa fa-chevron-down" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="saleInfo ">
                                                    <div className="col-md-5 pad-0">
                                                        <div className="totalSaleLeft m-top-37">
                                                            <div className="saleCards">
                                                                <h3><span className="rupeeSign">&#8377;</span>{this.numDifferentiation(basicStoreData.totalSalesLY)}<span></span></h3>
                                                                <h4>Total Sales</h4>
                                                                <p>Last 1 Year</p>
                                                            </div>
                                                            <div className="saleCards">
                                                                <h3>{this.numDifferentiation(basicStoreData.totalQTYSoldLY)}<span></span></h3>
                                                                <h4>Total Quantity Sold</h4>
                                                                <p>Last 1 Year</p>
                                                            </div>
                                                            <div className="saleCards m-top-30">
                                                                <h3><span className="rupeeSign">&#8377;</span>{this.numDifferentiation(basicStoreData.avgSalesLY)}<span></span></h3>
                                                                <h4>Average Sales Per Month</h4>
                                                                <p>Last 1 Year</p>
                                                            </div>
                                                            <div className="saleCards m-top-30">
                                                                <h3><span className="rupeeSign">&#8377;</span>{this.numDifferentiation(basicStoreData.totalSalesCM)}<span></span></h3>
                                                                <h4>Total Sales</h4>
                                                                <p>Current Month</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-7 pad-0  m-top-10 rightGraph">
                                                        <label>Details</label>
                                                        <div className="verticalTabs  m-top-10">
                                                            <div className="tabsShow">
                                                                <ul className="pad-0">
                                                                    <li className={this.state.topDetailsTab == 'basicInfo' ? "active" : null} onClick={() => this.basicDetails('basicInfo')}>Basic Information</li>
                                                                    <li className={this.state.topDetailsTab == 'saleTrend' ? "active" : null} onClick={() => this.basicDetails('saleTrend')}>Sales Trend</li>
                                                                    <li className={this.state.topDetailsTab == 'topArticle' ? "active" : null} onClick={() => this.basicDetails('topArticle')}>Top Articles</li>
                                                                    <li className={this.state.topDetailsTab == 'stockHand' ? "active" : null} onClick={() => this.basicDetails('stockHand')}>Stock in Hand</li>
                                                                    <li className={this.state.topDetailsTab == 'productDemand' ? "active" : null} onClick={() => this.basicDetails('productDemand')}>Product in Demand</li>
                                                                </ul>
                                                            </div>
                                                            <div className="tabsItems">
                                                                {this.state.topDetailsTab == 'basicInfo' ? <SaleDetailsBasic {...this.props} basicStoreData={this.state.basicStoreData} saleTabDetail={this.state.topDetailsTab} />
                                                                    : this.state.topDetailsTab == 'topArticle' ? <TopArticle {...this.props} topArticleType={this.state.topArticleType} articleDetail={this.state.topDetailsTab} topArticleData={this.state.topArticleData} activeTopArticle={(e) => this.activeTopArticle(e)} />
                                                                        : this.state.topDetailsTab == 'productDemand' ? <ProductInDemand {...this.props} productDetail={this.state.topDetailsTab} />
                                                                            : this.state.topDetailsTab == 'saleTrend' ? <SaleTrend {...this.props}{...this.state} activeTab={(e) => this.activeTab(e)} salesTrendType={this.state.salesTrendType} saleTrendItem={this.state.topDetailsTab} salesTrend={this.state.salesTrend} toolTip={(id, tab1, tab2) => this.toolTipLabel(id, tab1, tab2)} prevMonth={() => this.prevMonth()} />
                                                                                : this.state.topDetailsTab == 'stockHand' ? <StockInHand {...this.props} {...this.state} stockHandItem={this.state.topDetailsTab} stockHandCoverage={this.state.stockHandCoverage} />
                                                                                    : null}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="topBottomSale">
                                                    <div className="col-md-7 pad-0">
                                                        <div className="verticalTabs">
                                                            <div className="tabsShow">
                                                                <ul className="pad-0">
                                                                    <li className={this.state.topBotTab == "Barcode" ? "active" : null} onClick={(e) => this.sellThruType("Barcode")}>Barcode Sell Thru</li>
                                                                    <li className={this.state.topBotTab == "Size" ? "active" : null} onClick={(e) => this.sellThruType("Size")}>Size Sell Thru</li>
                                                                    <li className={this.state.topBotTab == "Pattern" ? "active" : null} onClick={(e) => this.sellThruType("Pattern")}>Pattern Sell Thru</li>
                                                                    <li className={this.state.topBotTab == "Material" ? "active" : null} onClick={(e) => this.sellThruType("Material")}>Material Sell Thru</li>
                                                                    <li className={this.state.topBotTab == "Brand" ? "active" : null} onClick={(e) => this.sellThruType("Brand")}>Brand Sell Thru</li>
                                                                    <li className={this.state.topBotTab == "Assortment" ? "active" : null} onClick={(e) => this.sellThruType("Assortment")}>Assortment Sell Thru</li>
                                                                    <li className={this.state.topBotTab == "Design" ? "active" : null} onClick={(e) => this.sellThruType("Design")}>Design Sell Thru</li>
                                                                </ul>
                                                            </div>
                                                            <div className="tabsItems posRelative">
                                                                <TopBottomSale {...this.props}{...this.state} sellThruData={this.state.sellThruData} topBotTab={this.state.topBotTab} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-5 topArticleStore">
                                                        <div className="topBotSaleGraph graphLabel graphHeight posRelative" id="totalSaleGraph">
                                                            <div>
                                                                <h3 className="graphHeading">Total Sales Vs Unit Sold</h3>
                                                                <div className="topBottomSwitch">
                                                                    <div className="col-md-6 topFivePerformer displayInline pad-0" onClick={(e) => this.tabActive('sales')}><label className={this.state.totalSaleUnit == "sales" ? "active" : ""}>Total Sales</label></div>
                                                                    <div className="col-md-6 botFivePerformer displayInline pad-0" onClick={(e) => this.tabActive('unitSold')}><label className={this.state.totalSaleUnit == "unitSold" ? "active" : ""}>Unit Sold</label></div>
                                                                </div>
                                                            </div>
                                                            <div className="graph graphWidth totalSaleVsUnitSold toolTipStyle" onMouseOver={() => this.toolTipLabel("totalSaleVsUnitSold", "Total sales", "Total units sold")}>
                                                                {this.state.totalUnitSales.length == 0 ? <div className="otbLoader textCenter centerDiv">
                                                                    <div className="loaderBgBLur"></div>
                                                                    <img src={loader} />
                                                                </div> :
                                                                    <LineChart width={400} height={350} data={this.state.totalUnitSales}
                                                                        margin={{ top: 5, right: 30, left: 0, bottom: 50 }}>
                                                                        <XAxis dataKey="billDate" >
                                                                            <Label value="Months" dy={7} offset={0} position="insideBottom" />
                                                                        </XAxis>
                                                                        <YAxis label={{ value: this.state.totalSaleUnit == 'sales' ? 'Total Sales' : 'Unit Sold', position: 'insideTop', dy: -30, dx: 5 }} />
                                                                        <Tooltip cursor={false} />
                                                                        <Line type="monotone" dataKey={this.state.totalSaleUnit == 'sales' ? "totalSellPrice" : "totalQty"} strokeWidth={2} stroke={this.state.totalSaleUnit == 'sales' ? "#bd10e0" : "#7ed321"} activeDot={{ r: 5 }} />
                                                                        {/* <Line type="monotone" dataKey="uv" strokeWidth={2} stroke="#7ed321" activeDot={{ r: 5 }} /> */}
                                                                    </LineChart>}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="festivalImpact m-top-30">
                                                    <h4>Festival Impact</h4>
                                                    <p>Top five performing article with positive percentages impacts on several festivals</p>
                                                    <table className="m-top-25">
                                                        <thead>
                                                            <tr>
                                                                <th ><label>Festival</label></th>
                                                                <th colSpan="2"><label>Article 1</label></th>
                                                                <th colSpan="2"><label>Article 2</label></th>
                                                                <th colSpan="2"><label>Article 3</label></th>
                                                                <th colSpan="2"><label>Article 4</label></th>
                                                                <th colSpan="2"><label>Article 5</label></th>
                                                            </tr>
                                                            <tr>
                                                                <th ><label></label></th>
                                                                <th ><label>Article</label></th>
                                                                <th ><label>% impact</label></th>
                                                                <th ><label>Article</label></th>
                                                                <th><label>% impact</label></th>
                                                                <th><label>Article</label></th>
                                                                <th ><label>% impact </label></th>
                                                                <th ><label>Article</label></th>
                                                                <th ><label>% impact </label></th>
                                                                <th><label>Article</label></th>
                                                                <th><label>% impact </label></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr className="tableNoData">
                                                                <td colSpan="11">There is no data available for this section</td>
                                                            </tr>

                                                            {/* <tr>
                                                                <td><label>Basant Panchami</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>

                                                            </tr>
                                                            <tr>
                                                                <td><label>Holi</label> </td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>
                                                                <td><label>1259</label></td>
                                                                <td><label>93.01%</label></td>

                                                            </tr>  */}
                                                        </tbody>

                                                    </table>
                                                    {/* ---------------------------------------------------Pagination---------------------------------- */}
                                                    {/* <div className="pagerDiv storeProfilePagination">
                                                        <ul className="list-inline pagination">
                                                            <li >
                                                                <button className="PageFirstBtn" id="prev">Prev</button>
                                                            </li>
                                                            <li>
                                                                <button className="PageFirstBtn pointerNone"> <span></span>
                                                                </button>
                                                            </li>
                                                            <li >
                                                                <button className="PageFirstBtn borderNone" id="next">Next</button>
                                                            </li>

                                                        </ul>
                                                    </div> */}

                                                    {/* -------------------------------------------------------Pagination End----------------------------- */}

                                                </div>


                                                <div className="totalSaleEstimation m-top-40">

                                                    <div className="col-md-6 pad-0">
                                                        <div className="totalSaleProfit pad-20 graphLabel graphHeight graphWidth posRelative toolTipStyle" onMouseOver={() => this.toolTipLabel("totalSaleProfit", "Total sales", "")} id="saleProfit">
                                                            <label className="graphHeading">Total Sales Vs Profit</label>
                                                            {this.state.totalUnitSales.length == 0 ? <div className="otbLoader textCenter centerDiv">
                                                                <div className="loaderBgBLur"></div>
                                                                <img src={loader} />
                                                            </div> :
                                                                <LineChart width={500} height={350} data={this.state.totalUnitSales}
                                                                    margin={{ top: 5, right: 30, left: 0, bottom: 30 }}>
                                                                    <XAxis dataKey='billDate' >
                                                                        <Label value="Months" dy={7} offset={0} position="insideBottom" />
                                                                    </XAxis>
                                                                    <YAxis label={{ value: 'Total Sales Vs Profit', position: 'insideTop', dy: -28, dx: 30 }} />
                                                                    <Tooltip cursor={false} />
                                                                    <Line type="monotone" dataKey="totalSellPrice" strokeWidth={2} stroke="#6d6dc9" activeDot={{ r: 5 }} />
                                                                    <Line type="monotone" dataKey="profit" strokeWidth={2} stroke="#7ed321" activeDot={{ r: 5 }} />
                                                                </LineChart>}
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6 pad-0">
                                                        <div className="salePerSquare pad-20 graphLabel graphHeight graphWidth posRelative toolTipStyle" onMouseOver={() => this.toolTipLabel("salePerSquare", "Sale per sqft :", "")} id="saleperSqare">
                                                            <label className="graphHeading">Sale per square foot</label>
                                                            {this.state.totalUnitSales.length == 0 ? <div className="otbLoader textCenter centerDiv">
                                                                <div className="loaderBgBLur"></div>
                                                                <img src={loader} />
                                                            </div> :
                                                                <LineChart width={500} height={350} data={this.state.totalUnitSales}
                                                                    margin={{ top: 5, right: 30, left: 0, bottom: 30 }}>
                                                                    {/* <CartesianGrid strokeDasharray="3 3" /> */}
                                                                    <XAxis dataKey="billDate" >
                                                                        <Label value="Months" dy={7} offset={0} position="insideBottom" />
                                                                    </XAxis>
                                                                    {/* <YAxis >
                                                                        <Label value='Sales per square foot' angle={-90} offset={10} />
                                                                    </YAxis> */}
                                                                    <YAxis label={{ value: 'Sale per square foot', position: 'insideTop', dy: -30, dx: 25 }} />
                                                                    <Tooltip cursor={false} />
                                                                    {/* <Legend verticalAlign="top" height={36} type={"circle"}/> */}
                                                                    <Line type="monotone" dataKey="areaPersqft" strokeWidth={2} stroke="#50e3c2" activeDot={{ r: 5 }} />
                                                                </LineChart>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.storeModal ? <ChooseStoreModal {...this.props} {...this.state} updateStoreData={(e) => this.updateStoreData(e)}
                    openStoreModal={(e) => this.openStoreModal(e)} closeStoreModal={(e) => this.closeStoreModal(e)} /> : null}
            </div>

        );
    }
}

export default StoreProfile;