import React from 'react';

import loader from '../../assets/Loader.svg';
class TopBottomSale extends React.Component{

    truncateStr(text){
        return text.slice(0,15).concat("...")
    }
    render(){

        const sellThruData = this.props.sellThruData;
        const sellThruType= this.props.topBotTab
        return(
            <div>
                    {this.props.sellThruData.length!=0 ?<div className="barcodeSell">
                    <div className="col-md-5 heading"><label>Top 5</label></div>
                    <div className="col-md-5 heading"><label>Bottom 5</label></div>
                    <table className="firstTable">
                        <thead>
                            <tr>
                                <th><label>{sellThruType}</label></th>
                                <th><label>Sell Thru</label></th>                                                                      
                                </tr>
                        </thead>
                        <tbody>
                          {this.props.sellThruData.length!=0 && this.props.sellThruData["top"]!=null? this.props.sellThruData["top"].map((dataa, keyy) => (

                                      <tr key={keyy}>
                                          {/* <td><label>{dataa.sellThroughType}</label></td>            */}
                                          <td><label>{dataa.sellThroughType.length < 15 ? dataa.sellThroughType : <span className="toolTipMain">{this.truncateStr(dataa.sellThroughType)}<span className="toolTip">{dataa.sellThroughType}</span></span>}</label></td>           
                                          <td><label>{dataa.sellThroughValue+"%"}</label></td>
                                      </tr>


                         ) ) :<tr><td colSpan="2" className="modalNoData"> NO DATA FOUND </td></tr>}
                           
               
                        </tbody>
                    </table>
                    <table className="secondTable">
                        <thead>
                            <tr>
                                <th><label>{sellThruType}</label></th>
                                <th><label>Sell Thru</label></th>                                                                      
                                </tr>
                        </thead>
                        <tbody>
                         {this.props.sellThruData.length!=0 && this.props.sellThruData["bottom"]!=null ? this.props.sellThruData["bottom"].map((data, key) => (

                            <tr key={key}>
                                <td><label>{data.sellThroughType.length < 15 ? data.sellThroughType : <span className="toolTipMain">{this.truncateStr(data.sellThroughType)}<span className="toolTip">{data.sellThroughType}</span></span>}</label>
                                
                                </td>           
                                <td><label>{data.sellThroughValue+"%"}</label></td>
                            </tr>


                            ) ):<tr><td colSpan="2" className="modalNoData"> NO DATA FOUND </td></tr>} 
                        </tbody>
                    </table>
                </div>: null}
                {this.props.sellThruData.length==0 ?   <div className="otbLoader textCenter centerDiv">
                <div className="loaderBgBLur"></div>
                    <img src={loader} />
                </div>:null}
            </div>
        )
    }
}
export default TopBottomSale;