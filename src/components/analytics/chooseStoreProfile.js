import React from "react";
import PoError from "../loaders/poError";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import DateRangeFilter from "./dateRangeFilter";
import Pagination from '../pagination';
import ToastLoader from '../loaders/toastLoader';
import {Bar, CartesianGrid, ComposedChart, Legend, Line, Tooltip, XAxis, YAxis} from "recharts";

class ChooseStoreProfile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            // exportToExcel: false,
            loading: true,
            // confirmDelete: false,
            // submit: false,
            // headerMsg: '',
            // paraMsg: '',
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",

            siteCode: 0,
            siteName: "All Sites",
            type: 1,
            storeAnalysisData: {},
            mouseHoverData: {},
            topMovingItemData: [],
            nonMovingItemData: [],
            dateWiseAovData: [],
            itemToBeStockOutData: [],
            salesTrendData: [],
            salesVsGmData: [],
            sitePlanningFilters: [],
            searchSite: "",
            noOfDays: 30,
            currentTab: "overview",

            storeData: [],
            storeSearch: "",
            errorMassage: "",
            poErrorMsg: false,
            dropDownShow: false,
            found: false,
            searchUser:"",
            showStores: false,
            showDays: false,
            individual: "",
            
            topMovingItemTopItem: false,
            topMovingItemTopItemValue: 10,
            topMovingItemSales: false,
            topMovingItemSalesValue: "sales",

            nonMovingItemTopItem: false,
            nonMovingItemTopItemValue: 10,
            nonMovingItemSales: false,
            nonMovingItemSalesValue: "sales",

            dateWiseDrop: false,
            dateWiseDropValue: "division",

            salesTrendDrop: false,
            salesTrendDropValue: "division",

            salesVsGmDrop: false,
            salesVsGmDropValue: "division",

            itemStockTopItem: false,
            topMovingItemdataType: "table",
            nonMovingItemdataType: "table",
            dateWisedataType: "table",
            itemStockdataType: "table",
            salesTrenddataType: "table",
            salesGmdataType: "table",

            prev3: '',
            current3: 1,
            next3: '',
            maxPage3: 0,
            totalItems3: 0,
            jumpPage3: 1,

            prev4: '',
            current4: 1,
            next4: '',
            maxPage4: 0,
            totalItems4: 0,
            jumpPage4: 1,

            prev5: '',
            current5: 1,
            next5: '',
            maxPage5: 0,
            totalItems5: 0,
            jumpPage5: 1,

            prev6: '',
            current6: 1,
            next6: '',
            maxPage6: 0,
            totalItems6: 0,
            jumpPage6: 1,

            toastMsg: "",
            toastLoader: false
        }
    }
    handleChange(e) {
        this.setState({
        storeSearch: e.target.value,
        searchUser:e.target.value
        })
        this.onSearchFunction()
    }

    openStore (e) {
        e.preventDefault ();
        this.setState ({
            showStores: true
        }, () => document.addEventListener('click', this.closeStore));
    }
    closeStore = (e) => {
        if (document.getElementById("stores") != null && !document.getElementById("stores").contains(e.target)) {
            this.setState({ showStores: false }, () => {
                document.removeEventListener('click', this.closeStore);
            });
        }
    }
    openDays (e) {
        e.preventDefault ();
        this.setState ({
            showDays: !this.state.showDays
        });
    }
    CloseDays = () => {
        this.setState({ showDays: false
        });
    }
    openTopItem (e) {
        e.preventDefault ();
        this.setState ({
            topMovingItemTopItem: !this.state.topMovingItemTopItem
        }, () => document.addEventListener('click', this.closeTopItem));
    }
    closeTopItem = () => {
        this.setState({ topMovingItemTopItem: false }, () => {
            document.removeEventListener('click', this.closeTopItem);
        });
    }
    openMovingItemSales (e) {
        e.preventDefault ();
        this.setState ({
            topMovingItemSales: !this.state.topMovingItemSales
        }, () => document.addEventListener('click', this.closeMovingItemSales));
    }
    closeMovingItemSales = () => {
        this.setState({ topMovingItemSales: false }, () => {
            document.removeEventListener('click', this.closeMovingItemSales);
        });
    }
    openNonMovingItemTopItem (e) {
        e.preventDefault ();
        this.setState ({
            nonMovingItemTopItem: !this.state.nonMovingItemTopItem
        }, () => document.addEventListener('click', this.closeNonMovingItemTopItem));
    }
    closeNonMovingItemTopItem = () => {
        this.setState({ nonMovingItemTopItem: false }, () => {
            document.removeEventListener('click', this.closeNonMovingItemTopItem);
        });
    }
    openNonMovingItemSales (e) {
        e.preventDefault ();
        this.setState ({
            nonMovingItemSales: !this.state.nonMovingItemSales
        }, () => document.addEventListener('click', this.closeNonMovingItemSales));
    }
    closeNonMovingItemSales = () => {
        this.setState({ nonMovingItemSales: false }, () => {
            document.removeEventListener('click', this.closeNonMovingItemSales);
        });
    }
    openDateWiseDrop (e) {
        e.preventDefault ();
        this.setState ({
            dateWiseDrop: !this.state.dateWiseDrop
        }, () => document.addEventListener('click', this.closeDateWiseDrop));
    }
    closeDateWiseDrop = () => {
        this.setState({ dateWiseDrop: false }, () => {
            document.removeEventListener('click', this.closeDateWiseDrop);
        });
    }
    openDateWiseTopItem (e) {
        e.preventDefault ();
        this.setState ({
            dateWiseTopItem: !this.state.dateWiseTopItem
        }, () => document.addEventListener('click', this.closeDateWiseTopItem));
    }
    closeDateWiseTopItem = () => {
        this.setState({ dateWiseTopItem: false }, () => {
            document.removeEventListener('click', this.closeDateWiseTopItem);
        });
    }
    openItemStockTopItem (e) {
        e.preventDefault ();
        this.setState ({
            itemStockTopItem: !this.state.itemStockTopItem
        }, () => document.addEventListener('click', this.closeItemStockTopItem));
    }
    closeItemStockTopItem = () => {
        this.setState({ itemStockTopItem: false }, () => {
            document.removeEventListener('click', this.closeItemStockTopItem);
        });
    }
    changeTopMovingItemdataType = () => {
        this.setState({
            topMovingItemdataType: this.state.topMovingItemdataType == "table" ? "graph" : "table",
        })
    }
    changeNonMovingItemdataType = () => {
        this.setState({
            nonMovingItemdataType: this.state.nonMovingItemdataType == "table" ? "graph" : "table",
        })
    }
    changeDateWisedataType = () => {
        this.setState({
            dateWisedataType: this.state.dateWisedataType == "table" ? "graph" : "table",
        })
    }
    changeItemStockdataType = () => {
        this.setState({
            itemStockdataType: this.state.itemStockdataType == "table" ? "graph" : "table",
        })
    }
    changeSalesTrenddataType = () => {
        this.setState({
            salesTrenddataType: this.state.salesTrenddataType == "table" ? "graph" : "table",
        })
    }
    changeSalesGmdataType = () => {
        this.setState({
            salesGmdataType: this.state.salesGmdataType == "table" ? "graph" : "table",
        })
    }

    openSalesTrendDrop (e) {
        e.preventDefault ();
        this.setState ({
            salesTrendDrop: !this.state.salesTrendDrop
        }, () => document.addEventListener('click', this.closeSalesTrendDrop));
    }
    closeSalesTrendDrop = () => {
        this.setState({ salesTrendDrop: false }, () => {
            document.removeEventListener('click', this.closeSalesTrendDrop);
        });
    }

    openSalesVsGmDrop (e) {
        e.preventDefault ();
        this.setState ({
            salesVsGmDrop: !this.state.salesVsGmDrop
        }, () => document.addEventListener('click', this.closeSalesVsGmDrop));
    }
    closeSalesVsGmDrop = () => {
        this.setState({ salesVsGmDrop: false }, () => {
            document.removeEventListener('click', this.closeSalesVsGmDrop);
        });
    }
    // componentWillMount() {
    //     if (this.props.analytics.getStoreData.isSuccess) {
    //     this.setState({
    //         storeData: this.props.analytics.getStoreData.data.resource,
    //     })
    //     } else {
    //     this.props.getStoreDataRequest();
    //     }
    // }
    // componentWillReceiveProps(nextProps) {
    //     if (nextProps.analytics.getStoreData.isSuccess) {
    //     if(nextProps.analytics.getStoreData.data.resource!=null){
    //     this.setState({
    //         storeData: nextProps.analytics.getStoreData.data.resource,
    //     })
    //     }else{
    //     this.setState({
    //         storeData: [],
    //     })
    //     }
    //     }
    //     if (nextProps.analytics.getStoreProfileCode.isSuccess) {
    //     if (nextProps.analytics.getStoreProfileCode.data.resource != null) {
    //         this.props.history.push("/analytics/storeProfile")
    //     } else {
    //         this.setState({
    //         errorMassage: nextProps.analytics.getStoreProfileCode.data.message,
    //         poErrorMsg: true
    //         })
    //     }
    //     }
    // }
    // _handleKeyPress = (e) => {
    //     if (e.key === 'Enter') {
    //     this.onSearch();
    //     }
    // }
    // closeErrorRequest(e) {
    //     this.setState({
    //     poErrorMsg: !this.state.poErrorMsg
    //     })
    // }
    // onSearch() {
    //     this.setState({
    //     dropDownShow: false
    //     })
    //     let storeData = this.state.storeData
    //     let code = ""
    //     if (this.state.storeSearch != "") {
    //     for (var i = 0; i < storeData.length; i++) {
    //         if (Object.keys(storeData[i]) == this.state.storeSearch) {
    //         code = this.state.storeSearch
    //         } else if (Object.values(storeData[i]) == this.state.storeSearch.toUpperCase()) {
    //         code = Object.keys(storeData[i])
    //         } else {
    //         code = this.state.storeSearch
    //         }
    //     }
    //     let payload = {
    //         storeCode: code
    //     }
    //     this.props.getStoreProfileCodeRequest(payload)
    //     }
    // }

    // onnChange(key,value) {
    //     this.setState({
    //     dropDownShow: false,
    //     storeSearch: key[0],
    //     searchUser: key[0] +"-"+value[0]
    //     })
    // }

    // componentDidMount() {
    //     document.addEventListener('mousedown', this.handleClickOutside);
    // }

    // componentWillUnmount() {
    //     document.removeEventListener('mousedown', this.handleClickOutside);
    // }

    // setWrapperRef(node) {
    //     this.wrapperRef = node;
    // }
    // handleClickOutside = (event) => {
    //     if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
    //     this.setState({
    //         dropDownShow: false
    //     })
    //     }
    // }
    
    // Capitalize(str) {
    //     if (str == undefined) {
    //     return "";
    //     } else {
    //     return str.charAt(0).toUpperCase() + str.slice(1);
    //     }
    // }

    // onSearchFunction() {
    //     var input = document.getElementById("storeProfile");
    //     var filter = input.value.toUpperCase();
    //     var ul = document.getElementById("myUL");
    //     var li = ul.getElementsByTagName("li");
    //     for (var i = 0; i < li.length; i++) {
    //     var a = li[i].getElementsByTagName("label")[0];
    //     var txtValue = a.textContent || a.innerText;
    //     if (txtValue.toUpperCase().indexOf(filter) > -1) {
    //         li[i].classList.remove('hideElement');
    //     } else {
    //         li[i].classList.add('hideElement');
    //     }
    //     }
    //     this.displayNoResult(li.length)
    // }

    // displayNoResult(allLI) {
    //     var hiddenLILength = document.querySelectorAll('li.hideElement');
    //     if (allLI === hiddenLILength.length) {
    //     document.getElementById('noResult').classList.remove('hideElement')
    //     } else {
    //     document.getElementById('noResult').classList.add('hideElement')
    //     }
    // }

    componentDidMount() {
        this.props.getSitePlanningFiltersRequest();
        this.props.getAllStoreAnalysisRequest(this.state.siteCode);
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
        // this.handleTabChange(this.state.currentTab);
        
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.analytics.getAllStoreAnalysis.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                success: false,
                storeAnalysisData: nextProps.analytics.getAllStoreAnalysis.data.resource.data[0],
                mouseHoverData: nextProps.analytics.getAllStoreAnalysis.data.resource.mouseHover,
                noOfDays: nextProps.analytics.getAllStoreAnalysis.data.resource.noOfDays === null ? 30 : nextProps.analytics.getAllStoreAnalysis.data.resource.noOfDays
            }, this.props.getAllStoreAnalysisClear);
            if (nextProps.analytics.getAllStoreAnalysis.data.resource.noOfDays === null) {
                this.props.createStoreAnalysisRequest(30);
            }
            else {
                this.handleTabChange(this.state.currentTab);
            }
        }
        else if(nextProps.analytics.getAllStoreAnalysis.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.analytics.getAllStoreAnalysis.message.status,
                errorCode: nextProps.analytics.getAllStoreAnalysis.message.error == undefined ? undefined : nextProps.analytics.getAllStoreAnalysis.message.error.errorCode,
                errorMessage: nextProps.analytics.getAllStoreAnalysis.message.error == undefined ? undefined : nextProps.analytics.getAllStoreAnalysis.message.error.errorMessage,
                storeAnalysisData: {},
                mouseHoverData: {},
                noOfDays: 0
            }, this.props.getAllStoreAnalysisClear);
        }

        if(nextProps.analytics.getDataStoreAnalysis.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                success: false,
                topMovingItemData: nextProps.analytics.getDataStoreAnalysis.data.type1 == undefined ? [] : nextProps.analytics.getDataStoreAnalysis.data.type1.resource.response,
                nonMovingItemData: nextProps.analytics.getDataStoreAnalysis.data.type2 == undefined ? [] : nextProps.analytics.getDataStoreAnalysis.data.type2.resource.response,
                dateWiseAovData: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? [] : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.response,
                itemToBeStockOutData: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? [] : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.response,
                salesTrendData: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? [] : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.response,
                salesVsGmData: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? [] : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.response,

                prev3: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.Previous,
                current3: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.currPage,
                next3: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.currPage + 1,
                maxPage3: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.maxPage,
                totalItems3: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.totalCount,
                jumpPage3: nextProps.analytics.getDataStoreAnalysis.data.type3 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type3.resource.currPage,

                prev4: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.Previous,
                current4: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.currPage,
                next4: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.currPage + 1,
                maxPage4: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.maxPage,
                totalItems4: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.totalCount,
                jumpPage4: nextProps.analytics.getDataStoreAnalysis.data.type4 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type4.resource.currPage,

                prev5: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.Previous,
                current5: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.currPage,
                next5: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.currPage + 1,
                maxPage5: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.maxPage,
                totalItems5: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.totalCount,
                jumpPage5: nextProps.analytics.getDataStoreAnalysis.data.type5 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type5.resource.currPage,

                prev6: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.Previous,
                current6: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.currPage,
                next6: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? '' : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.currPage + 1,
                maxPage6: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.maxPage,
                totalItems6: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? 0 : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.totalCount,
                jumpPage6: nextProps.analytics.getDataStoreAnalysis.data.type6 == undefined ? 1 : nextProps.analytics.getDataStoreAnalysis.data.type6.resource.currPage,
            });
            this.props.getDataStoreAnalysisClear();
        }
        else if(nextProps.analytics.getDataStoreAnalysis.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.analytics.getDataStoreAnalysis.message.status,
                errorCode: nextProps.analytics.getDataStoreAnalysis.message.error == undefined ? undefined : nextProps.analytics.getDataStoreAnalysis.message.error.errorCode,
                errorMessage: nextProps.analytics.getDataStoreAnalysis.message.error == undefined ? undefined : nextProps.analytics.getDataStoreAnalysis.message.error.errorMessage,
                //storeAnalysisData: {}
            }, this.props.getDataStoreAnalysisClear);
        }

        if(nextProps.analytics.createStoreAnalysis.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                // success: true,
                // successMessage: nextProps.analytics.createStoreAnalysis.data.resource.message
            }, this.props.createStoreAnalysisClear);
            this.CloseDays();
            this.props.getAllStoreAnalysisRequest(this.state.siteCode);
            this.handleTabChange(this.state.currentTab);
        }
        else if(nextProps.analytics.createStoreAnalysis.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.analytics.createStoreAnalysis.message.status,
                errorCode: nextProps.analytics.createStoreAnalysis.message.error == undefined ? undefined : nextProps.analytics.createStoreAnalysis.message.error.errorCode,
                errorMessage: nextProps.analytics.createStoreAnalysis.message.error == undefined ? undefined : nextProps.analytics.createStoreAnalysis.message.error.errorMessage,
                storeAnalysisData: {},
                noOfDays: 0
            }, this.props.createStoreAnalysisClear);
        }

        if (nextProps.seasonPlanning.getSitePlanningFilters.isSuccess) {
            if (nextProps.seasonPlanning.getSitePlanningFilters.data.resource != null && nextProps.seasonPlanning.getSitePlanningFilters.data.resource.length != 0) {
                this.setState({
                    loading: false,
                    success: false,
                    error: false,
                    sitePlanningFilters: nextProps.seasonPlanning.getSitePlanningFilters.data.resource,
                    // siteCode: nextProps.seasonPlanning.getSitePlanningFilters.data.resource == null ? "" : nextProps.seasonPlanning.getSitePlanningFilters.data.resource[0].siteCode,
                    // siteName: nextProps.seasonPlanning.getSitePlanningFilters.data.resource == null ? "" : nextProps.seasonPlanning.getSitePlanningFilters.data.resource[0].siteName
                });
            }
            this.props.getSitePlanningFiltersClear();
        }
        else if (nextProps.seasonPlanning.getSitePlanningFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSitePlanningFilters.message.status,
                errorCode: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorMessage
            });
            this.props.getSitePlanningFiltersClear();
        }

        if (nextProps.seasonPlanning.downloadReport.isSuccess) {
            this.setState({
                loading: false,
            });
            window.location.href = nextProps.seasonPlanning.downloadReport.data.resource;
            this.props.downloadReportClear();
        }
        else if (nextProps.seasonPlanning.downloadReport.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.seasonPlanning.downloadReport.message.status,
                errorCode: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorMessage
            });
            this.props.downloadReportClear();
        }

        if(nextProps.analytics.getAllStoreAnalysis.isLoading || nextProps.analytics.getDataStoreAnalysis.isLoading || nextProps.analytics.createStoreAnalysis.isLoading || nextProps.seasonPlanning.getSitePlanningFilters.isLoading || nextProps.seasonPlanning.downloadReport.isLoading) {
            this.setState({
                loading: true,
                error: false,
                success: false,
                //storeAnalysisData: {}
            })
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false
        }, () => {

        });
        this.props.getAllStoreAnalysisRequest(this.state.siteCode);
        // this.props.getDataStoreAnalysisRequest({
        //     siteCode: this.state.siteCode,
        //     type: this.state.type,
        //     pageNo: 1,
        //     dataBy: "",
        //     dataLimit: ""
        // });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 20) {
                return false;
            }
            return true;
        }
    }

    handleTabChange = (tab) => {
        this.setState({currentTab: tab});
        if (tab === "overview") {
            this.props.getDataStoreAnalysisRequest({
                siteCode: this.state.siteCode,
                type: 1,
                pageNo: 1,
                dataBy: this.state.topMovingItemSalesValue,
                dataLimit: this.state.topMovingItemTopItemValue
            });
            this.props.getDataStoreAnalysisRequest({
                siteCode: this.state.siteCode,
                type: 2,
                pageNo: 1,
                dataBy: this.state.nonMovingItemSalesValue,
                dataLimit: this.state.nonMovingItemTopItemValue
            });
            this.props.getDataStoreAnalysisRequest({
                siteCode: this.state.siteCode,
                type: 3,
                pageNo: 1,
                dataBy: this.state.dateWiseDropValue,
                dataLimit: ""
            });
            this.props.getDataStoreAnalysisRequest({
                siteCode: this.state.siteCode,
                type: 4,
                pageNo: 1,
                dataBy: "",
                dataLimit: ""
            });
        }
        else {
            this.props.getDataStoreAnalysisRequest({
                siteCode: this.state.siteCode,
                type: 5,
                pageNo: 1,
                dataBy: this.state.salesTrendDropValue,
                dataLimit: ""
            });
            this.props.getDataStoreAnalysisRequest({
                siteCode: this.state.siteCode,
                type: 6,
                pageNo: 1,
                dataBy: this.state.salesVsGmDropValue,
                dataLimit: ""
            });
        }
    }

    page = (e, type) => {
        let payload = {
            siteCode: this.state.siteCode,
            type: type,
            dataBy: type === 3 ? this.state.dateWiseDropValue : type === 4 ? "" : type === 5 ? this.state.salesTrendDropValue : this.state.salesVsGmDropValue,
            dataLimit: ""
        };
        
        if (e.target.id == "prev") {
            if (this.state["current" + type] == "" || this.state["current" + type] == undefined || this.state["current" + type] == 1) {
            } else {
                this.setState({
                    ["prev" + type]: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.Previous,
                    ["current" + type]: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage,
                    ["next" + type]: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage + 1,
                    ["maxPage" + type]: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage
                })
                if (this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.Previous != 0) {
                    this.props.getDataStoreAnalysisRequest({pageNo: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.Previous, ...payload});
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.Previous,
                    current: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage,
                    next: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage + 1,
                    maxPage: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage
                })
            if (this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage != this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage) {
                this.props.getDataStoreAnalysisRequest({pageNo: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage + 1, ...payload});
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.Previous,
                    current: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage,
                    next: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage + 1,
                    maxPage: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage
                })
                if (this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage <= this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage) {
                    this.props.getDataStoreAnalysisRequest({pageNo: 1, ...payload});
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.Previous,
                    current: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage,
                    next: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage + 1,
                    maxPage: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage
                })
                if (this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.currPage <= this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage) {
                    this.props.getDataStoreAnalysisRequest({pageNo: this.props.analytics.getDataStoreAnalysis.data["type" + type].resource.maxPage, ...payload});
                }
            }
        }
    }

    getAnyPage = (e, type) => {
        if (e.target.validity.valid) {
            this.setState({ ["jumpPage" + type]: e.target.value })
            if (e.key == "Enter" && e.target.value != this.state.current) {
                if (e.target.value != "") {
                    let payload = {
                        pageNo: e.target.value,
                        siteCode: this.state.siteCode,
                        type: type,
                        dataBy: type === 3 ? this.state.dateWiseDropValue : type === 4 ? "" : type === 5 ? this.state.salesTrendDropValue : this.state.salesVsGmDropValue,
                        dataLimit: ""
                    };
                    this.props.getDataStoreAnalysisRequest(payload);
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }


    downloadReport = (excelType, dataBy) => {
        this.props.downloadReportRequest({
            module: "STORE_ANALYSIS_OVERVIEW_COMPARISON",
            data: {
                pageNo: 1,
                type: 1,
                search: "",
                sortedBy: "",
                filter: {},
                excelType: excelType,
                dataBy: dataBy,
                siteCode: this.state.siteCode
            }
        });
    }

    render() {
        const { searchUser } = this.state
        // var result = _.filter(this.state.storeData, function (data) {
        //   return _.startsWith(Object.keys(data), storeSearch.toLowerCase()) || _.startsWith(String.prototype.toLowerCase.apply(Object.values(data)), storeSearch.toLowerCase())
        // });
        return (
            <div className={this.state.sitePlanningFilters.length == 0 ? "container-fluid pad-0" : "container-fluid pad-0 bg-gray"}>
                {/* <div className="col-md-12 col-sm-12 col-xs-12">
                    <div className="container_content height-70vh noPlanExist searchStoreProfileMain ars-store-ana">
                        <div className="middleContent " ref={(e) => this.setWrapperRef(e)}>
                            <h1>Choose Store</h1>
                            <h2>Select Store from below to view complete profile details</h2>
                            <input type="search" placeholder="Search Store" id="storeProfile" value={this.state.searchUser} onKeyPress={(e) => this._handleKeyPress(e)} onChange={(e) => this.handleChange(e)} onFocus={() => this.setState({ dropDownShow: true })} autoComplete="off" />
                            {this.state.dropDownShow ? <div className="storeSearchDrop m-top-7" >
                                <ul id="myUL">
                                    {this.state.storeData.length != 0 ? this.state.storeData.map((data, key) => (
                                        <li key={key} onClick={(e) => this.onnChange(Object.keys(data),Object.values(data))}><label>{Object.keys(data) + "-" + Object.values(data)}</label></li>

                                    )) : <li ><label>No Data Found</label></li>}

                                    <li className="hideElement" id="noResult">
                                        <label> No Data Found</label>
                                    </li>
                                </ul>
                        </div> : null}
                        {this.state.storeSearch != "" ? <button className="record_btn" type="button" onClick={(e) => this.onSearch(e)}>
                            Load Profile
                                </button> : <button className="record_btn btnDisabled" type="button">
                            Load Profile
                                </button>}
                        </div>
                    </div>
                </div>
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null} */}
                <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                    <div className="store-analytics-head">
                        <div className="sah-left">
                            <h3>Store Analysis</h3>
                            <div className="vendor-dashboard-tabs">
                                <div className="vendor-tablist-item">
                                    <ul className="nav nav-tabs vd-tab-list" role="tablist">
                                        <li className="nav-item active">
                                            <a className="nav-link vd-tab-btn" href="#overviewTab" role="tab" data-toggle="tab" onClick={() => this.handleTabChange("overview")}>Overview</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link vd-tab-btn" href="#comparisionTab" role="tab" data-toggle="tab" onClick={() => this.handleTabChange("comparison")}>Comparison</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="sah-right">
                            <div className="sahr-store-drop">
                                <button type="button" className="sahsd-btn" onClick={(e) => this.openStore(e)}>{this.state.siteName}</button>
                                {this.state.showStores && 
                                <div className="sahrsd-dropdown" id="stores">
                                    <ul className="sahrsdd-inner">
                                        <li className="sahrsdd-search">
                                            <input type="search" placeholder="Search Site Name" onChange={(e) => this.setState({searchSite: e.target.value})} />
                                            <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                        </li>
                                        <li className="bold" onClick={() => {
                                            this.setState({
                                                siteCode: 0,
                                                siteName: "All Sites",
                                                searchSite: "",
                                                showStores: false
                                            }, () => {
                                                this.props.getAllStoreAnalysisRequest(0);
                                                this.handleTabChange(this.state.currentTab);
                                            });
                                        }}>All Sites</li>
                                        {
                                            this.state.sitePlanningFilters.map((item) =>
                                            item.siteCode.toString().includes(this.state.searchSite) || item.siteName.includes(this.state.searchSite) ?
                                                <li onClick={() => {
                                                    this.setState({
                                                        siteCode: item.siteCode,
                                                        siteName: item.siteName,
                                                        searchSite: "",
                                                        showStores: false
                                                    }, () => {
                                                        this.props.getAllStoreAnalysisRequest(item.siteCode);
                                                        this.handleTabChange(this.state.currentTab);
                                                    });
                                                }}>
                                                    {item.siteCode} - {item.siteName}
                                                </li> : null)
                                        }
                                    </ul>
                                </div>}
                            </div>
                            <div className="sahr-store-drop">
                                <button type="button" className="sahsd-btn" onClick={(e) => this.openDays(e)}>Last {this.state.noOfDays} Days</button>
                                {/* {this.state.showDays && 
                                <div className="sahrsd-dropdown">
                                    <ul className="sahrsdd-inner">
                                        <li className="sahrsdd-head">Date Range Filter</li>
                                        <li>Last Week</li>
                                        <li>Last Month</li>
                                        <li>This Month</li>
                                        <li>Last 7 Days</li>
                                        <li>Last 30 Days</li>
                                        <li>Last 60 Days</li>
                                        <li>Last 90 Days</li>
                                        <li>Custom</li>
                                    </ul>
                                </div>} */}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.sitePlanningFilters.length == 0 ?
                <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                    <div className="summary-page-status m-top-30">
                        <div className="sps-error-msg">
                            <span className="spsem-msg">
                                <svg xmlns="http://www.w3.org/2000/svg" id="information" width="14.413" height="14.413" viewBox="0 0 15.413 15.413">
                                    <path fill="#ff8103" id="Path_1092" d="M7.706 0a7.706 7.706 0 1 0 7.706 7.706A7.715 7.715 0 0 0 7.706 0zm0 14.012a6.305 6.305 0 1 1 6.305-6.305 6.312 6.312 0 0 1-6.305 6.305z" class="cls-1"/>
                                    <path fill="#ff8103" id="Path_1093" d="M145.936 70a.934.934 0 1 0 .934.935.935.935 0 0 0-.934-.935z" class="cls-1" transform="translate(-138.23 -66.731)"/>
                                    <path fill="#ff8103" id="Path_1094" d="M150.7 140a.7.7 0 0 0-.7.7v4.2a.7.7 0 0 0 1.4 0v-4.2a.7.7 0 0 0-.7-.7z" class="cls-1" transform="translate(-142.994 -133.461)"/>
                                </svg>
                                No Sites Found!
                            </span>
                            <p>Currently no site has been configured to your account. Please contact support at support@supplymint.com</p>
                        </div>
                    </div>
                </div> :
                <div className="tab-content">
                    <div role="tabpanel" className="tab-pane fade in active" id="overviewTab">
                        <div className="col-lg-12 col-md-12 col-sm-12">
                            <div className="store-analytics-overview-card">
                                <div className="col-lg-7 col-md-7 col-sm-12 pad-0">
                                    <div className="col-lg-4 col-md-4 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-blue"><img src={require('../../assets/availableInventory.svg')} /></span>
                                            <span className="saocic-price">{this.state.storeAnalysisData.totalavailableinventory == undefined ? "NA" : this.state.storeAnalysisData.totalavailableinventory}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.totalavailableinventory == undefined ? "NA" : this.state.mouseHoverData.totalavailableinventory}</div>
                                            </span>
                                            <p>Total Available Inventory</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-yellow"><img src={require('../../assets/sku.svg')} /></span>
                                            <span className="saocic-price">{this.state.storeAnalysisData.totalskus == undefined ? "NA" : this.state.storeAnalysisData.totalskus}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.totalskus == undefined ? "NA" : this.state.mouseHoverData.totalskus}</div>
                                            </span>
                                            <p>Total SKU's</p>
                                            <span className="saocic-circle"></span>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-red"><img src={require('../../assets/costInventory.svg')} /></span>
                                            <span className="saocic-price"><img src={require('../../assets/rupee.svg')} />{this.state.storeAnalysisData.costofinventory == undefined ? "NA" : this.state.storeAnalysisData.costofinventory}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.costofinventory == undefined ? "NA" : this.state.mouseHoverData.costofinventory}</div>
                                            </span>
                                            <p>Cost of Inventory</p>
                                            {/* <span className="saocic-groth-percentage"><span className="triangle-up"></span>112.12%</span> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-5 col-md-5 col-sm-12 pad-0">
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-pprl"><img src={require('../../assets/revenue.svg')} /></span>
                                            <span className="saocic-price"><img src={require('../../assets/rupee.svg')} />{this.state.storeAnalysisData.revenue == undefined ? "NA" : this.state.storeAnalysisData.revenue}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.revenue == undefined ? "NA" : this.state.mouseHoverData.revenue}</div>
                                            </span>
                                            <p>Revenue</p>
                                            {/* <span className="saocic-degroth-percentage"><span className="triangle-down"></span>98.12%</span> */}
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon"><img src={require('../../assets/sold.svg')} /></span>
                                            <span className="saocic-price">{this.state.storeAnalysisData.soldqty == undefined ? "NA" : this.state.storeAnalysisData.soldqty}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.soldqty == undefined ? "NA" : this.state.mouseHoverData.soldqty}</div>
                                            </span>
                                            <p>Sold Qty</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                            <div className="col-lg-6 col-md-6 col-sm-12 pad-lft-0">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-lft-0">
                                    <div className="store-analytics-table-head">
                                        <div className="sath-left">
                                            <h3>Top Moving Item</h3>
                                            <div className="nph-switch-btn">
                                                <label className="tg-switch" >
                                                    <input type="checkbox" checked={this.state.topMovingItemdataType == "table"} onChange={(e) => this.changeTopMovingItemdataType(e)} />
                                                    <span className="tg-slider tg-round"></span>
                                                    {this.state.topMovingItemdataType === "table" ? 
                                                    <span className="nph-wbtext">Table</span>: 
                                                    <span className="nph-wbtext">Graph</span>}
                                                </label>
                                            </div>
                                        </div>
                                        <div className="sath-right">
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openMovingItemSales(e)}>{this.state.topMovingItemSalesValue === "sales" ? "Sales" : "Sell Through"}</button>
                                                {this.state.topMovingItemSales &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 1, pageNo: 1, siteCode: this.state.siteCode, dataBy: "sales", dataLimit: this.state.topMovingItemTopItemValue}); this.setState({topMovingItemSalesValue: "sales"})}}>Sales</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 1, pageNo: 1, siteCode: this.state.siteCode, dataBy: "sellThrough", dataLimit: this.state.topMovingItemTopItemValue}); this.setState({topMovingItemSalesValue: "sellThrough"})}}>Sell Through</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openTopItem(e)}>Top {this.state.topMovingItemTopItemValue}</button>
                                                {this.state.topMovingItemTopItem &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 1, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.topMovingItemSalesValue, dataLimit: 5}); this.setState({topMovingItemTopItemValue: 5})}}>Top 5</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 1, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.topMovingItemSalesValue, dataLimit: 10}); this.setState({topMovingItemTopItemValue: 10})}}>Top 10</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 1, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.topMovingItemSalesValue, dataLimit: 20}); this.setState({topMovingItemTopItemValue: 20})}}>Top 20</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 1, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.topMovingItemSalesValue, dataLimit: 50}); this.setState({topMovingItemTopItemValue: 50})}}>Top 50</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 m-top-10 pad-lft-0">
                                    {this.state.topMovingItemdataType === "table" ?
                                    <div className="store-analytics-table-body">
                                        <div className="satb-head">
                                            <button type="button"><img src={require('../../assets/dashBDownload.svg')} onClick={() => this.downloadReport("Store Analysis top moving item", this.state.topMovingItemSalesValue)} /></button>
                                        </div>
                                        <div className="satb-table">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th><label>SKU</label></th>
                                                        <th><label>Name</label></th>
                                                        <th style={{textAlign: "right"}}><label>Available Inventory</label></th>
                                                        <th style={{textAlign: "right"}}><label>{this.state.topMovingItemSalesValue === "sales" ? "Sales" : "Sell Through"}</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>{
                                                    this.state.topMovingItemData.length == 0 ?
                                                    <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                                    this.state.topMovingItemData.map((item) => (
                                                        <tr>
                                                            <td><label>{item.sku}</label></td>
                                                            <td>
                                                                <label>{item.name}</label>
                                                                {this.small && <div className="table-tooltip"><label>{item.name}</label></div>}
                                                            </td>
                                                            <td style={{textAlign: "right"}}><label>{item.availableinventory}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{this.state.topMovingItemSalesValue === "sales" ? item.sales : item.sellthrough}</label></td>
                                                        </tr>
                                                    ))
                                                }</tbody>
                                            </table>
                                        </div>
                                    </div> : 
                                    <div className="store-analytics-table-body">
                                        <ComposedChart height={380} width={558} data={this.state.topMovingItemData}>
                                            <XAxis dataKey="sku" />
                                            <YAxis yAxisId="left" />
                                            <YAxis yAxisId="right" orientation="right" />
                                            <CartesianGrid stroke="#cccccc" />
                                            <Tooltip />
                                            <Legend />
                                            <Bar yAxisId="left" dataKey="availableinventory" barSize={20} fill="#413ea0" />
                                            <Line yAxisId="right" type="monotone" dataKey={this.state.topMovingItemSalesValue === "sales" ? "sales" : "sellthrough"} stroke="#ff7300" />
                                        </ComposedChart>
                                    </div>}
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-12 pad-rgt-0">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-rgt-0">
                                    <div className="store-analytics-table-head">
                                        <div className="sath-left">
                                            <h3>Non Moving Item</h3>
                                            <div className="nph-switch-btn">
                                                <label className="tg-switch" >
                                                    <input type="checkbox" checked={this.state.nonMovingItemdataType == "table"} onChange={(e) => this.changeNonMovingItemdataType(e)} />
                                                    <span className="tg-slider tg-round"></span>
                                                    {this.state.nonMovingItemdataType === "table" ? 
                                                    <span className="nph-wbtext">Table</span>: 
                                                    <span className="nph-wbtext">Graph</span>}
                                                </label>
                                            </div>
                                        </div>
                                        <div className="sath-right">
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openNonMovingItemSales(e)}>{this.state.nonMovingItemSalesValue === "sales" ? "Sales" : "Sell Through"}</button>
                                                {this.state.nonMovingItemSales &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 2, pageNo: 1, siteCode: this.state.siteCode, dataBy: "sales", dataLimit: this.state.nonMovingItemTopItemValue}); this.setState({nonMovingItemSalesValue: "sales"})}}>Sales</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 2, pageNo: 1, siteCode: this.state.siteCode, dataBy: "sellThrough", dataLimit: this.state.nonMovingItemTopItemValue}); this.setState({nonMovingItemSalesValue: "sellThrough"})}}>Sell Through</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openNonMovingItemTopItem(e)}>Bottom {this.state.nonMovingItemTopItemValue}</button>
                                                {this.state.nonMovingItemTopItem &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 2, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.nonMovingItemSalesValue, dataLimit: 5}); this.setState({nonMovingItemTopItemValue: 5})}}>Bottom 5</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 2, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.nonMovingItemSalesValue, dataLimit: 10}); this.setState({nonMovingItemTopItemValue: 10})}}>Bottom 10</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 2, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.nonMovingItemSalesValue, dataLimit: 20}); this.setState({nonMovingItemTopItemValue: 20})}}>Bottom 20</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 2, pageNo: 1, siteCode: this.state.siteCode, dataBy: this.state.nonMovingItemSalesValue, dataLimit: 50}); this.setState({nonMovingItemTopItemValue: 50})}}>Bottom 50</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 m-top-10 pad-rgt-0">
                                    {this.state.nonMovingItemdataType === "table" ?
                                    <div className="store-analytics-table-body">
                                        <div className="satb-head">
                                            <button type="button"><img src={require('../../assets/dashBDownload.svg')} onClick={() => this.downloadReport("Store Analysis non moving item", this.state.nonMovingItemSalesValue)} /></button>
                                        </div>
                                        <div className="satb-table">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th><label>SKU</label></th>
                                                        <th><label>Name</label></th>
                                                        <th style={{textAlign: "right"}}><label>Available Inventory</label></th>
                                                        <th style={{textAlign: "right"}}><label>{this.state.nonMovingItemSalesValue === "sales" ? "Sales" : "Sell Through"}</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>{
                                                    this.state.nonMovingItemData.length == 0 ?
                                                    <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                                    this.state.nonMovingItemData.map((item) => (
                                                        <tr>
                                                            <td><label>{item.sku}</label></td>
                                                            <td>
                                                                <label>{item.name}</label>
                                                                {this.small && <div className="table-tooltip"><label>{item.name}</label></div>}
                                                            </td>
                                                            <td style={{textAlign: "right"}}><label>{item.availableinventory}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{this.state.nonMovingItemSalesValue === "sales" ? item.sales : item.sellthrough}</label></td>
                                                        </tr>
                                                    ))
                                                }</tbody>
                                            </table>
                                        </div>
                                    </div> : 
                                    <div className="store-analytics-table-body">
                                        <ComposedChart height={380} width={558} data={this.state.nonMovingItemData}>
                                            <XAxis dataKey="sku" />
                                            <YAxis yAxisId="left" />
                                            <YAxis yAxisId="right" orientation="right" />
                                            <CartesianGrid stroke="#cccccc" />
                                            <Tooltip />
                                            <Legend />
                                            <Bar yAxisId="left" dataKey="availableinventory" barSize={20} fill="#413ea0" />
                                            <Line yAxisId="right" type="monotone" dataKey={this.state.nonMovingItemSalesValue === "sales" ? "sales" : "sellthrough"} stroke="#ff7300" />
                                        </ComposedChart>
                                    </div>}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                            <div className="col-lg-6 col-md-6 col-sm-12 pad-lft-0">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-lft-0">
                                    <div className="store-analytics-table-head">
                                        <div className="sath-left">
                                            <h3>Date Wise AOV</h3>
                                            <div className="nph-switch-btn">
                                                <label className="tg-switch" >
                                                    <input type="checkbox" checked={this.state.dateWisedataType == "table"} onChange={(e) => this.changeDateWisedataType(e)} />
                                                    <span className="tg-slider tg-round"></span>
                                                    {this.state.dateWisedataType === "table" ? 
                                                    <span className="nph-wbtext">Table</span>: 
                                                    <span className="nph-wbtext">Graph</span>}
                                                </label>
                                            </div>
                                        </div>
                                        <div className="sath-right">
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openDateWiseDrop(e)}>{this.state.dateWiseDropValue === "division" ? "Division" : this.state.dateWiseDropValue === "section" ? "Section" : this.state.dateWiseDropValue === "department" ? "Department" : "Category"}</button>
                                                {this.state.dateWiseDrop &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 3, pageNo: 1, siteCode: this.state.siteCode, dataBy: "division", dataLimit: ""}); this.setState({dateWiseDropValue: "division"})}}>Division</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 3, pageNo: 1, siteCode: this.state.siteCode, dataBy: "section", dataLimit: ""}); this.setState({dateWiseDropValue: "section"})}}>Section</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 3, pageNo: 1, siteCode: this.state.siteCode, dataBy: "department", dataLimit: ""}); this.setState({dateWiseDropValue: "department"})}}>Department</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 3, pageNo: 1, siteCode: this.state.siteCode, dataBy: "category", dataLimit: ""}); this.setState({dateWiseDropValue: "category"})}}>Category</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 m-top-10 pad-lft-0">
                                    {this.state.dateWisedataType === "table" ?
                                    <div className="store-analytics-table-body">
                                        <div className="satb-head">
                                            <button type="button"><img src={require('../../assets/dashBDownload.svg')} onClick={() => this.downloadReport("Store Analysis date wise AOV", this.state.dateWiseDropValue)} /></button>
                                        </div>
                                        <div className="satb-table">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th><label>Date</label></th>
                                                        {this.state.dateWiseDropValue === "division" && <th><label>Division</label></th>}
                                                        {this.state.dateWiseDropValue === "section" && <th><label>Section</label></th>}
                                                        {this.state.dateWiseDropValue === "department" && <th><label>Department</label></th>}
                                                        {this.state.dateWiseDropValue === "category" && <th><label>Category</label></th>}
                                                        <th style={{textAlign: "right"}}><label>Sold Qty</label></th>
                                                        <th style={{textAlign: "right"}}><label>Sales Amount</label></th>
                                                        <th style={{textAlign: "right"}}><label>AOV</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>{
                                                    this.state.dateWiseAovData.length == 0 ?
                                                    <tr><td colSpan="8"><label>NO DATA FOUND!</label></td></tr> :
                                                    this.state.dateWiseAovData.map((item) => (
                                                        <tr>
                                                            <td><label>{item.bill_date}</label></td>
                                                            {this.state.dateWiseDropValue === "division" && <td><label>{item.division}</label></td>}
                                                            {this.state.dateWiseDropValue === "section" && <td><label>{item.section}</label></td>}
                                                            {this.state.dateWiseDropValue === "department" && <td><label>{item.department}</label></td>}
                                                            {this.state.dateWiseDropValue === "category" && <td><label>{item.category}</label></td>}
                                                            <td style={{textAlign: "right"}}><label>{item.sold_qty}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.sales_amt}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.aov}</label></td>
                                                        </tr>
                                                    ))
                                                }</tbody>
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage3} min="1" onKeyPress={(e) => this.getAnyPage(e, 3)} onChange={(e) => this.getAnyPage(e, 3)} value={this.state.jumpPage3} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems3}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={(e) => this.page(e, 3)}
                                                        prev={this.state.prev3} current={this.state.current3} maxPage={this.state.maxPage3} next={this.state.next3} />
                                                </div>
                                            </div>
                                        </div>
                                    </div> :
                                    <div className="store-analytics-table-body">
                                        <ComposedChart height={380} width={558} data={this.state.dateWiseAovData}>
                                            <XAxis dataKey={this.state.dateWiseDropValue} />
                                            <YAxis yAxisId="left" />
                                            <YAxis yAxisId="right" orientation="right" />
                                            <CartesianGrid stroke="#cccccc" />
                                            <Tooltip />
                                            <Legend />
                                            <Bar yAxisId="left" dataKey="sales_amt" barSize={20} fill="#413ea0" />
                                            <Line yAxisId="right" type="monotone" dataKey="aov" stroke="#ff7300" />
                                        </ComposedChart>
                                    </div>}
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-12 pad-rgt-0">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-rgt-0">
                                    <div className="store-analytics-table-head">
                                        <div className="sath-left">
                                            <h3>Item to be Stock Out</h3>
                                            {/* <div className="nph-switch-btn">
                                                <label className="tg-switch" >
                                                    <input type="checkbox" checked={this.state.itemStockdataType == "table"} onChange={(e) => this.changeItemStockdataType(e)} />
                                                    <span className="tg-slider tg-round"></span>
                                                    {this.state.itemStockdataType === "table" ? 
                                                    <span className="nph-wbtext">Table</span>: 
                                                    <span className="nph-wbtext">Graph</span>}
                                                </label>    
                                            </div> */}
                                        </div>
                                        <div className="sath-right">
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 m-top-10 pad-rgt-0">
                                    {this.state.itemStockdataType === "table" ?
                                    <div className="store-analytics-table-body">
                                        <div className="satb-head">
                                            <button type="button"><img src={require('../../assets/dashBDownload.svg')} onClick={() => this.downloadReport("Store Analysis item to be stock out", "")} /></button>
                                        </div>
                                        <div className="satb-table">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th><label>SKU</label></th>
                                                        <th><label>Name</label></th>
                                                        <th style={{textAlign: "right"}}><label>Rate of Sale</label></th>
                                                        <th style={{textAlign: "right"}}><label>Current Inv</label></th>
                                                        <th style={{textAlign: "right"}}><label>Inventory in-hand Days</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>{
                                                    this.state.itemToBeStockOutData.length == 0 ?
                                                    <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                                    this.state.itemToBeStockOutData.map((item) => (
                                                        <tr>
                                                            <td><label>{item.sku}</label></td>
                                                            <td>
                                                                <label>{item.name}</label>
                                                                {this.small && <div className="table-tooltip"><label>{item.name}</label></div>}
                                                            </td>
                                                            <td style={{textAlign: "right"}}><label>{item.rateofsale}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.stockqty}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.inventoryinhanddays}</label></td>
                                                        </tr>
                                                    ))
                                                }</tbody>
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage4} min="1" onKeyPress={(e) => this.getAnyPage(e, 4)} onChange={(e) => this.getAnyPage(e, 4)} value={this.state.jumpPage4} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems4}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={(e) => this.page(e, 4)}
                                                        prev={this.state.prev4} current={this.state.current4} maxPage={this.state.maxPage4} next={this.state.next4} />
                                                </div>
                                            </div>
                                        </div>
                                    </div> :
                                    <div className="store-analytics-table-body"></div>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="comparisionTab">
                        <div className="col-lg-12 col-md-12 col-sm-12">
                            <div className="store-analytics-overview-card">
                                <div className="col-lg-7 col-md-7 col-sm-12 pad-0">
                                <div className="col-lg-4 col-md-4 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-blue"><img src={require('../../assets/availableInventory.svg')} /></span>
                                            <span className="saocic-price">{this.state.storeAnalysisData.totalavailableinventory == undefined ? "NA" : this.state.storeAnalysisData.totalavailableinventory}
                                            <div className="generic-tooltip">{this.state.mouseHoverData.totalavailableinventory == undefined ? "NA" : this.state.mouseHoverData.totalavailableinventory}</div>
                                            </span>
                                            <p>Total Available Inventory</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-yellow"><img src={require('../../assets/sku.svg')} /></span>
                                            <span className="saocic-price">{this.state.storeAnalysisData.totalskus == undefined ? "NA" : this.state.storeAnalysisData.totalskus}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.totalskus == undefined ? "NA" : this.state.mouseHoverData.totalskus}</div>
                                            </span>
                                            <p>Total SKU's</p>
                                            <span className="saocic-circle"></span>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-red"><img src={require('../../assets/costInventory.svg')} /></span>
                                            <span className="saocic-price"><img src={require('../../assets/rupee.svg')} />{this.state.storeAnalysisData.costofinventory == undefined ? "NA" : this.state.storeAnalysisData.costofinventory}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.costofinventory == undefined ? "NA" : this.state.mouseHoverData.costofinventory}</div>
                                            </span>
                                            <p>Cost of Inventory</p>
                                            <span className="saocic-groth-percentage"><span className="triangle-up"></span>112.12%</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-5 col-md-5 col-sm-12 pad-0">
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon bg-pprl"><img src={require('../../assets/revenue.svg')} /></span>
                                            <span className="saocic-price"><img src={require('../../assets/rupee.svg')} />{this.state.storeAnalysisData.revenue == undefined ? "NA" : this.state.storeAnalysisData.revenue}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.revenue == undefined ? "NA" : this.state.mouseHoverData.revenue}</div>
                                            </span>
                                            <p>Revenue</p>
                                            <span className="saocic-degroth-percentage"><span className="triangle-down"></span>98.12%</span>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6">
                                        <div className="saoc-inner-card">
                                            <span className="saocic-icon"><img src={require('../../assets/sold.svg')} /></span>
                                            <span className="saocic-price">{this.state.storeAnalysisData.soldqty == undefined ? "NA" : this.state.storeAnalysisData.soldqty}
                                                <div className="generic-tooltip">{this.state.mouseHoverData.soldqty == undefined ? "NA" : this.state.mouseHoverData.soldqty}</div>
                                            </span>
                                            <p>Sold Qty</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                            <div className="col-lg-6 col-md-6 col-sm-12 pad-lft-0">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-lft-0">
                                    <div className="store-analytics-table-head">
                                        <div className="sath-left">
                                            <h3>Sales Trend Comparison</h3>
                                            <div className="nph-switch-btn">
                                                <label className="tg-switch" >
                                                    <input type="checkbox" checked={this.state.salesTrenddataType == "table"} onChange={(e) => this.changeSalesTrenddataType(e)} />
                                                    <span className="tg-slider tg-round"></span>
                                                    {this.state.salesTrenddataType === "table" ? 
                                                    <span className="nph-wbtext">Table</span>: 
                                                    <span className="nph-wbtext">Graph</span>}
                                                </label>
                                            </div>
                                        </div>
                                        <div className="sath-right">
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openSalesTrendDrop(e)}>{this.state.salesTrendDropValue === "division" ? "Division" : this.state.salesTrendDropValue === "section" ? "Section" : this.state.salesTrendDropValue === "department" ? "Department" : "Category"}</button>
                                                {this.state.salesTrendDrop &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 5, pageNo: 1, siteCode: this.state.siteCode, dataBy: "division", dataLimit: ""}); this.setState({salesTrendDropValue: "division"})}}>Division</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 5, pageNo: 1, siteCode: this.state.siteCode, dataBy: "section", dataLimit: ""}); this.setState({salesTrendDropValue: "section"})}}>Section</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 5, pageNo: 1, siteCode: this.state.siteCode, dataBy: "department", dataLimit: ""}); this.setState({salesTrendDropValue: "department"})}}>Department</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 5, pageNo: 1, siteCode: this.state.siteCode, dataBy: "category", dataLimit: ""}); this.setState({salesTrendDropValue: "category"})}}>Category</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 m-top-10 pad-lft-0">
                                    {this.state.salesTrenddataType === "table" ?
                                    <div className="store-analytics-table-body">
                                        <div className="satb-head">
                                            <button type="button"><img src={require('../../assets/dashBDownload.svg')} onClick={() => this.downloadReport("Sales trend comparison by Category", this.state.salesTrendDropValue)} /></button>
                                        </div>
                                        <div className="satb-table">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        {this.state.salesTrendDropValue === "division" && <th><label>Division</label></th>}
                                                        {this.state.salesTrendDropValue === "section" && <th><label>Section</label></th>}
                                                        {this.state.salesTrendDropValue === "department" && <th><label>Department</label></th>}
                                                        {this.state.salesTrendDropValue === "category" && <th><label>Category</label></th>}
                                                        <th style={{textAlign: "right"}}><label>Sales Qty</label></th>
                                                        <th style={{textAlign: "right"}}><label>Sales Amount</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>{
                                                    this.state.salesTrendData.length == 0 ?
                                                    <tr><td colSpan="100"><label>NO DATA FOUND!</label></td></tr> :
                                                    this.state.salesTrendData.map((item) => (
                                                        <tr>
                                                            {this.state.salesTrendDropValue === "division" && <td><label>{item.division}</label></td>}
                                                            {this.state.salesTrendDropValue === "section" && <td><label>{item.section}</label></td>}
                                                            {this.state.salesTrendDropValue === "department" && <td><label>{item.department}</label></td>}
                                                            {this.state.salesTrendDropValue === "category" && <td><label>{item.category}</label></td>}
                                                            <td style={{textAlign: "right"}}><label>{item.saleqty}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.sellingamt}</label></td>
                                                        </tr>
                                                    ))
                                                }</tbody>
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage5} min="1" onKeyPress={(e) => this.getAnyPage(e, 5)} onChange={(e) => this.getAnyPage(e, 5)} value={this.state.jumpPage5} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems5}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={(e) => this.page(e, 5)}
                                                        prev={this.state.prev5} current={this.state.current5} maxPage={this.state.maxPage5} next={this.state.next5} />
                                                </div>
                                            </div>
                                        </div>
                                    </div> :
                                    <div className="store-analytics-table-body">
                                        <ComposedChart height={380} width={558} data={this.state.salesTrendData}>
                                            <XAxis dataKey={this.state.salesTrendDropValue} />
                                            <YAxis yAxisId="left" />
                                            <YAxis yAxisId="right" orientation="right" />
                                            <CartesianGrid stroke="#cccccc" />
                                            <Tooltip />
                                            <Legend />
                                            <Bar yAxisId="left" dataKey="sellingamt" barSize={20} fill="#413ea0" />
                                            <Line yAxisId="right" type="monotone" dataKey="saleqty" stroke="#ff7300" />
                                        </ComposedChart>
                                    </div>}
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-12 pad-rgt-0">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-rgt-0">
                                    <div className="store-analytics-table-head">
                                        <div className="sath-left">
                                            <h3>Sales vs GM</h3>
                                            <div className="nph-switch-btn">
                                                <label className="tg-switch" >
                                                    <input type="checkbox" checked={this.state.salesGmdataType == "table"} onChange={(e) => this.changeSalesGmdataType(e)} />
                                                    <span className="tg-slider tg-round"></span>
                                                    {this.state.salesGmdataType === "table" ? 
                                                    <span className="nph-wbtext">Table</span>: 
                                                    <span className="nph-wbtext">Graph</span>}
                                                </label>
                                            </div>
                                        </div>
                                        <div className="sath-right">
                                            <div className="sathr-sales-through">
                                                <button type="button" className="" onClick={(e) => this.openSalesVsGmDrop(e)}>{this.state.salesVsGmDropValue === "division" ? "Division" : this.state.salesVsGmDropValue === "section" ? "Section" : this.state.salesVsGmDropValue === "department" ? "Department" : "Category"}</button>
                                                {this.state.salesVsGmDrop &&
                                                <div className="sathrst-dropdown">
                                                    <ul className="sathrstd-inner">
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 6, pageNo: 1, siteCode: this.state.siteCode, dataBy: "division", dataLimit: ""}); this.setState({salesVsGmDropValue: "division"})}}>Division</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 6, pageNo: 1, siteCode: this.state.siteCode, dataBy: "section", dataLimit: ""}); this.setState({salesVsGmDropValue: "section"})}}>Section</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 6, pageNo: 1, siteCode: this.state.siteCode, dataBy: "department", dataLimit: ""}); this.setState({salesVsGmDropValue: "department"})}}>Department</li>
                                                        <li onClick={() => {this.props.getDataStoreAnalysisRequest({type: 6, pageNo: 1, siteCode: this.state.siteCode, dataBy: "category", dataLimit: ""}); this.setState({salesVsGmDropValue: "category"})}}>Category</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 m-top-10 pad-rgt-0">
                                    {this.state.salesGmdataType === "table" ?
                                    <div className="store-analytics-table-body">
                                        <div className="satb-head">
                                            <button type="button"><img src={require('../../assets/dashBDownload.svg')} onClick={() => this.downloadReport("Sales vs GM", this.state.salesVsGmDropValue)} /></button>
                                        </div>
                                        <div className="satb-table">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        {this.state.salesVsGmDropValue === "division" && <th><label>Division</label></th>}
                                                        {this.state.salesVsGmDropValue === "section" && <th><label>Section</label></th>}
                                                        {this.state.salesVsGmDropValue === "department" && <th><label>Department</label></th>}
                                                        {this.state.salesVsGmDropValue === "category" && <th><label>Category</label></th>}
                                                        <th style={{textAlign: "right"}}><label>Sales Qty</label></th>
                                                        <th style={{textAlign: "right"}}><label>Sales Amount</label></th>
                                                        <th style={{textAlign: "right"}}><label>GM %</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>{
                                                    this.state.salesVsGmData.length == 0 ?
                                                    <tr><td colSpan="100"><label>NO DATA FOUND!</label></td></tr> :
                                                    this.state.salesVsGmData.map((item) => (
                                                        <tr>
                                                            {this.state.salesVsGmDropValue === "division" && <td><label>{item.division}</label></td>}
                                                            {this.state.salesVsGmDropValue === "section" && <td><label>{item.section}</label></td>}
                                                            {this.state.salesVsGmDropValue === "department" && <td><label>{item.department}</label></td>}
                                                            {this.state.salesVsGmDropValue === "category" && <td><label>{item.category}</label></td>}
                                                            <td style={{textAlign: "right"}}><label>{item.saleqty}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.sellingamt}</label></td>
                                                            <td style={{textAlign: "right"}}><label>{item.gmp}</label></td>
                                                        </tr>
                                                    ))
                                                }</tbody>
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage6} min="1" onKeyPress={(e) => this.getAnyPage(e, 6)} onChange={(e) => this.getAnyPage(e, 6)} value={this.state.jumpPage6} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems6}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={(e) => this.page(e, 6)}
                                                        prev={this.state.prev6} current={this.state.current6} maxPage={this.state.maxPage6} next={this.state.next6} />
                                                </div>
                                            </div>
                                        </div>
                                    </div> :
                                    <div className="store-analytics-table-body">
                                        <ComposedChart height={380} width={558} data={this.state.salesVsGmData}>
                                            <XAxis dataKey={this.state.salesVsGmDropValue} />
                                            <YAxis yAxisId="left" />
                                            <YAxis yAxisId="right" orientation="right" />
                                            <CartesianGrid stroke="#cccccc" />
                                            <Tooltip />
                                            <Legend />
                                            <Bar yAxisId="left" dataKey="sellingamt" barSize={20} fill="#413ea0" />
                                            <Line yAxisId="right" type="monotone" dataKey="gmp" stroke="#ff7300" />
                                        </ComposedChart>
                                    </div>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
                {this.state.showDays && <DateRangeFilter days={this.state.noOfDays} CloseDays={this.CloseDays} done={this.props.createStoreAnalysisRequest} />}
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        );
    }
}

export default ChooseStoreProfile;