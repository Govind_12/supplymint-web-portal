import React, { useState } from 'react';
import { ResponsiveContainer, Label, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, BarChart } from 'recharts';
import storeIcon from '../../assets/store.svg';
import searchIcon from '../../assets/search.svg';
import { BarcodeStoreModal } from './barcodeStoreModal';

const data = [
    {
        name: 'Page A', uv: 100, pv: 100, amt: 100,
    },
    {
        name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
    },
    {
        name: 'Page C', uv: 2000, pv: 9800, amt: 2290,
    },
    {
        name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
    },
    {
        name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
    },
    {
        name: 'Page F', uv: 2390, pv: 3800, amt: 2500,
    },
    {
        name: 'Page G', uv: 3490, pv: 4300, amt: 2100,
    },
];
const CustomTooltip = ({ active, payload, label }) => {
    if (active) {
        return (
            <div className="customToolTipGraph">
                <div className="subscribers-by-channel-tooltip">
                    <div className="subscribers-by-channel-tooltip-label"><label>Barcode :</label><span>{label}</span></div>
                    <div className="subscribers-by-channel-tooltip-value m-top-5">
                        <label>Sell Thru Percentage  : </label><span>{` ${payload[0].value}`}</span>
                    </div>
                </div>
            </div>
        );
    }

    return null;
};

export const SellThru = () => {
    const [sellDrop, setSellDrop] = useState(false)
    const [storeSelected, setStoreSelected] = useState(false)
    return (
        <div>
            <div className="container-fluid pad-0">
                <div className="container_div" id="home">
                    <div className="container-fluid">
                        <div className="container_div" id="">
                            <div className="container-fluid">
                                <div className="replenishment_container container-pad">
                                    <label className="contribution_mart">Sell Thru</label>
                                    <div className="row margin-right0 m-lft-0 pad-0 sellThruHead m-top-15">
                                        <div className="col-md-7 pad-lft-0">
                                            <div className="left">
                                                {storeSelected ? <div><div className="col-md-3 divPad borderRight">
                                                    <h3>Sell Thru</h3>
                                                    <h2 className="m-top-10">Barcode</h2>
                                                </div>
                                                    <div className="col-md-9 displayBlock divPad alignEnd">
                                                        <h3>View by Stores</h3>
                                                        <div className="col-md-12 pad-0 m-top-5 alignMiddle">
                                                            <div className="col-md-3 pad-0">
                                                                <button type="button">Top 5</button>
                                                            </div>
                                                            <div className="col-md-3 pad-0">
                                                                <button type="button" className="active">Top 10</button>
                                                            </div>
                                                            <div className="col-md-3 pad-0">
                                                                <button type="button">Bottom 5</button>
                                                            </div>
                                                            <div className="col-md-3 pad-0">
                                                                <button type="button">Bottom 10</button>
                                                            </div>
                                                        </div>
                                                    </div> </div> :
                                                    <div>
                                                        <div className="col-md-5 divPad borderRight alignMiddle height100">
                                                            <div className="col-md-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 20 20">
                                                                    <path fill="#c5c5c5" fill-rule="nonzero" d="M19.149 2.549C18.913 1.072 17.697 0 16.259 0H3.75C2.312 0 1.097 1.072.86 2.548L0 7.954c0 .987.462 1.858 1.163 2.415v6.58C1.163 18.63 2.478 20 4.093 20h11.821c1.616 0 2.93-1.369 2.93-3.051V10.36c.701-.567 1.16-1.46 1.156-2.505l-.851-5.307zm-7.346 16.273H8.206v-3.744h3.597v3.744zm5.911-1.872c0 1.032-.806 1.872-1.798 1.872h-2.98v-4.334a.578.578 0 0 0-.567-.59H7.64a.579.579 0 0 0-.566.59v4.334h-2.98c-.993 0-1.799-.84-1.799-1.872v-6.025c.204.047.414.076.63.076.968 0 1.828-.491 2.36-1.247C5.819 10.51 6.679 11 7.646 11c.968 0 1.827-.491 2.36-1.247.532.756 1.392 1.247 2.36 1.247.967 0 1.827-.491 2.359-1.247.532.756 1.392 1.247 2.36 1.247.217 0 .426-.029.63-.076v6.025zm.354-7.438c-.01.004-.019.01-.028.016a1.731 1.731 0 0 1-.956.293c-.989 0-1.793-.838-1.793-1.867a.578.578 0 0 0-.566-.59.579.579 0 0 0-.566.59c0 1.03-.806 1.867-1.794 1.867s-1.793-.838-1.793-1.867a.578.578 0 0 0-.566-.59.579.579 0 0 0-.567.59c0 1.03-.805 1.867-1.793 1.867-.988 0-1.793-.838-1.793-1.867a.578.578 0 0 0-.567-.59.579.579 0 0 0-.566.59c0 1.03-.805 1.867-1.793 1.867-.339 0-.653-.104-.924-.27-.026-.015-.048-.031-.076-.042-.48-.327-.797-.873-.802-1.457l.852-5.308c.145-.906.89-1.564 1.774-1.564h12.508c.883 0 1.629.658 1.773 1.564l.845 5.21c0 .651-.323 1.224-.81 1.558z" />
                                                                </svg>
                                                            </div>
                                                            <div className="col-md-9 m-lft-10">
                                                                <h3>Store Name</h3>
                                                                <h2 className="m-top-10 m-bot-0">MG SP-PRNA</h2>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-7 backDashboard">
                                                            <button className="m-rgt-15 alignMiddle">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="31" height="31" viewBox="0 0 31 31">
                                                                    <g fill="none" fill-rule="evenodd">
                                                                        <path fill="#6D6DC9" d="M19.5 20.973L14.555 16l4.945-4.972L17.978 9.5 11.5 16l6.478 6.5 1.522-1.527" />
                                                                    </g>
                                                                </svg>
                                                                <label className="displayPointer m0">Back to Main Dashboard</label>
                                                            </button>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        <div className="col-md-5 padRightNone">
                                            <div className="right">
                                                <div className="col-md-12 pad-0">
                                                    <label>Default Sell Thru <span>Barcode</span></label>
                                                    <div className="col-md-12 m-top-10 pad-0">
                                                        <div className="posRelative displayInline">
                                                            <button type="button" className="chooseSell" onClick={() => setSellDrop(!sellDrop)}>Choose sell Thru Type</button>
                                                            {sellDrop ? <div className="chooseSellDrop">
                                                                <ul className="pad-0">
                                                                    <li >Barcode</li>
                                                                    <li className="active">Size</li>
                                                                    <li>Pattern</li>
                                                                    <li>Fabric </li>
                                                                    <li>Brand</li>
                                                                    <li>Assortment</li>
                                                                    <li>Design</li>
                                                                </ul>
                                                            </div> : null}
                                                        </div>
                                                        <button type="button" className="chooseStore">
                                                            <img src={storeIcon} className="m-rgt-10 imgHeight" />Choose Different Store
                                                            <svg className="m-lft-15" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15 15">
                                                                <g fill="#535B78" fillRule="nonzero">
                                                                    <path d="M6.429 1.286a5.143 5.143 0 1 1-5.143 5.143 5.154 5.154 0 0 1 5.143-5.143zm0-1.286a6.429 6.429 0 1 0 0 12.857A6.429 6.429 0 0 0 6.429 0z" />
                                                                    <path d="M11.346 9.793l-1.51 1.51 3.021 3.033a1.071 1.071 0 0 0 1.522-1.479l-3.033-3.032v-.032z" />
                                                                </g>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row barcodeWiseGraph displayBlock margin-right0 m-lft-0 m-top-30">
                                        <h4>Barcode wise Graph</h4>
                                        {storeSelected ? <div className="col-md-12 pad-0 m-top-15">
                                            <div className="col-md-4 pad-lft-0">
                                                <BarChart width={350} height={220} data={data} margin={{ top: 0, right: 0, left: -10, bottom: 0 }}>
                                                    <CartesianGrid strokeDasharray="2 2" />
                                                    <XAxis data={false} tick={false} axisLine={false}>
                                                        <Label value='Barcode' offset={0} position="insideLeft" fill="#535b78" style={{ fontWeight: "600", fontSize: "12px" }} />
                                                    </XAxis>
                                                    <YAxis axisLine={false} label={{ value: "Sell Thru (%)", angle: -90, position: 'insideLeft', dy: 35, dx: 10, fill: "#535b78", fontWeight: "600", fontSize: "12px" }} />
                                                    <Tooltip cursor={false} content={<CustomTooltip />} />
                                                    {/* <Legend /> */}
                                                    <Bar dataKey="pv" barSize={28} background={{ fill: "#f3f3f3" }} fill="#8884d8" />
                                                </BarChart>
                                                <div className="GraphBottomData textCenter">
                                                    <label>Store Name</label>
                                                    <h3>MG SP-PRNA 2</h3>
                                                </div>
                                            </div>
                                            <div className="col-md-4">
                                                <BarChart width={350} height={220} data={data} margin={{ top: 0, right: 0, left: -10, bottom: 0 }}>
                                                    <CartesianGrid strokeDasharray="2 2" />
                                                    <XAxis data={false} tick={false} axisLine={false}>
                                                        <Label value='Barcode' offset={0} position="insideLeft" fill="#535b78" style={{ fontWeight: "600", fontSize: "12px" }} />
                                                    </XAxis>
                                                    <YAxis axisLine={false} label={{ value: "Sell Thru (%)", angle: -90, position: 'insideLeft', dy: 35, dx: 10, fill: "#535b78", fontWeight: "600", fontSize: "12px" }} />
                                                    <Tooltip cursor={false} content={<CustomTooltip />} />
                                                    {/* <Legend /> */}
                                                    <Bar dataKey="pv" barSize={28} background={{ fill: "#f3f3f3" }} fill="#8884d8" />
                                                </BarChart>
                                                <div className="GraphBottomData textCenter">
                                                    <label>Store Name</label>
                                                    <h3>MG SP-PRNA 2</h3>
                                                </div>
                                            </div>
                                            <div className="col-md-4">
                                                <BarChart width={350} height={220} data={data} margin={{ top: 0, right: 0, left: -10, bottom: 0 }}>
                                                    <CartesianGrid strokeDasharray="2 2" />
                                                    <XAxis data={false} tick={false} axisLine={false}>
                                                        <Label value='Barcode' offset={0} position="insideLeft" fill="#535b78" style={{ fontWeight: "600", fontSize: "12px" }} />
                                                    </XAxis>
                                                    <YAxis axisLine={false} label={{ value: "Sell Thru (%)", angle: -90, position: 'insideLeft', dy: 35, dx: 10, fill: "#535b78", fontWeight: "600", fontSize: "12px" }} />
                                                    <Tooltip cursor={false} content={<CustomTooltip />} />
                                                    {/* <Legend /> */}
                                                    <Bar dataKey="pv" barSize={28} background={{ fill: "#f3f3f3" }} fill="#8884d8" />
                                                </BarChart>
                                                <div className="GraphBottomData textCenter">
                                                    <label>Store Name</label>
                                                    <h3>MG SP-PRNA 2</h3>
                                                </div>
                                            </div>
                                        </div> :
                                            <div className="col-md-12 m-top-15">
                                                <ResponsiveContainer width='100%' height="30%">
                                                    <BarChart width={1000} connectNulls={true} height={220} data={data} margin={{ top: 0, right: 0, left: -10, bottom: 0 }}>
                                                        <CartesianGrid strokeDasharray="2 2" />
                                                        <XAxis data={false} interval={0} padding={{ left: 30, right: 30 }} axisLine={false}>
                                                            <Label value='Barcode' offset={0} position="insideLeft" fill="#535b78" style={{ fontWeight: "600", fontSize: "12px" }} />
                                                        </XAxis>
                                                        <YAxis axisLine={false} label={{ value: "Sell Thru (%)", angle: -90, position: 'insideLeft', dy: 35, dx: 10, fill: "#535b78", fontWeight: "600", fontSize: "12px" }} />
                                                        <Tooltip cursor={false} content={<CustomTooltip />} />
                                                        <Bar dataKey="pv" minPointSize={5} barSize={28} background={{ fill: "#f3f3f3" }} fill="#8884d8" />
                                                    </BarChart>
                                                </ResponsiveContainer>

                                            </div>
                                        }
                                    </div>
                                    <div className="col-md-12">
                                        <div className="col-md-8"></div>
                                        <div className="col-md-4 pad-lft-0 barcodeSearch displayBlock m-top-20 text-right padRightNone">
                                            <input type="search" id="search" className="search-box width_85" placeholder="Search..." autoComplete="off" />
                                        </div>

                                        <div className="promotionalEvents m-top-20 displayInline width100">
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 tableGeneric bordere3e7f3">
                                                <div className="Zui-wrapper">
                                                    <div className="scrollableOrgansation zui-scrollerHistory table-scroll scrollableTableFixed heightAuto sellThruTable">
                                                        <table className="table scrollTable zui-table manageUserTable border-bot-table ">
                                                            <thead className="tableHeadBg">
                                                                <tr>
                                                                    <th className="fixed-side-user fixed-side1 bgFixed">
                                                                        <label className="width65 pad-lft-5">Plot Graph</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>Barcode Name</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>Sell Thru</label>
                                                                    </th>
                                                                    <th className="positionRelative">
                                                                        <label>Qty Sold ( in last 90 days )</label>
                                                                    </th><th className="positionRelative">
                                                                        <label>Total Closing Stock</label>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td className="fixed-side1 displayFlex">
                                                                        <ul className="pad-0 posRelative squareCheckBox">
                                                                            <li><label className="checkBoxLabel0 displayPointer pad-0 posInherit">
                                                                                <input type="checkBox" /> <span className="checkmark1"></span></label>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                    <td className="curActive"><label>MSP0001</label></td>
                                                                    <td className="curActive"><label>25%</label></td>
                                                                    <td className="curActive"><label>6</label></td>
                                                                    <td className="curActive"><label>40T</label></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div className="pagerDiv">
                                            <ul className="list-inline pagination paginationUpdate displayFlex">
                                                <li >
                                                    <button className="PageFirstBtn pointerNone" type="button"  >
                                                        First
                                                     </button>
                                                </li>
                                                <li >
                                                    <button className="PageFirstBtn" type="button" id="prev">
                                                        Prev
                                                    </button>
                                                </li>
                                                <li>
                                                    <button className="PageFirstBtn" type="button">
                                                        <span></span>
                                                    </button>
                                                </li>
                                                <li>
                                                    <button className="PageFirstBtn" type="button">
                                                        <span>Next</span>
                                                    </button>
                                                </li>
                                                <li >
                                                    <button className="PageFirstBtn borderNone" type="button" id="next">
                                                        last
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            {/* <BarcodeStoreModal /> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}