import React from 'react';
import { Label, LineChart, Line, BarChart, ResponsiveContainer, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import upArrow from '../../assets/up-blue.svg';
import downArrow from '../../assets/down-arrow.svg';

const data = [
    {
        name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
    },
    {
        name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
    },
    {
        name: 'Page C', uv: 2000, pv: 4000, amt: 2290,
    },
    {
        name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
    },
    {
        name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
    },

];

export const SaleDetailsBasic = (props) => {


    return <div className="basicInfoStore">
        <div className="row m-top-17">
            <div className="col-md-6">
                <h5>Site Code</h5>
                <label>{props.basicStoreData.siteCode}</label>
            </div>
            <div className="col-md-6">
                <h5>Store Rank</h5>
                <label>{props.basicStoreData.storeRank}</label>
            </div>
        </div>
        <div className="row m-top-17">
            <div className="col-md-6">
                <h5>Best Performing Month</h5>
                <label>{props.basicStoreData.bestPermofmingMonth}</label>
            </div>
            <div className="col-md-6">
                <h5>Worst Performing Month</h5>
                <label>{props.basicStoreData.worstPerformingMonth}</label>
            </div>
        </div>
        <div className="row m-top-17">
            <div className="col-md-6">
                <h5>State</h5>
                <label>{props.basicStoreData.state}</label>
            </div>
            <div className="col-md-6">
                <h5>City</h5>
                <label>{props.basicStoreData.city}</label>
            </div>
        </div>
        <div className="row m-top-17">
            <div className="col-md-6">
                <h5>Total Area</h5>
                <label>{props.basicStoreData.totalArea}</label>
            </div>
            <div className="col-md-6">
                <h5>Number of Section</h5>
                <label>{props.basicStoreData.numberOfSection}</label>
            </div>
        </div>
        <div className="row m-top-17">
            <div className="col-md-6">
                <h5>Articles sales trend (in number)</h5>
                <label>{props.basicStoreData.numberOfArticles}</label>
            </div>
            <div className="col-md-6">
                <h5>Items sales trend (in number)</h5>
                <label>{props.basicStoreData.numberOfItems}</label>
            </div>
        </div>
    </div>
}


export const TopArticle = (props) => {
    return <div className="topArticleStore">
        <div className="heading">
            <h3>Top Articles</h3>
            <div className="topBottomSwitch">
                <div className="col-md-6 topFivePerformer displayInline pad-0" onClick={(e) => props.activeTopArticle('top')}><label className={props.topArticleType == 'top' ? "active" : ""}>Top 5 performer</label></div>
                <div className="col-md-6 botFivePerformer displayInline pad-0" onClick={(e) => props.activeTopArticle('bottom')}><label className={props.topArticleType == 'bottom' ? "active" : ""}>Bottom 5 performer</label></div>
            </div>
        </div>
        <div className="barcodeSell">
            <table>
                <thead>
                    <tr>
                        <th><label>Article</label></th>
                        <th><label>Last Year</label></th>
                        <th><label>Last 3M</label></th>
                        <th><label>Current Month</label></th>
                    </tr>
                </thead>
                <tbody>
                    {props.topArticleData.length != 0 ? props.topArticleType == 'top' ? Object.keys(props.topArticleData.top).map((topArticle, keyy) =>
                        props.topArticleData.top[topArticle].map((data, key) =>
                            <tr key={key}>
                                <td><label>{topArticle}</label></td>
                                <td><label>{data.lastYearQty}{data.lastYearQtyPercentage == 'HIGH' ? <img src={upArrow} /> : <img src={downArrow} />}</label></td>
                                <td><label>{data.lastThreeMonthQty} {data.lastThreeMonthQtyPercentage == 'HIGH' ? <img src={upArrow} /> : <img src={downArrow} />}</label></td>
                                <td><label>{data.currentMonthQty}{data.currentMonthQtyPercentage == 'HIGH' ? <img src={upArrow} /> : <img src={downArrow} />}</label></td>
                            </tr>
                        )) : Object.keys(props.topArticleData.bottom).map((bottomArticle, keyyy) =>
                            props.topArticleData.bottom[bottomArticle].map((data, key) =>
                                <tr key={key}>
                                    <td><label>{bottomArticle}</label></td>
                                    <td><label>{data.lastYearQty}{data.lastYearQtyPercentage == 'HIGH' ? <img src={upArrow} /> : <img src={downArrow} />}</label></td>
                                    <td><label>{data.lastThreeMonthQty} {data.lastThreeMonthQtyPercentage == 'HIGH' ? <img src={upArrow} /> : <img src={downArrow} />}</label></td>
                                    <td><label>{data.currentMonthQty}{data.currentMonthQtyPercentage == 'HIGH' ? <img src={upArrow} /> : <img src={downArrow} />}</label></td>
                                </tr>
                            )) : <tr className="noDaTaFoundStore textCenter"><td colSpan="4">No Data Found</td></tr>}
                </tbody>
            </table>
            {props.topArticleData.length != 0 ? <div className="infNote m-top-15">
                {/* <label><span>Note -</span> * The above figure is in quantity sold. As on date 11th Feb, 2019</label> */}
            </div> : null}
        </div>
    </div>
}

export const ProductInDemand = (props) => {
    return <div className="topArticleStore productDemandMain">

        {/* <div className="heading">
            <h3>Top 5 Stock</h3>
            <div className="topBottomSwitch">
                <div className="col-md-6 topFivePerformer displayInline pad-0"><label className="active">Top 5 performer</label></div>
                <div className="col-md-6 botFivePerformer displayInline pad-0"><label>Bottom 5 performer</label></div>
            </div>
        </div> */}
        <div className="noDataFoundStore alignMiddle">No Data Available</div>
        {/* <div className="productGraph graphLabel">
            <BarChart width={400} height={240} data={data} margin={{ top: 20, right: 30, left: 0, bottom: 5 }}>
              }
                <XAxis dataKey="name" >
                    <Label value="Articles" offset={0} position="insideBottom" />
                </XAxis>
                <YAxis label={{ value: 'Requirements', angle: -90, position: 'insideStart', dx: -15 }} />

                <Tooltip cursor={{ fill: 'transparent' }} />
                <defs>

                    <linearGradient id="productDemandBar" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="19%" stopColor="#f761a1" stopOpacity={1} />
                        <stop offset="95%" stopColor="#8c1bab" stopOpacity={0.8} />
                    </linearGradient>
                </defs>
           
                <Bar dataKey="pv" fill="url(#productDemandBar)" barSize={20} />
            </BarChart>
        </div>
        <div className="infNote m-top-15">
            <label><span>Note</span> - * The above figure is in quantity sold. As on date 11th Feb, 2019</label>
        </div> */}
    </div>
}

export const SaleTrend = (props) => {
    return <div className="topArticleStore saleTrendDiv">

        <div className="heading">
            <h3>Sales Trend</h3>
            <div className="topBottomSwitch">
                <div className="col-md-6 topFivePerformer displayInline pad-0" onClick={(e) => props.activeTab('Unit')}><label className={props.salesTrendType == "Unit" ? "active" : ""}>Unit Sales</label></div>
                <div className="col-md-6 botFivePerformer displayInline pad-0" onClick={(e) => props.activeTab('Sales')}><label className={props.salesTrendType == "Sales" ? "active" : ""}>Sales Value</label></div>
            </div>
        </div>
        {props.salesTrend.length != 0 ?
            <div className="productGraph graphLabel toolTipStyle" onMouseOver = {() => props.toolTip("productGraph" , "Total units sold" , "Total sales")}>
                <LineChart width={400} height={240} data={props.salesTrend}
                    margin={{ top: 20, right: 30, left: 0, bottom: 5 }}>
                    <XAxis dataKey="articleCode">
                        <Label value='Article Code' offset={0} position="insideBottom" />
                    </XAxis>
                    <YAxis label={{ value: props.salesTrendType == "Sales" ? "Sales Value" : "Unit Sales", position: 'insideStart', dy: -105, dx: 30 }} />
                    <Tooltip cursor={false} />

                    {props.salesTrendType == "Sales" ? <Line type="monotone" dataKey="lastMonthSV" strokeWidth={2} stroke={props.salesTrendType == "Sales" ? "#bd10e0" : "#7ed321"} activeDot={{ r: 5 }} />
                        : <Line type="monotone" dataKey="lastMonthQTY" strokeWidth={2} stroke={props.salesTrendType == "Sales" ? "#bd10e0" : "#7ed321"} activeDot={{ r: 5 }} />}
                </LineChart>
            </div> : <div className="noDataFoundStore alignMiddle">NO DATA FOUND</div>}
        {props.salesTrend.length != 0 ? <div className="infNote m-top-15">
        {props.salesTrendType == "Unit" ? <label><span>Note</span> - * The above figure represents quantity sold, as on date {props.prevMonth()}</label> : <label><span>Note</span> - * The above figure represents sales value, as on date {props.prevMonth()}</label>}
        </div> : null}
    </div>
}

export const StockInHand = (props) => {
    return <div className="topArticleStore productDemandMain">

        {props.stockHandCoverage.length != 0 ? <div className="heading">
            <h3>Top 5 Stock</h3>
        </div> : ""}
        {props.stockHandCoverage.length != 0 ?
            <div className="productGraph graphLabel">
                <BarChart width={400} height={240} data={props.stockHandCoverage} margin={{ top: 20, right: 30, left: 0, bottom: 5 }}>
                    {/* <CartesianGrid strokeDasharray="3 3" /> */}
                    <XAxis dataKey="articleCode" >
                        <Label value="Articles" offset={0} position="insideBottom" />
                    </XAxis>
                    <YAxis label={{ value: 'Days of Coverage', angle: -90, position: 'insideStart', dx: -15 }} />
                    <Tooltip cursor={{ fill: 'transparent' }} />
                    <defs>

                        <linearGradient id="productDemandBar" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="19%" stopColor="#f761a1" stopOpacity={1} />
                            <stop offset="95%" stopColor="#8c1bab" stopOpacity={0.8} />
                        </linearGradient>
                    </defs>
                    {/* <Legend /> */}
                    <Bar dataKey="dayCoverage" fill="url(#productDemandBar)" barSize={20} />
                </BarChart>
            </div> : <div className="noDaTaFoundStore alignMiddle">There isn't any data for the selected period or filter</div>}
        {props.stockHandCoverage.length != 0 ? <div className="infNote m-top-15">
            <label><span> Note </span>- * The above figure is in quantity sold. As on date 11th Feb, 2019</label>
        </div> : null}
    </div>
}