import React from 'react';
import ToastLoader from '../../loaders/toastLoader';
class CreateNewPlanModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toastLoader: false,
            toastMsg: "",
            tableData: [],
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            filter: false,
            dataFilter: false,
            type: 1,
            no: 1,
            search: "",
            loader: true,
            status: "",
            itemCount: "",
            createdOn: "",
            alert: false,
            code: "",
            errorMessage: "",
            errorCode: "",
            newPlan:true,
            percentage:false,   
            updateOption:false,
            planName:"",
            planNameerr:false,
            percent:"",
            percenterr:false,
            isUfAsd:false,
            isUfDfd:false,
            dataerr:false,
            activePlan:this.props.activePlan,
            activePlanTrue:[],
            makeCopy:this.props.makeCopy
        };
    }

    planName(){
    if (
        this.state.planName == "")
       {
        this.setState({
            planNameerr: true
        });
      } else {
        this.setState({
            planNameerr: false
        });
      }  
}


percent(){
    if (
        this.state.percent == "")
       {
        this.setState({
            percenterr: true
        });
      } else {
        this.setState({
            percenterr: false
        });
      }  
}
    submitNewPlan(){
        this.planName();
        const t= this
        setTimeout(function(){
        if(!t.state.planNameerr){
        t.setState({
            updateOption:true, 
            newPlan:false,
        })
    }},10)
    }
    submitUpdateOption(){
        this.data();
        const t= this
        setTimeout(function(){
        if(!t.state.dataerr){
                t.setState({
            updateOption:false, 
            percentage:true,
        })  }},10)
    }
    submitPercentage(){
        this.percent();
        const t= this
        setTimeout(function(){
            if(!t.state.percenterr){
  let payload={

    planName:t.state.planName,
    planType:"OTB-RETAIL",
    isUfAsd:t.state.isUfAsd,
    isUfDfd:t.state.isUfDfd,
    increasePercentageAsd:t.state.isUfAsd?t.state.percent:"",
    increasePercentageDfd:t.state.isUfDfd?t.state.percent:"",
    activeDataOption:t.state.isUfDfd?"UF_DFD":"UF_ASD",
    makeCopy:t.props.makeCopy!=""?"TRUE":"FALSE",
    createdCopy:t.props.makeCopy==""?"TRUE":"FALSE",
    isUserOTBPlan:"TRUE"
  }
  t.props.createPlanRequest(payload)
  t.props.closePlanModal()
         }

        },10)
    }

   data(){
       if (this.state.isUfAsd==false && this.state.isUfDfd==false){
        this.setState({
            dataerr: true
        });
      } else {
        this.setState({
            dataerr: false
        }); 
       }
   }


    handleChange(e){
        if (e.target.id == "planName") {
            this.setState(
           {
             planName: e.target.value
           },
           () => {
             this.planName();
           }
         );
       }else if (e.target.id == "percent") {
           if(e.target.validity.valid){
               if (e.target.value>100){
                   e.target.value=""
            this.setState({     
toastLoader:true,
toastMsg:"Precentage can not be greater than 100"
                   })

                   const t=this
                   setTimeout(function(){
                    t.setState({
                        toastLoader:false
                    })
                   },2000)

               }else{
        this.setState(
       {
         percent: e.target.value
       },
       () => {
         this.percent();
       }
     );
    }
}
   }else if (e.target.id == "lastYearData") {
    this.setState(
   {
  
    isUfDfd:false,
    isUfAsd: true
   },
   () => {
     this.data();
   }

 );
}else if (e.target.id == "forecastedData") {
    this.setState(
   {
    isUfAsd: false,
    isUfDfd:true
},
() => {
  this.data();
}
 );
}
    }

componentWillMount(){
  var activePlan=this.props.activePlan
  var activePlanTrue=[]
  if(activePlan!=null){
  for(var i=0 ;i<activePlan.length;i++){
    if(activePlan[i].status=="TRUE"){
        activePlanTrue.push(activePlan[i])
        this.setState({
            activePlanTrue:activePlanTrue
        })
    }
}   
}
if(this.props.makeCopy!=""){
 if(activePlanTrue.length!=0){
  var str = activePlanTrue[0].planName
  
  var flag = str.toString().includes("-COPY")
  var n = Number(str.split("-COPY")[1]).toString();
  var finalstr= str.split("-COPY")[0]
  if(flag){

    n =Number(n)+1
        finalstr = finalstr+"-COPY" + n
  }else{
    finalstr= finalstr+"-COPY"
  }
  this.setState({
    activePlanTrue:activePlanTrue
})
 }
}
const t= this
setTimeout(function()
{ 
let activePlanTrue = t.state.activePlanTrue
if(t.props.makeCopy!=""){
 if(activePlanTrue.length!=0){

t.setState({
    planName : finalstr,

})
if(activePlanTrue[0].isUfAsd=="YES"){
    t.setState({
        isUfAsd : true,
        isUfDfd:false
    
    })
}else{
    t.setState({
        isUfAsd : false,
        isUfDfd:true
    
    })
}}
}
},10)


}


    render() {

  const {planName,planNameerr,percent,percenterr,dataerr}=this.state
        return (
            <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain pad-0">
                    <div className="col-md-12 pad-0">   
                        <div className="col-md-9 modalLeft">
                            
                            <h4>{this.props.makeCopy=="" ||this.props.makeCopy==undefined?"Create new plan" :this.props.makeCopy}</h4>
                            <div className="changeData m-top-30">
                            {/* First */}
                            {this.state.newPlan? this.props.makeCopy=="" ||this.props.makeCopy==undefined? <input type="text" placeholder="Enter Plan Name" id="planName" value={planName} onChange={(e) => this.handleChange(e)}/>:
                             <input type="text" placeholder="Enter Plan Name" id="planName" className="btnDisabled" value={planName} disabled/>:null}
                            {this.state.newPlan?  planNameerr ? (
                          <span className="error">
                            Enter plan name
                          </span>
                        ) : null:null}
                            {/* first End */}
                            {/* Second */}
                       {this.state.updateOption? <label>Chooose Update option</label>:null}
                       {this.state.updateOption?  this.props.makeCopy==""||this.props.makeCopy==undefined?  <button type="button" value="Update From Last year Data" className={this.state.isUfAsd?"changeDataBlue":"disaledbtn"} id="lastYearData" onClick={e => this.handleChange(e)}>Update From Last year Data</button>:
                       <button type="button" value="Update From Last year Data" className={this.state.isUfAsd?"changeDataBlue":"disabledbtn"} id="lastYearData" >Update From Last year Data</button>:null}
                            {this.state.updateOption? this.props.makeCopy=="" ||this.props.makeCopy==undefined?   <button type="button" value="Update from Sales forecasted Data" className={this.state.isUfDfd?"changeDataBlue":""} id="forecastedData" onClick={(e) => this.handleChange(e)}>Update from Sales forecasted Data</button>:
                            <button type="button" value="Update from Sales forecasted Data" className={this.state.isUfDfd?"changeDataBlue":"disabledbtn"} id="forecastedData" >Update from Sales forecasted Data</button>:null}
                            {this.state.updateOption?  dataerr ? (
                          <span className="error">
                    Select one option
                          </span>
                        ) : null:null}
                            {/* Second End */}
                            {/* Third */}
                          {this.state.percentage? <label>The percentage increase from the Past year (e.g. 30)</label>:null}
                        {this.state.percentage?  <input type="text" value={percent} pattern="[0-9]+([\.][0-9]{0,2})?" maxLength="5" onChange={(e) => this.handleChange(e)}  id="percent"/> :null}
                        {this.state.percentage?  percenterr ? (
                          <span className="error">
                            Enter  percentage
                          </span>
                        ) : null:null}
                            {/* Third End */}
                            </div>
                            {/* <div className="emptyDiv"></div> */}
                            {/* <span className="glyphicon glyphicon-chevron-right"></span> */}
                            <div className="col-md-12 modalFooter pad-lft-0">
                            {this.state.newPlan?  <button className="nxtBtn" type="button" onClick={(e)=>this.submitNewPlan(e)}>Next <span><i className="fa fa-chevron-right" aria-hidden="true"></i></span></button>:null}
                            {this.state.updateOption?  <button className="nxtBtn" type="button" onClick={(e)=>this.submitUpdateOption(e)}>Next <span><i className="fa fa-chevron-right" aria-hidden="true"></i></span></button>:null}
                            {this.state.percentage?  <button className="nxtBtn" type="button" onClick={(e)=>this.submitPercentage(e)}>Next <span><i className="fa fa-chevron-right" aria-hidden="true"></i></span></button>:null}
                              
                               
                               
                                <button className="clear_button_vendor" type="button" onClick={(e) => this.props.closePlanModal(e)}>Cancel</button>
                            </div>
                        </div>
                        <div className="col-md-3 processState">
                            <ul className="pad-0 m-top-15">
                                
                                <li className={this.state.newPlan ||this.state.updateOption ||this.state.percentage?"active":""}><div className={this.state.newPlan ||this.state.updateOption ||this.state.percentage?"processBlue":"processGrey"}></div><label>Plan Name</label></li>
                                <li className={this.state.updateOption ||this.state.percentage?"active":""}><div className={this.state.updateOption ||this.state.percentage?"processBlue":"processGrey"}></div><label>Upload Data</label></li>
                                <li className={this.state.percentage?"active":""}><div className={this.state.percentage?"processBlue":"processGrey"}></div><label>Update Plan</label></li>
                            </ul>

                        </div>
                    </div>  
                           
                    
                    
                    

                
                    
                </div>
            
      
        </div>
        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
        </div>

            )
        }
    }
    
export default CreateNewPlanModal;