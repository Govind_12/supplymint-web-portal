import React from 'react';

class ArsFilterModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filterData : this.props.filterData,
            max: this.props.max !== undefined ? this.props.max : ((this.props.filterData.length % 10) == 0 ? Math.floor(this.props.filterData.length / 10) : (Math.floor(this.props.filterData.length / 10) + 1)),
            current: this.props.current !== undefined ? this.props.current : this.props.filterData.length ? 1 : 0,
        }
    }

    page =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.current == 0 || this.state.current == 1) {
            } else {
                if( this.props.current !== undefined){
                    let payload = {};
                    if( this.props.flag !== "openStoreCodeFilter" ){
                        payload ={
                            entity: "item",
                            key: this.props.flag == "openDivisionFilter" ? "hl1_name" : this.props.flag == "openSectionFilter" ? "hl2_name" : this.props.flag == "openDepartmentFilter" ? "hl3_name" : 
                                this.props.flag == "openArticleFilter" && "hl4_code",
                            search: this.props.search,
                            pageNo: (this.state.current - 1)
                        }
                    }else{
                        payload ={
                            entity: "site",
                            key: "name1", 
                            code: "site_code",
                            search: this.props.search,
                            pageNo: (this.state.current - 1)
                        }
                    }
                    this.props.resetFilterData(payload);
                }else{
                    this.setState({
                        current: (this.state.current - 1),
                        filterData: this.props.filterData
                    },()=>{
                        this.state.filterData && this.state.filterData.filter((data, key) => key >= (this.state.current*10)-10 && key < (this.state.current*10))
                    })
                } 
            }
        } else if (e.target.id == "next") {
            if(this.state.current == this.state.max){

            }else{
                if( this.props.current !== undefined){
                    let payload = {};
                    if( this.props.flag !== "openStoreCodeFilter" ){
                        payload ={
                            entity: "item",
                            key: this.props.flag == "openDivisionFilter" ? "hl1_name" : this.props.flag == "openSectionFilter" ? "hl2_name" : this.props.flag == "openDepartmentFilter" ? "hl3_name" : 
                                this.props.flag == "openArticleFilter" && "hl4_code",
                            search: this.props.search,
                            pageNo: (this.state.current + 1)
                        }
                    }else{
                        payload ={
                            entity: "site",
                            key: "name1", 
                            code: "site_code",
                            search: this.props.search,
                            pageNo: (this.state.current + 1)
                        }
                    }
                    this.props.resetFilterData(payload);
                }else{
                    this.setState({
                        current: (this.state.current + 1),
                        filterData: this.props.filterData
                    },()=>{
                        this.state.filterData && this.state.filterData.filter((data, key) => key >= (this.state.current*10)-10 && key < (this.state.current*10))
                    })
                }  
            }
        }
    }

    render () {
        const{filterData}= this.state
        return (
            <React.Fragment>
            <div className="backdrop-transparent zIndex2"></div>
            <div className="dropdown-menu-city1 gen-width-auto">
                <ul className="dropdown-menu-city-item">
                    {this.props.current == undefined && (filterData.length > 0 ? filterData.map( (data,key) => key >= (this.state.current*10)-10 && key < (this.state.current*10) && <li key={key} value={data}> 
                        <label className="checkBoxLabel0">
                            <input type="checkBox" name="selectEach" checked={this.props.checkedData[this.props.filterType].some((item) => item == data)} onChange={(e) => this.props.handleChange(e, this.props.filterType)}/>
                            <span className="checkmark1"></span>
                            {data}
                        </label>
                    </li>) :
                    <li>No Data</li>)}
                    {this.props.current !== undefined && (filterData.length > 0 ? filterData.map( (data,key) => <li key={key} value={data.slName == undefined ? data.code !== undefined ? data.code : data : data.slName} name={data.code !== undefined ? data.code +" - "+ data.name1 : ""}> 
                        <label className="checkBoxLabel0">
                            <input type="checkBox" name="selectEach" checked={this.props.checkedData[this.props.filterType].some((item) => item == (data.slName == undefined ? data.code !== undefined ? data.code : data : data.slName))} onChange={(e) => this.props.handleChange(e, this.props.filterType)}/>
                            <span className="checkmark1"></span>
                            {data.slName == undefined ? data.code !== undefined ? data.code +" - "+ data.name1 : data : data.slName}
                        </label>
                    </li>) :
                    <li>No Data</li>)}
                </ul>
                <div className="gen-dropdown-pagination">
                        <div className="page-close">
                            {/* <button className="btn-close" type="button" id="btn-close" onClick={this.props.onCloseArsFilter}>Close</button> */}
                        </div>
                        <div className="page-next-prew-btn">
                            <button className="pnpb-prev" type="button" id="prev" onClick={this.page}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="prev">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.max}</button>
                            <button className="pnpb-next" type="button" id="next" onClick={this.page}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="next">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>
                        </div>
                    </div>
            </div>
            </React.Fragment>
        )
    }
}
export default ArsFilterModal;