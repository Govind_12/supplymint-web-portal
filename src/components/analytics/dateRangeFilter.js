import React from 'react';

class DateRangeFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            days: this.props.days === null || this.props.days === undefined ? 0 : parseInt(this.props.days),
            change: false
        }
    }

    handleChange = (days) => {
        this.setState({
            days: days,
            change: days === this.props.days ? false : true
        });
    }

    render() {
        console.log(this.props.days, this.state.days);
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content date-range-filter">
                    <div className="drf-head">
                        <h3>Date Range Filter</h3>
                        <button type="button" className="bum-close-btn" onClick={this.props.CloseDays}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="drf-body">
                        <div className="drfb-inner">
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 7} onChange={() => this.handleChange(7)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 7 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 15} onChange={() => this.handleChange(15)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 15 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 30} onChange={() => this.handleChange(30)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 30 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 45} onChange={() => this.handleChange(45)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 45 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 60} onChange={() => this.handleChange(60)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 60 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 90} onChange={() => this.handleChange(90)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 90 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 180} onChange={() => this.handleChange(180)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 180 Days</span>
                            </label>
                            <label className="drfb-radio">
                                <input type="radio" name="daterange" checked={this.state.days === 365} onChange={() => this.handleChange(365)} />
                                <span className="drfb-check"></span>
                                <span className="drfb-text">Last 365 Days</span>
                            </label>
                        </div>
                    </div>
                    <div className="drf-footer m-top-100">
                        <button type="button" className={this.state.change ? "drff-done" : "drff-done btnDisabled"} disabled={this.state.change ? "" : "disabled"} onClick={() => this.props.done(this.state.days)} >Done</button>
                        <button type="button" className="drff-close" onClick={this.props.CloseDays}>Close</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default DateRangeFilter;