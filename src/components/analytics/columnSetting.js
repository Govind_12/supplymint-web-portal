import React from 'react';
import { Link } from 'react-router-dom';
import successIcon from '../../assets/greencheck.png'
import closeSearch from "../../assets/close-recently.svg"
import _ from 'lodash';
import cross from '../../assets/group-4.svg'
class ColumnSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ColumnSetting: this.props.ColumnSetting,
            search:""
        }
    }

    openColumnSetting(data) {
        this.props.openColumnSetting(data)
    }

    closeColumns(data) {
        this.props.closeColumn(data)
    }
    handleChange(e){
        this.setState({
            search:e.target.value
        })
    }
    clearSearch(){
        this.setState({
            search:""
        })
    }
    render() {
        const {search} = this.state
        var result = _.filter(this.props.fixedHeader, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        });
        // console.log(result, "result");
        return (
            <div>
                {this.props.ColumnSetting ? <div className="columnFilterGeneric" >
                    <span className="glyphicon glyphicon-menu-right" onClick={(e) => this.openColumnSetting("false")}></span>
                    <div className="columnSetting" onClick={(e) => this.openColumnSetting("false")}>Columns Setting</div>
                    <div className="columnFilterDropDown">
                        <div className="col-md-12 pad-0 filterHeader">
                            <div className="col-md-7 pad-0">
                                <input type="search" className="width100 inputSearch" value={this.state.search} onChange={(e)=>this.handleChange(e)} placeholder="Search Columns…" />
                               {this.state.search!=""? <div className="crossIcon"><img src={cross} onClick={(e)=>this.clearSearch(e)}/></div>:null}
                            </div>
                            <div className="col-md-5 pad-right-0 columns">
                                <div className="col-md-7 pad-0">
                                    <h3>Visible Columns <span>{this.props.headerCondition ? this.props.getHeaderConfig.length: this.props.customHeadersState.length}</span></h3>
                                </div>
                                <div className="col-md-5 pad-0 textCenter">
                                    <button className="resetBtn" onClick={(e) => this.props.resetColumnConfirmation(e)}>
                                        Reset to default
                                    </button>
                                    {!this.props.headerCondition ?  
                                    <button 
                                    className={this.props.customHeadersState.length == 0 || this.props.saveState.length == 0 ? "opacity saveBtn": "saveBtn" } 
                                    onClick={(e) =>this.props.customHeadersState.length == 0 || this.props.saveState.length == 0 ?null : this.props.saveColumnSetting(e)}>
                                        Save
                                        </button>:
                                    <button 
                                    className={this.props.getHeaderConfig.length == 0 || this.props.saveState.length == 0 ? "opacity saveBtn": "saveBtn" } 
                                    onClick={(e) =>this.props.getHeaderConfig.length == 0 || this.props.saveState.length == 0 ? null : this.props.saveColumnSetting(e)}>
                                        Save</button>}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-7 pad-0">
                            <div className="filterLeftHead">
                                <div className="col-md-12 identifier">
                                    <h2>Fixed</h2>
                                    <ul className="list-inline">
                                        {result.length!=0?result.map((data, key) => (<div key={key} className="inlineBlock col-md-4 pad-lft-0">
                                            {this.props.headerCondition ? <li className={!this.props.getHeaderConfig.includes(`${data}`) ? "active" : "opacity active"} onClick={(e) => !this.props.getHeaderConfig.includes(`${data}`) ? this.props.pushColumnData(data) : null}>{data}</li> :
                                                <li className={!this.props.customHeadersState.includes(`${data}`) ? "active" : "opacity active"} onClick={(e) => !this.props.customHeadersState.includes(`${data}`) ? this.props.pushColumnData(data) : null}>{data}</li>}
                                        </div>
                                        )):"No Data Found"}customHeadersState
                                        {/* <li>Assortment</li>
                                <li>Updated MBQ</li>
                                <li>Closing Stock</li>
                                <li className="active">Opening Stock</li>
                                <li className="active">Page Views</li>
                                <li>Requirements</li> */}
                                    </ul>
                                </div>
                                {/* <div className="col-md-12 identifier">
                                    <h2>Custom</h2>
                                    <ul className="list-inline">
                                        {this.props.customHeadersState.map((data, key) => (
                                            <li key={key} className="active">{data}</li>
                                        ))}
                                    </ul>
                                </div> */}
                            </div>
                        </div>
                        <div className="col-md-5 pad-0">
                            <div className="filterRightHead">
                                {/* <div className="col-md-12 savedColumn">
                            <h3>Saved Column presets</h3>
                            <label>My setting</label>
                            <label>Setting 2</label>
                        </div> */}
                                <div className="col-md-12 visibleColumn">
                                    <h3>All Visible Columns</h3>
                                <div className="row">
                                    {this.props.headerCondition ? this.props.getHeaderConfig.map((data, key) => (
                                        <div key={key} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter"><label className="alignMiddle displayFlex"><span className="eachVisibleColumn">{data}</span>
                                            <span onClick={(e) => this.closeColumns(data)}> <img src={closeSearch} /></span></label></div>
                                    )) : this.props.customHeadersState.map((dataa, keyy) => (
                                        <div key={keyy} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter">
                                            <label className="alignMiddle displayFlex">
                                            <span className="eachVisibleColumn">{dataa}</span><span onClick={(e) => this.closeColumns(dataa)}> <img className="float_Right displayPointer" src={closeSearch} /></span>
                                            </label>
                                        </div>
                                    ))}
                                    </div>
                                    {/* <div className="col-md-6 pad-0 m-top-3"><label>MBQ</label></div>
                            <div className="col-md-6 pad-0"> <label>Updated MBQ</label></div>
                            <div className="col-md-6 pad-0"><label>Closing Stock</label></div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : <div className="columnFilterGeneric" onClick={(e) => this.openColumnSetting("true")}>
                        <span className="glyphicon glyphicon-menu-left"></span></div>}
            </div>
        )
    }
}

export default ColumnSetting;