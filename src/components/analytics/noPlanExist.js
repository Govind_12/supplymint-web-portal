import React from "react";
import NoPlanIcon from '../../assets/planning.svg'
class NoPlanExist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="container_div" id="">
        <div className="col-md-12 col-sm-12 col-xs-12">
          <div className="container_content height-70vh noPlanExist">
            <label className="contribution_mart"> OPEN TO BUY </label>
            <div className="middleContent">
              <img src={NoPlanIcon} />
              <h2>No existing Plan Found</h2>
              <button className="record_btn" >
                Create New Plan
                    </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NoPlanExist;
