import React from 'react';
import InfoIcon from '../../assets/info-icon.svg';
import Download from '../../assets/download2.svg';
import Location from '../../assets/location.svg';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import {ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip,
    Legend} from "recharts";
// import moment from 'moment'
import ToastLoader from "../loaders/toastLoader";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import Pagination from '../pagination';
import AnalysisSheet from '../../assets/analyse-sheet.svg';
import Next from '../../assets/next.svg';
import ColoumSetting from '../replenishment/coloumSetting';
import filterIcon from '../../assets/headerFilter.svg';
import ConfirmationSummaryModal from '../replenishment/confirmationReset';
import Search from '../../assets/search2.svg';
import searchIcon from '../../assets/clearSearch.svg';
import FilterLoader from "../../components/loaders/filterLoader";
import { CONFIG } from "../../config/index";
import axios from 'axios';
import ToastError from '../utils/toastError';

class PlannerWorkSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toastLoader: false,
            toastMsg: "",
            loader: false,
            
            reviewReason: [],
            selectOption: "",
            noOfItems: 0,

            assortmentSalesHistory: [],
            salesType: "month",

            assortmentDetails: {},
            assortmentItems: [],
            prevItem: "",
            currentItem: 0,
            nextItem: "",
            maxPageItem: 0,
            selectedItemsItem: 0,
            jumpPageItem: 1,
            assortmentItemSearchText: "",

            assortmentTimePhased:[],
            prevTimePhased: "",
            currentTimePhased: 0,
            nextTimePhased: "",
            maxPageTimePhased: 0,
            selectedItemsTimePhased: 0,
            jumpPageTimePhased: 1,
            timePhasedSearchText: "",

            allocationData: [],
            prevAllocation: "",
            currentAllocation: 0,
            nextAllocation: "",
            maxPageAllocation: 0,
            selectedItemsAllocation: 0,
            jumpPageAllocation: 1,
            allocationSearchText: "",

            isReportPageFlag: true, //For Hiding Set/Item Header in Column Setting::
            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "REVIEW_REASON_ITEM",
                basedOn: "ALL"
            },

            filterItems: {},
            mainCustomHeaders: [],
            getMainHeaderConfig: [],
            mainAvailableHeaders: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderConfigState: {}, 
            mainFixedHeaderData: [],
            mainHeaderConfigDataState: {},
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mandateHeaderMain: [],
            headerCondition: false,
            coloumSetting: false,
            dragOn: false,
            changesInMainHeaders: false,
            saveMainState: [],
            headerConfigDataState: {},
            confirmModal: false,
            headerMsg: "",
            tabVal: "1",
            openOption: false,

            sortedIn: "",
            sortedBy: "",
            prevFilter: "",
            assortmentNameSearch: "",
            openAssortmentNameSearch: false,
            assortmentNameArray: [],
            toastError: false,
            toastErrorMsg: "",
            storeCode: sessionStorage.getItem("currentStoreCode") !== undefined && sessionStorage.getItem("currentStoreCode") !== null ? 
                       sessionStorage.getItem("currentStoreCode") : "",
        }
    }
    openCloseSelectOption =()=> {
        this.setState({openOption: !this.state.openOption});
    }

    componentDidMount() {
        if(this.props.reviewReason && this.props.reviewReason.data && this.props.reviewReason.data.resource && this.props.reviewReason.data.resource.response.length > 0) {
            this.setState({
                reviewReason: this.props.reviewReason.data.resource.response
            })
        }else{
            this.props.getReviewReasonRequest('asd');
        }
        this.props.getAssortmentDetailsRequest({assortmentNo: sessionStorage.getItem("assortmentIndex"), reason: sessionStorage.getItem("reason"), 
            storeCode: this.state.storeCode, storeName: sessionStorage.getItem("storeName"), 
            division: sessionStorage.getItem("division"), section: sessionStorage.getItem("section"), 
            department: sessionStorage.getItem("department"), priceBand: sessionStorage.getItem("priceBand"), 
            category: sessionStorage.getItem("category")});
        
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);

        sessionStorage.setItem('currentPage', "RDAANAMAIN")
        sessionStorage.setItem('currentPageName', "Review Reason Analysis")

        this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.assortmentDetails.isSuccess && nextProps.assortmentDetails.message.data.resource) {
            this.setState({
                assortmentDetails: nextProps.assortmentDetails.message.data.resource.response !== null ? nextProps.assortmentDetails.message.data.resource.response : {},
                selectOption: nextProps.assortmentDetails.message.data.resource.response !== null ? nextProps.assortmentDetails.message.data.resource.response.inventoryStatus : "",
                noOfItems: nextProps.assortmentDetails.message.data.resource.noOfItems,
                storeCode: nextProps.assortmentDetails.message.data.resource.response !== null ? nextProps.assortmentDetails.message.data.resource.response.storeCode : "",
                openAssortmentNameSearch: false,
            }, () => {
                if(Object.keys(nextProps.assortmentDetails.message.data.resource.response).length > 0 ) {
                    this.props.getAssortmentItemsRequest({
                        reason: this.state.assortmentDetails.inventoryStatus,
                        storeCode: this.state.assortmentDetails.storeCode,
                        pageNo: 1,
                        type: 1,
                        division: this.state.assortmentDetails.division,
                        section: this.state.assortmentDetails.section,
                        department: this.state.assortmentDetails.department,
                        category: this.state.assortmentDetails.category,
                        priceBand: this.state.assortmentDetails.priceBand,
                        searchText: this.state.assortmentItemSearchText,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                        assortment: this.state.assortmentDetails.assortment
                    });
                    this.props.getAssortmentTimePhasedRequest({
                        pageNo: 1,
                        type: 1,
                        storeCode: this.state.assortmentDetails.storeCode,
                        searchText: this.state.timePhasedSearchText,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                        
                    });
                    this.props.getAssortmentSalesHistoryRequest({
                        assortment: this.state.assortmentDetails.assortment, 
                        type: this.state.salesType,
                        storeCode: this.state.assortmentDetails.storeCode,
                    });
                    let payload ={
                        pageNo: 1,
                        type: 1,
                        storeCode: this.state.assortmentDetails.storeCode,
                        searchText: '',
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    }
                    this.props.getAllocationDataRequest(payload);
                }
            })
            this.props.getAssortmentDetailsClear();
        }

        if(nextProps.assortmentItems.isSuccess) {
            this.setState({
                assortmentItems: nextProps.assortmentItems.message.resource.response == null ? [] : nextProps.assortmentItems.message.resource.response,
                prevItem: nextProps.assortmentItems.data.resource.prevPage,
                currentItem: nextProps.assortmentItems.data.resource.currPage,
                nextItem: nextProps.assortmentItems.data.resource.currPage + 1,
                maxPageItem: nextProps.assortmentItems.data.resource.maxPage,
                selectedItemsItem: nextProps.assortmentItems.data.resource.totalSearchReasult == undefined || nextProps.assortmentItems.data.resource.totalSearchReasult == null 
                               ? 0 : nextProps.assortmentItems.data.resource.totalSearchReasult,
                jumpPageItem: nextProps.assortmentItems.data.resource.currPage
            
            })
            this.props.getAssortmentItemsClear();
        }
        if(nextProps.assortmentSalesHistory.isSuccess) {
            this.setState({
                assortmentSalesHistory: nextProps.assortmentSalesHistory.message.resource.response == null ? [] : nextProps.assortmentSalesHistory.message.resource.response
            })
            this.props.getAssortmentSalesHistoryClear();
        }
        if(nextProps.analytics.assortmentTimePhased.isSuccess) {
            this.setState({
                assortmentTimePhased: nextProps.analytics.assortmentTimePhased.message.resource.response == null ? [] : nextProps.analytics.assortmentTimePhased.message.resource.response,
                prevTimePhased: nextProps.analytics.assortmentTimePhased.data.resource.prevPage,
                currentTimePhased: nextProps.analytics.assortmentTimePhased.data.resource.currPage,
                nextTimePhased: nextProps.analytics.assortmentTimePhased.data.resource.currPage + 1,
                maxPageTimePhased: nextProps.analytics.assortmentTimePhased.data.resource.maxPage,
                selectedItemsTimePhased: nextProps.analytics.assortmentTimePhased.data.resource.totalSearchReasult == undefined || nextProps.analytics.assortmentTimePhased.data.resource.totalSearchReasult == null 
                               ? 0 : nextProps.analytics.assortmentTimePhased.data.resource.totalSearchReasult,
                jumpPageTimePhased: nextProps.analytics.assortmentTimePhased.data.resource.currPage
            })
            this.props.getAssortmentTimePhasedClear();
        }
        if(nextProps.reviewReason && nextProps.reviewReason.data && nextProps.reviewReason.data.resource && nextProps.reviewReason.data.resource.response && nextProps.reviewReason.data.resource.response.length > 0) {
            this.setState({
                reviewReason: nextProps.reviewReason.data.resource.response
            })
            this.props.getReviewReasonClear();
        }
        if(nextProps.analytics.allocationData.isSuccess) { 
            this.setState({
                allocationData: nextProps.analytics.allocationData.data.resource.response == null ? [] : nextProps.analytics.allocationData.data.resource.response,  
                prevAllocation: nextProps.analytics.allocationData.data.resource.prevPage,
                currentAllocation: nextProps.analytics.allocationData.data.resource.currPage,
                nextAllocation: nextProps.analytics.allocationData.data.resource.currPage + 1,
                maxPageAllocation: nextProps.analytics.allocationData.data.resource.maxPage,
                selectedItemsAllocation: nextProps.analytics.allocationData.data.resource.totalSearchReasult == undefined || nextProps.analytics.allocationData.data.resource.totalSearchReasult == null 
                               ? 0 : nextProps.analytics.allocationData.data.resource.totalSearchReasult,
                jumpPageAllocation: nextProps.analytics.allocationData.data.resource.currPage
           
            })
            this.props.getAllocationDataClear();
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
                let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                this.setState({
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    mainCustomHeaders: this.state.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
                    mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
                });
            }
            this.props.getMainHeaderConfigClear();
        }
        if (this.props.replenishment.createMainHeaderConfig.isSuccess && this.props.replenishment.createMainHeaderConfig.data.basedOn == "ALL") {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            this.props.createMainHeaderConfigClear();
        }
    }

    getAssortmentDetail =(val)=> {
        let currentIndex = Number(sessionStorage.getItem("assortmentIndex"))
        let totalAssortment = Number(sessionStorage.getItem("totalAssortment"))
        if(currentIndex >= 0) {
           if(val == "Increment") {
                if( (currentIndex + 1) < totalAssortment){
                this.props.getAssortmentDetailsRequest({assortmentNo: (currentIndex+1), reason: this.state.selectOption, storeCode: this.state.storeCode, storeName: sessionStorage.getItem("storeName"), division: sessionStorage.getItem("division"), section: sessionStorage.getItem("section"), department: sessionStorage.getItem("department"), priceBand: sessionStorage.getItem("priceBand"), category: sessionStorage.getItem("category")});
                sessionStorage.setItem("assortmentIndex", (currentIndex + 1));
                }

            } else if(val == "Decrement" && currentIndex > 0) {
                this.props.getAssortmentDetailsRequest({assortmentNo: (currentIndex-1), reason: this.state.selectOption, storeCode: this.state.storeCode, storeName: sessionStorage.getItem("storeName"), division: sessionStorage.getItem("division"), section: sessionStorage.getItem("section"), department: sessionStorage.getItem("department"), priceBand: sessionStorage.getItem("priceBand"), category: sessionStorage.getItem("category")});
                sessionStorage.setItem("assortmentIndex", (currentIndex-1));
            }
        }
    }

    selectReviewOption(e) {
        this.props.getDataWithoutFilterClear();
        this.setState({
            selectOption: e.target.value
        }, ()=>{
            this.props.getAssortmentDetailsRequest({assortmentNo: 0, reason: this.state.selectOption, 
                                 storeCode: this.state.storeCode, storeName: sessionStorage.getItem("storeName"), 
                                 division: sessionStorage.getItem("division"), section: sessionStorage.getItem("section"), 
                                 department: sessionStorage.getItem("department"), priceBand: sessionStorage.getItem("priceBand"), 
                                 category: sessionStorage.getItem("category")});

        })
    }
    // _exoprtPDF =()=> {
    //     this.setState({ loader: true},()=>{
    //         const input = document.getElementById('divToPrint');
    //         html2canvas(input)
    //           .then((canvas) => {
    //             const imgData = canvas.toDataURL('image/png');
    //             const pdf = new jsPDF({
    //                 orientation: 'landscape',
    //                 unit: 'in',
    //                 format: [1200, 500]
    //             });
    //             pdf.addImage(imgData, 'JPEG', 0, 0);
    //             pdf.save("download.pdf");
    //             this.setState({ loader: false})
    //         });
    //     })      
    // }
 
    xlscsv =()=> {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        let mainHeaderPayload = this.state.mainHeaderPayload
        let payload = {};
        if( mainHeaderPayload.displayName == "REVIEW_REASON_ITEM"){
            payload = {
                reason: this.state.assortmentDetails.inventoryStatus,
                storeCode: this.state.assortmentDetails.storeCode,
                pageNo: this.props.analytics.assortmentItems.data.resource.currPage,
                type: this.state.assortmentItemSearchText.length ? 2 : 1,
                division: this.state.assortmentDetails.division,
                section: this.state.assortmentDetails.section,
                department: this.state.assortmentDetails.department,
                category: this.state.assortmentDetails.category,
                priceBand: this.state.assortmentDetails.priceBand,
                searchText: this.state.assortmentItemSearchText,
                sortedIn: this.state.sortedIn,
                sortedBy: this.state.sortedBy,
            }
        }
        if( mainHeaderPayload.displayName == "REVIEW_REASON_ALLOCATION"){
            payload ={
                pageNo: this.props.analytics.allocationData.data.resource.currPage,
                type: this.state.allocationSearchText.length ? 2 : 1,
                storeCode: this.state.assortmentDetails.storeCode,
                searchText: this.state.allocationSearchText,
                sortedIn: this.state.sortedIn,
                sortedBy: this.state.sortedBy,
            }
        }
        if( mainHeaderPayload.displayName == "REVIEW_REASON_TIME_PHASE"){
            payload = {
                pageNo: this.props.analytics.assortmentTimePhased.data.resource.currPage,
                type: this.state.timePhasedSearchText.length ? 2 : 1,
                storeCode: this.state.assortmentDetails.storeCode,
                searchText: this.state.timePhasedSearchText,
                sortedIn: this.state.sortedIn,
                sortedBy: this.state.sortedBy,
                
            }
        }
        let response = ""
        axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=${mainHeaderPayload.displayName}&isAllData=true&isOnlyCurrentPage=false`, payload, { headers: headers })
            .then(res => {
                response = res
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
                setTimeout(()=>{
                    this.setState({
                        toastError: false
                    })
                }, 5000)
            });
    }

    closeToastError = () => {
        this.setState({ toastError: false })
    }

    changeTabCS = (e) => {
        if( e.target.hash == "#pwsassortmentdetails"){
            let mainHeaderPayload = this.state.mainHeaderPayload
            mainHeaderPayload.displayName = "REVIEW_REASON_ITEM"

            this.setState({ 
                mainHeaderPayload, 
                filterItems: {},
                mainCustomHeaders: [],
                getMainHeaderConfig: [],
                mainAvailableHeaders: [],
                mainFixedHeader: [],
                mainCustomHeadersState: [],
                mainHeaderConfigState: {}, 
                mainFixedHeaderData: [],
                mainHeaderConfigDataState: {},
                mainHeaderSummary: [],
                mainDefaultHeaderMap: [],
                mandateHeaderMain: [],
                headerCondition: false,
                coloumSetting: false,
                dragOn: false,
                changesInMainHeaders: false,
                saveMainState: [],
                headerConfigDataState: {},
                confirmModal: false,
                headerMsg: "",
                tabVal: "1",

                sortedIn: "",
                sortedBy: "",
                prevFilter: "",
            
            },()=>{
                 this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            })
        }
        if( e.target.hash == "#pwsallocation"){
            let mainHeaderPayload = this.state.mainHeaderPayload
            mainHeaderPayload.displayName = "REVIEW_REASON_ALLOCATION"

            this.setState({ 
                mainHeaderPayload,
                filterItems: {},
                mainCustomHeaders: [],
                getMainHeaderConfig: [],
                mainAvailableHeaders: [],
                mainFixedHeader: [],
                mainCustomHeadersState: [],
                mainHeaderConfigState: {}, 
                mainFixedHeaderData: [],
                mainHeaderConfigDataState: {},
                mainHeaderSummary: [],
                mainDefaultHeaderMap: [],
                mandateHeaderMain: [],
                headerCondition: false,
                coloumSetting: false,
                dragOn: false,
                changesInMainHeaders: false,
                saveMainState: [],
                headerConfigDataState: {},
                confirmModal: false,
                headerMsg: "",
                tabVal: "1", 

                sortedIn: "",
                sortedBy: "",
                prevFilter: "",

            },()=>{
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            })
        }
        if( e.target.hash == "#pwstimephased"){
            let mainHeaderPayload = this.state.mainHeaderPayload
            mainHeaderPayload.displayName = "REVIEW_REASON_TIME_PHASE"

            this.setState({ 
                mainHeaderPayload,
                filterItems: {},
                mainCustomHeaders: [],
                getMainHeaderConfig: [],
                mainAvailableHeaders: [],
                mainFixedHeader: [],
                mainCustomHeadersState: [],
                mainHeaderConfigState: {}, 
                mainFixedHeaderData: [],
                mainHeaderConfigDataState: {},
                mainHeaderSummary: [],
                mainDefaultHeaderMap: [],
                mandateHeaderMain: [],
                headerCondition: false,
                coloumSetting: false,
                dragOn: false,
                changesInMainHeaders: false,
                saveMainState: [],
                headerConfigDataState: {},
                confirmModal: false,
                headerMsg: "",
                tabVal: "1", 

                sortedIn: "",
                sortedBy: "",
                prevFilter: "",
                
            },()=>{
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            })
        }
    }

    //----------------------------- Assortment Item Tab----------------------------------//

    onSearchItem = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.key == 'Enter') {
                this.props.getAssortmentItemsRequest({
                    reason: this.state.assortmentDetails.inventoryStatus,
                    storeCode: this.state.assortmentDetails.storeCode,
                    pageNo: 1,
                    type: 2,
                    division: this.state.assortmentDetails.division,
                    section: this.state.assortmentDetails.section,
                    department: this.state.assortmentDetails.department,
                    category: this.state.assortmentDetails.category,
                    priceBand: this.state.assortmentDetails.priceBand,
                    searchText: this.state.assortmentItemSearchText,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                    assortment: this.state.assortmentDetails.assortment
                });
            }
        }
        this.setState({ assortmentItemSearchText: e.target.value }, () => {
            if (this.state.assortmentItemSearchText == "" ) {
                this.props.getAssortmentItemsRequest({
                    reason: this.state.assortmentDetails.inventoryStatus,
                    storeCode: this.state.assortmentDetails.storeCode,
                    pageNo: 1,
                    type: 1,
                    division: this.state.assortmentDetails.division,
                    section: this.state.assortmentDetails.section,
                    department: this.state.assortmentDetails.department,
                    category: this.state.assortmentDetails.category,
                    priceBand: this.state.assortmentDetails.priceBand,
                    searchText: "",
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                    assortment: this.state.assortmentDetails.assortment
                });
            }
        })
    }

    onSearchClearItem =()=> {
        this.setState({ assortmentItemSearchText: "" }, () => {
            this.props.getAssortmentItemsRequest({
                reason: this.state.assortmentDetails.inventoryStatus,
                storeCode: this.state.assortmentDetails.storeCode,
                pageNo: 1,
                type: 1,
                division: this.state.assortmentDetails.division,
                section: this.state.assortmentDetails.section,
                department: this.state.assortmentDetails.department,
                category: this.state.assortmentDetails.category,
                priceBand: this.state.assortmentDetails.priceBand,
                searchText: "",
                sortedIn: this.state.sortedIn,
                sortedBy: this.state.sortedBy,
                assortment: this.state.assortmentDetails.assortment
            });
        })
    }

    itemPage =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.currentItem == "" || this.state.currentItem == undefined || this.state.currentItem == 1) {
            } else {
                this.setState({
                    prevItem: this.props.analytics.assortmentItems.data.resource.prevPage,
                    currentItem: this.props.analytics.assortmentItems.data.resource.currPage,
                    nextItem: this.props.analytics.assortmentItems.data.resource.currPage + 1,
                    maxPageItem: this.props.analytics.assortmentItems.data.resource.maxPage,
                })
                if (this.props.analytics.assortmentItems.data.resource.currPage != 0) {
                    this.props.getAssortmentItemsRequest({
                        reason: this.state.assortmentDetails.inventoryStatus,
                        storeCode: this.state.assortmentDetails.storeCode,
                        pageNo: this.props.analytics.assortmentItems.data.resource.currPage -1,
                        type: this.state.assortmentItemSearchText == "" ? 1 : 2,
                        division: this.state.assortmentDetails.division,
                        section: this.state.assortmentDetails.section,
                        department: this.state.assortmentDetails.department,
                        category: this.state.assortmentDetails.category,
                        priceBand: this.state.assortmentDetails.priceBand,
                        searchText: this.state.assortmentItemSearchText,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                        assortment: this.state.assortmentDetails.assortment
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prevItem: this.props.analytics.assortmentItems.data.resource.prevPage,
                currentItem: this.props.analytics.assortmentItems.data.resource.currPage,
                nextItem: this.props.analytics.assortmentItems.data.resource.currPage + 1,
                maxPageItem: this.props.analytics.assortmentItems.data.resource.maxPage,
            })
            if (this.props.analytics.assortmentItems.data.resource.currPage != this.props.analytics.assortmentItems.data.resource.maxPage) {
                this.props.getAssortmentItemsRequest({
                    reason: this.state.assortmentDetails.inventoryStatus,
                    storeCode: this.state.assortmentDetails.storeCode,
                    pageNo: this.props.analytics.assortmentItems.data.resource.currPage + 1,
                    type: this.state.assortmentItemSearchText == "" ? 1 : 2,
                    division: this.state.assortmentDetails.division,
                    section: this.state.assortmentDetails.section,
                    department: this.state.assortmentDetails.department,
                    category: this.state.assortmentDetails.category,
                    priceBand: this.state.assortmentDetails.priceBand,
                    searchText: this.state.assortmentItemSearchText,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                    assortment: this.state.assortmentDetails.assortment
                });
            }
        }
        else if (e.target.id == "first") {
            if (this.state.currentItem == 1 || this.state.currentItem == "" || this.state.currentItem == undefined) {
            }
            else {
                this.setState({
                    prevItem: this.props.analytics.assortmentItems.data.resource.prevPage,
                    currentItem: this.props.analytics.assortmentItems.data.resource.currPage,
                    nextItem: this.props.analytics.assortmentItems.data.resource.currPage + 1,
                    maxPageItem: this.props.analytics.assortmentItems.data.resource.maxPage,
                })
                if (this.props.analytics.assortmentItems.data.resource.currPage <= this.props.analytics.assortmentItems.data.resource.maxPage) {
                    this.props.getAssortmentItemsRequest({
                        reason: this.state.assortmentDetails.inventoryStatus,
                        storeCode: this.state.assortmentDetails.storeCode,
                        pageNo: 1,
                        type: this.state.assortmentItemSearchText == "" ? 1 : 2,
                        division: this.state.assortmentDetails.division,
                        section: this.state.assortmentDetails.section,
                        department: this.state.assortmentDetails.department,
                        category: this.state.assortmentDetails.category,
                        priceBand: this.state.assortmentDetails.priceBand,
                        searchText: this.state.assortmentItemSearchText,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                        assortment: this.state.assortmentDetails.assortment
                    });
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.currentItem == this.state.maxPageItem || this.state.currentItem == undefined) {
            }
            else {
                this.setState({
                    prevItem: this.props.analytics.assortmentItems.data.resource.prevPage,
                    currentItem: this.props.analytics.assortmentItems.data.resource.currPage,
                    nextItem: this.props.analytics.assortmentItems.data.resource.currPage + 1,
                    maxPageItem: this.props.analytics.assortmentItems.data.resource.maxPage,
                })
                if (this.props.analytics.assortmentItems.data.resource.currPage <= this.props.analytics.assortmentItems.data.resource.maxPage) {
                    this.props.getAssortmentItemsRequest({
                        reason: this.state.assortmentDetails.inventoryStatus,
                        storeCode: this.state.assortmentDetails.storeCode,
                        pageNo: this.props.analytics.assortmentItems.data.resource.maxPage -1,
                        type: this.state.assortmentItemSearchText == "" ? 1 : 2,
                        division: this.state.assortmentDetails.division,
                        section: this.state.assortmentDetails.section,
                        department: this.state.assortmentDetails.department,
                        category: this.state.assortmentDetails.category,
                        priceBand: this.state.assortmentDetails.priceBand,
                        searchText: this.state.assortmentItemSearchText,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                        assortment: this.state.assortmentDetails.assortment
                    });
                }
            }
        }
    }

    getAnyItemPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPageItem: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.currentItem) {
                if (_.target.value != "") {
                    this.props.getAssortmentItemsRequest({
                        reason: this.state.assortmentDetails.inventoryStatus,
                        storeCode: this.state.assortmentDetails.storeCode,
                        pageNo: _.target.value,
                        type: this.state.assortmentItemSearchText == "" ? 1 : 2,
                        division: this.state.assortmentDetails.division,
                        section: this.state.assortmentDetails.section,
                        department: this.state.assortmentDetails.department,
                        category: this.state.assortmentDetails.category,
                        priceBand: this.state.assortmentDetails.priceBand,
                        searchText: this.state.assortmentItemSearchText,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                        assortment: this.state.assortmentDetails.assortment
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    //-----------------------------------------------------------------------------------//

    //-----------------------------Sales History Tab-------------------------------------//

    getSalesChart(type) {
        this.setState({
            salesType: type
        }, () => {
            this.props.getAssortmentSalesHistoryRequest({
                assortment: this.state.assortmentDetails.assortment, 
                type: this.state.salesType, 
                storeCode: this.state.assortmentDetails.storeCode
            })
        })
    }

    //-----------------------------------------------------------------------------------//
    //-----------------------------Time Phase Tab----------------------------------------//
  
    onSearchTimePhase = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.key == 'Enter') {
                this.props.getAssortmentTimePhasedRequest({
                    pageNo: 1,
                    type: 2,
                    storeCode: this.state.assortmentDetails.storeCode,
                    searchText: this.state.timePhasedSearchText,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                });
            }
        }
        this.setState({ timePhasedSearchText: e.target.value }, () => {
            if (this.state.timePhasedSearchText == "" ) {
                this.props.getAssortmentTimePhasedRequest({
                    pageNo: 1,
                    type: 1,
                    storeCode: this.state.assortmentDetails.storeCode,
                    searchText: "",
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                });
            }
        })
    }

    onSearchClearTimePhase =()=> {
        this.setState({ timePhasedSearchText: "" }, () => {
            this.props.getAssortmentTimePhasedRequest({
                pageNo: 1,
                type: 1,
                storeCode: this.state.assortmentDetails.storeCode,
                searchText: "",
                sortedIn: this.state.sortedIn,
                sortedBy: this.state.sortedBy,
            });
        })
    }

    timePhasedPage =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.currentTimePhased == "" || this.state.currentTimePhased == undefined || this.state.currentTimePhased == 1) {
            } else {
                this.setState({
                    prevTimePhased: this.props.analytics.assortmentTimePhased.data.resource.prevPage,
                    currentTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage,
                    nextTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage + 1,
                    maxPageTimePhased: this.props.analytics.assortmentTimePhased.data.resource.maxPage,
                })
                if (this.props.analytics.assortmentTimePhased.data.resource.currPage != 0) {
                    this.props.getAssortmentTimePhasedRequest({
                        pageNo: this.props.analytics.assortmentTimePhased.data.resource.currPage -1,
                        type: this.state.timePhasedSearchText == "" ? 1 : 2,
                        searchText: this.state.timePhasedSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prevTimePhased: this.props.analytics.assortmentTimePhased.data.resource.prevPage,
                currentTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage,
                nextTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage + 1,
                maxPageTimePhased: this.props.analytics.assortmentTimePhased.data.resource.maxPage,
            })
            if (this.props.analytics.assortmentTimePhased.data.resource.currPage != this.props.analytics.assortmentTimePhased.data.resource.maxPage) {
                this.props.getAssortmentTimePhasedRequest({
                    pageNo: this.props.analytics.assortmentTimePhased.data.resource.currPage + 1,
                    type: this.state.timePhasedSearchText == "" ? 1 : 2,
                    searchText: this.state.timePhasedSearchText,
                    storeCode: this.state.assortmentDetails.storeCode,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                });
            }
        }
        else if (e.target.id == "first") {
            if (this.state.currentTimePhased == 1 || this.state.currentTimePhased == "" || this.state.currentTimePhased == undefined) {
            }
            else {
                this.setState({
                    prevTimePhased: this.props.analytics.assortmentTimePhased.data.resource.prevPage,
                    currentTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage,
                    nextTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage + 1,
                    maxPageTimePhased: this.props.analytics.assortmentTimePhased.data.resource.maxPage,
                })
                if (this.props.analytics.assortmentTimePhased.data.resource.currPage <= this.props.analytics.assortmentTimePhased.data.resource.maxPage) {
                    this.props.getAssortmentTimePhasedRequest({
                        pageNo: 1,
                        type: this.state.timePhasedSearchText == "" ? 1 : 2,
                        searchText: this.state.timePhasedSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.currentTimePhased == this.state.maxPageTimePhased || this.state.currentTimePhased == undefined) {
            }
            else {
                this.setState({
                    prevTimePhased: this.props.analytics.assortmentTimePhased.data.resource.prevPage,
                    currentTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage,
                    nextTimePhased: this.props.analytics.assortmentTimePhased.data.resource.currPage + 1,
                    maxPageTimePhased: this.props.analytics.assortmentTimePhased.data.resource.maxPage,
                })
                if (this.props.analytics.assortmentTimePhased.data.resource.currPage <= this.props.analytics.assortmentTimePhased.data.resource.maxPage) {
                    this.props.getAssortmentTimePhasedRequest({
                        pageNo: this.props.analytics.assortmentTimePhased.data.resource.maxPage,
                        type: this.state.timePhasedSearchText == "" ? 1 : 2,
                        searchText: this.state.timePhasedSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
            }
        }
    }

    getAnyTimePhasedPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPageTimePhased: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.currentTimePhased) {
                if (_.target.value != "") {
                    this.props.getAssortmentTimePhasedRequest({
                        pageNo: _.target.value,
                        type: this.state.timePhasedSearchText == "" ? 1 : 2,
                        searchText: this.state.timePhasedSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    //-----------------------------------------------------------------------------------//

    //------------------------------Allocation Tab---------------------------------------//

    onSearchAllocation = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.key == 'Enter') {
                this.props.getAllocationDataRequest({
                    pageNo: 1,
                    type: 2,
                    searchText: this.state.allocationSearchText,
                    storeCode: this.state.assortmentDetails.storeCode,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                });
            }
        }
        this.setState({ allocationSearchText: e.target.value }, () => {
            if (this.state.allocationSearchText == "" ) {
                this.props.getAllocationDataRequest({
                    pageNo: 1,
                    type: 1,
                    searchText: "",
                    storeCode: this.state.assortmentDetails.storeCode,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                });
            }
        })
    }

    onSearchClearAllocation =()=> {
        this.setState({ allocationSearchText: "" }, () => {
            this.props.getAllocationDataRequest({
                pageNo: 1,
                type: 1,
                searchText: "",
                storeCode: this.state.assortmentDetails.storeCode,
                sortedIn: this.state.sortedIn,
                sortedBy: this.state.sortedBy,
            });
        })
    }

    allocationPage =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.currentAllocation == "" || this.state.currentAllocation == undefined || this.state.currentAllocation == 1) {
            } else {
                this.setState({
                    prevAllocation: this.props.analytics.allocationData.data.resource.prevPage,
                    currentAllocation: this.props.analytics.allocationData.data.resource.currPage,
                    nextAllocation: this.props.analytics.allocationData.data.resource.currPage + 1,
                    maxPageAllocation: this.props.analytics.allocationData.data.resource.maxPage,
                })
                if (this.props.analytics.allocationData.data.resource.currPage != 0) {
                    this.props.getAllocationDataRequest({
                        pageNo: this.props.analytics.allocationData.data.resource.currPage -1,
                        type: this.state.allocationSearchText == "" ? 1 : 2,
                        searchText: this.state.allocationSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prevAllocation: this.props.analytics.allocationData.data.resource.prevPage,
                currentAllocation: this.props.analytics.allocationData.data.resource.currPage,
                nextAllocation: this.props.analytics.allocationData.data.resource.currPage + 1,
                maxPageAllocation: this.props.analytics.allocationData.data.resource.maxPage,
            })
            if (this.props.analytics.allocationData.data.resource.currPage != this.props.analytics.allocationData.data.resource.maxPage) {
                this.props.getAllocationDataRequest({
                    pageNo: this.props.analytics.allocationData.data.resource.currPage + 1,
                    type: this.state.allocationSearchText == "" ? 1 : 2,
                    searchText: this.state.allocationSearchText,
                    storeCode: this.state.assortmentDetails.storeCode,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                });
            }
        }
        else if (e.target.id == "first") {
            if (this.state.currentAllocation == 1 || this.state.currentAllocation == "" || this.state.currentAllocation == undefined) {
            }
            else {
                this.setState({
                    prevAllocation: this.props.analytics.allocationData.data.resource.prevPage,
                    currentAllocation: this.props.analytics.allocationData.data.resource.currPage,
                    nextAllocation: this.props.analytics.allocationData.data.resource.currPage + 1,
                    maxPageAllocation: this.props.analytics.allocationData.data.resource.maxPage,
                })
                if (this.props.analytics.allocationData.data.resource.currPage <= this.props.analytics.allocationData.data.resource.maxPage) {
                    this.props.getAllocationDataRequest({
                        pageNo: 1,
                        type: this.state.allocationSearchText == "" ? 1 : 2,
                        searchText: this.state.allocationSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.currentAllocation == this.state.maxPageAllocation || this.state.currentAllocation == undefined) {
            }
            else {
                this.setState({
                    prevAllocation: this.props.analytics.allocationData.data.resource.prevPage,
                    currentAllocation: this.props.analytics.allocationData.data.resource.currPage,
                    nextAllocation: this.props.analytics.allocationData.data.resource.currPage + 1,
                    maxPageAllocation: this.props.analytics.allocationData.data.resource.maxPage,
                })
                if (this.props.analytics.allocationData.data.resource.currPage <= this.props.analytics.allocationData.data.resource.maxPage) {
                    this.props.getAllocationDataRequest({
                        pageNo: this.props.analytics.allocationData.data.resource.maxPage,
                        type: this.state.allocationSearchText == "" ? 1 : 2,
                        searchText: this.state.allocationSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
            }
        }
    }

    getAnyAllocationPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPageAllocation: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.currentAllocation) {
                if (_.target.value != "") {
                    this.props.getAllocationDataRequest({
                        pageNo: _.target.value,
                        type: this.state.allocationSearchText == "" ? 1 : 2,
                        searchText: this.state.allocationSearchText,
                        storeCode: this.state.assortmentDetails.storeCode,
                        sortedIn: this.state.sortedIn,
                        sortedBy: this.state.sortedBy,
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    openColoumSetting =(data)=> {
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
           this.setState({ coloumSetting: false }, () =>
               document.removeEventListener('click', this.closeColumnSetting))
       } 
    }

    handleDragStart =(e, key, dragItem, dragNode)=> {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({ dragOn:true })
    }
    handleDragEnd =(dragItem, dragNode)=> {
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({ dragOn:false})
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter =(e, key, dragItem, dragNode)=> {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true,  
                            mainCustomHeaders: configheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true, 
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList
                        }
                    })
                }
            }
        }
    }
    pushColumnData =(e,data)=> {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
               if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
                headerConfigDataState: customHeadersState,
            })
        }
    }
    closeColumn =(data)=> {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
    }
    saveColumnSetting =(e)=> {
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            let payload = {
                basedOn: "ALL",
                module: "ARS",
                subModule: "ANALYTICS",
                section: "REVIEW-REASON-ANALYSIS",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: this.state.mainHeaderPayload.displayName,
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }
            this.props.createMainHeaderConfigRequest(payload)
        }
    }
    resetColumnConfirmation =()=> {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            confirmModal: true,
        })
    }
    resetColumn =()=> {
        const {getMainHeaderConfig,mainFixedHeader} = this.state
        if (this.state.tabVal == 1) {
            let payload = {
                basedOn: "ALL",
                module: "ARS",
                subModule: "ANALYTICS",
                section: "REVIEW-REASON-ANALYSIS",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: this.state.mainHeaderPayload.displayName,
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainHeaderConfigDataState,
            }
            this.props.createMainHeaderConfigRequest(payload)
            let availableHeaders= mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders:availableHeaders
            })    
        }
    }
    closeConfirmModal =(e)=> {
        this.setState({ confirmModal: !this.state.confirmModal,})
    }
    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1) {
                this.openColoumSetting("true")
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            }
        })
    }
    //-----------------------------------------------------------------------------------------------//

    escFunction = (event) => {
        if( e.keyCode == 27 || (e.target.className.baseVal == undefined && (e.target.className.includes("backdrop")))){
            this.setState({ coloumSetting: false, headerCondition: false, confirmModal: false, openAssortmentNameSearch: false,})
        }
    }

    small =(str)=>{
        if (str != null) {
            var str = str.toString()
            if (str.length <= 25) {
                return false;
            }
            return true;
        }
    }

    filterHeader = (event) => {
        let mainHeaderPayload = this.state.mainHeaderPayload
        var data = event.target.dataset.key
        if( event.target.closest("th").classList.contains("rotate180"))
            event.target.closest("th").classList.remove("rotate180")
        else
            event.target.closest("th").classList.add("rotate180")  
        var def = {...this.state.mainHeaderConfigDataState};
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ sortedBy: filterKey, sortedIn: this.state.sortedIn == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ sortedBy: filterKey, sortedIn: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            if( mainHeaderPayload.displayName == "REVIEW_REASON_ITEM"){
                this.props.getAssortmentItemsRequest({
                    reason: this.state.assortmentDetails.inventoryStatus,
                    storeCode: this.state.assortmentDetails.storeCode,
                    pageNo: this.props.analytics.assortmentItems.data.resource.currPage,
                    type: this.state.assortmentItemSearchText.length ? 2 : 1,
                    division: this.state.assortmentDetails.division,
                    section: this.state.assortmentDetails.section,
                    department: this.state.assortmentDetails.department,
                    category: this.state.assortmentDetails.category,
                    priceBand: this.state.assortmentDetails.priceBand,
                    searchText: this.state.assortmentItemSearchText,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                    assortment: this.state.assortmentDetails.assortment
                });
            }
            if( mainHeaderPayload.displayName == "REVIEW_REASON_ALLOCATION"){
                let payload ={
                    pageNo: this.props.analytics.allocationData.data.resource.currPage,
                    type: this.state.allocationSearchText.length ? 2 : 1,
                    storeCode: this.state.assortmentDetails.storeCode,
                    searchText: this.state.allocationSearchText,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                }
                this.props.getAllocationDataRequest(payload);
            }
            if( mainHeaderPayload.displayName == "REVIEW_REASON_TIME_PHASE"){
                this.props.getAssortmentTimePhasedRequest({
                    pageNo: this.props.analytics.assortmentTimePhased.data.resource.currPage,
                    type: this.state.timePhasedSearchText.length ? 2 : 1,
                    storeCode: this.state.assortmentDetails.storeCode,
                    searchText: this.state.timePhasedSearchText,
                    sortedIn: this.state.sortedIn,
                    sortedBy: this.state.sortedBy,
                    
                });
            }
        })
    }

    openAssortmentNameSearch =(e)=>{
        const assortmentArray = sessionStorage.getItem("assortmentArray") !== null && sessionStorage.getItem("assortmentArray") !== undefined ? sessionStorage.getItem("assortmentArray").split(",") : []
        this.setState({
            openAssortmentNameSearch : !this.state.openAssortmentNameSearch, 
            assortmentNameSearch: "",
            assortmentNameArray: assortmentArray
        })
    }

    assortmentNameSearch =(e)=>{
        let assortmentNameArray = [];
        const assortmentArray = sessionStorage.getItem("assortmentArray") !== null && sessionStorage.getItem("assortmentArray") !== undefined ? sessionStorage.getItem("assortmentArray").split(",") : []
        assortmentNameArray = assortmentArray.filter(data => data.toLowerCase().includes(e.target.value.toLowerCase()))    
        this.setState({ assortmentNameSearch: e.target.value, assortmentNameArray})
    }
    getAssortmentDetailByName =(e, data)=>{
        this.props.getAssortmentDetailsRequest({assortmentName: data, reason: sessionStorage.getItem("reason"), 
            storeCode: this.state.storeCode, storeName: sessionStorage.getItem("storeName"), 
            division: sessionStorage.getItem("division"), section: sessionStorage.getItem("section"), 
            department: sessionStorage.getItem("department"), priceBand: sessionStorage.getItem("priceBand"), 
            category: sessionStorage.getItem("category")});
    }

    assortmentNameSearchClear =()=>{
        if( this.state.assortmentNameSearch.length){
            let assortmentNameArray = [];
            const assortmentArray = sessionStorage.getItem("assortmentArray") !== null && sessionStorage.getItem("assortmentArray") !== undefined ? sessionStorage.getItem("assortmentArray").split(",") : []
            this.setState({ assortmentNameSearch: "", assortmentNameArray: assortmentArray})
        }
    }

    render(){
        const {assortmentDetails,assortmentItems, assortmentTimePhased, assortmentSalesHistory, reviewReason, assortmentItemPagination, timePhasedPagination, salesType, selectOption, allocationData,allocationDataPagination} = this.state;
        return(
            <div className="container-fluid pad-0">
                { assortmentDetails && Object.keys(assortmentDetails).length > 0 && <div className="planner-work-sheet-head">
                    <div className="pwsh-left">
                        <div className="pws-slide">
                            <img className="pws-search" src={require('../../assets/searchicon.svg')} onClick={this.openAssortmentNameSearch}/>
                            <div className="pwss-drop">
                                <label>Assortment</label>
                                <h3> {assortmentDetails.assortment} </h3>
                            </div>
                            {this.state.openAssortmentNameSearch && <div className="pwssd-dropdown">
                                <ul>
                                    <li className="pwssd-search">
                                        <input type="search" value={this.state.assortmentNameSearch} onChange={this.assortmentNameSearch} onKeyDown={(e)=> {e.persist(); if(e.keyCode == 13) this.assortmentNameSearch(e)}} placeholder="Type To Search"/>
                                        <img className="search-image" src={Search}></img>
                                        <span className="closeSearch"><img src={searchIcon} onClick={this.assortmentNameSearchClear} /></span>
                                    </li>
                                    {this.state.assortmentNameArray !== null && this.state.assortmentNameArray !== undefined && this.state.assortmentNameArray.length ? this.state.assortmentNameArray.map( (data,key) =>(
                                      <li key={key} onClick={(e)=>this.getAssortmentDetailByName(e,data)}>{data}</li>)) :
                                    null}
                                </ul>
                            </div>}
                        </div>
                        <div className="pws-slide-btn">
                            <span onClick = {() =>this.getAssortmentDetail("Decrement")} className="prev"><img src={Next}></img></span>
                            <span onClick = {() =>this.getAssortmentDetail("Increment")} className="next"><img src={Next}></img></span>
                        </div>
                    </div>
                    <div className="pwsh-right">
                        <div className="pws-revive-reasons">
                            <label>Review Reasons</label>
                            <select onChange={(e) => this.selectReviewOption(e)}  value={selectOption}>
                                {reviewReason.length > 0 && reviewReason.map(reason => (
                                    <option key={reason} > {reason} </option>
                                ))}
                            </select>
                            {/* <button type="button" onClick={this.openCloseSelectOption} className="csd-area">{this.state.selectOption} </button>
                            {this.state.openOption && 
                            <div className="csd-option" onClick={this.selectReviewOption}>
                                <ul>
                                {reviewReason.length > 0 && reviewReason.map(reason => (
                                        <li  key={reason} value={reason}>{reason}</li>
                                    ))}
                                </ul>
                            </div>} */}
                        </div>
                    </div>
                </div>
                }
                <div className="pws-tablist">
                    <div className="col-lg-12 pad-0">
                        <div className="planner-work-sheet-tabs p-lr-47">
                            <div className="pwst-left">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className="nav-item active">
                                            <a className="nav-link gsti-btn" href="#pwsassortmentdetails" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Assortment Details</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#pwssaleshistory" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Sales History</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#pwstimephased" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Timephased View</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#pwsallocation" role="tab" data-toggle="tab" onClick={this.changeTabCS}>Allocation</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="pwst-right">
                                <div className="pwst-location">
                                    <label>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="11.333" height="14" viewBox="0 0 13.333 20">
                                            <path fill="#8b77fa" d="M6.667 0A6.627 6.627 0 0 0 0 6.335C0 9.833 2.891 14.01 6.667 20c3.776-5.99 6.667-10.167 6.667-13.665A6.626 6.626 0 0 0 6.667 0zm0 11.667a5 5 0 1 1 5-5 5 5 0 0 1-5 5z"/>
                                        </svg>
                                        Location</label>
                                    <p>{assortmentDetails.siteAlias}</p>
                                </div>
                            </div>
                        </div>
                        <div id="divToPrint" className="tab-content pws-tab-inner">
                            <div role="tabpanel" className="tab-pane fade pwstab-content in active" id="pwsassortmentdetails">
                                {/* {  assortmentDetails != undefined && Object.keys(assortmentDetails).length > 0 && 
                                <table className="pwsAstable">
                                    <tr>
                                        <td>Division</td>
                                        <td> {assortmentDetails.division} </td>
                                        <td>Store Code</td>
                                        <td> {assortmentDetails.storeCode} </td>
                                        <td>No. of days for sale</td>
                                        <td> {assortmentDetails.salesPerDay} </td>
                                    </tr>
                                    <tr>
                                        <td>Section</td>
                                        <td> {assortmentDetails.section} </td>
                                        <td>MRP Range</td>
                                        <td> {assortmentDetails.priceBand} </td>
                                        <td>Average COGS</td>
                                        <td> {assortmentDetails.avgCOGS} </td>
                                    </tr>
                                    <tr>
                                        <td>Department</td>
                                        <td> {assortmentDetails.department} </td>
                                        <td>No. of Items</td>
                                        <td> {this.state.noOfItems} </td>
                                    </tr>
                                </table>} */}
                                {  assortmentDetails != undefined && Object.keys(assortmentDetails).length > 0 && 
                                <div className="pws-data-details p-lr-47">
                                    <div className="pwsdd-col">
                                        <p>Division</p>
                                        <span>{assortmentDetails.division}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>Section</p>
                                        <span>{assortmentDetails.section}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>Department</p>
                                        <span>{assortmentDetails.department}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>Store Code</p>
                                        <span>{assortmentDetails.storeCode}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>No. of days for sale</p>
                                        <span>{assortmentDetails.salesPerDay}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>Average COGS</p>
                                        <span>{assortmentDetails.avgCOGS}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>No. of Items</p>
                                        <span>{this.state.noOfItems}</span>
                                    </div>
                                    <div className="pwsdd-col">
                                        <p>MRP Range</p>
                                        <span>{assortmentDetails.priceBand}</span>
                                    </div>
                                </div>}
                                <div className="arsPwq-data arsPws-data m-top-20 p-lr-47">
                                    <div className="ars-data-head">
                                        <div className="ars-data-head-search-bar">
                                            <input type="search" value={this.state.assortmentItemSearchText} onChange={(e) => this.onSearchItem(e)} onKeyDown={(e)=> {e.persist(); if(e.keyCode == 13) this.onSearchItem(e)}} placeholder="Type To Search"/>
                                            <img className="search-image" src={Search}></img>
                                            {this.state.assortmentItemSearchText != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.onSearchClearItem} /></span> : null}
                                        </div>
                                        <div className="adh-right">
                                            <div className="adhr-download">
                                                <button type="button" onClick={() => this.xlscsv()}>Download Report 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13.08" viewBox="0 0 15.583 17">
                                                        <path d="M7.792 14.875L2.125 8.5h4.25V0h2.833v8.5h4.25zm6.375-.708v1.417H1.417v-1.417H0V17h15.583v-2.833z"/>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="expand-new-table m-top-20">
                                        <div className="manage-table">
                                            <ColoumSetting {...this.props} {...this.state}
                                                handleDragStart={this.handleDragStart}
                                                handleDragEnter={this.handleDragEnter}
                                                onHeadersTabClick={this.onHeadersTabClick}
                                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                                closeColumn={(e) => this.closeColumn(e)}
                                                saveMainState={this.state.saveMainState}
                                                tabVal={this.state.tabVal}
                                                pushColumnData={this.pushColumnData}
                                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                                isReportPageFlag={this.state.isReportPageFlag}
                                            />
                                            <table className="table gen-main-table">
                                                <thead>
                                                    <tr>
                                                    {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                                            <label data-key={data}>{data}</label>
                                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                                        </th>
                                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                                            <label data-key={data}>{data}</label>
                                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                                        </th>
                                                    ))}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {assortmentItems.length != 0 ? assortmentItems.map((data, key) => (
                                                    <React.Fragment key={key}>
                                                    <tr>
                                                        {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                            <td key={key} ><label>{data[hdata]}</label>
                                                                {this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                                                            </td>
                                                        )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                            <td key={keyy} ><label className="table-td-text">{data[sdata]}</label>
                                                                {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                                            </td>
                                                        ))}
                                                    </tr>
                                                    </React.Fragment>
                                                    )) : <tr><td colSpan="100"><label>NO DATA FOUND</label></td></tr>}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPageItem} min="1" onKeyPress={this.getAnyItemPage} onChange={this.getAnyItemPage} value={this.state.jumpPageItem} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItemsItem}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={this.itemPage}
                                                        prev={this.state.prevItem} current={this.state.currentItem} maxPage={this.state.maxPageItem} next={this.state.nextItem} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" className="tab-pane fade pwstab-content p-lr-47" id="pwssaleshistory">
                                <div className="pws-sales-history m-top-10">
                                    <ul className="nav nav-tabs pws-salesh-list" role="tablist">
                                        <li className="nav-item ">
                                            <a onClick={() => this.getSalesChart("day")} className="nav-link pws-salesh-btn" href="#pwsSalesday" role="tab" data-toggle="tab">Day</a>
                                        </li>
                                        <li className="nav-item">
                                            <a onClick={() => this.getSalesChart("week")} className="nav-link pws-salesh-btn" href="#pwsSaleszweek" role="tab" data-toggle="tab">Week</a>
                                        </li>
                                        <li className="nav-item active">
                                            <a onClick={() => this.getSalesChart("month")} className="nav-link pws-salesh-btn" href="#pwsSalesmonth" role="tab" data-toggle="tab">Month</a>
                                        </li>
                                        <li className="nav-item">
                                            <a onClick={() => this.getSalesChart("quarter")} className="nav-link pws-salesh-btn" href="#pwsSalesquarter" role="tab" data-toggle="tab">Quarter</a>
                                        </li>
                                        <li className="nav-item">
                                            <a onClick={() => this.getSalesChart("year")} className="nav-link pws-salesh-btn" href="#pwsSalesyear" role="tab" data-toggle="tab">Year</a>
                                        </li>
                                    </ul>
                                    <div className="pws-sh-info">
                                        <span className="pws-shi-icon"><img src={InfoIcon}></img>
                                            <div className="pws-info-menu">
                                                <h4>Date Range Reference</h4>
                                                <div className="pws-Imlist">
                                                    <span>Days</span><span>30 days</span>
                                                </div>
                                                <div className="pws-Imlist">
                                                    <span>Week</span><span>26 Weeks</span>
                                                </div>
                                                <div className="pws-Imlist">
                                                    <span>Months</span><span>12 Months</span>
                                                </div>
                                                <div className="pws-Imlist">
                                                    <span>Quarter</span><span>8 Quarter</span>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div className="tab-content pws-salesh-tab">
                                    <div role="tabpanel" className="tab-pane fade" id="pwsSalesday"> 
                                    { assortmentSalesHistory && assortmentSalesHistory.length>0 && salesType == "day" &&  <ComposedChart
                                width={1000}
                                height={500}
                                data={assortmentSalesHistory}
                                margin={{
                                top: 20, right: 20, bottom: 20, left: 20,
                                }}
                            >
                                <CartesianGrid stroke="#f5f5f5" />
                                <XAxis dataKey="data" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Bar dataKey="sale" barSize={30} fill="#f3d9ff" />
                                <Line type="monotone" dataKey="forcast" stroke="#ecec1c" />
                            </ComposedChart>}
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade" id="pwsSaleszweek">
                                    { assortmentSalesHistory && assortmentSalesHistory.length>0 && salesType == "week" &&  <ComposedChart
                                width={1000}
                                height={500}
                                data={assortmentSalesHistory}
                                margin={{
                                top: 20, right: 20, bottom: 20, left: 20,
                                }}
                            >
                                <CartesianGrid stroke="#f5f5f5" />
                                <XAxis dataKey="data" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Bar dataKey="sale" barSize={30} fill="#f3d9ff" />
                                <Line type="monotone" dataKey="forcast" stroke="#ecec1c" />
                            </ComposedChart>}
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade in active" id="pwsSalesmonth"> 
                                    { assortmentSalesHistory && assortmentSalesHistory.length>0 && salesType == "month" &&  <ComposedChart
                                width={1000}
                                height={500}
                                data={assortmentSalesHistory}
                                margin={{
                                top: 20, right: 20, bottom: 20, left: 20,
                                }}
                            >
                                <CartesianGrid stroke="#f5f5f5" />
                                <XAxis dataKey="data" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Bar dataKey="sale" barSize={30} fill="#f3d9ff" />
                                <Line type="monotone" dataKey="forcast" stroke="#ecec1c" />
                            </ComposedChart>}
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade" id="pwsSalesquarter">
                                    { assortmentSalesHistory && assortmentSalesHistory.length>0 && salesType == "quarter" &&  <ComposedChart
                                width={1000}
                                height={500}
                                data={assortmentSalesHistory}
                                margin={{
                                top: 20, right: 20, bottom: 20, left: 20,
                                }}
                            >
                                <CartesianGrid stroke="#f5f5f5" />
                                <XAxis dataKey="data" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Bar dataKey="sale" barSize={30} fill="#f3d9ff" />
                                <Line type="monotone" dataKey="forcast" stroke="#ecec1c" />
                            </ComposedChart>}

                                    </div>
                                    <div role="tabpanel" className="tab-pane fade" id="pwsSalesyear">
                                    { assortmentSalesHistory && assortmentSalesHistory.length>0 && salesType == "year" &&  <ComposedChart
                                width={1000}
                                height={500}
                                data={assortmentSalesHistory}
                                margin={{
                                top: 20, right: 20, bottom: 20, left: 20,
                                }}
                            >
                                <CartesianGrid stroke="#f5f5f5" />
                                <XAxis dataKey="data" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Bar dataKey="sale" barSize={30} fill="#f3d9ff" />
                                <Line type="monotone" dataKey="forcast" stroke="#ecec1c" />
                            </ComposedChart>}
                                    </div>
                                {assortmentSalesHistory.length == 0 &&  <ComposedChart
                                width={1000}
                                height={500}
                                data=""
                                margin={{
                                top: 20, right: 20, bottom: 20, left: 20,
                                }}
                                >
                                <CartesianGrid stroke="#f5f5f5" />
                                <XAxis dataKey="data" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Bar dataKey="sale" barSize={30} fill="#f3d9ff" />
                                <Line type="monotone" dataKey="forcast" stroke="#ecec1c" />
                            </ComposedChart>}
                                </div>
                                { assortmentDetails != undefined && Object.keys(assortmentDetails).length > 0 && <table className="table pwsAstable">
                                    <thead><tr><th><h3>Details</h3></th></tr></thead>
                                    <tbody>
                                        <tr>
                                            <td>Last 30 Day Sales</td>
                                            <td> {assortmentDetails.last30DaySale == "" || assortmentDetails.last30DaySale == "-" || assortmentDetails.last30DaySale == null ? 0: assortmentDetails.last30DaySale} </td>
                                            <td>Required Inventory As Per Lead Days</td>
                                            <td> {assortmentDetails.mbqRol == "" || assortmentDetails.mbqRol == "-"  || assortmentDetails.mbqRol == null ? 0: assortmentDetails.mbqRol} </td>
                                            <td>Excess Inventory Days</td>
                                            <td> {assortmentDetails.excessInvDays == "" || assortmentDetails.excessInvDays == "-"  || assortmentDetails.excessInvDays == null ? 0: assortmentDetails.excessInvDays} </td>
                                        </tr>
                                        <tr>
                                            <td>Store Stock</td>
                                            <td> {assortmentDetails.storeInv == "" || assortmentDetails.storeInv == "-"  || assortmentDetails.storeInv == null ? 0: assortmentDetails.storeInv} </td>
                                            <td>MBQ(Minimum Base Quantity</td>
                                            <td> {assortmentDetails.mbq == "" || assortmentDetails.mbq == "-"  || assortmentDetails.mbq == null ? 0: assortmentDetails.mbq} </td>
                                            <td>Excess Inventory</td>
                                            <td> {assortmentDetails.excessInv == "" || assortmentDetails.excessInv == "-"  || assortmentDetails.excessInv == null ? 0: assortmentDetails.excessInv} </td>
                                        </tr>
                                        <tr>
                                            <td>Pack Pending Quantity</td>
                                            <td> {assortmentDetails.packPendingQuantity == "" || assortmentDetails.packPendingQuantity == "-"  || assortmentDetails.packPendingQuantity == null ? 0: assortmentDetails.packPendingQuantity} </td>
                                            <td>STR(Stock Turnover Ratio)</td>
                                            <td> {assortmentDetails.stockTurnOverRatio == "" || assortmentDetails.stockTurnOverRatio == "-"  || assortmentDetails.stockTurnOverRatio == null ? 0: assortmentDetails.stockTurnOverRatio} </td>
                                            <td>Excess Inventory COGS Value</td>
                                            <td> {assortmentDetails.excessInvCOGSValue == "" || assortmentDetails.excessInvCOGSValue == "-"  || assortmentDetails.excessInvCOGSValue == null ? 0: assortmentDetails.excessInvCOGSValue} </td>
                                        </tr>
                                        <tr>
                                            <td>Sales/Day (ROS)</td>
                                            <td> {assortmentDetails.salesPerDay == "" || assortmentDetails.salesPerDay == "-"  || assortmentDetails.salesPerDay == null ? 0: assortmentDetails.salesPerDay} </td>
                                            <td>Average COGS</td>
                                            <td> {assortmentDetails.avgCOGS == "" || assortmentDetails.avgCOGS == "-"  || assortmentDetails.avgCOGS == null ? 0: assortmentDetails.avgCOGS} </td>
                                            <td>Inventory Required</td>
                                            <td> {assortmentDetails.invRequired == "" || assortmentDetails.invRequired == "-"  || assortmentDetails.invRequired == null ? 0: assortmentDetails.invRequired} </td>
                                        </tr>
                                        <tr>
                                            <td>Stock Days</td>
                                            <td> {assortmentDetails.stockDays == "" || assortmentDetails.stockDays == "-"  || assortmentDetails.stockDays == null ? 0: assortmentDetails.stockDays} </td>
                                            <td>Potential Lost Days</td>
                                            <td> {assortmentDetails.potentialLostDays == "" || assortmentDetails.potentialLostDays == "-"  || assortmentDetails.potentialLostDays == null ? 0: assortmentDetails.potentialLostDays} </td>
                                            <td>Allocated Inventory</td>
                                            <td> {assortmentDetails.allocatedInv == "" || assortmentDetails.allocatedInv == "-"  || assortmentDetails.allocatedInv == null ? 0: assortmentDetails.allocatedInv} </td>
                                        </tr>
                                        <tr>
                                            <td>Lead Days</td>
                                            <td> {assortmentDetails.leadDays == "" || assortmentDetails.leadDays == "-"  || assortmentDetails.leadDays == null ? 0: assortmentDetails.leadDays} </td>
                                            <td>Potential Lost Inventory</td>
                                            <td> {assortmentDetails.potentialLostInv == "" || assortmentDetails.potentialLostInv == "-"  || assortmentDetails.potentialLostInv == null ? 0: assortmentDetails.potentialLostInv} </td>
                                            <td>Re-Order Level</td>
                                            <td> {assortmentDetails.reOrderLevel == "" || assortmentDetails.reOrderLevel == "-"  || assortmentDetails.reOrderLevel == null ? 0: assortmentDetails.reOrderLevel} </td>
                                        </tr>
                                        <tr>
                                            <td>Warehouse Stock</td>
                                            <td> {assortmentDetails.warehouseStock == "" || assortmentDetails.warehouseStock == "-"  || assortmentDetails.warehouseStock == null ? 0: assortmentDetails.warehouseStock} </td>
                                            <td>Potential Lost Value</td>
                                            <td> {assortmentDetails.potentialLostValue == "" || assortmentDetails.potentialLostValue == "-"  || assortmentDetails.potentialLostValue == null ? 0: assortmentDetails.potentialLostValue} </td>
                                        </tr>
                                    </tbody>
                                </table>}
                            </div>
                            <div role="tabpanel" className="tab-pane fade pwstab-content p-lr-47" id="pwstimephased">
                                <div className="arsPwq-data arsPws-data">
                                    <div className="ars-data-head m-top-20">
                                        <div className="ars-data-head-search-bar">
                                            <input type="search" value={this.state.timePhasedSearchText} onChange={(e) => this.onSearchTimePhase(e)} onKeyDown={(e)=> {e.persist(); if(e.keyCode == 13) this.onSearchTimePhase(e)}} placeholder="Type To Search"/>
                                            <img className="search-image" src={Search}></img>
                                            {this.state.timePhasedSearchText != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.onSearchClearTimePhase} /></span> : null}
                                        </div>
                                        <div className="adh-right">
                                            <div className="adhr-download">
                                                <button type="button" onClick={() => this.xlscsv()}>Download Report 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13.08" viewBox="0 0 15.583 17">
                                                        <path d="M7.792 14.875L2.125 8.5h4.25V0h2.833v8.5h4.25zm6.375-.708v1.417H1.417v-1.417H0V17h15.583v-2.833z"/>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="expand-new-table m-top-20">
                                        <div className="manage-table">
                                            <ColoumSetting {...this.props} {...this.state}
                                                handleDragStart={this.handleDragStart}
                                                handleDragEnter={this.handleDragEnter}
                                                onHeadersTabClick={this.onHeadersTabClick}
                                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                                closeColumn={(e) => this.closeColumn(e)}
                                                saveMainState={this.state.saveMainState}
                                                tabVal={this.state.tabVal}
                                                pushColumnData={this.pushColumnData}
                                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                                isReportPageFlag={this.state.isReportPageFlag}
                                            />
                                            <table className="table gen-main-table">
                                                <thead>
                                                    <tr>
                                                    {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                                            <label data-key={data}>{data}</label>
                                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                                        </th>
                                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                                            <label data-key={data}>{data}</label>
                                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                                        </th>
                                                    ))}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {assortmentTimePhased.length != 0 ? assortmentTimePhased.map((data, key) => (
                                                    <React.Fragment key={key}>
                                                    <tr>
                                                        {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                            <td key={key} ><label>{data[hdata]}</label>
                                                                {this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                                                            </td>
                                                        )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                            <td key={keyy} ><label className="table-td-text">{data[sdata]}</label>
                                                                {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                                            </td>
                                                        ))}
                                                    </tr>
                                                    </React.Fragment>
                                                    )) : <tr><td colSpan="100"><label>NO DATA FOUND</label></td></tr>}
                                                </tbody>
                                                {/* <thead>
                                                    <tr>
                                                        <th><label>Time Phased</label> <img src={filterIcon} className="imgHead" data-key="Time Phased"/></th>
                                                        <th><label>Opening Stock</label> <img src={filterIcon} className="imgHead" data-key="Opening Stock"/></th>
                                                        <th><label>Actual Sales</label> <img src={filterIcon} className="imgHead" data-key="Actual Sales"/></th>
                                                        <th><label>Sales Return</label> <img src={filterIcon} className="imgHead" data-key="Sales Return"/></th>
                                                        <th><label>Inward</label> <img src={filterIcon} className="imgHead" data-key="Inward"/></th>
                                                        <th><label>Closing Stock</label> <img src={filterIcon} className="imgHead" data-key="Closing Stock"/></th>
                                                    </tr>
                                                </thead> */}
                                                {/* <tbody>
                                                    {assortmentTimePhased.length ? assortmentTimePhased.map(item => (<tr>
                                                        <td><label> {item.monthYear != null ? item.monthYear : "NA"} </label></td>
                                                        <td><label> {item.openingStock != null ? item.openingStock : 0} </label></td>
                                                        <td><label> {item.actualSales != null ? item.actualSales : 0} </label></td>
                                                        <td><label> {item.salesReturns != null ? item.salesReturns : 0} </label></td>
                                                        <td><label> {item.inward != null ? item.inward : 0} </label></td>
                                                        <td><label> {item.closingStock != null ? item.closingStock : 0} </label></td>
                                                    </tr>))
                                                    : <tr><td colSpan="100"><label>NO DATA FOUND</label></td></tr>}
                                                </tbody> */}
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPageTimePhased} min="1" onKeyPress={this.getAnyTimePhasedPage} onChange={this.getAnyTimePhasedPage} value={this.state.jumpPageTimePhased} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItemsTimePhased}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={this.timePhasedPage}
                                                        prev={this.state.prevTimePhased} current={this.state.currentTimePhased} maxPage={this.state.maxPageTimePhased} next={this.state.nextTimePhased} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" className="tab-pane fade pwstab-content p-lr-47" id="pwsallocation">
                                <div className="arsPwq-data arsPws-data">
                                    <div className="ars-data-head m-top-20">
                                        <div className="ars-data-head-search-bar">
                                            <input type="search" value={this.state.allocationSearchText} onChange={(e) => this.onSearchAllocation(e)} onKeyDown={(e)=> {e.persist(); if(e.keyCode == 13) this.onSearchAllocation(e)}} placeholder="Type To Search"/>
                                            <img className="search-image" src={Search}></img>
                                            {this.state.allocationSearchText != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.onSearchClearAllocation} /></span> : null}
                                        </div>
                                        <div className="adh-right">
                                            <div className="adhr-download">
                                                <button type="button" onClick={() => this.xlscsv()}>Download Report 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13.08" viewBox="0 0 15.583 17">
                                                        <path d="M7.792 14.875L2.125 8.5h4.25V0h2.833v8.5h4.25zm6.375-.708v1.417H1.417v-1.417H0V17h15.583v-2.833z"/>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="expand-new-table m-top-20">
                                        <div className="manage-table">
                                            <ColoumSetting {...this.props} {...this.state}
                                                handleDragStart={this.handleDragStart}
                                                handleDragEnter={this.handleDragEnter}
                                                onHeadersTabClick={this.onHeadersTabClick}
                                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                                closeColumn={(e) => this.closeColumn(e)}
                                                saveMainState={this.state.saveMainState}
                                                tabVal={this.state.tabVal}
                                                pushColumnData={this.pushColumnData}
                                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                                isReportPageFlag={this.state.isReportPageFlag}
                                            />
                                            <table className="table gen-main-table">
                                                <thead>
                                                    <tr>
                                                    {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                                            <label data-key={data}>{data}</label>
                                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                                        </th>
                                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                                            <label data-key={data}>{data}</label>
                                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                                        </th>
                                                    ))}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {allocationData.length != 0 ? allocationData.map((data, key) => (
                                                    <React.Fragment key={key}>
                                                    <tr>
                                                        {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                            <td key={key} ><label>{data[hdata]}</label>
                                                                {this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                                                            </td>
                                                        )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                            <td key={keyy} ><label className="table-td-text">{data[sdata]}</label>
                                                                {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                                            </td>
                                                        ))}
                                                    </tr>
                                                    </React.Fragment>
                                                    )) : <tr><td colSpan="100"><label>NO DATA FOUND</label></td></tr>}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPageAllocation} min="1" onKeyPress={this.getAnyAllocationPage} onChange={this.getAnyAllocationPage} value={this.state.jumpPageAllocation} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItemsAllocation}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={this.allocationPage}
                                                        prev={this.state.prevAllocation} current={this.state.currentAllocation} maxPage={this.state.maxPageAllocation} next={this.state.nextAllocation} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg} closeToastError={this.closeToastError} />}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        assortmentDetails: state.analytics.assortmentDetails,
        assortmentItems: state.analytics.assortmentItems,
        assortmentSalesHistory: state.analytics.assortmentSalesHistory,
        // assortmentTimePhased: state.analytics.assortmentTimePhased,
        reviewReason: state.analytics.reviewReason.data
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PlannerWorkSheet)