import React from 'react';
import exclaimIcon from '../../assets/exclain.svg';

export const BarcodeStoreModal = () => {
    return (
        <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
            <div className=" display_block">
                <div className="modal-content vendorEditModalContent modalShow adHocModal otbModalMain barcodeSellModal customDateMain pad-0">
                    <div className="posRelative">
                        <div className="col-md-12 m0 pad-0 displayInline">
                            <div className="modalTop displayInline">
                                <div className="col-md-12 alignMiddle pad-lft-0">
                                    <div className="col-md-7 pad-0">
                                        <h2 className="fontBold">Barcode Sell Thru</h2>
                                    </div>
                                    <div className="col-md-5 pad-0">
                                        <div className="textRight">
                                            <button type="button" className="clearBtnGeneric m-rgt-15">Cancel</button>
                                            <button type="button" className="saveBtnGeneric">Save</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-5 pad-lft-0 barcodeSearch displayBlock m-top-20">
                                    <input type="search" id="search" className="search-box width100 " placeholder="Search by store name…" autoComplete="off" />
                                    {/* <div className="searchDropDown">
                                        <ul className="pad-0">
                                            <li className="active">SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                            <li>SPNARV666698</li>
                                        </ul>
                                    </div> */}
                                </div>
                            </div>
                            <div className="chooseAmong">
                                <h4>Choose among </h4>
                                <div className="m-top-20">
                                    <button type="button" className="storeBtn active m-rgt-20">Top 10 Stores</button>
                                    <button type="button" className="storeBtn">Bottom 10 Stores</button>
                                    <p className="m-top-35">Showing top 10 Stores for Barcode Sell Thru</p>
                                    <div className="storeNames">
                                        <ul className="list-inline">
                                            <li className="active">SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                            <li>SNM BD009</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    );
}