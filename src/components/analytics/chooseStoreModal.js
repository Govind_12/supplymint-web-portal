import React from 'react';
import FilterLoader from '../loaders/filterLoader';
class ChooseStoreModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchStoreValue: "",
      storeProfileData: [],
      loader: false,
      dropDownShow: false,
      searchUserModal : ""

    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.analytics.getStoreData.isSuccess) {
      this.setState({
        storeProfileData: nextProps.analytics.getStoreData.data.resource,
        loader: false
      })
    } else if (nextProps.analytics.getStoreData.isLoading) {
      this.setState({
        loader: true
      })
    }


  }

  handleChange(e) {
    this.setState({
      searchStoreValue: e.target.value,
      searchUserModal : e.target.value
    })
    this.onSearchFunction()

  }
  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.onDoneStore();
    }
  }
  onnChange(key , value) {
    this.setState({
      dropDownShow: false,
      searchStoreValue: key[0],
      searchUserModal: key[0] +"-"+value[0]
    })

  }
  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }
  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }
  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside = (event) => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        dropDownShow: false
      })
    }
  }

  onDoneStore() {
    if (this.state.searchStoreValue != "") {
      let payload = {
        storeCode: this.state.searchStoreValue
      }
      this.props.getStoreProfileCodeRequest(payload)
      this.props.closeStoreModal()
    }

  }

  onSearchFunction() {
    var input = document.getElementById("storeProfile");
    var filter = input.value.toUpperCase();
    var ul = document.getElementById("myUL");
    var li = ul.getElementsByTagName("li");
    for (var i = 0; i < li.length; i++) {
      var a = li[i].getElementsByTagName("label")[0];
      var txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].classList.remove('hideElement');
      } else {
        li[i].classList.add('hideElement');
      }
    }
    this.displayNoResult(li.length)
  }

  displayNoResult(allLI) {
    var hiddenLILength = document.querySelectorAll('li.hideElement');
    if (allLI === hiddenLILength.length) {
      document.getElementById('noResult').classList.remove('hideElement')
    } else {
      document.getElementById('noResult').classList.add('hideElement')
    }
  }

  render() {
    const { searchStoreValue } = this.state

    // var result = _.filter(this.state.storeProfileData, function (data) {

    //     return _.startsWith(Object.values(data), searchStoreValue.toUpperCase()) || _.startsWith(String.prototype.toLowerCase.apply(Object.values(data)), searchStoreValue.toLowerCase()
    //     )
    //   });
    return (
      <div className="modal  display_block" id="editVendorModal">
        <div className="backdrop display_block"></div>

        <div className=" display_block">
          <div className="modal-content vendorEditModalContent modalShow adHocModal searchStoreProfileMain chooseStoreModal pad-0">
            <div className="middleContent m-top-25" ref={(e) => this.setWrapperRef(e)}>
              <h1>Choose Store</h1>
              <h2>Select Store from below to view complete profile details</h2>
              <input type="search" id="storeProfile" placeholder="Search Store" value={this.state.searchUserModal} onKeyPress={(e) => this._handleKeyPress(e)} onChange={(e) => this.handleChange(e)} onFocus={() => this.setState({ dropDownShow: true })} />
              {this.state.dropDownShow ? <div className="storeSearchDrop m-top-5">
                <ul id="myUL">
                  {this.state.storeProfileData.length != 0 ? this.state.storeProfileData.map((data, key) => (
                    <li key={key} onClick={(e) => this.onnChange(Object.keys(data) , Object.values(data))}><label>{Object.keys(data) + "-" + Object.values(data)}</label></li>

                  )) : <li ><label>No Data Found</label></li>}
                  <li className="hideElement" id="noResult">
                    <label> No Data Found</label>
                  </li>
                </ul>
              </div> : null}

              <button className="record_btn" onClick={() => this.onDoneStore()}>Load Profile</button>
              <button type="button" className="cancelBtn" onClick={() => this.props.closeStoreModal()}>Cancel</button>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default ChooseStoreModal;