import React from 'react';
import Search from '../../assets/search2.svg';
import filterIcon from '../../assets/headerFilter.svg';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import ToastLoader from "../loaders/toastLoader";
import ColoumSetting from '../replenishment/coloumSetting';
import ConfirmationSummaryModal from '../replenishment/confirmationReset';
import Pagination from '../pagination';
import searchIcon from '../../assets/clearSearch.svg';
import ArsFilterModal from './arsFilterModal';

class PlannerWorkQueue2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rightbar: false,
            openRightBar: false,
            loader: false,
            errorMessage: "",
            errorCode: "",
            code: "",
            successMessage: "",
            success: false,
            alert: false,
            selectOption: "Select Reason",
            openOption: false,
            reviewReason: [],
            arsPlannerData: {},

            arsArray: [],
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            selectedItems: 0,
            jumpPage: 1,

            arsFilter: {},
            filterOption: {
                division: [],
                section: [],
                priceBand: [],
                storecode: [],
                storeName: [],
                department:[],
                category: [],
                assortment: [],
            },

            searchText: "",
            toastLoader: false,
            toastMsg: "",
            expanded: false,
            showFilters: false,

            isReportPageFlag: true, //For Hiding Set/Item Header in Column Setting::
            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "REVIEW_REASON",
                basedOn: "ALL"
            },

            filterItems: {},
            mainCustomHeaders: [],
            getMainHeaderConfig: [],
            mainAvailableHeaders: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderConfigState: {}, 
            mainFixedHeaderData: [],
            mainHeaderConfigDataState: {},
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mandateHeaderMain: [],
            headerCondition: false,
            coloumSetting: false,
            dragOn: false,
            changesInMainHeaders: false,
            saveMainState: [],
            headerConfigDataState: {},
            confirmModal: false,
            headerMsg: "",
            tabVal: "1",

            selectStoreLocation: false,
            selectDivision: false,
            selectSection: false,
            selectStoreName: false,
            selectDepartment: false,
            selectMrpRange: false,
            selectCategory: false,

            searchStoreLocFilter: "",
            searchDivisionFilter: "",
            searchSectionFilter: "",
            searchStoreNameFilter: "",
            searchDepartmentFilter: "",
            searchMrpFilter: "",
            searchCategoryFilter: "",
            filterData: [],
            openStoreLocFilter: false,
            openDivisionFilter: false,
            openSectionFilter: false,
            openStoreNameFilter: false,
            openDepartmentFilter: false,
            openMrpFilter: false,
            openCategoryFilter: false,

            sortedIn: "",
            sortedBy: "",
            prevFilter: "",

            appliedFilterCount: 0,
        }
    }

    componentDidMount() {
        if(this.props.reviewReason && this.props.reviewReason.data && this.props.reviewReason.data.resource && this.props.reviewReason.data.resource.response.length > 0) {
            this.setState({
                reviewReason: this.props.reviewReason.data.resource.response,
                selectOption: sessionStorage.getItem("reason")
            },()=>{
                this.getAssortmentData(1);
            })
        }else{
            this.props.getReviewReasonRequest('asd');
        }
        // this.props.getReviewReasonRequest('asd');
        document.addEventListener("keydown", this.escFunction, false);
        sessionStorage.setItem('currentPage', "RDAANAMAIN")
        sessionStorage.setItem('currentPageName', "Review Reason Analysis")

        this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }

    componentWillReceiveProps(nextProps) {
        // console.log("nextProps: ", nextProps,nextProps.reviewReason, nextProps.reviewReason.data, nextProps.reviewReason.data.resource,  nextProps.reviewReason.data.resource.length > 0)
        if(nextProps.analytics.reviewReason.isSuccess) {
            this.setState({
                reviewReason: nextProps.reviewReason.data.resource.response == null ? [] : nextProps.reviewReason.data.resource.response 
            })
        }
        if(nextProps.analytics.arsPwqData.isSuccess) {
            this.setState({
                arsArray: nextProps.arsPlannerData.data.resource.response == null ? [] : nextProps.arsPlannerData.data.resource.response, 
                prev: nextProps.arsPlannerData.data.resource.prevPage,
                current: nextProps.arsPlannerData.data.resource.currPage,
                next: nextProps.arsPlannerData.data.resource.currPage + 1,
                maxPage: nextProps.arsPlannerData.data.resource.maxPage,
                selectedItems: nextProps.arsPlannerData.data.resource.totalSearchReasult == undefined || nextProps.arsPlannerData.data.resource.totalSearchReasult == null 
                               ? 0 : nextProps.arsPlannerData.data.resource.totalSearchReasult,
                jumpPage: nextProps.arsPlannerData.data.resource.currPage
            })
        } 
        if(nextProps.arsFilter && nextProps.arsFilter.data &&  nextProps.arsFilter.data.resource && nextProps.arsFilter.data.resource.response && Object.keys(nextProps.arsFilter.data.resource.response).length > 0) {
            this.setState({
                arsFilter: nextProps.arsFilter.data.resource.response
            })
        }
        if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
                nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
                let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
                let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
                let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
                Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
                this.setState({
                    filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
                    mainCustomHeaders: this.state.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
                    mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
                    mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
                });
            }
            this.props.getMainHeaderConfigClear();
        }
        if (this.props.replenishment.createMainHeaderConfig.isSuccess && this.props.replenishment.createMainHeaderConfig.data.basedOn == "ALL") {
            this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            this.props.createMainHeaderConfigClear();
        }
    }

    selectReviewOption =(e)=> {
        this.setState({ selectOption: e.target.closest("li").getAttribute("value"),
                        openOption: false,
                        searchText: ""
            }, ()=>{
            //this.props.getDataWithoutFilterClear();
            this.getAssortmentData(1);
        })
    }

    openCloseSelectOption =()=> {
        this.setState({openOption: !this.state.openOption});
    }

    getAssortmentData(page) {
        let appliedFilterCount = 0;
        if(this.state.filterOption.division.length)
           appliedFilterCount++;
        if(this.state.filterOption.section.length)
           appliedFilterCount++;
        if(this.state.filterOption.priceBand.length)
           appliedFilterCount++;
        if(this.state.filterOption.storecode.length)
           appliedFilterCount++;
        if(this.state.filterOption.storeName.length)
           appliedFilterCount++;  
        if(this.state.filterOption.department.length)
           appliedFilterCount++;
        if(this.state.filterOption.category.length)
           appliedFilterCount++;

        let storeCode = [];   
        if(this.state.filterOption.storecode.length){
            storeCode = this.state.filterOption.storecode.map( data => data = data.split(" ")[0])
        }     

        this.setState({
            selectOption: this.state.selectOption == "Select Reason" ? "" : this.state.selectOption,
            appliedFilterCount,
            showFilters: false,
        },()=>{
            if(this.state.selectOption != "" && !Object.keys(this.state.filterOption).some((k) => this.state.filterOption[k].toString() !== "") && this.state.searchText == "") {
                this.props.getDataWithoutFilterRequest({
                    selectOption: this.state.selectOption, 
                    filterOption: {
                        storeCode: storeCode,
                        storeName:  this.state.filterOption.storeName,
                        division: this.state.filterOption.division,
                        section: this.state.filterOption.section,
                        department: this.state.filterOption.department,
                        priceBand: this.state.filterOption.priceBand,
                        category: this.state.filterOption.category,
                    }, 
                    type: 1, 
                    page: page, 
                    searchText: this.state.searchText, 
                    sortedIn: this.state.sortedIn, 
                    sortedBy: this.state.sortedBy
                });
                this.props.getfiltersArsDataRequest({reason: this.state.selectOption});
            } 
            else if(this.state.selectOption != "" &&  Object.keys(this.state.filterOption).some((k) => this.state.filterOption[k].toString() !== "") && this.state.searchText == "") {
                this.props.getDataWithoutFilterRequest({
                    selectOption: this.state.selectOption, 
                    filterOption: {
                        storeCode: storeCode,
                        storeName:  this.state.filterOption.storeName,
                        division: this.state.filterOption.division,
                        section: this.state.filterOption.section,
                        department: this.state.filterOption.department,
                        priceBand: this.state.filterOption.priceBand,
                        category: this.state.filterOption.category,
                    }, 
                    type: 2, 
                    page: page, 
                    searchText: this.state.searchText, 
                    sortedIn: this.state.sortedIn, 
                    sortedBy: this.state.sortedBy
                });
            } else if(this.state.selectOption != "" && !Object.keys(this.state.filterOption).some((k) => this.state.filterOption[k].toString() !== "") && this.state.searchText != "") {
                this.props.getDataWithoutFilterRequest({
                    selectOption: this.state.selectOption, 
                    filterOption: {
                        storeCode: storeCode,
                        storeName:  this.state.filterOption.storeName,
                        division: this.state.filterOption.division,
                        section: this.state.filterOption.section,
                        department: this.state.filterOption.department,
                        priceBand: this.state.filterOption.priceBand,
                        category: this.state.filterOption.category,
                    }, 
                    type: 4, 
                    page: page, 
                    searchText: this.state.searchText, 
                    sortedIn: this.state.sortedIn, 
                    sortedBy: this.state.sortedBy
                });
                this.props.getfiltersArsDataRequest({reason: this.state.selectOption});
            } 
            else if(this.state.selectOption != "" &&  Object.keys(this.state.filterOption).some((k) => this.state.filterOption[k].toString() !== "") && this.state.searchText != "") {
                this.props.getDataWithoutFilterRequest({
                    selectOption: this.state.selectOption, 
                    filterOption: {
                        storeCode: storeCode,
                        storeName:  this.state.filterOption.storeName,
                        division: this.state.filterOption.division,
                        section: this.state.filterOption.section,
                        department: this.state.filterOption.department,
                        priceBand: this.state.filterOption.priceBand,
                        category: this.state.filterOption.category,
                    }, 
                    type: 5, 
                    page: page, 
                    searchText: this.state.searchText, 
                    sortedIn: this.state.sortedIn, 
                    sortedBy: this.state.sortedBy
                });
            }
        })   
    }
        
    getAssortmentDetails =(id, data)=> {
        let finalId = ((this.state.current - 1)*10 + id)
        sessionStorage.setItem("storeCode", this.state.filterOption.storecode);
        sessionStorage.setItem("storeName", this.state.filterOption.storeName);
        sessionStorage.setItem("division", this.state.filterOption.division);
        sessionStorage.setItem("section", this.state.filterOption.section);
        sessionStorage.setItem("department", this.state.filterOption.department);
        sessionStorage.setItem("priceBand", this.state.filterOption.priceBand);
        sessionStorage.setItem("category", this.state.filterOption.category);
        sessionStorage.setItem("assortmentIndex", finalId);
        sessionStorage.setItem("reason", this.state.selectOption);
        sessionStorage.setItem("totalAssortment", this.state.selectedItems);
        sessionStorage.setItem("assortmentArray", this.state.arsFilter.assortment)
        sessionStorage.setItem("currentStoreCode", data.storecode);
      this.props.history.push('/analytics/plannerWorkSheet'); 
    }

    page =(e)=> {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.arsPlannerData.data.resource.prevPage,
                    current: this.props.arsPlannerData.data.resource.currPage,
                    next: this.props.arsPlannerData.data.resource.currPage + 1,
                    maxPage: this.props.arsPlannerData.data.resource.maxPage,
                })
                if (this.props.arsPlannerData.data.resource.currPage != 0) {
                    this.getAssortmentData(this.props.arsPlannerData.data.resource.currPage - 1);
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.arsPlannerData.data.resource.prevPage,
                current: this.props.arsPlannerData.data.resource.currPage,
                next: this.props.arsPlannerData.data.resource.currPage + 1,
                maxPage: this.props.arsPlannerData.data.resource.maxPage,
            })
            if (this.props.arsPlannerData.data.resource.currPage != this.props.arsPlannerData.data.resource.maxPage) {
                this.getAssortmentData(this.props.arsPlannerData.data.resource.currPage + 1);
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.arsPlannerData.data.resource.prevPage,
                    current: this.props.arsPlannerData.data.resource.currPage,
                    next: this.props.arsPlannerData.data.resource.currPage + 1,
                    maxPage: this.props.arsPlannerData.data.resource.maxPage,
                })
                if (this.props.arsPlannerData.data.resource.currPage <= this.props.arsPlannerData.data.resource.maxPage) {
                    this.getAssortmentData(1)
                }
            }

        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.arsPlannerData.data.resource.prevPage,
                    current: this.props.arsPlannerData.data.resource.currPage,
                    next: this.props.arsPlannerData.data.resource.currPage + 1,
                    maxPage: this.props.arsPlannerData.data.resource.maxPage,
                })
                if (this.props.arsPlannerData.data.resource.currPage <= this.props.arsPlannerData.data.resource.maxPage) {
                    this.getAssortmentData(this.props.arsPlannerData.data.resource.maxPage)
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    this.getAssortmentData(_.target.value)
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.key == 'Enter') {
                this.getAssortmentData(1);
            }
        }
        this.setState({ searchText: e.target.value }, () => {
            if (this.state.searchText == "" ) {
                this.getAssortmentData(1);
            }
        })
    }

    searchClear =()=> {
        this.setState({ searchText: "" }, () => {
                this.getAssortmentData(1);
        })
    }

    //-------------------------------- Review Reason Dynamic Header -------------------------------//
    openColoumSetting =(data)=> {
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
           this.setState({ coloumSetting: false }, () =>
               document.removeEventListener('click', this.closeColumnSetting))
       } 
    }

    handleDragStart =(e, key, dragItem, dragNode)=> {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({ dragOn:true })
    }
    handleDragEnd =(dragItem, dragNode)=> {
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({ dragOn:false})
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter =(e, key, dragItem, dragNode)=> {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true,  
                            mainCustomHeaders: configheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                        .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val =JSON.parse(JSON.stringify(swapCustomHeaderData,newCustomList))
                        let customheaderData = Object.keys(val)
                        .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { 
                            changesInMainHeaders: true, 
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList
                        }
                    })
                }
            }
        }
    }
    pushColumnData =(e,data)=> {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
               if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
                headerConfigDataState: customHeadersState,
            })
        }
    }
    closeColumn =(data)=> {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
    }
    saveColumnSetting =(e)=> {
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            let payload = {
                basedOn: "ALL",
                module: "ARS",
                subModule: "ANALYTICS",
                section: "REVIEW-REASON-ANALYSIS",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "REVIEW_REASON",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainCustomHeaders,
            }
            this.props.createMainHeaderConfigRequest(payload)
        }
    }
    resetColumnConfirmation =()=> {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            confirmModal: true,
        })
    }
    resetColumn =()=> {
        const {getMainHeaderConfig,mainFixedHeader} = this.state
        if (this.state.tabVal == 1) {
            let payload = {
                basedOn: "ALL",
                module: "ARS",
                subModule: "ANALYTICS",
                section: "REVIEW-REASON-ANALYSIS",
                source: "WEB-APP",
                typeConfig: "PORTAL",
                attributeType: "TABLE HEADER",
                displayName: "REVIEW_REASON",
                fixedHeaders: this.state.mainFixedHeaderData,
                defaultHeaders: this.state.mainHeaderConfigDataState,
                customHeaders: this.state.mainHeaderConfigDataState,
            }
            this.props.createMainHeaderConfigRequest(payload)
            let availableHeaders= mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders:availableHeaders
            })    
        }
    }
    closeConfirmModal =(e)=> {
        this.setState({ confirmModal: !this.state.confirmModal,})
    }
    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1) {
                this.openColoumSetting("true")
                this.props.getMainHeaderConfigRequest(this.state.mainHeaderPayload)
            }
        })
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.setState({ coloumSetting: false, headerCondition: false, confirmModal: false})
        }
    }

    small = (str) => {
        if (str != null) {
            var str = str.toString()
            if (str.length <= 25) {
                return false;
            }
            return true;
        }
    }

    storeLocationData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectStoreLocation: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectStoreLocation: false })
        }
    }
    divisionData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectDivision: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectDivision: false })
        }
    }
    sectionData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectSection: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectSection: false })
        }
    }
    storeNameData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectStoreName: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectStoreName: false })
        }
    }
    departmentData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectDepartment: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectDepartment: false })
        }
    }
    mrpRangeData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectMrpRange: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectMrpRange: false })
        }
    }
    categoryData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectCategory: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectCategory: false })
        }
    }

    //-------------------------------------- Search Filter ---------------------------------//
    openFilter =(e)=> {
        e.preventDefault();
        this.setState({
            showFilters: !this.state.showFilters
        });
    }

    openArsFilter =(e, filterData, searchFilterVariable, flag)=>{
        this.onCloseArsFilter(e)
        this.setState({ 
                filterData: filterData,
                //[searchFilterVariable]: "",
                [flag]: true,
        },()=>{
              document.addEventListener('click', this.onClickCloseArsFilter)
        })
    }

    onCloseArsFilter =(e)=>{
       this.setState({ 
            openStoreLocFilter: false,
            openDivisionFilter: false,
            openSectionFilter: false,
            openStoreNameFilter: false,
            openDepartmentFilter: false,
            openMrpFilter: false,
            openCategoryFilter: false,
        },()=>{
            document.removeEventListener('click', this.onClickCloseArsFilter)
       })
    }

    onClickCloseArsFilter =(e)=>{
        if( e !== undefined && e.target !== null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop")){
            this.setState({
                openStoreLocFilter: false,
                openDivisionFilter: false,
                openSectionFilter: false,
                openStoreNameFilter: false,
                openDepartmentFilter: false,
                openMrpFilter: false,
                openCategoryFilter: false,
            },()=>{
                document.removeEventListener('click', this.onClickCloseArsFilter)
            })
        }
    }

    onSearchFilter =(e, filterDataVal, searchFilterVariable, modalOpenFlag) => {
        this.setState({ 
            [searchFilterVariable]: e.target.value,
            [modalOpenFlag]: false,
        }, () => {
            if( this.state[searchFilterVariable].trim().length > 0){
               let searchedFilterData = filterDataVal && filterDataVal.filter( data => data.includes(this.state[searchFilterVariable].trim()))
               this.setState({
                    filterData : searchedFilterData,
                    [modalOpenFlag]: true,
               })
            }
        })
    }

    onSearchFilterClear =(e, filterData, searchFilterVariable, modalOpenFlag, filterTypeArray)=> {
       this.state.filterOption[filterTypeArray] = []
       this.setState({
           filterData:  filterData,
           [searchFilterVariable]: "",
          // [modalOpenFlag]: false
       },()=>{
          this.getAssortmentData(1);
       })
    }

    handleChange =(e, key)=> {
        e.persist();
        let value = e.target.closest("li").getAttribute("value");
        const objectFilterOption = this.state.filterOption;
        if(objectFilterOption[key].indexOf(value) == -1) {
            objectFilterOption[key].push(value);
            this.setState({
                filterOption: objectFilterOption
            })
        } else {
            objectFilterOption[key].splice(objectFilterOption[key].indexOf(value), 1);
            this.setState({
                filterOption: objectFilterOption
            })
        }
    }

    applyFilter =(e)=>{
        this.getAssortmentData(1);
    }

    removeFilter =(e)=> {
        e.persist();
        const objectFilterOption = this.state.filterOption;
        Object.keys(objectFilterOption).forEach(v => objectFilterOption[v] = []);
        this.setState({
            filterOption: objectFilterOption
        }, () => {
            this.getAssortmentData(1);
        })
    }

    filterHeader = (event) => {
        var data = event.target.dataset.key
        if( event.target.closest("th").classList.contains("rotate180"))
            event.target.closest("th").classList.remove("rotate180")
        else
            event.target.closest("th").classList.add("rotate180")  
        var def = {...this.state.mainHeaderConfigDataState};
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ sortedBy: filterKey, sortedIn: this.state.sortedIn == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ sortedBy: filterKey, sortedIn: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            this.getAssortmentData(this.state.current)
        })
    }

    //-----------------------------------------------------------------------------------------------//

    render(){
        const { reviewReason, arsArray, arsFilter, filterOption, assortmentDetailPagination, arsPlannerDataMessage,appliedFilterCount } =this.state;
        return(
            <div className="container-fluid pad-0">   
                <div className="col-lg-12 pad-0">
                    <div className="otb-reports-filter planner-work-queue-filter p-lr-47">
                        <div className="orf-head">
                            <div className="orfh-left">
                                <div className="orfhl-inner">
                                    <h3>Filters</h3> 
                                    {this.state.showFilters === false && <span>{appliedFilterCount} filters applied</span>}
                                </div>
                            </div>
                            <div className="orfh-rgt">
                                { arsArray.length || appliedFilterCount > 0 ? <button className={this.state.showFilters === false ? "" : "orfh-btn"} onClick={(e) => this.openFilter(e)}>
                                    <img src={require('../../assets/down-arrow-new.svg')} />
                                </button> :
                                <button className={this.state.showFilters === false ? "" : "orfh-btn"}>
                                    <img src={require('../../assets/down-arrow-new.svg')} />
                                </button>}
                            </div>
                        </div>
                        {this.state.showFilters && <div className="orf-body m-top-10">
                            <form>
                                <div className="col-lg-12 pad-0">
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Store Location</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.storecode.length ? filterOption.storecode : this.state.searchStoreLocFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.storeCode, "searchStoreLocFilter", "openStoreLocFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.storeCode, "searchStoreLocFilter", "openStoreLocFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.storeCode, "searchStoreLocFilter", "openStoreLocFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchStoreLocFilter != "" || filterOption.storecode.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.storeCode, "searchStoreLocFilter", "openStoreLocFilter", "storecode")} /></span> : null}
                                            {this.state.openStoreLocFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="storecode" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                        {/* <select className="select-checkbox"  multiple={true} value={filterOption.storecode} onChange={e => {this.handlechange(e, "storecode")}}>
                                            {Object.keys(arsFilter).length > 0 && arsFilter.storeCode && arsFilter.storeCode.length>0 && arsFilter.storeCode.map(i => (
                                                <option className={`csd-option ${this.state.openOption ? 'display_visible':''}`} value={i} key={i}> {i} </option>
                                            )) }
                                        </select> */}
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Division</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.division.length ? filterOption.division : this.state.searchDivisionFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.division, "searchDivisionFilter", "openDivisionFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.division, "searchDivisionFilter", "openDivisionFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.division, "searchDivisionFilter", "openDivisionFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchDivisionFilter != "" || filterOption.division.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.division, "searchDivisionFilter", "openDivisionFilter", "division")} /></span> : null}
                                            {this.state.openDivisionFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="division" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Section</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.section.length ? filterOption.section : this.state.searchSectionFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.section, "searchSectionFilter", "openSectionFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.section, "searchSectionFilter", "openSectionFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.section, "searchSectionFilter", "openSectionFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchSectionFilter != "" || filterOption.section.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.section, "searchSectionFilter", "openSectionFilter", "section")} /></span> : null}
                                            {this.state.openSectionFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="section" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Store Name</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.storeName.length ? filterOption.storeName : this.state.searchStoreNameFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.storeName, "searchStoreNameFilter", "openStoreNameFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.storeName, "searchStoreNameFilter", "openStoreNameFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.storeName, "searchStoreNameFilter", "openStoreNameFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchStoreNameFilter != "" || filterOption.storeName.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.storeName, "searchStoreNameFilter", "openStoreNameFilter", "storeName")} /></span> : null}
                                            {this.state.openStoreNameFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="storeName" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>Department</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.department.length ? filterOption.department : this.state.searchDepartmentFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.department, "searchDepartmentFilter", "openDepartmentFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.department, "searchDepartmentFilter", "openDepartmentFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.department, "searchDepartmentFilter", "openDepartmentFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchDepartmentFilter != "" || filterOption.department.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.department, "searchDepartmentFilter", "openDepartmentFilter", "department")} /></span> : null}
                                            {this.state.openDepartmentFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="department" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>MRP Range</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.priceBand.length ? filterOption.priceBand : this.state.searchMrpFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.priceBand, "searchMrpFilter", "openMrpFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.priceBand, "searchMrpFilter", "openMrpFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.priceBand, "searchMrpFilter", "openMrpFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchMrpFilter != "" || filterOption.priceBand.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.priceBand, "searchMrpFilter", "openMrpFilter", "priceBand")} /></span> : null}
                                            {this.state.openMrpFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="priceBand" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15 m-top-20">
                                        <label>Category</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" value={filterOption.category.length ? filterOption.category : this.state.searchCategoryFilter} className="onFocus pnl-purchase-input" onClick={(e)=>this.openArsFilter(e, arsFilter.category, "searchCategoryFilter", "openCategoryFilter")} onChange={(e)=>this.onSearchFilter(e, arsFilter.category, "searchCategoryFilter", "openCategoryFilter")} placeholder="Type To Search" />
                                            <span className="modal-search-btn" id="search" onClick={(e)=>this.openArsFilter(e, arsFilter.category, "searchCategoryFilter", "openCategoryFilter")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.searchCategoryFilter != "" || filterOption.category.length ? <span className="closeSearch"><img src={searchIcon} onClick={(e)=>this.onSearchFilterClear(e, arsFilter.category, "searchCategoryFilter", "openCategoryFilter", "category")} /></span> : null}
                                            {this.state.openCategoryFilter && this.state.filterData && <ArsFilterModal filterData={this.state.filterData} onCloseArsFilter={this.onCloseArsFilter} filterType="category" checkedData={this.state.filterOption} handleChange={this.handleChange}/>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0 m-top-20">
                                    <div className="orfb-apply-btn">
                                        <button type="button" className="orfbab-apply" onClick={this.applyFilter}>Apply Filter</button>
                                        <button type="button" className="" onClick={(e) => this.removeFilter(e)}>Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>}
                    </div>
                </div>
                <div className="col-lg-12 col-sm-12 col-md-12 pad-0 m-top-10">
                    <div className="planner-work-queue-head p-lr-47">
                        <div className="pwqh-left">
                            <div className="pwq-review-board">
                                <label>Choose Review Board</label>
                                <button type="button" onClick={this.openCloseSelectOption} className="csd-area">{this.state.selectOption} </button>
                                {this.state.openOption && 
                                <div className="csd-option" onClick={this.selectReviewOption}>
                                    <ul>
                                        {reviewReason && reviewReason.length>0 && reviewReason.map((data,key) => (
                                            data !== "" && <li  key={key} value={data}>{data}</li>
                                        ))}
                                    </ul>
                                </div>}
                            </div>
                        </div>
                        <div className="pwqh-right">
                            <div className="ars-data-head-search-bar">
                                <input type="search" value={this.state.searchText} onChange={(e) => this.onSearch(e)} onKeyDown={(e)=> {e.persist(); if(e.keyCode == 13) this.onSearch(e)}} placeholder="Type To Search"/>
                                <img className="search-image" src={Search}></img>
                                {this.state.searchText != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="expand-new-table">
                        <div className="manage-table">
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)}
                                isReportPageFlag={this.state.isReportPageFlag}
                            />
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                    <th className="fix-action-btn"></th>
                                    {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                            <label data-key={data}>{data}</label>
                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                        </th>
                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                        <th key={key} data-key={data} onClick={this.filterHeader}>
                                            <label data-key={data}>{data}</label>
                                            <img src={filterIcon} className="imgHead" data-key={data}/>
                                        </th>
                                    ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {arsArray.length != 0 ? arsArray.map((data, key) => (
                                    <React.Fragment key={key}>
                                    <tr>
                                        <td className="fix-action-btn width40">
                                            <ul className="table-item-list">
                                                <li key={key} className="til-inner til-eye-btn" onClick={() => {this.getAssortmentDetails(key, data)}}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12" viewBox="0 0 20.603 12.018">
                                                        <path d="M10.314 6.717a11.2 11.2 0 0 1 8.159 3.98c-1.2 1.577-4.046 4.6-8.159 4.6-3.794 0-6.808-3.036-8.136-4.642a11.311 11.311 0 0 1 8.136-3.943zm0-1.717C3.817 5 0 10.624 0 10.624s4.151 6.395 10.314 6.395c6.638 0 10.289-6.395 10.289-6.395S16.919 5 10.314 5zM10.3 7.575a3.434 3.434 0 1 0 3.434 3.434A3.435 3.435 0 0 0 10.3 7.575z" transform="translate(0 -5)"/>
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                            <td key={key} ><label>{data[hdata]}</label>
                                                {this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                                            </td>
                                        )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                            <td key={keyy} ><label className="table-td-text">{data[sdata]}</label>
                                                {this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                                            </td>
                                        ))}
                                    </tr>
                                    </React.Fragment>
                                    )) : <tr><td colSpan="100"><label>NO DATA FOUND</label></td></tr>}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        reviewReason: state.analytics.reviewReason.data,
        arsPlannerData: state.analytics.arsPwqData,
        arsFilter: state.analytics.arsPwqFilters
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PlannerWorkQueue2)