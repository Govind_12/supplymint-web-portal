import React from "react";

class DeleteAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {

    return (
   
        <div className="modal" id="confirmModal">
        <div className="modal-dialog">
          <div className="modal-content modalOrganisationAlert">
          
           <div className="col-md-12 col-sm-12">
               <div className="modal_confirmation">
                   <ul className="list-inline modalListOrganisation">
                       <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <path fill="#c44569" fillRule="evenodd" d="M9 13h2v2H9v-2zm0-8h2v6H9V5zm.99-5C4.47 0 0 4.48 0 10s4.47 10 9.99 10C15.52 20 20 15.52 20 10S15.52 0 9.99 0zM10 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>
                            </svg>
                       </li>
                        <li>
                            <p>
                                Are you sure you want to delete the record ! 
                            </p>
                            <span>
                                Note  -  This process is irreversible. data will be permanently deleted.
                            </span>
                        </li>
    
                        <li className="text_align_right">
                            <button className="confirmationButton">
                                CANCEL
                            </button>
                            <button className="confirmationButton">
                                CONFIRM
                            </button>
                        </li>
                   </ul>
               </div>
           </div>
            
          </div>
        </div>
      </div>
    

     
    );
  }
}

export default DeleteAlert;
