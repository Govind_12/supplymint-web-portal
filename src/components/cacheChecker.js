import React from "react";
import ClearCache from 'react-clear-cache';

class CacheChecker extends React.Component {
    render() {
    return (
        <div>
        <ClearCache>
        {({ isLatestVersion, emptyCacheStorage }) => (
          <div>
            {!isLatestVersion && (
              <div className="cache-banner">
                <div className="cb-left">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 25.544 25.544">
                    <g>
                      <path fill="#ffffff" d="M17.772 5a12.772 12.772 0 1 0 12.772 12.772A12.773 12.773 0 0 0 17.772 5zm-4.91 19.527a.313.313 0 0 1-.284-.142l-4.371-5.563a.3.3 0 0 1 0-.369.258.258 0 0 1 .284-.227h2.526c.057-4.144 2.81-7.237 7.1-7.237.142 0 .142 0 0 .057-.624.539-3.406 1.76-3.463 7.209H17.2a.361.361 0 0 1 .284.227c.057.142.085.284 0 .369l-4.342 5.563a.434.434 0 0 1-.28.113zm14.191-7.237h-2.526c-.057 4.144-2.81 7.237-7.1 7.237-.142 0-.142 0 0-.057.624-.511 3.406-1.731 3.463-7.181h-2.55a.361.361 0 0 1-.284-.227.3.3 0 0 1 0-.369l4.344-5.562a.349.349 0 0 1 .284-.142.313.313 0 0 1 .284.142l4.342 5.563a.3.3 0 0 1 0 .369c.027.137-.115.226-.257.226z" transform="translate(-5 -5)"/>
                    </g>
                  </svg>
                  <p>New version available  ! 
                    <span>Please click on reload to update</span>
                  </p>
                </div>
                <div className="cb-right">
                  <a class="cache-text" 
                      href="#"
                      onClick={e => {
                        e.preventDefault();
                        emptyCacheStorage();
                        window.location.reload();
                      }}
                    >
                    Reload
                  </a>
                </div>
              </div>
            )}
          </div>
        )}
      </ClearCache>
      </div>
    );
  }};

  export default CacheChecker;