import React from "react";
import Header from "./header";
import { Link } from "react-router-dom";
import profile from "../assets/avatar.svg";
import SessionTimeOut from './timeOutModals/sessionExpireTimerModal';
import IdleTimer from 'react-idle-timer'
import SessionExpired from './timeOutModals/sessionExpiredModal';

class OldSidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      over: false,
      firstShow: false,
      inactivityPopupS: false,
      expired: false,
      modules: [],
    };
    this.idleTimer = null
    this.onAction = this._onAction.bind(this)
    this.onActive = this._onActive.bind(this)
    this.onIdle = this._onIdle.bind(this)
  }

  componentWillMount() {
    if (sessionStorage.getItem('logout') == "true") {
      sessionStorage.clear();
      this.props.history.push('/');
    }
    if (sessionStorage.getItem('token') == null) {
      this.props.history.push('/');
    }
  }

  componentDidMount() {
    if (sessionStorage.getItem('logout') == "true") {
      sessionStorage.clear();
      this.props.history.push('/');
    }
    if (sessionStorage.getItem('token') == null) {
      this.props.history.push('/');
    }
    this.setState({ modules: JSON.parse(sessionStorage.getItem('modules')) })
  }

  componentWillReceiveProps(nextProps) {
    if (sessionStorage.getItem('logout') == "true") {
      sessionStorage.clear();
      this.props.history.push('/');
    }
    if (sessionStorage.getItem('token') == null) {
      this.props.history.push('/');
    }
  }

  _onAction() {

  }

  _onActive() {

  }

  _onIdle() {
    if (!this.state.expired) {
      this.inactivityPopupOpen();
    }
  }


  inactivityPopupOpen() {
    this.setState({
      inactivityPopup: true
    })
  }
  inactivityPopupClose(type) {
    if (type == 'auto') {
      sessionStorage.setItem('logout', true)
      this.setState({
        inactivityPopup: false,
        expired: true
      })
    } else {
      this.setState({
        inactivityPopup: false
      })
    }
  }

  isActiveSubLink(subLink) {
    return window.location.hash.split("/")[2] == subLink;
  }

  isActiveLink(menuLink) {
    return window.location.hash.split("/")[1] == `${menuLink}`;
  }
  isCustomActive(customLink) {
    return window.location.hash.split("/")[3] == `${customLink}`;
  }

  onShowHide() {
    this.setState({
      show: !this.state.show,
      firstShow: !this.state.firstShow
    });
  }

//   renderShit(root) {
//     if (Array.isArray(root) && root.length > 0 && root.length != undefined) {
//       return (
//         <ul className="collapse list" id={root[0].parentCode}>
//           {
//             root.map(node => (
//               // <ul className="collapse list" id={`#${_.name}`}>
//               <li data-toggle="collapse" data-target={`#${node.code}`}>{node.subModules != undefined && node.name}{node.subModules != undefined && <i className="fa fa-angle-down arrow_down_icon" />}{node.subModules == undefined ? this.renderShit(node) : this.renderShit(node.subModules)}</li>
//               // </ul>
//             ))
//           }
//         </ ul>
//       );
//     }
//     else {
//       return <li onClick={() => this.props.history.push(root.pageUrl.replace('#', ''))}>{root.name}</li>;
//     }
//   }

  render() {
    let miid = JSON.parse(sessionStorage.getItem('mid'));
    let role = "";
    let data = JSON.parse(sessionStorage.getItem('roles'));
    if (data != null) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].id == miid) {
          role = data[i].name;
        }
      }
    }

    var Change_Setting = JSON.parse(sessionStorage.getItem('Change Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Change Setting')).crud);
    var Ad_Hoc_Request = JSON.parse(sessionStorage.getItem('Ad-Hoc Request')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Ad-Hoc Request')).crud);
    var Administration = JSON.parse(sessionStorage.getItem('Administration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Administration')).crud);
    var ARS = JSON.parse(sessionStorage.getItem('ARS')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ARS')).crud);
    var Add_New_Vendors = JSON.parse(sessionStorage.getItem('Add New Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Add New Vendors')).crud);
    var Allocation_Report = JSON.parse(sessionStorage.getItem('Allocation Report')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Allocation Report')).crud);
    var Analytics = JSON.parse(sessionStorage.getItem('Analytics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Analytics')).crud);
    var Assortment = JSON.parse(sessionStorage.getItem('Assortment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Assortment')).crud);
    var Custom_Masters = JSON.parse(sessionStorage.getItem('Custom Masters')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Custom Masters')).crud);
    var Data_Sync = JSON.parse(sessionStorage.getItem('Data Sync')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Data Sync')).crud);
    var Demand_Planning = JSON.parse(sessionStorage.getItem('Demand Planning')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Demand Planning')).crud);
    var Fast_Moving_Articles = JSON.parse(sessionStorage.getItem('Fast Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Fast Moving Articles')).crud);
    var History = JSON.parse(sessionStorage.getItem('History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('History')).crud);
    var Inventory_Classification = JSON.parse(sessionStorage.getItem('Inventory Classification')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Inventory Classification')).crud);
    var Inventory_Planning = JSON.parse(sessionStorage.getItem('Inventory Planning')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Inventory Planning')).crud);
    var Item = JSON.parse(sessionStorage.getItem('Item')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item')).crud);
    var Item_Configuration = JSON.parse(sessionStorage.getItem('Item Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Configuration')).crud);
    var Item_Mapping = JSON.parse(sessionStorage.getItem('Item Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Mapping')).crud);
    var Item_route = JSON.parse(sessionStorage.getItem('Item route')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item route')).crud);
    var Manage_Users = JSON.parse(sessionStorage.getItem('Manage Users')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Users')).crud);
    var Manage_Vendors = JSON.parse(sessionStorage.getItem('Manage Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Vendors')).crud);
    var Moderate_Moving_Articles = JSON.parse(sessionStorage.getItem('Moderate Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Moderate Moving Articles')).crud);
    var Monthly = JSON.parse(sessionStorage.getItem('Monthly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly')).crud);
    var New_Articles = JSON.parse(sessionStorage.getItem('New Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('New Articles')).crud);
    var Organization = JSON.parse(sessionStorage.getItem('Organization')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Organization')).crud);

    var Priority_List = JSON.parse(sessionStorage.getItem('Priority List')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Priority List')).crud);

    var Replenishment = JSON.parse(sessionStorage.getItem('Replenishment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Replenishment')).crud);
    var Roles = JSON.parse(sessionStorage.getItem('Roles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Roles')).crud);
    var Sales_Achieved = JSON.parse(sessionStorage.getItem('Sales Achieved')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Sales Achieved')).crud);
    var Schedulers = JSON.parse(sessionStorage.getItem('Schedulers')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Schedulers')).crud);
    var Sell_Throughs = JSON.parse(sessionStorage.getItem('Sell Throughs')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Sell Throughs')).crud);
    var Site = JSON.parse(sessionStorage.getItem('Site')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site')).crud);
    var Site_Mapping = JSON.parse(sessionStorage.getItem('Site Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site Mapping')).crud);
    var Stale_Moving_Articles = JSON.parse(sessionStorage.getItem('Stale Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Stale Moving Articles')).crud);
    var Store_Profiling = JSON.parse(sessionStorage.getItem('Store Profiling')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Store Profiling')).crud);
    var Store_Ranking = JSON.parse(sessionStorage.getItem('Store Ranking')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Store Ranking')).crud);
    var Summary = JSON.parse(sessionStorage.getItem('Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Summary')).crud);
    var Vendor_Management = JSON.parse(sessionStorage.getItem('Vendor Management')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Vendor Management')).crud);
    var Auto_Configuration = JSON.parse(sessionStorage.getItem('Auto Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Auto Configuration')).crud);
    var Run_on_Demand = JSON.parse(sessionStorage.getItem('Run on Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Run on Demand')).crud);
    var Weekly = JSON.parse(sessionStorage.getItem('Weekly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly')).crud);
    var Retail = JSON.parse(sessionStorage.getItem('Retail Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Retail Master')).crud);
    var MBO = JSON.parse(sessionStorage.getItem('MBO Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('MBO Master')).crud);
    var Budgeted_Sales = JSON.parse(sessionStorage.getItem('Budgeted Sales')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales')).crud);
    var Budgeted_Sales_History = JSON.parse(sessionStorage.getItem('Budgeted Sales History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales History')).crud);
    var Forecast_On_Demand = JSON.parse(sessionStorage.getItem('Forecast On Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast On Demand')).crud);
    var Weekly_History = JSON.parse(sessionStorage.getItem('Weekly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly History')).crud);
    var Monthly_History = JSON.parse(sessionStorage.getItem('Monthly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly History')).crud);
    var Forecast_History = JSON.parse(sessionStorage.getItem('Forecast History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast History')).crud);
    //procurement
    var Procurement = JSON.parse(sessionStorage.getItem('Procurement')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Procurement')).crud);
    var Purchase_Indent = JSON.parse(sessionStorage.getItem('Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Indent')).crud);
    var Purchase_Order = JSON.parse(sessionStorage.getItem('Purchase Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order')).crud);
    var Purchase_Order_With_Upload = JSON.parse(sessionStorage.getItem('Purchase Order File Upload')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order File Upload')).crud);
    var PI_History = JSON.parse(sessionStorage.getItem('PI History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PI History')).crud);
    var PO_History = JSON.parse(sessionStorage.getItem('PO History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PO History')).crud);
    var Item_UDF_Mapping = JSON.parse(sessionStorage.getItem('Item UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Mapping')).crud)
    var Item_UDF_Setting = JSON.parse(sessionStorage.getItem('Item UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Setting')).crud)
    var UDF_Mapping = JSON.parse(sessionStorage.getItem('UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Mapping')).crud)
    var UDF_Setting = JSON.parse(sessionStorage.getItem('UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Setting')).crud)
    var Dept_Size_Mapping = JSON.parse(sessionStorage.getItem('Dept Size Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Dept Size Mapping')).crud)
    var Configuration = JSON.parse(sessionStorage.getItem('Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Configuration')).crud)
    var Manage_Rule_Engine = JSON.parse(sessionStorage.getItem('Manage Rule Engine')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Rule Engine')).crud)

    var Rule_Engine_Configuration = JSON.parse(sessionStorage.getItem('Rule Engine Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Rule Engine Configuration')).crud)
    var Role_Master = JSON.parse(sessionStorage.getItem('Role Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Role Master')).crud)
    var Add_Role = JSON.parse(sessionStorage.getItem('Add Role')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Add Role')).crud)
    var Manage_Role = JSON.parse(sessionStorage.getItem('Manage Role')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Role')).crud)
    var File_Upload_History = JSON.parse(sessionStorage.getItem('File Upload History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('File Upload History')).crud)
    // ANALYTICS
    var FMCG_Master = JSON.parse(sessionStorage.getItem('FMCG Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('FMCG Master')).crud)
    var UPLOAD_MASTER = JSON.parse(sessionStorage.getItem('Upload Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Upload Master')).crud)
    var manage_rule = JSON.parse(sessionStorage.getItem('Manage Rule Engines')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Rule Engines')).crud)
    var Event_Master = JSON.parse(sessionStorage.getItem('Event Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Event Master')).crud)

    var Purchase_Orders = JSON.parse(sessionStorage.getItem('Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Orders')).crud)
    var Purchase_Orders_History = JSON.parse(sessionStorage.getItem('Purchase Order History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order History')).crud)

    var Cancelled_Order = JSON.parse(sessionStorage.getItem('Cancelled Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled Orders')).crud)
    var Pending_Orders = JSON.parse(sessionStorage.getItem('Pending Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Pending Orders')).crud)

    var Asn_Under_Approval = JSON.parse(sessionStorage.getItem('ASN Under Approval')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ASN Under Approval')).crud)
    var Approved_Asn = JSON.parse(sessionStorage.getItem('Approved ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Approved ASN')).crud)
    var Cancelled_Asn = JSON.parse(sessionStorage.getItem('Cancelled ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled ASN')).crud)
    var Lr_Processing = JSON.parse(sessionStorage.getItem('LR Processing')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('LR Processing')).crud)
    var Goods_Intransit = JSON.parse(sessionStorage.getItem('Goods Intransit')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Intransit')).crud)
    var Goods_Delivered = JSON.parse(sessionStorage.getItem('Goods Delivered')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Delivered')).crud)
    var Logistics = JSON.parse(sessionStorage.getItem('Logistics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Logistics')).crud)
    var Order = JSON.parse(sessionStorage.getItem('Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Order')).crud)
    var Shipment = JSON.parse(sessionStorage.getItem('Shipment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment')).crud)
    var Shipment_Tracking = JSON.parse(sessionStorage.getItem('Shipment Tracking')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment Tracking')).crud)
    var Processed_Orders = JSON.parse(sessionStorage.getItem('Processed Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Processed Orders')).crud)
    var Generics_Purchase_Orders = JSON.parse(sessionStorage.getItem('Generics Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Generics Purchase Orders')).crud)
    var Generics_Purchase_Indent = JSON.parse(sessionStorage.getItem('Generics Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Generics Purchase Indent')).crud)



    return (
      < div >

        <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={1000 * 10800} />
        {this.state.inactivityPopup ? <SessionTimeOut inactivityPopupClose={(e) => this.inactivityPopupClose(e)} /> : null}
        {sessionStorage.getItem('logout') == "true" ? <SessionExpired {...this.props} /> : null}
        <Header {...this.props} show={this.state.show} showHide={() => this.onShowHide()} />
        {/* { */}
        {/* // !this.state.firstShow ? null : */}
        <nav
          className="sidemenu newSidebar2"
          // {
          //   this.state.show ? "sidemenu  menuShow" : "sidemenu "
          // }
          id="sidebar"
        >
          <div className="sidemenu_header">
            <ul>
              <li>
                {/* <div className="img_border sideBarProfileImg">
                  {sessionStorage.getItem("profile") != null ? <img className="profile_img" src={sessionStorage.getItem("profile")} /> : <img className="profile_img" src={profile} />}
                </div> */}
                <div className="userImage userImgEditProfile sidebarImg marginAuto">
                  <span className="firstLetter">
                    {sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName').charAt(0)}
                  </span>
                </div>
              </li>
              <li>
                <p>{sessionStorage.getItem('firstName') == null ? "" : sessionStorage.getItem('firstName')} {sessionStorage.getItem('lastName') == null ? "" : sessionStorage.getItem('lastName')}</p>
              </li>
              <li>
                <p>{sessionStorage.getItem('email')}</p>
              </li>
              <li>
                <p>{role}</p>
              </li>
            </ul>
          </div>
          {/* <ul className="generic-sidebar-container sidemenu_list m-p-0 sideBarMenuList">
            {this.state.modules[sessionStorage.getItem('mid')] != undefined && this.state.modules[sessionStorage.getItem('mid')].map((_) => (
              <li className="menu-list-item"><li className="menu-item-list-inner" data-toggle="collapse" data-target={`#${_.code}`}><span className="mli-icon"><img src={require(`../assets/${_.iconUrl}`)} /></span><span className="mli-name">{_.name}</span></li>
                {Array.isArray(_.subModules) && this.renderShit(_.subModules)}
              </li>
            ))}
          </ul> */}
          <ul className="sidemenu_list m-p-0 sideBarMenuList">
            {/* <li className={this.isActiveLink("home") ? "active" : null}>
              <div className="sidemenu_icons_div">
                <svg
                  className="icons"
                  xmlns="http://www.w3.org/2000/svg"
                  width="22"
                  height="20"
                  viewBox="0 0 22 20"
                >
                  <g fill="#6D6DC9" fillRule="nonzero">
                    <path d="M21.972 8.719a1.71 1.71 0 0 0-.646-1.148L12.342.514a2.181 2.181 0 0 0-2.7 0L.656 7.57a1.723 1.723 0 0 0-.29 2.417c.586.746 1.67.876 2.417.29l7.414-5.824a1.282 1.282 0 0 1 1.587 0l7.415 5.824a1.72 1.72 0 0 0 2.772-1.559" />
                    <path d="M18.575 10.843L11.83 5.584a1.363 1.363 0 0 0-1.678 0l-6.744 5.26a.45.45 0 0 0-.174.355V17.2c0 1.077.876 1.953 1.953 1.953h11.608a1.955 1.955 0 0 0 1.952-1.953V11.2a.45.45 0 0 0-.173-.356" />
                  </g>
                </svg>
              </div>

              <a href="#/home" id="home_toggle" className="">
                Home
              </a>
            </li> */}

            {Inventory_Planning > 0 ? <div>
              <li
                className={
                  this.isActiveLink("inventoryPlanning") ? "active" : null
                } data-toggle="collapse"
                data-target="#replenishment"
              >
                <div className="sidemenu_icons_div" >
                  <svg
                    className="icons"
                    xmlns="http://www.w3.org/2000/svg"
                    width="22"
                    height="26"
                    viewBox="0 0 22 26"
                  >
                    <g fill="#6D6DC9" fillRule="nonzero">
                      <path d="M17.39 14.588c-1.492.34-4.164.681-5.08.393-.393-.183-1.362-1.257-1.886-1.833A25.964 25.964 0 0 0 9.01 11.68c-.76-.76-1.362-1.283-2.462-1.571-1.336-.341-4.688-.734-5.631 1.492C-.08 13.933.079 20.01.13 24.331c0 .367.236.68.629.68h7.15c.445 0 .602-.235.628-.575.026-.236.262-4.348.42-7.281.68.655 1.361 1.205 2.12 1.44.708.236 1.546.315 2.41.315 2.043 0 4.19-.446 4.74-.577 1.022-.235 1.677-1.257 1.441-2.278a1.884 1.884 0 0 0-2.279-1.467z" />
                      <circle cx="5.055" cy="4.479" r="4.348" />
                      <path d="M20.874 3.955H19.8V3.3a.414.414 0 0 0-.419-.419h-.524a.414.414 0 0 0-.419.419v.655h-2.514V3.3a.414.414 0 0 0-.42-.419h-.523a.414.414 0 0 0-.42.419v.655h-1.073a1.05 1.05 0 0 0-1.048 1.047v6.26a1.05 1.05 0 0 0 1.048 1.048h7.36a1.05 1.05 0 0 0 1.047-1.048V4.976c.026-.55-.445-1.021-1.021-1.021zm.445 7.28c0 .236-.21.446-.445.446h-7.36a.459.459 0 0 1-.445-.445V6.364h8.25v4.872z" />
                      <path d="M14.221 8.643h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.261.262v.838c0 .13.13.262.261.262zM16.657 8.643h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .13.105.262.262.262zM19.093 8.643h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .13.105.262.262.262zM14.221 10.817h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.261.262v.838c0 .157.13.262.261.262zM16.657 10.817h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .157.105.262.262.262zM19.093 10.817h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .157.105.262.262.262z" />
                    </g>
                  </svg>
                </div>
                <a className="hoverMenu">Inventory Planning
              <i className="fa fa-angle-down arrow_down_icon" /></a>
              </li>
              <ul id="replenishment" className="collapse list" >
                {/* <li onClick={() => this.onShowHide()}>
                <a href="#/item/itemRoute"
                  className={
                    this.isActiveSubLink("item/itemRoute") ? "active" : null
                  }>
                  Item
                </a>
              </li>

              <li onClick={() => this.onShowHide()}>
                <a
                  href="#/item/itemMapping"
                  className={
                    this.isActiveSubLink("item/itemMapping")
                      ? "active"
                      : null
                  }
                >
                  Item Mapping
                </a>
              </li>
              <li onClick={() => this.onShowHide()}>
                <a
                  href="#/item/itemRoute"
                  className={
                    this.isActiveSubLink("item/itemRoute") ? "active" : null
                  }
                >
                  Item Route
                </a>
              </li> */}
                {Replenishment > 0 ? <div>
                  <li
                    // className={this.isActiveLink("replenishment") ? "active" : null}
                    data-toggle="collapse"
                    data-target="#scheduler"
                  >

                    <a
                      className="hoverMenu"
                    >
                      Replenishment
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                  </li>

                  <ul id="scheduler" className="collapse list pad-lft-10" >
                    <div>
                      {Schedulers > 0 ? <li
                        // className={this.isActiveLink("replenishment") ? "active" : null}
                        data-toggle="collapse"
                        data-target="#schedulerList"
                      >
                        <a
                          className="hoverMenu"
                        >
                          Scheduler
              <i className="fa fa-angle-down arrow_down_icon" />
                        </a>
                      </li> : null}



                      <ul id="schedulerList" className="collapse list pad-lft-10" >
                        {Auto_Configuration > 0 ?
                          <li onClick={() => this.onShowHide()}
                          >
                            <a
                              href="#/inventoryPlanning/inventoryAutoConfig"
                              className={
                                this.isActiveSubLink("inventoryAutoConfig")
                                  ? "active menuSubItem"
                                  : "menuSubItem"
                              }
                            >
                              Auto Configuration</a>
                          </li>
                          : null}
                        {Run_on_Demand > 0 ?
                          <li
                            onClick={() => this.onShowHide()}
                          >
                            <a
                              href="#/inventoryPlanning/runOnDemand"
                              className={
                                this.isActiveSubLink("runOnDemand")
                                  ? "active menuSubItem"
                                  : "menuSubItem"
                              }
                            >
                              Run-On-Demand</a>
                          </li>
                          : null}

                      </ul>
                    </div>

                    {Manage_Rule_Engine > 0 ? <li onClick={() => this.onShowHide()}
                    // className={this.isActiveLink("replenishment") ? "active" : null}
                    >
                      <a

                        href="#/inventoryPlanning/manageRuleEngine"
                        className={
                          this.isActiveSubLink("manageRuleEngine")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Manage Rule Engine
                  </a>
                    </li> : null}

                    {Rule_Engine_Configuration > 0 ? <li onClick={() => this.onShowHide()}
                    // className={this.isActiveLink("replenishment") ? "active" : null}
                    >
                      <a

                        href="#/inventoryPlanning/configuration"
                        className={
                          this.isActiveSubLink("configuration")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Configuration
                  </a>
                    </li> : null}
                    {Summary > 0 ? <li onClick={() => this.onShowHide()}
                    // className={this.isActiveLink("replenishment") ? "active" : null}
                    >
                      <a

                        href="#/inventoryPlanning/summary"
                        className={
                          this.isActiveSubLink("summary")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Summary
                  </a>
                    </li> : null}
                    {History > 0 ? <li onClick={() => this.onShowHide()}
                    // className={this.isActiveLink("replenishment") ? "active" : null}

                    >
                      <a

                        href="#/inventoryPlanning/history"
                        className={
                          this.isActiveSubLink("history")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        History
                  </a>
                    </li> : null}

                  </ul>
                </div> : null}
                {/* {Allocation_Report > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href=""
                      // className={
                      //   this.isActiveSubLink("replenishment") ? "active" : null
                      // }
                      >
                        Allocation Report
                     </a>
                    </li> : null} */}
                {Assortment > 0 ?
                  <li onClick={() => this.onShowHide()}>
                    <a

                      href="#/inventoryPlanning/assortment"
                      className={
                        this.isActiveSubLink("assortment")
                          ? "active menuSubItem"
                          : "menuSubItem"
                      }
                    >
                      Assortment
                </a>

                  </li> : null}

                {Ad_Hoc_Request > 0 ? <li>
                  <a

                    href="#/inventoryPlanning/manageAd-Hoc"
                    className={
                      this.isActiveSubLink("manageAdHoc")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Ad-Hoc Request
                </a>

                </li> : null}

              </ul>
            </div> : null}
            {Demand_Planning > 0 ? <div>
              <li
                className={this.isActiveLink("demandPlanning") ? "active" : null}
                data-toggle="collapse"
                data-target="#planning"
              >
                <div className="sidemenu_icons_div">
                  <svg
                    className="icons"
                    xmlns="http://www.w3.org/2000/svg"
                    width="23"
                    height="28"
                    viewBox="0 0 23 28"
                  >
                    <g fill="#6D6DC9" fillRule="nonzero">
                      <path d="M5.11 19.613a3.765 3.765 0 1 0 0-7.53 3.765 3.765 0 0 0 0 7.53zm0-5.34a1.574 1.574 0 1 1 0 3.15 1.574 1.574 0 0 1 0-3.15z" />
                      <path d="M21.828.036H8.411c-.604 0-1.095.49-1.095 1.095v9.789c0 .604.49 1.095 1.095 1.095h5.726l-.063 1.437h-.203c-.533 0-.988.384-1.079.91l-.722 4.205-3.08 1.32H1.171c-.605 0-1.095.49-1.095 1.095v6.23h2.19v-5.135h6.95c.148 0 .295-.03.432-.087l3.833-1.643c.342-.146.586-.455.65-.822l.654-3.882h.391l-.797 4.97-5.681 3.03c-.357.19-.58.562-.58.967v2.601h2.19v-1.944l5.572-2.979c.302-.16.512-.453.567-.791l1.095-6.777a1.095 1.095 0 0 0-1.082-1.27h-.197l.063-1.438h5.501c.605 0 1.095-.49 1.095-1.095V1.13c0-.605-.49-1.095-1.095-1.095zm-1.095 9.788h-4.304l.063-1.388-2.19-.098-.064 1.486H9.507V2.226h11.226v7.598z" />
                      <path d="M10.794 3.116h3.491v2.19h-3.491zM10.794 5.854h8.146v2.19h-8.146z" />
                    </g>
                  </svg>
                </div>
                <a className="hoverMenu">Demand Planning
              <i className="fa fa-angle-down arrow_down_icon" /></a>
              </li>
              <ul id="planning" className="collapse list " >
                <div>
                  {/* <li
                    data-toggle="collapse"
                    data-target="#forecast"
                  >

                    <a
                      className="hoverMenu"
                    >
                      Forecast
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                  </li> */}
                  <ul id="forecast" className="collapse list pad-lft-10" >
                    {/* <li><a href="#/demandPlanning/forecastAutoConfiguration" className={this.isActiveSubLink("forecastAutoConfiguration")? "active menuSubItem": "menuSubItem"}>Auto Configuration</a></li> */}
                    {/* {Forecast_On_Demand > 0 ? <li><a href="#/demandPlanning/forecastOnDemand" className={this.isActiveSubLink("forecastOnDemand") ? "active menuSubItem" : "menuSubItem"}>Forecast On Demand</a></li> : null} */}
                    {/* {Forecast_History > 0 ? <li><a href="#/demandPlanning/forecastHistory" className={this.isActiveSubLink("forecastHistory") ? "active menuSubItem" : "menuSubItem"}>Forecast History</a></li> : null} */}
                  </ul>
                </div>
                {/* <div>
                  <li
                    data-toggle="collapse"
                    data-target="#weekly"
                  >

                    <a
                      className="hoverMenu"
                    >
                      Weekly
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                  </li>



                  <ul id="weekly" className="collapse list pad-lft-10" >
                    {Weekly > 0 ? <li onClick={() => this.onShowHide()}
                    >
                      <a
                        href="#/demandPlanning/weekly"
                        className={
                          this.isActiveSubLink("weekly")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Insight</a>
                    </li> : null}
                    {Weekly_History > 0 ? <li onClick={() => this.onShowHide()}
                    >
                      <a
                        href="#/demandPlanning/weeklyHistory"
                        className={
                          this.isActiveSubLink("weeklyHistory")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        History</a>
                    </li> : null}
                  </ul></div>  */}
                {/* <div>
                  <li
                    data-toggle="collapse"
                    data-target="#monthly"
                  >

                    <a
                      className="hoverMenu"
                    >
                      Monthly
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                  </li>

                  <ul id="monthly" className="collapse list pad-lft-10">
                    {Monthly > 0 ? <li
                      onClick={() => this.onShowHide()}
                    >
                      <a
                        href="#/demandPlanning/monthly"
                        className={
                          this.isActiveSubLink("monthly")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Insight</a>
                    </li> : null}
                    {Monthly_History > 0 ? <li
                      onClick={() => this.onShowHide()}
                    >
                      <a
                        href="#/demandPlanning/monthlyHistory"
                        className={
                          this.isActiveSubLink("monthlyHistory")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        History</a>
                    </li> : null}</ul>
                </div> */}
                {/* <div>
                  <li
                    data-toggle="collapse"
                    data-target="#budgeted"
                  >

                    <a
                      className="hoverMenu"
                    >
                      Budgeted Sales
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                  </li>


                  <ul id="budgeted" className="collapse list pad-lft-10">

                    {Budgeted_Sales > 0 ? <li
                      onClick={() => this.onShowHide()}
                    >
                      <a
                        href="#/demandPlanning/budgetedSales"
                        className={
                          this.isActiveSubLink("budgetedSales")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Upload Data</a>
                    </li> : null}
                    {Budgeted_Sales_History > 0 ? <li
                      onClick={() => this.onShowHide()}
                    >
                      <a
                        href="#/demandPlanning/budgetedSalesHistory"
                        className={
                          this.isActiveSubLink("budgetedSalesHistory")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        History</a>
                    </li> : null}
                  </ul>
                </div> */}
                {/* <li><a href="#/demandPlanning/openToBuy" className={
                  this.isActiveSubLink("openToBuy")
                    ? "active menuSubItem"
                    : "menuSubItem"
                }>Open To Buy</a></li> */}
              </ul>
            </div> : null}
            {Vendor_Management > 0 ? <div>
              <li
                className={this.isActiveLink("vendor") ? "active" : null}
                data-toggle="collapse"
                data-target="#demo"
              >
                <div className="sidemenu_icons_div">
                  <svg
                    className="icons"
                    xmlns="http://www.w3.org/2000/svg"
                    width="22"
                    height="18"
                    viewBox="0 0 22 18"
                  >
                    <path
                      fill="#6D6DC9"
                      fillRule="nonzero"
                      d="M.128 7.348c-.26.677-.044 1.27-.044 1.27.27.702.591.816 1.267.816.226 0 .438-.048.62-.13v8.438h8.937v-5.118h5.584v5.118h3.352V9.304c.183.082.394.13.621.13.675 0 1.12-.559 1.223-.935 0 0 .234-.708 0-1.15-.4-.752-3.413-7.34-3.413-7.34H3.54L.128 7.349zm2.451 1.15v-.107h-.005V7.348l2.31-5.9h1.845l-1.818 5.9V8.5c0 .517-.522.935-1.166.935-.644 0-1.166-.418-1.166-.935zm4.416 6.06H3.972v-3.022h3.022v3.022zm2.693-7.21V8.5c0 .516-.522.935-1.165.935-.645 0-1.167-.418-1.167-.935V7.348l.966-5.9h1.84l-.474 5.9zm3.611 2.086c-.643 0-1.165-.418-1.165-.935V7.348l-.475-5.9h1.835l.965 5.9V8.5c0 .015.003.031.004.046-.031.495-.54.889-1.164.889zm5.943-1.043h-.006v.108c0 .516-.522.935-1.166.935-.643 0-1.165-.418-1.165-.935V7.348l-1.819-5.9h1.846l2.31 5.9v1.043z"
                    />
                  </svg>
                </div>
                <a className="collapse_para_list">
                  Vendor Management
                <i className="fa fa-angle-down arrow_down_icon" />
                </a>
              </li>

              <ul id="demo" className="collapse list">
                {Manage_Vendors > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/vendor/manageVendors"
                    className={
                      this.isActiveSubLink("manageVendors")
                        ? "active"
                        : null
                    }
                  >
                    Manage Vendors
                </a>
                </li> : null}

              </ul>
            </div> : null}
            {Administration > 0 ? <div>
              <li
                className={this.isActiveLink("administration") ? "active" : null}
                data-toggle="collapse"
                data-target="#admin"
              >
                <div className="sidemenu_icons_div">
                  <svg
                    className="icons"
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="23"
                    viewBox="0 0 20 23"
                  >
                    <path
                      fill="#6D6DC9"
                      fillRule="nonzero"
                      d="M.055 21.093v-4.048c-.004-.615.27-1.2.745-1.59a30.748 30.748 0 0 1 4.011-2.76.58.58 0 0 1 .82.26L8.36 18.97l.991-2.356a1.38 1.38 0 1 1 1.302 0l.984 2.359 2.732-6.018a.58.58 0 0 1 .82-.264 30.7 30.7 0 0 1 4.012 2.764c.475.39.749.975.745 1.59v4.05a1.177 1.177 0 0 1-1.177 1.178H1.234a1.18 1.18 0 0 1-1.18-1.18zM5.086 5.184c.115-.425.285-.833.505-1.214l-.568-.81a.75.75 0 0 1 .084-.962l.575-.584a.75.75 0 0 1 .961-.084l.812.568a5.052 5.052 0 0 1 1.222-.507l.17-.975A.75.75 0 0 1 9.588 0h.827a.75.75 0 0 1 .738.62l.17.97c.425.116.833.285 1.214.505l.812-.568a.75.75 0 0 1 .97.087l.584.584a.75.75 0 0 1 .084.961l-.568.811c.22.381.39.79.505 1.214l.975.17a.75.75 0 0 1 .62.74v.826a.75.75 0 0 1-.62.74l-.975.17a5.052 5.052 0 0 1-.505 1.215l.568.812a.75.75 0 0 1-.084.961l-.584.587a.75.75 0 0 1-.961.084l-.818-.58c-.381.22-.79.39-1.214.505l-.17.975a.75.75 0 0 1-.74.62h-.829a.75.75 0 0 1-.738-.62l-.17-.975a5.052 5.052 0 0 1-1.217-.505l-.811.568a.75.75 0 0 1-.968-.072l-.584-.585a.75.75 0 0 1-.084-.96l.568-.812c-.22-.382-.39-.79-.505-1.216l-.975-.17a.75.75 0 0 1-.62-.74v-.827a.75.75 0 0 1 .62-.738l.984-.173zm2.58 1.323a2.334 2.334 0 1 0 4.668 0 2.334 2.334 0 0 0-4.668.002v-.002z"
                    />
                  </svg>
                </div>
                <a className="hoverMenu">
                  Administration
                <i className="fa fa-angle-down arrow_down_icon" />
                </a>
              </li>

              <ul id="admin" className="collapse list">
                {Role_Master > 0 ? <li data-toggle="collapse"
                  data-target="#roleMatser"
                >
                  <a
                    className="hoverMenu"
                  >
                    Role Master
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                </li> : null}

                <ul id="roleMatser" className="collapse list pad-lft-10" >

                  {Add_Role > 0 ? <li onClick={() => this.onShowHide()}
                  >
                    <a
                      href="#/administration/rolesMaster/addRoles"
                      className={
                        this.isActiveSubLink("addRoles")
                          ? "active menuSubItem"
                          : "menuSubItem"
                      }
                    >
                      Add Role</a>
                  </li> : null}
                  {Manage_Role > 0 ? <li onClick={() => this.onShowHide()}
                  >
                    <a
                      href="#/administration/rolesMaster/manageRoles"
                      className={
                        this.isActiveSubLink("manageRoles")
                          ? "active menuSubItem"
                          : "menuSubItem"
                      }
                    >
                      Manager Role</a>
                  </li> : null}
                </ul>
                {Change_Setting > 0 ? <li data-toggle="collapse"
                  data-target="#changeSetting"
                >
                  <a
                    className="hoverMenu"
                  >
                    Change Setting
              <i className="fa fa-angle-down arrow_down_icon" /></a>
                </li> : null}

                <ul id="changeSetting" className="collapse list pad-lft-10" >
                  {/* {Change_Setting > 0 ? <li onClick={() => this.onShowHide()}
                  >
                    <a
                      href="#/administration/festivalSetting"
                      className={
                        this.isActiveSubLink("festivalSetting")
                          ? "active menuSubItem"
                          : "menuSubItem"
                      }
                    >
                      Festival Setting</a>
                  </li> : null} */}
                  {/* {Change_Setting > 0 ? <li onClick={() => this.onShowHide()}
                  >
                    <a
                      href="#/administration/promotionalEvents"
                      className={
                        this.isActiveSubLink("promotionalEvents")
                          ? "active menuSubItem"
                          : "menuSubItem"
                      }
                    >
                      Promotional Event</a>
                  </li> : null} */}
                </ul>
                {Organization > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/administration/organisation"
                    className={
                      this.isActiveSubLink("organisation")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Organisation
                  </a>
                </li> : null}
                {Manage_Users > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/administration/users"
                    className={
                      this.isActiveSubLink("users")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Users
                </a>
                </li> : null}
                {Roles > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/administration/roles"
                    className={
                      this.isActiveSubLink("roles")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Roles
                </a>
                </li> : null}

                {Site > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/administration/site"
                    className={
                      this.isActiveSubLink("site")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Site
                </a>
                </li> : null}
                {/* {Site_Mapping > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/administration/siteMapping"
                    className={
                      this.isActiveSubLink("siteMapping")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Site Mapping
                </a>
                </li> : null} */}
                {Data_Sync > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/administration/dataSync"
                    className={
                      this.isActiveSubLink("dataSync")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Data Sync
                </a>
                </li> : null}
                {/* li Class customHoverSubmenu */}
                {Custom_Masters > 0 ? <li id="leftDrop"
                  data-toggle="collapse" data-target="#custom" aria-expanded="true"
                >
                  <a
                    className={
                      this.isActiveSubLink("custom")
                        ? "active"
                        : null
                    }
                  >
                    Custom
                </a>

                  <div
                    className="HoverSideMenu" id="custom">
                    <label className="heading">
                      Choose Master
                        </label>
                    <div className="containerHoverMenu">
                      <ul className="list-inline">
                        <li>
                          {/* <button className="clearSelectedContent">
                                Clear
                          </button> */}
                        </li>
                      </ul>
                      <div className="sumenuList">
                        <ul className="list-inline">

                          {/* {sessionStorage.getItem('partnerEnterpriseName') == "SKECHERS" ? */}
                          {/* <div> */}
                          {Retail > 0 ? <li >
                            <label>
                              <Link to="/administration/custom/masterSkechersRetail" className={
                                this.isCustomActive("masterSkechersRetail")
                                  ? "customActive"
                                  : null
                              }>
                                Retail Master
                            </Link>
                            </label>
                          </li> : null}
                          {Event_Master > 0 ? <li >
                            <label>
                              <Link to="/administration/custom/masterEvents" className="customActive">
                                Master Events
                                </Link>
                            </label>
                          </li> : null}

                          {MBO > 0 ? <li >

                            <label>
                              <Link to="/administration/custom/masterSkechersMBO" className={
                                this.isCustomActive("masterSkechersMBO")
                                  ? "customActive"
                                  : null
                              }>
                                MBO Master
                          </Link>
                            </label>
                          </li> : null}
                          {/* </div>
                            : null} */}
                          {/* For NYSAA Retail master and Master MBO  */}

                          {FMCG_Master > 0 ?
                            <li >
                              <label>
                                <Link to="/administration/custom/uploadCustomMaster" className={
                                  this.isCustomActive("fmcgMaster")
                                    ? "customActive"
                                    : null
                                }>
                                  FMCG MASTER
                            </Link>
                              </label>
                            </li>

                            : null}

                          {UPLOAD_MASTER > 0 ? <li >
                            <label>
                              <Link to="/administration/custom/uploadCustomMaster" className={
                                this.isCustomActive("fmcgMaster")
                                  ? "customActive"
                                  : null
                              }>
                                UPLOAD MASTER
                            </Link>
                            </label>
                          </li> : null}
                          {/* NYSAA END */}
                        </ul>

                      </div>
                    </div>
                  </div>



                </li> : null}
              </ul>
            </div> : null}
            {Analytics > 0 ? <div>
              <li
                className={this.isActiveLink("analytics") ? "active" : null}
                data-toggle="collapse"
                data-target="#analytics"
              >
                <div className="sidemenu_icons_div">
                  {/* <svg
                        className="icons"
                        xmlns="http://www.w3.org/2000/svg"
                        width="20"
                        height="23"
                        viewBox="0 0 20 23"
                      >
                        <path
                          fill="#6D6DC9"
                          fillRule="nonzero"
                          d="M.055 21.093v-4.048c-.004-.615.27-1.2.745-1.59a30.748 30.748 0 0 1 4.011-2.76.58.58 0 0 1 .82.26L8.36 18.97l.991-2.356a1.38 1.38 0 1 1 1.302 0l.984 2.359 2.732-6.018a.58.58 0 0 1 .82-.264 30.7 30.7 0 0 1 4.012 2.764c.475.39.749.975.745 1.59v4.05a1.177 1.177 0 0 1-1.177 1.178H1.234a1.18 1.18 0 0 1-1.18-1.18zM5.086 5.184c.115-.425.285-.833.505-1.214l-.568-.81a.75.75 0 0 1 .084-.962l.575-.584a.75.75 0 0 1 .961-.084l.812.568a5.052 5.052 0 0 1 1.222-.507l.17-.975A.75.75 0 0 1 9.588 0h.827a.75.75 0 0 1 .738.62l.17.97c.425.116.833.285 1.214.505l.812-.568a.75.75 0 0 1 .97.087l.584.584a.75.75 0 0 1 .084.961l-.568.811c.22.381.39.79.505 1.214l.975.17a.75.75 0 0 1 .62.74v.826a.75.75 0 0 1-.62.74l-.975.17a5.052 5.052 0 0 1-.505 1.215l.568.812a.75.75 0 0 1-.084.961l-.584.587a.75.75 0 0 1-.961.084l-.818-.58c-.381.22-.79.39-1.214.505l-.17.975a.75.75 0 0 1-.74.62h-.829a.75.75 0 0 1-.738-.62l-.17-.975a5.052 5.052 0 0 1-1.217-.505l-.811.568a.75.75 0 0 1-.968-.072l-.584-.585a.75.75 0 0 1-.084-.96l.568-.812c-.22-.382-.39-.79-.505-1.216l-.975-.17a.75.75 0 0 1-.62-.74v-.827a.75.75 0 0 1 .62-.738l.984-.173zm2.58 1.323a2.334 2.334 0 1 0 4.668 0 2.334 2.334 0 0 0-4.668.002v-.002z"
                        />
                      </svg> */}
                  <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23">
                    <g fill="#6D6DC9" fillRule="evenodd">
                      <path d="M22.938 10.319h-4.724c-.187-1.026-.591-1.927-1.15-2.766l3.294-3.295c1.43 1.647 2.331 3.73 2.58 6.06zM18.214 12.619h4.724c-.56 5.843-5.502 10.35-11.438 10.35a12.29 12.29 0 0 1-2.642-.31l1.554-4.477c.373.063.715.063 1.088.063 3.357.093 6.216-2.394 6.714-5.626zM18.742 2.549l-3.295 3.294a6.81 6.81 0 0 0-2.766-1.15V.031c2.3.249 4.383 1.15 6.06 2.518zM.062 10.319C.591 4.879 4.911.622 10.35.03v4.724a6.731 6.731 0 0 0-5.564 5.564H.062zM8.205 17.468L6.651 21.85C3.015 20.172.435 16.69.062 12.588h4.724c.311 2.113 1.617 3.854 3.42 4.88z" />
                    </g>
                  </svg>
                </div>
                <a className="hoverMenu">
                  Analytics
                <i className="fa fa-angle-down arrow_down_icon" />
                </a>
              </li>
              <ul id="analytics" className="collapse list">
                {Store_Profiling > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/analytics"
                    className={
                      this.isActiveSubLink("")
                        ? "active"
                        : null
                    }
                  >
                    Store Profile
                </a>
                </li> : null}

                {ARS > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href=""
                    className={
                      this.isActiveSubLink("")
                        ? "active"
                        : null
                    }
                  >
                    ARS
                </a>
                </li> : null}
                {manage_rule > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href=""
                    className={
                      this.isActiveSubLink("")
                        ? "active"
                        : null
                    }
                  >
                    Manage Rule Engines
                </a>
                </li> : null}
                {Store_Ranking > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href=""
                    className={
                      this.isActiveSubLink("")
                        ? "active"
                        : null
                    }
                  >
                    Store Ranking
                </a>
                </li> : null}
                {Sales_Achieved > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href=""
                    className={
                      this.isActiveSubLink("")
                        ? "active"
                        : null
                    }
                  >
                    Sales Achieved
                </a>
                </li> : null}
                {Inventory_Classification > 0 ? <div>
                  <li data-toggle="collapse"
                    data-target="#inventory">
                    <a className="collapse_para_list">

                      Inventory Classification
                  <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="inventory" className="collapse list pad-lft-10">
                    {Fast_Moving_Articles > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href=""
                        className={
                          this.isActiveSubLink("")
                            ? "active"
                            : null
                        }
                      >
                        Fast Moving Article
                </a>
                    </li> : null}

                    {Stale_Moving_Articles > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href=""
                        className={
                          this.isActiveSubLink("")
                            ? "active"
                            : null
                        }
                      >
                        Stale Moving Article
                </a>
                    </li> : null}
                    {Moderate_Moving_Articles > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href=""
                        className={
                          this.isActiveSubLink("")
                            ? "active"
                            : null
                        }
                      >
                        Moderate Moving Article
                </a>
                    </li> : null}

                  </ul>
                </div> : null}

              </ul>
            </div> : null
            }                   {Procurement > 0 ? <div>
              <li
                className={this.isActiveLink("purchase") ? "active" : null}
                data-toggle="collapse"
                data-target="#purchase"
              >
                <div className="sidemenu_icons_div">
                  <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 22">
                    <path fill="#6D6DC9" fillRule="nonzero" d="M9.2.297c-.44.046-.809.455-.806.892v5.649h-4.29c-.44.045-.809.454-.806.892v5.054H.806c-.44.045-.808.454-.806.892v4.162c0 .467.428.892.9.892h2.117a3.297 3.297 0 0 0 3.278 2.973 3.297 3.297 0 0 0 3.28-2.973h5.732a3.297 3.297 0 0 0 3.28 2.973 3.297 3.297 0 0 0 3.278-2.973h2.117c.471 0 .9-.425.9-.892v-2.314a.87.87 0 0 0 0-.065v-1.783a.883.883 0 0 0 0-.084V4.45l1.555-.66a.92.92 0 0 0 .544-1.02.928.928 0 0 0-.928-.698.909.909 0 0 0-.328.074l-2.099.892a.914.914 0 0 0-.543.827v8.919h-1.799v-3.27c0-.467-.428-.892-.899-.892h-2.998V6.243c.01-.476-.428-.915-.908-.91-.475.005-.9.44-.89.91v2.379H12.29V7.73c0-.467-.429-.892-.9-.892h-1.198V2.08h5.396v1.19c-.007.47.424.904.899.904.475 0 .906-.434.9-.905V1.19c0-.468-.43-.893-.9-.893H9.199zM5.095 8.622h5.396v4.162H5.096V8.622zm7.195 1.783h7.195v2.379H12.29v-2.379zM1.799 14.568h21.284v.891a.889.889 0 0 0 0 .093v1.394h-1.574a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.923 1.784H9.218a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.922 1.784H1.799v-2.378zm4.496 2.378a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486zm12.291 0a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486z" />
                  </svg>

                </div>
                <a className="collapse_para_list">
                  Procurement
                <i className="fa fa-angle-down arrow_down_icon" />
                </a>
              </li>




              <ul id="purchase" className="collapse list">
                <div>
                  {Configuration > 0 ? <li data-toggle="collapse"
                    data-target="#configList">   <a

                      className="hoverMenu"

                    >Configuration
                      <i className="fa fa-angle-down arrow_down_icon" />
                    </a></li> : null}
                  <ul id="configList" className="collapse list pad-lft-10" >


                    {Dept_Size_Mapping > 0 ? <li
                    // className={this.isActiveLink("replenishment") ? "active" : null}

                    >
                      <a href="#/purchase/departmentSizeMapping"
                        className={
                          this.isActiveSubLink("departmentSizeMapping")
                            ? "active menuSubItem"
                            : "menuSubItem"}>Department Size Mapping
                            </a>
                    </li> : null}

                    {UDF_Setting > 0 ? <li
                    // className={this.isActiveLink("replenishment") ? "active" : null}

                    >
                      <a href="#/purchase/udfSetting"
                        className={
                          this.isActiveSubLink("udfSetting")
                            ? "active menuSubItem"
                            : "menuSubItem"}>UDF Setting
                            </a>
                    </li> : null}

                    {UDF_Mapping > 0 ? <li
                    // className={this.isActiveLink("replenishment") ? "active" : null}

                    >
                      <a href="#/purchase/udfMapping"
                        className={
                          this.isActiveSubLink("udfMapping")
                            ? "active menuSubItem"
                            : "menuSubItem"}>UDF Mapping
                            </a>
                    </li> : null}

                    {Item_UDF_Setting > 0 ? <li
                    // className={this.isActiveLink("replenishment") ? "active" : null}

                    >
                      <a href="#/purchase/itemUdfSetting"
                        className={
                          this.isActiveSubLink("itemUdfSetting")
                            ? "active menuSubItem"
                            : "menuSubItem"}>Item UDF Setting
                            </a>
                    </li> : null}
                    {Item_UDF_Mapping > 0 ? <li
                    // className={this.isActiveLink("replenishment") ? "active" : null}

                    >
                      <a href="#/purchase/itemUdfMapping"
                        className={
                          this.isActiveSubLink("itemUdfMapping")
                            ? "active menuSubItem"
                            : "menuSubItem"}>Item UDF Mapping
                            </a>
                    </li> : null}
                  </ul>
                </div>
                {Purchase_Indent > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/purchase/purchaseIndent"
                    className={
                      this.isActiveSubLink("purchaseIndent")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Purchase Indent
                </a>
                </li> : null}
                {Generics_Purchase_Indent > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/purchase/purchaseIndents"
                    className={
                      this.isActiveSubLink("purchaseIndents")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    New Purchase Indent
                </a>
                </li> : null}

                {Purchase_Orders > 0 ? <div> <li data-toggle="collapse"
                  data-target="#poList">
                  <a className="hoverMenu">
                    Purchase Order
                    <i className="fa fa-angle-down arrow_down_icon" />
                  </a>
                </li>
                  <ul id="poList" className="collapse list pad-lft-10" >
                    {Purchase_Order > 0 ? <li>
                      <a href="#/purchase/purchaseOrder"
                        className={
                          this.isActiveSubLink("purchaseOrder")
                            ? "active menuSubItem"
                            : "menuSubItem"}>Purchase Order </a>
                    </li> : null}
                    {Generics_Purchase_Orders > 0 ? <li>
                      <a href="#/purchase/purchaseOrders"
                        className={
                          this.isActiveSubLink("purchaseOrders")
                            ? "active menuSubItem"
                            : "menuSubItem"}>New Purchase Order </a>
                    </li> : null}
                    {Purchase_Order_With_Upload > 0 ? <li onClick={() => this.onShowHide()}><a className="menuSubItem" href="#/purchase/purchaseOrderWithUpload">Po with Upload</a></li> : null}
                  </ul>
                </div> : null}
                {PI_History > 0 ? <li onClick={() => this.onShowHide()}>
                  <a
                    href="#/purchase/purchaseIndentHistory"
                    className={
                      this.isActiveSubLink("purchaseIndentHistory")
                        ? "active menuSubItem"
                        : "menuSubItem"
                    }
                  >
                    Purchase Indent History
                </a>
                </li> : null}

                {Purchase_Orders_History > 0 ? <div>
                  <li data-toggle="collapse"
                    data-target="#poHistoryList">
                    <a className="hoverMenu">
                      Purchase Order History
                  <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="poHistoryList" className="collapse list pad-lft-10" >
                    {PO_History > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/purchase/purchaseOrderHistory"
                        className={
                          this.isActiveSubLink("purchaseOrderHistory")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Purchase Order History
                </a>
                    </li> : null}
                    {File_Upload_History > 0 ? <li onClick={() => this.onShowHide()}><a className="menuSubItem" href="#/purchase/uploadHistory">Upload History</a></li> : null}

                  </ul></div> : null}



              </ul>
            </div> : null}
            {Data_Sync > 0 ? <li className={this.isActiveLink("dataSync") ? "active" : null}>
              <div className="sidemenu_icons_div">
                <svg
                  className="icons"
                  xmlns="http://www.w3.org/2000/svg"
                  width="22"
                  height="20"
                  viewBox="0 0 22 20"
                >
                  <g fill="#6D6DC9" fillRule="nonzero">
                    <path d="M21.972 8.719a1.71 1.71 0 0 0-.646-1.148L12.342.514a2.181 2.181 0 0 0-2.7 0L.656 7.57a1.723 1.723 0 0 0-.29 2.417c.586.746 1.67.876 2.417.29l7.414-5.824a1.282 1.282 0 0 1 1.587 0l7.415 5.824a1.72 1.72 0 0 0 2.772-1.559" />
                    <path d="M18.575 10.843L11.83 5.584a1.363 1.363 0 0 0-1.678 0l-6.744 5.26a.45.45 0 0 0-.174.355V17.2c0 1.077.876 1.953 1.953 1.953h11.608a1.955 1.955 0 0 0 1.952-1.953V11.2a.45.45 0 0 0-.173-.356" />
                  </g>
                </svg>
              </div>

              <a href="#/dataSync" id="home_toggle" className="">
                Data_Sync
              </a>
            </li> : null}
            {/* _______________________________VENDOR PORTAL SIDEBAR__________________________________ */}
            {Shipment_Tracking > 0 && <div>
              <li
                className={
                  this.isActiveLink("shipmentTracking") ? "active" : null
                } data-toggle="collapse"
                data-target="#shipmentTrack"
              >
                <div className="sidemenu_icons_div" >
                  <svg
                    className="icons"
                    xmlns="http://www.w3.org/2000/svg"
                    width="22"
                    height="26"
                    viewBox="0 0 22 26"
                  >
                    <g fill="#6D6DC9" fillRule="nonzero">
                      <path d="M17.39 14.588c-1.492.34-4.164.681-5.08.393-.393-.183-1.362-1.257-1.886-1.833A25.964 25.964 0 0 0 9.01 11.68c-.76-.76-1.362-1.283-2.462-1.571-1.336-.341-4.688-.734-5.631 1.492C-.08 13.933.079 20.01.13 24.331c0 .367.236.68.629.68h7.15c.445 0 .602-.235.628-.575.026-.236.262-4.348.42-7.281.68.655 1.361 1.205 2.12 1.44.708.236 1.546.315 2.41.315 2.043 0 4.19-.446 4.74-.577 1.022-.235 1.677-1.257 1.441-2.278a1.884 1.884 0 0 0-2.279-1.467z" />
                      <circle cx="5.055" cy="4.479" r="4.348" />
                      <path d="M20.874 3.955H19.8V3.3a.414.414 0 0 0-.419-.419h-.524a.414.414 0 0 0-.419.419v.655h-2.514V3.3a.414.414 0 0 0-.42-.419h-.523a.414.414 0 0 0-.42.419v.655h-1.073a1.05 1.05 0 0 0-1.048 1.047v6.26a1.05 1.05 0 0 0 1.048 1.048h7.36a1.05 1.05 0 0 0 1.047-1.048V4.976c.026-.55-.445-1.021-1.021-1.021zm.445 7.28c0 .236-.21.446-.445.446h-7.36a.459.459 0 0 1-.445-.445V6.364h8.25v4.872z" />
                      <path d="M14.221 8.643h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.261.262v.838c0 .13.13.262.261.262zM16.657 8.643h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .13.105.262.262.262zM19.093 8.643h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .13.105.262.262.262zM14.221 10.817h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.261.262v.838c0 .157.13.262.261.262zM16.657 10.817h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .157.105.262.262.262zM19.093 10.817h1.1a.262.262 0 0 0 .262-.262v-.838a.262.262 0 0 0-.262-.262h-1.1a.262.262 0 0 0-.262.262v.838c0 .157.105.262.262.262z" />
                    </g>
                  </svg>
                </div>

                <a className="hoverMenu">Shipment Tracking
              <i className="fa fa-angle-down arrow_down_icon" /></a>
              </li>
              <ul id="shipmentTrack" className="collapse list">
                {sessionStorage.getItem("uType") == "VENDOR" && <ul className="list">
                  <li> <a
                    href="#/vendor/vendorDashboard"
                    className={"menuSubItem"}
                  >Overview
                      </a></li>
                </ul>}
                {sessionStorage.getItem("uType") == "VENDOR" && Order > 0 ? <div>
                  <li
                    className={this.isActiveLink("order") ? "active" : null}
                    data-toggle="collapse"
                    data-target="#order"
                  >
                    {/* <div className="sidemenu_icons_div">
                      <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 22">
                        <path fill="#6D6DC9" fillRule="nonzero" d="M9.2.297c-.44.046-.809.455-.806.892v5.649h-4.29c-.44.045-.809.454-.806.892v5.054H.806c-.44.045-.808.454-.806.892v4.162c0 .467.428.892.9.892h2.117a3.297 3.297 0 0 0 3.278 2.973 3.297 3.297 0 0 0 3.28-2.973h5.732a3.297 3.297 0 0 0 3.28 2.973 3.297 3.297 0 0 0 3.278-2.973h2.117c.471 0 .9-.425.9-.892v-2.314a.87.87 0 0 0 0-.065v-1.783a.883.883 0 0 0 0-.084V4.45l1.555-.66a.92.92 0 0 0 .544-1.02.928.928 0 0 0-.928-.698.909.909 0 0 0-.328.074l-2.099.892a.914.914 0 0 0-.543.827v8.919h-1.799v-3.27c0-.467-.428-.892-.899-.892h-2.998V6.243c.01-.476-.428-.915-.908-.91-.475.005-.9.44-.89.91v2.379H12.29V7.73c0-.467-.429-.892-.9-.892h-1.198V2.08h5.396v1.19c-.007.47.424.904.899.904.475 0 .906-.434.9-.905V1.19c0-.468-.43-.893-.9-.893H9.199zM5.095 8.622h5.396v4.162H5.096V8.622zm7.195 1.783h7.195v2.379H12.29v-2.379zM1.799 14.568h21.284v.891a.889.889 0 0 0 0 .093v1.394h-1.574a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.923 1.784H9.218a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.922 1.784H1.799v-2.378zm4.496 2.378a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486zm12.291 0a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486z" />
                      </svg>

                    </div> */}
                    <a className="collapse_para_list">
                      Order
                <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="order" className="collapse list pad-lft-10">
                    {Pending_Orders > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/purchaseOrder/pendingOrders"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Pending Orders
                  </a>
                    </li> : null}

                    {Processed_Orders > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/purchaseOrder/processedOrders"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Processed Orders
                  </a>
                    </li> : null}
                    {Cancelled_Order > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/purchaseOrder/cancelledOrders"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Cancelled Orders
                  </a>
                    </li> : null}
                  </ul>
                </div> : null}

                {/* ________________________________SHIPMENT____________________ */}

                {sessionStorage.getItem("uType") == "VENDOR" && Shipment > 0 ? <div>
                  <li
                    className={this.isActiveLink("shipment") ? "active" : null}
                    data-toggle="collapse"
                    data-target="#shipment"
                  >
                    {/* <div className="sidemenu_icons_div">
                      <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 22">
                        <path fill="#6D6DC9" fillRule="nonzero" d="M9.2.297c-.44.046-.809.455-.806.892v5.649h-4.29c-.44.045-.809.454-.806.892v5.054H.806c-.44.045-.808.454-.806.892v4.162c0 .467.428.892.9.892h2.117a3.297 3.297 0 0 0 3.278 2.973 3.297 3.297 0 0 0 3.28-2.973h5.732a3.297 3.297 0 0 0 3.28 2.973 3.297 3.297 0 0 0 3.278-2.973h2.117c.471 0 .9-.425.9-.892v-2.314a.87.87 0 0 0 0-.065v-1.783a.883.883 0 0 0 0-.084V4.45l1.555-.66a.92.92 0 0 0 .544-1.02.928.928 0 0 0-.928-.698.909.909 0 0 0-.328.074l-2.099.892a.914.914 0 0 0-.543.827v8.919h-1.799v-3.27c0-.467-.428-.892-.899-.892h-2.998V6.243c.01-.476-.428-.915-.908-.91-.475.005-.9.44-.89.91v2.379H12.29V7.73c0-.467-.429-.892-.9-.892h-1.198V2.08h5.396v1.19c-.007.47.424.904.899.904.475 0 .906-.434.9-.905V1.19c0-.468-.43-.893-.9-.893H9.199zM5.095 8.622h5.396v4.162H5.096V8.622zm7.195 1.783h7.195v2.379H12.29v-2.379zM1.799 14.568h21.284v.891a.889.889 0 0 0 0 .093v1.394h-1.574a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.923 1.784H9.218a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.922 1.784H1.799v-2.378zm4.496 2.378a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486zm12.291 0a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486z" />
                      </svg>

                    </div> */}
                    <a className="collapse_para_list">
                      Shipment
                <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="shipment" className="collapse list pad-lft-10">
                    {Asn_Under_Approval > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/shipment/asnUnderApproval"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        ASN Under Approval
                  </a>
                    </li> : null}
                    {Approved_Asn > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/shipment/approvedAsn"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Approved ASN
                  </a>
                    </li> : null}
                    {Cancelled_Asn > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/shipment/cancelledAsn"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Cancelled ASN
                  </a>
                    </li> : null}
                  </ul>
                </div> : null}

                {/* ________________________________LOGISTIC____________________________ */}

                {sessionStorage.getItem("uType") === "VENDOR" && Logistics > 0 ? <div>
                  <li
                    className={this.isActiveLink("logistic") ? "active" : null}
                    data-toggle="collapse"
                    data-target="#logistic"
                  >
                    <a className="collapse_para_list">
                      Logistic
                <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="logistic" className="collapse list pad-lft-10">
                    {Lr_Processing > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/logistics/lrProcessing"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        LR Processing
                  </a>
                    </li> : null}
                    {Goods_Intransit > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/logistics/goodsIntransit"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Goods Intransit
                  </a>
                    </li> : null}
                    {Goods_Delivered > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/vendor/logistics/goodsDelivered"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Goods Delivered
                  </a>
                    </li> : null}
                  </ul>
                </div> : null}
                {/*enterprise*/}
                {sessionStorage.getItem("uType") == "ENT" && <ul className="list">
                  <li> <a
                    href="#/enterprise/enterpriseDashboard"
                    className={"menuSubItem"}
                  >Overview
                      </a></li>
                </ul>}
                {sessionStorage.getItem("uType") == "ENT" && Order > 0 ? <div>
                  <li
                    className={this.isActiveLink("order") ? "active" : null}
                    data-toggle="collapse"
                    data-target="#order"
                  >
                    {/* <div className="sidemenu_icons_div">
                      <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 22">
                        <path fill="#6D6DC9" fillRule="nonzero" d="M9.2.297c-.44.046-.809.455-.806.892v5.649h-4.29c-.44.045-.809.454-.806.892v5.054H.806c-.44.045-.808.454-.806.892v4.162c0 .467.428.892.9.892h2.117a3.297 3.297 0 0 0 3.278 2.973 3.297 3.297 0 0 0 3.28-2.973h5.732a3.297 3.297 0 0 0 3.28 2.973 3.297 3.297 0 0 0 3.278-2.973h2.117c.471 0 .9-.425.9-.892v-2.314a.87.87 0 0 0 0-.065v-1.783a.883.883 0 0 0 0-.084V4.45l1.555-.66a.92.92 0 0 0 .544-1.02.928.928 0 0 0-.928-.698.909.909 0 0 0-.328.074l-2.099.892a.914.914 0 0 0-.543.827v8.919h-1.799v-3.27c0-.467-.428-.892-.899-.892h-2.998V6.243c.01-.476-.428-.915-.908-.91-.475.005-.9.44-.89.91v2.379H12.29V7.73c0-.467-.429-.892-.9-.892h-1.198V2.08h5.396v1.19c-.007.47.424.904.899.904.475 0 .906-.434.9-.905V1.19c0-.468-.43-.893-.9-.893H9.199zM5.095 8.622h5.396v4.162H5.096V8.622zm7.195 1.783h7.195v2.379H12.29v-2.379zM1.799 14.568h21.284v.891a.889.889 0 0 0 0 .093v1.394h-1.574a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.923 1.784H9.218a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.922 1.784H1.799v-2.378zm4.496 2.378a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486zm12.291 0a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486z" />
                      </svg>

                    </div> */}
                    <a className="collapse_para_list">
                      Order
                <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="order" className="collapse list pad-lft-10">
                    {Pending_Orders > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/enterprise/purchaseOrder/pendingOrders"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Pending Orders
                  </a>
                    </li> : null}
                    {Processed_Orders > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/enterprise/purchaseOrder/processedOrders"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Processed Orders
                  </a>
                    </li> : null}
                    {Cancelled_Order > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/enterprise/purchaseOrder/cancelledOrders"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Cancelled Order
                  </a>
                    </li> : null}
                  </ul>
                </div> : null}
                {sessionStorage.getItem("uType") == "ENT" && Shipment > 0 ? <div>
                  <li
                    className={this.isActiveLink("shipment") ? "active" : null}
                    data-toggle="collapse"
                    data-target="#shipment"
                  >
                    {/* <div className="sidemenu_icons_div">
                      <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 22">
                        <path fill="#6D6DC9" fillRule="nonzero" d="M9.2.297c-.44.046-.809.455-.806.892v5.649h-4.29c-.44.045-.809.454-.806.892v5.054H.806c-.44.045-.808.454-.806.892v4.162c0 .467.428.892.9.892h2.117a3.297 3.297 0 0 0 3.278 2.973 3.297 3.297 0 0 0 3.28-2.973h5.732a3.297 3.297 0 0 0 3.28 2.973 3.297 3.297 0 0 0 3.278-2.973h2.117c.471 0 .9-.425.9-.892v-2.314a.87.87 0 0 0 0-.065v-1.783a.883.883 0 0 0 0-.084V4.45l1.555-.66a.92.92 0 0 0 .544-1.02.928.928 0 0 0-.928-.698.909.909 0 0 0-.328.074l-2.099.892a.914.914 0 0 0-.543.827v8.919h-1.799v-3.27c0-.467-.428-.892-.899-.892h-2.998V6.243c.01-.476-.428-.915-.908-.91-.475.005-.9.44-.89.91v2.379H12.29V7.73c0-.467-.429-.892-.9-.892h-1.198V2.08h5.396v1.19c-.007.47.424.904.899.904.475 0 .906-.434.9-.905V1.19c0-.468-.43-.893-.9-.893H9.199zM5.095 8.622h5.396v4.162H5.096V8.622zm7.195 1.783h7.195v2.379H12.29v-2.379zM1.799 14.568h21.284v.891a.889.889 0 0 0 0 .093v1.394h-1.574a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.923 1.784H9.218a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.922 1.784H1.799v-2.378zm4.496 2.378a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486zm12.291 0a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486z" />
                      </svg>

                    </div> */}
                    <a className="collapse_para_list">
                      Shipment
                <i className="fa fa-angle-down arrow_down_icon" />
                    </a>
                  </li>
                  <ul id="shipment" className="collapse list pad-lft-10">
                    {Asn_Under_Approval > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/enterprise/shipment/asnUnderApproval"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        ASN Under Approval
                  </a>
                    </li> : null}
                    {Approved_Asn > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/enterprise/shipment/approvedAsn"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Approved ASN
                  </a>
                    </li> : null}
                    {Cancelled_Asn > 0 ? <li onClick={() => this.onShowHide()}>
                      <a
                        href="#/enterprise/shipment/cancelledAsn"
                        className={
                          this.isActiveSubLink("organisation")
                            ? "active menuSubItem"
                            : "menuSubItem"
                        }
                      >
                        Cancelled ASN
                  </a>
                    </li> : null}
                  </ul>
                </div> : null}
                {sessionStorage.getItem("uType") === "ENT" && Logistics > 0 ?
                  <div>
                    <li
                      className={this.isActiveLink("logisticEnterprise") ? "active" : null}
                      data-toggle="collapse"
                      data-target="#logisticEnterprise"
                    >
                      {/* <div className="sidemenu_icons_div">
                        <svg className="icons" xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 22">
                          <path fill="#6D6DC9" fillRule="nonzero" d="M9.2.297c-.44.046-.809.455-.806.892v5.649h-4.29c-.44.045-.809.454-.806.892v5.054H.806c-.44.045-.808.454-.806.892v4.162c0 .467.428.892.9.892h2.117a3.297 3.297 0 0 0 3.278 2.973 3.297 3.297 0 0 0 3.28-2.973h5.732a3.297 3.297 0 0 0 3.28 2.973 3.297 3.297 0 0 0 3.278-2.973h2.117c.471 0 .9-.425.9-.892v-2.314a.87.87 0 0 0 0-.065v-1.783a.883.883 0 0 0 0-.084V4.45l1.555-.66a.92.92 0 0 0 .544-1.02.928.928 0 0 0-.928-.698.909.909 0 0 0-.328.074l-2.099.892a.914.914 0 0 0-.543.827v8.919h-1.799v-3.27c0-.467-.428-.892-.899-.892h-2.998V6.243c.01-.476-.428-.915-.908-.91-.475.005-.9.44-.89.91v2.379H12.29V7.73c0-.467-.429-.892-.9-.892h-1.198V2.08h5.396v1.19c-.007.47.424.904.899.904.475 0 .906-.434.9-.905V1.19c0-.468-.43-.893-.9-.893H9.199zM5.095 8.622h5.396v4.162H5.096V8.622zm7.195 1.783h7.195v2.379H12.29v-2.379zM1.799 14.568h21.284v.891a.889.889 0 0 0 0 .093v1.394h-1.574a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.923 1.784H9.218a3.305 3.305 0 0 0-2.923-1.784c-1.27 0-2.373.73-2.922 1.784H1.799v-2.378zm4.496 2.378a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486zm12.291 0a1.48 1.48 0 0 1 1.5 1.486 1.48 1.48 0 0 1-1.5 1.487 1.48 1.48 0 0 1-1.499-1.487c0-.831.66-1.486 1.5-1.486z" />
                        </svg>

                      </div> */}
                      <a className="collapse_para_list">
                        Logistic
                <i className="fa fa-angle-down arrow_down_icon" />
                      </a>
                    </li>
                    <ul id="logisticEnterprise" className="collapse list pad-lft-10">
                      {Lr_Processing > 0 ? <li onClick={() => this.onShowHide()}>
                        <a
                          href="#/enterprise/logistics/lrProcessing"
                          className={
                            this.isActiveSubLink("organisation")
                              ? "active menuSubItem"
                              : "menuSubItem"
                          }
                        >
                          LR Processing
                  </a>
                      </li> : null}
                      {Goods_Intransit > 0 ? <li onClick={() => this.onShowHide()}>
                        <a
                          href="#/enterprise/logistics/goodsIntransit"
                          className={
                            this.isActiveSubLink("organisation")
                              ? "active menuSubItem"
                              : "menuSubItem"
                          }
                        >
                          Goods Intransit
                  </a>
                      </li> : null}
                      {Goods_Delivered > 0 ? <li onClick={() => this.onShowHide()}>
                        <a
                          href="#/enterprise/logistics/goodsDelivered"
                          className={
                            this.isActiveSubLink("organisation")
                              ? "active menuSubItem"
                              : "menuSubItem"
                          }
                        >
                          Goods Delivered
                  </a>
                      </li> : null}
                    </ul>
                  </div> : null}
              </ul>
            </div>}


          </ul>
          <footer>
            <div className="nav_footer sideBarMenu">
              <label>Supplymint version 1.0</label>
            </div>

          </footer>
        </nav>
        {/* } */}


      </div >
    );
  }
}

export default OldSidebar;
