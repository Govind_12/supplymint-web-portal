import React from 'react';

class Footer extends React.Component {
    
    render() {
        return (
            <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                <div className="page_footer">
                    <p>&copy; 2021 TurningCloud Solutions </p>
                    <span className="footer-links"><a href="http://support.supplymint.com" target="_blank">Help & Support</a> | <a href="https://www.supplymint.com/#/privacy-policy" target="_blank">Privacy</a> | <a href="https://www.supplymint.com/#/terms-of-use" target="_blank">Terms & Conditions</a></span>
                    {/* <div className="col-md-6 col-sm-12">
                        <ul className="list-inline">
                            <li>
                                <label className="home_link">PRIVACY POLICY | TERMS OF USE</label>
                            </li>
                        </ul>
                    </div>
                    <div className="col-md-6 col-sm-12">
                        <ul className="list-inline copyright copyrightText">
                            <li>
                                <label className="home_link">&copy; SUPPLYMINT</label>
                            </li>
                        </ul>
                    </div> */}
                </div>
            </div>
            
        )
    }
}

export default Footer;