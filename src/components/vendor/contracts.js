import React from "react";
import AddVendor from "./Managevendor/addVendor";
import EmptyBox from "./Managevendor/emptyBox";
import ViewVendor from "./Managevendor/viewVendor";
import VendorData from "../../json/vendorData.json";

class Contracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vendorState: [],
      // addVendor: false
    };
  }

  onAddVendor() {
    // this.setState({
    //   addVendor: true
    // });
    this.props.history.push('/vendor/contracts/addVendor')

  }

  onSaveVendor() {
    // this.setState({
      // addVendor: false,
      // vendorState: VendorData
    // });
    this.props.history.push('/vendor/contracts/viewVendor')
  }

  render() {
    
    const hash = window.location.hash.split("/")[3];
    
    return (
      <div className="container-fluid">
         { hash == "emptyBox" ?
             <EmptyBox
             {...this.props}
             vendorState = {this.state.vendorState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addVendor={() => this.onAddVendor()}
            />
           : hash == "viewVendor" ? 
            <ViewVendor
              VendorData={VendorData}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            
         : hash == "addVendor" ?
          <AddVendor
            {...this.props}
            saveVendor={() => this.onSaveVendor()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />: null} 
      </div>
    );
  }
}

export default Contracts;
