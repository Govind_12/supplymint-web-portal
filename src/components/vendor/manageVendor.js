import React from "react";
import AddVendor from "./Managevendor/addVendor";
import EmptyBox from "./Managevendor/emptyBox";
import ViewVendor from "./Managevendor/viewVendor";
import VendorData from "../../json/vendorData.json";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

class ManageVendor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vendorState: [],
      // addVendor: false
      page: 1,
      success: false,
      alert: false,
      loader: false,
      errorMessage: "",
      successMessage: "",
      emptyBox: false,
      viewVendor: false,

      errorCode: "",
      code: "",
    };
  }


  componentWillMount() {
    if (!this.props.vendor.getVendor.isSuccess) {
      // this.props.vendorRequest(this.state.page);
    } else {
      this.setState({
        loader: false
      })
      if (this.props.vendor.getVendor.data.resource == null) {
        this.setState({
          emptyBox: true,
          viewVendor: false
        })
      } else {
        this.setState({
          vendorState: this.props.vendor.getVendor.data.resource,
          emptyBox: false,
          viewVendor: true
        })
      }
    }
  }
  componentWillReceiveProps(nextProps) {
    if (!nextProps.vendor.editVendor.isLoading && !nextProps.vendor.editVendor.isSuccess && !nextProps.vendor.editVendor.isError) {
      this.setState({
        loader: false,

      })
    }
    if (nextProps.vendor.getAllManageVendor.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getAllManageVendorClear()
      if (nextProps.vendor.getAllManageVendor.data.resource == null) {
        this.setState({
          emptyBox: true
        })
      } else {
        this.setState({
          vendorState: nextProps.vendor.getAllManageVendor.data.resource,
          viewVendor: true
        })
      }
    } else if (nextProps.vendor.getAllManageVendor.isError) {
      this.setState({
        alert: true,
        success: false,
        errorCode: nextProps.vendor.getAllManageVendor.message.error == undefined ? undefined : nextProps.vendor.getAllManageVendor.message.error.errorCode,
        errorMessage: nextProps.vendor.getAllManageVendor.message.error == undefined ? undefined : nextProps.vendor.getAllManageVendor.message.error.errorMessage,
        loader: false,

        code: nextProps.vendor.getAllManageVendor.message.status,
      })
      this.props.getAllManageVendorClear()
    } else if (nextProps.vendor.getAllManageVendor.isLoading) {
      this.setState({
        loader: true
      })
    }

    if (nextProps.vendor.deleteVendor.isLoading) {
      this.setState({
        loader: true
      })
    }
    else if (nextProps.vendor.deleteVendor.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.vendor.deleteVendor.data.message,
        alert: false,
      })
      this.props.deleteVendorRequest();
    }
    else if (nextProps.vendor.deleteVendor.isError) {
      this.setState({
        alert: true,
        errorMessage: nextProps.vendor.deleteVendor.error.errorMessage,
        success: false,
        loader: false
      })
      this.props.deleteVendorRequest();
    }

    // //edit  organization modal
    if (nextProps.vendor.editVendor.isLoading) {
      this.setState({
        loader: true
      })

    }
    else if (nextProps.vendor.editVendor.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.vendor.editVendor.data.message,
        alert: false,
      })
      this.props.editVendorRequest();

    }
    else if (nextProps.vendor.editVendor.isError) {
      this.setState({
        alert: true,
        success: false,
        errorCode: nextProps.vendor.editVendor.message.error == undefined ? undefined : nextProps.vendor.editVendor.message.error.errorCode,
        errorMessage: nextProps.vendor.editVendor.message.error == undefined ? undefined : nextProps.vendor.editVendor.message.error.errorMessage,
        loader: false,

        code: nextProps.vendor.editVendor.message.status,
      })
      this.props.editVendorRequest();
    }

    // dynamic header
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getHeaderConfigClear()
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
      this.props.getHeaderConfigClear()
    }

    if (nextProps.vendor.accessVendorPortal.isSuccess) {
      this.setState({
        loader: false
      })
      // this.props.accessVendorPortalClear()
    } else if (nextProps.vendor.accessVendorPortal.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.vendor.accessVendorPortal.message.status,
        errorCode: nextProps.vendor.accessVendorPortal.message.error == undefined ? "NA" : nextProps.vendor.accessVendorPortal.message.error.code,
        errorMessage: nextProps.vendor.accessVendorPortal.message.error == undefined ? "NA" : nextProps.vendor.accessVendorPortal.message.error.message
      })
      //this.props.accessVendorPortalClear()
    }

    if (nextProps.vendor.sendEmail.isSuccess) {
      this.setState({
        success: true,
        loader: false,
        successMessage: nextProps.vendor.sendEmail.data.message,
        alert: false,
      })
      // this.props.sendEmailClear()
    } else if (nextProps.vendor.sendEmail.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.vendor.sendEmail.message.status,
        errorCode: nextProps.vendor.sendEmail.message.error == undefined ? "NA" : nextProps.vendor.sendEmail.message.error.code,
        errorMessage: nextProps.vendor.sendEmail.message.error == undefined ? "NA" : nextProps.vendor.sendEmail.message.error.message
      })
      //this.props.sendEmailClear()
    }

    if (nextProps.vendor.createVendor.isLoading || nextProps.replenishment.getHeaderConfig.isLoading
        || nextProps.vendor.accessVendorPortal.isLoading || nextProps.vendor.sendEmail.isLoading) {
      this.setState({
        loader: true
      })
    }
  }
  onAddVendor() {
    // this.setState({
    //   addVendor: true
    // });
    this.props.history.push('/vendor/manageVendors/addVendor')
  }

  onSaveVendor() {
    this.setState({
      // addVendor: false,
      vendorState: VendorData
    });
    // this.props.history.push('/vendor/manageVendors/viewVendor')
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  render() {
    const hash = window.location.hash.split("/")[3];
    return (
      <div className="container-fluid pad-0">
        {/*{ this.state.emptyBox  ?
             <EmptyBox
             {...this.props}
             vendorState = {this.state.vendorState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addVendor={() => this.onAddVendor()}
            />
           : null}*/}

        {/*{ this.state.viewVendor ? */}
        <ViewVendor
          VendorData={this.state.vendorState}
          {...this.props}
          clickRightSideBar={() => this.props.rightSideBar()}
        />
        {/*: null}*/}

        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default ManageVendor;
