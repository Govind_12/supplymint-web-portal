import React from 'react'

class PendingSignup extends React.Component {
    render() {
        return (
            <div className="vendor-gen-table" >
                <div className="manage-table">
                    <table className="table gen-main-table">
                        <thead>
                            <tr>
                                <th className="fix-action-btn">
                                    <ul className="rab-refresh">
                                        <li className="rab-rinner"><span><img src={require('../../../assets/refresh-block.svg')} /></span></li>
                                    </ul>
                                </th>
                                <th><label>Vendor Code</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Vendor Name</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>User Name</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Contact Person</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Email</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Alternate Email</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Contact Number</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>City Name</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>State Name</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Address</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Shipping Address</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Shipping GST</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Shipping State Code</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Shipping State Name</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Shipping City Name</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>GST</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>State Code</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Due Days</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Due Date Basis</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Delivery Buffdays</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Is Suspended</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>QC REQ</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                                <th><label>Invoice REQ</label><img src={require('../../../assets/headerFilter.svg')} className="imgHead" /></th>
                            </tr>
                            </thead>
                            <tbody>
                                {/* <tr>
                                    <td className="fix-action-btn">
                                        <ul className="table-item-list">
                                            <li className="til-inner til-eye-btn">
                                                <img src={require('../../../assets/eyeIcon.svg')} className="eye" />
                                            </li>
                                            <li className="til-inner til-edit-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                </svg>
                                                <span className="generic-tooltip">Edit</span>
                                            </li>
                                        </ul>
                                    </td>
                                    <td><label>313</label></td>
                                    <td><label>mohit</label></td>
                                    <td><label>ccpurb_1604298295313</label></td>
                                    <td><label>kumar</label></td>
                                    <td><label>mohit@gmail.com	</label></td>
                                    <td><label></label></td>
                                    <td><label>9939000000</label></td>
                                    <td><label></label></td>
                                    <td><label></label></td>
                                    <td><label>#1408 sector 17c, sukhrali	</label></td>
                                    <td><label>2510 , 2nd floor, Dlf Phase 4	</label></td>
                                    <td><label>084567890890890</label></td>
                                    <td><label>08</label></td>
                                    <td><label>Andaman and Nicobar Islands	</label></td>
                                    <td><label>Nicobar</label></td>
                                    <td><label>084567890890890	</label></td>
                                    <td><label>08</label></td>
                                    <td><label>10</label></td>
                                    <td><label>10</label></td>
                                    <td><label>10</label></td>
                                    <td><label></label></td>
                                    <td><label></label></td>
                                    <td><label></label></td>
                                </tr> */}
                                <tr className="tableNoData"><td colSpan="100%"><label>NO DATA FOUND</label></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default PendingSignup;