import React from "react";
import VendorFilter from "../../vendorPortal/vendorFilter";
import EditModal from "./editVendorModal";
import refreshIcon from "../../../assets/refresh.svg";
import ToastLoader from '../../loaders/toastLoader';
import eyeIcon from "../../../assets/eyeIcon.svg";
import filterIcon from '../../../assets/headerFilter.svg';
import searchIcon from '../../../assets/clearSearch.svg';
import SearchImage from '../../../assets/searchicon.svg';
import Reload from '../../../assets/refresh-block.svg';
import AlreadyUser from '../../../assets/alreadyUser.svg';
import ColoumSetting from "../../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../../replenishment/confirmationReset";
import Pagination from "../../pagination";
import ToastError from '../../utils/toastError';
import { CONFIG } from "../../../config/index";
import axios from 'axios';
import PendingSignup from "./pendingSignup";

export default class ViewVendor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prev: "",
      current: "",
      next: "",
      maxPage: "",
      filter: false,
      filterBar: false,
      editModal: false,
      vendorData: [],
      edit: "",
      id: "",
      slCode: "",
      slCityName: "",
      slName: "",
      firstName: "",
      lastName: "",
      middleName: "",
      contactNumber: "",
      email: "",
      slAddress: "",
      search: "",
      toastLoader: false,
      toastMsg: "",
      gstNo: "",

      // Dyanmic Header
      coloumSetting: false,
      getHeaderConfig: [],
      fixedHeader: [],
      customHeaders: {},
      headerConfigState: {},
      headerConfigDataState: {},
      fixedHeaderData: [],
      customHeadersState: [],
      headerState: {},
      headerSummary: [],
      defaultHeaderMap: [],
      confirmModal: false,
      headerMsg: "",
      paraMsg: "",
      headerCondition: false,
      tableCustomHeader: [],
      tableGetHeaderConfig: [],
      saveState: [],
      // Header Filter
      filterKey: "",
      filterType: "",
      prevFilter: "",
      filterItems: {},
      checkedFilters: [],
      jumpPage: 1,
      inputBoxEnable: false,
      tagState: false,
      filterValueForTag: [],
      filteredValue: [],
      // fromSlCode: "",
      // toSlCode : "",
      fromDueDays : "",
      toDueDays : "",
      fromDeliveryBuffDays : "",
      toDeliveryBuffDays : "",
      active: 0,

      exportToExcel: false,
      toastError: false,
      toastErrorMsg: "",
      confirmed : "notConfirm",
      type: 1,
      totalVendor: 0,
      pendingSignup : false,
    };
  }
  showPendingSignup(e) {
    e.preventDefault();
    this.setState({
      pendingSignup: !this.state.pendingSignup
    });
  }
  componentDidMount() {
    let payload = {
      pageNo: 1,
      type: 1,
      search: "",
      isActive: "1",
    }
    this.props.getAllManageVendorRequest(payload)

    let data = {
      enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
      attributeType: "TABLE HEADER",
      displayName: this.state.confirmed == "notConfirm" ? "MANAGE_VENDOR_ALL" : "MANAGE_VENDOR_INACTIVE", //PENDING KEY
      basedOn: "ALL"
    }
    this.props.getHeaderConfigRequest(data)
    document.addEventListener("keydown", this.escFunction, false);
    document.addEventListener("click", this.escFunction, false);

    sessionStorage.setItem('currentPage', "MDMMAIN")
    sessionStorage.setItem('currentPageName', "Vendors")
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.escFunction, false);
    document.removeEventListener("click", this.escFunction, false);
  }

  openEditModal(id) {
    let vendorData = this.state.vendorData
    for (let i = 0; i < vendorData.length; i++) {
      if (vendorData[i].id == id) {
        this.setState({
          firstName: vendorData[i].firstName,
          // lastName: vendorData[i].lastName,
          // middleName: vendorData[i].middleName,
          slCityName: vendorData[i].slCityName,
          email: vendorData[i].email,
          contactNumber: vendorData[i].contactNumber,
          slCode: vendorData[i].slCode,
          slName: vendorData[i].slName,
          slAddress: vendorData[i].slAddress,
          gstNo: vendorData[i].gstInNo,
          active: vendorData[i].isActive
        })
      }
    }
    this.setState({
      editId: id,
      editModal: !this.state.editModal
    });
  }
  close() {
    this.setState({
      editModal: false
    })
  }
  openFilter(e) {
    e.preventDefault();
    this.setState({
      filter: true,
      filterBar: !this.state.filterBar
    });
  }
  onClearSearch(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: ""
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.vendorRequest(data);
  }


  onSearch = (e) => {
    if (e.target.value.trim().length) {
      if (e.target.value != "" && e.key == "Enter") {
        let payload = {
          pageNo: this.state.current,
          type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
          search: e.target.value,
          status: "SHIPMENT_SHIPPED",
          filter: this.state.filteredValue,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
          isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
        }
        this.props.getAllManageVendorRequest(payload)
        this.setState({
          type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true, filterBar: false,
          filter: false, editModal: false, coloumSetting: false
        })
      }
    }
    this.setState({ search: e.target.value }, () => {
      if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
        let payload = {
          pageNo: this.state.current,
          type: this.state.type == 4 ? 2 : 1,
          search: "",
          filter: this.state.filteredValue,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
          isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
        }
        this.props.getAllManageVendorRequest(payload)
        this.setState({
          search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false,
          filterBar: false, filter: false, editModal: false, coloumSetting: false
        })
      }
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.vendor.getAllManageVendor.isSuccess) {
      this.setState({
        vendorData: nextProps.vendor.getAllManageVendor.data.resource,
        prev: nextProps.vendor.getAllManageVendor.data.prePage,
        current: nextProps.vendor.getAllManageVendor.data.currPage,
        next: nextProps.vendor.getAllManageVendor.data.currPage + 1,
        maxPage: nextProps.vendor.getAllManageVendor.data.maxPage,
        jumpPage: nextProps.vendor.getAllManageVendor.data.currPage,
        totalVendor: nextProps.vendor.getAllManageVendor.data.resultedDataCount
      })
    } else if (nextProps.vendor.getAllManageVendor.isErrror) {
      this.setState({
        vendorData: null
      })

    }
    if (nextProps.vendor.editVendor.isSuccess) {
      let payload = {
        type: 1,
        no: 1,
        search: ""
      }
      this.props.vendorRequest(payload)
    }
    // Dynamic Header 
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "ALL") {
        let getHeaderConfig = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"])
        let fixedHeader = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"])
        let customHeadersState = Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"])
        this.setState({
          filterItems: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
          loader: false,
          customHeaders: this.state.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"],
          getHeaderConfig,
          fixedHeader,
          customHeadersState,
          headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
          fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"],
          headerConfigDataState: { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] },
          headerSummary: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
          defaultHeaderMap: Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
          mandateHeaderMain: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
        })
        setTimeout(() => {
          this.setState({
            headerCondition: this.state.customHeadersState.length == 0 ? true : false,
          })
        }, 1000);
      }
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
    }

    if (nextProps.replenishment.createHeaderConfig.isSuccess) {
      this.setState({
        successMessage: nextProps.replenishment.createHeaderConfig.data.message,
        loader: false,
        success: true,
      })
      if (nextProps.replenishment.createHeaderConfig.data.basedOn == "ALL") {
        let payload = {
          enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
          attributeType: "TABLE HEADER",
          displayName: this.state.confirmed == "notConfirm" ? "MANAGE_VENDOR_ALL" : "MANAGE_VENDOR_INACTIVE", //PENDING KEY
          basedOn: "ALL"
        }
        this.props.getHeaderConfigRequest(payload)
      }
      this.props.createHeaderConfigClear()
    } else if (nextProps.replenishment.createHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.createHeaderConfig.message.status,
        errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
      })
      this.props.createHeaderConfigClear()
    }
    if (nextProps.vendor.accessVendorPortal.isSuccess) {
        if(nextProps.vendor.accessVendorPortal.data.resource !== null && nextProps.vendor.accessVendorPortal.data.resource.token !== null && 
          nextProps.vendor.accessVendorPortal.data.resource.token !== undefined && nextProps.vendor.accessVendorPortal.data.resource.token !== "")
        {
          this.close();
          let url = "/#/?accessVendorPortalToken="+ nextProps.vendor.accessVendorPortal.data.resource.token
          window.open(url);
        }
        this.props.accessVendorPortalClear()
    }
    else if (nextProps.vendor.accessVendorPortal.isError) {
      this.close();
      this.props.accessVendorPortalClear()
    }
    if (nextProps.vendor.sendEmail.isSuccess) {
       this.close();
       this.props.sendEmailClear()
    }
    else if (nextProps.vendor.sendEmail.isError) {
      this.close();
      this.props.sendEmailClear()
    }
  }

  getAnyPage = _ => {
    if (_.target.validity.valid) {
      this.setState({ jumpPage: _.target.value })
      if (_.key == "Enter" && _.target.value != "" && _.target.value != this.state.current) {
        let payload = {
          pageNo: _.target.value,
          type: this.state.type,
          search: this.state.search,
          filter: this.state.filteredValue,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
          isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
        }
        this.props.getAllManageVendorRequest(payload)
      }
      else {
        this.setState({
          toastMsg: "Page No should not be empty..",
          toastLoader: true
        })
        setTimeout(() => {
          this.setState({
            toastLoader: false
          })
        }, 3000);
      }
    }
  }

  page(e) {
    if (e.target.id == "prev") {
      if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
      } else {
        this.setState({
          prev: this.props.vendor.getAllManageVendor.data.prePage,
          current: this.props.vendor.getAllManageVendor.data.currPage,
          next: this.props.vendor.getAllManageVendor.data.currPage + 1,
          maxPage: this.props.vendor.getAllManageVendor.data.maxPage,
        })
        if (this.props.vendor.getAllManageVendor.data.currPage != 0) {
          let data = {
            type: this.state.type,
            pageNo: this.props.vendor.getAllManageVendor.data.currPage - 1,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: this.state.filteredValue,
            isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
          }
          this.props.getAllManageVendorRequest(data);
        }
      }
    } else if (e.target.id == "next") {
      this.setState({
        prev: this.props.vendor.getAllManageVendor.data.prePage,
        current: this.props.vendor.getAllManageVendor.data.currPage,
        next: this.props.vendor.getAllManageVendor.data.currPage + 1,
        maxPage: this.props.vendor.getAllManageVendor.data.maxPage,
      })
      if (this.props.vendor.getAllManageVendor.data.currPage != this.props.vendor.getAllManageVendor.data.maxPage) {
        let data = {
          type: this.state.type,
          pageNo: this.props.vendor.getAllManageVendor.data.currPage + 1,
          search: this.state.search,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
          filter: this.state.filteredValue,
          isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" ,
        }
        this.props.getAllManageVendorRequest(data)
      }
    }
    else if (e.target.id == "first") {
      if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
      }
      else {
        this.setState({
          prev: this.props.vendor.getAllManageVendor.data.prePage,
          current: this.props.vendor.getAllManageVendor.data.currPage,
          next: this.props.vendor.getAllManageVendor.data.currPage + 1,
          maxPage: this.props.vendor.getAllManageVendor.data.maxPage,
        })
        if (this.props.vendor.getAllManageVendor.data.currPage <= this.props.vendor.getAllManageVendor.data.maxPage) {
          let data = {
            type: this.state.type,
            pageNo: 1,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: this.state.filteredValue,
            isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
          }
          this.props.getAllManageVendorRequest(data)
        }
      }
    } else if (e.target.id == "last") {
      if (this.state.current == this.state.maxPage || this.state.current == undefined) {
      }
      else {
        this.setState({
          prev: this.props.vendor.getAllManageVendor.data.prePage,
          current: this.props.vendor.getAllManageVendor.data.currPage,
          next: this.props.vendor.getAllManageVendor.data.currPage + 1,
          maxPage: this.props.vendor.getAllManageVendor.data.maxPage,
        })
        if (this.props.vendor.getAllManageVendor.data.currPage <= this.props.vendor.getAllManageVendor.data.maxPage) {
          let data = {
            type: this.state.type,
            pageNo: this.props.vendor.getAllManageVendor.data.maxPage,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: this.state.filteredValue,
            isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
          }
          this.props.getAllManageVendorRequest(data)
        }
      }
    }
  }


  onClearSearch(e) {
    if (this.state.type == 3 || this.state.type == 4) {
      let payload = {
        pageNo: 1,
        type: this.state.type == 4 ? 2 : 1,
        search: "",
        filter: this.state.filteredValue,
        sortedBy: this.state.filterKey,
        sortedIn: this.state.filterType,
        isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" ,
      }
      this.props.getAllManageVendorRequest(payload)
      this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
    } else {
      this.setState({ search: "" })
    }
  }
  onRefresh() {
    let payload = {
      pageNo: 1,
      type: 1,
      search: "",
      isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
    }
    this.props.getAllManageVendorRequest(payload)
  }

  openColoumSetting(data) {
    if (this.state.customHeadersState.length == 0) {
      this.setState({
        headerCondition: true
      }, () => document.addEventListener('click', this.closeColumnSetting))
    }
    if (data == "true") {
      this.setState({
        coloumSetting: true
      }, () => document.addEventListener('click', this.closeColumnSetting))
    } else {
      this.setState({
        coloumSetting: false
      }, () => document.removeEventListener('click', this.closeColumnSetting))
    }
  }

  closeColumnSetting = (e) => {
    if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
      this.setState({ coloumSetting: false }, () =>
        document.removeEventListener('click', this.closeColumnSetting))
    }
  }

  pushColumnData(data) {
    let getHeaderConfig = this.state.getHeaderConfig
    let customHeadersState = this.state.customHeadersState
    let headerConfigDataState = this.state.headerConfigDataState
    let customHeaders = this.state.customHeaders
    let saveState = this.state.saveState
    if (this.state.headerCondition) {
      if (!data.includes(getHeaderConfig) || this.state.getHeaderConfig.length == 0) {
        getHeaderConfig.push(data)
        if (!getHeaderConfig.includes(headerConfigDataState)) {
          let keyget = (_.invert(headerConfigDataState))[data];
          Object.assign(customHeaders, { [keyget]: data })
          saveState.push(keyget)
        }
        if (!Object.keys(customHeaders).includes(this.state.defaultHeaderMap)) {
          let keygetvalue = (_.invert(headerConfigDataState))[data];
          this.state.defaultHeaderMap.push(keygetvalue)
        }
      }
    } else {
      if (!data.includes(customHeadersState) || this.state.customHeadersState.length == 0) {
        customHeadersState.push(data)
        if (!customHeadersState.includes(headerConfigDataState)) {
          let keyget = (_.invert(headerConfigDataState))[data];
          Object.assign(customHeaders, { [keyget]: data })
          saveState.push(keyget)
        }
        if (!Object.keys(customHeaders).includes(this.state.headerSummary)) {
          let keygetvalue = (_.invert(headerConfigDataState))[data];
          this.state.headerSummary.push(keygetvalue)
        }
      }
    }
    this.setState({
      getHeaderConfig,
      customHeadersState,
      customHeaders,
      saveState
    })
  }
  closeColumn(data) {
    let getHeaderConfig = this.state.getHeaderConfig
    let headerConfigState = this.state.headerConfigState
    let customHeaders = []
    let customHeadersState = this.state.customHeadersState
    if (!this.state.headerCondition) {
      for (let j = 0; j < customHeadersState.length; j++) {
        if (data == customHeadersState[j]) {
          customHeadersState.splice(j, 1)
        }
      }
      for (var key in headerConfigState) {
        if (!customHeadersState.includes(headerConfigState[key])) {
          customHeaders.push(key)
        }
      }
      if (this.state.customHeadersState.length == 0) {
        this.setState({
          headerCondition: false
        })
      }
    } else {
      for (var i = 0; i < getHeaderConfig.length; i++) {
        if (data == getHeaderConfig[i]) {
          getHeaderConfig.splice(i, 1)
        }
      }
      for (var key in headerConfigState) {
        if (!getHeaderConfig.includes(headerConfigState[key])) {
          customHeaders.push(key)
        }
      }
    }
    customHeaders.forEach(e => delete headerConfigState[e]);
    this.setState({
      getHeaderConfig,
      customHeaders: headerConfigState,
      customHeadersState,
    })
    setTimeout(() => {
      let keygetvalue = (_.invert(this.state.headerConfigDataState))[data];
      let saveState = this.state.saveState
      saveState.push(keygetvalue)
      let headerSummary = this.state.headerSummary
      let defaultHeaderMap = this.state.defaultHeaderMap
      if (!this.state.headerCondition) {
        for (let j = 0; j < headerSummary.length; j++) {
          if (keygetvalue == headerSummary[j]) {
            headerSummary.splice(j, 1)
          }
        }
      } else {
        for (let i = 0; i < defaultHeaderMap.length; i++) {
          if (keygetvalue == defaultHeaderMap[i]) {
            defaultHeaderMap.splice(i, 1)
          }
        }
      }
      this.setState({
        headerSummary,
        defaultHeaderMap,
        saveState
      })
    }, 100);
  }
  saveColumnSetting(e) {
    this.setState({
      coloumSetting: false,
      headerCondition: false,
      saveState: []
    })
    let payload = {
      module: "MANAGE_VENDOR",
      subModule: "VENDOR",
      section: "MANAGE-VENDOR",
      source: "WEB-APP",
      typeConfig: "PORTAL",
      attributeType: "TABLE HEADER",
      displayName: this.state.confirmed == "notConfirm" ? "MANAGE_VENDOR_ALL" : "MANAGE_VENDOR_INACTIVE", //PENDING KEY
      basedOn: "ALL",
      fixedHeaders: this.state.fixedHeaderData,
      defaultHeaders: this.state.headerConfigDataState,
      customHeaders: this.state.customHeaders
    }
    this.props.createHeaderConfigRequest(payload)
  }
  resetColumnConfirmation() {
    this.setState({
      headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
      confirmModal: true,
    })
  }
  resetColumn() {
    let payload = {
      module: "MANAGE_VENDOR",
      subModule: "VENDOR",
      section: "MANAGE-VENDOR",
      source: "WEB-APP",
      typeConfig: "PORTAL",
      attributeType: "TABLE HEADER",
      displayName: this.state.confirmed == "notConfirm" ? "MANAGE_VENDOR_ALL" : "MANAGE_VENDOR_INACTIVE", //PENDING KEY
      basedOn: "ALL",
      fixedHeaders: this.state.fixedHeaderData,
      defaultHeaders: this.state.headerConfigDataState,
      customHeaders: this.state.headerConfigDataState
    }
    this.props.createHeaderConfigRequest(payload)
    this.setState({
      headerCondition: true,
      coloumSetting: false,
      saveState: []
    })
  }
  closeConfirmModal(e) {
    this.setState({
      confirmModal: !this.state.confirmModal,
    })
  }

  filterHeader = (event) => {
    var data = event.target.dataset.key
    if (event.target.closest("th").classList.contains("rotate180"))
      event.target.closest("th").classList.remove("rotate180")
    else
      event.target.closest("th").classList.add("rotate180")

    var def = { ...this.state.headerConfigDataState };
    var filterKey = ""
    Object.keys(def).some(function (k) {
      if (def[k] == data) {
        filterKey = k
      }
    })
    if (this.state.prevFilter == data) {
      this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
    } else {
      this.setState({ filterKey, filterType: "ASC" })
    }
    this.setState({ prevFilter: data }, () => {
      let payload = {
        pageNo: this.state.current,
        type: this.state.type,
        search: this.state.search,
        sortedBy: this.state.filterKey,
        sortedIn: this.state.filterType,
        filter: this.state.filteredValue,
        isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
      }
      this.props.getAllManageVendorRequest(payload)
    })
  }

  openFilter(e) {
    e.preventDefault();
    this.setState({
      filter: !this.state.filter,
      filterBar: !this.state.filterBar
    });
  }

  closeFilter(e) {
    this.setState({
      filter: false,
      filterBar: false
    });
  }

  handleInputBoxEnable = (e, data) => {
    this.setState({ inputBoxEnable: true })
    this.handleCheckedItems(e, this.state.filterItems[data])
  }

  handleCheckedItems = (e, data) => {
    let array = [...this.state.checkedFilters]
    let len = Object.values(this.state.filterValueForTag).length > 0;
    if (this.state.checkedFilters.some((item) => item == data)) {
      array = array.filter((item) => item != data)
      delete this.state.filterValueForTag[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)]
      this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = "";
      this.setState({ [data]: "" })
      let flag = array.some(data => this.state[data] == "" || this.state[data] == undefined)
      if (!flag && len) {
        this.state.checkedFilters.some((item, index) => {
          if (item == data) {
            this.clearTag(e, index)
          }
        })
      }

    } else {
      array.push(data)
    }
    var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
    this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
  }

  handleInput = (event, filterName) => {
    if (event != null) {
      let id = event.target.id;
      let value = event.target.value;
      // if (event.target.id === "fromslCode"){
      //   this.setState({ fromSlCode: value }, () => {
      //       document.getElementById(id).setAttribute("value", this.state.fromSlCode);
      //       this.handleFromAndToValue(event)
      //   })
      // }
      // else if (event.target.id === "toslCode"){
      //   this.setState({ toSlCode: value }, () => {
      //       document.getElementById(id).setAttribute("value", this.state.toSlCode);
      //       this.handleFromAndToValue(event)
      //   })
      // }
      if (event.target.id === "fromdueDays") {
        this.setState({ fromDueDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.fromDueDays);
          this.handleFromAndToValue(event)
        })
      }
      else if (event.target.id === "todueDays") {
        this.setState({ toDueDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.toDueDays);
          this.handleFromAndToValue(event)
        })
      }
      else if (event.target.id === "fromdeliveryBuffDays") {
        this.setState({ fromDeliveryBuffDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.fromDeliveryBuffDays);
          this.handleFromAndToValue(event)
        })
      }
      else if (event.target.id === "todeliveryBuffDays") {
        this.setState({ toDeliveryBuffDays: value }, () => {
          document.getElementById(id).setAttribute("value", this.state.toDeliveryBuffDays);
          this.handleFromAndToValue(event)
        })
      }
      else
        this.handleFromAndToValue(event);
    }
  }

  handleFromAndToValue = () => {
    var value = "";
    var name = event.target.dataset.value;

    // if (event.target.id === "fromslCode" || event.target.id === "toslCode")
    //     value = this.state.fromSlCode + " | " + this.state.toSlCode
    if (event.target.id === "fromdueDays" || event.target.id === "todueDays")
      value = this.state.fromDueDays + " | " + this.state.toDueDays
    else if (event.target.id === "fromdeliveryBuffDays" || event.target.id === "todeliveryBuffDays")
      value = this.state.fromDeliveryBuffDays + " | " + this.state.toDeliveryBuffDays
    else
      value = event.target.value

    if (/^\s/g.test(value)) {
      value = value.replace(/^\s+/, '');
    }

    this.setState({ [name]: value, applyFilter: true }, () => {
      if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
        this.setState({ applyFilter: false })
      } else {
        this.setState({ applyFilter: true })
      }
      this.state.filteredValue[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === name)] = this.state[name];
    })
  }

  submitFilter = () => {
    let payload = {}
    let filtervalues = {}
    this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))

    //reset min and max value field that are not checked::
    // if( payload.slCode === undefined ){
    //   this.setState({fromSlCode: "", toSlCode: "" })
    // }
    if (payload.dueDays === undefined) {
      this.setState({ fromDueDays: "", toDueDays: "" })
    }
    if (payload.deliveryBuffDays === undefined) {
      this.setState({ fromDeliveryBuffDays: "", toDeliveryBuffDays: "" })
    }

    Object.keys(payload).map((data) => (data.includes("dueDays") || data.includes("deliveryBuffDays"))
      && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() }))

    this.state.checkedFilters.map((data) => (filtervalues[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
    Object.keys(filtervalues).map((data) => (data.includes("dueDays") || data.includes("deliveryBuffDays"))
      && (filtervalues[data] = filtervalues[data] == "" ? "" : { from: filtervalues[data].split("|")[0].trim(), to: filtervalues[data].split("|")[1].trim() }))

    let data = {
      pageNo: this.state.current == 0 ? 1 : this.state.current,
      type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
      search: this.state.search,
      filter: payload,
      sortedBy: this.state.filterKey,
      sortedIn: this.state.filterType,
      isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
    }
    this.props.getAllManageVendorRequest(data)

    this.setState({
      filter: false,
      filteredValue: payload,
      filterValueForTag: filtervalues,
      type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
      tagState: true
    })
  }

  clearFilter = () => {
    if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
      let payload = {
        pageNo: this.state.current == 0 ? 1 : this.state.current,
        type: this.state.type == 4 ? 3 : 1,
        search: this.state.search,
        sortedBy: this.state.filterKey,
        sortedIn: this.state.filterType,
        isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
      }
      this.props.getAllManageVendorRequest(payload)
    }
    this.setState({
      filteredValue: [],
      type: this.state.type == 4 ? 3 : 1,
      selectAll: false,
      filterValueForTag: [],
      // fromSlCode: "",
      // toSlCode: "",
      fromDueDays: "",
      toDueDays: "",
      fromDeliveryBuffDays: "",
      toDeliveryBuffDays: "",
    })
    this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
  }

  clearTag = (e, index) => {
    let deleteItem = this.state.checkedFilters;
    let deletedItem = this.state.checkedFilters[index];
    deleteItem.splice(index, 1)
    this.setState({
      checkedFilters: deleteItem,
      [deletedItem]: "",
    }, () => {
      if (this.state.checkedFilters.length == 0)
        this.clearFilter();
      else
        this.submitFilter();
    })
  }

  clearAllTag = (e) => {
    this.setState({
      checkedFilters: [],
    }, () => {
      this.clearFilter();
      this.clearFilterOutside();
    })
  }

  clearFilterOutside = () => {
    this.setState({
      filteredValue: [],
      selectAll: false,
      checkedFilters: []
    })
  }

  addVednor = () => {
    this.props.history.push('/vendor/manageVendors/addVendor')
    this.props.updateCreatedVendorRequest({})
  }
  updateVendor = (vendorDetails) => {
    this.props.history.push('/vendor/manageVendors/addVendor')
    this.props.updateCreatedVendorRequest(vendorDetails)
  }
  escFunction = (e) => {
    if (e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))) {
      this.setState({ filter: false, editModal: false, coloumSetting: false, exportToExcel: false, toastError: false })
    }
  }

  //For Address Tooltip::
  small = (str) => {
    if (str != null) {
      if (str.length <= 30) {
        return false;
      }
      return true;
    }
  }

  openExportToExcel(e) {
    e.preventDefault();
    this.setState({
        exportToExcel: !this.state.exportToExcel,
        toastLoader: false
    }, () => document.addEventListener('click', this.closeExportToExcel));
  }

  closeExportToExcel = () => {
    this.setState({ exportToExcel: false, }, () => {
        document.removeEventListener('click', this.closeExportToExcel);
    });
  }

  xlscsv() {
      let headers = {
          'X-Auth-Token': sessionStorage.getItem('token'),
          'Content-Type': 'application/json'
      }
      let payload = {
          pageNo: this.state.current,
          type: this.state.type,
          search: this.state.search,
          status: this.state.status,
          sortedBy: this.state.sortedBy || "",
          sortedIn: this.state.sortedIn || "",
          isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" 
      }
      let filter = { ...this.state.filteredValue }
      let final = { ...payload, filter}
      let selectAllFlag = false;
      let response = ""
      let module = this.state.confirmed == "notConfirm" ? "MANAGE_VENDOR_ALL" : "MANAGE_VENDOR_INACTIVE"; //PENDING KEY
      axios.post(`${CONFIG.BASE_URL}/download/module/data?fileType=XLS&module=${module}&isAllData=false&isOnlyCurrentPage=${selectAllFlag}`, final, { headers: headers })
      .then(res => {
          response = res
          window.open(`${res.data.data.resource}`)
      }).catch((error) => {
          this.setState({ toastError: true, toastErrorMsg: response.data.error.errorMessage}) 
          setTimeout(()=>{
              this.setState({
                  toastError: false
              })
          }, 5000)
      });
  }

  closeToastError = () => {
    this.setState({ toastError: false })
  }

  onConfirmed = (e, type) => {
    this.setState({
        confirmed: type,
    }, () => {
        let payload = {
          pageNo: 1,
          type: this.state.type == 2 || this.state.type == 4 ? 4 : this.state.search.length ? 3 : 1,
          search: this.state.search,
          filter: this.state.filteredValue,
          sortedBy: this.state.filterKey,
          sortedIn: this.state.filterType,
          isActive: this.state.confirmed == "notConfirm" ? "1" : this.state.confirmed == "confirm" ? "0" : "2" ,
        }
        this.props.getAllManageVendorRequest(payload)

        let data = {
          enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
          attributeType: "TABLE HEADER",
          displayName: this.state.confirmed == "notConfirm" ? "MANAGE_VENDOR_ALL" : "MANAGE_VENDOR_INACTIVE", //PENDING KEY
          basedOn: "ALL"
        }
        this.props.getHeaderConfigRequest(data) 
      })
  }

  render() {
    return (

      <div className="container-fluid pad-0">
        <div className="col-lg-12 pad-0 ">
          <div className="gen-vendor-potal-design p-lr-47">
            <div className="col-lg-8 pad-0">
              <div className="gvpd-left">
                <div className="gvpd-search">
                  <input type="search" onChange={this.onSearch} onKeyDown={this.onSearch} value={this.state.search} placeholder={this.state.search == "" ? "Type to Search" : this.state.search} className="search_bar" />
                  <img className="search-image" src={SearchImage} />
                  {this.state.search != "" && <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></span>}
                </div>
                <div className="drop-toggle-btn">
                  <button type="button" className="dtb-show">{this.state.confirmed === "confirm" ? "Vendor Portal In-Active" : this.state.confirmed === "notConfirm" ? "Vendor Portal Active" : "Pending Sign-Up"} <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                  <div className="dtb-dropdown">
                    <button type="button" className={this.state.confirmed === "notConfirm" ? "dtb-show" : "dtb-hide"} onClick={(e) => this.onConfirmed(e, "notConfirm")} value={this.state.confirmed === "notConfirm"}>Vendor Portal Active <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                    <button type="button" className={this.state.confirmed === "confirm" ? "dtb-show" : "dtb-hide"} onClick={(e) => this.onConfirmed(e, "confirm")} value={this.state.confirmed === "confirm"}>Vendor Portal In-Active <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                    <button type="button" className={this.state.confirmed === "pending" ?"dtb-show" : "dtb-hide"} onClick={(e) => this.onConfirmed(e, "pending")} value={this.state.confirmed === "pending"}>Pending Sign-Up <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 pad-0">
              <div className="gvpd-right">
                <div className="tooltip" onClick={this.addVednor}>
                  <span className="add-btn"><img src={require('../../../assets/add-green.svg')} />
                    <span className="generic-tooltip">Add Vendor</span>
                  </span>
                </div>
                <div className="gvpd-download-drop">
                  <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                          <g>
                              <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                              <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                              <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                          </g>
                      </svg>
                      <span className="generic-tooltip">Export Excel</span>
                  </button>
                  {this.state.exportToExcel &&
                      <ul className="pi-history-download">
                          <li>
                              <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                  <span className="pi-export-svg">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                          <g id="prefix__files" transform="translate(0 2)">
                                              <g id="prefix__Group_2456" data-name="Group 2456">
                                                  <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                      <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                      <text fontSize="7px" fontFamily="ProximaNova-Bold,Proxima Nova" fontWeight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                          <tspan x="0" y="0">XLS</tspan>
                                                      </text>
                                                  </g>
                                              </g>
                                          </g>
                                      </svg>
                                  </span>
                                  Export to Excel</button>
                          </li>
                      </ul>}
                </div>
                <div className="gvpd-filter">
                  <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                      <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                    </svg>
                    <span className="generic-tooltip">Filter</span>
                    {/* {this.state.filterCount != 0 && <p className="noOfFilter">{this.state.filterCount}</p>} */}
                  </button>
                  {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null}
                  {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-lg-12 p-lr-47">
          {this.state.tagState ? this.state.checkedFilters.map((keys, index) => (
            <div className="show-applied-filter">
              {(Object.values(this.state.filterValueForTag)[index]) !== undefined && index === 0 ? <button type="button" className="saf-clear-all" onClick={(e) => this.clearAllTag(e)}>Clear All</button> : null}
              {(Object.values(this.state.filterValueForTag)[index]) !== undefined && <button type="button" className="saf-btn">{keys}
                <img onClick={(e) => this.clearTag(e, index)} src={require('../../../assets/clearSearch.svg')} />
                <span className="generic-tooltip">{typeof (Object.values(this.state.filterValueForTag)[index]) == 'object' ? Object.values(Object.values(this.state.filterValueForTag)[index]).join(',') : Object.values(this.state.filterValueForTag)[index]}</span>
              </button>}
            </div>)) : ''}
        </div>

        <div className="col-md-12 pad-0 p-lr-47">
          {this.state.pendingSignup === false ?
          <div className="vendor-gen-table" >
            <div className="manage-table">
              {/* <ColoumSetting {...this.state} {...this.props} openColoumSetting={(e) => this.openColoumSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} /> */}
              <table className="table gen-main-table">
                <thead>
                  <tr>
                    <th className="fix-action-btn">
                      <ul className="rab-refresh">
                        <li className="rab-rinner">
                          <span><img onClick={() => this.onRefresh()} src={Reload} /></span>
                        </li>
                      </ul>
                    </th>
                    {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                      <th key={key} data-key={data} onClick={this.filterHeader}>
                        <label data-key={data}>{data}</label>
                        <img src={filterIcon} className="imgHead" data-key={data} />
                      </th>
                    )) : this.state.customHeadersState.map((data, key) => (
                      <th key={key} data-key={data} onClick={this.filterHeader}>
                        <label data-key={data}>{data}</label>
                        <img src={filterIcon} className="imgHead" data-key={data} />
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {this.state.vendorData != null ? this.state.vendorData.map((data, key) => (
                    <tr key={key}>
                      <td className="fix-action-btn">
                        <ul className="table-item-list">
                          <li className="til-inner til-eye-btn">
                            <img src={eyeIcon} className="eye" onClick={(e) => this.openEditModal(data.id)} />
                            {/* <span className="generic-tooltip">Edit</span> */}
                          </li>
                          {/*<li className="til-inner til-already-user">
                              <img src={AlreadyUser} />
                              <span className="generic-tooltip">Already a user</span>
                            </li>*/}
                          <li className="til-inner til-edit-btn" onClick={() => this.updateVendor(data)}>
                            {/* <img src={require("../../../assets/edit.svg")} className="height15" /> */}
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                              <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                              <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                              <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                              <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                            </svg>
                            <span className="generic-tooltip">Edit</span>
                          </li>
                        </ul>
                      </td>
                      {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => (
                        <td key={key} >
                          {<label >{data[hdata]}</label>}
                          {/* {hdata === "slName" ? (data.isActive == "0" ? null :
                            <span className="td-already-user">
                              <img src={AlreadyUser} />
                              <span className="generic-tooltip">Already a user</span>
                            </span>
                          ) : null} */}
                          {(hdata === "shippedAddress" || hdata === "slAddress") && this.small(data[hdata]) && <div className="table-tooltip"><label>{data[hdata]}</label></div>}
                        </td>
                      )) : this.state.headerSummary.map((sdata, keyy) => (
                        <td key={keyy} >
                          {<label className="table-td-text">{data[sdata]}</label>}
                          {/* {sdata === "slName" ? (data.isActive == "0" ? null :
                            <span className="td-already-user">
                              <img src={AlreadyUser} />
                              <span className="generic-tooltip">Already a user</span>
                            </span>
                          ) : null} */}
                          {(sdata === "shippedAddress" || sdata === "slAddress") && this.small(data[sdata]) && <div className="table-tooltip"><label>{data[sdata]}</label></div>}
                        </td>
                      ))}
                    </tr>
                  )) : <tr className="tableNoData"><td colSpan="100%"> <span className={"float_Left"}>NO DATA FOUND</span> </td></tr>}
                </tbody>
              </table>
            </div>
          </div>:
          <PendingSignup />}
          <div className="col-md-12 pad-0" >
            <div className="new-gen-pagination">
              <div className="ngp-left">
                <div className="table-page-no">
                  <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                  <span className="ngp-total-item">Total Items :</span> <span className="bold">{this.state.totalVendor}</span>
                </div>
              </div>
              <div className="ngp-right">
                <div className="nt-btn">
                  <div className="pagination-inner">
                    <ul className="pagination-item">
                      {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ?
                        <li>
                          <button className="disable-first-btn" >
                            <span className="page-item-btn-inner">
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                            </span>
                          </button>
                        </li> :
                        <li>
                          <button className="first-btn" onClick={(e) => this.page(e)} id="first" >
                            <span className="page-item-btn-inner">
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                            </span>
                          </button>
                        </li>}
                      {this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ?
                        <li>
                          <button className="prev-btn" onClick={(e) => this.page(e)} id="prev">
                            <span className="page-item-btn-inner">
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                            </span>
                          </button>
                        </li> :
                        <li>
                          <button className="dis-prev-btn">
                            <span className="page-item-btn-inner">
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                            </span>
                          </button>
                        </li>}
                      <li>
                        <button className="pi-number-btn">
                          <span>{this.state.current}/{this.state.maxPage}</span>
                        </button>
                      </li>
                      {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage ? <li >
                        <button className="next-btn" onClick={(e) => this.page(e)} id="next">
                          <span className="page-item-btn-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                              <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                          </span>
                        </button>
                      </li> : <li >
                          <button className="dis-next-btn" disabled>
                            <span className="page-item-btn-inner">
                              <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                              </svg>
                            </span>
                          </button>
                        </li>}
                      <li>
                        <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                          <span className="page-item-btn-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                              <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                              <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                          </span>
                        </button>
                      </li>
                    </ul>
                  </div>
                  {/* <Pagination {...this.state} {...this.props} page={this.page}
                                  prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.toastError && <ToastError toastErrorMsg={this.state.toastErrorMsg} closeToastError={this.closeToastError} />}
        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
        {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
        {this.state.editModal ? <EditModal {...this.props} {...this.state} firstName={this.state.firstName} slAddress={this.state.slAddress} slCityName={this.state.slCityName} slCode={this.state.slCode} slName={this.state.slName} contactNumber={this.state.contactNumber} email={this.state.email} id={this.state.editId} closeEdit={(e) => this.openEditModal(e)} close={(e) => this.close(e)} /> : null}
      </div>
    )
  }
}