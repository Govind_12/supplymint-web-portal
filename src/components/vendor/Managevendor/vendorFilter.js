import React from "react";

class VendorFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        data: [
            { "id": 0, "text": "Administartion" },
            { "id": 1, "text": "Manage Role" },
            { "id": 2, "text": "Admin" },
            { "id": 3, "text": "Manage Site" },
            { "id": 4, "text": "Site Mapping" },
            { "id": 5, "text": "Site" }
        ],
        selected: [],
        current: "",
        open: false,
        search: "",
        edit:""
    };
}

onnChange(e) {

    e.preventDefault();
    this.setState({
        current: e.target.value,
    })
    let match = _.find(this.state.data, function (o) {
        return o.id == e.target.value;
    })
    this.setState({
        selected: this.state.selected.concat(match)
    });
    let idd = match.id;
    var array = this.state.data;
    for (var i = 0; i < array.length; i++) {
        if (array[i].id === idd) {
            array.splice(i, 1);
        }
    }
    this.setState({
        data: array
    })

}
onDelete(e) {
    let match = _.find(this.state.selected, function (o) {
        return o.id == e;
    })
    this.setState({
        data: this.state.data.concat(match)
    });

    var array = this.state.selected;
    for (var i = 0; i < array.length; i++) {
        if (array[i].id === e) {
            array.splice(i, 1);
        }
    }
    this.setState({
        selected: array
    })


}
Change(e) {
    this.setState({
        search: e.target.value
    })
}

openClose(e) {
    e.preventDefault();
    this.setState({
        open: !this.state.open
    })
}
render() {
    const { search } = this.state;
    var result = _.filter(this.state.data, function (data) {
        return _.startsWith(data.text, search);
    });
    return (
      <div> 
        <div className={this.props.filterBar ? "modal fade display_block":"display_none"} id="myModal">
          <div className={this.props.filterBar ? "backdrop display_block":"display_none"}></div>
          <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
            <button
              onClick={(e) => this.props.closeFilter(e)}
              type="button"
              className="close" id=""
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <div className="col-md-12 col-sm-12 pad-0">
              <ul className="list-inline">
                <li>
                  <label className="filter_modal">FILTERS</label>
                </li>
                <li>
                  <label className="filter_text">5 Filters applied</label>
                </li>
              </ul>
            </div>

            <div className="col-md-12 col-sm-12 pad-0">
              <div className="container_modal">
                <div className="col-md-12 col-sm-12 pad-0">
                  <ul className="list-inline m-top-10 width_100 m-bot-10">
                    <li>
                      {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.id} className="selectectbuttondiv">
                        <button className="modal_btn">{data.text}</button>
                        <span onClick={(e) => this.onDelete(data.id)} value={data.id} className="close_btn" aria-hidden="true">&times;</span>
                      </div>)}

                    </li>

                  </ul>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                  <div className="col-md-2 col-xs-12 pad-0 m-rgt-20">

                    <div className="userModalSelect" onClick={(e) => this.openClose(e)}>

                      <label>Choose Role
                                                    <span>
                          ▾
                                                    </span>
                      </label>

                    </div>
                    {this.state.open ? <div className="dropdownDiv m-top-10">

                      <input type="search" className="searchFilterModal" value={search} onChange={(e) => this.Change(e)} />
                      <ul className="dropdownFilterOption">
                        {result.map((data, i) => <li value={data.id} key={data.id} onClick={(e) => this.onnChange(e)} >
                          {/* <label value={data.id} key={data.id}></label> */}
                          {data.text}
                        </li>)}
                      </ul>

                    </div> : null}
                  </div>
                  <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                    <input
                      type="text"
                      placeholder="Vendor Code"
                      className="vendorInputBox"
                    />
                  </div>

                  <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                  <select className="vendorFilterSelect">
                    <option>Status</option>
                  </select>
                  </div>
                  <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                  <select className="vendorFilterSelect">
                    <option>City</option>
                  </select>
                  </div>
                  <div className="col-md-2 col-xs-6 pad-0 m-rgt-20">
                  <select className="vendorFilterSelect">
                    <option>Country</option>
                  </select>
                  </div>
                </div>


              </div>
            </div>
            <div className="col-md-12 col-sm-12 pad-0">
              <div className="col-md-6 float_right pad-0 m-top-50">
                <ul className="list-inline text_align_right ">
                  <li>
                    <button className="modal_clear_btn">CLEAR FILTER</button>
                  </li>
                  <li>
                    <button className="modal_Apply_btn">APPLY</button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VendorFilter;
