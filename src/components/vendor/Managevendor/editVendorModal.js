import React from "react";
import ToastLoader from '../../loaders/toastLoader';
class EditModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        toastLoader: false,
        toastMsg: ""
        }
    }
    createUser() {
        let payload = {
            first: this.props.firstName,
            // last: this.props.lastName,
            // middle: this.props.middleName,
            email: this.props.email,
            mobile: this.props.contactNumber,
            phone: "",
            access: "UI",
            active: "Active",
            address: "",
            user: "",
            uType: "VENDOR",
            slCode: this.props.slCode,
            slName: this.props.slName,
            active: this.props.active,
            userAsAdmin: "true"
        }
        if (this.props.contactNumber == "" || this.props.email == "" || this.props.firstName == "") {
            this.setState({
                toastMsg: "Contact Number , Email ,Contact Person Name are mandatory to create user",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                toastLoader: false
                })
            }, 1500);
        } else {
            this.props.editVendorRequest(payload)
            this.props.close()
        }
    }

    accessVendorPortal =(e)=>{
            let payload ={
            userName: this.props.slCode,
            token: sessionStorage.getItem("token")
        }
        this.props.accessVendorPortalRequest(payload);
    }

    sendEmail =(e)=>{
        //     let payload ={
        //     userName: this.props.slCode,
        //     token: sessionStorage.getItem("token")
        // }
        let payload = {
            firstName: this.props.firstName === undefined ? "" : this.props.firstName,
            email: this.props.email === undefined ? "" : this.props.email,
            mobileNumber: this.props.contactNumber === undefined ? "" : this.props.contactNumber,
            workPhone: this.props.workPhone === undefined ? "" : this.props.workPhone,
            accessMode: this.props.accessMode === undefined ? "UI" : this.props.accessMode,
            status: this.props.status === undefined ? "0" : this.props.status,
            address: this.props.slAddress === undefined ? "" : this.props.slAddress,
            partnerEnterpriseId: sessionStorage.getItem("partnerEnterpriseId"),
            partnerEnterpriseName: sessionStorage.getItem("partnerEnterpriseName"),
            userName: this.props.userName === undefined ? "" : this.props.userName,
            userRoles: this.props.userRoles === undefined ? [] : this.props.userRoles,
            roles: this.props.roles === undefined ? "" : this.props.roles,
            uType: "VENDOR",
            slCode: this.props.slCode === undefined ? "" : this.props.slCode,
            slName: this.props.slName === undefined ? "" : this.props.slName,
            organisationId: this.props.organisationId === undefined ? "" : this.props.organisationId,
            userAsAdmin: this.props.userAsAdmin === undefined ? "true" : this.props.userAsAdmin
        }
        this.props.sendEmailRequest(payload);
    }

    render() {
        return (
        <div className="modal">
            <div className="backdrop modal-backdrop-new"></div>
            <div className="modal-content master-data-view-modal">
                <div className="mdvm-head">
                    <div className="mdvmh-left">
                        <h3>Manage Vendor</h3>
                    </div>
                    <div className="mdvmh-right">
                        <button onClick={(e) => this.sendEmail(e)} className={this.props.confirmed === "pending" ? "mdvmh-enable" : "mdvmh-enable btnDisabled"} type="button" disabled={this.props.confirmed === "pending" ? "" : "disabled"}>Re-send Email
                        {this.props.confirmed === "pending" ? <span class="generic-tooltip">Click here to re-send the Sign-up email to the Vendor</span>:null}
                        </button>
                        <button onClick={(e) => this.accessVendorPortal(e)} className={this.props.confirmed === "notConfirm" ? "mdvmh-enable" : "mdvmh-enable btnDisabled"} type="button" disabled={this.props.confirmed === "notConfirm" ? "" : "disabled"}>Access Vendor Portal</button>
                        {/* <button disabled className="mdvmh-enable btnDisabled" type="button">Access Vendor Portal</button> */}
                        {this.props.confirmed === "confirm" ? <button onClick={(e) => this.createUser(e)} className="mdvmh-enable" type="button">Enable Portal Access</button> : <button disabled className="mdvmh-enable btnDisabled" type="button">Enable Portal Access{this.props.confirmed === "notConfirm" && <span class="generic-tooltip">Already a user</span>}</button>}
                        <button className="mdvmh-close" onClick={(e) => this.props.closeEdit()}><img src={require('../../../assets/clearSearch.svg')} /></button>
                    </div>
                </div>
                <div className="mdvm-body">
                    <div className="col-md-12 m-top-20">
                        <h3>Basic Details</h3>
                    </div>
                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Supplier Code</label>
                            <p>{this.props.slCode}</p>
                        </div>
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Supplier Name</label>
                            <p>{this.props.slName}</p>
                        </div>
                        {/* <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Last name</label>
                            <p>{this.props.lastName}</p>
                        </div> */}
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Contact Person</label>
                            <p>{this.props.firstName}</p>
                        </div>
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Email</label>
                            <p>{this.props.email}</p>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 pad-0 m-top-40">
                        {/* <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>GST No.</label>
                            <p">{this.props.gstNo}</p>
                        </div> */}
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>GST No.</label>
                            <p>{this.props.gstNo}</p>
                        </div>
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Contact Number</label>
                            <p>{this.props.contactNumber}</p>
                        </div>
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Supplier City Name</label>
                            <p>{this.props.slCityName}</p>
                        </div>
                        <div className="col-md-3 col-sm-2 col-xs-3">
                            <label>Supplier Address</label>
                            <p>{this.props.slAddress}</p>
                        </div>
                    </div>
                </div>
            </div>
            {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
        </div>
        );
    }
}

export default EditModal;
