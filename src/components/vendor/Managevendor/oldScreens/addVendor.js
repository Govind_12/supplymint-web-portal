import React from "react";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";

// import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";
class AddVendor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      organisationName: "",
      organisationNameerr: false,
      vendorOrganisation: "",
      vendorOrganisationerr: false,
      vendorCode: "",
      vendorCodeerr: false,
      description: "",
      descriptionerr: false,
      status: "",
      statuserr: false,
      city: "",
      cityerr: false,
      state: "",
      stateerr: false,
      country: "",
      countryerr: false,
      zip: "",
      ziperr: false,
      contactNumber: "",
      contactNumbererr: false,
      fax: "",
      faxerr: false,
      email: "",
      emailerr: false,
      url: "",
      urlerr: false,
      note: "",
      noteerr: false,
      name: "",
      nameerr: false,
      phone: "",
      phoneerr: false,
      contactEmail: "",
      contactEmailerr: false,
      address: "",
      addresserr: false,
      paymentTerms: "",
      paymentTermserr: false,
      leadTimeDays: "",
      leadTimeDayserr: false,
      purchasingAddress: "",
      purchasingAddresserr: false,
      requestSuccess: false,
      requestError: false,
      loader: false,
      success: false,
      alert: false,
      rightbar: false,
      openRightBar: false,
      shippingAddresserr: false,
      shippingEmailerr: false,
      shippingNameerr: false,
      shippingPhoneerr: false,
      shippingAddress: "",
      shippingEmail: "",
      shippingName: "",
      shippingPhone: ""
    };
  }
  onClear(e) {
    e.preventDefault();
    this.setState({
      organizationName: "",
      vendorOrganisation: "",
      vendorCode: "",
      description: "",
      status: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      contactNumber: "",
      fax: "",
      email: "",
      url: "",
      note: "",
      name: "",
      phone: "",
      contactEmail: "",
      address: "",
      paymentTerms: "",
      leadTimeDays: "",
      purchasingAddress: "",
      shippingAddress: "",
      shippingEmail: "",
      shippingName: "",
      shippingPhone: ""
    })
  }
  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
  }

  handleChange(e) {
    if (e.target.id == "vendorOrganisation") {
      this.setState(
        {
          vendorOrganisation: e.target.value
        },
        () => {
          this.vendorOrganisation();
        }
      );
    }
    else if (e.target.id == "organisationName") {
      this.setState(
        {
          organisationName: e.target.value
        },
        () => {
          this.organisationName();
        }
      );
    }
    else if (e.target.id == "vendorCode") {
      this.setState(
        {
          vendorCode: e.target.value
        },
        () => {
          this.vendorCode();
        }
      );
    } else if (e.target.id == "description") {
      this.setState(
        {
          description: e.target.value
        },
        () => {
          this.description();
        }
      );
    } else if (e.target.id == "status") {
      this.setState(
        {
          status: e.target.value
        },
        () => {
          this.status();
        }
      );
    } else if (e.target.id == "city") {
      this.setState(
        {
          city: e.target.value
        },
        () => {
          this.city();
        }
      );
    } else if (e.target.id == "state") {
      this.setState(
        {
          state: e.target.value
        },
        () => {
          this.states();
        }
      );
    } else if (e.target.id == "country") {
      this.setState(
        {
          country: e.target.value
        },
        () => {
          this.country();
        }
      );
    } else if (e.target.id == "zip") {
      this.setState(
        {
          zip: e.target.value
        },
        () => {
          this.zip();
        }
      );
    } else if (e.target.id == "contactNumber") {
      this.setState(
        {
          contactNumber: e.target.value
        },
        () => {
          this.contactNumber();
        }
      );
    } else if (e.target.id == "name") {
      this.setState(
        {
          name: e.target.value
        },
        () => {
          this.name();
        }
      );
    } else if (e.target.id == "note") {
      this.setState(
        {
          note: e.target.value
        },
        () => {
          this.note();
        }
      );
    } else if (e.target.id == "url") {
      this.setState(
        {
          url: e.target.value
        },
        () => {
          this.url();
        }
      );
    } else if (e.target.id == "email") {
      this.setState(
        {
          email: e.target.value
        },
        () => {
          this.email();
        }
      );
    } else if (e.target.id == "fax") {
      this.setState(
        {
          fax: e.target.value
        },
        () => {
          this.fax();
        }
      );
    } else if (e.target.id == "phone") {
      this.setState(
        {
          phone: e.target.value
        },
        () => {
          this.phone();
        }
      );
    } else if (e.target.id == "contactEmail") {
      this.setState(
        {
          contactEmail: e.target.value
        },
        () => {
          this.contactEmail();
        }
      );
    } else if (e.target.id == "address") {
      this.setState(
        {
          address: e.target.value
        },
        () => {
          this.address();
        }
      );
    } else if (e.target.id == "paymentTerms") {
      this.setState(
        {
          paymentTerms: e.target.value
        },
        () => {
          this.paymentTerms();
        }
      );
    } else if (e.target.id == "leadTimeDays") {
      this.setState(
        {
          leadTimeDays: e.target.value
        },
        () => {
          this.leadTimeDays();
        }
      );
    } else if (e.target.id == "purchasingAddress") {
      this.setState(
        {
          purchasingAddress: e.target.value
        },
        () => {
          this.purchasingAddress();
        }
      );
    }
    else if (e.target.id == "shippingName") {
      this.setState(
        {
          shippingName: e.target.value
        },
        () => {
          this.shippingName();
        }
      );
    }
    else if (e.target.id == "shippingPhone") {
      this.setState(
        {
          shippingPhone: e.target.value
        },
        () => {
          this.shippingPhone();
        }
      );
    }
    else if (e.target.id == "shippingAddress") {
      this.setState(
        {
          shippingAddress: e.target.value
        },
        () => {
          this.shippingAddress();
        }
      );
    }
    else if (e.target.id == "shippingEmail") {
      this.setState(
        {
          shippingEmail: e.target.value
        },
        () => {
          this.shippingEmail();
        }
      );
    }


  }

  organisationName() {
    if (
      this.state.organisationName == "" ||
      !this.state.organisationName.match(/^[a-zA-Z ]+$/)
    ) {
      this.setState({
        organisationNameerr: true
      });
    } else {
      this.setState({
        organisationNameerr: false
      });
    }
  }
  //vendorOrganisation
  vendorOrganisation() {
    if (
      this.state.vendorOrganisation == "" ||
      !this.state.vendorOrganisation.match(/^[a-zA-Z ]+$/)
    ) {
      this.setState({
        vendorOrganisationerr: true
      });
    } else {
      this.setState({
        vendorOrganisationerr: false
      });
    }
  }
  //vendorCode
  vendorCode() {
    if (this.state.vendorCode == "" || this.state.vendorCode.trim() == "") {
      this.setState({
        vendorCodeerr: true
      });
    } else {
      this.setState({
        vendorCodeerr: false
      });
    }
  }

  //description
  description() {
    if (this.state.description == "" || this.state.description.trim() == "") {
      this.setState({
        descriptionerr: true
      });
    } else {
      this.setState({
        descriptionerr: false
      });
    }
  }
  //status

  status() {
    if (this.state.status == "") {
      this.setState({
        statuserr: true
      });
    } else {
      this.setState({
        statuserr: false
      });
    }
  }

  //city

  city() {
    if (this.state.city == "") {
      this.setState({
        cityerr: true
      });
    } else {
      this.setState({
        cityerr: false
      });
    }
  }
  //state

  states() {
    if (this.state.state == "") {
      this.setState({
        stateerr: true
      });
    } else {
      this.setState({
        stateerr: false
      });
    }
  }

  //country
  country() {
    if (this.state.country == "") {
      this.setState({
        countryerr: true
      });
    } else {
      this.setState({
        countryerr: false
      });
    }
  }

  //zip
  zip() {
    if (
      this.state.zip == "" ||
      !this.state.zip.match(/(^\d{6}$)|(^\d{6}-\d{4}$)/) ||
      !this.state.zip.match(/^.{6}$/)
    ) {
      this.setState({
        ziperr: true
      });
    } else {
      this.setState({
        ziperr: false
      });
    }
  }
  //contactNumber

  contactNumber() {
    if (
      this.state.contactNumber == "" ||
      !this.state.contactNumber.match(/^\d{10}$/) ||
      !this.state.contactNumber.match(/^[1-9]\d+$/) ||
      !this.state.contactNumber.match(/^.{10}$/)
    ) {
      this.setState({
        contactNumbererr: true
      });
    } else {
      this.setState({
        contactNumbererr: false
      });
    }
  }
  //fax
  fax() {
    if (
      this.state.fax == "" ||
      !this.state.fax.match(/^\d{6}$/) ||
      !this.state.fax.match(/^.{6}$/)
    ) {
      this.setState({
        faxerr: true
      });
    } else {
      this.setState({
        faxerr: false
      });
    }
  }
  //email
  email() {
    if (
      this.state.email == "" ||
      !this.state.email.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      this.setState({
        emailerr: true
      });
    } else {
      this.setState({
        emailerr: false
      });
    }
  }
  //url

  url() {
    if (
      this.state.url == "" ||
      !this.state.url.match(
        /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
      )
    ) {
      this.setState({
        urlerr: true
      });
    } else {
      this.setState({
        urlerr: false
      });
    }
  }

  //note

  note() {
    if (this.state.note == "" || this.state.note.trim() == "") {
      this.setState({
        noteerr: true
      });
    } else {
      this.setState({
        noteerr: false
      });
    }
  }

  //name
  name() {
    if (this.state.name == "" || this.state.name.trim() == "" || !this.state.name.match(/^[a-zA-Z ]+$/)) {
      this.setState({
        nameerr: true
      });
    } else {
      this.setState({
        nameerr: false
      });
    }
  }
  //phone

  phone() {
    if (
      this.state.phone == "" ||
      !this.state.phone.match(/^\d{10}$/) ||
      !this.state.phone.match(/^[1-9]\d+$/) ||
      !this.state.phone.match(/^.{10}$/)
    ) {
      this.setState({
        phoneerr: true
      });
    } else {
      this.setState({
        phoneerr: false
      });
    }
  }

  //contactEmail
  contactEmail() {
    if (
      this.state.contactEmail == "" ||
      !this.state.contactEmail.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      this.setState({
        contactEmailerr: true
      });
    } else {
      this.setState({
        contactEmailerr: false
      });
    }
  }
  //address
  address() {
    if (this.state.address == "" || this.state.address.trim() == "") {
      this.setState({
        addresserr: true
      });
    } else {
      this.setState({
        addresserr: false
      });
    }
  }

  //paymentTerms
  paymentTerms() {
    if (this.state.paymentTerms == "" || this.state.paymentTerms.trim() == "") {
      this.setState({
        paymentTermserr: true
      });
    } else {
      this.setState({
        paymentTermserr: false
      });
    }
  }
  //leadTimeDays
  leadTimeDays() {
    if (this.state.leadTimeDays == "" || this.state.leadTimeDays.trim() == "") {
      this.setState({
        leadTimeDayserr: true
      });
    } else {
      this.setState({
        leadTimeDayserr: false
      });
    }
  }
  //purchasingAddress
  purchasingAddress() {
    if (this.state.purchasingAddress == "" || this.state.purchasingAddress.trim() == "") {
      this.setState({
        purchasingAddresserr: true
      });
    } else {
      this.setState({
        purchasingAddresserr: false
      });
    }
  }



  //name
  shippingName() {
    if (this.state.shippingName == "" || !this.state.shippingName.match(/^[a-zA-Z ]+$/)) {
      this.setState({
        shippingNameerr: true
      });
    } else {
      this.setState({
        shippingNameerr: false
      });
    }
  }
  //phone

  shippingPhone() {
    if (
      this.state.shippingPhone == "" ||
      !this.state.shippingPhone.match(/^\d{10}$/) ||
      !this.state.shippingPhone.match(/^[1-9]\d+$/) ||
      !this.state.shippingPhone.match(/^.{10}$/)
    ) {
      this.setState({
        shippingPhoneerr: true
      });
    } else {
      this.setState({
        shippingPhoneerr: false
      });
    }
  }

  //contactEmail
  shippingEmail() {
    if (
      this.state.shippingEmail == "" ||
      !this.state.shippingEmail.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      this.setState({
        shippingEmailerr: true
      });
    } else {
      this.setState({
        shippingEmailerr: false
      });
    }
  }
  //address
  shippingAddress() {
    if (this.state.shippingAddress == "" || this.state.shippingAddress.trim() == "") {
      this.setState({
        shippingAddresserr: true
      });
    } else {
      this.setState({
        shippingAddresserr: false
      });
    }
  }




  componentWillReceiveProps(nextProps) {
    if (!nextProps.vendor.addVendor.isSuccess && !nextProps.vendor.addVendor.isError && !nextProps.vendor.addVendor.isLoading) {
      this.setState({
        loader: false
      })
    }

    if (nextProps.vendor.addVendor.isSuccess) {
      this.setState({
        loader: false,
        success: true
      })
      this.props.addVendorClear();
    } else if (nextProps.vendor.addVendor.isError) {
      this.setState({
        loader: false,
        alert: true
      })
      this.props.addVendorClear();
    } else if (nextProps.vendor.addVendor.isLoading) {
      this.setState({
        loader: true
      })
    }
  }

  contactSubmit(e) {
    e.preventDefault();
    this.organisationName();
    this.vendorOrganisation();
    this.vendorCode();
    this.description();
    this.status();
    this.city();
    this.states();
    this.country();
    this.zip();
    this.contactNumber();
    this.fax();
    this.email();
    this.url();
    this.note();
    this.name();
    this.phone();
    this.contactEmail();
    this.address();
    this.paymentTerms();
    this.leadTimeDays();
    this.purchasingAddress();
    this.shippingName();
    this.shippingPhone();
    this.shippingEmail();
    this.shippingAddress();
    const t = this;
    setTimeout(function () {
      const { organisationNameerr, paymentTermserr, faxerr, urlerr, vendorCodeerr, vendorOrganisationerr, noteerr, addresserr, phoneerr, contactNumbererr, contactEmailerr, purchasingAddresserr, leadTimeDayserr, nameerr, emailerr, descriptionerr, cityerr, statuserr, stateerr, countryerr, ziperr, shippingAddresserr, shippingEmailerr, shippingNameerr, shippingPhoneerr } = t.state;
      if (!organisationNameerr && !paymentTermserr && !faxerr && !urlerr && !vendorCodeerr && !vendorOrganisationerr && !noteerr && !addresserr && !phoneerr && !contactNumbererr && !contactEmailerr && !purchasingAddresserr && !leadTimeDayserr && !nameerr && !emailerr && !descriptionerr && !cityerr && !statuserr && !stateerr && !countryerr && !ziperr && !shippingAddresserr && !shippingEmailerr && !shippingNameerr && !shippingPhoneerr) {
        t.setState({
          loader: true,
          alert: false,
          success: false
        })
        let vendorData = {

          organisationName: t.state.organisationName,
          vendorName: t.state.vendorOrganisation,
          vendorCode: t.state.vendorCode,
          description: t.state.description,
          country: t.state.country,
          state: t.state.state,
          city: t.state.city,
          zipCode: t.state.zip,
          url: t.state.url,
          email: t.state.email,
          notes: t.state.note,
          fax: t.state.fax,
          phone: t.state.contactNumber,
          contactName: t.state.name,
          contactPhone: t.state.phone,
          contactEmail: t.state.contactEmail,
          contactAddress: t.state.address,
          shippingCNTName: t.state.shippingName,
          shippingPhone: t.state.shippingPhone,
          shippingEmail: t.state.shippingEmail,
          shippingCNTAddress: t.state.shippingAddress,
          purchasingAddress: t.state.purchasingAddress,
          paymentTerms: t.state.paymentTerms,
          orderLeadTimeDays: t.state.leadTimeDays,
          status: t.state.status
        }
        t.props.addVendorRequest(vendorData);
      }
    }, 100)

  }

  render() {
    $("body").on("keyup", ".numbersOnly", function () {
      if (this.value != this.value.replace(/[^0-9]/g, '')) {
        this.value = this.value.replace(/[^0-9]/g, '');
      }
    });
    const {
      organisationName,
      organisationNameerr,
      vendorOrganisation,
      vendorOrganisationerr,
      vendorCode,
      vendorCodeerr,
      description,
      descriptionerr,
      status,
      statuserr,
      city,
      cityerr,
      state,
      stateerr,
      country,
      countryerr,
      zip,
      ziperr,
      contactNumber,
      contactNumbererr,
      fax,
      faxerr,
      email,
      emailerr,
      url,
      urlerr,
      note,
      noteerr,
      name,
      nameerr,
      phone,
      phoneerr,
      contactEmail,
      contactEmailerr,
      address,
      addresserr,
      paymentTerms,
      paymentTermserr,
      leadTimeDays,
      leadTimeDayserr,
      purchasingAddress,
      purchasingAddresserr,
      shippingAddress,
      shippingAddresserr,
      shippingName,
      shippingNameerr,
      shippingEmail,
      shippingEmailerr, shippingPhone,

      shippingPhoneerr

    } = this.state;

    return (
      <div className="container-fluid">
        <SideBar {...this.props} />
        <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
        {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}

        <div className="container_div m-top-100 " id="">
          <div className="col-md-12 col-sm-12">
            <div className="menu_path">
              <ul className="list-inline width_100 pad20">
                <BraedCrumps {...this.props} />
              </ul>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="container_div" id="">
            <div className="container-fluid">

              <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                <div className="container_content1">
                  <div className="col-md-12 col-sm-12 pad-0">
                    <ul className="list_style">
                      <li>
                        <span className="contribution_mart">VENDORS</span>
                      </li>
                      <li>
                        {/* <p className="master_para">lorem ipsum doler immet</p> */}
                      </li>
                    </ul>
                  </div>

                  <form
                    name="vendorForm"
                    className="vendorForm"
                    onSubmit={(e) => this.contactSubmit((e))
                    }
                  >
                    <div className="StickyDiv">
                      <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                        <ul className="list_style">
                          <li>
                            <span className="detail_span">BASIC DETAILS</span>
                          </li>
                          <li>
                            {/* <p className="master_para">lorem ipsum doler immet</p> */}
                          </li>
                        </ul>
                      </div>
                      <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                        <div className="container_box list_margin">
                          <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                            <div className="col-md-2 pad-lft-0">
                              <input
                                name="organisationName"
                                id="organisationName"
                                type="text"
                                placeholder="Organisation"
                                className={organisationNameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={organisationName}
                              />
                              {/* {organisationNameerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {organisationNameerr ? (
                                <span className="error">
                                  Enter valid organisation name
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 col-sm-2 pad-lft-0">
                              <input
                                ref="vendorOrganisation"
                                name="vendorOrganisation"
                                id="vendorOrganisation"
                                type="text"
                                placeholder="Vendor Organisation"
                                className={vendorOrganisationerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={vendorOrganisation}
                              />
                              {/* {vendorOrganisationerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {vendorOrganisationerr ? (
                                <span className="error">
                                  Enter valid vendor organisation
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 col-sm-4 pad-lft-0">
                              <input
                                ref="vendorCode"
                                name="vendorCode"
                                id="vendorCode"
                                type="text"
                                placeholder="Vendor Code"
                                className={vendorCodeerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={vendorCode}
                              />
                              {/* {vendorCodeerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {vendorCodeerr ? (
                                <span className="error">
                                  Enter valid Vendor Code
                          </span>
                              ) : null}
                            </div>


                            <div className="col-md-2 pad-lft-0">
                              <select
                                ref="status"
                                name="status"
                                id="status"
                                className={statuserr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={status}
                              >
                                <option value="">Select</option>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                              </select>
                              {/* {statuserr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {statuserr ? (
                                <span className="error" >
                                  Select status
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                maxLength="10"
                                placeholder="Contact Number"
                                className={contactNumbererr ? "vendor_input_box errorBorder numbersOnly " : "vendor_input_box numbersOnly"}
                                ref="contactNumber"
                                name="contactNumber"
                                id="contactNumber"
                                className="vendor_input_box"
                                onChange={e => this.handleChange(e)}
                                value={contactNumber}
                              />
                              {/* {contactNumbererr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {contactNumbererr ? (
                                <span className="error" >
                                  Enter valid Contact Number
                          </span>
                              ) : null}
                            </div>
                          </div>

                          <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                            <div className="col-md-2 pad-lft-0">
                              <select
                                ref="city"
                                name="city"
                                id="city"
                                className={cityerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={city}
                              >
                                <option value="">City</option>
                                <option value="new delhi">New Delhi</option>
                              </select>
                              {/* {cityerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {cityerr ? (
                                <span className="error">
                                  Select city
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <select
                                ref="state"
                                name="state"
                                id="state"
                                className={stateerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={state}
                              >
                                <option value="">State</option>
                                <option value="delhi">Delhi</option>
                              </select>
                              {/* {stateerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {stateerr ? (
                                <span className="error">
                                  Select state
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <select
                                ref="country"
                                name="country"
                                id="country"
                                className={countryerr ? "errorBorder vendor_input_box" : "vendor_input_box"}
                                onChange={e => this.handleChange(e)}
                                value={country}
                              >
                                <option value="">Country</option>
                                <option value="india">India</option>
                              </select>
                              {/* {countryerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {countryerr ? (
                                <span className="error" >
                                  Select country
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Zip"
                                className={ziperr ? "numbersOnly errorBorder vendor_input_box " : "numbersOnly vendor_input_box"}
                                ref="zip"
                                name="zip"
                                id="zip"
                                maxLength="6"
                                className="vendor_input_box"
                                onChange={e => this.handleChange(e)}
                                value={zip}
                              />
                              {/* {ziperr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {ziperr ? (
                                <span className="error" >
                                  Enter valid zip
                          </span>
                              ) : null}
                            </div>

                          </div>

                          <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                maxLength="6"
                                placeholder="Fax"
                                className={faxerr ? "vendor_input_box numbersOnly errorBorder" : "vendor_input_box numbersOnly"}
                                ref="fax"
                                name="fax"
                                id="fax"
                                onChange={e => this.handleChange(e)}
                                value={fax}
                              />
                              {/* {faxerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {faxerr ? (
                                <span className="error" >
                                  Enter valid fax
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Email"
                                className={emailerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="email"
                                name="email"
                                id="email"
                                onChange={e => this.handleChange(e)}
                                value={email}
                              />
                              {/* {emailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {emailerr ? (
                                <span className="error" >
                                  Enter valid email
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="URL"
                                className={urlerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="url"
                                name="url"
                                id="url"
                                onChange={e => this.handleChange(e)}
                                value={url}
                              />
                              {/* {urlerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {urlerr ? (
                                <span className="error" >
                                  Enter valid URL
                          </span>
                              ) : null}
                            </div>
                          </div>

                          <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                            <div className="col-md-4 pad-lft-0">
                              <textarea
                                className={noteerr ? "text_area_vendor errorBorder" : "text_area_vendor"}
                                placeholder="Write Note..."
                                rows="4"
                                cols="10"
                                ref="note"
                                name="note"
                                id="note"
                                onChange={e => this.handleChange(e)}
                                value={note}
                              ></textarea>
                              {/* {noteerr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                              {noteerr ? (
                                <span className="error" >
                                  Enter valid note
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-4 pad-lft-0">
                              <textarea
                                ref="descirption"
                                name="description"
                                id="description"
                                type="text"
                                placeholder="Description"
                                className={descriptionerr ? "errorBorder text_area_vendor" : "text_area_vendor"}
                                onChange={e => this.handleChange(e)}
                                value={description}
                              ></textarea>
                              {/* {descriptionerr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                              {descriptionerr ? (
                                <span className="error">
                                  Enter valid description
                          </span>
                              ) : null}
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                        <ul className="list_style m-lft-0">
                          <li>
                            <span className="detail_span">CONTACT</span>
                          </li>
                          <li>
                            <p className="master_para">
                              Add your contact details here
                      </p>
                          </li>
                        </ul>
                      </div>

                      <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                        <div className="container_box list_margin">
                          <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Name"
                                className={nameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="name"
                                name="name"
                                id="name"
                                onChange={e => this.handleChange(e)}
                                value={name}
                              />
                              {/* {nameerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {nameerr ? (
                                <span className="error" >
                                  Enter valid name
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Phone"
                                className={phoneerr ? "vendor_input_box numbersOnly errorBorder" : "vendor_input_box numbersOnly"}
                                ref="phone"
                                name="phone"
                                maxLength="10"
                                id="phone"
                                onChange={e => this.handleChange(e)}
                                value={phone}
                              />
                              {/* {phoneerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {phoneerr ? (
                                <span className="error" >
                                  Enter valid phone
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Email"
                                className={contactEmailerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="contactemail"
                                name="contactemail"
                                id="contactEmail"
                                onChange={e => this.handleChange(e)}
                                value={contactEmail}
                              />
                              {/* {contactEmailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {contactEmailerr ? (
                                <span className="error" >
                                  Enter valid Contact Email
                          </span>
                              ) : null}
                            </div>
                          </div>
                          <div className="col-md-12 col-sm-12 pad-0 m-top-10">

                            <div className="col-md-4 pad-lft-0">

                              <textarea
                                placeholder="Address..."
                                className={addresserr ? "text_area_vendor errorBorder" : "text_area_vendor"}
                                ref="address"
                                name="address"
                                id="address"
                                onChange={e => this.handleChange(e)}
                                value={address}
                              />
                              {addresserr ? (
                                <span className="error" >
                                  Enter valid address
                          </span>
                              ) : null}
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                        <div className="container_box list_margin">
                          <div className="col-md-12 col-sm-12 pad-0 ">
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Payment terms"
                                className={paymentTermserr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="paymentTerms"
                                name="paymentTerms"
                                id="paymentTerms"
                                onChange={e => this.handleChange(e)}
                                value={paymentTerms}
                              />
                              {/* {paymentTermserr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {paymentTermserr ? (
                                <span className="error" >
                                  Enter valid Payment Terms
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Order Lead Time Days"
                                className={leadTimeDayserr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="leadtimeDays"
                                name="leadtimeDays"
                                id="leadTimeDays"
                                onChange={e => this.handleChange(e)}
                                value={leadTimeDays}
                              />
                              {/* {leadTimeDayserr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {leadTimeDayserr ? (
                                <span className="error" >
                                  Enter valid Order Lead Time Days
                          </span>
                              ) : null}
                            </div>
                          </div>

                          <div className="col-md-12 col-sm-12 pad-0 m-top-10">

                            <div className="col-md-4 pad-lft-0">

                              <textarea
                                placeholder="Purchasing Address"
                                className={purchasingAddresserr ? "text_area_vendor errorBorder" : "text_area_vendor"}
                                ref="purchasingAddress"
                                name="purchasingAddress"
                                id="purchasingAddress"
                                onChange={e => this.handleChange(e)}
                                value={purchasingAddress}
                              />
                              {/* {purchasingAddresserr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                              {purchasingAddresserr ? (
                                <span className="error" >
                                  Enter valid Purchasing Address
                          </span>
                              ) : null}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                        <ul className="list_style m-lft-0">
                          <li>
                            <span className="detail_span">SHIPPING DETAILS</span>
                          </li>
                          <li>
                            <p className="master_para">
                              Add your shipping details here
                      </p>
                          </li>
                        </ul>
                      </div>
                      <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                        <div className="container_box list_margin">
                          <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Shipping Name"
                                className={shippingNameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}

                                name="shippingName"
                                id="shippingName"
                                onChange={e => this.handleChange(e)}
                                value={shippingName}
                              />
                              {/* {shippingNameerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {shippingNameerr ? (
                                <span className="error" >
                                  Enter valid name
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="shipping Phone"
                                className={shippingPhoneerr ? "vendor_input_box numbersOnly errorBorder" : "vendor_input_box numbersOnly"}
                                ref="phone"
                                name="shippingPhone"
                                maxLength="10"
                                id="shippingPhone"
                                onChange={e => this.handleChange(e)}
                                value={shippingPhone}
                              />
                              {/* {shippingPhoneerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {shippingPhoneerr ? (
                                <span className="error" >
                                  Enter valid phone
                          </span>
                              ) : null}
                            </div>
                            <div className="col-md-2 pad-lft-0">
                              <input
                                type="text"
                                placeholder="Shipping Email"
                                className={shippingEmailerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                ref="contactemail"
                                name="shippingEmail"
                                id="shippingEmail"
                                onChange={e => this.handleChange(e)}
                                value={shippingEmail}
                              />
                              {/* {shippingEmailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                              {shippingEmailerr ? (
                                <span className="error" >
                                  Enter valid Contact Email
                          </span>
                              ) : null}
                            </div>
                          </div>
                          <div className="col-md-12 col-sm-12 pad-0 m-top-10">

                            <div className="col-md-4 pad-lft-0">


                              <textarea
                                placeholder="Shipping Address..."
                                className={shippingAddresserr ? "text_area_vendor errorBorder" : "text_area_vendor"}
                                ref="address"
                                name="shippingAddress"
                                id="shippingAddress"
                                onChange={e => this.handleChange(e)}
                                value={shippingAddress}
                              />
                              {/* {shippingAddresserr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                              {shippingAddresserr ? (
                                <span className="error" >
                                  Enter valid address
                          </span>
                              ) : null}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12 col-sm-12 pad-0">
                      <div className="footerDivForm">
                        <ul className="list-inline m-lft-0 m-top-10">
                          <li>
                            <button onClick={(e) => this.onClear(e)} className="clear_button_vendor" type="reset">
                              Clear
                        </button>
                          </li>
                          <li>
                            <button className="save_button_vendor">Save</button>
                          </li>
                        </ul>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError closeErrorRequest={(e) => this.onError(e)} /> : null}

      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    vendor: state.vendor
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddVendor);

