import React from "react";
import VendorFilter from "./vendorFilter";
import { CSVLink } from 'react-csv';
import BraedCrumps from "../../breadCrumps";
import EditModal from "./editVendorModal";
import refreshIcon from "../../../assets/refresh.svg";
import ToastLoader from '../../loaders/toastLoader';
import eyeIcon from "../../../assets/actions-buttons.svg";
class ViewVendor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prev: "",
      current: "",
      next: "",
      maxPage: "",
      filter: false,
      filterBar: false,
      editModal: false,
      vendorData: [],
      edit: "",
      id: "",
      slCode: "",
      slCityName: "",
      slName: "",
      firstName: "",
      lastName: "",
      middleName: "",
      contactNumber: "",
      email: "",
      slAddr: "",
      search: "",
      toastLoader: false,
      toastMsg: "",
      gstNo : ""


    };
  }





  openEditModal(id) {
    let vendorData = this.state.vendorData
    for (let i = 0; i < vendorData.length; i++) {
      if (vendorData[i].slCode == id) {
        this.setState({
          firstName: vendorData[i].firstName,
          lastName: vendorData[i].lastName,
          middleName: vendorData[i].middleName,
          slCityName: vendorData[i].slCityName,
          email: vendorData[i].email,
          contactNumber: vendorData[i].contactNumber,
          slCode: vendorData[i].slCode,
          slName: vendorData[i].slName,
          slAddr: vendorData[i].slAddr,
          gstNo : vendorData[i].gstInNo,
        })
      }
    }

    this.setState({
      editId: id,
      editModal: !this.state.editModal
    });
  }
  close() {
    this.setState({
      editModal: false
    })
  }
  openFilter(e) {
    e.preventDefault();
    this.setState({
      filter: true,
      filterBar: !this.state.filterBar
    });
  }


  componentWillMount() {
    let payload = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.vendorRequest(payload)

  }
  onClearSearch(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: ""
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.vendorRequest(data);
  }


  onSearch(e) {
    e.preventDefault();
    if (this.state.search == "") {
      this.setState({
        toastMsg: "Enter text on search input ",
        toastLoader: true
      })
      setTimeout(() => {
        this.setState({
          toastLoader: false
        })
      }, 1500);
    } else {
      this.setState({
        type: 3,

      })
      let data = {
        type: 3,
        no: 1,

        search: this.state.search,
      }
      this.props.vendorRequest(data)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.vendor.getVendor.isSuccess) {
      this.setState({
        vendorData: nextProps.vendor.getVendor.data.resource,
        prev: nextProps.vendor.getVendor.data.prePage,
        current: nextProps.vendor.getVendor.data.currPage,
        next: nextProps.vendor.getVendor.data.currPage + 1,
        maxPage: nextProps.vendor.getVendor.data.maxPage,
      })
    } else if (nextProps.vendor.getVendor.isErrror) {
      this.setState({
        vendorData: null
      })

    }
    if (nextProps.vendor.editVendor.isSuccess) {
      let payload = {
        type: 1,
        no: 1,
        search: ""
      }
      this.props.vendorRequest(payload)
    }
  }
  page(e) {
    if (e.target.id == "prev") {
      if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

      } else {

        this.setState({
          prev: this.props.vendor.getVendor.data.prePage,
          current: this.props.vendor.getVendor.data.currPage,
          next: this.props.vendor.getVendor.data.currPage + 1,
          maxPage: this.props.vendor.getVendor.data.maxPage,
        })
        if (this.props.vendor.getVendor.data.currPage != 0) {
          let data = {
            type: this.state.type,
            no: this.props.vendor.getVendor.data.currPage - 1,

            search: this.state.search,
          }
          this.props.vendorRequest(data);
        }
      }
    } else if (e.target.id == "next") {
      this.setState({
        prev: this.props.vendor.getVendor.data.prePage,
        current: this.props.vendor.getVendor.data.currPage,
        next: this.props.vendor.getVendor.data.currPage + 1,
        maxPage: this.props.vendor.getVendor.data.maxPage,
      })
      if (this.props.vendor.getVendor.data.currPage != this.props.vendor.getVendor.data.maxPage) {
        let data = {
          type: this.state.type,
          no: this.props.vendor.getVendor.data.currPage + 1,

          search: this.state.search,
        }
        this.props.vendorRequest(data)
      }
    }
    else if (e.target.id == "first") {
      if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

      }
      else {
        this.setState({
          prev: this.props.vendor.getVendor.data.prePage,
          current: this.props.vendor.getVendor.data.currPage,
          next: this.props.vendor.getVendor.data.currPage + 1,
          maxPage: this.props.vendor.getVendor.data.maxPage,
        })
        if (this.props.vendor.getVendor.data.currPage <= this.props.vendor.getVendor.data.maxPage) {
          let data = {
            type: this.state.type,
            no: 1,

            search: this.state.search,
          }
          this.props.vendorRequest(data)
        }
      }

    }
  }


  onClearSearch(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: ""
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.vendorRequest(data);
  }



  handleSearch(e) {
    this.setState({
      search: e.target.value
    })
  }

  onRefresh() {
    let data = {
      type: this.state.type,
      no: this.state.current,

      search: this.state.search,
    }
    this.props.vendorRequest(data)
  }




  render() {
    // let head = Object.keys(this.props.VendorData[0]);
    // let dataaaa = _.map(head, (data, i) => {
    //   return (<th key={i}>
    //     <label>{data}</label>
    //   </th>);
    // });
    return (
      <div className="container-fluid">
        <div className="container_div" id="">
          {/* <div className="container_div m-top-100" id="vendor_manage"> */}
          {/* <div className="col-md-12 col-sm-12 pad-0">
            <div className="menu_path">
              <ul className="list-inline m-lft-0 width_100 m-0"> */}
          {/* <li>
                  <label className="home_link">Home > User > Vendors</label>
                </li> */}
          {/* <BraedCrumps/> */}
          {/* <li
                  className="float_right"
                  onClick={() => this.props.clickRightSideBar()}
                >
                  <img src={openRack} className="right_sidemenu" />
                </li> */}

          {/* </ul>
            </div>
          </div> */}

          <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
            <div className="container_content heightAuto">
              <div className="col-md-6 col-sm-12 pad-0">
                <ul className="list_style">
                  <li>
                    <label className="contribution_mart">VENDORS</label>
                  </li>
                  <li>
                    {/* <p className="master_para">lorem ipsum doler immet</p> */}
                  </li>

                  <li className="m-top-20">
                    {/*<label className="export_data_div">Export Data</label>*/}
                    <ul className="list-inline m-lft-0 m-top-10">
                      {/*<li>
                        <CSVLink data={this.state.vendorData} headers={this.state.headers} filename={"Vendor-Data.csv"}>
                          <button className="button_home">
                            CSV
                        </button>
                        </CSVLink>
                      </li>*/}
                      {/*<li>
                        <button className="button_home">XLS</button>
                      </li>*/}
                      {/*<li>
                        <button
                          className="filter_button"
                          onClick={e => this.openFilter(e)}
                          data-toggle="modal"
                          data-target="#myModal"
                        >
                          FILTER

                        <svg
                            className="filter_control"
                            xmlns="http://www.w3.org/2000/svg"
                            width="17"
                            height="15"
                            viewBox="0 0 17 15"
                          >
                            <path
                              fill="#FFF"
                              fillRule="nonzero"
                              d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z"
                            />
                          </svg>
                        </button>
                      </li>*/}
                      <li className="refresh-table">
                        <img onClick={() => this.onRefresh()} src={refreshIcon} />
                        <p className="tooltiptext topToolTipGeneric">
                          Refresh
                                            </p>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>

              <div className="col-md-6 col-sm-12 pad-0">
                <ul className="list-inline m-lft-0 circle_list">
                  <li>
                    <div className="tooltip" onClick={() => this.props.history.push('/vendor/manageVendors/addVendor')}>
                      <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                        <g fill="none" fillRule="evenodd">
                          <circle cx="17.5" cy="17.5" r="17.5" fill="#6D6DC9" />
                          <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7" />
                        </g>
                      </svg>

                      <p className="tooltiptext tooltip-left">
                        Add Vendor Please click on add icon to add new vendor data

                      </p>
                    </div>

                  </li>
                </ul>
                <ul className="list-inline m-lft-0 search_list manageSearch">
                  <li>
                    <form onSubmit={(e) => this.onSearch(e)}>
                      <input type="search" onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                      <button onClick={(e) => this.onSearch(e)} className="searchWithBar">Search
                                        <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                          <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                          </path></svg>
                      </button>
                    </form>
                  </li>

                </ul>
                {this.state.type != 3 ? null : <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>}
              </div>
              <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3 piHistoryMain tableLeft95 fixedItemCenter">
                <div className="scrollableOrgansation scrollableTableFixed roleTableHeight colummnWidth100">
                  <table className="table table_vendor zui-table roleTable  border-bot-table">
                    <thead className="tableHeadBg">
                      <tr>
                        <th className="fixed-side-history fixed-side1 textCenter"><label>ACTION</label></th>
                        <th><label>VENDOR NAME</label></th>
                        <th><label>VENDOR CODE</label></th>
                        <th><label>VENDOR CITY NAME</label></th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.vendorData != null ? this.state.vendorData.map((data, key) => (
                        <tr key={key}>
                          <td className="fixed-side-history fixed-side1">
                            {data.isActive == "0" ? <span className="pad-top-3">
                              <img src={eyeIcon} onClick={(e) => this.openEditModal(data.slCode)} />
                            </span> :
                              <label><button type="button" className="alreadyUserBtn pointerNone">Already a user</button></label>
                            }
                          </td>
                          <td><label>{data.slName}</label></td>
                          <td><label>{data.slCode}</label></td>
                          <td><label>{data.slCityName}</label></td>
                        </tr>
                      )) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="pagerDiv">
                <ul className="list-inline pagination">
                  <li>
                    <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                      First
                                        </button>
                  </li>

                  <li>
                    <button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">
                      Prev
                  </button>
                  </li>

                  {/* {this.state.prev != 0 ? <li >
                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                        Prev
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" disabled>
                                            Prev
                  </button>
                                    </li>} */}
                  <li>
                    <button className="PageFirstBtn pointerNone">
                      <span>{this.state.current}/{this.state.maxPage}</span>
                    </button>
                  </li>
                  {this.state.current != undefined && this.state.current != "" && this.state.next - 1 != this.state.maxPage ? <li >
                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                      Next
                  </button>
                  </li> : <li >
                      <button className="PageFirstBtn borderNone" disabled>
                        Next
                  </button>
                    </li>}


                  {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                </ul>
              </div>
            </div>
          </div>
        </div>
        {this.state.filter ? <VendorFilter filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} /> : null}
        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}

        {this.state.editModal ? <EditModal {...this.props} {...this.state} firstName={this.state.firstName} lastName={this.state.lastName} middleName={this.state.middleName} slAddr={this.state.slAddr} slCityName={this.state.slCityName} slCode={this.state.slCode} slName={this.state.slName} contactNumber={this.state.contactNumber} email={this.state.email} id={this.state.editId} closeEdit={(e) => this.openEditModal(e)} close={(e) => this.close(e)} /> : null}
      </div>

    );
  }
}

export default ViewVendor;
