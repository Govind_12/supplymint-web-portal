import React from 'react';

class ManageVendorKyc extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            kycDetailData: [],
        }
    }

    componentDidMount() {
        this.props.getKycDetailsRequest('asd')
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if (nextProps.vendor.getKycDetails.isSuccess) {
            return {
                kycDetailData : nextProps.vendor.getKycDetails.data.resource.response
            } 
        } 
        return null      
    }
    componentDidUpdate(){
        if(this.props.vendor.getKycDetails.isSuccess){
            let kycDetailData = [...this.state.kycDetailData]
            kycDetailData.map( data=> data.expandDetail = false)
            this.setState({ kycDetailData })
            this.props.getKycDetailsClear();
        }
        sessionStorage.setItem('currentPage', "VENOTHMAIN")
        sessionStorage.setItem('currentPageName', "Manage KYC")
    }

    downloadFile =(e, url)=>{
        window.open(url)
    }

    expandDetail =(e, id)=> {
        let kycDetailData = [...this.state.kycDetailData]
        kycDetailData.map( data=> {if(data.id === id) data.expandDetail = !data.expandDetail})
        this.setState({ kycDetailData })
    }

    render () {
        const{ kycDetailData} = this.state;
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="manage-vendor-kyc-head">
                        <div className="mvkh-left">
                            {/* <div className="mvkh-search">
                                <input type="search" placeholder="Type to Search" />
                                <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                <span className="closeSearch"><img src={require('../../assets/clearSearch.svg')} /></span>
                            </div> */}
                        </div>
                        <div className="mvkh-right">
                            {/* <div className="mvkh-filter">
                                <button type="button" className="mvkh-filter-btn">Add Filter 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                </button>
                            </div> */}
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="manage-vendor-kyc-body">
                        {kycDetailData.length > 0 ? kycDetailData.map((data, key) => <div key={key} className="mvlb-row">
                            <div className="mvlb-left">
                                <div className="mvlbl-top">
                                    <p>Legal Name</p>
                                    <h3>{data.legalName}</h3>
                                    <span className="mvlbl-ctype">{data.legalEntityType}</span>
                                </div>
                                <div className="mvlb-status">
                                    <img src={require('../../assets/maps-and-flags.svg')} />
                                    <div className="mvlbs-inner">
                                        <span className="mvlbs-kyc-succes">KYC Successed</span>
                                        <p>{data.creationTime}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="mvlb-mid">
                                <div className="mvlbm-filled-details">
                                    {!data.expandDetail ? <div className="mvlbmfd-top">
                                        <div className="mvlbmt-inner">
                                            <label>CIN</label>
                                            <p>{data.cin}</p>
                                        </div>
                                        <div className="mvlbmt-width">
                                            <label>Pan</label>
                                            <p>{data.pan}</p>
                                        </div>
                                        <div className="mvlbmt-width">
                                            <label>Aadhar</label>
                                            <p>{data.contactAadhar}</p>
                                        </div>
                                        <div className="mvlbmt-width">
                                            <label>Mobile Number</label>
                                            <p>{"+91 "+data.mobile}</p>
                                        </div>
                                    </div> :
                                    <div className="mvlbmfd-bottom">
                                        <div className="mvlbmfdb-basicinfo">
                                            <h3>Legal Entity Basic Information</h3>
                                            <div className="mvlbmfdb-details">
                                                <label>CIN</label>
                                                <p>{data.cin}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Pan</label>
                                                <p>{data.pan}</p>
                                                <button type="button" onClick={(e)=>this.downloadFile(e, data.panFile)}>Download 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 21 20">
                                                        <g fill="#8b77fa" fillRule="evenodd">
                                                            <path d="M4.156 19.589h12.677a3.261 3.261 0 0 0 3.256-3.256v-5.177a.833.833 0 0 0-1.667 0v5.177a1.589 1.589 0 0 1-1.589 1.59H4.156a1.589 1.589 0 0 1-1.59-1.59v-5.177a.833.833 0 1 0-1.666 0v5.177a3.261 3.261 0 0 0 3.256 3.256z"/>
                                                            <path d="M10.5.411a.833.833 0 0 0-.833.833v10l-1.85-1.872a.845.845 0 0 0-1.206 1.184l3.178 3.21c.156.231.416.369.694.367a.85.85 0 0 0 .623-.244l3.283-3.228a.835.835 0 1 0-1.178-1.183l-1.855 1.85V1.244A.833.833 0 0 0 10.5.411z"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Aadhar</label>
                                                <p>{data.contactAadhar}</p>
                                                <button type="button" onClick={(e)=>this.downloadFile(e, data.contactAadharFile)}>Download 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 21 20">
                                                        <g fill="#8b77fa" fillRule="evenodd">
                                                            <path d="M4.156 19.589h12.677a3.261 3.261 0 0 0 3.256-3.256v-5.177a.833.833 0 0 0-1.667 0v5.177a1.589 1.589 0 0 1-1.589 1.59H4.156a1.589 1.589 0 0 1-1.59-1.59v-5.177a.833.833 0 1 0-1.666 0v5.177a3.261 3.261 0 0 0 3.256 3.256z"/>
                                                            <path d="M10.5.411a.833.833 0 0 0-.833.833v10l-1.85-1.872a.845.845 0 0 0-1.206 1.184l3.178 3.21c.156.231.416.369.694.367a.85.85 0 0 0 .623-.244l3.283-3.228a.835.835 0 1 0-1.178-1.183l-1.855 1.85V1.244A.833.833 0 0 0 10.5.411z"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>GST</label>
                                                <p>{data.gst}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Email</label>
                                                <p>{data.email}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Mobile Number</label>
                                                <p>{"+91 "+data.mobile}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Bank Statement</label>
                                                <button type="button" onClick={(e)=>this.downloadFile(e, data.bankStatementFile)}>Download 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 21 20">
                                                        <g fill="#8b77fa" fillRule="evenodd">
                                                            <path d="M4.156 19.589h12.677a3.261 3.261 0 0 0 3.256-3.256v-5.177a.833.833 0 0 0-1.667 0v5.177a1.589 1.589 0 0 1-1.589 1.59H4.156a1.589 1.589 0 0 1-1.59-1.59v-5.177a.833.833 0 1 0-1.666 0v5.177a3.261 3.261 0 0 0 3.256 3.256z"/>
                                                            <path d="M10.5.411a.833.833 0 0 0-.833.833v10l-1.85-1.872a.845.845 0 0 0-1.206 1.184l3.178 3.21c.156.231.416.369.694.367a.85.85 0 0 0 .623-.244l3.283-3.228a.835.835 0 1 0-1.178-1.183l-1.855 1.85V1.244A.833.833 0 0 0 10.5.411z"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="mvlbmfdb-indiinfo m-top-20">
                                            <h3>Director/Partner/Individual Info</h3>
                                            <div className="mvlbmfdb-details">
                                                <label>Name</label>
                                                <p>{data.contactName}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Date of Birth</label>
                                                <p>{data.contactDob}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Email</label>
                                                <p>{data.contactEmail}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Mobile Number</label>
                                                <p>{"+91 "+ data.contactMobile}</p>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Pan</label>
                                                <p>{data.contactPan}</p>
                                                <button type="button" onClick={(e)=>this.downloadFile(e, data.contactPanFile)}>Download 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 21 20">
                                                        <g fill="#8b77fa" fillRule="evenodd">
                                                            <path d="M4.156 19.589h12.677a3.261 3.261 0 0 0 3.256-3.256v-5.177a.833.833 0 0 0-1.667 0v5.177a1.589 1.589 0 0 1-1.589 1.59H4.156a1.589 1.589 0 0 1-1.59-1.59v-5.177a.833.833 0 1 0-1.666 0v5.177a3.261 3.261 0 0 0 3.256 3.256z"/>
                                                            <path d="M10.5.411a.833.833 0 0 0-.833.833v10l-1.85-1.872a.845.845 0 0 0-1.206 1.184l3.178 3.21c.156.231.416.369.694.367a.85.85 0 0 0 .623-.244l3.283-3.228a.835.835 0 1 0-1.178-1.183l-1.855 1.85V1.244A.833.833 0 0 0 10.5.411z"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Aadhar</label>
                                                <p>{data.contactAadhar}</p>
                                                <button type="button" onClick={(e)=>this.downloadFile(e, data.contactAadharFile)}>Download 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 21 20">
                                                        <g fill="#8b77fa" fillRule="evenodd">
                                                            <path d="M4.156 19.589h12.677a3.261 3.261 0 0 0 3.256-3.256v-5.177a.833.833 0 0 0-1.667 0v5.177a1.589 1.589 0 0 1-1.589 1.59H4.156a1.589 1.589 0 0 1-1.59-1.59v-5.177a.833.833 0 1 0-1.666 0v5.177a3.261 3.261 0 0 0 3.256 3.256z"/>
                                                            <path d="M10.5.411a.833.833 0 0 0-.833.833v10l-1.85-1.872a.845.845 0 0 0-1.206 1.184l3.178 3.21c.156.231.416.369.694.367a.85.85 0 0 0 .623-.244l3.283-3.228a.835.835 0 1 0-1.178-1.183l-1.855 1.85V1.244A.833.833 0 0 0 10.5.411z"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div className="mvlbmfdb-details">
                                                <label>Bank Statement</label>
                                                <button type="button" onClick={(e)=>this.downloadFile(e,data.contactBankStatementFile)}>Download 
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 21 20">
                                                        <g fill="#8b77fa" fillRule="evenodd">
                                                            <path d="M4.156 19.589h12.677a3.261 3.261 0 0 0 3.256-3.256v-5.177a.833.833 0 0 0-1.667 0v5.177a1.589 1.589 0 0 1-1.589 1.59H4.156a1.589 1.589 0 0 1-1.59-1.59v-5.177a.833.833 0 1 0-1.666 0v5.177a3.261 3.261 0 0 0 3.256 3.256z"/>
                                                            <path d="M10.5.411a.833.833 0 0 0-.833.833v10l-1.85-1.872a.845.845 0 0 0-1.206 1.184l3.178 3.21c.156.231.416.369.694.367a.85.85 0 0 0 .623-.244l3.283-3.228a.835.835 0 1 0-1.178-1.183l-1.855 1.85V1.244A.833.833 0 0 0 10.5.411z"/>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                            </div>
                            <div className={!data.expandDetail ? "mvlb-right" : "mvlb-right mvlbr-expend"}>
                                <button type="button" onClick={(e)=>this.expandDetail(e, data.id)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13.541" height="8.271" viewBox="0 0 10.541 6.271">
                                        <path fill="none" id="prefix__Path_409" d="M6 9l3.856 3.856L13.713 9" data-name="Path 409" transform="translate(-4.586 -7.586)" stroke="#12203c" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2px" />
                                    </svg>
                                </button>
                            </div>
                        </div>) : null}
                    </div>
                </div>
            </div>
        )
    }
}

export default ManageVendorKyc;