import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { CONFIG } from "../../config/index";
import axios from 'axios';
import FilterLoader from '../../components/loaders/filterLoader';
import { tuple } from 'antd/lib/_util/type';

class VendorKyc extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
           legalName: "",
           legalNameError: false,
           legalEntityType: "",
           legalEntityTypeError: false,
           cin: "",
           cinError: false,
           legalPan: "",
           legalPanError: false,
           //legalPanFile: [],
           legalPanFileError: false,
           gst: "",
           gstError: false,
           //gstFile: [],
           gstFileError: false,
           legalEmail: "",
           legalEmailError: false,
           legalMobile: "",
           legalMobileError: false,
           registeredAddress: "",
           registeredAddressError: false,
           corporateAddress: "",
           corporateAddressError: false,
           name: "",
           nameError: false,
           dob: "",
           dobError: false,
           email: "",
           emailError: false,
           mobile: "",
           mobileError: false,
           genderM: false,
           genderF: false,
           genderError: false,
           pan: "",
           panError: false,
           //panFile: [],
           panFileError: false,
           aadharNo: "",
           aadharNoError: false,
           //aadharNoFile: [],
           aadharNoFileError: false,
           signCheck: false,
           signCheckError: false,
           termCheck: false,
           termCheckError: false,
           uuid: "",
           loader: false,
           bucketName: "",

           legalBankStatementError: false,
           bankStatementError: false,
        }
    }

    componentDidMount(){
        let uuid = uuidv4();
        this.setState({ uuid: uuid})
    }

    handleChange =(e, id)=>{
        let alphaNumPattern = /^[0-9a-zA-Z]+$/;
        let numberPattern = /^[0-9]+$/;
        let value = e.target.value;

        if( id == "legalName")
           this.setState({ legalName: value}, ()=> this.legalNameError())
        if( id == "legalEntityType")
           this.setState({ legalEntityType: value == "" ? "Select Entity" : value},()=>this.legalEntityTypeError())
        if( id == "cin")
           this.setState({ cin: alphaNumPattern.test(value) ? value.length <= 50 ? value : this.state.cin : !value.length ? "" : this.state.cin },()=>this.cinError())

        if( id == "legalPan")
           this.setState({ legalPan: alphaNumPattern.test(value) ? value.length <= 10 ? value : this.state.legalPan : !value.length ? "" : this.state.legalPan},()=>this.legalPanError())
        if( id == "legalPanFile"){
            this.onFileUpload("legalPanFile", e.target.files)
        }
        if( id == "gst")
           this.setState({ gst: alphaNumPattern.test(value) ? value.length <= 15 ? value : this.state.gst : !value.length ? "" : this.state.gst},()=>this.gstError())
        if( id == "gstFile"){
            this.onFileUpload("gstFile", e.target.files) 
        }  

        if( id == "legalEmail")
           this.setState({ legalEmail: value},()=>this.legalEmailError())
        if( id == "legalMobile")
           this.setState({ legalMobile: numberPattern.test(value) ? value.length <= 10 ? value : this.state.legalMobile : !value.length ? "" : this.state.legalMobile},()=>this.legalMobileError())
        if( id == "legalBankStatement"){
            this.onFileUpload("legalBankStatement", e.target.files) 
        }
        if( id == "registeredAddress")
           this.setState({ registeredAddress: value}, ()=>this.registeredAddressError())
        if( id == "corporateAddress")
           this.setState({ corporateAddress: value},()=>this.corporateAddressError())

        if( id == "name")
           this.setState({ name: value},()=>this.nameError())
        if( id == "dob")
           this.setState({ dob: value},()=>this.dobError())
        if( id == "email")
           this.setState({ email: value},()=>this.emailError())
        if( id == "mobile")
           this.setState({ mobile:  numberPattern.test(value) ? value.length <= 10 ? value : this.state.mobile : !value.length ? "" : this.state.mobile},()=>this.mobileError())
        if( id == "genderM")
           this.setState({ genderM: !this.state.genderM , genderF: this.state.genderM},()=>this.genderError())
        if( id == "genderF")
           this.setState({ genderF: !this.state.genderF, genderM: this.state.genderF},()=>this.genderError())

        if( id == "pan")
           this.setState({ pan: alphaNumPattern.test(value) ? value.length <= 10 ? value : this.state.pan : !value.length ? "" : this.state.pan},()=>this.panError())
        if( id == "panFile"){
            this.onFileUpload("panFile", e.target.files) 
        }
        if( id == "aadharNo")
           this.setState({ aadharNo: numberPattern.test(value) ? value.length <= 12 ? value : this.state.aadharNo : !value.length ? "" : this.state.aadharNo},()=>this.aadharNoError())
        if( id == "aadharNoFile"){
            this.onFileUpload("aadharNoFile", e.target.files) 
        }   

        if( id == "bankStatement"){
            this.onFileUpload("bankStatement", e.target.files) 
        }
        if( id == "signCheck")
           this.setState({ signCheck: !this.state.signCheck},()=>this.signCheckError())
        if( id == "termCheck")
           this.setState({ termCheck: !this.state.termCheck},()=>this.termCheckError())   
    }

    legalNameError =()=>{
        this.setState({
            legalNameError: this.state.legalName.trim().length > 0 ? false : true,
        })
    }
    legalEntityTypeError =()=>{
        this.setState({
            legalEntityTypeError: this.state.legalEntityType.trim().length == 0 || this.state.legalEntityType == "Select Entity" ? true : false,
        })
    }
    cinError =()=>{
        this.setState({
            cinError: this.state.cin.trim().length > 0 ? false : true, 
        })
    }
    legalPanError =()=>{
         this.setState({
             legalPanError: this.state.legalPan.length == 10 ? false : true,
         })
    }
    legalPanFileError =()=>{
        this.setState({
            legalPanFileError: this.state.legalPanFile !== undefined && this.state.legalPanFile.fileName.length > 0 ? false : true, 
        })
    }
    gstError =()=>{
        this.setState({
            gstError: this.state.gst.length == 15 ? false : true,
        })
    }
    gstFileError =()=>{
        this.setState({
            gstFileError: this.state.gstFile !== undefined && this.state.gstFile.fileName.length > 0 ? false : true, 
        })
    }
    legalEmailError =()=>{
        var emailPattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.setState({
            legalEmailError: emailPattern.test(this.state.legalEmail) ? false : true
        })
    }
    legalMobileError =()=>{
        this.setState({
            legalMobileError: this.state.legalMobile.length == 10 ? false : true
        })
    }
    legalBankStatementError =()=>{
        this.setState({
            legalBankStatementError: this.state.legalBankStatement !== undefined && this.state.legalBankStatement.fileName.length > 0 ? false : true, 
        })
    }
    registeredAddressError =()=>{
        this.setState({
            registeredAddressError: this.state.registeredAddress.trim().length > 0 ? false : true
        })
    }
    corporateAddressError =()=>{
        this.setState({
            corporateAddressError: this.state.corporateAddress.trim().length > 0 ? false : true
        })
    }
    nameError =()=>{
        this.setState({
            nameError: this.state.name.trim().length > 0 ? false : true
        })
    }
    dobError =()=>{
        this.setState({
            dobError: this.state.dob.trim().length > 0 ? false : true
        })
    }
    emailError =()=>{
        var emailPattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.setState({
            emailError: emailPattern.test(this.state.email) ? false : true
        })
    }
    mobileError =()=>{
        this.setState({
            mobileError: this.state.mobile.length == 10 ? false : true
        })
    }
    genderError =()=>{
        this.setState({
            genderError: this.state.genderM || this.state.genderF ? false : true,
        })
    }
    panError =()=>{
        this.setState({
            panError: this.state.pan.length == 10 ? false : true,
        })
    }
    panFileError =()=>{
        this.setState({
            panFileError: this.state.panFile !== undefined && this.state.panFile.fileName.length > 0 ? false : true, 
        })
    }
    aadharNoError =()=>{
        this.setState({
            aadharNoError: this.state.aadharNo.length == 12 ? false : true,
        })
    }
    aadharNoFileError =()=>{
        this.setState({
            aadharNoFileError: this.state.aadharNoFile !== undefined && this.state.aadharNoFile.fileName.length > 0 ? false : true, 
        })
    }
    bankStatementError =()=>{
        this.setState({
            bankStatementError: this.state.bankStatement !== undefined && this.state.bankStatement.fileName.length > 0 ? false : true, 
        })
    }
    signCheckError =()=>{
        this.setState({
            signCheckError: this.state.signCheck ? false : true,
        })
    }
    termCheckError =()=>{
        this.setState({
            termCheckError: this.state.termCheck ? false : true,
        })
    }

    onFileUpload =(fileType, fileData)=> {
        this.setState({
            loader: true
        },()=>{
            let files = fileData
            const fd = new FormData();
            let uploadNode = {
                uuid: this.state.uuid,
                subFolder: fileType == "legalPanFile" ? "pan" : fileType == "gstFile" ? "gst" : 
                           fileType == "legalBankStatement" ? "bankStatement" : fileType == "panFile" ? "contactPan" :
                           fileType == "aadharNoFile" ? "contactAadhar" : fileType == "bankStatement" ? "contactBankStatement" : "",
                folder: "kyc/staging",
            }
            let headers = {
                'X-Auth-Token': sessionStorage.getItem('token'),
                'Content-Type': 'multipart/form-data'
            }
            for (var i = 0; i < files.length; i++) {
                fd.append("files", files[i]);
            }
            fd.append("uploadNode", JSON.stringify(uploadNode))
            if( files.length){
                axios.post(`${CONFIG.BASE_URL}/vendorportal/kyc/upload`, fd, { headers: headers })
                .then(res => {
                    this.setState({
                        [fileType] : res.data.data.resource.response,
                        loader: false,
                        bucketName: res.data.data.resource.response.bucketName
                    },()=>this.fileUploadValidation(fileType))
                }).catch((error) => {
                    this.setState({
                        [fileType]: [],
                        loader: false,
                    },()=>this.fileUploadValidation(fileType))
                })
            }
        });    
    }

    fileUploadValidation =(fileType)=>{
        if( fileType === "legalPanFile")
           this.legalPanFileError();
        if( fileType === "gstFile")
           this.gstFileError(); 
        if( fileType === "legalBankStatement")
           this.legalBankStatementError();
        if( fileType === "panFile")
           this.panFileError();   
        if( fileType === "aadharNoFile")
           this.aadharNoFileError();
        if( fileType === "bankStatement")
           this.bankStatementError();      
    }

    submitKyc =(e)=>{
        this.legalNameError()
        this.legalEntityTypeError()
        this.cinError()
        this.legalPanError()
        this.legalPanFileError()
        this.gstError()
        this.gstFileError()
        this.legalEmailError()
        this.legalMobileError()

        this.legalBankStatementError()
        this.registeredAddressError()
        this.corporateAddressError()
        this.nameError()
        this.dobError()

        this.emailError()
        this.mobileError()
        this.genderError()
        this.panError()
        this.panFileError()
        this.aadharNoError();
        this.aadharNoFileError()

        this.bankStatementError()
        this.signCheckError()
        this.termCheckError()

        setTimeout(() => {
            const{ legalName, legalNameError, legalEntityType, legalEntityTypeError, cin, cinError, legalPanError, legalPanFileError, gstError,
                gstFileError, legalEmailError,legalMobileError,legalBankStatementError, registeredAddress, registeredAddressError, corporateAddress, 
                corporateAddressError,name, nameError,dob,dobError,emailError,mobileError,genderM,genderF,genderError,panError,panFileError,aadharNoError,
                aadharNoFileError, bankStatementError, signCheck,signCheckError,termCheck, termCheckError,legalPan,gst,legalEmail,legalMobile,
                email,mobile,pan,aadharNo, uuid, bucketName} = this.state

            if( !legalNameError && !legalEntityTypeError && !cinError && !legalPanError && !legalPanFileError && !gstError && !gstFileError && 
                !legalEmailError && !legalMobileError && !legalBankStatementError && !registeredAddressError && !corporateAddressError &&
                !nameError && !dobError && !emailError && !mobileError && !genderError && !panError && !panFileError && !aadharNoError && !aadharNoFileError &&
                !bankStatementError && !signCheckError && !termCheckError ) 
            {    

                let payload ={
                    referenceId: uuid,
                    legalName: legalName,
                    legalEntityType: legalEntityType,
                    cin: cin,
                    pan: legalPan,
                    panFile: this.state.legalPanFile !== undefined ? this.state.legalPanFile.bucketPath : "",
                    gst: gst,
                    gstFile: this.state.gstFile !== undefined ? this.state.gstFile.bucketPath : "",
                    email: legalEmail,
                    mobile: legalMobile,
                    bankStatementFile: this.state.legalBankStatement !== undefined ? this.state.legalBankStatement.bucketPath : "",
                    registeredAddress: registeredAddress,
                    corporateAddress: corporateAddress,
                    contactName: name,
                    contactDob: dob,
                    contactEmail: email,
                    contactMobile: mobile,
                    contactGender: genderM ? "Male" : genderF ? "Female" : "NA",
                    contactPan: pan,
                    contactPanFile: this.state.panFile !== undefined ? this.state.panFile.bucketPath : "",
                    contactAadhar: aadharNo,
                    contactAadharFile: this.state.aadharNoFile !== undefined ? this.state.aadharNoFile.bucketPath : "",
                    contactBankStatementFile: this.state.bankStatement !== undefined ? this.state.bankStatement.bucketPath : "",
                    authorizedDeclaration: signCheck ? 1 : 0,
                    agreeTermsConditions: termCheck ? 1 : 0,
                    bucketName: bucketName,
                }
                this.props.saveKycDetailsRequest(payload)
            }
        },10);

    }

    cancelKyc =(e)=>{
       this.props.history.push('/home')
    }

    onChangeFun =(e)=>{
        //No need just for ignore warning.
    }

    render () {
        return (
            <div className="container-fluid pad-0">
                <div className="vendor-kyc-form m-top-30">
                    <div className="col-lg-12 pad-0">
                        <div className="vkf-head">
                            <h3>Vendor KYC Form</h3>
                            <p>Know Your Customer(KYC)/ Know Your Supplier(KYS)</p>
                        </div>
                    </div>
                    <div className="col-lg-12 pad-0 m-top-50">
                        <div className="vkf-body">
                            <div className="vkfb-details">
                                <div className="col-lg-12 pad-0">
                                    <div className="vkfb-heading">
                                        <h3>Legal Entity Basic Information <span>( All field are mandatory )</span></h3>
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0 m-top-20">
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Legal Name</label>
                                            <input type="text" className="onFocus" value={this.state.legalName} onChange={(e)=>this.handleChange(e, "legalName")}/>
                                            {this.state.legalNameError && <span className="error">Enter legal name</span>}
                                        </div>
                                    </div>
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Legal Entity Type</label>
                                            <select className="onFocus" value={this.state.legalEntityType} onChange={(e)=>this.handleChange(e,"legalEntityType")}>
                                                <option value="">Select Entity</option>
                                                <option value="Public Limited">Public Limited</option>
                                                <option value="Private Limited">Private Limited</option>
                                                <option value="LLP">LLP</option>
                                                <option value="Partnership">Partnership</option>
                                                <option value="Proprietorship">Proprietorship</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            {this.state.legalEntityTypeError && <span className="error">Select entity</span>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-7 pad-0 m-top-20">
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>CIN</label>
                                            <input type="text" className="onFocus" value={this.state.cin} onChange={(e)=>this.handleChange(e,"cin")}/>
                                            {this.state.cinError && <span className="error">Enter cin</span>}
                                        </div> 
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Pan</label>
                                            <input type="text" className="onFocus" value={this.state.legalPan} onChange={(e)=>this.handleChange(e, "legalPan")}/>
                                            <label className="vkfb-upload">
                                                <input type="file" onChange={(e)=>this.handleChange(e, "legalPanFile")}/>
                                                <img src={require('../../assets/upload-kyc.svg')} />
                                            </label>
                                            {this.state.legalPanFile !== undefined ? <span>{this.state.legalPanFile.fileName}</span> : null}
                                            {this.state.legalPanFileError && <span className="error">Please attach file</span>}
                                            {this.state.legalPanError && <span className="error">max length : 10</span>}
                                        </div> 
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>GST</label>
                                            <input type="text" className="onFocus" value={this.state.gst} onChange={(e)=>this.handleChange(e, "gst")}/>
                                            <label className="vkfb-upload">
                                                <input type="file" onChange={(e)=>this.handleChange(e, "gstFile")}/>
                                                <img src={require('../../assets/upload-kyc.svg')} />
                                            </label>
                                            {this.state.gstFile !== undefined ? <span>{this.state.gstFile.fileName}</span> : null}
                                            {this.state.gstFileError && <span className="error">Please attach file</span>}
                                            {this.state.gstError && <span className="error">max length : 15</span>}
                                        </div> 
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0 m-top-20">
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Email</label>
                                            <input type="text" className="onFocus" value={this.state.legalEmail} onChange={(e)=>this.handleChange(e, "legalEmail")}/>
                                            {this.state.legalEmailError && <span className="error">Enter valid email</span>}
                                        </div>
                                    </div>
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Mobile</label>
                                            <input type="text" className="onFocus" value={this.state.legalMobile} onChange={(e)=>this.handleChange(e, "legalMobile")}/>
                                            {this.state.legalMobileError && <span className="error">Enter 10 digit mobile number</span>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-7 pad-0 m-top-20">
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Bank Statement</label>
                                            <input type="text" className="onFocus" value={this.state.legalBankStatement !== undefined ? this.state.legalBankStatement.fileName : "Please attach file"} onChange={this.onChangeFun}/>
                                            <label className="vkfb-upload" >
                                                <input type="file" onChange={(e)=>this.handleChange(e, "legalBankStatement")}/>
                                                <img src={require('../../assets/upload-kyc.svg')} />
                                            </label>
                                            {this.state.legalBankStatementError && <span className="error">Please attach file</span>}
                                        </div> 
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0 m-top-20">
                                    <div className="col-lg-7 pad-0">
                                        <div className="col-lg-6 pad-lft-0">
                                            <div className="vkfb-field">
                                                <label>Registered Address</label>
                                                <textarea  className="onFocus" value={this.state.registeredAddress} onChange={(e)=>this.handleChange(e, "registeredAddress")}/>
                                                {this.state.registeredAddressError && <span className="error">Enter registered address</span>}
                                            </div> 
                                        </div>
                                        <div className="col-lg-6 pad-lft-0">
                                            <div className="vkfb-field">
                                                <label>Corporate Address</label>
                                                <textarea className="onFocus" value={this.state.corporateAddress} onChange={(e)=>this.handleChange(e, "corporateAddress")}/>
                                                {this.state.corporateAddressError && <span className="error">Enter corporate address</span>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="vkfb-details m-top-40">
                                <div className="col-lg-12 pad-0">
                                    <div className="vkfb-heading">
                                        <h3>Director/Partner/Individual Info <span>( All fields are mandatory )</span></h3>
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0 m-top-20">
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Name</label>
                                            <input type="text" className="onFocus" value={this.state.name} onChange={(e)=>this.handleChange(e, "name")}/>
                                            {this.state.nameError && <span className="error">Enter name</span>}
                                        </div>
                                    </div>
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Date of Birth</label>
                                            <input type="date" placeholder={this.state.dob == "" ? "Select DOB" : this.state.dob} className="onFocus" value={this.state.dob} onChange={(e)=>this.handleChange(e, "dob")}/>
                                            {this.state.dobError && <span className="error">Choose Dob</span>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-7 pad-0 m-top-20">
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Email</label>
                                            <input type="text" className="onFocus" value={this.state.email} onChange={(e)=>this.handleChange(e, "email")}/>
                                            {this.state.emailError && <span className="error">Enter valid email</span>}
                                        </div> 
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Mobile</label>
                                            <input type="text" className="onFocus" value={this.state.mobile} onChange={(e)=>this.handleChange(e, "mobile")}/>
                                            {this.state.mobileError && <span className="error">Enter 10 digit mobile number</span>}
                                        </div> 
                                    </div>
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Gender</label>
                                            <div className="vkfbf-gender">
                                                <label className="vkfb-radio">
                                                    <input type="radio" name="genderM" checked={this.state.genderM} onClick={(e)=>this.handleChange(e, "genderM")} onChange={this.onChangeFun}/>
                                                    <span className="checkmarkgen">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20.833" viewBox="0 0 20 20.833">
                                                            <path fill="#97abbf" d="M13.75 11.25a1.161 1.161 0 0 1-1.042 1.25 1.161 1.161 0 0 1-1.042-1.25A1.161 1.161 0 0 1 12.708 10a1.161 1.161 0 0 1 1.042 1.25zM7.292 10a1.161 1.161 0 0 0-1.042 1.25 1.161 1.161 0 0 0 1.042 1.25 1.161 1.161 0 0 0 1.042-1.25A1.161 1.161 0 0 0 7.292 10zM20 11.927a4.49 4.49 0 0 1-2.616 4.088c-1.719 2.746-4.156 4.817-7.384 4.817-3.118 0-5.7-2.135-7.384-4.817A4.492 4.492 0 0 1 0 11.927 3.268 3.268 0 0 1 1.181 9.02c-.325-1.27-.858-4.288.8-6.174a3.706 3.706 0 0 1 3.436-1.062C6.016.507 8.427 0 10 0c2.5 0 5.508.534 7.617 2.93 1.641 1.863 1.373 4.784 1.157 6.058A3.232 3.232 0 0 1 20 11.927zm-8 3.906H8a2.366 2.366 0 0 0 2 1.567 2.277 2.277 0 0 0 2-1.567zm5.886-5.423c-3.425.328-6.482-2.548-7.561-4.395a4.187 4.187 0 0 0 .807 2.8 7.46 7.46 0 0 1-6.216-3.227 7.8 7.8 0 0 0-1.035 3.979.834.834 0 0 1-1.172.762.627.627 0 0 0-.559.044c-.833.54-.746 3.486 1.294 4.177a.837.837 0 0 1 .447.358 12.1 12.1 0 0 0 1.738 2.268c.768.528.88-1.562 1.389-2.267A2.282 2.282 0 0 1 10 14.641a2.282 2.282 0 0 1 2.982.268c.509.7.621 2.795 1.389 2.267a12.139 12.139 0 0 0 1.738-2.268.841.841 0 0 1 .447-.358c1.969-.667 2.163-3.488 1.33-4.14z"/>
                                                        </svg>
                                                    </span>
                                                    <span className="checkmark-text">Male</span>
                                                </label>
                                                <label className="vkfb-radio">
                                                    <input type="radio" name="genderF" checked={this.state.genderF} onClick={(e)=>this.handleChange(e, "genderF")} onChange={this.onChangeFun}/>
                                                    <span className="checkmarkgen">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18.462" viewBox="0 0 20 18.462">
                                                            <path fill="#97abbf" d="M13.462 9.615a1.071 1.071 0 0 1-.962 1.154 1.071 1.071 0 0 1-.962-1.154 1.071 1.071 0 0 1 .962-1.153 1.071 1.071 0 0 1 .962 1.153zM7.5 8.462a1.071 1.071 0 0 0-.962 1.154 1.071 1.071 0 0 0 .962 1.153 1.071 1.071 0 0 0 .962-1.154A1.071 1.071 0 0 0 7.5 8.462zm2.5 6.426a2.628 2.628 0 0 0 2.308-1.811H7.692A2.73 2.73 0 0 0 10 14.888zM20 9.7a15.448 15.448 0 0 1-1.487 6.341 15.183 15.183 0 0 1-1.4 2.425L14.2 16.987a6.8 6.8 0 0 1-4.2 1.475 6.9 6.9 0 0 1-4.16-1.493l-2.955 1.493a15.183 15.183 0 0 1-1.4-2.425A15.448 15.448 0 0 1 0 9.7C0 3.857 4.062 0 10 0s10 3.857 10 9.7zM15.639 13a.776.776 0 0 1 .412-.331 2.709 2.709 0 0 0 1.387-3.659c-5.058-.3-8.377-2.587-9.745-4.863-1.736 3.895-3.906 5.1-5.279 5.227a2.69 2.69 0 0 0 1.535 3.287.773.773 0 0 1 .412.331c1.314 2.176 3.088 3.931 5.639 3.931s4.326-1.757 5.639-3.931z"/>
                                                        </svg>
                                                    </span>
                                                    <span className="checkmark-text">Female</span>
                                                </label>
                                            </div>
                                            {this.state.genderError && <span className="error">Select gender</span>}
                                        </div> 
                                    </div>
                                </div>
                                <div className="col-lg-5 pad-0 m-top-20">
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Pan</label>
                                            <input type="text" className="onFocus" value={this.state.pan} onChange={(e)=>this.handleChange(e, "pan")}/>
                                            <label className="vkfb-upload">
                                                <input type="file" onChange={(e)=>this.handleChange(e, "panFile")}/>
                                                <img src={require('../../assets/upload-kyc.svg')} />
                                            </label>
                                            {this.state.panFile !== undefined ? <span>{this.state.panFile.fileName}</span> : null}
                                            {this.state.panFileError && <span className="error">Please attach file</span>}
                                            {this.state.panError && <span className="error">max length : 10</span>}
                                        </div>  
                                    </div>
                                    <div className="col-lg-6 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Aadhar Number</label>
                                            <input type="text" className="onFocus" value={this.state.aadharNo} onChange={(e)=>this.handleChange(e, "aadharNo")}/>
                                            <label className="vkfb-upload">
                                                <input type="file" onChange={(e)=>this.handleChange(e, "aadharNoFile")}/>
                                                <img src={require('../../assets/upload-kyc.svg')} />
                                            </label>
                                            {this.state.aadharNoFile !== undefined ? <span>{this.state.aadharNoFile.fileName}</span> : null}
                                            {this.state.aadharNoFileError && <span className="error">Please attach file</span>}
                                            {this.state.aadharNoError && <span className="error">max length : 12</span>}
                                        </div> 
                                    </div>
                                </div>
                                <div className="col-lg-7 pad-0 m-top-20">
                                    <div className="col-lg-4 pad-lft-0">
                                        <div className="vkfb-field">
                                            <label>Bank Statement</label>
                                            <input type="text" className="onFocus" value={this.state.bankStatement !== undefined ? this.state.bankStatement.fileName : "Please attach file"} onChange={this.onChangeFun}/>
                                            <label className="vkfb-upload" >
                                                <input type="file" onChange={(e)=>this.handleChange(e, "bankStatement")}/>
                                                <img src={require('../../assets/upload-kyc.svg')} />
                                            </label>
                                            {this.state.bankStatementError && <span className="error">Please attach file</span>}
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 pad-0 m-top-40">
                        <div className="vlf-footer">
                            <div className="vlf-detail">
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" checked={this.state.signCheck} onClick={(e)=>this.handleChange(e, "signCheck")} onChange={this.onChangeFun}/>
                                    <span className="checkmark1"></span>
                                    <span className="checkmarktext">
                                        I hereby declare that I am the authorized signatory.
                                    </span>
                                    {this.state.signCheckError && <span className="error">Sign check mandatory</span>}
                                </label>
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" checked={this.state.termCheck} onClick={(e)=>this.handleChange(e, "termCheck")} onChange={this.onChangeFun}/>
                                    <span className="checkmark1"></span>
                                    <span className="checkmarktext">
                                        I have read and agree to the <a>Terms & Conditions.</a>
                                    </span>
                                    {this.state.termCheckError && <span className="error">Terms check mandatory</span>}
                                </label>
                            </div>
                            <div className="vlf-btn">
                                <button type="button" className="vlfb-submit" onClick={this.submitKyc}>Submit Request</button>
                                <button type="button" className="vlfb-cancel" onClick={this.cancelKyc}>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
            </div>
        )
    }
}

export default VendorKyc;