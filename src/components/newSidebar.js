import React from "react";
import Header from "./header";
import { Link } from "react-router-dom";
import profile from "../assets/avatar.svg";
import SessionTimeOut from './timeOutModals/sessionExpireTimerModal';
import IdleTimer from 'react-idle-timer'
import SessionExpired from './timeOutModals/sessionExpiredModal';
import RightArrow from '../assets/rightArrow.svg';
import DigiPlanIcon from '../assets/digiPlanIcon.svg';

class NewSideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            over: false,
            firstShow: false,
            inactivityPopupS: false,
            expired: false,
            modules: [],
            showSubmenu: true,
            logoutMenu: false,
            hideMenu: false,
            showUpDown: false,
            visibleIcons: 1,
            topIcon: 0,
            extraSpace: 0
        };
        this.idleTimer = null
        this.onAction = this._onAction.bind(this)
        this.onActive = this._onActive.bind(this)
        this.onIdle = this._onIdle.bind(this)
    }
    openHideMenu(e) {
        e.preventDefault();
        this.setState({
            hideMenu: !this.state.hideMenu
        }
        );
    }
    openLogoutMenu(e) {
        e.preventDefault();
        this.setState({
            logoutMenu: !this.state.logoutMenu
        }
        // , () => document.addEventListener('click', this.closeLogoutMenu)
        );
    }
    closeLogoutMenu = () => {
        this.setState({ logoutMenu: false 
        // }, () => {document.removeEventListener('click', this.closeLogoutMenu);
        });
    }

    componentWillMount() {
        // if (sessionStorage.getItem('logout') == "true") {
        //     sessionStorage.clear();
        //     this.props.history.push('/');
        // }
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }
    }

    componentDidMount() {
        // if (sessionStorage.getItem('logout') == "true") {
        //     sessionStorage.clear();
        //     this.props.history.push('/');
        // }
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }
        this.changeIcon()
        this.setState({ modules: JSON.parse(sessionStorage.getItem('modules')) }, this.compareHeight)
        // document.getElementById(sessionStorage.getItem("currentMenuId"))
        window.addEventListener("resize", this.compareHeight);
    }

    compareHeight = () => {
        let iconsCount = this.state.modules[sessionStorage.getItem('mid')] != undefined ? this.state.modules[sessionStorage.getItem('mid')].length + 1 : 1;
        let iconsHeight = iconsCount * 70;
        let sideBarHeight = document.getElementById("mainUl").clientHeight - 110
        if (sideBarHeight < iconsHeight) {
            this.setState({
                showUpDown: true,
                visibleIcons: Math.floor(sideBarHeight / 70)
            });
        }
        else {
            this.setState({
                showUpDown: false,
                visibleIcons: iconsCount,
                topIcon: 0,
                extraSpace: 0
            });
        }
    }

    shiftIcons = (dir) => {
        let iconsCount = this.state.modules[sessionStorage.getItem('mid')] != undefined ? this.state.modules[sessionStorage.getItem('mid')].length + 1 : 1;
        if (dir === "down") {
            this.setState(prevState => {
                if (prevState.topIcon + prevState.visibleIcons < iconsCount - 1) {
                    return {
                        topIcon: prevState.topIcon + 1
                    }
                }
                else {
                    let extraSpace = 70 - ((document.getElementById("mainUl").clientHeight - 110) % 70) ;
                    return {
                        extraSpace: extraSpace
                    };
                }
            });
        }
        else if (dir === "up") {
            this.setState(prevState => {
                if (prevState.extraSpace !== 0) {
                    return {
                        extraSpace: 0
                    };
                }
                else if (prevState.topIcon !== 0) {
                    return {
                        topIcon: prevState.topIcon - 1
                    };
                }
            });
        }
    }

    changeIcon = () => {
        if (
            document.getElementsByClassName('sub-menu')
                .length > 0 &&
            document.getElementsByClassName('sub-menu').length > 0
        ) {
            var h1 = document.querySelectorAll('.sub-menu');
            for (var i = 0; i < h1.length; i++) {
                var bounding = h1[i].getBoundingClientRect()
                if (bounding.top >= 0 &&
                    bounding.left >= 0 &&
                    bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    bounding.right <= (window.innerWidth || document.documentElement.clientWidth)) {
                } else {
                    h1[i].className += " last-sub-menu-bottom-up";
                }
            }
        } else {
            setTimeout(this.changeIcon, 5000);
        }
    }
    
    componentWillReceiveProps(nextProps) {
        // if (sessionStorage.getItem('logout') == "true") {
        //     sessionStorage.clear();
        //     this.props.history.push('/');
        // }
        // if (sessionStorage.getItem('token') == null) {
        //     this.props.history.push('/');
        // }
    }

    _onAction() {

    }

    _onActive() {

    }

    _onIdle() {
        if (!this.state.expired) {
            this.inactivityPopupOpen();
        }
    }


    inactivityPopupOpen() {
        this.setState({
            inactivityPopup: true
        })
    }
    inactivityPopupClose(type) {
        if (type == 'auto') {
            sessionStorage.setItem('logout', true)
            this.setState({
                inactivityPopup: false,
                expired: true
            })
        } else {
            this.setState({
                inactivityPopup: false
            })
        }
    }

    isActiveSubLink(subLink) {
        return window.location.hash.split("/")[2] == subLink;
    }

    isActiveLink(menuLink) {
        return window.location.hash.split("/")[1] == `${menuLink}`;
    }
    isCustomActive(customLink) {
        return window.location.hash.split("/")[3] == `${customLink}`;
    }

    onShowHide() {
        this.setState({
            show: !this.state.show,
            firstShow: !this.state.firstShow
        });
    }
    renderShit(root, data) {
        if (Array.isArray(root) && root.length > 0 && root.length != undefined) {
            return (
                <ul className="sub-menu" onClick={this.activeMenu} id="subMenu">
                    {data != undefined && <li className="sm-head"><span className="sm-head-inner">{data.name}</span></li>}
                    {root.map(node => (
                        <React.Fragment>
                            {(data != undefined && sessionStorage.getItem('currentPage') == data.code) && sessionStorage.setItem('parentName', data.name), sessionStorage.getItem('currentPage') == node.code && sessionStorage.setItem('parentName', node.name)}
                            <li className="sm-inner-item">{node.subModules != undefined && (`${node.name}`)}{node.subModules != undefined && 
                            // <img className="drop-menu-right-arrow" src={RightArrow} />
                            <svg className="drop-menu-right-arrow" xmlns="http://www.w3.org/2000/svg" width="6.485" height="10.97" viewBox="0 0 6.485 10.97">
                                <path fill="none" stroke="#000000" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2px" id="prefix__Path_206" d="M6 9l4.071 4.071L14.142 9" data-name="Path 206" transform="rotate(-90 3.985 11.571)" />
                            </svg>
                            }{node.subModules == undefined ? this.renderShit(node) : this.renderShit(node.subModules)}</li>
                        </React.Fragment>
                    ))}
                </ul>
            );
        }
        else {
            return <span className="sm3-inner-item" onClick={() => this.openPage(root)}> {root.name}</span>;
        }
    }

    activeMenu = (e) => {
        if (e.target.id == "homeUrl") {
            sessionStorage.setItem('activeMenu', e.target.closest("li.h2-main-item").id)
        } else {
            e.target.closest("li.h2-main-item").classList.add("active")
            sessionStorage.setItem('activeMenu', e.target.closest("li.h2-main-item").id)
        }
    }
    openPage = (root, data) => {
        this.props.history.push(root.pageUrl.replace('#', ''))
        sessionStorage.setItem('currentPage', root.parentCode)
        sessionStorage.setItem('currentPageName', root.name)
        sessionStorage.setItem('isDashboardComment', 0);
        sessionStorage.setItem("inspectionStatus", "");
        sessionStorage.setItem("lrProcessingApprovedStatus", "");
        sessionStorage.setItem("cancelledInspection", "");
        //For Redirection of Rejected ASN at Vendor Side::
        sessionStorage.setItem('isDashboardShipment', 0)
    }
    onLogout(e) {
        e.preventDefault();
        this.props.dashboardTilesRequest();
        this.props.storesArticlesRequest();
        this.props.salesTrendGraphRequest();
        this.props.slowFastArticleRequest();
        let payload = {
          userName: sessionStorage.getItem('userName'),
          token: sessionStorage.getItem('token'),
          type: "LOGGEDOUT",
          emailId: sessionStorage.getItem('email'),
          keepMeSignIn: "false",
        }
        this.props.userSessionCreateRequest(payload);
        this.props.userSessionCreateRequest();
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('partnerEnterpriseId');
        sessionStorage.removeItem('partnerEnterpriseName');
        sessionStorage.removeItem('firstName');
        sessionStorage.removeItem('lastName');
        sessionStorage.removeItem('roles');
        sessionStorage.removeItem('storeCode');
        document.getElementById("chatIconContainer").style.display='none';
        sessionStorage.clear();
        this.props.history.push('/');
    }

    render() {
        let id = sessionStorage.getItem('activeMenu')
        if (id !== null && document.getElementById(id) !== null) {
            document.getElementById(id).classList.add('active')
        }
        let miid = JSON.parse(sessionStorage.getItem('mid'));
        let role = "";
        let data = JSON.parse(sessionStorage.getItem('roles'));
        if (data != null) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == miid) {
                    role = data[i].name;
                }
            }
        }


        var Ad_Hoc_Request = JSON.parse(sessionStorage.getItem('Ad-Hoc Request')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Ad-Hoc Request')).crud);
        var Administration = JSON.parse(sessionStorage.getItem('Administration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Administration')).crud);
        var ARS = JSON.parse(sessionStorage.getItem('ARS')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ARS')).crud);
        var Add_New_Vendors = JSON.parse(sessionStorage.getItem('Add New Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Add New Vendors')).crud);
        var Allocation_Report = JSON.parse(sessionStorage.getItem('Allocation Report')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Allocation Report')).crud);
        var Analytics = JSON.parse(sessionStorage.getItem('Analytics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Analytics')).crud);
        var Assortment = JSON.parse(sessionStorage.getItem('Assortment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Assortment')).crud);
        var Custom_Masters = JSON.parse(sessionStorage.getItem('Custom Masters')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Custom Masters')).crud);
        var Data_Sync = JSON.parse(sessionStorage.getItem('Data Sync')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Data Sync')).crud);
        var Demand_Planning = JSON.parse(sessionStorage.getItem('Demand Planning')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Demand Planning')).crud);
        var Fast_Moving_Articles = JSON.parse(sessionStorage.getItem('Fast Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Fast Moving Articles')).crud);
        var History = JSON.parse(sessionStorage.getItem('History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('History')).crud);
        var Inventory_Classification = JSON.parse(sessionStorage.getItem('Inventory Classification')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Inventory Classification')).crud);
        var Inventory_Planning = JSON.parse(sessionStorage.getItem('Inventory Planning')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Inventory Planning')).crud);
        var Item = JSON.parse(sessionStorage.getItem('Item')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item')).crud);
        var Item_Configuration = JSON.parse(sessionStorage.getItem('Item Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Configuration')).crud);
        var Item_Mapping = JSON.parse(sessionStorage.getItem('Item Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item Mapping')).crud);
        var Item_route = JSON.parse(sessionStorage.getItem('Item route')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item route')).crud);
        var Manage_Users = JSON.parse(sessionStorage.getItem('Manage Users')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Users')).crud);
        var Manage_Vendors = JSON.parse(sessionStorage.getItem('Manage Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Vendors')).crud);
        var Moderate_Moving_Articles = JSON.parse(sessionStorage.getItem('Moderate Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Moderate Moving Articles')).crud);
        var Monthly = JSON.parse(sessionStorage.getItem('Monthly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly')).crud);
        var New_Articles = JSON.parse(sessionStorage.getItem('New Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('New Articles')).crud);
        var Organization = JSON.parse(sessionStorage.getItem('Organization')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Organization')).crud);

        var Priority_List = JSON.parse(sessionStorage.getItem('Priority List')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Priority List')).crud);

        var Replenishment = JSON.parse(sessionStorage.getItem('Replenishment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Replenishment')).crud);
        var Roles = JSON.parse(sessionStorage.getItem('Roles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Roles')).crud);
        var Sales_Achieved = JSON.parse(sessionStorage.getItem('Sales Achieved')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Sales Achieved')).crud);
        var Schedulers = JSON.parse(sessionStorage.getItem('Schedulers')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Schedulers')).crud);
        var Sell_Throughs = JSON.parse(sessionStorage.getItem('Sell Throughs')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Sell Throughs')).crud);
        var Site = JSON.parse(sessionStorage.getItem('Site')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site')).crud);
        var Site_Mapping = JSON.parse(sessionStorage.getItem('Site Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Site Mapping')).crud);
        var Stale_Moving_Articles = JSON.parse(sessionStorage.getItem('Stale Moving Articles')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Stale Moving Articles')).crud);
        var Store_Profiling = JSON.parse(sessionStorage.getItem('Store Profiling')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Store Profiling')).crud);
        var Store_Ranking = JSON.parse(sessionStorage.getItem('Store Ranking')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Store Ranking')).crud);
        var Summary = JSON.parse(sessionStorage.getItem('Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Summary')).crud);
        var Vendor_Management = JSON.parse(sessionStorage.getItem('Vendor Management')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Vendor Management')).crud);
        var Auto_Configuration = JSON.parse(sessionStorage.getItem('Auto Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Auto Configuration')).crud);
        var Run_on_Demand = JSON.parse(sessionStorage.getItem('Run on Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Run on Demand')).crud);
        var Weekly = JSON.parse(sessionStorage.getItem('Weekly')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly')).crud);
        var Retail = JSON.parse(sessionStorage.getItem('Retail Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Retail Master')).crud);
        var MBO = JSON.parse(sessionStorage.getItem('MBO Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('MBO Master')).crud);
        var Budgeted_Sales = JSON.parse(sessionStorage.getItem('Budgeted Sales')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales')).crud);
        var Budgeted_Sales_History = JSON.parse(sessionStorage.getItem('Budgeted Sales History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Budgeted Sales History')).crud);
        var Forecast_On_Demand = JSON.parse(sessionStorage.getItem('Forecast On Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast On Demand')).crud);
        var Weekly_History = JSON.parse(sessionStorage.getItem('Weekly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Weekly History')).crud);
        var Monthly_History = JSON.parse(sessionStorage.getItem('Monthly History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Monthly History')).crud);
        var Forecast_History = JSON.parse(sessionStorage.getItem('Forecast History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Forecast History')).crud);
        //procurement
        var Procurement = JSON.parse(sessionStorage.getItem('Procurement')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Procurement')).crud);
        var Purchase_Indent = JSON.parse(sessionStorage.getItem('Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Indent')).crud);
        var Purchase_Order = JSON.parse(sessionStorage.getItem('Purchase Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order')).crud);
        var Purchase_Order_With_Upload = JSON.parse(sessionStorage.getItem('Purchase Order File Upload')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order File Upload')).crud);
        var PI_History = JSON.parse(sessionStorage.getItem('PI History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PI History')).crud);
        var PO_History = JSON.parse(sessionStorage.getItem('PO History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PO History')).crud);
        var Item_UDF_Mapping = JSON.parse(sessionStorage.getItem('Item UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Mapping')).crud)
        var Item_UDF_Setting = JSON.parse(sessionStorage.getItem('Item UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Setting')).crud)
        var UDF_Mapping = JSON.parse(sessionStorage.getItem('UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Mapping')).crud)
        var UDF_Setting = JSON.parse(sessionStorage.getItem('UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Setting')).crud)
        var Dept_Size_Mapping = JSON.parse(sessionStorage.getItem('Dept Size Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Dept Size Mapping')).crud)
        var Configuration = JSON.parse(sessionStorage.getItem('Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Configuration')).crud)
        var File_Upload_History = JSON.parse(sessionStorage.getItem('File Upload History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('File Upload History')).crud)
        // ANALYTICS
        var FMCG_Master = JSON.parse(sessionStorage.getItem('FMCG Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('FMCG Master')).crud)
        var UPLOAD_MASTER = JSON.parse(sessionStorage.getItem('Upload Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Upload Master')).crud)
        var manage_rule = JSON.parse(sessionStorage.getItem('Manage Rule Engines')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Rule Engines')).crud)
        var Event_Master = JSON.parse(sessionStorage.getItem('Event Master')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Event Master')).crud)

        var Purchase_Orders = JSON.parse(sessionStorage.getItem('Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Orders')).crud)
        var Purchase_Orders_History = JSON.parse(sessionStorage.getItem('Purchase Order History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order History')).crud)

        var Cancelled_Order = JSON.parse(sessionStorage.getItem('Cancelled Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled Orders')).crud)
        var Pending_Orders = JSON.parse(sessionStorage.getItem('Pending Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Pending Orders')).crud)

        var Asn_Under_Approval = JSON.parse(sessionStorage.getItem('ASN Under Approval')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ASN Under Approval')).crud)
        var Approved_Asn = JSON.parse(sessionStorage.getItem('Approved ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Approved ASN')).crud)
        var Cancelled_Asn = JSON.parse(sessionStorage.getItem('Cancelled ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled ASN')).crud)
        var Lr_Processing = JSON.parse(sessionStorage.getItem('LR Processing')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('LR Processing')).crud)
        var Goods_Intransit = JSON.parse(sessionStorage.getItem('Goods Intransit')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Intransit')).crud)
        var Goods_Delivered = JSON.parse(sessionStorage.getItem('Goods Delivered')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Delivered')).crud)
        var Logistics = JSON.parse(sessionStorage.getItem('Logistics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Logistics')).crud)
        var Order = JSON.parse(sessionStorage.getItem('Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Order')).crud)
        var Shipment = JSON.parse(sessionStorage.getItem('Shipment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment')).crud)
        var Shipment_Tracking = JSON.parse(sessionStorage.getItem('Shipment Tracking')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment Tracking')).crud)
        var Processed_Orders = JSON.parse(sessionStorage.getItem('Processed Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Processed Orders')).crud)

        return (

            <div>
                <div className="sidebar-menu2" id="sidebarDiv">
                    <ul className="generic-sidebar-container main-menu2" id="mainUl">
                        <li className={this.state.showUpDown ? "h2-main-item show-more-item-up show-hide-item" : "h2-main-item show-more-item-up"} onClick={() => this.shiftIcons("up")}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.541" height="7.271" viewBox="0 0 10.541 6.271">
                                <path fill="none" stroke="#8b77fa" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2px" id="prefix__Path_409" d="M6 9l3.856 3.856L13.713 9" data-name="Path 409" transform="translate(-4.586 -7.586)" />
                            </svg>
                        </li>
                        <div className="gsc-top-items" style={this.state.showUpDown ? {top: `${(this.state.topIcon * -70) - this.state.extraSpace + 25}px`} : {top: "0px"}}>
                            {/* <div id="mainUl2"> */}
                                <li className="h2-main-item" id="home-page"><a className="h2mi-home" id="homeUrl" onClick={this.activeMenu} href="#/home">Home</a></li>
                                {this.state.modules[sessionStorage.getItem('mid')] != undefined && this.state.modules[sessionStorage.getItem('mid')].map((_, key) => (
                                    <li className="h2-main-item" id={`main${key}`} key={key}>{_.iconUrl === null ? <img src={require(`../assets/07.svg`)} /> : <img src={require(`../assets/${_.iconUrl}`)} />}
                                        {Array.isArray(_.subModules) && this.renderShit(_.subModules, _)}
                                    </li>
                                ))}
                            {/* </div> */}
                        </div>
                        {/* {this.state.showUpDown && <li className="h2-main-item extra-space" style={{height: `${this.state.extraSpace}px`, top: `${this.state.extraSpace - 70}px`}}></li>} */}
                        <li className={this.state.showUpDown ? "h2-main-item show-more-item show-hide-item" : "h2-main-item show-more-item"} onClick={() => this.shiftIcons("down")} id="newUl">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.541" height="7.271" viewBox="0 0 10.541 6.271">
                                <path fill="none" stroke="#8b77fa" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2px" id="prefix__Path_409" d="M6 9l3.856 3.856L13.713 9" data-name="Path 409" transform="translate(-4.586 -7.586)" />
                            </svg>
                        </li>
                        <li className={this.state.logoutMenu === false ? "h2-main-item h2-lobtn" : "h2-main-item h2-lobtn h2l-open"} onClick={(e) => this.openLogoutMenu(e)}>
                            <a href="javascript:void(0);">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20.469" height="21.271" viewBox="0 0 20.469 21.271">
                                    <g id="prefix__logout" transform="translate(-1.492 17)">
                                        <g id="prefix__Group_1970" data-name="Group 1970" transform="translate(2.62 -16)">
                                            <g id="prefix__Group_1966" data-name="Group 1966" transform="translate(0 1.752)">
                                                <path fill="none" stroke="#eb0000" stroke-linecap="round" stroke-linejoin="round" strokeWidth="2px" id="prefix__Path_392" d="M4.036 0A9.847 9.847 0 00.4 10.763a9.1 9.1 0 008.71 6.756 9.1 9.1 0 008.71-6.756A9.847 9.847 0 0014.178 0" className="prefix__cls-1" data-name="Path 392" />
                                            </g>
                                            <g id="prefix__Group_1967" data-name="Group 1967" transform="translate(9.107)">
                                                <path fill="none" stroke="#eb0000" stroke-linecap="round" stroke-linejoin="round" strokeWidth="2px" id="prefix__Path_393" d="M0 0v9.635" className="prefix__cls-1" data-name="Path 393" />
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                            {this.state.logoutMenu && 
                            <div className="hmi-logout">
                                <p>Are you sure you want to logout?</p>
                                <button type="button" className="hmil-yes" onClick={(e) => this.onLogout(e)}>Yes</button>
                                <button type="button" className="" onClick={this.closeLogoutMenu}>No</button>
                            </div>}
                        </li>
                    </ul>
                </div>
                <IdleTimer
                    ref={ref => { this.idleTimer = ref }}
                    element={document}
                    onActive={this.onActive}
                    onIdle={this.onIdle}
                    onAction={this.onAction}
                    debounce={250}
                    timeout={1000 * 10800} />
                {this.state.inactivityPopup ? <SessionTimeOut inactivityPopupClose={(e) => this.inactivityPopupClose(e)} /> : null}
                {sessionStorage.getItem('logout') == "true" ? <SessionExpired {...this.props} /> : null}
            </div>
        )
    }
}

export default NewSideBar;
