import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper'
class InventoryPlanningDrop extends React.Component {

  render() {
    var Schedulers = JSON.parse(sessionStorage.getItem('Schedulers')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Schedulers')).crud);
    var History = JSON.parse(sessionStorage.getItem('History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('History')).crud);
    var Summary = JSON.parse(sessionStorage.getItem('Summary')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Summary')).crud);
    var Auto_Configuration = JSON.parse(sessionStorage.getItem('Auto Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Auto Configuration')).crud);
    var Run_on_Demand = JSON.parse(sessionStorage.getItem('Run on Demand')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Run on Demand')).crud);
    var Assortment = JSON.parse(sessionStorage.getItem('Assortment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Assortment')).crud);
    var Allocation_Report = JSON.parse(sessionStorage.getItem('Allocation Report')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Allocation Report')).crud);
    var Ad_Hoc_Request = JSON.parse(sessionStorage.getItem('Ad-Hoc Request')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Ad-Hoc Request')).crud);
    var Manage_Rule_Engine = JSON.parse(sessionStorage.getItem('Manage Rule Engine')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Rule Engine')).crud)

    var Rule_Engine_Configuration = JSON.parse(sessionStorage.getItem('Rule Engine Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Rule Engine Configuration')).crud)


    return (
      <label>
        <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
          <button className="dropbtn home_link">Inventory Planning
              <i className="fa fa-chevron-down"></i>
          </button>
          {/* <div className="displayNone adminBreacrumbsDropdown anchorError" id="dropMenu">
            {Schedulers > 0 ? <span className="scheduler">Replenishment<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>

              <div className="hovershowDropdown subDropPos anchorErrorSubMenu">
                {Schedulers > 0 ? <span className="scheduler schedulerHover">Scheduler<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
                  <div className="hovershowDropdownScheduler subDropPos">
                    {Auto_Configuration > 0 ? <Link to="/inventoryPlanning/inventoryAutoConfig" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Auto Configuration</Link> : null}
                    {Run_on_Demand > 0 ? <Link to="/inventoryPlanning/runOnDemand" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Run-On-Demand</Link> : null}
                  </div>
                </span> : null}
                {Rule_Engine_Configuration>0?<Link to="/inventoryPlanning/configuration" id="menuFavItem">Configuration</Link>:null}
                {Manage_Rule_Engine>0?<Link to="/inventoryPlanning/manageRuleEngine" id="menuFavItem">Manage Rule Engine</Link>:null}


                {Summary > 0 ? <Link to="/inventoryPlanning/summary" onClick={(e) => closeMyDrop(e)} className="menuFavItem">Summary</Link> : null}
                {History > 0 ? <Link to="/inventoryPlanning/history" onClick={(e) => closeMyDrop(e)} className="menuFavItem">History</Link> : null}
              </div>
            </span> : null}
            <Link to="/demandPlanning/custom">Replenishment</Link>


            {Allocation_Report > 0 ?<Link to="/inventoryPlanning/allocationReport"> Allocation Report</Link>:null}
            {Assortment > 0 ? <Link to="/inventoryPlanning/assortment" onClick={(e) => closeMyDrop(e)} className="menuFavItem"> Assortment</Link> : null}
            {Ad_Hoc_Request > 0 ? <Link to="/inventoryPlanning/manageAd-Hoc" onClick={(e) => closeMyDrop(e)} className="menuFavItem"> Ad-Hoc Request</Link> : null}

          </div> */}
        </div>
      </label>

    );
  }
}

export default InventoryPlanningDrop;