import React from "react";
import openRack from "../assets/open-rack.svg"
import openRackRight from "../assets/rack-open-right.svg"

class RightSideBar extends React.Component {
  render() {
    return (
      <div>
             
      {/* <nav className={this.props.rsbar ? "right_sidebar rightShow" : "right_sidebar rightHide"} id="rightsidebar">
          <div onClick={() => this.props.clickRightSideBar()}>
       <img  src={openRackRight} onClick={() => this.props.rightSideBar()}  className="right_sidemenu1" /> 
            
          </div> */}
          <nav className="right_sidebar" id="rightsidebar">
            <div>
              <img src={openRackRight} className="right_sidemenu1" /> 
            </div>
        <div className="col-md-12 col-sm-12">
          <ul className="pad-0">
            <li>
              {/* <h2 className="right_menu_head">Heading</h2> */}
            </li>
            {/* <li>
              <p className="para_right_menu">
                Supplymint is designed to track and manage all IT and non-IT
                Assets of organizations. Each company wants to track the status
                and condition of their all assets to improve their productivity
                and lower the maintenance cost. This solution will help in
                achieving the same.
              </p>
            </li> */}

            <li>
              {/* <p className="content_goes m-top-5">Content Goes Here…</p> */}
            </li>
          </ul>
        </div>
      </nav>
        </div>
    );
  }
}

export default RightSideBar;
