import React from "react";
import AddTransporter from "./Managetransporter/addTransporter";
import EmptyBox from "./Managetransporter/emptyBox";
import ViewTransporter from "./Managetransporter/viewTransporter";
import TransporterData from "../../json/transporterData.json";

class Contracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transporterState: [],
      // addTransporter: false
    };
  }

  onAddTransporter() {
    // this.setState({
    //   addTransporter: true
    // });
    this.props.history.push('/vendor/contracts/addVendor')

  }

  onSaveTransporter() {
    // this.setState({
      // addTransporter: false,
      // transporterState: TransporterData
    // });
    this.props.history.push('/vendor/contracts/viewVendor')
  }

  render() {
    
    const hash = window.location.hash.split("/")[3];
    
    return (
      <div className="container-fluid">
         { hash == "emptyBox" ?
             <EmptyBox
             {...this.props}
             transporterState = {this.state.transporterState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addTransporter={() => this.onAddTransporter()}
            />
           : hash == "viewTransporter" ? 
            <ViewTransporter
              TransporterData={TransporterData}
              {...this.props}
              clickRightSideBar={() => this.props.rightSideBar()}
            />
            
         : hash == "addTransporter" ?
          <AddTransporter
            {...this.props}
            saveTransporter={() => this.onSaveTransporter()}
            clickRightSideBar={() => this.props.rightSideBar()}
          />: null} 
      </div>
    );
  }
}

export default Contracts;
