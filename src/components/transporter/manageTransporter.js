import React from "react";
import AddTransporter from "./Managetransporter/addTransporter";
import EmptyBox from "./Managetransporter/emptyBox";
import ViewTransporter from "./Managetransporter/viewTransporter";
import TransporterData from "../../json/transporterData.json";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

class ManageTransporter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transporterState: [],
      // addVendor: false
      page: 1,
      success: false,
      alert: false,
      loader: false,
      errorMessage: "",
      successMessage: "",
      emptyBox: false,
      viewTransporter: false,

      errorCode: "",
      code: "",
    };
  }


  // componentWillMount() {
  //   if (!this.props.vendor.getTransporter.isSuccess) {
  //     // this.props.vendorRequest(this.state.page);
  //   } else {
  //     this.setState({
  //       loader: false
  //     })
  //     if (this.props.vendor.getTransporter.data.resource == null) {
  //       this.setState({
  //         emptyBox: true,
  //         viewTransporter: false
  //       })
  //     } else {
  //       this.setState({
  //         transporterState: this.props.vendor.getTransporter.data.resource,
  //         emptyBox: false,
  //         viewTransporter: true
  //       })
  //     }
  //   }
  // }
  componentWillReceiveProps(nextProps) {
    // if (!nextProps.vendor.editTransporter.isLoading && !nextProps.vendor.editTransporter.isSuccess && !nextProps.vendor.editTransporter.isError) {
    //   this.setState({
    //     loader: false,

    //   })
    // }
    if (nextProps.vendor.getAllManageTransporter.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getAllManageTransporterClear()
      if (nextProps.vendor.getAllManageTransporter.data.resource == null) {
        this.setState({
          emptyBox: true
        })
      } else {
        this.setState({
          transporterState: nextProps.vendor.getAllManageTransporter.data.resource,
          viewTransporter: true
        })
      }
    } else if (nextProps.vendor.getAllManageTransporter.isError) {
      this.setState({
        alert: true,
        success: false,
        errorCode: nextProps.vendor.getAllManageTransporter.message.error == undefined ? undefined : nextProps.vendor.getAllManageTransporter.message.error.errorCode,
        errorMessage: nextProps.vendor.getAllManageTransporter.message.error == undefined ? undefined : nextProps.vendor.getAllManageTransporter.message.error.errorMessage,
        loader: false,

        code: nextProps.vendor.getAllManageTransporter.message.status,
      })
      this.props.getAllManageTransporterClear()
    } else if (nextProps.vendor.getAllManageTransporter.isLoading) {
      this.setState({
        loader: true
      })
    }

    // if (nextProps.vendor.deleteTransporter.isLoading) {
    //   this.setState({
    //     loader: true
    //   })
    // }
    // else if (nextProps.vendor.deleteTransporter.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.deleteTransporter.data.message,
    //     alert: false,
    //   })
    //   this.props.deleteTransporterRequest();
    // }
    // else if (nextProps.vendor.deleteTransporter.isError) {
    //   this.setState({
    //     alert: true,
    //     errorMessage: nextProps.vendor.deleteTransporter.error.errorMessage,
    //     success: false,
    //     loader: false
    //   })
    //   this.props.deleteTransporterRequest();
    // }

    // //edit  organization modal
    // if (nextProps.vendor.editTransporter.isLoading) {
    //   this.setState({
    //     loader: true
    //   })

    // }
    // else if (nextProps.vendor.editTransporter.isSuccess) {
    //   this.setState({
    //     success: true,
    //     loader: false,
    //     successMessage: nextProps.vendor.editTransporter.data.message,
    //     alert: false,
    //   })
    //   this.props.editVendorRequest();

    // }
    // else if (nextProps.vendor.editTransporter.isError) {
    //   this.setState({
    //     alert: true,
    //     success: false,
    //     errorCode: nextProps.vendor.editTransporter.message.error == undefined ? undefined : nextProps.vendor.editTransporter.message.error.errorCode,
    //     errorMessage: nextProps.vendor.editTransporter.message.error == undefined ? undefined : nextProps.vendor.editTransporter.message.error.errorMessage,
    //     loader: false,

    //     code: nextProps.vendor.editTransporter.message.status,
    //   })
    //   this.props.editVendorRequest();
    // }

    // dynamic header
    if (nextProps.replenishment.getHeaderConfig.isSuccess) {
      this.setState({
        loader: false
      })
      this.props.getHeaderConfigClear()
    } else if (nextProps.replenishment.getHeaderConfig.isError) {
      this.setState({
        loader: false,
        alert: true,
        code: nextProps.replenishment.getHeaderConfig.message.status,
        errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
        errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
      })
      this.props.getHeaderConfigClear()
    }

    // if (nextProps.vendor.accessTransporterPortal.isSuccess) {
    //   this.setState({
    //     loader: false
    //   })
    //   // this.props.accessTransporterPortalClear()
    // } else if (nextProps.vendor.accessTransporterPortal.isError) {
    //   this.setState({
    //     loader: false,
    //     alert: true,
    //     code: nextProps.vendor.accessTransporterPortal.message.status,
    //     errorCode: nextProps.vendor.accessTransporterPortal.message.error == undefined ? "NA" : nextProps.vendor.accessTransporterPortal.message.error.code,
    //     errorMessage: nextProps.vendor.accessTransporterPortal.message.error == undefined ? "NA" : nextProps.vendor.accessTransporterPortal.message.error.message
    //   })
    //   //this.props.accessTransporterPortalClear()
    // }

    if (//nextProps.vendor.createTransporter.isLoading || 
      nextProps.replenishment.getHeaderConfig.isLoading
        //|| nextProps.vendor.accessTransporterPortal.isLoading
        ) {
      this.setState({
        loader: true
      })
    }
  }
  onAddTransporter() {
    // this.setState({
    //   addVendor: true
    // });
    this.props.history.push('/mdm/manageTransporters/addTransporter')
  }

  onSaveTransporter() {
    this.setState({
      // addVendor: false,
      transporterState: TransporterData
    });
    // this.props.history.push('/vendor/manageVendors/viewVendor')
  }

  onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  }

  render() {
    const hash = window.location.hash.split("/")[3];
    return (
      <div className="container-fluid pad-0">
        {/*{ this.state.emptyBox  ?
             <EmptyBox
             {...this.props}
             vendorState = {this.state.vendorState}
              clickRightSideBar={() => this.props.rightSideBar()}
              addVendor={() => this.onAddVendor()}
            />
           : null}*/}

        {/*{ this.state.viewVendor ? */}
        <ViewTransporter
          TransporterData={this.state.transporterState}
          {...this.props}
          clickRightSideBar={() => this.props.rightSideBar()}
        />
        {/*: null}*/}

        {this.state.loader ? <FilterLoader /> : null}
        {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
      </div>
    );
  }
}

export default ManageTransporter;
