import React from "react";
import openRack from "../../../assets/open-rack.svg";
import errorIcon from "../../../assets/error_icon.svg";
import BraedCrumps from "../../breadCrumps";
import SideBar from "../../sidebar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../../redux/actions";

// import errorIcon from "../../../assets/error_icon.svg";
import FilterLoader from "../../loaders/filterLoader";
import RequestSuccess from "../../loaders/requestSuccess";
import RequestError from "../../loaders/requestError";
import Json from '../../administration/jsonFile.json';
import NewSideBar from "../../newSidebar";

const allState = Object.keys(Json[0].states)
const allLocations = Json[0].states
class AddVendor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            slName: "",
            slNameerr: false,
            firstNameerr: false,
            firstName: "",
            // lastName: "",
            alternateemail: "",
            shippedStateName: "",
            shippedStateNameerr: false,
            city: "",
            cityerr: false,
            state: "",
            stateerr: false,
            purchaseTermCode: "",
            contactNumber: "",
            contactNumbererr: false,
            email: "",
            emailerr: false,
            purchaseTermName: "",
            billingStateCode: "",
            tradeGrpCode: "",
            address: "",
            addresserr: false,
            billingGSTNumber: "",
            billingGSTNumbererr: false,
            requestSuccess: false,
            requestError: false,
            loader: false,
            success: false,
            alert: false,
            rightbar: false,
            openRightBar: false,
            shippingAddresserr: false,
            shippedGSTNoerr: false,
            shippingAddress: "",
            shippedStateCode: "",
            shippedGSTNo: "",
            deliveryBuffDays: "",
            dueDateBasis: "",
            dueDays: "",
            agentRate: "",
            agentName: "",
            agentCode: "",
            suspendedTo: "",
            suspendedFrom: "",
            isSuspended: "",
            isActive: "",
            successMessage: "",
            cities: [],
            shippedCityNameerr: false,
            shippedCityName: "",
            shippedCities: [],
            id: "",
            qcRequire: "FALSE",
            invoiceRequire: "FALSE",
            dueDaysErr: false,
        };
        if (window.performance) {
            if (performance.navigation.type == 1) {
                this.props.history.push("/mdm/manageTransaporters")
            }
        }
    }
    componentDidMount() {
        if (Object.keys(this.props.vendor.updateCreatedVendor.data).length != 0) {
            let data = this.props.vendor.updateCreatedVendor.data
            this.setState({
                id: data.id,
                slName: data.slName,
                firstName: data.firstName,
                // lastName: data.lastName,
                email: data.email,
                alternateEmail: data.alternateEmail,
                contactNumber: data.contactNumber,
                city: data.slCityName !== null && data.slCityName !== undefined ? this.camelCase(data.slCityName) : data.slCityName,
                state: data.slState !== null && data.slState !== undefined ? this.camelCase(data.slState == "UP" ? "Uttar Pradesh" : data.slState) : data.slState,
                address: data.slAddress,
                shippingAddress: data.shippedAddress,
                shippedGSTNo: data.shippedGSTNumber == null || data.shippedGSTNumber == undefined ? "" : data.shippedGSTNumber,
                shippedStateCode: data.shippedStateCode,
                shippedStateName: data.shippedStateName !== null && data.shippedStateName !== undefined ? this.camelCase(data.shippedStateName == "UP" ? "Uttar Pradesh" : data.shippedStateName) : data.shippedStateName,
                shippedCityName: data.shippedCityName !== null && data.shippedCityName !== undefined ? this.camelCase(data.shippedCityName) : data.shippedCityName,
                // purchaseTermCode: data.purchaseTermCode,
                // purchaseTermName: data.purchaseTermName,
                billingGSTNumber: data.billingGSTNumber == null || data.billingGSTNumber == undefined ? "" : data.billingGSTNumber,
                billingStateCode: data.billingStateCode,
                // tradeGrpCode: data.tradeGrpCode,
                dueDays: data.dueDays,
                dueDateBasis: data.dueDateBasis,
                deliveryBuffDays: data.deliveryBuffDays,
                // agentCode: data.agentCode,
                // agentName: data.agentName,
                // agentRate: data.agentRate,
                isActive: data.isExt,
                // isSuspended: data.isSuspended,
                // suspendedFrom: data.suspendedFrom,
                // suspendedTo: data.suspendedTo,
                qcRequire: data.isQC,
                invoiceRequire: data.isInvoice,
                cities: allLocations[data.slState],
                shippedCities: allLocations[data.shippedStateName]
            })
            document.getElementById('city').value = data.slCityName

        }
    }

    camelCase = (data)=>{
        data = data.replace(/[-_\s.]+(.)?/g, (_, char) => char ? char.toUpperCase() : '');
        return data.substr(0, 1).toLowerCase() + data.substr(1);
    }
    
    onClear(e) {
        // e.preventDefault();
        this.setState({
            slName: "",
            // lastName: "",
            firstName: "",
            alternateemail: "",
            city: "",
            state: "",
            purchaseTermCode: "",
            contactNumber: "",
            email: "",
            phone: "",
            contactEmail: "",
            address: "",
            billingGSTNumber: "",
            shippingAddress: "",
            shippedStateCode: "",
            shippedGSTNo: "",
            shippedStateName: "",
            billingStateCode: "",
            dueDays: "",
            dueDateBasis: "",
            deliveryBuffDays: "",
            isActive: "",
        })
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
        this.props.history.push('/mdm/manageTransaporters')
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }

    handleChange(e) {
        if (e.target.id == "slName") {
            this.setState(
                {
                    slName: e.target.value
                },
                () => {
                    this.slName();
                }
            );
        } else if (e.target.id == "firstName") {
            this.setState(
                {
                    firstName: e.target.value
                },
                () => {
                    this.firstName();
                }
            );

        // } else if (e.target.id == "lastName") {
        //     this.setState(
        //         {
        //             lastName: e.target.value
        //         }
        //     );
        } else if (e.target.id == "alternateemail") {
            this.setState(
                {
                    alternateemail: e.target.value
                }
            );
        } else if (e.target.id == "city") {
            this.setState(
                {
                    city: e.target.value
                },
                () => {
                    this.city();
                }
            );
        }
        // else if (e.target.id == "purchaseTermCode") {
        //     this.setState(
        //         {
        //             purchaseTermCode: e.target.value
        //         });
        // } 
        else if (e.target.id == "contactNumber") {
            this.setState(
                {
                    contactNumber: e.target.value
                },
                () => {
                    this.contactNumber();
                }
            );
        }
        // else if (e.target.id == "purchaseTermName") {
        //     this.setState(
        //         {
        //             purchaseTermName: e.target.value
        //         });
        // } 
        else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );
        } else if (e.target.id == "billingStateCode") {
            this.setState(
                {
                    billingStateCode: e.target.value
                });
        } else if (e.target.id == "tradeGrpCode") {
            this.setState(
                {
                    tradeGrpCode: e.target.value
                });
        } else if (e.target.id == "address") {
            this.setState(
                {
                    address: e.target.value
                },
                () => {
                    this.address();
                }
            );
        } else if (e.target.id == "billingGSTNumber") {
            this.setState(
                {
                    billingGSTNumber: e.target.value
                },
                () => {
                    this.billingGSTNumber();
                }
            );
        }
        else if (e.target.id == "shippedGSTNo") {
            this.setState(
                {
                    shippedGSTNo: e.target.value
                },
                () => {
                    this.shippedGSTNo();
                }
            );
        }
        else if (e.target.id == "shippingAddress") {
            this.setState(
                {
                    shippingAddress: e.target.value
                },
                () => {
                    this.shippingAddress();
                }
            );
        } else if (e.target.id == "dueDays") {
            this.setState(
                {
                    dueDays: e.target.value
                },
                () => {
                    this.dueDays();
                }
            );
        } else if (e.target.id == "dueDateBasis") {
            this.setState(
                {
                    dueDateBasis: e.target.value
                });
        } else if (e.target.id == "deliveryBuffDays") {
            this.setState(
                {
                    deliveryBuffDays: e.target.value
                });
        } else if (e.target.id == "agentCode") {
            this.setState(
                {
                    agentCode: e.target.value
                });
        } else if (e.target.id == "agentName") {
            this.setState(
                {
                    agentName: e.target.value
                });
        }
        // else if (e.target.id == "agentRate") {
        //     this.setState(
        //         {
        //             agentRate: e.target.value
        //         });
        // }
        else if (e.target.id == "isActive") {
            this.setState(
                {
                    isActive: e.target.value
                });

        } else if (e.target.id == "shippedCityName") {
            this.setState(
                {
                    shippedCityName: e.target.value
                }, () => {
                    this.shippedCityName()
                });
        } else if (e.target.id == "qcRequire") {
            this.setState({ qcRequire: e.target.value })
        } else if (e.target.id == "invoiceRequire") {
            this.setState({ invoiceRequire: e.target.value })
        }

        // else if (e.target.id == "isSuspended") {
        //     this.setState(
        //         {
        //             isSuspended: e.target.value
        //         });
        // } 
        // else if (e.target.id == "suspendedFrom") {
        //     this.setState(
        //         {
        //             suspendedFrom: e.target.value
        //         });
        // }
        //  else if (e.target.id == "suspendedTo") {
        //     this.setState(
        //         {
        //             suspendedTo: e.target.value
        //         });
        // }
    }
    shippedStateName() {
        if (
            this.state.shippedStateName == "" || this.state.shippedStateName == null) {
            this.setState({
                shippedStateNameerr: true
            });
        } else {
            this.setState({
                shippedStateNameerr: false
            });
        }
    }
    slName() {
        if (
            //this.state.slName == "" || this.state.slName == null || !this.state.slName.match(/^[a-zA-Z ]+$/)
            this.state.slName == "" || this.state.slName == null 
        ) {
            this.setState({
                slNameerr: true
            });
        } else {
            this.setState({
                slNameerr: false
            });
        }
    }
    //first Mame
    firstName() {
        // if (this.state.firstName == "" || this.state.firstName == null || !this.state.firstName.match(/^[A-Za-z\s]+$/)) {
        if (this.state.firstName == "" || this.state.firstName == null) {
            this.setState({
                firstNameerr: true
            });
        } else {
            this.setState({
                firstNameerr: false
            });
        }
    }
    //city

    city() {
        if (this.state.city == "" || this.state.city == null) {
            this.setState({
                cityerr: true
            });
        } else {
            this.setState({
                cityerr: false
            });
        }
    }
    //state

    states() {
        if (this.state.state == "" || this.state.state == null) {
            this.setState({
                stateerr: true
            });
        } else {
            this.setState({
                stateerr: false
            });
        }
    }
    //contactNumber
    contactNumber() {
        if (
            this.state.contactNumber == "" || this.state.contactNumber == null ||
            !this.state.contactNumber.match(/^\d{10}$/) ||
            !this.state.contactNumber.match(/^[1-9]\d+$/) ||
            !this.state.contactNumber.match(/^.{10}$/)
        ) {
            this.setState({
                contactNumbererr: true
            });
        } else {
            this.setState({
                contactNumbererr: false
            });
        }
    }
    //email
    email() {
        if (
            this.state.email == "" || this.state.email == null
            ||
            !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }
    //address
    address() {
        if (this.state.address == "" || this.state.address == null) {
            this.setState({
                addresserr: true
            });
        } else {
            this.setState({
                addresserr: false
            });
        }
    }

    //billingGSTNumber
    billingGSTNumber() {
        // let gstPattern = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        let gstPattern = /^[0-9]{2}[A-Z]{4}[1-9A-Z]{1}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/ 
        if ((this.state.billingGSTNumber.toString()).length < 15 || this.state.billingGSTNumber == "" || this.state.billingGSTNumber == null || !gstPattern.test(this.state.billingGSTNumber)) {
            this.setState({
                billingGSTNumbererr: true
            });
        } else {
            this.setState({
                billingGSTNumbererr: false
            });
        }
    }

    shippedGSTNo() {
        // let gstPattern = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        let gstPattern = /^[0-9]{2}[A-Z]{4}[1-9A-Z]{1}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        if ((this.state.shippedGSTNo.toString()).length < 15 || this.state.shippedGSTNo == "" || this.state.shippedGSTNo == null || !gstPattern.test(this.state.shippedGSTNo)) {
            this.setState({
                shippedGSTNoerr: true
            });
        } else {
            this.setState({
                shippedGSTNoerr: false
            });
        }
    }

    shippedCityName() {
        if (this.state.shippedCityName == "" || this.state.shippedCityName == null) {
            this.setState({
                shippedCityNameerr: true
            });
        } else {
            this.setState({
                shippedCityNameerr: false
            });
        }
    }
    //address
    shippingAddress() {
        if (this.state.shippingAddress == "") {
            this.setState({
                shippingAddresserr: true
            });
        } else {
            this.setState({
                shippingAddresserr: false
            });
        }
    }
    dueDays() {
        let numPattern = /^[0-9]+$/
        if( this.state.dueDays == "" || !numPattern.test(this.state.dueDays))
           this.setState({ dueDaysErr: true})
        else
           this.setState({ dueDaysErr: false})   
    }
    componentWillReceiveProps(nextProps) {
        if (!nextProps.vendor.addVendor.isSuccess && !nextProps.vendor.addVendor.isError && !nextProps.vendor.addVendor.isLoading) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.vendor.addVendor.isSuccess) {
            this.setState({
                loader: false,
                success: true
            })
            this.props.addVendorClear();
        } else if (nextProps.vendor.addVendor.isError) {
            this.setState({
                loader: false,
                alert: true
            })
            this.props.addVendorClear();
        } else if (nextProps.vendor.addVendor.isLoading || nextProps.vendor.createVendor.isLoading) {
            this.setState({
                loader: true
            })
        }
        console.log(nextProps.vendor.createVendor)
        if (nextProps.vendor.createVendor.isSuccess) {
            this.setState({
                success: true,
                loader: false,
                successMessage: nextProps.vendor.createVendor.data.message,
                alert: false,
            })
            this.onClear();
            this.props.createVendorClear();
        }
        else if (nextProps.vendor.createVendor.isError) {
            this.setState({
                alert: true,
                success: false,
                errorCode: nextProps.vendor.createVendor.message.error == undefined ? undefined : nextProps.vendor.createVendor.message.error.errorCode,
                errorMessage: nextProps.vendor.createVendor.message.error == undefined ? undefined : nextProps.vendor.createVendor.message.error.errorMessage,
                loader: false,
                code: nextProps.vendor.createVendor.message.status,
            })
            this.props.createVendorClear();
        }

        if (nextProps.vendor.updateManageVendor.isSuccess) {
            this.setState({
                success: true,
                loader: false,
                successMessage: nextProps.vendor.updateManageVendor.data.message,
                alert: false,
            })
            this.onClear();
            this.props.updateManageVendorClear();
        } else if (nextProps.vendor.updateManageVendor.isError) {
            this.setState({
                alert: true,
                success: false,
                errorCode: nextProps.vendor.updateManageVendor.message.error == undefined ? undefined : nextProps.vendor.updateManageVendor.message.error.errorCode,
                errorMessage: nextProps.vendor.updateManageVendor.message.error == undefined ? undefined : nextProps.vendor.updateManageVendor.message.error.errorMessage,
                loader: false,
                code: nextProps.vendor.updateManageVendor.message.status,
            })
            this.props.updateManageVendorClear();
        }

        if (nextProps.vendor.updateManageVendor.isLoading) {
            this.setState({ loader: true })
        }
    }

    contactSubmit(e) {
        e.preventDefault();
        this.slName();
        this.firstName();
        this.email();
        this.city();
        this.states();
        this.contactNumber();
        this.address();
        this.billingGSTNumber();
        this.shippedGSTNo();
        this.shippingAddress();
        this.shippedStateName();
        this.shippedCityName();
        this.dueDays();
        const t = this;
        setTimeout(function () {
            const { slNameerr, shippedCityNameerr, shippedStateNameerr, addresserr, contactNumbererr, billingGSTNumbererr, cityerr, emailerr, stateerr, shippingAddresserr, shippedGSTNoerr, dueDaysErr, firstNameerr } = t.state;
            if ( !slNameerr && !shippedCityNameerr && !shippedStateNameerr && !addresserr && !contactNumbererr && !billingGSTNumbererr && !emailerr && !cityerr && !stateerr && !shippingAddresserr && !shippedGSTNoerr && !dueDaysErr 
                && !firstNameerr ) {
                t.setState({
                    // loader: true,
                    alert: false,
                    success: false
                })
                let vendorData = {
                    slName: t.state.slName,
                    slId: "",
                    slCode: "",
                    firstName: t.state.firstName,
                    // lastName: t.state.lastName,
                    email: t.state.email,
                    alternateEmail: t.state.alternateemail,
                    contactNumber: t.state.contactNumber,
                    slState: t.state.state,
                    slCityName: t.state.city,
                    slAddress: t.state.address,
                    shippedAddress: t.state.shippingAddress,
                    shippedGSTNumber: t.state.shippedGSTNo,
                    shippedStateCode: t.state.shippedStateCode,
                    shippedStateName: t.state.shippedStateName,
                    billingGSTNumber: t.state.billingGSTNumber,
                    billingStateCode: t.state.billingStateCode,
                    deliveryBuffDays: t.state.deliveryBuffDays || 0,
                    dueDayBasis: t.state.dueDateBasis,
                    dueDays: t.state.dueDays || 0,
                    isExt: t.state.isActive == "FALSE" ? "Y" : "N",
                    shippedCityName: t.state.shippedCityName,
                    isQC: t.state.qcRequire,
                    isInvoice: t.state.invoiceRequire
                }
                console.log(vendorData)
                let id = { "id": t.state.id }
                let payload = { ...vendorData, ...id }
                if (Object.keys(t.props.vendor.updateCreatedVendor.data).length == 0) {
                    t.props.createVendorRequest(vendorData);
                } else {
                    t.props.updateManageVendorRequest(payload)
                }
            }
        }, 100)
    }
    findCities = (event) => {
        document.getElementById('city').value = ""
        this.setState({ cities: allLocations[event.target.value], state: event.target.value }, () => {
            this.states()
        })
    }
    findShippedCity = (event) => {
        document.getElementById('shippedCityName').value = ""
        this.setState({ shippedCities: allLocations[event.target.value], shippedStateName: event.target.value }, () => {
            this.shippedStateName()
        })
    }
    getGSTStateCode = (e, type) => {
        if (type == "shippedGSTNo") {
            this.setState({ shippedStateCode: e.target.value.slice(0, 2) })
        } else if (type == "billingGSTNumber") {
            this.setState({ billingStateCode: e.target.value.slice(0, 2) })
        }
    }
    render() {
        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        const {
            slName,
            slNameerr,
            firstName, firstNameerr,
            //lastName, 
            alternateemail,
            shippedStateName, shippedStateNameerr,
            city,
            cityerr,
            state,
            stateerr,
            purchaseTermCode,
            contactNumber,
            contactNumbererr,
            email,
            emailerr,
            purchaseTermName,
            billingStateCode,
            tradeGrpCode,
            address,
            addresserr,
            billingGSTNumber,
            billingGSTNumbererr,
            shippingAddress,
            shippingAddresserr,
            shippedStateCode,
            shippedGSTNo, shippedGSTNoerr, dueDays,
            dueDateBasis, deliveryBuffDays,
            agentCode, agentName, agentRate,
            isActive, isSuspended, suspendedFrom,
            suspendedTo, shippedCityName, shippedCityNameerr, dueDaysErr
        } = this.state;
        return (
            <div className="container-fluid nc-design pad-l60">
                <NewSideBar {...this.props} />
                <SideBar {...this.props} />
                <img onClick={() => this.onRightSideBar()} src={openRack} className="right_sidemenu rightstick" />
                {this.state.openRightBar ? <RightSideBar rsbar={this.state.rightbar} rightSideBar={() => this.onRightSideBar()} /> : null}

                <div className="container_div m-top-75 " id="">
                    <div className="col-md-12 col-sm-12 border-btm">
                        <div className="menu_path n-menu-path">
                            <ul className="list-inline width_100 pad20 nmp-inner">
                                <BraedCrumps {...this.props} />
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="container-fluid pad-0">
                    <div className="container_div" id="">
                        <div className="container-fluid pad-0">
                            <form name="vendorForm" className="vendorForm" onSubmit={(e) => this.contactSubmit((e))} >
                                <div className="col-lg-12 pad-0 ">
                                    <div className="gen-vendor-potal-design p-lr-47">
                                        <div className="col-lg-6 pad-0">
                                            <div className="gvpd-left">
                                                <button className="gvpd-back" onClick={() => this.props.history.push("/mdm/manageTransaporters")} type="reset">
                                                    <span className="back-arrow">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                                        </svg>
                                                    </span>
                                                    Back
                                                </button>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 pad-0">
                                            <div className="gvpd-right">
                                                {Object.keys(this.props.vendor.updateCreatedVendor.data).length == 0 && <button onClick={(e) => this.onClear(e)} className="gen-clear" type="reset">Clear</button>}
                                                <button className="gen-save">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="add-vendor-page-layout">
                                        <div className="StickyDiv manage-sticky-div">
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                                <div className="ao-billing-details">
                                                    <h4>BASIC DETAILS</h4>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-15">
                                                <div className="container_box list_margin">
                                                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>Transporter Name <span className="mandatory">*</span></label>
                                                            <input
                                                                name="slName"
                                                                id="slName"
                                                                type="text"
                                                                className={slNameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                                onChange={e => this.handleChange(e)}
                                                                value={slName}
                                                            />
                                                            {/* {slNameerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                            {slNameerr ? (
                                                                <span className="error">
                                                                    Enter valid Vendor name
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="col-md-2 col-sm-4 pad-lft-0">
                                                            <label>Contact Person <span className="mandatory">*</span></label>
                                                            <input
                                                                ref="firstName"
                                                                name="firstName"
                                                                id="firstName"
                                                                type="text"
                                                                className={firstNameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                                onChange={e => this.handleChange(e)}
                                                                value={firstName}
                                                            />
                                                           
                                                            {firstNameerr ? (
                                                                <span className="error">
                                                                    Enter Valid Contact Person Name
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                        {/* <div className="col-md-2 col-sm-4 pad-lft-0">
                                                            <label>Last Name</label>
                                                            <input
                                                                ref="lastName"
                                                                name="lastName"
                                                                id="lastName"
                                                                type="text"
                                                                className={"vendor_input_box"}
                                                                onChange={e => this.handleChange(e)}
                                                                value={lastName}
                                                            />
                                                        </div> */}
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>Email <span className="mandatory">*</span></label>
                                                            <input
                                                                type="text"
                                                                className={emailerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                                ref="email"
                                                                name="email"
                                                                id="email"
                                                                onChange={e => this.handleChange(e)}
                                                                value={email}
                                                            />
                                                            {/* {emailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                            {emailerr ? (
                                                                <span className="error" >
                                                                    Enter valid email
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>Alternate Email</label>
                                                            <input
                                                                type="text"
                                                                className={"vendor_input_box"}
                                                                ref="alternateemail"
                                                                name="alternateemail"
                                                                id="alternateemail"
                                                                onChange={e => this.handleChange(e)}
                                                                value={alternateemail}
                                                            />
                                                        </div>
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>Contact Number <span className="mandatory">*</span></label>
                                                            <input
                                                                type="text"
                                                                maxLength="10"
                                                                className={contactNumbererr ? "vendor_input_box errorBorder numbersOnly " : "vendor_input_box numbersOnly"}
                                                                ref="contactNumber"
                                                                name="contactNumber"
                                                                id="contactNumber"
                                                                onChange={e => this.handleChange(e)}
                                                                value={contactNumber}
                                                            />
                                                            {contactNumbererr ? (
                                                                <span className="error" >
                                                                    Enter valid Contact Number
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 pad-0 m-top-30">
                                                <div className="ao-billing-details">
                                                    <h4>BILLING DETAILS</h4>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-15">
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>GST Number <span className="mandatory">*</span></label>
                                                    <input
                                                        type="text"
                                                        className={billingGSTNumbererr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                        ref="billingGSTNumber"
                                                        name="billingGSTNumber"
                                                        id="billingGSTNumber"
                                                        onChange={e => this.handleChange(e)} onBlur={(e) => this.getGSTStateCode(e, 'billingGSTNumber')}
                                                        value={billingGSTNumber.toUpperCase()} maxLength="15"
                                                    />
                                                    {billingGSTNumbererr ? (
                                                        <span className="error" >
                                                            Please enter the correct 15 Digit GST number
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>Select State <span className="mandatory">*</span></label>
                                                    <select
                                                        ref="state"
                                                        name="state"
                                                        id="state"
                                                        className={stateerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                        onChange={e => this.findCities(e)}
                                                        value={state}
                                                    >
                                                        <option value="">Select State</option>
                                                        {allState.map((_, key) => (
                                                            <option value={_} key={key}>{_}</option>
                                                        ))}
                                                    </select>
                                                    {stateerr ? (
                                                        <span className="error">
                                                            Select state
                                                        </span>
                                                    ) : null}

                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>City Name <span className="mandatory">*</span></label>
                                                    <select
                                                        ref="city"
                                                        name="city"
                                                        id="city"
                                                        className={cityerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                        onChange={e => this.handleChange(e)}
                                                        value={city}
                                                    >
                                                        <option value="">Select City</option>
                                                        {state !== "" && (this.state.cities !== undefined && this.state.cities.length > 0) && this.state.cities.map((_, k) => (
                                                            <option value={_} key={k}>{_}</option>
                                                        ))}
                                                    </select>
                                                    {/* {cityerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                    {cityerr ? (
                                                        <span className="error">
                                                            Select city
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <div className="col-md-2 pad-lft-0">
                                                    <label>GST State Code</label>
                                                    <input
                                                        type="text"
                                                        className="vendor_input_box"
                                                        ref="billingStateCode"
                                                        name="billingStateCode"
                                                        maxLength="10"
                                                        id="billingStateCode"
                                                        onChange={e => this.handleChange(e)}
                                                        value={billingStateCode} disabled
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-15">
                                                <div className="col-md-4 pad-lft-0">
                                                    <label>Address <span className="mandatory">*</span></label>
                                                    <textarea
                                                        className={addresserr ? "text_area_vendor errorBorder" : "text_area_vendor"}
                                                        ref="address"
                                                        name="address"
                                                        id="address"
                                                        onChange={e => this.handleChange(e)}
                                                        value={address}
                                                    />
                                                    {addresserr ? (
                                                        <span className="error" >
                                                            Enter valid address
                                                        </span>
                                                    ) : null}
                                                </div>
                                            </div>

                                            <div className="col-md-12 col-sm-12 pad-0 m-top-30">
                                                <div className="ao-billing-details">
                                                    <h4>SHIPPING DETAILS</h4>
                                                </div>
                                                <ul className="list_style m-lft-0">
                                                    <li>
                                                        <p className="master_para">
                                                            Add your shipping details here
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-15">
                                                <div className="container_box list_margin">
                                                    <div className="col-md-12 col-sm-12 pad-0">
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>GST NO <span className="mandatory">*</span></label>
                                                            <input
                                                                type="text"
                                                                className={shippedGSTNoerr ? "vendor_input_box  errorBorder" : "vendor_input_box "}
                                                                name="shippedGSTNo"
                                                                maxLength="10"
                                                                id="shippedGSTNo"
                                                                onChange={e => this.handleChange(e)} onBlur={(e) => this.getGSTStateCode(e, 'shippedGSTNo')}
                                                                value={shippedGSTNo.toUpperCase()} maxLength="15"
                                                            />
                                                            {/* {shippedGSTNoerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                                            {shippedGSTNoerr ? (
                                                                <span className="error" >
                                                                    Please enter the correct 15 Digit GST number
                                                                </span>
                                                            ) : null}
                                                        </div>

                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>State Name <span className="mandatory">*</span></label>
                                                            <select
                                                                name="shippedStateName"
                                                                id="shippedStateName"
                                                                className={shippedStateNameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                                onChange={e => this.findShippedCity(e)}
                                                                value={shippedStateName}
                                                            >
                                                                <option value="">Select State</option>
                                                                {allState.map((_, key) => (
                                                                    <option value={_} key={key}>{_}</option>
                                                                ))}
                                                            </select>
                                                            {shippedStateNameerr ? (
                                                                <span className="error">
                                                                    Select State Name                          </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>City Name <span className="mandatory">*</span></label>
                                                            <select
                                                                ref="shippedCityName"
                                                                name="shippedCityName"
                                                                id="shippedCityName"
                                                                className={shippedCityNameerr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                                onChange={e => this.handleChange(e)}
                                                                value={shippedCityName}
                                                            >
                                                                <option value="">Select City</option>
                                                                {this.state.shippedCities != undefined && this.state.shippedCities.map((_, k) => (
                                                                    <option value={_} key={k}>{_}</option>
                                                                ))}
                                                            </select>
                                                            {shippedCityNameerr ? (
                                                                <span className="error">
                                                                    Select City Name                          </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>GST State Code</label>
                                                            <input
                                                                type="text"
                                                                className="vendor_input_box"
                                                                ref="contactemail"
                                                                name="shippedStateCode"
                                                                id="shippedStateCode"
                                                                onChange={e => this.handleChange(e)}
                                                                value={shippedStateCode} disabled
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                                        <div className="col-md-4 pad-lft-0">
                                                            <label>Address <span className="mandatory">*</span></label>
                                                            <textarea
                                                                className={shippingAddresserr ? "text_area_vendor errorBorder" : "text_area_vendor"}
                                                                ref="address"
                                                                name="shippingAddress"
                                                                id="shippingAddress"
                                                                onChange={e => this.handleChange(e)}
                                                                value={shippingAddress}
                                                            />
                                                            {/* {shippingAddresserr ? <img src={errorIcon} className="error_icon-txtarea" /> : null} */}
                                                            {shippingAddresserr ? (
                                                                <span className="error" >
                                                                    Enter valid address
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                    </div>
                                                    
                                                    <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>Status</label>
                                                            <select
                                                                name="isActive"
                                                                id="isActive"
                                                                className="vendor_input_box"
                                                                onChange={e => this.handleChange(e)}
                                                                value={isActive}>
                                                                <option value="">Active</option>
                                                                <option value="InActive">In Active</option>
                                                                {/* <option value="FALSE">FALSE</option> */}
                                                            </select>
                                                        </div>

                                                        <div className="col-md-2 pad-lft-0">
                                                            <label>Due Days<span className="mandatory">*</span></label>
                                                            <input
                                                                type="text"
                                                                className={dueDaysErr ? "vendor_input_box errorBorder" : "vendor_input_box"}
                                                                ref="dueDays"
                                                                name="dueDays"
                                                                id="dueDays"
                                                                onChange={e => this.handleChange(e)}
                                                                value={dueDays}
                                                            />
                                                            {dueDaysErr ? (
                                                                <span className="error" >
                                                                    Enter numbers only
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                        <div className="col-md-3 pad-0">
                                                            <div className="col-md-6 pad-lft-0">
                                                                <label>Due Date Basis</label>
                                                                <input
                                                                    type="text"
                                                                    className={"vendor_input_box"}
                                                                    ref="dueDateBasis"
                                                                    name="dueDateBasis"
                                                                    id="dueDateBasis"
                                                                    onChange={e => this.handleChange(e)}
                                                                    value={dueDateBasis}
                                                                />
                                                            </div>

                                                            <div className="col-md-6 pad-lft-0">
                                                                <label>Delivery Buff Days</label>
                                                                <input
                                                                    type="text"
                                                                    className={"vendor_input_box numbersOnly"}
                                                                    ref="deliveryBuffDays"
                                                                    name="deliveryBuffDays"
                                                                    id="deliveryBuffDays"
                                                                    onChange={e => this.handleChange(e)}
                                                                    value={deliveryBuffDays}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} requestSuccess={this.state.requestSuccess} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
        );
    }
}

export function mapStateToProps(state) {
    return {
        vendor: state.vendor
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddVendor);

