import React from 'react';

const ToastError = (props) => {
    return (
        <div className="toast-error-container">
            <img className="toast-close displayPointer" onClick={props.closeToastError} src={require('../../assets/toast-close.svg')} />
            <div className="toast-head">
                <h4>Alert</h4><img className="toast-warning-icon" src={require('../../assets/warning.svg')} />
            </div>
            {props.toastErrorMsg !== undefined ? <p>{props.toastErrorMsg}</p> : 
               <p>Loading Error ! Please try again.</p>}
        </div>
    )
}
export default ToastError