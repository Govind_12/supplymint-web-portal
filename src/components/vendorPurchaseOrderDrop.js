import React from 'react';
import { Link } from 'react-router-dom';

class VendorPurchaseOrderDrop extends React.Component {

  render() {
    var Priority_List = JSON.parse(sessionStorage.getItem('Priority List')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Priority List')).crud);
    var Manage_Vendors = JSON.parse(sessionStorage.getItem('Manage Vendors')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Manage Vendors')).crud);

    return (
      <label>
        <div className="dropdown">
          <button className="dropbtn home_link">Purchase Order
        <i className="fa fa-chevron-down"></i>
          </button>
          <div className="dropdown-content adminBreacrumbsDropdown">
            {Manage_Vendors > 0 ? <Link to="/vendor/manageVendors/viewVendor"> Manage Vendors</Link> : null}
          </div>
        </div>
      </label>
    );
  }
}

export default VendorPurchaseOrderDrop;