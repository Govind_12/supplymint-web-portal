import React from 'react';
import { Link } from 'react-router-dom';
import { openMyDrop, closeMyDrop } from '../helper/index'

class PurchaseDrop extends React.Component {

  render() {
    var PI_History = JSON.parse(sessionStorage.getItem('PI History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PI History')).crud);
    var PO_History = JSON.parse(sessionStorage.getItem('PO History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('PO History')).crud);
    var Purchase_Indent = JSON.parse(sessionStorage.getItem('Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Indent')).crud);
    var Purchase_Order = JSON.parse(sessionStorage.getItem('Purchase Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order')).crud);
    var Item_UDF_Mapping = JSON.parse(sessionStorage.getItem('Item UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Mapping')).crud)
    var Item_UDF_Setting = JSON.parse(sessionStorage.getItem('Item UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Item UDF Setting')).crud)
    var UDF_Mapping = JSON.parse(sessionStorage.getItem('UDF Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Mapping')).crud)
    var UDF_Setting = JSON.parse(sessionStorage.getItem('UDF Setting')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('UDF Setting')).crud)
    var Dept_Size_Mapping = JSON.parse(sessionStorage.getItem('Dept Size Mapping')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Dept Size Mapping')).crud)
    var Configuration = JSON.parse(sessionStorage.getItem('Configuration')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Configuration')).crud)
    var Purchase_Order_With_Upload = JSON.parse(sessionStorage.getItem('Purchase Order File Upload')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order File Upload')).crud);
    var File_Upload_History = JSON.parse(sessionStorage.getItem('File Upload History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('File Upload History')).crud)
    var Purchase_Orders = JSON.parse(sessionStorage.getItem('Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Orders')).crud)
    var Purchase_Orders_History = JSON.parse(sessionStorage.getItem('Purchase Order History')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Purchase Order History')).crud)
    var Generics_Purchase_Orders = JSON.parse(sessionStorage.getItem('Generics Purchase Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Generics Purchase Orders')).crud)
    var Generics_Purchase_Indent = JSON.parse(sessionStorage.getItem('Generics Purchase Indent')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Generics Purchase Indent')).crud)



    return (
      <label>
        <div className="dropdown" onMouseOver={(e) => openMyDrop(e)} id="myFav">
          <button className="dropbtn home_link">Procurement
            <i className="fa fa-chevron-down"></i>
          </button>

          {/* <div className="displayNone adminBreacrumbsDropdown anchorErrorSubMenu" id="dropMenu">
            {Configuration > 0 ? <span className="scheduler">Configuration<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown subDropPos">
                {Dept_Size_Mapping > 0 ? <Link to="/purchase/departmentSizeMapping" onClick={(e) => closeMyDrop(e)}>Department Size Mapping</Link> : null}
                {UDF_Setting > 0 ? <Link to="/purchase/udfSetting" onClick={(e) => closeMyDrop(e)}>UDF Setting</Link> : null}
                {UDF_Mapping > 0 ? <Link to="/purchase/udfMapping" onClick={(e) => closeMyDrop(e)}>UDF Mapping</Link> : null}
                {Item_UDF_Setting > 0 ? <Link to="/purchase/itemUdfSetting" onClick={(e) => closeMyDrop(e)}>Item UDF Setting</Link> : null}
                {Item_UDF_Mapping > 0 ? <Link to="/purchase/itemUdfMapping" onClick={(e) => closeMyDrop(e)}>Item UDF Mapping</Link> : null}
              </div>
            </span> : null}
            <span className="scheduler">Purchase Indent<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown subDropPos">
                {Purchase_Indent > 0 && sessionStorage.getItem('partnerEnterpriseName') !="VMART" ? <Link to="/purchase/purchaseIndent" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Create Indent v1</Link> : null}
                {<Link to="/purchase/purchaseIndents" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Create Indent v2</Link>}
                {PI_History > 0 ? <Link to="/purchase/purchaseIndentHistory" id="menuFavItem" onClick={(e) => closeMyDrop(e)}> View Indents</Link> : null}
              </div>
            </span>

            {Purchase_Orders > 0 ? <span className="scheduler">Purchase Order<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown subDropPos">
                {Purchase_Order > 0 && sessionStorage.getItem('partnerEnterpriseName') =="VMART" ? <Link to="/purchase/purchaseOrder" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Create Order v1</Link> : null}
                {sessionStorage.getItem('partnerEnterpriseName') !="VMART" && <Link to="/purchase/purchaseOrders" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Create Order v2</Link>}
                {PO_History > 0 ? <Link to="/purchase/purchaseOrderHistory" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>View Orders</Link> : null}
                {Purchase_Order_With_Upload ? <Link to="/purchase/purchaseOrderWithUpload" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>PO With Upload</Link> : null}
              </div>
            </span> : null}
            {PI_History > 0 ? <Link to="/purchase/purchaseIndentHistory" id="menuFavItem" onClick={(e) => closeMyDrop(e)}> Purchase Indent History</Link> : null}
            {Purchase_Orders_History>0?  <span className="scheduler">Purchase Order History<span className="breadCrumbArrow float_Right"><i className="fa fa-chevron-right"></i></span>
              <div className="hovershowDropdown subDropPos m-neg-35">
                {PO_History > 0 ? <Link to="/purchase/purchaseOrderHistory" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Purchase Order History</Link> : null}
                {File_Upload_History > 0 ? <Link to="/purchase/uploadHistory" id="menuFavItem" onClick={(e) => closeMyDrop(e)}>Upload History</Link> : null}
              </div></span>:null}
          </div> */}
        </div>
      </label>
    );
  }
}

export default PurchaseDrop;