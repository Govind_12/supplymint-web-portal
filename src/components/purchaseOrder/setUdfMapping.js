import React from 'react';
import { poUdfMappingRequest } from '../../redux/purchaseIndent/action';
import Pagination from '../pagination';
import ConfigArticle from './configArticleModal';

class SetUdfMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exportToExcel: false,            
            isAdded: false,
            setUdfData: [],
            udfName: '',
            udfType: '',
            insertUdf: '',
            tableData: [],
            tableSearch: '',
            changedTableData: '',
        }
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, ()=> document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () =>{
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }
    
    componentDidMount(){
        let data = {
            displayName: "",
            isCompulsary: "",
            ispo: false,
            search: "",
            searchBy: "contains",
            type: 1,
            udfType: "",   
        }
        this.props.getUdfMappingRequest(data)
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.purchaseIndent.getUdfMapping.isSuccess){
            if(nextProps.purchaseIndent.getUdfMapping.data.resource != null){
                this.setState({
                    setUdfData: nextProps.purchaseIndent.getUdfMapping.data.resource
                })
            } else{
                this.setState({
                    setUdfData: []
                })
            }
            this.props.getUdfMappingClear()
        }
        if(nextProps.purchaseIndent.poUdfMapping.isSuccess){
            if(nextProps.purchaseIndent.poUdfMapping.data.resource != null){
                this.setState({
                    tableData: nextProps.purchaseIndent.poUdfMapping.data.resource
                })
            } else{
                this.setState({
                    tableData: [],
                    changedTableData: [],
                })
            }
            this.props.saveCheckMap('')
            this.props.poUdfMappingClear();
        }
        if(nextProps.purchaseIndent.updateUdfMapping.isSuccess){
            this.setState({
                insertUdf:'',
                changedTableData: [],
            })
            this.props.saveCheckMap('');
            this.props.updateUdfMappingClear();
        }
    }

    setUdfType = (e) =>{
        this.setState({
            udfType: e.target.value.split('|')[0],
            udfName: e.target.value.split('|')[1],
            insertUdf: '',    
            isAdded : false,  
        }, () => this.getSetUdfData() )
    }

    openAdded = () =>{
        this.setState({
            isAdded: true,
        })
    }

    closeAdded = () =>{
        this.setState({
            isAdded: false,
        })
    }

    changeInsertUdf = (e) =>{
        this.setState({
            insertUdf: e.target.value,
        })
    }

    clearMapValue = () =>{
        this.setState({
            insertUdf:'',
        })
    }

    saveMapValue = () =>{
        let data = {
          isInsert: true,
          udfMappingData: [{
            name: this.state.insertUdf,
            code: '',
            udfType: this.state.udfType,
            ext: "N"
          }]
        }

        this.props.updateUdfMappingRequest(data)
    }

    updateSetUdfData = () =>{
        let data = {
            isInsert: false,
            udfMappingData: this.state.changedTableData
          }
  
          this.props.updateUdfMappingRequest(data)
    }

    resetSetUdfData = () =>{
        this.getSetUdfData()
    }

    clearSetUdfData = () => {
        this.setState({            
            udfName: '',
            udfType: '',
            insertUdf: '',
            tableData: [],
            tableSearch: '',
            changedTableData: '',
        }, this.props.saveCheckMap(''))    
        document.getElementById("mySelect").value = "Please Select!";    
    }

    getSetUdfData = () =>{
        let data = {
            "no":1,
            "type":1,
            "search":"",
            "udfType": this.state.udfType,
            "code":"",
            "name":"",
            "ext":"",
            "searchBy":"contains"}
        this.props.poUdfMappingRequest(data)
    }

    tableSearchHandler = (e) => {
        this.setState({
            tableSearch: e.target.value,
        })
    }

    clearTableSearch = () =>{
        this.setState({
            tableSearch: '',
        })
    }

    changeTableDataHandler = (data, key) =>{
        let tableData = [...this.state.tableData];
        let extVal = data.ext == 'Y' ? 'N' : 'Y';
        let changedTableData = [...this.state.changedTableData];
        let updatedTable = tableData.map((item, indx )=> {
            if(indx == key){                        
                return {...item, ext: extVal}
            } else{
                return item
            }
        })
        if(changedTableData == '' || changedTableData.filter(itm => itm.code == data.code) == ''){
            changedTableData.push({name: data.value, code: data.code, udfType: data.udfType, ext: extVal})
        } else {
            changedTableData = changedTableData.map(itm => {
               if(itm.code == data.code){
                   return {name: data.value, code: data.code, udfType: data.udfType, ext: extVal}
               } else {
                   return itm
               }
            })
        }

        this.setState({
            tableData: updatedTable,
            changedTableData: changedTableData
        })
        this.props.saveCheckMap(changedTableData)
    }

    render() {
        console.log(this.props.purchaseIndent.poUdfMapping, this.state.tableData)
        const {tableData, tableSearch} = this.state;
        let filterTableData = tableData.filter(data=>{
            if (tableSearch == ''){
                return data;
            }
            else if (data.udfType.toLowerCase().includes(tableSearch.toLowerCase()) || data.value.toLowerCase().includes(tableSearch.toLowerCase())){
                return data;
            }
        })
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="udf-additional-section">
                        <div className="pi-new-layout">
                            <div className="col-lg-2 pad-lft-0">
                                <label className="pnl-purchase-label">Select UDF</label>
                                <select className="pnl-purchase-select onFocus" id="mySelect" onChange={this.setUdfType}>
                                    <option value = "Please Select!">Please Select</option>
                                    {this.state.setUdfData.length != 0 && 
                                        this.state.setUdfData.map(udf=>{
                                            return <option value={udf.udfType + '|' + udf.displayName}>{udf.udfType}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="col-lg-2">
                                <label className="pnl-purchase-label">Name</label>
                                <input autocomplete="off" disabled="" readonly="" type="text" class="pnl-purchase-read" id="termData" placeholder="" value={this.state.udfName} />
                            </div>
                            {!this.state.isAdded && <div className="col-lg-2 udf-flex" onClick={this.openAdded}>
                                <img className="udff-add-icon" src={require('../../assets/add-green.svg')} />
                            </div>}
                            {this.state.isAdded && <div className="col-lg-2">
                                <label className="pnl-purchase-label">Enter Value</label>
                                <input type="text" className="uas-input" value={this.state.insertUdf} onChange={this.changeInsertUdf} />
                            </div>}
                            {this.state.isAdded && <div className="col-lg-1 udf-flex" onClick={this.closeAdded}>
                                <img className="udff-close-icon" src={require('../../assets/closeRedNew.svg')} />
                            </div>}
                        </div>
                        <div className="col-lg-6 pad-0 gen-item-udf">
                        {this.state.insertUdf != '' && <div className="gvpd-left">
                            <button type="button" className="giu-save" onClick={this.saveMapValue} >Save</button>
                            <button type="button" onClick={this.clearMapValue}>Clear</button>
                        </div>}                            
                    </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="gen-item-udf">
                        <div className="col-lg-6 pad-0">
                            {/* <div className="gvpd-left">
                                <button type="button" className="remove-mapping-btn">Remove Mapping</button>
                            </div> */}
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="gvpd-search">
                                    <input type="search"  placeholder="Type to Search..." onChange={this.tableSearchHandler} value={this.state.tableSearch}/>
                                    <img className="search-image" src={require('../../assets/searchicon.svg')}  />
                                    {this.state.tableSearch && <span className="closeSearch" id="clearSearchFilter" onClick={this.clearTableSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                                </div>
                                {/* <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                            <li>
                                                <button className="export-excel" type="button">
                                                    <span className="pi-export-svg">
                                                        <img src={require('../../assets/downloadAll.svg')} />
                                                    </span>
                                                Download All</button>
                                            </li>
                                        </ul>}
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="gen-item-udf-table">
                        <div className="giut-headfix" id="table-scroll">
                        {/* <div className="columnFilterGeneric">
                            <span className="glyphicon glyphicon-menu-left"></span>
                        </div> */}
                            <table className="table gen-main-table">
                                <thead >
                                    <tr>
                                        <th className="fix-action-btn"></th>
                                        <th className="width145">
                                            <label>UDF Type</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Value</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Ext</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                        {
                                            filterTableData.length == 0 ?
                                            <tr>
                                                <td align="center" colSpan="12">
                                                    <label>No Data Available</label>
                                                </td>
                                            </tr>:
                                            filterTableData.map((item, key) => (
                                                <tr key={key}>
                                                    <td className="fix-action-btn">
                                                        {/* <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                        </ul> */}
                                                    </td>
                                                    <td className="width145 border-lft"><label className="bold">{item.udfType}</label></td>
                                                    <td ><label className="bold">{item.value}</label></td>                                                   
                                                    <td>
                                                        <label className="checkBoxLabel0 displayPointer">
                                                            <input type="checkBox" checked={item.ext == 'Y' ? true : false} className="checkBoxTab" onChange={() => this.changeTableDataHandler(item, key)} /> 
                                                            <span className="checkmark1"></span> 
                                                        </label>
                                                    </td>
                                                </tr>
                                            ))
                                        }                                                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-12 pad-0" >
                        <div className="new-gen-pagination">
                            {/* <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} /> */}
                                    {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                {/* </div>
                            </div> */}
                            {/* <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination />
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SetUdfMapping;