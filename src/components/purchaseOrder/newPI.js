import React from "react";
import { CONFIG } from '../../config/index';
import axios from 'axios';
import { throttle, debounce } from 'throttle-debounce';
import UdfMappingModal from "./udfMappingModal";
import SupplierModal from "../purchaseIndent/purchaseVendorModal";
import ToastLoader from "../loaders/toastLoader";
import ArticleModal from "./articleModal";
import PiColorModal from "../purchaseIndent/piColorModal";
import moment from 'moment';
import { getDate } from '../../helper';
import ItemCodeModal from "../purchaseOrder/itemCodeModal";
import PiImageModal from "../purchaseIndent/piImageModal";
import VendorDesignModal from "../purchaseOrder/vendorDesignModal";
import TransporterSelection from "../purchaseIndent/transporterSelection";
import LoadIndent from "./loadIndent";
import PoError from "../loaders/poError";
import AlertNotificationModal from "./alertNotificationModal";
import ItemUdfMappingModal from "./itemUdfMappingModal";
import ConfirmModalPo from "../loaders/confirmModalPo";
import DeleteModalPo from "../loaders/deleteConfirmModal"

import exclaimIcon from "../../assets/exclain.svg";
import _ from 'lodash';
import OrderNumberModal from "./orderNumber";
import SetVendorModal from "./setVendorModal";
import SetDepartmentModal from "./setDepartmentModal";
import ResetAndDelete from "../loaders/resetAndDelete";
import copyIcon from "../../assets/copy-icon-copy.svg";
import invoice from "../../assets/invoice.svg";
import HsnCodeModal from "./hsnCodeModal";
import SiteModal from "./siteModal";
import IcodeModalInner from "./poIcodeModal2";
import IcodeModal from "./poIcodeModal"
import NewIcodeModal from "./newIcodemodal";
import CityModal from "./cityModal";
import FilterLoader from '../loaders/filterLoader';
import DiscountModal from "./discountModal";
import PoArticleModal from "./poArticleModal"
import SetModal from './setModal'

class GenericPurchaseIndent extends React.Component {
    constructor(props) {
        super(props);
        // this.escChild = React.createRef()

        this.state = {
            disId: "",
            mrpId: "",
            isSet: true,
            hsnId: "",
            uuid: "",
            discountSearch: "",
            mrpSearch: "",
            hsnSearch: "",
            poArticleSearch: "",
            focusedObj: {
                type: "",
                rowId: "",
                cname: "",
                value: "",
                radio: "",
                finalRate: "",
                colorObj: {
                    color: [],
                    colorList: []
                }

            },

            code: "",

            lineItemRowId: "",
            setModal: false,
            setModalAnimation: false,
            selectedRowId: "",
            sectionName: "",
            divisionName: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            hl3Code: "",
            hl4Name: "",
            hl4Code: "",
            basedOn: "",
            mrpStart: "",
            mrpEnd: "",
            startRange: "",
            endRange: "",
            poArticle: false,
            poArticleAnimation: false,


            isVendorDesignNotReq: false,
            typeOfBuying: "",
            displayOtb: true,
            transporterValidation: true,
            addRow: true,
            isRequireSiteId: false,
            itemDetailCode: "",
            dateValidationRes: false,
            validFromValidation: false,
            valtdToValidation: false,
            restrictedStartDate: "",
            restrictedEndDate: "",
            isMRPEditable: false,
            isRSP: false,
            discountMrp: "",
            selectedDiscount: "",
            discountGrid: "",
            discountModal: false,
            loader: false,
            clickedOneTime: false,
            cityModal: false,
            cityModalAnimation: false,
            city: "",
            cityerr: false,
            isCityExist: false,
            itemudf1Validation: false,
            itemudf2Validation: false,
            itemudf3Validation: false,
            itemudf4Validation: false,
            itemudf5Validation: false,
            itemudf6Validation: false,
            itemudf7Validation: false,
            itemudf8Validation: false,
            itemudf9Validation: false,
            itemudf10Validation: false,
            itemudf11Validation: false,
            itemudf12Validation: false,
            itemudf13Validation: false,
            itemudf14Validation: false,
            itemudf15Validation: false,
            itemudf16Validation: false,
            itemudf17Validation: false,
            itemudf18Validation: false,
            itemudf19Validation: false,
            itemudf20Validation: false,

            udf1Validation: false,
            udf2Validation: false,
            udf3Validation: false,
            udf4Validation: false,
            udf5Validation: false,
            udf6Validation: false,
            udf7Validation: false,
            udf8Validation: false,
            udf9Validation: false,
            udf10Validation: false,

            udf11Validation: false,
            udf12Validation: false,
            udf13Validation: false,
            udf14Validation: false,
            udf15Validation: false,
            udf16Validation: false,
            udf17Validation: false,
            udf18Validation: false,
            udf19Validation: false,
            udf20Validation: false,

            cat1Validation: false,
            cat2Validation: false,
            cat3Validation: false,
            cat4Validation: false,
            cat5Validation: false,
            cat6Validation: false,

            desc1Validation: false,
            desc2Validation: false,
            desc3Validation: false,
            desc4Validation: false,
            desc5Validation: false,
            desc6Validation: false,

            isDiscountAvail: false,
            isDiscountMap: false,
            isArticleSeparated: false,
            oldValue: "",
            multipleErrorpo: false,

            lineError: [],
            focusedQty: {
                id: "",
                qty: ""
            },
            copingLineItem: [],
            deleteGridId: "",
            deleteSetNo: "",
            deleteConfirmModal: false,
            lineItemChange: false,
            saveMarginRule: "",
            poItemBarcodeData: {},
            icodeModalAnimation: false,
            itemBarcodeModal: false,
            itemcodeerr: false,
            itemCodeList: [],
            colorListValue: [],
            isAdhoc: false,
            isIndent: false,
            isSetBased: false,
            isHoldPo: false,
            isPOwithICode: false,
            poWithUpload: false,
            copyColor: "false",
            focusImage: "",
            focusState: "",
            rateFocus: "",
            focusId: "",
            itemFocus: "",
            itemCodeId: "",
            section3ColorId: "",
            mappingId: "",
            onFieldSite: false,
            siteName: "",
            siteCode: "",
            siteNameerr: false,
            siteModal: false,
            siteModalAnimation: false,
            itemUdfExist: "false",
            isUDFExist: "false",
            isSiteExist: "false",
            isOtbValidationPi: "false",


            markUpYes: false,
            tradeGrpCode: "",
            hsnModal: false,
            hsnModalAnimation: false,


            changeLastIndate: true,
            descSix: "",
            minDate: getDate(),
            maxDate: "",
            slCityName: "",
            deleteMrp: "",
            deleteOtb: "",
            desc6: "",
            loadIndentId: "",
            imageRowId: "",
            imageState: {},
            imageModal: false,
            imageModalAnimation: false,
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            gstInNo: "",
            itemValue: "",
            RadioChange: "",
            gridFirst: false,
            gridSecond: false,
            gridThird: false,
            gridFourth: false,
            gridFivth: false,
            itemUdfName: "",
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: false,
            itemUdfId: "",

            itemUdfType: "",
            vendorId: "",
            articleName: "",
            udfRowMapping: [],
            udfMappingData: [],
            colorNewData: [],
            mrp: "",
            itemName: "",
            trasporterModal: false,
            sizeMapping: [],
            transporterAnimation: false,
            transporterCode: "",
            transporterName: "",
            loadIndentModal: false,
            loadIndentAnimation: false,
            purchaseTermState: [],
            itemModal: false,
            itemModalAnimation: false,
            toastLoader: false,
            poErrorMsg: false,
            toastMsg: "",
            errorMassage: "",
            poItemcodeData: [],
            term: "",
            udfMapping: false,
            udfMappingAnimation: false,
            udfSetting: false,
            udfSettingAnimation: false,
            poItems: false,
            poItemsAnimation: false,
            poColor: false,
            indentRadio: "",
            indentRadioerr: false,
            poDate: "",
            poDateerr: false,
            loadData: this.props.purchaseIndent.loadIndent.data.resource,
            loadIndent: "",
            loadIndenterr: false,
            poValidFrom: "",
            lastInDate: "",
            lastAsnDate: "",

            slCode: "",
            slAddr: "",
            slName: "",
            item: "",
            text: "",
            vendor: "",
            leadDays: "",
            transporter: "",

            supplier: "",

            supplierModal: false,
            supplierModalAnimation: false,
            articleModalAnimation: false,
            articleModal: false,
            codeRadio: "Adhoc",
            open: false,
            search: "",
            colorData: [],
            colorRow: "",
            indentLoad: true,
            isDescriptionChecked: false,
            isSecTwoDescriptionChecked: false,
            isUdfDescriptionChecked: false,
            isItemUdfDescriptionChecked: false,
            termCode: "",
            termName: "",
            poAmount: 0,
            poQuantity: 0,
            vendorPoState: [],
            udfType: "",
            udfName: "",
            udfRow: "",
            colorCode: "",
            colorModal: false,
            colorModalAnimation: false,
            itemUdfMappingState: [],
            supplierCode: "",
            // __________________________validation error states_________________
            combineRow: false,
            poValidFromerr: false,
            lastInDateerr: false,
            lastAsnDateerr: false,

            itemerr: false,
            vendorerr: false,
            transportererr: false,
            otbStatus: false,
            setValue: "",
            vendorDesignVal: "",
            indentValue: "",
            colorValue: "",
            flag1: 0,
            flag: false,
            otbArray: [],
            otbValue: {},
            otbValueData: "",
            mrpValueData: "",
            itemDetailName: "",
            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: false
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",

                marginRule: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],

                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",

                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: [],
                    lineItemChk: false

                }]

            }],

            //last 
            currentDate: "",
            sizeRows: [],
            vendorDesignModal: false,
            vendorDesignAnimation: false,
            vendorDesignData: [],

            itemDetailsHeader: [],
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            itemType: "",
            itemId: "",
            itemDetailValue: "",
            setBased: "",
            poSetVendorerr: false,
            departmentSeterr: false,
            departmentSet: "",
            poSetVendor: "",
            orderSet: "",
            orderSeterr: false,
            loadIndentTrue: false,
            fourApi: false,
            piKeyData: [],
            descId: "",
            departmentSetBasedAnimation: false,
            departmentModal: false,
            orderNumber: false,
            orderNumberModalAnimation: false,
            SetVendor: false,
            setVendorModalAnimation: false,
            hl3CodeDepartment: "",
            poSetVendorCode: "",
            setDepartment: "",
            resetAndDelete: false,
            itemArticleCode: "",

            icodeId: "",
            icodesArray: [],
            openIcodeModal: false,
            icodeValidation: false,
            isMrpRequired: true,
            isRspRequired: true,
            isColorRequired: true,
            isDisplayFinalRate: false,
            isTransporterDependent: true,
            isDisplayMarginRule: true,
            isGSTDisable: false,
            isTaxDisable: false,
            isBaseAmountActive: false,
            typeOfBuyingErr: false,
            isModalShow: false,
            supplierSearch: "",
            transporterSearch: "",
            citySearch: "",
            siteSearch: "",
            loadIndentSearch: "",
            setVendorSearch: "",
            orderSearch: "",
            isMrpRangeDisplay: true,
            isMarginRulePi: false,
            parent: "PI"


        }
    }
    onnRadioChange(cr) {
        if (cr != true && cr != false) {
            this.setState({
                codeRadio: cr,
            })

            setTimeout(() => {
                if (cr === "raisedIndent") {
                    document.getElementById("loadIndentInput").focus()
                } else if (cr === "setBased" || cr === "holdPo") {
                    document.getElementById("poSetVendor").focus()
                } else if (cr === "Adhoc" || cr === "poIcode") {
                    document.getElementById("poValidFrom").focus()
                }
                this.onClear();
                this.setBasedResest();
            }, 10);
        } else if (cr == true || cr == false) {
            this.setState({
                isSet: cr
            }, () => {

                this.resetPoRows()
            })
        }
    }

    handleRadioChange(dat) {
        let indent = ""
        if (dat == "Adhoc") {
            indent = "adhoc"
        } else if (dat == "raisedIndent") {
            indent = "raised indent"
        } else if (dat == "setBased") {
            indent = "set based"
        }
        else if (dat == "holdPo") {
            indent = "hold PO"
        }
        else if (dat == "poIcode") {
            indent = "PO with Icode"
        }
        this.setState({
            headerMsg: "Are you sure you want to raise purchase order on the basis of " + indent + " ?",
            paraMsg: "Click confirm to continue.",
            radioChange: dat,
            confirmModal: true,
        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })

    }
    Capitalize(str) {
        if (str == undefined) {
            return "";
        } else {

            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    }
    _handleKeyPressDate(e, id) {
        let idd = id;
        if (e.key === "Enter") {
            document.getElementById(idd).click();
        }
    }
    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        } else if (e.key === "Enter") {
            if (id == "vendor") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openSupplier() : null
            }
            else if (id == "city") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openCityModal() : null
            } else if (id == "siteName") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openSiteModal() : null
            } else if (id == "transporter") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openTransporterSelection() : null
            } else if (id == "loadIndentInput") {
                this.openloadIndentSelection()
            } else if (id == "poSetVendor") {
                this.setVendorModalOpen()
            } else if (id == "orderSet") {
                this.onSetOrderNumber()
            }
        } else {
            document.getElementById(id).focus()
        }
    }

    handleInputChange(e) {

        if (e.target.id == "lastInDate") {
            // let restrictedStartDate = this.state.restrictedStartDate
            // let restrictedEndDate = this.state.restrictedEndDate
            // let flag = false
            // if (restrictedStartDate != "" && restrictedEndDate != "" && e.target.value != "") {
            //     if (restrictedStartDate <= e.target.value && e.target.value <= restrictedEndDate) {
            //         this.setState({
            //             poErrorMsg: true,
            //             errorMassage: "You are unable to save PO(Purchase Order) for the selected date(" + e.target.value + ")",

            //         })
            //         flag = true

            //     } else {
            //         e.target.placeholder = e.target.value;
            //         this.setState({
            //             lastInDate: e.target.value
            //         }, () => {
            //             this.lastInDate()
            //         })
            //     }} 

            //     else{
            e.target.placeholder = e.target.value;
            this.setState({
                lastInDate: e.target.value
            }, () => {
                this.lastInDate()
            })
        }
        //    if(!flag){
        if (e.target.value != "") {
            var d = new Date(e.target.value);
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            var lastinDateValue = moment(new Date(year, month, day)).format("YYYY-MM-DD")
            document.getElementById("lastInDate").placeholder = lastinDateValue
            this.setState({
                lastInDate: lastinDateValue,
                maxDate: lastinDateValue,
            }, () => {
                this.lastInDate()
            })
            // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.hl4Code != "") {

            let poRows = [...this.state.poRows]

            if (this.state.isMrpRequired && this.state.displayOtb) {
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];
                const date = new Date(lastinDateValue);
                let year = date.getFullYear()
                let monthName = (monthNames[date.getMonth()]);
                let articleList = []
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].articleCode != "" && poRows[i].vendorMrp != "") {
                        let payload = {
                            rowId: poRows[i].gridOneId,
                            articleCode: poRows[i].articleCode,
                            mrp: poRows[i].vendorMrp
                        }
                        articleList.push(payload)


                    }
                }
                let dataa = {
                    articleList: articleList,
                    month: monthName,
                    year: year
                }
                this.setState({
                    changeLastIndate: true
                })
                if (articleList.length != 0) {
                    this.props.multipleOtbRequest(dataa)
                }
            }
            for (let i = 0; i < poRows.length; i++) {
                poRows[i].deliveryDate = lastinDateValue
            }
            this.setState({
                poRows: poRows
            })

        }
        else if (e.target.id == "loadIndent") {
            this.setState({
                loadIndent: e.target.value,
                indentLoad: true
            })
        }
        else if (e.target.id == "Adhoc") {
            this.setState({
                indentLoad: false
            })
        }
    }

    openloadIndentSelection(e) {
        this.onEsc()
        var data = {
            no: 1,
            type: this.state.loadIndent == "" || this.state.isModalShow ? 1 : 3,
            search: this.state.isModalShow ? "" : this.state.loadIndent,
            orderId: "",
            supplier: "",
            cityName: "",
            piFromDate: "",
            piToDate: ""
        }
        this.props.loadIndentRequest(data)
        this.setState({
            loadIndentSearch: this.state.isModalShow ? "" : this.state.loadIndent,
            indentValue: this.state.loadIndentId,
            loadIndentModal: true,
            loadIndentAnimation: !this.state.loadIndentAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onCloseLoadIndent(e) {
        this.setState({
            loadIndentModal: false,
            loadIndentAnimation: !this.state.loadIndentAnimation
        });


        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("loadIndentInput").focus()
    }
    updateLoadIndentState(code) {

        this.setState({
            getPoDataRequest: code.sCode,
            loadIndent: code.pattern,
            loadIndentId: code.indentId
        })
        let data = {
            indentNo: code.sCode
        }

        this.props.gen_IndentBasedRequest(data);

        document.getElementById("loadIndentInput").focus()



    }

    departmentSetModal(e) {
        this.setState({
            departmentModal: true,
            departmentSetBasedAnimation: !this.state.departmentSetBasedAnimation,
        })
        let data = {
            no: 1,
            type: 1,
            search: "",
        }
        this.props.departmentSetBasedRequest(data)

    }
    onCloseDepartmentModal(e) {
        this.setState({
            departmentModal: false,
            departmentSetBasedAnimation: !this.state.departmentSetBasedAnimation,
        })
        document.getElementById('setDepartment').focus()
    }
    updateDepartment(data) {
        this.setState({
            setDepartment: data.setDepartment,
            hl3CodeDepartment: data.hl3code,
            poSetVendor: "",
            orderSet: ""
        })
        this.onClear();


    }

    setVendorModalOpen(e, id) {
        // if (this.state.setDepartment != "") {
        this.onEsc()
        let data = {
            no: 1,
            type: this.state.poSetVendor == "" || this.state.isModalShow ? 1 : 3,
            search: this.state.isModalShow ? "" : this.state.poSetVendor,
            hl3code: this.state.hl3CodeDepartment,
        }
        this.props.selectVendorRequest(data)
        this.setState({
            setVendorSearch: this.state.isModalShow ? "" : this.state.poSetVendor,
            focusId: id,
            setVendor: true,
            setVendorModalAnimation: !this.state.setVendorModalAnimation,
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
        // } else {
        //     this.setState({
        //         errorMassage: "Select Department",
        //         poErrorMsg: true,
        //         focusId: "setDepartment"


        //     })
        // }
    }
    onCloseVendor() {
        this.setState({
            setVendor: false,
            setVendorModalAnimation: !this.state.setVendorModalAnimation,
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("poSetVendor").focus()
    }

    updateVendorPo(data) {
        this.setState({
            poSetVendor: data.poSetVendor,
            poSetVendorCode: data.poSetVendorCode,
            orderSet: ""
        })
        this.onClear();
    }

    onSetOrderNumber(e) {
        this.onEsc()
        if (this.state.lastInDate != "") {
            if (this.state.poSetVendor != "") {
                this.setState({
                    orderNumber: true,
                    orderNumberModalAnimation: !this.state.orderNumberModalAnimation,
                    orderSearch: this.state.isModalShow ? "" : this.state.orderSet
                })
                let data = {
                    no: 1,
                    type: this.state.orderSet == "" || this.state.isModalShow ? 1 : 3,

                    search: this.state.isModalShow ? "" : this.state.orderSet,
                    hl3Code: this.state.hl3CodeDepartment,
                    supplierCode: this.state.poSetVendorCode
                }
                this.props.selectOrderNumberRequest(data)
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            } else {
                this.setState({
                    focusId: "setVendor",
                    errorMassage: "Select Vendor",
                    poErrorMsg: true

                })
            }
        } else {
            this.setState({
                errorMassage: "Select Last In Date",
                poErrorMsg: true

            })
        }

    }

    closeSetNumber() {
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }

        this.setState({
            orderNumber: false,
            orderNumberModalAnimation: !this.state.orderNumberModalAnimation


        })
        document.getElementById("orderSet").focus()

    }

    updateOrderNumber(data) {

        this.setState({
            orderSet: data
        })
        let payload = {
            orderNo: data,
            validTo: moment(this.state.lastInDate).format("DD-MMM-YYYY"),
            poType: this.state.codeRadio
        }

        this.props.gen_SetBasedRequest(payload)
        document.getElementById("orderSet").focus()

    }

    openSiteModal() {
        this.onEsc()
        let data = {
            type: this.state.siteName == "" || this.state.isModalShow ? 1 : 3,
            no: 1,
            search: this.state.isModalShow ? "" : this.state.siteName
        }
        this.props.procurementSiteRequest(data)
        this.setState({
            siteSearch: this.state.isModalShow ? "" : this.state.siteName,
            siteModal: true,
            siteModalAnimation: !this.state.siteModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }


    }
    closeSiteModal() {
        this.setState({
            siteModal: false,
            siteModalAnimation: !this.state.siteModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("siteName").focus()
    }

    updateSite(data) {
        this.setState({
            siteCode: data.siteCode,
            siteName: data.siteName
        })
        document.getElementById("siteName").focus()
    }

    openCityModal() {

        this.onEsc()
        let data = {
            type: this.state.city == "" || this.state.isModalShow ? 1 : 3,
            no: 1,
            search: this.state.isModalShow ? "" : this.state.city
        }
        this.props.getAllCityRequest(data)

        this.setState({
            citySearch: this.state.isModalShow ? "" : this.state.city,
            cityModal: true,
            cityModalAnimation: !this.state.cityModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeCity() {
        this.setState({
            cityModal: false,
            cityModalAnimation: !this.state.cityModalAnimation
        })
        document.getElementById("city").focus()
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateCity(data) {
        this.setState({
            city: data.city,
            supplier: "",
            stateCode: "",
            slCode: "",
            slName: "",
            slAddr: "",
            transporterCode: "",
            transporterName: "",
            leadDays: "",
            transporter: "",
            tradeGrpCode: "",
            term: "",
            termCode: "",
            termName: "",
            gstInNo: "",
            slCityName: "",
            poAmount: 0,
            poQuantity: 0,
            supplierCode: ""
        }, () => {
            this.resetPoRows();


        })


    }

    openSupplier(e, id) {
        this.onEsc()
        if (this.state.isCityExist == true) {
            if (this.state.city != "") {

                var data = {
                    no: 1,
                    type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                    slCode: "",
                    name: "",
                    address: "",
                    department: "",
                    departmentCode: "",
                    siteCode: this.state.siteCode,
                    city: this.state.city,

                    search: this.state.isModalShow ? "" : this.state.supplier,
                }
                this.props.supplierRequest(data);
                this.setState({
                    supplierModal: true,
                    supplierModalAnimation: !this.state.supplierModalAnimation

                });
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            } else {


                this.setState({
                    focusId: "city",
                    errorMassage: "Select city",
                    poErrorMsg: true
                })
            }

        } else {

            var data = {
                no: 1,
                type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                slCode: "",
                name: "",
                address: "",
                department: "",
                departmentCode: "",
                siteCode: this.state.siteCode,
                city: "",
                search: this.state.isModalShow ? "" : this.state.supplier,
            }
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }
            this.props.supplierRequest(data);
            this.setState({
                supplierModal: true,
                supplierModalAnimation: !this.state.supplierModalAnimation

            });
        }



    }
    onCloseSupplier(e) {
        this.setState({
            supplierModal: false,
            supplierModalAnimation: !this.state.supplierModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("vendor").focus()
    }

    updateSupplierState(data) {

        this.setState({
            supplier: data.supplier,
            stateCode: data.stateCode,
            slCode: data.slcode,
            slName: data.slName,
            slAddr: data.slAddr,
            transporterCode: data.transporterCode,
            transporterName: data.transporterName,
            flag: this.state.flag + 1,
            transporter: data.transporter,
            tradeGrpCode: data.tradeGrpCode,
            term: data.termName,
            termCode: data.purtermMainCode,
            termName: data.termName,
            gstInNo: data.gstInNo,
            slCityName: data.city,
            poAmount: 0,
            poQuantity: 0,
            supplierCode: data.slcode,
            itemCodeList: []


        }, () => {
            this.supplier();
            this.transporter();
        })
        if (data.stateCode == "" && data.data.stateCode == undefined) {
            this.setState({
                focusId: "vendor",
                errorMassage: "Supplier GSTIN can't be null",
                poErrorMsg: true
            })
        }
        this.props.leadTimeRequest(data.slcode)

        setTimeout(() => {
            this.resetPoRows();

        }, 10)
        document.getElementById("vendor").focus()
    }
    openTransporterSelection(e, id) {
        this.onEsc()
        if (this.state.supplier != "") {
            var data = {
                no: 1,
                type: this.state.transporter == "" || this.state.isModalShow ? 1 : 3,
                search: this.state.isModalShow ? "" : this.state.transporter,
                transporterCode: "",
                transporterName: "",
                city: this.state.isTransporterDependent != false ? this.state.city : ""

            }
            this.props.getTransporterRequest(data)
            this.setState({
                trasporterModal: true,
                transporterAnimation: !this.state.transporterAnimation,
                transporterSearch: this.state.isModalShow ? "" : this.state.transporter,
            });
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }
        }
        else {
            this.setState({
                focusId: "vendor",
                errorMassage: "Select Vendor ",
                poErrorMsg: true
            })
        }

    }
    onCloseTransporter(e) {
        this.setState({
            trasporterModal: false,
            transporterAnimation: !this.state.transporterAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("transporter").focus()
    }

    updateTransporterState(code) {

        this.setState({

            transporterCode: code.transporterCode,
            transporterName: code.transporterName,

            transporter: code.transporterName,

        },
            () => {
                this.transporter();
            })

        document.getElementById("transporter").focus()


    }
    typeOfBuying() {
        if (this.state.typeOfBuying != "") {
            this.setState({ typeOfBuyingErr: false })
        } else {
            this.setState({ typeOfBuyingErr: true })
        }
    }
    handleChange(e) {
        if (e.target.id == "typeofBuying") {
            this.setState({ typeOfBuying: e.target.value })
        }


    }
    //validationn msg 

    loadIndent(e) {
        if (this.state.loadIndent == "") {
            this.setState({
                loadIndenterr: true
            });
        } else {
            this.setState({
                loadIndenterr: false
            });
        }

    }
    poValid(e) {
        if (this.state.poValidFrom == "") {
            this.setState({
                poValidFromerr: true
            });
        } else {
            this.setState({
                poValidFromerr: false
            });
        }
    }

    lastInDate(e) {
        if (this.state.lastInDate == "") {
            this.setState({
                lastInDateerr: true
            });
        } else {
            this.setState({
                lastInDateerr: false
            });
        }
    }


    supplier(e) {
        if (this.state.supplier == "") {
            this.setState({
                vendorerr: true
            });
        } else {
            this.setState({
                vendorerr: false
            });
        }
    }

    transporter(e) {
        if (this.state.transporterValidation) {
            if (this.state.transporter == "") {
                this.setState({
                    transportererr: true
                });
            } else {
                this.setState({
                    transportererr: false
                });
            }
        } else {
            this.setState({
                transportererr: false
            });
        }
    }
    site(e) {
        if (this.state.siteName == "") {
            this.setState({
                siteNameerr: true
            });
        } else {
            this.setState({
                siteNameerr: false
            });
        }
    }
    city(e) {
        if (this.state.city == "") {
            this.setState({
                cityerr: true
            });
        } else {
            this.setState({
                cityerr: false
            });
        }
    }


    icode(e) {
        if (this.state.itemCodeList.length == 0) {
            this.setState({
                itemcodeerr: true
            });
        } else {
            this.setState({
                itemcodeerr: false
            });
        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false
        })
        if (document.getElementById(this.state.focusId) != null) {
            document.getElementById(this.state.focusId).focus()
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }



    openNewIcode(id, articleCode, idx) {

        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            if (this.state.slCode != "") {
                if (articleCode != "") {
                    this.setState({
                        icodeModalAnimation: !this.state.icodeModalAnimation,
                        itemBarcodeModal: true,
                        focusId: "itemCode" + idx,
                        selectedRowId: id,
                        hl4Code: articleCode
                    })

                    let payload = {
                        type: 1,
                        no: 1,
                        search: "",
                        articleCode: articleCode,
                        supplierCode: this.state.slCode,
                        siteCode: this.state.siteCode
                    }

                    this.props.getPoItemCodeRequest(payload)
                } else {
                    this.setState({
                        errorMassage: "Article code  is mandatory",
                        poErrorMsg: true,
                        focusId: "articleCode" + idx

                    })
                }
            } else {
                this.setState({
                    errorMassage: "Supplier code  is mandatory",
                    poErrorMsg: true,
                    focusId: "vendor"

                })
            }
        }
    }
    closeNewIcode() {
        this.setState({
            icodeModalAnimation: !this.state.icodeModalAnimation,
            itemBarcodeModal: false,
        })
        document.getElementById(this.state.focusId).focus()
    }
    updateNewIcode(data) {
        let poRows = [...this.state.poRows]
        let articleCode = ""
        poRows.map((e) => {
            if (e.gridOneId == this.state.selectedRowId) {
                e.itemCodeList = data
                articleCode = e.articleCode
            }

        })
        this.setState({
            poRows
        })
        let payload = {
            validTo: moment(this.state.lastInDate).format("DD-MMM-YYYY"),
            articleCode: articleCode,
            items: data
        }
        this.props.poItemBarcodeRequest(payload)
    }

    openHsnCodeModal(id, articleCode, idx, departmentCode, hsnSacCode, hsnId) {
        this.onEsc()
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            if (articleCode != "") {
                this.setState({
                    hsnRowId: "",
                    hsnModal: true,
                    hsnModalAnimation: !this.state.hsnModalAnimation,
                    focusId: "hsnCode" + idx,
                    selectedRowId: id,
                    departmentCode: departmentCode,
                    hsnSearch: this.state.isModalShow ? "" : hsnSacCode,
                    hsnId: hsnId

                })
                let data = {
                    rowId: "",
                    code: departmentCode,
                    no: 1,
                    type: hsnSacCode == "" || this.state.isModalShow ? 1 : 3,
                    search: this.state.isModalShow ? "" : hsnSacCode,
                }
                this.props.hsnCodeRequest(data)
            } else {
                this.setState({
                    focusId: "articleCode" + idx,
                    errorMassage: "Select Article",
                    poErrorMsg: true
                })
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }

    }
    closeHsnModal() {
        this.setState({
            hsnModal: false,
            hsnModalAnimation: !this.state.hsnModalAnimation
        })
        document.getElementById(this.state.focusId).focus()
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateHsnCode(data) {
        let poRows = [...this.state.poRows]
        poRows.forEach((e) => {
            if (e.gridOneId == this.state.selectedRowId) {
                e.hsnSacCode = data.hsnSacCode
                e.hsnCode = data.hsnCode
            }

        })


        this.setState({
            poRows
        }, () => {


            let poRows = [...this.state.poRows]

            let lineItemArray = []
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.state.selectedRowId) {

                    for (let j = 0; j < poRows[i].lineItem.length; j++) {
                        if (poRows[i].lineItem[j].setQty != "" && poRows[i].finalRate != "" && poRows[i].finalRate != 0) {
                            let taxData = {
                                hsnSacCode: data.hsnSacCode,
                                qty: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total),
                                rate: poRows[i].finalRate,
                                rowId: poRows[i].lineItem[j].gridTwoId,
                                designRowid: Number(this.state.selectedRowId),
                                basic: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total) * Number(poRows[i].finalRate),
                                clientGstIn: sessionStorage.getItem('gstin'),
                                piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                supplierGstinStateCode: this.state.stateCode,
                                purtermMainCode: this.state.termCode,
                                siteCode: this.state.siteCode
                            }
                            lineItemArray.push(taxData)
                        }
                    }

                }
            }
            if (lineItemArray.length != 0) {
                this.setState({
                    lineItemChange: true
                }, () => {
                    this.props.multipleLineItemRequest(lineItemArray)
                }
                )

            }

            document.getElementById(this.state.focusId).focus()
        })

    }
    _handleKeyPressRow(e, idName, count) {

        if (e.key === "F7" || e.key === "F2") {
            let idd = idName + count;
            document.getElementById(idd).click();
        }
        if (e.key === "ArrowDown") {
            count++;

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }
        }
        if (e.key === "ArrowUp") {
            count--

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }

        }
        if (e.key == "Enter") {
            document.getElementById(idName + "Div" + count) != null ? document.getElementById(idName + "Div" + count).click() : null
        }

    }
    openItemCode(e, mrp, id, idFocus, idx, hsnSacCode, mrpStart, mrpEnd, articleCode) {
        this.onEsc()
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            let idd = id
            let mrpp = mrp
            if (this.state.supplier != "") {
                if (hsnSacCode.toString() != "") {
                    this.setState({
                        desc6: mrpp,
                        descId: idd,
                        itemModal: true,
                        itemModalAnimation: !this.state.itemModalAnimation,
                        itemCodeId: idFocus + idx,
                        hl4Code: articleCode,
                        selectedRowId: id,
                        code: articleCode,
                        startRange: mrpStart,
                        endRange: mrpEnd,
                        mrpSearch: this.state.isModalShow ? "" : mrp,
                        mrpId: idFocus + idx,
                        focusId: idFocus + idx,

                    })
                    var data = {
                        code: articleCode,
                        type: mrp == "" || this.state.isModalShow ? 1 : 3,
                        search: this.state.isModalShow ? "" : mrp,

                        no: 1,
                        mrpRangeFrom: mrpStart,
                        mrpRangeTo: mrpEnd
                    }
                    this.props.poItemcodeRequest(data)
                } else {
                    this.setState({
                        errorMassage: "Select HSN or SAC code",
                        poErrorMsg: true,
                        focusId: "hsnCode" + idx,
                    })
                }
            } else {
                this.setState({
                    errorMassage: "Select Vendor",
                    poErrorMsg: true,
                    focusId: "vendor",
                })
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeItemmModal() {
        document.getElementById(this.state.itemCodeId).focus()
        this.setState({
            itemModal: false,
            itemModalAnimation: !this.state.itemModalAnimation
        })

        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }


    updateItem(data) {

        document.getElementById(this.state.itemCodeId).focus()
        let poRows = [...this.state.poRows]
        let articleCode = ""
        let calculatedMargin = []

        for (var i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                poRows[i].vendorMrp = data.mrp
                poRows[i].rsp = data.rsp
                poRows[i].mrp = data.mrp
                poRows[i].rate = ""
                poRows[i].discount = {
                    discountType: "",
                    discountValue: "",
                    discountPer: false
                }
                poRows[i].finalRate = ""
                articleCode = poRows[i].articleCode
                calculatedMargin = poRows[i].calculatedMargin
            }
        }
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        const date = new Date(this.state.lastInDate);
        var year = date.getFullYear()
        var monthName = (monthNames[date.getMonth()]);
        let dataa = {
            rowId: data.id,
            articleCode: articleCode,
            mrp: data.mrp,
            month: monthName,
            year: year
        }
        data.mrp != "" && this.state.isMrpRequired && this.state.displayOtb ? this.props.otbRequest(dataa) : null


        this.setState({
            poRows: poRows
        }, () => {
            if (calculatedMargin.length != 0) {
                this.multiMarkUp(this.state.selectedRowId)
            }
            this.gridFirst()
        })
    }

    handleChangeGridOne(id, e, idx) {
        let idd = id
        let r = [...this.state.poRows];

        if (e.target.id == "vendorDesign" + idx) {
            if (this.state.supplier != "") {
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == idd) {
                        r[i].vendorDesign = e.target.value
                    }
                }

                this.setState({
                    poRows: r,

                })
                const t = this
                setTimeout(function () {
                    t.gridFirst();
                }, 1000)
            } else {
                this.setState({
                    focusId: "vendor",
                    errorMassage: "Supplier is mandatory",
                    poErrorMsg: true
                })
            }
        }
        else if (e.target.id == "rsp" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        r[i].rsp = e.target.value
                    }
                }
                this.setState({
                    poRows: r
                })
            }
        }
        else if (e.target.id == "mrp" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        r[i].mrp = e.target.value
                    }
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "rate" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].vendorDesign == "" && !this.state.isVendorDesignNotReq) {
                            this.setState({
                                focusId: "rate",
                                poErrorMsg: true,
                                errorMassage: "Vendor Design is Complusory "
                            })
                        } else if (r[i].vendorMrp == "" && this.state.isMrpRequired) {
                            this.setState({
                                focusId: "vendorMrp" + idx,
                                poErrorMsg: true,
                                errorMassage: "mrp is Complusory "
                            })
                        } else {
                            r[i].rate = e.target.value
                            r[i].netRate = e.target.value

                        }
                    }
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "netrate" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].vendorDesign == "") {
                            if (!this.state.isVendorDesignNotReq) {
                                this.setState({
                                    errorId: "netrate",
                                    poErrorMsg: true,
                                    errorMassage: "Vendor Design is Complusory "
                                })
                            }
                        } else if (r[i].vendorMrp == "" && this.state.isMrpRequired) {
                            this.setState({
                                errorId: "netrate",
                                poErrorMsg: true,
                                errorMassage: "mrp is Complusory "
                            })

                        } else {
                            r[i].rate = e.target.value
                            r[i].netRate = e.target.value
                        }
                    }
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "date" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].deliveryDate = e.target.value
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        } else if (e.target.id == "remarks" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].remarks = e.target.value
                }
                this.setState({
                    poRows: r
                })
            }
        }
    }

    mouseOverCmprRate(dataRate) {
        let ellRate = document.getElementById(`toolIdRate${dataRate}`)
        var viewportOffset = ellRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - 55;

        if (!document.getElementById(`moveLeftRate${dataRate}`)) {
        } else {
            document.getElementById(`moveLeftRate${dataRate}`).style.left = left;
            document.getElementById(`moveLeftRate${dataRate}`).style.top = top;
        }


    }
    mouseOverCmprMrp(dataRate) {
        let ellRate = document.getElementById(`toolIdMrp${dataRate}`)
        var viewportOffset = ellRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - 55;

        if (!document.getElementById(`moveLeftMrp${dataRate}`)) {
        } else {
            document.getElementById(`moveLeftMrp${dataRate}`).style.left = left;
            document.getElementById(`moveLeftMrp${dataRate}`).style.top = top;
        }


    }
    rateValueOnFocus(e, rowId, type, finalRate) {

        let focusedObj = this.state.focusedObj
        focusedObj.value = e.target.value,
            focusedObj.finalRate = finalRate
        focusedObj.type = type,
            focusedObj.rowId = rowId,
            focusedObj.cname = "",
            focusedObj.radio = this.state.codeRadio,
            focusedObj.colorObj = {
                color: [],
                colorList: []
            }


        this.setState({
            rateFocus: e.target.value,
            focusedObj: focusedObj
        })

    }
    onBlurRate(idd, e) {
        let r = [...this.state.poRows]
        if (e.target.value != "") {
            if (Number(e.target.value) == 0) {

                this.setState({
                    errorMassage: "Rate value can't be less than or equal to Zero(0)",
                    poErrorMsg: true

                })
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == idd) {
                        r[i].rate = ""
                        r[i].finalRate = ""
                    }
                }
                this.setState({
                    poRows: r
                })
            } else {

                if (e.target.value != this.state.rateFocus) {
                    let calculatedMarginValue = ""
                    let finalRate = ""

                    for (let i = 0; i < r.length; i++) {
                        if (r[i].gridOneId == idd) {
                            if (r[i].discount.discountType != "" && this.state.isDiscountAvail) {

                                if (r[i].discount.discountPer) {
                                    let value = e.target.value * (r[i].discount.discountValue / 100)
                                    r[i].finalRate = e.target.value - value
                                    finalRate = e.target.value - value

                                } else {
                                    r[i].finalRate = e.target.value - r[i].discount.discountValue
                                    finalRate = e.target.value - r[i].discount.discountValue
                                }
                            } else {
                                r[i].finalRate = e.target.value
                                finalRate = e.target.value

                            }
                        }
                    }
                    let hsnSacCode = ""
                    for (let i = 0; i < r.length; i++) {
                        if (r[i].gridOneId == idd) {
                            hsnSacCode = r[i].hsnSacCode
                            if ((r[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                                let lineItemArray = []

                                for (let j = 0; j < r[i].lineItem.length; j++) {

                                    if (r[i].lineItem[j].setQty != "" && r[i].lineItem[j].finalRate != "" && r[i].lineItem[j].finalRate != 0) {
                                        let taxData = {
                                            hsnSacCode: r[i].hsnSacCode,
                                            qty: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total),
                                            rate: finalRate,
                                            rowId: r[i].lineItem[j].gridTwoId,
                                            designRowid: Number(idd),
                                            basic: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total) * Number(finalRate),
                                            clientGstIn: sessionStorage.getItem('gstin'),
                                            piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                            supplierGstinStateCode: this.state.stateCode,
                                            purtermMainCode: this.state.termCode,
                                            siteCode: this.state.siteCode
                                        }
                                        lineItemArray.push(taxData)
                                    }
                                }
                                if (hsnSacCode != "" || hsnSacCode != null) {
                                    if (lineItemArray.length != 0) {
                                        this.setState({
                                            lineItemChange: true
                                        }, () => {
                                            this.props.multipleLineItemRequest(lineItemArray)
                                        }
                                        )
                                    }
                                } else {

                                    this.setState({
                                        errorMassage: "HSN code is complusory",
                                        poErrorMsg: true

                                    })
                                }

                            }
                        }
                    }



                    this.setState({
                        poRows: r,

                    })
                }
            }
        } else {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == idd) {

                    r[i].finalRate = ""
                }
            }

            this.setState({
                poRows: r,

            })
        }
    }

    openDiscountModal(discount, grid, idx) {
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {

            let poRows = [...this.state.poRows]
            let mrp = ""
            let articleCode = ""
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == grid) {
                    mrp = poRows[i].vendorMrp
                    articleCode = poRows[i].articleCode
                }

            }
            if (mrp != "" || !this.state.isMrpRequired) {
                this.setState({
                    selectedDiscount: discount,
                    discountGrid: grid,
                    focusId: "discount" + idx,
                    discountModal: true,
                    discountMrp: mrp,
                    disId: idx,
                    discountSearch: discount
                })
                if (this.state.isDiscountMap) {
                    let payload = {
                        articleCode: articleCode,
                        mrp: this.state.isMrpRequired ? mrp : 0,
                        discountType: "percentage"
                    }
                    this.props.discountRequest(payload)
                }
            } else {
                this.setState({
                    focusId: "mrp" + idx,
                    errorMassage: "Mrp is compulsory",
                    poErrorMsg: true
                })
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeDiscountModal() {
        document.getElementById(this.state.focusId) != null ? document.getElementById(this.state.focusId).focus() : null

        this.setState({
            discountModal: false
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateDiscountModal(discountData) {
        let poRows = [...this.state.poRows]
        let value = ""
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].gridOneId == discountData.discountGrid) {

                if (poRows[j].discount.discountPer) {
                    value = poRows[j].rate * discountData.discount.discountValue / 100

                    if (Number(poRows[j].rate - value) < 0) {
                        poRows[j].discount = {
                            discountType: "",
                            discountValue: "",

                        }
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true,

                        })

                    } else {
                        poRows[j].finalRate = poRows[j].rate - value
                        poRows[j].discount = discountData.discount
                    }

                } else {
                    value = poRows[j].rate - discountData.discount.discountValue
                    if (value < 0) {
                        poRows[j].discount = {
                            discountType: "",
                            discountValue: ""
                        }
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true
                        })

                    } else {
                        poRows[j].finalRate = value
                        poRows[j].discount = discountData.discount
                    }

                }
            }
        }
        this.setState({
            poRows
        }, () => {
            let calculatedMarginValue = ""
            let finalRate = ""
            let r = [...this.state.poRows]
            let hsnSacCode = ""


            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == discountData.discountGrid) {
                    hsnSacCode = r[i].hsnSacCode

                    if ((r[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                        let lineItemArray = []

                        for (let j = 0; j < r[i].lineItem.length; j++) {

                            if (r[i].lineItem[j].setQty != "" && r[i].finalRate != "" && r[i].finalRate != 0) {
                                let taxData = {
                                    hsnSacCode: r[i].hsnSacCode,
                                    qty: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total),
                                    rate: r[i].finalRate,
                                    rowId: r[i].lineItem[j].gridTwoId,
                                    designRowid: Number(discountData.discountGrid),
                                    basic: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total) * Number(r[i].finalRate),
                                    clientGstIn: sessionStorage.getItem('gstin'),
                                    piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                    supplierGstinStateCode: this.state.stateCode,
                                    purtermMainCode: this.state.termCode,
                                    siteCode: this.state.siteCode
                                }
                                lineItemArray.push(taxData)
                            }
                        }
                        if (lineItemArray.length != 0) {
                            this.setState({
                                lineItemChange: true
                            }, () => {
                                if (hsnSacCode != "" && hsnSacCode != null) {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                } else {

                                    this.setState({
                                        errorMassage: "HSN code is complusory",
                                        poErrorMsg: true

                                    })
                                }
                            }
                            )
                        }

                    }
                }
            }

            this.setState({
                poRows: r,

            })

        })
    }
    openImageModal(id, image, articleCode, idx) {

        if (articleCode == "" || articleCode == undefined) {
            this.setState({
                errorMassage: "Article code is mandatory",
                poErrorMsg: true,
                focusId: "articleCode" + idx

            })

        }
        else {
            let rows = [...this.state.poRows]

            for (var i = 0; i < rows.length; i++) {
                if (rows[i].gridOneId == id) {

                    this.setState({
                        imageState: rows[i].imageUrl
                    })
                }
            }
            this.setState({
                imageRowId: id,
                imageModal: true,
                imageModalAnimation: !this.state.imageModalAnimation,
                focusId: image
            });
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }

    closePiImageModal() {
        document.getElementById(this.state.focusId).focus()
        this.setState({
            imageModal: false,
            imageModalAnimation: !this.state.imageModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }


    updateImage(data) {
        let c = [...this.state.poRows];
        for (var i = 0; i < c.length; i++) {
            if (c[i].gridOneId.toString() == data.imageRowId) {
                c[i].imageUrl = data.file
                c[i].image = Object.keys(data.file)
                c[i].containsImage = Object.keys(data.file).length != 0 ? true : false
            }

        }

        this.setState({
            poRows: c,

        })
        document.getElementById(this.state.focusId).focus()

    }
    calculatedMargin(marginId, marginLeft) {


        let marginRate = document.getElementById(marginId)
        var viewportOffset = marginRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - (marginId.length <= 8 ? 345 : 175);


        if (!document.getElementById(marginLeft)) {
        } else {
            document.getElementById(marginLeft).style.left = left;
            document.getElementById(marginLeft).style.top = top;
        }
    }


    componentDidMount() {
        this.props.poRadioValidationRequest('data')



    }
    openPoArticle(e, id, idx, value, item, index) {
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            this.onEsc()
            if (this.state.supplier != "") {
                if (value == "division") {
                    this.setState({
                        poArticle: true,
                        poArticleAnimation: true,
                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: "",
                        hl2Name: "",
                        hl3Name: "",
                        hl3Code: "",
                        basedOn: "division",
                        mrpStart: item.mrpStart,
                        mrpEnd: item.mrpEnd,
                        poArticleSearch: this.state.isModalShow ? "" : item.divisionName,
                        uuid: idx,
                        focusId: idx

                    })
                    let payload = {
                        pageNo: 1,
                        type: this.state.isModalShow || item.divisionName == "" ? 1 : 3,
                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: "",
                        hl2Name: "",
                        hl3Name: "",
                        hl3Code: "",
                        divisionSearch: this.state.isModalShow ? "" : item.divisionName,
                        sectionSearch: "",
                        departmentSearch: "",
                        articleSearch: "",
                        basedOn: "division",
                        supplier: this.state.slCode

                    }
                    this.props.poArticleRequest(payload)


                } else if (value == "section") {
                    this.setState({
                        poArticle: true,
                        poArticleAnimation: true,
                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: item.divisionName,
                        hl2Name: "",
                        hl3Name: "",
                        hl3Code: "",
                        basedOn: "section",
                        mrpStart: item.mrpStart,
                        mrpEnd: item.mrpEnd,
                        poArticleSearch: this.state.isModalShow ? "" : item.sectionName,
                        uuid: idx,
                        focusId: idx


                    })
                    let payload = {
                        pageNo: 1,
                        type: this.state.isModalShow || item.sectionName == "" ? 1 : 3,

                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: item.divisionName,
                        hl2Name: "",
                        hl3Name: "",
                        hl3Code: "",
                        divisionSearch: "",
                        sectionSearch: this.state.isModalShow ? "" : item.sectionName,
                        departmentSearch: "",
                        articleSearch: "",
                        basedOn: "section",
                        supplier: this.state.slCode
                    }
                    this.props.poArticleRequest(payload)
                } else if (value == "department") {
                    this.setState({
                        poArticle: true,
                        poArticleAnimation: true,
                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: item.divisionName,
                        hl2Name: item.sectionName,
                        hl3Name: "",
                        hl3Code: "",
                        basedOn: "department",
                        mrpStart: item.mrpStart,
                        mrpEnd: item.mrpEnd,
                        poArticleSearch: this.state.isModalShow ? "" : item.departmentName,
                        uuid: idx,
                        focusId: idx


                    })
                    let payload = {
                        pageNo: 1,
                        type: this.state.isModalShow || item.departmentName == "" ? 1 : 3,

                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: item.divisionName,
                        hl2Name: item.sectionName,
                        hl3Name: "",
                        hl3Code: "",
                        divisionSearch: "",
                        sectionSearch: "",
                        departmentSearch: this.state.isModalShow ? "" : item.departmentName,
                        articleSearch: "",
                        basedOn: "department",
                        supplier: this.state.slCode
                    }
                    this.props.poArticleRequest(payload)
                } else if (value == "articleCode") {
                    this.setState({
                        poArticle: true,
                        poArticleAnimation: true,
                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: item.divisionName,
                        hl2Name: item.sectionName,
                        hl3Name: "",
                        hl3Code: "",
                        basedOn: "article",
                        mrpStart: item.mrpStart,
                        mrpEnd: item.mrpEnd,
                        poArticleSearch: this.state.isModalShow ? "" : item.articleCode,
                        uuid: idx,
                        focusId: idx


                    })
                    let payload = {
                        pageNo: 1,
                        type: this.state.isModalShow || item.articleCode == "" ? 1 : 3,

                        hl4Code: "",
                        hl4Name: "",
                        hl1Name: item.divisionName,
                        hl2Name: item.sectionName,
                        hl3Name: item.departmentName,
                        hl3Code: item.departmentCode,
                        divisionSearch: "",
                        sectionSearch: "",
                        departmentSearch: "",
                        articleSearch: this.state.isModalShow ? "" : item.articleCode,
                        basedOn: "article",
                        supplier: this.state.slCode
                    }
                    this.props.poArticleRequest(payload)
                }
                document.getElementById(idx).focus()
                this.setState({
                    selectedRowId: id
                })
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            } else {
                this.setState({
                    errorMassage: "Supplier is compulsory",
                    poErrorMsg: true,
                    focusId: "vendor"

                })
            }
        }
    }
    closePOArticle() {
        this.setState({
            poArticle: false,
            poArticleAnimation: false,
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById(this.state.focusId).focus()

    }
    updateMrpState(data) {

        let poRows = [...this.state.poRows]
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                poRows[i].articleCode = data.articleCode,
                    poRows[i].articleName = data.articleName,
                    poRows[i].divisionCode = data.divisionCode,
                    poRows[i].divisionName = data.division,
                    poRows[i].sectionCode = data.sectionCode,
                    poRows[i].sectionName = data.section,
                    poRows[i].departmentCode = data.departmentCode,
                    poRows[i].departmentName = data.department,
                    poRows[i].mrpStart = data.mrpStart,
                    poRows[i].mrpEnd = data.mrpEnd,
                    poRows[i].itemCodeList = [],
                    poRows[i].itemCodeSearch = "",
                    poRows[i].color = [],
                    poRows[i].size = [],
                    poRows[i].ratio = [],
                    poRows[i].vendorMrp = "",
                    poRows[i].vendorDesign = "",
                    poRows[i].mrk = [],
                    poRows[i].discount = {
                        discountType: "",
                        discountValue: "",
                        discountPer: false
                    },

                    poRows[i].finalRate = "",
                    poRows[i].rate = "",
                    poRows[i].netRate = "",
                    poRows[i].rsp = "",
                    poRows[i].mrp = "",
                    poRows[i].quantity = "",
                    poRows[i].amount = "",
                    poRows[i].otb = "",
                    poRows[i].remarks = "",
                    poRows[i].gst = [],
                    poRows[i].finCharges = [],

                    poRows[i].tax = [],
                    poRows[i].calculatedMargin = [],
                    poRows[i].gridOneId = poRows[i].gridOneId,
                    poRows[i].deliveryDate = "",

                    poRows[i].marginRule = "",
                    poRows[i].image = [],
                    poRows[i].imageUrl = {},
                    poRows[i].containsImage = false,
                    //new

                    poRows[i].hsnCode = "",
                    poRows[i].hsnSacCode = "",

                    poRows[i].catDescHeader = [],
                    poRows[i].catDescArray = [],
                    poRows[i].itemUdfHeader = [],
                    poRows[i].itemUdfArray = [],
                    poRows[i].lineItem = [{

                        colorChk: false,
                        icodeChk: false,
                        colorSizeList: [],
                        colorSearch: "",
                        icodes: [],
                        itemBarcode: "",
                        setHeaderId: "",
                        gridTwoId: 1,
                        color: [],
                        colorList: [],
                        sizeSearch: "",
                        sizes: [],
                        sizeList: [],
                        ratio: [],
                        size: "",
                        setRatio: "",
                        option: "",
                        setNo: 1,
                        total: "",
                        setQty: this.state.isSet ? "" : 1,
                        quantity: "",
                        amount: "",
                        rate: "",

                        gst: "",
                        finCharges: [],
                        tax: "",
                        otb: "",
                        calculatedMargin: "",
                        mrk: "",
                        setUdfHeader: [],
                        setUdfArray: [],
                        lineItemChk: false

                    }]




            }
        }
        this.setState({

            poRows: poRows
        })
        document.getElementById(this.state.focusId).focus()
        if (data.departmentCode != "") {

            let hsnData = {
                rowId: "",
                code: data.departmentCode,
                no: 1,
                type: 1,
                search: ""
            }
            this.props.hsnCodeRequest(hsnData)
            let marginData = {
                slCode: this.state.slCode,
                articleCode: data.articleCode,
                siteCode: this.state.siteCode,
                rId: ""
            }
            this.props.marginRuleRequest(marginData)
            let Payload = {
                hl3Code: data.departmentCode,
                hl3Name: data.department,
            }
            this.props.getItemDetailsRequest(Payload)

            if (this.state.isUDFExist == "true") {

                let udfdata = {

                    type: 1,
                    search: "",
                    isCompulsary: "",
                    displayName: "",
                    udfType: "",
                    ispo: true
                }
                this.props.getUdfMappingRequest(udfdata);

            }
            if (this.state.itemUdfExist == "true") {
                let uData = {
                    hl3Code: data.departmentCode,
                    ispo: true,
                    hl3Name: data.department

                }
                this.props.getCatDescUdfRequest(uData)
            }
        }
        setTimeout(() => {

            if (!this.state.isColorRequired) {
                let poRows = [...this.state.poRows]
                let color = []
                let colorObj = {
                    id: 1,
                    code: "",
                    cname: "NA"

                }
                color.push(colorObj)
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.state.selectedRowId) {
                        for (let j = 0; j < poRows[i].lineItem.length; j++) {
                            poRows[i].lineItem[j].color = ["NA"],
                                poRows[i].lineItem[j].colorList = color,
                                poRows[i].lineItem[j].option = 1
                        }
                    }

                }
                this.setState({
                    poRows
                })
            }


        }, 10)

    }

    updatePoAmount() {
        let c = [...this.state.poRows]
        let amount = 0;
        for (var x = 0; x < c.length; x++) {
            amount += c[x].amount;
        }
        this.setState({
            poAmount: amount
        })
    }
    updateNetAmt(id, designRow) {
        let poRows = [...this.state.poRows]
        let quantity = 0
        let rate = ""
        let rsp = ""
        let finalRate = ""
        for (let i = 0; i < poRows.length; i++) {
            quantity = quantity + Number(poRows[i].quantity)

        }

        // let otbArray =this.state.otbArray
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].gridOneId == this.state.selectedRowId) {

                rate = poRows[j].rate,

                    rsp = this.state.isRspRequired ? this.state.isRSP ? poRows[j].rsp : poRows[j].vendorMrp : 0
                finalRate = poRows[j].finalRate
            }
        }
        if (this.state.isRspRequired) {
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.state.selectedRowId) {
                    for (let j = 0; j < poRows[i].lineItem.length; j++) {
                        if (poRows[i].lineItem[j].gridTwoId == id) {


                            let intakedata = {
                                rowId: id,
                                rate: finalRate,
                                rsp: rsp,

                                gst: poRows[i].lineItem[j].gst,
                                designRow: this.state.selectedRowId

                            }
                            this.setState({
                                markUpYes: true
                            })
                            this.props.markUpRequest(intakedata)


                        }
                    }
                }


            }

        }

        this.setState({
            poQuantity: quantity
        })




    }
    resetPoRows() {
        this.setState({


            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: false
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",

                marginRule: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: this.state.isSet ? "" : 1,
                    quantity: "",
                    amount: "",
                    rate: "",

                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: []

                }]

            }]
        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.gen_IndentBased.isSuccess) {
            this.setState({
                poRows: []
            })
            if (nextProps.purchaseIndent.gen_IndentBased.data.resource != null) {

                this.setState({
                    piKeyData: nextProps.purchaseIndent.gen_IndentBased.data.resource,


                })
                this.updatePoData(nextProps.purchaseIndent.gen_IndentBased.data.resource);
            }

        }
        if (nextProps.purchaseIndent.gen_SetBased.isSuccess) {
            this.setState({
                poRows: []
            })
            if (nextProps.purchaseIndent.gen_SetBased.data.resource != null) {

                this.setState({
                    piKeyData: nextProps.purchaseIndent.gen_SetBased.data.resource,


                })
                this.updatePoData(nextProps.purchaseIndent.gen_SetBased.data.resource);
            }

        }

        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
            // if(nextProps.purchaseIndent.poRadioValidation.data.resource!={}){

            this.setState({
                isAdhoc: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isAdhoc != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isAdhoc : false,
                isIndent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isIndent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isIndent : false,
                isSetBased: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSetBased != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSetBased : false,
                isHoldPo: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isHoldPo != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isHoldPo : false,
                isPOwithICode: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isPOwithICode != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isPOwithICode : false,
                poWithUpload: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload : false,
                isCityExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck : false,
                isMRPEditable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMRPEditable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMRPEditable : false,
                isDiscountAvail: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountAvail != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountAvail : false,
                isDiscountMap: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap : false,
                isRSP: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRSP != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload : false,
                displayOtb: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb : true,
                transporterValidation: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation : true,
                addRow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow : true,

                cat1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 : false,
                cat2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 : false,
                cat3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 : false,
                cat4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 : false,
                cat5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 : false,
                cat6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 : false,
                desc1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 : false,
                desc2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 : false,
                desc3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 : false,
                desc4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 : false,
                desc5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 : false,
                desc6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 : false,

                itemudf1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 : false,
                itemudf2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 : false,
                itemudf3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 : false,
                itemudf4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 : false,
                itemudf5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 : false,
                itemudf6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 : false,
                itemudf7Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 : false,
                itemudf8Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 : false,
                itemudf9Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 : false,
                itemudf10Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 : false,
                itemudf11Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 : false,
                itemudf12Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 : false,
                itemudf13Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 : false,
                itemudf14Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 : false,
                itemudf15Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 : false,
                itemudf16Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 : false,
                itemudf17Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 : false,
                itemudf18Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 : false,
                itemudf19Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 : false,
                itemudf20Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 : false,

                udf1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 : false,
                udf2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 : false,
                udf3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 : false,
                udf4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 : false,
                udf5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 : false,
                udf6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 : false,
                udf7Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 : false,
                udf8Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 : false,
                udf9Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 : false,
                udf10Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 : false,

                udf11Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 : false,
                udf12Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 : false,
                udf13Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 : false,
                udf14Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 : false,
                udf15Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 : false,
                udf16Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 : false,
                udf17Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 : false,
                udf18Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 : false,
                udf19Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 : false,
                udf20Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 : false,



                restrictedStartDate: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate : "",
                restrictedEndDate: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate : "",
                isRequireSiteId: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId : false,
                isVendorDesignNotReq: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq : false,

                isMrpRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpReq : true,
                isRspRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRspReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRspReq : true,
                isColorRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq : true,
                isDisplayFinalRate: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayFinalRate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayFinalRate : false,
                isTransporterDependent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent : true,
                isDisplayMarginRule: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule : true,
                isTaxDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable : false,
                isGSTDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable : false,
                isBaseAmountActive: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive : false,
                isOtbValidationPi: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isOtbValidationPi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isOtbValidationPi ? "true" : "false" : "false",
                isSiteExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSiteExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSiteExist ? "true" : "false" : "false",
                itemUdfExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isItemUdfExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isItemUdfExist ? "true" : "false" : "false",
                copyColor: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCopyColor != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCopyColor ? "true" : "false" : "false",
                isModalshow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isModalshow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isModalshow : "false",
                isUDFExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isUdfExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isUdfExist ? "true" : "false" : "false",

                isMrpRangeDisplay: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpRangeDisplay != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpRangeDisplay : true,
                isMarginRulePi: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMarginRulePi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMarginRulePi : false

            })
        }

        // }










        if (nextProps.purchaseIndent.getItemDetails.isSuccess) {
            let poUdfData = []
            let c = []
            if (nextProps.purchaseIndent.getItemDetails.data.essentialProParam != undefined) {
                this.setState({
                    cat1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 : this.state.cat1Validation,
                    cat2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 : this.state.cat2Validation,
                    cat3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 : this.state.cat3Validation,
                    cat4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 : this.state.cat4Validation,
                    cat5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 : this.state.cat5Validation,
                    cat6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 : this.state.cat6Validation,
                    desc1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 : this.state.desc1Validation,
                    desc2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 : this.state.desc2Validation,
                    desc3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 : this.state.desc3Validation,
                    desc4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 : this.state.desc4Validation,
                    desc5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 : this.state.desc5Validation,
                    desc6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 : this.state.desc6Validation,

                    itemudf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 : this.state.itemudf1Validation,
                    itemudf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 : this.state.itemudf2Validation,
                    itemudf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 : this.state.itemudf3Validation,
                    itemudf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 : this.state.itemudf4Validation,
                    itemudf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 : this.state.itemudf5Validation,
                    itemudf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 : this.state.itemudf6Validation,
                    itemudf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 : this.state.itemudf7Validation,
                    itemudf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 : this.state.itemudf8Validation,
                    itemudf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 : this.state.itemudf9Validation,
                    itemudf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 : this.state.itemudf10Validation,
                    itemudf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 : this.state.itemudf11Validation,
                    itemudf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 : this.state.itemudf12Validation,
                    itemudf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 : this.state.itemudf13Validation,
                    itemudf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 : this.state.itemudf14Validation,
                    itemudf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 : this.state.itemudf15Validation,
                    itemudf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 : this.state.itemudf16Validation,
                    itemudf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 : this.state.itemudf17Validation,
                    itemudf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 : this.state.itemudf18Validation,
                    itemudf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 : this.state.itemudf19Validation,
                    itemudf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 : this.state.itemudf20Validation,

                    udf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 : this.state.udf1Validation,
                    udf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 : this.state.udf2Validation,
                    udf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 : this.state.udf3Validation,
                    udf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 : this.state.udf4Validation,
                    udf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 : this.state.udf5Validation,
                    udf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 : this.state.udf6Validation,
                    udf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 : this.state.udf7Validation,
                    udf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 : this.state.udf8Validation,
                    udf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 : this.state.udf9Validation,
                    udf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 : this.state.udf10Validation,

                    udf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 : this.state.udf11Validation,
                    udf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 : this.state.udf12Validation,
                    udf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 : this.state.udf13Validation,
                    udf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 : this.state.udf14Validation,
                    udf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 : this.state.udf15Validation,
                    udf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 : this.state.udf16Validation,
                    udf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 : this.state.udf17Validation,
                    udf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 : this.state.udf18Validation,
                    udf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 : this.state.udf19Validation,
                    udf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 : this.state.udf20Validation,



                })
            }
            if (nextProps.purchaseIndent.getItemDetails.data.resource != null) {
                poUdfData = nextProps.purchaseIndent.getItemDetails.data.resource
                let poRows = [...this.state.poRows]
                let c = []
                for (let j = 0; j < poUdfData.length; j++) {
                    let x = poUdfData[j]
                    x.value = "";
                    x.code = ""
                    let a = x
                    c.push(a)

                }
                let obj = [{ catDescId: 1, catDesc: c }]

                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.state.selectedRowId) {
                        poRows[i].catDescHeader = nextProps.purchaseIndent.getItemDetails.data.resource
                        poRows[i].catDescArray = obj

                    }
                }
                let poRowss = _.map(
                    _.uniq(
                        _.map(poRows, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                this.setState({
                    poRows: poRowss
                })





            } else {
                this.setState({

                    poErrorMsg: true,
                    errorMassage: "Categories and Description are not available corresponding department"
                })
            }
        }

        if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {

            if (nextProps.purchaseIndent.getUdfMapping.data.resource != null) {
                let udfMappingData = nextProps.purchaseIndent.getUdfMapping.data.resource
                let c = []
                for (let j = 0; j < udfMappingData.length; j++) {
                    let x = udfMappingData[j]
                    x.value = "";
                    let a = x
                    c.push(a)
                }
                let poRows = [...this.state.poRows]
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.state.selectedRowId) {
                        poRows[i].lineItem[0].setUdfHeader = nextProps.purchaseIndent.getUdfMapping.data.resource
                        poRows[i].lineItem[0].setUdfArray = c
                    }
                }

                let poRowss = _.map(
                    _.uniq(
                        _.map(poRows, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                this.setState({
                    poRows: poRowss
                })


            }
        }

        if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {

            if (nextProps.purchaseIndent.getCatDescUdf.data.resource != null) {
                let poUdfData = []
                let c = []
                poUdfData = nextProps.purchaseIndent.getCatDescUdf.data.resource.cat_desc_udf
                for (let j = 0; j < poUdfData.length; j++) {

                    poUdfData[j].value = "";

                    c.push(poUdfData[j])
                }
                let poRows = [...this.state.poRows]
                let obj = [{ itemUdfId: 1, itemUdf: c }]
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.state.selectedRowId) {
                        poRows[i].itemUdfHeader = nextProps.purchaseIndent.getCatDescUdf.data.resource.cat_desc_udf
                        poRows[i].itemUdfArray = obj
                    }

                }
                let poRowss = _.map(
                    _.uniq(
                        _.map(poRows, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                this.setState({
                    poRows: poRowss
                })




            }

        }
        if (nextProps.purchaseIndent.gen_PurchaseIndent.isSuccess) {

            this.onClear();
            this.setBasedResest();
            this.setState({
                loader: false,
                currentDate: getDate()
            })


        } else if (nextProps.purchaseIndent.gen_PurchaseIndent.isError) {
            this.setState({
                loader: false
            })

        }

        if (nextProps.purchaseIndent.loadIndent.isSuccess) {

            this.setState({
                loadData: nextProps.purchaseIndent.loadIndent.data.resource
            })

        }

        if (nextProps.purchaseIndent.leadTime.isSuccess) {
            this.setState({
                leadDays: nextProps.purchaseIndent.leadTime.data.resource.leadTime
            })
        }


        if (nextProps.purchaseIndent.markUp.isSuccess) {
            if (nextProps.purchaseIndent.markUp.data.resource != null) {


                let poRows = [...this.state.poRows]

                for (var x = 0; x < poRows.length; x++) {
                    if (poRows[x].gridOneId == this.state.selectedRowId) {
                        for (let y = 0; y < poRows[x].lineItem.length; y++) {

                            if (poRows[x].lineItem[y].gridTwoId == nextProps.purchaseIndent.markUp.data.rowId) {
                                poRows[x].lineItem[y].mrk = nextProps.purchaseIndent.markUp.data.resource.actualMarkUp
                                poRows[x].lineItem[y].calculatedMargin = nextProps.purchaseIndent.markUp.data.resource.calculatedMargin
                                poRows[x].lineItem[y].lineItemChk = false
                            }
                        }


                    }
                }

                this.setState({
                    poRows: poRows
                }, () => {

                    this.updatePo()
                    setTimeout(() => {

                        this.gridFirst();
                    }, 10)


                })


            }
            this.setState({
                markUpYes: false
            })

            this.setState({
                lineItemChange: false
            })

        }

        if (nextProps.purchaseIndent.lineItem.isSuccess) {
            if (nextProps.purchaseIndent.lineItem.data.resource != null) {


                let rows = [...this.state.poRows]
                for (let x = 0; x < rows.length; x++) {
                    if (rows[x].gridOneId == this.state.selectedRowId) {
                        for (let y = 0; y < rows[x].lineItem.length; y++) {
                            if (rows[x].lineItem[y].gridTwoId == nextProps.purchaseIndent.lineItem.data.rowId) {

                                rows[x].lineItem[y].amount = Math.round(nextProps.purchaseIndent.lineItem.data.resource.netAmount * 100) / 100
                                rows[x].lineItem[y].gst = nextProps.purchaseIndent.lineItem.data.resource.gst
                                rows[x].lineItem[y].tax = nextProps.purchaseIndent.lineItem.data.resource.tax
                                rows[x].lineItem[y].finCharges = nextProps.purchaseIndent.lineItem.data.resource.finCharges



                            }
                        }
                    }

                }

                this.setState({
                    poRows: rows,

                }, () => {

                    this.updateNetAmt(nextProps.purchaseIndent.lineItem.data.rowId, nextProps.purchaseIndent.lineItem.data.designRow)
                    this.updatePoAmount();
                    setTimeout(() => {

                        this.updatePo()
                    }, 10)


                })



                !this.state.isRspRequired ? this.setState({
                    lineItemChange: false
                }) : null

            }

            this.props.lineItemClear();
        } else if (nextProps.purchaseIndent.lineItem.isError) {


            this.setState({
                lineItemChange: false
            })

            let poRows = [...this.state.poRows]
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.state.selectedRowId) {
                    for (let j = 0; j < poRows[i].lineItem.length; j++) {
                        if (poRows[i].lineItem[j].gridTwoId == nextProps.purchaseIndent.lineItem.message.rowId) {
                            poRows[i].lineItem[j].lineItemChk = true
                        }
                    }
                }
            }



            this.setState({
                poRows: poRows
            })


        }
        if (nextProps.purchaseIndent.poItemcode.isSuccess) {
            if (nextProps.purchaseIndent.poItemcode.data.resource != null) {

                let c = []
                for (let i = 0; i < nextProps.purchaseIndent.poItemcode.data.resource.length; i++) {


                    let x = nextProps.purchaseIndent.poItemcode.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    poItemcodeData: c
                })
            }
        }


        if (nextProps.purchaseIndent.color.isSuccess) {
            if (nextProps.purchaseIndent.color.data.resource != null) {
                this.setState({
                    colorNewData: nextProps.purchaseIndent.color.data.resource
                })
            }
        }
        if (nextProps.purchaseIndent.getMultipleMargin.isSuccess) {
            let lineItem = nextProps.purchaseIndent.getMultipleMargin.data.resource.multipleMarkup
            let poRows = [...this.state.poRows]

            for (let i = 0; i < lineItem.length; i++) {
                for (let j = 0; j < poRows.length; j++) {
                    if (poRows[j].gridOneId == lineItem[i].designRowid) {
                        for (let k = 0; k < poRows[j].lineItem.length; k++) {
                            if (poRows[j].lineItem[k].gridTwoId == lineItem[i].rowId) {
                                poRows[j].lineItem[k].mrk = lineItem[i].actualMarkUp
                                poRows[j].lineItem[k].calculatedMargin = lineItem[i].calculatedMargin

                            }
                        }
                    }


                }
            }
            this.setState({
                poRows: poRows,
                lineItemChange: false
            }, () => {

                this.updatePo()
            })

        }

        if (nextProps.purchaseIndent.multipleLineItem.isSuccess) {

            let lineItem = nextProps.purchaseIndent.multipleLineItem.data.resource.multipleLineItem
            let poRows = [...this.state.poRows]
            let designId = lineItem[0].designRowid
            for (let i = 0; i < lineItem.length; i++) {
                for (let j = 0; j < poRows.length; j++) {
                    if (poRows[j].gridOneId == designId) {
                        for (let k = 0; k < poRows[j].lineItem.length; k++) {
                            if (poRows[j].lineItem[k].gridTwoId == lineItem[i].rowId) {
                                poRows[j].lineItem[k].amount = Math.round(lineItem[i].netAmount * 100) / 100
                                poRows[j].lineItem[k].gst = lineItem[i].gst
                                poRows[j].lineItem[k].tax = lineItem[i].tax
                                poRows[j].lineItem[k].finCharges = lineItem[i].finCharges
                                poRows[j].lineItem[k].lineItemChk = false
                            }
                        }
                    }


                }
            }
            this.setState({
                poRows: poRows
            }, () => {

                this.updatePo()
                setTimeout(() => {


                    this.state.isRspRequired ? this.multiMarkUp(designId) : null
                    !this.state.isRspRequired ? this.setState({
                        lineItemChange: false
                    }) : null
                }, 10)
            })




        } else if (nextProps.purchaseIndent.multipleLineItem.isError) {
            let focusedObj = this.state.focusedObj
            if (focusedObj.type == "rate") {
                let poRows = [...this.state.poRows]
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == focusedObj.rowId) {
                        poRows[i].rate = focusedObj.value
                        poRows[i].finalRate = focusedObj.finalRate
                    }
                }


                this.setState({
                    poRows: poRows,
                    focusedObj: {
                        type: "",
                        rowId: "",
                        cname: "",
                        value: "",
                        radio: "",
                        finalRate: "",
                        colorObj: {
                            color: [],
                            colorList: []
                        }

                    }
                })
            }



        }

        if (nextProps.purchaseIndent.otb.isSuccess) {
            if (nextProps.purchaseIndent.otb.data.resource != null) {
                // let c = this.state.poRowsfor (var x = 0; x < c.length; x++) { if (c[x].gridOneId == nextProps.purchaseIndent.otb.data.rowId) {
                //     c[x].otb = nextProps.purchaseIndent.otb.data.resource.otbthis.setState({    poRows: c})}}
                if (this.state.isOtbValidationPi == "true") {
                    if (nextProps.purchaseIndent.otb.data.resource.otb.toString() == "0") {
                        this.setState({
                            poErrorMsg: true,
                            errorMassage: "OTB is zero for the corresponding Article and MRP"
                        })
                    }
                }
                const t = this
                setTimeout(function () {
                    if (t.state.changeLastIndate) {

                        t.getOtbLastInDate(nextProps.purchaseIndent.otb.data.rowId, nextProps.purchaseIndent.otb.data.resource.otb)
                        t.gridFirst();
                    } else {

                        t.gridFirst();
                        t.getOpenToBuy(nextProps.purchaseIndent.otb.data.rowId, nextProps.purchaseIndent.otb.data.resource.otb);
                    }
                }, 1)
            }
            this.props.otbClear();
        }
        if (nextProps.purchaseIndent.marginRule.isSuccess) {
            if (nextProps.purchaseIndent.marginRule.data.resource != null) {
                let rows = [...this.state.poRows]
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].gridOneId == this.state.selectedRowId) {
                        rows[i].marginRule = nextProps.purchaseIndent.marginRule.data.resource
                        this.setState({
                            poRows: rows,
                            saveMarginRule: nextProps.purchaseIndent.marginRule.data.resource
                        })
                    }
                }
            }
            else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Margin rule is not available for the corresponding Article and Supplier"
                })
            }
            this.props.marginRuleClear()
        }

        if (nextProps.purchaseIndent.multipleOtb.isSuccess) {
            let poRows = [...this.state.poRows]
            if (nextProps.purchaseIndent.multipleOtb.data.resource != null) {
                let multipleOtb = nextProps.purchaseIndent.multipleOtb.data.resource
                for (let i = 0; i < multipleOtb.length; i++) {
                    for (let j = 0; j < poRows.length; j++) {
                        if (multipleOtb[i].rowId == poRows[j].gridOneId) {
                            poRows[j].lineItem[0].otb = multipleOtb[i].otb
                            poRows[j].otb = multipleOtb[i].otb

                        }
                    }
                }
            }
            this.setState({
                poRows: poRows
            }, () => {
                if (this.state.changeLastIndate) {
                    this.updateMultipleOtb()
                }
            })

        }
    }

    createSet(id, item) {
        this.setState({
            selectedRowId: id,
            setModal: true,
            setModalAnimation: true,
            departmentCode: item.departmentCode,
            department: item.departmentName,
            articleName: item.articleName,
            articleCode: item.articleCode,
            divisionName: item.divisionName,
            sectionName: item.sectionName




        })


    }
    closeSetModal() {
        this.setState({
            setModal: false,
            setModalAnimation: false,
        })
    }
    updatePoRows(data) {
        this.setState({
            poRows: data
        })

    }

    gridFirst() {
        let poRows = [...this.state.poRows]

        let flag = false
        poRows.forEach(po => {
            flag = (po.vendorMrp == "" && this.state.isMrpRequired) || (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) || po.rate == "" || po.marginRule == "" && this.state.isMarginRulePi || po.hsnSacCode == ""
                || (po.mrp == "" && this.state.isMRPEditable) || po.deliveryDate == "" ? true : false
        })
        if (flag) {
            this.setState({
                gridFirst: false
            })
        } else {
            this.setState({
                gridFirst: true
            })
        }
    }
    gridSecond() {
        let poRows = [...this.state.poRows]
        let flag = false
        for (let i = 0; i < poRows.length; i++) {


            for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                    if ((poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT1" && ((this.state.cat1Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT2" && ((this.state.cat2Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT3" && ((this.state.cat3Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT4" && ((this.state.cat4Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT5" && ((this.state.cat5Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT6" && ((this.state.cat6Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC1" && ((this.state.desc1Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC2" && ((this.state.desc2Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC3" && ((this.state.desc3Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC4" && ((this.state.desc4Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC5" && ((this.state.desc5Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC6" && ((this.state.desc6Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "")) {
                        flag = true

                        break
                    }


                }
            }
        }
        if (flag) {
            this.setState({
                gridSecond: false
            })
        } else {
            this.setState({
                gridSecond: true
            })
        }
    }

    gridThird() {
        let poRows = [...this.state.poRows]
        let flag = true
        for (var i = 0; i < poRows.length; i++) {
            for (var j = 0; j < poRows[i].itemUdfArray.length; j++) {

                if ((poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING01" && ((this.state.itemudf1Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING02" && ((this.state.itemudf2Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING03" && ((this.state.itemudf3Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING04" && ((this.state.itemudf4Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING05" && ((this.state.itemudf5Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING06" && ((this.state.itemudf6Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING07" && ((this.state.itemudf7Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING08" && ((this.state.itemudf8Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING09" && ((this.state.itemudf9Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING10" && ((this.state.itemudf10Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM01" && ((this.state.itemudf11Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM02" && ((this.state.itemudf12Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM03" && ((this.state.itemudf13Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM04" && ((this.state.itemudf14Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM05" && ((this.state.itemudf15Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE01" && ((this.state.itemudf16Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE02" && ((this.state.itemudf17Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE04" && ((this.state.itemudf19Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE05" && ((this.state.itemudf20Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "")) {
                    flag = false
                    break
                }

            }
        }
        if (flag) {
            this.setState({
                gridThird: true
            })
        } else {
            this.setState({
                gridThird: false
            })
        }
    }
    gridFourth() {

        let poRows = [...this.state.poRows]
        let flag = true
        poRows.forEach(po => {
            po.lineItem.forEach(cd => {
                if (cd.color.length == 0 || cd.setQty == "" || cd.setQty.toString() == "0" || cd.setQty == 0) {

                    flag = false
                }
            })

        })

        if (flag) {
            this.setState({
                gridFourth: true
            })
        } else {
            this.setState({
                gridFourth: false
            })
        }

    }

    gridFivth() {

        let poRows = [...this.state.poRows]
        let flag = true
        for (let i = 0; i < poRows.length; i++) {
            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {

                    if ((poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING01" && ((this.state.udf1Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING02" && ((this.state.udf2Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING03" && ((this.state.udf3Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING04" && ((this.state.udf4Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING05" && ((this.state.udf5Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING06" && ((this.state.udf6Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING07" && ((this.state.udf7Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING08" && ((this.state.udf8Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING09" && ((this.state.udf9Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING10" && ((this.state.udf10Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM01" && ((this.state.udf11Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM02" && ((this.state.udf12Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM03" && ((this.state.udf13Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM04" && ((this.state.udf14Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM05" && ((this.state.udf15Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE01" && ((this.state.udf16Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE02" && ((this.state.udf17Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE03" && ((this.state.udf18Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE04" && ((this.state.udf19Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE05" && ((this.state.udf20Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "")) {
                        flag = false

                        break
                    }
                }
            }
        }

        if (flag) {
            this.setState({
                gridFivth: true
            })
        } else {
            this.setState({
                gridFivth: false
            })
        }
    }

    validateCatDescRow() {
        let poRows = [...this.state.poRows]
        let type = ""
        for (let i = 0; i < poRows.length; i++) {


            for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                    if ((poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT1" && ((this.state.cat1Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT2" && ((this.state.cat2Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT3" && ((this.state.cat3Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT4" && ((this.state.cat4Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT5" && ((this.state.cat5Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT6" && ((this.state.cat6Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC1" && ((this.state.desc1Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC2" && ((this.state.desc2Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC3" && ((this.state.desc3Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC4" && ((this.state.desc4Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC5" && ((this.state.desc5Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC6" && ((this.state.desc6Validation == true || poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "")) {


                        type = typeof (poRows[i].catDescArray[j].catDesc[k].displayName) == "string" && poRows[i].catDescArray[j].catDesc[k].displayName != "null" ? poRows[i].catDescArray[j].catDesc[k].displayName : poRows[i].catDescArray[j].catDesc[k].catdesc
                        break
                    }


                }
            }
        }
        if (type != "") {
            this.setState({
                poErrorMsg: true,
                errorMassage: type + " is compulsory"
            })
        }
    }

    validateItemdesc() {

        let poRows = [...this.state.poRows]
        poRows.forEach(po => {
            if (po.vendorMrp == "" && this.state.isMrpRequired) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "MRP is compulsory"
                })


            }

            else if (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor Design is compulsory"
                })

            }
            else if (po.hsnSacCode == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "HSN  is compulsory"
                })

            }
            else if (po.mrp == "" && this.state.isMRPEditable) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Mrp is compulsory"
                })

            }

            else if (po.rate == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Rate is compulsory"
                })


            } else if (po.deliveryDate == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Delivery date is compulsory"
                })


            } else if (po.marginRule == "" && this.state.isMarginRulePi) {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Margin Rule is compulsory"
                })
            }

        })
    }
    validateUdf() {
        let poRows = [...this.state.poRows]
        var cat_desc_udf = ""

        for (var i = 0; i < poRows.length; i++) {
            for (var j = 0; j < poRows[i].itemUdfArray.length; j++) {

                if ((poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING01" && ((this.state.itemudf1Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING02" && ((this.state.itemudf2Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING03" && ((this.state.itemudf3Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING04" && ((this.state.itemudf4Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING05" && ((this.state.itemudf5Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING06" && ((this.state.itemudf6Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING07" && ((this.state.itemudf7Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING08" && ((this.state.itemudf8Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING09" && ((this.state.itemudf9Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFSTRING10" && ((this.state.itemudf10Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM01" && ((this.state.itemudf11Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM02" && ((this.state.itemudf12Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM03" && ((this.state.itemudf13Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM04" && ((this.state.itemudf14Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFNUM05" && ((this.state.itemudf15Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE01" && ((this.state.itemudf16Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE02" && ((this.state.itemudf17Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE04" && ((this.state.itemudf19Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == "") ||
                    (poRows[i].itemUdfArray[j].cat_desc_udf == "UDFDATE05" && ((this.state.itemudf20Validation == true || poRows[i].itemUdfArray[j].isLovPI == "Y") && poRows[i].itemUdfArray[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[j].value == ""))
                // if (poRows[i].itemUdfArray[j].isCompulsoryPI == "Y" && poRows[i].itemUdfArray[j].value == "")
                {

                    cat_desc_udf = poRows[i].itemUdfArray[j].displayName != null ? poRows[i].itemUdfArray[j].displayName : poRows[i].itemUdfArray[j].cat_desc_udf
                    break
                }

            }
        }

        if (cat_desc_udf != "") {
            this.setState({
                poErrorMsg: true,
                errorMassage: cat_desc_udf + " is compulsory"
            })
        }
    }
    validateLineItem() {
        let poRows = [...this.state.poRows]
        console.log(poRows)

        poRows.forEach(po => {
            po.lineItem.forEach(sec => {


                if (sec.setQty == "" || sec.setQty.toString() == "0" || sec.setQty == 0) {

                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Set quantity is compulsory"
                    })
                }

                else if (sec.color.length == 0) {

                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Color is compulsory"
                    })


                }
            })

        })
    }
    //_______________________________________validate set udf___________________________

    validateSetUdf() {

        let poRows = [...this.state.poRows]
        var displayName = ""

        for (let i = 0; i < poRows.length; i++) {
            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {

                    if ((poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING01" && ((this.state.udf1Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING02" && ((this.state.udf2Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING03" && ((this.state.udf3Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING04" && ((this.state.udf4Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING05" && ((this.state.udf5Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING06" && ((this.state.udf6Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING07" && ((this.state.udf7Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING08" && ((this.state.udf8Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING09" && ((this.state.udf9Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING10" && ((this.state.udf10Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM01" && ((this.state.udf11Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM02" && ((this.state.udf12Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM03" && ((this.state.udf13Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM04" && ((this.state.udf14Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM05" && ((this.state.udf15Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE01" && ((this.state.udf16Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE02" && ((this.state.udf17Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE03" && ((this.state.udf18Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE04" && ((this.state.udf19Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE05" && ((this.state.udf20Validation == true || poRows[i].lineItem[j].setUdfArray[k].isLovPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == ""))

                    // if (poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y" && poRows[i].lineItem[j].setUdfArray[k].value == "") 
                    {


                        displayName = poRows[i].lineItem[j].setUdfArray[k].displayName != null ? poRows[i].lineItem[j].setUdfArray[k].displayName : poRows[i].lineItem[j].setUdfArray[k].udfType
                        break
                    }
                }
            }
        }
        if (displayName != "") {
            this.setState({
                poErrorMsg: true,
                errorMassage: displayName + " is compulsory"
            })

        }
    }


    updatePoAmountNpoQuantity() {

        let poRows = [...this.state.poRows]
        let poQuantity = ""
        let poAmount = ""
        for (let j = 0; j < poRows.length; j++) {
            poAmount = Number(poAmount) + Number(poRows[j].amount)
            poQuantity = Number(poQuantity) + Number(poRows[j].quantity)
        }
        this.setState({
            poAmount: poAmount,
            poQuantity: poQuantity

        })

    }
    updateLineItemChange(data) {
        this.setState({
            lineItemChange: data
        })
    }
    getOtbForPoRows() {
        let poRows = [...this.state.poRows]
        let id = ""
        let otbb = ""
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                poRows[i].otb = poRows[i].lineItem[0].otb
            }
        }
        this.setState({
            poRows: poRows
        })
    }
    getOtbLastInDate(rowId, otb) {
        let poRows = [...this.state.poRows]
        let otbb = ""
        for (let i = 0; i < poRows.length; i++) {

            if (poRows[i].gridOneId == rowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (j == 0) {
                        poRows[i].lineItem[j].otb = otb
                        otbb = otb
                    }
                    if (j > 0) {
                        poRows[i].lineItem[j].otb = otbb - poRows[i].lineItem[j - 1].amount
                        otbb = poRows[i].lineItem[j - 1].amount
                    }
                }

            }

        }

        this.setState({
            changeLastIndate: false,
            poRows: poRows
        }, () => {

            this.getOtbForPoRows();
        })
    }
    updateMultipleOtb() {
        let poRows = [...this.state.poRows]
        let otbb = ""

        for (let i = 0; i < poRows.length; i++) {


            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                if (j == 0) {
                    poRows[i].lineItem[j].otb = poRows[i].otb
                    otbb = poRows[i].otb
                }
                if (j > 1) {
                    poRows[i].lineItem[j].otb = otbb - poRows[i].lineItem[j - 1].amount
                    otbb = poRows[i].lineItem[j - 1].amount
                }
            }

        }
        this.setState({
            changeLastIndate: false,
            poRows: poRows
        })



    }

    setBasedResest() {
        this.setState({
            setDepartment: "",
            hl3CodeDepartment: "",
            poSetVendor: "",
            orderSet: "",
        })
    }
    onClear() {
        this.setState({
            loadIndenterr: false,
            poSetVendorerr: false,
            orderSeterr: false,
            vendorerr: false,
            transportererr: false,
            siteNameerr: false,
            cityerr: false,
            typeOfBuyingErr: false,

            isSet: true,
            selectedRowId: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            hl3Code: "",
            hl4Name: "",
            hl4Code: "",
            basedOn: "",
            mrpStart: "",
            mrpEnd: "",
            leadDays: "",
            supplierCode: "",
            itemCodeList: [],
            city: "",

            saveMarginRule: "",

            slCode: "",
            loadIndentId: "",
            loadIndent: "",

            item: "",
            text: "",
            vendor: "",

            transporter: "",

            supplier: "",

            term: "",
            poQuantity: 0,
            poAmount: 0,



            siteName: "",
            siteCode: "",


            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: false
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],
                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",

                marginRule: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",

                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: []

                }]

            }]
        })

    }


    updatePo() {
        let poRows = [...this.state.poRows]


        for (let k = 0; k < poRows.length; k++) {
            poRows[k].amount = ""
            poRows[k].quantity = ""
            poRows[k].calculatedMargin = []
            poRows[k].mrk = []
            poRows[k].tax = []
            poRows[k].gst = []
            poRows[k].finCharges = []
            poRows[k].basic = ""

            poRows[k].otb = ""

        }

        for (let i = 0; i < poRows.length; i++) {

            for (let j = 0; j < poRows[i].lineItem.length; j++) {


                poRows[i].amount = Math.round((Number(poRows[i].amount) + Number(poRows[i].lineItem[j].amount)) * 100) / 100

                poRows[i].quantity = Number(poRows[i].quantity) + Number(poRows[i].lineItem[j].quantity)

                poRows[i].calculatedMargin = this.state.isRspRequired ? [...poRows[i].calculatedMargin, poRows[i].lineItem[j].calculatedMargin.toString()] : []

                poRows[i].mrk = this.state.isRspRequired ? [...poRows[i].mrk, poRows[i].lineItem[j].mrk.toString()] : []

                poRows[i].tax = [...poRows[i].tax, poRows[i].lineItem[j].tax.toString()]

                poRows[i].gst = [...poRows[i].gst, poRows[i].lineItem[j].gst.toString()]

                poRows[i].finCharges = poRows[i].finCharges.concat(poRows[i].lineItem[j].finCharges)

                poRows[i].basic = Number(poRows[i].basic) + Number(Number(poRows[i].rate) * Number(poRows[i].lineItem[j].setQty))

                poRows[i].otb = poRows[i].lineItem[0].otb




            }

        }

        this.setState({
            poRows: poRows
        }, () => {
            this.updatePoAmountNpoQuantity()
            setTimeout(() => {

                this.gridFirst()
            }, 10)



        })


    }

    multiMarkUp(designId) {
        let poRows = [...this.state.poRows]

        let multiMarkUp = []
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == designId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].setQty != "") {
                        let payload = {
                            rate: poRows[i].finalRate,
                            rsp: this.state.isRSP ? poRows[i].rsp : poRows[i].vendorMrp,
                            designRowid: designId,
                            rowId: poRows[i].lineItem[j].gridTwoId,
                            gst: poRows[i].lineItem[j].gst
                        }
                        multiMarkUp.push(payload)

                    }

                }
            }
        }
        this.props.getMultipleMarginRequest(multiMarkUp)


    }


    getOpenToBuy(id, otbb) {


        let rows = [...this.state.poRows]

        let mrp = ""
        let count = 0
        let otb = []
        let netAmount = []
        let articleCode = ""
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].gridOneId == id) {

                mrp = rows[i].vendorMrp
                articleCode = rows[i].articleCode

            }
        }

        for (var i = 0; i < rows.length; i++) {
            if (rows[i].vendorMrp == mrp && rows[i].articleCode == articleCode) {
                count++
            }
        }

        if (count > 1 && rows.length > 1) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].vendorMrp == mrp && rows[i].articleCode == articleCode) {
                    if (rows[i].otb.toString() != "") {
                        otb.push(rows[i].otb)
                    }
                    if (rows[i].amount != "") {
                        netAmount.push(rows[i].amount)
                    }
                }

            }





            let minOtb = Math.min.apply(null, otb)


            let index = ""
            let netAmtFinal = ""
            for (var k = 0; k < otb.length; k++) {
                if (otb[k] == minOtb) {
                    // index = k
                }
            }
            for (var l = 0; l < netAmount.length; l++) {
                netAmtFinal = netAmount[index]
            }


            for (var m = 0; m < rows.length; m++) {
                if (rows[m].gridOneId == id) {
                    rows[m].otb = minOtb - netAmtFinal
                }
            }


            this.setState({
                poRows: rows
            }, () => {

                this.setOtbAfterDelete(id)
            })

        }
        if (count == 1) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].vendorMrp == mrp && rows[i].articleCode == articleCode) {
                    rows[i].otb = otbb

                }
            }
            this.setState({
                poRows: rows
            })
        }
        // this.getOtbForPoRows()





    }

    copyRow(rowId, otb) {
        let poRows = [...this.state.poRows]
        let rowids = []
        let finalId = ""
        let obj = {}
        poRows.forEach(po => {
            rowids.push(po.gridOneId)

        })
        finalId = Math.max(...rowids);
        poRows.forEach(po => {
            if (po.gridOneId == rowId) {
                obj = {

                    vendorMrp: po.vendorMrp,
                    vendorDesign: po.vendorDesign,
                    mrk: po.mrk,
                    discount: po.discount,
                    finalRate: po.finalRate,
                    rate: po.rate,
                    netRate: po.netRate,
                    rsp: po.rsp,
                    mrp: po.mrp,
                    quantity: po.quantity,
                    amount: po.amount,
                    otb: po.otb,
                    remarks: po.remarks,
                    gst: po.gst,
                    finCharges: po.finCharges,
                    basic: po.basic,
                    tax: po.tax,
                    calculatedMargin: po.calculatedMargin,
                    gridOneId: finalId + 1,
                    deliveryDate: po.deliveryDate,

                    marginRule: po.marginRule,
                    image: po.image,
                    imageUrl: po.imageUrl,
                    containsImage: po.containsImage,
                    //new
                    articleCode: po.articleCode,
                    articleName: po.articleName,
                    departmentCode: po.departmentCode,
                    departmentName: po.departmentName,
                    sectionCode: po.sectionCode,
                    sectionName: po.sectionName,
                    divisionCode: po.divisionCode,
                    divisionName: po.divisionName,
                    itemCodeList: po.itemCodeList,
                    itemCodeSearch: po.itemCodeSearch,
                    hsnCode: po.hsnCode,
                    hsnSacCode: po.hsnSacCode,
                    mrpStart: po.mrpStart,
                    mrpEnd: po.mrpEnd,
                    catDescHeader: po.catDescHeader,
                    catDescArray: po.catDescArray,
                    itemUdfHeader: po.itemUdfHeader,
                    itemUdfArray: po.itemUdfArray,
                    lineItem: [...po.lineItem]

                }
            }
        })
        poRows.push(obj)
        this.setState({
            poRows,
            selectedRowId: rowId
        }, () => {
            this.getOpenToBuy(rowId, otb)
            this.updatePoAmountNpoQuantity()
        })



    }
    handleRemoveSpecificRow = (idx, otb, mrp) => () => {
        const rows = [...this.state.poRows]

        if (rows.length > 1) {
            this.setState({
                resetAndDelete: true,
                headerMsg: "Are you sure you want to delete row?",
                paraMsg: "Click confirm to continue.",
                rowDelete: "row Delete",
                rowsId: idx,
                otbValueData: otb,
                mrpValueData: mrp,
                selectedRowId: idx
            })

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })

        }

    }

    DeleteRowPo(idx, otb, mrp) {

        var rows = [...this.state.poRows]
        if (rows.length > 1) {
            for (var z = 0; z < rows.length; z++) {
                if (rows[z].gridOneId == idx) {
                    rows.splice(z, 1)
                }
            }

            this.setState({
                poRows: rows,
                deleteMrp: mrp,
                deleteOtb: otb,

            }, () => {

                this.setOtbAfterDelete(idx)
                setTimeout(() => {
                    this.updatePo()
                    this.updatePoAmount()
                    this.updatePoAmountNpoQuantity()
                }, 10)
            })

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })
        }
        // document.getElementById("addIndentRow").disabled = false;
    }
    reset() {
        this.setState({
            resetAndDelete: true,
            headerMsg: "Are you sure to reset the form?",
            paraMsg: "Click confirm to continue.",
            rowDelete: ""
        })
    }
    closeResetDeleteModal() {
        this.setState({
            resetAndDelete: !this.state.resetAndDelete,
        })
    }
    resetRows() {

        this.setState({
            codeRadio: "Adhoc"
        })

        this.onClear();
        this.setBasedResest();
    }
    closeDeleteConfirmModal(e) {
        this.setState({
            deleteConfirmModal: !this.state.deleteConfirmModal,
        })

    }
    deleteLineItem(idxx, setNo) {
        let rows = [...this.state.poRows]
        rows.forEach(ro => {
            if (ro.gridOneId == this.state.selectedRowId) {
                if (ro.lineItem.length > 1) {
                    this.setState({
                        deleteConfirmModal: true,
                        deleteGridId: idxx,
                        deleteSetNo: setNo,
                        headerMsg: "Are you sure to delete the row?",
                        paraMsg: "Click confirm to continue.",

                    })
                }
                else {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Single row can't be deleted"
                    })
                }


            }
        })

    }
    handleRemoveSpecificSecRow(idxx, setNo) {
        let rows = [...this.state.poRows]
        rows.forEach(ro => {
            if (ro.gridOneId == this.state.selectedRowId) {
                ro.lineItem.forEach(li => {
                    if (li.gridTwoId == this.state.deleteGridId) {
                        ro.lineItem.splice(li, 1)
                    }
                })

            }
        })
        rows.forEach(ro => {

            let number = 1
            ro.lineItem.forEach(li => {
                li.setNo = number++

            })


        })
        this.setState({
            poRows: rows
        }, () => {
            this.updateMultipleOtb()
            setTimeout(() => {

                this.updatePo()
                this.getOpenToBuy(this.state.selectedRowId)
            }, 10)


        })

    }
    setOtbAfterDelete(idx) {
        let poRows = [...this.state.poRows]
        let otbb = ""
        let id = ""
        for (let i = 0; i < poRows.length; i++) {
            id = poRows[0].gridOneId
            if (poRows[i].gridOneId == id) {
                poRows[i].otb = poRows[i].otb
                otbb = poRows[i].otb

            }
            if (poRows[i].gridOneId > id) {
                poRows[i].otb = otbb - poRows[i - 1].amount
                otbb = otbb - poRows[i - 1].amount

            }

        }

        this.setState({
            poRows: poRows,
            deleteMrp: "",
            deleteOtb: ""

        })



    }

    addRow = () => {
        let rowids = []
        let poRows = [...this.state.poRows]
        let netAmount = poRows[poRows.length - 1].amount
        if (netAmount != "") {

            poRows.forEach(po => {
                rowids.push(po.gridOneId)

            })
            let finalId = Math.max(...rowids);
            let poRowsObj = {

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: false
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: finalId + 1,
                deliveryDate: "",

                marginRule: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorSearch: "",
                    colorList: [],
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: this.state.isSet ? "" : 1,
                    quantity: "",
                    amount: "",
                    rate: "",

                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: [],
                    lineItemChk: false

                }]

            }
            this.setState({
                poRows: [...this.state.poRows, poRowsObj]
            })
        } else {
            this.setState({
                errorMassage: "Amount is complusory",
                poErrorMsg: true

            })
        }

    }
    copingFocus(data) {
        let poRows = [...this.state.poRows]
        this.setState({
            copingLineItem: [...poRows]
        })
        let focusedObj = this.state.focusedObj
        focusedObj.value = ""
        focusedObj.finalRate = ""
        focusedObj.type = "copying color"
        focusedObj.rowId = ""
        focusedObj.cname = ""
        focusedObj.radio = this.state.codeRadio
        focusedObj.colorObj = {
            color: [],
            colorList: []
        }

        this.setState({
            focusedObj
        }, () => {
            this.copingColor(data.rowId, data.id, data.color, data.colorList, data.option, data.total, data.search)

        })

    }
    copingColor(rowId, idd, color, colorList, option, total) {
        let poRows = [...this.state.poRows]


        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == rowId) {
                        poRows[i].lineItem[j].colorChk = true
                    } else {
                        let sum = 0

                        for (let n = 0; n < poRows[i].lineItem[j].ratio.length; n++) {

                            sum += Number(poRows[i].lineItem[j].ratio[n]);
                        }

                        poRows[i].lineItem[j].colorChk = false
                        poRows[i].lineItem[j].color = color
                        poRows[i].lineItem[j].colorSearch = search
                        poRows[i].lineItem[j].colorList = colorList
                        poRows[i].lineItem[j].option = option
                        poRows[i].lineItem[j].total = Number(sum) * Number(option)
                        poRows[i].lineItem[j].quantity = Number(sum) * Number(option) * Number(poRows[i].lineItem[j].setQty)
                    }

                }
            }
        }

        this.setState({

            poRows: poRows
        }, () => {




            let r = [...this.state.poRows]
            let calculatedMarginValue = ""
            let secTwoRows = this.state.secTwoRows
            for (let k = 0; k < r.length; k++) {
                if (r[k].gridOneId == idd) {
                    if (r[k].hsnSacCode != "" || r[k].hsnSacCode != null) {

                        if ((r[k].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {


                            let lineItemArray = []
                            for (let i = 0; i < r[k].lineItem.length; i++) {

                                if (r[k].gridOneId == idd && r[k].lineItem[i].setQty != "" && r[k].finalRate != "" && r[k].finalRate != 0) {

                                    let taxData = {
                                        hsnSacCode: r[k].hsnSacCode,
                                        qty: Number(r[k].lineItem[i].setQty) * Number(r[k].lineItem[i].total),
                                        rate: r[k].finalRate,
                                        rowId: r[k].lineItem[i].gridTwoId,
                                        designRowid: Number(idd),
                                        basic: Number(r[k].lineItem[i].setQty) * Number(r[k].lineItem[i].total) * Number(r[k].finalRate),
                                        clientGstIn: sessionStorage.getItem('gstin'),
                                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                        supplierGstinStateCode: this.state.stateCode,
                                        purtermMainCode: this.state.termCode,
                                        siteCode: this.state.siteCode
                                    }

                                    lineItemArray.push(taxData)
                                }
                            }
                            if (lineItemArray.length != 0) {
                                this.setState({
                                    lineItemChange: true
                                }, () => {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                }
                                )
                            }

                        }
                    }
                    else {

                        this.setState({
                            errorMassage: "HSN code is complusory",
                            poErrorMsg: true

                        })
                    }
                }
            }



        })
    }


    debounceFun() {

        if (!this.state.loader) {
            this.setState({
                loader: true
            }, () => {
                this.contactSubmit();
            }
            )
        }

    }
    otbNegative() {
        let flag = false
        let datevalue = false
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].amount > poRows[i].otb) {
                flag = true
                break
            }
        }


        if (this.state.isOtbValidationPi == "true") {
            if (flag) {
                this.setState({
                    loader: false
                })
                this.setState({
                    otbStatus: true,
                    errorMassage: "Otb is less than Net Amount",
                    poErrorMsg: true
                })

            } else {

                this.setState({
                    otbStatus: false
                })

            }
        } else {


            this.setState({
                otbStatus: false
            })
        }
    }
    lineItemChkFun = () => {

        let poRows = this.state.poRows
        let lineError = []
        for (let i = 0; i < poRows.length; i++) {
            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                if (poRows[i].lineItem[j].lineItemChk == true) {

                    let payload = {
                        "sNo": poRows[i].lineItem[j].gridTwoId,
                        "message": "LineItem charges is wrongly calculated .Please Rentered the value"

                    }
                    lineError.push(payload)
                }
            }
        }
        return lineError
    }
    lineItemArray(lineItem, rate) {
        let lineItemArray = []

        lineItem.forEach(li => {
            let ratio = []
            let ratioIndex = 1
            let itemObj = {}
            let sumOfRatio = 0


            let sr = li.ratio
            for (let k = 0; k < sr.length; k++) {

                let ra = {
                    id: ratioIndex++,
                    ratio: sr[k]
                }
                sumOfRatio += Number(sr[k])
                ratio.push(ra)
            }
            let setUdfArray = li.setUdfArray
            if (setUdfArray.length != 0) {
                setUdfArray.forEach(uArr => {


                    itemObj[uArr.udfType] = uArr.value


                })
            }

            let lineObj = {
                itemId: li.itemBarcode,
                setHeaderId: li.setHeaderId,
                sizes: li.sizeList,
                ratios: ratio,
                colors: li.colorList,
                setUdf: setUdfArray.length != 0 ? itemObj : {},
                qty: li.quantity,
                sumOfRatio: sumOfRatio,
                noOfSets: li.setQty,
                amount: li.amount,
                gst: li.gst,
                tax: li.tax,
                otb: li.otb,
                finCharge: li.finCharges,
                calculatedMargin: li.calculatedMargin,
                intakeMargin: li.mrk,
                setNo: li.setNo,
                basic: li.setQty * rate


            }
            lineItemArray.push(lineObj)
        })
        return lineItemArray

    }
    contactSubmit() {
        this.supplier();

        this.transporter();
        this.typeOfBuying();

        { this.state.isCityExist == true ? this.city() : null }

        var lineError = this.lineItemChkFun()

        this.otbNegative()
        this.gridFirst()
        { this.state.isSiteExist == "true" ? this.site() : null }
        { this.state.isUDFExist == "true" ? this.gridFivth() : null }
        this.gridSecond()
        { this.state.itemUdfExist == "true" ? this.gridThird() : null }
        this.gridFourth()
        const t = this;
        setTimeout(function (e) {

            const { cityerr, typeOfBuyingErr, vendorerr, transportererr, otbStatus, lineItemChange } = t.state;
            if (!cityerr && !vendorerr && !transportererr && !typeOfBuyingErr) {
                // const { poValidFromerr, lastInDateerr, vendorerr, articleerr, transportererr, otbStatus, } = t.state;

                if (!otbStatus) {
                    if (t.state.gridFirst) {
                        if (t.state.gridSecond) {

                            if ((t.state.gridThird && t.state.itemUdfExist == "true") || t.state.itemUdfExist == "false") {
                                if (t.state.gridFourth) {
                                    if ((t.state.gridFivth && t.state.isUDFExist == "true") || t.state.isUDFExist == "false") {
                                        if (!lineItemChange) {

                                            if (lineError.length == 0) {
                                                let lineItem = []
                                                let poRows = [...t.state.poRows]
                                                poRows.forEach(po => {
                                                    let { cat1, cat2, cat3, cat4, cat5, cat6, desc1, desc2, desc3, desc4, desc5, desc6 } = ""
                                                    let itemObj = {}
                                                    if (po.catDescArray.length != 0) {
                                                        po.catDescArray.forEach(cdArr => {
                                                            cat1 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT1") {
                                                                    return item
                                                                }
                                                            })
                                                            cat2 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT2") {
                                                                    return item
                                                                }
                                                            })
                                                            cat3 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT3") {
                                                                    return item
                                                                }
                                                            })
                                                            cat4 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT4") {
                                                                    return item
                                                                }
                                                            })
                                                            cat5 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT5") {
                                                                    return item
                                                                }
                                                            })
                                                            cat6 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT6") {
                                                                    return item
                                                                }
                                                            })
                                                            desc1 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC1") {
                                                                    return item
                                                                }
                                                            })
                                                            desc2 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC2") {
                                                                    return item
                                                                }
                                                            })
                                                            desc3 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC3") {
                                                                    return item
                                                                }
                                                            })
                                                            desc4 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC4") {
                                                                    return item
                                                                }
                                                            })
                                                            desc5 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC5") {
                                                                    return item
                                                                }
                                                            })
                                                            desc6 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC6") {
                                                                    return item
                                                                }


                                                            })
                                                        })

                                                    }
                                                    if (po.itemUdfArray.length != 0) {
                                                        po.itemUdfArray.forEach(uArr => {
                                                            uArr.itemUdf.forEach(iu => {

                                                                itemObj[iu.cat_desc_udf] = iu.value

                                                            })
                                                        })

                                                    }
                                                    let lineItemArray = t.lineItemArray(po.lineItem, po.finalRate)

                                                    let poDetails = {
                                                        design: po.vendorDesign,
                                                        designWiseRowId: po.gridOneId,
                                                        hsnSacCode: po.hsnSacCode,
                                                        hsnCode: po.hsnCode,
                                                        hl1Name: po.divisionName,
                                                        hl1Code: po.divisionCode,
                                                        hl2Name: po.sectionName,
                                                        hl2Code: po.sectionCode,
                                                        hl3Name: po.departmentName,
                                                        hl3Code: po.departmentCode,
                                                        hl4Name: po.articleName,
                                                        hl4Code: po.articleCode,
                                                        hl4CodeSeq: po.articleCode + "_" + po.gridOneId,
                                                        rate: po.rate,
                                                        finalRate: po.finalRate,
                                                        discountType: po.discount.discountPer ? po.discount.discountType + "%" : po.discount.discountType,
                                                        discountValue: po.discount.discountValue,
                                                        editableMrp: po.mrp,
                                                        rsp: po.rsp,
                                                        mrp: po.vendorMrp,
                                                        mrpStart: po.mrpStart,
                                                        mrpEnd: po.mrpEnd,
                                                        images: po.image,
                                                        marginRule: po.marginRule,
                                                        designWiseTotalQty: po.quantity,
                                                        designWiseNetAmountTotal: po.amount,
                                                        otb: po.otb,
                                                        totalBasic: po.basic,
                                                        designWiseTotalGST: po.gst,
                                                        designWiseTotalFinCharges: po.finCharges,
                                                        remarks: po.remarks,
                                                        designWiseTotalTax: po.tax,
                                                        deliveryDate: moment(po.deliveryDate).format(),
                                                        containsImage: po.containsImage,
                                                        designWiseTotalCalculatedMargin: po.calculatedMargin,
                                                        designWiseTotalIntakeMargin: po.mrk,
                                                        headers: {
                                                            catDescArray: po.catDescHeader,
                                                            itemUdfHeader: po.itemUdfHeader,
                                                            setUdfArray: po.lineItem[0].setUdfHeader

                                                        },
                                                        catDescArray: po.catDescArray.length == 0 ? {} : {
                                                            cat1Code: cat1.code,
                                                            cat1Name: cat1.value,
                                                            cat2Code: cat2.code,
                                                            cat2Name: cat2.value,
                                                            cat3Code: cat3.code,
                                                            cat3Name: cat3.value,
                                                            cat4Code: cat4.code,
                                                            cat4Name: cat4.value,
                                                            cat5Code: cat5.code,
                                                            cat5Name: cat5.value,
                                                            cat6Code: cat6.code,
                                                            cat6Name: cat6.value,
                                                            desc1Code: desc1.code,
                                                            desc1Name: desc1.value,
                                                            desc2Code: desc2.code,
                                                            desc2Name: desc2.value,
                                                            desc3Code: desc3.code,
                                                            desc3Name: desc3.value,
                                                            desc4Code: desc4.code,
                                                            desc4Name: desc4.value,
                                                            desc5Code: desc5.code,
                                                            desc5Name: desc5.value,
                                                            desc6Code: desc6.code,
                                                            desc6Name: desc6.value

                                                        },
                                                        itemUdf: po.itemUdfArray.length == 0 ? {} : itemObj,
                                                        lineItem: lineItemArray





                                                    }
                                                    lineItem.push(poDetails)
                                                })



                                                let finalSubmitData = {
                                                    isSet: t.state.isSet,
                                                    typeOfBuying: t.state.typeOfBuying,

                                                    slCode: t.state.slCode,
                                                    slName: t.state.slName,
                                                    slCityName: t.state.isCityExist == true ? t.state.city : t.state.slCityName,
                                                    slAddr: t.state.slAddr,
                                                    leadTime: t.state.leadDays,
                                                    termCode: t.state.termCode,
                                                    termName: t.state.termName,
                                                    transporterCode: t.state.transporterCode,
                                                    transporterName: t.state.transporterName,
                                                    poQuantity: t.state.poQuantity,
                                                    poAmount: t.state.poAmount,
                                                    stateCode: t.state.stateCode,
                                                    itemUdfExist: t.state.itemUdfExist,
                                                    isUDFExist: t.state.isUDFExist,
                                                    enterpriseGstin: sessionStorage.getItem("gstin"),
                                                    piDetails: lineItem,

                                                    siteCode: t.state.siteCode,
                                                    siteName: t.state.siteName
                                                }

                                                t.props.gen_PurchaseIndentRequest(finalSubmitData)


                                            } else {
                                                t.setState({
                                                    loader: false,
                                                    multipleErrorpo: true,
                                                    lineError: lineError
                                                })


                                            }
                                        } else {

                                            t.setState({
                                                loader: false
                                            })
                                            t.setState({
                                                toastMsg: "Wait a moment untill charges calculated!!",
                                                toastLoader: true
                                            })

                                            setTimeout(function () {
                                                t.setState({
                                                    toastLoader: false
                                                })
                                            }, 1000)
                                        }
                                    }
                                    else {
                                        t.setState({
                                            loader: false
                                        })
                                        t.validateSetUdf();

                                    }
                                } else {
                                    t.setState({
                                        loader: false
                                    })
                                    t.validateLineItem();


                                }

                            } else {
                                t.setState({
                                    loader: false
                                })
                                t.validateUdf();

                            }

                        } else {
                            t.setState({
                                loader: false
                            })
                            t.validateCatDescRow();

                        }
                    } else {
                        t.setState({
                            loader: false
                        })

                        t.validateItemdesc();
                    }

                } else {
                    t.setState({
                        loader: false
                    })
                }
            } else {
                t.setState({
                    loader: false
                })
            }
        }, 1000)





    }
    catDescValue(catdesc, data) {
        if (catdesc.length != 0) {
            for (let i = 0; i < catdesc.length; i++) {
                if (catdesc[i].catdesc == "CAT1") {
                    catdesc[i].code = data.cat1Code
                    catdesc[i].value = data.cat1Name

                }
                if (catdesc[i].catdesc == "CAT2") {
                    catdesc[i].code = data.cat2Code
                    catdesc[i].value = data.cat2Name

                }
                if (catdesc[i].catdesc == "CAT3") {
                    catdesc[i].code = data.cat3Code
                    catdesc[i].value = data.cat3Name

                }
                if (catdesc[i].catdesc == "CAT4") {
                    catdesc[i].code = data.cat4Code
                    catdesc[i].value = data.cat4Name

                }
                if (catdesc[i].catdesc == "CAT5") {
                    catdesc[i].code = data.cat5Code
                    catdesc[i].value = data.cat5Name

                }
                if (catdesc[i].catdesc == "CAT6") {
                    catdesc[i].code = data.cat6Code
                    catdesc[i].value = data.cat6Name

                }
                if (catdesc[i].catdesc == "DESC1") {
                    catdesc[i].code = data.desc1Code
                    catdesc[i].value = data.desc1Name

                }
                if (catdesc[i].catdesc == "DESC2") {
                    catdesc[i].code = data.desc2Code
                    catdesc[i].value = data.desc2Name

                }
                if (catdesc[i].catdesc == "DESC3") {
                    catdesc[i].code = data.desc3Code
                    catdesc[i].value = data.desc3Name

                }
                if (catdesc[i].catdesc == "DESC4") {
                    catdesc[i].code = data.desc4Code
                    catdesc[i].value = data.desc4Name

                }
                if (catdesc[i].catdesc == "DESC5") {
                    catdesc[i].code = data.desc5Code
                    catdesc[i].value = data.desc5Name

                }
                if (catdesc[i].catdesc == "DESC6") {
                    catdesc[i].code = data.desc6Code
                    catdesc[i].value = data.desc6Name

                }


            }
        }
        if (catdesc.length != 0) {
            catdesc = [{ catDescId: 1, catDesc: catdesc }]
        }
        return catdesc


    }
    itemUdfValue(itemUdf, data) {

        if (itemUdf.length != 0) {
            itemUdf.forEach(itemUdfDetails => {
                Object.keys(data).forEach(keys => {
                    if (itemUdfDetails.cat_desc_udf == keys) {
                        itemUdfDetails.value = data[keys]
                    }
                })


            });
        }
        if (itemUdf.length != 0) {
            itemUdf = [{ itemUdfId: 1, itemUdf: itemUdf }]
        }
        return itemUdf

    }
    setUdfValue(setUdf, data) {

        if (setUdf.length != 0) {
            setUdf.forEach(setUdfDetails => {
                Object.keys(data).forEach(keys => {
                    if (setUdfDetails.udfType == keys) {
                        setUdfDetails.value = data[keys]
                    }
                })


            });
        }

        return setUdf

    }
    lineItemValue(data, lineItem) {
        let li_Array = []
        lineItem.forEach(li => {
            let id = 1;
            let setno = 1
            let color = [];
            let size = [];
            let ratio = []



            li.colors.forEach(co => {
                color.push(co.cname)
            });
            li.sizes.forEach(si => {
                size.push(si.cname)
            })
            li.ratios.forEach(si => {
                ratio.push(si.ratio)
            })
            let obj = {
                colorChk: false,
                icodeChk: false,
                colorSizeList: [],
                icodes: [],
                itemBarcode: "",
                setHeaderId: li.setHeaderId,
                gridTwoId: id++,
                color: color,
                colorSearch: li.colorSearch,
                colorList: li.colors,
                sizes: size,
                sizeSearch: li.sizeSearch,
                sizeList: li.sizes,
                ratio: ratio,
                size: "",
                setRatio: "",
                option: li.colors.length,
                setNo: setno++,
                total: li.sumOfRatio * li.colors.length,
                setQty: li.noOfSets,
                quantity: li.qty,
                amount: li.amount,


                gst: li.gst,
                finCharges: li.finCharge,
                tax: li.tax,
                otb: li.otb,
                calculatedMargin: li.calculatedMargin,
                mrk: li.intakeMargin,
                setUdfHeader: data,
                setUdfArray: this.setUdfValue(data, li.setUdf),
                lineItemChk: false
            }
            li_Array.push(obj)

        })
        return li_Array
    }
    updatePoData(data) {

        let poRows = []
        let id = 1;

        let designArray = []


        let imageUrl = {};

        let piDetails = data.piDetails != undefined ? data.piDetails : data.poDetails
        piDetails.forEach(pi => {




            pi.images.forEach(img => {
                if (img != null) {
                    imageUrl[img.split('/')[9].split('?')[0]] = img
                }
            })

            let piData = {

                setHeaderId: "",

                vendorMrp: pi.mrp,
                vendorDesign: pi.design,
                mrk: pi.designWiseTotalIntakeMargin,
                discount: {
                    discountType: pi.discountType.split("%")[0],
                    discountValue: pi.discountValue,
                    discountPer: pi.discountType.includes("%") ? true : false
                },
                rate: pi.rate,
                finalRate: pi.finalRate,
                netRate: pi.rate,



                basic: pi.designWiseTotalBasic,
                rsp: pi.rsp,
                mrp: pi.mrp,
                quantity: pi.designWiseTotalQty,
                amount: pi.designWiseNetAmountTotal,
                remarks: pi.remarks != null ? pi.remarks : "",
                otb: pi.designWiseTotalOtb,
                gst: pi.designWiseTotalGST,
                finCharges: pi.designWiseTotalFinCharges,
                tax: pi.designWiseTotalTax,
                calculatedMargin: pi.designWiseTotalCalculatedMargin,
                gridOneId: id++,
                deliveryDate: this.state.lastInDate,

                marginRule: pi.marginRule,
                image: Object.keys(imageUrl),

                imageUrl: imageUrl,
                containsImage: pi.containsImage,
                articleCode: pi.hl4Code != null ? pi.hl4Code : "",
                articleName: pi.hl4Name != null ? pi.hl4Name : "",
                departmentCode: pi.hl3Code != null ? pi.hl3Code : "",
                departmentName: pi.hl3Name != null ? pi.hl3Name : "",
                sectionCode: pi.hl2Code != null ? pi.hl2Code : "",
                sectionName: pi.hl2Name != null ? pi.hl2Name : "",
                divisionCode: pi.hl1Code != null ? pi.hl1Code : "",
                divisionName: pi.hl1Name != null ? pi.hl1Name : "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: pi.hsnCode != null ? pi.hsnCode : "",
                hsnSacCode: pi.hsnSacCode != null ? pi.hsnSacCode : "",
                mrpStart: pi.mrpStart != null ? pi.mrpStart : "",
                mrpEnd: pi.mrpEnd != null ? pi.mrpEnd : "",
                catDescHeader: pi.headers.catDescArray,
                catDescArray: this.catDescValue(pi.headers.catDescArray, pi.catDescArray),
                itemUdfHeader: pi.headers.itemUdfHeader,
                itemUdfArray: this.itemUdfValue(pi.headers.itemUdfHeader, pi.itemUdf),
                lineItem: this.lineItemValue(pi.headers.setUdfArray, pi.lineItem)

            }
            poRows.push(piData)

        })

        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss
        })

        this.setState({

            stateCode: data.stateCode,
            isSet: data.isSet == "true" ? true : false,

            slCode: data.slCode != null ? data.slCode : "",
            slAddr: data.slAddr != null ? data.slAddr : "",
            slName: data.slName != null ? data.slName : "",
            city: data.city != null ? data.slCityName : "",
            slCityName: data.slCityName != null ? data.slCityName : "",
            supplier: data.slName != null ? data.slName + "-" + data.slAddr : "",
            leadDays: data.leadTime != null ? data.leadTime : "",
            termCode: data.termCode != null ? data.termCode : "",
            termName: data.termName != null ? data.termName : "",
            transporterCode: data.transporterCode != null ? data.transporterCode : "",
            transporterName: data.transporterName != null ? data.transporterName : "",
            transporter: data.transporterName != null ? data.transporterName : "",
            term: data.termName != null ? data.termName : "",
            typeOfBuying: data.typeofBuying != null || data.typeofBuying != undefined ? data.typeofBuying : "Adhoc",

            poRows: poRowss,
            poQuantity: data.poQuantity != null ? data.poQuantity : "",
            poAmount: data.poAmount != null ? data.poAmount : "",
            loadIndentTrue: true,

            siteCode: data.siteCode != null ? data.siteCode : "",
            siteName: data.siteName != null ? data.siteName : "",




        }, () => {
            let poRows = [...this.state.poRows]

            if (this.state.isMrpRequired && this.state.displayOtb) {
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];
                const date = new Date(this.state.lastInDate);
                let year = date.getFullYear()
                let monthName = (monthNames[date.getMonth()]);
                let articleList = []
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].articleCode != "" && poRows[i].vendorMrp != "") {
                        let payload = {
                            rowId: poRows[i].gridOneId,
                            articleCode: poRows[i].articleCode,
                            mrp: poRows[i].vendorMrp
                        }
                        articleList.push(payload)


                    }
                }
                let dataa = {
                    articleList: articleList,
                    month: monthName,
                    year: year
                }
                this.setState({
                    changeLastIndate: true
                })
                if (articleList.length != 0) {
                    this.props.multipleOtbRequest(dataa)
                }
            }
            this.gridSecond()
            this.gridThird()
            this.gridFourth()
            this.gridFivth()
        })
    }
    handleInput(e) {

        if (e.target.id == "vendor") {
            this.setState({
                supplier: e.target.value,
                supplierSearch: this.state.isModalShow ? "" : e.target.value
            })


        } else if (e.target.id == "city") {
            this.setState({
                city: e.target.value,
                citySearch: this.state.isModalShow ? "" : e.target.value
            })
        } else if (e.target.id == "siteName") {
            this.setState({
                siteName: e.target.value,
                siteSearch: this.state.isModalShow ? "" : e.target.value
            })
        } else if (e.target.id == "transporter") {
            this.setState({
                transporter: e.target.value,
                transporterSearch: this.state.isModalShow ? "" : e.target.value
            })

        } else if (e.target.id == "loadIndentInput") {
            this.setState({
                loadIndent: e.target.value,
                loadIndentSearch: this.state.isModalShow ? "" : e.target.value
            })


        } else if (e.target.id == "poSetVendor") {
            this.setState({
                poSetVendor: e.target.value,
                setVendorSearch: this.state.isModalShow ? "" : e.target.value


            })
        } else if (e.target.id == "orderSet") {
            this.setState({
                orderSet: e.target.value,
                orderSearch: this.state.isModalShow ? "" : e.target.value
            })
        }
    }
    onEsc = () => {
        this.setState({

            poArticle: false,
            poArticleAnimation: false,
            discountModal: false,
            cityModal: false,
            cityModalAnimation: false,
            icodeModalAnimation: false,
            itemBarcodeModal: false,
            siteModal: false,
            siteModalAnimation: false,
            hsnModal: false,
            hsnModalAnimation: false,
            imageModal: false,
            imageModalAnimation: false,
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: false,
            trasporterModal: false,
            transporterAnimation: false,

            loadIndentModal: false,
            loadIndentAnimation: false,
            itemModal: false,
            itemModalAnimation: false,
            udfMapping: false,
            udfMappingAnimation: false,
            udfSetting: false,
            udfSettingAnimation: false,
            supplierModal: false,
            supplierModalAnimation: false,
            articleModalAnimation: false,
            articleModal: false,
            colorModal: false,
            colorModalAnimation: false,
            vendorDesignModal: false,
            vendorDesignAnimation: false,
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            departmentSetBasedAnimation: false,
            departmentModal: false,


            orderNumber: false,
            orderNumberModalAnimation: false,

            SetVendor: false,
            setVendorModalAnimation: false,
        })
        //  this.escChild.current.childEsc();
    }
    handleSearch(e, type, id, index) {
        let poRows = [...this.state.poRows]
        if (type == "divisionName") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].divisionName = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value

                })


            }
        } else if (type == "sectionName") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].sectionName = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value


                })


            }

        } else if (type == "departmentName") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].departmentName = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value


                })


            }
        } else if (type == "articleCode") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].articleCode = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value


                })



            }
        } else if (type == "hsnCode") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].hsnSacCode = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    hsnSearch: e.target.value


                })



            }
        } else if (type == "vendorMrp") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].vendorMrp = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    mrpSearch: e.target.value


                })



            }
        }
        else if (type == "discount") {
            if (e.target.id == type + id) {
                var numberPattern = /\d+/g;
                let value = ""
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        if (e.target.value.match(/^[a-zA-Z0-9 ]+$/)) {
                            poRows[i].discount.discountType = e.target.value
                            value = e.target.value
                            var Numvalue = e.target.value.match(numberPattern)

                            poRows[i].discount.discountValue = Numvalue != null ? Numvalue[0] : ""
                        }
                    }

                }
                this.setState({
                    poRows: poRows,
                    discountSearch: e.target.value


                }, () => {
                    if (!this.state.isDiscountMap) {
                        let data = {
                            discount: {
                                discountType: value,
                                discountValue: Numvalue != null ? Numvalue[0] : ""
                            },
                            discountGrid: index
                        }
                        this.updateDiscountModal(data)
                    }
                })



            }

        } else if (type == "itemCode") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].itemCodeSearch = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    itemCodeSearch: e.target.value


                })

            }

        }
        this.setState({
            focusId: type + id
        })
    }
    changeSet() {
        if (this.state.isSet) {

            this.setState({


                headerMsg: "Are you sure you want to raise purchase order on the basis of NON SET ",
                paraMsg: "Click confirm to continue.",
                radioChange: false,
                confirmModal: true,

            })
        } else {
            this.setState({

                headerMsg: "Are you sure you want to raise purchase order on the basis of  SET ",
                paraMsg: "Click confirm to continue.",
                radioChange: true,
                confirmModal: true,
            })
        }
    }
    closeMultipleError() {
        this.setState({
            multipleErrorpo: false
        })

        document.onkeydown = function (t) {

            return true;

        }
    }

    checkErr() {
        if (this.state.siteNameerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Site is Complusory",

                })
            document.getElementById("siteName").focus()
        } else if (this.state.cityerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "City is Complusory",

                })
            document.getElementById("city").focus()
        }
        else if (this.state.vendorerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Vendor is Complusory",

                })
            document.getElementById("vendor").focus()
        }
        else if (this.state.transportererr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Transporter is Complusory",

                })
            document.getElementById("transporter").focus()
        }
        else if (this.state.typeOfBuyingErr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Type of Buying is Complusory",

                })
            document.getElementById("typeOfBuying").focus()
        }
        else if (this.state.loadIndenterr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "load Indent is Complusory",

                })
            document.getElementById("loadIndentInput").focus()
        }
        else if (this.state.poSetVendorerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Venoor is Complusory",

                })
            document.getElementById("poSetVendor").focus()
        }
        else if (this.state.orderSeterr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Order Number is Complusory",

                })
            document.getElementById("orderSet").focus()
        }
    }
    updateFocusId(data) {

        this.setState({
            focusId: data
        })
    }
    discountPer(id, percentage) {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == id) {
                if (percentage) {
                    poRows[i].discount.discountPer = false
                } else {
                    poRows[i].discount.discountPer = true

                }

            }
        }
        this.setState({
            poRows
        }, () => {


            let calculatedMarginValue = ""
            let finalRate = ""
            let rate = ""

            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == id) {
                    if (poRows[i].rate != "") {
                        if (poRows[i].discount.discountType != "" && this.state.isDiscountAvail) {

                            if (poRows[i].discount.discountPer) {
                                let value = poRows[i].rate * (poRows[i].discount.discountValue / 100)
                                poRows[i].finalRate = poRows[i].rate - value
                                finalRate = poRows[i].rate - value

                            } else {
                                poRows[i].finalRate = poRows[i].rate - poRows[i].discount.discountValue
                                finalRate = poRows[i].rate - poRows[i].discount.discountValue
                            }
                        } else {
                            poRows[i].finalRate = poRows[i].rate
                            finalRate = poRows[i].rate

                        }
                    }
                }
            }
            let hsnSacCode = ""
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == id) {
                    hsnSacCode = poRows[i].hsnSacCode
                    if ((poRows[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                        let lineItemArray = []

                        for (let j = 0; j < poRows[i].lineItem.length; j++) {

                            if (poRows[i].lineItem[j].setQty != "" && poRows[i].lineItem[j].finalRate != "" && poRows[i].lineItem[j].finalRate != 0) {
                                let taxData = {
                                    hsnSacCode: poRows[i].hsnSacCode,
                                    qty: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total),
                                    rate: finalRate,
                                    rowId: poRows[i].lineItem[j].gridTwoId,
                                    designRowid: Number(idd),
                                    basic: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total) * Number(finalRate),
                                    clientGstIn: sessionStorage.getItem('gstin'),
                                    piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                    supplierGstinStateCode: this.state.stateCode,
                                    purtermMainCode: this.state.termCode,
                                    siteCode: this.state.siteCode
                                }
                                lineItemArray.push(taxData)
                            }
                        }
                        if (hsnSacCode != "" || hsnSacCode != null) {
                            if (lineItemArray.length != 0) {
                                this.setState({
                                    lineItemChange: true
                                }, () => {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                }
                                )
                            }
                        } else {

                            this.setState({
                                errorMassage: "HSN code is complusory",
                                poErrorMsg: true

                            })
                        }

                    }
                }
            }



            this.setState({
                poRows: poRows,

            })

        })

    }



    render() {

        const { typeofBuying, city, cityerr, itemcodeerr, itemCodeList, siteNameerr, siteName, hsnSacCode, hsnerr, poDate, loadIndent, loadIndenterr, poValidFrom, poValidFromerr, lastInDate, lastInDateerr, articleerr, division, department, section,
            supplier, vendorerr, leadDays, leadDayserr, transporter, transportererr, startRange, endRange, term, articleName, departmentSeterr, poSetVendorerr, setDepartment, poSetVendor, orderSet, orderSeterr } = this.state;

        return (
            <div className="container_div pors-pg" id="" >
                <form name="siteForm" className="organizationForm">
                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <div className="purchasedOrderContainer poFormMain">
                            <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                                <div className="col-lg-6 col-md-4 col-sm-12 pad-0">
                                    <ul className="list_style">
                                        <li><label className="contribution_mart">PURCHASE INDENT</label></li>
                                        <li><p className="master_para">Create purchase indent</p></li>
                                    </ul>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6 text-right">
                                    <label className="purchaseLabel set">
                                        {this.state.isSet ? "SET" : "NON SET"}</label>
                                    <label className="tg-switch" >
                                        <input type="checkbox" checked={this.state.isSet} onChange={(e) => this.changeSet(e)} />
                                        <span className="tg-slider tg-round"></span>
                                    </label>
                                </div>

                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 mo-change pad-0">
                                <div className="col-lg-9 col-md-9 col-sm-9 pad-0 m-top-20 m-top-0">
                                    {this.state.isSiteExist == "true" ? <div className="col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Site<span className="mandatory">*</span>
                                        </label>

                                        <div className={siteNameerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                            <div className="topToolTip toolTipSupplier width100">
                                                {!this.state.isModalShow ?
                                                    <input autoComplete="off" type="text" onChange={(e) => this.handleInput(e)} className="inputTextKeyFuc onFocus inputBgLiteY" onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" />

                                                    : <input autoComplete="off" type="text" readOnly className="inputTextKeyFuc" onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openSiteModal(e) : null} />}
                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openSiteModal(e)}>
                                                    ▾
                                                    </span> : null} */}
                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn" onClick={(e) => this.openSiteModal(e)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span> : null}
                                                {!this.state.isModalShow ? this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteName={this.state.siteName} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null : null}

                                                {siteName != "" ? <span className="topToolTipText topAuto">{siteName}</span> : null}

                                            </div>
                                        </div>
                                        {siteNameerr ? (
                                            <span className="error">
                                                Enter Site
                                            </span>
                                        ) : null}
                                    </div> : null}
                                    {this.state.isCityExist == true ? <div className="col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            City<span className="mandatory">*</span>
                                        </label>

                                        <div className={cityerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                            <div className="topToolTip toolTipSupplier width100">
                                                {!this.state.isModalShow ?
                                                    <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="inputTextKeyFuc m-b-10 inputBgLiteY" onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={city} placeholder="Choose City" />

                                                    :
                                                    <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc m-b-10" onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={city} placeholder="Choose City" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openCityModal(e) : null} />}
                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openCityModal(e)}>
                                                    ▾
                                                </span> : null} */}
                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn" onClick={(e) => this.openCityModal(e)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span> : null}
                                                {!this.state.isModalShow ? this.state.cityModal ? <CityModal city={this.state.city} citySearch={this.state.citySearch} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null : null}

                                                {city !== "" && <span className="topToolTipText topAuto">{city}</span>}
                                            </div>

                                        </div>
                                        {cityerr ? (
                                            <span className="error">
                                                Enter City
                                            </span>
                                        ) : null}
                                    </div> : null}
                                    <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Vendor<span className="mandatory">*</span>
                                        </label>
                                        <div className={vendorerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"}  >
                                            <div className="topToolTip toolTipSupplier width100">
                                                {!this.state.isModalShow ?
                                                    <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="inputTextKeyFuc m-b-10 onFocus inputBgLiteY" onKeyDown={(e) => this._handleKeyPress(e, "vendor")} id="vendor" value={supplier} placeholder="Choose Vendor" />

                                                    :
                                                    <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc m-b-10 onFocus" onKeyDown={(e) => this._handleKeyPress(e, "vendor")} id="vendor" value={supplier} placeholder="Choose Vendor" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openSupplier(e, "vendor") : null} />}
                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openSupplier(e, "vendor")}>
                                                    ▾
                                                </span> : null} */}
                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn" onClick={(e) => this.openSupplier(e, "vendor")}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span> : null}
                                                {!this.state.isModalShow ? this.state.supplierModal ? <SupplierModal {...this.props}{...this.state} city={this.state.city} supplierSearch={this.state.supplierSearch} siteName={this.state.siteName} siteCode={this.state.siteCode} departmentCode={this.state.hl3Code} department={this.state.department} supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null : null}

                                                {supplier != "" ? <span className="topToolTipText topAuto">{supplier}</span> : null}
                                            </div>
                                        </div>

                                        {vendorerr ? (
                                            <span className="error">
                                                Enter Vendor
                                            </span>
                                        ) : null}

                                    </div>
                                    <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Lead Days<span className="mandatory">*</span>
                                        </label>

                                        <input autoComplete="off" readOnly type="text" value={leadDays} id="leadDays" onChange={(e) => this.onChange(e)} className={leadDayserr ? "m-b-10 errorBorder purchaseTextbox" : "purchaseTextbox"} disabled />

                                    </div>
                                    <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Transporter<span className="mandatory">*</span>
                                        </label>
                                        <div className={transportererr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                            <div className="topToolTip toolTipSupplier width100">
                                                {!this.state.isModalShow ?
                                                    <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="inputTextKeyFuc m-b-10 onFocus inputBgLiteY" onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" />

                                                    :
                                                    <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc m-b-10 onFocus" onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openTransporterSelection(e, "transporter") : null} />}
                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openTransporterSelection(e, "transporter")}>
                                                    ▾
                                                </span> : null} */}
                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn" onClick={(e) => this.openTransporterSelection(e, "transporter")}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span> : null}
                                                {!this.state.isModalShow ? this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} isTransporterDependent={this.state.isTransporterDependent} city={this.state.city} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null : null}

                                                {transporter != "" ? <span className="topToolTipText topAuto">{transporter}</span> : null}
                                            </div>
                                        </div>
                                        {transportererr ? (
                                            <span className="error">
                                                Enter Transporter
                                            </span>
                                        ) : null}
                                    </div>
                                    <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Term<span className="mandatory">*</span>
                                        </label>
                                        <input autoComplete="off" readOnly type="text" className="purchaseSelectBox" id="termData" value={term} placeholder="" disabled />
                                    </div>

                                    <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Type Of Buying<span className="mandatory">*</span>
                                        </label>
                                        <select value={this.state.typeOfBuying} className="purchaseSelectBox onFocus" id="typeofBuying" onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChange(e) : null}>
                                            <option>Select</option>
                                            <option value="Adhoc" > Adhoc</option>
                                            <option value="Planned">Planned </option>
                                        </select>
                                        {this.state.typeOfBuyingErr ? (
                                            <span className="error">
                                                Select Type of Buying
                                            </span>
                                        ) : null}
                                    </div>

                                </div>
                                <div className="col-lg-3 col-md-5 col-sm-12 pr-order-1 pad-0">
                                    <div className="poAmtTotal">
                                        <ul className="pad-0">
                                            <li>
                                                <label>PI Quantity</label>
                                                <span>
                                                    {this.state.poQuantity}
                                                </span>
                                            </li>
                                            <li>
                                                <label> PI Amount </label>
                                                <span>{Math.round(this.state.poAmount * 100) / 100} </span>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-12">
                        <div className="purchasedOrderContainer1">
                            <div className="StickyDiv1 overflowVisible">

                                <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper tableGeneric sectionOneTable bordere3e7f3 poTableMain">
                                    <div className="table-scroll oflxAuto pad-0 marginLeft_150" >
                                        <table className="table scrollTable table scrollTable main-table zui-table border-bot-table  top-min-2" >
                                            <thead>
                                                <tr>
                                                    <th className="fixed-side alignMiddle width-150" >
                                                        <ul className="list-inline">

                                                            <li className="width70">
                                                                <label className="width-45 lableFixed">
                                                                    Action
                                                                        </label>
                                                            </li>
                                                        </ul>
                                                    </th>


                                                    <th>
                                                        <label>Division </label>
                                                    </th>
                                                    <th><label>
                                                        Section
                                                        </label></th>

                                                    <th><label>
                                                        Department</label>
                                                    </th>
                                                    <th>
                                                        <label>Article Code </label>
                                                    </th>
                                                    <th><label>
                                                        Article Name
                                                        </label></th>

                                                    {this.state.codeRadio == "poIcode" ? <th><label>
                                                        Item Code</label>
                                                    </th> : null}
                                                    <th>
                                                        <label>
                                                            HSN
                                                        </label>
                                                    </th>
                                                    {this.state.isMrpRangeDisplay
                                                        ? <th>
                                                            <label>MRP range(Start-End)</label>
                                                        </th> : null}
                                                    {this.state.isMrpRequired ? <th>
                                                        {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? <label>
                                                            DESC 6<span className="mandatory">*</span>
                                                        </label> : <label>MRP<span className="mandatory">*</span></label>}
                                                    </th> : null}
                                                    {!this.state.isVendorDesignNotReq ? <th>
                                                        <label>
                                                            Vendor Design No.<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}

                                                    {this.state.isRspRequired ? <th>
                                                        <label>
                                                            RSP<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isMRPEditable ? <th>
                                                        <label>
                                                            MRP<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}



                                                    <th>
                                                        <label>
                                                            Rate<span className="mandatory">*</span>
                                                        </label>
                                                    </th>
                                                    {this.state.isDiscountAvail ? <th>
                                                        <label>
                                                            Discount<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isDisplayFinalRate ? <th>
                                                        <label>
                                                            Final rate
                                                        </label>
                                                    </th> : null}


                                                    <th>
                                                        <label>
                                                            Image
                                                             </label>
                                                    </th>
                                                    {this.state.displayOtb ? <th>
                                                        <label>
                                                            OTB<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    <th>
                                                        <label>
                                                            Delivery Date<span className="mandatory">*</span>
                                                        </label>
                                                    </th>

                                                    {this.state.isDisplayMarginRule ? <th>
                                                        <label>
                                                            Margin Rule
                                                   </label>

                                                    </th> : null}
                                                    <th><label>
                                                        Remarks
                                                    </label></th>
                                                    <th><label>
                                                        Total Quantity
                                                    </label></th>
                                                    {this.state.isBaseAmountActive ? <th><label>
                                                        Basic
                                                    </label></th> : null}
                                                    <th><label>
                                                        Total Net Amount
                                                    </label></th>
                                                    {!this.state.isTaxDisable ? <th><label>
                                                        Tax
                                                    </label></th> : null}
                                                    {this.state.isRspRequired ? <th><label>
                                                        Calculated Margin
                                                    </label></th> : null}
                                                    {!this.state.isGSTDisable ? <th><label>
                                                        GST
                                                    </label></th> : null}

                                                    {this.state.isRspRequired ? <th>

                                                        <label>
                                                            Mark Up / Down
                                                                     </label>
                                                    </th> : null}
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {this.state.poRows.map((item, idx) => (
                                                    <tr id={idx} key={idx}>
                                                        <td className="fixed-side alignMiddle">
                                                            <ul className="list-inline">
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <li className="">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 19" onClick={(e) => this.DeleteRowPo(item.gridOneId, item.otb, item.mrp)}>
                                                                        <path fill={item.checked ? "#FF0000" : "#e5e5e5"} fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                    </svg>
                                                                </li> : null}
                                                                <li>
                                                                    <button className="doneButton create-set-icon" type="button" onClick={(e) => this.createSet(item.gridOneId, item)} ><img src={invoice} /></button></li>

                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <li className="text-center padRightNone" onClick={(e) => this.copyRow(item.gridOneId, item.otb)} >
                                                                    <img src={copyIcon} />
                                                                    {/*<svg xmlns="http://www.w3.org/2000/svg" onClick={this.deleteSecTwoRows(itemGridTwo.gridTwoId, itemGridTwo.setNo)} width="17" height="17" viewBox="0 0 17 19">
                                                                                            <path fill="#e5e5e5" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                                        </svg>*/}
                                                                </li> : null}

                                                            </ul>
                                                        </td>


                                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >

                                                            <div className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ?
                                                                        <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "divisionName", idx, item.gridOneId)} className="inputTable" value={item.divisionName} onKeyDown={(e) => this._handleKeyPressRow(e, "divisionName", idx)} id={"divisionName" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly className="inputTable " value={item.divisionName} onKeyDown={(e) => this._handleKeyPressRow(e, "divisionName", idx)} id={"divisionName" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "divisionName" + idx, "division", item) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"divisionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "divisionName" + idx, "division", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"divisionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "divisionName" + idx, "division", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "division" ? <div className="calculatedToolTip set-pi-modal-position" > <PoArticleModal {...this.props}{...this.state} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /></div> : null : null : null}*/}
                                                            </div>

                                                        </td>

                                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >
                                                            <div className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ? <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "sectionName", idx, item.gridOneId)} className="inputTable" value={item.sectionName} onKeyDown={(e) => this._handleKeyPressRow(e, "sectionName", idx)} id={"sectionName" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly className="inputTable " value={item.sectionName} onKeyDown={(e) => this._handleKeyPressRow(e, "sectionName", idx)} id={"sectionName" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "sectionName" + idx, "section", item) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"sectionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "sectionName" + idx, "section", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"sectionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "sectionName" + idx, "section", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "section" ? <div className="calculatedToolTip  set-pi-modal-position" > <PoArticleModal {...this.props}{...this.state} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> </div> : null : null : null}*/}
                                                            </div>

                                                        </td>
                                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >
                                                            <div className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ? <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "departmentName", idx, item.gridOneId)} className="inputTable" value={item.departmentName} onKeyDown={(e) => this._handleKeyPressRow(e, "departmentName", idx)} id={"departmentName" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly className="inputTable " value={item.departmentName} onKeyDown={(e) => this._handleKeyPressRow(e, "departmentName", idx)} id={"departmentName" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "departmentName" + idx, "department", item) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"departmentNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "departmentName" + idx, "department", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"departmentNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "departmentName" + idx, "department", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "department" ? <div className="calculatedToolTip  set-pi-modal-position" > <PoArticleModal {...this.props}{...this.state} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> </div> : null : null : null}*/}
                                                            </div>
                                                        </td>

                                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >
                                                            <div className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ?
                                                                        <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "articleCode", idx, item.gridOneId)} className="inputTable" value={item.articleCode} onKeyDown={(e) => this._handleKeyPressRow(e, "articleCode", idx)} id={"articleCode" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly className="inputTable " value={item.articleCode} onKeyDown={(e) => this._handleKeyPressRow(e, "articleCode", idx)} id={"articleCode" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, "articleCode" + idx) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"articleCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, "articleCode" + idx) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"articleCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, "articleCode" + idx) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/* {!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "article" ? <div className="calculatedToolTip  set-pi-modal-position" id="uuid"> <PoArticleModal {...this.props}{...this.state} uuid ={this.state.uuid} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> </div> : null : null : null} */}

                                                            </div>
                                                        </td>
                                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >

                                                            <input autoComplete="off" type="text" readOnly className="inputTable " value={item.articleName} onKeyDown={(e) => this._handleKeyPressRow(e, "articleName", idx)} id={"articleName" + idx} />


                                                        </td>


                                                        {this.state.codeRadio == "poIcode" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >

                                                            <input autoComplete="off" type="text" readOnly className="inputTable " value={item.itemCodeList} onKeyDown={(e) => this._handleKeyPressRow(e, "itemCode", idx)} id={"itemCode" + idx} onClick={(e) => this.openNewIcode(`${item.gridOneId}`, item.articleCode, idx)} />
                                                            <div className="purchaseTableDiv" onClick={(e) => this.openNewIcode(`${item.gridOneId}`, item.articleCode, idx)} >
                                                                {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg> */}

                                                            </div>

                                                        </td> : null}
                                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >
                                                            <div className="pos-rel">
                                                                {!this.state.isModalShow ?
                                                                    <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "hsnCode", idx, item.gridOneId)} className="inputTable" value={item.hsnSacCode} onKeyDown={(e) => this._handleKeyPressRow(e, "hsnCode", idx)} id={"hsnCode" + idx} />

                                                                    : <input autoComplete="off" type="text" readOnly className="inputTable " value={item.hsnSacCode} onKeyDown={(e) => this._handleKeyPressRow(e, "hsnCode", idx)} id={"hsnCode" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openHsnCodeModal(item.gridOneId, item.articleCode, idx, item.departmentCode, item.hsnSacCode, "hsnCode" + idx) : null} />}
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"hsnCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openHsnCodeModal(item.gridOneId, item.articleCode, idx, item.departmentCode, item.hsnSacCode, "hsnCode" + idx) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"hsnCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openHsnCodeModal(item.gridOneId, item.articleCode, idx, item.departmentCode, item.hsnSacCode, "hsnCode" + idx) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/* {!this.state.isModalShow ? this.state.hsnModal ?<div className="calculatedToolTip set-pi-modal-position" >  <HsnCodeModal {...this.props} {...this.state} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /> </div>: null : null} */}
                                                            </div>

                                                        </td>
                                                        {this.state.isMrpRangeDisplay ? <td className="pad-0 hoverTable">
                                                            <label className="purchaseTableLabel">
                                                                {item.mrpStart}-{item.mrpEnd}
                                                            </label>
                                                        </td> : null}


                                                        {this.state.isMrpRequired ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >
                                                            <div className="pos-rel">
                                                                {this.state.isModalShow ?
                                                                    <input autoComplete="off" type="text" readOnly className="inputTable " value={item.vendorMrp} onKeyDown={(e) => this._handleKeyPressRow(e, "vendorMrp", idx)} id={"vendorMrp" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openItemCode(e, `${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp", idx, `${item.hsnSacCode}`, `${item.mrpStart}`, `${item.mrpEnd}`, `${item.articleCode}`
                                                                    ) : null} />
                                                                    : <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "vendorMrp", idx, item.gridOneId)} className="inputTable" value={item.vendorMrp} onKeyDown={(e) => this._handleKeyPressRow(e, "vendorMrp", idx)} id={"vendorMrp" + idx} />}
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"vendorMrpDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openItemCode(e, `${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp", idx, `${item.hsnSacCode}`, `${item.mrpStart}`, `${item.mrpEnd}`, `${item.articleCode}`) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"vendorMrpDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openItemCode(e, `${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp", idx, `${item.hsnSacCode}`, `${item.mrpStart}`, `${item.mrpEnd}`, `${item.articleCode}`) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.itemModal ?<div className="calculatedToolTip set-pi-modal-position" >  <ItemCodeModal {...this.props} {...this.state} desc6={this.state.desc6} poRows={this.state.poRows} descId={this.state.descId} itemModalAnimation={this.state.itemModalAnimation} startRange={this.state.startRange} endRange={this.state.endRange} code={this.state.hl4Code} openItemCode={() => this.openItemCode()} closeItemmModal={() => this.closeItemmModal()} updateItem={(e) => this.updateItem(e)} /> </div>: "" : null}*/}

                                                            </div>
                                                        </td> : null}
                                                        {!this.state.isVendorDesignNotReq ? <td className="pad-0 hoverTable tdFocus inputBgLiteY">

                                                            <input autoComplete="off" type="text" name="vendorDesign" id={"vendorDesign" + idx} className="inputTable " onKeyDown={(e) => this._handleKeyPressRow(e, "vendorDesign", idx)} value={item.vendorDesign} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />

                                                        </td> : null}

                                                        {this.state.isRspRequired ? <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" readOnly type="text" className="inputTable" id={"rsp" + idx} value={item.rsp} onKeyDown={(e) => this._handleKeyPressRow(e, "rsp", idx)} />
                                                        </td> : null}

                                                        {this.state.isMRPEditable ? <td className={this.state.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? "errorBorder pad-0" : "pad-0 tdFocus inputBgLiteY" : "pad-0 tdFocus inputBgLiteY"}>
                                                            <label className="rateHover" id={"toolIdMrp" + item.gridOneId} onMouseOver={this.state.codeRadio != "setBased" ? (e) => this.mouseOverCmprMrp(`${item.gridOneId}`) : null}>
                                                                <input autoComplete="off" type="text" className="inputTable" id={"mrp" + idx} onKeyDown={(e) => this._handleKeyPressRow(e, "mrp", idx)} pattern="[0-9]+([\.][0-9]{0,2})?" value={item.mrp} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                                {this.state.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? <span> <div className="showRate" id={"moveLeftMrp" + item.gridOneId}><p>Rsp cannot be greater then Mrp</p></div></span> : "" : ""}
                                                            </label>
                                                        </td> : null}

                                                        <td className={this.state.isMrpRequired || this.state.isMRPEditable ? Number(item.rate) > Number(item.mrp) ? "errorBorder pad-0" : "pad-0 tdFocus inputBgLiteY" : "pad-0 tdFocus inputBgLiteY"}>
                                                            <label className="rateHover" id={"toolIdRate" + item.gridOneId} onMouseOver={this.state.codeRadio != "setBased" ? (e) => this.mouseOverCmprRate(`${item.gridOneId}`) : null}>
                                                                <input autoComplete="off" type="text" className="inputTable" id={"rate" + idx} pattern="[0-9.]*" value={item.rate} onFocus={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.rateValueOnFocus(e, item.gridOneId, "rate", item.finalRate) : null} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} onBlur={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.onBlurRate(`${item.gridOneId}`, e) : null} />
                                                                {this.state.isMrpRequired || this.state.isMRPEditable ? Number(item.rate) > Number(item.mrp) ? <span> <div className="showRate" id={"moveLeftRate" + item.gridOneId}><p>Rate cannot be greater then Mrp</p></div></span> : "" : ""}
                                                            </label>
                                                        </td>
                                                        {this.state.isDiscountAvail ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus inputBgLiteY" >
                                                            {this.state.isModalShow ? <input autoComplete="off" type="text" readOnly className="inputTable" value={item.discount.discountType} onKeyDown={(e) => this._handleKeyPressRow(e, "discount", idx)} id={"discount" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || (this.state.codeRadio == "poIcode") ? (e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.gridOneId}`, "discount" + idx) : null} />

                                                                : <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "discount", idx, item.gridOneId)} className="inputTable dis-input" value={item.discount.discountType} onKeyDown={(e) => this._handleKeyPressRow(e, "discount", idx)} id={"discount" + idx} />}
                                                            {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"discountDiv" + idx} onClick={(this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode") && this.state.isDiscountMap ? (e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.gridOneId}`, "discount" + idx) : null} >
                                                                {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg> */}
                                                            </div> : null}
                                                            <div className="srb-content2">
                                                                <label className="clr-name">%
                                                                    <input type="checkbox" checked={item.discount.discountPer} onChange={(e) => this.discountPer(item.gridOneId, item.discount.discountPer)} />
                                                                    <span className="checkmark"></span>
                                                                </label>
                                                            </div>

                                                        </td> : null}
                                                        {this.state.isDisplayFinalRate ? <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" type="text" readOnly className="inputTable" id="finalRate" value={item.finalRate} />
                                                        </td> : null}
                                                        <td className="pad-0 pad-0 hoverTable openModalBlueBtn openModalBlueBtn tdFocus displayPointer inputBgLiteY" onClick={this.state.codeRadio != "setBased" ? (e) =>
                                                            this.openImageModal(`${item.gridOneId}`, "image" + idx, item.articleCode, idx) : null}>
                                                            <label className="piToolTip">
                                                                <div className="topToolTip">
                                                                    <input type="text" className="imageInput" readOnly onKeyDown={this.state.codeRadio != "setBased" ? (e) => this._handleKeyPressRow(e, "image", idx) : null} id={"image" + idx} value={item.image != undefined ? item.image.join(',') : ""} />
                                                                    {/* <label>{item.image != undefined ? item.image.join(',') : ""}</label> */}
                                                                    {item.image != undefined ? <span className="topToolTipText"> {item.image.join(',')}</span> : null}
                                                                </div>
                                                            </label>
                                                            {this.state.codeRadio != "setBased" ? <div className="purchaseTableDiv" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="34" height="44" viewBox="0 0 33 43">
                                                                    <g fill="none" fillRule="nonzero">
                                                                        <path fill="#fffae7" d="M0 0h33v43H0z" />
                                                                        <path fill="#000" d="M13.143 22.491A2.847 2.847 0 0 1 16 19.635a2.847 2.847 0 0 1 2.857 2.856A2.847 2.847 0 0 1 16 25.348a2.847 2.847 0 0 1-2.857-2.857zm-6.026 4.05v-8.236c0-.94.763-1.703 1.703-1.703h3.404l.156-.685c.255-1.056 1.174-1.8 2.25-1.8h2.7c1.077 0 2.016.744 2.25 1.8l.157.685h3.404c.94 0 1.702.763 1.702 1.702v8.237c0 .998-.802 1.82-1.82 1.82H8.938c-.998 0-1.82-.822-1.82-1.82zm4.676-4.05A4.2 4.2 0 0 0 16 26.698a4.2 4.2 0 0 0 4.207-4.207A4.183 4.183 0 0 0 16 18.304a4.196 4.196 0 0 0-4.207 4.187zm-2.817-3.443c0 .47.391.86.861.86s.86-.39.86-.86-.39-.861-.86-.861c-.49.02-.86.391-.86.86z" />
                                                                    </g>
                                                                </svg>
                                                            </div> : null}
                                                        </td>
                                                        {this.state.displayOtb ? <td className={item.otb < item.amount ? "errorBorder pad-0" : "pad-0 tdFocus"}>
                                                            <input autoComplete="off" readOnly type="text" className="inputTable" id="otb" value={item.otb} onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                        </td> : null}

                                                        <td className="pad-0 positionRelative tdFocus editDateFormat">
                                                            <input type="date" className="inputTable height0" min={this.state.minDate} placeholder="" id={"date" + idx} onChange={(e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx)} name="date" value={item.deliveryDate} />
                                                        </td>

                                                        {this.state.isDisplayMarginRule ? <td className="typeOfBuying tdFocus td-disabled">
                                                            <label> {item.marginRule}</label>
                                                        </td> : null}
                                                        <td className="pad-0 tdFocus inputBgLiteY">
                                                            <input autoComplete="off" type="text" className="inputTable" id={"remarks" + idx} value={item.remarks} onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                        </td>
                                                        <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" disabled className="inputTable widthauto" id="quantity" value={item.quantity} />
                                                        </td>
                                                        {this.state.isBaseAmountActive ? <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" disabled className="inputTable widthauto" id="basic" value={item.basic} />
                                                        </td> : null}
                                                        <td className="pad-0 posRelative poToolTipMain tdFocus td-disabled">
                                                            <label>
                                                                <input autoComplete="off" type="text" disabled className="inputTable widthauto" id="amount" value={item.amount} />
                                                                {/*<span className="netAmtHover netAmtTotal displayInline displayPointer">
                                                                    {item.amount != "" ? <img id={"toolId" + item.gridOneId} className="exclaimIconToolTip" onMouseOver={(e) => this.calculatedMargin(`toolId${item.gridOneId}`, `moveLeft${item.gridOneId}`)} src={exclaimIcon} />
                                                                        : null}
                                                                    <div id={"moveLeft" + item.gridOneId} className="totalAmtDescDrop">
                                                                        <div className="amtContent">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr><th><p>Charge Name</p></th>
                                                                                        <th><p>% / Amount</p></th>
                                                                                        <th><p>Total</p></th></tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    {item.finCharges.map((data, key) => (
                                                                                        <tr key={key}>
                                                                                            <td><p>{data.chgName}</p></td>
                                                                                            <td><p>{data.rates != undefined ? <span>{data.rates.igstRate} </span> : ""}</p>{}</td>
                                                                                            <td><p>{data.charges != undefined ? <span>{data.charges.sign}{data.charges.chargeAmount}</span> : data.sign + data.finChgRate}</p>

                                                                                            </td>
                                                                                        </tr>))}
                                                                                </tbody>

                                                                            </table>
                                                                        </div>
                                                                        <div className="amtBottom">
                                                                            <div className="col-md-12">
                                                                                <div className="col-md-6 pad-lft-7">

                                                                                    <span>Basic</span>
                                                                                </div>
                                                                                <div className="col-lg-6 col-md-6 textRight padRightNone">

                                                                                    <span className="totalAmtt">+{item.basic}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="amtBottom">
                                                                            <div className="col-lg-12 col-md-12 pad-top-5 m-top-5">
                                                                                <div className="col-lg-6 col-md-6">

                                                                                    <p>Grand Total</p>
                                                                                </div>
                                                                                <div className="col-lg-6 col-md-6 textRight padRightNone">

                                                                                    <p className="totalAmt">{item.amount}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </span>*/}
                                                            </label>
                                                        </td>
                                                        {!this.state.isTaxDisable ? <td className="pad-0 tdFocus td-disabled">
                                                            <label className="piToolTip m0 poToolTip">
                                                                <div className="topToolTip">
                                                                    <label>{item.tax.length != 0 ? item.tax.join(',') : ""}</label>
                                                                    <span className="topToolTipText">{item.tax.length != 0 ? item.tax.join(',') : ""}</span>
                                                                </div>
                                                            </label>
                                                            {/*<input autoComplete="off" type="text" className="inputTable" id="tax" value={item.tax.length != 0 ? item.tax.join(',') : ""} />*/}
                                                        </td> : null}

                                                        {this.state.isRspRequired ? <td className="pad-0 posRelative poToolTipMain tdFocus td-disabled">
                                                            <label>
                                                                <label className="piToolTip m0 poToolTip">
                                                                    <div className="topToolTip">
                                                                        <label className="max-width80">{item.calculatedMargin.length != 0 ? item.calculatedMargin.join(',') : ""}</label>
                                                                        <span className="topToolTipText">{item.calculatedMargin.length != 0 ? item.calculatedMargin.join(',') : ""}</span>
                                                                    </div>
                                                                </label>
                                                                {/*<input autoComplete="off" type="text" className="inputTable" id="calculatedMargin" value={item.calculatedMargin.length != 0 ? item.calculatedMargin.join(',') : ""} />*/}
                                                                {/*<span className="calculatedMargin displayInline displayPointer ">
                                                                    {item.calculatedMargin != "" ? <img src={exclaimIcon} className="exclaimIconToolTip" id={"marginIdSet" + item.gridOneId} onMouseOver={(e) => this.calculatedMargin(`marginIdSet${item.gridOneId}`, `marginLeft${item.gridOneId}`)} /> : null}
                                                                    <div className="calculatedToolTip totalAmtDescDrop" id={"marginLeft" + item.gridOneId}>
                                                                        <div className="formula">
                                                                            <h3>Calculated Margin</h3>
                                                                            <h5>RSP-COST <span>X</span> <span className="pad-0">100</span><p>RSP</p></h5>
                                                                        </div>
                                                                    </div>
                                                                </span>*/}
                                                            </label>
                                                        </td> : null}

                                                        {!this.state.isGSTDisable ? <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" disabled className="inputTable widthauto" id="gst" value={item.gst.length != 0 ? item.gst.join(',') : ""} />
                                                        </td> : null}
                                                        {this.state.isMrpRequired ? <td className="pad-0 posRelative poToolTipMain tdFocus td-disabled">
                                                            <label>
                                                                <label className="piToolTip m0 poToolTip">
                                                                    <div className="topToolTip">
                                                                        <label className="max-width80">{item.mrk.length != 0 ? item.mrk.join(',') : ""}</label>
                                                                        <span className="topToolTipText">{item.mrk.length != 0 ? item.mrk.join(',') : ""}</span>
                                                                    </div>
                                                                </label>
                                                                {/*<input autoComplete="off" type="text" className="inputTable" id="mrk" value={item.mrk.length != 0 ? item.mrk.join(',') : ""} />*/}
                                                                {/*<span className="calculatedMargin displayInline displayPointer inTakeMargin">
                                                                    {item.mrk != "" ? <img src={exclaimIcon} className="exclaimIconToolTip" id={"intakeMarginSet" + item.gridOneId} onMouseOver={(e) => this.calculatedMargin(`intakeMarginSet${item.gridOneId}`, `inTakeMarginLeft${item.gridOneId}`)} />
                                                                        : null}
                                                                    <div className="calculatedToolTip totalAmtDescDrop" id={"inTakeMarginLeft" + item.gridOneId}>
                                                                        <div className="formula">
                                                                            <h3>Intake Margins</h3>
                                                                            <h5><span>RSP</span> <span>X</span> <span className="pad-0">100</span><p className="pad-lft-0">100+COST</p></h5>
                                                                        </div>
                                                                    </div>
                                                                </span>*/}
                                                            </label>
                                                        </td> : null}

                                                    </tr>
                                                ))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                {/* __________________________________ITEM CODE TABLE END____________________ */}
                                {this.state.addRow ? <div className="newRowAdd m-top-10">
                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <button onClick={this.addRow} type="button" id="addIndentRow" className="zuiBtn">
                                        Add Row
                        </button> : <button type="button" className="zuiBtn btnDisabled">
                                            Add Row
                        </button>}
                                </div> : null}



                            </div>
                            <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                <div className="footerDivForm displayFlex">
                                    <div className="col-md-6 alignMiddle">
                                        <ul className="list-inline m-lft-0 m-top-10">
                                            <li>{this.state.dateValidationRes || this.state.supplier == "" ? <button className="clear_button_vendor btnDisabled" type="reset" disabled>Clear</button> : <button className="clear_button_vendor" type="reset" onClick={(e) => this.reset(e)}>Clear</button>} </li>
                                            <li>{this.state.dateValidationRes || this.state.supplier == "" ? <button className="save_button_vendor btnDisabled" type="button" disabled>Save</button> : <button className="save_button_vendor" id="saveButton" type="button" onClick={(e) => this.debounceFun(e)} >Save</button>} </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-6 poAmountBottom">
                                        <ul>
                                            <li><label>PO Quantity -</label><span>{this.state.poQuantity}</span></li>
                                            <li><label>PO Amount -</label><span>{this.state.poAmount}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {this.state.discountModal ? <DiscountModal {...this.props}{...this.state} disId={this.state.disId} discountSearch={this.state.discountSearch} selectedDiscount={this.state.selectedDiscount} discountGrid={this.state.discountGrid} closeDiscountModal={(e) => this.closeDiscountModal(e)} isDiscountMap={this.state.isDiscountMap} vendorMrpPo={this.state.vendorMrpPo} updateDiscountModal={(e) => this.updateDiscountModal(e)} discountMrp={this.state.discountMrp} /> : null}

                            {this.state.poArticle ? <PoArticleModal {...this.props}{...this.state} uuid={this.state.uuid} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> : null}

                            {this.state.itemModal ? <ItemCodeModal {...this.props} {...this.state} mrpId={this.state.mrpId} desc6={this.state.desc6} poRows={this.state.poRows} descId={this.state.descId} itemModalAnimation={this.state.itemModalAnimation} startRange={this.state.startRange} endRange={this.state.endRange} code={this.state.hl4Code} openItemCode={() => this.openItemCode()} closeItemmModal={() => this.closeItemmModal()} updateItem={(e) => this.updateItem(e)} /> : null}
                            {this.state.hsnModal ? <HsnCodeModal {...this.props} {...this.state} hsnId={this.state.hsnId} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /> : null}
                        </div>
                    </div>
                </form>
                {this.state.isModalShow ? this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} isTransporterDependent={this.state.isTransporterDependent} city={this.state.city} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null : null}
                {this.state.itemBarcodeModal ? <NewIcodeModal {...this.props} {...this.state} icodeModalAnimation={this.state.icodeModalAnimation} articleCode={this.state.hl4Code} supplierCode={this.state.slCode} siteCode={this.state.siteCode} closeNewIcode={(e) => this.closeNewIcode(e)} itemCodeList={this.state.itemCodeList} updateNewIcode={(e) => this.updateNewIcode(e)} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

                {this.state.departmentModal ? <SetDepartmentModal {...this.props} {...this.state} onCloseDepartmentModal={(e) => this.onCloseDepartmentModal(e)} updateDepartment={(e) => this.updateDepartment(e)} departmentSetBasedAnimation={this.state.departmentSetBasedAnimation} setDepartment={this.state.setDepartment} hl3CodeDepartment={this.state.hl3CodeDepartment} /> : null}
                {this.state.isModalShow ? this.state.setVendor ? <SetVendorModal {...this.props} {...this.state} onCloseVendor={(e) => this.onCloseVendor(e)} updateVendorPo={(e) => this.updateVendorPo(e)} /> : null : null}
                {this.state.isModalShow ? this.state.orderNumber ? <OrderNumberModal {...this.props} {...this.state} orderSet={this.state.orderSet} hl3CodeDepartment={this.state.hl3CodeDepartment} poSetVendorCode={this.state.poSetVendorCode} updateOrderNumber={(e) => this.updateOrderNumber(e)} orderSet={this.state.orderSet} departmentSet={this.state.departmentSet} poSetVendor={this.state.poSetVendor} closeSetNumber={(e) => this.closeSetNumber(e)} orderNumberModalAnimation={this.state.orderNumberModalAnimation} /> : null : null}
                {this.state.isModalShow ? this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteName={this.state.siteName} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null : null}
                {this.state.isModalShow ? this.state.cityModal ? <CityModal city={this.state.city} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.confirmModal ? <ConfirmModalPo {...this.props} {...this.state} pi={this.state.pi} radioChange={this.state.radioChange} onnRadioChange={(e) => this.onnRadioChange(e)} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
                {this.state.isModalShow ? this.state.loadIndentModal ? <LoadIndent {...this.props} {...this.state} indentValue={this.state.indentValue} updateLoadIndentState={(e) => this.updateLoadIndentState(e)} loadIndentCloseModal={(e) => this.openloadIndentSelection(e)} onCloseLoadIndent={(e) => this.onCloseLoadIndent(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null : null}
                {this.state.isModalShow ? this.state.supplierModal ? <SupplierModal {...this.props} {...this.state} city={this.state.city} departmentCode="" siteCode={this.state.siteCode} department="" supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null : null}
                {/* { this.state.poArticle ? <PoArticleModal {...this.props}{...this.state} uuid ={this.state.uuid}  copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> : null} */}
                {this.state.setModal ? <SetModal ref={instance => { this.escChil = instance }} copingFocus={(e) => this.copingFocus(e)} updateFocusId={(e) => this.updateFocusId(e)} validateSetUdf={(e) => this.validateSetUdf(e)} onEsc={this.onEsc} isSet={this.state.isSet} setModalAnimation={this.state.setModalAnimation}  {...this.props} {...this.state} updatePo={(e) => this.updatePo(e)} copingColor={(e) => this.copingColor(e)} deleteLineItem={(e) => this.deleteLineItem(e)} closeSetModal={(e) => this.closeSetModal(e)} updatePoRows={(e) => this.updatePoRows(e)} validateCatDescRow={(e) => this.validateCatDescRow(e)} gridSecond={(e) => this.gridSecond(e)}
                    validateItemdesc={(e) => this.validateItemdesc(e)} getOtbForPoRows={(e) => this.getOtbForPoRows(e)} gridThird={(e) => this.gridThird(e)} gridFourth={(e) => this.gridFourth(e)} validateLineItem={(e) => this.validateLineItem(e)} gridFivth={() => this.gridFivth()} updatePoAmountNpoQuantity={(e) => this.updatePoAmountNpoQuantity(e)} updateLineItemChange={(e) => this.updateLineItemChange(e)} /> : null}
                {this.state.imageModal ? <PiImageModal {...this.state} {...this.props} file={this.state.imageState} imageName={this.state.imageName} updateImage={(e) => this.updateImage(e)} imageModalAnimation={this.state.imageModalAnimation} closePiImageModal={(e) => this.closePiImageModal(e)} closeImageModal={(e) => this.openImageModal(e)} imageRowId={this.state.imageRowId} /> : null}
                {this.state.resetAndDelete ? <ResetAndDelete {...this.props} {...this.state} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeResetDeleteModal={(e) => this.closeResetDeleteModal(e)} DeleteRowPo={(e) => this.DeleteRowPo(e)} resetRows={(e) => this.resetRows(e)} /> : null}
                {this.state.deleteConfirmModal ? <DeleteModalPo closeDeleteConfirmModal={(e) => this.closeDeleteConfirmModal(e)} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} deleteLineItem={(e) => this.deleteLineItem(e)} handleRemoveSpecificSecRow={(e) => this.handleRemoveSpecificSecRow(e)} deleteGridId={this.state.deleteGridId} deleteSetNo={this.state.deleteSetNo} /> : null}
                {this.state.multipleErrorpo ? <AlertNotificationModal closeMultipleError={(e) => this.closeMultipleError(e)} lineError={this.state.lineError} /> : null}

            </div>
        );
    }
}
export default GenericPurchaseIndent;