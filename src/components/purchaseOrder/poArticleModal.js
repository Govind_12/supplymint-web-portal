import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class PoArticleModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {

            type: 1,
            no: 1,
            searchMrp: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            poArticleData: [],
            searchBy: "startWith",
            vendorMrpPoData: {},
            isUDFExist: "false",
            itemUdfExist: "false",
            isSiteExist: "false",
            isOtbValidation: "false",
            copyColor: "false",
            focusedLi: "",
            modalDisplay: false,

        }
    }
    componentDidMount() {
        if (this.props.isModalShow) {

            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                // this.textInput.current.focus();
            }
        }



    }
    componentWillMount() {
        this.setState({
            id: this.props.rowId,
            // vendorMrpPoData: this.props.hl1Code + this.props.hl2Code + this.props.hl3Code + this.props.hl4Code + this.props.mrpStart + this.props.mrpEnd,
            searchMrp: this.props.isModalShow ? "" : this.props.poArticleSearch,
            type: this.props.isModalShow || this.props.poArticleSearch == "" ? 1 : 3



        })

    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.poArticle.isSuccess) {
            let c = []
            if (nextProps.purchaseIndent.poArticle.data.resource != null) {
                this.setState({
                    poArticleData: nextProps.purchaseIndent.poArticle.data.resource,
                    prev: nextProps.purchaseIndent.poArticle.data.prePage,
                    current: nextProps.purchaseIndent.poArticle.data.currPage,
                    next: nextProps.purchaseIndent.poArticle.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.poArticle.data.maxPage,
                    isUDFExist: nextProps.purchaseIndent.poArticle.data.isUDFExist,
                    itemUdfExist: nextProps.purchaseIndent.poArticle.data.itemUdfExist,
                    isSiteExist: nextProps.purchaseIndent.poArticle.data.isSiteExist,
                    isOtbValidation: nextProps.purchaseIndent.poArticle.data.otbValidation,
                    copyColor: nextProps.purchaseIndent.poArticle.data.copyColor
                })
            }
            else {
                this.setState({
                    poArticleData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,

                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {
                    this.textInput.current.focus();

                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.poArticle.data.resource != null) {
                        let data = nextProps.purchaseIndent.poArticle.data.resource[0]
                        this.setState({
                            focusedLi: data.id,
                            searchMrp: this.props.isModalShow ? "" : this.props.poArticleSearch,
                            type: this.props.isModalShow || this.props.poArticleSearch == "" ? 1 : 3

                        })
                        document.getElementById(data.id) != null ? document.getElementById(data.id).focus() : null
                    }



                }
            }
            this.setState({
                modalDisplay: true,

            })
        }
    }


    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }


    selectedData(e, data) {
        let c = this.state.poArticleData;
        for (let i = 0; i < c.length; i++) {
            if (c[i].id == e) {

                this.setState({
                    vendorMrpPoData: e
                }, () => {
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
                })
            }
        }

    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.prePage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.poArticle.data.prePage,

                    hl4Code: this.props.hl4Code,
                    hl4Name: this.props.hl4Name,
                    hl1Name: this.props.hl1Name,
                    hl2Name: this.props.hl2Name,
                    hl3Name: this.props.hl3Name,
                    hl3Code: this.props.hl3Code,
                    divisionSearch: this.props.basedOn == "division" ? this.state.searchMrp : "",
                    sectionSearch: this.props.basedOn == "section" ? this.state.searchMrp : "",
                    departmentSearch: this.props.basedOn == "department" ? this.state.searchMrp : "",
                    articleSearch: this.props.basedOn == "article" ? this.state.searchMrp : "",
                    basedOn: this.props.basedOn,
                    supplier: this.props.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: this.props.mrpRangeSearch
                }
                this.props.poArticleRequest(data);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.currPage != this.props.purchaseIndent.poArticle.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.poArticle.data.currPage + 1,
                    hl4Code: this.props.hl4Code,
                    hl4Name: this.props.hl4Name,
                    hl1Name: this.props.hl1Name,
                    hl2Name: this.props.hl2Name,
                    hl3Name: this.props.hl3Name,
                    hl3Code: this.props.hl3Code,
                    divisionSearch: this.props.basedOn == "division" ? this.state.searchMrp : "",
                    sectionSearch: this.props.basedOn == "section" ? this.state.searchMrp : "",
                    departmentSearch: this.props.basedOn == "department" ? this.state.searchMrp : "",
                    articleSearch: this.props.basedOn == "article" ? this.state.searchMrp : "",
                    basedOn: this.props.basedOn,
                    supplier: this.props.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: this.props.mrpRangeSearch
                }
                this.props.poArticleRequest(data)
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }

        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.currPage <= this.props.purchaseIndent.poArticle.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    hl4Code: this.props.hl4Code,
                    hl4Name: this.props.hl4Name,
                    hl1Name: this.props.hl1Name,
                    hl2Name: this.props.hl2Name,
                    hl3Name: this.props.hl3Name,
                    hl3Code: this.props.hl3Code,
                    divisionSearch: this.props.basedOn == "division" ? this.state.searchMrp : "",
                    sectionSearch: this.props.basedOn == "section" ? this.state.searchMrp : "",
                    departmentSearch: this.props.basedOn == "department" ? this.state.searchMrp : "",
                    articleSearch: this.props.basedOn == "article" ? this.state.searchMrp : "",
                    basedOn: this.props.basedOn,
                    supplier: this.props.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: this.props.mrpRangeSearch
                }
                this.props.poArticleRequest(data)
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }

        }
    }

    onDone() {
        let flag = false;
        let c = this.state.poArticleData;
        let idd = "";
        let articleCode = ""
        let division = ""
        let divisionCode = ""
        let sectionCode = ""
        let section = ""
        let department = ""
        let departmentCode = ""
        let mrpStart = ""
        let mrpEnd = ""
        let articleName = ""
        let hsnSacCode = ""
        let data = {}
        if (c != undefined) {
            for (let i = 0; i < c.length; i++) {
                if (this.props.isModalShow ? this.state.vendorMrpPoData != "" : this.state.vendorMrpPoData != "" || this.props.poArticleSearch == this.state.searchMrp || this.props.poArticleSearch != this.state.searchMrp) {
                    if (c[i].id == this.state.vendorMrpPoData) {
                        // document.getElementById("mrpBasedOn").value=""
                        idd = c[i].id
                        articleCode = c[i].hl4Code ? c[i].hl4Code : ""
                        division = c[i].hl1Name ? c[i].hl1Name : ""
                        divisionCode = c[i].hl1Code ? c[i].hl1Code : ""
                        section = c[i].hl2Name ? c[i].hl2Name : ""
                        sectionCode = c[i].hl2Code ? c[i].hl2Code : ""
                        department = c[i].hl3Name ? c[i].hl3Name : ""
                        departmentCode = c[i].hl3Code ? c[i].hl3Code : ""
                        mrpStart = c[i].mrpStart == 0 ? 0 : c[i].mrpStart != undefined ? c[i].mrpStart : ""
                        mrpEnd = c[i].mrpEnd ? c[i].mrpEnd : ""
                        articleName = c[i].hl4Name ? c[i].hl4Name : "",
                            flag = true
                        break
                    }
                }
                else {
                    flag = false
                }
            }
        } else {
            this.setState({
                done: true
            })
        }
        if (flag) {
            this.setState({
                done: true,
                searchMrp: "",
            })
            data = {
                id: idd,

                articleCode: articleCode,
                division: division,
                divisionCode: divisionCode,
                section: section,
                sectionCode: sectionCode,
                department: department,
                departmentCode: departmentCode,
                mrpStart: mrpStart,
                mrpEnd: mrpEnd,
                articleName: articleName,
                isUDFExist: this.state.isUDFExist,
                isSiteExist: this.state.isSiteExist,
                itemUdfExist: this.state.itemUdfExist,

                isOtbValidation: this.state.isOtbValidation,
                copyColor: this.state.copyColor
            }

            setTimeout(() => {

                this.props.updateMrpState(data);
                this.onMrpCloseModal()
            }, 10)
        } else {
            this.setState({
                toastMsg: "Select Data",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
            this.setState({
                done: false
            })
        }

        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    onMrpCloseModal(e) {
        this.setState({
            searchMrp: "",
            vendorMrpPoData: {},
            type: 1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            poArticleData: []
        })
        this.props.closePOArticle(e)
        this.props.poArticleClear()
    }
    onSearch(e) {
        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {

            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                hl4Code: this.props.hl4Code,
                hl4Name: this.props.hl4Name,
                hl1Name: this.props.hl1Name,
                hl2Name: this.props.hl2Name,
                hl3Name: this.props.hl3Name,
                hl3Code: this.props.hl3Code,
                divisionSearch: this.props.basedOn == "division" ? this.state.searchMrp : "",
                sectionSearch: this.props.basedOn == "section" ? this.state.searchMrp : "",
                departmentSearch: this.props.basedOn == "department" ? this.state.searchMrp : "",
                articleSearch: this.props.basedOn == "article" ? this.state.searchMrp : "",
                basedOn: this.props.basedOn,
                supplier: this.props.slCode,
                searchBy: this.state.searchBy,
                mrpSearch: "",
                isMrp: this.props.mrpRangeSearch
            }
            this.props.poArticleRequest(data)

        }
    }
    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                sectionSelection: "",
                type: "",
                no: 1
            }, () => {

                if (this.state.type == 2 || this.state.type == 3) {
                    var data = {
                        type: "",
                        no: 1,
                        hl4Code: this.props.hl4Code,
                        hl4Name: this.props.hl4Name,
                        hl1Name: this.props.hl1Name,
                        hl2Name: this.props.hl2Name,
                        hl3Name: this.props.hl3Name,
                        hl3Code: this.props.hl3Code,
                        divisionSearch: this.props.basedOn == "division" ? this.state.searchMrp : "",
                        sectionSearch: this.props.basedOn == "section" ? this.state.searchMrp : "",
                        departmentSearch: this.props.basedOn == "department" ? this.state.searchMrp : "",
                        articleSearch: this.props.basedOn == "article" ? this.state.searchMrp : "",
                        basedOn: this.props.basedOn,
                        supplier: this.props.slCode,
                        searchBy: this.state.searchBy,
                        mrpSearch: "",
                        isMrp: this.props.mrpRangeSearch
                    }
                    this.props.poArticleRequest(data)
                }
            })
        { this.props.isModalShow ? document.getElementById("searchArticle").focus() : null }
    }

    handleChange(e) {
        if (e.target.id == "searchByarticle") {
            this.setState(
                {
                    searchBy: e.target.value
                }, () => {
                    if (this.state.searchMrp != "") {
                        let data = {
                            type: this.state.type,
                            no: 1,
                            hl4Code: this.props.hl4Code,
                            hl4Name: this.props.hl4Name,
                            hl1Name: this.props.hl1Name,
                            hl2Name: this.props.hl2Name,
                            hl3Name: this.props.hl3Name,
                            hl3Code: this.props.hl3Code,
                            divisionSearch: this.props.basedOn == "division" ? this.state.searchMrp : "",
                            sectionSearch: this.props.basedOn == "section" ? this.state.searchMrp : "",
                            departmentSearch: this.props.basedOn == "department" ? this.state.searchMrp : "",
                            articleSearch: this.props.basedOn == "article" ? this.state.searchMrp : "",
                            basedOn: this.props.basedOn,
                            supplier: this.props.slCode,
                            searchBy: this.state.searchBy,
                            mrpSearch: "",
                            isMrp: this.props.mrpRangeSearch
                        }
                        this.props.poArticleRequest(data);
                    }
                }

            );
        } else if (e.target.id == "searchArticle") {
            this.setState({
                searchMrp: e.target.value
            })
        }
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    _handleKeyDown = (e) => {
        let c = this.state.poArticleData
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.poArticleData.length != 0) {
                    this.setState({
                        // selectedId: this.state.poArticleData[0].hl4Code,
                        vendorMrpPoData: c[0].id
                    })
                    // let vendorMrp = c[0].hl1Code + c[0].hl2Code + c[0].hl3Code + c[0].hl4Code + c[0].mrpStart + c[0].mrpEnd

                    window.setTimeout(function () {
                        document.getElementById("searchArticle").blur();

                        document.getElementById(c[0].id).focus()
                    }, 0)
                } else {
                    window.setTimeout(function () {
                        document.getElementById("searchArticle").blur();
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }

            if (e.target.value != "") {
                document.getElementById("searchArticle").blur()
                document.getElementById("findButton").focus()
            }
        }
        if (e.key == "ArrowDown") {
            if (this.state.poArticleData.length != 0) {
                this.setState({
                    // selectedId: this.state.poArticleData[0].hl4Code,
                    vendorMrpPoData: c[0].id
                })
                // let vendorMrp = c[0].hl1Code + c[0].hl2Code + c[0].hl3Code + c[0].hl4Code + c[0].mrpStart + c[0].mrpEnd

                window.setTimeout(function () {
                    document.getElementById("searchArticle").blur();

                    document.getElementById(c[0].id).focus()
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("searchArticle").blur();
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        let c = this.state.poArticleData
        if (e.key === "Tab") {
            // let code = c[0].hl1Code + c[0].hl2Code + c[0].hl3Code + c[0].hl4Code + c[0].mrpStart + c[0].mrpEnd
            window.setTimeout(function () {
                document.getElementById(c[0].id).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.onMrpCloseModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("searchArticle").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        let c = this.state.poArticleData
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.poArticleData.length != 0) {
                this.setState({
                    // selectedId: this.state.poArticleData[0].hl4Code,
                    vendorMrpPoData: c[0].id
                })
                // let code = c[0].hl1Code + c[0].hl2Code + c[0].hl3Code + c[0].hl4Code + c[0].mrpStart + c[0].mrpEnd
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(c[0].id).focus()
                }, 0)
            }
            else {
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById("searchArticle").focus()
                }, 0)
            }
        }
    }
    selectLi(e, code, data) {
        let poArticleData = this.state.poArticleData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < poArticleData.length; i++) {
                if (poArticleData[i].id == code) {
                    index = i
                }
            }
            if (index < poArticleData.length - 1 || index == 0) {
                document.getElementById(poArticleData[index + 1].id) != null ? document.getElementById(poArticleData[index + 1].id).focus() : null

                this.setState({
                    focusedLi: poArticleData[index + 1].id
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < poArticleData.length; i++) {
                if (poArticleData[i].id == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(poArticleData[index - 1].id) != null ? document.getElementById(poArticleData[index - 1].id).focus() : null

                this.setState({
                    focusedLi: poArticleData[index - 1].id
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code, data)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.onMrpCloseModal(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.poArticleData.length != 0 ?
                            document.getElementById(poArticleData[0].id).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.poArticleData.length != 0) {
                    document.getElementById(poArticleData[0].id).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.onMrpCloseModal(e)
        }


    }


    render() {
        if (this.state.focusedLi != "" && this.props.poArticleSearch == this.state.searchMrp) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        const {
            division, articleCode, section, department, articleName, searchMrp, sectionSelection
        } = this.state;
        const vendor = (data) => {
        }
        //open modal for dropdown code start here 
        if (this.props.uuid != "" && this.props.uuid != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let windowWidth = window.innerWidth;
            let position = document.getElementById(this.props.uuid).getBoundingClientRect();
            let leftPosition = position.left;
            let left = 0;
            let diff = windowWidth - leftPosition;
            if (diff >= modalWidth) {
                left = leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                left = leftPosition - removeWidth;
            }
            let idNo = parseInt(this.props.uuid.replace(/\D/g, '')) + 1;
            // let top = position.top -100
            let top = this.props.parent == "PO" ? 59 + (35 * idNo) : 59 + (35 * idNo);
            let newLeft = left > 50 ? left - 50 : left;

            $('#poArticleModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');
        }
        //open modal for dropdown code end here
        return (
            this.props.isModalShow ? <div className={this.props.poArticleAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.poArticleAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.poArticleAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.poArticleAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT{this.props.basedOn == "article" ? " ARTICLE" : this.props.basedOn == "section" ? " SECTION" : this.props.basedOn == "department" ? " DEPARTMENT" : " DIVISION"}</label>
                                        </li>
                                        <li>
                                            <p className="departmentpara-content">You can select article code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    <select id="searchByarticle" name="searchByarticle" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>
                                                    </select>
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchArticle" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                    <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                        <li className="float_right" >

                                            <label>
                                                {this.state.searchMrp == "" && (this.state.type == "" || this.state.type == 1) ? <button type="button" className="clearbutton btnDisabled" >CLEAR</button> :
                                                    <button type="button" className="clearbutton" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                }
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover vendorMrpMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    {this.props.basedOn == "article" && this.props.mrpRangeSearch == false ? <th>Article Code</th> : null}
                                                    {this.props.basedOn == "article" && this.props.mrpRangeSearch == false ? <th>Article Name</th> : null}
                                                    {this.props.mrpRangeSearch == false && <th>Division</th>}
                                                    {((this.props.basedOn == "section" || this.props.basedOn == "department" || this.props.basedOn == "article") && this.props.mrpRangeSearch == false) ? <th>Section</th> : null}
                                                    {((this.props.basedOn == "department" || this.props.basedOn == "article") && this.props.mrpRangeSearch == false) ? <th>Department</th> : null}
                                                    {this.props.mrpRangeSearch == true && <th>MRP Start</th>}
                                                    {this.props.mrpRangeSearch == true && <th>MRP End</th>}

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.poArticleData == undefined || this.state.poArticleData == null || this.state.poArticleData.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.poArticleData.map((data, key) => (
                                                    <tr key={key} onClick={(e) => this.selectedData(data.id, data)} >
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.id} checked={this.state.vendorMrpPoData == data.id} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        {this.props.basedOn == "article" ? <td>{data.hl4Code}</td> : null}
                                                        {this.props.basedOn == "article" ? <td>{data.hl4Name}</td> : null}
                                                        <td>{data.hl1Name}</td>
                                                        {(this.props.basedOn == "section" || this.props.basedOn == "department" || this.props.basedOn == "article") && this.props.mrpRangeSearch == false ? <td>{data.hl2Name}</td> : null}
                                                        {(this.props.basedOn == "department" || this.props.basedOn == "article") && this.props.mrpRangeSearch == false ? <td>{data.hl3Name}</td> : null}
                                                        {this.props.mrpRangeSearch == true && <td>{data.mrpStart}</td>}
                                                        {this.props.mrpRangeSearch == true && <td>{data.mrpEnd}</td>}
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" data-dismiss="modal" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onMrpCloseModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div> :

                this.state.modalDisplay ? <div className="poArticleModalPosition" id="poArticleModalPosition">
                    <div className="dropdown-menu-city2 po-article-mod-dropdown header-dropdown" id="pocolorModel">
                        <div className="dropdown-modal-header">
                            {this.props.basedOn == "article" && this.props.mrpRangeSearch == false && <span className="div-col-7">Article Code</span>}
                            {this.props.basedOn == "article" && this.props.mrpRangeSearch == false && <span className="div-col-4">Article </span>}
                            {this.props.mrpRangeSearch == false && <span className="div-col-5">Division </span>}
                            {(this.props.basedOn == "section" || this.props.basedOn == "department" || this.props.basedOn == "article") && this.props.mrpRangeSearch == false ? <span className="div-col-5">Section</span> : null}
                            {(this.props.basedOn == "department" || this.props.basedOn == "article") && this.props.mrpRangeSearch == false ? <span className="div-col-5">Department </span> : null}
                            {this.props.mrpRangeSearch == true && <span className="div-col-5">Mrp Start</span>}
                            {this.props.mrpRangeSearch == true && <span className="div-col-5">Mrp End</span>}
                        </div>

                        <ul className="dropdown-menu-city-item">
                            {this.state.poArticleData == undefined || this.state.poArticleData.length == 0 ? <li><span>No Data Found</span></li>
                                : this.state.poArticleData.map((data, key) => (
                                    <li key={key} onClick={(e) => this.selectedData(data.id, data)} id={data.id} className={this.state.vendorMrpPoData == data.id ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.id, data)}>
                                        <span className="vendor-details">
                                            {this.props.mrpRangeSearch == true && <span className="vd-code div-col-5">{data.mrpStart}</span>}
                                            {this.props.mrpRangeSearch == true && <span className="vd-code div-col-5">{data.mrpEnd}</span>}
                                            {this.props.basedOn == "article" ? <span className="vd-name div-col-7">{data.hl4Code}</span> : null}
                                            {this.props.basedOn == "article" ? <span className="vd-loc div-col-4">{data.hl4Name}</span> : null}
                                            <span className="vd-num div-col-5">{data.hl1Name}</span>
                                            {this.props.basedOn == "section" || this.props.basedOn == "department" || this.props.basedOn == "article" ? <span className="vd-code div-col-5">{data.hl2Name}</span> : null}
                                            {this.props.basedOn == "department" || this.props.basedOn == "article" ? <span className="vd-code div-col-5">{data.hl3Name}</span> : null}

                                        </span>
                                    </li>))}

                        </ul>
                        <div className="gen-dropdown-pagination">
                            <div className="page-close">
                                <button className="btn-close" type="button" onClick={(e) => this.onMrpCloseModal(e)} id="btn-close">Close</button>
                                {/* <button className="btn-clear" type="button">Clear</button> */}
                            </div>
                            <div className="page-next-prew-btn">
                                {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                    </svg>
                                </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                        </svg></button>}
                                <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                    </svg>
                                </button>
                                    : <button className="pnpb-next" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                        </svg>
                                    </button> : <button className="pnpb-next" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                        </svg></button>}
                            </div>
                        </div>
                    </div>
                </div> : null


        );
    }
}

export default PoArticleModal;
