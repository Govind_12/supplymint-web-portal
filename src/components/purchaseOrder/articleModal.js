import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class ArticleModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            isUDFExist: "false",
            isSiteExist: "false",
            itemUdfExist: "false",
            isOtbValidation: "false",
            copyColor: "false",
            checked: false,
            vendorPoState: this.props.vendorPoState,
            selectedId: "",
            id: "",
            addNew: true,
            save: false,
            division: "",
            articleCode: "",
            section: "",
            department: "",
            articleName: "",
            done: false,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            searchMrp: "",
            toastLoader: false,
            sectionSelection: "",
            toastMsg: "",
            vendorMrpPoData: this.props.vendorMrpPo,
            hl3Name: "",
            searchBy: "startWith",
            focusedLi: "",

        }
    }
    componentDidMount() {

        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }

    }
    componentWillMount() {
        this.setState({
            id: this.props.rowId,
            vendorMrpPoData: this.props.vendorMrpPo,
            searchMrp: this.props.isModalShow ? "" : this.props.articleSearch

        })
        if (this.props.purchaseIndent.articlePo.isSuccess) {
            let c = []
            if (this.props.purchaseIndent.articlePo.data.resource != null) {
                for (let i = 0; i < this.props.purchaseIndent.articlePo.data.resource.length; i++) {
                    let x = this.props.purchaseIndent.articlePo.data.resource[i];
                    x.checked = false;
                    let a = x
                    c.push(a)
                }
                this.setState({
                    vendorPoState: c,
                    prev: this.props.purchaseIndent.articlePo.data.prePage,
                    current: this.props.purchaseIndent.articlePo.data.currPage,
                    next: this.props.purchaseIndent.articlePo.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.articlePo.data.maxPage,
                })
            }
            else {
                this.setState({
                    vendorPoState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            id: nextProps.rowId,
            vendorMrpPoDataData: this.props.vendorMrpPoData
        })
        if (nextProps.purchaseIndent.articlePo.isSuccess) {
            let c = []
            if (nextProps.purchaseIndent.articlePo.data.resource != null) {
                for (let i = 0; i < nextProps.purchaseIndent.articlePo.data.resource.length; i++) {
                    let x = nextProps.purchaseIndent.articlePo.data.resource[i];
                    x.checked = false;
                    let a = x
                    c.push(a)
                }
                this.setState({
                    vendorPoState: c,
                    prev: nextProps.purchaseIndent.articlePo.data.prePage,
                    current: nextProps.purchaseIndent.articlePo.data.currPage,
                    next: nextProps.purchaseIndent.articlePo.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.articlePo.data.maxPage,
                    isUDFExist: nextProps.purchaseIndent.articlePo.data.isUDFExist,
                    itemUdfExist: nextProps.purchaseIndent.articlePo.data.itemUdfExist,
                    isSiteExist: nextProps.purchaseIndent.articlePo.data.isSiteExist,
                    isOtbValidation: nextProps.purchaseIndent.articlePo.data.otbValidation,
                    copyColor: nextProps.purchaseIndent.articlePo.data.copyColor
                })
            }
            else {
                this.setState({
                    vendorPoState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                    itemUdfExist: nextProps.purchaseIndent.articlePo.data.itemUdfExist,
                    isUDFExist: nextProps.purchaseIndent.articlePo.data.isUDFExist,
                    isSiteExist: nextProps.purchaseIndent.articlePo.data.isSiteExist
                })
            }
               if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("searchArticle").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.articlePo.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.articlePo.data.resource[0].hl4Code
                        })
                        document.getElementById(nextProps.purchaseIndent.articlePo.data.resource[0].hl4Code) != null ? document.getElementById(nextProps.purchaseIndent.articlePo.data.resource[0].hl4Code).focus() : null
                    }


                
                }
            }
        }
    }
    onAddNew(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: false,
                save: true,
                searchMrp: "",
            })
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onSave(e) {
        var array = this.state.vendorPoState;
        let c = {
            id: this.state.vendorPoState.length + 1,
            articleCode: this.state.articleCode,
            division: this.state.division,
            section: this.state.section,
            department: this.state.department,
            articleName: this.state.articleName
        };
        array.push(c);
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false,
                vendorPoState: array
            }
        )
    }
    onCancel(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false
            }
        )
    }
    selectedData(e) {
        let c = this.state.vendorPoState;
        for (let i = 0; i < c.length; i++) {
            if (c[i].hl4Code == e) {
                c[i].checked = true;
                this.setState({
                    vendorMrpPoData: c[i].hl4Code
                })
            } else {
                c[i].checked = false;
            }
        }
        this.setState({
            vendorPoState: c
         }, () => {
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
                })
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.articlePo.data.prePage,
                current: this.props.purchaseIndent.articlePo.data.currPage,
                next: this.props.purchaseIndent.articlePo.data.currPage + 1,
                maxPage: this.props.purchaseIndent.articlePo.data.maxPage,
            })
            if (this.props.purchaseIndent.articlePo.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.articlePo.data.currPage - 1,
                    articleCode: this.state.articleCode,
                    division: this.state.division,
                    section: this.state.section,
                    department: this.state.department,
                    articleName: this.state.articleName,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.articlePoRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.articlePo.data.prePage,
                current: this.props.purchaseIndent.articlePo.data.currPage,
                next: this.props.purchaseIndent.articlePo.data.currPage + 1,
                maxPage: this.props.purchaseIndent.articlePo.data.maxPage,
            })
            if (this.props.purchaseIndent.articlePo.data.currPage != this.props.purchaseIndent.articlePo.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.articlePo.data.currPage + 1,
                    articleCode: this.state.articleCode,
                    division: this.state.division,
                    section: this.state.section,
                    department: this.state.department,
                    articleName: this.state.articleName,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.articlePoRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.articlePo.data.prePage,
                current: this.props.purchaseIndent.articlePo.data.currPage,
                next: this.props.purchaseIndent.articlePo.data.currPage + 1,
                maxPage: this.props.purchaseIndent.articlePo.data.maxPage,
            })
            if (this.props.purchaseIndent.articlePo.data.currPage <= this.props.purchaseIndent.articlePo.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    articleCode: this.state.articleCode,
                    division: this.state.division,
                    section: this.state.section,
                    department: this.state.department,
                    articleName: this.state.articleName,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.articlePoRequest(data)
            }
            this.textInput.current.focus();
        }
    }
    onDone() {
        let flag = false;
        let c = this.state.vendorPoState;
        let idd = "";
        let articleCode = ""
        let division = ""
        let divisionCode = ""
        let sectionCode = ""
        let section = ""
        let department = ""
        let departmentCode = ""
        let mrpStart = ""
        let mrpEnd = ""
        let articleName = ""
        let hsnSacCode = ""
        let data = {}
        if (c != undefined) {
            for (let i = 0; i < c.length; i++) {
                if (this.state.vendorMrpPoData != "") {
                    if (c[i].hl4Code == this.state.vendorMrpPoData) {
                        // document.getElementById("mrpBasedOn").value=""
                        idd = c[i].id
                        articleCode = c[i].hl4Code
                        division = c[i].hl1Name
                        divisionCode = c[i].hl1Code
                        section = c[i].hl2Name
                        sectionCode = c[i].hl2Code
                        department = c[i].hl3Name
                        departmentCode = c[i].hl3Code
                        mrpStart = c[i].mrpStart
                        mrpEnd = c[i].mrpEnd
                        articleName = c[i].hl4Name,
                            flag = true
                        break
                    }
                }
                else {
                    flag = false
                }
            }
        } else {
            this.setState({
                done: true
            })
        }
        if (flag) {
            this.setState({
                done: true,
                search: "",
            })
            data = {
                id: idd,
                rId: this.state.id,
                articleCode: articleCode,
                division: division,
                divisionCode: divisionCode,
                section: section,
                sectionCode: sectionCode,
                department: department,
                departmentCode: departmentCode,
                mrpStart: mrpStart,
                mrpEnd: mrpEnd,
                articleName: articleName,
                isUDFExist: this.state.isUDFExist,
                isSiteExist: this.state.isSiteExist,
                itemUdfExist: this.state.itemUdfExist,

                isOtbValidation: this.state.isOtbValidation,
                copyColor: this.state.copyColor
            }
        } else {
            this.setState({
                done: false
            })
        }
        for (let i = 0; i < c.length; i++) {
            if (this.state.vendorMrpPoData != "") {
                if (c[i].hl4Code == this.state.vendorMrpPoData) {
                    // document.getElementById("mrpBasedOn").value=""
                    this.props.updateMrpState(data);
                    this.props.articlePoClear()
                } this.onMrpCloseModal();
            } else {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    onMrpCloseModal(e) {
        this.setState({
            searchMrp: "",
            sectionSelection: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            vendorPoState: []
        })
        this.props.mrpCloseModal(e)
        this.props.articlePoClear()
    }
    onSearch(e) {
        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,
                })
                let data = {
                    type: 3,
                    no: 1,
                    articleCode: this.state.articleCode,
                    articleName: this.state.articleName,
                    division: this.state.division,
                    section: this.state.section,
                    department: this.state.department,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.articlePoRequest(data)
            }
            else {
                this.setState({
                    type: 2,
                })
                let data = {
                    type: 2,
                    no: 1,
                    articleCode: this.state.articleCode,
                    articleName: this.state.articleName,
                    division: this.state.division,
                    section: this.state.section,
                    department: this.state.department,
                    search: "",
                    searchBy: this.state.searchBy
                }
                this.props.articlePoRequest(data)
            }
        }
    }
    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                sectionSelection: "",
                type: "",
                no: 1
            })
        if (this.state.type == 2 || this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                articleCode: "",
                articleName: "",
                division: "",
                section: "",
                department: "",
                search: "",
                searchBy: this.state.searchBy
            }
            this.props.articlePoRequest(data)
        }
        document.getElementById("searchArticle").focus()
    }

    handleChange(e) {
        if (e.target.id == "searchByarticle") {
            this.setState(
                {
                    searchBy: e.target.value
                }, () => {
                    if (this.state.searchMrp != "") {
                        let data = {
                            type: this.state.type,
                            no: 1,
                            articleCode: this.state.articleCode,
                            division: this.state.division,
                            section: this.state.section,
                            department: this.state.department,
                            articleName: this.state.articleName,
                            search: this.state.searchMrp,
                            searchBy: this.state.searchBy
                        }
                        this.props.articlePoRequest(data);
                    }
                }

            );
        } else if (e.target.id == "division") {
            this.setState(
                {
                    division: e.target.value
                },

            );
        } else if (e.target.id == "articleCode") {
            this.setState(
                {
                    articleCode: e.target.value
                },

            );
        } else if (e.target.id == "section") {
            this.setState(
                {
                    section: e.target.value
                },

            );
        }
        else if (e.target.id == "department") {
            this.setState(
                {
                    department: e.target.value
                },

            );
        } else if (e.target.id == "articleName") {
            this.setState(
                {
                    articleName: e.target.value
                },

            );
        } else if (e.target.id == "searchArticle") {

            this.setState(
                {
                    searchMrp: e.target.value
                },

            );
            if (e.target.value == "") {
                //     var data={
                //         type:"",
                //         no:1,
                //         articleCode:"",
                //         articleName:"",
                //         division:"",
                //         section:"",
                //         department:"",
                //         search:""
                //     }
                //     this.props.articlePoRequest(data)
                // }else{
                //     if(document.getElementById("mrpBasedOn").value=="articleCode"){
                //         this.setState(
                //             {
                //                 articleCode: e.target.value,
                //                 articleName:"",
                //                 division:"",
                //                 section:"",
                //                 department:"",
                //             },

                //         );
                //     }
                //    else if(document.getElementById("mrpBasedOn").value=="articleName"){
                //         this.setState(
                //             {
                //                 articleCode:"" ,
                //                 articleName:e.target.value,
                //                 division:"",
                //                 section:"",
                //                 department:"",
                //             },

                //         );
                //     }    else if(document.getElementById("mrpBasedOn").value=="division"){
                //         this.setState(
                //             {
                //                 articleCode:"" ,
                //                 articleName:"",
                //                 division:e.target.value,
                //                 section:"",
                //                 department:""
                //             },

                //         );
                //     }
                //     else if(document.getElementById("mrpBasedOn").value=="section"){
                //         this.setState(
                //             {
                //                 articleCode:"" ,
                //                 articleName:"",
                //                 division:"",
                //                 section:e.target.value,
                //                 department:""
                //             },

                //         );
                //     }
                //     else if(document.getElementById("mrpBasedOn").value=="department"){
                //         this.setState(
                //             {
                //                 articleCode:"" ,
                //                 articleName:"",
                //                 division:"",
                //                 section:"",
                //                 department:e.target.value,
                //             },

                //         );
                //     }
                //     else if(document.getElementById("mrpBasedOn").value==""){
                //         this.setState(
                //             {
                //                 articleCode:"" ,
                //                 mrp:"",
                //                 rsp:"",
                //                 searchMrp:e.target.value
                //             },


                //         );
                //     }
            }
        }
        //  else if (e.target.id == "mrpBasedOn") {
        //         this.setState(
        //             {
        //                 searchMrp: "",
        //                 sectionSelection:e.target.value
        //             }

        //         );
        //     }
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    _handleKeyDown = (e) => {
        console.log(e.key)
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.vendorPoState.length != 0) {
                    this.setState({
                        // selectedId: this.state.vendorPoState[0].hl4Code,
                        vendorMrpPoData: this.state.vendorPoState[0].hl4Code
                    })
                    let vendorMrp = this.state.vendorPoState[0].hl4Code

                    window.setTimeout(function () {
                        document.getElementById("searchArticle").blur();

                        document.getElementById(vendorMrp).focus()
                    }, 0)
                } else {
                    window.setTimeout(function () {
                        document.getElementById("searchArticle").blur();
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }

            if (e.target.value != "") {
                document.getElementById("searchArticle").blur()
                document.getElementById("findButton").focus()
            }
        }
        if (e.key == "ArrowDown") {
            if (this.state.vendorPoState.length != 0) {
                this.setState({
                    // selectedId: this.state.vendorPoState[0].hl4Code,
                    vendorMrpPoData: this.state.vendorPoState[0].hl4Code
                })
                let vendorMrp = this.state.vendorPoState[0].hl4Code

                window.setTimeout(function () {
                    document.getElementById("searchArticle").blur();

                    document.getElementById(vendorMrp).focus()
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("searchArticle").blur();
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let code = this.state.vendorPoState[0].hl4Code
            window.setTimeout(function () {
                document.getElementById(code).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.onMrpCloseModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("searchArticle").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.vendorPoState.length != 0) {
                this.setState({
                    // selectedId: this.state.vendorPoState[0].hl4Code,
                    vendorMrpPoData: this.state.vendorPoState[0].hl4Code
                })
                let code = this.state.vendorPoState[0].hl4Code
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(code).focus()
                }, 0)
            }
            else {
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById("searchArticle").focus()
                }, 0)
            }
        }
    }
 selectLi(e, code) {
        let vendorPoState = this.state.vendorPoState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < vendorPoState.length; i++) {
                if (vendorPoState[i].hl4Code == code) {
                    index = i
                }
            }
            if (index < vendorPoState.length - 1 || index == 0) {
                document.getElementById(vendorPoState[index + 1].hl4Code) != null ? document.getElementById(vendorPoState[index + 1].hl4Code).focus() : null

                this.setState({
                    focusedLi: vendorPoState[index + 1].hl4Code
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < vendorPoState.length; i++) {
                if (vendorPoState[i].hl4Code == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(vendorPoState[index - 1].hl4Code) != null ? document.getElementById(vendorPoState[index - 1].hl4Code).focus() : null

                this.setState({
                    focusedLi: vendorPoState[index - 1].hl4Code
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.onMrpCloseModal(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.vendorPoState.length!=0?
                document.getElementById(this.state.vendorPoState[0].hl4Code).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.vendorPoState.length!=0){
                document.getElementById(this.state.vendorPoState[0].hl4Code).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.onMrpCloseModal(e)
        }


    }
  

    render() {
        if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        const {
            division, articleCode, section, department, articleName, searchMrp, sectionSelection
        } = this.state;
        return (

         this.props.isModalShow ?     <div className={this.props.articleModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.articleModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.articleModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.articleModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT ARTICLE</label>
                                        </li>
                                        <li>
                                            <p className="departmentpara-content">You can select article code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">
                                        {this.state.save ? <div className="col-md-7 col-sm-7 pad-0 mrpLi vendorMrpList">
                                            <li>
                                                <input type="text" className="" onChange={e => this.handleChange(e)} id="articleName" name="articleName" value={articleName} placeholder="articleName" /> </li>
                                            <li> <input type="text" className="" onChange={e => this.handleChange(e)} id="articleCode" name="articleCode" value={articleCode} placeholder="Article Code" /> </li>
                                            <li>  <input type="text" className="" onChange={e => this.handleChange(e)} id="division" value={division} name="division" placeholder="division" /> </li>
                                            <li> <input type="text" className="" onChange={e => this.handleChange(e)} id="section" name="section" value={section} placeholder="section" /> </li>
                                            <li>  <input type="text" className="" onChange={e => this.handleChange(e)} id="department" value={department} name="department" placeholder="department" /> </li>

                                        </div> : null}
                                        {this.state.addNew ? <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>

                                                    {/* <select id="mrpBasedOn" name="sectionSelection" value={sectionSelection} onChange={e => this.handleChange(e)}>
                                    <option value="">Choose</option>
                                        <option value="articleCode">Article Code</option>
                                        <option value="articleName">Article Name</option>
                                        <option value="division">Division</option>
                                        <option value="section">Section</option>
                                        <option value="department">Department</option>
                                    </select>
                                                                         */}
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByarticle" name="searchByarticle" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchArticle" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                    <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div> : null}
                                        {this.state.addNew ? <li className="float_right" >

                                            <label>
                                                {this.state.searchMrp == "" && (this.state.type == "" || this.state.type == 1) ? <button type="button" className="clearbutton btnDisabled" >CLEAR</button> :
                                                    <button type="button" className="clearbutton" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>

                                                }
                                            </label>
                                        </li> : null}

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover vendorMrpMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Article Code</th>
                                                    <th>Article Name</th>
                                                    <th>Division</th>
                                                    <th>Section</th>
                                                    <th>Department</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.vendorPoState == undefined || this.state.vendorPoState == null || this.state.vendorPoState.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.vendorPoState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.hl4Code}`)} >
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.hl4Code} checked={this.state.vendorMrpPoData == `${data.hl4Code}`} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.hl4Code}</td>
                                                        <td>{data.hl4Name}</td>
                                                        <td>{data.hl1Name}</td>
                                                        <td>{data.hl2Name}</td>
                                                        <td>{data.hl3Name}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" data-dismiss="modal" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onMrpCloseModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
            :


                <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.vendorPoState != undefined || this.state.vendorPoState.length != 0 ?
                            this.state.vendorPoState.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.hl4Code}`)} id={data.hl4Code} className={this.state.vendorMrpPoData == `${data.hl4Code}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.hl4Code)}>
                                    <span className="vendor-details">
                                        <span className="vd-name">{data.hl4Code}</span>
                                        <span className="vd-loc">{data.hl4Name}</span>
                                        <span className="vd-num"> {data.hl1Name}</span>
                                        <span className="vd-code">{data.hl2Name}</span>
                                        <span className="vd-code">{data.hl3Name}</span>
                                        
                                    </span>
                                </li>)) : <li><span>No Data Found</span></li>}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.onMrpCloseModal(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn margin-180">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                     <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>

        );
    }
}

export default ArticleModal;
