import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class CnameModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            checked: false,
            cnameState: this.props.cnameData,
            selectedId: "",
            id: "",
            addNew: true,
            save: false,
            mrp: "",
            articleCode: "",
            rsp: "",
            done: false,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            searchMrp: "",
            toastLoader: false,
            sectionSelection: "",
            toastMsg: "",
            cnameValue: this.props.cnameValue,
            searchBy: "contains"

        }

    }
    componentDidMount() {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
    }
    componentWillMount() {
        this.setState({
            cnameValue: this.props.cnameValue
        })
        if (this.props.purchaseIndent.cname.isSuccess) {
            let c = []
            if (this.props.purchaseIndent.cname.data.resource != null) {
                for (let i = 0; i < this.props.purchaseIndent.cname.data.resource.length; i++) {
                    let x = this.props.purchaseIndent.cname.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    cnameState: c,
                    prev: this.props.purchaseIndent.cname.data.prePage,
                    current: this.props.purchaseIndent.cname.data.currPage,
                    next: this.props.purchaseIndent.cname.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.cname.data.maxPage,
                })

            }
            else {
                this.setState({
                    cnameState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            cnameValue: this.props.cnameValue
        })
        if (nextProps.purchaseIndent.cname.isSuccess) {
            if (nextProps.purchaseIndent.cname.data.resource != null) {
                this.setState({
                    cnameState: nextProps.purchaseIndent.cname.data.resource,
                    prev: nextProps.purchaseIndent.cname.data.prePage,
                    current: nextProps.purchaseIndent.cname.data.currPage,
                    next: nextProps.purchaseIndent.cname.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.cname.data.maxPage,
                })

            }
            else {
                this.setState({
                    cnameState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

        if (nextProps.purchaseIndent.cname.isSuccess) {
            this.setState({
                loader: false
            })
            this.props.cnameRequest();

        } else if (nextProps.purchaseIndent.cname.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.cname.message.error == undefined ? undefined : nextProps.purchaseIndent.cname.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.cname.message.error == undefined ? undefined : nextProps.purchaseIndent.cname.message.error.errorCode,
                code: nextProps.purchaseIndent.cname.message.status,
                alert: true,
                loader: false
            })
            this.props.cnameRequest();
        } else if (!nextProps.purchaseIndent.cname.isLoading) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.purchaseIndent.cname.isLoading) {
            this.setState({
                loader: true
            })
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onSave(e) {
        var array = this.state.cnameState;
        let c = {
            id: this.state.cnameState.length + 1,
            articleCode: this.state.articleCode,
            mrp: this.state.mrp,
            rsp: this.state.rsp,
            mrpStart: this.state.mrpStart,
            mrpEnd: this.state.mrpEnd
        };

        array.push(c);
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false,
                cnameState: array
            }
        )
    }
    selectedData(e) {
        let c = this.props.purchaseIndent.cname.data.resource;
        for (let i = 0; i < c.length; i++) {
            if (c[i].header == e) {
                c[i].checked = true
                this.setState({
                    cnameValue: c[i].header
                })
            }
            if (c[i].header != e) {
                c[i].checked = false
            }
        }
        this.setState({
            cnameState: c
        })
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.cname.data.prePage,
                current: this.props.purchaseIndent.cname.data.currPage,
                next: this.props.purchaseIndent.cname.data.currPage + 1,
                maxPage: this.props.purchaseIndent.cname.data.maxPage,
            })
            if (this.props.purchaseIndent.cname.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.cname.data.currPage - 1,
                    udfType: this.props.catDesc,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.cnameRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.cname.data.prePage,
                current: this.props.purchaseIndent.cname.data.currPage,
                next: this.props.purchaseIndent.cname.data.currPage + 1,
                maxPage: this.props.purchaseIndent.cname.data.maxPage,
            })
            if (this.props.purchaseIndent.cname.data.currPage != this.props.purchaseIndent.cname.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.cname.data.currPage + 1,
                    udfType: this.props.catDesc,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.cnameRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.cname.data.prePage,
                current: this.props.purchaseIndent.cname.data.currPage,
                next: this.props.purchaseIndent.cname.data.currPage + 1,
                maxPage: this.props.purchaseIndent.cname.data.maxPage,
            })
            if (this.props.purchaseIndent.cname.data.currPage <= this.props.purchaseIndent.cname.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    udfType: this.props.catDesc,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.cnameRequest(data)
            }
            this.textInput.current.focus();
        }
    }
    onDone() {
        let flag = false;
        let c = this.props.purchaseIndent.cname.data.resource;
        let idd = "";
        let data = {}
        if (c != undefined) {
            for (let i = 0; i < c.length; i++) {
                if (this.state.cnameValue != "") {
                    if (c[i].header == this.state.cnameValue) {
                        idd = c[i].id
                        data = {
                            code: c[i].code,
                            cname: c[i].header,
                            ext: c[i].ext,
                        }
                        flag = true
                        break
                    }
                }
                else {
                    flag = false
                }
            }
        } else {
            this.setState({
                done: true
            })
        }
        if (flag) {
            this.setState({
                done: true,
                search: "",
                type: "",
                no: 1,
            })
        } else {
            this.setState({
                done: false
            })
        }

        for (let i = 0; i < c.length; i++) {
            if (this.state.cnameValue != "") {
                if (c[i].header == this.state.cnameValue) {
                    let flag1 = false
                    let array = ['CAT1', 'CAT2', 'CAT3', 'CAT4', 'CAT5', 'CAT6', 'DESC1', 'DESC2', 'DESC3', 'DESC4', 'DESC5', 'DESC6']

                    if (array.includes(this.props.catDesc)) {
                        flag1 = true
                    } else {
                        flag1 = false
                    }

                    if (flag1) {
                        this.props.updateDescription(data)
                    } else {
                        this.props.updateDescriptionUdf(data)
                    }
                }
                this.onMrpCloseModal();
            } else {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    onMrpCloseModal(e) {
        this.setState({
            searchMrp: "",
            cnameState: []
        })
        this.props.closeDescription(e)
    }
    onSearch(e) {
        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,
                    udfType: this.props.catDesc,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.cnameRequest(data)
            }
            else {

                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    udfType: this.props.catDesc,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.cnameRequest(data)
            }
        }
    }
    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                type: ""

            })
        if (this.state.type == 3) {
            var data = {

                type: "",
                no: 1,
                udfType: this.props.catDesc,
                search: this.state.searchMrp,
                searchBy: this.state.searchBy
            }
            this.props.cnameRequest(data)
        }
    }

    handleChange(e) {
        if (e.target.id == "searchMrp") {
            this.setState({
                searchMrp: e.target.value
            })

        } else if (e.target.id == "searchByCname") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.searchMrp != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        udfType: this.props.catDesc,
                        search: this.state.searchMrp,
                        searchBy: this.state.searchBy
                    }
                    this.props.cnameRequest(data)
                }
            })
        }
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    // _______________________________-TAB FUNCTIONALITY__________________________________

    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.cnameState.length != 0) {
                    this.setState({
                        cnameValue: this.state.cnameState[0].header
                    })
                    this.selectedData(this.state.cnameState[0].header)
                    window.setTimeout(() => {
                        document.getElementById(this.state.cnameState[0].header).focus()
                    }, 0)
                } else {
                    window.setTimeout(() => {
                        document.getElementById("searchMrp").blur();
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }

            if (e.target.value != "") {
                window.setTimeout(() => {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if (e.key === "ArrowDown") {
            if (this.state.cnameState.length != 0) {
                this.setState({
                    cnameValue: this.state.cnameState[0].header
                })
                this.selectedData(this.state.cnameState[0].header)
                window.setTimeout(() => {
                    document.getElementById(this.state.cnameState[0].header).focus()
                }, 0)
            } else {
                window.setTimeout(() => {
                    document.getElementById("searchMrp").blur();
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById(this.state.cnameState[0].header).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if(e.key == "Enter"){
            this.onDone()

        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.onMrpCloseModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("closeButton").blur()
                document.getElementById("searchMrp").focus()
            }, 0)
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.cnameState.length != 0) {
                this.setState({
                    cnameValue: this.state.cnameState[0].header
                })
                this.selectedData(this.state.cnameState[0].header)
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById(this.state.cnameState[0].header).focus()
                }, 0)
            }
            else {
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById("searchMrp").focus()
                }, 0)
            }
        }
    }

    render() {

        const { rsp, articleCode, mrp, mrpEnd, mrpStart, searchMrp, sectionSelection } = this.state;
        return (
            <div className={this.props.cnameModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.cnameModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.cnameModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.cnameModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT CNAME</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only one cname from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByCname" name="searchByCname" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                    <label>
                                                        <button type="button" className="clearbutton m-lft-15" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover cnameMain">
                                            <thead>
                                                <tr>
                                                    <th>select</th>
                                                    <th>Cname</th>
                                                    <th>Ext</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.cnameState == undefined || this.state.cnameState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.cnameState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.header}`)} >
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input type="radio" name="cnameCheck" id={data.header} checked={`${data.header}` == this.state.cnameValue} onKeyDown={(e) => this.focusDone(e)} readOnly/>
                                                                <span className="checkradio-select select_all positionCheckbox"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.header}</td>
                                                        <td className="pad-lft-8">{data.ext == "N" ? "False" : "True"}</td>

                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" data-dismiss="modal" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onMrpCloseModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        );
    }
}
export default CnameModal;
