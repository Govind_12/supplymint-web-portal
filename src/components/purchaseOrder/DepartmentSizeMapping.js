import React from 'react';
import SectionDataSelection from "../../components/purchaseIndent/sectionDataSelection";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import PoError from "../loaders/poError";
class DeptSizeMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            sizeMapping: [],
            udfType: "",
            division: "",
            section: "",
            department: "",
            selectionModal: false,
            selectionAnimation: false,
            divisionState: [],
            loader: false,
            success: false,
            alert: false,
            code: "",
            successMessage: "",
            errorCode: "",
            errorMessage: "",
            poErrorMsg: false,
            errorMassage: "",
            payloadId: [],
            hl3Code: "",
          

        };
    }
    componentDidMount() {
        window.setTimeout(function () {
            document.getElementById("dataPiBtn").focus()
        }, 0)
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    closeSelection(e) {
        this.setState({
            selectionModal: false,
            selectionAnimation: !this.state.selectionAnimation
        });
        window.setTimeout(function () {
            document.getElementById("dataPiBtn").focus()
        }, 0)

    }
    openSelection(e) {
        var data = {
            no: 1,
            type: "",
            hl1name: "",
            hl2name: "",
            hl3name: "",
            search: "",
            searchBy:this.state.searchBy
        }
        this.props.divisionSectionDepartmentRequest(data);
        this.setState({
            department: this.state.department,
            selectionModal: true,
            selectionAnimation: !this.state.selectionAnimation

        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }

    updateState(upData) {
        var sectionData = []
        this.setState({
            division: upData.hl1Name,
            section: upData.hl2Name,
            department: upData.hl3Name,
            hl3Code: upData.hl3Code
        })
        let data = {
            deptName: upData.hl3Name,
            deptCode: upData.hl3Code
        }
        this.props.poSizeRequest(data)

    }

    onResetData() {
        let data = {
            deptName: this.state.department,
            deptCode: this.state.hl3Code
        }
        this.setState({
            payloadId: []
        })
        this.props.poSizeRequest(data)
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.divisionData.isSuccess) {
            let c = []
            if (nextProps.purchaseIndent.divisionData.data.resource != null) {
                for (let i = 0; i < nextProps.purchaseIndent.divisionData.data.resource.length; i++) {

                    let x = nextProps.purchaseIndent.divisionData.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    divisionState: c,
                })
            }

            this.setState({
                loader: false
            })
            this.props.divisionSectionDepartmentRequest();


        } else if (nextProps.purchaseIndent.divisionData.isError) {

            this.setState({
                errorMessage: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorCode,
                code: nextProps.purchaseIndent.divisionData.message.status,
                alert: true,

                loader: false

            })
            this.props.divisionSectionDepartmentRequest();
        } else if (!nextProps.purchaseIndent.divisionData.isLoading) {
            this.setState({
                loader: false
            })
        }


        //poSize


        if (nextProps.purchaseIndent.poSize.isSuccess) {
            this.setState({
                sizeMapping: nextProps.purchaseIndent.poSize.data.resource,
                loader: false
            })

            window.setTimeout(() => {
                document.getElementById(this.state.sizeMapping[0].code).focus();
            })

            this.props.poSizeRequest();
        } else if (nextProps.purchaseIndent.poSize.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.poSize.message.error == undefined ? undefined : nextProps.purchaseIndent.poSize.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.poSize.message.error == undefined ? undefined : nextProps.purchaseIndent.poSize.message.error.errorCode,
                code: nextProps.purchaseIndent.poSize.message.status,
                alert: true,

                loader: false

            })
            this.props.poSizeRequest();
        } else if (!nextProps.purchaseIndent.poSize.isLoading) {
            this.setState({
                loader: false
            })
        }



        if (nextProps.purchaseIndent.updateSizeDept.isSuccess) {

            this.setState({


                successMessage: nextProps.purchaseIndent.updateSizeDept.data.message,
                loader: false,
                success: true,
                payloadId: []
            })

            this.props.updateSizeDeptRequest();


        } else if (nextProps.purchaseIndent.updateSizeDept.isError) {

            this.setState({
                errorMessage: nextProps.purchaseIndent.updateSizeDept.message.error == undefined ? undefined : nextProps.purchaseIndent.updateSizeDept.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.updateSizeDept.message.error == undefined ? undefined : nextProps.purchaseIndent.updateSizeDept.message.error.errorCode,
                code: nextProps.purchaseIndent.updateSizeDept.message.status,
                alert: true,

                loader: false

            })
            this.props.updateSizeDeptRequest();
        }

        if (nextProps.purchaseIndent.divisionData.isLoading || nextProps.purchaseIndent.poSize.isLoading || nextProps.purchaseIndent.updateSizeDept.isLoading) {
            this.setState(
                {
                    loader: true
                }
            )
        }
    }


    handleChange(id, e) {
        if (e.key != "Tab") {
            let ids = id
            let payloadId = this.state.payloadId
            if (!payloadId.includes(ids)) {

                payloadId.push(ids)
            }
            this.setState({
                payloadId: payloadId
            })
            let sizeMapping = this.state.sizeMapping

            for (var i = 0; i < sizeMapping.length; i++) {
                if (sizeMapping[i].code == id) {
                    if (e.target.validity.valid) {
                        sizeMapping[i].orderBy = e.target.value.trim()
                    }
                }
            }

            this.setState({
                sizeMapping: sizeMapping
            })
        }
    }
    // checkOrderBy(id, e) {
    //     let sizeMapping = this.state.sizeMapping
    //     let orderByData = []
    //     let flag = false
    //     sizeMapping.forEach(s => {
    //         let size = s.orderBy

    //         orderByData.push(size)

    //     })
    //     console.log(orderByData);

    //     let unique = [];
    //     orderByData.forEach(function (item) {
    //         let items = item
    //         if (!unique[items])
    //             unique[items] = 0;
    //         unique[items] += 1;
    //     })
    //     console.log(unique);

    //     for (var prop in unique) {
    //         if (unique[prop] >= 2) {
    //             flag = true
    //         }
    //     }

    //     if (flag) {
    //         for (let i = 0; i < sizeMapping.length; i++) {
    //             if (sizeMapping[i].code == id) {
    //                 sizeMapping[i].orderBy = ""
    //             }
    //         }
    //         this.setState({
    //             sizeMapping: sizeMapping,
    //             poErrorMsg: true,
    //             errorMassage: "order by no. can't be same"
    //         })
    //     }
    // }

    onSubmit() {
        let sizeMapping = this.state.sizeMapping

        let payload = []
        if (this.state.payloadId.length == 0) {
            this.setState({
                poErrorMsg: true,
                errorMassage: "There is no change in data"
            })
        } else {
            sizeMapping.forEach(cat => {
                if (this.state.payloadId.includes(cat.code)) {
                    let catPayload = {
                        orderBy: cat.orderBy,
                        code: cat.code,
                        cname: cat.cname,
                        hl3Name: this.state.department,
                        hl3Code: this.state.hl3Code

                    }
                    payload.push(catPayload)
                }
            });

            this.props.updateSizeDeptRequest(payload)
        }
    }

    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }
    }
    focusDone(code, e) {
        if (e.key === "Tab") {
            if (this.state.sizeMapping[this.state.sizeMapping.length - 1].code == code && this.state.payloadId.length == 0) {
                document.getElementById("dataPiBtn").focus()
                e.preventDefault();
            }
            if (this.state.sizeMapping[this.state.sizeMapping.length - 1].code == code && this.state.payloadId.length != 0) {
                document.getElementById("reset").focus();
                e.preventDefault();
            }
        }
        this.handleChange(code, e)
    }
    focusreset(e) {
        if (e.key == "Enter") {
            this.onResetData();
        }
        if (e.key === "Tab") {

            window.setTimeout(() => {
                document.getElementById("reset").blur();
                document.getElementById("saveButton").focus()
            }, 0);
        }
    }
    focusSave(e) {
        if (e.key === "Enter") {
            this.onSubmit();
        }
        if (e.key === "Tab") {
            document.getElementById("dataPiBtn").focus();
            e.preventDefault()
        }
    }

    render() {
        const { division, section, department } = this.state
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search"></div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                {this.state.department != "" ? <span>{this.state.payloadId.length != 0 ?
                                    <button className="gen-clear" id="reset" type="button" onKeyDown={(e) => this.focusreset(e)} onClick={(e) => this.onResetData(e)}>Reset</button>
                                    : <button className="btnDisabled" type="button">Reset</button>}
                                </span> : null}
                                {this.state.payloadId.length == 0 ? <button type="button" className="btnDisabled">Save</button>
                                    : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.focusSave(e)} className="gen-save" id="saveButton">Save</button>}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid p-lr-47">
                    <div className="col-md-12 col-sm-12 col-xs-12 udfMappingMain pad-0 departmentSizeMapMain">
                        <div className="container_content heightAuto pipo-con">
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="col-md-12 pad-0 m-top-30">
                                    <div className="piFormDiv col-md-2 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Department
                                        </label>
                                        <input type="text" className="purchaseSelectBox" value={department} placeholder="Choose Department" disabled />
                                    </div>
                                    <div className="piFormDiv col-md-2 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Division
                                        </label>
                                        <input type="text" className="purchaseSelectBox" value={division} placeholder="Select  Division" disabled />
                                    </div>
                                    <div className="piFormDiv col-md-2 pad-lft-0">
                                        <label className="purchaseLabel">
                                            Section
                                        </label>
                                        <input type="text" className="purchaseSelectBox" value={section} placeholder="Choose Section" disabled />
                                    </div>
                                    <div className="piFormDiv col-md-2 pad-lft-0 m-top-18">
                                        <button className="dataPiBtn" id="dataPiBtn" type="button" onKeyDown={(e) => this._handleKeyPress(e, "dataPiBtn")} onClick={(e) => this.openSelection(e)}>
                                            Choose Data
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {/* deptMapTable */}
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30 tableGeneric siteMapppingMain udfMappingTable">
                                <div className="tableHeadFix pipo-con-table">
                                    <div className="scrollableTableFixed table-scroll zui-scroller scrollableOrgansation orgScroll tableHeadFixHeight" id="table-scroll">
                                        <table className="table zui-table sitemappingTable gen-main-table">
                                            <thead >
                                                <tr>
                                                    <th className="pad-lft-15"><label>Size</label></th>
                                                    <th className="pad-lft-15"><label>Order By</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.sizeMapping == undefined || this.state.sizeMapping == "" || this.state.sizeMapping.length == 0 ? <tr className="tableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.sizeMapping.map((data, key) => (
                                                    <tr key={key}>
                                                        <td className="pad-lft-15"><label>{data.cname}</label></td>
                                                        <td className="pad-lft-15"><input id={data.code} pattern="[0-9]*" onKeyDown={(e) => this.focusDone(`${data.code}`, e)} onChange={(e) => this.handleChange(`${data.code}`, e)} maxLength="2" value={data.orderBy == null ? "" : data.orderBy} className="inputTable " /></td>
                                                    </tr>))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            {/* <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                <div className="footerDivForm height4per">
                                    <ul className="list-inline m-lft-0 m-top-10">
                                        {this.state.department != "" ? <li>{this.state.payloadId.length != 0 ?
                                            <button className=" clear_button_vendor" id="reset" type="button" onKeyDown={(e) => this.focusreset(e)} onClick={(e) => this.onResetData(e)}>Reset</button>
                                            : <button className="textDisable pointerNone clear_button_vendor" type="button">Reset</button>}
                                        </li> : null}
                                        <li>{this.state.payloadId.length == 0 ? <button type="button" className="btnDisabled save_button_vendor" >Save</button>
                                            : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.focusSave(e)} className="save_button_vendor" id="saveButton">Save</button>}
                                        </li>
                                    </ul>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
                {this.state.selectionModal ? <SectionDataSelection {...this.props} department={this.state.department} divisionState={this.state.divisionState} sectionCloseModal={(e) => this.sectionCloseModal(e)} selectionCloseModal={(e) => this.selectionCloseModal(e)} updateState={(e) => this.updateState(e)} selectionAnimation={this.state.selectionAnimation} closeSelection={(e) => this.closeSelection(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default DeptSizeMapping;