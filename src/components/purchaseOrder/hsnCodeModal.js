import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class HsnCodeModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            getHsnCodeData: [],
            hsnrow: "",
            hsnCode: "",
            search: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: 1,
            no: 1,
            toastLoader: false,
            toastMsg: "",
            hsnSacCode: this.props.hsnSacCode,
            searchBy: "startWith",
            focusedLi: "",
            modalDisplay: false,

        }
    }
    handleChange(e) {

        if (e.target.id == "adhocSearch") {
            this.setState({
                search: e.target.value
            })

        } else if (e.target.id = "searchByHsn") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        rowId: this.props.hsnRowId,
                        no: 1,
                        code: this.props.departmentCode,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.hsnCodeRequest(data);
                }
            })
        }
    }

    onsearchClear() {

        this.setState(
            {
                search: "",
                type: 1,
                no: 1,
            })
        if (this.state.type == 3 || this.state.type == 2) {
            let data = {
                rowId: this.props.hsnRowId,
                code: this.props.departmentCode,
                no: 1,
                type: 1,
                search: this.state.search,
                searchBy: this.state.searchBy

            }
            this.props.hsnCodeRequest(data);
        }
        document.getElementById("adhocSearch").focus()


    }




    onSelectedData(id) {


        let sdata = this.state.getHsnCodeData

        for (var i = 0; i < sdata.length; i++) {
            if (sdata[i].hsnSacCode == id) {
                this.setState({
                    hsnSacCode: sdata[i].hsnSacCode,
                    hsnCode: sdata[i].code
                })


            }

        }
        this.setState({
            getHsnCodeData: sdata
        }, () => {
            if (!this.props.isModalShow) {
                this.onDone()
            }
        })

    }
    onDone() {

        let sdata = this.state.getHsnCodeData

        var hsnSacCode = "";

        let finalData = {}
        if (sdata != null) {


            finalData = {


                hsnSacCode: this.state.hsnSacCode,
                rowId: this.state.hsnrow,
                hsnCode: this.state.hsnCode




            }
        }

        if (this.props.isModalShow ? this.state.hsnSacCode != "" : this.state.hsnSacCode != "" || this.state.search == this.props.hsnSearch || this.state.search != this.props.hsnSearch) {


            this.props.updateHsnCode(finalData);



            this.onCloseSection();
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }

        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }

    onCloseSection(e) {

        this.setState({
            Search: "",

            getHsnCodeData: [],
            search: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

        this.props.closeHsnModal();
    }


    onSearch(e) {
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {

            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,

                code: this.props.departmentCode,


                search: this.state.search,
                rowId: this.props.hsnRowId,
                searchBy: this.state.searchBy
            }

            this.props.hsnCodeRequest(data)
        }


    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.hsnCode.data.prePage,
                current: this.props.purchaseIndent.hsnCode.data.currPage,
                next: this.props.purchaseIndent.hsnCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.hsnCode.data.maxPage,
            })
            if (this.props.purchaseIndent.hsnCode.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    rowId: this.props.hsnRowId,
                    no: this.props.purchaseIndent.hsnCode.data.currPage - 1,
                    code: this.props.departmentCode,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.hsnCodeRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.hsnCode.data.prePage,
                current: this.props.purchaseIndent.hsnCode.data.currPage,
                next: this.props.purchaseIndent.hsnCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.hsnCode.data.maxPage,
            })
            if (this.props.purchaseIndent.hsnCode.data.currPage != this.props.purchaseIndent.hsnCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    rowId: this.props.hsnRowId,
                    no: this.props.purchaseIndent.hsnCode.data.currPage + 1,
                    code: this.props.departmentCode,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.hsnCodeRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.hsnCode.data.prePage,
                current: this.props.purchaseIndent.hsnCode.data.currPage,
                next: this.props.purchaseIndent.hsnCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.hsnCode.data.maxPage,
            })
            if (this.props.purchaseIndent.hsnCode.data.currPage <= this.props.purchaseIndent.hsnCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    rowId: this.props.hsnRowId,
                    no: 1,
                    code: this.props.departmentCode,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.hsnCodeRequest(data)
            }
            this.textInput.current.focus();
        }
    }


    componentWillMount() {
        this.setState({
            hsnSacCode: this.props.hsnSacCode,
            search: this.props.isModalShow ? "" : this.props.hsnSearch,
            type: this.props.isModalShow || this.props.hsnSearch == "" ? 1 : 3


        })
        if (this.props.purchaseIndent.hsnCode.isSuccess) {
            if (this.props.purchaseIndent.hsnCode.data.resource != null) {
                this.setState({
                    getHsnCodeData: this.props.purchaseIndent.hsnCode.data.resource,
                    hsnrow: this.props.purchaseIndent.hsnCode.data.rowId,
                    prev: this.props.purchaseIndent.hsnCode.data.prePage,
                    current: this.props.purchaseIndent.hsnCode.data.currPage,
                    next: this.props.purchaseIndent.hsnCode.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.hsnCode.data.maxPage
                })
            } else {
                this.setState({
                    getHsnCodeData: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })

            }


        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            hsnSacCode: this.props.hsnSacCode,

        })


        if (nextProps.purchaseIndent.hsnCode.isSuccess) {
            if (nextProps.purchaseIndent.hsnCode.data.resource != null) {
                this.setState({
                    getHsnCodeData: nextProps.purchaseIndent.hsnCode.data.resource,
                    hsnrow: nextProps.purchaseIndent.hsnCode.data.rowId,
                    prev: nextProps.purchaseIndent.hsnCode.data.prePage,
                    current: nextProps.purchaseIndent.hsnCode.data.currPage,
                    next: nextProps.purchaseIndent.hsnCode.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.hsnCode.data.maxPage

                })
            } else {
                this.setState({
                    getHsnCodeData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0

                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("adhocSearch").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.hsnCode.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.hsnCode.data.resource[0].hsnSacCode + "hsnSacCode",
                            search: this.props.isModalShow ? "" : this.props.hsnSearch,
                            type: this.props.isModalShow || this.props.hsnSearch == "" ? 1 : 3
                        })
                        document.getElementById(nextProps.purchaseIndent.hsnCode.data.resource[0].hsnSacCode) != null ? document.getElementById(nextProps.purchaseIndent.hsnCode.data.resource[0].hsnSacCode).focus() : null
                    }


                }
            }
            this.setState({
                modalDisplay: true,

            })


        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.getHsnCodeData.length != 0) {
                    this.setState({
                        hsnSacCode: this.state.getHsnCodeData[0].hsnSacCode,
                        hsnCode: this.state.getHsnCodeData[0].code
                    })
                    let hsnSacCode = this.state.getHsnCodeData[0].hsnSacCode
                    this.onSelectedData(hsnSacCode)
                    window.setTimeout(function () {
                        document.getElementById("adhocSearch").blur()
                        document.getElementById(hsnSacCode).focus()
                    }, 0);
                } else {
                    window.setTimeout(function () {
                        document.getElementById("adhocSearch").blur()
                        document.getElementById("closeButton").focus()
                    }, 0);
                }
            }

            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("adhocSearch").blur()
                    document.getElementById("findButton").focus();
                }, 0);
            }
        }
        if (e.key === "ArrowDown") {

            if (this.state.getHsnCodeData.length != 0) {
                this.setState({
                    hsnSacCode: this.state.getHsnCodeData[0].hsnSacCode,
                    hsnCode: this.state.getHsnCodeData[0].code
                })
                let hsnSacCode = this.state.getHsnCodeData[0].hsnSacCode
                this.onSelectedData(hsnSacCode)
                window.setTimeout(function () {
                    document.getElementById("adhocSearch").blur()
                    document.getElementById(hsnSacCode).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("adhocSearch").blur()
                    document.getElementById("closeButton").focus()
                }, 0);
            }
        }


        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0);
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let hsnSacCode = this.state.getHsnCodeData[0].hsnSacCode
            window.setTimeout(function () {
                document.getElementById(hsnSacCode).blur()
                document.getElementById("doneButton").focus()
            }, 0);
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0);
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.onCloseSection();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("adhocSearch").focus()
            }, 0);
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.getHsnCodeData.length != 0) {
                this.setState({
                    hsnSacCode: this.state.getHsnCodeData[0].hsnSacCode,
                    hsnCode: this.state.getHsnCodeData[0].code
                })
                let hsnSacCode = this.state.getHsnCodeData[0].hsnSacCode
                this.onSelectedData(hsnSacCode)
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(hsnSacCode).focus()
                }, 0);

            } else {

                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0);

            }
        }
    }


    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }

    }
    selectLi(e, code) {
        let getHsnCodeData = this.state.getHsnCodeData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < getHsnCodeData.length; i++) {
                if (getHsnCodeData[i].hsnSacCode == code) {
                    index = i
                }
            }
            if (index < getHsnCodeData.length - 1 || index == 0) {
                document.getElementById(getHsnCodeData[index + 1].hsnSacCode) != null ? document.getElementById(getHsnCodeData[index + 1].hsnSacCode).focus() : null

                this.setState({
                    focusedLi: getHsnCodeData[index + 1].hsnSacCode + "hsnSacCode"
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < getHsnCodeData.length; i++) {
                if (getHsnCodeData[i].hsnSacCode == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(getHsnCodeData[index - 1].hsnSacCode) != null ? document.getElementById(getHsnCodeData[index - 1].hsnSacCode).focus() : null

                this.setState({
                    focusedLi: getHsnCodeData[index - 1].hsnSacCode + "hsnSacCode"
                })

            }
        }
        if (e.which === 13) {
            this.onSelectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.onCloseSection(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.getHsnCodeData.length != 0 ?
                            document.getElementById(this.state.getHsnCodeData[0].hsnSacCode).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.getHsnCodeData.length != 0) {
                    document.getElementById(this.state.getHsnCodeData[0].hsnSacCode).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.onCloseSection(e)
        }


    }

    render() {
        console.log(this.state.focusedLi, this.state.search == this.props.hsnSearch)
        if (this.state.focusedLi != "" && this.state.search == this.props.hsnSearch) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        //open modal for dropdown code start here
        if (this.props.hsnId != "" && this.props.hsnId != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let windowWidth = window.innerWidth;
            let position = document.getElementById(this.props.hsnId).getBoundingClientRect();
            let leftPosition = position.left;
            let left = 0;
            let diff = windowWidth - leftPosition;

            if (diff >= modalWidth) {
                left = leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                // let removeleft = diff >= 260 ? diff - 2 : diff;
                // left = leftPosition - removeleft;

                left = (leftPosition - removeWidth) - 20;
            }

            let idNo = parseInt(this.props.hsnId.replace(/\D/g, '')) + 1;
            // let top = 195 + (35 * idNo);
            let top = this.props.parent=="PO"? 59 + (35 * idNo):59 + (35 * idNo);

            let newLeft = left > 40 ? left - 40 : left;

            console.log('leftPosition: ' + leftPosition)
            console.log('diff: ' + diff)
            console.log('left: ' + left)
            console.log('newLeft: ' + newLeft)



            $('#hsnModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');

        }
        //open modal for dropdown code end here

        const { search, sectionSelection } = this.state;
        return (

            this.props.isModalShow ? <div className={this.props.hsnModalAnimation ? "modal display_block" : "display_none"} id="piopensizeModel">
                <div className={this.props.hsnModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.hsnModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.hsnModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Size">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">Select HSN code</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-5">

                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByHsn" name="searchByHsn" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}

                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box adhocSearch" ref={this.textInput} onKeyPress={this._handleKeyPress} value={search} id="adhocSearch" onKeyDown={this._handleKeyDown} onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                        <li className="float_right">
                                            <label>
                                                {this.state.search == "" && (this.state.type == "" || this.state.type == 1) ?
                                                    <button type="button" className="clearbutton btnDisabled">CLEAR</button>
                                                    : <button type="button" className="clearbutton" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>

                                                }
                                            </label>
                                        </li>

                                    </ul>
                                </div>


                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    {/* <th>hsnSacCode Code

                                                    </th> */}
                                                    <th>HSN code</th>
                                                    {/* <th>Rsp</th> */}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.getHsnCodeData == null || this.state.getHsnCodeData.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.getHsnCodeData.map((data, key) => (
                                                    <tr key={key} onClick={(e) => this.onSelectedData(`${data.hsnSacCode}`)} >
                                                        <td>  <label className="select_modalRadio" >
                                                            <input type="radio" name="vendorMrpCheck " id={data.hsnSacCode} onKeyDown={(e) => this.focusDone(e)} checked={this.state.hsnSacCode == `${data.hsnSacCode}`} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        {/* <td>{data.itemCode}</td> */}

                                                        <td className="pad-lft-8">{data.hsnSacCode}</td>
                                                        {/* <td>{data.rsp}</td>
                                               */}
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={() => this.onDone()}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onCloseSection(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button" >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 && this.state.prev != "" ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.next - 1 != this.state.maxPage && this.state.maxPage != 0 ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div> :
                this.state.modalDisplay ? <div className="poArticleModalPosition" id="hsnModalPosition" >
                    <div className="dropdown-menu-city2 dropdown-menu-vendor pi-modal-po" id="pocolorModel">

                        <ul className="dropdown-menu-city-item">
                            {this.state.getHsnCodeData == undefined || this.state.getHsnCodeData.length == 0 ? <li><span>No Data Found</span></li> :
                                this.state.getHsnCodeData.map((data, key) => (
                                    <li key={key} onClick={(e) => this.onSelectedData(`${data.hsnSacCode}`)} id={data.hsnSacCode + "hsnSacCode"} className={this.state.hsnSacCode == `${data.hsnSacCode}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.hsnSacCode)}>
                                        <span className="vendor-details">
                                            <span className="vd-name div-col-1">{data.hsnSacCode}</span>

                                        </span>
                                    </li>))}

                        </ul>
                        <div className="gen-dropdown-pagination">
                            <div className="page-close">
                                <button className="btn-close" type="button" onClick={(e) => this.onCloseSection(e)} id="btn-close">Close</button>
                                {/* <button className="btn-clear" type="button">Clear</button> */}
                            </div>
                            <div className="page-next-prew-btn margin-180">
                                {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                                </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                                <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                                </button>
                                    : <button className="pnpb-next" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                    </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                            </div>
                        </div>
                    </div>
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                </div> : null


        );
    }
}

export default HsnCodeModal;
