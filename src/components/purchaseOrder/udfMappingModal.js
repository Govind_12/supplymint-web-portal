import React from "react";

import ToastLoader from "../loaders/toastLoader";

class UdfMappingModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            udfTypeState: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            search: "",
            toastLoader: false,
            sectionSelection: "",

            toastMsg: "",
            code: "",
            name: "",
            setValue: this.props.setValue,
            setCode: "",
            searchBy: "startWith",
            focusedLi: "",
            modalDisplay: false,

        }
    }
    componentDidMount() {
        if (this.props.isModalShow || this.props.isModalShow == undefined) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
        document.addEventListener('mousedown', this.handleClickOutsideSet)
    }

    handleClickOutsideSet = (e) => {
        if (this.textInput && !this.textInput.current.contains(e.target) && e.target.id != this.props.focusId){
            this.closeUdf()
        }
    }

    componentWillUnmount(){
        document.removeEventListener('mousedown', this.handleClickOutsideSet)
    }

    componentWillMount() {
        setTimeout(() => {
            this.setState({
                setValue: this.props.setValue,
                search: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.setUdfSearch,
                type: this.props.isModalShow || this.props.isModalShow == undefined || this.props.setUdfSearch == "" ? 1 : 3

            })

        }, 10)

        if (this.props.purchaseIndent.udfType.isSuccess) {

            if (this.props.purchaseIndent.udfType.data.resource != null) {

                this.setState({
                    udfTypeState: this.props.purchaseIndent.udfType.data.resource,
                    prev: this.props.purchaseIndent.udfType.data.prePage,
                    current: this.props.purchaseIndent.udfType.data.currPage,
                    next: this.props.purchaseIndent.udfType.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.udfType.data.maxPage,
                })
            } else {
                this.setState({
                    udfTypeState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }


    }

    handleChange(e) {
        if (e.target.id == "search") {

            this.setState(
                {
                    search: e.target.value
                },

            );

            //       if(document.getElementById("basedOn").value=="code"){
            //           this.setState(
            //               {
            //                   code: e.target.value,
            //                   name:"",

            //               },

            //           );
            //       }
            //      else if(document.getElementById("basedOn").value=="name"){
            //           this.setState(
            //               {
            //                   code:"" ,
            //                   name:e.target.value,

            //               },

            //           );
            //       }    
            //       else if(document.getElementById("basedOn").value==""){
            //           this.setState(
            //               {
            //                   code:"" ,
            //                   name:"",

            //                   search:e.target.value
            //               },

            //           );

            //   }
        } else if (e.target.id == "searchBymapmodal") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        udfType: this.props.udfType,
                        name: this.state.name,
                        code: this.state.code,
                        search: this.state.search,
                        ispo: true,
                        searchBy: this.state.searchBy
                    }
                    this.props.udfTypeRequest(data)

                }
            })
        }
        // else if (e.target.id == "basedOn") {
        //       this.setState(
        //           {
        //               search: "",
        //               sectionSelection:e.target.value
        //           },
        //       );


        //   }

    }

    onSearch(e) {

        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter data on search input",
                toastLoader: true,

            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,
                    udfType: this.props.udfType,
                    name: this.state.name,
                    code: this.state.code,
                    search: this.state.search,
                    ispo: true,
                    searchBy: this.state.searchBy
                }
                this.props.udfTypeRequest(data)

            }
            else {
                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    udfType: this.props.udfType,
                    name: this.state.name,
                    code: this.state.code,
                    search: "",
                    ispo: true,
                    searchBy: this.state.searchBy
                }
                this.props.udfTypeRequest(data)
            }
        }

    }
    onsearchClear() {

        if (this.state.type == 2 || this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                udfType: this.props.udfType,
                name: "",
                code: "",
                search: "",
                ispo: true,
                searchBy: this.state.searchBy
            }
            this.props.udfTypeRequest(data)
        }
        this.setState(
            {
                search: "",
                sectionSelection: "",
                type: "",
                no: 1
            })
        document.getElementById("search").focus()
    }
    componentWillReceiveProps(nextProps) {


        if (nextProps.purchaseIndent.udfType.isSuccess) {
            let c = []
            if (nextProps.purchaseIndent.udfType.data.resource != null) {

                this.setState({
                    udfTypeState: nextProps.purchaseIndent.udfType.data.resource,
                    prev: nextProps.purchaseIndent.udfType.data.prePage,
                    current: nextProps.purchaseIndent.udfType.data.currPage,
                    next: nextProps.purchaseIndent.udfType.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.udfType.data.maxPage,
                })
            } else {
                this.setState({
                    udfTypeState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

            if (window.screen.width > 1200) {
                if (this.props.isModalShow || this.props.isModalShow == undefined) {
                    this.textInput.current.focus();


                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.udfType.data.resource != null) {
                        this.setState({
                            focusedLi: "setUdf"+ nextProps.purchaseIndent.udfType.data.resource[0].name,
                            search: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.setUdfSearch,
                            type: this.props.isModalShow || this.props.isModalShow == undefined || this.props.setUdfSearch == "" ? 1 : 3

                        })
                        document.getElementById("setUdf"+nextProps.purchaseIndent.udfType.data.resource[0].name) != null ? document.getElementById("setUdf"+nextProps.purchaseIndent.udfType.data.resource[0].name).focus() : null
                    }


                }
            }
            this.setState({
                modalDisplay: true
            })
        }


    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.udfType.data.prePage,
                current: this.props.purchaseIndent.udfType.data.currPage,
                next: this.props.purchaseIndent.udfType.data.currPage + 1,
                maxPage: this.props.purchaseIndent.udfType.data.maxPage,
            })
            if (this.props.purchaseIndent.udfType.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.udfType.data.currPage - 1,
                    udfType: this.props.udfType,
                    name: this.state.name,
                    code: this.state.code,
                    search: this.state.search,
                    ispo: true,
                    searchBy: this.state.searchBy
                }
                this.props.udfTypeRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.udfType.data.prePage,
                current: this.props.purchaseIndent.udfType.data.currPage,
                next: this.props.purchaseIndent.udfType.data.currPage + 1,
                maxPage: this.props.purchaseIndent.udfType.data.maxPage,
            })
            if (this.props.purchaseIndent.udfType.data.currPage != this.props.purchaseIndent.udfType.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.udfType.data.currPage + 1,
                    udfType: this.props.udfType,
                    name: this.state.name,
                    code: this.state.code,
                    search: this.state.search,
                    ispo: true,
                    searchBy: this.state.searchBy
                }
                this.props.udfTypeRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.udfType.data.prePage,
                current: this.props.purchaseIndent.udfType.data.currPage,
                next: this.props.purchaseIndent.udfType.data.currPage + 1,
                maxPage: this.props.purchaseIndent.udfType.data.maxPage,
            })
            if (this.props.purchaseIndent.udfType.data.currPage <= this.props.purchaseIndent.udfType.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    udfType: this.props.udfType,
                    name: this.state.name,
                    code: this.state.code,
                    search: this.state.search,
                    ispo: true,
                    searchBy: this.state.searchBy
                }
                this.props.udfTypeRequest(data)
            }

        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }


    selectedData(e) {
        let array = this.state.udfTypeState

        for (let i = 0; i < array.length; i++) {
            if (array[i].name == e) {
                this.setState({
                    setValue: array[i].name,
                    setCode: array[i].code
                })
            }
        }
        this.setState({
            udfTypeState: array
        }, () => {
            if (!this.props.isModalShow) {
                this.onDone()
            }
        })
    }



    onDone(e) {
        console.log('setValue', this.state.setValue, 'p', this.props.setValue, 'search', this.props.setUdfSearch, 's', this.state.search)
        if (this.state.setValue != undefined || this.state.setValue != "" || this.props.setUdfSearch == this.state.search || this.props.setUdfSearch != this.state.search) {

            // let array = this.state.udfTypeState
            //  for(let i=0;i<array.length;i++){
            if (this.props.setValue != this.state.setValue) {
                let data = {
                    udfType: this.props.udfType,
                    code: this.state.setCode,
                    name: this.state.setValue,
                    udfRow: this.props.udfRow,
                    mappingId: this.props.mappingId
                }


                let t = this
                // setTimeout( function(){
                this.props.updateUdf(data);
                // }, 10)


                // }
            } this.closeUdf(e)

        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }
    closeUdf() {

        this.props.closeUdf()
        const t = this

        t.setState({
            udfTypeState: [],
            search: "",
            sectionSelection: "",


            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })


        // document.getElementById("basedOn").value!=null?basedOn"":null

    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    deselectValue() {

        let data = {
            udfType: this.props.udfType,
            code: "",
            name: "",
            udfRow: this.props.udfRow

        }
        this.props.updateUdfSetvalue()
        this.props.updateUdf(data);
        window.setTimeout(() => {
            document.getElementById("search").focus()
        }, 0)

    }

    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.setValue != "") {

                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById("deselectButton").focus()
                    }, 0)
                } else {
                    if (this.state.udfTypeState.length != 0) {
                        this.setState({
                            setValue: this.state.udfTypeState[0].name
                        })
                        this.selectedData(this.state.udfTypeState[0].name)
                        let icode = "setUdf"+this.state.udfTypeState[0].name
                        window.setTimeout(function () {
                            document.getElementById("search").blur()
                            document.getElementById(icode).focus()
                        }, 0)

                    } else {
                        window.setTimeout(function () {
                            document.getElementById("search").blur()
                            document.getElementById("closeButton").focus()
                        }, 0)
                    }

                }
            }
            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }

        if (e.key === "ArrowDown") {
            if (this.state.udfTypeState.length != 0) {
                this.setState({
                    setValue: this.state.udfTypeState[0].name
                })
                this.selectedData(this.state.udfTypeState[0].name)
                let icode ="setUdf"+ this.state.udfTypeState[0].name
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById(icode).focus()
                }, 0)

            } else {
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }

        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            const t = this
            window.setTimeout(function () {

                document.getElementById("findButton").blur()
                t.state.setValue == "" ? document.getElementById("clearButton").focus()
                    : document.getElementById("deselectButton").focus()
            }, 0)
        }

    }
    focusDone(e) {

        if (e.key === "Tab") {
            let icode = this.state.udfTypeState[0].name

            window.setTimeout(function () {


                document.getElementById(icode).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.onDone(e);
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {
            this.closeUdf();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("search").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.udfTypeState.length != 0) {
                this.setState({
                    setValue: this.state.udfTypeState[0].name
                })
                let icode = "setUdf"+ this.state.udfTypeState[0].name
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById(icode).focus()
                }, 0)
            } else {
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById("search").focus()
                }, 0)
            }
        }
    }

    onDeselectValue(e) {
        if (e.key === "Enter") {
            this.deselectValue();
        }
        if (e.key === "Tab") {
            if (this.state.search == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.udfTypeState.length != 0) {
                    this.setState({
                        setValue: this.state.udfTypeState[0].name
                    })
                    let icode = "setUdf"+ this.state.udfTypeState[0].name
                    window.setTimeout(function () {

                        document.getElementById("deselectButton").blur()
                        document.getElementById(icode).focus()
                    }, 0)
                } else {
                    window.setTimeout(function () {

                        document.getElementById("deselectButton").blur()
                        document.getElementById("search").focus()
                    }, 0)
                }

            } else {
                window.setTimeout(function () {

                    document.getElementById("deselectButton").blur()
                    document.getElementById("clearButton").focus()
                }, 0)
            }
        }
    }

    selectLi(e, code) {
        let udfTypeState = this.state.udfTypeState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < udfTypeState.length; i++) {
                if (udfTypeState[i].name == code) {
                    index = i
                }
            }
            if (index < udfTypeState.length - 1 || index == 0) {
                document.getElementById("setUdf"+udfTypeState[index + 1].name) != null ? document.getElementById("setUdf"+udfTypeState[index + 1].name).focus() : null

                this.setState({
                    focusedLi: "setUdf"+udfTypeState[index + 1].name
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < udfTypeState.length; i++) {
                if (udfTypeState[i].name == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById("setUdf"+udfTypeState[index - 1].name) != null ? document.getElementById("setUdf"+udfTypeState[index - 1].name).focus() : null

                this.setState({
                    focusedLi:"setUdf"+ udfTypeState[index - 1].name
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
            

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next") != null ? document.getElementById("next").focus() : null }

        }
        if (e.key == "Escape") {
            this.closeUdf(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.udfTypeState.length != 0 ?
                            document.getElementById("setUdf"+this.state.udfTypeState[0].name).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.udfTypeState.length != 0) {
                    document.getElementById("setUdf"+this.state.udfTypeState[0].name).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeUdf(e)
        }


    }

    render() {


        if (this.state.focusedLi != "" && this.props.setUdfSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        if (this.props.setUdfId != "" && this.props.setUdfId != undefined && !this.props.isModalShow) {
            console.log(this.state.setUdfId)
            let modalWidth = 500;
            let modalWindowWidth = document.getElementById('itemSetModal').getBoundingClientRect();
            let position = document.getElementById(this.props.setUdfId).getBoundingClientRect();
            let leftPosition = position.left;
            let idNo = parseInt(this.props.setUdfId.split('_')[1]) + 1;
            let top = 60 + (35 * idNo);
            let newLeft = 0;
            let diff = modalWindowWidth.width - leftPosition;

            if (diff >= modalWidth) {

                newLeft = leftPosition > 157 ? leftPosition - 157 : leftPosition - 47;
            }
            else {
                let removeWidth = modalWidth - diff;
                newLeft = (leftPosition >= 560 && leftPosition < 700) ? leftPosition - removeWidth - 127 : leftPosition - removeWidth - 37;

                // newLeft = leftPosition <= 700 ? (leftPosition - removeWidth - 77) : (leftPosition - removeWidth - 5);
            }
            console.log("diff:" + diff, "top:" + top, "leftPosition:" + leftPosition, "newLeft:" + newLeft)

            $('#setUdfModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');

        }
        const { search, sectionSelection } = this.state
        return (

            this.props.isModalShow || this.props.isModalShow == undefined ? <div className={this.props.udfMappingAnimation ? "modal display_block" : "display_none"} id="udfmappingModel">
                <div className={this.props.udfMappingAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.udfMappingAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.udfMappingAnimation ? "modal-content modaludfMapping modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Mapping">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">Select {this.props.udfName == null || this.props.udfName == "null" ? this.props.udfType : this.props.udfName}</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 chooseDataModal">

                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn">

                                            <div className="mrpSelectCode">
                                                <li>

                                                    {/* <select id="basedOn" className="width_20" name="sectionSelection" value={sectionSelection} onChange={e => this.handleChange(e)}>
                                        <option value="">Choose</option>
                                        <option value="name">Name</option>
                                        <option value="code">Code</option>
                                    </select> */}
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBymapmodal" name="searchBymapmodal" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" onKeyPress={this._handleKeyPress} onKeyDown={this._handleKeyDown} ref={this.textInput} className="search-box" value={search} id="search" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div> </div>
                                        <li className="float_right">
                                            <label>
                                                <button type="button" className={this.state.setValue == "" ? "btnDisabled clearbutton widthAuto m-rgt-20" : "clearbutton widthAuto m-rgt-20"} id="deselectButton" onKeyDown={(e) => this.onDeselectValue(e)} onClick={(e) => this.deselectValue(e)}>DESELECT</button>
                                            </label>
                                            <label>
                                                <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th> Code</th>
                                                    <th>{this.props.udfName == null || this.props.udfName == "null" ? this.props.udfType : this.props.udfName}</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.udfTypeState == undefined || this.state.udfTypeState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.udfTypeState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.udfTypeState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.name}`)}>
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={"setUdf" +data.name} type="radio" name="udfType" checked={`${data.name}` == this.state.setValue} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            {data.code}

                                                        </td>
                                                        <td className="pad-lft-8">
                                                            {data.name}
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom m-top-10">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeUdf(e)}>Close</button>
                                            </label>
                                        </li>

                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
                :
                this.state.modalDisplay ? <div ref={this.textInput} className="poArticleModalPosition" id="setUdfModalPosition">

                    <div className="dropdown-menu-city2 dropdown-menu-vendor" id="pocolorModel">

                        <ul className="dropdown-menu-city-item">
                            {this.state.udfTypeState == undefined || this.state.udfTypeState.length == 0 ? <li><span>No Data Found</span></li> :
                                this.state.udfTypeState.map((data, key) => (
                                    <li key={key} onClick={(e) => this.selectedData(`${data.name}`)} id={"setUdf"+data.name} className={this.state.itemValue == `${data.name}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.name)}>
                                        <span className="vendor-details">
                                            <span className="vd-name">{data.name}</span>

                                        </span>
                                    </li>))}

                        </ul>
                        <div className="gen-dropdown-pagination">
                            <div className="page-close">
                                <button className="btn-close" type="button" onClick={(e) => this.closeUdf(e)} id="btn-close">Close</button>
                                {/* <button className="btn-clear" type="button">Clear</button> */}
                            </div>
                            <div className="page-next-prew-btn">
                                {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                                </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                                <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                                </button>
                                    : <button className="pnpb-next" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                    </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                            </div>
                        </div>
                    </div>
                </div> : null


        );
    }
}

export default UdfMappingModal;
