import React from "react";
import ToastLoader from "../loaders/toastLoader";
class SetDepartmentModal extends React.Component {
    constructor(props) {
        super(props);
        this.searchInput = React.createRef();
        this.state = {
            searchMrp: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            setDepartment: this.props.setDepartment,
            hl3code: this.props.hl3CodeDepartment,
            searchBy: "startWith",
            focusedLi: "",
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.departmentSetBased.data.prePage,
                current: this.props.purchaseIndent.departmentSetBased.data.currPage,
                next: this.props.purchaseIndent.departmentSetBased.data.currPage + 1,
                maxPage: this.props.purchaseIndent.departmentSetBased.data.maxPage,
            })
            if (this.props.purchaseIndent.departmentSetBased.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.departmentSetBased.data.currPage - 1,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.departmentSetBasedRequest(data);
            }
            this.searchInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.departmentSetBased.data.prePage,
                current: this.props.purchaseIndent.departmentSetBased.data.currPage,
                next: this.props.purchaseIndent.departmentSetBased.data.currPage + 1,
                maxPage: this.props.purchaseIndent.departmentSetBased.data.maxPage,
            })
            if (this.props.purchaseIndent.departmentSetBased.data.currPage != this.props.purchaseIndent.departmentSetBased.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.departmentSetBased.data.currPage + 1,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.departmentSetBasedRequest(data)
            }
            this.searchInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.departmentSetBased.data.prePage,
                current: this.props.purchaseIndent.departmentSetBased.data.currPage,
                next: this.props.purchaseIndent.departmentSetBased.data.currPage + 1,
                maxPage: this.props.purchaseIndent.departmentSetBased.data.maxPage,
            })
            if (this.props.purchaseIndent.departmentSetBased.data.currPage <= this.props.purchaseIndent.departmentSetBased.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.departmentSetBasedRequest(data)
            }
            this.searchInput.current.focus();
        }
    }

    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.searchInput.current.blur();
            } else {
                this.searchInput.current.focus();
            }
        }
        this.setState({
            setDepartment: this.props.setDepartment,
            hl3code: this.props.hl3CodeDepartment,

            searchMrp: this.props.isModalShow ? "" : this.props.setDepartmentSearch

        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.departmentSetBased.isSuccess) {
            if (nextProps.purchaseIndent.departmentSetBased.data.resource != null) {
                this.setState({
                    setBasedDepartment: nextProps.purchaseIndent.departmentSetBased.data.resource,
                    prev: nextProps.purchaseIndent.departmentSetBased.data.prePage,
                    current: nextProps.purchaseIndent.departmentSetBased.data.currPage,
                    next: nextProps.purchaseIndent.departmentSetBased.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.departmentSetBased.data.maxPage,
                })
            } else {
                this.setState({
                    setBasedDepartment: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {
                    this.searchInput.current.focus();

                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.departmentSetBased.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.departmentSetBased.data.resource[0].hl3Code
                        })
                        document.getElementById(nextProps.purchaseIndent.departmentSetBased.data.resource[0].hl3Code) != null ? document.getElementById(nextProps.purchaseIndent.departmentSetBased.data.resource[0].hl3Code).focus() : null
                    }



                }
            }
        }

    }


    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                type: "",
                no: 1
            })
        if (this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                search: "",
                searchBy: this.state.searchBy
            }
            this.props.departmentSetBasedRequest(data)
        }
        document.getElementById("searchMrp").focus()
    }

    handleChange(e) {
        if (e.target.id == "searchMrp") {
            this.setState({
                searchMrp: e.target.value
            })
        } else if (e.target.id == "searchBySetDepart") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.searchMrp != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.searchMrp,
                        searchBy: this.state.searchBy
                    }
                    this.props.departmentSetBasedRequest(data)
                }
            })
        }
    }

    selectedData(e) {
        let setBasedDepartment = this.state.setBasedDepartment;
        for (let i = 0; i < setBasedDepartment.length; i++) {
            if (setBasedDepartment[i].hl3Code == e) {
                this.setState({
                    setDepartment: setBasedDepartment[i].hl3Name,
                    hl3code: setBasedDepartment[i].hl3Code,
                })
            }
        }
        this.setState({
            setBasedDepartment: setBasedDepartment
        }, () => {
            if (!this.props.isModalShow) {
                this.onDone()
            }
        })
    }

    onDone(e) {
        let setBasedDepartment = this.state.setBasedDepartment
        let data = {}
        // for (var i = 0; i < setBasedDepartment.length; i++) {
        if (this.state.setDepartment != "") {
            //         if (setBasedDepartment[i].hl3Name == this.state.setDepartment) {
            data = {
                setDepartment: this.state.setDepartment,
                hl3code: this.state.hl3code,
            }
            //         }
            //     }
            // }
            setTimeout(() => {
                this.props.updateDepartment(data)
            })
        }
        // for (var i = 0; i < setBasedDepartment.length; i++) {
        if (this.state.setDepartment != "") {
            // if (setBasedDepartment[i].hl3Name == this.state.setDepartment) {
            this.setState(
                {
                    search: "",
                })

            // }
            this.props.onCloseDepartmentModal()
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1500)
        }
        // }

    }

    onSearch(e) {

        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.searchMrp,
                searchBy: this.state.searchBy
            }
            this.props.departmentSetBasedRequest(data)
        }
    }

    _handleKeyDown(e) {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.setBasedDepartment.length != 0) {
                    this.setState({
                        setDepartment: this.state.setBasedDepartment[0].hl3Name,
                        hl3code: this.state.setBasedDepartment[0].hl3Code
                    })
                    let setDepartment = this.state.setBasedDepartment[0].hl3Name
                    this.selectedData(setDepartment)
                    window.setTimeout(function () {
                        document.getElementById("searchMrp").blur()
                        document.getElementById(setDepartment).focus()
                    }, 0);
                } else {
                    window.setTimeout(function () {
                        document.getElementById("searchMrp").blur()
                        document.getElementById("closeButton").focus()
                    }, 0);
                }
            }

            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("findButton").focus();
                }, 0);
            }
        }
        if (e.key === "ArrowDown") {
            if (this.state.setBasedDepartment.length != 0) {
                this.setState({
                    setDepartment: this.state.setBasedDepartment[0].hl3Name,
                    hl3code: this.state.setBasedDepartment[0].hl3Code
                })
                let setDepartment = this.state.setBasedDepartment[0].hl3Name
                this.selectedData(setDepartment)
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById(setDepartment).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("closeButton").focus()
                }, 0);
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0);
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let setDepartment = this.state.setBasedDepartment[0].hl3Name
            window.setTimeout(function () {
                document.getElementById(setDepartment).blur()
                document.getElementById("doneButton").focus()
            }, 0);
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone(e);
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0);
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.props.onCloseDepartmentModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("searchMrp").focus()
            }, 0);
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.setBasedDepartment.length != 0) {
                this.setState({
                    setDepartment: this.state.setBasedDepartment[0].hl3Name,
                    hl3code: this.state.setBasedDepartment[0].hl3Code
                })
                let setDepartment = this.state.setBasedDepartment[0].hl3Name
                this.selectedData(setDepartment)

                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(setDepartment).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("searchMrp").focus()
                }, 0);
            }

        }
    }
    _handleKeyPress(e) {
        if (e.key === 'Enter') {
            if (e.target.value) {
                this.onSearch();
            }
        }
    }
    selectLi(e, code) {
        let setBasedDepartment = this.state.setBasedDepartment
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < setBasedDepartment.length; i++) {
                if (setBasedDepartment[i].hl3Code == code) {
                    index = i
                }
            }
            if (index < setBasedDepartment.length - 1 || index == 0) {
                document.getElementById(setBasedDepartment[index + 1].hl3Code) != null ? document.getElementById(setBasedDepartment[index + 1].hl3Code).focus() : null

                this.setState({
                    focusedLi: setBasedDepartment[index + 1].hl3Code
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < setBasedDepartment.length; i++) {
                if (setBasedDepartment[i].hl3Code == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(setBasedDepartment[index - 1].hl3Code) != null ? document.getElementById(setBasedDepartment[index - 1].hl3Code).focus() : null

                this.setState({
                    focusedLi: setBasedDepartment[index - 1].hl3Code
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.props.onCloseDepartmentModal(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.setBasedDepartment.length != 0 ?
                            document.getElementById(this.state.setBasedDepartment[0].hl3Code).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.setBasedDepartment.length != 0) {
                    document.getElementById(this.state.setBasedDepartment[0].hl3Code).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.props.onCloseDepartmentModal(e)
        }


    }


    render() {
        if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }


        const { searchMrp } = this.state;
        return (

            this.props.isModalShow ? <div className={this.props.departmentSetBasedAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.departmentSetBasedAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.departmentSetBasedAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.departmentSetBasedAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT DEPARTMENT</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only one Department from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn chooseDataModal">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBySetDepart" name="searchBySetDepart" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.searchInput} onKeyDown={(e) => this._handleKeyDown(e)} onKeyPress={(e) => this._handleKeyPress(e)} onChange={(e) => this.handleChange(e)} value={searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                    <label>
                                                        {this.state.searchMrp == "" && (this.state.type == "" || this.state.type == 1) ?
                                                            <button type="button" id="clearButton" className="clearbutton m-lft-15 btnDisabled" >CLEAR</button>
                                                            : <button type="button" id="clearButton" className="clearbutton m-lft-15" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                        }
                                                    </label>
                                                </li>


                                            </div>
                                        </div>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover departmentSetBasedMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    {sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? <th>Division </th> : null}
                                                    {sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? <th>Section</th> : null}
                                                    <th>Department code</th>
                                                    <th>Department Name</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.setBasedDepartment == undefined || this.state.setBasedDepartment == null || this.state.setBasedDepartment.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.setBasedDepartment.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.hl3Code}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.hl3Code} onKeyDown={(e) => this.focusDone(e)} checked={this.state.hl3code == `${data.hl3Code}`} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        {sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? <td>{data.hl1Name}</td> : null}
                                                        {sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? <td>{data.hl2Name}</td> : null}
                                                        <td>{data.hl3Code}</td>
                                                        <td>{data.hl3Name}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" id="doneButton" className="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" id="closeButton" className="closeButton" data-dismiss="modal" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.props.onCloseDepartmentModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn" type="button"  >
                                                    First
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                                                    </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                                                    </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                                                </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}

            </div>
                : <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">
                    <div className="dropdown-modal-header">
                        <span className="div-col-4">Name</span>
                        <span className="div-col-4">Code</span>
                        <span className="div-col-4">Name</span>
                        <span className="div-col-4">Code</span>
                    </div>

                    <ul className="dropdown-menu-city-item">
                        {this.state.setBasedDepartment != undefined || this.state.setBasedDepartment.length != 0 ?
                            this.state.setBasedDepartment.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.hl3Code}`)} id={data.hl3Code} className={this.state.hl3code == `${data.hl3Code}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.hl3Code)}>
                                    <span className="vendor-details">

                                        <span className="vd-name">{data.hl3Name}</span>
                                        <span className="vd-loc">{data.hl3Code}</span>
                                        <span className="vd-num">{data.hl1Name}</span>
                                        <span className="vd-code">{data.hl1Code}</span>

                                    </span>
                                </li>)) : <li><span>No Data Found</span></li>}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                            <button className="btn-close" type="button" onClick={(e) => this.props.onCloseDepartmentModal(e)} id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn margin-180">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                     <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>

        );
    }
}

export default SetDepartmentModal;
