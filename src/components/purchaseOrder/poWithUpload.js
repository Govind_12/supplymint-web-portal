import React, { Component } from 'react'
import fileIcon from '../../assets/attachment.svg';
import { CONFIG } from '../../config/index';
import axios from 'axios';
import RequestSuccess from '../loaders/requestSuccess';
import RequestError from '../loaders/requestError';
export default class PoWithUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fileData: "",
            fileName: "",
            generateTemplate: false,
            errorCode:"",
            code:"",
            successMessage:"",
            success: false,
            alert: false,
            errorMessage:""
        }
    }

    onError(e) {
        e.preventDefault();
        this.setState({
          alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
      }

      onRequest(e) {
        e.preventDefault();
        this.setState({
          success: false
        });
      }
    // ___________________________UPLOAD PO _____________________________

    changeFile(e) {
        let values = e.target.files
      

        if (values[0].type == "application/vnd.ms-excel" || values[0].type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" 
           || values[0].type == "text/csv" ) {
            if (values[0].size < 52428800) {
                this.setState({
                    fileData: values[0],
                    fileName: values[0].name
                })
            }
        }
    }

    uploadPo(e) {
        const formData = new FormData();
        this.setState({
            generateTemplate: true
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        formData.append('type', "FPO");
        formData.append('fileData', this.state.fileData);

        axios.post(`${CONFIG.BASE_URL}${CONFIG.PO}/upload/data`, formData, { headers: headers })
            .then(res => {
                if (res.data.status == "2000") {
                    this.setState({
                        generateTemplate: false,
                        success: true,
                        successMessage: res.data.data.message,
                        fileName: "",
                        fileData:""
                    })
                } else if (res.data.status == "4000") {
                    this.setState({
                        alert: true,
                        alert: true,
                        code: res.data.status,
                        errorMessage: res.data.data.message,
                    })
                }
            }).catch((error) => {
                this.setState({
                    generateTemplate: false,
                })
            })
    }

    discardModal(e){
        this.setState({
            fileName:"",
            fileData:""
        })
    }


    render() {
        return (
            <div>
                <div className="container-fluid pad-l65">
                    <div className="container_div">
                        <div className="col-md-12 col-sm-12 col-xs-12 udfMappingMain setUdfMappingMain itemUdfSet udfMappingHead">
                            <div className="container_content heightAuto">
                                {/* <div className="backdrop display_block"></div> */}
                                <div className="display_block rightModal">
                                    <div className="col-md-12 col-sm-12 poUploadModal pad-0">
                                        <div className="modal_Color selectVendorPopUp">
                                            <div className="modalTop">
                                                <div className="col-md-12 pad-0 m-top-20 text-center">
                                                    <h3>Generate PO with Upload</h3>
                                                    <div className="content m-top-80">
                                                        {/* <input type="file" className="custom-file-input" name="files" placeholder="Select File" /> */}
                                                        <label className="file" >
                                                            <input type="file" className={!this.state.generateTemplate ? "" : "btnDisabled"} onChange={(e) => !this.state.generateTemplate ? this.changeFile(e) : null} />
                                                            <span title={this.state.fileName}>{this.state.fileName == "" ? "Choose file" : this.state.fileName}</span>
                                                            <img src={fileIcon} />
                                                        </label>
                                                        {/* <button type="button" className="uploadBtn" onClick={()=> this.state.uploadPo()}>Upload</button> */}
                                                        <p className="note m-top-60"><span>Note :</span>File size Should not exceed 50MB & Valid file size should be .xls and csv only</p>
                                                        {this.state.generateTemplate ? <div className="generateLoader m-top-80"><p>Generating...</p></div> : null}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="modalBottom alignMiddle displayFlex justifyCenter">
                                                <div className="footerBtn">
                                                    <button className={this.state.fileName != "" ? "displayBlock submitBtn" : "btnDisabled displayBlock submitBtn"} onClick={() =>this.state.fileName != "" ? this.uploadPo() : null}>Submit</button>
                                                    {this.state.fileName != "" ? <span className="displayBlock discardBtn" onClick={(e) => this.discardModal(e)}>Clear</span> :
                                                        <span className="displayBlock discardBtn textDisable" >Clear</span>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}
