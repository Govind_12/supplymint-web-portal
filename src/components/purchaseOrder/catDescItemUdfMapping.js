import React from 'react';
import Pagination from '../pagination';
import ConfigArticle from './configMapModal';
import ChooseValue from './chooseValueModal';

class CatDescItemUdfMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '....',
            pageNo: 1,
            current: 1,
            maxPage: 1,
            prev: 0,
            updatedTableData: [],
            tableSearch: '',
            resultUDFType: [{ displayName:"", ext:"N", map:"Y", description:"shirt", code:"" }],
            isInsert: true,
            searchVal: '',
            chooseName : "",
            extValue : "",
            siteModal: false,
            chooseModalAnimation : false,
            valueSearch:'',
            isAdded: false,
            catValue: '',
            catType: '',
            tableData: [],
            isMrpArticle: '',
            selectCat: [],
            divisionCheck: false,
            sectionCheck: false,
            departmentCheck: true,
            articleCheck: false,
            exportToExcel: false,
            selectDepartment: false,
            selectDivision: false,
            selectSection: false,
            selectArticle: false,
            pageDetails: '',
            currentActive: "Division",
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            poArticle: true,
            poArticleAnimation: true,
            hl4Code: "",
            hl4Name: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            hl3Code: "",
            basedOn: "division",
            poArticleSearch: "",
            mrpRangeSearch: false,
            hierarchyDropHandle: false,
        }
    }

    componentDidMount() {   
        this.statusHandler()
        let data = {
            template : false,
            type : ''
        }
        this.props.mappingExcelStatusRequest(data)        
    }

    componentWillUnmount() {
        clearInterval(this.callStatus)
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.purchaseIndent.mappingExcelStatus.isSuccess){
            if(nextProps.purchaseIndent.mappingExcelStatus.data.resource != null){
                if((nextProps.purchaseIndent.mappingExcelStatus.data.resource.status != null || nextProps.purchaseIndent.mappingExcelStatus.data.resource.status != "") && (nextProps.purchaseIndent.mappingExcelStatus.data.resource.templateUrl == null || nextProps.purchaseIndent.mappingExcelStatus.data.resource.templateUrl == "")){
                    this.setState({
                        status: nextProps.purchaseIndent.mappingExcelStatus.data.resource.status,
                    })
                    this.props.mappingExcelStatusClear();
                }
            }
        }
        if(nextProps.purchaseIndent.mappingExcelUpload.isSuccess){
            this.setState({
                status: "...."
            }, () => this.statusHandler())
        }
        if (nextProps.purchaseIndent.updateItemUdfMapping.isSuccess){
            let data = {
                type: 1,
                description: "",
                itemUdfType: this.state.catValue,
                ispo: "false",
                no: 1,
                search: "",
                hl3Code: this.state.hierarchy["Department"].code,
                hl4Code: this.state.hierarchy["Article"].code,
                hl4Name: this.state.hierarchy["Article"].name,
                searchBy: 'contains'
            }
            this.setState({
                updatedTableData: [],
                searchVal: '',
                valueSearch: '',
            })
            this.props.saveCheckMap('')
            this.props.itemCatDescUdfRequest(data)            
            this.props.updateItemUdfMappingClear()
        }
        if (nextProps.purchaseIndent.itemCatDescUdf.isSuccess){
            if(nextProps.purchaseIndent.itemCatDescUdf.data.resource != null){
                this.setState({
                    tableData: nextProps.purchaseIndent.itemCatDescUdf.data.resource,
                    current : nextProps.purchaseIndent.itemCatDescUdf.data.currPage,
                    maxPage: nextProps.purchaseIndent.itemCatDescUdf.data.maxPage,
                    prev: nextProps.purchaseIndent.itemCatDescUdf.data.prePage,
                    pageNo: nextProps.purchaseIndent.itemCatDescUdf.data.currPage,
                }) 
            } else {
                this.setState({
                    tableData: [],
                    current: 1,
                    maxPage: 1,
                    prev: 0,
                    pageNo: 1,
                }) 
            }
            this.props.saveCheckMap('')
            this.props.itemCatDescUdfClear()
        }
        if (nextProps.purchaseIndent.catDescDropdown.isSuccess) {
            this.setState({
                selectCat: nextProps.purchaseIndent.catDescDropdown.data.resource,
            })
        }
        if (nextProps.purchaseIndent.poArticle.isSuccess){
            let pageDetails = {                
                currPage: nextProps.purchaseIndent.poArticle.data.currPage,                
                maxPage: nextProps.purchaseIndent.poArticle.data.maxPage,
            }
            this.setState({
                pageDetails: pageDetails
            })            
            this.props.poArticleClear()
        }
    }

    statusHandler = () => {
        let data = {
            template : false,
            type : ''
        }
        this.callStatus = setInterval(() =>  {
            this.props.mappingExcelStatusRequest(data)
            if(this.state.status.toLowerCase() == "succeeded" || this.state.status.toLowerCase() == "failed" || this.state.status.toLowerCase() == "no data found") {
                clearInterval(this.callStatus)
            }   
        } , 5000);  
   
    }

    updateMapCheck = (e, data, index) =>{ 
        let updatedData = [...this.state.updatedTableData]       
        let updateObj ={};
        let tableData = [...this.state.tableData];        
        if(e.target.id == 'Map'){
            let value = data.map == 'Y' ? 'N' : 'Y'
            updateObj = {...data, map: value}
        }
        if(e.target.id == 'Ext'){
            let value = data.ext == 'Y' ? 'N' : 'Y'
            updateObj = {...data, ext: value}
        }
        tableData[index] = {...updateObj};
        this.setState({
            tableData: tableData,
        })
        if(updatedData == '' || updatedData.filter(item=> item.description == data.description) == ''){
            updatedData.push({ displayName: updateObj.displayName, ext: updateObj.ext, map: updateObj.map, description: updateObj.description, code:"" })
        } else {
            updatedData = updatedData.map(item => {
                if(item.description == data.description ){
                    return { displayName: updateObj.displayName, ext: updateObj.ext, map: updateObj.map, description: updateObj.description, code:"" }
                } else {
                    return item
                }
            })
        }
        this.setState({
            updatedTableData: updatedData
        })
        this.props.saveCheckMap(updatedData)
    }

    showDropDown = (event) => {
        // event.preventDefault();
        this.setState({ hierarchyDropHandle: true }, () => {
            document.addEventListener('click', this.closeDropDown);
        });
    }

    closeDropDown = () => {
        this.setState({ hierarchyDropHandle: false }, () => {
            document.removeEventListener('click', this.closeDropDown);
        });
    }

    setHierarchySelectedData = (data) => {
        this.setState({
            search: "",
            hierarchy: { ...this.state.hierarchy, Division: { name: data.hl1Name, code: data.hl1Code } || "", Section: { name: data.hl2Name, code: data.hl2Code } || "", Department: { name: data.hl3Name, code: data.hl3Code } || "", Article: { name: data.hl4Name, code: data.hl4Code } || "" },
            hierarchyDropHandle: false
        }, () => {
            if(this.state.hierarchy["Department"].name != '' && this.state.hierarchy["Article"].name == ''){
                let payload = {
                    hl3Code: this.state.hierarchy["Department"].code,
                }     
                this.props.catDescDropdownRequest(payload);
            }
            if(this.state.hierarchy["Article"].name != ''){
                let data = {
                    type: 1,
                    description: "",
                    itemUdfType: this.state.catValue,
                    ispo: "false",
                    no: 1,
                    search: "",
                    hl3Code: this.state.hierarchy["Department"].code,
                    hl4Code: this.state.hierarchy["Article"].code,
                    hl4Name: this.state.hierarchy["Article"].name,
                    searchBy: 'contains'
                }
                this.props.itemCatDescUdfRequest(data)
            }
        })
    }
    handleHierarchyModal = () => {
        this.setState({ hierarchyDropHandle: !this.state.hierarchyDropHandle })
    };

    openAdded = () =>{
        this.setState({
            isAdded : true,
        })
    }

    closeAdded = () =>{
        this.setState({
            isAdded : false,
        })
    }

    enterMapValue = (e) =>{
        this.setState({
            valueSearch: e.target.value,
            resultUDFType: [{ displayName:"", ext:"N", map:"Y", description: e.target.value, code:"" }],
        })
    }

    clearMapValue = () =>{
        this.setState({
            valueSearch: '',
            searchVal: '',
        })
    }

    saveMapValue = () =>{
        this.setState({
            isInsert: true,
        }, () => this.mapHandler())
    }

    updateCatDescMapping = () =>{
        this.setState({
            isInsert: false,
        }, () => this.mapHandler())
    }

    resetCatDescMapping = () =>{
        let data = {
            type: 1,
            description: "",
            itemUdfType: this.state.catValue,
            ispo: "false",
            no: 1,
            search: "",
            hl3Code: this.state.hierarchy["Department"].code,
            hl4Code: this.state.articleCheck ? this.state.hierarchy["Article"].code : '',
            hl4Name: this.state.articleCheck ? this.state.hierarchy["Article"].name : '',
            searchBy: 'contains'
        }
        this.props.itemCatDescUdfRequest(data)
    }

    clearCatDescMapping = () =>{
        this.setState({
            tableData: [],
            isMrpArticle: '',
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
        }, this.props.saveCheckMap(''))
        document.getElementById('mySelectCat').value = 'Please Select'
    }

    pageHandler = (e) =>{
        let pageNo = 1;
        if(e.target.id == 'next'){
            pageNo = this.state.current + 1
        } else if(e.target.id == 'prev'){
            pageNo = this.state.current - 1;
        } else if(e.target.id == 'first'){
            pageNo = 1;
        } else if(e.target.id == 'last'){
            pageNo = this.state.maxPage;
        }
        let data = {
            type: this.state.tableSearch == '' ? 1 : 3,
            description: "",
            itemUdfType: this.state.catValue,
            ispo: "false",
            no: pageNo,
            search: this.state.tableSearch,
            hl3Code: this.state.hierarchy["Department"].code,
            hl4Code: this.state.articleCheck ? this.state.hierarchy["Article"].code : '',
            hl4Name: this.state.articleCheck ? this.state.hierarchy["Article"].name : '',
            searchBy: 'contains'
        }
        this.props.itemCatDescUdfRequest(data)
    }

    pageNoHandler = (e) =>{
        this.setState({
            pageNo: e.target.value,
        })
    }

    getItemData = (e) =>{
        let pageNo = 1;
        if (e.target.id == 'page') {
            pageNo = this.state.pageNo
        }
        if(e.keyCode == '13'){
            let data = {
                type: this.state.tableSearch == '' ? 1 : 3,
                description: "",
                itemUdfType: this.state.catValue,
                ispo: "false",
                no: pageNo,
                search: this.state.tableSearch,
                hl3Code: this.state.hierarchy["Department"].code,
                hl4Code: this.state.articleCheck ? this.state.hierarchy["Article"].code : '',
                hl4Name: this.state.articleCheck ? this.state.hierarchy["Article"].name : '',
                searchBy: 'contains'
            }
            this.props.itemCatDescUdfRequest(data)
        }
        
    }

    mapHandler = () =>{
        let data = {
            isInsert: this.state.isInsert,
            hl1code: this.state.divisionCheck ? this.state.hierarchy['Division'].code : '',
            hl2code: this.state.sectionCheck ? this.state.hierarchy['Section'].code : '',
            hl3code: this.state.departmentCheck ? this.state.hierarchy['Department'].code : '',
            hl3Name: this.state.departmentCheck ? this.state.hierarchy['Department'].name : '',
            hl4code: this.state.articleCheck ? this.state.hierarchy['Article'].code : '',
            hl4Name: this.state.articleCheck ? this.state.hierarchy['Article'].name : '',
            cat_desc_udf: this.state.catValue,
            resultUDFType: this.state.isInsert ? this.state.resultUDFType : this.state.updatedTableData,
        }
        this.props.updateItemUdfMappingRequest(data)
    }

    tableItemHandler = (e) =>{
        this.setState({
            isMrpArticle: e.target.value.split('|')[1],
            catType:e.target.value.split('|')[2],   
            catValue: e.target.value.split('|')[0],
            valueSearch: '',    
            searchVal: '',   
            isAdded : false,  
        })  
        if(e.target.value.split('|')[1] == 'MRP'){
            this.setState({
                divisionCheck: false,
                departmentCheck: false,
                sectionCheck: false,
                articleCheck: true,                
            })
        }
        if(e.target.value.split('|')[1] != 'MRP'){
            let data = {
                type: 1,
                description: "",
                itemUdfType: e.target.value.split('|')[0],
                ispo: "false",
                no: 1,
                search: "",
                hl3Code: this.state.hierarchy["Department"].code,
                hl4Code: '',
                hl4Name: '',
                searchBy: 'contains'
            }
            this.props.itemCatDescUdfRequest(data)
        }
    }

    confirmMapCheck = (e, value) =>{
        if(this.state.isMrpArticle != 'MRP'){
            let data = e.target.id;
        let displayData = data;
        if(data == 'departmentCheck'){
            displayData = 'Department'
        }
        if(data == 'divisionCheck'){
            displayData = 'Division'
        }
        if(data == 'sectionCheck'){
            displayData = 'Section'
        }
        if(data == 'articleCheck'){
            displayData = 'Article'
        }
        if(!value){
            this.setState({
                mapData: data,            
            }, () => {
                let dataVal = {
                    headerMsg: `All settings will be effective on the selected ${displayData}.`,
                    paraMsg: 'Please Confirm',
                    confirmMap: true,
                    data: data
                }
                this.props.confirmHandler(dataVal)
            })
        }
        else{            
            this.setState({
                divisionCheck: false,
                sectionCheck: false,
                departmentCheck: true,
                articleCheck: false,
            }, () =>{
                // let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck || this.state.articleCheck;
                // let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                // let data ={hierarchy: hierarchyCheck, table: tableCheck}
                // this.props.saveCheck(data)
            }
            )
        }
        }
    }

    childConfirmHandler = (data) =>{        
        this.setState({
            mapData: data,
        }, this.mappingConfirmHandler())
    }

    mappingConfirmHandler = () => {
        console.log(this.state.mapData)
        let t = this;
        if(this.state.mapData == 'divisionCheck'){
            this.setState({
                divisionCheck: !this.state.divisionCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}                
                if(this.state.divisionCheck){
                    this.setState({
                        departmentCheck: false,
                        sectionCheck: false,
                        articleCheck: false,
                    })
                }
            })
        }
        if(this.state.mapData == 'departmentCheck'){
            this.setState({
                departmentCheck: !this.state.departmentCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}                
                if(this.state.departmentCheck){
                    this.setState({
                        divisionCheck: false,
                        sectionCheck: false,
                        articleCheck: false,
                    })
                }
            })
        }
        if(this.state.mapData == 'sectionCheck'){
            this.setState({
                sectionCheck: !this.state.sectionCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}               
                if(this.state.sectionCheck){
                    this.setState({
                        divisionCheck: false,
                        departmentCheck: false,
                        articleCheck: false,
                    })
                }
            })
        }
        if(this.state.mapData == 'articleCheck'){
            this.setState({
                articleCheck: !this.state.articleCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck || this.state.articleCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}                
                if(this.state.articleCheck){
                    this.setState({
                        departmentCheck: false,
                        sectionCheck: false,
                        divisionCheck: false,
                    })
                }
            })
        }
    }



    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }


    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }

    exportSelectedExcel = () =>{
        let data = {
                catDescUdfType: this.state.catValue,
                hl3Code: this.state.hierarchy["Department"].code,
                hl1Code: "",
                hl2Code: "",
        }
        this.props.mappingExcelExportRequest(data)
    }

    handleSearch = (e) => {
        let dName = e.target.dataset.name

        this.setState({
            hierarchy: {
                ...this.state.hierarchy, [dName]: {
                    name: e.target.value,
                    code: ""
                }
            },
            search: e.target.value
        })
    }

    setDropData = (e, value) => {
        let { startDate, endDate } = this.state
        let dName = e.target.dataset.name
        if (e.keyCode == 13 || e.target.id == 'click') {
            this.setState({ currentActive: dName, hierarchyDropHandle: true })
            if (value == "division") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "division",
                    poArticleSearch: "",
                    mrpRangeSearch: false

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Division'].name == '' ? 1 : 3,
                    hl4Code: this.state.hierarchy['Article'].name,
                    hl4Name: this.state.hierarchy['Article'].name,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.hierarchy['Division'].name,
                    sectionSearch: "",
                    departmentSearch: "",
                    articleSearch: "",
                    basedOn: "division",
                    supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false

                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)


            } else if (value == "section") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "section",
                    poArticleSearch: "",
                    mrpRangeSearch: false
                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Section'].name == '' ? 1 : 3,
                    hl4Code: this.state.hierarchy['Article'].name,
                    hl4Name: this.state.hierarchy['Article'].name,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: this.state.hierarchy['Section'].name,
                    departmentSearch: "",
                    articleSearch: "",
                    basedOn: "section",
                    //supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false
                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)
            } else if (value == "department") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "department",
                    poArticleSearch: "",
                    mrpRangeSearch: false,
                    isSave: true

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Department'].name == '' ? 1 : 3,
                    hl4Code: this.state.hierarchy['Article'].name,
                    hl4Name: this.state.hierarchy['Article'].name,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: "",
                    departmentSearch: this.state.hierarchy['Department'].name,
                    articleSearch: "",
                    basedOn: "department",
                    // supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false
                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)
            }else if (value == "article") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "article",
                    poArticleSearch: "",
                    mrpRangeSearch: false

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Article'].name == '' ? 1 : 3,
                    hl4Code: this.state.hierarchy['Article'].name,
                    hl4Name: this.state.hierarchy['Article'].name,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: "",
                    departmentSearch: "",
                    articleSearch: this.state.hierarchy['Article'].name,
                    basedOn: "article",
                    supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false

                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)


            }
        }
    }

    page = (e) => {
        console.log(e.target.id)
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.prePage != 0) {
                let data = {
                    type: this.state.hierarchy['Division'].name != '' || this.state.hierarchy['Section'].name != '' || this.state.hierarchy['Department'].name != '' ? 3 : 1,
                    no: this.props.purchaseIndent.poArticle.data.prePage,

                    hl4Code: this.state.hierarchy['Article'].name,
                    hl4Name: this.state.hierarchy['Article'].name,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.basedOn == "division" ? this.state.hierarchy['Division'].name : "",
                    sectionSearch: this.state.basedOn == "section" ? this.state.hierarchy['Section'].name : "",
                    departmentSearch: this.state.basedOn == "department" ? this.state.hierarchy['Department'].name : "",
                    articleSearch: "",
                    basedOn: this.state.basedOn,
                    supplier: this.state.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: false,
                }
                this.props.poArticleRequest(data);
                setTimeout(() => this.showDropDown(), 2);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.currPage != this.props.purchaseIndent.poArticle.data.maxPage) {
                let data = {
                    type: this.state.hierarchy['Division'].name != '' || this.state.hierarchy['Section'].name != '' || this.state.hierarchy['Department'].name != '' ? 3 : 1,
                    no: this.props.purchaseIndent.poArticle.data.currPage + 1,
                    hl4Code: this.state.hierarchy['Article'].name,
                    hl4Name: this.state.hierarchy['Article'].name,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.basedOn == "division" ? this.state.hierarchy['Division'].name : "",
                    sectionSearch: this.state.basedOn == "section" ? this.state.hierarchy['Section'].name : "",
                    departmentSearch: this.state.basedOn == "department" ? this.state.hierarchy['Department'].name : "",
                    articleSearch: "",
                    basedOn: this.state.basedOn,
                    supplier: this.state.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: false
                }
                this.props.poArticleRequest(data)
                setTimeout(() => this.showDropDown(), 2);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }

        }        
    }

    openCNameModal = (e) =>{
        if(e.keyCode == '13' || e.target.id == 'cNameSearch'){            
            let data = {
                type: this.state.searchVal == '' ? 1 : 3,
                no: 1,
                udfType: this.state.catValue,
                search: this.state.searchVal,
                searchBy: "contains"
            }
            this.props.cnameRequest(data);
            this.openChooseModal();
        }
    }

    handleInput = (e) =>{
        this.setState({
            searchVal: e.target.value,
        })
    }

    openChooseModal = () =>{
        console.log('clicked')
        this.setState({
            chooseModalAnimation: true,
        }, () => {
            document.addEventListener('click', this.closeChooseModal)
        })
    }

    closeChooseModal = () =>{
        this.setState({
            chooseModalAnimation: false,
        }, () => {
            document.removeEventListener('click', this.closeChooseModal)
        })
    }

    updateCValue = (data) => {
        this.setState({
            searchVal: data.header,
            chooseModalAnimation: false,
            resultUDFType: [{ displayName:"", ext: data.ext, map:"Y", description: data.header, code: data.code}],
        })
    }

    tableSearchHandler = (e) => {
        this.setState({
            tableSearch: e.target.value,
        })
    }

    clearTableSearch = () =>{
        this.setState({
            tableSearch: '',
        })
    }


    render() {  
        console.log(this.state.updatedTableData)
        const {hierarchy, hierarchyDropHandle, tableData} = this.state;
        
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="menu_path n-menu-path">
                        <div className="nmp-right" style={{width:'100%'}}>
                            <span className="breadcum-text">Last Excel Upload Status : <span className="breadcum-text-bold">{this.state.status}</span></span>
                        </div>
                    </div>
                    <div className="pi-new-layout">                        
                        <div className="col-lg-2 check-input">
                            <label className="pnl-purchase-label">Division</label>
                            <div className="inputTextKeyFucMain">
                                <input type="text" className="onFocus pnl-purchase-input" data-name="Division" value={hierarchy['Division'].name} placeholder="Choose Division" onChange={(e) => this.handleSearch(e)} onKeyDown={(e) => this.setDropData(e, "division")} />
                                <span className="modal-search-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" id="click" data-name="Division" onClick={(e) => this.setDropData(e, "division")}>
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {hierarchyDropHandle && this.state.isMrpArticle != 'MRP' && <ConfigArticle {...this.state} page={this.page} setHierarchySelectedData={this.setHierarchySelectedData} handleHierarchyModal={this.handleHierarchyModal} currentActive={this.state.currentActive} hierarchyData={this.props.purchaseIndent.poArticle.data.resource || this.props.purchaseIndent.poArticle.data.resource || []} />}
                                {/* {this.state.selectDivision && <ConfigArticle handleDepartmentModal={this.handleDepartmentModal} />} */}
                            </div>
                            <div className="pnlci-check">
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" className="checkBoxTab" checked= {this.state.divisionCheck} id = 'divisionCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.divisionCheck)}/>
                                    <span className="checkmark1"></span>
                                </label>
                            </div>
                        </div>
                        <div className="col-lg-2 check-input">
                            <label className="pnl-purchase-label">Section</label>
                            <div className="inputTextKeyFucMain">
                                <input type="text" className="onFocus pnl-purchase-input" data-name="Section" value={this.state.hierarchy['Section'].name} placeholder="Choose Section" onChange={(e) => this.handleSearch(e)} onKeyDown={(e) => this.setDropData(e, "section")} />
                                <span className="modal-search-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" id="click" data-name="Section" onClick={(e) => this.setDropData(e, "section")}>
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {/* {this.state.selectSection && <ConfigArticle handleDepartmentModal={this.handleDepartmentModal} />} */}
                            </div>
                            <div className="pnlci-check">
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" className="checkBoxTab" checked= {this.state.sectionCheck} id = 'sectionCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.sectionCheck)}/>
                                    <span className="checkmark1"></span>
                                </label>
                            </div>
                        </div>     
                        <div className="col-lg-2 pad-lft-0 check-input">
                            <label className="pnl-purchase-label">Department</label>
                            <div className="inputTextKeyFucMain">
                                <input type="text" className="onFocus pnl-purchase-input" data-name="Department" placeholder="Choose Department" value={hierarchy["Department"].name} onChange={(e) => this.handleSearch(e)} onKeyDown={(e) => this.setDropData(e, "department")}/>
                                <span className="modal-search-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" id="click" data-name="Department" onClick={(e) => this.setDropData(e, "department")}>
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>                                
                                {/* {this.state.selectDepartment && <ConfigArticle handleDepartmentModal={this.handleDepartmentModal} />} */}
                            </div>
                            <div className="pnlci-check">
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" className="checkBoxTab" checked= {this.state.departmentCheck} id = 'departmentCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.departmentCheck)}/>
                                    <span className="checkmark1"></span>
                                </label>
                            </div>
                        </div>                   
                        {/* <div className="col-lg-2 udf-flex">
                            <button className="udf-search-btn">
                                <img src={require('../../assets/transparentSearch.svg')} />
                                Search</button>
                        </div> */}
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="udf-additional-section">
                        <div className="pi-new-layout">
                            <div className="col-lg-2 pad-lft-0">
                                <label className="pnl-purchase-label">Select Cat/Desc/UDF</label>
                                <select className="pnl-purchase-select onFocus" id="mySelectCat" onChange={this.tableItemHandler}>
                                    <option value="Please Select">Please Select</option>
                                    {
                                        this.state.selectCat.map(item=>{
                                            return <option value={item.name+"|"+item.displayName+"|"+item.catType} >{item.name}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="col-lg-2">
                                <label className="pnl-purchase-label">Name</label>
                                <input autocomplete="off" disabled="" readonly="" type="text" class="pnl-purchase-read" id="termData" placeholder="" value={this.state.isMrpArticle} />
                            </div>
                            {this.state.isMrpArticle == "MRP" && <div className="col-lg-2 check-input">
                                <label className="pnl-purchase-label">Article</label>
                                <div className="inputTextKeyFucMain">
                                    <input type="text" className="onFocus pnl-purchase-input" value={this.state.hierarchy['Article'].name} data-name="Article" placeholder="Choose Article" onChange={(e) => this.handleSearch(e)} onKeyDown={(e) => this.setDropData(e, "article")} />
                                    <span className="modal-search-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" id="click" data-name="Article" onClick={(e) => this.setDropData(e, "article")}>
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                        </svg>
                                    </span>
                                    {hierarchyDropHandle && this.state.isMrpArticle == 'MRP' && <ConfigArticle {...this.state} page={this.page} setHierarchySelectedData={this.setHierarchySelectedData} handleHierarchyModal={this.handleHierarchyModal} currentActive={this.state.currentActive} hierarchyData={this.props.purchaseIndent.poArticle.data.resource || this.props.purchaseIndent.poArticle.data.resource || []} />}
                                    {/* {this.state.selectArticle && <ConfigArticle handleDepartmentModal={this.handleDepartmentModal} />} */}
                                </div>
                                <div className="pnlci-check">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" className="checkBoxTab" checked= {this.state.articleCheck} id = 'articleCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.articleCheck)}/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>}
                            {!this.state.isAdded && <div className="col-lg-2 udf-flex" onClick={this.openAdded}>
                                <img className="udff-add-icon" src={require('../../assets/add-green.svg')} />
                            </div>}
                            {this.state.isAdded && this.state.catType == 'C' && <div className="col-lg-2">
                                <label className="pnl-purchase-label">Choose Value</label>
                                {/* <select className="pnl-purchase-select uas-select onFocus">
                                    <option></option>
                                </select> */}
                                <div className="inputTextKeyFucMain">
                                    <input autoComplete="off" type="text" className="inputTextKeyFuc onFocus pnl-purchase-input" id="cName" value={this.state.searchVal} placeholder="Choose Value" onKeyDown={(e) => this.openCNameModal(e)} onChange={(e) => this.handleInput(e)} />
                                    <span className="modal-search-btn" id='cNameIcon' onClick={(e) => this.openCNameModal(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                        </svg>
                                    </span>
                                {this.state.chooseModalAnimation && <ChooseValue {...this.props} {...this.state} chooseName={this.state.chooseName} chooseModalAnimation={this.state.chooseModalAnimation} closeChooseModal={(e) => this.closeChooseModal(e)} updateCValue={this.updateCValue} openChooseModal={this.openChooseModal} />}
                                </div>
                            </div>}
                            {this.state.isAdded && this.state.catType != 'C' && <div className="col-lg-2">
                                <label className="pnl-purchase-label">Enter Value</label>
                                <input type={this.state.isMrpArticle == 'MRP' ? 'number' : "text"} className="uas-input" onChange={this.enterMapValue} value={this.state.valueSearch}/>
                            </div>}  
                            {this.state.isAdded && <div className="col-lg-1 udf-flex" onClick={this.closeAdded}>
                                <img className="udff-close-icon" src={require('../../assets/closeRedNew.svg')} />
                            </div>}                          
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="col-lg-6 pad-0 gen-item-udf">
                        {(this.state.valueSearch != '' || this.state.searchVal != '') && <div className="gvpd-left">
                            <button type="button" className="giu-save" onClick={this.saveMapValue} >Save</button>
                            <button type="button" onClick={this.clearMapValue}>Clear</button>
                        </div>}
                    </div>
                    <div className="gen-item-udf">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                {/* <button type="button" className="remove-mapping-btn">Remove Mapping</button> */}
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type to Search..." value={this.state.tableSearch} onChange={this.tableSearchHandler} onKeyDown={this.getItemData}/>
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                    {this.state.tableSearch && <span className="closeSearch" id="clearSearchFilter" onClick={this.clearTableSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                                </div>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={() => this.exportSelectedExcel()}>
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                            <li>
                                                <button className="export-excel" type="button">
                                                    <span className="pi-export-svg">
                                                        <img src={require('../../assets/downloadAll.svg')} />
                                                    </span>
                                                Download All</button>
                                            </li>
                                        </ul>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="gen-item-udf-table">
                        <div className="giut-headfix" id="table-scroll">
                            {/* <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div> */}
                            <table className="table gen-main-table">
                                <thead >
                                    <tr>
                                        <th className="fix-action-btn"></th>
                                        <th className="width145">
                                            <label>Name</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        {/* <th>
                                            <label>Display Name</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th> */}
                                        <th>
                                            <label>Map</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Ext</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {tableData.length == 0 ?
                                    <tr>
                                        <td align="center" colSpan="12">
                                            <label>No Data Available</label>
                                        </td>
                                    </tr>:
                                    tableData.map((item, indx)=>{
                                        return <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td className="width145 border-lft"><label className="bold">{item.description}</label></td>
                                        {/* <td>
                                            <input type="text" className="modal_tableBox " value={item.displayName}/>
                                        </td> */}
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" checked={item.map == "N" ? false : true} id='Map' onChange={(e) =>this.updateMapCheck(e, item, indx)} />
                                                <span className="checkmark1"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" checked={item.ext == "N" ? false : true} id='Ext' onChange={(e) =>this.updateMapCheck(e, item, indx)}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </td>
                                    </tr>
                                    })    
                                }                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-12 pad-0" >
                        <div className="new-gen-pagination">
                            <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder" min="1" id='page' onChange = {this.pageNoHandler} onKeyDown={this.getItemData} value={this.state.pageNo} />
                                    {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination {...this.state} {...this.props} page={this.pageHandler}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CatDescItemUdfMapping;