import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class SiteModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            searchMrp: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
            department: "",
            siteCode: this.props.siteCode,
            siteData: [],
            siteName: this.props.siteName,
            searchBy: "startWith",
            focusedLi: "",

        }

    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    componentWillMount() {

        this.setState({
            siteCode: this.props.siteCode,
            searchMrp: this.props.isModalShow ? "" : this.props.siteSearch,
            type: this.props.isModalShow || this.props.siteSearch == "" ? 1 : 3


        })
    }
    componentWillReceiveProps(nextProps) {


        if (nextProps.purchaseIndent.procurementSite.isSuccess) {
            if (nextProps.purchaseIndent.procurementSite.data.resource != null) {
                this.setState({
                    siteData: nextProps.purchaseIndent.procurementSite.data.resource,
                    prev: nextProps.purchaseIndent.procurementSite.data.prePage,
                    current: nextProps.purchaseIndent.procurementSite.data.currPage,
                    next: nextProps.purchaseIndent.procurementSite.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.procurementSite.data.maxPage,
                })
            } else {
                this.setState({
                    siteData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("searchMrp").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.procurementSite.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.procurementSite.data.resource[0].siteCode,
                            searchMrp: this.props.isModalShow ? "" : this.props.siteSearch,
                            type: this.props.isModalShow || this.props.siteSearch == "" ? 1 : 3


                        })
                        document.getElementById(nextProps.purchaseIndent.procurementSite.data.resource[0].siteCode) != null ? document.getElementById(nextProps.purchaseIndent.procurementSite.data.resource[0].siteCode).focus() : null
                    }



                }
            }

        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.procurementSite.data.prePage,
                current: this.props.purchaseIndent.procurementSite.data.currPage,
                next: this.props.purchaseIndent.procurementSite.data.currPage + 1,
                maxPage: this.props.purchaseIndent.procurementSite.data.maxPage,
            })
            if (this.props.purchaseIndent.procurementSite.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.procurementSite.data.currPage - 1,

                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.procurementSiteRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.procurementSite.data.prePage,
                current: this.props.purchaseIndent.procurementSite.data.currPage,
                next: this.props.purchaseIndent.procurementSite.data.currPage + 1,
                maxPage: this.props.purchaseIndent.procurementSite.data.maxPage,
            })
            if (this.props.purchaseIndent.procurementSite.data.currPage != this.props.purchaseIndent.procurementSite.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.procurementSite.data.currPage + 1,

                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.procurementSiteRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.procurementSite.data.prePage,
                current: this.props.purchaseIndent.procurementSite.data.currPage,
                next: this.props.purchaseIndent.procurementSite.data.currPage + 1,
                maxPage: this.props.purchaseIndent.procurementSite.data.maxPage,
            })
            if (this.props.purchaseIndent.procurementSite.data.currPage <= this.props.purchaseIndent.procurementSite.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,

                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.procurementSiteRequest(data)
            }

        }
    }

    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                type: "",
                no: 1
            })
        if (this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                search: "",
                searchBy: this.state.searchBy

            }
            this.props.procurementSiteRequest(data)
        }
        document.getElementById("searchMrp").focus()

    }

    handleChange(e) {
        if (e.target.id == "searchMrp") {
            this.setState({
                searchMrp: e.target.value
            })
        } else if (e.target.id == "searchBySite") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.searchMrp != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,

                        search: this.state.searchMrp,
                        searchBy: this.state.searchBy
                    }
                    this.props.procurementSiteRequest(data)
                }

            })
        }
    }

    selectedData(e) {
        let c = this.state.siteData;
        for (let i = 0; i < c.length; i++) {
            if (c[i].siteCode == e) {
                this.setState({
                    siteCode: c[i].siteCode,
                    siteName: c[i].siteName
                }, () => {
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
                })
            }
        }

    }

    onDone() {

        if (this.props.isModalShow ? this.state.siteCode != this.props.siteCode : this.state.siteCode != this.props.siteCode || this.props.siteSearch == this.state.searchMrp || this.props.siteSearch != this.state.searchMrp) {

            let data = {
                siteCode: this.state.siteCode,
                siteName: this.state.siteName
            }

            setTimeout(() => {
                this.props.updateSite(data)
            }, 10);
        }
        if (this.state.siteCode != "") {

            this.setState(
                {
                    search: "",
                })

            this.props.closeSiteModal()
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1500)
        }


    }

    onSearch(e) {

        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.searchMrp,
                searchBy: this.state.searchBy

            }
            this.props.procurementSiteRequest(data)
        }
    }


    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == 1 || this.state.type == "")) {
                if (this.state.siteCode.length != 0) {
                    this.setState({
                        siteCode: this.state.siteData[0].siteCode
                    })
                    let siteCode = this.state.siteData[0].siteCode
                    this.selectedData(siteCode)
                    window.setTimeout(function () {
                        document.getElementById("searchMrp").blur()
                        document.getElementById(siteCode).focus()
                    }, 0);
                } else {
                    window.setTimeout(function () {
                        document.getElementById("searchMrp").blur()
                        document.getElementById("closeButton").focus()
                    }, 0);
                }
            }

            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("findButton").focus();
                }, 0);
            }
        }
        if (e.key === "ArrowDown") {
            if (this.state.siteCode.length != 0) {
                this.setState({
                    siteCode: this.state.siteData[0].siteCode
                })
                let siteCode = this.state.siteData[0].siteCode
                this.selectedData(siteCode)
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById(siteCode).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("closeButton").focus()
                }, 0);
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0);
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let siteCode = this.state.siteData[0].siteCode
            window.setTimeout(function () {
                document.getElementById(siteCode).blur()
                document.getElementById("doneButton").focus()
            }, 0);
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0);
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.props.closeSiteModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("searchMrp").focus()
            }, 0);
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.siteData.length != 0) {
                let siteCode = this.state.siteData[0].siteCode
                this.selectedData(siteCode)

                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(siteCode).focus()
                }, 0);
            }
            else {

                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("searchMrp").focus()

                }, 0);

            }
        }
    }


    selectLi(e, code) {
        let siteData = this.state.siteData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < siteData.length; i++) {
                if (siteData[i].siteCode == code) {
                    index = i
                }
            }
            if (index < siteData.length - 1 || index == 0) {
                document.getElementById(siteData[index + 1].siteCode) != null ? document.getElementById(siteData[index + 1].siteCode).focus() : null

                this.setState({
                    focusedLi: siteData[index + 1].siteCode
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < siteData.length; i++) {
                if (siteData[i].siteCode == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(siteData[index - 1].siteCode) != null ? document.getElementById(siteData[index - 1].siteCode).focus() : null

                this.setState({
                    focusedLi: siteData[index - 1].siteCode
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.props.closeSiteModal(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.siteData.length != 0 ?
                            document.getElementById(this.state.siteData[0].siteCode).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.siteData.length != 0) {
                    document.getElementById(this.state.siteData[0].siteCode).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.props.closeSiteModal(e)
        }


    }


    render() {
        console.log(this.props.siteCode, this.state.siteCode)
        if (this.state.focusedLi != "" && this.props.siteSearch == this.state.searchMrp) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }


        const { searchMrp } = this.state;
        return (

            this.props.isModalShow ? <div className={this.props.siteModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.siteModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.siteModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.siteModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT SITE</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only one Site from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBySite" name="searchBySite" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                    <label>
                                                        {this.state.searchMrp == "" && (this.state.type == 1 || this.state.type == "") ? <button type="button" className="clearbutton btnDisabled m-lft-15">CLEAR</button>
                                                            : <button type="button" className="clearbutton m-lft-15" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>

                                                        }
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover procurementSiteMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Site Code</th>
                                                    <th>Site Name</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.siteData == undefined || this.state.siteData == null || this.state.siteData.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.siteData.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.siteCode}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.siteCode} checked={this.state.siteCode == `${data.siteCode}`} />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>
                                                            {data.siteCode}
                                                        </td>
                                                        <td className="pad-lft-10">
                                                            {data.siteName}
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" data-dismiss="modal" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.props.closeSiteModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
                :

                  
                <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                        <div className="dropdown-modal-header">
                            <span className="div-col-2">Site Code</span>
                            <span className="div-col-2">Site Name</span>
                        </div>

                    <ul className="dropdown-menu-city-item">
                        {this.state.siteData == undefined || this.state.siteData.length == 0 ?
                            <li><span>No Data Found</span></li> :
                            this.state.siteData.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.siteCode}`)} id={data.siteCode} className={this.state.siteCode == `${data.siteCode}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.siteCode)}>
                                    <span className="vendor-details">
                                        <span className="vd-name div-col-2">{data.siteCode}</span>
                                        <span className="vd-loc div-col-2">{data.siteName}</span>


                                    </span>
                                </li>))}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                            <button className="btn-close" type="button" onClick={(e) => this.props.closeSiteModal(e)} id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button> : 
                            <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                     <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button>}
                        </div>
                    </div>
                </div>


        );
    }
}

export default SiteModal;
