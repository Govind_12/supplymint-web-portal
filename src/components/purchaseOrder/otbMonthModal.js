import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";
import { months } from "moment";

class OtbArticleModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef()
        this.state = {
            monthYrString: [],
            selectedMonths: this.props.monthList,
            search: '',              
            isCross: false,
            selectedData: false,
        }

    }

    componentDidMount() {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
        this.setState({
            value: this.props.articleId,  
            articleDataList: this.props.aritcleValue,
            articleDisplayList: this.props.articleInput,
        })

        let currMonth = new Date().getMonth() + 1;
        let currYear = new Date().getFullYear();
        let nextYearIndex = 12 - currMonth;
        let monthYrString = [];
        for(let i = 0; i < 12; i++){
            let month = currMonth + i;
            let year = currYear;
            let yearString = '';
            let monthString = '';
            let monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            if(i > nextYearIndex){
                month = i - nextYearIndex;
                year = currYear +1;
            }
            monthString = monthList[month - 1];
            yearString = year.toString();         
            monthYrString.push(monthString + "-" + yearString)
        }
        this.setState({
            monthYrString: monthYrString,
        })
    }

    componentWillReceiveProps(nextProps, prevState) {       
    }

    onCheckbox(name) {
        let selectedMonths = [...this.state.selectedMonths];
        if (selectedMonths.includes(name)) {
            var index = selectedMonths.indexOf(name);

            if (index > -1) {
                selectedMonths.splice(index, 1);
                this.setState({
                    selectedMonths: selectedMonths,
                })

            }

        } else {
            selectedMonths.push(name)
            this.setState({
                selectedMonths: selectedMonths,
            })

        }

    }


    
            
    handleChange(e) {
        this.setState({
            search: e.target.value,
            isCross: true,
        })
        if(e.target.value == ''){
            this.setState({
                isCross: false
            })
        }
    }
    
    onsearchClear = () =>{
        this.setState({
            isCross: false,
            search: ''
        })        
    }


    changeSelectedData = (event) => {
        this.setState({
            selectedData: !this.state.selectedData
        })
    }

    removeSelected = (name) => {
        let selectedMonths = [...this.state.selectedMonths];
        let index = selectedMonths.findIndex((_) => _ == name)
        selectedMonths.splice(index, 1)
        this.setState({ selectedMonths})
    }

    render() {   
        const { selectedData } = this.state;
        let monthYrString = [...this.state.monthYrString];
        let filteredMonthYr = monthYrString.filter(month => {
            if(this.state.search == '' ){
                return month
            }
            else{
                if(month.toLowerCase().includes(this.state.search.toLowerCase())){
                    return month
                }
            }
        })
        return (
            <div className={this.props.isMonthOpen ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.isMonthOpen ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.isMonthOpen ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.isMonthOpen ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12 piClorModal">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list-inline width_100 m-top-15 modelHeader">
                                        <li>
                                            <label className="select_name-content">SELECT Month</label>
                                            <p className="para-content">You can select multiple month from below records</p>
                                        </li>


                                    </ul>

                                    <ul className="list-inline width_100 chooseDataModal">
                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn addNewWidth m-top-5">
                                            
                                            <li className="width100">
                                                
                                                <div className="gen-new-pi-history">
                                                    <form>
                                                        <input type="search" autoComplete="off" autoCorrect="off" className="search_bar" ref={this.textInput} value={this.state.search} id="articleSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                        <button className="searchWithBar" id = 'search'>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                                                <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                                            </svg>
                                                        </button>
                                                        {this.state.isCross ? <span className="closeSearch" id='search' onClick={(e) => this.onsearchClear(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                                                    </form>
                                                </div>
                                                
                                            </li>
                                                </div>
                                        
                                        <div className="nph-switch-btn">
                                            <label className="tg-switch" >
                                                <input type="checkbox" checked={selectedData} onChange={this.changeSelectedData} />
                                                <span className="tg-slider tg-round"></span>
                                            </label>
                                            {selectedData && <label className="nph-wbtext">Selected data</label>}
                                            {!selectedData && <label className="nph-wbtext"> All data</label>}
                                        </div>                                        
                                    </ul>
                                </div>
                                {!selectedData && <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                <th>Select</th>
                                                    <th>Months</th>
                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {
                                                     filteredMonthYr.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : 
                                                     filteredMonthYr.map((data, key) => (
                                                        // <React.Fragment>
                                                        <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data}`)}>

                                                            <td>
                                                                <div className="checkBoxRowClick pi-c2">
                                                                    <label className="checkBoxLabel0">
                                                                        <input type="checkBox" checked={this.state.selectedMonths.includes(`${data}`)} id={data} onKeyDown={(e) => this.focusDone(e, `${data}`)} readOnly />
                                                                        <label className="checkmark1" htmlFor={data} ></label>
                                                                    </label>

                                                                </div>
                                                            </td>
                                                            <td>{data}</td>
                                                        </tr>
                                                    )) 
                                                    //  {/* </React.Fragment> */}
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>}
                                {selectedData && <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Months</th>
                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {filteredMonthYr.map((data, i) => (
                                                    <React.Fragment key={i}>
                                                        {this.state.selectedMonths.includes(`${data}`) &&
                                                            <tr onClick={() => this.removeSelected(`${data}`)}>
                                                                <td> <div className="checkBoxRowClick pi-c4">
                                                                    <label className="checkBoxLabel0"  >
                                                                        <input type="checkBox" checked={true} id={data} onKeyDown={(e) => this.removeSelected(e, `${data}`)} readOnly />
                                                                        <label className="checkmark1" htmlFor={data} ></label>
                                                                    </label>
                                                                </div> </td>
                                                                <td>{data}</td>
                                                            </tr>
                                                        }

                                                    </React.Fragment>
                                                ))}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>}
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={() => this.props.monthSubmit(this.state.selectedMonths)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={this.props.closeMonthDropDown}>Close</button>
                                            </label>
                                        </li>                                        
                                    </ul>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        );
    }
}

export default OtbArticleModal;