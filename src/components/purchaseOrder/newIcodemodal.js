import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";
class NewIcodeModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef()
        this.state = {
            checked: false,

            loader: false,
            success: false,
            alert: false,
            ItemCodeData: [],

            itemcode: this.props.itemCodeList,

            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1,
            no: 1,
            search: "",
            toastMsg: "",
            toastLoader: false,



            errorMassage: "",
            poErrorMsg: false,
            checkBoxEnter: "",
            searchBy:"startWith",
              focusedLi: "",

        }

    }

    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }


    onsearchClear() {


        if (this.state.type == 3) {

            let data = {
                type: 1,
                no: 1,
                // code: this.props.colorCode,

                search: "",
                articleCode: this.props.articleCode,
                supplierCode: this.props.supplierCode,

                siteCode: this.props.siteCode,
                 searchBy:this.state.searchBy
            }
            this.props.getPoItemCodeRequest(data)
        }
        this.setState(
            {
                search: "",
                type: 1,
                no: 1
            })
        document.getElementById('deselectAll').checked = false
        document.getElementById('selectAll').checked = false
        this.textInput.current.focus();

    }
    onSearch(e) {


        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,
                // type=""
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
            document.getElementById('deselectAll').checked = false
            document.getElementById('selectAll').checked = false
        }
        else {
            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,

                search: this.state.search,
                articleCode: this.props.articleCode,
                supplierCode: this.props.supplierCode,

                siteCode: this.props.siteCode,
                 searchBy:this.state.searchBy
            }
            this.props.getPoItemCodeRequest(data)
        }
        document.getElementById('deselectAll').checked = false
        document.getElementById('selectAll').checked = false
    }


    onCheckbox(id) {
        var le = this.state.ItemCodeData

        let values = this.state.itemcode



        for (let i = 0; i < le.length; i++) {
            if (le[i].itemId == id) {

                if (values.includes(le[i].itemId)) {

                    var index = values.indexOf(le[i].itemId);



                    if (index > -1) {
                        values.splice(index, 1);
                        this.setState({
                            itemcode: values
                        })

                    }

                } else {
                    values.push(le[i].itemId)


                    this.setState({
                        itemcode: values,

                    })

                }
            }
        }

        let array = this.state.ItemCodeData;
        for (let i = 0; i < array.length; i++) {
            let cArray = []
            array.forEach(a => {
                cArray.push(a.itemId)
            });
            if (cArray.length == this.state.itemcode.length) {
                document.getElementById('selectAll').checked = true
                document.getElementById('deselectAll').checked = false
            } else if (cArray.length != this.state.itemcode.length) {
                document.getElementById('selectAll').checked = false
                document.getElementById('deselectAll').checked = false
            }

        }


    }


    componentWillMount() {

        this.setState({
            itemcode: this.props.itemCodeList,
            search: this.props.isModalShow ? "" : this.props.barcodeSearch

        })

        // if (this.props.purchaseIndent.getPoItemCode.isSuccess) {
        //     if (this.props.purchaseIndent.getPoItemCode.data.resource != null) {
        //         this.setState({
        //             ItemCodeData: this.props.purchaseIndent.getPoItemCode.data.resource,

        //             prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
        //             current: this.props.purchaseIndent.getPoItemCode.data.currPage,
        //             next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
        //             maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage
        //         })
        //     }
        //     else {
        //         this.setState({
        //             ItemCodeData: [],
        //             prev: 0,
        //             current: 0,
        //             next: 0,
        //             maxPage: 0
        //         })
        //     }
        // }

    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.getPoItemCode.isSuccess) {
            if (nextProps.purchaseIndent.getPoItemCode.data.resource != null) {
                this.setState({
                    ItemCodeData: nextProps.purchaseIndent.getPoItemCode.data.resource,
                    prev: nextProps.purchaseIndent.getPoItemCode.data.prePage,
                    current: nextProps.purchaseIndent.getPoItemCode.data.currPage,
                    next: nextProps.purchaseIndent.getPoItemCode.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getPoItemCode.data.maxPage,

                })
            }
            else {
                this.setState({
                    ItemCodeData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,

                })
            }
        
           if (window.screen.width > 1200) {
                    if (this.props.isModalShow) {
                        this.textInput.current.focus();

                    }
                    else if (!this.props.isModalShow) {
                        if (nextProps.purchaseIndent.loadIndent.data.resource != null) {
                            this.setState({
                                focusedLi: nextProps.purchaseIndent.loadIndent.data.resource[0].id
                            })
                            document.getElementById(nextProps.purchaseIndent.loadIndent.data.resource[0].id) != null ? document.getElementById(nextProps.purchaseIndent.loadIndent.data.resource[0].id).focus() : null
                        }



                    }
                }
        }

    }

    selectall(e) {

        // if(!e.key=="ArrowUp" || !e.key=="ArrowDown" || !e.key == "ArrowRight" || !e.key =="ArrowLeft"){
        var c = this.state.ItemCodeData
        let values = this.state.itemcode


        if (c == null) {
            document.getElementById("selectAll").checked = false
        } else {
            for (var i = 0; i < c.length; i++) {
                if (!values.includes(c[i].itemId)) {
                    values.push(c[i].itemId)

                }
            }
            this.setState({
                itemcode: values,


            })


            document.getElementById("selectAll").focus()
            document.getElementById("selectAll").checked = true
        }
    }
    deselectall(e) {
        // if(!e.key=="ArrowUp" || !e.key=="ArrowDown" || !e.key == "ArrowRight" || !e.key =="ArrowLeft"){
        var c = this.state.ItemCodeData
        var value = this.state.itemcode

        if (c == null) {
            document.getElementById("deselectAll").checked = false
        } else {
            for (let j = 0; j < c.length; j++) {
                for (var i = 0; i < value.length; i++) {
                    if (c[j].itemId == value[i] || c[j].itemId != value[i])
                        value.splice(i)
                }
            }


            this.setState({

                itemcode: value
            })





            document.getElementById("deselectAll").focus()
            document.getElementById("deselectAll").checked = true
            // }
        }

    }
    handleChange(e) {
        if (e.target.id == "colorSearch") {

            this.setState(
                {
                    search: e.target.value
                }

            );

        }else if(e.target.id=="searchByiCode"){
            this.setState({
                searchBy:e.target.value
            },()=>{
                if(this.state.search!="")
                {
                    let data = {
                    type: this.state.type,
                    no: 1,

                    search: this.state.search,
                    articleCode: this.props.articleCode,
                    supplierCode: this.props.supplierCode,

                    siteCode: this.props.siteCode,
                     searchBy:this.state.searchBy
                }
                this.props.getPoItemCodeRequest(data)
                }


            })
        }


    }





    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,

            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getPoItemCode.data.currPage - 1,

                    search: this.state.search,
                    articleCode: this.props.articleCode,
                    supplierCode: this.props.supplierCode,

                    siteCode: this.props.siteCode,
                     searchBy:this.state.searchBy
                }
                this.props.getPoItemCodeRequest(data);

                document.getElementById('deselectAll').checked = false
                document.getElementById('selectAll').checked = false
            }
        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,

            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage != this.props.purchaseIndent.getPoItemCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,

                    search: this.state.search,
                    articleCode: this.props.articleCode,
                    supplierCode: this.props.supplierCode,

                    siteCode: this.props.siteCode,
                    searchBy:this.state.searchBy
                }

                this.props.getPoItemCodeRequest(data)

            }

            document.getElementById('deselectAll').checked = false
            document.getElementById('selectAll').checked = false
        } else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,

            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage <= this.props.purchaseIndent.getPoItemCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,

                    search: this.state.search,
                    articleCode: this.props.articleCode,
                    supplierCode: this.props.supplierCode,

                    siteCode: this.props.siteCode,
                     searchBy:this.state.searchBy
                }
                this.props.getPoItemCodeRequest(data)
            }

        }

        document.getElementById('deselectAll').checked = false
        document.getElementById('selectAll').checked = false
    }


    ondone() {


        let value = this.state.itemcode
        if (value.length != 0) {



            this.props.updateNewIcode(value);


            this.closeNewIcode();
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }

        document.getElementById('deselectAll').checked = false
        document.getElementById('selectAll').checked = false
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    closeNewIcode(e) {


        this.props.closeNewIcode()

        // this.props.colorClear() 
        this.setState({

            search: "",

            type: 1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

        document.getElementById('deselectAll').checked = false
        document.getElementById('selectAll').checked = false
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }



    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "") {

                var le = this.state.ItemCodeData
                if (le != null) {
                    let itemId = le[0].itemId
                    this.onCheckbox(itemId)
                    window.setTimeout(function () {
                        document.getElementById("colorSearch").blur()
                        document.getElementById(itemId).focus()
                    }, 0)
                }
                else {
                    window.setTimeout(function () {
                        document.getElementById("colorSearch").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }
            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("colorSearch").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if(e.key ==="ArrowDown"){
             var le = this.state.ItemCodeData
                if (le != null) {
                    let itemId = le[0].itemId
                    this.onCheckbox(itemId)
                    window.setTimeout(function () {
                        document.getElementById("colorSearch").blur()
                        document.getElementById(itemId).focus()
                    }, 0)
                }
                else {
                    window.setTimeout(function () {
                        document.getElementById("colorSearch").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
        }




        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {

            window.setTimeout(function () {

                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e, cname) {

        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("selectAll").focus()
            }, 0)
        }
        if (e.key === "ArrowDown") {
            let index = 0
            var le = this.state.ItemCodeData
            for (let i = 0; i < le.length; i++) {
                if (le[i].itemId == cname) {
                    index = i
                }
            }

            if (index < le.length - 1) {

                this.setState({
                    checkBoxEnter: le[index + 1].itemId
                })

                document.getElementById(le[index + 1].itemId).focus()
            }
        }
        if (e.key === "ArrowUp") {
            let index = 0
            var le = this.state.ItemCodeData

            for (let i = 0; i < le.length; i++) {
                if (le[i].itemId == cname) {
                    index = i
                }
            }
            if (index > 0) {

                this.setState({
                    checkBoxEnter: le[index - 1].itemId
                })
                document.getElementById(le[index - 1].itemId).focus()
            }
        }
        if (e.key === "Enter") {
            if (this.state.checkBoxEnter != "") {
                this.onCheckbox(this.state.checkBoxEnter)
            }

        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.ondone();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {
            this.closeNewIcode();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("colorSearch").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            var le = this.state.ItemCodeData

            if (le != null) {
                let itemId = le[0].itemId
                this.onCheckbox(itemId)
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(itemId).focus()
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("colorSearch").focus()
                }, 0)
            }
        }
    }

    selectallKeyDown(e) {
        if (e.key == "Tab") {
            window.setTimeout(function () {

                document.getElementById("selectAll").blur()
                document.getElementById("deselectAll").focus()

            }, 0)
        }
        else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
            e.preventDefault()
        }
        else if (e.key == "Enter") {
            this.selectall()
        }
    }
    deselectallKeyDown(e) {
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("deselectAll").blur()
                document.getElementById("doneButton").focus()

            }, 0)
        }
        else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
            e.preventDefault()
        }
        else if (e.key == "Enter") {
            this.deselectall()
        }
    }

    render() {
        
        const { search } = this.state;

        return (

            <div className={this.props.icodeModalAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.icodeModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.icodeModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.icodeModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12 piClorModal">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list-inline width_100 m-top-15 modelHeader">
                                        <li>
                                            <label className="select_name-content">SELECT ICODE</label>
                                            <p className="para-content">You can select multiple icode from below records</p>
                                        </li>

                                        <li className="float_right">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeNewIcode(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul className="list-inline width_100 chooseDataModal">
                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn addNewWidth m-top-5">
                                            <div className="col-md-12 col-sm-8 pad-0 mrpLi">
                                                <li> 
                                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ?                       <select id="searchByiCode" name="searchByiCode" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                       
                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select>:null}

                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} value={search} id="colorSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />

                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                                
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <li className="float_right">
                                                    <label>
                                                        <button type="button" className={this.state.search == "" ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                    </label>
                                                </li>
                                        </div>
                                    </ul>
                                </div>
                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>ICODE</th>

                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {this.state.ItemCodeData.length != 0 ? this.state.ItemCodeData.map((data, key) => (
                                                    <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data.itemId}`)} >
                                                        <td>
                                                            <div className="checkBoxRowClick">
                                                                <label className="checkBoxLabel0"  >
                                                                    <input type="checkBox" checked={this.state.itemcode.includes(`${data.itemId}`)} id={data.itemId} onKeyDown={(e) => this.focusDone(e, `${data.itemId}`)} readOnly/>
                                                                    <label className="checkmark1" htmlFor={data.itemId} ></label>
                                                                </label>
                                                            </div>

                                                        </td>
                                                        <td>{data.itemId}</td>
                                                    </tr>
                                                )) : <tr><td colSpan="2"> No Data Found</td> </tr>
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">SELECT ALL
                                            <input type="radio" name="colorRadio" id="selectAll" onKeyDown={(e) => this.selectallKeyDown(e)} onClick={(e) => this.selectall(e)} />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                        <li className="selectAllFocus">
                                            <label className="select_modalRadio">DESELECT ALL
                                            <input type="radio" name="colorRadio" id="deselectAll" onKeyDown={(e) => this.deselectallKeyDown(e)} onClick={(e) => this.deselectall(e)} />
                                                <span className="checkradio-select deselect_all"></span>
                                            </label>
                                        </li>
                                        <li className="float_right">

                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth75 m0 modalPagination sizeModalPagination">
                                        <ul className="list-inline pagination paginationWidth40">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

            </div>


        );
    }
}

export default NewIcodeModal;