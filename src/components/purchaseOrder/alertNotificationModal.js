import React, { Component } from 'react'
import warningIcon from '../../assets/warning-dark.svg';

export default class AlertNotificationModal extends Component {
    render() {
        return (
            <div>
                <div className="save-current-data">
                    <div className="col-md-12 modal-dialog">
                        <div className="background-blur"></div>
                        <div className="col-md-12 col-sm-12 alertNotificationModal modalpoColor modal-content modalShow">
                            <h3 className="alignMiddle">Notification <img src={warningIcon} /></h3>
                            <h4>Error Found while submitting ! please correct these first and then proceed</h4>
                            <div className="modal-middle">
                            <ul>
                                {this.props.lineError.map((item, idx) => (
                                <li><span>Row {item.sNo}</span> ,{item.message}</li>
                                ))}
                               
                            </ul>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="close-btn" onClick={(e)=>this.props.closeMultipleError(e)}>Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
