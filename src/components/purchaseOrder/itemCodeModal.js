import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class ItemCodeModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            getMrpData: [],
            search: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: 1,
            no: 1,
            toastLoader: false,
            toastMsg: "",
            item: this.props.desc6,
            searchBy: "startWith",
            focusedLi: "",
            modalDisplay: false,

        }
    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }

    handleChange(e) {

        if (e.target.id == "adhocSearch") {
            this.setState({
                search: e.target.value
            })

        } else if (e.target.id == "searchByItem") {
            this.setState(
                {
                    searchBy: e.target.value
                }, () => {
                    if (this.state.search != "") {
                        let data = {
                            type: this.state.type,
                            no: 1,

                            code: this.props.code,
                            search: this.state.search,

                            mrpRangeFrom: this.props.startRange,
                            mrpRangeTo: this.props.endRange,
                            searchBy: this.state.searchBy
                        }
                        this.props.poItemcodeRequest(data);
                    }

                })
        }


    }

    onsearchClear() {

        this.setState(
            {
                search: "",
                type: 1,
                no: 1,
            })
        if (this.state.type == 3 || this.state.type == 2) {
            let data = {
                code: this.props.code,
                no: 1,
                type: 1,
                search: this.state.search,
                mrpRangeFrom: this.props.startRange,
                mrpRangeTo: this.props.endRange,
                searchBy: this.state.searchBy

            }
            this.props.poItemcodeRequest(data);
        }
        document.getElementById("adhocSearch").focus()
    }
    onSelectedData(id) {


        let sdata = this.state.getMrpData

        for (var i = 0; i < sdata.length; i++) {
            if (sdata[i].mrp == id) {
                this.setState({
                    item: sdata[i].mrp
                })
            }
        }
        this.setState({
            getMrpData: sdata
        }, () => {
            if (!this.props.isModalShow) {
                this.onDone()
            }
        })

    }
    onDone() {

        let sdata = this.state.getMrpData

        var mrp = "";
        var rsp = ""


        if (sdata != null) {
            for (var i = 0; i < sdata.length; i++) {
                if (sdata[i].mrp == this.state.item) {
                    mrp = sdata[i].mrp
                    rsp = sdata[i].rsp
                }

            }
        }
        let finalData = {
            id: this.props.descId,
            mrp: mrp,
            rId: this.props.rowIdentity,
            rsp: rsp,
            // itemCodeId: this.props.itemCodeId,
            articleCode: this.props.deleteArticleCode,
            articleName: this.props.newArticleName,
            iCode: this.props.iCode,
            startRange: this.props.startRange,
            endRange: this.props.endRange
        }
        if (this.props.isModalShow ? this.state.item.toString() != "" : this.state.item.toString() != "" || this.props.mrpSearch == this.state.search || this.props.mrpSearch != this.state.search) {
            this.props.updateItem(finalData);

            this.closeItemmModal();
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }

        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }

    closeItemmModal(e) {
        this.setState({
            Search: "",
            getMrpData: [],
            search: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })
        this.props.closeItemmModal();
    }



    onSearch(e) {
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {

            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,

                code: this.props.code,

                mrpRangeFrom: this.props.startRange,
                mrpRangeTo: this.props.endRange,
                search: this.state.search,
                searchBy: this.state.searchBy
            }

            this.props.poItemcodeRequest(data)
        }
    }

    page(e) {

        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.poItemcode.data.prePage,
                current: this.props.purchaseIndent.poItemcode.data.currPage,
                next: this.props.purchaseIndent.poItemcode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poItemcode.data.maxPage,
            })
            if (this.props.purchaseIndent.poItemcode.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.poItemcode.data.currPage - 1,

                    code: this.props.code,
                    search: this.state.search,

                    mrpRangeFrom: this.props.startRange,
                    mrpRangeTo: this.props.endRange,
                    searchBy: this.state.searchBy
                }
                this.props.poItemcodeRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.poItemcode.data.prePage,
                current: this.props.purchaseIndent.poItemcode.data.currPage,
                next: this.props.purchaseIndent.poItemcode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poItemcode.data.maxPage,
            })
            if (this.props.purchaseIndent.poItemcode.data.currPage != this.props.purchaseIndent.poItemcode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.poItemcode.data.currPage + 1,

                    code: this.props.code,
                    search: this.state.search,

                    mrpRangeFrom: this.props.startRange,
                    mrpRangeTo: this.props.endRange,
                    searchBy: this.state.searchBy
                }
                this.props.poItemcodeRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.poItemcode.data.prePage,
                current: this.props.purchaseIndent.poItemcode.data.currPage,
                next: this.props.purchaseIndent.poItemcode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poItemcode.data.maxPage,
            })
            if (this.props.purchaseIndent.poItemcode.data.currPage <= this.props.purchaseIndent.poItemcode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    code: this.props.code,

                    search: this.state.search,

                    mrpRangeFrom: this.props.startRange,
                    mrpRangeTo: this.props.endRange,
                    searchBy: this.state.searchBy
                }
                this.props.poItemcodeRequest(data)
            }
            this.textInput.current.focus();
        }
    }


    componentWillMount() {
        this.setState({
            item: this.props.desc6,
            search: this.props.isModalShow ? "" : this.props.mrpSearch,
            type: this.props.isModalShow || this.props.mrpSearch == "" ? 1 : 3

        })
        if (this.props.purchaseIndent.poItemcode.isSuccess) {
            if (this.props.purchaseIndent.poItemcode.data.resource != null) {
                this.setState({
                    getMrpData: this.props.purchaseIndent.poItemcode.data.resource,
                    prev: this.props.purchaseIndent.poItemcode.data.prePage,
                    current: this.props.purchaseIndent.poItemcode.data.currPage,
                    next: this.props.purchaseIndent.poItemcode.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.poItemcode.data.maxPage
                })
            } else {
                this.setState({
                    getMrpData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })

            }


        }
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.poItemcode.isSuccess) {
            if (nextProps.purchaseIndent.poItemcode.data.resource != null) {
                this.setState({
                    getMrpData: nextProps.purchaseIndent.poItemcode.data.resource,

                    prev: nextProps.purchaseIndent.poItemcode.data.prePage,
                    current: nextProps.purchaseIndent.poItemcode.data.currPage,
                    next: nextProps.purchaseIndent.poItemcode.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.poItemcode.data.maxPage

                })
            } else {
                this.setState({
                    getMrpData: [],


                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0

                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("adhocSearch").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.poItemcode.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.poItemcode.data.resource[0].mrp + "mrp",
                            search: this.props.isModalShow ? "" : this.props.mrpSearch,
                            type: this.props.isModalShow || this.props.mrpSearch == "" ? 1 : 3
                        })
                        document.getElementById(nextProps.purchaseIndent.poItemcode.data.resource[0].mrp) != null ? document.getElementById(nextProps.purchaseIndent.poItemcode.data.resource[0].mrp).focus() : null
                    }


                }
            }
            this.setState({
                modalDisplay: true,

            })
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }

    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.getMrpData.length != 0) {
                    this.setState({
                        // selectedId: this.state.getMrpData[0].mrp,
                        item: this.state.getMrpData[0].mrp
                    })
                    let mrp = this.state.getMrpData[0].mrp
                    window.setTimeout(function () {
                        document.getElementById("adhocSearch").blur()
                        document.getElementById(mrp).focus()
                    }, 0);
                } else {
                    window.setTimeout(function () {
                        document.getElementById("adhocSearch").blur()
                        document.getElementById("closeButton").focus()
                    }, 0);

                }
            }

            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("adhocSearch").blur()
                    document.getElementById("findButton").focus()
                }, 0);
            }
        }
        if (e.key === "ArrowDown") {
            if (this.state.getMrpData.length != 0) {
                this.setState({
                    // selectedId: this.state.getMrpData[0].mrp,
                    item: this.state.getMrpData[0].mrp
                })
                let mrp = this.state.getMrpData[0].mrp
                window.setTimeout(function () {
                    document.getElementById("adhocSearch").blur()
                    document.getElementById(mrp).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("adhocSearch").blur()
                    document.getElementById("closeButton").focus()
                }, 0);

            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0);
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let mrp = this.state.getMrpData[0].mrp
            window.setTimeout(function () {
                document.getElementById(mrp).blur()
                document.getElementById("doneButton").focus()
            }, 0);
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {

            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0);
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.closeItemmModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("adhocSearch").focus()
            }, 0);
        }
        this.props.closeItemmModal()
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.getMrpData.length != 0) {
                this.setState({
                    item: this.state.getMrpData[0].mrp
                })
                let mrp = this.state.getMrpData[0].mrp
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(mrp).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("adhocSearch").focus()
                }, 0);
            }
        }
    }

    selectLi(e, code) {
        let getMrpData = this.state.getMrpData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < getMrpData.length; i++) {
                if (getMrpData[i].mrp == code) {
                    index = i
                }
            }
            if (index < getMrpData.length - 1 || index == 0) {
                document.getElementById(getMrpData[index + 1].mrp) != null ? document.getElementById(getMrpData[index + 1].mrp).focus() : null

                this.setState({
                    focusedLi: getMrpData[index + 1].mrp + "mrp"
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < getMrpData.length; i++) {
                if (getMrpData[i].mrp == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(getMrpData[index - 1].mrp) != null ? document.getElementById(getMrpData[index - 1].mrp).focus() : null

                this.setState({
                    focusedLi: getMrpData[index - 1].mrp + "mrp"
                })

            }
        }
        if (e.which === 13) {
            this.onSelectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.closeKeyDown(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.getMrpData.length != 0 ?
                            document.getElementById(this.state.getMrpData[0].mrp).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.getMrpData.length != 0) {
                    document.getElementById(this.state.getMrpData[0].mrp).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeItemmModal(e)
        }


    }

    render() {
        console.log(this.props.mrpId, this.state.getMrpData)
        if (this.state.focusedLi != "" && this.props.mrpSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        if (this.props.mrpId != "" && this.props.mrpId != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let windowWidth = window.innerWidth;
            let position = document.getElementById(this.props.mrpId).getBoundingClientRect();
            let leftPosition = position.left;
            let left = 0;
            let diff = windowWidth - leftPosition;

            if (diff >= modalWidth) {
                left = leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                // let removeleft = diff >= 260 ? diff - 2 : diff;
                // left = leftPosition - removeleft;

                left = (leftPosition - removeWidth) - 20;
            }

            let idNo = parseInt(this.props.mrpId.replace(/\D/g, '')) + 1;
            // let top = 195 + (35 * idNo);
            let top = this.props.parent == "PO" ? 59 + (35 * idNo) : 59 + (35 * idNo);

            let newLeft = left > 40 ? left - 40 : left;

            console.log('leftPosition: ' + leftPosition)
            console.log('diff: ' + diff)
            console.log('left: ' + left)
            console.log('newLeft: ' + newLeft)


            $('#mrpModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');

        }
        //open modal for dropdown code end here
        const { search, sectionSelection } = this.state;
        return (
            this.props.isModalShow ? <div className={this.props.itemModalAnimation ? "modal display_block" : "display_none"} id="piopensizeModel">
                <div className={this.props.itemModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.itemModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.itemModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Size">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? "DESC 6" : "MRP"}</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select MRP from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-5 chooseDataModal">

                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByItem" name="searchByItem" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyPress={this._handleKeyPress} onKeyDown={this._handleKeyDown} value={search} id="adhocSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                        <li className="float_right">
                                            <label>
                                                {this.state.search == "" && (this.state.type == "" || this.state.type == 1) ?
                                                    <button type="button" className="clearbutton btnDisabled" >CLEAR</button>
                                                    : <button type="button" className="clearbutton" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>

                                                }
                                            </label>
                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    {/* <th>Item Code

                                                    </th> */}
                                                    <th>MRP</th>
                                                    <th>Rsp</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.getMrpData == null || this.state.getMrpData.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.getMrpData.map((data, key) => (
                                                    <tr key={key} onClick={(e) => this.onSelectedData(`${data.mrp}`)}>
                                                        <td>  <label className="select_modalRadio" >
                                                            <input type="radio" name="vendorMrpCheck " onKeyDown={(e) => this.focusDone(e)} id={data.mrp} checked={this.state.item == `${data.mrp}`} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        {/* <td>{data.itemCode}</td> */}

                                                        <td className="pad-lft-8">{data.mrp}</td>
                                                        <td>{data.rsp}</td>

                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} id="doneButton" onClick={() => this.onDone()}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} id="closeButton" onClick={(e) => this.closeItemmModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == "" ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button" >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 && this.state.prev != "" ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.next - 1 != this.state.maxPage && this.state.maxPage != 0 ? <li >

                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div> :
                this.state.modalDisplay ? <div className="poArticleModalPosition" id="mrpModalPosition">
                    <div className="dropdown-menu-city2 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                        <div className="dropdown-modal-header">
                            <span className="div-col-2">Mrp</span>
                            <span className="div-col-2">Rsp</span>
                        </div>

                        <ul className="dropdown-menu-city-item">
                            {this.state.getMrpData == null || this.state.getMrpData.length == 0 ? <li><span>No Data Found</span></li>
                                : this.state.getMrpData.map((data, key) => (
                                    <li key={key} onClick={(e) => this.onSelectedData(`${data.mrp}`)} id={data.mrp + "mrp"} className={this.state.item == `${data.mrp}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.mrp)}>
                                        <span className="vendor-details">
                                            <span className="vd-name div-col-2">{data.mrp}</span>
                                            <span className="vd-loc div-col-2">{data.rsp}</span>

                                        </span>
                                    </li>))}

                        </ul>
                        <div className="gen-dropdown-pagination">
                            <div className="page-close">
                                <button className="btn-close" type="button" onClick={(e) => this.closeItemmModal(e)} id="btn-close">Close</button>
                            </div>
                            <div className="page-next-prew-btn">
                                {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                                </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                                <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                                </button>
                                    : <button className="pnpb-next" type="button" disabled>
                                         <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                    </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                            </div>
                        </div>
                    </div>
                </div> : null

        );
    }
}

export default ItemCodeModal;
