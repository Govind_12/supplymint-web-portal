import React from "react";
import ToastLoader from "../loaders/toastLoader";
import ItemDetailsModal from '../purchaseIndent/itemDetailsModal';
import copyIcon from "../../assets/copy-icon-copy.svg";
import exclaimIcon from "../../assets/exclain.svg";
import ErrorIcon from "../../assets/eicon.svg";
import ItemUdfMappingModal from "./itemUdfMappingModal";
import PoError from "../loaders/poError";
import UdfMappingModal from "./udfMappingModal";
import PiColorModal from "../purchaseIndent/piColorModal";
import PiSizeModal from "../purchaseIndent/piSizeModal";
import PiImageModal from "../purchaseIndent/piImageModal";
import moment from "moment";
import _ from 'lodash';

// import { object } from "prop-types";



class SetModalPI extends React.Component {
    constructor(props) {
        super(props);
        this.searchInput = React.createRef();
        this.state = {
            itemObj: {},
            setGridId: '',
            setDataErr: false,
            setFocusId: '',
            isSetModalOpen: false,
            selectedSetId: '',
            selectedSetType: '',
            itemDataErr: false,
            itemFocusId: '',
            isItemModalOpen: false,
            selectedItemId: '',
            selectedItemType: '',
            catDataErr: false,
            catFocusId: '',
            isCatModalOpen: false,
            selectedCatId: '',
            selectedCatType: '',
            imagePth: "",
            selectedRowId: this.props.selectedRowId,
            copyUdf: '',
            colorPosId: "",
            sizePosId: "",
            catDescId: "",
            setUdfId: "",
            itemFocusId: "",
            searchIU: "",
            searchSU: "",
            errorMassage: "",
            poErrorMsg: false,
            poRows: [...this.props.poRows],
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            itemType: "",
            itemId: "",
            itemDetailValue: "",
            itemDetailCode: "",
            itemDetailName: "",
            focusId: "",
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: false,
            itemUdfId: "",
            itemUdfType: "",
            itemArticleCode: "",
            itemUdfName: "",
            itemValue: "",
            itemFocus: "",
            setValue: "",
            udfType: "",
            udfName: "",
            udfRow: "",
            udfMapping: false,
            udfMappingAnimation: false,
            mappingId: "",
            colorValue: "",
            colorRow: "",
            colorCode: this.props.departmentCode,
            colorModal: false,
            colorModalAnimation: false,
            section3ColorId: "",
            colorListValue: [],
            sizes: [],
            sizeArray: [],
            sizeId: [],
            sizeValue: [],
            ratioValue: [],
            sizeRow: "",
            sizeCode: "",
            sizeModal: false,
            sizeModalAnimatiion: false,
            sizeDataList: [],
            focusedQty: {
                id: "",
                qty: ""
            },
            errorId: "",
            catdescSearch: "",
            setUdfSearch: "",
            itemUdfSearch: "",
            colorSearch: "",
            sizeSearch: "",
            predefinedSetAvailable: "",
            isToggled: this.props.isSet == true ? "" : true,  // for first time
            imageModal: false,
            imageModalAnimation: false,
            imageState: {},
            ratioData: [],
            splitRow: true,
            multipleItemArray: [],
            setQty: "",
            simpleData: '',
            basicLineItem: ''


        }
    }
    componentDidMount() {        
        this.setState({
            poRows: this.props.poRows
        })
    }
        copyUdfHandler = (data) => {
        console.log(data)
        this.setState({
            copyUdf: data,
        })
        let udfArray = []
        data.setUdfArray.map(item => {
            if (item.value != '') {
                udfArray.push(JSON.stringify(item.value))
            }
        })
        let obj = { 'Set No': data.setNo, 'Color': [data.color].join(' '), 'Type': data.sizeType, 'Udf': [udfArray].join(' ') }
        navigator.clipboard.writeText(JSON.stringify(obj))
    }

    pasteUdfHandler = (rowId, itemId, data) => {
        let row = [...this.state.poRows];
        // for(let i = 0; i<row.length; i++){
        //     row[i].lineItem.map((item, index) => {
        //         console.log('index', itemId, index)
        //         if(itemId == index){
        //             console.log('reached');
        //             item = {...this.state.copyUdf}
        //             item.setNo = itemId + 1;
        //         }
        //     })
        // }
        let item = this.state.copyUdf;
        row[rowId].lineItem[itemId].setUdfArray = [...this.state.copyUdf.setUdfArray]
        // row[rowId].lineItem[itemId].color = data.color
        // row[rowId].lineItem[itemId].sizeType = data.sizeType
        // row[rowId].lineItem[itemId].setNo = itemId + 1
        this.setState({
            poRows: row
        })
    }


    _handleKeyPressRow(e, idName, count, type) {
        if (e.key === "F7" || e.key === "F2") {
            let idd = idName + count;
            document.getElementById(idd).click();
        }
        if (e.key === "ArrowDown") {
            count++;

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }
        }
        if (e.key === "ArrowUp") {
            count--

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }

        }
        if (e.key === "Enter") {
            e.preventDefault();
            if (type == "catdesc")
                this.props.codeRadio == "Adhoc" || this.props.codeRadio == "poIcode" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ?
                    document.getElementById("categories" + idName + count) != null ? document.getElementById("categories" + idName + count).click() : null
                    : null

            if (type == "itemUdf") {
                this.props.codeRadio == "Adhoc" || this.props.codeRadio == "poIcode" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ?
                    document.getElementById("itemUdf" + idName + count) != null ? document.getElementById("itemUdf" + idName + count).click() : null
                    : null

            }
            if (type == "setUdf") {
                this.props.codeRadio == "Adhoc" || this.props.codeRadio == "poIcode" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ?
                    document.getElementById("setUdf" + idName) != null ? document.getElementById("setUdf" + idName).click() : null
                    : null

            }
            if (idName == "color") {
                this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ?
                    document.getElementById(idName + "Div" + count) != null ? document.getElementById(idName + "Div" + count).click() : null
                    : null
            }
            if (idName == "size") {
                this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ?
                    document.getElementById(idName + "Div" + count) != null ? document.getElementById(idName + "Div" + count).click() : null
                    : null
            }
        }


    }
    componentWillReceiveProps(nextProps) {
        // if (nextProps.purchaseIndent.po_edit_data.data != null) {
        //     this.updateSetUdf()
        // }
        if(nextProps.purchaseIndent.getItemDetailsValue.isSuccess){
            if(nextProps.purchaseIndent.getItemDetailsValue.data.resource != null && !this.state.isCatModalOpen){
                let data = nextProps.purchaseIndent.getItemDetailsValue.data.resource;
                let catArray = [];
                data.map(item=>{catArray.push(item.cname)})
                let poRows = this.state.poRows
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.props.selectedRowId) {
                        for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                            if (poRows[i].catDescArray[j].catDescId == this.state.selectedCatId) {
                                for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                                    if (poRows[i].catDescArray[j].catDesc[k].catdesc == this.state.selectedCatType && poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") {
                                        let value = poRows[i].catDescArray[j].catDesc[k].value.trim();
                                        if(catArray.includes(value.toUpperCase())){
                                            data.map(item=> {
                                                if(item.cname.toUpperCase() == value.toUpperCase()){
                                                    poRows[i].catDescArray[j].catDesc[k].value = item.cname
                                                    poRows[i].catDescArray[j].catDesc[k].code = item.code
                                                    poRows[i].catDescArray[j].catDesc[k].isLovSelect = true
                                                }
                                            })                                        
                                        } else {
                                            poRows[i].catDescArray[j].catDesc[k].value = ''
                                            this.setState({
                                                catDataErr: true,
                                                poErrorMsg: true,
                                                errorMassage: 'Please select correct data from the drop-down list'
                                            })                                            
                                        }                                        
                                    }
                                }

                            }

                        }


                    }
                }
                this.setState({
                    poRows: poRows,
                },() => {

                    this.props.updatePoRows(poRows)

                    setTimeout(() => {
                        this.props.gridSecond()

                    }, 10)})
                } else if (nextProps.purchaseIndent.getItemDetailsValue.data.resource == null && !this.state.isCatModalOpen) {
                let poRows = this.state.poRows
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.props.selectedRowId) {
                        for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                            if (poRows[i].catDescArray[j].catDescId == this.state.selectedCatId) {
                                for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                                    if (poRows[i].catDescArray[j].catDesc[k].catdesc == this.state.selectedCatType && poRows[i].catDescArray[j].catDesc[k].isLovPI == "Y") {
                                            poRows[i].catDescArray[j].catDesc[k].value = ''
                                            this.setState({
                                                catDataErr: true,
                                                poErrorMsg: true,
                                                errorMassage: 'Please Select data from the drop-down list'
                                            })
                                    }
                                }

                            }

                        }


                    }
                }
                this.setState({
                    poRows: poRows,
                },() => {

                    this.props.updatePoRows(poRows)

                    setTimeout(() => {
                        this.props.gridSecond()

                    }, 10)})
                }
        }

        if(nextProps.purchaseIndent.itemUdfMapping.isSuccess){
            if (!this.state.isItemModalOpen) {
                if(nextProps.purchaseIndent.itemUdfMapping.data.resource != null){
                    let data = nextProps.purchaseIndent.itemUdfMapping.data.resource;
                    let desc = [];
                    data.map(item => {desc.push(item.description.toLowerCase())})
                    let poRows = this.state.poRows
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.props.selectedRowId) {
                            for (let j = 0; j < poRows[i].itemUdfArray.length; j++) {
                                if (poRows[i].itemUdfArray[j].itemUdfId == this.state.selectedItemId) {
                                    for (let k = 0; k < poRows[i].itemUdfArray[j].itemUdf.length; k++) {
                                        if (poRows[i].itemUdfArray[j].itemUdf[k].cat_desc_udf == this.state.selectedItemType) {
                                            if(poRows[i].itemUdfArray[j].itemUdf[k].isLovPI == 'Y'){
                                                let value = poRows[i].itemUdfArray[j].itemUdf[k].value.trim();
                                                if(desc.includes(value.toLowerCase())) {
                                                    data.map(item => {
                                                        if(item.description.toLowerCase() == value.toLowerCase()) {
                                                            poRows[i].itemUdfArray[j].itemUdf[k].value = item.description
                                                            poRows[i].itemUdfArray[j].itemUdf[k].isItemLovSelect = true
                                                        }
                                                    })
                                                } else {
                                                    poRows[i].itemUdfArray[j].itemUdf[k].value = ""
                                                    this.setState({
                                                        itemDataErr: true,
                                                        poErrorMsg: true,
                                                        errorMassage: 'Please select correct data from the drop-down list'
                                                    })  
                                                }
                                            }

                                        }
                                    }

                                }

                            }


                        }
                    }
                    this.setState({
                        poRows: poRows,
                    }, () => this.props.updatePoRows(poRows))
                } else if(nextProps.purchaseIndent.itemUdfMapping.data.resource == null) {
                    let poRows = this.state.poRows
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.props.selectedRowId) {
                            for (let j = 0; j < poRows[i].itemUdfArray.length; j++) {
                                if (poRows[i].itemUdfArray[j].itemUdfId == this.state.selectedItemId) {
                                    for (let k = 0; k < poRows[i].itemUdfArray[j].itemUdf.length; k++) {
                                        if (poRows[i].itemUdfArray[j].itemUdf[k].cat_desc_udf == this.state.selectedItemType) {
                                            if(poRows[i].itemUdfArray[j].itemUdf[k].isLovPI == 'Y'){
                                                    poRows[i].itemUdfArray[j].itemUdf[k].value = ""
                                                    this.setState({
                                                        itemDataErr: true,
                                                        poErrorMsg: true,
                                                        errorMassage: 'Please select correct data from the drop-down list'
                                                    })  
                                            }

                                        }
                                    }

                                }

                            }


                        }
                    }
                    this.setState({
                        poRows: poRows,
                    }, () => this.props.updatePoRows(poRows))
                }
            }
        }

        if (nextProps.purchaseIndent.udfType.isSuccess){
            if (!this.state.isSetModalOpen) {
                if (nextProps.purchaseIndent.udfType.data.resource != null) {
                    let data = nextProps.purchaseIndent.udfType.data.resource;
                    let setArray = [];
                    data.map(item => {setArray.push(item.name.toLowerCase())})
                    let poRows = this.state.poRows
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.props.selectedRowId) {
                            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                                if (poRows[i].lineItem[j].gridTwoId == this.state.setGridId) {
                                    for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                                        if (poRows[i].lineItem[j].gridTwoId == this.state.setGridId && poRows[i].lineItem[j].setNo == this.state.selectedSetId && poRows[i].lineItem[j].setUdfArray[k].udfType == this.state.selectedSetType) {
                                            if(poRows[i].lineItem[j].setUdfArray[k].isLovPI == 'Y') {
                                                let value = poRows[i].lineItem[j].setUdfArray[k].value.trim();
                                                if (setArray.includes(value.toLowerCase())) {
                                                    data.map(item => {
                                                        if (item.name.toLowerCase() == value.toLowerCase()) {
                                                            poRows[i].lineItem[j].setUdfArray[k].value = item.name
                                                            poRows[i].lineItem[j].setUdfArray[k].isSetLovSelect = true
                                                        }
                                                    })
                                                } else {
                                                    poRows[i].lineItem[j].setUdfArray[k].value = ""
                                                    this.setState({
                                                        setDataErr: true,
                                                        poErrorMsg: true,
                                                        errorMassage: 'Please select correct data from the drop-down list'
                                                    })
                                                }
                                            }

                                        }
                                    }

                                }

                            }


                        }
                    }
                    let poRowss = _.map(
                        _.uniq(
                            _.map(poRows, function (obj) {

                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    this.setState({
                        poRows: poRowss,
                    }, () => this.props.updatePoRows(poRows))
                } else if (nextProps.purchaseIndent.udfType.data.resource == null) {
                    let poRows = this.state.poRows
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.props.selectedRowId) {
                            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                                if (poRows[i].lineItem[j].gridTwoId == this.state.setGridId) {
                                    for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                                        if (poRows[i].lineItem[j].gridTwoId == this.state.setGridId && poRows[i].lineItem[j].setNo == this.state.selectedSetId && poRows[i].lineItem[j].setUdfArray[k].udfType == this.state.selectedSetType) {
                                            if(poRows[i].lineItem[j].setUdfArray[k].isLovPI == 'Y') {
                                                poRows[i].lineItem[j].setUdfArray[k].value = ""
                                                this.setState({
                                                    setDataErr: true,
                                                    poErrorMsg: true,
                                                    errorMassage: 'Please select correct data from the drop-down list'
                                                })
                                            }

                                        }
                                    }

                                }

                            }


                        }
                    }
                    let poRowss = _.map(
                        _.uniq(
                            _.map(poRows, function (obj) {

                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    this.setState({
                        poRows: poRowss,
                    }, () => this.props.updatePoRows(poRows))
                }
            }
        }

        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {   
            console.log( nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO)         
            this.setState({
                simpleData: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.ppQty != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.ppQty : 0,                
            })
        }
        if (nextProps.purchaseIndent.lineItem.data.resource != null) {
            this.setState({
                basicLineItem: nextProps.purchaseIndent.lineItem.data.resource.basic,
            })
        }

    }
    // updateSetUdf = () => {
    //     var setUdfEdit = {}
    //     //console.log(this.props.purchaseIndent.po_edit_data.data.resource)
    //     if (this.props.purchaseIndent.po_edit_data.data.resource != undefined) {
    //         this.props.purchaseIndent.po_edit_data.data.resource.poDetails.map(d => {
    //             d.lineItem.map(e => {
    //                 setUdfEdit = e.setUdf
    //             })
    //         })
    //     }

    //     let setUdfEditArray = Object.keys(setUdfEdit)
    //     let poRows = [...this.state.poRows]
    //     for (let i = 0; i < poRows.length; i++) {
    //         poRows[i].lineItem.map(a => {
    //             a.setUdfArray.map(b => {
    //                 setUdfEditArray.forEach((key, index) => {
    //                     //console.log("index: ", index, "setUdfEdit", setUdfEdit, "b.udfType", b.udfType)
    //                     if (setUdfEditArray[index] == b.udfType) {
    //                         //console.log("setUdfEditArray[index] ", setUdfEditArray[index], "b.udfType", b.udfType)
    //                         b.value = setUdfEdit[key]
    //                     }

    //                 })
    //             })
    //         })
    //     }
    //     //console.log("variable poRows: ", poRows)
    //     this.setState({
    //         poRows: poRows
    //     })

    // }
    closeItemModal = () => {
        this.setState({
            isCatModalOpen: false,
            itemDetailsModal: false,
            itemDetailsModalAnimation: !this.state.itemDetailsModalAnimation,
            catdescSearch: ""
        })
        setTimeout(() => {
            document.getElementById(this.state.focusId).focus()
        }, 100);
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }

    catDropValidation = (type, id, value, displayName, idFocus, code, key, e) =>{
        console.log('blurrrr', this.state.isCatModalOpen)
        let poRows = this.state.poRows
        if(e.target.value != ''){
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.props.selectedRowId) {
                    for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                        if (poRows[i].catDescArray[j].catDescId == id) {
                            for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                                if (poRows[i].catDescArray[j].catDesc[k].catdesc == type) {                                 
                                    if (poRows[i].catDescArray[j].catDesc[k].isLovSelect == false || poRows[i].catDescArray[j].catDesc[k].isLovSelect == undefined){
                                            if(!this.state.isCatModalOpen){
                                                let data = {
                                                    hl3Code: this.props.departmentCode,
                                                    hl3Name: this.props.department,
                                                    itemType: type,
                                                    no: 1,
                                                    type: this.props.isModalShow || value == "" ? 1 : 3,
                                                    search: this.props.isModalShow ? "" : value,
                                                }        
                                                this.setState({
                                                    catFocusId: idFocus,
                                                    selectedCatId: id,
                                                    selectedCatType: type,
                                                }, () => this.props.getItemDetailsValueRequest(data))
                                            }
                                    }
                                }
                            }
    
                        }
    
                    }
    
    
                }
            }              
        }    
    }

    openItemModal(type, id, value, displayName, idFocus, code, key) {

        this.childEsc()
        let poRows = [...this.state.poRows]
        this.setState({
            isCatModalOpen: true,
            focusId: idFocus
        })
        this.props.updateFocusId(idFocus)
        // let flag = false
        // poRows.forEach(po => {
        //     flag = (po.vendorDesign == "" && !this.props.isVendorDesignNotReq) || po.rate == "" || po.marginRule == "" || (po.mrp == "" && this.props.isMRPEditable) || (po.vendorMrp == "" && this.props.isMrpRequired) || po.deliveryDate == "" ? true : false

        // })
        // if (flag) {
        //     this.setState({
        //         focusId: idFocus,
        //     })
        //     this.props.validateItemdesc()
        // } else {
        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss
        })
        this.props.updatePoRows(poRowss)
        var data = {
            hl3Code: this.props.departmentCode,
            hl3Name: this.props.department,
            itemType: type,
            no: 1,
            type: this.props.isModalShow || value == "" ? 1 : 3,
            search: this.props.isModalShow ? "" : value,

        }

        this.props.getItemDetailsValueRequest(data);
        this.setState({
            catDescId: idFocus,
            catdescSearch: this.props.isModalShow ? "" : value,
            itemDetailsModal: true,
            itemDetailsModalAnimation: !this.state.itemDetailsModalAnimation,
            itemId: id,
            itemDetailValue: value,
            itemDetailCode: code,
            itemType: type,
            itemDetailName: typeof (displayName) == "string" && displayName != "null" ? displayName : type,
            focusId: idFocus,

        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
        // }

    }
    updateItemDetailValue() {
        this.setState({
            itemDetailValue: ""
        })
    }
    updateUdfSetvalue() {
        this.setState({
            setValue: ""
        })
    }
    updateItemValue() {
        this.setState({
            itemValue: ""
        })
    }
    Capitalize(str) {
        if (str == undefined) {
            return "";
        } else {

            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    }
    updateItemDetails(data) {

        let poRows = [...this.state.poRows]

        poRows.forEach(iDetails => {
            if (iDetails.gridOneId == this.props.selectedRowId) {
                iDetails.catDescArray.forEach(iListt => {

                    if (iListt.catDescId == this.state.itemId) {

                        iListt.catDesc.forEach(iList => {
                            if (iList.catdesc == data.type) {
                                iList.value = data.checkedData
                                iList.code = data.checkedCode
                                iList.isLovSelect = true
                            }
                        })
                    }
                })
            }
        })
        document.getElementById(this.state.focusId).focus()

        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss

        }, () => {

            this.props.updatePoRows(poRows)

            setTimeout(() => {
                this.props.gridSecond()

            }, 10)
        })

    }

    itemDropdownValidation = (udfType, name, id, value, idFocus, e) => {
        if(e.target.value != ''){
            let poRows = this.state.poRows
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.props.selectedRowId) {
                    for (let j = 0; j < poRows[i].itemUdfArray.length; j++) {
                        if (poRows[i].itemUdfArray[j].itemUdfId == id) {
                            for (let k = 0; k < poRows[i].itemUdfArray[j].itemUdf.length; k++) {
                                if (poRows[i].itemUdfArray[j].itemUdf[k].cat_desc_udf == udfType) {
                                    if(poRows[i].itemUdfArray[j].itemUdf[k].isItemLovSelect == undefined || poRows[i].itemUdfArray[j].itemUdf[k].isItemLovSelect == false){
                                        if(!this.state.isItemModalOpen){
                                            let data = {
                                                type: this.props.isModalShow || value == "" ? 1 : 3,
                                                search: this.props.isModalShow ? "" : value,
                                                no: 1,
                                                description: "",
                                                itemUdfType: udfType,
                                                ispo: true,
                                                hl3Code: this.props.departmentCode,
                                                hl4Code: this.props.articleCode,
                                                hl4Name: this.props.articleName,
                                
                                            }
                                            this.setState({
                                                itemFocusId: idFocus,
                                                selectedItemId: id,
                                                selectedItemType: udfType
                                            }, () => this.props.itemUdfMappingRequest(data) )
                                        }
                                    }
                                }
                            }

                        }

                    }


                }
            }            
        }
    }


    openItemUdfModal(udfType, name, id, value, idFocus) {
        this.childEsc()
        let poRows = [...this.state.poRows]
        this.setState({
            focusId: idFocus,            
        })
        this.props.updateFocusId(idFocus)

        let flag = false

        poRows.forEach(po => {
            if (po.gridOneId == this.props.selectedRowId && this.props.isCatDescExist) {

                po.catDescArray.forEach(cd => {
                    if (cd.catDescId == id) {
                        cd.catDesc.forEach(val => {

                            if ((val.catdesc == "CAT1" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "CAT2" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "CAT3" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "CAT4" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "CAT5" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "CAT6" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "DESC1" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "DESC2" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "DESC3" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "DESC4" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "DESC5" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "DESC6" && ((val.isDisplayPI == "Y") && val.isCompulsoryPI == "Y") && val.value == "") ||
                                (val.catdesc == "CATREMARK" && this.props.isMandateCatRemark == true && val.value == "")) {
                                flag = true

                                return flag
                            }
                        })
                    }
                })
            }
        })

        if (flag) {
            this.setState({
                errorId: idFocus,
            })
            this.props.validateCatDescRow();
        } else {
            let poRowss = _.map(
                _.uniq(
                    _.map(poRows, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );
            this.setState({
                poRows: poRowss
            })
            this.props.updatePoRows(poRowss)
            let data = {
                type: this.props.isModalShow || value == "" ? 1 : 3,
                search: this.props.isModalShow ? "" : value,

                no: 1,

                description: "",
                itemUdfType: udfType,
                ispo: true,
                hl3Code: this.props.departmentCode,
                hl4Code: this.props.articleCode,
                hl4Name: this.props.articleName,

            }
            this.props.itemUdfMappingRequest(data)
            this.setState({
                isItemModalOpen: true,
                itemUdfSearch: this.props.isModalShow ? "" : value,
                itemUdfMappingModal: true,
                itemUdfMappingAnimation: !this.state.itemUdfMappingAnimation,
                itemUdfId: id,
                itemUdfType: udfType,
                itemArticleCode: this.props.articleCode,
                itemUdfName: name != null ? name : udfType,
                itemValue: value,
                itemFocus: idFocus,
                searchIU: "itemUdf" + idFocus,
                itemFocusId: idFocus,
                focusId: idFocus
            })
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }
        }
    }
    closeItemUdfModal() {
        this.setState({
            isItemModalOpen: false,
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: !this.state.itemUdfMappingAnimation,
            itemUdfSearch: ""
        })
        setTimeout(() => {
            document.getElementById(this.state.itemFocus).focus()
        }, 100);
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    itemUdfUpdate(data) {
        let poRows = [...this.state.poRows]
        if (poRows != undefined) {
            for (var k = 0; k < poRows.length; k++) {
                if (poRows[k].gridOneId == this.props.selectedRowId) {
                    for (var l = 0; l < poRows[k].itemUdfArray.length; l++) {
                        if (poRows[k].itemUdfArray[l].itemUdfId == this.state.itemUdfId) {
                            poRows[k].itemUdfArray[l].itemUdf.forEach(val => {
                                if (val.cat_desc_udf == data.itemUdfType) {
                                    val.value = data.value
                                    val.isItemLovSelect = true
                                }
                            })
                        }
                    }
                }
            }

            let poRowss = _.map(
                _.uniq(
                    _.map(poRows, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );
            this.setState({
                poRows: poRowss

            }, () => {
                this.props.gridThird();
                this.props.updatePoRows(poRows)

            }
            )
            document.getElementById(this.state.itemFocus).focus()

        }
    }

    setDropdownValidation = (code, name, id, value, idFocus, gridId, e) => {
        if(e.target.value != '') {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == gridId) {
                        for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                            if (poRows[i].lineItem[j].gridTwoId == gridId && poRows[i].lineItem[j].setNo == id && poRows[i].lineItem[j].setUdfArray[k].udfType == code) {
                                if (poRows[i].lineItem[j].setUdfArray[k].isSetLovSelect == undefined || poRows[i].lineItem[j].setUdfArray[k].isSetLovSelect == false) {
                                    let udfType = code;
                                    var data = {
                                        no: 1,
                                        type: this.props.isModalShow || value == "" ? 1 : 3,
                                        search: this.props.isModalShow ? "" : value,
                                        udfType: udfType,
                                        code: "",
                                        name: "",
                                        ispo: true,
                                    }
                                    this.setState({
                                        setGridId: gridId,
                                        selectedSetId: id,
                                        selectedSetType: code,
                                        setFocusId: idFocus,
                                    }, () => this.props.udfTypeRequest(data))
                                }
                            }
                        }

                    }

                }


            }
        }
        }
    }

    openUdfMappingModal(code, name, id, value, idFocus, gridId) {
        this.childEsc()
        this.setState({
            focusId: idFocus

        })
        this.props.updateFocusId(idFocus)

        let poRows = [...this.state.poRows]
        let flag = false
        poRows.forEach(row => {
            if (row.gridOneId == this.props.selectedRowId) {
                row.lineItem.forEach(li => {


                    flag = li.setQty == "" || li.color.length == 0 ? true : false

                })
            }

        })
        if (flag) {
            this.setState({
                errorId: idFocus,
            })
            this.props.validateLineItem()
        }
        else {
            let poRowss = _.map(
                _.uniq(
                    _.map(poRows, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );
            this.setState({
                poRows: poRowss
            })
            this.props.updatePoRows(poRowss)
            let udfType = code;
            let udfName = name == null ? code : name;
            let idd = id;
            var data = {
                no: 1,
                type: this.props.isModalShow || value == "" ? 1 : 3,
                search: this.props.isModalShow ? "" : value,
                udfType: udfType,
                code: "",
                name: "",
                ispo: true,

            }

            this.props.udfTypeRequest(data);
            this.setState({
                isSetModalOpen: true,
                searchSU: "setUdf" + idFocus + gridId,
                setUdfSearch: this.props.isModalShow ? "" : value,
                setValue: value,
                udfType: udfType,
                udfName: udfName,
                udfRow: gridId,
                udfMapping: true,
                udfMappingAnimation: !this.state.udfMappingAnimation,
                mappingId: idFocus,
                setUdfId: idFocus,
            });
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeUdf() {
        this.setState({
            isSetModalOpen: false,
            udfMapping: false,
            udfMappingAnimation: !this.state.udfMappingAnimation,
            setUdfSearch: ""
        });
        setTimeout(() => {
            document.getElementById(this.state.mappingId).focus()
        }, 100);
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateUdf(data) {
        let poRows = [...this.state.poRows]
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == this.state.udfRow) {
                        for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                            if (poRows[i].lineItem[j].setUdfArray[k].udfType == data.udfType) {
                                poRows[i].lineItem[j].setUdfArray[k].value = data.name
                                poRows[i].lineItem[j].setUdfArray[k].isSetLovSelect = true
                            }
                        }
                    }
                }
            }
        }

        document.getElementById(this.state.mappingId).focus()
        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss
        }, () => {

            this.props.updatePoRows(poRowss)
            setTimeout(() => {

                this.props.gridFivth();
            }, 10)
        })
    }
    copySet(copyid, designId, setNo, wholeData) {
        var flag = false;
        if(wholeData.setQty == "" || wholeData.color.length == 0 || wholeData.sizes.length == 0){
            flag = true
        }
        else{
            flag = false;
        }
        if(flag == true){
            this.setState({
                poErrorMsg: true,
                errorMassage: "Please fill mandate fields first."
            })
        }
        else{
            let poRows = [...this.state.poRows]
            let flag = true
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == designId) {
                    for (let j = 0; j < poRows[i].lineItem.length; j++) {
                        if (poRows[i].lineItem[j].gridTwoId == copyid) {
    
                            for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                                if ((poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING01" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING02" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING03" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING04" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING05" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING06" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING07" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING08" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING09" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING10" && (( poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM01" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM02" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM03" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM04" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM05" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE01" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE02" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE03" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE04" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                                    (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE05" && ((poRows[i].lineItem[j].setUdfArray[k].isLov == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "")) {
                                    flag = false
                                    break
                                }
    
                            }
                        }
                    }
                }
            }
    
            if (flag) {
    
                let poRows = [...this.state.poRows]
                let rowids = []
                let finalId = ""
                let secTwoRowsObj = {}
                let sets = []
                let finalSetnO = ""
                poRows.filter(po => {
                    if (po.gridOneId == this.props.selectedRowId) {
                        po.lineItem.forEach(li => {
                            rowids.push(li.gridTwoId)
                            sets.push(li.setNo)
                        })
                    }
    
                })
    
                finalId = Math.max(...rowids);
    
                finalSetnO = Math.max(...sets)
                let lineItem = {}
                poRows.forEach((po) => {
                    if (po.gridOneId == this.props.selectedRowId) {
                        console.log(po.lineItem, this.state.simpleData)
                        po.lineItem.forEach((li, key) => {
                            if (li.gridTwoId == copyid) {
                                lineItem = {
                                    basic: li.basic,
                                    colorChk: false,
                                    icodeChk: false,
                                    colorSizeList: li.colorSizeList ? [...li.colorSizeList] : [],
                                    icodes: li.icodes,
                                    itemBarcode: li.itemBarcode,
                                    setHeaderId: "",
                                    gridTwoId: po.lineItem.length + Math.floor(Math.random() * 100),
                                    //gridTwoId: po.lineItem.length, //// multipleLineItem comparison getting 77 and 83 sort of numbers 
                                    color: li.color,
                                    colorSearch: li.colorSearch,
                                    colorList: [...li.colorList],
                                    sizes: li.sizes,
                                    sizeSearch: li.sizeSearch,
                                    sizeList: [...li.sizeList],
                                    ratio: li.ratio,
                                    size: li.size,
                                    setRatio: li.setRatio,
                                    option: li.option,
                                    setNo: po.lineItem.length + 1,
                                    total: li.total,
                                    setQty: li.setQty,
                                    quantity: li.quantity,
                                    amount: li.amount,
                                    rate: li.rate,
                                    image: li.image,
                                    imagePath: li.imagePath,
                                    imageUrl: li.imageUrl,
                                    containsImage: li.containsImage,
                                    gst: li.gst,
                                    finCharges: li.finCharges ? [...li.finCharges] : li.finCharge ? [...li.finCharge] : [],
                                    tax: li.tax,
                                    otb: li.otb - li.amount,
                                    calculatedMargin: li.calculatedMargin,
                                    mrk: li.mrk,
                                    setUdfHeader: [...li.setUdfHeader],
                                    setUdfArray: [...li.setUdfArray],
                                    sizeType: li.sizeType,
                                    ppQty: li.sizeType == "complex" && li.total != "undefined" ? li.total : this.state.simpleData,
                                }
                            }
                        })
                        po.lineItem.push(lineItem)
                    }
                })
                this.setState({
                    poRows
    
                }, () => {
                    this.props.updatePoRows(poRows)
                    setTimeout(() => {
                        this.props.getOtbForPoRows()
                        setTimeout(() => {
    
                            this.props.updatePo()
                        }, 10)
    
                    }, 10)
                })
            } else {
                this.props.validateSetUdf()
            }
        }

        // },10)
    }

    openPoColorModal(id, checkValue, colorList, idFocus, colorSearch, type) {
        this.childEsc()
        let poRows = [...this.state.poRows]
        this.setState({
            focusId: idFocus,
            sizeType: type
        })
        this.props.updateFocusId(idFocus)
        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {
                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss
        }, () => {
            let flag = false
            if(this.props.mrpValidation){
                poRows.forEach(po => {
                    flag = (po.vendorDesign == "" && !this.props.isVendorDesignNotReq) || po.rate == "" || po.marginRule == "" && this.props.isMarginRulePo || po.mrp == "" && this.props.isMRPEditable || po.vendorMrp == "" && this.state.isMrpRequired || po.deliveryDate == "" ? true : false
                })
            }
            else{
                poRows.forEach(po => {
                    flag = (po.vendorDesign == "" && !this.props.isVendorDesignNotReq) || po.rate == "" || po.marginRule == "" && this.props.isMarginRulePo || po.deliveryDate == "" ? true : false
                })
            }
            
            if (flag) {
                this.setState({
                    errorId: idFocus,
                })
                this.props.validateItemdesc()
            } else {

                let poRows = [...this.state.poRows]
                var dataColor = {
                    id: id,
                    no: 1,
                    // type: this.props.isModalShow || colorSearch == "" ? 1 : 3,
                    type: 1,
                    search: this.props.isModalShow ? "" : colorSearch != undefined ? colorSearch : "",
                    hl3Name: this.props.department,
                    hl3Code: this.props.departmentCode,
                    itemType: 'color'
                }
                this.props.colorRequest(dataColor)

                this.setState({
                    colorSearch: this.props.isModalShow ? "" : colorSearch,

                    colorValue: checkValue == "" ? [] : checkValue.split(','),
                    colorRow: id,
                    colorType: type,
                    colorCode: this.props.departmentCode,
                    colorModal: true,
                    colorModalAnimation: !this.state.colorModalAnimation,
                    section3ColorId: idFocus,
                    colorListValue: colorList == "" ? [] : colorList,
                    colorPosId: idFocus,
                    focusId: idFocus
                });

            }
            // }
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }
        })
    }
    deselectallProp(data) {
        this.setState({
            colorListValue: data.colorList
        })
    }
    closePiColorModal() {
        document.getElementById(this.state.section3ColorId).focus()
        this.setState({
            colorModal: false,
            colorModalAnimation: !this.state.colorModalAnimation
        });
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }

    updateColorList(udata) {
        let colorL = [...udata.colorList]
        let colorA = []
        var reCollectData = [];
        let index = 0
        console.log('updateColorStatePre', this.state.poRows)
        colorL.forEach(color => {
            if (!colorA.includes(color.code)) {
                let colorData = {
                    id: index++,
                    code: color.code,
                    cname: color.cname,
                }
                colorA.push(color.code)
                reCollectData.push(colorData);
            }
        })

        var poRows = [...this.state.poRows]
        if (colorL.length != 0) {
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.props.selectedRowId) {
                    for (var x = 0; x < poRows[i].lineItem.length; x++) {
                        if (poRows[i].lineItem[x].gridTwoId == udata.colorrId) {
                            poRows[i].lineItem[x].colorList = reCollectData

                        }
                    }
                }
            }
        }
        else {
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.props.selectedRowId) {
                    for (var x = 0; x < poRows[i].lineItem.length; x++) {
                        if (poRows[i].lineItem[x].gridTwoId == udata.colorrId) {
                            poRows[i].lineItem[x].colorList = reCollectData

                        }
                    }
                }
            }
        }
        console.log('updateColorList', poRows)
        this.setState({
            poRows: poRows
        }, () => {
        })
    }
    updateColorState(data2) {
        console.log('updateColorStatePre', this.state.poRows)
        let cat6 = data2.colorData;
        let poRows = [...this.state.poRows]
        let setQty = ""
        let c = 0
        let designId = 0
        let rate = ""
        let mrp = ""
        let finalRate = ""
        for (let k = 0; k < poRows.length; k++) {
            if (poRows[k].gridOneId == this.props.selectedRowId) {
                rate = poRows[k].rate
                finalRate = poRows[k].finalRate
                mrp = poRows[k].vendorMrp
            }
        }
        for (let x = 0; x < poRows.length; x++) {
            if (poRows[x].gridOneId == this.props.selectedRowId) {
                for (let l = 0; l < poRows[x].lineItem.length; l++) {
                    if (poRows[x].lineItem[l].gridTwoId == data2.colorrId) {
                        for (let i = 0; i < poRows[x].lineItem[l].ratio.length; i++) {
                            c += Number(poRows[x].lineItem[l].ratio[i])
                        }
                        poRows[x].lineItem[l].color = Array.isArray(cat6) ? cat6.join(',') : cat6;
                        poRows[x].lineItem[l].colorSearch = Array.isArray(cat6) ? cat6.join(',') : cat6
                        poRows[x].lineItem[l].option = data2.colorData.length
                        setQty = poRows[x].lineItem[l].setQty
                        poRows[x].lineItem[l].total = data2.colorData.length * c
                        // if(poRows[x].lineItem[l].quantity != ''){
                        //     poRows[x].lineItem[l].quantity = (poRows[x].lineItem[l].ppQty) * (poRows[x].lineItem[l].setQty)
                        // }
                        if (setQty != "") {
                            if (finalRate != 0 || finalRate != "") {
                                if (c != 0) {
                                    let taxData = {
                                        hsnSacCode: poRows[x].hsnSacCode,
                                        qty: poRows[x].lineItem[l].sizeType == "simple" ? Number(setQty) * poRows[x].lineItem[l].ppQty : Number(setQty) * Number(data2.colorData.length * c),
                                        rate: finalRate,
                                        rowId: poRows[x].lineItem[l].gridTwoId,
                                        designRow: poRows[x].gridOneId,
                                        mrp: mrp,
                                        piDate: new Date(),
                                        supplierGstinStateCode: this.props.stateCode,
                                        purtermMainCode: this.props.termCode,
                                        siteCode: this.props.siteCode
                                    }
                                    if (poRows[x].hsnSacCode != "" || poRows[x].hsnSacCode != null) {
                                        this.props.lineItemRequest(taxData)
                                    } else {
                                        this.setState({
                                            errorMassage: "HSN code is complusory",
                                            poErrorMsg: true
                                        })
                                    }
                                }
                            } else {
                                this.setState({
                                    errorMassage: "Rate is complusory",
                                    poErrorMsg: true

                                })
                            }
                        }
                    }
                }
            }
        }

        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {
                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        console.log('updateColorState', poRows)
        this.setState({
            poRows: poRowss
        }, () => {
            if (setQty != "") {
                this.updateLineItem(data2.colorrId)
            }
            this.props.updatePoRows(poRows)
            document.getElementById(data2.section3ColorId).focus()
            setTimeout(() => {

                this.props.gridFourth();
            }, 10)
        })
    }
    updateLineItem(id) {
        console.log("updatelineItemCode")
        let idd = id
        let rows = [...this.state.poRows];
        let designRow = ""
        let quantity = ""
        let prevQuantity = ""
        rows.forEach(pData => {
            if (pData.gridOneId == this.props.selectedRowId) {
                pData.lineItem.forEach(li => {
                    if (li.gridTwoId == idd) {
                        designRow = li.gridOneId
                        prevQuantity = li.quantity
                        li.quantity = li.sizeType == "simple" ? li.setQty * li.ppQty : li.setQty * li.total
                        li.option = li.sizeType == "simple" ? 1 : li.option
                        quantity = li.setQty * li.total
                    }
                })
            }
        })
        rows.forEach(r => {
            if (r.gridOneId == designRow) {
                r.quantity = Number(r.quantity) - Number(prevQuantity)
                r.quantity = Number(r.quantity) + Number(quantity)
            }
        })
        this.setState({
            poRows: rows
        }, () => {
            this.props.updatePoRows(rows)
            this.props.updatePoAmountNpoQuantity()
        }, () => {
        })
    }

    openPiSizeModal(code, id, sizeValue, ratioValue, articleName, sizeList, key, sizeSearch, type, wholeData) {
        this.childEsc()
        this.setState({
            focusId: "size" + key
        })
        this.props.updateFocusId("size" + key)
        let sizeArray = []
        let poRows = this.state.poRows
        let idd = [];
        console.log(poRows, id)
        if(wholeData.colorList.length == 0 && this.props.isColorRequired){
            this.setState({
                poErrorMsg: true,
                errorMassage: "Please fill Color first!"
            })
        }
        else{
            let poRowss = _.map(
                _.uniq(
                    _.map(poRows, function (obj) {
                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );
            this.setState({
                poRows: poRowss
            }, () => {
                let sizeRatioArray = []
                if (articleName != "") {
                    if (sizeValue != "") {
                        sizeValue = sizeValue
                        ratioValue = ratioValue
                        for (var i = 0; i < sizeValue.length; i++) {
                            for (var j = 0; j < ratioValue.length; j++) {
                                if (i == j) {
                                    let data = {
                                        size: sizeValue[i],
                                        ratio: ratioValue[j]
                                    }
                                    sizeArray.push(data)
                                }
                            }
                        }
                    }
                    console.log('sizelist', sizeList)
                    if (Object.keys(sizeList).length != 0) {
                        sizeList.ratio = ratioValue[0]
                        for (var i = 0; i < sizeList.length; i++) {
                            idd.push(sizeList[i].id)
                            for (var j = 0; j < ratioValue.length; j++) {
                                if (i == j) {
                                    sizeList[i].ratio = ratioValue[j]
                                }
                            }
                        }
                    }
                    console.log('sizepropid', idd)
                    let poRows = [...this.state.poRows];
                    let itemUdfArray = poRows[this.props.selectedRowId -1].itemUdfArray[0].itemUdf;
                    let itemObj = {};
                    itemUdfArray.forEach(item => {
                        itemObj[item.cat_desc_udf] = item.value
                    })
                    var dataColor = {
                        id: id,
                        // code: code,
                        no: 1,
                        // type: this.props.isModalShow || sizeSearch == "" ? 1 : 3,
                        type: 1,
                        search: this.props.isModalShow ? "" : sizeSearch,
                        hl3Name: this.props.department,
                        hl1Name: this.props.divisionName,
                        hl2Name: this.props.sectionName,
                        hl3Code: this.props.departmentCode,
                        hl4Name: this.props.articleName,
                        itemType: 'size',
                        isToggled: type == "simple" ? true : this.state.isToggled,
                        itemUdfObj: itemObj,
                    }
                    this.props.sizeRequest(dataColor)
                    this.props.updatePoRows(poRowss)
                    this.setState({
                        itemObj: itemObj,
                        sizeId: idd,
                        sizeSearch: this.props.isModalShow ? "" : sizeSearch,
                        sizes: sizeValue == "" ? [] : sizeValue,
                        sizeArray: sizeArray,
                        sizeValue: sizeValue == "" ? [] : sizeValue,
                        ratioValue: ratioValue == "" ? [] : ratioValue,
                        sizeRow: id,
                        sizeCode: code,
                        sizeModal: true,
                        sizeModalAnimatiion: !this.state.sizeModalAnimatiion,
                        sizeDataList: sizeList,
                        focusId: "size" + key,
                        sizePosId: "size" + key,
                        sizeType: type == undefined ? "complex" : type,
                    });
                } else {
                    this.setState({
                        toastMsg: "Select Article",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1000)
                }
            })
        }
    }
    updateSizeList(sizeList) {
        let index = 1
        let reCollectData = [];
        console.log('ss', sizeList)
        sizeList.forEach(size => {
            let sizeData = {
                id: size.id,
                code: size.code,
                cname: size.cname
            }
            reCollectData.push(sizeData);
        })
        console.log('sizeUpdate', reCollectData)
        let array = [...this.state.poRows]
        for (let x = 0; x < array.length; x++) {
            if (array[x].gridOneId == this.props.selectedRowId) {
                for (let y = 0; y < array[x].lineItem.length; y++) {
                    if (array[x].lineItem[y].gridTwoId == sizeList[0].lineId) {
                        array[x].lineItem[y].sizeList = reCollectData
                    }
                }
            }
        }
        this.setState({
            poRows: array
        }, () => {
            this.props.updatePoRows(array)
        })
    }
    childEsc() {
        this.setState({
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            colorModal: false,
            colorModalAnimation: false,
            itemUdfMappingModal: false,
            isItemModalOpen: false,
            isSetModalOpen: false,
            itemUdfMappingAnimation: false,
            sizeModal: false,
            sizeModalAnimatiion: false,
            udfMapping: false,
            udfMappingAnimation: false,
        })
    }
    // updateSimpleModalRatio = (data1) => {    
    // }
    updateSizeState(data1, sizeList) {
        this.setState({
            predefinedSetAvailable: data1.predefinedSetAvailable,
            isToggled: data1.isToggled
        })
        let cat5 = data1.catFive;
        let catFiveRatio = data1.cat5Ratio;
        let id = 0

        let poRows = [...this.state.poRows]
        let sum = 0;
        for (let i = 0; i < catFiveRatio.length; i++) {
            sum += Number(catFiveRatio[i])
        }
        for (let x = 0; x < poRows.length; x++) {
            if (poRows[x].gridOneId == this.props.selectedRowId) {
                for (let y = 0; y < poRows[x].lineItem.length; y++) {
                    if (poRows[x].lineItem[y].gridTwoId == data1.sizerId) {
                        poRows[x].lineItem[y].ratio = catFiveRatio
                        poRows[x].lineItem[y].sizes = cat5
                        poRows[x].lineItem[y].sizeSearch = cat5.join(',')
                        poRows[x].lineItem[y].total = poRows[x].lineItem[y].option * sum
                        poRows[x].lineItem[y].quantity = poRows[x].lineItem[y].option * sum * poRows[x].lineItem[y].setQty
                    }

                }
            }


        }


        let setQty = ""


        let rate = ""
        let mrp = ""
        let finalRate = ""


        for (let k = 0; k < poRows.length; k++) {
            if (poRows[k].gridOneId == this.props.selectedRowId) {
                rate = poRows[k].rate
                finalRate = poRows[k].finalRate

                mrp = poRows[k].vendorMrp
            }
        }

        for (let x = 0; x < poRows.length; x++) {
            if (poRows[x].gridOneId == this.props.selectedRowId) {
                for (let l = 0; l < poRows[x].lineItem.length; l++) {
                    if (poRows[x].lineItem[l].gridTwoId == data1.sizerId) {
                        setQty = poRows[x].lineItem[l].setQty
                        let qty = Number(setQty) * Number(poRows[x].lineItem[l].option * sum)
                        if (setQty != "") {
                            if (finalRate != 0 || finalRate != "") {
                                if (sum != 0) {
                                    let taxData = {
                                        hsnSacCode: poRows[x].hsnSacCode,
                                        qty: qty,
                                        rate: finalRate,
                                        rowId: poRows[x].lineItem[l].gridTwoId,
                                        designRow: poRows[x].gridOneId,
                                        mrp: mrp,
                                        piDate: new Date(),
                                        supplierGstinStateCode: this.props.stateCode,
                                        purtermMainCode: this.props.termCode,
                                        siteCode: this.props.siteCode
                                    }
                                    if (poRows[x].hsnSacCode != "" || poRows[x].hsnSacCode != null) {
                                        this.props.lineItemRequest(taxData)
                                    } else {
                                        this.setState({
                                            errorMassage: "HSN code is complusory",
                                            poErrorMsg: true
                                        })
                                    }
                                }
                            } else {
                                this.setState({
                                    errorMassage: "Rate is complusory",
                                    poErrorMsg: true

                                })
                            }
                        }
                    }
                }
            }
        }
        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {
                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss
        }, () => {

        }, () => {
            if (setQty != "") {
                this.updateLineItem(data1.sizerId)
            }
            this.props.updatePoRows(poRows)

            document.getElementById(this.state.focusId).focus()
            setTimeout(() => {

                this.props.gridFourth();

                this.props.updatePoRows(poRowss)
            }, 10)



        })

    }
    closePiSizeModal() {
        this.setState({
            sizeModal: false,
            sizeModalAnimatiion: !this.state.sizeModalAnimatiion
        });
        document.getElementById(this.state.focusId).focus()
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let totalBasic = 0
        for (let i = 0; i < poRows[mainIndex].lineItem.length; i++) {
            totalBasic += poRows[mainIndex].lineItem[i]["basic"]
        }
        // poRows[mainIndex].basic = totalBasic
        this.setState({ poRows })
    }
    focusQty(e, id, wholeData) {
        if(wholeData.color.length == 0){
            this.setState({
                poErrorMsg: true,
                errorMassage: "Please fill Color first!"
            })
        }
        else{
            let focusedQty = this.state.focusedQty

            focusedQty.id = id
            focusedQty.qty = e.target.value
            this.setState({
                focusedQty: focusedQty
            })
        }
    }
    closeErrorRequest(e) {

        this.setState({
            poErrorMsg: false,
            catDataErr: false,
            itemDataErr: false,
            setDataErr: false,
        })
        if (document.getElementById(this.state.focusId) != null) {
            document.getElementById(this.state.focusId).focus()
        }
        if (this.state.catDataErr){
            document.getElementById(this.state.catFocusId).focus()
        }
        if (this.state.itemDataErr){
            document.getElementById(this.state.itemFocusId).focus()
        }
        if (this.state.setDataErr){
            document.getElementById(this.state.setFocusId).focus()
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    setQtyupDownArrow(e, id) {

        if (e.key == 'ArrowDown') {
            let idd = Number(id) + 1
            var c = [...this.state.poRows]
            for (let i = 0; i < c.length; i++) {
                if (c[i].gridOneId == this.props.selectedRowId) {
                    if (idd < c[i].lineItem.length) {
                        let nextId = "setQty" + idd
                        document.getElementById(nextId).focus()
                    }
                }
            }

        } else if (e.key == 'ArrowUp') {
            if (id > 0) {
                let idd = Number(id) - 1

                let nextId = "setQty" + idd

                document.getElementById(nextId).focus()


            }
        }
    }

    onBlurSimpleRatio = (idx, qty, total, row, e, targetId, hsnSacCode, designId) =>{
        let taxData = {

            hsnSacCode: hsnSacCode,
            qty: Number(qty) * Number(total),
            rate: finalRate,
            rowId: idx,
            designRow: row,
            mrp: mrp,
            piDate: new Date(),
            supplierGstinStateCode: this.props.stateCode,
            purtermMainCode: this.props.termCode,
            siteCode: this.props.siteCode,
            basic
        }
        this.props.lineItemRequest(taxData)
    }

    onBlurQuantity(idx, qty, total, row, e, targetId, hsnSacCode) {
        let rows;
        if (this.props.codeRadio !== "raisedIndent") {
            rows = _.cloneDeep(this.state.poRows)
            let c = 0
            let rate = ""
            let flag = false
            let quantity = ""
            rows.forEach(ro => {
                if (e.target.validity.valid) {
                    if (ro.gridOneId == row) {
                        ro.lineItem.forEach((pData) => {
                                if (pData.ratio.length != 0) flag = true
                                    console.log("yesss",idx, qty, total, row, e, targetId, hsnSacCode)
                                        pData.quantity = pData.setQty * pData.total
                                        if (pData.setQty != "") {
                                            this.props.updateLineItemChange(false)
                                        }
                        })
                    }
                }
            })


        }
        this.setState({
            poRows: rows,

        }, () => {
            this.props.updatePoRows(rows)
            this.props.gridFourth()
        })

        if (hsnSacCode != "" || hsnSacCode != null) {
            let rate = ""
            let mrp = ""
            let finalRate = "", basic = null
            let poRows = [...this.state.poRows]
            for (var k = 0; k < poRows.length; k++) {
                if (poRows[k].gridOneId == row) {
                    rate = poRows[k].rate
                    finalRate = poRows[k].finalRate
                    mrp = poRows[k].vendorMrp
                    basic = Number(poRows[k].finalRate) * Number(e.target.value)
                }
            }

            if (qty != "") {
                if (finalRate != "" || finalRate != 0) {

                    this.props.updateLineItemChange(false)
                    let taxData = {

                        hsnSacCode: hsnSacCode,
                        qty: Number(qty) * Number(total),
                        rate: finalRate,
                        rowId: idx,
                        designRow: row,
                        mrp: mrp,
                        piDate: new Date(),
                        supplierGstinStateCode: this.props.stateCode,
                        purtermMainCode: this.props.termCode,
                        siteCode: this.props.siteCode,
                        basic
                    }
                    if (e.target.value != "") {

                        this.props.lineItemRequest(taxData)


                    }
                } else {
                    this.setState({
                        errorMassage: "Rate is complusory",
                        poErrorMsg: true

                    })
                }
            }

        } else {

            this.setState({
                errorMassage: "HSN code is complusory",
                poErrorMsg: true

            })
        }
        // }
        //  },10)
        // this.state.codeRadio == "poIcode" ? this.makeListOfIcodes(idx) : null
    }

    handleChange(id, row, e, targetId, wholeData, bool) {
        if(wholeData.color.length == 0){
            this.setState({
                poErrorMsg: true,
                errorMassage: "Please fill Color first!"
            })
        }
        else{
            let idd = id

            let rows = [...this.state.poRows];
            if (this.state.setQty == "") {
                this.setState({ setQty: e.target.dataset.value })
            }
            // else if(this.state.setQty == ""){
            //     this.setState({ setQty: e.target.dataset.value })
            // }
            if (this.props.codeRadio != "poIcode") {
                if (e.target.id == targetId) {
                    if ((Number(e.target.value) <= Number(this.state.setQty)) && this.props.codeRadio === "raisedIndent") {
                        let c = 0
                        let rate = ""
                        let flag = false
                        let quantity = ""
                        rows.forEach(ro => {
                            if (e.target.validity.valid) {
                                if (ro.gridOneId == row) {
                                    ro.lineItem.forEach((pData) => {
                                        if (pData.gridTwoId == idd) {
                                            if (pData.ratio.length != 0) flag = true
    
    
    
                                            if (flag) {
                                                pData.setQty = e.target.value
                                                pData.quantity = e.target.value * pData.total
                                                if (e.target.value != "") {
                                                    this.props.updateLineItemChange(false)
    
                                                }
    
                                            } else {
                                                this.setState({
                                                    poErrorMsg: true,
                                                    errorId: idd,
                                                    errorMassage: "Enter ratio of atleast one from the available sizes"
                                                })
                                            }
                                        }
                                    })
                                }
                            }
                        })
    
    
                    }
                    else if (this.props.codeRadio !== "raisedIndent") {
                        let c = 0
                        let rate = ""
                        let flag = false
                        let quantity = ""
                        rows.forEach(ro => {
                            if (e.target.validity.valid) {
                                if (ro.gridOneId == row) {
                                    ro.lineItem.forEach((pData) => {
                                        if (pData.gridTwoId == idd) {
                                            if (pData.ratio.length != 0) flag = true
    
    
    
                                            if (flag) {
                                                console.log("yesss", id, row, e, targetId, wholeData)
                                                if(bool == "setQty"){
                                                    pData.setQty = e.target.value
                                                    pData.quantity = e.target.value * pData.total
                                                    if (e.target.value != "") {
                                                        this.props.updateLineItemChange(false)
                                                    }
                                                }
                                                else if(bool == "quantity"){
                                                    pData.quantity = e.target.value
                                                    pData.setQty = (e.target.value / pData.total).toFixed(0)
                                                    if (e.target.value != "") {
                                                        this.props.updateLineItemChange(false)
                                                    }
                                                }

    
                                            } else {
                                                this.setState({
                                                    poErrorMsg: true,
                                                    errorId: idd,
                                                    errorMassage: "Enter ratio of atleast one from the available sizes"
                                                })
                                            }
                                        }
                                    })
                                }
                            }
                        })
    
    
                    }
                    this.setState({
                        poRows: rows,
    
                    }, () => {
                        this.props.updatePoRows(rows)
                        this.props.gridFourth()
                    }, () => {
    
                    })
                }
            } else {
                console.log(e.target.id, targetId, e.target.value)
                if (e.target.id == targetId) {
                    rows.forEach(ro => {
                        if (e.target.validity.valid) {
                            if (ro.gridOneId == row) {
                                ro.lineItem.forEach((pData) => {
                                    if (pData.setRatio != "") {
                                        pData.setQty = e.target.value
                                        pData.quantity = e.target.value * pData.total
                                        if (e.target.value != "") {
                                            this.props.updateLineItemChange(false)
    
                                        }
                                    } else {
                                        this.setState({
                                            poErrorMsg: true,
                                            errorId: idd,
                                            errorMassage: "Fill ratio first"
                                        })
                                    }
                                })
    
    
                            }
                        }
                    })
    
                    this.setState({
                        poRows: rows,
    
                    }, () => {
                        this.props.updatePoRows(rows)
                        this.props.gridFourth()
                    }, () => {
    
                    })
                }
    
            }
        }
    }
    handleInput(id, catdesc, e, type) {
        let poRows = this.state.poRows
        this.setState({
            isCatModalOpen: false,
        })
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                    if (poRows[i].catDescArray[j].catDescId == id) {
                        for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                            if (poRows[i].catDescArray[j].catDesc[k].catdesc == catdesc) {
                                poRows[i].catDescArray[j].catDesc[k].value = e.target.value
                                if (poRows[i].catDescArray[j].catDesc[k].isLovPI == 'Y'){
                                    poRows[i].catDescArray[j].catDesc[k].isLovSelect = false
                                }
                            }
                        }

                    }

                }


            }
        }
        console.log('category', poRows)
        this.setState({
            poRows: poRows,
            catdescSearch: e.target.value
        },() => {

            this.props.updatePoRows(poRows)

            setTimeout(() => {
                this.props.gridSecond()

            }, 10)})
        console.log('updateRow', poRows)
    }
    handleInputItem(id, itemUdf, e) {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].itemUdfArray.length; j++) {
                    if (poRows[i].itemUdfArray[j].itemUdfId == id) {
                        for (let k = 0; k < poRows[i].itemUdfArray[j].itemUdf.length; k++) {
                            if (poRows[i].itemUdfArray[j].itemUdf[k].cat_desc_udf == itemUdf) {
                                poRows[i].itemUdfArray[j].itemUdf[k].value = e.target.value
                                if(poRows[i].itemUdfArray[j].itemUdf[k].isLovPI == 'Y'){
                                    poRows[i].itemUdfArray[j].itemUdf[k].isItemLovSelect = false
                                }

                            }
                        }

                    }

                }


            }
        }
        this.setState({
            poRows: poRows,
            itemUdfSearch: e.target.value
        }, () => this.props.updatePoRows(poRows))


    }
    handleInputUdf(id, setNo, setUdf, e) {
        let poRows = this.state.poRows
        console.log('udf', id, setNo, setUdf, e.target.value)
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == id) {
                        for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                            console.log('udfL', k, e.target.value)
                            if (poRows[i].lineItem[j].gridTwoId == id && poRows[i].lineItem[j].setNo == setNo && poRows[i].lineItem[j].setUdfArray[k].udfType == setUdf) {
                                console.log('udfinner', poRows[i].lineItem[j].gridTwoId, id, poRows[i].lineItem[j].setNo, setNo, poRows[i].lineItem[j].setUdfArray[k].udfType, setUdf, e.target.value)
                                poRows[i].lineItem[j].setUdfArray[k].value = e.target.value
                                poRows[i].lineItem[j].setUdfArray[k].isSetLovSelect = false

                            }
                        }

                    } else {

                    }

                }


            }
        }
        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss,
            setUdfSearch: e.target.value
        }, () => this.props.updatePoRows(poRows))


    }
    copingFocus(gridTwoId, id, color, icolorList, option, total, colorSearch, sizeType) {
        let data = { gridTwoId: gridTwoId, id: id, color: color, icolorList: icolorList, option: option, total: total, search: colorSearch, type: sizeType }
        //console.log('copy color', data)
        this.props.copingFocus(data)
    }

    closeSetModal() {
        this.childEsc()
        this.props.closeSetModal()
    }
    handleColor(id, e) {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == id) {
                        poRows[i].lineItem[j].colorSearch = e.target.value
                    }
                }
            }

        }

        this.setState({
            poRows: poRows,
            colorSearch: e.target.value
        })


    }
    handleSize(id, e) {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == id) {
                        poRows[i].lineItem[j].sizeSearch = e.target.value
                    }
                }
            }

        }
        this.setState({
            poRows: poRows,
            sizeSearch: e.target.value
        })


    }

    openImageModal(idOne, idTwo, image) {
        // if (articleCode == "" || articleCode == undefined) {
        //     this.setState({
        //         errorMassage: "Article code is mandatory",
        //         poErrorMsg: true,
        //         focusId: "articleCode" + idx

        //     })

        // }
        // else {
        //     let rows = [...this.state.poRows]

        //     for (var i = 0; i < rows.length; i++) {
        //         if (rows[i].gridOneId == id) {

        //             this.setState({
        //                 imageState: rows[i].imageUrl
        //             })
        //         }
        //     }
        //     this.setState({
        //         imageRowId: id,
        //         imageModal: true,
        //         imageModalAnimation: !this.state.imageModalAnimation,
        //         focusId: image
        //     });
        // }
        let c = [...this.state.poRows]

        // for (var i = 0; i < rows.length; i++) {
        //     if (rows[i].gridOneId == idOne) {

        //         this.setState({
        //             imageState: rows[i].imageUrl
        //         })
        //     }
        // }
        // this.props.viewImagesRequest({ filePath: "s3://vmart-supplymint-develop-com/DATA/IMAGE/PROC_PI/ARTICLE/fed6c2a2-593b-4297-a0a5-abbb8463b45a", gridTwoId: idTwo });
        for (let i = 0; i < c.length; i++) {
            if (c[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < c[i].lineItem.length; j++) {
                    if (c[i].lineItem[j].gridTwoId == idTwo) {
                        let data = {
                            path: c[i].lineItem[j].filePath
                        }
                        this.props.piImageUrlRequest(data)
                        this.setState({
                            imageState: c[i].lineItem[j].imageUrl,
                            imagePth: c[i].lineItem[j].filePath == "null" ? "" : c[i].lineItem[j].filePath
                        })
                    }
                }
            }
        }
        this.setState({
            imageRowId: idTwo,
            imageModal: true,
            imageModalAnimation: !this.state.imageModalAnimation,
            focusId: image
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }

    closePiImageModal() {
        document.getElementById(this.state.focusId).focus()
        this.setState({
            imageModal: false,
            imageModalAnimation: !this.state.imageModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }


    updateImage(data) {

        let c = [...this.state.poRows];
        // for (var i = 0; i < c.length; i++) {
        //     if (c[i].gridOneId.toString() == data.imageRowId) {
        //         c[i].imageUrl = data.file
        //         c[i].image = Object.keys(data.file)
        //         c[i].containsImage = Object.keys(data.file).length != 0 ? true : false
        //     }

        // }
        for (let i = 0; i < c.length; i++) {
            if (c[i].gridOneId == this.props.selectedRowId) {
                for (let j = 0; j < c[i].lineItem.length; j++) {
                    if (c[i].lineItem[j].gridTwoId == this.state.imageRowId) {
                        c[i].lineItem[j].imageUrl = data.file
                        c[i].lineItem[j].image = Object.keys(data.file)
                        c[i].lineItem[j].containsImage = Object.keys(data.file).length != 0 ? true : false
                        c[i].lineItem[j].imagePath = data.imagePath
                        c[i].lineItem[j].filePath = data.imagePath
                    }
                }
            }
        }

        this.setState({
            poRows: c,

        })
        document.getElementById(this.state.focusId).focus()

    }

    simpleCompleToggle(id, mainId) {
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == mainId));
        let index = poRows[mainIndex].lineItem.findIndex((obj => obj.gridTwoId == id));

        poRows[mainIndex].lineItem[index].sizeType = poRows[mainIndex].lineItem[index].sizeType == "complex" ? "simple" : "complex"
        poRows[mainIndex].lineItem[index].sizeList = []
        poRows[mainIndex].lineItem[index].sizes = []
        poRows[mainIndex].lineItem[index].sizeSearch = ""
        poRows[mainIndex].lineItem[index].ratio = []
        poRows[mainIndex].lineItem[index].total = ""
        poRows[mainIndex].lineItem[index].setQty = ""
        poRows[mainIndex].lineItem[index].colorSearch = ""
        poRows[mainIndex].lineItem[index].color = [""]
        poRows[mainIndex].lineItem[index].colorList = []
        poRows[mainIndex].lineItem[index].amount = ""
        poRows[mainIndex].lineItem[index].tax = ""
        poRows[mainIndex].lineItem[index].quantity = ""
        poRows[mainIndex].lineItem[index].image = []
        poRows[mainIndex].lineItem[index].imageUrl = {}
        poRows[mainIndex].lineItem[index].imagePath = "",
            poRows[mainIndex].lineItem[index].basic = "",
            poRows[mainIndex].lineItem[index].ppQty = "",
            poRows[mainIndex].lineItem[index].containsImage = false,
            poRows[mainIndex].lineItem[index].option = ""
        this.setState({
            poRows: poRows
        })
    }

    updateSimpleSize = (data1, data2) => {
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let index = poRows[mainIndex].lineItem.findIndex((obj => obj.gridTwoId == this.state.sizeRow));
        let currLineItem = { ...poRows[mainIndex].lineItem[index] }
        var details = new Array();
        let multipleItemArray = [], sizeData = {};
        // if (this.state.splitRow) {
        poRows[mainIndex].lineItem.splice(index, 1)
        // this.setState({ splitRow: false })
        // }
        for (let i = 0; i < data2.length; i++) {
            sizeData = [{
                id: data2[i].id,
                code: data2[i].code,
                cname: data2[i].cname
            }]
            details[i] = (currLineItem)
            console.log('gridTwoid', details[i]["gridTwoId"])
            details[i] = { ...details[i], ppQty: this.props.simpleData, gridTwoId: details[i]["gridTwoId"] + i.toString(), sizeList: sizeData, sizes: [data2[i].cname], sizeSearch: data2[i].cname, ratio: [data2[i].ratio], total: 6, setQty: Math.round(Number(data2[i].ratio) / 6), quantity: Math.round(Number(data2[i].ratio) / 6) * 6, setNo: poRows[mainIndex].lineItem.length + i + 1, basic: (poRows[mainIndex].finalRate * (Math.round(Number(data2[i].ratio) / 6) * 6)) }
            multipleItemArray[i] = {
                ...multipleItemArray[i],
                hsnSacCode: poRows[mainIndex].hsnSacCode,
                qty: Math.round(Number(data2[i].ratio) / 6) * 6,
                rate: poRows[mainIndex].finalRate,
                rowId: details[i]["gridTwoId"],
                designRowid: this.props.selectedRowId,
                mrp: poRows[mainIndex].mrp,
                basic: (poRows[mainIndex].finalRate) * (Math.round(Number(data2[i].ratio) / 6) * 6), //basic
                clientGstIn: sessionStorage.getItem('gstin'),
                piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
                supplierGstinStateCode: this.props.stateCode,
                purtermMainCode: this.props.termCode,
                siteCode: this.props.siteCode
            }
            sizeData = {}
        }
        this.props.multipleLineItemRequest(multipleItemArray)
        Array.prototype.push.apply(poRows[mainIndex].lineItem, details)
        this.setState({
            poRows,
            ratioData: data2,
            multipleItemArray
        })
        this.props.updatePo()
    }



    // updateSimpleSize = (data1, data2) => {
    //     let poRows = [...this.state.poRows];
    //     let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
    //     let index = poRows[mainIndex].lineItem.findIndex((obj => obj.gridTwoId == this.state.sizeRow));
    //     let currLineItem = { ...poRows[mainIndex].lineItem[index] }
    //     var details = new Array();
    //     let multipleItemArray = [], sizeData = {};
    //     // if (this.state.splitRow) {
    //     poRows[mainIndex].lineItem.splice(index, 1)
    //     // this.setState({ splitRow: false })
    //     // }
    //     for (let i = 0; i < data2.length; i++) {
    //         sizeData = [{
    //             id: i + "1",
    //             code: data2[i].code,
    //             cname: data2[i].cname
    //         }]
    //         details[i] = (currLineItem)
    //         console.log('gridTwoid', details[i]["gridTwoId"])
    //         details[i] = { ...details[i], ppQty: this.props.simpleData, gridTwoId: details[i]["gridTwoId"] + i.toString(), sizeList: sizeData, sizes: [data2[i].cname], sizeSearch: data2[i].cname, ratio: [data2[i].ratio], total: 6, setQty: Math.round(Number(data2[i].ratio) / 6), quantity: Math.round(Number(data2[i].ratio) / 6) * 6, setNo: poRows[mainIndex].lineItem.length + i + 1, basic: (poRows[mainIndex].finalRate * (Math.round(Number(data2[i].ratio) / 6) * 6)) }

    //         multipleItemArray[i] = {
    //             ...multipleItemArray[i],
    //             hsnSacCode: poRows[mainIndex].hsnSacCode,
    //             qty: Math.round(Number(data2[i].ratio) / 6) * 6,
    //             rate: poRows[mainIndex].finalRate,
    //             rowId: details[i]["gridTwoId"],
    //             designRowid: this.props.selectedRowId,
    //             mrp: poRows[mainIndex].mrp,
    //             basic: (poRows[mainIndex].finalRate) * (Math.round(Number(data2[i].ratio) / 6) * 6), //basic 
    //             clientGstIn: sessionStorage.getItem('gstin'),
    //             piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
    //             supplierGstinStateCode: this.props.stateCode,
    //             purtermMainCode: this.props.termCode,
    //             siteCode: this.props.siteCode
    //         }
    //         sizeData = {}
    //     }

    //     this.props.multipleLineItemRequest(multipleItemArray)
    //     Array.prototype.push.apply(poRows[mainIndex].lineItem, details)
    //     this.setState({
    //         poRows,
    //         ratioData: data2,
    //         multipleItemArray
    //     })
    //     this.props.updatePo()
    // }
    updateSizeDetails = (value, ratioId) => {
        console.log(value, ratioId)
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let index = poRows[mainIndex].lineItem.findIndex((obj => obj.gridTwoId == this.state.sizeRow));
        let currLineItem = { ...poRows[mainIndex].lineItem[index] }
        if (ratioId == currLineItem.sizeList.code) {
            poRows[mainIndex].lineItem[index].ratio = [value]
        }
        this.setState({ poRows })
    }
    spliceRow = () => {
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let index = poRows[mainIndex].lineItem.findIndex((obj => obj.gridTwoId == this.state.sizeRow));
        poRows[mainIndex].lineItem.splice(index, 1)
        this.setState({ poRows })
    }

    updateSetRatio = (size, index, ratio, sizeList, sizeRow, ppQty, sizeType, setQty, option) => {
        let poRows = [...this.state.poRows]
        let mainIndex = this.props.selectedRowId -1;
        if(sizeType == "complex"){
            poRows[mainIndex].lineItem[index].total = option * parseInt(ratio.join())
        } else {
            poRows[mainIndex].lineItem[index].ppQty = 6
        }
        this.setState({
            poRows: poRows
        }, () => {
            if(setQty != '' || sizeType == "simple") {
                let finalPasteData = {
                    catFive: size,
                    cat5Ratio: ratio,
                    sizerId: sizeRow,
                    predefinedSetAvailable: '',
                    isToggled: true,
                }
                this.updateSizeState(finalPasteData);
                this.updateSizeList(sizeList);
            }
        })
        
    }

    updateNonSetRatio = (size, index, ratio, sizeList, sizeRow) => {
        let finalPasteData = {
            catFive: size,
            cat5Ratio: ratio,
            sizerId: sizeRow,
            predefinedSetAvailable: '',
            isToggled: true,
        }
        console.log('sizeId', finalPasteData, sizeList)
        this.updateSizeState(finalPasteData);
        this.updateSizeList(sizeList);
    }

    updateSingleRatio = (size, index, ratio) => {
        console.log(this.state.poRows, size, index, ratio);
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));        
        let arr = []
        let payload = {
            hsnSacCode: poRows[mainIndex].hsnSacCode,
            qty: Math.round(Number(ratio) / 6) * 6,
            rate: poRows[mainIndex].finalRate,
            rowId: poRows[mainIndex].lineItem[index]["gridTwoId"],
            designRowid: this.props.selectedRowId,
            mrp: poRows[mainIndex].mrp,
            basic: (poRows[mainIndex].finalRate) * (Math.round(Number(ratio) / 6) * 6),
            clientGstIn: sessionStorage.getItem('gstin'),
            piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
            supplierGstinStateCode: this.props.stateCode,
            purtermMainCode: this.props.termCode,
            siteCode: this.props.siteCode
        }        
        poRows[mainIndex].lineItem[index] = {...poRows[mainIndex].lineItem[index] , setQty: Math.round(Number(ratio) / 6), quantity: Math.round(Number(ratio) / 6)*6, basic: (poRows[mainIndex].finalRate) * (Math.round(Number(ratio) / 6) * 6), ppQty: 6 }
        arr.push(payload)
        this.props.multipleLineItemRequest(arr)
        console.log(poRows)
        this.setState({
            poRows
        })
        this.props.updatePo()
    }

    updateSingleAmount = (size) => {
        console.log('size', size)
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let index = poRows[mainIndex].lineItem.findIndex((obj => obj.sizeSearch == size));
        let arr = []
        let payload = {
            hsnSacCode: poRows[mainIndex].hsnSacCode,
            qty: Math.round(Number(poRows[mainIndex].lineItem[index].ratio) / 6) * 6,
            rate: poRows[mainIndex].lineItem[index].ratio[0],
            rowId: poRows[mainIndex].lineItem[index]["gridTwoId"],
            designRowid: this.props.selectedRowId,
            mrp: poRows[mainIndex].mrp,
            basic: (poRows[mainIndex].finalRate) * (Math.round(Number(poRows[mainIndex].lineItem[index].ratio) / 6) * 6),
            clientGstIn: sessionStorage.getItem('gstin'),
            piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
            supplierGstinStateCode: this.props.stateCode,
            purtermMainCode: this.props.termCode,
            siteCode: this.props.siteCode
        }
        // poRows[mainIndex].lineItem[index] = {...poRows[mainIndex].lineItem[index] , }
        arr.push(payload)
        this.props.multipleLineItemRequest(arr)
        this.props.updatePo()
    }
    handleSetRationBlur(gridTwoId, qty, c, d, event) {
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let data = {
            hsnSacCode: poRows[mainIndex].hsnSacCode,
            qty: qty,
            rate: poRows[mainIndex].finalRate,
            rowId: gridTwoId,
            designRow: poRows[mainIndex].gridOneId,
            mrp: poRows[mainIndex].mrp,
            piDate: new Date(),
            supplierGstinStateCode: this.props.stateCode,
            purtermMainCode: this.props.termCode,
            siteCode: this.props.siteCode
        }
        this.props.lineItemRequest(data)

    }

    render() {
        console.log("poRowsssss", this.state.poRows, this.state.focusId)
        // console.log('catRemark', this.state.isMandateCatRemark, this.state.isDisplayCatRemark)
        const { searchMrp } = this.state;
        return (
            <div className={this.props.setModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.setModalAnimation ? "backdrop display_block modal-backdrop-new" : "display_none"}></div>

                <div id="itemSetModal" className={this.props.setModalAnimation ? "set-modal-display" : "display_none"}>
                    <div className={this.props.setModalAnimation ? "modal-content set-modal modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="set-modal-inner">
                                <div className="set-modal-top">
                                    <h3>Item Details</h3>
                                    <button type="button" id="closeButton" className="closeButtonIcon float_Right" data-dismiss="modal" onClick={(e) => this.closeSetModal(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15.307" height="15.307" viewBox="0 0 15.307 15.307">
                                            <path d="M15.307 13.446l-5.861-5.8 5.8-5.851L13.446 0l-5.8 5.854L1.792.058 0 1.846l5.859 5.811-5.8 5.857 1.787 1.793 5.816-5.861 5.855 5.8z" />
                                        </svg>
                                    </button>
                                </div>
                                <div className="set-modal-body">
                                    <div className="set-modal-tab">
                                        <ul className="nav nav-tabs set-modal-tab-list" role="tablist">
                                            {this.props.isCatDescExist && <li className="nav-item active" >
                                                <a className="nav-link smt-btn" href="#setmodalcatdesc" role="tab" data-toggle="tab" onClick={(e) => this.childEsc(e)}>CAT / DESC</a>
                                            </li>}
                                            {this.props.itemUdfExist == "true" ?
                                                <li className={this.props.isCatDescExist ? "nav-item" : "nav-item active"} onClick={(e) => this.childEsc(e)}>
                                                    <a className="nav-link smt-btn" href="#setmodalitemmudf" role="tab" data-toggle="tab">ITEM UDF</a>
                                                </li>
                                                : null}
                                            <li className={(this.props.isCatDescExist == false && this.props.itemUdfExist == "false") ? "nav-item active" : "nav-item"} onClick={(e) => this.childEsc(e)}>
                                                <a className="nav-link smt-btn" href="#setmodallineitem" role="tab" data-toggle="tab">LINE ITEM</a>
                                            </li>
                                            {this.props.isUDFExist == "true" ? <li className="nav-item" onClick={(e) => this.childEsc(e)}>
                                                <a className="nav-link smt-btn" href="#setmodalsetudf" role="tab" data-toggle="tab">SET UDF</a>
                                            </li> : null}
                                        </ul>
                                    </div>
                                    <div className="tab-content set-modal-tab-content">
                                        {this.props.isCatDescExist && <div className="tab-pane fade in active" id="setmodalcatdesc" role="tabpanel">
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-20 catdesk-table">
                                                <div className="manage-cat-desk-table">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>

                                                                {this.state.poRows.map((item, key) => (

                                                                    item.gridOneId == this.props.selectedRowId ?
                                                                        item.catDescHeader ? item.catDescHeader.length != 0 ? item.catDescHeader.map((data, key) => (

                                                                            ((data.catdesc == "CAT1" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "CAT2" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "CAT3" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "CAT4" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "CAT5" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "CAT6" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "DESC1" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "DESC2" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "DESC3" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "DESC4" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "DESC5" && (data.isDisplayPI == "Y"))
                                                                                || (data.catdesc == "DESC6" && (data.isDisplayPI == "Y")) ?
                                                                                <th key={key}>

                                                                                    <label>
                                                                                        {data.displayName != "" ? data.displayName : data.catdesc}{data.isCompulsoryPI == "Y" ? <span className="mandatory">*</span> : null}
                                                                                        {/* {data.displayName!=null? data.catdesc:data.displayName} */}
                                                                                    </label>
                                                                                </th> : null))) : <th><label>No Header</label></th> : null : null))}
                                                                                {this.props.isDisplayCatRemark && 
                                                                                <th>
                                                                                    <label>
                                                                                        {this.props.catRemarkLabel}
                                                                                        {this.props.isMandateCatRemark && <span className="mandatory">*</span>}
                                                                                    </label>
                                                                                </th>}

                                                            </tr>
                                                        </thead>

                                                        <tbody>

                                                            {this.state.poRows.map((item, key) => (
                                                                item.gridOneId == this.props.selectedRowId ?
                                                                    item.catDescArray ? item.catDescArray.length > 0 ? item.catDescArray.map((itemm, iddd) => (
                                                                        <tr id={iddd} key={iddd} >
                                                                            {itemm.catDesc.map((items, itemkey) => (

                                                                                ((items.catdesc == "CAT1" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "CAT2" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "CAT3" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "CAT4" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "CAT5" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "CAT6" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "DESC1" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "DESC2" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "DESC3" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "DESC4" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "DESC5" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "DESC6" && (items.isDisplayPI == "Y"))
                                                                                    || (items.catdesc == "CATREMARK")) ?
                                                                                    (items.catdesc != "CATREMARK" && items.isLovPI == 'Y') && <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputText" key={itemkey}  >
                                                                                        <div className="pos-rel">
                                                                                            {!this.props.isModalShow ?
                                                                                                <input autoComplete="off" type="text" disabled={this.props.codeRadio === "poIcode"} onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.handleInput(itemm.catDescId, items.catdesc, e, "catdesc") : null} name="" className="inputTable" id={items.catdesc + itemkey} onKeyDown={(e) => this._handleKeyPressRow(e, `${items.catdesc}`, itemkey, "catdesc")} value={items.value} onBlur={items.isValidationPi == 'Y' ? (e) => this.catDropValidation(`${items.catdesc}`, `${itemm.catDescId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + itemkey}`, items.code, itemkey, e) : null}/>

                                                                                                : <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={items.catdesc + itemkey} onKeyDown={(e) => this._handleKeyPressRow(e, `${items.catdesc}`, itemkey, "catdesc")} value={items.value} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.openItemModal(`${items.catdesc}`, `${itemm.catDescId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + itemkey}`, items.code, itemkey) : null} />}
                                                                                            {/* {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? <div className="purchaseTableDiv" id={"categories" + items.catdesc + itemkey} onClick={(e) => this.openItemModal(`${items.catdesc}`, `${itemm.catDescId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + itemkey}`, items.code, itemkey)} >
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                                    <g fill="none" fillRule="evenodd">
                                                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                                    </g>
                                                                                                </svg>
                                                                                            </div> : null} */}
                                                                                            {this.props.codeRadio !== "poIcode" && (this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode") ? <div className="modal-search-btn-table" id={"categories" + items.catdesc + itemkey} onClick={(e) => this.openItemModal(`${items.catdesc}`, `${itemm.catDescId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + itemkey}`, items.code, itemkey)}>
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                                                    <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                                                </svg>
                                                                                            </div> : null}

                                                                                        </div>
                                                                                    </td> 
                                                                                    || (items.catdesc == "CATREMARK" || items.isLovPI == "N") && <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputText" key={itemkey}  >
                                                                                        <div className="pos-rel">
                                                                                            {!this.props.isModalShow ?
                                                                                                <input autoComplete="off" type="text" disabled={this.props.codeRadio === "poIcode"} onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.handleInput(itemm.catDescId, items.catdesc, e, "catdesc") : null} name="" className="inputTable" id={items.catdesc + itemkey} value={items.value}/>

                                                                                                : <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={items.catdesc + itemkey} onKeyDown={(e) => this._handleKeyPressRow(e, `${items.catdesc}`, itemkey, "catdesc")} value={items.value} />}                                                                                         
                                                                                            

                                                                                        </div>
                                                                                    </td>
                                                                                    : null)
                                                                                    // (items.catdesc == "CATREMARK" ? <input autoComplete="off" type="text" disabled={this.props.codeRadio === "poIcode"} onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.handleInput(itemm.catDescId, items.catdesc, e, "catdesc") : null} name="" className="inputTable" id={items.catdesc + itemkey} onKeyDown={(e) => this._handleKeyPressRow(e, `${items.catdesc}`, itemkey, "catdesc")} value={items.value} /> : null)
                                                                                    )
                                                                                    }

                                                                        </tr>)) : null : null : null))}


                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>


                                        </div>}
                                        <div className={(this.props.isCatDescExist == false && this.props.itemUdfExist == "true") ? "tab-pane fade new-generic-table in active" : "tab-pane fade new-generic-table"} id="setmodalitemmudf" role="tabpanel">
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper pnt-bxsha">
                                                <div className="zui-scroller pad-0 m0 max-width-set" >
                                                    <table className="table scrollTable zui-table border-bot-table pit-new-design disTable" >
                                                        <thead>
                                                            <tr>

                                                                {this.state.poRows.map((item, key) => (

                                                                    item.gridOneId == this.props.selectedRowId ?
                                                                        item.itemUdfHeader ? item.itemUdfHeader.length > 0 ? item.itemUdfHeader.sort((a, b) => a.orderBy - b.orderBy).map((data, keyyyy) => (

                                                                            ((data.cat_desc_udf == "UDFSTRING01" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING02" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING03" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING04" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING05" && (data.isDisplayPI == "Y")) ||
                                                                                (data.cat_desc_udf == "UDFSTRING06" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING07" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING08" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING09" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFSTRING10" && (data.isDisplayPI == "Y")) ||
                                                                                (data.cat_desc_udf == "UDFNUM01" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFNUM02" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFNUM03" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFNUM04" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFNUM05" && (data.isDisplayPI == "Y")) ||
                                                                                (data.cat_desc_udf == "UDFDATE01" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFDATE02" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFDATE03" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFDATE04" && (data.isDisplayPI == "Y")) || (data.cat_desc_udf == "UDFDATE05" && (data.isDisplayPI == "Y"))) ? <th id={keyyyy} key={keyyyy} >
                                                                                    <label>  {data.displayName != null ? data.displayName : this.Capitalize(data.cat_desc_udf.toLowerCase())}{data.isCompulsoryPI == "Y" ? <span className="mandatory">*</span> : null} </label>
                                                                                </th> : null
                                                                        )) : null : null : null))}

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.poRows.map((item, key) => (
                                                                item.gridOneId == this.props.selectedRowId ?
                                                                    item.itemUdfArray.map((data, iukey) => (
                                                                        <tr id={iukey} key={iukey} >
                                                                            {data.itemUdf != undefined ? data.itemUdf.sort((a, b) => a.orderBy - b.orderBy).map((dataa, keyy) => (
                                                                                ((dataa.cat_desc_udf == "UDFSTRING01" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING02" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING03" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING04" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING05" && (dataa.isDisplayPI == "Y")) ||
                                                                                    (dataa.cat_desc_udf == "UDFSTRING06" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING07" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING08" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING09" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFSTRING10" && (dataa.isDisplayPI == "Y")) ||
                                                                                    (dataa.cat_desc_udf == "UDFNUM01" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFNUM02" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFNUM03" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFNUM04" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFNUM05" && (dataa.isDisplayPI == "Y")) ||
                                                                                    (dataa.cat_desc_udf == "UDFDATE01" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFDATE02" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFDATE03" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFDATE04" && (dataa.isDisplayPI == "Y")) || (dataa.cat_desc_udf == "UDFDATE05" && (dataa.isDisplayPI == "Y"))) ?
                                                                                    dataa.isLovPI == 'Y' && <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputText" key={keyy} >
                                                                                        <div className="pos-rel">
                                                                                            {!this.props.isModalShow ?
                                                                                                <input autoComplete="off" type="text" onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.handleInputItem(data.itemUdfId, dataa.cat_desc_udf, e, "itemudf") : null} name="" className="inputTable" id={dataa.cat_desc_udf + keyy} onKeyDown={(e) => this._handleKeyPressRow(e, `${dataa.cat_desc_udf}`, keyy, "itemUdf")} value={dataa.value} onBlur={dataa.isValidationPi == 'Y' ? (e) => this.itemDropdownValidation(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfId}`, `${dataa.value}`, `${dataa.cat_desc_udf + keyy}`, e) : null} />

                                                                                                : <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={dataa.cat_desc_udf + keyy} onKeyDown={(e) => this._handleKeyPressRow(e, `${dataa.cat_desc_udf}`, keyy, "itemUdf")} value={dataa.value} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfId}`, `${dataa.value}`, `${dataa.cat_desc_udf + keyy}`) : null} />}
                                                                                            {/* {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? <div className="purchaseTableDiv" id={"itemUdf" + dataa.cat_desc_udf + keyy} onClick={(e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfId}`, `${dataa.value}`, `${dataa.cat_desc_udf + keyy}`)}>
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                                    <g fill="none" fillRule="evenodd">
                                                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                                    </g>
                                                                                                </svg>
                                                                                            </div> : null} */}
                                                                                            {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? <div className="modal-search-btn-table" id={"itemUdf" + dataa.cat_desc_udf + keyy} onClick={(e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfId}`, `${dataa.value}`, `${dataa.cat_desc_udf + keyy}`)}>
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                                                    <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                                                </svg>
                                                                                            </div> : null}
                                                                                            {/*{!this.state.isModalShow ? this.state.itemUdfMappingModal ? this.state.searchIU == "itemUdf" + dataa.cat_desc_udf + keyy ? <div className="calculatedToolTip totalAmtDescDrop set-pi-modal-position" ><ItemUdfMappingModal {...this.props} {...this.state} department={this.state.department} itemArticleCode={this.state.itemArticleCode} itemUdfId={this.state.itemUdfId} vendorMrpPo={this.props.articleCode} itemUdfName={this.state.itemUdfName} itemUdfType={this.state.itemUdfType} itemUdfUpdate={(e) => this.itemUdfUpdate(e)} {...this.props} {...this.state} articleName={this.state.articleName} updateItemValue={(e) => this.updateItemValue(e)} itemUdfMappingAnimation={this.state.itemUdfMappingAnimation} itemValue={this.state.itemValue} closeItemUdfModal={(e) => this.closeItemUdfModal()} openItemUdfModal={(e) => this.openItemUdfModal(e)} /></div> : null : null : null}*/}
                                                                                        </div>
                                                                                    </td>
                                                                                    || dataa.isLovPI == 'N' && <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputText" key={keyy} >
                                                                                    <div className="pos-rel">
                                                                                        {!this.props.isModalShow ?
                                                                                            <input autoComplete="off" type="text" onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "poIcode" ? (e) => this.handleInputItem(data.itemUdfId, dataa.cat_desc_udf, e, "itemudf") : null} name="" className="inputTable" id={dataa.cat_desc_udf + keyy} value={dataa.value} />

                                                                                            : <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={dataa.cat_desc_udf + keyy} value={dataa.value} />}

                                                                                        </div>
                                                                                </td>: null
                                                                            )) : null}
                                                                        </tr>
                                                                    )) : null))}
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div className={(this.props.isCatDescExist == false && this.props.itemUdfExist == "false") ? "tab-pane fade new-generic-table in active" : "tab-pane fade new-generic-table"} id="setmodallineitem" role="tabpanel">
                                            <div className={this.props.copyColor == "true" ? "col-md-12 c ol-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionTwoTable sectionthreeTable pnt-bxsha" : "col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionTwoTable sectionthreeTable tableWid pnt-bxsha jcBrother"}>
                                                <div className={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" ? "table-scroll zui-scroller pad-0 marginLeft_190" : "table-scroll zui-scroller pad-0 m0 max-width-set"}>
                                                    <table className="table scrollTable zui-table border-bot-table pit-new-design disTable">
                                                        <thead>
                                                            <tr>
                                                                {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "setBased" ? <th className="fixed-side alignMiddle pnt-fixed width190" >
                                                                    <ul className="list-inline">
                                                                        <li className="width70">
                                                                            <label className="width-45 lableFixed">Action</label>
                                                                        </li>

                                                                    </ul></th> : null}

                                                                {this.props.codeRadio == "poIcode" ? <th>
                                                                    <label>ICODE<span className="mandatory">*</span></label>
                                                                </th> : null}
                                                                {this.props.isColorRequired ? <th>
                                                                    <label>Color<span className="mandatory">*</span></label>
                                                                </th> : null}
                                                                <th>
                                                                    <label> Image{this.props.isMandateImage && <span className="mandatory">*</span>}</label>
                                                                </th>
                                                                {this.props.isColorRequired ? <th className="set-tdWidth">
                                                                    <label> No. of Color</label>
                                                                </th> : null}
                                                                {this.props.isSet ? <th className="set-tdWidth">
                                                                    <label>Set Number</label>
                                                                </th> : <th>
                                                                        <label>Serial Number</label>
                                                                    </th>}

                                                                {this.props.codeRadio == "poIcode" ? <th>
                                                                    <label>Size<span className="mandatory">*</span></label>
                                                                </th> : null}
                                                                {this.props.isSet ? this.props.codeRadio == "poIcode" ? <th className="set-tdWidth">
                                                                    <label>Set Ratio<span className="mandatory">*</span></label>
                                                                </th> : null : null}
                                                                {this.props.codeRadio != "poIcode" ? <th>
                                                                    <label>  Size <span className="mandatory">*</span></label>
                                                                </th> : null}
                                                                {this.props.isSet ? this.props.codeRadio != "poIcode" ? <th className="set-tdWidth">
                                                                    <label>  Ratio/Qty</label>
                                                                </th> : null : this.props.codeRadio != "poIcode" ? <th>
                                                                    <label>  Size Quantity</label>
                                                                </th> : null}
                                                                {this.props.isSet ? <th>
                                                                    <label>PP Qty</label>
                                                                </th> : null}
                                                                {this.props.isSet ? <th className="set-tdWidth">
                                                                    <label>Set Qty<span className="mandatory">*</span></label>
                                                                </th> : null
                                                                }
                                                                <th className="set-tdWidth">
                                                                    {this.props.isSet ? <label>Net Quantity</label> : <label>  Quantity</label>}
                                                                </th>
                                                                {this.props.isBaseAmountActive ? <th className="set-tdWidth">
                                                                    <label>  Basic</label>
                                                                </th> : null}
                                                                <th className="set-tdWidth"><label>Net Amount Total</label></th>
                                                                {!this.props.isTaxDisable ? <th className="set-tdWidth"><label>TAX</label></th> : null}
                                                                {this.props.isRspRequired ? <th className="set-tdWidth"><label>Calculated Margin</label></th> : null}
                                                                {!this.props.isGSTDisable ? <th className="set-tdWidth"><label>GST</label></th> : null}
                                                                {this.props.isRspRequired ? <th className="set-tdWidth"><label>Mark Up/Down</label></th> : null}

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.poRows.map((item, keyy) => (

                                                                item.gridOneId == this.props.selectedRowId ?
                                                                    item.lineItem.map((itemGrid, key) => {
                                                                        //console.log('itemgrid', itemGrid)
                                                                        return <tr key={key}>
                                                                            {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" || this.props.codeRadio == "setBased" ? <td className="fixed-side alignMiddle tdFocus pnt-fixed width190" >
                                                                                <ul className="list-inline">
                                                                                    <li className={this.props.copyColor == "true" ? "text-center pnt-f-icon pnt-bg-yellow" : "text-center pnt-f-icon pnt-bg-yellow"}>
                                                                                        {/* <img src={copyIcon} onClick={(e) => this.copySet(itemGrid.gridTwoId, item.gridOneId, itemGrid.setNo)} /> */}
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="17" viewBox="0 0 21 21" onClick={(e) => this.copySet(itemGrid.gridTwoId, item.gridOneId, itemGrid.setNo, itemGrid)}>
                                                                                            <g fill="#a4b9dd" fill-rule="nonzero">
                                                                                                <path d="M17.705 4.66H15.84V2.794A2.795 2.795 0 0 0 13.045 0H2.795A2.795 2.795 0 0 0 0 2.795v10.25a2.795 2.795 0 0 0 2.795 2.796H4.66v1.864A2.795 2.795 0 0 0 7.455 20.5h10.25a2.795 2.795 0 0 0 2.795-2.795V7.455a2.795 2.795 0 0 0-2.795-2.796zM1.864 13.044V2.795c0-.514.417-.931.931-.931h10.25c.515 0 .932.417.932.931V4.66H7.455a2.795 2.795 0 0 0-2.796 2.796v6.522H2.795a.932.932 0 0 1-.931-.932zm16.772 4.66a.932.932 0 0 1-.931.931H7.455a.932.932 0 0 1-.932-.931V7.455c0-.515.417-.932.932-.932h10.25c.514 0 .931.417.931.932v10.25z" />
                                                                                                <path d="M15.375 11.648h-1.864V9.784a.932.932 0 1 0-1.863 0v1.864H9.784a.932.932 0 1 0 0 1.863h1.864v1.864a.932.932 0 1 0 1.863 0v-1.864h1.864a.932.932 0 1 0 0-1.863z" />
                                                                                            </g>
                                                                                        </svg>
                                                                                        <span className="generic-tooltip">Copy</span>
                                                                                        {/* { itemGrid.lineItemChk?  <img src={ErrorIcon}/>:false} */}
                                                                                        {/* {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" id={"colorDiv" + key} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPoColorModal(`${itemGrid.gridTwoId}`, `${itemGrid.color}`, itemGrid.colorList, `${"color" + key}`,itemGrid.colorSearch) : null} >
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                            <g fill="none" fillRule="evenodd">
                                                                                                <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                                <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                            </g>
                                                                                        </svg>
                                                                                    </div> : null} */}
                                                                                    </li>
                                                                                    {this.props.showSimple && this.props.isSet && <li className="pnt-f-icon">
                                                                                        <div className="nph-switch-btn">
                                                                                            <label className="tg-switch">
                                                                                                <input type="checkbox" checked={itemGrid.sizeType == "complex"} onClick={() => this.simpleCompleToggle(itemGrid.gridTwoId, item.gridOneId)} />
                                                                                                <span className="tg-slider tg-round"></span>
                                                                                            </label>
                                                                                            {itemGrid.sizeType === "complex" && <label className="nph-wbtext">C</label>}
                                                                                            {itemGrid.sizeType !== "complex" && <label className="nph-wbtext"> S</label>}
                                                                                        </div>
                                                                                    </li>}
                                                                                    <li className={this.props.copyColor == "true" ? "text-center pnt-f-icon pnt-bg-red" : "text-center pnt-f-icon pnt-bg-red"}>
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" onClick={(e) => this.props.deleteLineItem(itemGrid.gridTwoId, itemGrid.setNo)} width="17" height="17" viewBox="0 0 17 19">
                                                                                            <path fill="#a4b9dd" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                                        </svg>
                                                                                        <span className="generic-tooltip">Delete</span>
                                                                                    </li>
                                                                                    {itemGrid.lineItemChk ? 
                                                                                    <li className="pnt-f-icon"><img src={ErrorIcon} />
                                                                                        <span className="generic-tooltip">LineItem charges is wrongly calculated</span>
                                                                                    </li> 
                                                                                    : false}
                                                                                    {this.props.copyColor == "true" && this.props.isColorRequired ? <li className="pnt-f-icon pnt-ccradio"><label className="containerPurchaseRadio displayPointer copiedBtn" >
                                                                                        {itemGrid.colorChk ? "Copied" : ""}
                                                                                        <input autoComplete="off" type="radio" id={"colorRadio" + key} checked={itemGrid.colorChk} name="colorRadio" value="colorRadio" onClick={(e) => this.copingFocus(itemGrid.gridTwoId, this.props.selectedRowId, itemGrid.color, itemGrid.colorList, itemGrid.option, itemGrid.total, itemGrid.colorSearch, itemGrid.sizeType)} />
                                                                                        <span className="checkmark"></span>
                                                                                    </label>
                                                                                        <span className="generic-tooltip">Copy Color</span>
                                                                                    </li> : null}
                                                                                </ul>
                                                                            </td> : null}

                                                                            {this.props.codeRadio == "poIcode" ? <td className="pad-0 hoverTable openModalBlueBtn tInputRead"  >
                                                                                <input autoComplete="off" type="text" readOnly name="itemBarcode" onKeyDown={(e) => this._handleKeyPress(e, "itemBarcode" + `${itemGrid.gridTwoId}`)} id={"itemBarcode" + `${itemGrid.gridTwoId}`} value={itemGrid.itemBarcode} className="inputTable " />
                                                                            </td> : null}
                                                                            {this.props.isColorRequired ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputText" >
                                                                                {/* load indent check */}
                                                                                {this.props.codeRadio !== "raisedIndent" ? <div className="pos-rel">
                                                                                    <label className="piToolTip m0 poToolTip">
                                                                                        <div className="topToolTip">
                                                                                            {/*{this.state.isModalShow ?*/}
                                                                                            <input autoComplete="off" type="text" name="color" readOnly onKeyDown={(e) => this._handleKeyPressRow(e, "color", key)} id={"color" + key} value={itemGrid.color}
                                                                                                onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPoColorModal(`${itemGrid.gridTwoId}`, `${itemGrid.color}`, itemGrid.colorList, `${"color" + key}`, itemGrid.colorSearch, itemGrid.sizeType) : null} className="inputTable " />

                                                                                            {/*: <input autoComplete="off" type="text" name="color" onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent"?(e) => this.handleColor(itemGrid.gridTwoId, e):null} onKeyDown={(e) => this._handleKeyPressRow(e, "color", key)} id={"color" + key} value={itemGrid.colorSearch} className="inputTable" />}*/}

                                                                                            <span className="topToolTipText">{itemGrid.color}</span>
                                                                                        </div>
                                                                                    </label>
                                                                                    {/* {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" id={"colorDiv" + key} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPoColorModal(`${itemGrid.gridTwoId}`, `${itemGrid.color}`, itemGrid.colorList, `${"color" + key}`,itemGrid.colorSearch) : null} >
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                            <g fill="none" fillRule="evenodd">
                                                                                                <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                                <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                            </g>
                                                                                        </svg>
                                                                                    </div> : null} */}
                                                                                    {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? <div className="modal-search-btn-table" id={"colorDiv" + key} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPoColorModal(`${itemGrid.gridTwoId}`, `${itemGrid.color}`, itemGrid.colorList, `${"color" + key}`, itemGrid.colorSearch, itemGrid.sizeType) : null}>
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                                            <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                                        </svg>
                                                                                    </div> : null}
                                                                                </div>
                                                                                    //disable input type for load indent 
                                                                                    : <div className="pos-rel">
                                                                                        <input type="text" className="inputTable " value={itemGrid.color} disabled />
                                                                                    </div>}
                                                                            </td> : null}

                                                                            {/* upload image functionality */}
                                                                            <td className="pad-0 pad-0 hoverTable openModalBlueBtn openModalBlueBtn tdFocus displayPointer tInputText" onClick={this.props.codeRadio != "setBased" ? (e) =>
                                                                                this.openImageModal(`${itemGrid.gridOneId}`, `${itemGrid.gridTwoId}`, `${"image" + key}`) : null}>
                                                                                <label className="piToolTip">
                                                                                    <div className="topToolTip">
                                                                                        <input type="text" className="imageInput" readOnly onKeyDown={this.props.codeRadio != "setBased" ? (e) => this._handleKeyPressRow(e, "image", key) : null} id={"image" + key} placeholder={(itemGrid.imagePath != '' && itemGrid.imagePath != "null" && itemGrid.imagePath != null) ? 'View Image' : 'Upload Image'} value={itemGrid.image != undefined ? itemGrid.image.join(',') : ""} />
                                                                                        {/* <label>{item.image != undefined ? item.image.join(',') : ""}</label> */}
                                                                                        {itemGrid.image != undefined ? <span className="topToolTipText"> {itemGrid.image.join(',')}</span> : null}
                                                                                    </div>
                                                                                </label>
                                                                                {this.props.codeRadio != "setBased" ? <div className="modal-search-btn-table" >
                                                                                    {/* <svg xmlns="http://www.w3.org/2000/svg" width="34" height="44" viewBox="0 0 33 43">
                                                                                        <g fill="none" fillRule="nonzero">
                                                                                            <path fill="#fffae7" d="M0 0h33v43H0z" />
                                                                                            <path fill="#000" d="M13.143 22.491A2.847 2.847 0 0 1 16 19.635a2.847 2.847 0 0 1 2.857 2.856A2.847 2.847 0 0 1 16 25.348a2.847 2.847 0 0 1-2.857-2.857zm-6.026 4.05v-8.236c0-.94.763-1.703 1.703-1.703h3.404l.156-.685c.255-1.056 1.174-1.8 2.25-1.8h2.7c1.077 0 2.016.744 2.25 1.8l.157.685h3.404c.94 0 1.702.763 1.702 1.702v8.237c0 .998-.802 1.82-1.82 1.82H8.938c-.998 0-1.82-.822-1.82-1.82zm4.676-4.05A4.2 4.2 0 0 0 16 26.698a4.2 4.2 0 0 0 4.207-4.207A4.183 4.183 0 0 0 16 18.304a4.196 4.196 0 0 0-4.207 4.187zm-2.817-3.443c0 .47.391.86.861.86s.86-.39.86-.86-.39-.861-.86-.861c-.49.02-.86.391-.86.86z" />
                                                                                        </g>
                                                                                    </svg> */}
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17.245" height="14.37" viewBox="0 0 17.245 14.37">
                                                                                        <path d="M3.593 1.437H1.437V.719h2.156zm7.853 0l.583.876a2.867 2.867 0 0 0 2.391 1.28h1.387v9.341H1.437V3.593h2.824a2.867 2.867 0 0 0 2.391-1.28l.583-.876zM12.215 0H6.467L5.456 1.515a1.436 1.436 0 0 1-1.2.64H0V14.37h17.245V2.156h-2.824a1.436 1.436 0 0 1-1.2-.64L12.215 0zm-7.9 5.748a.719.719 0 1 0-.719.719.718.718 0 0 0 .715-.719zm5.03 0A2.156 2.156 0 1 1 7.185 7.9a2.158 2.158 0 0 1 2.156-2.152zm0-1.437A3.593 3.593 0 1 0 12.933 7.9a3.593 3.593 0 0 0-3.592-3.589z" />
                                                                                    </svg>
                                                                                </div> : null}
                                                                            </td>
                                                                            {this.props.isColorRequired ? <td className="pad-0 tInputRead set-tdWidth">

                                                                                <input autoComplete="off" type="text" className="inputTable" id={"option" + key} readOnly pattern="[0-9]*" value={itemGrid.option} />
                                                                            </td> : null}


                                                                            <td className="pad-0 tInputRead set-tdWidth">
                                                                                <input autoComplete="off" type="text" className="inputTable" id={"setNo" + key} pattern="[0-9]*" value={itemGrid.setNo} readOnly />
                                                                            </td>

                                                                            {this.props.codeRadio == "poIcode" ? <td className="pad-0 tInputRead">
                                                                                <input autoComplete="off" type="text" className="inputTable" id={"sizeId" + key} value={itemGrid.size} readOnly />

                                                                            </td> : null}
                                                                            {this.props.isSet ? this.props.codeRadio == "poIcode" ? <td className="pad-0 tdFocus tInputType set-tdWidth">
                                                                                <input autoComplete="off" type="text" className="inputTable" pattern="[0-9]*" id={"setRatio" + key} value={itemGrid.setRatio} onBlur={(e) => this.handleSetRationBlur(`${itemGrid.gridTwoId}`, `${itemGrid.setQty}`, `${itemGrid.total}`, `${itemGrid.designRowId}`, e)} onChange={(e) => this.props.sizeHandleChange(`${itemGrid.gridTwoId}`, `${itemGrid.size}`, e, `${itemGrid.color}`)} />

                                                                            </td> : null : null}
                                                                            {this.props.codeRadio != "poIcode" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputText" >
                                                                                {this.props.codeRadio !== "raisedIndent" ? <div className="pos-rel">

                                                                                    <label className="piToolTip m0 poToolTip">
                                                                                        <div className="topToolTip">
                                                                                            {/*{this.state.isModalShow ?*/}
                                                                                            <input autoComplete="off" type="text" name="size" readOnly onKeyDown={(e) => this._handleKeyPressRow(e, "size", key)} id={"size" + key} value={itemGrid.sizes.join(',')} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPiSizeModal(this.props.departmentCode, itemGrid.gridTwoId, itemGrid.sizes, itemGrid.ratio, this.props.articleName, itemGrid.sizeList, key, itemGrid.sizeSearch, itemGrid.sizeType, itemGrid) : null} className="inputTable " />
                                                                                            {/*: <input autoComplete="off" type="text" name="size" onChange={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ?(e) => this.handleSize(itemGrid.gridTwoId, e):null} onKeyDown={(e) => this._handleKeyPressRow(e, "size", key)} id={"size" + key} value={itemGrid.sizeSearch} className="inputTable" />}*/}

                                                                                            <span className="topToolTipText">{itemGrid.sizes.join(',')}</span>
                                                                                        </div>
                                                                                    </label>
                                                                                    {/* {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" id={"sizeDiv" + key} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPiSizeModal(this.props.departmentCode, itemGrid.gridTwoId, itemGrid.sizes, itemGrid.ratio, this.props.articleName, itemGrid.sizeList, key,itemGrid.sizeSearch) : null} >
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                            <g fill="none" fillRule="evenodd">
                                                                                                <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                                <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                            </g>
                                                                                        </svg>
                                                                                    </div> : null} */}
                                                                                    {this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? <div className="modal-search-btn-table" id={"sizeDiv" + key} onClick={this.props.codeRadio == "Adhoc" || this.props.codeRadio == "holdPo" || this.props.codeRadio == "raisedIndent" ? (e) => this.openPiSizeModal(this.props.departmentCode, itemGrid.gridTwoId, itemGrid.sizes, itemGrid.ratio, this.props.articleName, itemGrid.sizeList, key, itemGrid.sizeSearch, itemGrid.sizeType, itemGrid) : null} >
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                                            <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                                        </svg>
                                                                                    </div> : null}
                                                                                </div>
                                                                                    //disable size for load indent

                                                                                    : <div className="pos-rel"><input type="text" className="inputTable" disabled value={itemGrid.sizes.join(',')} /></div>}
                                                                            </td> : null}
                                                                            {this.props.isSet ? this.props.codeRadio != "poIcode" ? <td className={((itemGrid.sizeType === "simple" && this.props.isCopyIndent) || itemGrid.sizeList.length ==1) ? "pad-0 tInputType set-tdWidth" : "pad-0 tInputRead set-tdWidth"}>
                                                                                <input autoComplete="off" type="text" className="inputTable" id={"ration" + key} readOnly={!((itemGrid.sizeType === "simple" && this.props.isCopyIndent) || itemGrid.sizeList.length == 1)} value={(itemGrid.sizeType === "simple" && this.props.isCopyIndent) ? itemGrid.ratio : itemGrid.ratio.join(',')} onChange={(e) => this.props.ratioHandleChange(`${itemGrid.gridTwoId}`, `${itemGrid.size}`, e, `${itemGrid.color}`, key)} onBlur={(itemGrid.sizeType === "simple" && itemGrid.sizeList.length ==1) ? () => this.updateSingleRatio(itemGrid.sizes, key, itemGrid.ratio) : (itemGrid.sizeType === "complex" && itemGrid.sizeList.length ==1) ? () => this.updateSetRatio(itemGrid.sizes, key, itemGrid.ratio, itemGrid.sizeList, itemGrid.gridTwoId, (itemGrid.ppQty != undefined ? itemGrid.ppQty : itemGrid.total), itemGrid.sizeType, itemGrid.setQty, itemGrid.option) : null}/>
                                                                                </td> : null : <td className={itemGrid.sizeList.length == 1 ? "pad-0 tInputType set-tdWidth" : "pad-0 tInputRead set-tdWidth"}><input type={itemGrid.sizeList.length == 1 ? "number" : "text"} className="inputTable" value={itemGrid.ratio.join(',')} readOnly={itemGrid.sizeList.length > 1} onChange={(e) => this.props.ratioHandleChange(`${itemGrid.gridTwoId}`, `${itemGrid.size}`, e, `${itemGrid.color}`, key)} onBlur={() => this.updateNonSetRatio(itemGrid.sizes, key, itemGrid.ratio, itemGrid.sizeList, itemGrid.gridTwoId,)} /></td>}
                                                                            {this.props.isSet ? <td className="pad-0 tInputRead set-tdWidth">
                                                                                {itemGrid.sizeType == "simple" ?
                                                                                    <input autoComplete="off" type="text" className="inputTable" pattern="[0-9]*" id={"total" + key} value={itemGrid.ppQty} readOnly />
                                                                                    : <input autoComplete="off" type="text" className="inputTable" pattern="[0-9]*" id={"total" + key} value={itemGrid.total} readOnly />
                                                                                }
                                                                            </td> : null}
                                                                            {this.props.isSet ? <td className="pad-0 tdFocus tInputType set-tdWidth">
                                                                                <input autoComplete="off" type="text" className="inputTable sd" disabled={itemGrid.sizeType === "simple" || this.props.codeRadio == "poIcode" ? true : false} id={"setQty" + key} pattern="[0-9]*" onFocus={(e) => this.focusQty(e, itemGrid.gridTwoId, itemGrid)} value={itemGrid.setQty} data-value={itemGrid.setQty} onKeyDown={(e) => this.setQtyupDownArrow(e, key)} onBlur={(e) => this.onBlurQuantity(`${itemGrid.gridTwoId}`, `${itemGrid.setQty}`, `${itemGrid.total}`, `${item.gridOneId}`, e, `${"setQty" + key}`, item.hsnSacCode)} onChange={(e) => this.handleChange(`${itemGrid.gridTwoId}`, `${item.gridOneId}`, e, `${"setQty" + key}`, itemGrid, "setQty")} />
                                                                            </td> : null}

                                                                            {this.props.isSet ? <td className="pad-0 tdFocus tInputType set-tdWidth">
                                                                                <input autoComplete="off" type="text" className="inputTable sd" disabled={itemGrid.sizeType === "simple" || this.props.codeRadio == "poIcode" ? true : false} id={"quantity" + key} pattern="[0-9]*" onFocus={(e) => this.focusQty(e, itemGrid.gridTwoId, itemGrid, itemGrid)} value={itemGrid.quantity} data-value={itemGrid.quantity} onKeyDown={(e) => this.setQtyupDownArrow(e, key)} onBlur={(e) => this.onBlurQuantity(`${itemGrid.gridTwoId}`, `${itemGrid.setQty}`, `${itemGrid.total}`, `${item.gridOneId}`, e, `${"quantity" + key}`, item.hsnSacCode)} onChange={(e) => this.handleChange(`${itemGrid.gridTwoId}`, `${item.gridOneId}`, e, `${"quantity" + key}`, itemGrid, "quantity")} />
                                                                            </td> : 
                                                                            <td className="pad-0 pad-lft-8 tInputRead set-tdWidth">
                                                                                <label>{itemGrid.quantity}</label>
                                                                            </td> }

                                                                            {this.props.isBaseAmountActive ? <td className="pad-0 pad-lft-8 tInputRead set-tdWidth">
                                                                                <label>{itemGrid.basic == undefined || isNaN(itemGrid.basic) || itemGrid.basic == "" || itemGrid.basic == 0 ? "" : Number(itemGrid.basic).toFixed(2)}</label>

                                                                            </td> : null}
                                                                            <td className="pad-0 pad-lft-8 totalAmountDesc tInputRead set-tdWidth">
                                                                                <label id={"toolIdCmpr" + itemGrid.gridTwoId} className={this.props.displayOtb ? item.amount > itemGrid.otb ? "txtColor amtCompareHover displayPointer" : "pad-top-3 amtCompareHover displayPointer" : "pad-top-3 amtCompareHover displayPointer"}>
                                                                                    {itemGrid.amount && itemGrid.amount.toFixed(2)}{item.amount > item.otb ?
                                                                                        <span>
                                                                                            {this.props.displayOtb ? <div className="amtCompare" id={"moveLeftCmpr" + itemGrid.gridTwoId}><p>Pi amount cannot be greater than OTB value.</p></div> : null}

                                                                                        </span> : ""}</label>
                                                                                {/*
                                                                                {item.amount != "" ? <span className="netAmtHover">
                                                                                    <img id={"toolId" + itemGrid.gridTwoId} onMouseOver={(e) => this.calculatedMargin(`toolId${itemGrid.gridTwoId}`, `moveLeft${itemGrid.gridTwoId}`)} src={exclaimIcon} />

                                                                                    <div id={"moveLeft" + itemGrid.gridTwoId} className="totalAmtDescDrop">
                                                                                        {item.finCharges != undefined ? itemGrid.finCharges.length != 0 ? <div className="amtContent">

                                                                                            <table>
                                                                                                <thead>
                                                                                                    <tr><th><p>Charge Name</p></th>
                                                                                                        <th><p>% / Amount</p></th>
                                                                                                        <th><p>Total</p></th></tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    {itemGrid.finCharges.map((data, key) => (
                                                                                                        <tr key={key}>
                                                                                                            <td><p>{data.chgName}</p></td>
                                                                                                            <td><p>{data.rates != undefined ? <span>{data.rates.igstRate} % on {itemGrid.basic}</span> : ""}</p>{}</td>
                                                                                                            <td><p>{data.charges != undefined ? <span>{data.charges.sign}{data.charges.chargeAmount}</span> : data.sign + data.finChgRate}</p>

                                                                                                            </td>
                                                                                                        </tr>))}
                                                                                                </tbody>

                                                                                            </table>
                                                                                        </div> : null
                                                                                         : null}
                                                                                        <div className="amtBottom">
                                                                                            <div className="col-md-12">
                                                                                                <div className="col-md-6 pad-lft-7">

                                                                                                    <span>Basic</span>
                                                                                                </div>
                                                                                                <div className="col-md-6 textRight padRightNone">

                                                                                                    <span className="totalAmtt">+{itemGrid.basic}</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="amtBottom">
                                                                                            <div className="col-md-12 pad-top-5 m-top-5">
                                                                                                <div className="col-md-6">

                                                                                                    <p>Grand Total</p>
                                                                                                </div>
                                                                                                <div className="col-md-6 textRight padRightNone">

                                                                                                    <p className="totalAmt">{Math.round(itemGrid.amount * 100) / 100}</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </span> : ""}*/}
                                                                            </td>
                                                                            {!this.props.isTaxDisable ? <td className="pad-0 pad-lft-8 tInputRead set-tdWidth">
                                                                                <label>{itemGrid.tax && Number(itemGrid.tax).toFixed(2)}</label>

                                                                            </td> : null}
                                                                            {this.props.isRspRequired ? <td className="tInputRead set-tdWidth" >
                                                                                <label className="displayInline line-h-2" >{itemGrid.calculatedMargin}</label>
                                                                                {/*{itemGrid.calculateMargin.toString() != "" ? <span className="calculatedMargin displayInline displayPointer">*/}

                                                                                {/*</span> : null}*/}
                                                                            </td> : null}
                                                                            {!this.props.isGSTDisable ? <td className="tInputRead set-tdWidth">
                                                                                <label>{itemGrid.gst.toString() != "" ? itemGrid.gst + "%" : ""} </label>
                                                                            </td> : null}

                                                                            {this.props.isRspRequired ? <td className="tInputRead set-tdWidth">
                                                                                <label className="displayInline line-h-2">{itemGrid.mrk}</label>
                                                                                {/*{itemGrid.intakeMargin.toString() != "" ? <span className="calculatedMargin displayInline displayPointer inTakeMargin">*/}

                                                                                {/*<div className="calculatedToolTip totalAmtDescDrop" id={"inTakeMarginLeft" + itemGrid.gridTwoId}>
                                                                <div className="formula">
                                                                    <h3>Intake Margins</h3>
                                                                    <h5><span>RSP</span> <span>X</span> <span className="pad-0">100</span><p className="pad-lft-0">100+COST</p></h5>
                                                                </div>
                                                            </div>
                                                        </span> : null}*/}
                                                                            </td> : null}
                                                                        </tr>
                                                                    }) : null))}

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="tab-pane fade new-generic-table" id="setmodalsetudf" role="tabpanel">
                                            <div className="col-md-12 col-sm-12 pad-0 m-top-20 sectionOneTable pnt-bxsha">
                                                <div className="table-scroll zui-scroller pad-0" >
                                                    {/* table-scroll zui-scroller pad-0 */}
                                                    <table className="table scrollTable zui-table pit-new-design disTable">
                                                        <thead>
                                                            {this.state.poRows.map((item, key) => (

                                                                item.gridOneId == this.props.selectedRowId ?

                                                                    <tr key={key}>
                                                                        <th className="fixed-side pnt-fixed width80" ></th>
                                                                        <th className="set-setNo-width"><label>Set No.</label></th>
                                                                        {this.props.isColorRequired ? <th> <label> Color</label></th> : null}
                                                                        {item.lineItem[0].setUdfHeader.length != 0 ? item.lineItem[0].setUdfHeader.map((data, idd) => (

                                                                            ((data.udfType == "SMUDFSTRING01" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING02" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING03" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING04" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING05" && (data.isDisplayPI == "Y")) ||
                                                                                (data.udfType == "SMUDFSTRING06" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING07" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING08" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING09" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRIN10" && (data.isDisplayPI == "Y")) ||
                                                                                (data.udfType == "SMUDFNUM01" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM02" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM03" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM04" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM05" && (data.isDisplayPI == "Y")) ||
                                                                                (data.udfType == "SMUDFDATE01" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE02" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE03" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE04" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE05" && (data.isDisplayPI == "Y"))) ? <th id={idd} key={idd} >
                                                                                    <label>{data.displayName != null ? data.displayName : data.udfType}{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null}
                                                                                    </label>
                                                                                </th> : null)) : null}
                                                                    </tr>

                                                                    : null))}
                                                        </thead>
                                                        <tbody>
                                                            {this.state.poRows.map((item, key) => (
                                                                item.gridOneId == this.props.selectedRowId ?
                                                                    item.lineItem.map((udfGrid, idxxx) => (
                                                                        <tr id={idxxx} key={idxxx} >
                                                                            <td className="fixed-side alignMiddle tdFocus pnt-fixed width80" >
                                                                                <ul className="list-inline">
                                                                                    <li className="text-center pnt-f-icon pnt-bg-yellow" onClick={() => this.copyUdfHandler(udfGrid)}>
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 29.884 35.5">
                                                                                            <path id="copy_2_" fill="#a4b9dd" d="M18.79 35.5H5.547A5.553 5.553 0 0 1 0 29.953v-18.79a5.553 5.553 0 0 1 5.547-5.547H18.79a5.553 5.553 0 0 1 5.547 5.547v18.79A5.553 5.553 0 0 1 18.79 35.5zM5.547 8.39a2.777 2.777 0 0 0-2.773 2.773v18.79a2.777 2.777 0 0 0 2.773 2.773H18.79a2.777 2.777 0 0 0 2.773-2.773v-18.79A2.777 2.777 0 0 0 18.79 8.39zm24.337 18.1V5.547A5.553 5.553 0 0 0 24.337 0H8.944a1.387 1.387 0 0 0 0 2.773h15.393a2.777 2.777 0 0 1 2.773 2.774v20.939a1.387 1.387 0 0 0 2.773 0zm0 0" data-name="copy (2)" />
                                                                                        </svg>
                                                                                        <span className="generic-tooltip">Copy</span>
                                                                                    </li>
                                                                                    <li className="text-center pnt-f-icon pnt-paste-btn" onClick={() => this.pasteUdfHandler(key, idxxx, udfGrid)}>
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 33.011 38.5">
                                                                                            <g id="paste" transform="translate(-36.5)">
                                                                                                <g id="Group_2960" data-name="Group 2960" transform="translate(36.5)">
                                                                                                    <path id="Path_907" fill="#a4b9dd" d="M63.946 18.822V7.52a4.517 4.517 0 0 0-4.512-4.512h-4.92a4.512 4.512 0 0 0-8.508 0h-4.994A4.517 4.517 0 0 0 36.5 7.52v23.46a4.517 4.517 0 0 0 4.512 4.512h9.733A4.519 4.519 0 0 0 55 38.5l10-.006a4.517 4.517 0 0 0 4.512-4.511v-9.565zm.44 4.709h-1.944a1.506 1.506 0 0 1-1.5-1.5v-1.968zM45.749 6.016h3.008v-1.5a1.5 1.5 0 0 1 3.008 0v1.5h3.008v2.932h-9.024zm4.737 13.459v13.009h-9.474a1.506 1.506 0 0 1-1.5-1.5V7.52a1.506 1.506 0 0 1 1.5-1.5h1.729v5.94H57.78V6.016h1.654a1.506 1.506 0 0 1 1.5 1.5V15.8l-.828-.833H55a4.517 4.517 0 0 0-4.514 4.508zM66.5 33.983a1.505 1.505 0 0 1-1.5 1.5l-10 .006a1.505 1.505 0 0 1-1.5-1.5V19.475a1.505 1.505 0 0 1 1.5-1.5h2.933v4.055a4.517 4.517 0 0 0 4.512 4.512H66.5z" data-name="Path 907" transform="translate(-36.5)" />
                                                                                                </g>
                                                                                            </g>
                                                                                        </svg>
                                                                                        <span className="generic-tooltip">Paste</span>
                                                                                    </li>
                                                                                </ul>
                                                                            </td>
                                                                            <td className="tdFocus tInputText set-setNo-width"><label>{udfGrid.setNo}</label></td>
                                                                            {this.props.isColorRequired ? <td className="pad-0 hoverTable tdFocus tInputRead">
                                                                                <label className="piToolTip m0">
                                                                                    <div className="topToolTip">
                                                                                        <label className="displayPointer">{udfGrid.color}</label>
                                                                                        <span className="topToolTipText">{udfGrid.color}</span>
                                                                                    </div>
                                                                                </label>
                                                                            </td> : null}
                                                                            {udfGrid.setUdfArray != undefined ? udfGrid.setUdfArray.map((data, kkey) => (

                                                                                ((data.udfType == "SMUDFSTRING01" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING02" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING03" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING04" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING05" && (data.isDisplayPI == "Y")) ||
                                                                                    (data.udfType == "SMUDFSTRING06" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING07" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING08" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRING09" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFSTRIN10" && (data.isDisplayPI == "Y")) ||
                                                                                    (data.udfType == "SMUDFNUM01" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM02" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM03" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM04" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFNUM05" && (data.isDisplayPI == "Y")) ||
                                                                                    (data.udfType == "SMUDFDATE01" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE02" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE03" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE04" && (data.isDisplayPI == "Y")) || (data.udfType == "SMUDFDATE05" && (data.isDisplayPI == "Y"))) ?
                                                                                    data.isLovPI == 'Y' && <td className="pad-0 hoverTable tdFocus openModalBlueBtn tInputText" key={kkey} >
                                                                                        <div className="pos-rel">
                                                                                            {!this.props.isModalShow ?
                                                                                                <input autoComplete="off" disabled={this.props.showSimple && data.displayName == "Simple / Complex"} type="text" name="" className="inputTable" onChange={(e) => this.handleInputUdf(udfGrid.gridTwoId, udfGrid.setNo, data.udfType, e, "setUdf")} onKeyDown={(e) => this._handleKeyPressRow(e, `${data.udfType + "_" + idxxx + "_" + kkey}`, kkey, "setUdf")} id={data.udfType + "_" + idxxx + "_" + kkey} value={(this.props.showSimple && data.displayName == "Simple / Complex") ? udfGrid.sizeType : data.value} onBlur={data.isValidationPi == 'Y' ? (e) => this.setDropdownValidation(`${data.udfType}`, `${data.displayName}`, `${udfGrid.setNo}`, `${data.value}`, `${data.udfType + "_" + idxxx + "_" + kkey}`, udfGrid.gridTwoId, e) : null}/>
                                                                                                // <input autoComplete="off" type="text" name="" className="inputTable" onChange={(e) => this.handleInputUdf(udfGrid.gridTwoId, udfGrid.setNo, data.udfType, e, "setUdf")} onKeyDown={(e) => this._handleKeyPressRow(e, `${data.udfType + "_" + idxxx + "_" + kkey}`, kkey, "setUdf")} id={data.udfType + "_" + idxxx + "_" + kkey} value={data.value} />
                                                                                                : <input autoComplete="off" type="text" name="" className="inputTable " readOnly onKeyDown={(e) => this._handleKeyPressRow(e, `${data.udfType + "_" + idxxx + "_"}`, kkey, "setUdf")} id={data.udfType + "_" + idxxx + "_" + kkey} value={data.value} onClick={this.props.codeRadio != "setBased" ? (e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.setNo}`, `${data.value}`, `${data.udfType + "_" + idxxx + "_" + kkey}`, udfGrid.gridTwoId
                                                                                                ) : null} />}
                                                                                            {/* {this.props.codeRadio != "setBased" ? <div className="purchaseTableDiv" id={"setUdf" + data.udfType + "_" + idxxx + "_" + kkey} onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.setNo}`, `${data.value}`, `${data.udfType + "_" + idxxx + "_" + kkey}`, udfGrid.gridTwoId)}>
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                                    <g fill="none" fillRule="evenodd">
                                                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                                    </g>
                                                                                                </svg>
                                                                                            </div> : null} */}
                                                                                            {this.props.codeRadio != "setBased" ? <div className="modal-search-btn-table" id={"setUdf" + data.udfType + "_" + idxxx + "_" + kkey} onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.setNo}`, `${data.value}`, `${data.udfType + "_" + idxxx + "_" + kkey}`, udfGrid.gridTwoId)}>
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                                                    <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                                                </svg>
                                                                                            </div> : null}
                                                                                            {/*{!this.state.isModalShow ? this.state.udfMapping ? this.state.searchSU == "setUdf" + data.udfType + kkey + udfGrid.gridTwoId ? <UdfMappingModal closeUdf={(e) => this.closeUdf(e)} {...this.props}{...this.state} updateUdf={(e) => this.updateUdf(e)} udfType={this.state.udfType} updateUdfSetvalue={(e) => this.updateUdfSetvalue(e)} udfName={this.state.udfName} setValue={this.state.setValue} udfRow={this.state.udfRow} udfMappingAnimation={this.state.udfMappingAnimation} closeUdfMappingModal={(e) => this.openUdfMappingModal(e)} /> : null : null : null}*/}

                                                                                        </div>
                                                                                    </td> 
                                                                                    || data.isLovPI == 'N' && <td className="pad-0 hoverTable tdFocus openModalBlueBtn tInputText" key={kkey} >
                                                                                    <div className="pos-rel">
                                                                                        {!this.props.isModalShow ?
                                                                                            <input autoComplete="off" disabled={this.props.showSimple && data.displayName == "Simple / Complex"} type="text" name="" className="inputTable" onChange={(e) => this.handleInputUdf(udfGrid.gridTwoId, udfGrid.setNo, data.udfType, e, "setUdf")} id={data.udfType + "_" + idxxx + "_" + kkey} value={(this.props.showSimple && data.displayName == "Simple / Complex") ? udfGrid.sizeType : data.value} />
                                                                                            
                                                                                            : <input autoComplete="off" type="text" name="" className="inputTable " readOnly id={data.udfType + "_" + idxxx + "_" + kkey} value={data.value} />}                                                                                        
                                                                                    </div>
                                                                                </td>: null
                                                                            )) : null}
                                                                        </tr>)) : null))}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {this.state.udfMapping ? <UdfMappingModal setUdfId={this.state.setUdfId} closeUdf={(e) => this.closeUdf(e)} {...this.props}{...this.state} updateUdf={(e) => this.updateUdf(e)} udfType={this.state.udfType} updateUdfSetvalue={(e) => this.updateUdfSetvalue(e)} udfName={this.state.udfName} setValue={this.state.setValue} udfRow={this.state.udfRow} udfMappingAnimation={this.state.udfMappingAnimation} closeUdfMappingModal={(e) => this.openUdfMappingModal(e)} /> : null}

                                        </div>



                                        {this.state.itemDetailsModal ? <ItemDetailsModal {...this.props} {...this.state} catDescId={this.state.catDescId} itemDetailName={this.state.itemDetailName} itemId={this.state.itemId} updateItemDetails={(e) => this.updateItemDetails(e)} updateItemDetailValue={(e) => this.updateItemDetailValue(e)} itemDetailCode={this.state.itemDetailCode} itemDetailValue={this.state.itemDetailValue} hl3Code={this.props.departmentCode} department={this.props.department} closeItemModal={(e) => this.closeItemModal(e)} itemDetailsModalAnimation={this.state.itemDetailsModalAnimation} itemDetailsModal={this.state.itemDetailsModal} openItemModal={(e) => this.openItemModal(e)} itemType={this.state.itemType} itemId={this.state.itemId} /> : null}
                                        {this.state.itemUdfMappingModal ? <ItemUdfMappingModal {...this.props} {...this.state} itemFocusId={this.state.itemFocusId} department={this.state.department} itemArticleCode={this.state.itemArticleCode} itemUdfId={this.state.itemUdfId} vendorMrpPo={this.props.articleCode} itemUdfName={this.state.itemUdfName} itemUdfType={this.state.itemUdfType} itemUdfUpdate={(e) => this.itemUdfUpdate(e)} {...this.props} {...this.state} articleName={this.state.articleName} updateItemValue={(e) => this.updateItemValue(e)} itemUdfMappingAnimation={this.state.itemUdfMappingAnimation} itemValue={this.state.itemValue} closeItemUdfModal={(e) => this.closeItemUdfModal()} openItemUdfModal={(e) => this.openItemUdfModal(e)} /> : null}


                                    </div>


                                    {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

                                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}

                                </div>
                            </div>
                        </div>  </div>
                </div>
                {this.state.colorModal ? <PiColorModal {...this.props} {...this.state} colorPosId={this.state.colorPosId} deselectallProp={(e) => this.deselectallProp(e)} colorListValue={this.state.colorListValue} colorCode={this.state.colorCode} hl3Code={this.props.departmentCode} updateColorList={(e) => this.updateColorList(e)} updateColorState={(e) => this.updateColorState(e)} colorModalAnimation={this.state.colorModalAnimation} colorValue={this.state.colorValue} colorRow={this.state.colorRow} closePiColorModal={(e) => this.closePiColorModal(e)} closePiiColorModal={(e) => this.openPoColorModal(e)} /> : null}

                {
                    this.state.sizeModal ? <PiSizeModal ref={(sizeFun) => this.sizeFun = sizeFun} updateSingleAmount={this.updateSingleAmount} {...this.props} {...this.state} spliceRow={this.spliceRow} updateSizeDetails={this.updateSizeDetails} updateSimpleSize={this.updateSimpleSize} sizePosId={this.state.sizePosId} sizeDataList={this.state.sizeDataList} division={this.props.divisionName} section={this.props.sectionName} hl3Code={this.props.departmentCode} sizeArray={this.state.sizeArray} updateSizeList={(e) => this.updateSizeList(e)} closePiSizeModal={(e) => this.closePiSizeModal(e)} updateSizeState={(e) => this.updateSizeState(e)}
                        sizeRow={this.state.sizeRow} sizeCode={this.state.sizeCode} articleName={this.props.articleName} department={this.props.departmentName} sizeValue={this.state.sizeValue} sizes={this.state.sizes} sizeModalAnimatiion={this.state.sizeModalAnimatiion} /> : null
                }
                {this.state.imageModal ? <PiImageModal {...this.state} {...this.props} file={this.state.imageState} imageName={this.state.imageName} updateImage={(e) => this.updateImage(e)} imageModalAnimation={this.state.imageModalAnimation} closePiImageModal={(e) => this.closePiImageModal(e)} closeImageModal={(e) => this.openImageModal(e)} imageRowId={this.state.imageRowId} /> : null}

            </div >

        );
    }
}

export default SetModalPI;