import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class SetVendorModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            searchMrp: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
            department: "",
            poSetVendor: this.props.poSetVendor,
            poSetVendorCode: this.props.poSetVendorCode,
            setVendorState: [],
            searchBy: "startWith",
            focusedLi: "",

        }

    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    componentWillMount() {
        this.setState({
            poSetVendor: this.props.poSetVendor,
            poSetVendorCode: this.props.poSetVendorCode,
            searchMrp: this.props.isModalShow ? "" : this.props.setVendorSearch,
            type: this.props.isModalShow || this.props.setVendorSearch == "" ? 1 : 3


        })
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.selectVendor.isSuccess) {
            if (nextProps.purchaseIndent.selectVendor.data.resource != null) {
                this.setState({
                    setVendorState: nextProps.purchaseIndent.selectVendor.data.resource,
                    prev: nextProps.purchaseIndent.selectVendor.data.prePage,
                    current: nextProps.purchaseIndent.selectVendor.data.currPage,
                    next: nextProps.purchaseIndent.selectVendor.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.selectVendor.data.maxPage,
                })
            } else {
                this.setState({
                    setVendorState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {
                    this.textInput.current.focus();

                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.selectVendor.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.selectVendor.data.resource[0].supplierName,
                            searchMrp: this.props.isModalShow ? "" : this.props.setVendorSearch,
                            type: this.props.isModalShow || this.props.setVendorSearch == "" ? 1 : 3
                        })
                        document.getElementById(nextProps.purchaseIndent.selectVendor.data.resource[0].supplierName) != null ? document.getElementById(nextProps.purchaseIndent.selectVendor.data.resource[0].supplierName).focus() : null
                    }



                }
            }
        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.selectVendor.data.prePage,
                current: this.props.purchaseIndent.selectVendor.data.currPage,
                next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
            })
            if (this.props.purchaseIndent.selectVendor.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.selectVendor.data.currPage - 1,
                    hl3code: this.props.hl3CodeDepartment,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.selectVendorRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.selectVendor.data.prePage,
                current: this.props.purchaseIndent.selectVendor.data.currPage,
                next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
            })
            if (this.props.purchaseIndent.selectVendor.data.currPage != this.props.purchaseIndent.selectVendor.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                    hl3code: this.props.hl3CodeDepartment,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.selectVendorRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.selectVendor.data.prePage,
                current: this.props.purchaseIndent.selectVendor.data.currPage,
                next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
            })
            if (this.props.purchaseIndent.selectVendor.data.currPage <= this.props.purchaseIndent.selectVendor.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    hl3code: this.props.hl3CodeDepartment,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.selectVendorRequest(data)
            }
            this.textInput.current.focus();
        }
    }

    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                type: "",
                no: 1
            })
        if (this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                search: "",
                hl3code: this.props.hl3CodeDepartment,
                searchBy: this.state.searchBy
            }
            this.props.selectVendorRequest(data)
        }
        document.getElementById("searchMrp").focus()
    }

    handleChange(e) {
        if (e.target.id == "searchMrp") {
            this.setState({
                searchMrp: e.target.value
            })
        } else if (e.target.id == "searchBySetVend") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.searchMrp != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        hl3code: this.props.hl3CodeDepartment,
                        search: this.state.searchMrp,
                        searchBy: this.state.searchBy
                    }
                    this.props.selectVendorRequest(data)
                }
            })
        }
    }

    selectedData(e) {
        let c = this.state.setVendorState;
        for (let i = 0; i < c.length; i++) {
            if (c[i].supplierName == e) {
                this.setState({
                    poSetVendor: c[i].supplierName,
                    poSetVendorCode: c[i].supplierCode,
                })
            }
        }
        this.setState({
            setVendorState: c
        }, () => {
            if (!this.props.isModalShow) {
                this.onDone()
            }
        })
    }

    onDone() {
        let setVendorState = this.state.setVendorState
        let data = {}
        // for (var i = 0; i < setVendorState.length; i++) {
        if (this.state.isModalShow ? this.state.poSetVendor != "":this.state.poSetVendor != ""||this.props.setVendorSearch == this.state.searchMrp||this.props.setVendorSearch != this.state.searchMrp) {
            //         if (setVendorState[i].supplierName == this.state.poSetVendor) {
            data = {
                poSetVendor: this.state.poSetVendor,
                poSetVendorCode: this.state.poSetVendorCode
            }
            setTimeout(() => {
                this.props.updateVendorPo(data)
            }, 10);
            //         }
        }
        // }

        // for (var i = 0; i < setVendorState.length; i++) {
        if (this.state.isModalShow ? this.state.poSetVendor != "":this.state.poSetVendor != ""||this.props.setVendorSearch == this.state.searchMrp||this.props.setVendorSearch != this.state.searchMrp) {
            // if (setVendorState[i].supplierName == this.state.poSetVendor) {
            this.setState(
                {
                    search: "",
                })

            // }
            this.props.onCloseVendor()
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1500)
        }


    }

    onSearch(e) {

        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.searchMrp,
                hl3code: this.props.hl3CodeDepartment,
                searchBy: this.state.searchBy
            }
            this.props.selectVendorRequest(data)
        }
    }


    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.setVendorState.length != 0) {
                    this.setState({
                        poSetVendor: this.state.setVendorState[0].supplierName
                    })
                    let supplierName = this.state.setVendorState[0].supplierName
                    this.selectedData(supplierName)

                    window.setTimeout(function () {
                        document.getElementById("searchMrp").blur()
                        document.getElementById(supplierName).focus()
                    }, 0);
                } else {
                    window.setTimeout(function () {
                        document.getElementById("searchMrp").blur()
                        document.getElementById("closeButton").focus()
                    }, 0);
                }
            }

            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("findButton").focus();
                }, 0);
            }
        }
        if (e.key === "ArrowDown") {
            if (this.state.setVendorState.length != 0) {
                this.setState({
                    poSetVendor: this.state.setVendorState[0].supplierName
                })
                let supplierName = this.state.setVendorState[0].supplierName
                this.selectedData(supplierName)

                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById(supplierName).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("searchMrp").blur()
                    document.getElementById("closeButton").focus()
                }, 0);
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0);
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let supplierName = this.state.setVendorState[0].supplierName
            window.setTimeout(function () {
                document.getElementById(supplierName).blur()
                document.getElementById("doneButton").focus()
            }, 0);
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0);
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.props.onCloseVendor();
        }
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("searchMrp").focus()
            }, 0);
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.setVendorState.length != 0) {
                this.setState({
                    poSetVendor: this.state.setVendorState[0].supplierName
                })
                let supplierName = this.state.setVendorState[0].supplierName
                this.selectedData(supplierName)

                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(supplierName).focus()
                }, 0);
            } else {
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("searchMrp").focus()
                }, 0);
            }
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }


    selectLi(e, code) {
        let setVendorState = this.state.setVendorState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < setVendorState.length; i++) {
                if (setVendorState[i].supplierName == code) {
                    index = i
                }
            }
            if (index < setVendorState.length - 1 || index == 0) {
                document.getElementById(setVendorState[index + 1].supplierName) != null ? document.getElementById(setVendorState[index + 1].supplierName).focus() : null

                this.setState({
                    focusedLi: setVendorState[index + 1].supplierName
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < setVendorState.length; i++) {
                if (setVendorState[i].supplierName == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(setVendorState[index - 1].supplierName) != null ? document.getElementById(setVendorState[index - 1].supplierName).focus() : null

                this.setState({
                    focusedLi: setVendorState[index - 1].supplierName
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.props.onCloseVendor(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.setVendorState.length != 0 ?
                            document.getElementById(this.state.setVendorState[0].supplierName).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.setVendorState.length != 0) {
                    document.getElementById(this.state.setVendorState[0].supplierName).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.props.onCloseVendor(e)
        }


    }


    render() {

            !this.props.isModalShow ? $('.poSearchModal').removeClass('hideSmoothly')
            : null
        
        if (this.state.focusedLi != ""&& this.props.setVendorSearch == this.state.searchMrp) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }



        const { searchMrp } = this.state;
        return (

            this.props.isModalShow ? <div className={this.props.setVendorModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.setVendorModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.setVendorModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.setVendorModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT VENDOR</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only one Vendor from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBySetVend" name="searchBySetVend" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                    <label>
                                                        {this.state.searchMrp == "" && (this.state.type == "" || this.state.type == 1) ?
                                                            <button type="button" id="clearButton" className="clearbutton m-lft-15 btnDisabled" >CLEAR</button>
                                                            : <button type="button" id="clearButton" className="clearbutton m-lft-15" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                        }

                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover selectVendorMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Vendor</th>
                                                    <th></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.setVendorState == undefined || this.state.setVendorState == null || this.state.setVendorState.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.setVendorState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.supplierName}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.supplierName} onKeyDown={(e) => this.focusDone(e)} checked={this.state.poSetVendor == `${data.supplierName}`} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>
                                                            {data.supplierName}
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" id="doneButton" className="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" data-dismiss="modal" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.props.onCloseVendor(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
                :


                <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.setVendorState == undefined || this.state.setVendorState.length == 0 ?  <li><span>No Data Found</span></li>:
                            this.state.setVendorState.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.supplierName}`)} id={data.supplierName} className={this.state.poSetVendor == `${data.supplierName}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.supplierName)}>
                                    <span className="vendor-details">
                                        <span className="vd-name div-col-1">{data.supplierName}</span>

                                    </span>
                                </li>))}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                            <button className="btn-close" type="button" onClick={(e) => this.props.onCloseVendor(e)} id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn margin-180">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>

        );
    }
}

export default SetVendorModal;
