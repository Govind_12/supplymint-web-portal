import React from "react";

import FilterLoader from "../loaders/filterLoader";

import ToastLoader from "../loaders/toastLoader";

class vendorDesignModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {

            search: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
            errorMassage: "",
            poErrorMsg: false,
            vendorId: this.props.vendorId,
            vendorDesignVal: this.props.vendorDesignVal,
            itemtype: this.props.itemtype,
            vendorDesignData: [],
            searchBy: "contains"

        }

    }



    handleChange(e) {
        if (e.target.id = "searchArticle") {
            this.setState(
                {
                    search: e.target.value,

                });

        } else if (e.target.id == "searchByvd") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        hl3Code: this.props.hl3Code,
                        hl3Name: this.props.department,

                        itemType: this.props.itemType,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.getItemDetailsValueRequest(data)
                }
            })
        }

    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }





    onSelectedData(id) {

        let adata = this.state.vendorDesignData

        for (var i = 0; i < adata.length; i++) {
            if (adata[i].vendorDesign == id) {

                this.setState({
                    vendorDesignVal: adata[i].vendorDesign
                })
            }


        }


    }






    onDone() {
        if (this.state.vendorDesignVal != undefined && this.state.vendorDesignVal != "") {
            let finalData = {
                rowId: this.state.vendorId,
                value: this.state.vendorDesignVal
            }

            this.props.updateVendorDesign(finalData);

            this.props.closeVendorModal();


        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }

        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }


    componentWillMount() {
        this.setState({
            vendorId: this.props.vendorId,
            vendorDesignVal: this.props.vendorDesignVal,
            itemtype: this.props.itemtype,
        })


    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.getItemDetailsValue.isSuccess) {
            if (nextProps.purchaseIndent.getItemDetailsValue.data.resource != null) {
                this.setState({
                    vendorDesignData: nextProps.purchaseIndent.getItemDetailsValue.data.resource,

                    prev: nextProps.purchaseIndent.getItemDetailsValue.data.prePage,
                    current: nextProps.purchaseIndent.getItemDetailsValue.data.currPage,
                    next: nextProps.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getItemDetailsValue.data.maxPage

                })
            } else {
                this.setState({

                    vendorDesignData: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })


            }
            this.textInput.current.focus();
        }



    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage,
            })
            if (this.props.purchaseIndent.getItemDetailsValue.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getItemDetailsValue.data.currPage - 1,
                    hl3Code: this.props.hl3Code,
                    hl3Name: this.props.department,

                    itemType: this.props.itemType,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.getItemDetailsValueRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage,
            })
            if (this.props.purchaseIndent.getItemDetailsValue.data.currPage != this.props.purchaseIndent.getItemDetailsValue.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                    hl3Code: this.props.hl3Code,
                    hl3Name: this.props.department,

                    itemType: this.props.itemType,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.getItemDetailsValueRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage,
            })
            if (this.props.purchaseIndent.getItemDetailsValue.data.currPage <= this.props.purchaseIndent.getItemDetailsValue.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    hl3Name: this.props.department,

                    itemType: this.props.itemType,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.getItemDetailsValueRequest(data)
            }

        }
    }

    onSearch(e) {


        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,

                hl3Code: this.props.hl3Code,
                hl3Name: this.props.department,

                itemType: this.props.itemType,
                search: this.state.search,
                searchBy: this.state.searchBy
            }
            this.props.getItemDetailsValueRequest(data)

        }
    }

    onsearchClear() {
        this.setState(
            {
                search: "",
                type: "",
                no: 1
            })
        var data = {
            type: "",
            no: 1,
            hl3Code: this.props.hl3Code,
            hl3Name: this.props.department,

            itemType: this.props.itemType,
            search: "",
            searchBy: this.state.searchBy

        }
        this.props.getItemDetailsValueRequest(data)

        document.getElementById("searchArticle").focus()
    }

    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.vendorDesignData.length != 0) {
                    this.setState({

                        vendorDesignVal: this.state.vendorDesignData[0].vendorDesign,


                    })
                    this.selectedData(this.state.vendorDesignData[0].vendorDesign)
                    // let scode= this.state.supplierState[0].code
                    window.setTimeout(() => {
                        document.getElementById("searchArticle").blur()
                        document.getElementById(this.state.vendorDesignData[0].vendorDesign).focus()
                    }, 0)
                } else {

                    window.setTimeout(() => {
                        document.getElementById("searchArticle").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            } else if (e.target.value != "") {
                window.setTimeout(() => {
                    document.getElementById("searchArticle").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if(e.key==="ArrowDown"){
                 if (this.state.vendorDesignData.length != 0) {
                    this.setState({

                        vendorDesignVal: this.state.vendorDesignData[0].vendorDesign,


                    })
                    this.selectedData(this.state.vendorDesignData[0].vendorDesign)
                    // let scode= this.state.supplierState[0].code
                    window.setTimeout(() => {
                        document.getElementById("searchArticle").blur()
                        document.getElementById(this.state.vendorDesignData[0].vendorDesign).focus()
                    }, 0)
                } else {

                    window.setTimeout(() => {
                        document.getElementById("searchArticle").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById(this.state.vendorDesignData[0].vendorDesign).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if(e.key ==="Enter"){
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone(e);
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.closeArticleModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("closeButton").blur()
                document.getElementById("searchArticle").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.piArticle.length != 0) {
                this.setState({

                    vendorDesignVal: this.state.vendorDesignData[0].vendorDesign,
                })
                this.selectedData(this.state.vendorDesignData[0].vendorDesign)
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById(this.state.vendorDesignData[0].vendorDesign).focus()
                }, 0)
            } else {
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }
    render() {
        const { search
          } = this.state;

        return (

            <div className={this.props.vendorDesignAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.vendorDesignAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.vendorDesignAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.vendorDesignAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT VENDOR DESIGN</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select vendor Design from below records</p>
                                        </li>
                                    </ul>
                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByvd" name="searchByvd" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                       
                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={this.state.search} id="searchArticle" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onClick={(e) => this.onSearch(e)}>FIND
                                                    <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                        <li className="float_right" >
                                            <label>
                                                {this.state.search != "" && (this.state.type == 1 || this.state.type == "") ?
                                                    <button type="button" className="clearbutton" id="clearButton" onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                    : <button type="button" className="clearbutton btnDisabled" >CLEAR</button>
                                                }
                                            </label>
                                        </li>

                                    </ul>


                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Vendor Design</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.vendorDesignData.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.vendorDesignData.map((data, key) => (

                                                    <tr key={key} onClick={() => this.onSelectedData(`${data.vendorDesign}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" checked={`${data.vendorDesign}` == this.state.vendorDesignVal} id={data.id}  readOnly/>
                                                            <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.vendorDesign}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onClick={() => this.onDone()}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" onClick={() => this.props.closeVendorModal()}>Close</button>
                                            </label>
                                        </li>

                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}



                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}

                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>


        );
    }
}

export default vendorDesignModal;