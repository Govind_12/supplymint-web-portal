import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";

class OtbArticleModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef()
        this.state = {
            search: '',  
            currPage: 1,
            maxPage: 1,
            articleSelectedList: [],
            articleData: [],  
            value: [],  
            articleDataList: [],
            articleDisplayList: [],
            isCross: false,
            selectedData: false,
        }

    }

    componentDidMount() {
        console.log('otbarticle')
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
        this.setState({
            value: this.props.articleId,  
            articleDataList: this.props.aritcleValue,
            articleDisplayList: this.props.articleInput,
            articleSelectedList: this.props.articleSelectedList,
        })

    }

    componentWillReceiveProps(nextProps, prevState) {
        if (nextProps.purchaseIndent.poArticle.isSuccess){
            this.setState({
                currPage: nextProps.purchaseIndent.poArticle.data.currPage,                
                maxPage: nextProps.purchaseIndent.poArticle.data.maxPage,
                articleData: nextProps.purchaseIndent.poArticle.data.resource,
            }  )      
        }
    }

    onCheckbox(name, code, mrp, idd, rsp) {
        let values = [...this.state.value];
        let articleDataList = [...this.state.articleDataList];
        let articleDisplayList = [...this.state.articleDisplayList];
        let articleSelectedList = [...this.state.articleSelectedList];

        if (values.includes(name+mrp)) {
            var index = values.indexOf(name+mrp);

            if (index > -1) {
                values.splice(index, 1);
                articleDataList.splice(index, 1);
                articleDisplayList.splice(index, 1);
                articleSelectedList.splice(index, 1)
                this.setState({
                    value: values,
                    articleDataList: articleDataList,
                    articleDisplayList: articleDisplayList,
                    articleSelectedList: articleSelectedList,
                })

            }

        } else {
            values.push(name+mrp)
            let payload = {
                hl4Code: parseInt(code),
                mrp: parseInt(mrp)
            }
            articleDataList.push(payload)
            let data = {
                name: name,
                mrp: mrp
            }
            articleDisplayList.push(data)
            let articleData = {
                hl4Code: code,
                hl4Name: name,
                id: idd,
                mrp: mrp,
                rsp: rsp,
            }
            articleSelectedList.push(articleData)
            this.setState({
                value: values,
                articleDataList: articleDataList,
                articleDisplayList: articleDisplayList,
                articleSelectedList: articleSelectedList,
            })

        }

    }


    
            
    handleChange(e) {
        this.setState({
            search: e.target.value,
            isCross: true,
        })
        if(e.target.value == ''){
            this.setState({
                isCross: false
            })
        }
    }
    
    onsearchClear = () =>{
        this.setState({
            isCross: false,
            search: ''
        }, () => {
            let payload = {
                no: 1,
                type: 1,
                hl4Code: "",
                hl4Name: "",
                hl1Name: this.props.hierarchy['Division'].name,
                hl1Code: this.props.hierarchy['Division'].code,
                hl2Code: this.props.hierarchy['Section'].code,
                hl2Name: this.props.hierarchy['Section'].name,
                hl3Name: this.props.hierarchy['Department'].name,
                hl3Code: this.props.hierarchy['Department'].code,
                divisionSearch: "",
                sectionSearch: "",
                departmentSearch: "",
                articleSearch: this.state.search,
                basedOn: "article",
                //supplier: this.state.slCode,
                mrpSearch: "",
                isMrp: false,
                mrpSearch: "",
                mrpRangeSearch: false,
                isArticleWithMrp:true
            }
            this.props.poArticleRequest(payload)
        })        
    }

    onSearch = (e) =>{        
        if(e.keyCode == 13 || e.target.id == 'search'){
            e.preventDefault();
            let payload = {
                no: 1,
                type: this.state.search == '' ? 1 : 3,
                hl4Code: "",
                hl4Name: "",
                hl1Name: this.props.hierarchy['Division'].name,
                hl1Code: this.props.hierarchy['Division'].code,
                hl2Code: this.props.hierarchy['Section'].code,
                hl2Name: this.props.hierarchy['Section'].name,
                hl3Name: this.props.hierarchy['Department'].name,
                hl3Code: this.props.hierarchy['Department'].code,
                divisionSearch: "",
                sectionSearch: "",
                departmentSearch: "",
                articleSearch: this.state.search,
                basedOn: "article",
                //supplier: this.state.slCode,
                mrpSearch: "",
                isMrp: false,
                mrpSearch: "",
                mrpRangeSearch: false,
                isArticleWithMrp:true
            }
            this.props.poArticleRequest(payload)
        }
    }

    page = (e) => {
        let number = this.state.currPage;
        if(e.target.id == 'first'){
            number = 1;
        } else if(e.target.id == 'prev'){
            number = number -1;
        } else if(e.target.id == 'next'){
            number = number +1;
            console.log('next', number)
        }
        let payload = {
            no: number,
            type: this.state.search == '' ? 1 : 3,
            hl4Code: "",
            hl4Name: "",
            hl1Name: this.props.hierarchy['Division'].name,
            hl1Code: this.props.hierarchy['Division'].code,
            hl2Code: this.props.hierarchy['Section'].code,
            hl2Name: this.props.hierarchy['Section'].name,
            hl3Name: this.props.hierarchy['Department'].name,
            hl3Code: this.props.hierarchy['Department'].code,
            divisionSearch: "",
            sectionSearch: "",
            departmentSearch: "",
            articleSearch: this.state.search,
            basedOn: "article",
            //supplier: this.state.slCode,
            mrpSearch: "",
            isMrp: false,
            mrpSearch: "",
            mrpRangeSearch: false,
            isArticleWithMrp:true
        }
        this.props.poArticleRequest(payload)
}

    changeSelectedData = (event) => {
        this.setState({
            selectedData: !this.state.selectedData
        })
    }

    removeSelected = (id) => {
        let articleDataList = [...this.state.articleDataList];
        let articleDisplayList = [...this.state.articleDisplayList];
        let value = [...this.state.value];
        let articleSelectedList = [...this.state.articleSelectedList]

        let index = value.findIndex((_) => _ == id)
        articleDataList.splice(index, 1)
        articleDisplayList.splice(index, 1)
        value.splice(index, 1)
        articleSelectedList.splice(index, 1)
        this.setState({ articleDataList, value, articleDisplayList, articleSelectedList })
    }

    render() {   
        console.log('article length', this.state.articleDataList, this.state.value, this.state.articleDisplayList)     
        const { selectedData } = this.state;
        
        return (
            <div className={this.props.isArticleOpen ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.isArticleOpen ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.isArticleOpen ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.isArticleOpen ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12 piClorModal">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list-inline width_100 m-top-15 modelHeader">
                                        <li>
                                            <label className="select_name-content">SELECT ARTICLE</label>
                                            <p className="para-content">You can select multiple article from below records</p>
                                        </li>


                                    </ul>

                                    <ul className="list-inline width_100 chooseDataModal">
                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn addNewWidth m-top-5">
                                            
                                            <li className="width100">
                                                
                                                <div className="gen-new-pi-history">
                                                    <form>
                                                        <input type="search" autoComplete="off" autoCorrect="off" className="search_bar" ref={this.textInput} onKeyDown={this.onSearch} value={this.state.search} id="articleSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                        <button className="searchWithBar" id = 'search' onClick={(e) => this.onSearch(e)} >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                                                <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                                            </svg>
                                                        </button>
                                                        {this.state.isCross ? <span className="closeSearch" id='search' onClick={(e) => this.onsearchClear(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                                                    </form>
                                                </div>
                                                
                                            </li>
                                                </div>
                                        
                                        <div className="nph-switch-btn">
                                            <label className="tg-switch" >
                                                <input type="checkbox" checked={selectedData} onChange={this.changeSelectedData} />
                                                <span className="tg-slider tg-round"></span>
                                            </label>
                                            {selectedData && <label className="nph-wbtext">Selected data</label>}
                                            {!selectedData && <label className="nph-wbtext"> All data</label>}
                                        </div>                                        
                                    </ul>
                                </div>
                                {!selectedData && <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                <th>Select</th>
                                                    <th>Article Code</th>
                                                    <th>Article Name</th>
                                                    <th>Mrp</th>
                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {
                                                     this.state.articleData.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : 
                                                     this.state.articleData.map((data, key) => (
                                                        // <React.Fragment>
                                                        <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data.hl4Name}`, `${data.hl4Code}`, `${data.mrp}`, `${data.id}`, `${data.rsp}`)}>

                                                            <td>
                                                                <div className="checkBoxRowClick pi-c2">
                                                                    <label className="checkBoxLabel0">
                                                                        <input type="checkBox" checked={this.state.value.includes(`${data.hl4Name}`+`${data.mrp}`)} id={data.hl4Code} onKeyDown={(e) => this.focusDone(e, `${data.code}`)} readOnly />
                                                                        <label className="checkmark1" htmlFor={data.hl4Code} ></label>
                                                                    </label>

                                                                </div>
                                                            </td>
                                                            <td>{data.hl4Code}</td>
                                                            <td>{data.hl4Name}</td>
                                                            <td>{data.mrp}</td>
                                                        </tr>
                                                    )) 
                                                    //  {/* </React.Fragment> */}
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>}
                                {selectedData && <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Article Code</th>
                                                    <th>Article Name</th>
                                                    <th>Mrp</th>
                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {this.state.articleSelectedList.map((data, i) => (
                                                    <React.Fragment key={i}>
                                                        {this.state.value.includes(`${data.hl4Name}` + `${data.mrp}`) &&
                                                            <tr onClick={() => this.removeSelected(`${data.hl4Name}` + `${data.mrp}`)}>
                                                                <td> <div className="checkBoxRowClick pi-c4">
                                                                    <label className="checkBoxLabel0"  >
                                                                        <input type="checkBox" checked={true} id={data.hl4Code} onKeyDown={(e) => this.removeSelected(e, `${data.id}`)} readOnly />
                                                                        <label className="checkmark1" htmlFor={data.hl4Code} ></label>
                                                                    </label>
                                                                </div> </td>
                                                                <td>{data.hl4Code}</td>
                                                                <td>{data.hl4Name}</td>
                                                                <td>{data.mrp}</td>
                                                            </tr>
                                                        }

                                                    </React.Fragment>
                                                ))}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>}
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={() => this.props.articleSubmit(this.state.articleDisplayList, this.state.articleDataList, this.state.value, this.state.articleSelectedList)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={this.props.closeArticleDropDown}>Close</button>
                                            </label>
                                        </li>                                        
                                    </ul>
                                    <div className="pagerDiv pagerWidth75 m0 modalPagination sizeModalPagination">
                                        {!selectedData && <ul className="list-inline pagination paginationWidth40">
                                            {this.state.currPage == 1 || this.state.currPage == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.currPage != 1 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.currPage}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.currPage < this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        );
    }
}

export default OtbArticleModal;