import React from 'react';
import Pagination from '../pagination';

class SetUDFSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exportToExcel: false,
        }
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, ()=> document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () =>{
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="gen-item-udf">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="gvpd-search">
                                    <input type="search"  placeholder="Type to Search..." />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')}  />
                                    <span className="closeSearch" id="clearSearchFilter"><img src={require('../../assets/clearSearch.svg')} /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 p-lr-47">
                    <div className="gen-item-udf-table">
                        <div className="giut-headfix" id="table-scroll">
                        <div className="columnFilterGeneric">
                            <span className="glyphicon glyphicon-menu-left"></span>
                        </div>
                            <table className="table gen-main-table">
                                <thead >
                                    <tr>
                                        <th className="width145"> 
                                            <label>UDF Type</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Display Name</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Order By</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Is PI Compulsory</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Is PI LOV (List of Values)</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Is PO Compulsory</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Is PO LOV (List of Values)</label>
                                            <img src={require('../../assets/headerFilter.svg')} />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFSTRING08</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFSTRING09</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFSTRING10</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFNUM01</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFNUM03</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFNUM04</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFNUM05</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="width145"><label className="bold">SMUDFNUM05</label></td>
                                        <td> 
                                            <input type="text" className="modal_tableBox " />
                                        </td>
                                        <td><input pattern="[0-9]*" maxLength="2" className="inputTable " /></td>
                                        <td >
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab"  /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                        <td>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" /> 
                                                <span className="checkmark1"></span> 
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-12 pad-0" >
                        <div className="new-gen-pagination">
                            <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                    {/* <span className="ngp-total-item">Total Items </span> <span className="bold"></span> */}
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SetUDFSetting;