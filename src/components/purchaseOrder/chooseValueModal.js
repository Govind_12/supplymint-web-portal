import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class ChooseModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            searchMrp: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
            department: "",
            extValue: this.props.extValue,
            cData: [],
            chooseName: this.props.chooseName,
            searchBy: "startWith",
            focusedLi: "",

        }

    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    componentWillMount() {

    }
    componentWillReceiveProps(nextProps) {


        if (nextProps.purchaseIndent.cname.isSuccess) {
            if (nextProps.purchaseIndent.cname.data.resource != null) {
                this.setState({
                    cData: nextProps.purchaseIndent.cname.data.resource,
                    prev: nextProps.purchaseIndent.cname.data.prePage,
                    current: nextProps.purchaseIndent.cname.data.currPage,
                    next: nextProps.purchaseIndent.cname.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.cname.data.maxPage,
                })
            } else {
                this.setState({
                    cData: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }           

        }
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.cname.data.prePage,
                current: this.props.purchaseIndent.cname.data.currPage,
                next: this.props.purchaseIndent.cname.data.currPage + 1,
                maxPage: this.props.purchaseIndent.cname.data.maxPage,
            })
            if (this.props.purchaseIndent.cname.data.currPage != 0) {
                let data = {
                    type: this.props.searchVal == '' ? 1 : 3,
                    no: this.props.purchaseIndent.cname.data.currPage - 1,
                    udfType: this.props.catValue,
                    search: this.props.searchVal,
                    searchBy: "contains"
                }
                this.props.cnameRequest(data);
                setTimeout(() => this.props.openChooseModal(), 1);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.cname.data.prePage,
                current: this.props.purchaseIndent.cname.data.currPage,
                next: this.props.purchaseIndent.cname.data.currPage + 1,
                maxPage: this.props.purchaseIndent.cname.data.maxPage,
            })
            if (this.props.purchaseIndent.cname.data.currPage != this.props.purchaseIndent.cname.data.maxPage) {
                let data = {
                    type: this.props.searchVal == '' ? 1 : 3,
                    no: this.props.purchaseIndent.cname.data.currPage + 1,
                    udfType: this.props.catValue,
                    search: this.props.searchVal,
                    searchBy: "contains"
                }
                this.props.cnameRequest(data);
                setTimeout(() => this.props.openChooseModal(), 1);
            }
        }        
    }

    render() {
        return (           

                  
                <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                        <div className="dropdown-modal-header">
                            <span className="div-col-2">cName</span>
                            <span className="div-col-2">Ext</span>
                        </div>

                    <ul className="dropdown-menu-city-item">
                        {this.state.cData == undefined || this.state.cData.length == 0 ?
                            <li><span>No Data Found</span></li> :
                            this.state.cData.map((data, key) => (
                                <li key={key} onClick={() => this.props.updateCValue(data)} id={key}  >
                                    <span className="vendor-details">
                                        <span className="vd-name div-col-2">{data.header}</span>
                                        <span className="vd-loc div-col-2">{data.ext == "Y" ? 'True' : 'False'}</span>


                                    </span>
                                </li>))}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                            <button className="btn-close" type="button" onClick={(e) => this.props.closeChooseModal(e)} id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button> : 
                            <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                     <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button>}
                        </div>
                    </div>
                </div>


        );
    }
}

export default ChooseModal;
