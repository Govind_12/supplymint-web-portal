import React from "react";

import ToastLoader from "../loaders/toastLoader";

class ItemUdfMappingModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            checked: false,
            itemUdfMappingState: [],
            search: "",
            toastLoader: false,
            sectionSelection: "",
            type: 1,
            toastMsg: "",
            code: "",
            name: "",
            value: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

            no: 1,
            itemValue: this.props.itemValue,
            searchBy: "startWith",
            focusedLi: "",
            modalDisplay: false
        }
    }
    componentDidMount() {
        if (this.props.isModalShow || this.props.isModalShow == undefined) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
        document.addEventListener('mousedown', this.handleClickOutsideItem)
    }

    handleClickOutsideItem = (e) => {
        if (this.textInput && !this.textInput.current.contains(e.target) && e.target.id != this.props.focusId) {
            this.props.closeItemUdfModal()
        }
    }

    componentWillUnmount(){
        document.removeEventListener('mousedown', this.handleClickOutsideItem)
    }

    componentWillMount() {
        this.setState({
            itemValue: this.props.itemValue,
            search: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.itemUdfSearch,
            type: this.props.isModalShow || this.props.isModalShow == undefined || this.props.itemUdfSearch == "" ? 1 : 3

        })


        if (this.props.purchaseIndent.itemUdfMapping.isSuccess) {

            if (this.props.purchaseIndent.itemUdfMapping.data.resource != null) {

                this.setState({
                    itemUdfMappingState: this.props.purchaseIndent.itemUdfMapping.data.resource,
                    prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                    current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                    next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
                })
            } else {
                this.setState({
                    itemUdfMappingState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }


    }

    handleChange(e) {
        if (e.target.id == "search") {

            this.setState(
                {
                    search: e.target.value
                },

            );



        } else if (e.target.id == "searchByudf") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.search,
                        description: "",
                        itemUdfType: this.props.itemUdfType,
                        ispo: true,

                        hl3Code: this.props.departmentCode,
                        hl4Code: this.props.itemArticleCode,
                        hl4Name: this.props.articleName,
                        searchBy: this.state.searchBy,
                    }
                    this.props.itemUdfMappingRequest(data);
                }
            })
        }

    }

    onSearch(e) {

        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter data on search input",
                toastLoader: true,

            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {

            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.search,
                description: "",
                itemUdfType: this.props.itemUdfType,
                ispo: true,

                hl3Code: this.props.departmentCode,
                hl4Code: this.props.itemArticleCode,
                hl4Name: this.props.articleName,
                searchBy: this.state.searchBy,



            }
            this.props.itemUdfMappingRequest(data)



        }

    }
    onsearchClear() {

        if (this.state.type == 3) {
            var data = {
                type: 1,
                no: 1,
                search: this.state.search,
                description: "",
                itemUdfType: this.props.itemUdfType,
                ispo: true,

                hl3Code: this.props.departmentCode,
                hl4Code: this.props.itemArticleCode,
                hl4Name: this.props.articleName,
                searchBy: this.state.searchBy,
            }
            this.props.itemUdfMappingRequest(data)
        }
        this.setState(
            {
                search: "",
                sectionSelection: "",
                type: "",
                no: 1,
                value: ""
            })
        document.getElementById("search").focus()
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.itemUdfMapping.isSuccess) {

            if (nextProps.purchaseIndent.itemUdfMapping.data.resource != null) {

                this.setState({
                    itemUdfMappingState: nextProps.purchaseIndent.itemUdfMapping.data.resource,
                    prev: nextProps.purchaseIndent.itemUdfMapping.data.prePage,
                    current: nextProps.purchaseIndent.itemUdfMapping.data.currPage,
                    next: nextProps.purchaseIndent.itemUdfMapping.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.itemUdfMapping.data.maxPage,
                })
            } else {
                this.setState({
                    itemUdfMappingState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

            if (window.screen.width > 1200) {
                if (this.props.isModalShow || this.props.isModalShow == undefined) {


                    document.getElementById("search").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.itemUdfMapping.data.resource != null) {
                        this.setState({
                            focusedLi:"udf"+ nextProps.purchaseIndent.itemUdfMapping.data.resource[0].description,
                            search: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.itemUdfSearch,
                            type: this.props.isModalShow || this.props.isModalShow == undefined || this.props.itemUdfSearch == "" ? 1 : 3

                        })
                        document.getElementById("udf"+ nextProps.purchaseIndent.itemUdfMapping.data.resource[0].description) != null ? document.getElementById("udf"+ nextProps.purchaseIndent.itemUdfMapping.data.resource[0].description).focus() : null
                    }


                }
            }
            this.setState({
                modalDisplay: true
            })
        }

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
            })
            if (this.props.purchaseIndent.itemUdfMapping.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.itemUdfMapping.data.currPage - 1,
                    search: "",
                    description: "",
                    itemUdfType: this.props.itemUdfType,
                    ispo: true,

                    hl3Code: this.props.departmentCode,
                    hl4Code: this.props.itemArticleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy,
                }
                this.props.itemUdfMappingRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
            })
            if (this.props.purchaseIndent.itemUdfMapping.data.currPage != this.props.purchaseIndent.itemUdfMapping.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                    search: "",
                    description: "",
                    itemUdfType: this.props.itemUdfType,
                    ispo: true,

                    hl3Code: this.props.departmentCode,
                    hl4Code: this.props.itemArticleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy,
                }
                this.props.itemUdfMappingRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
            })
            if (this.props.purchaseIndent.itemUdfMapping.data.currPage <= this.props.purchaseIndent.itemUdfMapping.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: "",
                    description: "",
                    itemUdfType: this.props.itemUdfType,
                    ispo: true,

                    hl3Code: this.props.departmentCode,
                    hl4Code: this.props.itemArticleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy,
                }
                this.props.itemUdfMappingRequest(data)
            }

        }
    }


    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }


    selectedData(value) {
        let array = this.state.itemUdfMappingState
        for (let i = 0; i < array.length; i++) {
            if (array[i].description == value) {
                this.setState({
                    itemValue: this.state.itemUdfMappingState[i].description
                }, () => {
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
                })
            }
        }
    }

    onDone(e) {
        var data = {};

        if (this.state.itemValue != undefined || this.state.itemValue != "" || this.props.itemUdfSearch == this.state.search || this.props.itemUdfSearch != this.state.search) {



            data = {
                value: this.state.itemValue,
                itemUdfId: this.props.itemUdfId,
                itemUdfType: this.props.itemUdfType,
                itemFocus: this.props.itemFocus
            }
            this.props.itemUdfUpdate(data);
            this.closeUdf(e)

        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)


        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    closeUdf() {

        this.props.closeItemUdfModal()
        const t = this

        t.setState({
            itemUdfMappingState: [],
            search: "",
            sectionSelection: "",


            type: "",


        })


    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    deselectValue() {
        let data = {
            value: "",
            itemUdfId: this.props.itemUdfId,
            itemUdfType: this.props.itemUdfType
        }

        this.props.updateItemValue()
        this.props.itemUdfUpdate(data);
        window.setTimeout(() => {
            document.getElementById("search").focus()
        })

    }


    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.itemValue != "") {
                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById("deselectButton").focus()
                    }, 0)
                } else {
                    if (this.state.itemUdfMappingState.length != 0) {
                        this.setState({
                            itemValue: this.state.itemUdfMappingState[0].description
                        })
                        let icode = "udf"+ this.state.itemUdfMappingState[0].description
                        window.setTimeout(function () {
                            document.getElementById("search").blur()
                            document.getElementById(icode).focus()
                        }, 0)
                    } else {
                        window.setTimeout(function () {
                            document.getElementById("search").blur()
                            document.getElementById("closeButton").focus()
                        }, 0)
                    }
                }
            }
            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }

        if (e.key === "ArrowDown") {

            if (this.state.itemValue != "") {
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById("deselectButton").focus()
                }, 0)
            } else {
                if (this.state.itemUdfMappingState.length != 0) {
                    this.setState({
                        itemValue: this.state.itemUdfMappingState[0].description
                    })
                    let icode = "udf"+ this.state.itemUdfMappingState[0].description
                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById(icode).focus()
                    }, 0)
                } else {
                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }
        }


        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            const t = this
            window.setTimeout(function () {

                document.getElementById("findButton").blur()
                t.state.itemValue == "" ? document.getElementById("clearButton").focus() :
                    document.getElementById("deselectButton").focus()
            }, 0)
        }

    }
    focusDone(e) {

        if (e.key === "Tab") {
            let icode ="udf"+  this.state.itemUdfMappingState[0].description

            window.setTimeout(function () {


                document.getElementById(icode).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.onDone(e);
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {
            this.closeUdf();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("search").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.itemUdfMappingState.length != 0) {
                this.setState({
                    itemValue: this.state.itemUdfMappingState[0].description
                })
                let icode = "udf"+ this.state.itemUdfMappingState[0].description
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById(icode).focus()
                }, 0)
            } else {
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }

    onDeselectValue(e) {
        if (e.key === "Enter") {
            this.deselectValue();
        }
        if (e.key === "Tab") {
            if (this.state.search == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.itemUdfMappingState.length != 0) {
                    this.setState({
                        itemValue: this.state.itemUdfMappingState[0].description
                    })
                    let icode = "udf"+ this.state.itemUdfMappingState[0].description
                    window.setTimeout(function () {

                        document.getElementById("deselectButton").blur()
                        document.getElementById(icode).focus()
                    }, 0)
                } else {
                    window.setTimeout(function () {

                        document.getElementById("deselectButton").blur()
                        document.getElementById("search").focus()
                    }, 0)
                }

            } else {
                window.setTimeout(function () {

                    document.getElementById("deselectButton").blur()
                    document.getElementById("clearButton").focus()
                }, 0)
            }
        }
    }


    selectLi(e, code) {
        let itemUdfMappingState = this.state.itemUdfMappingState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < itemUdfMappingState.length; i++) {
                if (itemUdfMappingState[i].description == code) {
                    index = i
                }
            }
            if (index < itemUdfMappingState.length - 1 || index == 0) {
                document.getElementById("udf"+ itemUdfMappingState[index + 1].description) != undefined && itemUdfMappingState[index + 1].description != null ? document.getElementById("udf"+ itemUdfMappingState[index + 1].description).focus() : null

                this.setState({
                    focusedLi:"udf"+  itemUdfMappingState[index + 1].description
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < itemUdfMappingState.length; i++) {
                if (itemUdfMappingState[i].description == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById("udf"+ itemUdfMappingState[index - 1].description) != null ? document.getElementById("udf"+ itemUdfMappingState[index - 1].description).focus() : null

                this.setState({
                    focusedLi: "udf"+ itemUdfMappingState[index - 1].description
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.closeUdf(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.itemUdfMappingState.length != 0 ?
                            document.getElementById("udf"+ this.state.itemUdfMappingState[0].description).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.itemUdfMappingState.length != 0) {
                    document.getElementById("udf"+ this.state.itemUdfMappingState[0].description).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeUdf(e)
        }


    }

    render() {
        if (this.state.focusedLi != "" && this.props.itemUdfSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        console.log(this.props.itemFocusId)
        if (this.props.itemFocusId != "" && this.props.itemFocusId != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let modalWindowWidth = document.getElementById('itemSetModal').getBoundingClientRect();
            let position = document.getElementById(this.props.itemFocusId).getBoundingClientRect();
            let leftPosition = position.left;
            let top = 214;
            let newLeft = 0;
            let diff = modalWindowWidth.width - leftPosition;

            if (diff >= modalWidth) {
                newLeft = leftPosition > 140 ? leftPosition - 140 : leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                if (leftPosition < 700) {
                    newLeft = (leftPosition >= 615 && leftPosition < 625) ? (leftPosition - removeWidth - 60) : (leftPosition >= 625 && leftPosition < 670) ? (leftPosition - removeWidth - 20) : (leftPosition - removeWidth - 90);
                }
                else {
                    newLeft = leftPosition - removeWidth - 5;
                }
                // newLeft = leftPosition >= 600 && leftPosition < 700 ? (leftPosition - removeWidth - 60) : (leftPosition - removeWidth - 5);
            }
            console.log("top:" + top, "leftPosition:" + leftPosition, "newLeft:" + newLeft)

            $('#itemUdfModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');

        }
        const { search, sectionSelection } = this.state
        return (

            this.props.isModalShow || this.props.isModalShow == undefined ? <div className={this.props.itemUdfMappingAnimation ? "modal display_block" : "display_none"} id="udfmappingModel">
                <div className={this.props.itemUdfMappingAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.itemUdfMappingAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.itemUdfMappingAnimation ? "modal-content modaludfMapping modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Mapping">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">Select {this.props.itemUdfName == null || this.props.itemUdfName == "null" ? this.props.itemUdfType : this.props.itemUdfName}</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 chooseDataModal">

                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn">

                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByudf" name="searchByudf" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}


                                                    <input type="search" autoComplete="off" autoCorrect="off" ref={this.textInput} onKeyPress={this._handleKeyPress} onKeyDown={this._handleKeyDown} className="search-box" value={search} id="search" onChange={e => this.handleChange(e)} placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div> </div>
                                        <li className="float_right">

                                            <label>
                                                <button type="button" className={this.state.itemValue == "" ? "btnDisabled clearbutton widthAuto m-rgt-20" : "clearbutton widthAuto m-rgt-20"} id="deselectButton" onKeyDown={(e) => this.onDeselectValue(e)} onClick={(e) => this.deselectValue(e)}>DESELECT</button>
                                            </label>
                                            <label>
                                                <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>

                                            </label>
                                        </li>
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th> {this.props.itemUdfName == null || this.props.itemUdfName == "null" ? this.props.itemUdfType : this.props.itemUdfName}</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.itemUdfMappingState == undefined || this.state.itemUdfMappingState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.itemUdfMappingState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.itemUdfMappingState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.description}`)} >
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={"udf"+ data.description} type="radio" name="itemUdfMapping" checked={this.state.itemValue == `${data.description}`} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>

                                                        <td className="pad-lft-8">
                                                            {data.description}
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom m-top-10">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeUdf(e)}>Close</button>
                                            </label>
                                        </li>

                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
                :
                this.state.modalDisplay ? <div ref={this.textInput} className="poArticleModalPosition" id="itemUdfModalPosition">

                    <div className="dropdown-menu-city2 dropdown-menu-vendor" id="pocolorModel">

                        <ul className="dropdown-menu-city-item">
                            {this.state.itemUdfMappingState == undefined || this.state.itemUdfMappingState.length == 0 ? <li><span>No Data Found</span></li> :
                                this.state.itemUdfMappingState.map((data, key) => (
                                    <li key={key} onClick={(e) => this.selectedData(`${data.description}`)} id={"udf"+data.description} className={this.state.itemValue == `${data.description}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.description)}>
                                        <span className="vendor-details">
                                            <span className="vd-name div-col-1">{data.description}</span>

                                        </span>
                                    </li>))}

                        </ul>
                        <div className="gen-dropdown-pagination">
                            <div className="page-close">
                                <button className="btn-close" type="button" onClick={(e) => this.closeUdf(e)} id="btn-close">Close</button>
                                {/* <button className="btn-clear" type="button">Clear</button> */}
                            </div>
                            <div className="page-next-prew-btn margin-180">
                                {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                                </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                                <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                                </button>
                                    : <button className="pnpb-next" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                    </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                            </div>
                        </div>
                    </div>
                </div> : null



        );
    }
}

export default ItemUdfMappingModal;
