import React from "react";
import { CONFIG } from '../../config/index';
import axios from 'axios';
import { throttle, debounce } from 'throttle-debounce';
import UdfMappingModal from "./udfMappingModal";
import SupplierModal from "../purchaseIndent/purchaseVendorModal";
import ToastLoader from "../loaders/toastLoader";
import ArticleModal from "./articleModal";
import PiColorModal from "../purchaseIndent/piColorModal";
import moment from 'moment';
import { getDate } from '../../helper';
import ItemCodeModal from "../purchaseOrder/itemCodeModal";
import PiImageModal from "../purchaseIndent/piImageModal";
import VendorDesignModal from "../purchaseOrder/vendorDesignModal";
import TransporterSelection from "../purchaseIndent/transporterSelection";
import LoadIndent from "./loadIndent";
import PoError from "../loaders/poError";
import AlertNotificationModal from "./alertNotificationModal";
import ItemUdfMappingModal from "./itemUdfMappingModal";
import ConfirmModalPo from "../loaders/confirmModalPo";
import DeleteModalPo from "../loaders/deleteConfirmModal"
import ItemDetailsModal from '../purchaseIndent/itemDetailsModal';
import exclaimIcon from "../../assets/exclain.svg";
import _ from 'lodash';
import OrderNumberModal from "./orderNumber";
import SetVendorModal from "./setVendorModal";
import SetDepartmentModal from "./setDepartmentModal";
import ResetAndDelete from "../loaders/resetAndDelete";
import copyIcon from "../../assets/copy-icon-copy.svg";
import HsnCodeModal from "./hsnCodeModal";
import SiteModal from "./siteModal";
import IcodeModalInner from "./poIcodeModal2";
import IcodeModal from "./poIcodeModal"
import NewIcodeModal from "./newIcodemodal";
import CityModal from "./cityModal";
import FilterLoader from '../loaders/filterLoader';
import DiscountModal from "./discountModal"
import PiSizeModal from "../purchaseIndent/piSizeModal";
class PurchaseOrderForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            desginSectionId: "",
            calculatedMarginExact: "",
            isVendorDesignNotReq: false,
            displayOtb: true,
            transporterValidation: true,
            addRow: false,
            isRequireSiteId: false,
            itemDetailCode: "",
            dateValidationRes: false,
            validFromValidation: false,
            valtdToValidation: false,
            restrictedStartDate: "",
            restrictedEndDate: "",
            isMRPEditable: false,
            isRSP: false,
            discountMrp: "",
            selectedDiscount: "",
            discountGrid: "",
            discountModal: false,
            loader: false,
            clickedOneTime: false,
            cityModal: false,
            cityModalAnimation: false,
            city: "",
            cityerr: false,
            isCityExist: false,
            itemudf1Validation: false,
            itemudf2Validation: false,
            itemudf3Validation: false,
            itemudf4Validation: false,
            itemudf5Validation: false,
            itemudf6Validation: false,
            itemudf7Validation: false,
            itemudf8Validation: false,
            itemudf9Validation: false,
            itemudf10Validation: false,
            itemudf11Validation: false,
            itemudf12Validation: false,
            itemudf13Validation: false,
            itemudf14Validation: false,
            itemudf15Validation: false,
            itemudf16Validation: false,
            itemudf17Validation: false,
            itemudf18Validation: false,
            itemudf19Validation: false,
            itemudf20Validation: false,

            udf1Validation: false,
            udf2Validation: false,
            udf3Validation: false,
            udf4Validation: false,
            udf5Validation: false,
            udf6Validation: false,
            udf7Validation: false,
            udf8Validation: false,
            udf9Validation: false,
            udf10Validation: false,

            udf11Validation: false,
            udf12Validation: false,
            udf13Validation: false,
            udf14Validation: false,
            udf15Validation: false,
            udf16Validation: false,
            udf17Validation: false,
            udf18Validation: false,
            udf19Validation: false,
            udf20Validation: false,

            cat1Validation: false,
            cat2Validation: false,
            cat3Validation: false,
            cat4Validation: false,
            cat5Validation: false,
            cat6Validation: false,

            desc1Validation: false,
            desc2Validation: false,
            desc3Validation: false,
            desc4Validation: false,
            desc5Validation: false,
            desc6Validation: false,

            isDiscountAvail: false,
            isDiscountMap: false,
            isArticleSeparated: false,
            oldValue: "",
            multipleErrorpo: false,

            lineError: [],
            focusedQty: {
                id: "",
                qty: ""
            },
            focusedObj: {
                type: "",
                rowId: "",
                cname: "",
                value: "",
                radio: "",
                finalRate: "",
                colorObj: {
                    color: [],
                    colorList: []
                }

            },
            copingLineItem: [],
            deleteGridId: "",
            deleteSetNo: "",
            deleteConfirmModal: false,
            lineItemChange: false,
            saveMarginRule: "",
            poItemBarcodeData: {},
            icodeModalAnimation: false,
            itemBarcodeModal: false,
            itemcodeerr: false,
            itemCodeList: [],
            colorListValue: [],
            isAdhoc: false,
            isIndent: false,
            isSetBased: false,
            isHoldPo: false,
            isPOwithICode: false,
            poWithUpload: false,
            copyColor: "false",
            focusImage: "",
            focusState: "",
            rateFocus: "",
            focusId: "",
            itemFocus: "",
            itemCodeId: "",
            section3ColorId: "",
            mappingId: "",
            onFieldSite: false,
            siteName: "",
            siteCode: "",
            siteNameerr: false,
            siteModal: false,
            siteModalAnimation: false,
            itemUdfExist: "false",
            isUDFExist: "false",
            isSiteExist: "false",
            isOtbValidation: "false",
            onFieldhsn: false,
            hsnRowId: "",
            hsnCode: "",
            markUpYes: false,
            tradeGrpCode: "",
            hsnModal: false,
            hsnModalAnimation: false,
            hsnerr: false,
            totalOtb: "",
            changeLastIndate: true,
            descSix: "",
            minDate: "",
            maxDate: "",
            slCityName: "",
            deleteMrp: "",
            deleteOtb: "",
            desc6: "",
            loadIndentId: "",
            imageRowId: "",
            imageState: {},
            imageModal: false,
            imageModalAnimation: false,
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            gstInNo: "",
            itemValue: "",
            RadioChange: "",
            gridFirst: false,
            gridSecond: false,
            gridThird: false,
            gridFourth: false,
            gridFivth: false,
            itemUdfName: "",
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: false,
            itemUdfId: "",
            hsnSacCode: "",
            itemUdfType: "",
            vendorId: "",
            articleName: "",
            udfRowMapping: [],
            udfMappingData: [],
            colorNewData: [],
            mrp: "",
            itemName: "",
            trasporterModal: false,
            sizeMapping: [],
            transporterAnimation: false,
            transporterCode: "",
            transporterName: "",
            loadIndentModal: false,
            loadIndentAnimation: false,
            purchaseTermState: [],
            itemModal: false,
            itemModalAnimation: false,
            toastLoader: false,
            poErrorMsg: false,
            toastMsg: "",
            errorMassage: "",
            poItemcodeData: [],
            term: "",
            udfMapping: false,
            udfMappingAnimation: false,
            udfSetting: false,
            udfSettingAnimation: false,
            poItems: false,
            poItemsAnimation: false,
            poColor: false,
            indentRadio: "",
            indentRadioerr: false,
            poDate: "",
            dateUnformatted: "",
            poDateerr: false,
            loadData: this.props.purchaseIndent.loadIndent.data.resource,
            loadIndent: "",
            loadIndenterr: false,
            poValidFrom: "",
            lastInDate: "",
            lastAsnDate: "",
            article: "",
            division: "",
            divisionCode: "",
            department: "",
            departmentCode: "",
            section: "",
            sectionCode: "",
            slCode: "",
            slAddr: "",
            slName: "",
            item: "",
            text: "",
            vendor: "",
            leadDays: "",
            transporter: "",
            startRange: "",
            endRange: "",
            supplier: "",
            vendorMrpPo: "",
            supplierModal: false,
            supplierModalAnimation: false,
            articleModalAnimation: false,
            articleModal: false,
            codeRadio: "Adhoc",
            open: false,
            search: "",
            colorData: [],
            colorRow: "",
            indentLoad: true,
            isDescriptionChecked: false,
            isSecTwoDescriptionChecked: false,
            isUdfDescriptionChecked: false,
            isItemUdfDescriptionChecked: false,
            termCode: "",
            termName: "",
            poAmount: 0,
            poQuantity: 0,
            vendorPoState: [],
            udfType: "",
            udfName: "",
            udfRow: "",
            colorCode: "",
            colorModal: false,
            colorModalAnimation: false,
            itemUdfMappingState: [],
            supplierCode: "",
            // __________________________validation error states_________________
            combineRow: false,
            poValidFromerr: false,
            lastInDateerr: false,
            lastAsnDateerr: false,
            articleerr: false,
            itemerr: false,
            vendorerr: false,
            transportererr: false,
            otbStatus: false,
            setValue: "",
            vendorDesignVal: "",
            indentValue: "",
            colorValue: "",
            flag1: 0,
            flag: false,
            otbArray: [],
            otbValue: {},
            otbValueData: "",
            mrpValueData: "",
            itemDetailName: "",
            poRows: [{
                color: [],
                size: [],
                ratio: [],
                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: ""
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],
                concatFinCharges: [],
                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",
                typeOfBuying: "",
                marginRule: "",
                image: [],
                imageUrl: {},
                containsImage: false,

            }],
            secTwoRows: [{
                discount: {
                    discountType: "",
                    discountValue: ""
                },

                colorChk: false,
                icodeChk: false,
                colorSizeList: [],
                icodes: [],
                itemBarcode: "",
                finalRate: "",


                setHeaderId: "",
                designRowId: 1,
                color: [],
                colorList: [],
                sizeListData: [],
                vendorDesignNo: "",
                size: "",
                setRatio: "",
                option: "",
                setNo: 1,
                total: "",
                setQty: "",
                quantity: "",
                amount: "",
                rate: "",
                ratio: [],
                gst: "",
                finCharges: [],
                tax: "",
                calculatedMargin: "",
                mrk: "",
                otb: "",
                totalQuantity: "",
                totalNetAmt: "",
                totaltax: [],
                totalCalculatedMargin: [],
                totalgst: [],
                totalIntakeMargin: [],
                totalFinCharges: [],
                totalBasic: "",
                totalCalculatedOtb: "",
                gridTwoId: 1,
                catOneCode: "",
                catOneName: "",
                catTwoCode: "",
                catTwoName: "",
                catThreeCode: "",
                catThreeName: "",
                catFourCode: "",
                catFourName: "",
                catFiveCode: "",
                catFiveName: "",
                catSixCode: "",
                catSixName: "",
                descOneCode: "",
                descOneName: "",

                descTwoCode: "",
                descTwoName: "",
                descThreeCode: "",
                descThreeName: "",
                descFourCode: "",
                descFourName: "",
                descFiveCode: "",
                descFiveName: "",
                descSixCode: "",
                descSixName: "",
                itemudf1: "",
                itemudf2: "",
                itemudf3: "",
                itemudf4: "",
                itemudf5: "",
                itemudf6: "",
                itemudf7: "",
                itemudf8: "",
                itemudf9: "",
                itemudf10: "",
                itemudf11: "",
                itemudf12: "",
                itemudf13: "",
                itemudf14: "",
                itemudf15: "",
                itemudf16: "",
                itemudf17: "",
                itemudf18: "",
                itemudf19: "",
                itemudf20: "",
                deliveryDate: "",
                typeOfBuying: "",
                marginRule: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                lineItemChk: false
            }],
            udfRows: [{
                designRowId: 1,
                setUdfNo: 1,
                udfColor: [],
                udfGridId: 1,
            }],
            catDescRows: [{
                vendorDesign: "",
                gridId: 1,
                designRowId: 1,
            }],
            itemUdf: [{
                itemUdfGridId: 1,
                vendorDesign: "",
                designRowId: 1,
            }],
            currentDate: "",
            sizeRows: [],
            vendorDesignModal: false,
            vendorDesignAnimation: false,
            vendorDesignData: [],

            itemDetailsHeader: [],
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            itemType: "",
            itemId: "",
            itemDetailValue: "",
            setBased: "",
            poSetVendorerr: false,
            departmentSeterr: false,
            departmentSet: "",
            poSetVendor: "",
            orderSet: "",
            orderSeterr: false,
            loadIndentTrue: false,
            fourApi: false,
            piKeyData: [],
            descId: "",
            departmentSetBasedAnimation: false,
            departmentModal: false,
            orderNumber: false,
            orderNumberModalAnimation: false,
            SetVendor: false,
            setVendorModalAnimation: false,
            hl3CodeDepartment: "",
            poSetVendorCode: "",
            setDepartment: "",
            resetAndDelete: false,
            itemArticleCode: "",
            errorId: "",
            icodeId: "",
            icodesArray: [],
            openIcodeModal: false,
            icodeValidation: false,
            isMrpRequired: true,
            isRspRequired: true,
            isColorRequired: true,
            isDisplayFinalRate: false,
            isTransporterDependent: true,
            isDisplayMarginRule: true,
            isGSTDisable: false,
            isTaxDisable: false,
            isBaseAmountActive: false,
            isModalShow: true,
            orderNo: "",
            sizeModalStatus: false,
            sizeState: [],
            sizeValue: [],
            sizeDataList: [],
            isToggled: false,
            sizeRef: ""
        };
        this.sizeRef = React.createRef();
    }
    // ______________________________________handlechange code start__________________________
    closeItemModal() {
        document.getElementById(this.state.focusId).focus()
        this.setState({
            itemDetailsModal: false,
            itemDetailsModalAnimation: !this.state.itemDetailsModalAnimation,
        })
    }
    updateItemDetails(data) {
        let catDescRows = this.state.catDescRows
        catDescRows.forEach(iDetails => {
            if (iDetails.gridId == Number(data.checkedId)) {
                iDetails.itemDetails.forEach(iList => {
                    if (iList.catdesc == data.type) {
                        iList.value = data.checkedData
                        iList.code = data.checkedCode
                    }
                })
            }
        })
        document.getElementById(this.state.focusId).focus()
        this.setState({
            catDescRows: catDescRows
        })
        setTimeout(() => {
            this.gridSecond()
        }, 1000)

    }
    openVendorModal(id, value) {


        this.setState({
            vendorDesignVal: value,
            vendorId: id,
            vendorDesignModal: true,
            vendorDesignAnimation: true,
            itemType: "vdesign"

        })
        var data = {
            hl3Code: this.state.departmentCode,
            hl3Name: this.state.department,
            itemType: "vdesign",
            no: 1,
            type: 1,
            search: ""
        }

        this.props.getItemDetailsValueRequest(data);


    }
    closeVendorModal() {
        this.setState({
            vendorDesignModal: false,
            vendorDesignAnimation: !this.state.vendorDesignAnimation,
        })
    }
    updateVendorDesign(vendorData) {
        let poRows = this.state.poRows
        let catDescRows = this.state.catDescRows
        let secTwoRows = this.state.secTwoRows
        let itemUdf = this.state.itemUdf
        for (let i = 0; i < poRows.length; i++) {
            if (poRows.gridOneId == vendorData.rowId) {
                poRows[i].vendorDesign = vendorData.value

            }
        }
        for (var l = 0; l < itemUdf.length; l++) {
            if (itemUdf[l].designRowId == vendorData.rowId) {
                itemUdf[l].vendorDesign = vendorData.value
            }
        }
        for (var j = 0; j < catDescRows.length; j++) {
            if (catDescRows[j].designRowId == vendorData.rowId) {
                catDescRows[j].vendorDesign = vendorData.value
            }
        }
        for (var k = 0; k < secTwoRows.length; k++) {
            if (secTwoRows[k].designRowId == vendorData.rowId) {
                secTwoRows[k].vendorDesignNo = vendorData.value
            }
        }
        this.setState({
            poRows: poRows,
            catDescRows: catDescRows,
            secTwoRows: secTwoRows,
            itemUdf: itemUdf
        })

    }
    AddSecTwoRows() {
        let catDescRows = this.state.catDescRows
        let rows = this.state.secTwoRows
        let itemUdf = this.state.itemUdf
        let poRows = this.state.poRows
        let itemDetails = [];

        for (var i = 0; i < rows.length; i++) {
            for (var j = 0; j < catDescRows.length; j++) {
                if (catDescRows[j].designRowId == rows[i].designRowId) {
                    itemDetails = catDescRows[j].itemDetails

                    let cat1 = itemDetails.find(item => {
                        if (item.catdesc == "CAT1") {
                            return item
                        }
                    })
                    let cat2 = itemDetails.find(item => {
                        if (item.catdesc == "CAT2") {
                            return item
                        }
                    })
                    let cat3 = itemDetails.find(item => {
                        if (item.catdesc == "CAT3") {
                            return item
                        }
                    })
                    let cat4 = itemDetails.find(item => {
                        if (item.catdesc == "CAT4") {
                            return item
                        }
                    })
                    let cat5 = itemDetails.find(item => {
                        if (item.catdesc == "CAT5") {
                            return item
                        }
                    })
                    let cat6 = itemDetails.find(item => {
                        if (item.catdesc == "CAT6") {
                            return item
                        }
                    })
                    let desc1 = itemDetails.find(item => {
                        if (item.catdesc == "DESC1") {
                            return item
                        }
                    })
                    let desc2 = itemDetails.find(item => {
                        if (item.catdesc == "DESC2") {
                            return item
                        }
                    })
                    let desc3 = itemDetails.find(item => {
                        if (item.catdesc == "DESC3") {
                            return item
                        }
                    })
                    let desc4 = itemDetails.find(item => {
                        if (item.catdesc == "DESC4") {
                            return item
                        }
                    })
                    let desc5 = itemDetails.find(item => {
                        if (item.catdesc == "DESC5") {
                            return item
                        }
                    })
                    let desc6 = itemDetails.find(item => {
                        if (item.catdesc == "DESC6") {
                            return item
                        }
                    })
                    rows[i].catOneCode = cat1.code
                    rows[i].catOneName = cat1.value
                    rows[i].catTwoName = cat2.value
                    rows[i].catTwoCode = cat2.code
                    rows[i].catThreeCode = cat3.code
                    rows[i].catThreeName = cat3.value
                    rows[i].catFourCode = cat4.code
                    rows[i].catFourName = cat4.value
                    rows[i].catFiveCode = cat5.code
                    rows[i].catFiveName = cat5.value
                    rows[i].catSixCode = cat6.code
                    rows[i].catSixName = cat6.value
                    rows[i].descOneCode = desc1.code
                    rows[i].descOneName = desc1.value
                    rows[i].descTwoCode = desc2.code
                    rows[i].descTwoName = desc2.value
                    rows[i].descThreeCode = desc3.code
                    rows[i].descThreeName = desc3.value
                    rows[i].descFourCode = desc4.code
                    rows[i].descFourName = desc4.value
                    rows[i].descFiveCode = desc5.code
                    rows[i].descFiveName = desc5.value
                    rows[i].descSixCode = desc6.code
                    rows[i].descSixName = desc6.value
                }
            }
            poRows.forEach(r => {
                if (r.gridOneId == rows[i].designRowId) {
                    rows[i].rate = r.rate
                    rows[i].typeOfBuying = r.typeOfBuying
                    rows[i].otb = r.otb
                    rows[i].deliveryDate = r.deliveryDate
                    rows[i].discount = r.discount
                    rows[i].finalRate = r.finalRate
                    rows[i].remarks = r.remarks
                    rows[i].image = r.image
                    rows[i].imageUrl = r.imageUrl
                    rows[i].containsImage = r.containsImage
                    rows[i].mrp = r.mrp
                    rows[i].rsp = r.rsp
                    rows[i].totalBasic = r.basic
                    rows[i].totalCalculatedMargin = r.calculatedMargin
                    rows[i].totalCalculatedOtb = r.otb
                    rows[i].totalFinCharges = r.finCharges
                    rows[i].totalgst = r.gst
                    rows[i].totalIntakeMargin = r.mrk
                    rows[i].totalAmount = r.amount
                    rows[i].totaltax = r.tax
                    rows[i].totalQuantity = r.quantity
                    rows[i].marginRule = r.marginRule
                }
            })
            if (this.state.itemUdfExist == "true") {
                itemUdf.forEach(itemm => {
                    if (itemm.designRowId == rows[i].designRowId) {
                        //  itemm.udfList.forEach(udf=>{
                        let itemudf1 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING01") {
                                return item
                            }
                        })

                        let itemudf2 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING02") {
                                return item
                            }
                        })
                        let itemudf3 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING03") {
                                return item
                            }
                        })
                        let itemudf4 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING04") {
                                return item
                            }
                        })
                        let itemudf5 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING05") {
                                return item
                            }
                        })
                        let itemudf6 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING06") {
                                return item
                            }
                        })
                        let itemudf7 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING07") {
                                return item
                            }
                        })

                        let itemudf8 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING08") {
                                return item
                            }
                        })
                        let itemudf9 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING09") {
                                return item
                            }
                        })
                        let itemudf10 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFSTRING10") {
                                return item
                            }
                        })
                        let itemudf11 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFNUM01") {
                                return item
                            }
                        })
                        let itemudf12 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFNUM02") {
                                return item
                            }
                        })
                        let itemudf13 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFNUM03") {
                                return item
                            }
                        })
                        let itemudf14 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFNUM04") {
                                return item
                            }
                        })
                        let itemudf15 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFNUM05") {
                                return item
                            }
                        })
                        let itemudf16 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFDATE01") {
                                return item
                            }
                        })
                        let itemudf17 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFDATE02") {
                                return item
                            }
                        })
                        let itemudf18 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFDATE03") {
                                return item
                            }
                        })
                        let itemudf19 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFDATE04") {
                                return item
                            }
                        })
                        let itemudf20 = itemm.udfList.find(item => {
                            if (item.cat_desc_udf == "UDFDATE05") {
                                return item
                            }
                        })
                        rows[i].itemudf1 = itemudf1.value
                        rows[i].itemudf2 = itemudf2.value
                        rows[i].itemudf3 = itemudf3.value
                        rows[i].itemudf4 = itemudf4.value
                        rows[i].itemudf5 = itemudf5.value
                        rows[i].itemudf6 = itemudf6.value
                        rows[i].itemudf7 = itemudf7.value
                        rows[i].itemudf8 = itemudf8.value
                        rows[i].itemudf9 = itemudf9.value
                        rows[i].itemudf10 = itemudf10.value
                        rows[i].itemudf11 = itemudf11.value
                        rows[i].itemudf12 = itemudf12.value
                        rows[i].itemudf13 = itemudf13.value
                        rows[i].itemudf14 = itemudf14.value
                        rows[i].itemudf15 = itemudf15.value
                        rows[i].itemudf16 = itemudf16.value
                        rows[i].itemudf17 = itemudf17.value
                        rows[i].itemudf18 = itemudf18.value
                        rows[i].itemudf19 = itemudf19.value
                        rows[i].itemudf20 = itemudf20.value
                    }
                })
            } else {
                rows[i].itemudf1 = ""
                rows[i].itemudf2 = ""
                rows[i].itemudf3 = ""
                rows[i].itemudf4 = ""
                rows[i].itemudf5 = ""
                rows[i].itemudf6 = ""
                rows[i].itemudf7 = ""
                rows[i].itemudf8 = ""
                rows[i].itemudf9 = ""
                rows[i].itemudf10 = ""
                rows[i].itemudf11 = ""
                rows[i].itemudf12 = ""
                rows[i].itemudf13 = ""
                rows[i].itemudf14 = ""
                rows[i].itemudf15 = ""
                rows[i].itemudf16 = ""
                rows[i].itemudf17 = ""
                rows[i].itemudf18 = ""
                rows[i].itemudf19 = ""
                rows[i].itemudf20 = ""
            }
        }
        this.setState({
            secTwoRows: rows,
            poRows: poRows,
            itemUdf: itemUdf,
            combineRow: true
        })
    }
    sizeHandleChange(id, size, e, color) {

        let sizeRows = this.state.secTwoRows
        if (color.length != 0) {
            let c = 0
            let ratio = []
            let setQty = ""
            for (var j = 0; j < sizeRows.length; j++) {
                if (sizeRows[j].gridTwoId == id) {
                    setQty = sizeRows[j].setQty
                    for (var i = 0; i < sizeRows[j].sizeList.length; i++) {
                        if (sizeRows[j].sizeList[i].cname == size) {
                            if (e.target.validity.valid) {

                                sizeRows[j].sizeList[i].ratio = e.target.value
                                this.setState({
                                    lineItemChange: setQty != "" ? true : false
                                })
                            }
                        }

                    }
                    if (this.state.codeRadio == "poIcode") {
                        if (e.target.validity.valid) {
                            sizeRows[j].setRatio = e.target.value
                        }
                    }
                    let list = sizeRows[j].sizeList;
                    let sizeList = [];
                    let sizeIndex = 1
                    for (var i = 0; i < sizeRows[j].sizeList.length; i++) {
                        c += Number(sizeRows[j].sizeList[i].ratio)
                        if (sizeRows[j].sizeList[i].ratio != "") {
                            let ratios = sizeRows[j].sizeList[i].ratio
                            ratio.push(ratios)
                            this.setState({
                                lineItemChange: setQty != "" ? true : false
                            })
                        }
                    }

                    sizeRows[j].total = c * sizeRows[j].option
                    sizeRows[j].ratio = ratio
                    sizeRows[j].quantity = c * sizeRows[j].option * Number(sizeRows[j].setQty)
                    list.forEach(size => {
                        if (size.ratio != "") {
                            let sizeData = {
                                id: sizeIndex++,
                                code: size.code,
                                cname: size.cname,
                                ratio: size.ratio
                            }
                            sizeList.push(sizeData);
                        }
                    })
                    sizeRows[j].sizeListData = sizeList
                }
            }
            this.setState({
                secTwoRows: sizeRows,

            })
        } else {
            this.setState({
                errorId: id,
                poErrorMsg: true,
                errorMassage: "Color is Complusory "
            })
        }
    }
    openItemUdfModal(udfType, name, id, value, idFocus) {
        let catDescRowss = this.state.catDescRows
        let flag = false

        catDescRowss.forEach(po => {
            po.itemDetails.forEach(val => {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                    // if (val.value == "" && (val.catdesc == "CAT2" || val.catdesc == "CAT3" || val.catdesc == "CAT4" || val.catdesc == "DESC3")) {
                    if ((val.catdesc == "CAT1" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT2" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT3" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT4" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC1" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC2" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC3" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC5" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "")) {
                        flag = true
                        return flag
                    }
                } else {
                    if ((val.catdesc == "CAT1" && ((this.state.cat1Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT2" && ((this.state.cat2Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT3" && ((this.state.cat3Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT4" && ((this.state.cat4Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT5" && ((this.state.cat5Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT6" && ((this.state.cat6Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC1" && ((this.state.desc1Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC2" && ((this.state.desc2Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC3" && ((this.state.desc3Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC4" && ((this.state.desc4Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC5" && ((this.state.desc5Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC6" && ((this.state.desc6Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "")) {
                        flag = true
                        return flag
                    }
                }
            })
        })
        if (flag) {
            this.setState({
                errorId: idFocus,
            })
            this.validateCatDescRow();
        } else {
            let data = {
                type: 1,
                no: 1,
                search: "",
                description: "",
                itemUdfType: udfType,
                ispo: true,
                hl3Code: this.state.departmentCode,
                hl4Code: this.state.vendorMrpPo,
                hl4Name: this.state.articleName
            }
            this.props.itemUdfMappingRequest(data)
            this.setState({
                itemUdfMappingModal: true,
                itemUdfMappingAnimation: !this.state.itemUdfMappingAnimation,
                itemUdfId: id,
                itemUdfType: udfType,
                itemArticleCode: this.state.vendorMrpPo,
                itemUdfName: name != null ? name : udfType,
                itemValue: value,
                itemFocus: idFocus
            })
        }
    }
    closeItemUdfModal() {
        this.setState({
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: !this.state.itemUdfMappingAnimation,
        })
        document.getElementById(this.state.itemFocus).focus()
    }

    itemUdfUpdate(data) {
        let itemUdf = this.state.itemUdf
        if (itemUdf != undefined) {
            for (var k = 0; k < itemUdf.length; k++) {
                if (itemUdf[k].itemUdfGridId == Number(data.itemUdfId)) {
                    for (var l = 0; l < itemUdf[k].udfList.length; l++) {
                        if (itemUdf[k].udfList[l].cat_desc_udf == data.itemUdfType) {
                            itemUdf[k].udfList[l].value = data.value
                        }
                    }
                }
            }
            this.setState({
                itemUdf: itemUdf
            })
            document.getElementById(this.state.itemFocus).focus()
            const t = this
            setTimeout(function () {
                t.gridThird();
            }, 1000)
        }
    }
    onBlurVendorDesign(id, e) {
        if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
            let idd = id
            let r = this.state.poRows;
            let catDescRows = this.state.catDescRows
            let secTwoRows = this.state.secTwoRows
            let itemUdf = this.state.itemUdf
            let flag = false
            let vendorDesignNo = []
            if (e.target.id == "vendorDesign") {
                for (let i = 0; i < r.length; i++) {
                    vendorDesignNo.push(r[i].vendorDesign)
                }
                for (let i = 0; i < r.length; i++) {
                    let unique = [];
                    vendorDesignNo.forEach(s => {
                        if (!unique.includes(s)) {
                            flag = false;
                            unique.push(s)
                        }
                        else {
                            flag = true
                        }
                    });
                    if (flag) {
                        if (r[i].gridOneId == idd) {
                            r[i].vendorDesign = ""
                        }
                        for (var j = 0; j < catDescRows.length; j++) {
                            if (catDescRows[j].designRowId == idd) {
                                catDescRows[j].vendorDesign = ""
                            }
                        }
                        for (var k = 0; k < secTwoRows.length; k++) {
                            if (secTwoRows[k].designRowId == idd) {
                                secTwoRows[k].vendorDesignNo = ""
                            }
                        }
                        for (var l = 0; l < itemUdf.length; l++) {
                            if (itemUdf[l].designRowId == idd) {
                                itemUdf[l].vendorDesign = ""
                            }
                        }

                        this.setState({
                            poRows: r,
                            catDescRows: catDescRows,
                            secTwoRows: secTwoRows,
                            itemUdf: itemUdf
                        })
                        this.setState({
                            errorId: id,
                            errorMassage: "Same value not allowed",
                            poErrorMsg: true
                        })
                    }
                }
            }
        }
    }
    handleChangeGridOne(id, e) {
        let idd = id
        let r = this.state.poRows;
        let catDescRows = this.state.catDescRows
        let secTwoRows = this.state.secTwoRows
        let itemUdf = this.state.itemUdf
        if (e.target.id == "vendorDesign") {
            if (this.state.supplier != "") {
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == idd) {
                        r[i].vendorDesign = e.target.value
                    }
                }
                for (var l = 0; l < itemUdf.length; l++) {
                    if (itemUdf[l].designRowId == idd) {
                        itemUdf[l].vendorDesign = e.target.value
                    }
                }
                for (var j = 0; j < catDescRows.length; j++) {
                    if (catDescRows[j].designRowId == idd) {
                        catDescRows[j].vendorDesign = e.target.value
                    }
                }
                for (var k = 0; k < secTwoRows.length; k++) {
                    if (secTwoRows[k].designRowId == idd) {
                        secTwoRows[k].vendorDesignNo = e.target.value
                    }
                }
                this.setState({
                    poRows: r,
                    catDescRows: catDescRows,
                    secTwoRows: secTwoRows,
                    itemUdf: itemUdf
                })
                const t = this
                setTimeout(function () {
                    t.gridFirst();
                }, 1000)
            } else {
                this.setState({
                    errorId: "vendor",
                    errorMassage: "Supplier is mandatory",
                    poErrorMsg: true
                })
            }
        }
        else if (e.target.id == "rsp") {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        r[i].rsp = e.target.value
                    }
                }
                this.setState({
                    poRows: r
                })
            }
        }
        else if (e.target.id == "mrp") {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        r[i].mrp = e.target.value
                    }
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "rate") {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].vendorDesign == "" && !this.state.isVendorDesignNotReq) {
                            this.setState({
                                errorId: "rate",
                                poErrorMsg: true,
                                errorMassage: "Vendor Design is Complusory "
                            })
                        } else if (r[i].mrp == "" && this.state.isMrpRequired) {
                            this.setState({
                                errorId: "rate",
                                poErrorMsg: true,
                                errorMassage: "mrp is Complusory "
                            })
                        } else {
                            r[i].rate = e.target.value
                            r[i].netRate = e.target.value

                        }
                    }
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "netrate") {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].vendorDesign == "") {
                            if (!this.state.isVendorDesignNotReq) {
                                this.setState({
                                    errorId: "netrate",
                                    poErrorMsg: true,
                                    errorMassage: "Vendor Design is Complusory "
                                })
                            }
                        } else if (r[i].mrp == "" && this.state.isMrpRequired) {
                            this.setState({
                                errorId: "netrate",
                                poErrorMsg: true,
                                errorMassage: "mrp is Complusory "
                            })

                        } else {
                            r[i].rate = e.target.value
                            r[i].netRate = e.target.value
                        }
                    }
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "date") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].deliveryDate = e.target.value
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        } else if (e.target.id == 'typeofBuying') {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].typeOfBuying = e.target.value
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        } else if (e.target.id == "remarks") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].remarks = e.target.value
                }
                this.setState({
                    poRows: r
                })
            }
        }
    }
    openItemCode(mrp, id, idFocus) {
        let idd = id
        let mrpp = mrp
        if (this.state.supplier != "") {
            if (this.state.hsnSacCode != "") {
                this.setState({
                    desc6: mrpp,
                    descId: idd,
                    itemModal: true,
                    itemModalAnimation: !this.state.itemModalAnimation,
                    itemCodeId: idFocus
                })
                var data = {
                    code: this.state.vendorMrpPo,
                    search: "",
                    type: 1,
                    no: 1,
                    mrpRangeFrom: this.state.startRange,
                    mrpRangeTo: this.state.endRange
                }
                this.props.poItemcodeRequest(data)
            } else {
                this.setState({
                    errorMassage: "Select HSN or SAC code",
                    poErrorMsg: true,
                    errorId: idFocus,
                })
            }
        } else {
            this.setState({
                errorMassage: "Select Vendor",
                poErrorMsg: true,
                errorId: idFocus,
            })
        }
    }
    updatePoAmount() {
        let c = this.state.secTwoRows
        let amount = 0;
        for (var x = 0; x < c.length; x++) {
            amount += c[x].amount;
        }
        this.setState({
            poAmount: amount
        })
    }
    closeItemmModal() {
        document.getElementById(this.state.itemCodeId).focus()
        this.setState({
            itemModal: false,
            itemModalAnimation: !this.state.itemModalAnimation
        })
    }

    handleChange(id, row, e, targetId) {

        let idd = id
        let p = this.state.secTwoRows;
        let rows = this.state.poRows;
        // if (this.state.sizeMapping.length != 0) {

        if (this.state.codeRadio != "poIcode") {

            if (e.target.id == targetId) {
                let c = 0
                let rate = ""
                let flag = false
                let quantity = ""
                p.forEach(pData => {
                    if (e.target.validity.valid) {
                        if (pData.gridTwoId == idd) {
                            pData.sizeList.forEach(slist => {
                                if (slist.ratio != "" || slist.ratio.length != 0) flag = true;
                            })
                            // if (flag) {
                            pData.setQty = e.target.value
                            pData.quantity = e.target.value * pData.total
                            if (e.target.value != "") {
                                this.setState({
                                    lineItemChange: true
                                })
                            }
                            // quantity = e.target.value * pData.total * pData.option
                            // } else {
                            //     this.setState({
                            //         poErrorMsg: true,
                            //         errorId: idd,
                            //         errorMassage: "Enter ratio of atleast one from the available sizes"
                            //     })
                            // }
                            // if (flag) {
                            //     c += Number(pData.quantity)
                            // }
                        }
                    }
                })
                // rows.forEach(r => {
                //     if (r.gridOneId == row) {
                //         r.quantity = Number(r.quantity) + Number(quantity)
                //     }
                // })

                this.setState({
                    secTwoRows: p,

                    // poQuantity: c,
                    // poRows: rows
                })
                const t = this
                setTimeout(function () {
                    t.gridFourth();
                }, 1000)
            }
        } else {
            if (e.target.id == targetId) {
                p.forEach(pData => {
                    if (e.target.validity.valid) {
                        if (pData.gridTwoId == idd) {
                            if (pData.setRatio != "") {
                                pData.setQty = e.target.value
                                pData.quantity = e.target.value * pData.total
                                if (e.target.value != "") {
                                    this.setState({
                                        lineItemChange: true
                                    })
                                }
                            } else {
                                this.setState({
                                    poErrorMsg: true,
                                    errorId: idd,
                                    errorMassage: "Fill ratio first"
                                })
                            }

                        }
                    }
                })

                this.setState({
                    secTwoRows: p,

                    // poQuantity: c,
                    // poRows: rows
                })
                const t = this
                setTimeout(function () {
                    t.gridFourth();
                }, 1000)
            }

        }
        // }

        // else {
        //     this.setState({
        //         poErrorMsg: true,

        //         errorMassage: "Sizes are not available"
        //     })
        // }
    }
    updateLineItem(id) {

        let idd = id
        let p = this.state.secTwoRows;
        let rows = this.state.poRows;
        let designRow = ""
        let quantity = ""
        let prevQuantity = ""
        p.forEach(pData => {
            if (pData.gridTwoId == idd) {
                designRow = pData.designRowId
                prevQuantity = pData.quantity
                pData.quantity = pData.setQty * pData.total
                quantity = pData.setQty * pData.total
            }
        })
        rows.forEach(r => {
            if (r.gridOneId == designRow) {
                r.quantity = Number(r.quantity) - Number(prevQuantity)
                r.quantity = Number(r.quantity) + Number(quantity)
            }
        })
        this.setState({
            secTwoRows: p,
            poRows: rows
        })
        setTimeout(() => {
            this.updatePoAmountNpoQuantity()
        }, 10)
    }


    handleInputChange(e) {

        if (e.target.id == "lastInDate") {
            // let restrictedStartDate = this.state.restrictedStartDate
            // let restrictedEndDate = this.state.restrictedEndDate
            // let flag = false
            // if (restrictedStartDate != "" && restrictedEndDate != "" && e.target.value != "") {
            //     if (restrictedStartDate <= e.target.value && e.target.value <= restrictedEndDate) {
            //         this.setState({
            //             poErrorMsg: true,
            //             errorMassage: "You are unable to save PO(Purchase Order) for the selected date(" + e.target.value + ")",

            //         })
            //         flag = true

            //     } else {
            //         e.target.placeholder = e.target.value;
            //         this.setState({
            //             lastInDate: e.target.value
            //         }, () => {
            //             this.lastInDate()
            //         })
            //     }} 

            //     else{
            e.target.placeholder = e.target.value;
            this.setState({
                lastInDate: e.target.value
            }, () => {
                this.lastInDate()
            })
        }
        //    if(!flag){
        if (e.target.value != "") {
            var d = new Date(e.target.value);
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            var lastinDateValue = moment(new Date(year, month, day)).format("YYYY-MM-DD")
            document.getElementById("lastInDate").placeholder = lastinDateValue
            this.setState({
                lastInDate: lastinDateValue,
                maxDate: lastinDateValue,
            }, () => {
                this.lastInDate()
            })
            // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.vendorMrpPo != "") {

            let poRows = this.state.poRows
            if (poRows[0].vendorMrp != "" && this.state.isMrpRequired && this.state.displayOtb) {
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];
                const date = new Date(e.target.value);
                var year = date.getFullYear()
                var monthName = (monthNames[date.getMonth()]);
                let dataa = {
                    rowId: poRows[0].gridOneId,
                    articleCode: this.state.vendorMrpPo,
                    mrp: poRows[0].vendorMrp,
                    month: monthName,
                    year: year
                }
                this.setState({
                    changeLastIndate: true
                })
                this.props.otbRequest(dataa)
            }
            for (let i = 0; i < poRows.length; i++) {
                poRows[i].deliveryDate = e.target.value
            }
            this.setState({
                poRows: poRows
            })
            // }
            // else if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo") && this.state.vendorMrpPo != "") {

            //     this.onClear();
            //     this.setState({
            //         orderSet: ""
            //     })

            // }
            // }
            //    }
        }
        else if (e.target.id == "loadIndent") {
            this.setState({
                loadIndent: e.target.value,
                indentLoad: true
            })
        }
        else if (e.target.id == "Adhoc") {
            this.setState({
                indentLoad: false
            })
        }
    }
    // ___________________-datevalidation_______________________________
    dateVali(e) {
        if (e.target.id == "poValidFrom") {
            let restrictedStartDate = this.state.restrictedStartDate
            let restrictedEndDate = this.state.restrictedEndDate
            let flag = false
            if (restrictedStartDate != "" && restrictedEndDate != "" && e.target.value != "") {
                if (restrictedStartDate <= e.target.value && e.target.value <= restrictedEndDate) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "You are unable to save PO(Purchase Order) between " + moment(restrictedStartDate).format("DD-MMM-YYYY") + " to " + moment(restrictedEndDate).format("DD-MMM-YYYY"),

                    })
                    flag = true

                } else {
                    e.target.placeholder = e.target.value,
                        this.setState({
                            poValidFrom: e.target.value
                        }, () => {
                            this.poValid();
                            this.lastInDate();
                        });
                }
            } else {
                e.target.placeholder = e.target.value,
                    this.setState({
                        poValidFrom: e.target.value
                    }, () => {
                        this.poValid();
                        this.lastInDate();
                    });
            }


            if (!flag) {
                let poRows = this.state.poRows
                if (e.target.value != "") {
                    let lInDate = new Date(e.target.value)
                    lInDate.setDate(lInDate.getDate() + 14)
                    let mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                    let day = ("0" + lInDate.getDate()).slice(-2);
                    let yr = lInDate.getFullYear()
                    let finalLInDate = [yr, mnth, day].join("-")
                    var d = new Date(e.target.value);
                    var year = d.getFullYear();
                    var month = d.getMonth();
                    var day = d.getDate();
                    var c = moment(new Date(year, month, day)).format("YYYY-MM-DD")
                    document.getElementById("lastInDate").placeholder = finalLInDate
                    this.setState({
                        lastInDate: finalLInDate,
                        minDate: getDate(),
                        maxDate: finalLInDate,
                    }, () => {
                        this.lastInDate();
                    });

                    // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.vendorMrpPo != "") {

                    if (poRows[0].vendorMrp != "" && this.state.isMrpRequired && this.state.displayOtb) {
                        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                        ];
                        const date = new Date(finalLInDate);
                        var year = date.getFullYear()
                        var monthName = (monthNames[date.getMonth()]);
                        let dataa = {
                            rowId: poRows[0].gridOneId,
                            articleCode: this.state.vendorMrpPo,
                            mrp: poRows[0].vendorMrp,
                            month: monthName,
                            year: year
                        }
                        this.setState({
                            changeLastIndate: true
                        })
                        this.props.otbRequest(dataa)
                    }
                    for (let i = 0; i < poRows.length; i++) {
                        poRows[i].deliveryDate = finalLInDate
                    }
                    this.setState({
                        poRows: poRows
                    })
                    // }

                    // else if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo") && this.state.vendorMrpPo != "") {

                    //     this.onClear();
                    //     this.setState({
                    //         orderSet: ""
                    //     })

                    // } 
                }
                else {
                    //  this.onClear();
                    // this.setState({
                    //     lastInDate: "",
                    //      orderSet: ""
                    // })
                    document.getElementById("lastInDate").placeholder = "Last in date"
                }
            }

        }

    }
    openUdfMappingModal(code, name, id, value, idFocus) {
        let secTwoRows = this.state.secTwoRows
        let flag = false
        secTwoRows.forEach(s => {
            flag = (s.vendorDesignNo == "" && !this.state.isVendorDesignNotReq) || s.setQty == "" || s.color.length == 0 ? true : false
        })
        if (flag) {
            this.setState({
                errorId: idFocus,
            })
            this.validateLineItem()
        }
        else {
            let udfType = code;
            let udfName = name == null ? code : name;
            let idd = id;
            var data = {
                no: 1,
                type: 1,
                search: "",
                udfType: udfType,
                code: "",
                name: "",
                ispo: true
            }
            this.props.udfTypeRequest(data);
            this.setState({
                setValue: value,
                udfType: udfType,
                udfName: udfName,
                udfRow: idd,
                udfMapping: true,
                udfMappingAnimation: !this.state.udfMappingAnimation,
                mappingId: idFocus
            });
        }
    }
    closeUdf() {
        document.getElementById(this.state.mappingId).focus()
        this.setState({
            udfMapping: false,
            udfMappingAnimation: !this.state.udfMappingAnimation
        });
    }
    updateUdf(data) {
        let udfRows = this.state.udfRows
        if (udfRows != undefined) {
            for (var i = 0; i < udfRows.length; i++) {
                if (udfRows[i].udfGridId == data.udfRow) {
                    for (var j = 0; j < udfRows[i].setUdfList.length; j++) {
                        if (udfRows[i].setUdfList[j].udfType == data.udfType) {
                            udfRows[i].setUdfList[j].value = data.name
                        }
                    }
                }
            }
        }
        document.getElementById(this.state.mappingId).focus()
        this.setState({
            udfRows: udfRows
        })
        const s = this
        setTimeout(function () {
            s.gridFivth();
        }, 10)
    }
    setCorrect(value) {
        var date = new Date(value);
        var month = date.getMonth();
        var day = date.getDate();
        var year = date.getFullYear();
        let dateFormat = [day, month, year].join("-")

        return dateFormat
    }
    // ____________________________________SUPPLIERMODAL______________________
    openSupplier(e, id) {
        if (this.state.vendorMrpPo != "") {
            if (this.state.isCityExist == true) {
                if (this.state.city != "") {
                    var data = {
                        no: 1,
                        type: "",
                        slCode: "",
                        name: "",
                        address: "",
                        department: this.state.department,
                        departmentCode: this.state.departmentCode,
                        siteCode: this.state.siteCode,
                        city: this.state.city
                    }
                    this.props.supplierRequest(data);
                    this.setState({
                        supplierModal: true,
                        supplierModalAnimation: !this.state.supplierModalAnimation

                    });
                } else {


                    this.setState({
                        errorId: "city",
                        errorMassage: "Select city",
                        poErrorMsg: true
                    })
                }
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            } else {
                var data = {
                    no: 1,
                    type: "",
                    slCode: "",
                    name: "",
                    address: "",
                    department: this.state.department,
                    departmentCode: this.state.departmentCode,
                    siteCode: this.state.siteCode,
                    city: ""
                }
                this.props.supplierRequest(data);
                this.setState({
                    supplierModal: true,
                    supplierModalAnimation: !this.state.supplierModalAnimation

                });
            }

        } else {
            this.setState({
                errorId: id,
                errorMassage: "Select Article",
                poErrorMsg: true
            })
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onCloseSupplier(e) {
        this.setState({
            supplierModal: false,
            supplierModalAnimation: !this.state.supplierModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
        document.getElementById("vendor").focus()
    }
    componentDidMount() {
        let lInDate = ""
        let mnth = ""
        let day = ""
        let yr = ""
        let finalLInDate = ""
        if (this.state.codeRadio == "raisedIndent") {
            this.setState({
                poValidFrom: getDate(),
                currentDate: getDate()
            })

            document.getElementById('poValidFrom').placeholder = getDate();
            lInDate = new Date(getDate())
            lInDate.setDate(lInDate.getDate() + 14)
            mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
            day = ("0" + lInDate.getDate()).slice(-2);
            yr = lInDate.getFullYear()
            finalLInDate = [yr, mnth, day].join("-")
            document.getElementById("lastInDate").placeholder = finalLInDate
            this.setState({
                lastInDate: finalLInDate,
                minDate: getDate(),
                maxDate: finalLInDate
            })
        } else {
            if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" || this.state.codeRadio === "Adhoc") && sessionStorage.getItem("poValidFrom") != null) {
                this.setState({
                    poValidFrom: sessionStorage.getItem("poValidFrom"),
                    currentDate: getDate()
                })

                document.getElementById('poValidFrom').placeholder = sessionStorage.getItem("poValidFrom");
                lInDate = new Date(sessionStorage.getItem("poValidFrom"))
                lInDate.setDate(lInDate.getDate() + 14)
                mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                day = ("0" + lInDate.getDate()).slice(-2);
                yr = lInDate.getFullYear()
                finalLInDate = [yr, mnth, day].join("-")
                document.getElementById("lastInDate").placeholder = finalLInDate
                this.setState({
                    lastInDate: finalLInDate,
                    minDate: getDate(),
                    maxDate: finalLInDate
                })
            } else if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" || this.state.codeRadio === "Adhoc") && sessionStorage.getItem("poValidFrom") == null) {
                this.setState({
                    poValidFrom: getDate(),
                    currentDate: getDate()
                })

                document.getElementById('poValidFrom').placeholder = getDate();
                lInDate = new Date(getDate())
                lInDate.setDate(lInDate.getDate() + 14)
                mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                day = ("0" + lInDate.getDate()).slice(-2);
                yr = lInDate.getFullYear()
                finalLInDate = [yr, mnth, day].join("-")
                document.getElementById("lastInDate").placeholder = finalLInDate
                this.setState({
                    lastInDate: finalLInDate,
                    minDate: getDate(),
                    maxDate: finalLInDate
                })

            }
        }
        // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.vendorMrpPo != "") {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            poRows[i].deliveryDate = finalLInDate

        }
        this.setState({
            poRows: poRows
        })

        document.getElementById('poValidFrom').focus()
    }
    openTransporterSelection(e, id) {
        if (this.state.supplier != "") {
            var data = {
                no: 1,
                type: 1,
                search: "",
                transporterCode: "",
                transporterName: "",
                city: this.state.isTransporterDependent != false ? this.state.city : ""

            }
            this.props.getTransporterRequest(data)
            this.setState({
                trasporterModal: true,
                transporterAnimation: !this.state.transporterAnimation
            });
        }
        else {
            this.setState({
                errorId: id,
                errorMassage: "Select Vendor ",
                poErrorMsg: true
            })
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onCloseTransporter(e) {
        this.setState({
            trasporterModal: false,
            transporterAnimation: !this.state.transporterAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("transporter").focus()
    }
    openloadIndentSelection(e) {
        var data = {
            no: 1,
            type: 1,
            search: "",
            orderId: "",
            supplier: "",
            cityName: "",
            piFromDate: "",
            piToDate: ""
        }
        this.props.loadIndentRequest(data)
        this.setState({
            indentValue: this.state.loadIndentId,
            loadIndentModal: true,
            loadIndentAnimation: !this.state.loadIndentAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onCloseLoadIndent(e) {
        this.setState({
            loadIndentModal: false,
            loadIndentAnimation: !this.state.loadIndentAnimation
        });


        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("loadIndentInput").focus()
    }
    getOtbLastInDate(rowId, otb) {
        let poRows = this.state.poRows
        let otbb = ""
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == rowId) {
                poRows[i].otb = otb
                otbb = otb
            }
            if (poRows[i].gridOneId > rowId) {
                poRows[i].otb = otbb - poRows[i - 1].amount
                otbb = otbb - poRows[i - 1].amount
            }
        }

        this.setState({
            changeLastIndate: false,
            poRows: poRows
        })
        this.getOtbForSecTwoRows();
    }
    getOtbForSecTwoRows() {
        let secTwoRows = this.state.secTwoRows
        let id = ""
        let otbb = ""
        for (let i = 0; i < secTwoRows.length; i++) {
            id = secTwoRows[0].gridTwoId
            if (secTwoRows[i].gridTwoId == id) {
                secTwoRows[i].otb = this.state.totalOtb
                otbb = this.state.totalOtb
            }
            if (secTwoRows[i].gridTwoId > id) {
                secTwoRows[i].otb = otbb - secTwoRows[i - 1].amount
                otbb = otbb - secTwoRows[i - 1].amount
            }
        }
        this.setState({
            secTwoRows: secTwoRows
        })
    }
    componentWillMount() {

        this.props.poRadioValidationRequest('data')
        if (this.props.purchaseIndent.articlePo.isSuccess) {
            let c = []
            if (this.props.purchaseIndent.articlePo.data.resource != null) {
                for (let i = 0; i < this.props.purchaseIndent.articlePo.data.resource.length; i++) {
                    let x = this.props.purchaseIndent.articlePo.data.resource[i];
                    x.checked = false;
                    let a = x
                    c.push(a)
                }
                this.setState({
                    vendorPoState: c,
                })
            }
            else {
                this.setState({
                    vendorPoState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

        var current = new Date()
        this.setState({
            dateUnformatted: current,
            poDate: moment(current).format("DD-MMM-YYYY"),
            lastAsnDate: moment(current).format("DD-MMM-YYYY")
        })

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.size.isSuccess) {
            if (nextProps.purchaseIndent.size.data.resource != null) {
                if (!this.state.sizeState.length) {
                    this.setState({ sizeState: nextProps.purchaseIndent.size.data.resource })
                }
            }
        }
        if (nextProps.purchaseIndent.poItemBarcode.isSuccess) {
            if (nextProps.purchaseIndent.poItemBarcode.data.resource != null) {
                this.setState({
                    poItemBarcodeData: nextProps.purchaseIndent.poItemBarcode.data.resource
                })
                this.resetPoRows();
                this.resetBarcodeRows()
                setTimeout(() => {
                    sessionStorage.getItem("partnerEnterpriseName") != "VMART" ?
                        this.updatepoRowsWithItemBarcodeGeneric(nextProps.purchaseIndent.poItemBarcode.data.resource)
                        : this.updatepoRowsWithItemBarcode(nextProps.purchaseIndent.poItemBarcode.data.resource)

                }, 1000)
            }

        }
        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
            this.setState({
                isAdhoc: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isAdhoc != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isAdhoc : false,
                isIndent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isIndent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isIndent : false,
                isSetBased: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSetBased != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSetBased : false,
                isHoldPo: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isHoldPo != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isHoldPo : false,
                isPOwithICode: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isPOwithICode != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isPOwithICode : false,
                poWithUpload: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload : false,
                isCityExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck : false,
                isMRPEditable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMRPEditable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMRPEditable : false,
                isDiscountAvail: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountAvail != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountAvail : false,
                isDiscountMap: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap : false,
                isRSP: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRSP != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload : false,
                displayOtb: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb : true,
                transporterValidation: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation : true,
                addRow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow : false,

                cat1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 : false : false,
                cat2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 : false : false,
                cat3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 : false : false,
                cat4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 : false : false,
                cat5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 : false : false,
                cat6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 : false : false,
                desc1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 : false : false,
                desc2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 : false : false,
                desc3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 : false : false,
                desc4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 : false : false,
                desc5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 : false : false,
                desc6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 : false : false,

                itemudf1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 : false : false,
                itemudf2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 : false : false,
                itemudf3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 : false : false,
                itemudf4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 : false : false,
                itemudf5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 : false : false,
                itemudf6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 : false : false,
                itemudf7Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 : false : false,
                itemudf8Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 : false : false,
                itemudf9Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 : false : false,
                itemudf10Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 : false : false,
                itemudf11Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 : false : false,
                itemudf12Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 : false : false,
                itemudf13Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 : false : false,
                itemudf14Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 : false : false,
                itemudf15Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 : false : false,
                itemudf16Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 : false : false,
                itemudf17Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 : false : false,
                itemudf18Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 : false : false,
                itemudf19Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 : false : false,
                itemudf20Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 : false : false,

                udf1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 : false : false,
                udf2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 : false : false,
                udf3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 : false : false,
                udf4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 : false : false,
                udf5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 : false : false,
                udf6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 : false : false,
                udf7Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 : false : false,
                udf8Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 : false : false,
                udf9Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 : false : false,
                udf10Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 : false : false,

                udf11Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 : false : false,
                udf12Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 : false : false,
                udf13Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 : false : false,
                udf14Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 : false : false,
                udf15Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 : false : false,
                udf16Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 : false : false,
                udf17Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 : false : false,
                udf18Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 : false : false,
                udf19Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 : false : false,
                udf20Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 : false : false,



                restrictedStartDate: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate : "",
                restrictedEndDate: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate : "",
                isRequireSiteId: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId : false,
                isVendorDesignNotReq: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq : false,

                isMrpRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpReq : true,
                isRspRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRspReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRspReq : true,
                isColorRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq : true,
                isDisplayFinalRate: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayFinalRate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayFinalRate : false,
                isTransporterDependent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent : true,
                isDisplayMarginRule: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule : true,
                isTaxDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable : false,
                isGSTDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable : false,
                isBaseAmountActive: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive : false,




            })

            let startDate = nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate
            let endDate = nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate
            let poDate = moment(this.state.poDate).format('YYYY-MM-DD')
            if (startDate != "" && endDate != "") {
                if (startDate <= poDate && poDate <= endDate) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "You are unable to save PO(Purchase Order) between " + moment(startDate).format("DD-MMM-YYYY") + " to " + moment(endDate).format("DD-MMM-YYYY"),

                        dateValidationRes: true
                    })
                }
            }





        }
        if (nextProps.purchaseIndent.hsnCode.isSuccess && this.state.onFieldhsn == true) {
            if (nextProps.purchaseIndent.hsnCode.data.resource != null) {
                this.setState({
                    hsnCode: nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNCode == null ? "" : nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNCode,
                    hsnSacCode: nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNSACCode == null ? "" : nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNSACCode,
                    onFieldhsn: false
                })
            } else {
                this.setState({
                    hsnCode: "",
                    hsnSacCode: "",
                    onFieldhsn: false
                })
            }
        }
        if (nextProps.purchaseIndent.procurementSite.isSuccess && this.state.onFieldSite == true) {
            if (nextProps.purchaseIndent.procurementSite.data.resource != null) {
                this.setState({
                    siteCode: nextProps.purchaseIndent.procurementSite.data.defaultSiteKey.defaultSiteCode,
                    siteName: nextProps.purchaseIndent.procurementSite.data.defaultSiteKey.defaultSiteName,
                    onFieldSite: false
                })
            } else {
                this.setState({
                    siteCode: "",
                    siteName: "",
                    onFieldSite: false
                })
            }
        }
        if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {
            console.log(nextProps.purchaseIndent.getCatDescUdf.data.resource)

            if (nextProps.purchaseIndent.getCatDescUdf.data.resource != null) {
                let poUdfData = []
                let c = []
                poUdfData = nextProps.purchaseIndent.getCatDescUdf.data.resource.cat_desc_udf
                this.setState({
                    udfRowMapping: nextProps.purchaseIndent.getCatDescUdf.data.resource.cat_desc_udf
                })
                if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "true") {
                    if (this.state.itemDetailsHeader.length != 0 && this.state.udfMappingData.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {
                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                } else if (this.state.isUDFExist == "false" && this.state.itemUdfExist == "true") {
                    if (this.state.itemDetailsHeader.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {
                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }

                }
                for (let j = 0; j < poUdfData.length; j++) {
                    // let x = poUdfData[j]
                    poUdfData[j].value = "";
                    // let a = x
                    c.push(poUdfData[j])
                }

                let itemUdf = this.state.itemUdf
                for (let z = 0; z < itemUdf.length; z++) {
                    itemUdf[z].udfList = c
                }
                this.setState({
                    itemUdf: itemUdf
                })
            }

        }

        if (nextProps.purchaseIndent.getItemDetails.isSuccess) {
            console.log(nextProps.purchaseIndent.getItemDetails.data.resource)
            let poUdfData = []
            let c = []
            if (nextProps.purchaseIndent.getItemDetails.data.essentialProParam != undefined) {
                this.setState({
                    cat1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 : this.state.cat1Validation,
                    cat2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 : this.state.cat2Validation,
                    cat3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 : this.state.cat3Validation,
                    cat4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 : this.state.cat4Validation,
                    cat5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 : this.state.cat5Validation,
                    cat6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 : this.state.cat6Validation,
                    desc1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 : this.state.desc1Validation,
                    desc2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 : this.state.desc2Validation,
                    desc3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 : this.state.desc3Validation,
                    desc4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 : this.state.desc4Validation,
                    desc5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 : this.state.desc5Validation,
                    desc6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 : this.state.desc6Validation,

                    itemudf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 : this.state.itemudf1Validation,
                    itemudf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 : this.state.itemudf2Validation,
                    itemudf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 : this.state.itemudf3Validation,
                    itemudf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 : this.state.itemudf4Validation,
                    itemudf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 : this.state.itemudf5Validation,
                    itemudf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 : this.state.itemudf6Validation,
                    itemudf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 : this.state.itemudf7Validation,
                    itemudf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 : this.state.itemudf8Validation,
                    itemudf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 : this.state.itemudf9Validation,
                    itemudf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 : this.state.itemudf10Validation,
                    itemudf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 : this.state.itemudf11Validation,
                    itemudf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 : this.state.itemudf12Validation,
                    itemudf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 : this.state.itemudf13Validation,
                    itemudf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 : this.state.itemudf14Validation,
                    itemudf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 : this.state.itemudf15Validation,
                    itemudf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 : this.state.itemudf16Validation,
                    itemudf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 : this.state.itemudf17Validation,
                    itemudf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 : this.state.itemudf18Validation,
                    itemudf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 : this.state.itemudf19Validation,
                    itemudf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 : this.state.itemudf20Validation,

                    udf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 : this.state.udf1Validation,
                    udf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 : this.state.udf2Validation,
                    udf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 : this.state.udf3Validation,
                    udf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 : this.state.udf4Validation,
                    udf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 : this.state.udf5Validation,
                    udf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 : this.state.udf6Validation,
                    udf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 : this.state.udf7Validation,
                    udf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 : this.state.udf8Validation,
                    udf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 : this.state.udf9Validation,
                    udf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 : this.state.udf10Validation,

                    udf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 : this.state.udf11Validation,
                    udf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 : this.state.udf12Validation,
                    udf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 : this.state.udf13Validation,
                    udf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 : this.state.udf14Validation,
                    udf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 : this.state.udf15Validation,
                    udf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 : this.state.udf16Validation,
                    udf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 : this.state.udf17Validation,
                    udf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 : this.state.udf18Validation,
                    udf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 : this.state.udf19Validation,
                    udf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 : this.state.udf20Validation,



                })
            }
            if (nextProps.purchaseIndent.getItemDetails.data.resource != null) {
                poUdfData = nextProps.purchaseIndent.getItemDetails.data.resource
                this.setState({
                    itemDetailsHeader: nextProps.purchaseIndent.getItemDetails.data.resource
                })

                if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "true") {
                    if (this.state.udfRowMapping.length != 0 && this.state.udfMappingData.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {
                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                } else if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "false") {
                    if (this.state.udfMappingData.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {
                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                }
                else if (this.state.isUDFExist == "false" && this.state.itemUdfExist == "true") {
                    if (this.state.udfRowMapping.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {
                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                }

                else if (this.state.isUDFExist == "false" && this.state.itemUdfExist == "false") {
                    if (this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {
                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                }
                for (let j = 0; j < poUdfData.length; j++) {
                    let x = poUdfData[j]
                    x.value = "";
                    x.code = ""
                    let a = x
                    c.push(a)

                }
                let catDescRows = this.state.catDescRows
                for (let z = 0; z < catDescRows.length; z++) {
                    catDescRows[z].itemDetails = c
                }
                this.setState({
                    catDescRows: catDescRows
                })
            } else {
                this.setState({
                    itemDetailsHeader: [],
                    poErrorMsg: true,
                    errorMassage: "Categories and Description are not available corresponding department"
                })
            }
        }
        if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {
            this.setState({
                udfRows: [{
                    setUdfNo: 1,
                    udfColor: [],
                    udfGridId: 1,
                }],
            })

            if (nextProps.purchaseIndent.getUdfMapping.data.resource != null) {
                let udfMappingData = nextProps.purchaseIndent.getUdfMapping.data.resource
                let c = []
                this.setState({
                    udfMappingData: nextProps.purchaseIndent.getUdfMapping.data.resource
                })
                if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "true") {
                    if (this.state.udfRowMapping.length != 0 && this.state.itemDetailsHeader.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {

                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                } else if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "false") {
                    if (this.state.itemDetailsHeader.length != 0 && this.state.sizeMapping.length != 0) {
                        if (this.state.loadIndentTrue) {
                            this.updatePoData(this.state.piKeyData)
                        }
                        if (this.state.setBasedTrue) {

                            this.updateSetBasedData(this.state.setBasedData)
                        }
                    }
                }
                for (let j = 0; j < udfMappingData.length; j++) {
                    let x = udfMappingData[j]
                    x.value = "";
                    let a = x
                    c.push(a)
                }
                let udfRows = this.state.udfRows
                for (let z = 0; z < udfRows.length; z++) {
                    udfRows[z].setUdfList = c
                }
                this.setState({
                    udfRows: udfRows
                })
            }
        }
        if (nextProps.purchaseIndent.poCreate.isSuccess) {
            sessionStorage.setItem("poValidFrom", this.state.poValidFrom)
            this.onClear();
            this.setBasedResest();
            this.setState({
                loader: false,
                currentDate: getDate()
            })


        } else if (nextProps.purchaseIndent.poCreate.isError) {
            this.setState({
                loader: false
            })

        }
        if (nextProps.purchaseIndent.otb.isSuccess) {
            if (nextProps.purchaseIndent.otb.data.resource != null) {
                // let c = this.state.poRowsfor (var x = 0; x < c.length; x++) { if (c[x].gridOneId == nextProps.purchaseIndent.otb.data.rowId) {
                //     c[x].otb = nextProps.purchaseIndent.otb.data.resource.otbthis.setState({    poRows: c})}}
                if (this.state.isOtbValidation == "true") {
                    if (nextProps.purchaseIndent.otb.data.resource.otb.toString() == "0") {
                        this.setState({
                            poErrorMsg: true,
                            errorMassage: "OTB is zero for the corresponding Article and MRP"
                        })
                    }
                }
                const t = this
                setTimeout(function () {
                    if (t.state.changeLastIndate) {
                        t.setState({
                            totalOtb: nextProps.purchaseIndent.otb.data.resource.otb
                        })
                        t.gridFirst();
                        t.getOtbLastInDate(nextProps.purchaseIndent.otb.data.rowId, nextProps.purchaseIndent.otb.data.resource.otb)
                    } else {
                        t.setState({
                            totalOtb: nextProps.purchaseIndent.otb.data.resource.otb
                        })
                        t.gridFirst();
                        t.getOpenToBuy(nextProps.purchaseIndent.otb.data.rowId, nextProps.purchaseIndent.otb.data.resource.otb);
                    }
                }, 1)
            }
            this.props.otbClear();
        }
        if (nextProps.purchaseIndent.marginRule.isSuccess) {
            if (nextProps.purchaseIndent.marginRule.data.resource != null) {
                let rows = this.state.poRows
                for (var i = 0; i < rows.length; i++) {
                    rows[i].marginRule = nextProps.purchaseIndent.marginRule.data.resource
                    this.setState({
                        poRows: rows,
                        saveMarginRule: nextProps.purchaseIndent.marginRule.data.resource
                    })
                }
            }
            else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Margin rule is not available for the corresponding Article and Supplier"
                })
            }
            this.props.marginRuleClear()
        }


        if (nextProps.purchaseIndent.getPoData.isSuccess) {
            this.setState({
                poRows: []
            })
            if (nextProps.purchaseIndent.getPoData.data.resource != null) {

                this.setState({
                    piKeyData: nextProps.purchaseIndent.getPoData.data.resource.piKey,

                    isOtbValidation: nextProps.purchaseIndent.getPoData.data.resource.otbValidation,
                    cat1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.cat1 : false,
                    cat2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.cat2 : false,
                    cat3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.cat3 : false,
                    cat4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.cat4 : false,
                    cat5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.cat5 : false,
                    cat6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.cat6 : false,
                    desc1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.desc1 : false,
                    desc2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.desc2 : false,
                    desc3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.desc3 : false,
                    desc4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.desc4 : false,
                    desc5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.desc5 : false,
                    desc6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.getPoData.data.resource.availableKeys.desc6 : false
                })
                this.updatePoData(nextProps.purchaseIndent.getPoData.data.resource.piKey);
            }

        }
        if (nextProps.purchaseIndent.setBased.isSuccess) {
            this.setState({
                poRows: [],
                secTwoRows: [],
                udfRows: [],
                udfMappingData: [],
                udfRowMapping: [],
                itemDetailsHeader: [],
                itemUdf: [],
                catDescRows: [],
                isOtbValidation: nextProps.purchaseIndent.setBased.data.otbValidation,
                cat1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.cat1 : false,
                cat2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.cat2 : false,
                cat3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.cat3 : false,
                cat4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.cat4 : false,
                cat5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.cat5 : false,
                cat6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.cat6 : false,
                desc1Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.desc1 : false,
                desc2Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.desc2 : false,
                desc3Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.desc3 : false,
                desc4Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.desc4 : false,
                desc5Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.desc5 : false,
                desc6Validation: sessionStorage.getItem("partnerEnterpriseName") != "VMART" ? nextProps.purchaseIndent.setBased.data.availableKeys.desc6 : false,
            })
            if (nextProps.purchaseIndent.setBased.data.resource != null) {
                this.setState({
                    setBasedData: nextProps.purchaseIndent.setBased.data.resource,

                    // setBasedTrue:false
                })

            }
            this.updateSetBasedData(nextProps.purchaseIndent.setBased.data.resource);
            this.props.setBasedClear()
        }
        if (nextProps.purchaseIndent.loadIndent.isSuccess) {

            this.setState({
                loadData: nextProps.purchaseIndent.loadIndent.data.resource
            })

        }

        if (nextProps.purchaseIndent.leadTime.isSuccess) {
            this.setState({
                leadDays: nextProps.purchaseIndent.leadTime.data.resource.leadTime
            })
        }


        if (nextProps.purchaseIndent.markUp.isSuccess) {
            if (nextProps.purchaseIndent.markUp.data.resource != null) {
                this.setState({
                    calculatedMarginExact: nextProps.purchaseIndent.markUp.data.resource.calculatedMargin
                })

                let c = this.state.poRows
                let secTwoRows = this.state.secTwoRows
                // for (var x = 0; x < c.length; x++) {
                //     if (c[x].gridOneId == nextProps.purchaseIndent.markUp.data.designRow) {
                //         // let mrk = c[x].mrk
                //         // let calculatedMargin = c[x].calculatedMargin
                //         c[x].mrk = [...c[x].mrk, nextProps.purchaseIndent.markUp.data.resource.actualMarkUp]
                //         c[x].calculatedMargin = [...c[x].calculatedMargin, nextProps.purchaseIndent.markUp.data.resource.calculatedMargin]
                //     }

                // }
                // this.setState({
                //     poRows: c
                // })
                for (var x = 0; x < secTwoRows.length; x++) {
                    if (secTwoRows[x].gridTwoId == nextProps.purchaseIndent.markUp.data.rowId) {
                        secTwoRows[x].mrk = nextProps.purchaseIndent.markUp.data.resource.actualMarkUp
                        secTwoRows[x].calculatedMargin = nextProps.purchaseIndent.markUp.data.resource.calculatedMargin
                        secTwoRows[x].lineItemChk = false
                    }
                }

                this.setState({
                    secTwoRows: secTwoRows
                })
                const t = this

                setTimeout(function () {
                    t.updatePorows()
                    t.gridFirst();
                }, 100)

            }
            this.setState({
                markUpYes: false
            })

            this.setState({
                lineItemChange: false
            })

        }
        if (nextProps.purchaseIndent.lineItem.isSuccess) {
            console.log("ye it ws success once")
            if (nextProps.purchaseIndent.lineItem.data.resource != null) {


                let c = this.state.secTwoRows
                let rows = this.state.poRows
                for (var x = 0; x < c.length; x++) {
                    if (c[x].gridTwoId == nextProps.purchaseIndent.lineItem.data.rowId) {

                        c[x].amount = Math.round(nextProps.purchaseIndent.lineItem.data.resource.netAmount * 100) / 100
                        c[x].gst = nextProps.purchaseIndent.lineItem.data.resource.gst
                        c[x].tax = nextProps.purchaseIndent.lineItem.data.resource.tax
                        c[x].finCharges = nextProps.purchaseIndent.lineItem.data.resource.finCharges



                    }

                }
                // for (var x = 0; x < rows.length; x++) {
                //     if (rows[x].gridOneId == nextProps.purchaseIndent.lineItem.data.designRow) {
                //         let amount = rows[x].amount
                //         let gst = rows[x].gst
                //         let tax = rows[x].tax
                //         let finCharges = rows[x].finCharges

                //         rows[x].amount = Number(amount) + Number(nextProps.purchaseIndent.lineItem.data.resource.netAmount)
                //         rows[x].gst = [...gst, nextProps.purchaseIndent.lineItem.data.resource.gst]
                //         rows[x].tax = [...tax, nextProps.purchaseIndent.lineItem.data.resource.tax]
                //         rows[x].finCharges = [...finCharges, nextProps.purchaseIndent.lineItem.data.resource.finCharges]


                //     }
                // }
                // this.setState({
                //     poRows: rows
                // })
                this.setState({
                    secTwoRows: c,

                })


                this.updatePoAmount();
                this.updateNetAmt(nextProps.purchaseIndent.lineItem.data.rowId, nextProps.purchaseIndent.lineItem.data.designRow)
                setTimeout(() => {
                    this.updatePorows()
                    // this.getOtbLastInDate(1,this.state.totalOtb)

                }, 10)
                !this.state.isRspRequired ? this.setState({
                    lineItemChange: false
                }) : null

            }

            this.props.lineItemClear();
        } else if (nextProps.purchaseIndent.lineItem.isError) {
            console.log("nope it was never success")

            this.setState({
                lineItemChange: false
            })
            // let focusedObj = this.state.focusedObj
            let c = this.state.secTwoRows

            for (var x = 0; x < c.length; x++) {
                if (c[x].gridTwoId == nextProps.purchaseIndent.lineItem.message.rowId) {
                    c[x].lineItemChk = true
                }
            }
            this.setState({
                secTwoRows: c
            })

            // if (focusedObj.type == "setQty") {
            //     let secTwoRows = this.state.secTwoRows
            //     for (let i = 0; i < secTwoRows.length; i++) {
            //         if (secTwoRows[i].gridTwoId == focusedObj.rowId) {
            //             secTwoRows[i].setQty = focusedObj.value

            //         }

            //     }
            //     this.setState({
            //         secTwoRows
            //     })

            // }
            // if (focusedObj.type == "ratio") {
            //     let secTwoRows = this.state.secTwoRows
            //     for (let j = 0; j < secTwoRows.length; j++) {
            //         if (secTwoRows[j].gridTwoId == focusedObj.rowId) {
            //             if( focusedObj.cname!=""){
            //             if (secTwoRows[j].size == focusedObj.cname) {
            //                 secTwoRows[j].setRatio = focusedObj.value

            //             }
            //             for (let l = 0; l < secTwoRows[j].sizeList.length; l++) {
            //                 if (secTwoRows[j].sizeList[l].cname = focusedObj.cname) {
            //                     secTwoRows[j].sizeList[l].ratio = focusedObj.value
            //                 }
            //             }
            //         }
            //     }}
            //     this.setState({
            //         secTwoRows
            //     })
            // }

            // if (focusedObj.type == "setRatio") {
            //     let secTwoRows = this.state.secTwoRows
            //     for (let j = 0; j < secTwoRows.length; j++) {
            //         if (secTwoRows[j].gridTwoId == focusedObj.rowId) {
            //                   if(focusedObj.cname!=""){
            //             for (let l = 0; l < secTwoRows[j].sizeList.length; l++) {
            //                 if (secTwoRows[j].sizeList[l].cname = focusedObj.cname) {
            //                     secTwoRows[j].sizeList[l].ratio = focusedObj.value
            //                 }
            //             }
            //         }
            //         }
            //     }
            //     this.setState({
            //         secTwoRows
            //     })
            // }
            // if(focusedObj.type=="color"){
            //      let secTwoRows = this.state.secTwoRows
            //     for (let j = 0; j < secTwoRows.length; j++) {
            //         if (secTwoRows[j].gridTwoId == focusedObj.rowId) {
            //             secTwoRows[j].color= focusedObj.color
            //             secTwoRows[j].colorList = focusedObj.colorList
            //         }}

            //         this.setState({
            //             secTwoRows
            //         })
            // }

        }


        if (nextProps.purchaseIndent.poItemcode.isSuccess) {
            if (nextProps.purchaseIndent.poItemcode.data.resource != null) {

                let c = []
                for (let i = 0; i < nextProps.purchaseIndent.poItemcode.data.resource.length; i++) {


                    let x = nextProps.purchaseIndent.poItemcode.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    poItemcodeData: c
                })
            }
        }
        if (nextProps.purchaseIndent.poSize.isSuccess) {
            this.setState({
                secTwoRows: [{
                    discount: {
                        discountType: "",
                        discountValue: ""
                    },
                    finalRate: "",

                    colorChk: false,
                    icodeChk: false,
                    designRowId: 1,
                    color: this.state.isColorRequired ? [] : ["NA"],
                    colorList: this.state.isColorRequired ? [] : [{ id: 1, code: "", cname: "NA" }],
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    sizeListData: [],
                    size: "",
                    setRatio: "",
                    vendorDesignNo: "",
                    option: "",
                    setNo: 1,
                    mrk: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",
                    ratio: [],
                    total: "",
                    gst: "",
                    finCharges: [],
                    tax: "",
                    calculatedMargin: "",
                    gridTwoId: 1,
                    otb: "",
                    totalQuantity: "",
                    totalNetAmt: "",
                    totaltax: [],
                    totalCalculatedMargin: [],
                    totalgst: [],
                    totalIntakeMargin: [],
                    totalFinCharges: [],
                    totalBasic: "",
                    totalCalculatedOtb: "",

                    catOneCode: "",
                    catOneName: "",
                    catTwoCode: "",
                    catTwoName: "",
                    catThreeCode: "",
                    catThreeName: "",
                    catFourCode: "",
                    catFourName: "",
                    catFiveCode: "",
                    catFiveName: "",
                    catSixCode: "",
                    catSixName: "",

                    descOneCode: "",
                    descOneName: "",
                    descTwoCode: "",
                    descTwoName: "",
                    descThreeCode: "",
                    descThreeName: "",
                    descFourCode: "",
                    descFourName: "",
                    descFiveCode: "",
                    descFiveName: "",
                    descSixCode: "",
                    descSixName: "",

                    itemudf1: "",
                    itemudf2: "",
                    itemudf3: "",
                    itemudf4: "",
                    itemudf5: "",
                    itemudf6: "",
                    itemudf7: "",
                    itemudf8: "",
                    itemudf9: "",
                    itemudf10: "",
                    itemudf11: "",
                    itemudf12: "",
                    itemudf13: "",
                    itemudf14: "",
                    itemudf15: "",
                    itemudf16: "",
                    itemudf17: "",
                    itemudf18: "",
                    itemudf19: "",
                    itemudf20: "",
                    deliveryDate: "",
                    typeOfBuying: "",
                    marginRule: "",
                    image: [],

                    imageUrl: {},
                    containsImage: false,
                    lineItemChk: false
                }],
            })
            let c = []
            let poSizeData = []
            if (nextProps.purchaseIndent.poSize.data.resource != null) {
                poSizeData = nextProps.purchaseIndent.poSize.data.resource
                this.setState({
                    sizeMapping: poSizeData

                })
            } else {
                this.setState({
                    sizeMapping: [],

                    errorMassage: "Sizes are not available for corresponding department",
                    poErrorMsg: true



                })
            }
            if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "true") {
                if (this.state.udfRowMapping.length != 0 && this.state.itemDetailsHeader.length != 0 && this.state.udfMappingData.length != 0) {
                    if (this.state.loadIndentTrue) {
                        this.updatePoData(this.state.piKeyData)
                    }
                    if (this.state.setBasedTrue) {

                        this.updateSetBasedData(this.state.setBasedData)
                    }
                }

            } else if (this.state.isUDFExist == "true" && this.state.itemUdfExist == "false") {
                if (this.state.itemDetailsHeader.length != 0 && this.state.udfMappingData.length != 0) {
                    if (this.state.loadIndentTrue) {
                        this.updatePoData(this.state.piKeyData)
                    }
                    if (this.state.setBasedTrue) {

                        this.updateSetBasedData(this.state.setBasedData)
                    }
                }

            } else if (this.state.isUDFExist == "false" && this.state.itemUdfExist == "true") {
                if (this.state.udfRowMapping.length != 0 && this.state.itemDetailsHeader.length != 0) {
                    if (this.state.loadIndentTrue) {
                        this.updatePoData(this.state.piKeyData)
                    }
                    if (this.state.setBasedTrue) {

                        this.updateSetBasedData(this.state.setBasedData)
                    }
                }

            }
            else if (this.state.isUDFExist == "false" && this.state.itemUdfExist == "false") {
                if (this.state.itemDetailsHeader.length != 0) {
                    if (this.state.loadIndentTrue) {
                        this.updatePoData(this.state.piKeyData)
                    }
                    if (this.state.setBasedTrue) {
                        this.updateSetBasedData(this.state.setBasedData)
                    }
                }
            }
            setTimeout(() => {
                if (poSizeData != null) {
                    for (let j = 0; j < poSizeData.length; j++) {


                        let x = nextProps.purchaseIndent.poSize.data.resource[j]

                        x.ratio = "";

                        let a = x
                        c.push(a)

                    }
                }
                let secTwoRows = this.state.secTwoRows
                for (let z = 0; z < 1; z++) {
                    secTwoRows[z].sizeList = c
                }
                this.setState({
                    secTwoRows: secTwoRows
                })
            })
        }

        if (nextProps.purchaseIndent.color.isSuccess) {
            if (nextProps.purchaseIndent.color.data.resource != null) {
                this.setState({
                    colorNewData: nextProps.purchaseIndent.color.data.resource
                })
            }
        }
        if (nextProps.purchaseIndent.getMultipleMargin.isSuccess) {
            let lineItem = nextProps.purchaseIndent.getMultipleMargin.data.resource.multipleMarkup
            let secTwoRows = this.state.secTwoRows

            for (let i = 0; i < lineItem.length; i++) {
                for (let j = 0; j < secTwoRows.length; j++) {
                    if (lineItem[i].designRowid == secTwoRows[j].designRowId && lineItem[i].rowId == secTwoRows[j].gridTwoId) {

                        secTwoRows[j].mrk = lineItem[i].actualMarkUp
                        secTwoRows[j].calculatedMargin = lineItem[i].calculatedMargin



                    }

                }
            }
            this.setState({
                secTwoRows: secTwoRows,
                lineItemChange: false
            })
            setTimeout(() => {
                this.updatePorows()


            }, 10)
        }

        if (nextProps.purchaseIndent.multipleLineItem.isSuccess) {

            let lineItem = nextProps.purchaseIndent.multipleLineItem.data.resource.multipleLineItem
            console.log("lineItem,", lineItem)
            let secTwoRows = this.state.secTwoRows
            let designId = lineItem[0].designRowid
            for (let i = 0; i < lineItem.length; i++) {
                for (let j = 0; j < secTwoRows.length; j++) {
                    if (lineItem[i].designRowid == secTwoRows[j].designRowId && lineItem[i].rowId == secTwoRows[j].gridTwoId) {

                        secTwoRows[j].amount = Math.round(lineItem[i].netAmount * 100) / 100
                        secTwoRows[j].gst = lineItem[i].gst
                        secTwoRows[j].tax = lineItem[i].tax
                        secTwoRows[j].finCharges = lineItem[i].finCharges
                        secTwoRows[j].lineItemChk = false

                    }

                }
            }
            this.setState({
                secTwoRows: secTwoRows
            })
            setTimeout(() => {
                this.updatePorows()
                console.log(this.state.isRspRequired, designId)
                this.state.isRspRequired ? this.multiMarkUp(designId) : null
                !this.state.isRspRequired ? this.setState({
                    lineItemChange: false
                }) : null
                // this.getOtbLastInDate(1,this.state.totalOtb)

            }, 10)



        } else if (nextProps.purchaseIndent.multipleLineItem.isError) {
            let focusedObj = this.state.focusedObj
            if (focusedObj.type == "rate") {
                let poRows = this.state.poRows
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == focusedObj.rowId) {
                        poRows[i].rate = focusedObj.value
                        poRows[i].finalRate = focusedObj.finalRate
                    }
                }

                this.setState({
                    poRows: poRows,
                    focusedObj: {
                        type: "",
                        rowId: "",
                        cname: "",
                        value: "",
                        radio: "",
                        finalRate: "",
                        colorObj: {
                            color: [],
                            colorList: []
                        }

                    }
                })

            }

            // if (focusedObj.type == "copying color") {
            //     this.setState({
            //         secTwoRows: this.state.copingLineItem,
            //         focusedObj: {
            //             type: "",
            //             rowId: "",
            //             cname: "",
            //             value: "",
            //             radio: "",
            //             colorObj: {
            //                 color: [],
            //                 colorList: []
            //             }

            //         }
            //     })


            // }

        }


        if (nextProps.purchaseIndent.po_edit_data.isSuccess) {
            if (nextProps.purchaseIndent.po_edit_data.data.resource != null) {
                this.updatePoData(nextProps.purchaseIndent.po_edit_data.data.resource);
            }
        }

        if (nextProps.purchaseIndent.save_draft_po.isSuccess && this.state.orderNo == "") {
            if (nextProps.purchaseIndent.save_draft_po.data.resource != null) {
                this.setState({
                    orderNo: nextProps.purchaseIndent.save_draft_po.data.resource.orderNo
                })
            }
        }
        // if(nextProps.purchaseIndent.get_draft_data.isSuccess) {
        //     if(nextProps.purchaseIndent.get_draft_data.data.resource != null) {
        //         this.updatePoData(nextProps.purchaseIndent.get_draft_data.data.resource);
        //     }
        // }

    }

    updateSupplierState(data) {

        this.setState({
            supplier: data.supplier,
            stateCode: data.stateCode,
            slCode: data.slcode,
            slName: data.slName,
            slAddr: data.slAddr,
            transporterCode: data.transporterCode,
            transporterName: data.transporterName,
            flag: this.state.flag + 1,
            transporter: data.transporter,
            tradeGrpCode: data.tradeGrpCode,
            term: data.termName,
            termCode: data.purtermMainCode,
            termName: data.termName,
            gstInNo: data.gstInNo,
            slCityName: data.city,
            poAmount: 0,
            poQuantity: 0,
            supplierCode: data.slcode,
            itemCodeList: []


        }, () => {
            this.supplier();
            this.transporter();
        })
        this.props.leadTimeRequest(data.slcode)
        let marginData = {
            slCode: data.slcode,
            articleCode: this.state.vendorMrpPo,
            siteCode: this.state.siteCode,
            rId: ""
        }
        this.props.marginRuleRequest(marginData)
        setTimeout(() => {
            this.resetPoRows();
            this.resetsecRows();
        }, 10)
        document.getElementById("vendor").focus()
    }
    resetsecRows() {
        let udfMappingData = this.state.udfMappingData
        let udfRowMapping = this.state.udfRowMapping
        let itemDetailsHeader = this.state.itemDetailsHeader
        let sizeMapping = this.state.sizeMapping
        udfRowMapping.forEach(mapData => {
            mapData.value = ""
        })
        udfMappingData.forEach(mapData => {
            mapData.value = ""
        })
        itemDetailsHeader.forEach(mapData => {
            mapData.value = ""
            mapData.code = ""
        })
        let c = []
        for (let j = 0; j < sizeMapping.length; j++) {


            let x = sizeMapping[j]

            x.ratio = "";

            let a = x
            c.push(a)

        }

        this.setState({
            secTwoRows: [{
                discount: {
                    discountType: "",
                    discountValue: ""
                },
                finalRate: "",
                colorChk: false,
                icodeChk: false,
                setHeaderId: "",
                designRowId: 1,
                color: this.state.isColorRequired ? [] : ["NA"],
                colorList: this.state.isColorRequired ? [] : [{ id: 1, code: "", cname: "NA" }],
                colorSizeList: [],
                icodes: [],
                itemBarcode: "",
                sizeListData: [],
                sizeList: sizeMapping,
                vendorDesignNo: "",
                option: 1,
                setNo: 1,
                size: "",
                setRatio: "",
                total: "",
                setQty: "",
                quantity: "",
                amount: "",
                rate: "",
                ratio: [],
                gst: "",
                finCharges: [],
                tax: "",
                calculatedMargin: "",
                mrk: "",
                gridTwoId: 1,
                otb: "",
                totalQuantity: "",
                totalNetAmt: "",
                totaltax: [],
                totalCalculatedMargin: [],
                totalgst: [],
                totalIntakeMargin: [],
                totalFinCharges: [],
                totalBasic: "",
                totalCalculatedOtb: "",
                catOneCode: "",
                catOneName: "",
                catTwoCode: "",
                catTwoName: "",
                catThreeCode: "",
                catThreeName: "",
                catFourCode: "",
                catFourName: "",
                catFiveCode: "",
                catFiveName: "",
                catSixCode: "",
                catSixName: "",
                descOneCode: "",
                descOneName: "",

                descTwoCode: "",
                descTwoName: "",
                descThreeCode: "",
                descThreeName: "",
                descFourCode: "",
                descFourName: "",
                descFiveCode: "",
                descFiveName: "",
                descSixCode: "",
                descSixName: "",
                itemudf1: "",
                itemudf2: "",
                itemudf3: "",
                itemudf4: "",
                itemudf5: "",
                itemudf6: "",
                itemudf7: "",
                itemudf8: "",
                itemudf9: "",
                itemudf10: "",
                itemudf11: "",
                itemudf12: "",
                itemudf13: "",
                itemudf14: "",
                itemudf15: "",
                itemudf16: "",
                itemudf17: "",
                itemudf18: "",
                itemudf19: "",
                itemudf20: "",
                deliveryDate: "",
                typeOfBuying: "",
                marginRule: "",
                image: [],

                imageUrl: {},
                containsImage: false,
                lineItemChk: false

            }],
            udfRows: [{
                setUdfNo: 1,
                udfColor: [],

                designRowId: 1,
                udfGridId: 1,
                setUdfList: udfMappingData
            }],
            catDescRows: [{
                vendorDesign: "",
                designRowId: 1,
                gridId: 1,
                itemDetails: itemDetailsHeader
            }],
            itemUdf: [{
                designRowId: 1,
                itemUdfGridId: 1,
                vendorDesign: "",
                udfList: udfRowMapping

            }]
        })


    }

    updateMrpState(data) {
        this.props.supplierClear()
        let deptName = data.department
        let deptCode = data.departmentCode

        this.setState({
            vendorMrpPo: data.articleCode,
            division: data.division,
            divisionCode: data.divisionCode,
            section: data.section,
            sectionCode: data.sectionCode,
            department: data.department,
            departmentCode: data.departmentCode,
            startRange: data.mrpStart,
            endRange: data.mrpEnd,
            articleName: data.articleName,
            // hsnSacCode: data.hsnSacCode,
            itemUdfExist: data.itemUdfExist,
            isUDFExist: data.isUDFExist,
            isSiteExist: data.isSiteExist,
            copyColor: data.copyColor,
            isOtbValidation: data.isOtbValidation,
            leadDays: "",
            transporter: "",
            term: "",
            supplier: "",
            onFieldhsn: true,
            onFieldSite: true,
            supplierCode: "",
            itemCodeList: [],
            city: "",

            catDescRows: [{
                vendorDesign: "",
                designRowId: 1,
                gridId: 1
            }],
        },
            () => {
                this.vendorMrpPo();
            });
        if (this.state.codeRadio != "poIcode") {
            let poData = {
                deptName: deptName,
                deptCode: deptCode,
                hl1Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.division : "",
                hl2Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.section : "",
                hl4Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.articleName : "",
            }
            // this.props.poSizeRequest(poData)
        }
        let hsnData = {
            rowId: "",
            code: data.departmentCode,
            no: 1,
            type: 1,
            search: ""
        }
        this.props.hsnCodeRequest(hsnData)

        if (data.isSiteExist == "true" || this.state.isRequireSiteId) {

            let site = {
                type: 1,
                no: 1,
                search: ""
            }
            this.props.procurementSiteRequest(site)
        }
        let Payload = {
            hl3Code: data.departmentCode,
            hl3Name: deptName,
        }
        this.props.getItemDetailsRequest(Payload)

        if (data.isUDFExist == "true") {

            let udfdata = {

                type: 1,
                search: "",
                isCompulsary: "",
                displayName: "",
                udfType: "",
                ispo: true
            }
            this.props.getUdfMappingRequest(udfdata);

        }
        if (data.itemUdfExist == "true") {
            let uData = {
                hl3Code: data.departmentCode,
                ispo: true,
                hl3Name: deptName

            }
            this.props.getCatDescUdfRequest(uData)
        }
        setTimeout(() => {
            this.resetPoRows()
            if (!this.state.isColorRequired) {
                let secTwoRows = this.state.secTwoRows
                let color = []
                let colorObj = {
                    id: 1,
                    code: "",
                    cname: "NA"

                }
                color.push(colorObj)
                for (let i = 0; i < secTwoRows.length; i++) {
                    secTwoRows[i].color = ["NA"],
                        secTwoRows[i].colorList = color,
                        secTwoRows[i].option = 1

                }
                this.setState({
                    secTwoRows
                })
            }


        }, 10)

    }

    resetPoRows() {
        let poRows = [{
            color: [],
            size: [],
            ratio: [],
            vendorMrp: "",
            vendorDesign: "",
            mrk: [],
            discount: {
                discountType: "",
                discountValue: ""
            },
            rate: "",
            finalRate: "",
            netRate: "",
            rsp: "",
            mrp: "",
            quantity: "",
            amount: "",
            otb: "",
            remarks: "",
            gst: [],
            finCharges: [],
            concatFinCharges: [],
            basic: "",
            tax: [],
            calculatedMargin: [],

            gridOneId: 1,
            deliveryDate: this.state.lastInDate,
            typeOfBuying: "",
            marginRule: this.state.codeRadio == "poIcode" ? this.state.saveMarginRule : "",
            image: [],

            imageUrl: {},
            containsImage: false,

        }]
        this.setState({
            poRows: poRows,
            descSix: ""

        })
    }

    updateItem(data) {

        document.getElementById(this.state.itemCodeId).focus()
        let poRows = this.state.poRows
        let flag = false

        //    for(let l=0 ;l<poRows.length;l++){
        //        if(poRows[l].gridOneId == data.id && poRows[i].vendorMrp != data.mrp){
        //             flag = true


        //        }
        //    }
        //    if(flag){
        if (poRows.length == 1) {
            for (var i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == data.id) {
                    poRows[i].vendorMrp = data.mrp
                    poRows[i].rsp = data.rsp
                    poRows[i].mrp = data.mrp
                    poRows[i].rate = ""
                    poRows[i].discount = {
                        discountType: "",
                        discountValue: ""
                    }
                    poRows[i].finalRate = ""
                }
            }
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            const date = new Date(this.state.lastInDate);
            var year = date.getFullYear()
            var monthName = (monthNames[date.getMonth()]);
            let dataa = {
                rowId: data.id,
                articleCode: this.state.vendorMrpPo,
                mrp: data.mrp,
                month: monthName,
                year: year
            }
            data.mrp != "" && this.state.isMrpRequired && this.state.displayOtb ? this.props.otbRequest(dataa) : null

            this.setState({
                poRows: poRows
            })

            this.setState({
                descSix: data.mrp
            })

            const t = this

            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        } else {
            // if (this.state.descSix == "" || this.state.descSix == data.mrp) {

            for (var i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == data.id) {
                    poRows[i].vendorMrp = data.mrp
                    poRows[i].rsp = data.mrp
                    poRows[i].mrp = data.mrp
                    poRows[i].rate = ""
                    poRows[i].discount = {
                        discountType: "",
                        discountValue: ""
                    }
                    poRows[i].finalRate = ""
                }
            }
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            const date = new Date(this.state.lastInDate);
            var year = date.getFullYear()
            var monthName = (monthNames[date.getMonth()]);


            let dataa = {
                rowId: data.id,
                articleCode: this.state.vendorMrpPo,
                mrp: data.mrp,
                month: monthName,
                year: year
            }
            data.mrp != "" && this.state.displayOtb && this.state.isMrpRequired ? this.props.otbRequest(dataa) : null

            this.setState({
                poRows: poRows
            })

            this.setState({
                descSix: data.mrp
            })
            const t = this

            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        //  else {
        //     let mrp = sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? "Desc 6" : "MRP"
        //     this.setState({
        //         errorMassage: "You can select same " + mrp + " value used above",
        //         poErrorMsg: true
        //     })

        // }
        // }
        //    }

    }

    //   _________________________VENDOR MRP_______________________
    openMrpModal() {
        //sdfdsfdsfds 
        if (this.state.codeRadio == "setBased" && this.state.orderSet == "") {
            this.setState({
                errorMassage: "In case of Set Based you need to select Order number",
                poErrorMsg: true
            })
        }
        else {
            var data = {
                type: "",
                no: 1,
                articleCode: "",
                articleName: "",
                division: "",
                section: "",
                department: "",
                search: ""
            }
            this.props.articlePoRequest(data)
            this.setState({
                // rowIdentity: id,
                departmentSet: this.state.departmentSet,
                articleModal: true,
                articleModalAnimation: !this.state.articleModalAnimation

            });

            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }

        }

    }


    closeMrpModal() {
        this.setState({
            articleModal: false,
            articleModalAnimation: !this.state.articleModalAnimation
        })
        document.getElementById('article').focus()
    }

    mrpCloseModal() {
        var c = this.props.purchaseIndent.articlePo.data.resource
        if (this.props.purchaseIndent.articlePo.data.resource == null) {
            this.setState({
                vendorPoState: c,
                articleModal: false,
                articleModalAnimation: !this.state.articleModalAnimation
            })
        }
        if (c != null) {
            for (var i = 0; i < c.length; i++) {
                if (c[i].checked == true) {
                    c[i].checked = false
                    document.getElementById(c[i].hl4Code).checked = false
                }
                if (document.getElementById(c[i].hl4Code).checked) { document.getElementById(c[i].hl4Code).checked = false }
            }
            this.setState({
                vendorPoState: c,
                articleModal: false,
                articleModalAnimation: !this.state.articleModalAnimation
            })
        }
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("article").focus()
    }

    openPoColorModal(id, checkValue, colorList, idFocus) {

        // if (this.state.vendorMrpPo == "") {

        //     this.setState({
        //         errorMassage: "Select Article Code",
        //         poErrorMsg: true
        //     })

        // }
        // else {
        //     let itemUdf = this.state.itemUdf
        //     let flag = false
        //     for (var i = 0; i < itemUdf.length; i++) {
        //         for (var j = 0; j < itemUdf[i].udfList.length; j++) {
        //             if (itemUdf[i].udfList[j].isCompulsoryPO == "Y") {
        //                 if (itemUdf[i].udfList[j].value == "") {
        //                     flag = true
        //                 }
        //             }
        //         }
        //     }

        //     if (flag) {
        //         this.validateUdf()
        //     } else {

        let poRows = this.state.poRows
        let flag = false
        poRows.forEach(po => {
            flag = (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) || po.rate == "" || po.marginRule == "" || po.mrp == "" || po.vendorMrp == "" || po.typeOfBuying == "" || po.deliveryDate == "" ? true : false

        })
        if (flag) {
            this.setState({
                errorId: idFocus,
            })
            this.validateItemdesc()
        } else {
            let output = false
            let secTwoRows = this.state.secTwoRows
            for (var i = 0; i < secTwoRows.length; i++) {
                if (secTwoRows[i].gridTwoId == id) {
                    if (secTwoRows[i].vendorDesignNo == "" && !this.state.isVendorDesignNotReq) {
                        output = true
                    }
                }
            }
            if (output) {
                this.setState({
                    errorId: idFocus,
                    errorMassage: "Vedor design number in Section 3 is mandate",
                    poErrorMsg: true
                })


            } else {
                var dataColor = {
                    id: id,
                    type: "",
                    no: 1,
                    search: "",
                    hl3Name: this.state.department,
                    hl3Code: this.state.departmentCode,
                    itemType: 'color'
                }
                this.props.colorRequest(dataColor)
                // let focusedObj = this.state.focusedObj
                // focusedObj.value =""
                // focusedObj.type = "color"
                // focusedObj.rowId = id
                // focusedObj.cname = ""
                // focusedObj.radio = this.state.codeRadio
                // focusedObj.colorObj = {
                //     color: checkValue == "" ? [] : checkValue,
                //     colorList: colorList == "" ? [] : colorList
                // }

                // this.setState({
                //     focusedObj
                // })
                this.setState({
                    colorValue: checkValue == "" ? [] : checkValue.split(','),
                    colorRow: id,
                    colorCode: this.state.vendorMrpPo,
                    colorModal: true,
                    colorModalAnimation: !this.state.colorModalAnimation,
                    section3ColorId: idFocus,
                    colorListValue: colorList == "" ? [] : colorList
                });
            }
        }


        // }

        document.onkeydown = function (t) {

            if (t.which == 9) {
                return false;
            }
        }
    }
    deselectallProp(data) {
        this.setState({
            colorListValue: data.colorList
        })
    }
    closePiColorModal() {
        document.getElementById(this.state.section3ColorId).focus()
        this.setState({
            colorModal: false,
            colorModalAnimation: !this.state.colorModalAnimation
        });
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }


    // ____________________________MODAL UPDATATION________________________

    getOpenToBuy(id, otbb) {

        // let poRows = this.state.poRows
        // let mrp = ""
        // let count =0
        // let otbArray =this.state.otbArray
        //  for(let z=0 ;z<poRows.length;z++){
        //     if(poRows[z].gridOneId==id){
        //     mrp = poRows[z].vendorMrp


        //     }

        //  }
        //  for(let z=0;z<poRows.length;z++){
        //       if(poRows[z].vendorMrp==mrp){
        //           count++
        //       }
        //  }

        // for(let i=0;i<poRows.length;i++){

        // if(poRows.length==1){

        //           if(poRows[i].gridOneId==id){
        //           let data={
        //               mrp :poRows[i].vendorMrp,
        //               otb:poRows[i].otb,
        //               amount:poRows[i].amount
        //           }
        //           otbArray.push(data)
        //         }
        //       }
        //     else{
        //         if(count==1){
        //             if(poRows[i].gridOneId==id){
        //                 let data={
        //                     mrp :poRows[i].vendorMrp,
        //                     otb:poRows[i].otb,
        //                     amount:poRows[i].amount
        //                 }
        //                 otbArray.push(data)
        //               }
        //         }else if(count>1) {

        //         for(let j=0;j<otbArray.length;j++){
        //             if(poRows[i].gridOneId==id){
        //                 if(otbArray[j].mrp == poRows[i].vendorMrp){
        //                     poRows[i].otb = otbArray[j].otb - otbArray[j].amount
        //                     otbArray[j].amount=  poRows[i].amount
        //                     otbArray[j].otb =poRows[i].otb - poRows[i].amount

        //                 }
        //             }
        //         }
        //     }  

        //     }


        // }
        // this.setState({
        //     otbArray:otbArray,
        //     poRows:poRows

        // })



        let rows = this.state.poRows

        let mrp = ""
        let count = 0
        let otb = []
        let netAmount = []
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].gridOneId == id) {

                mrp = rows[i].vendorMrp

            }
        }

        for (var i = 0; i < rows.length; i++) {
            if (rows[i].vendorMrp == mrp) {
                count++
            }
        }

        if (count > 1 && rows.length > 1) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].vendorMrp == mrp) {
                    if (rows[i].otb.toString() != "") {
                        otb.push(rows[i].otb)
                    }
                    if (rows[i].amount != "") {
                        netAmount.push(rows[i].amount)
                    }
                }

            }





            let minOtb = Math.min.apply(null, otb)


            let index = ""
            let netAmtFinal = ""
            for (var k = 0; k < otb.length; k++) {
                if (otb[k] == minOtb) {
                    index = k
                }
            }
            for (var l = 0; l < netAmount.length; l++) {
                netAmtFinal = netAmount[index]
            }


            for (var m = 0; m < rows.length; m++) {
                if (rows[m].gridOneId == id) {
                    rows[m].otb = minOtb - netAmtFinal
                }
            }


            this.setState({
                poRows: rows
            })
            setTimeout(() => {
                this.setOtbAfterDelete(id)

            }, 10)
        }
        if (count == 1) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].vendorMrp == mrp) {
                    rows[i].otb = otbb

                }
            }
            this.setState({
                poRows: rows
            })
        }
        this.getOtbForSecTwoRows()





    }

    setOtbAfterDelete(idx) {
        let poRows = this.state.poRows
        let otbb = ""
        let id = ""
        for (let i = 0; i < poRows.length; i++) {
            id = poRows[0].gridOneId
            if (poRows[i].gridOneId == id) {
                poRows[i].otb = this.state.totalOtb
                otbb = this.state.totalOtb
            }
            if (poRows[i].gridOneId > id) {
                poRows[i].otb = otbb - poRows[i - 1].amount
                otbb = otbb - poRows[i - 1].amount

            }

        }

        this.setState({
            poRows: poRows,
            deleteMrp: "",
            deleteOtb: ""

        })
        this.getOtbForSecTwoRows()


    }

    onChangeNetAmt(id, mrp) {

        let rows = this.state.poRows
        let otb = ""
        let netAmount = ""
        let idd = id


        for (var i = 0; i < rows.length; i++) {
            if (rows[i].gridOneId == idd) {
                otb = rows[i].otb
                netAmount = rows[i].amount

            }
            if (rows[i].gridOneId > idd) {

                if (rows[i].vendorMrp == mrp) {
                    rows[i].otb = otb - netAmount,
                        // idd = rows[i].rowId
                        // if(rows[i].rowId==idd){
                        otb = otb - netAmount
                    netAmount = rows[i].amount

                    // }
                }

            }
        }
        this.setState({
            poRows: rows
        })

    }
    updateNetAmt(id, designRow) {
        let poRows = this.state.poRows
        let secTwoRows = this.state.secTwoRows
        let quantity = 0
        let rate = ""
        let rsp = ""
        let finalRate = ""
        for (let i = 0; i < poRows.length; i++) {
            quantity = quantity + Number(poRows[i].quantity)

        }

        // let otbArray =this.state.otbArray
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].gridOneId == designRow) {

                rate = poRows[j].rate,

                    rsp = this.state.isRspRequired ? this.state.isRSP ? poRows[j].rsp : poRows[j].vendorMrp : 0
                finalRate = poRows[j].finalRate
            }
        }
        if (this.state.isRspRequired) {
            for (let i = 0; i < secTwoRows.length; i++) {

                if (secTwoRows[i].gridTwoId == id) {


                    // let intakedata = {
                    //     rowId: id,
                    //     rate: finalRate,
                    //     rsp: rsp,

                    //     gst: secTwoRows[i].gst,
                    //     designRow: designRow

                    // }

                    let intakedata = {
                        rate: finalRate,
                        rsp: rsp,

                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
                        hsnSacCode: this.state.hsnSacCode
                    }

                    this.setState({
                        markUpYes: true
                    })
                    this.props.markUpRequest(intakedata)


                }


            }

        }

        this.setState({
            poQuantity: quantity
        })




    }
    openClose(e) {

        this.setState({
            open: !this.state.open
        })
    }

    // updateSizeState(data1) {
    //     this.setState({
    //         catFive: data1.catFive
    //     })
    // }

    updateSizeList(sizeList) {
        console.log("sizeList", sizeList)
    }

    updateColorState(data2) {
        var cat6 = data2.colorData;
        var array = this.state.secTwoRows
        var arrayUdf = this.state.udfRows
        let setQty = ""
        let c = 0
        let designId = 0
        let rate = ""
        let mrp = ""
        let finalRate = ""
        for (var x = 0; x < array.length; x++) {
            if (array[x].gridTwoId == data2.colorrId) {
                designId = array[x].designRowId


            }
        }
        let poRows = this.state.poRows
        for (var k = 0; k < poRows.length; k++) {
            if (poRows[k].gridOneId == Number(designId)) {
                rate = poRows[k].rate
                finalRate = poRows[k].finalRate

                mrp = poRows[k].vendorMrp
            }
        }

        for (var x = 0; x < array.length; x++) {
            if (array[x].gridTwoId == data2.colorrId) {
                for (var i = 0; i < array[x].sizeList.length; i++) {
                    c += Number(array[x].sizeList[i].ratio)

                }


                array[x].color = cat6.join(',');
                array[x].option = data2.colorData.length
                setQty = array[x].setQty
                array[x].total = data2.colorData.length * c
                if (setQty != "") {
                    if (finalRate != 0 || finalRate != "") {
                        if (c != 0) {
                            let taxData = {
                                hsnSacCode: this.state.hsnSacCode,
                                qty: Number(setQty) * Number(data2.colorData.length * c),
                                rate: finalRate,
                                rowId: array[x].gridTwoId,
                                designRow: array[x].designRowId,
                                mrp: mrp,
                                piDate: new Date(),
                                supplierGstinStateCode: this.state.stateCode,
                                purtermMainCode: this.state.termCode,
                                siteCode: this.state.siteCode
                            }
                            if (this.state.hsnSacCode != "" || this.state.hsnSacCode != null) {


                                this.props.lineItemRequest(taxData)


                            } else {
                                this.hsn();
                                this.setState({
                                    errorMassage: "HSN code is complusory",
                                    poErrorMsg: true

                                })
                            }

                        }
                    } else {
                        this.setState({
                            errorMassage: "Rate is complusory",
                            poErrorMsg: true

                        })
                    }
                }
            }
        }
        this.setState({
            secTwoRows: array
        })
        document.getElementById(data2.section3ColorId).focus()

        for (var x = 0; x < arrayUdf.length; x++) {
            if (arrayUdf[x].udfGridId == data2.colorrId) {
                arrayUdf[x].udfColor = cat6.join(',')
            }
        }

        const t = this
        setTimeout(function () {
            if (setQty != "") {
                t.updateLineItem(data2.colorrId)
            }
            t.gridFourth();

        }, 1000)
    }
    updateColorList(udata) {
        let colorL = [...udata.colorList]
        let colorA = []
        var reCollectData = [];
        let index = 0

        colorL.forEach(color => {
            if (!colorA.includes(color.code)) {


                let colorData = {
                    id: index++,
                    code: color.code,
                    cname: color.cname,

                }
                colorA.push(color.code)
                reCollectData.push(colorData);
            }
        })

        var array = this.state.secTwoRows
        if (colorL.length != 0) {
            for (var x = 0; x < array.length; x++) {

                if (array[x].gridTwoId == udata.colorrId) {
                    array[x].colorList = reCollectData

                }
            }
        }

        this.setState({
            secTwoRows: array
        })

    }

    // _____________________ROW FUNCTIONALITY__________________
    handleAddRow = () => {
        var array = this.state.poRows;
        var idd = []
        if (array.length > 0) {
            for (var i = 0; i < array.length; i++) {
                idd.push(array[i].gridOneId)
            }
            var finalId = Math.max(...idd)
            let x = false
            for (var i = 0; i < array.length; i++) {
                if (array[i].amount != "") {

                    x = true

                } else {

                    x = false
                }
            }
            if (x) {

                this.addFirstRow(finalId);
                this.fun(finalId);
            } else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Net Amount is mandatory to add row "
                })
            }
        } else {
            this.addFirstRow(finalId);
            this.fun(finalId);
        }
    };
    addFirstRow(finalId) {
        let itemudfList = []
        if (this.state.itemUdfExist == "true") {
            let itemUdf = this.state.itemUdf

            itemUdf[0].udfList.forEach(c => {
                let udf = {
                    cat_desc_udf: c.cat_desc_udf,
                    displayName: c.displayName,
                    isCompulsoryPO: c.isCompulsoryPO,
                    isLovPO: c.isLovPO,
                    value: ""

                }
                itemudfList.push(udf)
            });
        }
        let catDescRows = this.state.catDescRows
        let catDescList = []
        catDescRows[0].itemDetails.forEach(c => {
            let udf = {
                catdesc: c.catdesc,
                displayName: c.displayName,
                isCompulsoryPO: c.isCompulsoryPO,
                isLovPO: c.isLovPO,
                value: "",
                code: ""

            }
            catDescList.push(udf)
        });
        let firstRows = this.state.poRows;


        let marginRule = ""
        for (var i = 0; i < firstRows.length; i++) {


            marginRule = firstRows[0].marginRule
        }

        // if (this.state.poRows.length < 10) {

        const id = finalId + 1
        const item = {
            finalRate: "",
            color: [],
            size: [],
            ratio: [],
            vendorMrp: "",
            vendorDesign: "",
            mrk: [],
            discount: {
                discountType: "",
                discountValue: ""
            },
            rate: "",
            netRate: "",

            rsp: "",
            mrp: "",
            quantity: "",
            amount: "",
            otb: "",
            remarks: "",
            gst: [],
            finCharges: [],
            concatFinCharges: [],
            basic: "",
            tax: [],
            calculatedMargin: [],
            gridOneId: id,
            designRowId: id,
            deliveryDate: this.state.lastInDate,
            typeOfBuying: "",
            marginRule: marginRule,
            image: [],

            imageUrl: {},
            containsImage: false,
        };

        const catDescItem = {
            vendorDesign: "",
            itemDetails: catDescList,
            designRowId: id,
            gridId: id
        }

        if (this.state.itemUdfExist == "true") {
            const itemUdfobj = {
                itemUdfGridId: id,
                designRowId: id,
                vendorDesign: "",
                udfList: itemudfList
            }

            this.setState({
                isDescriptionChecked: false,
                poRows: [...this.state.poRows, item],
                catDescRows: [...this.state.catDescRows, catDescItem],
                itemUdf: [...this.state.itemUdf, itemUdfobj]
            });
        }
        else {
            this.setState({
                isDescriptionChecked: false,
                poRows: [...this.state.poRows, item],
                catDescRows: [...this.state.catDescRows, catDescItem],
                itemUdf: []

            });
        }
        // }
        this.handleSecTwoAddRow()
    }

    handleRemoveSpecificRow = (idx, otb, mrp) => () => {
        const rows = this.state.poRows

        if (rows.length > 1) {
            this.setState({
                resetAndDelete: true,
                headerMsg: "Are you sure you want to delete row?",
                paraMsg: "Click confirm to continue.",
                rowDelete: "row Delete",
                rowsId: idx,
                otbValueData: otb,
                mrpValueData: mrp
            })

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })

        }

    }

    DeleteRowPo(idx, otb, mrp) {

        var rows = this.state.poRows
        for (var z = 0; z < rows.length; z++) {
            if (rows[z].gridOneId == idx) {
                rows.splice(z, 1)
            }
        }
        this.removeCatDescRow(idx);
        this.state.itemUdfExist == "true" ? this.removeUdfItemRows(idx) : null
        this.state.isUDFExist == "true" ? this.removeUdfViaUpperRow(idx) : null
        this.removeSecViaUpperRow(idx);
        this.setState({
            poRows: rows,
            deleteMrp: mrp,
            deleteOtb: otb,

        })
        setTimeout(() => {
            this.setOtbAfterDelete(idx)
        })
        // document.getElementById("addIndentRow").disabled = false;
    }
    removeSecViaUpperRow(idx) {
        let secTwoRows = this.state.secTwoRows

        for (var z = 0; z < secTwoRows.length;) {
            if (secTwoRows[z].designRowId == idx) {


                secTwoRows.splice(z, 1)
                z = 0;

            } else {
                z++;
            }
        }

        let setNo = 1
        for (var j = 0; j < secTwoRows.length; j++) {
            secTwoRows[j].setNo = setNo++
        }
        this.setState({

            secTwoRows: secTwoRows
        })

    }
    removeUdfViaUpperRow(idx) {
        let udfRows = this.state.udfRows

        for (var z = 0; z < udfRows.length;) {
            if (udfRows[z].designRowId == idx) {
                udfRows.splice(z, 1)
                z = 0
            } else {
                z++
            }
        }
        let udfId = 1
        for (var i = 0; i < udfRows.length; i++) {
            udfRows[i].setUdfNo = udfId++
        }
        this.setState({

            udfRows: udfRows
        })
    }


    removeSecTwoRows(idx) {
        let secTwoRows = this.state.secTwoRows

        for (var z = 0; z < secTwoRows.length; z++) {
            if (secTwoRows[z].gridTwoId == idx) {
                secTwoRows.splice(z, 1)

            }
        }
        let setNo = 1
        for (var j = 0; j < secTwoRows.length; j++) {
            secTwoRows[j].setNo = setNo++
        }

        this.setState({

            secTwoRows: secTwoRows
        })

    }
    removeCatDescRow(idx) {
        let catDescRows = this.state.catDescRows
        // if (catDescRows.length > 1) {
        for (var z = 0; z < catDescRows.length; z++) {
            if (catDescRows[z].gridId == idx) {
                catDescRows.splice(z, 1)

            }
        }
        // }
        this.setState({

            catDescRows: catDescRows
        })

    }
    checkSpecificRow(id) {
        let finalRows = this.state.poRows;
        for (var i = 0; i < finalRows.length; i++) {
            if (finalRows[i].gridOneId == id) {
                if (finalRows[i].checked) {
                    finalRows[i].checked = false
                    this.setState({
                        isDescriptionChecked: false
                    })
                } else {
                    finalRows[i].checked = true
                    let x = 0;
                    for (var j = 0; j < finalRows.length; j++) {
                        if (finalRows[j].checked) {
                            x++;
                        }
                    }
                    if (x == finalRows.length) {
                        this.setState({
                            isDescriptionChecked: true,
                        })
                    }
                }
            }
        }
        this.setState({
            poRows: finalRows
        })
    }

    onDescription(e) {
        let r = this.state.poRows
        if (this.state.isDescriptionChecked) {
            for (var i = 0; i < r.length; i++) {
                r[i].checked = false
            }
        } else {
            for (var i = 0; i < r.length; i++) {
                r[i].checked = true
            }
        }
        this.setState({
            isDescriptionChecked: !this.state.isDescriptionChecked,
            poRows: r
        })
    }
    // ---------------------------------------------SECTION 2 ADD row------------------------------
    handleItemUdfAddRow = () => {
        var array = this.state.itemUdf;
        var idd = []
        if (array.length > 0) {
            for (var i = 0; i < array.length; i++) {
                idd.push(array[i].gridTwoId)
            }
            var finalId = Math.max(...idd)
            let x = false
            for (var i = 0; i < array.length; i++) {

                if (array[i].vendorDesign != "") {
                    x = true

                } else {

                    x = false
                }
            }
        }
    }
    // _____________________________SECTION3 ADDROW_________________________-
    handleSecTwoAddRow = () => {

        var array = this.state.secTwoRows;
        var idd = []
        if (array.length > 0) {
            for (var i = 0; i < array.length; i++) {
                idd.push(array[i].gridTwoId)
            }
            var finalId = Math.max(...idd)
            let x = false
            //     for (var i = 0; i < array.length; i++) {

            //         if (array[i].color.length != 0 && array[i].vendorDesignNo != "") {
            //             x = true

            //         } else {

            //             x = false
            //         }
            //     }
            //     if (x) {

            //         this.fun(finalId);


            //     } else {
            //         this.setState({
            //             poErrorMsg: true,
            //             errorMassage: "Vendordesign No. and color is mandatory to add row"
            //         })

            //     }
            // } else {
            this.fun(finalId);


        }
    };

    fun(finalId) {
        let udfRows = this.state.udfRows
        var array = this.state.secTwoRows;
        var idd = []
        let sets = []
        let setUdfList = []
        let finalSetnO = ""
        for (let i = 0; i < array.length; i++) {
            sets.push(array[i].setNo)

        }
        finalSetnO = Math.max(...sets)
        if (array.length > 0) {
            for (var i = 0; i < array.length; i++) {
                idd.push(array[i].gridTwoId)
            }
            var finalSecId = Math.max(...idd)

        }

        if (this.state.isUDFExist == "true") {

            udfRows[0].setUdfList.forEach(c => {
                let udf = {
                    displayName: c.displayName,
                    udfType: c.udfType,
                    isCompulsary: c.isCompulsary,
                    isLovPO: c.isLovPO,
                    value: ""

                }
                setUdfList.push(udf)
            });
        }
        let c = this.state.sizeMapping
        let sizeArray = []
        c.forEach(element => {
            var array = {}
            array = {
                orderBy: element.orderBy,
                code: element.code,
                cname: element.cname
            }
            sizeArray.push(array)

        });

        for (var i = 0; i < sizeArray.length; i++) {
            sizeArray[i].ratio = ""
        }


        const id = finalId + 1
        const secFinalId = finalSecId + 1
        let num = finalSetnO + 1

        const itemGridTwo = {
            discount: {
                discountType: "",
                discountValue: ""
            },
            finalRate: "",
            colorChk: false,
            icodeChk: false,
            designRowId: id,
            color: this.state.isColorRequired ? [] : ["NA"],
            colorList: this.state.isColorRequired ? [] : [{ id: 1, code: "", cname: "NA" }],
            itemBarcode: "",
            sizeListData: [],
            colorSizeList: [],
            icodes: [],
            vendorDesignNo: "",
            option: 1,
            setNo: num,
            total: "",
            size: "",
            setRatio: "",
            setQty: "",
            quantity: "",
            amount: "",
            ratio: [],
            rate: "",
            gst: "",
            finCharges: [],
            tax: "",
            calculatedMargin: "",
            mrk: "",
            gridTwoId: secFinalId,
            sizeList: sizeArray,
            otb: "",
            totalQuantity: "",
            totalNetAmt: "",
            totaltax: [],
            totalCalculatedMargin: [],
            totalgst: [],
            totalIntakeMargin: [],
            totalFinCharges: [],
            totalBasic: "",
            totalCalculatedOtb: "",

            catOneCode: "",
            catOneName: "",
            catTwoCode: "",
            catTwoName: "",
            catThreeCode: "",
            catThreeName: "",
            catFourCode: "",

            catFourName: "",
            catFiveCode: "",
            catFiveName: "",
            catSixCode: "",
            catSixName: "",
            descOneCode: "",
            descOneName: "",
            descTwoCode: "",
            descTwoName: "",
            descThreeCode: "",
            descThreeName: "",
            descFourCode: "",
            descFourName: "",
            descFiveCode: "",
            descFiveName: "",
            descSixCode: "",
            descSixName: "",
            itemudf1: "",
            itemudf2: "",
            itemudf3: "",
            itemudf4: "",
            itemudf5: "",
            itemudf6: "",
            itemudf7: "",
            itemudf8: "",
            itemudf9: "",
            itemudf10: "",
            itemudf11: "",
            itemudf12: "",
            itemudf13: "",
            itemudf14: "",
            itemudf15: "",
            itemudf16: "",
            itemudf17: "",
            itemudf18: "",
            itemudf19: "",
            itemudf20: "",
            deliveryDate: "",
            typeOfBuying: "",
            marginRule: "",
            image: [],
            imageUrl: {},
            containsImage: false,
            lineItemChk: false

        };

        if (this.state.isUDFExist == "true") {
            const udfGrid = {
                setUdfNo: num,
                udfColor: [],
                setUdfList: setUdfList,

                designRowId: id,
                udfGridId: secFinalId,

            };
            this.setState({
                isSecTwoDescriptionChecked: false,
                secTwoRows: [...this.state.secTwoRows, itemGridTwo],
                isUdfDescriptionChecked: false,
                udfRows: [...this.state.udfRows, udfGrid],
            });

            setTimeout(() => {
                this.getOtbForSecTwoRows()
            }, 10)
        }
        else {
            this.setState({
                isSecTwoDescriptionChecked: false,
                secTwoRows: [...this.state.secTwoRows, itemGridTwo],
                udfRows: []
            });

            setTimeout(() => {
                this.getOtbForSecTwoRows()
            }, 10)
        }
    }
    deleteSecTwoRows = (idxx, setNo) => () => {
        const rows = this.state.secTwoRows
        if (rows.length > 1) {
            this.setState({
                deleteConfirmModal: true,
                deleteGridId: idxx,
                deleteSetNo: setNo,
                headerMsg: "Are you sure to delete the row?",
                paraMsg: "Click confirm to continue.",

            })
        }
        else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })
        }
    }

    handleRemoveSpecificSecRow(idxx, setNo) {

        let number = 1
        const rows = this.state.secTwoRows
        let flag = false
        let vDesi = ""
        let index = 0
        let designRow = ""

        for (var z = 0; z < rows.length; z++) {
            if (rows[z].gridTwoId == idxx) {
                vDesi = rows[z].vendorDesignNo
                designRow = rows[z].designRowId

            }
        }
        if (!this.state.isVendorDesignNotReq) {
            for (var z = 0; z < rows.length; z++) {
                if (vDesi == rows[z].vendorDesignNo) {
                    index++

                }
            }
        } else {
            for (var z = 0; z < rows.length; z++) {
                if (designRow == rows[z].designRowId) {
                    index++

                }
            }
        }

        if (index > 1) {

            for (var z = 0; z < rows.length; z++) {
                if (rows[z].gridTwoId == idxx) {
                    rows.splice(z, 1)
                }
            }
            for (var z = 0; z < rows.length; z++) {

                rows[z].setNo = number++


            }

            this.state.isUDFExist == "true" ? this.removeUdfRows(idxx, number) : null
            setTimeout(() => {
                this.updatePorows()
            }, 100)

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row  can't be deleted"
            })


        }


        this.setState({
            secTwoRows: rows
        })
        setTimeout(() => {
            this.getOtbForSecTwoRows()
        }, 10)


    }

    updatePorows() {
        let secTwoRows = this.state.secTwoRows

        let poRows = this.state.poRows
        for (let k = 0; k < poRows.length; k++) {
            poRows[k].amount = ""
            poRows[k].quantity = ""
            poRows[k].calculatedMargin = []
            poRows[k].mrk = []
            poRows[k].tax = []
            poRows[k].gst = []
            poRows[k].finCharges = []
            poRows[k].basic = ""
            poRows[k].concatFinCharges = []

        }
        for (let i = 0; i < secTwoRows.length; i++) {
            for (let j = 0; j < poRows.length; j++) {
                if (secTwoRows[i].designRowId == poRows[j].gridOneId) {
                    // let finchg = poRows[j].finCharges.concat(secTwoRows[i].finCharges)
                    // let chgCodeArr =[]
                    // let concatFin=[]

                    // console.log(finchg)
                    // for(i=0 ;i<finchg.length;i++){
                    //     if(finchg[i].chgCode!=undefined){
                    //      if(!chgCodeArr.includes(finchg[i].chgCode)){
                    //          let obj={
                    //              chgCode:finchg[i].chgCode,
                    //              chgName:finchg[i].chgName!=undefined?finchg[i].chgName:"",
                    //              seq
                    //          }

                    //      }
                    //     }
                    // }
                    poRows[j].amount = Math.round((Number(poRows[j].amount) + Number(secTwoRows[i].amount)) * 100) / 100

                    poRows[j].quantity = Number(poRows[j].quantity) + Number(secTwoRows[i].quantity)

                    poRows[j].calculatedMargin = this.state.isRspRequired ? [...poRows[j].calculatedMargin, secTwoRows[i].calculatedMargin.toString()] : []

                    poRows[j].mrk = this.state.isRspRequired ? [...poRows[j].mrk, secTwoRows[i].mrk.toString()] : []

                    poRows[j].tax = [...poRows[j].tax, secTwoRows[i].tax.toString()]

                    poRows[j].gst = [...poRows[j].gst, secTwoRows[i].gst.toString()]

                    poRows[j].finCharges = poRows[j].finCharges.concat(secTwoRows[i].finCharges)

                    poRows[j].basic = Number(poRows[j].basic) + Number(Number(poRows[j].rate) * Number(secTwoRows[i].setQty))





                }
            }
        }
        this.setState({
            poRows: poRows
        })
        setTimeout(() => {
            this.getOtbLastInDate(1, this.state.totalOtb)
            this.updatePoAmountNpoQuantity()
            this.gridFirst()

        }, 10)

    }
    updatePoAmountNpoQuantity() {

        let poRows = this.state.poRows
        let poQuantity = ""
        let poAmount = ""
        for (let j = 0; j < poRows.length; j++) {
            poAmount = Number(poAmount) + Number(poRows[j].amount)
            poQuantity = Number(poQuantity) + Number(poRows[j].quantity)
        }
        this.setState({
            poAmount: poAmount,
            poQuantity: poQuantity

        })

    }

    handleRemoveSpecificItemUdfRow = (idxx) => () => {
        const rows = [...this.state.itemUdf]
        if (rows.length > 1) {
            for (var z = 0; z < rows.length; z++) {
                if (rows[z].itemUdfGridId == idxx) {
                    rows.splice(z, 1)
                }
            }
            this.removeUdfItemRows(idx);

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })
        }
        this.setState({
            itemUdf: rows
        })



    }


    removeUdfItemRows(idxx) {
        const itemUdf = this.state.itemUdf
        // if (itemUdf.length > 1) {
        for (var zy = 0; zy < itemUdf.length; zy++) {
            if (itemUdf[zy].itemUdfGridId == idxx) {
                itemUdf.splice(zy, 1)
            }
        }
        // }
        this.setState({
            itemUdf: itemUdf
        })
    }

    removeUdfRows(idxx, setNo) {
        const udfRows = this.state.udfRows
        for (var zy = 0; zy < udfRows.length; zy++) {
            if (udfRows[zy].udfGridId == idxx) {
                udfRows.splice(zy, 1)
            }
        }
        let setNum = 1
        if (setNo != "") {
            for (var i = 0; i < udfRows.length; i++) {

                udfRows[i].setUdfNo = setNum++



            }
        }


        // }
        this.setState({
            udfRows: udfRows
        })
    }
    checkSpecificSecRow(id) {
        let finalRows = this.state.secTwoRows;
        for (var i = 0; i < finalRows.length; i++) {
            if (finalRows[i].gridTwoId == id) {
                if (finalRows[i].checked) {
                    finalRows[i].checked = false
                    this.setState({
                        isSecTwoDescriptionChecked: false
                    })
                } else {
                    finalRows[i].checked = true
                    let x = 0;
                    for (var j = 0; j < finalRows.length; j++) {
                        if (finalRows[j].checked) {
                            x++;
                        }
                    }
                    if (x == finalRows.length) {
                        this.setState({
                            isSecTwoDescriptionChecked: true,
                        })
                    }
                }
            }
        }
        this.setState({
            secTwoRows: finalRows
        })
    }

    //Item udf row
    checkSpecificItemUdfRow(id) {
        let finalRows = this.state.itemUdf;
        for (var i = 0; i < finalRows.length; i++) {
            if (finalRows[i].itemUdfGridId == id) {
                if (finalRows[i].checked) {
                    finalRows[i].checked = false
                    this.setState({
                        isItemUdfDescriptionChecked: false
                    })
                } else {
                    finalRows[i].checked = true
                    let x = 0;
                    for (var j = 0; j < finalRows.length; j++) {
                        if (finalRows[j].checked) {
                            x++;
                        }
                    }
                    if (x == finalRows.length) {
                        this.setState({
                            isItemUdfDescriptionChecked: true,
                        })
                    }
                }
            }
        }
        this.setState({
            itemUdf: finalRows
        })
    }


    onDescriptionSec(e) {
        let r = this.state.secTwoRows
        if (this.state.isSecTwoDescriptionChecked) {
            for (var i = 0; i < r.length; i++) {
                r[i].checked = false
            }
        } else {
            for (var i = 0; i < r.length; i++) {
                r[i].checked = true
            }
        }
        this.setState({
            isSecTwoDescriptionChecked: !this.state.isSecTwoDescriptionChecked,
            secTwoRows: r
        })
    }
    updateTransporterState(code) {

        this.setState({

            transporterCode: code.transporterCode,
            transporterName: code.transporterName,

            transporter: code.transporterName,

        },
            () => {
                this.transporter();
            })

        document.getElementById("transporter").focus()


    }
    updateLoadIndentState(code) {

        this.setState({
            getPoDataRequest: code.sCode,
            loadIndent: code.pattern,
            loadIndentId: code.indentId
        })
        let data = {
            orderDetailId: code.pattern,
            orderId: code.sCode
        }

        this.props.getPoDataRequest(data);

        document.getElementById("loadIndentInput").focus()



    }
    updatePoData(data) {
        if (!this.state.loadIndentTrue) {
            let poRows = []
            let id = 1;

            let designArray = []


            let imageUrl = {};
            let piDetails = data.piDetails != undefined ? data.piDetails : data.draft.poDetails

            // let piDetails = data.piDetails
            piDetails.length > 0 && piDetails.forEach(pi => {

                let color = [];
                let size = [];
                let ratio = [];
                let tax = [];
                let gst = []
                let mrk = []
                let calculatedMargin = []

                pi.colorList ? pi.colorList.forEach(co => {
                    color.push(co.cname)
                }) : []
                pi.sizeList ? pi.sizeList.forEach(si => {
                    size.push(si.cname)
                }) : []
                pi.image ? pi.image.forEach(img => {
                    imageUrl[img.split('/')[9].split('?')[0]] = img
                }) : {}
                pi.ratioList ? pi.ratioList.forEach(ra => {
                    ratio.push(ra.ratio)
                }) : []
                let piData = {

                    setHeaderId: "",
                    color: color,
                    size: size,
                    ratio: ratio,
                    vendorMrp: pi.mrp,
                    vendorDesign: pi.design,
                    mrk: [...mrk, pi.intakeMargin],
                    discount: {
                        discountType: pi.discountType ? pi.discountType : '',
                        discountValue: pi.discountValue ? pi.discountValue : ''
                    },
                    rate: pi.rate,
                    finalRate: sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? pi.rate : pi.finalRate,


                    netRate: pi.rate,
                    basic: pi.noOfSets * (pi.discountType == "" || pi.discountType == "null" || pi.discountType == null ? pi.rate : pi.finalRate),
                    rsp: pi.rsp,
                    mrp: pi.mrp,
                    quantity: pi.quantity,
                    amount: pi.netAmountTotal,
                    remarks: pi.remarks,
                    otb: pi.otb,
                    gst: [...gst, pi.gst],
                    finCharges: pi.finCharge,
                    tax: [...tax, pi.tax],
                    calculatedMargin: [...calculatedMargin, pi.calculatedMargin],
                    gridOneId: id++,
                    deliveryDate: this.state.lastInDate,
                    typeOfBuying: pi.typeOfBuying,
                    marginRule: pi.marginRule,
                    image: Object.keys(imageUrl),

                    imageUrl: imageUrl,
                    containsImage: pi.containsImage,

                }
                poRows.push(piData)

            })



            this.setState({

                stateCode: data.stateCode,
                hsnSacCode: piDetails.length != 0 ? piDetails[0].hsnSacCode : "",
                hsnCode: piDetails.length != 0 ? piDetails[0].hsnCode : "",
                division: data.hl1Name,
                section: data.hl2Name,
                department: data.hl3Name,
                divisionCode: data.hl1Code,
                sectionCode: data.hl2Code,
                departmentCode: data.hl3Code,
                slCode: data.slCode,
                slAddr: data.slAddr,
                slName: data.slName,
                city: data.slCityName,
                slCityName: data.slCityName,
                supplier: data.slName + "-" + data.slAddr,
                leadDays: data.leadTime,
                termCode: data.termCode,
                termName: data.termName,
                transporterCode: data.transporterCode,
                transporterName: data.transporterName,
                transporter: data.transporterName,
                term: data.termName,
                startRange: piDetails.length != 0 ? piDetails[0].mrpStart : "",
                endRange: piDetails.length != 0 ? piDetails[0].mrpEnd : "",
                vendorMrpPo: piDetails.length != 0 ? piDetails[0].hl4Code : "",
                articleName: piDetails.length != 0 ? piDetails[0].hl4Name : "",
                poRows: poRows,
                poQuantity: data.poQuantity,
                poAmount: data.poAmount,
                loadIndentTrue: true,
                itemUdfExist: data.itemUdfExist,
                isUDFExist: data.isUDFExist,
                siteCode: data.siteCode,
                siteName: data.siteName,
                isSiteExist: data.isSiteExist,
                copyColor: data.copyColor



            })

            let poData = {
                deptName: data.hl3Name,
                deptCode: data.hl3Code,
                hl1Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.hl1Name : "",
                hl2Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.hl2Name : "",
                hl4Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.piDetails[0].hl4Name : "",
            }
            // this.props.poSizeRequest(poData)
            let Payload = {
                hl3Code: data.hl3Code,
                hl3Name: data.hl3Name,
            }
            this.props.getItemDetailsRequest(Payload)
            if (data.itemUdfExist == "true") {


                let uData = {
                    hl3Code: data.hl3Code,
                    ispo: true,
                    hl3Name: data.hl3Name

                }
                this.props.getCatDescUdfRequest(uData)
            }
            if (data.isUDFExist == "true") {
                let udfdata = {

                    type: 1,
                    search: "",
                    isCompulsary: "",
                    displayName: "",
                    udfType: "",
                    ispo: true
                }
                this.props.getUdfMappingRequest(udfdata);
            }

            const t = this
            setTimeout(function () {
                let poRows = t.state.poRows
                if (poRows[0].vendorMrp != "" && this.state.isMrpRequired && this.state.displayOtb) {
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                    ];
                    const date = new Date(t.state.lastInDate);
                    var year = date.getFullYear()
                    var monthName = (monthNames[date.getMonth()]);
                    let dataa = {
                        rowId: poRows[0].gridOneId,
                        articleCode: t.state.vendorMrpPo,
                        mrp: poRows[0].vendorMrp,
                        month: monthName,
                        year: year
                    }

                    t.props.otbRequest(dataa)
                }

            }, 10)

        } else if (this.state.loadIndentTrue) {

            setTimeout(() => {
                let imageUrl = {};

                let udfId = 1;
                let secId = 1;
                let itemId = 1;
                let catDescId = 1;
                let setNo = 1
                let setNum = 1
                let designSec = 1
                let designItem = 1
                let designItemDetail = 1
                let designUdf = 1
                let udfMappingData = this.state.udfMappingData
                let udfRowMapping = this.state.udfRowMapping
                let itemDetailsHeader = this.state.itemDetailsHeader
                let iUdf = []
                let udf = []
                let secRows = []
                let itemD = []

                let catDescRows = this.state.catDescRows
                let piDetails = data.piDetails
                let designArrayy = []
                if (data.itemUdfExist == "true") {
                    piDetails.forEach(pi => {

                        udfRowMapping.forEach(mapData => {
                            mapData.value = ""
                        })

                        udfRowMapping.forEach(list => {
                            if (list.cat_desc_udf == "UDFSTRING01") {
                                list.value = pi.udf.itemudf1
                            } else if (list.cat_desc_udf == "UDFSTRING02") {
                                list.value = pi.udf.itemudf2
                            } else if (list.cat_desc_udf == "UDFSTRING03") {
                                list.value = pi.udf.itemudf3
                            }
                            else if (list.cat_desc_udf == "UDFSTRING04") {
                                list.value = pi.udf.itemudf4
                            }
                            else if (list.cat_desc_udf == "UDFSTRING05") {
                                list.value = pi.udf.itemudf5
                            }
                            else if (list.cat_desc_udf == "UDFSTRING06") {
                                list.value = pi.udf.itemudf6
                            }
                            else if (list.cat_desc_udf == "UDFSTRING07") {
                                list.value = pi.udf.itemudf7
                            }
                            else if (list.cat_desc_udf == "UDFSTRING08") {
                                list.value = pi.udf.itemudf8
                            }
                            else if (list.cat_desc_udf == "UDFSTRING09") {
                                list.value = pi.udf.itemudf9
                            }
                            else if (list.cat_desc_udf == "UDFSTRING10") {
                                list.value = pi.udf.itemudf10
                            } else if (list.cat_desc_udf == "UDFNUM01") {
                                list.value = pi.udf.itemudf11

                            } else if (list.cat_desc_udf == "UDFNUM02") {
                                list.value = pi.udf.itemudf12
                            } else if (list.cat_desc_udf == "UDFNUM03") {
                                list.value = pi.udf.itemudf13
                            }
                            else if (list.cat_desc_udf == "UDFNUM04") {
                                list.value = pi.udf.itemudf14
                            }
                            else if (list.cat_desc_udf == "UDFNUM05") {
                                list.value = pi.udf.itemudf15
                            }
                            else if (list.cat_desc_udf == "UDFDATE01") {
                                list.value = pi.udf.itemudf16 == null ? "" : pi.udf.itemudf16
                            }
                            else if (list.cat_desc_udf == "UDFDATE02") {
                                list.value = pi.udf.itemudf17 == null ? "" : pi.udf.itemudf17
                            }
                            else if (list.cat_desc_udf == "UDFDATE03") {
                                list.value = pi.udf.itemudf18 == null ? "" : pi.udf.itemudf18
                            }
                            else if (list.cat_desc_udf == "UDFDATE04") {
                                list.value = pi.udf.itemudf19 == null ? "" : pi.udf.itemudf19
                            }
                            else if (list.cat_desc_udf == "UDFDATE05") {
                                list.value = pi.udf.itemudf20 == null ? "" : pi.udf.itemudf20
                            }
                        })

                        let udfRowMappings = _.map(
                            _.uniq(
                                _.map(udfRowMapping, function (obj) {

                                    return JSON.stringify(obj);
                                })
                            ), function (obj) {
                                return JSON.parse(obj);
                            }
                        );
                        let data = {
                            designRowId: designItem++,
                            itemUdfGridId: itemId++,
                            vendorDesign: pi.design,
                            udfList: udfRowMappings

                        }
                        iUdf.push(data)

                    })
                }
                let itemDesignHeader = []

                piDetails.forEach(pi => {

                    itemDetailsHeader.forEach(mapData => {
                        mapData.value = ""
                        mapData.code = ""
                    })
                    itemDetailsHeader.forEach(list => {

                        if (list.catdesc == "CAT1") {
                            list.value = pi.cat1Name
                            list.code = pi.cat1Code

                        } else if (list.catdesc == "CAT2") {
                            list.code = pi.cat2Code
                            list.value = pi.cat2Name
                        } else if (list.catdesc == "CAT3") {
                            list.code = pi.cat3Code
                            list.value = pi.cat3Name
                        }
                        else if (list.catdesc == "CAT4") {
                            list.code = pi.cat4Code
                            list.value = pi.cat4Name
                        }
                        else if (list.catdesc == "CAT5") {
                            list.code = pi.cat5Code
                            list.value = pi.cat5Name
                        }
                        else if (list.catdesc == "CAT6") {
                            list.code = pi.cat6Code
                            list.value = pi.cat6Name
                        }
                        else if (list.catdesc == "DESC1") {
                            list.code = pi.desc1Code
                            list.value = pi.desc1Name
                        }
                        else if (list.catdesc == "DESC2") {
                            list.code = pi.desc2Code
                            list.value = pi.desc2Name
                        }
                        else if (list.catdesc == "DESC3") {
                            list.code = pi.desc3Code
                            list.value = pi.desc3Name
                        }
                        else if (list.catdesc == "DESC4") {
                            list.code = pi.desc4Code
                            list.value = pi.desc4Name
                        } else if (list.catdesc == "DESC5") {
                            list.code = pi.desc5Code
                            list.value = pi.desc5Name

                        } else if (list.catdesc == "DESC6") {
                            list.code = pi.desc6Code
                            list.value = pi.desc6Name

                        }

                    })

                    let itemDetailsHeaders = _.map(
                        _.uniq(
                            _.map(itemDetailsHeader, function (obj) {

                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    let payload = {
                        designRowId: designItemDetail++,
                        vendorDesign: pi.design,
                        gridId: catDescId++,
                        itemDetails: itemDetailsHeaders
                    }
                    itemD.push(payload)


                })
                if (data.isUDFExist == "true") {
                    piDetails.forEach(pi => {
                        let color = [];
                        let size = [];
                        pi.colorList.forEach(co => {
                            color.push(co.cname)
                        });
                        pi.sizeList.forEach(si => {
                            size.push(si.cname)
                        })
                        udfMappingData.forEach(mapData => {
                            mapData.value = ""
                        })

                        udfMappingData.forEach(list => {

                            if (list.udfType == "SMUDFSTRING01") {
                                list.value = pi.udf.udf1

                            } else if (list.udfType == "SMUDFSTRING02") {
                                list.value = pi.udf.udf2
                            } else if (list.udfType == "SMUDFSTRING03") {
                                list.value = pi.udf.udf3
                            }
                            else if (list.udfType == "SMUDFSTRING04") {
                                list.value = pi.udf.udf4
                            }
                            else if (list.udfType == "SMUDFSTRING05") {
                                list.value = pi.udf.udf5
                            }
                            else if (list.udfType == "SMUDFSTRING06") {
                                list.value = pi.udf.udf6
                            }
                            else if (list.udfType == "SMUDFSTRING07") {
                                list.value = pi.udf.udf7
                            }
                            else if (list.udfType == "SMUDFSTRING08") {
                                list.value = pi.udf.udf8
                            }
                            else if (list.udfType == "SMUDFSTRING09") {
                                list.value = pi.udf.udf9
                            }
                            else if (list.udfType == "SMUDFSTRING10") {
                                list.value = pi.udf.udf10
                            } else if (list.udfType == "SMUDFNUM01") {
                                list.value = pi.udf.udf11

                            } else if (list.udfType == "SMUDFNUM02") {
                                list.value = pi.udf.udf12
                            } else if (list.udfType == "SMUDFNUM03") {
                                list.value = pi.udf.udf13
                            }
                            else if (list.udfType == "SMUDFNUM04") {
                                list.value = pi.udf.udf14
                            }
                            else if (list.udfType == "SMUDFNUM05") {
                                list.value = pi.udf.udf15
                            }
                            else if (list.udfType == "SMUDFDATE01") {
                                list.value = pi.udf.udf16 == null ? "" : pi.udf.udf16
                            }
                            else if (list.udfType == "SMUDFDATE02") {
                                list.value = pi.udf.udf17 == null ? "" : pi.udf.udf17
                            }
                            else if (list.udfType == "SMUDFDATE03") {
                                list.value = pi.udf.udf18 == null ? "" : pi.udf.udf18
                            }
                            else if (list.udfType == "SMUDFDATE04") {
                                list.value = pi.udf.udf19 == null ? "" : pi.udf.udf19
                            }
                            else if (list.udfType == "SMUDFDATE05") {
                                list.value = pi.udf.udf20 == null ? "" : pi.udf.udf20
                            }
                        })
                        let udfMappingDatas = _.map(
                            _.uniq(
                                _.map(udfMappingData, function (obj) {

                                    return JSON.stringify(obj);
                                })
                            ), function (obj) {
                                return JSON.parse(obj);
                            }
                        );

                        let data = {
                            designRowId: udfId++,
                            udfColor: color,
                            udfGridId: designUdf++,
                            setUdfNo: setNo++,
                            setUdfList: udfMappingDatas

                        }
                        udf.push(data)

                        // }
                    })
                }
                // })

                piDetails.forEach(pi => {
                    let color = [];
                    let size = [];

                    let sizeRatio = []
                    let sum = 0

                    pi.colorList.forEach(co => {
                        color.push(co.cname)
                    });
                    pi.sizeList.forEach(si => {
                        size.push(si.cname)
                    })
                    pi.image.forEach(img => {
                        imageUrl[img.split('/')[9].split('?')[0]] = img
                    })


                    for (let i = 0; i < pi.sizeList.length; i++) {
                        for (let j = 0; j < pi.ratioList.length; j++) {
                            if (i == j) {
                                let sii = {
                                    code: pi.sizeList[i].code,
                                    value: pi.ratioList[j].ratio
                                }
                                sizeRatio.push(sii)
                            }
                        }
                    }


                    let sizeMapping = this.state.sizeMapping
                    for (var i = 0; i < sizeMapping.length; i++) {
                        sizeMapping[i].ratio = ""
                    }
                    for (let i = 0; i < sizeMapping.length; i++) {
                        for (let j = 0; j < sizeRatio.length; j++) {
                            if (sizeMapping[i].code == sizeRatio[j].code) {
                                sizeMapping[i].ratio = sizeRatio[j].value
                            }
                        }

                    }

                    let sizeMappingrows = _.map(
                        _.uniq(
                            _.map(sizeMapping, function (obj) {

                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );

                    sizeMapping.forEach(size => {
                        sizeRatio.forEach(ratio => {
                            if (size.code == ratio.code) {
                                size.ratio = ratio.value
                            }
                        })


                    })
                    for (var j = 0; j < pi.ratioList.length; j++) {
                        if (isNaN(pi.ratioList[j].ratio)) {
                            continue;
                        }
                        sum += Number(pi.ratioList[j].ratio);
                    }
                    let ratio = []
                    pi.ratioList.forEach(ra => {
                        ratio.push(ra.ratio)
                    })

                    let secData = {
                        discount: {
                            discountType: pi.discountType == "null" ? "" : pi.discountType,
                            discountValue: pi.discountValue == "null" ? "" : pi.discountValue
                        },
                        finalRate: pi.discountType == "" || pi.discountType == "null" ? pi.rate : pi.finalRate,





                        colorChk: false,
                        icodeChk: false,
                        setHeaderId: "",
                        itemBarcode: "",
                        size: "",
                        setRatio: "",
                        designRowId: designSec++,
                        amount: pi.netAmountTotal,
                        vendorDesignNo: pi.design,
                        setQty: pi.noOfSets,
                        color: color.join(','),
                        colorList: pi.colorList,
                        option: color.length,
                        deliveryDate: pi.deliveryDate,
                        image: Object.keys(imageUrl),
                        imageUrl: imageUrl,
                        containsImage: pi.containsImage,
                        otb: pi.otb,
                        quantity: pi.quantity,
                        rate: pi.rate,
                        ratio: ratio,
                        mrk: pi.intakeMargin,
                        total: sum * color.length,
                        setNo: setNum++,
                        typeOfBuying: pi.typeOfBuying,
                        marginRule: pi.marginRule,

                        gst: pi.gst,
                        finCharges: pi.finCharge,
                        tax: pi.tax,
                        calculatedMargin: pi.calculatedMargin,
                        totalQuantity: pi.quantity,
                        totalNetAmt: pi.netAmountTotal,
                        totaltax: [],
                        totalCalculatedMargin: [],
                        totalgst: [],
                        totalIntakeMargin: [],
                        totalFinCharges: pi.finCharge,
                        totalBasic: pi.finalRate * pi.noOfSets * pi.total,
                        totalCalculatedOtb: "",
                        gridTwoId: secId++,
                        catOneCode: pi.cat1Code,
                        catOneName: pi.cat1Name,
                        catTwoCode: pi.cat2Code,
                        catTwoName: pi.cat2Name,
                        catThreeCode: pi.cat3Code,
                        catThreeName: pi.cat3Name,
                        catFourCode: pi.cat4Code,
                        catFourName: pi.cat4Name,
                        catFiveCode: pi.cat5Code,
                        catFiveName: pi.cat5Name,
                        catSixCode: pi.cat6Code,
                        catSixName: pi.cat6Name,
                        descOneCode: pi.desc1Code,
                        descOneName: pi.desc1Name,
                        descTwoCode: pi.desc2Code,
                        descTwoName: pi.desc2Name,
                        descThreeCode: pi.desc3Code,
                        descThreeName: pi.desc3Name,
                        descFourCode: pi.desc4Code,
                        descFourName: pi.desc4Name,
                        descFiveCode: pi.desc5Code,
                        descFiveName: pi.desc5Name,
                        descSixCode: pi.desc6Code,
                        descSixName: pi.desc6Name,
                        itemudf1: this.state.itemUdfExist == "true" ? pi.udf.itemudf1 : "",
                        itemudf2: this.state.itemUdfExist == "true" ? pi.udf.itemudf2 : "",
                        itemudf3: this.state.itemUdfExist == "true" ? pi.udf.itemudf3 : "",
                        itemudf4: this.state.itemUdfExist == "true" ? pi.udf.itemudf4 : "",
                        itemudf5: this.state.itemUdfExist == "true" ? pi.udf.itemudf5 : "",
                        itemudf6: this.state.itemUdfExist == "true" ? pi.udf.itemudf6 : "",
                        itemudf7: this.state.itemUdfExist == "true" ? pi.udf.itemudf7 : "",
                        itemudf8: this.state.itemUdfExist == "true" ? pi.udf.itemudf8 : "",
                        itemudf9: this.state.itemUdfExist == "true" ? pi.udf.itemudf9 : "",
                        itemudf10: this.state.itemUdfExist == "true" ? pi.udf.itemudf10 : "",
                        itemudf11: this.state.itemUdfExist == "true" ? pi.udf.itemudf11 : "",
                        itemudf12: this.state.itemUdfExist == "true" ? pi.udf.itemudf12 : "",
                        itemudf13: this.state.itemUdfExist == "true" ? pi.udf.itemudf13 : "",
                        itemudf14: this.state.itemUdfExist == "true" ? pi.udf.itemudf14 : "",
                        itemudf15: this.state.itemUdfExist == "true" ? pi.udf.itemudf15 : "",
                        itemudf16: this.state.itemUdfExist == "true" ? pi.udf.itemudf16 : "",
                        itemudf17: this.state.itemUdfExist == "true" ? pi.udf.itemudf17 : "",
                        itemudf18: this.state.itemUdfExist == "true" ? pi.udf.itemudf18 : "",
                        itemudf19: this.state.itemUdfExist == "true" ? pi.udf.itemudf19 : "",
                        itemudf20: this.state.itemUdfExist == "true" ? pi.udf.itemudf20 : "",
                        sizeListData: pi.sizeList,
                        sizeList: sizeMappingrows,
                        lineItemChk: false

                    }

                    secRows.push(secData)
                })//

                let itemUdfRows = _.map(
                    _.uniq(
                        _.map(iUdf, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                let udfRowss = _.map(
                    _.uniq(
                        _.map(udf, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                let secRowss = _.map(
                    _.uniq(
                        _.map(secRows, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                let catDescRowss = _.map(
                    _.uniq(
                        _.map(itemD, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );

                this.setState({
                    loadIndentTrue: false,
                    udfRows: udfRowss,
                    secTwoRows: secRowss,
                    catDescRows: catDescRowss,
                    itemUdf: itemUdfRows

                })

                setTimeout(() => {
                    this.gridSecond()
                    this.gridThird()
                    this.gridFourth()
                    this.gridFivth()
                }, 10)

            }, 10)

        }



    }
    updateSetBasedData(data) {

        if (!this.state.setBasedTrue) {
            let poRows = []
            let id = 1;


            let imageUrl = {};
            let designArray = []
            let piDetails = data.pol
            piDetails.forEach(pi => {
                if (!designArray.includes(pi.designWiseRowId)) {
                    designArray.push(pi.designWiseRowId)
                    let color = [];
                    let size = [];
                    let ratio = [];

                    pi.colors.forEach(co => {
                        color.push(co.cname)
                    });
                    pi.sizes.forEach(si => {
                        size.push(si.cname)
                    })
                    pi.images.forEach(img => {
                        imageUrl[img.split('/')[9].split('?')[0]] = img
                    })
                    pi.ratios.forEach(ra => {
                        ratio.push(ra.ratio)
                    })
                    let piData = {

                        color: color,
                        size: size,
                        ratio: ratio,
                        vendorMrp: pi.mrp,
                        vendorDesign: pi.design,
                        mrk: pi.designWiseTotalIntakeMargin,
                        discount: {
                            discountType: pi.discountType,
                            discountValue: pi.discountValue
                        },
                        rate: pi.rate,
                        finalRate: sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? pi.rate : pi.finalRate,
                        netRate: pi.rate,

                        rsp: pi.rsp,
                        mrp: pi.mrp,
                        quantity: pi.designWiseTotalQty,
                        amount: pi.designWiseNetAmountTotal,
                        basic: pi.designWiseTotalBasic,
                        remarks: pi.remarks == null || pi.remarks == "null" ? "" : pi.remarks,

                        otb: pi.designWiseTotalOtb,
                        gst: pi.designWiseTotalGST,
                        finCharges: pi.designWiseTotalFinCharges,
                        tax: pi.designWiseTotalTax,
                        calculatedMargin: pi.designWiseTotalCalculatedMargin,
                        gridOneId: pi.designWiseRowId,
                        deliveryDate: this.state.lastInDate,
                        typeOfBuying: pi.typeOfBuying,
                        marginRule: pi.marginRule,
                        image: Object.keys(imageUrl),

                        imageUrl: imageUrl,
                        containsImage: pi.containsImage,

                    }
                    poRows.push(piData)
                }
            })


            this.setState({

                stateCode: data.stateCode,
                hsnSacCode: data.hsnSacCode,
                hsnCode: data.hsnCode,
                division: data.hl1Name,
                section: data.hl2Name,
                department: data.hl3Name,
                divisionCode: data.hl1Code,
                sectionCode: data.hl2Code,
                departmentCode: data.hl3Code,
                slCode: data.slCode,
                slAddr: data.slAddr,
                slName: data.slName,
                city: data.slCityName,
                slCityName: data.slCityName,
                supplier: data.slName + "-" + data.slAddr,
                leadDays: data.leadTime,
                termCode: data.termCode,
                termName: data.termName,
                transporterCode: data.transporterCode,
                transporterName: data.transporterName,
                transporter: data.transporterName,
                term: data.termName,
                startRange: data.mrpStart,
                endRange: data.mrpEnd,
                vendorMrpPo: data.hl4Code,
                articleName: data.hl4Name,
                poRows: poRows,
                poQuantity: data.poQuantity,
                poAmount: data.poAmount,
                setBasedTrue: true,
                itemUdfExist: data.itemUdfExist,
                isUDFExist: data.isUDFExist,
                siteCode: data.siteCode,
                siteName: data.siteName,
                isSiteExist: data.isSiteExist,
                copyColor: data.copyColor




            })

            let poData = {
                deptName: data.hl3Name,
                deptCode: data.hl3Code,
                hl1Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.hl1Name : "",
                hl2Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.hl2Name : "",
                hl4Name: sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR" ? data.hl4Name : "",
            }
            // this.props.poSizeRequest(poData)
            let Payload = {
                hl3Code: data.hl3Code,
                hl3Name: data.hl3Name,
            }
            this.props.getItemDetailsRequest(Payload)
            if (data.itemUdfExist == "true") {


                let uData = {
                    hl3Code: data.hl3Code,
                    ispo: true,
                    hl3Name: data.hl3Name

                }
                this.props.getCatDescUdfRequest(uData)
            }
            if (data.isUDFExist == "true") {
                let udfdata = {

                    type: 1,
                    search: "",
                    isCompulsary: "",
                    displayName: "",
                    udfType: "",
                    ispo: true
                }
                this.props.getUdfMappingRequest(udfdata);
            }

            const t = this
            setTimeout(function () {
                let poRows = t.state.poRows

                t.setState({
                    totalOtb: poRows[0].otb
                })


                t.gridFirst();
            }, 10)

        } else if (this.state.setBasedTrue) {


            setTimeout(() => {
                let imageUrl = {};
                let itemUdfRows = [];
                let udfId = 1;
                let secId = 1;
                let itemId = 1;
                let catDescId = 1;
                let setNo = 1
                let setNum = 1
                let udfMappingData = this.state.udfMappingData
                let udfRowMapping = this.state.udfRowMapping
                let itemDetailsHeader = this.state.itemDetailsHeader

                let udf = []
                let secRows = []
                let itemD = []

                let catDescRows = this.state.catDescRows
                let piDetails = data.pol
                let iUdf = []
                let designUdf = []
                if (data.itemUdfExist == "true") {
                    piDetails.forEach(pi => {
                        if (!designUdf.includes(pi.designWiseRowId)) {

                            udfRowMapping.forEach(mapData => {
                                mapData.value = ""
                            })

                            udfRowMapping.forEach(list => {
                                if (list.cat_desc_udf == "UDFSTRING01") {
                                    list.value = pi.udf.itemudf1 == undefined ? "" : pi.udf.itemudf1
                                } else if (list.cat_desc_udf == "UDFSTRING02") {
                                    list.value = pi.udf.itemudf2 == undefined ? "" : pi.udf.itemudf2
                                } else if (list.cat_desc_udf == "UDFSTRING03") {
                                    list.value = pi.udf.itemudf3 == undefined ? "" : pi.udf.itemudf3
                                }
                                else if (list.cat_desc_udf == "UDFSTRING04") {
                                    list.value = pi.udf.itemudf4 == undefined ? "" : pi.udf.itemudf4
                                }
                                else if (list.cat_desc_udf == "UDFSTRING05") {
                                    list.value = pi.udf.itemudf5 == undefined ? "" : pi.udf.itemudf5
                                }
                                else if (list.cat_desc_udf == "UDFSTRING06") {
                                    list.value = pi.udf.itemudf6 == undefined ? "" : pi.udf.itemudf6
                                }
                                else if (list.cat_desc_udf == "UDFSTRING07") {
                                    list.value = pi.udf.itemudf7 == undefined ? "" : pi.udf.itemudf7
                                }
                                else if (list.cat_desc_udf == "UDFSTRING08") {
                                    list.value = pi.udf.itemudf8 == undefined ? "" : pi.udf.itemudf8
                                }
                                else if (list.cat_desc_udf == "UDFSTRING09") {
                                    list.value = pi.udf.itemudf9 == undefined ? "" : pi.udf.itemudf9

                                }
                                else if (list.cat_desc_udf == "UDFSTRING10") {
                                    list.value = pi.udf.itemudf10 == undefined ? "" : pi.udf.itemudf10
                                } else if (list.cat_desc_udf == "UDFNUM01") {
                                    list.value = pi.udf.itemudf11 == undefined ? "" : pi.udf.itemudf11

                                } else if (list.cat_desc_udf == "UDFNUM02") {
                                    list.value = pi.udf.itemudf12 == undefined ? "" : pi.udf.itemudf12
                                } else if (list.cat_desc_udf == "UDFNUM03") {
                                    list.value = pi.udf.itemudf13 == undefined ? "" : pi.udf.itemudf13
                                }
                                else if (list.cat_desc_udf == "UDFNUM04") {
                                    list.value = pi.udf.itemudf14 == undefined ? "" : pi.udf.itemudf14
                                }
                                else if (list.cat_desc_udf == "UDFNUM05") {
                                    list.value = pi.udf.itemudf15 == undefined ? "" : pi.udf.itemudf15
                                }
                                else if (list.cat_desc_udf == "UDFDATE01") {
                                    list.value = pi.udf.itemudf16 == null || pi.udf.itemudf16 == undefined ? "" : pi.udf.itemudf16
                                }
                                else if (list.cat_desc_udf == "UDFDATE02") {
                                    list.value = pi.udf.itemudf17 == null || pi.udf.itemudf17 == undefined ? "" : pi.udf.itemudf17
                                }
                                else if (list.cat_desc_udf == "UDFDATE03") {
                                    list.value = pi.udf.itemudf18 == null || pi.udf.itemudf18 == undefined ? "" : pi.udf.itemudf18
                                }
                                else if (list.cat_desc_udf == "UDFDATE04") {
                                    list.value = pi.udf.itemudf19 == null || pi.udf.itemudf19 == undefined ? "" : pi.udf.itemudf19
                                }
                                else if (list.cat_desc_udf == "UDFDATE05") {
                                    list.value = pi.udf.itemudf20 == null || pi.udf.itemudf20 == undefined ? "" : pi.udf.itemudf20
                                }
                            })


                            let udfRowMappings = _.map(
                                _.uniq(
                                    _.map(udfRowMapping, function (obj) {

                                        return JSON.stringify(obj);
                                    })
                                ), function (obj) {
                                    return JSON.parse(obj);
                                }
                            );


                            let data = {
                                designRowId: pi.designWiseRowId,
                                itemUdfGridId: pi.designWiseRowId,
                                vendorDesign: pi.design,
                                udfList: udfRowMappings

                            }
                            iUdf.push(data)

                            itemUdfRows = _.map(
                                _.uniq(
                                    _.map(iUdf, function (obj) {

                                        return JSON.stringify(obj);
                                    })
                                ), function (obj) {
                                    return JSON.parse(obj);
                                }
                            );
                        }
                    })
                }
                let itemDesign = []

                piDetails.forEach(pi => {
                    if (!itemDesign.includes(pi.designWiseRowId)) {
                        itemDetailsHeader.forEach(mapData => {
                            mapData.value = ""
                            mapData.code = ""
                        })
                        itemDetailsHeader.forEach(list => {
                            // catDesc.vendorDesign= pi.design
                            // catDesc.gridId = catDescId++
                            // if(catDesc.itemDetails!=undefined){
                            // catDesc.itemDetails.forEach(list=>{
                            if (list.catdesc == "CAT1") {
                                list.value = pi.cat1Name
                                list.code = pi.cat1Code

                            } else if (list.catdesc == "CAT2") {
                                list.code = pi.cat2Code
                                list.value = pi.cat2Name
                            } else if (list.catdesc == "CAT3") {
                                list.code = pi.cat3Code
                                list.value = pi.cat3Name
                            }
                            else if (list.catdesc == "CAT4") {
                                list.code = pi.cat4Code
                                list.value = pi.cat4Name
                            }
                            else if (list.catdesc == "CAT5") {
                                list.code = pi.cat5Code
                                list.value = pi.cat5Name
                            }
                            else if (list.catdesc == "CAT6") {
                                list.code = pi.cat6Code
                                list.value = pi.cat6Name
                            }
                            else if (list.catdesc == "DESC1") {
                                list.code = pi.desc1Code
                                list.value = pi.desc1Name
                            }
                            else if (list.catdesc == "DESC2") {
                                list.code = pi.desc2Code
                                list.value = pi.desc2Name
                            }
                            else if (list.catdesc == "DESC3") {
                                list.code = pi.desc3Code
                                list.value = pi.desc3Name
                            }
                            else if (list.catdesc == "DESC4") {
                                list.code = pi.desc4Code
                                list.value = pi.desc4Name
                            } else if (list.catdesc == "DESC5") {
                                list.code = pi.desc5Code
                                list.value = pi.desc5Name

                            } else if (list.catdesc == "DESC6") {
                                list.code = pi.desc6Code
                                list.value = pi.desc6Name

                            }


                        })

                        let itemDetailsHeaders = _.map(
                            _.uniq(
                                _.map(itemDetailsHeader, function (obj) {

                                    return JSON.stringify(obj);
                                })
                            ), function (obj) {
                                return JSON.parse(obj);
                            }
                        );

                        let payload = {
                            designRowId: pi.designWiseRowId,
                            vendorDesign: pi.design,
                            gridId: pi.designWiseRowId,
                            itemDetails: itemDetailsHeaders
                        }
                        itemD.push(payload)

                    }
                })
                if (data.isUDFExist == "true") {



                    piDetails.forEach(pi => {

                        let color = [];
                        let size = [];
                        pi.colors.forEach(co => {
                            color.push(co.cname)
                        });
                        pi.sizes.forEach(si => {
                            size.push(si.cname)
                        })
                        udfMappingData.forEach(mapData => {
                            mapData.value = ""
                        })

                        udfMappingData.forEach(list => {

                            if (list.udfType == "SMUDFSTRING01") {
                                list.value = pi.udf.udf1

                            } else if (list.udfType == "SMUDFSTRING02") {
                                list.value = pi.udf.udf2
                            } else if (list.udfType == "SMUDFSTRING03") {
                                list.value = pi.udf.udf3
                            }
                            else if (list.udfType == "SMUDFSTRING04") {
                                list.value = pi.udf.udf4
                            }
                            else if (list.udfType == "SMUDFSTRING05") {
                                list.value = pi.udf.udf5
                            }
                            else if (list.udfType == "SMUDFSTRING06") {
                                list.value = pi.udf.udf6
                            }
                            else if (list.udfType == "SMUDFSTRING07") {
                                list.value = pi.udf.udf7
                            }
                            else if (list.udfType == "SMUDFSTRING08") {
                                list.value = pi.udf.udf8
                            }
                            else if (list.udfType == "SMUDFSTRING09") {
                                list.value = pi.udf.udf9
                            }
                            else if (list.udfType == "SMUDFSTRING10") {
                                list.value = pi.udf.udf10
                            } else if (list.udfType == "SMUDFNUM01") {
                                list.value = pi.udf.udf11

                            } else if (list.udfType == "SMUDFNUM02") {
                                list.value = pi.udf.udf12
                            } else if (list.udfType == "SMUDFNUM03") {
                                list.value = pi.udf.udf13
                            }
                            else if (list.udfType == "SMUDFNUM04") {
                                list.value = pi.udf.udf14
                            }
                            else if (list.udfType == "SMUDFNUM05") {
                                list.value = pi.udf.udf15
                            }
                            else if (list.udfType == "SMUDFDATE01") {
                                list.value = pi.udf.udf16 == null ? "" : pi.udf.udf16
                            }
                            else if (list.udfType == "SMUDFDATE02") {
                                list.value = pi.udf.udf17 == null ? "" : pi.udf.udf17
                            }
                            else if (list.udfType == "SMUDFDATE03") {
                                list.value = pi.udf.udf18 == null ? "" : pi.udf.udf18
                            }
                            else if (list.udfType == "SMUDFDATE04") {
                                list.value = pi.udf.udf19 == null ? "" : pi.udf.udf19
                            }
                            else if (list.udfType == "SMUDFDATE05") {
                                list.value = pi.udf.udf20 == null ? "" : pi.udf.udf20
                            }
                        })
                        let udfMappingDatas = _.map(
                            _.uniq(
                                _.map(udfMappingData, function (obj) {

                                    return JSON.stringify(obj);
                                })
                            ), function (obj) {
                                return JSON.parse(obj);
                            }
                        );
                        let data = {
                            designRowId: pi.designWiseRowId,
                            udfColor: color,
                            udfGridId: udfId++,
                            setUdfNo: setNo++,
                            setUdfList: udfMappingDatas

                        }
                        udf.push(data)

                        // }
                    })
                }
                // })
                let designSecArray = []

                piDetails.forEach(pi => {

                    let color = [];
                    let size = [];

                    let sizeRatio = []
                    let sum = 0

                    pi.colors.forEach(co => {
                        color.push(co.cname)
                    });
                    pi.sizes.forEach(si => {
                        size.push(si.cname)
                    })
                    pi.images.forEach(img => {
                        imageUrl[img.split('/')[9].split('?')[0]] = img
                    })

                    // pi.sizes.forEach(si => {
                    //     pi.ratios.forEach(ra => {
                    //         let sii = {
                    //             cname: si.cname,
                    //             value: ra.ratio
                    //         }
                    //         sizeRatio.push(sii)
                    //     })

                    // })

                    for (let i = 0; i < pi.sizes.length; i++) {
                        for (let j = 0; j < pi.ratios.length; j++) {
                            if (i == j) {
                                let sii = {
                                    code: pi.sizes[i].code,
                                    value: pi.ratios[j].ratio
                                }
                                sizeRatio.push(sii)
                            }
                        }
                    }


                    let sizeMapping = this.state.sizeMapping
                    for (var i = 0; i < sizeMapping.length; i++) {
                        sizeMapping[i].ratio = ""
                    }


                    sizeMapping.forEach(size => {
                        sizeRatio.forEach(ratio => {
                            if (size.code == ratio.code) {
                                size.ratio = ratio.value
                            }
                        })


                    })
                    let sizeMappingrows = _.map(
                        _.uniq(
                            _.map(sizeMapping, function (obj) {

                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    // for (var j = 0; j < pi.ratios.length; j++) {
                    //     if (isNaN(pi.ratios[j].ratio)) {
                    //         continue;
                    //     }
                    //     sum += Number(pi.ratios[j].ratio);
                    // }
                    let ratio = []
                    pi.ratios.forEach(ra => {
                        ratio.push(ra.ratio)
                    })

                    let secData = {
                        discount: {
                            discountType: pi.discountType == "null" ? "" : pi.discountType,
                            discountValue: pi.discountValue == "null" ? "" : pi.discountValue
                        },
                        finalRate: pi.discountType == "" || pi.discountType == "null" || pi.discountType == null ? pi.rate : pi.finalRate,
                        colorChk: false,
                        icodeChk: false,
                        setHeaderId: pi.setHeaderId,
                        designRowId: pi.designWiseRowId,
                        amount: pi.netAmountTotal,
                        itemBarcode: "",
                        size: "",
                        setRatio: "",
                        vendorDesignNo: pi.design,
                        setQty: pi.noOfSets,
                        color: color.join(','),
                        colorList: pi.colors,
                        option: color.length,
                        deliveryDate: pi.deliveryDate,
                        image: Object.keys(imageUrl),
                        imageUrl: imageUrl,
                        containsImage: pi.containsImage,
                        otb: pi.otb,
                        quantity: pi.qty,
                        rate: pi.rate,
                        ratio: ratio,
                        mrk: pi.intakeMargin,
                        total: pi.sumOfRatio,
                        setNo: setNum++,
                        typeOfBuying: pi.typeOfBuying,
                        marginRule: pi.marginRule,

                        gst: pi.gst,
                        finCharges: pi.finCharge,
                        tax: pi.tax,
                        calculatedMargin: pi.calculatedMargin,
                        totalQuantity: pi.designWiseTotalQty,
                        totalNetAmt: pi.designWiseNetAmountTotal,
                        totaltax: pi.designWiseTotalTax,
                        totalCalculatedMargin: pi.designWiseTotalCalculatedMargin,
                        totalgst: pi.designWiseTotalGST,
                        totalIntakeMargin: pi.designWiseTotalIntakeMargin,
                        totalFinCharges: pi.designWiseTotalFinCharges,
                        totalBasic: pi.designWiseTotalBasic,
                        totalCalculatedOtb: pi.designWiseTotalOtb,
                        gridTwoId: secId++,
                        catOneCode: pi.cat1Code,
                        catOneName: pi.cat1Name,
                        catTwoCode: pi.cat2Code,
                        catTwoName: pi.cat2Name,
                        catThreeCode: pi.cat3Code,
                        catThreeName: pi.cat3Name,
                        catFourCode: pi.cat4Code,
                        catFourName: pi.cat4Name,
                        catFiveCode: pi.cat5Code,
                        catFiveName: pi.cat5Name,
                        catSixCode: pi.cat6Code,
                        catSixName: pi.cat6Name,
                        descOneCode: pi.desc1Code,
                        descOneName: pi.desc1Name,
                        descTwoCode: pi.desc2Code,
                        descTwoName: pi.desc2Name,
                        descThreeCode: pi.desc3Code,
                        descThreeName: pi.desc3Name,
                        descFourCode: pi.desc4Code,
                        descFourName: pi.desc4Name,
                        descFiveCode: pi.desc5Code,
                        descFiveName: pi.desc5Name,
                        descSixCode: pi.desc6Code,
                        descSixName: pi.desc6Name,
                        itemudf1: this.state.itemUdfExist == "true" ? pi.udf.itemudf1 : "",
                        itemudf2: this.state.itemUdfExist == "true" ? pi.udf.itemudf2 : "",
                        itemudf3: this.state.itemUdfExist == "true" ? pi.udf.itemudf3 : "",
                        itemudf4: this.state.itemUdfExist == "true" ? pi.udf.itemudf4 : "",
                        itemudf5: this.state.itemUdfExist == "true" ? pi.udf.itemudf5 : "",
                        itemudf6: this.state.itemUdfExist == "true" ? pi.udf.itemudf6 : "",
                        itemudf7: this.state.itemUdfExist == "true" ? pi.udf.itemudf7 : "",
                        itemudf8: this.state.itemUdfExist == "true" ? pi.udf.itemudf8 : "",
                        itemudf9: this.state.itemUdfExist == "true" ? pi.udf.itemudf9 : "",
                        itemudf10: this.state.itemUdfExist == "true" ? pi.udf.itemudf10 : "",
                        itemudf11: this.state.itemUdfExist == "true" ? pi.udf.itemudf11 : "",
                        itemudf12: this.state.itemUdfExist == "true" ? pi.udf.itemudf12 : "",
                        itemudf13: this.state.itemUdfExist == "true" ? pi.udf.itemudf13 : "",
                        itemudf14: this.state.itemUdfExist == "true" ? pi.udf.itemudf14 : "",
                        itemudf15: this.state.itemUdfExist == "true" ? pi.udf.itemudf15 : "",
                        itemudf16: this.state.itemUdfExist == "true" ? pi.udf.itemudf16 : "",
                        itemudf17: this.state.itemUdfExist == "true" ? pi.udf.itemudf17 : "",
                        itemudf18: this.state.itemUdfExist == "true" ? pi.udf.itemudf18 : "",
                        itemudf19: this.state.itemUdfExist == "true" ? pi.udf.itemudf19 : "",
                        itemudf20: this.state.itemUdfExist == "true" ? pi.udf.itemudf20 : "",
                        sizeListData: pi.sizes,
                        sizeList: sizeMappingrows,
                        lineItemChk: false

                    }
                    secRows.push(secData)

                })//



                let udfRowss = _.map(
                    _.uniq(
                        _.map(udf, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                let secRowss = _.map(
                    _.uniq(
                        _.map(secRows, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );
                let catDescRowss = _.map(
                    _.uniq(
                        _.map(itemD, function (obj) {

                            return JSON.stringify(obj);
                        })
                    ), function (obj) {
                        return JSON.parse(obj);
                    }
                );

                this.setState({
                    setBasedTrue: false,
                    udfRows: udfRowss,
                    secTwoRows: secRowss,
                    catDescRows: catDescRowss,
                    itemUdf: itemUdfRows

                })

                setTimeout(() => {
                    this.gridSecond()
                    this.gridThird()
                    this.gridFourth()
                    this.gridFivth()
                }, 10)

            }, 10)

        }
    }





    mouseOverCmprRate(dataRate) {
        let ellRate = document.getElementById(`toolIdRate${dataRate}`)
        var viewportOffset = ellRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - 55;

        if (!document.getElementById(`moveLeftRate${dataRate}`)) {
        } else {
            document.getElementById(`moveLeftRate${dataRate}`).style.left = left;
            document.getElementById(`moveLeftRate${dataRate}`).style.top = top;
        }


    }
    mouseOverCmprMrp(dataRate) {
        let ellRate = document.getElementById(`toolIdMrp${dataRate}`)
        var viewportOffset = ellRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - 55;

        if (!document.getElementById(`moveLeftMrp${dataRate}`)) {
        } else {
            document.getElementById(`moveLeftMrp${dataRate}`).style.left = left;
            document.getElementById(`moveLeftMrp${dataRate}`).style.top = top;
        }


    }


    otbNegative() {
        let flag = false
        let datevalue = false
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].amount > poRows[i].otb) {
                flag = true
                break
            }
        }
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].deliveryDate != "") {
                if (this.state.minDate > poRows[i].deliveryDate || poRows[i].deliveryDate < this.state.maxDate) {
                    datevalue = true
                    break
                }
            }
        }
        // if(datevalue){
        //     this.setState({

        //         errorMassage: "Delivery date must be in between PO Valid From and Last In Date",
        //         poErrorMsg: true
        //     })
        // }else{
        if (this.state.isOtbValidation == "true") {
            if (flag) {
                this.setState({
                    loader: false
                })
                this.setState({
                    otbStatus: true,
                    errorMassage: "Otb is less than Net Amount",
                    poErrorMsg: true
                })

            } else {

                this.setState({
                    otbStatus: false
                })

                //   }
            }
        } else {


            this.setState({
                otbStatus: false
            })
        }
    }
    lineItemChkFun = () => {

        let secTwoRows = this.state.secTwoRows
        let lineError = []
        for (let i = 0; i < secTwoRows.length; i++) {

            if (secTwoRows[i].lineItemChk == true) {

                let payload = {
                    "sNo": secTwoRows[i].gridTwoId,
                    "message": "LineItem charges is wrongly calculated .Please Rentered the value"

                }
                lineError.push(payload)
            }
        }
        return lineError
    }

    saveDraftCall() {

        let t = this;
        let itemData = t.state.secTwoRows;
        let udfRows = t.state.udfRows;
        let lineItem = []
        t.setTimeout(() => {
            if (t.state.isUDFExist == "true" || t.state.itemUdfExist == "true") {
                itemData.forEach(element => {
                    if (t.state.isUDFExist == "true") {

                        udfRows.forEach(article => {
                            if (article.setUdfNo == element.setNo) {
                                udf1 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING01") {
                                        return item
                                    }
                                })
                                udf2 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING02") {
                                        return item
                                    }
                                })
                                udf3 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING03") {
                                        return item
                                    }
                                })
                                udf4 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING04") {
                                        return item
                                    }
                                })
                                udf5 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING05") {
                                        return item
                                    }
                                })
                                udf6 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING06") {
                                        return item
                                    }
                                })
                                udf7 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING07") {
                                        return item
                                    }
                                })
                                udf8 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING08") {
                                        return item
                                    }
                                })
                                udf9 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING09") {
                                        return item
                                    }
                                })
                                udf10 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFSTRING10") {
                                        return item
                                    }
                                })
                                udf11 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFNUM01") {
                                        return item
                                    }
                                })
                                udf12 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFNUM02") {
                                        return item
                                    }
                                })
                                udf13 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFNUM03") {
                                        return item
                                    }
                                })
                                udf14 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFNUM04") {
                                        return item
                                    }
                                })
                                udf15 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFNUM05") {
                                        return item
                                    }
                                })
                                udf16 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFDATE01") {
                                        return item
                                    }
                                })
                                udf17 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFDATE02") {
                                        return item
                                    }
                                })
                                udf18 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFDATE03") {
                                        return item
                                    }
                                })
                                udf19 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFDATE04") {
                                        return item
                                    }
                                })
                                udf20 = article.setUdfList.find(item => {
                                    if (item.udfType == "SMUDFDATE05") {
                                        return item
                                    }
                                })
                                let ratio = []
                                let ratioIndex = 1

                                let sr = element.ratio
                                for (var k = 0; k < sr.length; k++) {

                                    let ra = {
                                        id: ratioIndex++,
                                        ratio: sr[k]
                                    }
                                    ratio.push(ra)
                                }

                                var finalDataList = {};
                                let sizeListDataa = []
                                let elementSize = element.sizeListData
                                for (let l = 0; l < elementSize.length; l++) {
                                    let payload = {
                                        id: elementSize[l].id,
                                        code: elementSize[l].code,
                                        cname: elementSize[l].cname
                                    }
                                    sizeListDataa.push(payload)
                                }


                                finalDataList = {

                                    setHeaderId: t.state.codeRadio === "setBased" ? element.setHeaderId : "",
                                    rate: element.rate,
                                    finalRate: element.finalRate,
                                    discountType: element.discount.discountType,
                                    discountValue: element.discount.discountValue,
                                    itemId: element.itemBarcode,
                                    sizes: sizeListDataa,
                                    ratios: ratio,
                                    colors: element.colorList,
                                    qty: element.quantity,
                                    noOfSets: element.setQty,
                                    rsp: t.state.isRspRequired ? element.rsp : 0,
                                    mrp: t.state.isMrpRequired ? element.mrp : 0,
                                    images: Object.values(element.imageUrl),
                                    typeOfBuying: element.typeOfBuying,
                                    marginRule: element.marginRule,
                                    totalAmount: element.amount,
                                    netAmountTotal: element.amount,
                                    otb: element.otb,
                                    gst: element.gst,
                                    sumOfRatio: element.total,
                                    design: element.vendorDesignNo,
                                    finCharge: element.finCharges,
                                    remarks: element.remarks,
                                    tax: element.tax,

                                    deliveryDate: moment(element.deliveryDate).format(),
                                    containsImage: element.containsImage,
                                    calculatedMargin: this.state.calculatedMarginExact,
                                    intakeMargin: element.mrk,
                                    cat1Code: element.catOneCode == null ? "" : element.catOneCode,
                                    cat2Code: element.catTwoCode == null ? "" : element.catTwoCode,
                                    cat3Code: element.catThreeCode == null ? "" : element.catThreeCode,
                                    cat4Code: element.catFourCode == null ? "" : element.catFourCode,
                                    cat5Code: element.catFiveCode == null ? "" : element.catFiveCode,
                                    cat6Code: element.catSixCode == null ? "" : element.catSixCode,
                                    cat1Name: element.catOneName,
                                    cat2Name: element.catTwoName,
                                    cat3Name: element.catThreeName,
                                    cat4Name: element.catFourName,
                                    cat5Name: element.catFiveName,
                                    cat6Name: element.catSixName,
                                    desc1Code: element.descOneCode == null ? "" : element.descOneCode,
                                    desc2Code: element.descTwoCode == null ? "" : element.descTwoCode,
                                    desc3Code: element.descThreeCode == null ? "" : element.descThreeCode,
                                    desc4Code: element.descFourCode == null ? "" : element.descFourCode,
                                    desc5Code: element.descFiveCode == null ? "" : element.descFiveCode,
                                    desc6Code: element.descSixCode == null ? "" : element.descSixCode,
                                    desc1Name: element.descOneName,
                                    desc2Name: element.descTwoName,
                                    desc3Name: element.descThreeName,
                                    desc4Name: element.descFourName,
                                    desc5Name: element.descFiveName,
                                    desc6Name: element.descSixName,
                                    udf: {
                                        itemudf1: element.itemudf1,
                                        itemudf2: element.itemudf2,
                                        itemudf3: element.itemudf3,
                                        itemudf4: element.itemudf4,
                                        itemudf5: element.itemudf5,
                                        itemudf6: element.itemudf6,
                                        itemudf7: element.itemudf7,
                                        itemudf8: element.itemudf8,
                                        itemudf9: element.itemudf9,
                                        itemudf10: element.itemudf10,
                                        itemudf11: element.itemudf11,
                                        itemudf12: element.itemudf12,
                                        itemudf13: element.itemudf13,
                                        itemudf14: element.itemudf14,
                                        itemudf15: element.itemudf15,
                                        itemudf16: element.itemudf16,
                                        itemudf17: element.itemudf17,
                                        itemudf18: element.itemudf18,
                                        itemudf19: element.itemudf19,
                                        itemudf20: element.itemudf20,
                                        udf1: udf1 != undefined ? udf1.value : "",
                                        udf2: udf2 != undefined ? udf2.value : "",
                                        udf3: udf3 != undefined ? udf3.value : "",
                                        udf4: udf4 != undefined ? udf4.value : "",
                                        udf5: udf5 != undefined ? udf5.value : "",
                                        udf6: udf6 != undefined ? udf6.value : "",
                                        udf7: udf7 != undefined ? udf7.value : "",
                                        udf8: udf8 != undefined ? udf8.value : "",
                                        udf9: udf9 != undefined ? udf9.value : "",
                                        udf10: udf10 != undefined ? udf10.value : "",
                                        udf11: udf11 != undefined ? udf11.value : "",
                                        udf12: udf12 != undefined ? udf12.value : "",
                                        udf13: udf13 != undefined ? udf13.value : "",
                                        udf14: udf14 != undefined ? udf14.value : "",
                                        udf15: udf15 != undefined ? udf15.value : "",
                                        udf16: udf16 != undefined ? udf16.value : "",
                                        udf17: udf17 != undefined ? udf17.value : "",
                                        udf18: udf18 != undefined ? udf18.value : "",
                                        udf19: udf19 != undefined ? udf19.value : "",
                                        udf20: udf20 != undefined ? udf20.value : ""
                                    },
                                    designWiseTotalQty: element.totalQuantity,
                                    designWiseNetAmountTotal: element.totalAmount,
                                    designWiseTotalTax: element.totaltax,
                                    designWiseTotalCalculatedMargin: element.totalCalculatedMargin,
                                    designWiseTotalGST: element.totalgst,
                                    designWiseTotalIntakeMargin: element.totalIntakeMargin,
                                    designWiseRowId: element.designRowId,
                                    designWiseTotalFinCharges: element.totalFinCharges,
                                    designWiseTotalBasic: element.totalBasic,
                                    designWiseTotalOtb: element.totalCalculatedOtb




                                }
                                lineItem.push(finalDataList);
                            }

                        });
                    } else {

                        let ratio = []
                        let ratioIndex = 1

                        let sr = element.ratio
                        for (var k = 0; k < sr.length; k++) {

                            let ra = {
                                id: ratioIndex++,
                                ratio: sr[k]
                            }
                            ratio.push(ra)
                        }

                        var finalDataList = {};
                        let sizeListDataa = []
                        let elementSize = element.sizeListData
                        for (let l = 0; l < elementSize.length; l++) {
                            let payload = {
                                id: elementSize[l].id,
                                code: elementSize[l].code,
                                cname: elementSize[l].cname
                            }
                            sizeListDataa.push(payload)
                        }


                        finalDataList = {

                            setHeaderId: t.state.codeRadio === "setBased" ? element.setHeaderId : "",
                            rate: element.rate,
                            finalRate: element.finalRate,
                            discountType: element.discount.discountType,
                            discountValue: element.discount.discountValue,
                            itemId: element.itemBarcode,
                            sizes: sizeListDataa,
                            ratios: ratio,
                            colors: element.colorList,
                            qty: element.quantity,
                            noOfSets: element.setQty,
                            rsp: t.state.isRspRequired ? element.rsp : 0,
                            mrp: t.state.isMrpRequired ? element.mrp : 0,
                            images: Object.values(element.imageUrl),
                            typeOfBuying: element.typeOfBuying,
                            marginRule: element.marginRule,
                            totalAmount: element.amount,
                            netAmountTotal: element.amount,
                            otb: element.otb,
                            gst: element.gst,
                            sumOfRatio: element.total,
                            design: element.vendorDesignNo,
                            finCharge: element.finCharges,
                            remarks: element.remarks,
                            tax: element.tax,

                            deliveryDate: moment(element.deliveryDate).format(),
                            containsImage: element.containsImage,
                            calculatedMargin: this.state.calculatedMarginExact,
                            intakeMargin: element.mrk,
                            cat1Code: element.catOneCode == null ? "" : element.catOneCode,
                            cat2Code: element.catTwoCode == null ? "" : element.catTwoCode,
                            cat3Code: element.catThreeCode == null ? "" : element.catThreeCode,
                            cat4Code: element.catFourCode == null ? "" : element.catFourCode,
                            cat5Code: element.catFiveCode == null ? "" : element.catFiveCode,
                            cat6Code: element.catSixCode == null ? "" : element.catSixCode,
                            cat1Name: element.catOneName,
                            cat2Name: element.catTwoName,
                            cat3Name: element.catThreeName,
                            cat4Name: element.catFourName,
                            cat5Name: element.catFiveName,
                            cat6Name: element.catSixName,
                            desc1Code: element.descOneCode == null ? "" : element.descOneCode,
                            desc2Code: element.descTwoCode == null ? "" : element.descTwoCode,
                            desc3Code: element.descThreeCode == null ? "" : element.descThreeCode,
                            desc4Code: element.descFourCode == null ? "" : element.descFourCode,
                            desc5Code: element.descFiveCode == null ? "" : element.descFiveCode,
                            desc6Code: element.descSixCode == null ? "" : element.descSixCode,
                            desc1Name: element.descOneName,
                            desc2Name: element.descTwoName,
                            desc3Name: element.descThreeName,
                            desc4Name: element.descFourName,
                            desc5Name: element.descFiveName,
                            desc6Name: element.descSixName,
                            udf: {
                                itemudf1: element.itemudf1,
                                itemudf2: element.itemudf2,
                                itemudf3: element.itemudf3,
                                itemudf4: element.itemudf4,
                                itemudf5: element.itemudf5,
                                itemudf6: element.itemudf6,
                                itemudf7: element.itemudf7,
                                itemudf8: element.itemudf8,
                                itemudf9: element.itemudf9,
                                itemudf10: element.itemudf10,
                                itemudf11: element.itemudf11,
                                itemudf12: element.itemudf12,
                                itemudf13: element.itemudf13,
                                itemudf14: element.itemudf14,
                                itemudf15: element.itemudf15,
                                itemudf16: element.itemudf16,
                                itemudf17: element.itemudf17,
                                itemudf18: element.itemudf18,
                                itemudf19: element.itemudf19,
                                itemudf20: element.itemudf20,
                                udf1: "",
                                udf2: "",
                                udf3: "",
                                udf4: "",
                                udf5: "",
                                udf6: "",
                                udf7: "",
                                udf8: "",
                                udf9: "",
                                udf10: "",
                                udf11: "",
                                udf12: "",
                                udf13: "",
                                udf14: "",
                                udf15: "",
                                udf16: "",
                                udf17: "",
                                udf18: "",
                                udf19: "",
                                udf20: ""
                            },
                            designWiseTotalQty: element.totalQuantity,
                            designWiseNetAmountTotal: element.totalAmount,
                            designWiseTotalTax: element.totaltax,
                            designWiseTotalCalculatedMargin: element.totalCalculatedMargin,
                            designWiseTotalGST: element.totalgst,
                            designWiseTotalIntakeMargin: element.totalIntakeMargin,
                            designWiseRowId: element.designRowId,
                            designWiseTotalFinCharges: element.totalFinCharges,
                            designWiseTotalBasic: element.totalBasic,
                            designWiseTotalOtb: element.totalCalculatedOtb




                        }
                        lineItem.push(finalDataList);

                    }

                });

            } else {
                itemData.forEach(element => {
                    let ratio = []
                    let ratioIndex = 1

                    let sr = element.ratio
                    for (var k = 0; k < sr.length; k++) {

                        let ra = {
                            id: ratioIndex++,
                            ratio: sr[k]
                        }
                        ratio.push(ra)
                    }

                    var finalDataList = {};
                    let sizeListDataa = []
                    let elementSize = element.sizeListData
                    for (let l = 0; l < elementSize.length; l++) {
                        let payload = {
                            id: elementSize[l].id,
                            code: elementSize[l].code,
                            cname: elementSize[l].cname
                        }
                        sizeListDataa.push(payload)
                    }

                    finalDataList = {

                        setHeaderId: t.state.codeRadio === "setBased" ? element.setHeaderId : "",
                        rate: element.rate,
                        finalRate: element.finalRate,
                        discountType: element.discount.discountType,
                        discountValue: element.discount.discountValue,
                        itemId: element.itemBarcode,
                        sizes: sizeListDataa,
                        ratios: ratio,
                        colors: element.colorList,
                        qty: element.quantity,
                        noOfSets: element.setQty,
                        rsp: t.state.isRspRequired ? element.rsp : 0,
                        mrp: t.state.isMrpRequired ? element.mrp : 0,
                        images: Object.values(element.imageUrl),
                        typeOfBuying: element.typeOfBuying,
                        marginRule: element.marginRule,
                        totalAmount: element.amount,
                        netAmountTotal: element.amount,
                        otb: element.otb,
                        gst: element.gst,
                        sumOfRatio: element.total,
                        design: element.vendorDesignNo,
                        finCharge: element.finCharges,
                        remarks: element.remarks,
                        tax: element.tax,

                        deliveryDate: moment(element.deliveryDate).format(),
                        containsImage: element.containsImage,
                        calculatedMargin: this.state.calculatedMarginExact,
                        intakeMargin: element.mrk,
                        cat1Code: element.catOneCode,
                        cat2Code: element.catTwoCode,
                        cat3Code: element.catThreeCode,
                        cat4Code: element.catFourCode,
                        cat5Code: element.catFiveCode,
                        cat6Code: element.catSixCode,

                        cat1Name: element.catOneName,
                        cat2Name: element.catTwoName,
                        cat3Name: element.catThreeName,
                        cat4Name: element.catFourName,
                        cat5Name: element.catFiveName,
                        cat6Name: element.catSixName,
                        desc1Code: element.descOneCode,
                        desc2Code: element.descTwoCode,
                        desc3Code: element.descThreeCode,
                        desc4Code: element.descFourCode,
                        desc5Code: element.descFiveCode,
                        desc6Code: element.descSixCode,
                        desc1Name: element.descOneName,
                        desc2Name: element.descTwoName,
                        desc3Name: element.descThreeName,
                        desc4Name: element.descFourName,
                        desc5Name: element.descFiveName,
                        desc6Name: element.descSixName,
                        udf: null,
                        designWiseTotalQty: element.totalQuantity,
                        designWiseNetAmountTotal: element.totalAmount,
                        designWiseTotalTax: element.totaltax,
                        designWiseTotalCalculatedMargin: element.totalCalculatedMargin,
                        designWiseTotalGST: element.totalgst,
                        designWiseTotalIntakeMargin: element.totalIntakeMargin,
                        designWiseRowId: element.designRowId,
                        designWiseTotalFinCharges: element.totalFinCharges,
                        designWiseTotalBasic: element.totalBasic,
                        designWiseTotalOtb: element.totalCalculatedOtb




                    }
                    lineItem.push(finalDataList);




                })



            }
            let finalSubmitData = {
                prevOrderNo: t.state.codeRadio === "setBased" ? t.state.orderSet : "",
                poType: t.state.codeRadio,
                orderDate: t.state.poDate,
                validFrom: t.state.poValidFrom,
                validTo: t.state.lastInDate,
                hl1Name: t.state.division,
                hl1Code: t.state.divisionCode,
                hl2Name: t.state.section,
                hl2Code: t.state.sectionCode,
                hl3Name: t.state.department,
                hl3Code: t.state.departmentCode,
                hl4Name: t.state.articleName,
                hl4Code: t.state.vendorMrpPo,
                slCode: t.state.slCode,
                slName: t.state.slName,
                slCityName: t.state.isCityExist == true ? t.state.city : t.state.slCityName,
                slAddr: t.state.slAddr,
                leadTime: t.state.leadDays,
                termCode: t.state.termCode,
                termName: t.state.termName,
                transporterCode: t.state.transporterCode,
                transporterName: t.state.transporterName,
                hsnSacCode: t.state.hsnSacCode,
                hsnCode: t.state.hsnCode,
                mrpStart: t.state.startRange,
                mrpEnd: t.state.endRange,
                poQuantity: t.state.poQuantity,
                poAmount: t.state.poAmount,
                stateCode: t.state.stateCode,
                itemUdfExist: t.state.itemUdfExist,
                isUDFExist: t.state.isUDFExist,
                isSiteExist: t.state.isSiteExist,
                pol: lineItem,
                isHoldPO: t.state.codeRadio == "holdPo" ? "true" : "false",
                siteCode: t.state.siteCode,
                siteName: t.state.siteName,
                orderNo: t.state.orderNo
            }

            t.props.poSaveDraftRequest(finalSubmitData)
        }, 5000);

    }

    debounceFun() {

        if (!this.state.loader) {
            this.setState({
                loader: true
            }, () => {
                this.contactSubmit();
            }
            )
        }

    }

    contactSubmit(e) {
        // e.preventDefault();

        // if (this.state.clickedOneTime == false) {

        this.supplier();
        this.poValid();
        this.lastInDate();
        this.vendorMrpPo();
        this.hsn();
        this.transporter();
        this.AddSecTwoRows();
        { this.state.isCityExist == true ? this.city() : null }

        var lineError = this.lineItemChkFun()

        this.otbNegative()
        this.gridFirst()
        { this.state.isSiteExist == "true" ? this.site() : null }
        { this.state.isUDFExist == "true" ? this.gridFivth() : null }
        this.gridSecond()
        { this.state.itemUdfExist == "true" ? this.gridThird() : null }
        this.gridFourth()


        const t = this;
        setTimeout(function (e) {

            const { cityerr, hsnerr, poValidFromerr, lastInDateerr, vendorerr, articleerr, transportererr, combineRow, otbStatus, lineItemChange } = t.state;
            let udf1 = "", udf2 = "", udf3 = "", udf4 = "", udf5 = "", udf6 = "", udf7 = "", udf8 = "", udf9 = "", udf10 = ""
            let udf11 = "", udf12 = "", udf13 = "", udf14 = "", udf15 = "", udf16 = "", udf17 = "", udf18 = "", udf19 = "", udf20 = ""

            if (!cityerr && !poValidFromerr && !hsnerr && !lastInDateerr && !vendorerr && !articleerr && !transportererr && combineRow) {
                const { hsnerr, poValidFromerr, lastInDateerr, vendorerr, articleerr, transportererr, combineRow, otbStatus, } = t.state;

                if (!otbStatus) {
                    if (t.state.gridFirst) {
                        if (t.state.gridSecond) {

                            if ((t.state.gridThird && t.state.itemUdfExist == "true") || t.state.itemUdfExist == "false") {
                                if (t.state.gridFourth) {
                                    if ((t.state.gridFivth && t.state.isUDFExist == "true") || t.state.isUDFExist == "false") {
                                        if (t.props.purchaseIndent.lineItem.data.resource != null && t.props.purchaseIndent.markUp.data.resource != null) {
                                            console.log(lineError.length)
                                            if (lineError.length == 0) {
                                                let itemData = t.state.secTwoRows;
                                                let udfRows = t.state.udfRows;
                                                let lineItem = []

                                                if (t.state.isUDFExist == "true" || t.state.itemUdfExist == "true") {
                                                    console.log(itemData)
                                                    itemData.forEach(element => {
                                                        if (t.state.isUDFExist == "true") {

                                                            udfRows.forEach(article => {
                                                                if (article.setUdfNo == element.setNo) {
                                                                    udf1 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING01") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf2 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING02") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf3 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING03") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf4 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING04") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf5 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING05") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf6 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING06") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf7 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING07") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf8 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING08") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf9 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING09") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf10 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFSTRING10") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf11 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFNUM01") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf12 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFNUM02") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf13 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFNUM03") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf14 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFNUM04") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf15 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFNUM05") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf16 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFDATE01") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf17 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFDATE02") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf18 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFDATE03") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf19 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFDATE04") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    udf20 = article.setUdfList.find(item => {
                                                                        if (item.udfType == "SMUDFDATE05") {
                                                                            return item
                                                                        }
                                                                    })
                                                                    let ratio = []
                                                                    let ratioIndex = 1
                                                                    let sizeIndex = 1

                                                                    // let sr = element.ratio
                                                                    let sr = t.state.sizeRef.state.sizeDateValue
                                                                    for (var k = 0; k < sr.length; k++) {

                                                                        let ra = {
                                                                            id: ratioIndex++,
                                                                            ratio: sr[k].ratio
                                                                        }
                                                                        ratio.push(ra)
                                                                    }
                                                                    var finalDataList = {};
                                                                    let sizeListDataa = []
                                                                    let elementSize = t.state.sizeRef.state.sizeDateValue
                                                                    for (let l = 0; l < elementSize.length; l++) {
                                                                        let payload = {
                                                                            id: sizeIndex++,
                                                                            code: elementSize[l].code,
                                                                            cname: elementSize[l].cname
                                                                        }
                                                                        sizeListDataa.push(payload)
                                                                    }

                                                                    finalDataList = {

                                                                        setHeaderId: t.state.codeRadio === "setBased" ? element.setHeaderId : "",
                                                                        rate: element.rate,
                                                                        finalRate: element.finalRate,
                                                                        discountType: element.discount.discountType,
                                                                        discountValue: element.discount.discountValue,
                                                                        itemId: element.itemBarcode,
                                                                        sizes: sizeListDataa,
                                                                        ratios: ratio,
                                                                        colors: element.colorList,
                                                                        qty: element.quantity,
                                                                        noOfSets: element.setQty,
                                                                        rsp: t.state.isRspRequired ? element.rsp : 0,
                                                                        mrp: t.state.isMrpRequired ? element.mrp : 0,
                                                                        images: Object.values(element.imageUrl),
                                                                        typeOfBuying: element.typeOfBuying,
                                                                        marginRule: element.marginRule,
                                                                        totalAmount: element.amount,
                                                                        netAmountTotal: element.amount,
                                                                        otb: element.otb,
                                                                        gst: element.gst,
                                                                        sumOfRatio: element.total,
                                                                        design: element.vendorDesignNo,
                                                                        finCharge: element.finCharges,
                                                                        remarks: element.remarks,
                                                                        tax: element.tax,

                                                                        deliveryDate: moment(element.deliveryDate).format(),
                                                                        containsImage: element.containsImage,
                                                                        calculatedMargin: t.state.calculatedMarginExact,
                                                                        intakeMargin: element.mrk,
                                                                        cat1Code: element.catOneCode == null ? "" : element.catOneCode,
                                                                        cat2Code: element.catTwoCode == null ? "" : element.catTwoCode,
                                                                        cat3Code: element.catThreeCode == null ? "" : element.catThreeCode,
                                                                        cat4Code: element.catFourCode == null ? "" : element.catFourCode,
                                                                        cat5Code: element.catFiveCode == null ? "" : element.catFiveCode,
                                                                        cat6Code: element.catSixCode == null ? "" : element.catSixCode,
                                                                        cat1Name: element.catOneName,
                                                                        cat2Name: element.catTwoName,
                                                                        cat3Name: element.catThreeName,
                                                                        cat4Name: element.catFourName,
                                                                        cat5Name: element.catFiveName,
                                                                        cat6Name: element.catSixName,
                                                                        // desc1Code: element.descOneCode == null ? "" : element.descOneCode,
                                                                        // desc2Code: element.descTwoCode == null ? "" : element.descTwoCode,
                                                                        // desc3Code: element.descThreeCode == null ? "" : element.descThreeCode,
                                                                        // desc4Code: element.descFourCode == null ? "" : element.descFourCode,
                                                                        // desc5Code: element.descFiveCode == null ? "" : element.descFiveCode,
                                                                        // desc6Code: element.descSixCode == null ? "" : element.descSixCode,
                                                                        desc1: element.descOneName,
                                                                        desc2: element.descTwoName,
                                                                        desc3: element.descThreeName,
                                                                        desc4: element.descFourName,
                                                                        desc5: element.descFiveName,
                                                                        desc6: element.descSixName,
                                                                        udf: {
                                                                            itemudf1: element.itemudf1,
                                                                            itemudf2: element.itemudf2,
                                                                            itemudf3: element.itemudf3,
                                                                            itemudf4: element.itemudf4,
                                                                            itemudf5: element.itemudf5,
                                                                            itemudf6: element.itemudf6,
                                                                            itemudf7: element.itemudf7,
                                                                            itemudf8: element.itemudf8,
                                                                            itemudf9: element.itemudf9,
                                                                            itemudf10: element.itemudf10,
                                                                            itemudf11: element.itemudf11,
                                                                            itemudf12: element.itemudf12,
                                                                            itemudf13: element.itemudf13,
                                                                            itemudf14: element.itemudf14,
                                                                            itemudf15: element.itemudf15,
                                                                            itemudf16: element.itemudf16,
                                                                            itemudf17: element.itemudf17,
                                                                            itemudf18: element.itemudf18,
                                                                            itemudf19: element.itemudf19,
                                                                            itemudf20: element.itemudf20,
                                                                            udf1: udf1 != undefined ? udf1.value : "",
                                                                            udf2: udf2 != undefined ? udf2.value : "",
                                                                            udf3: udf3 != undefined ? udf3.value : "",
                                                                            udf4: udf4 != undefined ? udf4.value : "",
                                                                            udf5: udf5 != undefined ? udf5.value : "",
                                                                            udf6: udf6 != undefined ? udf6.value : "",
                                                                            udf7: udf7 != undefined ? udf7.value : "",
                                                                            udf8: udf8 != undefined ? udf8.value : "",
                                                                            udf9: udf9 != undefined ? udf9.value : "",
                                                                            udf10: udf10 != undefined ? udf10.value : "",
                                                                            udf11: udf11 != undefined ? udf11.value : "",
                                                                            udf12: udf12 != undefined ? udf12.value : "",
                                                                            udf13: udf13 != undefined ? udf13.value : "",
                                                                            udf14: udf14 != undefined ? udf14.value : "",
                                                                            udf15: udf15 != undefined ? udf15.value : "",
                                                                            udf16: udf16 != undefined ? udf16.value : "",
                                                                            udf17: udf17 != undefined ? udf17.value : "",
                                                                            udf18: udf18 != undefined ? udf18.value : "",
                                                                            udf19: udf19 != undefined ? udf19.value : "",
                                                                            udf20: udf20 != undefined ? udf20.value : ""
                                                                        },
                                                                        designWiseTotalQty: element.totalQuantity,
                                                                        designWiseNetAmountTotal: element.totalAmount,
                                                                        designWiseTotalTax: element.totaltax,
                                                                        designWiseTotalCalculatedMargin: element.totalCalculatedMargin,
                                                                        designWiseTotalGST: element.totalgst,
                                                                        designWiseTotalIntakeMargin: element.totalIntakeMargin,
                                                                        designWiseRowId: element.designRowId,
                                                                        designWiseTotalFinCharges: element.totalFinCharges,
                                                                        designWiseTotalBasic: element.totalBasic,
                                                                        designWiseTotalOtb: element.totalCalculatedOtb




                                                                    }
                                                                    lineItem.push(finalDataList);
                                                                }

                                                            });
                                                        } else {

                                                            let ratio = []
                                                            let ratioIndex = 1

                                                            let sr = element.ratio
                                                            for (var k = 0; k < sr.length; k++) {

                                                                let ra = {
                                                                    id: ratioIndex++,
                                                                    ratio: sr[k]
                                                                }
                                                                ratio.push(ra)
                                                            }

                                                            var finalDataList = {};
                                                            let sizeListDataa = []
                                                            let elementSize = element.sizeListData
                                                            for (let l = 0; l < elementSize.length; l++) {
                                                                let payload = {
                                                                    id: elementSize[l].id,
                                                                    code: elementSize[l].code,
                                                                    cname: elementSize[l].cname
                                                                }
                                                                sizeListDataa.push(payload)
                                                            }


                                                            finalDataList = {

                                                                setHeaderId: t.state.codeRadio === "setBased" ? element.setHeaderId : "",
                                                                rate: element.rate,
                                                                finalRate: element.finalRate,
                                                                discountType: element.discount.discountType,
                                                                discountValue: element.discount.discountValue,
                                                                itemId: element.itemBarcode,
                                                                sizes: sizeListDataa,
                                                                ratios: ratio,
                                                                colors: element.colorList,
                                                                qty: element.quantity,
                                                                noOfSets: element.setQty,
                                                                rsp: t.state.isRspRequired ? element.rsp : 0,
                                                                mrp: t.state.isMrpRequired ? element.mrp : 0,
                                                                images: Object.values(element.imageUrl),
                                                                typeOfBuying: element.typeOfBuying,
                                                                marginRule: element.marginRule,
                                                                totalAmount: element.amount,
                                                                netAmountTotal: element.amount,
                                                                otb: element.otb,
                                                                gst: element.gst,
                                                                sumOfRatio: element.total,
                                                                design: element.vendorDesignNo,
                                                                finCharge: element.finCharges,
                                                                remarks: element.remarks,
                                                                tax: element.tax,

                                                                deliveryDate: moment(element.deliveryDate).format(),
                                                                containsImage: element.containsImage,
                                                                calculatedMargin: this.state.calculatedMarginExact,
                                                                intakeMargin: element.mrk,
                                                                cat1Code: element.catOneCode == null ? "" : element.catOneCode,
                                                                cat2Code: element.catTwoCode == null ? "" : element.catTwoCode,
                                                                cat3Code: element.catThreeCode == null ? "" : element.catThreeCode,
                                                                cat4Code: element.catFourCode == null ? "" : element.catFourCode,
                                                                cat5Code: element.catFiveCode == null ? "" : element.catFiveCode,
                                                                cat6Code: element.catSixCode == null ? "" : element.catSixCode,
                                                                cat1Name: element.catOneName,
                                                                cat2Name: element.catTwoName,
                                                                cat3Name: element.catThreeName,
                                                                cat4Name: element.catFourName,
                                                                cat5Name: element.catFiveName,
                                                                cat6Name: element.catSixName,
                                                                // desc1Code: element.descOneCode == null ? "" : element.descOneCode,
                                                                // desc2Code: element.descTwoCode == null ? "" : element.descTwoCode,
                                                                // desc3Code: element.descThreeCode == null ? "" : element.descThreeCode,
                                                                // desc4Code: element.descFourCode == null ? "" : element.descFourCode,
                                                                // desc5Code: element.descFiveCode == null ? "" : element.descFiveCode,
                                                                // desc6Code: element.descSixCode == null ? "" : element.descSixCode,
                                                                desc1: element.descOneName,
                                                                desc2: element.descTwoName,
                                                                desc3: element.descThreeName,
                                                                desc4: element.descFourName,
                                                                desc5: element.descFiveName,
                                                                desc6: element.descSixName,
                                                                udf: {
                                                                    itemudf1: element.itemudf1,
                                                                    itemudf2: element.itemudf2,
                                                                    itemudf3: element.itemudf3,
                                                                    itemudf4: element.itemudf4,
                                                                    itemudf5: element.itemudf5,
                                                                    itemudf6: element.itemudf6,
                                                                    itemudf7: element.itemudf7,
                                                                    itemudf8: element.itemudf8,
                                                                    itemudf9: element.itemudf9,
                                                                    itemudf10: element.itemudf10,
                                                                    itemudf11: element.itemudf11,
                                                                    itemudf12: element.itemudf12,
                                                                    itemudf13: element.itemudf13,
                                                                    itemudf14: element.itemudf14,
                                                                    itemudf15: element.itemudf15,
                                                                    itemudf16: element.itemudf16,
                                                                    itemudf17: element.itemudf17,
                                                                    itemudf18: element.itemudf18,
                                                                    itemudf19: element.itemudf19,
                                                                    itemudf20: element.itemudf20,
                                                                    udf1: "",
                                                                    udf2: "",
                                                                    udf3: "",
                                                                    udf4: "",
                                                                    udf5: "",
                                                                    udf6: "",
                                                                    udf7: "",
                                                                    udf8: "",
                                                                    udf9: "",
                                                                    udf10: "",
                                                                    udf11: "",
                                                                    udf12: "",
                                                                    udf13: "",
                                                                    udf14: "",
                                                                    udf15: "",
                                                                    udf16: "",
                                                                    udf17: "",
                                                                    udf18: "",
                                                                    udf19: "",
                                                                    udf20: ""
                                                                },
                                                                designWiseTotalQty: element.totalQuantity,
                                                                designWiseNetAmountTotal: element.totalAmount,
                                                                designWiseTotalTax: element.totaltax,
                                                                designWiseTotalCalculatedMargin: element.totalCalculatedMargin,
                                                                designWiseTotalGST: element.totalgst,
                                                                designWiseTotalIntakeMargin: element.totalIntakeMargin,
                                                                designWiseRowId: element.designRowId,
                                                                designWiseTotalFinCharges: element.totalFinCharges,
                                                                designWiseTotalBasic: element.totalBasic,
                                                                designWiseTotalOtb: element.totalCalculatedOtb




                                                            }
                                                            lineItem.push(finalDataList);

                                                        }

                                                    });

                                                } else {
                                                    itemData.forEach(element => {
                                                        let ratio = []
                                                        let ratioIndex = 1

                                                        let sr = element.ratio
                                                        for (var k = 0; k < sr.length; k++) {

                                                            let ra = {
                                                                id: ratioIndex++,
                                                                ratio: sr[k]
                                                            }
                                                            ratio.push(ra)
                                                        }

                                                        var finalDataList = {};
                                                        let sizeListDataa = []
                                                        let elementSize = element.sizeListData
                                                        for (let l = 0; l < elementSize.length; l++) {
                                                            let payload = {
                                                                id: elementSize[l].id,
                                                                code: elementSize[l].code,
                                                                cname: elementSize[l].cname
                                                            }
                                                            sizeListDataa.push(payload)
                                                        }

                                                        finalDataList = {

                                                            setHeaderId: t.state.codeRadio === "setBased" ? element.setHeaderId : "",
                                                            rate: element.rate,
                                                            finalRate: element.finalRate,
                                                            discountType: element.discount.discountType,
                                                            discountValue: element.discount.discountValue,
                                                            itemId: element.itemBarcode,
                                                            sizes: sizeListDataa,
                                                            ratios: ratio,
                                                            colors: element.colorList,
                                                            qty: element.quantity,
                                                            noOfSets: element.setQty,
                                                            rsp: t.state.isRspRequired ? element.rsp : 0,
                                                            mrp: t.state.isMrpRequired ? element.mrp : 0,
                                                            images: Object.values(element.imageUrl),
                                                            typeOfBuying: element.typeOfBuying,
                                                            marginRule: element.marginRule,
                                                            totalAmount: element.amount,
                                                            netAmountTotal: element.amount,
                                                            otb: element.otb,
                                                            gst: element.gst,
                                                            sumOfRatio: element.total,
                                                            design: element.vendorDesignNo,
                                                            finCharge: element.finCharges,
                                                            remarks: element.remarks,
                                                            tax: element.tax,

                                                            deliveryDate: moment(element.deliveryDate).format(),
                                                            containsImage: element.containsImage,
                                                            calculatedMargin: t.state.calculatedMarginExact,
                                                            intakeMargin: element.mrk,
                                                            cat1Code: element.catOneCode,
                                                            cat2Code: element.catTwoCode,
                                                            cat3Code: element.catThreeCode,
                                                            cat4Code: element.catFourCode,
                                                            cat5Code: element.catFiveCode,
                                                            cat6Code: element.catSixCode,

                                                            cat1Name: element.catOneName,
                                                            cat2Name: element.catTwoName,
                                                            cat3Name: element.catThreeName,
                                                            cat4Name: element.catFourName,
                                                            cat5Name: element.catFiveName,
                                                            cat6Name: element.catSixName,
                                                            desc1Code: element.descOneCode,
                                                            desc2Code: element.descTwoCode,
                                                            desc3Code: element.descThreeCode,
                                                            desc4Code: element.descFourCode,
                                                            desc5Code: element.descFiveCode,
                                                            desc6Code: element.descSixCode,
                                                            desc1Name: element.descOneName,
                                                            desc2Name: element.descTwoName,
                                                            desc3Name: element.descThreeName,
                                                            desc4Name: element.descFourName,
                                                            desc5Name: element.descFiveName,
                                                            desc6Name: element.descSixName,
                                                            udf: null,
                                                            designWiseTotalQty: element.totalQuantity,
                                                            designWiseNetAmountTotal: element.totalAmount,
                                                            designWiseTotalTax: element.totaltax,
                                                            designWiseTotalCalculatedMargin: element.totalCalculatedMargin,
                                                            designWiseTotalGST: element.totalgst,
                                                            designWiseTotalIntakeMargin: element.totalIntakeMargin,
                                                            designWiseRowId: element.designRowId,
                                                            designWiseTotalFinCharges: element.totalFinCharges,
                                                            designWiseTotalBasic: element.totalBasic,
                                                            designWiseTotalOtb: element.totalCalculatedOtb




                                                        }
                                                        lineItem.push(finalDataList);




                                                    })



                                                }
                                                let finalSubmitData = {
                                                    prevOrderNo: t.state.codeRadio === "setBased" ? t.state.orderSet : "",
                                                    poType: t.state.codeRadio,
                                                    orderDate: t.state.poDate,
                                                    validFrom: t.state.poValidFrom,
                                                    validTo: t.state.lastInDate,
                                                    hl1Name: t.state.division,
                                                    hl1Code: t.state.divisionCode,
                                                    hl2Name: t.state.section,
                                                    hl2Code: t.state.sectionCode,
                                                    hl3Name: t.state.department,
                                                    hl3Code: t.state.departmentCode,
                                                    hl4Name: t.state.articleName,
                                                    hl4Code: t.state.vendorMrpPo,
                                                    slCode: t.state.slCode,
                                                    slName: t.state.slName,
                                                    slCityName: t.state.isCityExist == true ? t.state.city : t.state.slCityName,
                                                    slAddr: t.state.slAddr,
                                                    leadTime: t.state.leadDays,
                                                    termCode: t.state.termCode,
                                                    termName: t.state.termName,
                                                    transporterCode: t.state.transporterCode,
                                                    transporterName: t.state.transporterName,
                                                    hsnSacCode: t.state.hsnSacCode,
                                                    hsnCode: t.state.hsnCode,
                                                    mrpStart: t.state.startRange,
                                                    mrpEnd: t.state.endRange,
                                                    poQuantity: t.state.poQuantity,
                                                    poAmount: t.state.poAmount,
                                                    stateCode: t.state.stateCode,
                                                    itemUdfExist: t.state.itemUdfExist,
                                                    isUDFExist: t.state.isUDFExist,
                                                    isSiteExist: t.state.isSiteExist,
                                                    pol: lineItem,
                                                    isHoldPO: t.state.codeRadio == "holdPo" ? "true" : "false",
                                                    siteCode: t.state.siteCode,
                                                    siteName: t.state.siteName
                                                }
                                                if (t.state.orderNo !== "") {
                                                    finalSubmitData.orderNo = t.state.orderNo
                                                    t.props.editedPoSaveRequest(finalSubmitData);
                                                }

                                                t.props.poCreateRequest(finalSubmitData)


                                            } else {
                                                t.setState({
                                                    loader: false,
                                                    multipleErrorpo: true,
                                                    lineError: lineError
                                                })


                                            }
                                        } else {

                                            t.setState({
                                                loader: false
                                            })
                                            t.setState({
                                                toastMsg: "Wait a moment untill charges calculated!!",
                                                toastLoader: true
                                            })

                                            setTimeout(function () {
                                                t.setState({
                                                    toastLoader: false
                                                })
                                            }, 1000)
                                        }
                                    }
                                    else {
                                        t.setState({
                                            loader: false
                                        })
                                        t.validateSetUdf();

                                    }
                                } else {
                                    t.setState({
                                        loader: false
                                    })
                                    t.validateLineItem();


                                }

                            } else {
                                t.setState({
                                    loader: false
                                })
                                t.validateUdf();

                            }

                        } else {
                            t.setState({
                                loader: false
                            })
                            t.validateCatDescRow();

                        }
                    } else {
                        t.setState({
                            loader: false
                        })

                        t.validateItemdesc();
                    }

                } else {
                    t.setState({
                        loader: false
                    })
                }
            } else {
                t.setState({
                    loader: false
                })
            }
        }, 1000)
        // }

    }


    // ______________________________VALIDATION MSG__________________

    loadIndent(e) {
        if (this.state.loadIndent == "") {
            this.setState({
                loadIndenterr: true
            });
        } else {
            this.setState({
                loadIndenterr: false
            });
        }

    }
    poValid(e) {
        if (this.state.poValidFrom == "") {
            this.setState({
                poValidFromerr: true
            });
        } else {
            this.setState({
                poValidFromerr: false
            });
        }
    }

    lastInDate(e) {
        if (this.state.lastInDate == "") {
            this.setState({
                lastInDateerr: true
            });
        } else {
            this.setState({
                lastInDateerr: false
            });
        }
    }

    vendorMrpPo(e) {
        if (this.state.vendorMrpPo == "") {
            this.setState({
                articleerr: true
            });
        } else {
            this.setState({
                articleerr: false
            });
        }
    }

    supplier(e) {
        if (this.state.supplier == "") {
            this.setState({
                vendorerr: true
            });
        } else {
            this.setState({
                vendorerr: false
            });
        }
    }

    transporter(e) {
        if (this.state.transporterValidation) {
            if (this.state.transporter == "") {
                this.setState({
                    transportererr: true
                });
            } else {
                this.setState({
                    transportererr: false
                });
            }
        } else {
            this.setState({
                transportererr: false
            });
        }
    }
    site(e) {
        if (this.state.siteName == "") {
            this.setState({
                siteNameerr: true
            });
        } else {
            this.setState({
                siteNameerr: false
            });
        }
    }
    city(e) {
        if (this.state.city == "") {
            this.setState({
                cityerr: true
            });
        } else {
            this.setState({
                cityerr: false
            });
        }
    }
    hsn(e) {
        if (this.state.hsnSacCode == "") {
            this.setState({
                hsnerr: true
            });
        } else {
            this.setState({
                hsnerr: false
            });
        }
    }
    icode(e) {
        if (this.state.itemCodeList.length == 0) {
            this.setState({
                itemcodeerr: true
            });
        } else {
            this.setState({
                itemcodeerr: false
            });
        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false
        })
        if (document.getElementById(this.state.errorId) != null) {
            document.getElementById(this.state.errorId).focus()
        }
    }

    // ___________________________________Clear Data_____________________________


    reset() {
        this.setState({
            resetAndDelete: true,
            headerMsg: "Are you sure to reset the form?",
            paraMsg: "Click confirm to continue.",
            rowDelete: ""
        })
    }
    resetRows() {

        this.setState({
            codeRadio: "Adhoc"
        })

        this.onClear();
        this.setBasedResest();
    }

    onClear(e) {
        this.setState({
            itemCodeList: [],
            city: "",

            saveMarginRule: "",
            descSix: "",
            slCode: "",
            loadIndentId: "",
            loadIndent: "",
            article: "",
            division: "",
            department: "",
            section: "",
            item: "",
            text: "",
            vendor: "",
            leadDays: "",
            transporter: "",
            startRange: "",
            endRange: "",
            catOne: "",
            catTwo: "",
            catThree: "",
            catFour: "",
            descTwo: "",
            descThree: "",
            descFive: "",
            supplier: "",
            vendorMrpPo: "",
            term: "",
            poQuantity: 0,
            poAmount: 0,
            articleName: "",

            hsnSacCode: "",
            itemUdfExist: "false",
            isUDFExist: "false",
            isSiteExist: "false",
            siteName: "",
            siteCode: "",
            supplierCode: "",
            sizeMapping: []



        })

        const t = this

        setTimeout(function () {
            t.setState({
                leadDays: "",
                supplierCode: "",

                poRows: [{
                    finalRate: "",
                    color: [],
                    size: [],
                    ratio: [],
                    vendorMrp: "",
                    vendorDesign: "",
                    mrk: "",
                    discount: {
                        discountType: "",
                        discountValue: ""
                    },
                    rate: "",
                    netRate: "",
                    rsp: "",
                    mrp: "",
                    quantity: "",
                    amount: "",
                    otb: "",
                    remarks: "",
                    gst: [],
                    finCharges: [],
                    concatFinCharges: [],
                    basic: "",
                    tax: [],
                    calculatedMargin: [],
                    gridOneId: 1,
                    deliveryDate: "",
                    typeOfBuying: "",
                    marginRule: "",
                    image: [],
                    imageUrl: {},
                    containsImage: false,

                }],
                secTwoRows: [{
                    discount: {
                        discountType: "",
                        discountValue: ""
                    },
                    finalRate: "",
                    colorChk: false,
                    icodeChk: false,
                    setHeaderId: "",
                    designRowId: 1,
                    color: [],
                    colorList: [],
                    itemBarcode: "",
                    size: "",
                    setRatio: "",
                    sizeListData: [],
                    colorSizeList: [],
                    icodes: [],
                    vendorDesignNo: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",
                    ratio: [],
                    gst: "",
                    finCharges: [],
                    tax: "",
                    calculatedMargin: "",
                    mrk: "",
                    gridTwoId: 1,
                    otb: "",
                    totalQuantity: "",
                    totalNetAmt: "",
                    totaltax: [],
                    totalCalculatedMargin: [],
                    totalgst: [],
                    totalIntakeMargin: [],
                    totalFinCharges: [],
                    totalBasic: "",
                    totalCalculatedOtb: "",
                    catOneCode: "",
                    catOneName: "",
                    catTwoCode: "",
                    catTwoName: "",
                    catThreeCode: "",
                    catThreeName: "",
                    catFourCode: "",
                    catFourName: "",
                    catFiveCode: "",
                    catFiveName: "",
                    catSixCode: "",

                    catSixName: "",
                    descOneCode: "",
                    descOneName: "",
                    descTwoCode: "",
                    descTwoName: "",
                    descThreeCode: "",
                    descThreeName: "",
                    descFourCode: "",
                    descFourName: "",
                    descFiveCode: "",
                    descFiveName: "",
                    descSixCode: "",
                    descSixName: "",
                    itemudf1: "",
                    itemudf2: "",
                    itemudf3: "",
                    itemudf4: "",
                    itemudf5: "",
                    itemudf6: "",
                    itemudf7: "",
                    itemudf8: "",
                    itemudf9: "",
                    itemudf10: "",
                    itemudf11: "",
                    itemudf12: "",
                    itemudf13: "",
                    itemudf14: "",
                    itemudf15: "",
                    itemudf16: "",
                    itemudf17: "",
                    itemudf18: "",
                    itemudf19: "",
                    itemudf20: "",
                    deliveryDate: "",
                    typeOfBuying: "",
                    marginRule: "",
                    image: [],
                    imageUrl: {},
                    containsImage: false,
                    lineItemChk: false
                }],
                udfRows: [{
                    setUdfNo: 1,
                    udfColor: [],

                    designRowId: 1,
                    udfGridId: 1,
                }],
                catDescRows: [{
                    vendorDesign: "",
                    designRowId: 1,
                    gridId: 1
                }],
                itemUdf: [{
                    designRowId: 1,
                    itemUdfGridId: 1,
                    vendorDesign: ""
                }]
            })
        }, 10)
    }
    setBasedResest() {
        this.setState({
            setDepartment: "",
            hl3CodeDepartment: "",
            poSetVendor: "",
            orderSet: "",
        })
    }
    // ___________________________________PURCHASEORDER ADDNEW FUNCTIONALITY HIDE___________________

    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }
    }
    _handleKeyPressRow(e, idName, count) {

        if (e.key === "F7" || e.key === "F2") {
            let idd = idName + count;
            document.getElementById(idd).click();
        }
        if (e.key === "ArrowDown") {
            count++;

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }
        }
        if (e.key === "ArrowUp") {
            count--

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }

        }

    }
    _handleKeyPressDate(e, id) {
        let idd = id;
        if (e.key === "Enter") {
            document.getElementById(idd).click();
        }
    }
    gridFirst() {
        let poRows = this.state.poRows

        let flag = false
        poRows.forEach(po => {
            flag = (po.vendorMrp == "" && this.state.isMrpRequired) || (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) || po.rate == "" || po.marginRule == ""
                || (po.mrp == "" && this.state.isMrpRequired) || po.deliveryDate == "" || po.typeOfBuying == "" ? true : false
        })
        if (flag) {
            this.setState({
                gridFirst: false
            })
        } else {
            this.setState({
                gridFirst: true
            })
        }
    }
    gridSecond() {
        let catDescRows = this.state.catDescRows
        let flag = false

        // sessionStorage.getItem("partnerEnterpriseName") == "VMART" ?
        //     catDescRows.forEach(po => {
        //         po.itemDetails.forEach(val => {
        //             if (val.value == "" && (val.catdesc == "CAT2" || val.catdesc == "CAT3" || val.catdesc == "CAT4" || val.catdesc == "DESC3")) {
        //                 flag = true
        //                 return flag
        //             }
        //         })
        //     }) : 
        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ?
            catDescRows.forEach(po => {
                po.itemDetails.forEach(val => {
                    // if (val.value == "" && (val.catdesc != "CAT5" && val.catdesc != "CAT6" && val.catdesc != "DESC1" && val.catdesc != "DESC4" &&val.catdesc!= "DESC6" )) {
                    if ((val.catdesc == "CAT1" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT2" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT3" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT4" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC1" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC2" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC3" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC5" && (val.isLovPO == "Y" && val.isCompulsoryPO == "Y") && val.value == "")) {
                        flag = true
                        return flag
                    }
                })
            }) :

            catDescRows.forEach(po => {
                po.itemDetails.forEach(val => {
                    if ((val.catdesc == "CAT1" && ((this.state.cat1Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT2" && ((this.state.cat2Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT3" && ((this.state.cat3Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT4" && ((this.state.cat4Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT5" && ((this.state.cat5Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "CAT6" && ((this.state.cat6Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC1" && ((this.state.desc1Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC2" && ((this.state.desc2Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC3" && ((this.state.desc3Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC4" && ((this.state.desc4Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC5" && ((this.state.desc5Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.catdesc == "DESC6" && ((this.state.desc6Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "")) {
                        flag = true
                        return flag
                    }
                })
            })
        if (flag) {
            this.setState({
                gridSecond: false
            })
        } else {
            this.setState({
                gridSecond: true
            })
        }
    }
    gridThird() {
        let itemUdf = this.state.itemUdf
        let flag = true
        itemUdf.forEach(iu => {
            iu.udfList.forEach(val => {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                    if (val.value == "" && (val.isCompulsoryPO == "Y" && val.isLovPO == "Y")) {
                        flag = false;
                    }
                } else {
                    if ((val.cat_desc_udf == "UDFSTRING01" && ((this.state.itemudf1Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING02" && ((this.state.itemudf2Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING03" && ((this.state.itemudf3Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING04" && ((this.state.itemudf4Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING05" && ((this.state.itemudf5Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING06" && ((this.state.itemudf6Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING07" && ((this.state.itemudf7Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING08" && ((this.state.itemudf8Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING09" && ((this.state.itemudf9Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFSTRING10" && ((this.state.itemudf10Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFNUM01" && ((this.state.itemudf11Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFNUM02" && ((this.state.itemudf12Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFNUM03" && ((this.state.itemudf13Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFNUM04" && ((this.state.itemudf14Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFNUM05" && ((this.state.itemudf15Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFDATE01" && ((this.state.itemudf16Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFDATE02" && ((this.state.itemudf17Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFDATE03" && ((this.state.itemudf18Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFDATE04" && ((this.state.itemudf19Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "UDFDATE05" && ((this.state.itemudf20Validation == true || val.isLovPO == "Y") && val.isCompulsoryPO == "Y") && val.value == "")) {
                        flag = false;
                    }
                }
            })
        })
        if (flag) {
            this.setState({
                gridThird: true
            })
        } else {
            this.setState({
                gridThird: false
            })
        }
    }
    gridFourth() {

        let secTwoRows = this.state.secTwoRows
        let flag = true
        secTwoRows.forEach(cd => {

            if (cd.color.length == 0 || (cd.vendorDesignNo == "" && !this.state.isVendorDesignNotReq) || cd.setQty == "" || cd.setQty.toString() == "0" || cd.setQty == 0) {

                flag = false
            }
        })

        if (flag) {
            this.setState({
                gridFourth: true
            })
        } else {
            this.setState({
                gridFourth: false
            })
        }

    }

    gridFivth() {

        let udfRows = this.state.udfRows
        let flag = true
        udfRows.forEach(ud => {

            ud.setUdfList.forEach(val => {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {

                    if (val.value == "" && val.isCompulsary == "Y" && val.isLovPO == "Y") flag = false;
                } else {
                    if ((val.cat_desc_udf == "SMUDFSTRING01" && ((this.state.udf1Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING02" && ((this.state.udf2Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING03" && ((this.state.udf3Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING04" && ((this.state.udf4Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING05" && ((this.state.udf5Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING06" && ((this.state.udf6Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING07" && ((this.state.udf7Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING08" && ((this.state.udf8Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING09" && ((this.state.udf9Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFSTRING10" && ((this.state.udf10Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFNUM01" && ((this.state.udf11Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFNUM02" && ((this.state.udf12Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFNUM03" && ((this.state.udf13Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFNUM04" && ((this.state.udf14Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFNUM05" && ((this.state.udf15Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFDATE01" && ((this.state.udf16Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFDATE02" && ((this.state.udf17Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFDATE03" && ((this.state.udf18Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFDATE04" && ((this.state.udf19Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "") ||
                        (val.cat_desc_udf == "SMUDFDATE05" && ((this.state.udf20Validation == true || val.isLovPO == "Y") && val.isCompulsary == "Y") && val.value == "")) {
                        flag = false;
                    }
                }
            })
        })

        if (flag) {
            this.setState({
                gridFivth: true
            })
        } else {
            this.setState({
                gridFivth: false
            })
        }
    }

    validateCatDescRow() {
        let catDescRows = this.state.catDescRows
        var type = ""
        for (var i = 0; i < catDescRows.length; i++) {

            for (var j = 0; j < catDescRows[i].itemDetails.length; j++) {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                    // if ((catDescRows[i].itemDetails[j].catdesc == "CAT2" || catDescRows[i].itemDetails[j].catdesc == "CAT3" || catDescRows[i].itemDetails[j].catdesc == "CAT4" || catDescRows[i].itemDetails[j].catdesc == "DESC3") && catDescRows[i].itemDetails[j].value == "") {
                    if ((catDescRows[i].itemDetails[j].catdesc == "CAT1" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT2" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT3" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT4" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC1" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC2" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC3" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC4" && (catDescRows[i].itemDetails[j].isLovPO == "Y" && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "")) {

                        type = typeof (catDescRows[i].itemDetails[j].displayName) == "string" && catDescRows[i].itemDetails[j].displayName != "null" ? catDescRows[i].itemDetails[j].displayName : catDescRows[i].itemDetails[j].catdesc
                        break
                    }
                } else {

                    if ((catDescRows[i].itemDetails[j].catdesc == "CAT1" && ((this.state.cat1Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT2" && ((this.state.cat2Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT3" && ((this.state.cat3Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT4" && ((this.state.cat4Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT5" && ((this.state.cat5Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "CAT6" && ((this.state.cat6Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC1" && ((this.state.desc1Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC2" && ((this.state.desc2Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC3" && ((this.state.desc3Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC4" && ((this.state.desc4Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC5" && ((this.state.desc5Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "") ||
                        (catDescRows[i].itemDetails[j].catdesc == "DESC6" && ((this.state.desc6Validation == true || catDescRows[i].itemDetails[j].isLovPO == "Y") && catDescRows[i].itemDetails[j].isCompulsoryPO == "Y") && catDescRows[i].itemDetails[j].value == "")) {


                        type = typeof (catDescRows[i].itemDetails[j].displayName) == "string" && catDescRows[i].itemDetails[j].displayName != "null" ? catDescRows[i].itemDetails[j].displayName : catDescRows[i].itemDetails[j].catdesc
                        break

                    }
                }
            }
        }

        if (type != "") {
            this.setState({
                poErrorMsg: true,
                errorMassage: type + " is compulsory"
            })
        }
    }

    validateItemdesc() {

        let poRows = this.state.poRows
        poRows.forEach(po => {
            if (po.vendorMrp == "" && this.state.isMrpRequired) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? "Desc 6  is compulsory" : "MRP is compulsory"
                })


            }

            else if (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor Design is compulsory"
                })

            }
            else if (po.mrp == "" && this.state.isMrpRequired) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Mrp is compulsory"
                })

            }

            else if (po.rate == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Rate is compulsory"
                })


            } else if (po.deliveryDate == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Delivery date is compulsory"
                })


            } else if (po.typeOfBuying == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Type of Buying is compulsory"
                })


            } else if (po.marginRule == "") {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Margin Rule is compulsory"
                })
            }

        })
    }
    validateUdf() {
        let itemUdf = this.state.itemUdf
        var cat_desc_udf = ""

        for (var i = 0; i < itemUdf.length; i++) {
            for (var j = 0; j < itemUdf[i].udfList.length; j++) {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                    if (itemUdf[i].udfList[j].isCompulsoryPO == "Y" && itemUdf[i].udfList[j].isLovPO == "Y" && itemUdf[i].udfList[j].value == "") {

                        cat_desc_udf = itemUdf[i].udfList[j].displayName != null ? itemUdf[i].udfList[j].displayName : itemUdf[i].udfList[j].cat_desc_udf
                        break
                    }


                } else {
                    if ((itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING01" && ((this.state.itemudf1Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING02" && ((this.state.itemudf2Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING03" && ((this.state.itemudf3Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING04" && ((this.state.itemudf4Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING05" && ((this.state.itemudf5Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING06" && ((this.state.itemudf6Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING07" && ((this.state.itemudf7Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING08" && ((this.state.itemudf8Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING09" && ((this.state.itemudf9Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFSTRING10" && ((this.state.itemudf10Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFNUM01" && ((this.state.itemudf11Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFNUM02" && ((this.state.itemudf12Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFNUM03" && ((this.state.itemudf13Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFNUM04" && ((this.state.itemudf14Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFNUM05" && ((this.state.itemudf15Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFDATE01" && ((this.state.itemudf16Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFDATE02" && ((this.state.itemudf17Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (itemUdf[i].udfList[j].cat_desc_udf == "UDFDATE04" && ((this.state.itemudf19Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == "") ||
                        (vaitemUdf[i].udfList[j].cat_desc_udf == "UDFDATE05" && ((this.state.itemudf20Validation == true || itemUdf[i].udfList[j].isLovPO == "Y") && itemUdf[i].udfList[j].isCompulsoryPO == "Y") && itemUdf[i].udfList[j].value == ""))
                    // if (itemUdf[i].udfList[j].isCompulsoryPO == "Y" && itemUdf[i].udfList[j].value == "")
                    {

                        cat_desc_udf = itemUdf[i].udfList[j].displayName != null ? itemUdf[i].udfList[j].displayName : itemUdf[i].udfList[j].cat_desc_udf
                        break
                    }
                }
            }
        }

        if (cat_desc_udf != "") {
            this.setState({
                poErrorMsg: true,
                errorMassage: cat_desc_udf + " is compulsory"
            })
        }
    }
    validateLineItem() {
        let secTwoRows = this.state.secTwoRows
        console.log(this.state.secTwoRows)
        secTwoRows.forEach(sec => {

            if (sec.vendorDesignNo == "" && !this.state.isVendorDesignNotReq) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor design is compulsory in section 3"
                })

            } else if (sec.setQty == "" || sec.setQty.toString() == "0" || sec.setQty == 0) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Set quantity is compulsory"
                })
            }

            else if (sec.color.length == 0) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Color is compulsory"
                })


            }
        })
    }
    //_______________________________________validate set udf___________________________

    validateSetUdf() {
        let udfRows = this.state.udfRows
        var displayName = ""

        for (let i = 0; i < udfRows.length; i++) {
            for (var j = 0; j < udfRows[i].setUdfList.length; j++) {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                    if (udfRows[i].setUdfList[j].isCompulsary == "Y" && udfRows[i].setUdfList[j].isLovPO == "Y" && udfRows[i].setUdfList[j].value == "") {
                        displayName = udfRows[i].setUdfList[j].displayName != null ? udfRows[i].setUdfList[j].displayName : udfRows[i].setUdfList[j].udfType
                        break
                    }
                } else {
                    if ((udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING01" && ((this.state.udf1Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING02" && ((this.state.udf2Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING03" && ((this.state.udf3Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING04" && ((this.state.udf4Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING05" && ((this.state.udf5Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING06" && ((this.state.udf6Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING07" && ((this.state.udf7Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING08" && ((this.state.udf8Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING09" && ((this.state.udf9Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFSTRING10" && ((this.state.udf10Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFNUM01" && ((this.state.udf11Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFNUM02" && ((this.state.udf12Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFNUM03" && ((this.state.udf13Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFNUM04" && ((this.state.udf14Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFNUM05" && ((this.state.udf15Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFDATE01" && ((this.state.udf16Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFDATE02" && ((this.state.udf17Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFDATE03" && ((this.state.udf18Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFDATE04" && ((this.state.udf19Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == "") ||
                        (udfRows[i].setUdfList[j].cat_desc_udf == "SMUDFDATE05" && ((this.state.udf20Validation == true || udfRows[i].setUdfList[j].isLovPO == "Y") && udfRows[i].setUdfList[j].isCompulsary == "Y") && udfRows[i].setUdfList[j].value == ""))

                    // if (udfRows[i].setUdfList[j].isCompulsary == "Y" && udfRows[i].setUdfList[j].value == "") 
                    {


                        displayName = udfRows[i].setUdfList[j].displayName != null ? udfRows[i].setUdfList[j].displayName : udfRows[i].setUdfList[j].udfType
                        break
                    }
                }
            }
        }
        if (displayName != "") {
            this.setState({
                poErrorMsg: true,
                errorMassage: displayName + " is compulsory"
            })

        }
    }

    // _________________________________CONFIRM MODAL______________________________
    onnRadioChange(cr) {

        this.setState({
            codeRadio: cr,
        })

        setTimeout(() => {
            if (cr === "raisedIndent") {
                document.getElementById("loadIndentInput").focus()
            } else if (cr === "setBased" || cr === "holdPo") {
                document.getElementById("setDepartment").focus()
            } else if (cr === "Adhoc" || cr === "poIcode") {
                document.getElementById("poValidFrom").focus()
            }
            this.onClear();
            this.setBasedResest();
        }, 10);

    }
    handleRadioChange(dat) {
        let indent = ""
        if (dat == "Adhoc") {
            indent = "adhoc"
        } else if (dat == "raisedIndent") {
            indent = "raised indent"
        } else if (dat == "setBased") {
            indent = "set based"
        }
        else if (dat == "holdPo") {
            indent = "hold PO"
        }
        else if (dat == "poIcode") {
            indent = "PO with Icode"
        }
        this.setState({
            headerMsg: "Are you sure you want to raise purchase order on the basis of " + indent + " ?",
            paraMsg: "Click confirm to continue.",
            radioChange: dat,
            confirmModal: true,
        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
        if (this.state.codeRadio == "poUpload") {

        }
    }
    closeDeleteConfirmModal(e) {
        this.setState({
            deleteConfirmModal: !this.state.deleteConfirmModal,
        })

    }

    closeResetDeleteModal() {
        this.setState({
            resetAndDelete: !this.state.resetAndDelete,
        })
    }

    Capitalize(str) {
        if (str == undefined) {
            return "";
        } else {

            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    }
    openItemModal(type, id, value, displayName, idFocus, code) {
        let poRows = this.state.poRows
        let flag = false
        poRows.forEach(po => {
            flag = (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) || po.rate == "" || po.marginRule == "" || (po.mrp == "" && this.state.isMrpRequired) || (po.vendorMrp == "" && this.state.isMrpRequired) || po.typeOfBuying == "" || po.deliveryDate == "" ? true : false

        })
        if (flag) {
            this.setState({
                errorId: idFocus,
            })
            this.validateItemdesc()
        } else {
            var data = {
                hl3Code: this.state.departmentCode,
                hl3Name: this.state.department,
                itemType: type,
                no: 1,
                type: 1,
                search: ""
            }
            this.props.getItemDetailsValueRequest(data);
            this.setState({
                itemDetailsModal: true,
                itemDetailsModalAnimation: !this.state.itemDetailsModalAnimation,
                itemId: id,
                itemDetailValue: value,
                itemDetailCode: code,
                itemType: type,
                itemDetailName: typeof (displayName) == "string" && displayName != "null" ? displayName : type,
                focusId: idFocus,

            })
        }
    }
    //image modal

    openImageModal(id, image) {

        if (this.state.vendorMrpPo == "" || this.state.vendorMrpPo == undefined) {
            this.setState({
                toastMsg: "Select Article",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            let rows = this.state.poRows

            for (var i = 0; i < rows.length; i++) {
                if (rows[i].gridOneId == id) {

                    this.setState({
                        imageState: rows[i].imageUrl
                    })
                }
            }
            this.setState({
                imageRowId: id,
                imageModal: true,
                imageModalAnimation: !this.state.imageModalAnimation,
                focusImage: image
            });
        }
    }

    closePiImageModal() {
        document.getElementById(this.state.focusImage).focus()
        this.setState({
            imageModal: false,
            imageModalAnimation: !this.state.imageModalAnimation
        });
    }


    updateImage(data) {
        let c = this.state.poRows;
        for (var i = 0; i < c.length; i++) {
            if (c[i].gridOneId.toString() == data.imageRowId) {
                c[i].imageUrl = data.file
                c[i].image = Object.keys(data.file)
                c[i].containsImage = Object.keys(data.file).length != 0 ? true : false
            }

        }

        this.setState({
            poRows: c,

        })
        document.getElementById(this.state.focusImage).focus()

    }

    calculatedMargin(marginId, marginLeft) {


        let marginRate = document.getElementById(marginId)
        var viewportOffset = marginRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - (marginId.length <= 8 ? 345 : 175);


        if (!document.getElementById(marginLeft)) {
        } else {
            document.getElementById(marginLeft).style.left = left;
            document.getElementById(marginLeft).style.top = top;
        }
    }
    //end of image modal

    //______________________SET BASED MODALS START_____________________

    departmentSetModal(e) {
        this.setState({
            departmentModal: true,
            departmentSetBasedAnimation: !this.state.departmentSetBasedAnimation,
        })
        let data = {
            no: 1,
            type: 1,
            search: "",
        }
        this.props.departmentSetBasedRequest(data)

    }
    onCloseDepartmentModal(e) {
        this.setState({
            departmentModal: false,
            departmentSetBasedAnimation: !this.state.departmentSetBasedAnimation,
        })
        document.getElementById('setDepartment').focus()
    }
    setVendorModalOpen(e, id) {
        if (this.state.setDepartment != "") {
            let data = {
                no: 1,
                type: 1,
                search: "",
                hl3code: this.state.hl3CodeDepartment,
            }
            this.props.selectVendorRequest(data)
            this.setState({
                errorId: id,
                setVendor: true,
                setVendorModalAnimation: !this.state.setVendorModalAnimation,
            })
        } else {
            this.setState({
                errorMassage: "Select Department",
                poErrorMsg: true

            })
        }
    }
    onCloseVendor() {
        this.setState({
            setVendor: false,
            setVendorModalAnimation: !this.state.setVendorModalAnimation,
        })
        document.getElementById("poSetVendor").focus()
    }
    updateDepartment(data) {
        this.setState({
            setDepartment: data.setDepartment,
            hl3CodeDepartment: data.hl3code,
            poSetVendor: "",
            orderSet: ""
        })
        this.onClear();


    }
    updateVendorPo(data) {
        this.setState({
            poSetVendor: data.poSetVendor,
            poSetVendorCode: data.poSetVendorCode,
            orderSet: ""
        })
        this.onClear();
    }

    onSetOrderNumber(e) {
        if (this.state.lastInDate != "") {
            if (this.state.poSetVendor != "") {
                this.setState({
                    orderNumber: true,
                    orderNumberModalAnimation: !this.state.orderNumberModalAnimation
                })
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    hl3Code: this.state.hl3CodeDepartment,
                    supplierCode: this.state.poSetVendorCode
                }
                this.props.selectOrderNumberRequest(data)
            } else {
                this.setState({
                    errorId: id,
                    errorMassage: "Select Vendor",
                    poErrorMsg: true

                })
            }
        } else {
            this.setState({
                errorMassage: "Select Last In Date",
                poErrorMsg: true

            })
        }
    }

    closeSetNumber() {


        this.setState({
            orderNumber: false,
            orderNumberModalAnimation: !this.state.orderNumberModalAnimation


        })
        document.getElementById("orderSet").focus()

    }

    updateOrderNumber(data) {

        this.setState({
            orderSet: data
        })
        let payload = {
            orderNo: data,
            validTo: moment(this.state.lastInDate).format("DD-MMM-YYYY"),
            poType: this.state.codeRadio
        }

        this.props.setBasedRequest(payload)
        document.getElementById("orderSet").focus()

    }



    copySet(copyid, designId, setNo) {


        let rows = this.state.secTwoRows
        let rowids = []
        let finalId = ""
        let secTwoRowsObj = {}
        let sets = []
        let finalSetnO = ""

        for (let i = 0; i < rows.length; i++) {
            rowids.push(rows[i].gridTwoId)

        }
        finalId = Math.max(...rowids);
        for (let i = 0; i < rows.length; i++) {
            sets.push(rows[i].setNo)

        }
        finalSetnO = Math.max(...sets)
        let c = this.state.sizeMapping
        let sizeArray = []
        c.forEach(element => {
            var array = {}
            array = {
                orderBy: element.orderBy,
                code: element.code,
                cname: element.cname
            }
            sizeArray.push(array)

        });

        for (var i = 0; i < sizeArray.length; i++) {
            sizeArray[i].ratio = ""
        }
        for (let i = 0; i < rows.length; i++) {
            if (rows[i].gridTwoId == copyid) {
                let gid = finalId + 1
                let set = finalSetnO + 1
                secTwoRowsObj = {
                    discount: {
                        discountType: "",
                        discountValue: ""
                    },
                    finalRate: "",
                    colorChk: false,
                    icodeChk: false,
                    designRowId: designId + i.toString(),
                    color: rows[i].color,
                    colorList: rows[i].colorList,
                    size: "",
                    setRatio: "",
                    sizeListData: [],
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    vendorDesignNo: rows[i].vendorDesignNo,
                    option: rows[i].option,
                    setNo: set,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: rows[i].rate,
                    ratio: [],
                    gst: "",
                    finCharges: [],
                    tax: "",
                    calculatedMargin: "",
                    mrk: "",
                    gridTwoId: gid,
                    sizeList: sizeArray,
                    otb: "",
                    totalQuantity: "",
                    totalNetAmt: "",
                    totaltax: [],
                    totalCalculatedMargin: [],
                    totalgst: [],
                    totalIntakeMargin: [],
                    totalFinCharges: [],
                    totalBasic: "",
                    totalCalculatedOtb: "",

                    catOneCode: "",
                    catOneName: "",
                    catTwoCode: "",
                    catTwoName: "",
                    catThreeCode: "",
                    catThreeName: "",
                    catFourCode: "",
                    catFourName: "",
                    catFiveCode: "",
                    catFiveName: "",
                    catSixCode: "",
                    catSixName: "",
                    descOneCode: "",
                    descOneName: "",
                    descTwoCode: "",
                    descTwoName: "",
                    descThreeCode: "",
                    descThreeName: "",
                    descFourCode: "",
                    descFourName: "",
                    descFiveCode: "",
                    descFiveName: "",
                    descSixCode: "",
                    descSixName: "",
                    itemudf1: "",
                    itemudf2: "",
                    itemudf3: "",
                    itemudf4: "",
                    itemudf5: "",
                    itemudf6: "",
                    itemudf7: "",
                    itemudf8: "",
                    itemudf9: "",
                    itemudf10: "",
                    itemudf11: "",
                    itemudf12: "",
                    itemudf13: "",
                    itemudf14: "",
                    itemudf15: "",
                    itemudf16: "",
                    itemudf17: "",
                    itemudf18: "",
                    itemudf19: "",
                    itemudf20: "",
                    deliveryDate: "",
                    typeOfBuying: "",
                    marginRule: "",
                    image: [],
                    imageUrl: {},
                    containsImage: false,
                }


            }

        }
        if (this.state.isUDFExist == "true") {
            let udfRows = this.state.udfRows
            let udfGrid = {}
            for (let j = 0; j < udfRows.length; j++) {
                if (udfRows[j].udfGridId == copyid) {
                    let id = finalId + 1
                    let set = finalSetnO + 1

                    udfGrid = {
                        setUdfNo: set,
                        udfColor: udfRows[j].udfColor,
                        setUdfList: udfRows[j].setUdfList,

                        designRowId: udfRows[j].designRowId,
                        udfGridId: id,

                    };

                    udfRows.push(udfGrid)
                }
            }


            let udfRowss = _.map(
                _.uniq(
                    _.map(udfRows, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );



            this.setState({
                secTwoRows: [...this.state.secTwoRows, secTwoRowsObj],
                udfRows: udfRowss
            })
            setTimeout(() => {
                this.getOtbForSecTwoRows()
            }, 100)
        } else {
            this.setState({
                secTwoRows: [...this.state.secTwoRows, secTwoRowsObj],

            })
            setTimeout(() => {
                this.getOtbForSecTwoRows()
            }, 100)
        }
    }
    openHsnCodeModal(e, id) {
        if (this.state.vendorMrpPo != "") {
            this.setState({
                hsnRowId: "",
                hsnModal: true,
                hsnModalAnimation: !this.state.hsnModalAnimation
            })
            let data = {
                rowId: "",
                code: this.state.departmentCode,
                no: 1,
                type: 1,
                search: ""
            }
            this.props.hsnCodeRequest(data)
        } else {
            this.setState({
                errorId: id,
                errorMassage: "Select Article",
                poErrorMsg: true
            })
        }
    }
    closeHsnModal() {
        this.setState({
            hsnModal: false,
            hsnModalAnimation: !this.state.hsnModalAnimation
        })
        document.getElementById("hsn").focus()
    }
    updateHsnCode(data) {

        this.setState({
            hsnSacCode: data.hsnSacCode,
            hsnCode: data.hsnCode
        }, () => {
            this.hsn();

        })

        let r = this.state.poRows
        console.log("poRows, ", r)
        let secTwoRows = this.state.secTwoRows
        let lineItemArray = []
        for (let i = 0; i < r.length; i++) {
            let idd = r[i].gridOneId



            for (let j = 0; j < secTwoRows.length; j++) {
                if (secTwoRows[j].designRowId == idd && secTwoRows[j].setQty != "" && r[i].finalRate != "" && r[i].finalRate != 0) {
                    let taxData = {
                        hsnSacCode: data.hsnSacCode,
                        qty: Number(secTwoRows[j].setQty) * Number(secTwoRows[j].total),
                        rate: r[i].finalRate,
                        rowId: secTwoRows[j].gridTwoId,
                        designRowid: Number(idd),
                        basic: Number(secTwoRows[j].setQty) * Number(secTwoRows[j].total) * Number(r[i].finalRate),
                        clientGstIn: sessionStorage.getItem('gstin'),
                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                        supplierGstinStateCode: this.state.stateCode,
                        purtermMainCode: this.state.termCode,
                        siteCode: this.state.siteCode
                    }
                    lineItemArray.push(taxData)
                }


                // let marginlength = r[i].calculatedMargin.length
                // calculatedMarginValue = Math.round(((r[i].rsp - finalRate) / r[i].rsp) * 100 * 100) / 100

                // let marginarray = []
                // for (let i = 0; i < marginlength; i++) {
                //     marginarray.push(calculatedMarginValue)
                // }
                // r[i].calculatedMargin = marginarray
            }
            if (lineItemArray.length != 0) {
                this.setState({
                    lineItemChange: true
                }, () => {
                    this.props.multipleLineItemRequest(lineItemArray)
                }
                )

            }


        }
        document.getElementById("hsn").focus()
    }
    onBlurRate(idd, e) {
        let r = this.state.poRows
        if (e.target.value != "") {
            if (Number(e.target.value) == 0) {

                this.setState({
                    errorMassage: "Rate value can't be less than or equal to Zero(0)",
                    poErrorMsg: true

                })
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == idd) {
                        r[i].rate = ""
                        r[i].finalRate = ""
                    }
                }
                this.setState({
                    poRows: r
                })
            } else {
                if (e.target.value != this.state.rateFocus) {
                    let calculatedMarginValue = ""
                    let finalRate = ""
                    let secTwoRows = this.state.secTwoRows
                    for (let i = 0; i < r.length; i++) {
                        if (r[i].gridOneId == idd) {
                            if (r[i].discount.discountType != "" && this.state.isDiscountAvail) {

                                if (r[i].discount.discountType.includes("%")) {
                                    let value = e.target.value * (r[i].discount.discountValue / 100)
                                    r[i].finalRate = e.target.value - value
                                    finalRate = e.target.value - value

                                } else {
                                    r[i].finalRate = e.target.value - r[i].discount.discountValue
                                    finalRate = e.target.value - r[i].discount.discountValue
                                }
                            } else {
                                r[i].finalRate = e.target.value
                                finalRate = e.target.value

                            }
                        }
                    }
                    for (let i = 0; i < r.length; i++) {
                        if (r[i].gridOneId == idd) {
                            if ((r[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                                let lineItemArray = []

                                for (let j = 0; j < secTwoRows.length; j++) {
                                    if (secTwoRows[j].designRowId == idd && secTwoRows[j].setQty != "" && r[i].finalRate != "" && r[i].finalRate != 0) {
                                        let taxData = {
                                            hsnSacCode: this.state.hsnSacCode,
                                            qty: Number(secTwoRows[j].setQty) * Number(secTwoRows[j].total),
                                            rate: finalRate,
                                            rowId: secTwoRows[j].gridTwoId,
                                            designRowid: Number(idd),
                                            basic: Number(secTwoRows[j].setQty) * Number(secTwoRows[j].total) * Number(finalRate),
                                            clientGstIn: sessionStorage.getItem('gstin'),
                                            piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                            supplierGstinStateCode: this.state.stateCode,
                                            purtermMainCode: this.state.termCode,
                                            siteCode: this.state.siteCode
                                        }
                                        lineItemArray.push(taxData)
                                    }
                                }
                                if (this.state.hsnSacCode != "" || this.state.hsnSacCode != null) {
                                    if (lineItemArray.length != 0) {
                                        this.setState({
                                            lineItemChange: true
                                        }, () => {
                                            this.props.multipleLineItemRequest(lineItemArray)
                                        }
                                        )
                                    }
                                } else {
                                    this.hsn();
                                    this.setState({
                                        errorMassage: "HSN code is complusory",
                                        poErrorMsg: true

                                    })
                                }
                                // let marginlength = r[i].calculatedMargin.length
                                // calculatedMarginValue = Math.round(((r[i].rsp - finalRate) / r[i].rsp) * 100 * 100) / 100

                                // let marginarray = []
                                // for (let i = 0; i < marginlength; i++) {
                                //     marginarray.push(calculatedMarginValue)
                                // }
                                // r[i].calculatedMargin = marginarray
                            }
                        }
                    }
                    // let secTwoRows = this.state.secTwoRows
                    // for (let j = 0; j < secTwoRows.length; j++) {
                    //     if (secTwoRows[j].designRowId == idd) {
                    //         secTwoRows[j].calculatedMargin = calculatedMarginValue
                    //     }
                    // }
                    // for (let i = 0; i < secTwoRows.length; i++) {
                    //     if (secTwoRows[i].designRowId == idd) {
                    //         let secicodes = secTwoRows[i].icodes
                    //         if (secicodes.length != 0) {
                    //             for (let j = 0; j < secicodes.length; j++) {
                    //                 secicodes[j].rate = e.target.value
                    //             }
                    //         }
                    //     }
                    // }

                    this.setState({
                        poRows: r,
                        secTwoRows: secTwoRows
                    })
                }
            }
        } else {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == idd) {

                    r[i].finalRate = ""
                }
            }

            this.setState({
                poRows: r,

            })
        }
    }



    downArrow(e, id, cname) {
        if (e.key == 'ArrowLeft') {
            var c = this.state.secTwoRows
            let nextId = ""
            let index = ""
            for (let i = 0; i < c.length; i++) {
                if (id == c[i].gridTwoId) {
                    for (let j = 0; j < c[i].sizeList.length; j++) {
                        if (c[i].sizeList[j].cname == cname) {
                            index = j
                        }
                    }
                    if (index != 0) {
                        if (index != c[i].sizeList.length) {
                            nextId = c[i].sizeList[index - 1].cname + id

                        }

                    }
                }
            }
            if (nextId != "") {
                document.getElementById(nextId).focus()
            }

        }

        else if (e.key == 'ArrowRight') {
            var c = this.state.secTwoRows
            let nextId = ""
            let index = ""
            for (let i = 0; i < c.length; i++) {
                if (id == c[i].gridTwoId) {
                    for (let j = 0; j < c[i].sizeList.length; j++) {
                        if (c[i].sizeList[j].cname == cname) {
                            index = j
                        }
                    }
                    if (index != c[i].sizeList.length) {
                        nextId = c[i].sizeList[index + 1].cname + id

                    }

                }
            }
            if (nextId != "") {
                document.getElementById(nextId).focus()
            }

        }
        else if (e.key == 'ArrowDown') {
            let idd = Number(id) + 1
            var c = this.state.secTwoRows
            if (id < this.state.secTwoRows.length) {
                let nextId = ""
                let index = ""
                for (let i = 0; i < c.length; i++) {
                    if (idd == c[i].gridTwoId) {
                        for (let j = 0; j < c[i].sizeList.length; j++) {
                            if (c[i].sizeList[j].cname == cname) {
                                index = j
                            }
                        }
                        if (index != c[i].sizeList.length) {
                            nextId = c[i].sizeList[index].cname + idd
                        }

                    }
                }
                if (nextId != "") {
                    document.getElementById(nextId).focus()
                }
            }
        } else if (e.key == 'ArrowUp') {
            if (id > 1) {
                let idd = Number(id) - 1
                var c = this.state.secTwoRows
                let nextId = ""
                let index = ""
                for (let i = 0; i < c.length; i++) {
                    if (idd == c[i].gridTwoId) {
                        for (let j = 0; j < c[i].sizeList.length; j++) {
                            if (c[i].sizeList[j].cname == cname) {
                                index = j
                            }
                        }
                        if (index != c[i].sizeList.length) {
                            nextId = c[i].sizeList[index].cname + idd

                        }

                    }
                }
                if (nextId != "") {
                    document.getElementById(nextId).focus()
                }

            }
        }
    }

    setQtyupDownArrow(e, id) {

        if (e.key == 'ArrowDown') {
            let idd = Number(id) + 1
            var c = this.state.secTwoRows
            if (idd < this.state.secTwoRows.length) {
                let nextId = "setQty" + idd
                document.getElementById(nextId).focus()

            }
        } else if (e.key == 'ArrowUp') {
            if (id > 0) {
                let idd = Number(id) - 1
                var c = this.state.secTwoRows
                let nextId = "setQty" + idd

                document.getElementById(nextId).focus()


            }
        }
    }
    openSiteModal() {
        this.setState({
            siteModal: true,
            siteModalAnimation: !this.state.siteModalAnimation
        })

        let data = {
            type: 1,
            no: 1,
            search: ""
        }
        this.props.procurementSiteRequest(data)

    }
    closeSiteModal() {
        this.setState({
            siteModal: false,
            siteModalAnimation: !this.state.siteModalAnimation
        })

        document.getElementById("siteName").focus()
    }

    updateSite(data) {
        this.setState({
            siteCode: data.siteCode,
            siteName: data.siteName
        })
        document.getElementById("siteName").focus()
    }
    qtyDown(qty, id) {
        if (e.key == 'ArrowDown') {
            let secTwoRows = this.state.secTwoRows
            let index = 0
            for (let i = 0; i < secTwoRows.length; i++) {
                if (secTwoRows[i].gridTwoId == id) {
                    index = i + 1
                }
            }
        } else if (e.key == "ArrowUp") { }
    }
    updateItemDetailValue() {
        this.setState({
            itemDetailValue: ""
        })
    }
    updateUdfSetvalue() {
        this.setState({
            setValue: ""
        })
    }
    updateItemValue() {
        this.setState({
            itemValue: ""
        })
    }
    rateValueOnFocus(e, rowId, type, finalRate) {
        let focusedObj = this.state.focusedObj
        focusedObj.value = e.target.value,
            focusedObj.finalRate = finalRate
        focusedObj.type = type,
            focusedObj.rowId = rowId,
            focusedObj.cname = "",
            focusedObj.radio = this.state.codeRadio,
            focusedObj.colorObj = {
                color: [],
                colorList: []
            }


        this.setState({
            rateFocus: e.target.value,
            focusedObj: focusedObj
        })


    }
    makeListOfIcodes(rowId) {
        let secTwoRows = this.state.secTwoRows
        let index = 1

        for (let j = 0; j < secTwoRows.length; j++) {
            if (secTwoRows[j].gridTwoId == rowId) {
                index = j
            }
        }


        if (secTwoRows[index].colorList.length == 0 || secTwoRows[index].sizeListData.length == 0 || secTwoRows[index].setQty == "") {
            secTwoRows[index].colorSizeList = []
            secTwoRows[index].icodes = []
        } else {
            let poRows = this.state.poRows
            let designId = secTwoRows[index].designRowId
            let rate = ""
            let mrp = ""
            let finalRate = ""
            for (let k = 0; k < poRows.length; k++) {
                if (poRows[k].gridOneId == designId) {
                    rate = poRows[k].rate
                    finalRate = poRows[k].finalRate
                    mrp = poRows[k].mrp
                }
            }


            let color = secTwoRows[index].colorList
            let size = secTwoRows[index].sizeListData

            for (let a = 0; a < color.length; a++) {
                for (let b = 0; b < size.length; b++) {
                    if (!secTwoRows[index].colorSizeList.includes(color[a].cname + "-" + size[b].cname + "-" + size[b].ratio)) {
                        secTwoRows[index].colorSizeList.push(color[a].cname + "-" + size[b].cname + "-" + size[b].ratio)
                        let icodeObj = {
                            designRowId: secTwoRows[index].designRowId,
                            validFrom: this.state.poValidFrom,
                            validTo: this.state.lastInDate,
                            siteId: this.state.siteCode,
                            setRatio: size[b].ratio,
                            articleId: this.state.vendorMrpPo,
                            sizeCode: size[b].code,
                            sizeValue: size[b].cname,
                            colorCode: color[a].code,
                            colorValue: color[a].cname,
                            icode: "",
                            desc1: secTwoRows[index].vendorDesignNo,
                            mrp: mrp,
                            qty: secTwoRows[index].quantity,
                            rate: rate,
                            finalRate: finalRate

                        }

                        secTwoRows[index].icodes = [...secTwoRows[index].icodes, icodeObj]

                    }

                }
            }
            for (let a = 0; a < color.length; a++) {
                for (let b = 0; b < size.length; b++) {
                    let secIcodes = secTwoRows[index].icodes
                    for (let j = 0; j < secTwoRows[index].icodes.length; j++) {
                        if (color[a].cname + "-" + size[b].cname + "-" + size[b].ratio != secTwoRows[index].icodes[j].colorValue + "-" + secTwoRows[index].icodes[j].sizeValue + "-" + secTwoRows[index].icodes[j].setRatio) {
                            secIcodes.slice(j, 1)
                        }
                    }
                }
            }
            for (let a = 0; a < color.length; a++) {
                for (let b = 0; b < size.length; b++) {
                    let colorrList = secTwoRows[index].colorSizeList
                    for (let j = 0; j < secTwoRows[index].colorSizeList.length; j++) {
                        if (color[a].cname + "-" + size[b].cname + "-" + size[b].ratio != secTwoRows[index].colorSizeList[j]) {
                            colorrList.slice(j, 1)
                        }
                    }

                }
            }

        }
        this.setState({
            secTwoRows
        })
        setTimeout(() => {
            this.icodeChkTrue();
        }, 1000)

    }

    onBlurQuantity(idx, qty, total, row, e, targetId) {
        console.log("in blur", idx, qty, total, row, e)
        let id = this.state.desginSectionId
        let section2 = [...this.state.secTwoRows]
        // let data = this.sizeRef.current.state.sizeDateValue.map((_) => _.ratio)
        // section2.map((_) => {
        //     if (_.designRowId == id) {
        //         _.ratio = data
        //     }
        // })
        // this.setState({
        //     secTwoRows: section2
        // })


        // let value = e.target.value
        // setTimeout(()=>{


        // let focusedQty = this.state.focusedQty

        // if(focusedQty.id==idx && focusedQty.qty != value){
        if (this.state.hsnSacCode != "" || this.state.hsnSacCode != null) {
            let rate = ""
            let mrp = ""
            let finalRate = ""
            let poRows = this.state.poRows
            console.log(poRows)
            for (var k = 0; k < poRows.length; k++) {
                // if (poRows[k].gridOneId == row) {
                rate = poRows[k].rate
                finalRate = poRows[k].finalRate
                mrp = poRows[k].vendorMrp
                // }
            }
            if (qty != "") {
                console.log(finalRate != "" || finalRate != 0, finalRate)
                if (finalRate != "" || finalRate != 0) {
                    this.setState({
                        lineItemChange: true
                    })
                    let taxData = {

                        hsnSacCode: this.state.hsnSacCode,
                        qty: Number(qty) * Number(total),
                        rate: finalRate,
                        rowId: idx,
                        designRow: row,
                        mrp: mrp,
                        piDate: new Date(),
                        supplierGstinStateCode: this.state.stateCode,
                        purtermMainCode: this.state.termCode,
                        siteCode: this.state.siteCode
                    }
                    if (e.target.value != "") {

                        this.props.lineItemRequest(taxData)


                    }
                } else {
                    this.setState({
                        errorMassage: "Rate is complusory",
                        poErrorMsg: true

                    })
                }
            }

        } else {
            this.hsn();
            this.setState({
                errorMassage: "HSN code is complusory",
                poErrorMsg: true

            })
        }
        // }
        //  },10)
        // this.state.codeRadio == "poIcode" ? this.makeListOfIcodes(idx) : null
    }
    blurSizeHandle(id, qty, total, row, e) {


        if (e.target.value.toString() != "0") {
            // this.state.codeRadio == "poIcode" ? this.makeListOfIcodes(id) : null
            if (this.state.hsnSacCode != "" || this.state.hsnSacCode != null) {
                let secTwoRows = this.state.secTwoRows
                for (let i = 0; i < secTwoRows.length; i++) {
                    if (secTwoRows[i].gridTwoId == id) {
                        if (secTwoRows[i].setQty != "") {
                            this.setState({
                                lineItemChange: true
                            })
                            setTimeout(() => {
                                let rate = ""
                                let mrp = ""
                                let finalRate = ""
                                let poRows = this.state.poRows
                                for (var k = 0; k < poRows.length; k++) {
                                    if (poRows[k].gridOneId == row) {
                                        rate = poRows[k].rate
                                        mrp = poRows[k].vendorMrp
                                        finalRate = poRows[k].finalRate
                                    }
                                }
                                if (qty != "") {
                                    if (finalRate != 0 || finalRate != "") {
                                        let taxData = {

                                            hsnSacCode: this.state.hsnSacCode,
                                            qty: Number(qty) * Number(total),
                                            rate: finalRate,
                                            rowId: id,
                                            designRow: row,
                                            mrp: mrp,
                                            piDate: new Date(),
                                            supplierGstinStateCode: this.state.stateCode,
                                            purtermMainCode: this.state.termCode,
                                            siteCode: this.state.siteCode
                                        }

                                        this.props.lineItemRequest(taxData)



                                    } else {
                                        this.setState({
                                            errorMassage: "Rate is complusory",
                                            poErrorMsg: true

                                        })
                                    }
                                }
                            }, 1000)
                        }
                    }
                }

            } else {
                this.hsn();
                this.setState({
                    errorMassage: "HSN code is complusory",
                    poErrorMsg: true

                })
            }
        } else {

            this.setState({
                errorMassage: "Ratio value can't be equal to Zero(0)",
                poErrorMsg: true

            })

        }
    }
    blurMrp(id, mrp) {
        // let secTwoRows = this.state.secTwoRows
        // for (let i = 0; i < secTwoRows.length; i++) {
        //     if (secTwoRows[i].designRowId == id) {
        //         let secicodes = secTwoRows[i].icodes
        //         if (secicodes.length != 0) {
        //             for (let j = 0; j < secicodes.length; j++) {
        //                 secicodes[j].mrp = mrp
        //             }
        //         }
        //     }
        // }

        // this.setState({
        //     secTwoRows
        // })

    }

    openPOIcode(secRowId, icodeArray, colorList, sizeListData, setQty) {
        if (colorList.length != 0) {
            if (sizeListData.length != 0) {
                if (setQty != "") {
                    this.setState({
                        openIcodeModal: true,
                        icodeId: secRowId,
                        icodesArray: icodeArray,

                    })
                } else {
                    this.setState({
                        errorMassage: "Set Quantity is mandatory",
                        poErrorMsg: true

                    })
                }
            } else {
                this.setState({
                    errorMassage: "Set-ratio of atleast one size is mandatory",
                    poErrorMsg: true

                })
            }
        } else {
            this.setState({
                errorMassage: "Color is Mandatory",
                poErrorMsg: true

            })
        }
    }
    updateIcode(iarray) {
        let secTwoRows = this.state.secTwoRows
        for (let j = 0; j <= secTwoRows.length; j++) {
            if (secTwoRows[i].gridTwoId == icodeId) {
                secTwoRows[i].icodes = iarray
            }
        }
        this.setState({
            secTwoRows
        })

    }


    closePoIcode() {
        this.setState({
            openIcodeModal: false,


        })
    }

    icodeChkTrue() {
        let secTwoRows = this.state.secTwoRows

        for (let i = 0; i < secTwoRows.length; i++) {

            if (secTwoRows[i].icodes.length != 0) {
                for (let j = 0; j < secTwoRows[i].icodes.length; j++) {
                    if (secTwoRows[i].icodes[j].icode == "") {

                        secTwoRows[i].icodeChk = true
                        break
                    } else {
                        secTwoRows[i].icodeChk = false
                    }
                }
            }

        }

        this.setState({
            secTwoRows
        })

    }
    icodeValidation() {
        let secTwoRows = this.state.secTwoRows
        let flag = false

        for (let i = 0; i < secTwoRows.length; i++) {

            if (secTwoRows[i].icodeChk = true) {
                this.setState({
                    icodeValidation: true
                })
            }
        }
    }
    copingColor(rowId, idd, color, colorList, option, total) {

        let secTwoRows = this.state.secTwoRows
        let udfRow = this.state.udfRows

        for (let i = 0; i < secTwoRows.length; i++) {
            if (secTwoRows[i].gridTwoId == rowId) {
                secTwoRows[i].colorChk = true
            } else {
                let sum = 0

                for (let n = 0; n < secTwoRows[i].sizeList.length; n++) {

                    sum += Number(secTwoRows[i].sizeList[n].ratio);
                }

                secTwoRows[i].colorChk = false
                secTwoRows[i].color = color
                secTwoRows[i].colorList = colorList
                secTwoRows[i].option = option
                secTwoRows[i].total = Number(sum) * Number(option)
                secTwoRows[i].quantity = Number(sum) * Number(option) * Number(secTwoRows[i].setQty)
            }

        }
        for (let j = 0; j < udfRow.length; j++) {
            if (udfRow[j].udfGridId != rowId) {
                udfRow[j].udfColor = color
            }
        }

        this.setState({
            udfRows: udfRow,
            secTwoRows: secTwoRows
        })
        if (this.state.hsnSacCode != "" || this.state.hsnSacCode != null) {
            setTimeout(() => {

                let r = this.state.poRows
                let calculatedMarginValue = ""
                let secTwoRows = this.state.secTwoRows
                for (let k = 0; k < r.length; k++) {
                    if (r[k].gridOneId == idd) {

                        if ((r[k].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {


                            let lineItemArray = []
                            for (let i = 0; i < secTwoRows.length; i++) {

                                if (secTwoRows[i].designRowId == idd && secTwoRows[i].setQty != "" && r[k].finalRate != "" && r[k].finalRate != 0) {

                                    let taxData = {
                                        hsnSacCode: this.state.hsnSacCode,
                                        qty: Number(secTwoRows[i].setQty) * Number(secTwoRows[i].total),
                                        rate: r[k].finalRate,
                                        rowId: secTwoRows[i].gridTwoId,
                                        designRowid: Number(idd),
                                        basic: Number(secTwoRows[i].setQty) * Number(secTwoRows[i].total) * Number(r[k].finalRate),
                                        clientGstIn: sessionStorage.getItem('gstin'),
                                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                        supplierGstinStateCode: this.state.stateCode,
                                        purtermMainCode: this.state.termCode,
                                        siteCode: this.state.siteCode
                                    }

                                    lineItemArray.push(taxData)
                                }
                            }
                            if (lineItemArray.length != 0) {
                                this.setState({
                                    lineItemChange: true
                                }, () => {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                }
                                )
                            }
                            // let marginlength = r[k].calculatedMargin.length
                            // calculatedMarginValue = Math.round(((r[k].rsp - r[k].rate) / r[k].rsp) * 100 * 100) / 100

                            // let marginarray = []
                            // for (let i = 0; i < marginlength; i++) {
                            //     marginarray.push(calculatedMarginValue)
                            // }
                            // r[k].calculatedMargin = marginarray
                        }
                    }
                }
                // let secTwoRows = this.state.secTwoRows
                // for (let j = 0; j < secTwoRows.length; j++) {
                //     if (secTwoRows[j].designRowId == idd) {
                //         secTwoRows[j].calculatedMargin = calculatedMarginValue
                //     }
                // }
                // for (let i = 0; i < secTwoRows.length; i++) {
                //     if (secTwoRows[i].designRowId == idd) {
                //         let secicodes = secTwoRows[i].icodes
                //         if (secicodes.length != 0) {
                //             for (let j = 0; j < secicodes.length; j++) {
                //                 secicodes[j].rate = e.target.value
                //             }
                //         }
                //     }
                // }

                this.setState({
                    poRows: r,
                    secTwoRows: secTwoRows
                })
            }, 1000)
        } else {
            this.hsn();
            this.setState({
                errorMassage: "HSN code is complusory",
                poErrorMsg: true

            })
        }
    }
    openNewIcode(e, id) {

        if (this.state.vendorMrpPo != "") {
            if (this.state.slCode != "") {
                this.setState({
                    icodeModalAnimation: !this.state.icodeModalAnimation,
                    itemBarcodeModal: true,
                })

                let payload = {
                    type: 1,
                    no: 1,
                    search: "",
                    articleCode: this.state.vendorMrpPo,
                    supplierCode: this.state.slCode,
                    siteCode: this.state.siteCode
                }

                this.props.getPoItemCodeRequest(payload)
            } else {
                this.setState({
                    errorMassage: "Supplier code  is mandatory",
                    poErrorMsg: true

                })
            }
        } else {
            this.setState({
                errorMassage: "Article code  is mandatory",
                poErrorMsg: true

            })
        }
    }
    closeNewIcode() {
        this.setState({
            icodeModalAnimation: !this.state.icodeModalAnimation,
            itemBarcodeModal: false,
        })
        document.getElementById("itemCode").focus()
    }
    updateNewIcode(data) {
        this.setState({
            itemCodeList: data
        })
        let payload = {
            validTo: moment(this.state.lastInDate).format("DD-MMM-YYYY"),
            articleCode: this.state.vendorMrpPo,
            items: data
        }
        this.props.poItemBarcodeRequest(payload)
    }
    updatepoRowsWithItemBarcodeGeneric(barcodeData) {
        let poRows = []
        // let details = barcodeData
        // let barcodeItemDetail = data.itemDetail
        let designIdRow = 1;
        let designIdRowcnd = 1
        let designIdRowudf = 1
        let designIdRowSec = 1;
        let designIdRowsetudf = 1
        barcodeData.forEach((details) => {
            let piData = {

                setHeaderId: "",
                color: [],
                size: [],
                ratio: [],
                vendorMrp: details.listedMRP,
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: ""
                },
                rate: details.rate,
                finalRate: details.rate,
                netRate: details.rate,
                basic: "",
                rsp: details.rsp,
                mrp: details.mrp,
                quantity: "",
                amount: "",
                remarks: "",
                otb: "",
                gst: [],
                finCharges: [],
                concatFinCharges: [],
                tax: [],
                calculatedMargin: [],
                gridOneId: designIdRow++,
                deliveryDate: this.state.lastInDate,
                typeOfBuying: "",
                marginRule: this.state.saveMarginRule,
                image: [],
                imageUrl: {},
                containsImage: false,

            }
            poRows.push(piData)
        })

        this.setState({
            poRows
        })

        let udfId = 1;
        let secId = 1;
        let itemId = 1;
        let catDescId = 1;
        let setNo = 1
        let setNum = 1
        let designSec = 1
        let designItem = 1
        let designItemDetail = 1
        let designUdf = 1
        let udfMappingData = this.state.udfMappingData
        let udfRowMapping = this.state.udfRowMapping
        let itemDetailsHeader = this.state.itemDetailsHeader
        let iUdf = []
        let udf = []
        let secRows = []
        let itemD = []

        let catDescRows = this.state.catDescRows
        // let piDetails = data.piDetails
        let designArrayy = []

        barcodeData.forEach((details) => {



            udfRowMapping.forEach(mapData => {
                mapData.value = ""
            })

            udfRowMapping.forEach(list => {

                if (list.cat_desc_udf == "UDFSTRING01") {
                    list.value = details.poudfStrin01
                } else if (list.cat_desc_udf == "UDFSTRING02") {
                    list.value = details.poudfStrin02
                } else if (list.cat_desc_udf == "UDFSTRING03") {
                    list.value = details.poudfStrin03
                }
                else if (list.cat_desc_udf == "UDFSTRING04") {
                    list.value = details.poudfStrin04
                }
                else if (list.cat_desc_udf == "UDFSTRING05") {
                    list.value = details.poudfStrin05
                }
                else if (list.cat_desc_udf == "UDFSTRING06") {
                    list.value = details.poudfStrin06
                }
                else if (list.cat_desc_udf == "UDFSTRING07") {
                    list.value = details.poudfStrin07
                }
                else if (list.cat_desc_udf == "UDFSTRING08") {
                    list.value = details.poudfStrin08
                }
                else if (list.cat_desc_udf == "UDFSTRING09") {
                    list.value = details.poudfStrin09
                }
                else if (list.cat_desc_udf == "UDFSTRING10") {
                    list.value = details.poudfStrin010
                } else if (list.cat_desc_udf == "UDFNUM01") {
                    list.value = details.poudfNum01

                } else if (list.cat_desc_udf == "UDFNUM02") {
                    list.value = details.poudfNum02
                } else if (list.cat_desc_udf == "UDFNUM03") {
                    list.value = details.poudfNum03
                }
                else if (list.cat_desc_udf == "UDFNUM04") {
                    list.value = details.poudfNum04
                }
                else if (list.cat_desc_udf == "UDFNUM05") {
                    list.value = details.poudfNum05
                }
                else if (list.cat_desc_udf == "UDFDATE01") {
                    list.value = details.poudfDate01 == null ? "" : details.poudfDate01
                }
                else if (list.cat_desc_udf == "UDFDATE02") {
                    list.value = details.poudfDate02 == null ? "" : details.poudfDate02
                }
                else if (list.cat_desc_udf == "UDFDATE03") {
                    list.value = details.poudfDate03 == null ? "" : details.poudfDate03
                }
                else if (list.cat_desc_udf == "UDFDATE04") {
                    list.value = details.poudfDate04 == null ? "" : details.poudfDate04
                }
                else if (list.cat_desc_udf == "UDFDATE05") {
                    list.value = details.poudfDate05 == null ? "" : details.poudfDate05
                }
            })

            let udfRowMappings = _.map(
                _.uniq(
                    _.map(udfRowMapping, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );

            let dataiudf = {
                designRowId: designIdRowudf++,
                itemUdfGridId: itemId++,
                vendorDesign: "",
                udfList: udfRowMappings

            }
            iUdf.push(dataiudf)


        })


        barcodeData.forEach((details) => {
            itemDetailsHeader.forEach(mapData => {
                mapData.value = ""
                mapData.code = ""
            })
            itemDetailsHeader.forEach(list => {

                if (list.catdesc == "CAT1") {
                    list.code = details.cCode1
                    list.value = details.cName1

                } else if (list.catdesc == "CAT2") {
                    list.code = details.cCode2
                    list.value = details.cName2
                } else if (list.catdesc == "CAT3") {
                    list.code = details.cCode3
                    list.value = details.cName3
                }
                else if (list.catdesc == "CAT4") {
                    list.code = details.cCode4
                    list.value = details.cName4
                }
                else if (list.catdesc == "CAT5") {
                    list.code = details.cCode5
                    list.value = details.cName5
                }
                else if (list.catdesc == "CAT6") {
                    list.code = details.cCode6
                    list.value = details.cName6
                }
                else if (list.catdesc == "DESC1") {
                    list.code = details.desc1Code
                    list.value = details.desc1
                }
                else if (list.catdesc == "DESC2") {
                    list.code = details.desc2Code
                    list.value = details.desc2
                }
                else if (list.catdesc == "DESC3") {
                    list.code = details.desc3Code
                    list.value = details.desc3
                }
                else if (list.catdesc == "DESC4") {
                    list.code = details.desc4Code
                    list.value = details.desc4
                } else if (list.catdesc == "DESC5") {
                    list.code = details.desc5Code
                    list.value = details.desc5

                } else if (list.catdesc == "DESC6") {
                    list.code = details.desc6Code
                    list.value = details.desc6

                }


            })




            let itemDetailsHeaders = _.map(
                _.uniq(
                    _.map(itemDetailsHeader, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );
            let payload = {
                designRowId: designIdRowcnd++,
                vendorDesign: "",
                gridId: catDescId++,
                itemDetails: itemDetailsHeaders
            }
            itemD.push(payload)
        });
        let sizeMappingArray = []
        barcodeData.forEach(list => {


            let idd = 1

            let obj = {
                orderBy: idd++,
                code: list.sizeCode,
                cname: list.size,
                ratio: ""

            }
            sizeMappingArray.push(obj)

        })
        var obj = {};

        for (var i = 0, len = sizeMappingArray.length; i < len; i++)
            obj[sizeMappingArray[i]['code']] = sizeMappingArray[i];

        let sizeMapping = new Array();
        for (var key in obj)
            sizeMapping.push(obj[key]);

        this.setState({
            sizeMapping
        })
        let sizeMappingrows = _.map(
            _.uniq(
                _.map(sizeMapping, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );

        let secTwoRows = []
        barcodeData.forEach(list => {
            let colorList = []
            let colorObj = {
                id: 1,
                code: list.colorCode,
                cname: list.color
            }
            colorList.push(colorObj)
            let color = []
            color.push(list.color)


            let secData = {
                discount: {
                    discountType: "",
                    discountValue: ""
                },
                colorChk: false,
                icodeChk: false,
                finalRate: list.rate,
                setHeaderId: "",
                itemBarcode: list.itemId,
                designRowId: designIdRowSec++,
                amount: "",
                size: list.size,
                setRatio: "",
                vendorDesignNo: "",
                setQty: 0,
                color: color.join(','),
                colorList: colorList,
                option: color.length,
                deliveryDate: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                otb: "",
                quantity: "",
                rate: list.rate,
                ratio: "",
                mrk: "",
                total: "",
                setNo: setNum++,
                typeOfBuying: "",
                marginRule: this.state.saveMarginRule,

                gst: "",
                finCharges: [],
                tax: "",
                calculatedMargin: "",
                totalQuantity: "",
                totalNetAmt: "",
                totaltax: [],
                totalCalculatedMargin: [],
                totalgst: [],
                totalIntakeMargin: [],
                totalFinCharges: [],
                totalBasic: "",
                totalCalculatedOtb: "",

                gridTwoId: secId++,
                catOneCode: list.cCode1,
                catOneName: list.cName1,
                catTwoCode: list.cCode2,
                catTwoName: list.cName2,
                catThreeCode: list.cCode3,
                catThreeName: list.cName3,
                catFourCode: list.cCode4,
                catFourName: list.cName4,
                catFiveCode: list.cCode5,
                catFiveName: list.cName5,
                catSixCode: list.cCode6,
                catSixName: list.cName6,
                descOneCode: list.desc1Code,
                descOneName: list.desc1,
                descTwoCode: list.desc2Code,
                descTwoName: list.desc2,
                descThreeCode: list.desc3Code,
                descThreeName: list.desc3,
                descFourCode: list.desc4Code,
                descFourName: list.desc4,
                descFiveCode: list.desc5Code,
                descFiveName: list.desc5,
                descSixCode: list.desc6Code,
                descSixName: list.desc6,
                itemudf1: this.state.itemUdfExist == "true" ? list.poudfStrin01 : "",
                itemudf2: this.state.itemUdfExist == "true" ? list.poudfStrin02 : "",
                itemudf3: this.state.itemUdfExist == "true" ? list.poudfStrin03 : "",
                itemudf4: this.state.itemUdfExist == "true" ? list.poudfStrin04 : "",
                itemudf5: this.state.itemUdfExist == "true" ? list.poudfStrin05 : "",
                itemudf6: this.state.itemUdfExist == "true" ? list.poudfStrin06 : "",
                itemudf7: this.state.itemUdfExist == "true" ? list.poudfStrin07 : "",
                itemudf8: this.state.itemUdfExist == "true" ? list.poudfStrin08 : "",
                itemudf9: this.state.itemUdfExist == "true" ? list.poudfStrin09 : "",
                itemudf10: this.state.itemUdfExist == "true" ? list.poudfStrin010 : "",
                itemudf11: this.state.itemUdfExist == "true" ? list.poudfNum01 : "",
                itemudf12: this.state.itemUdfExist == "true" ? list.poudfNum02 : "",
                itemudf13: this.state.itemUdfExist == "true" ? list.poudfNum03 : "",
                itemudf14: this.state.itemUdfExist == "true" ? list.poudfNum04 : "",
                itemudf15: this.state.itemUdfExist == "true" ? list.poudfNum05 : "",
                itemudf16: this.state.itemUdfExist == "true" ? list.poudfDate01 : "",
                itemudf17: this.state.itemUdfExist == "true" ? list.poudfDate02 : "",
                itemudf18: this.state.itemUdfExist == "true" ? list.poudfDate03 : "",
                itemudf19: this.state.itemUdfExist == "true" ? list.poudfDate04 : "",
                itemudf20: this.state.itemUdfExist == "true" ? list.poudfDate05 : "",
                sizeListData: [],
                sizeList: sizeMappingrows,
                lineItemChk: false

            }

            secRows.push(secData)

        })

        barcodeData.forEach(lists => {
            let color = [];
            color.push(lists.color)
            udfMappingData.forEach(mapData => {
                mapData.value = ""
            })

            udfMappingData.forEach(list => {

                if (list.udfType == "SMUDFSTRING01") {
                    list.value = lists.SMUDFSTRING01

                } else if (list.udfType == "SMUDFSTRING02") {
                    list.value = lists.SMUDFSTRING02
                } else if (list.udfType == "SMUDFSTRING03") {
                    list.value = lists.SMUDFSTRING03
                }
                else if (list.udfType == "SMUDFSTRING04") {
                    list.value = lists.SMUDFSTRING04
                }
                else if (list.udfType == "SMUDFSTRING05") {
                    list.value = lists.SMUDFSTRING05
                }
                else if (list.udfType == "SMUDFSTRING06") {
                    list.value = lists.SMUDFSTRING06
                }
                else if (list.udfType == "SMUDFSTRING07") {
                    list.value = lists.SMUDFSTRING07
                }
                else if (list.udfType == "SMUDFSTRING08") {
                    list.value = lists.SMUDFSTRING08
                }
                else if (list.udfType == "SMUDFSTRING09") {
                    list.value = lists.SMUDFSTRING09
                }
                else if (list.udfType == "SMUDFSTRING10") {
                    list.value = lists.SMUDFSTRING10
                } else if (list.udfType == "SMUDFNUM01") {
                    list.value = lists.smUDFNum01

                } else if (list.udfType == "SMUDFNUM02") {
                    list.value = lists.smUDFNum02
                } else if (list.udfType == "SMUDFNUM03") {
                    list.value = lists.smUDFNum03
                }
                else if (list.udfType == "SMUDFNUM04") {
                    list.value = lists.smUDFNum04
                }
                else if (list.udfType == "SMUDFNUM05") {
                    list.value = lists.smUDFNum05
                }
                else if (list.udfType == "SMUDFDATE01") {
                    list.value = lists.smUDFDate01 == null ? "" : lists.smUDFDate01
                }
                else if (list.udfType == "SMUDFDATE02") {
                    list.value = lists.smUDFDate02 == null ? "" : lists.smUDFDate02
                }
                else if (list.udfType == "SMUDFDATE03") {
                    list.value = lists.smUDFDate03 == null ? "" : lists.smUDFDate03
                }
                else if (list.udfType == "SMUDFDATE04") {
                    list.value = lists.smUDFDate04 == null ? "" : lists.smUDFDate04
                }
                else if (list.udfType == "SMUDFDATE05") {
                    list.value = lists.smUDFDate05 == null ? "" : lists.smUDFDate05
                }
            })


            let udfMappingDatas = _.map(
                _.uniq(
                    _.map(udfMappingData, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );

            let dataq = {
                designRowId: designIdRowsetudf++,
                udfColor: color,
                udfGridId: designUdf++,
                setUdfNo: setNo++,
                setUdfList: udfMappingDatas

            }
            udf.push(dataq)
        })

        let itemUdfRows = _.map(
            _.uniq(
                _.map(iUdf, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let udfRowss = _.map(
            _.uniq(
                _.map(udf, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let secRowss = _.map(
            _.uniq(
                _.map(secRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let catDescRowss = _.map(
            _.uniq(
                _.map(itemD, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );

        this.setState({

            udfRows: udfRowss,
            secTwoRows: secRowss,
            catDescRows: catDescRowss,
            itemUdf: itemUdfRows,
            totalOtb: ""

        })


    }
    updatepoRowsWithItemBarcode(data) {

        let poRows = []
        let details = data.catdescudf
        let barcodeItemDetail = data.itemDetail
        let designIdRow = 1;
        let piData = {

            setHeaderId: "",
            color: [],
            size: [],
            ratio: [],
            vendorMrp: details.desc6Name,
            vendorDesign: details.desc1Name,
            mrk: [],
            discount: {
                discountType: "",
                discountValue: ""
            },
            rate: details.rate,
            finalRate: details.rate,
            netRate: details.rate,
            basic: "",
            rsp: details.rsp,
            mrp: details.mrp,
            quantity: "",
            amount: "",
            remarks: "",
            otb: details.otb,
            gst: [],
            finCharges: [],
            concatFinCharges: [],
            tax: [],
            calculatedMargin: [],
            gridOneId: designIdRow,
            deliveryDate: this.state.lastInDate,
            typeOfBuying: "",
            marginRule: this.state.saveMarginRule,
            image: [],
            imageUrl: {},
            containsImage: false,

        }
        poRows.push(piData)
        this.setState({
            poRows
        })

        let udfId = 1;
        let secId = 1;
        let itemId = 1;
        let catDescId = 1;
        let setNo = 1
        let setNum = 1
        let designSec = 1
        let designItem = 1
        let designItemDetail = 1
        let designUdf = 1
        let udfMappingData = this.state.udfMappingData
        let udfRowMapping = this.state.udfRowMapping
        let itemDetailsHeader = this.state.itemDetailsHeader
        let iUdf = []
        let udf = []
        let secRows = []
        let itemD = []

        let catDescRows = this.state.catDescRows
        let piDetails = data.piDetails
        let designArrayy = []



        udfRowMapping.forEach(mapData => {
            mapData.value = ""
        })

        udfRowMapping.forEach(list => {

            if (list.cat_desc_udf == "UDFSTRING01") {
                list.value = details.itemudf1
            } else if (list.cat_desc_udf == "UDFSTRING02") {
                list.value = details.itemudf2
            } else if (list.cat_desc_udf == "UDFSTRING03") {
                list.value = details.itemudf3
            }
            else if (list.cat_desc_udf == "UDFSTRING04") {
                list.value = details.itemudf4
            }
            else if (list.cat_desc_udf == "UDFSTRING05") {
                list.value = details.itemudf5
            }
            else if (list.cat_desc_udf == "UDFSTRING06") {
                list.value = details.itemudf6
            }
            else if (list.cat_desc_udf == "UDFSTRING07") {
                list.value = details.itemudf7
            }
            else if (list.cat_desc_udf == "UDFSTRING08") {
                list.value = details.itemudf8
            }
            else if (list.cat_desc_udf == "UDFSTRING09") {
                list.value = details.itemudf9
            }
            else if (list.cat_desc_udf == "UDFSTRING10") {
                list.value = details.itemudf10
            } else if (list.cat_desc_udf == "UDFNUM01") {
                list.value = details.itemudf11

            } else if (list.cat_desc_udf == "UDFNUM02") {
                list.value = details.itemudf12
            } else if (list.cat_desc_udf == "UDFNUM03") {
                list.value = details.itemudf13
            }
            else if (list.cat_desc_udf == "UDFNUM04") {
                list.value = details.itemudf14
            }
            else if (list.cat_desc_udf == "UDFNUM05") {
                list.value = details.itemudf15
            }
            else if (list.cat_desc_udf == "UDFDATE01") {
                list.value = details.itemudf16 == null ? "" : details.itemudf16
            }
            else if (list.cat_desc_udf == "UDFDATE02") {
                list.value = details.itemudf17 == null ? "" : details.itemudf17
            }
            else if (list.cat_desc_udf == "UDFDATE03") {
                list.value = details.itemudf18 == null ? "" : details.itemudf18
            }
            else if (list.cat_desc_udf == "UDFDATE04") {
                list.value = details.itemudf19 == null ? "" : details.itemudf19
            }
            else if (list.cat_desc_udf == "UDFDATE05") {
                list.value = details.itemudf20 == null ? "" : details.itemudf20
            }
        })

        let udfRowMappings = _.map(
            _.uniq(
                _.map(udfRowMapping, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );

        let dataiudf = {
            designRowId: designIdRow,
            itemUdfGridId: itemId++,
            vendorDesign: details.desc1Name,
            udfList: udfRowMappings

        }
        iUdf.push(dataiudf)






        itemDetailsHeader.forEach(mapData => {
            mapData.value = ""
            mapData.code = ""
        })
        itemDetailsHeader.forEach(list => {

            if (list.catdesc == "CAT1") {
                list.value = details.cat1Name
                list.code = details.cat1Code

            } else if (list.catdesc == "CAT2") {
                list.code = details.cat2Code
                list.value = details.cat2Name
            } else if (list.catdesc == "CAT3") {
                list.code = details.cat3Code
                list.value = details.cat3Name
            }
            else if (list.catdesc == "CAT4") {
                list.code = details.cat4Code
                list.value = details.cat4Name
            }
            else if (list.catdesc == "CAT5") {
                list.code = ""
                list.value = ""
            }
            else if (list.catdesc == "CAT6") {
                list.code = ""
                list.value = ""
            }
            else if (list.catdesc == "DESC1") {
                list.code = ""
                list.value = ""
            }
            else if (list.catdesc == "DESC2") {
                list.code = details.desc2Code
                list.value = details.desc2Name
            }
            else if (list.catdesc == "DESC3") {
                list.code = details.desc3Code
                list.value = details.desc3Name
            }
            else if (list.catdesc == "DESC4") {
                list.code = details.desc4Code
                list.value = details.desc4Name
            } else if (list.catdesc == "DESC5") {
                list.code = details.desc5Code
                list.value = details.desc5Name

            } else if (list.catdesc == "DESC6") {
                list.code = ""
                list.value = ""

            }


        })




        let itemDetailsHeaders = _.map(
            _.uniq(
                _.map(itemDetailsHeader, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let payload = {
            designRowId: designIdRow,
            vendorDesign: details.desc1Name,
            gridId: catDescId++,
            itemDetails: itemDetailsHeaders
        }
        itemD.push(payload)
        let sizeMappingArray = []
        barcodeItemDetail.forEach(list => {


            let idd = 0

            let obj = {
                orderBy: idd++,
                code: list.cat5Code,
                cname: list.cat5Name,
                ratio: ""

            }
            sizeMappingArray.push(obj)

        })
        var obj = {};

        for (var i = 0, len = sizeMappingArray.length; i < len; i++)
            obj[sizeMappingArray[i]['code']] = sizeMappingArray[i];

        let sizeMapping = new Array();
        for (var key in obj)
            sizeMapping.push(obj[key]);
        this.setState({
            sizeMapping
        })
        let sizeMappingrows = _.map(
            _.uniq(
                _.map(sizeMapping, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );

        let secTwoRows = []
        barcodeItemDetail.forEach(list => {
            let colorList = []
            let colorObj = {
                id: 1,
                code: list.cat6Code,
                cname: list.cat6Name
            }
            colorList.push(colorObj)
            let color = []
            color.push(list.cat6Name)


            let secData = {
                discount: {
                    discountType: "",
                    discountValue: ""
                },
                colorChk: false,
                icodeChk: false,
                finalRate: details.finalRate,
                setHeaderId: "",
                itemBarcode: list.itemId,
                designRowId: designIdRow,
                amount: "",
                size: list.cat5Name,
                setRatio: "",
                vendorDesignNo: details.desc1Name,
                setQty: 0,
                color: color.join(','),
                colorList: colorList,
                option: color.length,
                deliveryDate: "",
                image: [],
                imageUrl: {},
                containsImage: false,
                otb: details.otb,
                quantity: "",
                rate: details.rate,
                ratio: "",
                mrk: "",
                total: "",
                setNo: setNum++,
                typeOfBuying: "",
                marginRule: this.state.saveMarginRule,

                gst: "",
                finCharges: [],
                tax: "",
                calculatedMargin: "",
                totalQuantity: "",
                totalNetAmt: "",
                totaltax: [],
                totalCalculatedMargin: [],
                totalgst: [],
                totalIntakeMargin: [],
                totalFinCharges: [],
                totalBasic: "",
                totalCalculatedOtb: "",

                gridTwoId: secId++,
                catOneCode: details.cat1Code,
                catOneName: details.cat1Name,
                catTwoCode: details.cat2Code,
                catTwoName: details.cat2Name,
                catThreeCode: details.cat3Code,
                catThreeName: details.cat3Name,
                catFourCode: details.cat4Code,
                catFourName: details.cat4Name,
                catFiveCode: details.cat5Code,
                catFiveName: details.cat5Name,
                catSixCode: details.cat6Code,
                catSixName: details.cat6Name,
                descOneCode: details.desc1Code,
                descOneName: details.desc1Name,
                descTwoCode: details.desc2Code,
                descTwoName: details.desc2Name,
                descThreeCode: details.desc3Code,
                descThreeName: details.desc3Name,
                descFourCode: details.desc4Code,
                descFourName: details.desc4Name,
                descFiveCode: details.desc5Code,
                descFiveName: details.desc5Name,
                descSixCode: details.desc6Code,
                descSixName: details.desc6Name,
                itemudf1: this.state.itemUdfExist == "true" ? details.itemudf1 : "",
                itemudf2: this.state.itemUdfExist == "true" ? details.itemudf2 : "",
                itemudf3: this.state.itemUdfExist == "true" ? details.itemudf3 : "",
                itemudf4: this.state.itemUdfExist == "true" ? details.itemudf4 : "",
                itemudf5: this.state.itemUdfExist == "true" ? details.itemudf5 : "",
                itemudf6: this.state.itemUdfExist == "true" ? details.itemudf6 : "",
                itemudf7: this.state.itemUdfExist == "true" ? details.itemudf7 : "",
                itemudf8: this.state.itemUdfExist == "true" ? details.itemudf8 : "",
                itemudf9: this.state.itemUdfExist == "true" ? details.itemudf9 : "",
                itemudf10: this.state.itemUdfExist == "true" ? details.itemudf10 : "",
                itemudf11: this.state.itemUdfExist == "true" ? details.itemudf11 : "",
                itemudf12: this.state.itemUdfExist == "true" ? details.itemudf12 : "",
                itemudf13: this.state.itemUdfExist == "true" ? details.itemudf13 : "",
                itemudf14: this.state.itemUdfExist == "true" ? details.itemudf14 : "",
                itemudf15: this.state.itemUdfExist == "true" ? details.itemudf15 : "",
                itemudf16: this.state.itemUdfExist == "true" ? details.itemudf16 : "",
                itemudf17: this.state.itemUdfExist == "true" ? details.itemudf17 : "",
                itemudf18: this.state.itemUdfExist == "true" ? details.itemudf18 : "",
                itemudf19: this.state.itemUdfExist == "true" ? details.itemudf19 : "",
                itemudf20: this.state.itemUdfExist == "true" ? details.itemudf20 : "",
                sizeListData: [],
                sizeList: sizeMappingrows,
                lineItemChk: false

            }

            secRows.push(secData)

        })

        barcodeItemDetail.forEach(lists => {
            let color = [];
            color.push(lists.cat6Name)
            udfMappingData.forEach(mapData => {
                mapData.value = ""
            })

            udfMappingData.forEach(list => {

                if (list.udfType == "SMUDFSTRING01") {
                    list.value = lists.udf1

                } else if (list.udfType == "SMUDFSTRING02") {
                    list.value = lists.udf2
                } else if (list.udfType == "SMUDFSTRING03") {
                    list.value = lists.udf3
                }
                else if (list.udfType == "SMUDFSTRING04") {
                    list.value = lists.udf4
                }
                else if (list.udfType == "SMUDFSTRING05") {
                    list.value = lists.udf5
                }
                else if (list.udfType == "SMUDFSTRING06") {
                    list.value = lists.udf6
                }
                else if (list.udfType == "SMUDFSTRING07") {
                    list.value = lists.udf7
                }
                else if (list.udfType == "SMUDFSTRING08") {
                    list.value = lists.udf8
                }
                else if (list.udfType == "SMUDFSTRING09") {
                    list.value = lists.udf9
                }
                else if (list.udfType == "SMUDFSTRING10") {
                    list.value = lists.udf10
                } else if (list.udfType == "SMUDFNUM01") {
                    list.value = lists.udf11

                } else if (list.udfType == "SMUDFNUM02") {
                    list.value = lists.udf12
                } else if (list.udfType == "SMUDFNUM03") {
                    list.value = lists.udf13
                }
                else if (list.udfType == "SMUDFNUM04") {
                    list.value = lists.udf14
                }
                else if (list.udfType == "SMUDFNUM05") {
                    list.value = lists.udf15
                }
                else if (list.udfType == "SMUDFDATE01") {
                    list.value = lists.udf16 == null ? "" : lists.udf16
                }
                else if (list.udfType == "SMUDFDATE02") {
                    list.value = lists.udf17 == null ? "" : lists.udf17
                }
                else if (list.udfType == "SMUDFDATE03") {
                    list.value = lists.udf18 == null ? "" : lists.udf18
                }
                else if (list.udfType == "SMUDFDATE04") {
                    list.value = lists.udf19 == null ? "" : lists.udf19
                }
                else if (list.udfType == "SMUDFDATE05") {
                    list.value = lists.udf20 == null ? "" : lists.udf20
                }
            })


            let udfMappingDatas = _.map(
                _.uniq(
                    _.map(udfMappingData, function (obj) {

                        return JSON.stringify(obj);
                    })
                ), function (obj) {
                    return JSON.parse(obj);
                }
            );

            let dataq = {
                designRowId: designIdRow,
                udfColor: color,
                udfGridId: designUdf++,
                setUdfNo: setNo++,
                setUdfList: udfMappingDatas

            }
            udf.push(dataq)
        })

        let itemUdfRows = _.map(
            _.uniq(
                _.map(iUdf, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let udfRowss = _.map(
            _.uniq(
                _.map(udf, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let secRowss = _.map(
            _.uniq(
                _.map(secRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let catDescRowss = _.map(
            _.uniq(
                _.map(itemD, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );

        this.setState({

            udfRows: udfRowss,
            secTwoRows: secRowss,
            catDescRows: catDescRowss,
            itemUdf: itemUdfRows,
            totalOtb: details.otb

        })



    }

    resetBarcodeRows() {
        let udfMappingData = this.state.udfMappingData
        let udfRowMapping = this.state.udfRowMapping
        let itemDetailsHeader = this.state.itemDetailsHeader

        udfMappingData.forEach(mapData => {
            mapData.value = ""
        })
        itemDetailsHeader.forEach(mapData => {
            mapData.value = ""
            mapData.code = ""
        })
        udfRowMapping.forEach(mapData => {
            mapData.value = ""
        })
        this.setState({
            udfRows: [{
                designRowId: 1,
                setUdfNo: 1,
                udfColor: [],
                udfGridId: 1,
                setUdfList: udfMappingData
            }],
            catDescRows: [{
                vendorDesign: "",
                gridId: 1,
                designRowId: 1,
                itemDetails: itemDetailsHeader
            }],
            itemUdf: [{
                itemUdfGridId: 1,
                vendorDesign: "",
                designRowId: 1,
                udfList: udfRowMapping

            }],
            secTwoRows: [{
                discount: {
                    discountType: "",
                    discountValue: ""
                },
                finalRate: "",

                colorChk: false,
                icodeChk: false,
                colorSizeList: [],
                icodes: [],
                itemBarcode: "",
                size: "",
                setRatio: "",

                setHeaderId: "",
                designRowId: 1,
                color: this.state.isColorRequired ? [] : ["NA"],
                colorList: this.state.isColorRequired ? [] : [{ id: 1, code: "", cname: "NA" }],
                sizeListData: [],
                sizeList: [],
                vendorDesignNo: "",
                option: "",
                setNo: 1,
                total: "",
                setQty: "",
                quantity: "",
                amount: "",
                rate: "",
                ratio: [],
                gst: "",
                finCharges: [],
                tax: "",
                calculatedMargin: "",
                mrk: "",
                otb: "",
                totalQuantity: "",
                totalNetAmt: "",
                totaltax: [],
                totalCalculatedMargin: [],
                totalgst: [],
                totalIntakeMargin: [],
                totalFinCharges: [],
                totalBasic: "",
                totalCalculatedOtb: "",
                gridTwoId: 1,
                catOneCode: "",
                catOneName: "",
                catTwoCode: "",
                catTwoName: "",
                catThreeCode: "",
                catThreeName: "",
                catFourCode: "",
                catFourName: "",
                catFiveCode: "",
                catFiveName: "",
                catSixCode: "",
                catSixName: "",
                descOneCode: "",
                descOneName: "",


                descTwoCode: "",
                descTwoName: "",
                descThreeCode: "",
                descThreeName: "",
                descFourCode: "",
                descFourName: "",
                descFiveCode: "",
                descFiveName: "",
                descSixCode: "",
                descSixName: "",
                itemudf1: "",
                itemudf2: "",
                itemudf3: "",
                itemudf4: "",
                itemudf5: "",
                itemudf6: "",
                itemudf7: "",
                itemudf8: "",
                itemudf9: "",
                itemudf10: "",
                itemudf11: "",
                itemudf12: "",
                itemudf13: "",
                itemudf14: "",
                itemudf15: "",
                itemudf16: "",
                itemudf17: "",
                itemudf18: "",
                itemudf19: "",
                itemudf20: "",
                deliveryDate: "",
                typeOfBuying: "",
                marginRule: this.state.saveMarginRule,
                image: [],
                imageUrl: {},
                containsImage: false,
                lineItemChk: false
            }],
        })

    }

    // ___________________________UPLOAD PO _____________________________

    changeFile(e) {
        let values = e.target.files

        if (values[0].type == "application/vnd.ms-excel" || values[0].type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            if (values[0].size < 52428800) {
                this.setState({
                    fileData: values[0],
                    fileName: values[0].name
                })
            }
        }
    }

    uploadPo(e) {
        const formData = new FormData();
        this.setState({
            generateTemplate: true
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }
        formData.append('type', "FPO");
        formData.append('fileData', this.state.fileData);

        axios.post(`${CONFIG.BASE_URL}${CONFIG.PO}/upload/data`, formData, { headers: headers })
            .then(res => {
                if (res.data.status == "2000") {
                    this.props.errorUpload(res)
                    this.setState({
                        generateTemplate: false,
                        fileName: ""
                    })
                } else if (res.data.status == "4000") {
                    this.props.errorUpload(res)
                    this.setState({
                        alert: true,
                    })
                }
            }).catch((error) => {
                this.setState({
                    generateTemplate: false,
                })
            })
    }
    // OnFocusSetQty(e, rowId, type) {

    //     let focusedObj = this.state.focusedObj
    //     focusedObj.value = e.target.value
    //     focusedObj.rowId = rowId
    //     focusedObj.type = type
    //     focusedObj.cname = ""
    //     focusedObj.radio = this.state.codeRadio
    //     focusedObj.colorObj = {
    //         color: [],
    //         colorList: []
    //     }

    //     this.setState({
    //         focusedObj: focusedObj
    //     })


    // }
    // onSetRatio(e, rowId, type, cname) {
    //     let focusedObj = this.state.focusedObj
    //     focusedObj.value = e.target.value
    //     focusedObj.type = type
    //     focusedObj.rowId = rowId
    //     focusedObj.cname = cname
    //     focusedObj.radio = this.state.codeRadio
    //     focusedObj.colorObj = {
    //         color: [],
    //         colorList: []
    //     }

    //     this.setState({
    //         focusedObj
    //     })


    // }

    copingFocus(rowId, idd, color, colorList, option, total) {
        let secTwoRows = this.state.secTwoRows
        this.setState({
            copingLineItem: [...secTwoRows]
        })
        let focusedObj = this.state.focusedObj
        focusedObj.value = ""
        focusedObj.finalRate = ""
        focusedObj.type = "copying color"
        focusedObj.rowId = ""
        focusedObj.cname = ""
        focusedObj.radio = this.state.codeRadio
        focusedObj.colorObj = {
            color: [],
            colorList: []
        }

        this.setState({
            focusedObj
        })
        setTimeout(() => {
            this.copingColor(rowId, idd, color, colorList, option, total)
        }, 10)

    }
    focusQty(e, id) {
        let focusedQty = this.state.focusedQty

        focusedQty.id = id
        focusedQty.qty = e.target.value
        this.setState({
            focusedQty: focusedQty
        })

    }
    closeMultipleError() {
        this.setState({
            multipleErrorpo: false
        })

        document.onkeydown = function (t) {

            return true;

        }
    }

    multiMarkUp(designId) {
        console.log("designId", designId)
        let poRows = this.state.poRows
        let secTwoRows = this.state.secTwoRows
        let multiMarkUp = []
        for (let i = 0; i < poRows.length; i++) {
            console.log("inside for", "poRows[i].gridOneId", poRows[i].gridOneId, "designId", designId)
            if (poRows[i].gridOneId == designId) {
                console.log("inside if")

                for (let j = 0; j < secTwoRows.length; j++) {
                    if (secTwoRows[j].designRowId == designId && secTwoRows[j].setQty != "") {
                        let payload = {
                            rate: poRows[i].finalRate,
                            rsp: this.state.isRSP ? poRows[i].rsp : poRows[i].mrp,
                            piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
                            hsnSacCode: this.state.hsnSacCode,
                            designRowid: designId,
                            rowId: secTwoRows[j].gridTwoId,
                            //gst: secTwoRows[j].gst
                        }

                        multiMarkUp.push(payload)

                    }

                }
            }
        }
        this.props.getMultipleMarginRequest(multiMarkUp)


    }
    openCityModal() {
        if (this.state.vendorMrpPo != "") {
            this.setState({
                cityModal: true,
                cityModalAnimation: !this.state.cityModalAnimation
            })

            let data = {
                type: 1,
                no: 1,
                search: ""
            }
            this.props.getAllCityRequest(data)
        } else {
            this.setState({
                errorId: id,
                errorMassage: "Select Article",
                poErrorMsg: true
            })
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeCity() {
        this.setState({
            cityModal: false,
            cityModalAnimation: !this.state.cityModalAnimation
        })
    }
    updateCity(data) {
        this.setState({
            city: data.city,
            supplier: "",
            stateCode: "",
            slCode: "",
            slName: "",
            slAddr: "",
            transporterCode: "",
            transporterName: "",
            leadDays: "",
            transporter: "",
            tradeGrpCode: "",
            term: "",
            termCode: "",
            termName: "",
            gstInNo: "",
            slCityName: "",
            poAmount: 0,
            poQuantity: 0,
            supplierCode: ""
        }, () => {
            this.resetPoRows();
            this.resetsecRows();

        })


    }
    openDiscountModal(discount, grid, idx) {


        let poRows = this.state.poRows
        let mrp = ""
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == grid) {
                mrp = poRows[i].vendorMrp
            }

        }
        if (mrp != "" || !this.state.isMrpRequired) {
            this.setState({
                selectedDiscount: discount,
                discountGrid: grid,
                itemCodeId: idx,
                discountModal: true,
                discountMrp: mrp
            })
            if (this.state.isDiscountMap) {
                let payload = {
                    articleCode: this.state.vendorMrpPo,
                    mrp: this.state.isMrpRequired ? mrp : 0,
                    discountType: "percentage"
                }
                this.props.discountRequest(payload)
            }
        } else {
            this.setState({
                errorId: idx,
                errorMassage: "Mrp is compulsory",
                poErrorMsg: true
            })
        }
    }
    closeDiscountModal() {
        document.getElementById(this.state.itemCodeId).focus()

        this.setState({
            discountModal: false
        })
    }
    updateDiscountModal(discountData) {
        let poRows = this.state.poRows
        let value = ""
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].gridOneId == discountData.discountGrid) {

                if (discountData.discount.discountType.includes("%")) {
                    value = poRows[j].rate * discountData.discount.discountValue / 100

                    if (Number(poRows[j].rate - value) < 0) {
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true
                        })

                    } else {
                        poRows[j].finalRate = poRows[j].rate - value
                        poRows[j].discount = discountData.discount
                    }

                } else {
                    value = poRows[j].rate - discountData.discount.discountValue
                    if (value < 0) {
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true
                        })

                    } else {
                        poRows[j].finalRate = value
                        poRows[j].discount = discountData.discount
                    }

                }
            }
        }
        this.setState({
            poRows
        }, () => {
            let calculatedMarginValue = ""
            let finalRate = ""
            let r = this.state.poRows
            let secTwoRows = this.state.secTwoRows
            if (this.state.hsnSacCode != "" || this.state.hsnSacCode != null) {
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == discountData.discountGrid) {
                        if ((r[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                            let lineItemArray = []

                            for (let j = 0; j < secTwoRows.length; j++) {

                                if (secTwoRows[j].designRowId == discountData.discountGrid && secTwoRows[j].setQty != "" && r[i].finalRate != "" && r[i].finalRate != 0) {
                                    let taxData = {
                                        hsnSacCode: this.state.hsnSacCode,
                                        qty: Number(secTwoRows[j].setQty) * Number(secTwoRows[j].total),
                                        rate: r[i].finalRate,
                                        rowId: secTwoRows[j].gridTwoId,
                                        designRowid: Number(discountData.discountGrid),
                                        basic: Number(secTwoRows[j].setQty) * Number(secTwoRows[j].total) * Number(r[i].finalRate),
                                        clientGstIn: sessionStorage.getItem('gstin'),
                                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                        supplierGstinStateCode: this.state.stateCode,
                                        purtermMainCode: this.state.termCode,
                                        siteCode: this.state.siteCode
                                    }
                                    lineItemArray.push(taxData)
                                }
                            }
                            if (lineItemArray.length != 0) {
                                this.setState({
                                    lineItemChange: true
                                }, () => {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                }
                                )
                            }
                            // let marginlength = r[i].calculatedMargin.length
                            // calculatedMarginValue = Math.round(((r[i].rsp - r[i].finalRate) / r[i].rsp) * 100 * 100) / 100

                            // let marginarray = []
                            // for (let i = 0; i < marginlength; i++) {
                            //     marginarray.push(calculatedMarginValue)
                            // }
                            // r[i].calculatedMargin = marginarray
                        }
                    }
                }
                // let secTwoRows = this.state.secTwoRows
                // for (let j = 0; j < secTwoRows.length; j++) {
                //     if (secTwoRows[j].designRowId == idd) {
                //         secTwoRows[j].calculatedMargin = calculatedMarginValue
                //     }
                // }
                // for (let i = 0; i < secTwoRows.length; i++) {
                //     if (secTwoRows[i].designRowId == idd) {
                //         let secicodes = secTwoRows[i].icodes
                //         if (secicodes.length != 0) {
                //             for (let j = 0; j < secicodes.length; j++) {
                //                 secicodes[j].rate = e.target.value
                //             }
                //         }
                //     }
                // }

                this.setState({
                    poRows: r,
                    secTwoRows: secTwoRows
                })
            } else {
                this.hsn();
                this.setState({
                    errorMassage: "HSN code is complusory",
                    poErrorMsg: true

                })
            }
        })
    }
    handleSizeModel = () => {
        console.log("in size modal")
        this.setState({ sizeModalStatus: !this.state.sizeModalStatus })
    }

    openSizeModal = (e) => {
        // let section2 = [...this.state.secTwoRows]
        // let id = e.target.id, allSizes = []
        // let sizeState = [...this.state.sizeState]
        // let index = ""
        // console.log(this.state.sizeValue, this.state.sizeState, section2)
        // // let sizeState = [...this.state.sizeState]
        // console.log(e.target.id, this.state.sizeValue, this.state.sizeState)
        // section2.map((_) => {
        //     if (_.designRowId == id) {
        //         allSizes = _.size
        //         _.sizeListData.map((__) => {
        //             index = sizeState.findIndex((obj) => obj.code == __.code)
        //             if (__.code == sizeState[index].code) {
        //                 sizeState[index].ratio = __.ratio
        //             }
        //         })
        //     }
        // })
        // console.log("allSizes", allSizes, sizeState)
        // this.setState({
        //     sizeValue: allSizes
        // })
        // if (this.state.vendorMrpPo == "") {

        //     this.setState({
        //         errorMassage: "Select Article Code",
        //         poErrorMsg: true
        //     })

        // }
        // else {

        var dataColor = {
            no: 1,
            type: 1,
            search: "",
            hl3Name: this.state.department,
            hl1Name: this.state.divisionName,
            hl2Name: this.state.sectionName,
            hl3Code: this.state.departmentCode,
            hl4Name: this.state.articleName,
            itemType: 'size',
            isToggled: true
        }

        // var dataColor = {
        //     code: this.state.vendorMrpPo,
        //     type: "",
        //     no: 1,
        //     search: ""
        // }
        this.props.sizeRequest(dataColor)
        this.setState({
            sizeCode: this.state.vendorMrpPo,
            sizeModal: true,
            sizeModalAnimatiion: !this.state.sizeModalAnimatiion,
            desginSectionId: e.target.id,
            // sizeState
        });

        // }
        // document.onkeydown = function (t) {

        //     if (t.which == 9) {
        //         return false;
        //     }
        // }
    }

    closePiSizeModal() {
        console.log("in close pi size", this.sizeRef.current.state.sizeDateValue)
        // [...this.state.poRows]
        let id = this.state.desginSectionId
        let section2 = [...this.state.secTwoRows]
        console.log(this.sizeRef.current.state.sizeValue)
        let data = this.sizeRef.current.state.sizeDateValue.map((_) => _.ratio)
        console.log(id, section2)
        section2.map((_) => {
            if (_.designRowId == id) {
                _.ratio = data
                _.total = Number(_.option) * Number(data.reduce((curr, next) => Number(curr) + Number(next))),
                    _.size = this.sizeRef.current.state.sizeValue
            }
        })
        console.log("after", section2)
        console.log(data)
        this.setState({
            sizeModal: false,
            sizeModalAnimatiion: !this.state.sizeModalAnimatiion,
            sizeRef: this.sizeRef.current,
            secTwoRows: section2
        });

        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }

    render() {
        console.log(this.props.purchaseIndent, this.state.secTwoRows)
        const { city, cityerr, itemcodeerr, itemCodeList, siteNameerr, siteName, hsnSacCode, hsnerr, poDate, loadIndent, loadIndenterr, poValidFrom, poValidFromerr, lastInDate, lastInDateerr, articleerr, division, department, section,
            supplier, vendorerr, leadDays, leadDayserr, transporter, transportererr, startRange, endRange, vendorMrpPo, term, articleName, departmentSeterr, poSetVendorerr, setDepartment, poSetVendor, orderSet, orderSeterr } = this.state;
        return (
            <div className="container_div pors-pg" id="" >
                <form name="siteForm" className="organizationForm">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                        <div className="purchasedOrderContainer poFormMain">
                            <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                                <div className="col-lg-6 col-md-4 col-sm-12 pad-0">
                                    <ul className="list_style">
                                        <li><label className="contribution_mart">PURCHASE ORDER</label></li>
                                        <li><p className="master_para">Create purchase order</p></li>
                                    </ul>
                                </div>
                                <div className="col-lg-6 col-md-8 col-sm-12 text-right">
                                    <ul className=" pad-right-20 pad-right-0 width_100 list-inline text-right purchaseList m-top-10 m-top-0 ">
                                        {this.state.isAdhoc ? <li>
                                            <label className="containerPurchaseRadio displayPointer">ADHOC
                                            <input autoComplete="off" type="radio" id="Adhoc" name="codeRadio" checked={this.state.codeRadio === "Adhoc"} value="Adhoc" onChange={e => this.handleRadioChange("Adhoc")} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </li> : null}
                                        {this.state.isIndent ? <li className="pad-0">
                                            <label className="containerPurchaseRadio displayPointer">INDENT
                                            <input autoComplete="off" type="radio" id="loadIndent" checked={this.state.codeRadio === "raisedIndent"} name="codeRadio" value="raisedIndent" onChange={e => this.handleRadioChange("raisedIndent")} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </li> : null}

                                        {this.state.isSetBased ? <li>
                                            <label className="containerPurchaseRadio displayPointer">SET BASED
                                            <input autoComplete="off" type="radio" id="setBased" name="codeRadio" checked={this.state.codeRadio === "setBased"} value="setBased" onChange={e => this.handleRadioChange("setBased")} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </li> : null}
                                        {this.state.isHoldPo ? <li>
                                            <label className="containerPurchaseRadio displayPointer">HOLD PO
                                            <input autoComplete="off" type="radio" id="holdPo" name="codeRadio" checked={this.state.codeRadio === "holdPo"} value="holdPo" onChange={e => this.handleRadioChange("holdPo")} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </li> : null}
                                        {this.state.isPOwithICode ? <li className={this.state.isSetBased ? "pad-lft-20" : ""}>
                                            <label className="containerPurchaseRadio displayPointer">ICODE
                                            <input autoComplete="off" type="radio" id="poIcode" name="codeRadio" checked={this.state.codeRadio === "poIcode"} value="poIcode" onChange={e => this.handleRadioChange("poIcode")} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </li> : null}

                                    </ul>

                                </div>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 mo-change pad-0">
                                <div className="col-lg-9 col-md-7 col-sm-12 pr-order-2 pad-0 m-top-5">
                                    <div className="col-lg-12 col-md-12 col-sm-12  pad-0">
                                        <div className="col-lg-3 col-md-6 col-sm-6 pad-0 piFormDiv">
                                            <label className="purchaseLabel">
                                                PO Date<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" type="text" className="purchaseOrderTextbox" readOnly id="poDate" name="poDate" value={poDate} onKeyDown={(e) => this._handleKeyPressDate(e, "poDate")} placeholder="Choose PO Date" disabled />
                                        </div>
                                        {this.state.codeRadio == "raisedIndent" ? <div className="col-lg-3 col-md-3 col-sm-4 pad-0 piFormDiv">
                                            <label className="purchaseLabel">
                                                Load Indent<span className="mandatory">*</span>
                                            </label>


                                            <div className={loadIndenterr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >

                                                <input autoComplete="off" type="text" id="loadIndentInput" className="inputTextKeyFuc onFocus" value={loadIndent} onKeyDown={(e) => this._handleKeyPress(e, "loadIndentInput")} placeholder="Select Load Indent" onClick={(e) => this.openloadIndentSelection(e)} />

                                                <span className="modalBlueBtn" onClick={(e) => this.openloadIndentSelection(e)}>
                                                    ▾
                                            </span>


                                            </div>
                                            {loadIndenterr ? (
                                                <span className="error">
                                                    Select Load Indent
                                                </span>
                                            ) : null}

                                        </div> : null}
                                        <div className="col-lg-3 col-md-5 col-sm-5 pad-0 piFormDiv">
                                            <label className="purchaseLabel">
                                                PO Valid From<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" type="date" min={this.state.currentDate} className={poValidFromerr ? "errorBorder purchaseOrderTextbox" : "purchaseOrderTextbox onFocus"} id="poValidFrom" name="poValidFrom" value={poValidFrom} onKeyDown={(e) => this._handleKeyPressDate(e, "poValidFrom")} onChange={(e) => this.dateVali(e)} placeholder="" />
                                            {poValidFromerr ? (
                                                <span className="error">
                                                    Enter PO Valid From
                                                </span>
                                            ) : null}
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-15">

                                        <div className="col-lg-3 col-md-6 col-sm-6 pad-0 piFormDiv">
                                            <label className="purchaseLabel">
                                                {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? "Last In Date" : "PO Valid To"}<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" type="date" className={lastInDateerr ? "errorBorder purchaseOrderTextbox" : "purchaseOrderTextbox onFocus"} min={this.state.poValidFrom} id="lastInDate" name="lastInDate" value={lastInDate} onChange={(e) => this.handleInputChange(e)} placeholder="Last In Date" />

                                            {lastInDateerr ? (
                                                <span className="error">
                                                    {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? " Select Last In Date" : "Select PO Valid To"}


                                                </span>
                                            ) : null}

                                        </div>
                                        {/* <div className="col-md-3 col-sm-4 pad-0 piFormDiv">
                                                    <label className="purchaseLabel">
                                                        Last ASN Date
                                </label>
                                                    <input autoComplete="off" type="text" className=" purchaseOrderTextbox" id="lastAsnDate" name="lastAsnDate" value={lastAsnDate} onChange={e => this.handleInputChange(e)} placeholder=" Last ASN Date" disabled />


                                                </div> */}

                                        {this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" ?

                                            <div className="col-lg-3 col-md-3 col-sm-4 pad-0 piFormDiv">
                                                <label className="purchaseLabel">
                                                    Department<span className="mandatory">*</span>
                                                </label>
                                                <div className={departmentSeterr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                                    <input autoComplete="off" readOnly type="text" id="setDepartment" className="inputTextKeyFuc onFocus" value={setDepartment} onKeyDown={(e) => this._handleKeyPress(e, "setDepartment")} placeholder="Select Department" onClick={(e) => this.departmentSetModal(e)} />
                                                    <span className="modalBlueBtn" onClick={(e) => this.departmentSetModal(e)}>
                                                        ▾
                                                    </span>
                                                </div>
                                                {departmentSeterr ? (
                                                    <span className="error">
                                                        Select Department
                                                    </span>) : null}
                                            </div> : null}
                                        {this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" ? <div className="col-md-3 col-sm-4 pad-0 piFormDiv">
                                            <label className="purchaseLabel">
                                                Vendor<span className="mandatory">*</span>
                                            </label>
                                            <div className={poSetVendorerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                                <input autoComplete="off" type="text" readOnly id="poSetVendor" className="inputTextKeyFuc onFocus" value={poSetVendor} onKeyDown={(e) => this._handleKeyPress(e, "poSetVendor")} placeholder="Select Vendor" onClick={(e) => this.setVendorModalOpen(e, "poSetVendor")} />
                                                <span className="modalBlueBtn" onClick={(e) => this.setVendorModalOpen(e, "poSetVendor")}>
                                                    ▾
                                            </span>
                                            </div>
                                            {poSetVendorerr ? (
                                                <span className="error">
                                                    Select Po Number
                                                </span>
                                            ) : null}
                                        </div> : null}
                                        {this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" ? <div className="col-md-3 col-sm-4 pad-0 piFormDiv" >
                                            <label className="purchaseLabel">
                                                Order No.<span className="mandatory">*</span>
                                            </label>
                                            <div className={orderSeterr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                                <input autoComplete="off" readOnly type="text" id="orderSet" className="inputTextKeyFuc onFocus" value={orderSet} onClick={(e) => this.onSetOrderNumber(e, "orderSet")} onKeyDown={(e) => this._handleKeyPress(e, "orderSet")} placeholder="Select Order No." />
                                                <span className="modalBlueBtn" onClick={(e) => this.onSetOrderNumber(e, "orderSet")}>
                                                    ▾
                                                </span>
                                            </div>
                                            {orderSeterr ? (
                                                <span className="error">
                                                    Select Order No.
                                                </span>) : null}
                                        </div> : null}
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-5 col-sm-12 pr-order-1 pad-0">
                                    <div className="poAmtTotal">
                                        <ul className="pad-0">
                                            <li>
                                                <label>PO Quantity</label>
                                                <span>
                                                    {this.state.poQuantity}
                                                </span>
                                            </li>
                                            <li>
                                                <label> PO Amount </label>
                                                <span>{Math.round(this.state.poAmount * 100) / 100} </span>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                        <div className="purchasedOrderContainer1">
                            <div className="StickyDiv1 overflowVisible">
                                <div className="col-lg-12 col-md-12 col-sm-12 validateItemdesc pad-0">
                                    <div className="col-lg-12 col-md-12 col-sm-8 pad-0">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    SECTION 1
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                                        <div className="col-lg-2 col-md-2 col-sm-6 m-b-15 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Article<span className="mandatory">*</span>
                                            </label>
                                            <div className={articleerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >

                                                <input autoComplete="off" readOnly type="text" id="article" className="inputTextKeyFuc onFocus" value={vendorMrpPo} onKeyDown={(e) => this._handleKeyPress(e, "article")} placeholder="Choose article code" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio === "poIcode" ? (e) => this.openMrpModal(e) : null} />

                                                {this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent" || this.state.codeRadio === "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openMrpModal(e)}>
                                                    ▾
                                            </span> : null}
                                            </div>

                                            {articleerr ? (
                                                <span className="error">
                                                    Select Article
                                                </span>
                                            ) : null}

                                        </div>
                                        <div className="col-lg-2 col-md-2 col-sm-6 m-b-15 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Division<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" type="text" readOnly className="purchaseOrderSelectBox" id="division" value={division} placeholder="" disabled />

                                        </div>
                                        <div className="col-lg-2 col-md-2 col-sm-6 m-b-15 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Section<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" readOnly type="text" className="purchaseOrderSelectBox" id="section" value={section} placeholder="" disabled />

                                        </div>
                                        <div className="col-lg-2 col-md-2 col-sm-6 m-b-15 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Department<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" readOnly type="text" className="purchaseOrderSelectBox" id="department" value={department} placeholder="" disabled />

                                        </div>

                                        <div className="col-lg-2 col-md-2 col-sm-6 m-b-15 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Article Name<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" readOnly type="text" className="purchaseOrderSelectBox" id="articleName" value={articleName} placeholder="" disabled />

                                        </div>
                                    </div>

                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-20 m-top-0">

                                        {sessionStorage.getItem("partnerEnterpriseName") == "VMART" || this.state.isSiteExist == "true" ? <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Site<span className="mandatory">*</span>
                                            </label>

                                            <div className={siteNameerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                                <div className="topToolTip toolTipSupplier width100">
                                                    <input autoComplete="off" type="text" readOnly className="inputTextKeyFuc" onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openSiteModal(e) : null} />
                                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openSiteModal(e)}>
                                                        ▾
                                                    </span> : null}
                                                    {siteName != "" ? <span className="topToolTipText topAuto">{siteName}</span> : null}

                                                </div>
                                            </div>
                                            {siteNameerr ? (
                                                <span className="error">
                                                    Enter Site
                                                </span>
                                            ) : null}
                                        </div> : null}
                                        {this.state.isCityExist == true ? <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="purchaseLabel">
                                                City<span className="mandatory">*</span>
                                            </label>

                                            <div className={cityerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                                <div className="topToolTip toolTipSupplier width100">
                                                    <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc m-b-10" onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={city} placeholder="Choose City" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openCityModal(e) : null} />
                                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openCityModal(e)}>
                                                        ▾
                                                </span> : null}
                                                    {city !== "" && <span className="topToolTipText topAuto">{city}</span>}
                                                </div>

                                            </div>
                                            {cityerr ? (
                                                <span className="error">
                                                    Enter City
                                                </span>
                                            ) : null}
                                        </div> : null}
                                        <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Vendor<span className="mandatory">*</span>
                                            </label>
                                            <div className={vendorerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"}  >
                                                <div className="topToolTip toolTipSupplier width100">
                                                    <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc m-b-10 onFocus" onKeyDown={(e) => this._handleKeyPress(e, "vendor")} id="vendor" value={supplier} placeholder="Choose Vendor" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openSupplier(e, "vendor") : null} />
                                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openSupplier(e, "vendor")}>
                                                        ▾
                                                </span> : null}
                                                    {supplier != "" ? <span className="topToolTipText topAuto">{supplier}</span> : null}
                                                </div>
                                            </div>

                                            {vendorerr ? (
                                                <span className="error">
                                                    Enter Vendor
                                                </span>
                                            ) : null}
                                        </div>
                                        <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Lead Days<span className="mandatory">*</span>
                                            </label>

                                            <input autoComplete="off" readOnly type="text" value={leadDays} id="leadDays" onChange={(e) => this.onChange(e)} className={leadDayserr ? "m-b-10 errorBorder purchaseTextbox" : "purchaseTextbox"} disabled />

                                        </div>
                                        <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Transporter<span className="mandatory">*</span>
                                            </label>
                                            <div className={transportererr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                                <div className="topToolTip toolTipSupplier width100">
                                                    <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc m-b-10 onFocus" onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openTransporterSelection(e, "transporter") : null} />
                                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openTransporterSelection(e, "transporter")}>
                                                        ▾
                                                </span> : null}
                                                    {transporter != "" ? <span className="topToolTipText topAuto">{transporter}</span> : null}
                                                </div>
                                            </div>
                                            {transportererr ? (
                                                <span className="error">
                                                    Enter Transporter
                                                </span>
                                            ) : null}
                                        </div>
                                        <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Term<span className="mandatory">*</span>
                                            </label>
                                            <input autoComplete="off" readOnly type="text" className="purchaseSelectBox" id="termData" value={term} placeholder="" disabled />
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-20">
                                        <div className="col-lg-2 col-md-2 col-sm-4 pad-lft-0">
                                            <label className="purchaseLabel">
                                                HSN code<span className="mandatory">*</span>
                                            </label>
                                            <div className={hsnerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"}  >
                                                <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc onFocus" onKeyDown={(e) => this._handleKeyPress(e, "hsn")} id="hsn" value={hsnSacCode} placeholder="Choose HSN code" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openHsnCodeModal(e, "hsn") : null} />
                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openHsnCodeModal(e, "hsn")}>
                                                    ▾
                                                </span> : null}
                                            </div>
                                            {hsnerr ? (
                                                <span className="error">
                                                    Select HSN or SAC code
                                                </span>
                                            ) : null}
                                        </div>
                                        {this.state.codeRadio == "poIcode" ? <div className="col-md-2 col-sm-4 pad-lft-0">
                                            <label className="purchaseLabel">
                                                Item Code<span className="mandatory">*</span>
                                            </label>
                                            <div className={itemcodeerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"}  >
                                                <input autoComplete="off" readOnly type="text" className="inputTextKeyFuc onFocus" onKeyDown={(e) => this._handleKeyPress(e, "itemCode")} id="itemCode" value={itemCodeList.join(',')} placeholder="Choose Item code" onClick={(e) => this.openNewIcode(e, "itemCode")} />
                                                <span className="modalBlueBtn" onClick={(e) => this.openNewIcode(e, "itemCode")}>
                                                    ▾
                                                </span>
                                            </div>
                                            {itemcodeerr ? (
                                                <span className="error">
                                                    Select Item Code
                                                </span>
                                            ) : null}
                                        </div> : null}
                                        {this.state.isMrpRequired ? <div className="col-lg-4 col-md-4 pad-0">
                                            <div className="col-lg-12 col-md-12 col-sm-8 pad-0">
                                                <label className="purchaseLabel">
                                                    MRP Range<span className="mandatory">*</span>
                                                </label>
                                            </div>
                                            <div className="col-lg-12 col-md-12 col-sm-8 pad-0">
                                                <div className="col-lg-3 col-md-3 col-sm-3 pad-0">
                                                    <input autoComplete="off" type="text" type="text" readOnly className="purchaseOrderRangeTextbox" id="startRange" name="startRange" value={startRange} placeholder="Start Range" disabled />

                                                </div>
                                                <div className="col-lg-1 col-md-1 col-sm-1 pad-0 rangeLine">
                                                </div>

                                                <div className="col-lg-3 col-md-3 col-sm-3 pad-0">
                                                    <input autoComplete="off" type="text" type="text" readOnly className="purchaseOrderRangeTextbox" id="endRange" name="endRange" value={endRange} placeholder="End Range" disabled />

                                                </div>
                                            </div>

                                        </div> : null}

                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper tableGeneric sectionOneTable bordere3e7f3 poTableMain">
                                    <div className={this.state.addRow ? this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? "table-scroll oflxAuto pad-0 marginLeft_80" : "table-scroll oflxAuto pad-0" : this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? "table-scroll oflxAuto pad-0" : "table-scroll oflxAuto pad-0"}>
                                        <table className="table scrollTable table scrollTable main-table zui-table border-bot-table">
                                            <thead>
                                                <tr>
                                                    {this.state.addRow ? this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <th className="fixed-side alignMiddle" >
                                                        <ul className="list-inline">

                                                            <li className="width70">
                                                                <label className="width-45 lableFixed">
                                                                    Action
                                                                        </label>
                                                            </li>
                                                        </ul>
                                                    </th> : null : null}

                                                    {this.state.codeRadio == "raisedIndent" ? <th>
                                                        <label>
                                                            Color
                                                              </label>
                                                    </th> : ""}
                                                    {this.state.codeRadio == "raisedIndent" ? <th>
                                                        <label>
                                                            Size
                                                               </label>
                                                    </th> : ""}
                                                    {this.state.codeRadio == "raisedIndent" ? <th>
                                                        <label>
                                                            Ratio
                                                              </label>
                                                    </th> : ""}
                                                    {this.state.isMrpRequired ? <th>
                                                        {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? <label>
                                                            DESC 6<span className="mandatory">*</span>
                                                        </label> : <label>MRP<span className="mandatory">*</span></label>}
                                                    </th> : null}

                                                    {!this.state.isVendorDesignNotReq ? <th>
                                                        <label>
                                                            Vendor Design No.<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}

                                                    {this.state.isRspRequired ? <th>
                                                        <label>
                                                            RSP<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isMRPEditable ? <th>
                                                        <label>
                                                            MRP<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}

                                                    <th>
                                                        <label>
                                                            Rate<span className="mandatory">*</span>
                                                        </label>
                                                    </th>
                                                    {this.state.isDiscountAvail ? <th>
                                                        <label>
                                                            Discount<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isDisplayFinalRate ? <th>
                                                        <label>
                                                            Final rate
                                                        </label>
                                                    </th> : null}

                                                    <th>
                                                        <label>
                                                            Image
                                                             </label>
                                                    </th>
                                                    {this.state.displayOtb ? <th>
                                                        <label>
                                                            OTB<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    <th>
                                                        <label>
                                                            Delivery Date<span className="mandatory">*</span>
                                                        </label>
                                                    </th>
                                                    <th>
                                                        <label>
                                                            Type Of Buying<span className="mandatory">*</span>
                                                        </label>
                                                    </th>
                                                    {this.state.isDisplayMarginRule ? <th>
                                                        <label>
                                                            Margin Rule
                                                   </label>

                                                    </th> : null}
                                                    <th><label>
                                                        Remarks
                                                    </label></th>
                                                    <th><label>
                                                        Total Quantity
                                                    </label></th>
                                                    {this.state.isBaseAmountActive ? <th><label>
                                                        Basic
                                                    </label></th> : null}
                                                    <th><label>
                                                        Total Net Amount
                                                    </label></th>
                                                    {!this.state.isTaxDisable ? <th><label>
                                                        Tax
                                                    </label></th> : null}
                                                    {this.state.isRspRequired ? <th><label>
                                                        Calculated Margin
                                                    </label></th> : null}
                                                    {!this.state.isGSTDisable ? <th><label>
                                                        GST
                                                    </label></th> : null}

                                                    {this.state.isRspRequired ? <th>

                                                        <label>
                                                            Mark Up / Down
                                                                     </label>
                                                    </th> : null}
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {this.state.poRows.map((item, idx) => (
                                                    <tr id={idx} key={idx} className={
                                                        this.state.isDescriptionChecked ? "onclickChangeBackground" : ""}>
                                                        {this.state.addRow ? this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <td className="fixed-side alignMiddle">
                                                            <ul className="list-inline">
                                                                <li className="text-center width70">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" onClick={this.handleRemoveSpecificRow(item.gridOneId, item.otb, item.vendorMrp)} width="17" height="17" viewBox="0 0 17 19">
                                                                        <path fill={item.checked ? "#FF0000" : "#e5e5e5"} fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                    </svg>
                                                                </li>
                                                            </ul>
                                                        </td> : null : null}

                                                        {this.state.codeRadio == "raisedIndent" ? <td className="pad-0 hoverTable"    >
                                                            <label className="piToolTip m0 toolTipPos">
                                                                <div className="topToolTip">
                                                                    <label className="displayPointer">{item.color.join(',')}</label>
                                                                    <span className="topToolTipText">{item.color.join(',')}</span>
                                                                </div>

                                                            </label>
                                                        </td> : ""}
                                                        {this.state.codeRadio == "raisedIndent" ? <td className="pad-0 hoverTable">
                                                            <label className="piToolTip m0 toolTipPos">
                                                                <div className="topToolTip">
                                                                    <label className="displayPointer">{item.size.join(',')}</label>
                                                                    <span className="topToolTipText">{item.size.join(',')}</span>
                                                                </div>
                                                            </label>
                                                            {/* <label className="purchaseTableLabel">
                                            {item.size}
                                        </label> */}
                                                        </td> : ""}
                                                        {this.state.codeRadio == "raisedIndent" ? <td className="pad-0 hoverTable">
                                                            <label className="purchaseTableLabel">
                                                                {item.ratio.join(',')}
                                                            </label>
                                                        </td> : ""}
                                                        {this.state.isMrpRequired ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >

                                                            <input autoComplete="off" type="text" readOnly className="inputTable " value={item.vendorMrp} onKeyDown={(e) => this._handleKeyPressRow(e, "vendorMrp", idx)} id={"vendorMrp" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openItemCode(`${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp" + idx) : null} />
                                                            {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openItemCode(`${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp" + idx) : null} >
                                                                <svg xmlns="http://www.w3.org/2000/svg" className="svgHover" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg>

                                                            </div> : null}

                                                        </td> : null}
                                                        {!this.state.isVendorDesignNotReq ? <td className="pad-0 hoverTable tdFocus">

                                                            <input autoComplete="off" type="text" name="vendorDesign" id="vendorDesign" className="inputTable " value={item.vendorDesign} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e) : null} onBlur={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.onBlurVendorDesign(`${item.gridOneId}`, e) : null} />
                                                            {/*<input autoComplete="off" type="text" name="vendorDesign" id="vendorDesign" className="inputTable " value={item.vendorDesign} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openVendorModal(`${item.gridOneId}`, item.vendorDesign) : null} onBlur={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.onBlurVendorDesign(`${item.gridOneId}`, e) : null} />
                                                            {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openVendorModal(`${item.gridOneId}`, item.vendorDesign) : null} >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg>

                                                            </div> : null}*/}
                                                        </td> : null}

                                                        {this.state.isRspRequired ? <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" readOnly type="text" className="inputTable" id="rsp" value={item.rsp} />
                                                        </td> : null}

                                                        {this.state.isMRPEditable ? <td className={this.state.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? "errorBorder pad-0" : "pad-0 tdFocus" : "pad-0 tdFocus"}>
                                                            <label className="rateHover" id={"toolIdMrp" + item.gridOneId} onMouseOver={this.state.codeRadio != "setBased" ? (e) => this.mouseOverCmprMrp(`${item.gridOneId}`) : null}>
                                                                <input autoComplete="off" type="text" className="inputTable" id="mrp" pattern="[0-9]+([\.][0-9]{0,2})?" value={item.mrp} onBlur={(e) => this.blurMrp(`${item.gridOneId}`, `${item.mrp}`)} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e) : null} />
                                                                {this.state.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? <span> <div className="showRate" id={"moveLeftMrp" + item.gridOneId}><p>Rsp cannot be greater then Mrp</p></div></span> : "" : ""}
                                                            </label>
                                                        </td> : null}

                                                        <td className={this.state.isMrpRequired || this.state.isMRPEditable ? Number(item.rate) > Number(item.mrp) ? "errorBorder pad-0" : "pad-0 tdFocus" : "pad-0 tdFocus"}>
                                                            <label className="rateHover" id={"toolIdRate" + item.gridOneId} onMouseOver={this.state.codeRadio != "setBased" ? (e) => this.mouseOverCmprRate(`${item.gridOneId}`) : null}>
                                                                <input autoComplete="off" type="text" className="inputTable" id="rate" pattern="[0-9.]*" value={item.rate} onFocus={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.rateValueOnFocus(e, item.gridOneId, "rate", item.finalRate) : null} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e) : null} onBlur={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.onBlurRate(`${item.gridOneId}`, e) : null} />
                                                                {this.state.isMrpRequired || this.state.isMRPEditable ? Number(item.rate) > Number(item.mrp) ? <span> <div className="showRate" id={"moveLeftRate" + item.gridOneId}><p>Rate cannot be greater then Mrp</p></div></span> : "" : ""}
                                                            </label>
                                                        </td>
                                                        {this.state.isDiscountAvail ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >

                                                            <input autoComplete="off" type="text" readOnly className="inputTable " value={item.discount.discountType} onKeyDown={(e) => this._handleKeyPressRow(e, "discount", idx)} id={"discount" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || (sessionStorage.getItem("partnerEnterpriseName") != "VMART" && this.state.codeRadio == "poIcode") ? (e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.gridOneId}`, "discount" + idx) : null} />
                                                            {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || (sessionStorage.getItem("partnerEnterpriseName") != "VMART" && this.state.codeRadio == "poIcode") ? (e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.gridOneId}`, "discount" + idx) : null} >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg>

                                                            </div> : null}

                                                        </td> : null}
                                                        {this.state.isDisplayFinalRate ? <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" type="text" readOnly className="inputTable" id="finalRate" value={item.finalRate} />
                                                        </td> : null}
                                                        <td className="pad-0 pad-0 hoverTable openModalBlueBtn openModalBlueBtn tdFocus displayPointer" onClick={this.state.codeRadio != "setBased" ? (e) =>
                                                            this.openImageModal(`${item.gridOneId}`, "image" + idx) : null}>
                                                            <label className="piToolTip">
                                                                <div className="topToolTip">
                                                                    <input type="text" className="imageInput" readOnly onKeyDown={this.state.codeRadio != "setBased" ? (e) => this._handleKeyPressRow(e, "image", idx) : null} id={"image" + idx} value={item.image != undefined ? item.image.join(',') : ""} />
                                                                    {/* <label>{item.image != undefined ? item.image.join(',') : ""}</label> */}
                                                                    {item.image != undefined ? <span className="topToolTipText"> {item.image.join(',')}</span> : null}
                                                                </div>
                                                            </label>
                                                            {this.state.codeRadio != "setBased" ? <div className="purchaseTableDiv" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="34" height="44" viewBox="0 0 33 43">
                                                                    <g fill="none" fillRule="nonzero">
                                                                        <path fill="#6D6DC9" d="M0 0h33v43H0z" />
                                                                        <path fill="#FFF" d="M13.143 22.491A2.847 2.847 0 0 1 16 19.635a2.847 2.847 0 0 1 2.857 2.856A2.847 2.847 0 0 1 16 25.348a2.847 2.847 0 0 1-2.857-2.857zm-6.026 4.05v-8.236c0-.94.763-1.703 1.703-1.703h3.404l.156-.685c.255-1.056 1.174-1.8 2.25-1.8h2.7c1.077 0 2.016.744 2.25 1.8l.157.685h3.404c.94 0 1.702.763 1.702 1.702v8.237c0 .998-.802 1.82-1.82 1.82H8.938c-.998 0-1.82-.822-1.82-1.82zm4.676-4.05A4.2 4.2 0 0 0 16 26.698a4.2 4.2 0 0 0 4.207-4.207A4.183 4.183 0 0 0 16 18.304a4.196 4.196 0 0 0-4.207 4.187zm-2.817-3.443c0 .47.391.86.861.86s.86-.39.86-.86-.39-.861-.86-.861c-.49.02-.86.391-.86.86z" />
                                                                    </g>
                                                                </svg>
                                                            </div> : null}
                                                        </td>
                                                        {this.state.displayOtb ? <td className={item.otb < item.amount ? "errorBorder pad-0" : "pad-0 tdFocus"}>
                                                            <input autoComplete="off" readOnly type="text" className="inputTable" id={"otb" + idx} value={item.otb} onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e) : null} />
                                                        </td> : null}

                                                        <td className={item.deliveryDate != "" ? item.deliveryDate < this.state.minDate || item.deliveryDate > this.state.maxDate ? "errorBorder pad-0 positionRelative" : "pad-0 positionRelative tdFocus" : "pad-0 positionRelative tdFocus"}>
                                                            <input type="date" className="inputTable height0" min={this.state.minDate} max={this.state.maxDate} placeholder={item.deliveryDate} id="date" onChange={(e) => this.handleChangeGridOne(`${item.gridOneId}`, e)} name="date" value={item.deliveryDate} />
                                                        </td>
                                                        <td className="typeOfBuying tdFocus">
                                                            <select value={item.typeOfBuying} id="typeofBuying" onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e) : null}>
                                                                <option>Select</option>
                                                                <option value="Adhoc" > Adhoc</option>
                                                                <option value="Planned ">Planned </option>
                                                            </select>
                                                        </td>
                                                        {this.state.isDisplayMarginRule ? <td className="typeOfBuying tdFocus td-disabled">
                                                            <label> {item.marginRule}</label>
                                                        </td> : null}
                                                        <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" type="text" className="inputTable" id="remarks" value={item.remarks} onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e) : null} />
                                                        </td>
                                                        <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" disabled className="inputTable" id="quantity" value={item.quantity} />
                                                        </td>
                                                        {this.state.isBaseAmountActive ? <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" disabled className="inputTable" id="basic" value={item.basic} />
                                                        </td> : null}
                                                        <td className="pad-0 posRelative poToolTipMain tdFocus td-disabled">
                                                            <label>
                                                                <input autoComplete="off" type="text" disabled className="inputTable" id={"amount" + idx} value={item.amount} />
                                                                <span className="netAmtHover netAmtTotal displayInline displayPointer">
                                                                    {item.amount != "" ? <img id={"toolId" + item.gridOneId} className="exclaimIconToolTip" onMouseOver={(e) => this.calculatedMargin(`toolId${item.gridOneId}`, `moveLeft${item.gridOneId}`)} src={exclaimIcon} />
                                                                        : null}
                                                                    <div id={"moveLeft" + item.gridOneId} className="totalAmtDescDrop">
                                                                        <div className="amtContent">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr><th><p>Charge Name</p></th>
                                                                                        <th><p>% / Amount</p></th>
                                                                                        <th><p>Total</p></th></tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    {item.finCharges && item.finCharges.length > 0 && item.finCharges.map((data, key) => (
                                                                                        <tr key={key}>
                                                                                            <td><p>{data.chgName}</p></td>
                                                                                            <td><p>{data.rates != undefined ? <span>{data.rates.igstRate} </span> : ""}</p>{}</td>
                                                                                            <td><p>{data.charges != undefined ? <span>{data.charges.sign}{data.charges.chargeAmount}</span> : data.sign + data.finChgRate}</p>

                                                                                            </td>
                                                                                        </tr>))}
                                                                                </tbody>

                                                                            </table>
                                                                        </div>
                                                                        <div className="amtBottom">
                                                                            <div className="col-md-12">
                                                                                <div className="col-md-6 pad-lft-7">

                                                                                    <span>Basic</span>
                                                                                </div>
                                                                                <div className="col-lg-6 col-md-6 textRight padRightNone">

                                                                                    <span className="totalAmtt">+{item.basic}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="amtBottom">
                                                                            <div className="col-lg-12 col-md-12 pad-top-5 m-top-5">
                                                                                <div className="col-lg-6 col-md-6">

                                                                                    <p>Grand Total</p>
                                                                                </div>
                                                                                <div className="col-lg-6 col-md-6 textRight padRightNone">

                                                                                    <p className="totalAmt">{item.amount}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </label>
                                                        </td>
                                                        {!this.state.isTaxDisable ? <td className="pad-0 tdFocus td-disabled">
                                                            <label className="piToolTip m0 poToolTip">
                                                                <div className="topToolTip">
                                                                    <label>{item.tax.length != 0 ? item.tax.join(',') : ""}</label>
                                                                    <span className="topToolTipText">{item.tax.length != 0 ? item.tax.join(',') : ""}</span>
                                                                </div>
                                                            </label>
                                                            {/*<input autoComplete="off" type="text" className="inputTable" id="tax" value={item.tax.length != 0 ? item.tax.join(',') : ""} />*/}
                                                        </td> : null}

                                                        {this.state.isRspRequired ? <td className="pad-0 posRelative poToolTipMain tdFocus td-disabled">
                                                            <label>
                                                                <label className="piToolTip m0 poToolTip">
                                                                    <div className="topToolTip">
                                                                        <label className="max-width80">{this.state.calculatedMarginExact}</label>
                                                                        <span className="topToolTipText">{this.state.calculatedMarginExact}</span>
                                                                    </div>
                                                                </label>
                                                                {/*<input autoComplete="off" type="text" className="inputTable" id="calculatedMargin" value={item.calculatedMargin.length != 0 ? item.calculatedMargin.join(',') : ""} />*/}
                                                                <span className="calculatedMargin displayInline displayPointer ">
                                                                    {this.state.calculatedMarginExact != "" ? <img src={exclaimIcon} className="exclaimIconToolTip" id={"marginIdSet" + item.gridOneId} onMouseOver={(e) => this.calculatedMargin(`marginIdSet${item.gridOneId}`, `marginLeft${item.gridOneId}`)} /> : null}
                                                                    <div className="calculatedToolTip totalAmtDescDrop" id={"marginLeft" + item.gridOneId}>
                                                                        <div className="formula">
                                                                            <h3>Calculated Margin</h3>
                                                                            <h5>RSP-COST <span>X</span> <span className="pad-0">100</span><p>RSP</p></h5>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </label>
                                                        </td> : null}

                                                        {!this.state.isGSTDisable ? <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" disabled className="inputTable" id={"gst" + idx} value={item.gst.length != 0 ? item.gst.join(',') : ""} />
                                                        </td> : null}
                                                        {this.state.isMrpRequired ? <td className="pad-0 posRelative poToolTipMain tdFocus td-disabled">
                                                            <label>
                                                                <label className="piToolTip m0 poToolTip">
                                                                    <div className="topToolTip">
                                                                        <label className="max-width80">{item.mrk.length != 0 ? item.mrk.join(',') : ""}</label>
                                                                        <span className="topToolTipText">{item.mrk.length != 0 ? item.mrk.join(',') : ""}</span>
                                                                    </div>
                                                                </label>
                                                                {/*<input autoComplete="off" type="text" className="inputTable" id="mrk" value={item.mrk.length != 0 ? item.mrk.join(',') : ""} />*/}
                                                                <span className="calculatedMargin displayInline displayPointer inTakeMargin">
                                                                    {item.mrk != "" ? <img src={exclaimIcon} className="exclaimIconToolTip" id={"intakeMarginSet" + item.gridOneId} onMouseOver={(e) => this.calculatedMargin(`intakeMarginSet${item.gridOneId}`, `inTakeMarginLeft${item.gridOneId}`)} />
                                                                        : null}
                                                                    <div className="calculatedToolTip totalAmtDescDrop" id={"inTakeMarginLeft" + item.gridOneId}>
                                                                        <div className="formula">
                                                                            <h3>Intake Margins</h3>
                                                                            <h5><span>RSP</span> <span>X</span> <span className="pad-0">100</span><p className="pad-lft-0">100+COST</p></h5>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </label>
                                                        </td> : null}

                                                    </tr>
                                                ))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                {/* __________________________________ITEM CODE TABLE END____________________ */}
                                {this.state.addRow ? <div className="newRowAdd m-top-10">
                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <button onClick={this.handleAddRow} type="button" id="addIndentRow" className="zuiBtn">
                                        Add Row
                        </button> : <button type="button" className="zuiBtn btnDisabled">
                                            Add Row
                        </button>}
                                </div> : null}
                                {/* __________________________________UNIQUE CODE TABLE______________________ */}

                                {department != "" ? <div className="col-md-12 col-sm-8 pad-0 m-top-35">
                                    <ul className="list_style">
                                        <li>
                                            <label className="contribution_mart">
                                                Item Details
                                                </label>

                                        </li>

                                    </ul>
                                </div> : null}
                                {department != "" ? <div className="col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden catDesTable sectionOneTable bordere3e7f3 poTableMain">
                                    <div className="table-scroll zui-scroller m0">
                                        <table className="table scrollTable table scrollTable main-table zui-table oflHidden border-bot-table displayTable">
                                            <thead>
                                                <tr>
                                                    {!this.state.isVendorDesignNotReq ? <th>
                                                        <label>
                                                            Vendor Design No.
                                        </label>
                                                    </th> : null}
                                                    {this.state.itemDetailsHeader.length != 0 ? this.state.itemDetailsHeader.map((data, key) => (
                                                        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.catdesc != "CAT5" && data.catdesc != "CAT6" && data.catdesc != "DESC1" && data.catdesc != "DESC4" && data.catdesc != "DESC6" ?
                                                            <th key={key}>
                                                                <label>
                                                                    {typeof (data.displayName) == "string" && data.displayName.length ? data.displayName : data.catdesc}{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null}
                                                                </label>
                                                            </th> : null :
                                                            ((data.catdesc == "CAT1" && (this.state.cat1Validation == true || data.isLovPO == "Y")) || (data.catdesc == "CAT2" && (this.state.cat2Validation == true || data.isLovPO == "Y")) || (data.catdesc == "CAT3" && (this.state.cat3Validation == true || data.isLovPO == "Y")) || (data.catdesc == "CAT4" && (this.state.cat4Validation == true || data.isLovPO == "Y")) || (data.catdesc == "CAT5" && (this.state.cat5Validation == true || data.isLovPO == "Y")) || (data.catdesc == "CAT6" && (this.state.cat6Validation == true || data.isLovPO == "Y")) || (data.catdesc == "DESC1" && (this.state.desc1Validation == true || data.isLovPO == "Y")) || (data.catdesc == "DESC2" && (this.state.desc2Validation == true || data.isLovPO == "Y")) || (data.catdesc == "DESC3" && (this.state.desc3Validation == true || data.isLovPO == "Y")) || (data.catdesc == "DESC4" && (this.state.desc4Validation == true || data.isLovPO == "Y")) || (data.catdesc == "DESC5" && (this.state.desc5Validation == true || data.isLovPO == "Y")) || (data.catdesc == "DESC6" && (this.state.desc6Validation == true || data.isLovPO == "Y")) ?
                                                                <th key={key}>
                                                                    <label>
                                                                        {typeof (data.displayName) == "string" ? data.displayName : data.catdesc}{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null}
                                                                        {/* {data.displayName!=null? data.catdesc:data.displayName} */}
                                                                    </label>
                                                                </th> : null
                                                            ))) : null}
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {this.state.catDescRows.map((item, iddd) => (
                                                    <tr id={iddd} key={iddd} >
                                                        {!this.state.isVendorDesignNotReq ? <td className="pad-0 hoverTable tdFocus">

                                                            <label className="purchaseTableLabel">
                                                                {item.vendorDesign}
                                                            </label>

                                                        </td> : null}
                                                        {item.itemDetails != undefined ? item.itemDetails.map((items, itemkey) => (
                                                            sessionStorage.getItem("partnerEnterpriseName") == "VMART" ?
                                                                items.catdesc != "DESC1" && items.catdesc != "DESC4" && items.catdesc != "DESC6" && items.catdesc != "CAT5" && items.catdesc != "CAT6" ?
                                                                    <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={itemkey}  >
                                                                        <input autoComplete="off" readOnly type="text" name="" className="inputTable " id={items.catdesc + iddd} onKeyDown={(e) => this._handleKeyPressRow(e, `${items.catdesc}`, iddd)} value={items.value} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openItemModal(`${items.catdesc}`, `${item.gridId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + iddd}`, items.code) : null} />
                                                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" onClick={(e) => this.openItemModal(`${items.catdesc}`, `${item.gridId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + iddd}`, items.code)} >
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                <g fill="none" fillRule="evenodd">
                                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                </g>
                                                                            </svg>

                                                                        </div> : null}

                                                                    </td> : null

                                                                :
                                                                ((items.catdesc == "CAT1" && (this.state.cat1Validation == true || items.isLovPO == "Y")) || (items.catdesc == "CAT2" && (this.state.cat2Validation == true || items.isLovPO == "Y")) || (items.catdesc == "CAT3" && (this.state.cat3Validation == true || items.isLovPO == "Y")) || (items.catdesc == "CAT4" && (this.state.cat4Validation == true || items.isLovPO == "Y")) || (items.catdesc == "CAT5" && (this.state.cat5Validation == true || items.isLovPO == "Y")) || (items.catdesc == "CAT6" && (this.state.cat6Validation == true || items.isLovPO == "Y")) || (items.catdesc == "DESC1" && (this.state.desc1Validation == true || items.isLovPO == "Y")) || (items.catdesc == "DESC2" && (this.state.desc2Validation == true || items.isLovPO == "Y")) || (items.catdesc == "DESC3" && (this.state.desc3Validation == true || items.isLovPO == "Y")) || (items.catdesc == "DESC4" && (this.state.desc4Validation == true || items.isLovPO == "Y")) || (items.catdesc == "DESC5" && (this.state.desc5Validation == true || items.isLovPO == "Y")) || (items.catdesc == "DESC6" && (this.state.desc6Validation == true || items.isLovPO == "Y")) ?
                                                                    <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={itemkey}  >
                                                                        <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={items.catdesc + iddd} onKeyDown={(e) => this._handleKeyPressRow(e, `${items.catdesc}`, iddd)} value={items.value} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openItemModal(`${items.catdesc}`, `${item.gridId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + iddd}`, items.code) : null} />
                                                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <div className="purchaseTableDiv" onClick={(e) => this.openItemModal(`${items.catdesc}`, `${item.gridId}`, `${items.value}`, `${items.displayName}`, `${items.catdesc + iddd}`, items.code)} >
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                <g fill="none" fillRule="evenodd">
                                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                </g>
                                                                            </svg>

                                                                        </div> : null}

                                                                    </td> : null

                                                                ))) : null}
                                                    </tr>
                                                ))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div> : null}

                                {/* ________________________________________UNIQUE CODE TABLE END________________________ */}


                                {/* -----------------------------------------------Section 2----------------------------------- */}


                                {department != "" ? this.state.itemUdfExist == "true" ? <div className="col-md-12 col-sm-12 col-xs-12 pad-0">

                                    <div className="col-md-12 col-sm-8 pad-0 m-top-35">
                                        <ul className="list_style">
                                            <li>
                                                <label className="contribution_mart">
                                                    SECTION 2
                                 </label>

                                            </li>

                                        </ul>
                                    </div>
                                </div> : null : null}

                                {department != "" ? this.state.itemUdfExist == "true" ? <div className="col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionOneTable bordere3e7f3 poTableMain">
                                    <div className="zui-scroller pad-0 m0">
                                        <table className="table scrollTable table scrollTable main-table zui-table border-bot-table displayTable" >
                                            <thead>
                                                <tr>
                                                    {!this.state.isVendorDesignNotReq ? <th ><label>
                                                        Vendor Design  </label>
                                                    </th> : null}
                                                    {this.state.udfRowMapping != null ? this.state.udfRowMapping.length != 0 ? this.state.udfRowMapping.map((data, keyyyy) => (
                                                        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.isLovPO == "Y" ? <th id={keyyyy} key={keyyyy} >
                                                            <label>  {data.displayName != null ? data.displayName : this.Capitalize(data.cat_desc_udf.toLowerCase())}{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null} </label>
                                                        </th> : null :
                                                            ((data.cat_desc_udf == "UDFSTRING01" && (this.state.itemudf1Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING02" && (this.state.itemudf2Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING03" && (this.state.itemudf3Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING04" && (this.state.itemudf4Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING05" && (this.state.itemudf5Validation == true || data.isLovPO == "Y")) ||
                                                                (data.cat_desc_udf == "UDFSTRING06" && (this.state.itemudf6Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING07" && (this.state.itemudf7Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING08" && (this.state.itemudf8Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING09" && (this.state.itemudf9Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING10" && (this.state.itemudf10Validation == true || data.isLovPO == "Y")) ||
                                                                (data.cat_desc_udf == "UDFNUM01" && (this.state.itemudf11Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM02" && (this.state.itemudf12Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM03" && (this.state.itemudf13Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM04" && (this.state.itemudf14Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM05" && (this.state.itemudf15Validation == true || data.isLovPO == "Y")) ||
                                                                (data.cat_desc_udf == "UDFDATE01" && (this.state.itemudf16Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE02" && (this.state.itemudf17Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE03" && (this.state.itemudf18Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE04" && (this.state.itemudf19Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE05" && (this.state.itemudf20Validation == true || data.isLovPO == "Y"))) ? <th id={keyyyy} key={keyyyy} >
                                                                    <label>  {data.displayName != null ? data.displayName : this.Capitalize(data.cat_desc_udf.toLowerCase())}{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null} </label>
                                                                </th> : null
                                                    )) : null : null}

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.itemUdf.map((data, iukey) => (
                                                    <tr id={iukey} key={iukey} className={
                                                        this.state.isItemTwoDescriptionChecked ? "onclickChangeBackground" : ""
                                                    }>
                                                        {!this.state.isVendorDesignNotReq ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                                            <input autoComplete="off" type="text" readOnly name="vendordesign" className="inputTable " value={data.vendorDesign} />

                                                        </td> : null}
                                                        {data.udfList != undefined ? data.udfList.map((dataa, keyy) => (
                                                            sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? dataa.isLovPO == "Y" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={keyy} >
                                                                <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={dataa.cat_desc_udf + iukey} onKeyDown={(e) => this._handleKeyPressRow(e, `${dataa.cat_desc_udf}`, iukey)} value={dataa.value} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfGridId}`, `${dataa.value}`, `${dataa.cat_desc_udf + iukey}`) : null} />
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" onClick={(e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfGridId}`, `${dataa.value}`, `${dataa.cat_desc_udf + iukey}`)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null}
                                                            </td> : null :
                                                                ((dataa.cat_desc_udf == "UDFSTRING01" && (this.state.itemudf1Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING02" && (this.state.itemudf2Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING03" && (this.state.itemudf3Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING04" && (this.state.itemudf4Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING05" && (this.state.itemudf5Validation == true || dataa.isLovPO == "Y")) ||
                                                                    (dataa.cat_desc_udf == "UDFSTRING06" && (this.state.itemudf6Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING07" && (this.state.itemudf7Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING08" && (this.state.itemudf8Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING09" && (this.state.itemudf9Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING10" && (this.state.itemudf10Validation == true || dataa.isLovPO == "Y")) ||
                                                                    (dataa.cat_desc_udf == "UDFNUM01" && (this.state.itemudf11Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM02" && (this.state.itemudf12Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM03" && (this.state.itemudf13Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM04" && (this.state.itemudf14Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM05" && (this.state.itemudf15Validation == true || dataa.isLovPO == "Y")) ||
                                                                    (dataa.cat_desc_udf == "UDFDATE01" && (this.state.itemudf16Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE02" && (this.state.itemudf17Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE03" && (this.state.itemudf18Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE04" && (this.state.itemudf19Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE05" && (this.state.itemudf20Validation == true || dataa.isLovPO == "Y"))) ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={keyy} >
                                                                        <input autoComplete="off" type="text" readOnly name="" className="inputTable " id={dataa.cat_desc_udf + iukey} onKeyDown={(e) => this._handleKeyPressRow(e, `${dataa.cat_desc_udf}`, iukey)} value={dataa.value} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? (e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfGridId}`, `${dataa.value}`, `${dataa.cat_desc_udf + iukey}`) : null} />
                                                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "poIcode" ? <div className="purchaseTableDiv" onClick={(e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemUdfGridId}`, `${dataa.value}`, `${dataa.cat_desc_udf + iukey}`)}>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                <g fill="none" fillRule="evenodd">
                                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                </g>
                                                                            </svg>
                                                                        </div> : null}
                                                                    </td> : null
                                                        )) : null}
                                                    </tr>
                                                ))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div> : null : null}

                                {/* -----------------------------------------------Section 2 end----------------------------------- */}
                                {department != "" ? <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                                    <div className="col-md-12 col-sm-8 pad-0 m-top-35">
                                        <ul className="list_style">
                                            <li>
                                                {this.state.itemUdfExist == "true" ? <label className="contribution_mart">
                                                    SECTION 3</label> : <label className="contribution_mart">SECTION 2</label>}
                                            </li>
                                        </ul>
                                    </div>
                                </div> : null}
                                {department != "" ? <div className={this.state.copyColor == "true" ? "col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionTwoTable tableGeneric sectionOneTable bordere3e7f3 poTableMain sectionthreeTable" : "col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionTwoTable tableGeneric sectionOneTable bordere3e7f3 poTableMain sectionthreeTable tableWid manage-sec-table"}>
                                    <div className={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? "table-scroll zui-scroller pad-0" : "table-scroll zui-scroller pad-0 m0"}>
                                        <table className="table scrollTable table scrollTable main-table zui-table border-bot-table">
                                            <thead>
                                                <tr>
                                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <th className="fixed-side alignMiddle" >
                                                        <ul className="list-inline">
                                                            <li className="width70">
                                                                <label className="width-45 lableFixed">Action</label>
                                                            </li>

                                                        </ul></th> : null}
                                                    {!this.state.isVendorDesignNotReq ? <th>
                                                        <label>Vendor Design No.</label>
                                                    </th> : null}
                                                    {this.state.codeRadio == "poIcode" ? <th>
                                                        <label>ICODE<span className="mandatory">*</span></label>
                                                    </th> : null}
                                                    {this.state.isColorRequired ? <th>
                                                        <label>Color<span className="mandatory">*</span></label>
                                                    </th> : null}
                                                    {this.state.isColorRequired ? <th>
                                                        <label> Option</label>
                                                    </th> : null}
                                                    <th>
                                                        <label>Set Number</label>
                                                    </th>


                                                    {this.state.codeRadio == "poIcode" ? <th>
                                                        <label>Size<span className="mandatory">*</span></label>
                                                    </th> : null}
                                                    <th><label>Size</label></th>
                                                    <th>
                                                        <label>Size Ratio</label>
                                                    </th>
                                                    {this.state.codeRadio == "poIcode" ? <th>
                                                        <label>Set Ratio<span className="mandatory">*</span></label>
                                                    </th> : null}
                                                    <th>
                                                        <label>PP Qty</label>
                                                    </th>
                                                    <th>
                                                        <label>Set Qty<span className="mandatory">*</span></label>
                                                    </th>
                                                    <th>
                                                        <label>Net Qty</label>
                                                    </th>

                                                    {/* {this.state.codeRadio != "poIcode" ? this.state.sizeMapping != null ? this.state.sizeMapping.length != 0 ? this.state.sizeMapping.map((data, idd) => (
                                                        <th id={idd} key={idd} ><label>  {data.cname} </label> </th>)) : null : null : null} */}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.secTwoRows.map((itemGridTwo, key) => (
                                                    <tr id={key} key={key} className={this.state.isSecTwoDescriptionChecked ? "onclickChangeBackground" : ""}>
                                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <td className="fixed-side alignMiddle tdFocus" >
                                                            <ul className="list-inline">
                                                                <li className={this.state.copyColor == "true" ? "text-center width_50px padRightNone" : "text-center  width_80px padRightNone"}>
                                                                    <img src={copyIcon} onClick={(e) => this.copySet(itemGridTwo.gridTwoId, itemGridTwo.designRowId, itemGridTwo.setNo)} />
                                                                    <svg xmlns="http://www.w3.org/2000/svg" onClick={this.deleteSecTwoRows(itemGridTwo.gridTwoId, itemGridTwo.setNo)} width="17" height="17" viewBox="0 0 17 19">
                                                                        <path fill="#e5e5e5" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                    </svg>
                                                                </li>
                                                                {/*{this.state.codeRadio == "poIcode" ? <li className="pad-0"><button type="button" className="itemCodeBtn" onClick={(e) => this.openPOIcode(itemGridTwo.gridTwoId, itemGridTwo.icodes, itemGridTwo.colorList, itemGridTwo.sizeListData, itemGridTwo.setQty)}><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                                                    <path fill="#6629db" fillRule="nonzero" d="M16.556 7.218h-5.774V1.444a1.443 1.443 0 1 0-2.887 0v5.774H2.121a1.443 1.443 0 1 0 0 2.887h5.774v5.774a1.443 1.443 0 1 0 2.887 0v-5.774h5.774a1.443 1.443 0 1 0 0-2.887z" />
                                                                </svg>ICode</button></li> : null}*/}

                                                                {this.state.copyColor == "true" && this.state.isColorRequired ? <label className="containerPurchaseRadio displayPointer copiedBtn"> {itemGridTwo.colorChk ? "Copied" : "Copy Color"}
                                                                    <input autoComplete="off" type="radio" id={"colorRadio" + key} readOnly onClick={(e) => this.copingFocus(itemGridTwo.gridTwoId, itemGridTwo.designRowId, itemGridTwo.color, itemGridTwo.colorList, itemGridTwo.option, itemGridTwo.total)} checked={itemGridTwo.colorChk} name="colorRadio" value="colorRadio" />
                                                                    <span className="checkmark"></span>
                                                                </label> : null}

                                                            </ul>
                                                        </td> : null}
                                                        {!this.state.isVendorDesignNotReq ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus"  >
                                                            <input autoComplete="off" type="text" readOnly name="vend" onKeyDown={(e) => this._handleKeyPress(e, "vendorDesign" + `${itemGridTwo.gridTwoId}`)} id={"vendorDesign" + `${itemGridTwo.gridTwoId}`} value={itemGridTwo.vendorDesignNo} className="inputTable " />
                                                        </td> : null}
                                                        {this.state.codeRadio == "poIcode" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus"  >
                                                            <input autoComplete="off" type="text" readOnly name="itemBarcode" onKeyDown={(e) => this._handleKeyPress(e, "itemBarcode" + `${itemGridTwo.gridTwoId}`)} id={"itemBarcode" + `${itemGridTwo.gridTwoId}`} value={itemGridTwo.itemBarcode} className="inputTable " />
                                                        </td> : null}
                                                        {this.state.isColorRequired ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                                            <label className="piToolTip m0 poToolTip">
                                                                <div className="topToolTip">
                                                                    <input autoComplete="off" type="text" name="color" readOnly onKeyDown={(e) => this._handleKeyPressRow(e, "color", key)} id={"color" + key} value={itemGridTwo.color} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? (e) => this.openPoColorModal(`${itemGridTwo.gridTwoId}`, `${itemGridTwo.color}`, itemGridTwo.colorList, `${"color" + key}`) : null} className="inputTable " />
                                                                    <span className="topToolTipText">{itemGridTwo.color}</span>
                                                                </div>
                                                            </label>
                                                            {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" ? <div className="purchaseTableDiv" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg>
                                                            </div> : null}
                                                        </td> : null}
                                                        {this.state.isColorRequired ? <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" className="inputTable" id="option" readOnly pattern="[0-9]*" value={itemGridTwo.option} />
                                                        </td> : null}

                                                        <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" className="inputTable" id="setNo" pattern="[0-9]*" value={itemGridTwo.setNo} readOnly />
                                                        </td>

                                                        {this.state.codeRadio == "poIcode" ? <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" className="inputTable" id="sizeId" value={itemGridTwo.size} readOnly />

                                                        </td> : null}
                                                        <td>
                                                            <input type="text" className="inputTable" id={itemGridTwo.designRowId} onClick={this.openSizeModal} value={itemGridTwo.size.length && itemGridTwo.size.join(',')} />
                                                        </td>
                                                        <td>
                                                            <input type="text" className="inputTable" value={itemGridTwo.ratio.join(',')} />
                                                        </td>
                                                        {this.state.codeRadio == "poIcode" ? <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" type="text" className="inputTable" pattern="[0-9]*" id="setRatio" value={itemGridTwo.setRatio} onBlur={(e) => this.blurSizeHandle(`${itemGridTwo.gridTwoId}`, `${itemGridTwo.setQty}`, `${itemGridTwo.total}`, `${itemGridTwo.designRowId}`, e)} onChange={(e) => this.sizeHandleChange(`${itemGridTwo.gridTwoId}`, `${itemGridTwo.size}`, e, `${itemGridTwo.color}`)} />

                                                        </td> : null}
                                                        <td className="pad-0 tdFocus td-disabled">
                                                            <input autoComplete="off" type="text" className="inputTable" pattern="[0-9]*" id="total" value={itemGridTwo.total} readOnly />

                                                        </td>
                                                        <td className="pad-0 tdFocus">
                                                            <input autoComplete="off" type="text" className="inputTable" id={"setQty" + key} pattern="[0-9]*" onFocus={(e) => this.focusQty(e, itemGridTwo.gridTwoId)} value={itemGridTwo.setQty} onKeyDown={(e) => this.setQtyupDownArrow(e, key)} onBlur={(e) => this.onBlurQuantity(`${itemGridTwo.gridTwoId}`, `${itemGridTwo.setQty}`, `${itemGridTwo.total}`, `${itemGridTwo.designRowId}`, e, `${"setQty" + key}`)} onChange={(e) => this.handleChange(`${itemGridTwo.gridTwoId}`, `${itemGridTwo.designRowId}`, e, `${"setQty" + key}`)} />
                                                        </td>
                                                        <td>
                                                            <input type="text" className="inputTable" value={itemGridTwo.quantity} />
                                                        </td>


                                                        {/* {this.state.codeRadio != "poIcode" ? itemGridTwo.sizeList != undefined ? itemGridTwo.sizeList.map((data, skey) => (
                                                            <td key={skey} className="tdFocus">
                                                                <input autoComplete="off" type="text" className="inputTable" id={data.cname + itemGridTwo.gridTwoId} pattern="[0-9]*" value={data.ratio} onBlur={this.state.codeRadio != "setBased" ? (e) => this.blurSizeHandle(itemGridTwo.gridTwoId, `${itemGridTwo.setQty}`, `${itemGridTwo.total}`, `${itemGridTwo.designRowId}`, e) : null} onKeyDown={(e) => this.downArrow(e, itemGridTwo.gridTwoId, data.cname)} onChange={this.state.codeRadio != "setBased" ? (e) => this.sizeHandleChange(`${itemGridTwo.gridTwoId}`, `${data.cname}`, e, `${itemGridTwo.color}`) : null} />
                                                            </td>
                                                        )) : null : null} */}
                                                    </tr>))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div> : null}
                                {department != "" ? <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                    <div className="col-md-3 pad-0"></div>
                                </div> : null}
                                {department != "" && this.state.isUDFExist == "true" ? <div className="col-md-12 col-sm-12 pad-0 m-top-20 tableWidthAuto sectionOneTable dynamicTable poTableMain">
                                    <div className="scrollableTableFixed">
                                        <table className="table border-bot-table tableBorderFocus">
                                            <thead>
                                                <tr>
                                                    <th><label>Set No.</label></th>
                                                    {this.state.isColorRequired ? <th> <label> Color</label></th> : null}
                                                    {this.state.udfMappingData.length != 0 ? this.state.udfMappingData.map((data, idd) => (
                                                        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.isLovPO == "Y" ? <th id={idd} key={idd} >
                                                            <label>{data.displayName != null ? data.displayName : data.udfType}{data.isCompulsary == "Y" ? <span className="mandatory">*</span> : null}
                                                            </label>
                                                        </th> : null :
                                                            ((data.udfType == "SMUDFSTRING01" && (this.state.udf1Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING02" && (this.state.udf2Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING03" && (this.state.udf3Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING04" && (this.state.udf4Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING05" && (this.state.udf5Validation == true || data.isLovPO == "Y")) ||
                                                                (data.udfType == "SMUDFSTRING06" && (this.state.udf6Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING07" && (this.state.udf7Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING08" && (this.state.udf8Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING09" && (this.state.udf9Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRIN10" && (this.state.udf10Validation == true || data.isLovPO == "Y")) ||
                                                                (data.udfType == "SMUDFNUM01" && (this.state.udf11Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM02" && (this.state.udf12Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM03" && (this.state.udf13Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM04" && (this.state.udf14Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM05" && (this.state.udf15Validation == true || data.isLovPO == "Y")) ||
                                                                (data.udfType == "SMUDFDATE01" && (this.state.udf16Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE02" && (this.state.udf17Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE03" && (this.state.udf18Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE04" && (this.state.udf19Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE05" && (this.state.udf20Validation == true || data.isLovPO == "Y"))) ? <th id={idd} key={idd} >
                                                                    <label>{data.displayName != null ? data.displayName : data.udfType}{data.isCompulsary == "Y" ? <span className="mandatory">*</span> : null}
                                                                    </label>
                                                                </th> : null)) : null}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.udfRows.map((udfGrid, idxxx) => (
                                                    <tr id={idxxx} key={idxxx} className={this.state.isUdfDescriptionChecked ? "onclickChangeBackground" : ""}>
                                                        <td className="tdFocus"><label>{udfGrid.setUdfNo}</label></td>
                                                        {this.state.isColorRequired ? <td className="pad-0 hoverTable tdFocus">
                                                            <label className="piToolTip m0">
                                                                <div className="topToolTip">
                                                                    <label className="displayPointer">{udfGrid.udfColor}</label>
                                                                    <span className="topToolTipText">{udfGrid.udfColor}</span>
                                                                </div>
                                                            </label>
                                                        </td> : null}
                                                        {udfGrid.setUdfList != undefined ? udfGrid.setUdfList.map((data, kkey) => (
                                                            sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.isLovPO == "Y" ? <td className="pad-0 hoverTable tdFocus openModalBlueBtn" key={kkey} >
                                                                <input autoComplete="off" type="text" name="" className="inputTable " readOnly onKeyDown={(e) => this._handleKeyPressRow(e, `${data.udfType}`, idxxx)} id={data.udfType + idxxx} value={data.value} onClick={this.state.codeRadio != "setBased" ? (e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.udfGridId}`, `${data.value}`, `${data.udfType + idxxx}`) : null} />
                                                                {this.state.codeRadio != "setBased" ? <div className="purchaseTableDiv" onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.udfGridId}`, `${data.value}`, `${data.udfType + idxxx}`)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null}
                                                            </td> : null :
                                                                ((data.udfType == "SMUDFSTRING01" && (this.state.udf1Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING02" && (this.state.udf2Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING03" && (this.state.udf3Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING04" && (this.state.udf4Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING05" && (this.state.udf5Validation == true || data.isLovPO == "Y")) ||
                                                                    (data.udfType == "SMUDFSTRING06" && (this.state.udf6Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING07" && (this.state.udf7Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING08" && (this.state.udf8Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING09" && (this.state.udf9Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRIN10" && (this.state.udf10Validation == true || data.isLovPO == "Y")) ||
                                                                    (data.udfType == "SMUDFNUM01" && (this.state.udf11Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM02" && (this.state.udf12Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM03" && (this.state.udf13Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM04" && (this.state.udf14Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM05" && (this.state.udf15Validation == true || data.isLovPO == "Y")) ||
                                                                    (data.udfType == "SMUDFDATE01" && (this.state.udf16Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE02" && (this.state.udf17Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE03" && (this.state.udf18Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE04" && (this.state.udf19Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE05" && (this.state.udf20Validation == true || data.isLovPO == "Y"))) ? <td className="pad-0 hoverTable tdFocus openModalBlueBtn" key={kkey} >
                                                                        <input autoComplete="off" type="text" name="" className="inputTable " readOnly onKeyDown={(e) => this._handleKeyPressRow(e, `${data.udfType}`, idxxx)} id={data.udfType + idxxx} value={data.value} onClick={this.state.codeRadio != "setBased" ? (e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.udfGridId}`, `${data.value}`, `${data.udfType + idxxx}`) : null} />
                                                                        {this.state.codeRadio != "setBased" ? <div className="purchaseTableDiv" onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.udfGridId}`, `${data.value}`, `${data.udfType + idxxx}`)}>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                                <g fill="none" fillRule="evenodd">
                                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                                </g>
                                                                            </svg>
                                                                        </div> : null}
                                                                    </td> : null
                                                        )) : null}
                                                    </tr>))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div> : null}
                            </div>
                            <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                <div className="footerDivForm displayFlex">
                                    <div className="col-md-6 alignMiddle">
                                        <ul className="list-inline m-lft-0 m-top-10">
                                            <li>{this.state.dateValidationRes || this.state.vendorMrpPo == "" ? <button className="clear_button_vendor btnDisabled" type="reset" disabled>Clear</button> : <button className="clear_button_vendor" type="reset" onClick={(e) => this.reset(e)}>Clear</button>} </li>
                                            <li>{this.state.dateValidationRes || this.state.vendorMrpPo == "" ? <button className="save_button_vendor btnDisabled" type="button" disabled>Save</button> : <button className="save_button_vendor" id="saveButton" type="button" onClick={(e) => this.debounceFun(e)} >Save</button>} </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-6 poAmountBottom">
                                        <ul>
                                            <li><label>PO Quantity -</label><span>{this.state.poQuantity}</span></li>
                                            <li><label>PO Amount -</label><span>{this.state.poAmount}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                {this.state.sizeModal && <PiSizeModal ref={this.sizeRef} {...this.state} {...this.props} size="old" closePiSizeModal={() => this.closePiSizeModal()} />}
                {this.state.udfMapping ? <UdfMappingModal closeUdf={(e) => this.closeUdf(e)} {...this.props}{...this.state} updateUdf={(e) => this.updateUdf(e)} udfType={this.state.udfType} updateUdfSetvalue={(e) => this.updateUdfSetvalue(e)} udfName={this.state.udfName} setValue={this.state.setValue} udfRow={this.state.udfRow} udfMappingAnimation={this.state.udfMappingAnimation} closeUdfMappingModal={(e) => this.openUdfMappingModal(e)} /> : null}
                {this.state.articleModal ? <ArticleModal {...this.props}{...this.state} vendorMrpPo={this.state.vendorMrpPo} vendorPoState={this.state.vendorPoState} mrpCloseModal={(e) => this.mrpCloseModal(e)} updateMrpState={(e) => this.updateMrpState(e)} articleModalAnimation={this.state.articleModalAnimation} rowId={this.state.rowIdentity} closeMrpModal={(e) => this.closeMrpModal(e)} /> : null}
                {this.state.supplierModal ? <SupplierModal {...this.props} {...this.state} city={this.state.city} departmentCode={this.state.departmentCode} siteCode={this.state.siteCode} department={this.state.department} supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null}
                {this.state.colorModal ? <PiColorModal {...this.props} {...this.state} deselectallProp={(e) => this.deselectallProp(e)} colorListValue={this.state.colorListValue} colorCode={this.state.colorCode} hl3Code={this.state.departmentCode} updateColorList={(e) => this.updateColorList(e)} updateColorState={(e) => this.updateColorState(e)} colorModalAnimation={this.state.colorModalAnimation} colorValue={this.state.colorValue} colorRow={this.state.colorRow} closePiColorModal={(e) => this.closePiColorModal(e)} closePiiColorModal={(e) => this.openPoColorModal(e)} /> : null}
                {this.state.itemModal ? <ItemCodeModal {...this.props} {...this.state} desc6={this.state.desc6} poRows={this.state.poRows} descId={this.state.descId} itemModalAnimation={this.state.itemModalAnimation} startRange={this.state.startRange} endRange={this.state.endRange} code={this.state.vendorMrpPo} openItemCode={() => this.openItemCode()} closeItemmModal={() => this.closeItemmModal()} updateItem={(e) => this.updateItem(e)} /> : ""}
                {this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} isTransporterDependent={this.state.isTransporterDependent} city={this.state.city} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null}
                {this.state.loadIndentModal ? <LoadIndent {...this.props} {...this.state} indentValue={this.state.indentValue} updateLoadIndentState={(e) => this.updateLoadIndentState(e)} loadIndentCloseModal={(e) => this.openloadIndentSelection(e)} onCloseLoadIndent={(e) => this.onCloseLoadIndent(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null}
                {this.state.vendorDesignModal ? <VendorDesignModal {...this.props} {...this.state} updateVendorDesign={(e) => this.updateVendorDesign(e)} hl3Code={this.state.departmentCode} department={this.state.department} vendorDesignVal={this.state.vendorDesignVal} vendorDesignAnimation={this.state.vendorDesignAnimation} closeVendorModal={() => this.closeVendorModal()} vendorId={this.state.vendorId} itemType={this.state.itemType} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.itemUdfMappingModal ? <ItemUdfMappingModal {...this.props} {...this.state} department={this.state.department} itemArticleCode={this.state.itemArticleCode} itemUdfId={this.state.itemUdfId} vendorMrpPo={this.state.vendorMrpPo} itemUdfName={this.state.itemUdfName} itemUdfType={this.state.itemUdfType} itemUdfUpdate={(e) => this.itemUdfUpdate(e)} {...this.props} {...this.state} articleName={this.state.articleName} updateItemValue={(e) => this.updateItemValue(e)} itemUdfMappingAnimation={this.state.itemUdfMappingAnimation} itemValue={this.state.itemValue} closeItemUdfModal={(e) => this.closeItemUdfModal()} openItemUdfModal={(e) => this.openItemUdfModal(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmModal ? <ConfirmModalPo {...this.props} {...this.state} pi={this.state.pi} radioChange={this.state.radioChange} onnRadioChange={(e) => this.onnRadioChange(e)} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
                {this.state.imageModal ? <PiImageModal {...this.state} {...this.props} file={this.state.imageState} imageName={this.state.imageName} updateImage={(e) => this.updateImage(e)} imageModalAnimation={this.state.imageModalAnimation} closePiImageModal={(e) => this.closePiImageModal(e)} closeImageModal={(e) => this.openImageModal(e)} imageRowId={this.state.imageRowId} /> : null}
                {this.state.itemDetailsModal ? <ItemDetailsModal {...this.props} {...this.state} itemDetailName={this.state.itemDetailName} itemId={this.state.itemId} updateItemDetails={(e) => this.updateItemDetails(e)} updateItemDetailValue={(e) => this.updateItemDetailValue(e)} itemDetailCode={this.state.itemDetailCode} itemDetailValue={this.state.itemDetailValue} hl3Code={this.state.departmentCode} department={this.state.department} closeItemModal={(e) => this.closeItemModal(e)} itemDetailsModalAnimation={this.state.itemDetailsModalAnimation} itemDetailsModal={this.state.itemDetailsModal} openItemModal={(e) => this.openItemModal(e)} itemType={this.state.itemType} itemId={this.state.itemId} /> : null}
                {this.state.departmentModal ? <SetDepartmentModal {...this.props} {...this.state} onCloseDepartmentModal={(e) => this.onCloseDepartmentModal(e)} updateDepartment={(e) => this.updateDepartment(e)} departmentSetBasedAnimation={this.state.departmentSetBasedAnimation} setDepartment={this.state.setDepartment} hl3CodeDepartment={this.state.hl3CodeDepartment} /> : null}
                {this.state.orderNumber ? <OrderNumberModal {...this.props} {...this.state} orderSet={this.state.orderSet} hl3CodeDepartment={this.state.hl3CodeDepartment} poSetVendorCode={this.state.poSetVendorCode} updateOrderNumber={(e) => this.updateOrderNumber(e)} orderSet={this.state.orderSet} departmentSet={this.state.departmentSet} poSetVendor={this.state.poSetVendor} closeSetNumber={(e) => this.closeSetNumber(e)} orderNumberModalAnimation={this.state.orderNumberModalAnimation} /> : null}
                {this.state.setVendor ? <SetVendorModal {...this.props} {...this.state} onCloseVendor={(e) => this.onCloseVendor(e)} updateVendorPo={(e) => this.updateVendorPo(e)} /> : null}
                {this.state.resetAndDelete ? <ResetAndDelete {...this.props} {...this.state} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeResetDeleteModal={(e) => this.closeResetDeleteModal(e)} DeleteRowPo={(e) => this.DeleteRowPo(e)} resetRows={(e) => this.resetRows(e)} /> : null}
                {this.state.hsnModal ? <HsnCodeModal {...this.props} {...this.state} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /> : null}
                {this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteName={this.state.siteName} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null}
                {this.state.openIcodeModal ? <IcodeModalInner {...this.props} {...this.state} icodeId={this.state.icodeId} updateIcode={(e) => this.updateIcode(e)} icodesArray={this.state.icodesArray} closePoIcode={(e) => this.closePoIcode(e)} /> : null}
                {this.state.itemBarcodeModal ? <NewIcodeModal {...this.props} {...this.state} icodeModalAnimation={this.state.icodeModalAnimation} articleCode={this.state.vendorMrpPo} supplierCode={this.state.slCode} siteCode={this.state.siteCode} closeNewIcode={(e) => this.closeNewIcode(e)} itemCodeList={this.state.itemCodeList} updateNewIcode={(e) => this.updateNewIcode(e)} /> : null}
                {this.state.deleteConfirmModal ? <DeleteModalPo closeDeleteConfirmModal={(e) => this.closeDeleteConfirmModal(e)} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} handleRemoveSpecificSecRow={(e) => this.handleRemoveSpecificSecRow(e)} deleteGridId={this.state.deleteGridId} deleteSetNo={this.state.deleteSetNo} /> : null}
                {this.state.multipleErrorpo ? <AlertNotificationModal closeMultipleError={(e) => this.closeMultipleError(e)} lineError={this.state.lineError} /> : null}
                {this.state.cityModal ? <CityModal {...this.props}{...this.state} city={this.state.city} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.discountModal ? <DiscountModal {...this.props}{...this.state} {...this.props} selectedDiscount={this.state.selectedDiscount} discountGrid={this.state.discountGrid} closeDiscountModal={(e) => this.closeDiscountModal(e)} isDiscountMap={this.state.isDiscountMap} vendorMrpPo={this.state.vendorMrpPo} updateDiscountModal={(e) => this.updateDiscountModal(e)} discountMrp={this.state.discountMrp} /> : null}

            </div>
        );
    }
}
export default PurchaseOrderForm;