import React from 'react';
import IcodeModal from './poIcodeModal'

export default class IcodeModalInner extends React.Component {
    constructor(props) {
        super(props)
        this.textInput = React.createRef();
        this.state = {
            icodesArray: [...this.props.icodesArray],
            search: "",
            openModal: false,
            sizeColor: "",
            siteId: "",
            articleId: "",
            desc6: "",
            sizeCode: "",
            sizeName: "",
            colorCode: "",
            colorName: "",
            selectedIcode: "",
            udfIcode:"",
            searchBy:"startWith"
        }

    }
    componentWillMount() {

        this.setState({
            icodesArray: [...this.props.icodesArray]

        })
    }
    componentDidMount() {
        if(window.screen.width < 1200){      
           this.textInput.current.blur();   
         }else{  
            this.textInput.current.focus();}
    }
    componentWillReceiveProps(nextProps) {
                   if(nextProps.purchaseIndent.poItemBarcode.isSucces){
                            //  this.updateUdf(nextProps.purchaseIndent.poItemBarcode.data.resource)

                   }
    }
    updateIcodesArray(data) {
        let icodesArray = this.state.icodesArray
        for (let i = 0; i < icodesArray.length; i++) {
            if (icodesArray[i].sizeValue + icodesArray[i].colorValue == sizeColor) {
                icodesArray[i].icode = data.icodeId
               
            }
        }
        this.setState({
            icodesArray
        })
        let payload = {
            itemId: data.icodeId
        }
        this.props.poItemBarcodeRequest(payload)
    }
    // updateUdf(data){
    //       let icodesArray = this.state.icodesArray
    //     for (let i = 0; i < icodesArray.length; i++) {
    //         if (icodesArray[i].sizeValue + icodesArray[i].colorValue == sizeColor) {
    //             icodesArray[i].icodeUdf = data
               
    //         }
    //     }
    //     this.setState({
    //         icodesArray
    //     })
    // }
    handleChange(e) {
        this.setState({
            search: e.target.value
        })
    }
    closeModal() {
        this.setState({
            openModal: false,
        })
    }
    openIcode(data, siteId, articleId, desc6, sizeCode, sizeName, colorCode, colorName, selectedIcode) {
        this.setState({
            sizeColor: data,
            openModal: true,
            siteId: siteId,
            articleId: articleId,
            desc6: desc6,
            sizeCode: sizeCode,
            sizeName: sizeName,
            colorCode: colorCode,
            colorName: colorName,
            selectedIcode: selectedIcode

        })
        let payload = {
            type: "1",
            no: 1,
            search: "",
            siteId: siteId,
            articleId: articleId,
            desc6: desc6,
            cat5Code: sizeCode,
            cat5Name: sizeName,
            cat6Code: colorCode,
            cat6Name: colorName
        }
        this.props.getPoItemCodeRequest(payload)

    }
    _handleKeyDown = (e) => {
        if (e.key === "Tab" || e.key =="ArrowDown") {
            var rows = document.getElementById("tableExample").getElementsByTagName("tr").length;
            if (rows != 0) {
                window.setTimeout(function () {
                    document.getElementById("button0").focus();
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("closeButton").focus();
                }, 0)
            }
        }
     

    }
    handleButtonDown(idd, nextId,e) {
        if (e.key === "ArrowDown") {
            let id = nextId + 1
            if (document.getElementById("button" + id) != undefined) {

                window.setTimeout(function () {

                    document.getElementById("button" + id).focus();
                }, 0)
            }
        }
        if (e.key == "ArrowUp") {
            let id = nextId - 1
            if (!id < 0) {
                window.setTimeout(function () {

                    document.getElementById("button" + id).focus();
                }, 0)
            }
        }
        if (e.key == "Tab") {
             window.setTimeout(function () {
            document.getElementById("closeButton").focus();
             },0)
        }
        if (e.key === "F2" || e.key === "F7") {
             window.setTimeout(function () {
            document.getElementById(idd).click();
             },0)
        }
         if (e.key === "Enter") {
            window.setTimeout(function () {
                document.getElementById("saveButton").click();
            }, 0)
        }

    }
    saveButton(e) {
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("saveButton").blur();
                document.getElementById("search").focus();
            }, 0)
        }
        if (e.key === "Enter") {
            window.setTimeout(function () {
                document.getElementById("saveButton").click();
            }, 0)
        }
    }
    closeButton(e) {
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur();
                document.getElementById("saveButton").focus();
            }, 0)
        }
        if (e.key === "Enter") {
            window.setTimeout(function () {
                document.getElementById("closeButton").click();
            }, 0)
        }
    }
    onDone() {
        this.props.updateIcode(this.state.icodesArray)
        this.props.closePoIcode()
    }

    render() {

        const { search } = this.state
        var result = _.filter(this.state.icodesArray, function (data) {
            return _.startsWith(data.sizeValue.toLowerCase(), search.toLowerCase()) || _.startsWith(data.colorValue.toLowerCase(), search.toLowerCase());
        });
        console.log(result)
        return (
            <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block newPoModal">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="modal_Color">
                                    <div className="modal-top alignMiddle">
                                        <div className="col-md-6 pad-0">
                                            <h2 className="select_name-content m0">Purchase Order with ICode</h2>
                                        </div>
                                        <div className="col-md-6 pad-0 text-right">
                                            <button type="button" id="closeButton" className="discardBtns selectAllFocus m-rgt-10" onKeyDown={(e) => this.closeButton(e)} onClick={(e) => this.props.closePoIcode(e)}>Close</button>
                                            <button type="button" id="saveButton" className="saveBtnBlue dataPiBtn" onKeyDown={(e) => this.saveButton(e)} onClick={(e) => this.onDone(e)} >Save</button>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 displayInline modalMidPad">
                                        <div className="modalMid">
                                            <form className="newSearch floatRight m-bot-0 width_30 m-top-5">
                                                <input type="text" autoComplete="off" autoCorrect="off" placeholder="Search..." id="search" ref={this.textInput} value={search} onKeyDown={this._handleKeyDown} onChange={(e) => this.handleChange(e)} className="search-box width100" autoComplete="off" />
                                            </form>
                                            <div className="modal_table modalTablescroll">
                                                <table className="table tableModal vendorMrpMain m-top-25">
                                                    <thead className="tableHeadBg">
                                                        <tr>
                                                            <th className="wid100px">Item Code</th>
                                                            <th>Valid From</th>
                                                            <th>Valid To</th>
                                                            <th>Set Ratio</th>
                                                            <th>Site Id</th>
                                                            <th>Article_ID</th>
                                                            <th>Cname5</th>
                                                            <th>Cname6</th>

                                                            <th>Desc1</th>
                                                            <th>MRP</th>

                                                            <th>Qty</th>
                                                            <th>Rate</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tableExample">
                                                        {result.length != 0 ? result.map((data, iukey) => (
                                                            <tr key={iukey}>
                                                                <td ><button type="button" id={"button" + iukey} onKeyDown={(e) => this.handleButtonDown("button" + iukey, iukey ,e)} onClick={(e) => this.openIcode(data.sizeValue + data.colorValue, data.siteId, data.articleId, data.mrp, data.sizeCode, data.sizeValue, data.colorCode, data.colorValue, data.icode)}>{data.icode != "" ? data.icode : "Choose Item Code"}<i className="fa fa-angle-down"></i></button></td>
                                                                <td>{data.validFrom}</td>
                                                                <td>{data.validTo}</td>
                                                                <td>{data.setRatio}</td>
                                                                <td>{data.siteId}</td>
                                                                <td>{data.articleId}</td>
                                                                <td>{data.sizeValue}</td>
                                                                <td>{data.colorValue}</td>
                                                                <td>{data.desc1}</td>
                                                                <td>{data.mrp}</td>
                                                                <td>{data.qty}</td>
                                                                <td>{data.rate}</td>

                                                            </tr>
                                                        )) : <tr><td colSpan="12">No Data Found</td> </tr>}


                                                    </tbody>
                                                </table>
                                            </div>
                                            {/*<div className="m-top-7">
                                                <div className="pagerDiv pagerWidth65 m0 modalPagination newPagination">
                                                    <ul className="list-inline pagination paginationWidth50">
                                                        <li >
                                                            <button className="PageFirstBtn pointerNone" type="button"  >
                                                                First</button>
                                                        </li>
                                                        <li >
                                                            <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                                Prev </button>
                                                        </li>
                                                        <li>
                                                            <button className="PageFirstBtn" type="button">
                                                                <span>1/2</span>
                                                            </button>
                                                        </li>
                                                        <li >
                                                            <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                                Next </button>
                                                        </li>
                                                        <li >
                                                            <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                                Last </button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>*/}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.openModal ? <IcodeModal {...this.props} {...this.state} closeModal={(e) => this.closeModal(e)} /> : null}
            </div >
        )
    }
}