import React from 'react';
import ToastLoader from "../loaders/toastLoader";
export default class IcodeModal extends React.Component {
    constructor(props) {
        super(props)
        this.textInput = React.createRef();
        this.state = {
            search: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            getPoItemCodeState: [],
            selectedIcode: [...this.props.selectedIcode],
            searchBy: "contains",
                   focusedLi: "",
        }
    }
    componentDidMount() {
         if (this.props.isModalShow) {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
         }
    }
    componentWillMount() {
        this.setState({
            selectedIcode: [...this.props.selectedIcode],
            search: this.props.isModalShow ? "" : this.props.icodeSearch

        })
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,
            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getPoItemCode.data.currPage - 1,
                    search: this.state.search,
                    siteId: this.props.siteId,
                    articleId: this.props.articleId,
                    desc6: this.props.desc6,
                    sizeCode: this.props.sizeCode,
                    sizeName: this.props.sizeName,
                    colorCode: this.props.colorCode,
                    colorName: this.props.colorName,
                    searchBy: this.state.searchBy
                }
                this.props.getPoItemCodeRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,
            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage != this.props.purchaseIndent.getPoItemCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                    search: this.state.search,
                    siteId: this.props.siteId,
                    articleId: this.props.articleId,
                    desc6: this.props.desc6,
                    sizeCode: this.props.sizeCode,
                    sizeName: this.props.sizeName,
                    colorCode: this.props.colorCode,
                    colorName: this.props.colorName,
                    searchBy: this.state.searchBy
                }
                this.props.getPoItemCodeRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,
            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage <= this.props.purchaseIndent.getPoItemCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.search,
                    siteId: this.props.siteId,
                    articleId: this.props.articleId,
                    desc6: this.props.desc6,
                    sizeCode: this.props.sizeCode,
                    sizeName: this.props.sizeName,
                    colorCode: this.props.colorCode,
                    colorName: this.props.colorName,
                    searchBy: this.state.searchBy
                }
                this.props.getPoItemCodeRequest(data)
            }

        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.getPoItemCode.isSuccess) {
            if (nextProps.purchaseIndent.getPoItemCode.data.resource != null) {


                this.setState({
                    getPoItemCodeState: nextProps.purchaseIndent.getPoItemCode.data.resource,
                    prev: nextProps.purchaseIndent.getPoItemCode.data.prePage,
                    current: nextProps.purchaseIndent.getPoItemCode.data.currPage,
                    next: nextProps.purchaseIndent.getPoItemCode.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getPoItemCode.data.maxPage,
                })
            } else {
                this.setState({
                    getPoItemCodeState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                    if (this.props.isModalShow) {
                        this.textInput.current.focus();

                    }
                    else if (!this.props.isModalShow) {
                        if (nextProps.purchaseIndent.getPoItemCode.data.resource != null) {
                            this.setState({
                                focusedLi: nextProps.purchaseIndent.getPoItemCode.data.resource[0].itemId
                            })
                            document.getElementById(nextProps.purchaseIndent.getPoItemCode.data.resource[0].itemId) != null ? document.getElementById(nextProps.purchaseIndent.getPoItemCode.data.resource[0].itemId).focus() : null
                        }



                    }
                }

        }

    }
    selectedData(icode) {

        for (let i = 0; i < this.state.getPoItemCodeState.length; i++) {
            if (this.state.getPoItemCodeState[i].itemId == icode) {

                this.setState({
                    selectedIcode: this.state.getPoItemCodeState[i].itemId
               }, () => {
                    if (!this.props.isModalShow) {
                        this.ondone()
                    }
                })
            }

        }

    }


    ondone() {
        var sCode = "";
        var pattern = "";
        let data = {}
        if (this.state.selectedIcode != "") {

            if (this.state.selectedIcode == this.props.selectedIcode) {
                this.props.closeModal()
            } else {
                for (let i = 0; i < this.state.getPoItemCodeState.length; i++) {
                    if (this.state.getPoItemCodeState[i].itemId == this.state.selectedIcode) {
                        data = {
                            icodeId: this.state.getPoItemCodeState[i].itemId,


                        }
                    }
                }
                let t = this
                setTimeout(function () {
                    t.props.updateIcodesArray(data);
                    t.props.closeModal()
                }, 100)
            }

            this.setState(
                {
                    search: "",
                })

            this.props.closeModal()


        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)

        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }

    handleChange(e) {
        if (e.target.id == "search") {

            this.setState(
                {
                    search: e.target.value
                },

            );
        } else if (e.target.id == "searchByPOitem") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if(this.state.search!=""){
                    let data = {
                        type: this.state.type,
                        no: 1,
                        search: this.state.search,
                        siteId: this.props.siteId,
                        articleId: this.props.articleId,
                        desc6: this.props.desc6,
                        sizeCode: this.props.sizeCode,
                        sizeName: this.props.sizeName,
                        colorCode: this.props.colorCode,
                        colorName: this.props.colorName,
                        searchBy: this.state.searchBy
                    }
                    this.props.getPoItemCodeRequest(data)
                }
                })
        }
    }

    onSearch(e) {

        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter data on search input",
                toastLoader: true,

            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {

            this.setState({
                type: 2,

            })
            let data = {
                type: 2,
                no: 1,
                search: this.state.search,
                siteId: this.props.siteId,
                articleId: this.props.articleId,
                desc6: this.props.desc6,
                sizeCode: this.props.sizeCode,
                sizeName: this.props.sizeName,
                colorCode: this.props.colorCode,
                colorName: this.props.colorName,
                searchBy: this.state.searchBy
            }
            this.props.getPoItemCodeRequest(data)
        }


    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    _handleKeyDown = (e) => {
        if (e.key == "Tab") {
            if (this.state.getPoItemCodeState.length != 0) {
                this.setState({
                    selectedIcode: this.state.getPoItemCodeState[0].itemId
                })
                let icode = this.state.getPoItemCodeState[0].itemId
                this.selectedData(icode)
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById(icode).focus()
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("search").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.onDone();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("saveButton").blur()
                document.getElementById("search").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {
            this.props.closeModal();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("saveButton").focus()
            }, 0)
        }
    }
    focusDone(e) {

        if (e.key === "Tab") {


            window.setTimeout(function () {


                document.getElementById("saveButton").focus()
            }, 0)
        }
        if(e.key ==="Enter"){
            this.ondone()
        }
    }

  selectLi(e, code) {
        let getPoItemCodeState = this.state.getPoItemCodeState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < getPoItemCodeState.length; i++) {
                if (getPoItemCodeState[i].itemId == code) {
                    index = i
                }
            }
            if (index < getPoItemCodeState.length - 1 || index == 0) {
                document.getElementById(getPoItemCodeState[index + 1].itemId) != null ? document.getElementById(getPoItemCodeState[index + 1].itemId).focus() : null

                this.setState({
                    focusedLi: getPoItemCodeState[index + 1].itemId
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < getPoItemCodeState.length; i++) {
                if (getPoItemCodeState[i].itemId == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(getPoItemCodeState[index - 1].itemId) != null ? document.getElementById(getPoItemCodeState[index - 1].itemId).focus() : null

                this.setState({
                    focusedLi: getPoItemCodeState[index - 1].itemId
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.props.closeModal(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.getPoItemCodeState.length!=0?
                document.getElementById(this.state.getPoItemCodeState[0].itemId).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.getPoItemCodeState.length!=0){
                document.getElementById(this.state.getPoItemCodeState[0].itemId).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.props.closeModal(e)
        }


    }
  

    render() {
        if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

        const { search } = this.state
        return (
           this.props.isModalShow ?  <div>
                <div className="modal display_block" id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="modal_Indent display_block newPoModal">
                        <div className="col-md-12 col-sm-12 previousAddortmentModal modalpoColor modal-content modalShow pad-0">
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="modal_Color">
                                    <div className="modal-top alignMiddle">
                                        <div className="col-md-6 pad-0">
                                            <h2 className="select_name-content m0">Purchase Order with ICode</h2>
                                        </div>
                                        <div className="col-md-6 pad-0 text-right">
                                            <button type="button" id="closeButton" className="discardBtns m-rgt-10" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.props.closeModal(e)}>Close</button>
                                            <button type="button" id="saveButton" className="saveBtnBlue" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Save</button>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-sm-12 displayInline modalMidPad">
                                        <div className="modal-content modalMid">
                                            <h4>Choose ICode for Article ID </h4>
                                            {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByPOitem" name="searchByPOitem" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                
                                                <option value="contains">Contains</option>
                                                <option value="startWith"> Start with</option>
                                                <option value="endWith">End with</option>

                                            </select> : null}
                                            <form className="newSearch m-bot-0 width_35 m-top-15">
                                                <input type="text" autoCorrect="off" autoComplete="off" placeholder="Search..." id="search" ref={this.textInput} onKeyPress={this._handleKeyPress} onkeydown={this._handleKeyDown} value={search} onChange={(e) => this.handleChange(e)} className="search-box width100" autoComplete="off" />
                                            </form>
                                            <div className="modal_table">
                                                <table className="table tableModal vendorMrpMain m-top-25">
                                                    <thead>
                                                        <tr>
                                                            <th className="wid100px">Action</th>
                                                            <th>Item Codes</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.getPoItemCodeState.length != 0 ? this.state.getPoItemCodeState.map((data, iukey) => (

                                                            <tr key={iukey} onClick={() => this.selectedData(`${data.itemId}`)}>
                                                                <td className="blueRadioBtn">  <label className="select_modalRadio">
                                                                    <input type="radio" name="vendorMrpCheck" id={data.itemId} onKeyDown={(e) => this.focusDone(e)}  checked={this.state.selectedIcode == data.itemId ? true : false} readOnly/>
                                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                                </label>
                                                                </td>
                                                                <td>{data.itemId}</td>

                                                            </tr>
                                                        )) : <tr colSpan="3">No data Found</tr>}

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="m-top-7">
                                                <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                                    <ul className="list-inline pagination paginationWidth50">
                                                        {this.state.current == 1 || this.state.current == 0 ? <li >
                                                            <button className="PageFirstBtn" type="button"  >
                                                                First
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                                    First
                  </button>
                                                            </li>}
                                                        {this.state.prev != 0 ? <li >
                                                            <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                                Prev
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn" type="button" disabled>
                                                                    Prev
                  </button>
                                                            </li>}
                                                        <li>
                                                            <button className="PageFirstBtn pointerNone" type="button">
                                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                                            </button>
                                                        </li>
                                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                            <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                                Next
                  </button>
                                                        </li> : <li >
                                                                <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                    Next
                  </button>
                                                            </li> : <li >
                                                                <button className="PageFirstBtn borderNone" type="button" disabled>
                                                                    Next
                  </button>
                                                            </li>}



                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div >: <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.getPoItemCodeState != undefined || this.state.getPoItemCodeState.length != 0 ?
                            this.state.getPoItemCodeState.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.itemId}`)} id={data.itemId} className={this.state.selectedIcode == `${data.itemId}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.itemId)}>
                                    <span className="vendor-details">
                                        <span className="vd-name">{data.itemId}</span>

                                       
                                    </span>
                                </li>)) : <li><span>No Data Found</span></li>}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.props.closeModal(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn margin-180">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>


        )
    }
}