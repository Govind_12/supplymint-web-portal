import React from 'react';
import SectionDataSelection from "../../components/purchaseIndent/sectionDataSelection";
import addMore from '../../assets/add.svg'
import ToastLoader from '../loaders/toastLoader';
import CnameModal from './cnameModel';
import PoError from '../loaders/poError';
import { CONFIG } from "../../config/index";
import axios from 'axios';
import ArticleName from './articleName';
class UdfMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            poErrorMsg: false,
            filter: false,
            filterBar: true,
            udfMappingData: [],
            isCompulsary: "",
            displayName: "",
            udfType: "",
            udfData: this.props.udfData,
            udf: this.props.udf,
            selectionModal: false,
            selectionAnimation: false,
            divisionUdf: this.props.divisionUdf,
            sectionUdf: this.props.sectionUdf,
            departmentUdf: this.props.departmentUdf,
            divisionState: this.props.divisionState,
            itemUdfMapping: this.props.itemUdfMapping,
            // __________________________________item udfmapping________________________
            newItemUdf: false,
            nameerror: false,
            disNameerror: false,
            maperror: false,
            udfValueerror: false,
            udfName: "",
            hl3Name: "",
            displayUdf: "",
            mapUdf: "",
            udfValue: "",
            payloadId: [],
            ext: "N",
            itemUdfMappingData: [],
            toastLoader: false,
            toastMsg: "",

            codeRadio: this.props.codeRadioUdf,
            description: this.props.descriptionUdf,
            articleName: this.props.articleNameUdf,
            articleCode: this.props.articleCodeUdf,
            codeUdf: this.props.codeUdf,

            showArticleUdf: this.props.showArticleUdf,
            articleCheck: this.props.articleCheckUdf,
            cnameModal: false,
            cnameModalAnimation: false,
            cnameData: [],
            articleCat: false,
            articleNameModal: false,
            articleNameModalAnimation: false,
            udfDept: this.props.udfDept,
            no: 1,
            type: this.props.udfType,
            search: this.props.searchUdf,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            addNewImg: true,
            hl3Code: this.props.hl3Code,
            dropdown: false,
            searchBy:"contains",
            exportToExcel: false

        };
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }

    componentDidMount() {
        window.setTimeout(function () {chooseBtn
            document.getElementById("dropdownMenu1").focus()
        }, 0)
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
            })
            if (this.props.purchaseIndent.itemUdfMapping.data.currPage != 0) {
                let data = {

                    no: this.props.purchaseIndent.itemUdfMapping.data.currPage - 1,
                    type: this.state.type,
                    itemUdfType: this.state.udf,
                    ispo: "false",
                    search: this.props.searchUdf,
                    description: this.state.codeRadio == "udf" ? this.state.description : "",
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCodeUdf,
                    hl4Name: this.props.articleNameUdf,
                    searchBy:this.state.searchBy


                }
                this.props.itemUdfMappingRequest(data);
            }

        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
            })
            if (this.props.purchaseIndent.itemUdfMapping.data.currPage != this.props.purchaseIndent.itemUdfMapping.data.maxPage) {
                let data = {

                    no: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                    type: this.state.type,
                    itemUdfType: this.state.udf,
                    ispo: "false",
                    search: this.props.searchUdf,
                    description: this.state.codeRadio == "udf" ? this.state.description : "",
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCodeUdf,
                    hl4Name: this.props.articleNameUdf,
                    searchBy:this.state.searchBy


                }
                this.props.itemUdfMappingRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
            })
            if (this.props.purchaseIndent.itemUdfMapping.data.currPage <= this.props.purchaseIndent.itemUdfMapping.data.maxPage) {
                let data = {

                    no: 1,
                    type: this.state.type,
                    itemUdfType: this.state.udf,
                    ispo: "false",
                    search: this.props.searchUdf,
                    description: this.state.codeRadio == "udf" ? this.state.description : "",
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCodeUdf,
                    hl4Name: this.props.articleNameUdf,
                    searchBy:this.state.searchBy


                }
                this.props.itemUdfMappingRequest(data)
            }

        }
    }

    handleSearch(e) {
        if (e.target.id == "search") {
            this.setState({
                search: e.target.value
            })
        }
        this.props.searchUdfFun(e.target.value)


    }
    onSearchBlur(e) {


    }
    componentWillMount() {
        this.setState({
            udfData: this.props.udfData,
            divisionState: this.props.divisionState,
            itemUdfMapping: this.props.itemUdfMapping,
            type: this.props.udfType,
            search: this.props.searchUdf,
            description: this.props.descriptionUdf,
            articleName: this.props.articleNameUdf,
            articleCode: this.props.articleCodeUdf,
            codeUdf: this.props.codeUdf,

            showArticleUdf: this.props.showArticleUdf,
            articleCheck: this.props.articleCheckUdf,
             codeRadio: this.props.codeRadioUdf,
       
          udfDept: this.props.udfDept,
          
        })
        if (this.props.purchaseIndent.itemUdfMapping.isSuccess) {
            if (this.props.purchaseIndent.itemUdfMapping.data.resource != null) {
                this.setState({
                    prev: this.props.purchaseIndent.itemUdfMapping.data.prePage,
                    current: this.props.purchaseIndent.itemUdfMapping.data.currPage,
                    next: this.props.purchaseIndent.itemUdfMapping.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.itemUdfMapping.data.maxPage,
                })
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        const t = this
        setTimeout(function () {
            t.setState({
                itemUdfMapping: t.props.itemUdfMapping,
                type: t.props.udfType,
                search: t.props.searchUdf,
            })
        }, 10);
        if (nextProps.purchaseIndent.cname.isSuccess) {
            if (nextProps.purchaseIndent.cname.data.resource != null) {

                let c = []
                for (let i = 0; i < nextProps.purchaseIndent.cname.data.resource.length; i++) {


                    let x = nextProps.purchaseIndent.cname.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    cnameData: c
                })
            }
        }
        if (nextProps.purchaseIndent.itemUdfMapping.isSuccess) {
            if (nextProps.purchaseIndent.itemUdfMapping.data.resource != null) {
                this.setState({
                    prev: nextProps.purchaseIndent.itemUdfMapping.data.prePage,
                    current: nextProps.purchaseIndent.itemUdfMapping.data.currPage,
                    next: nextProps.purchaseIndent.itemUdfMapping.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.itemUdfMapping.data.maxPage,
                })
            } else {
                this.setState({
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.purchaseIndent.updateItemUdf.isSuccess) {
            this.setState({
                payloadId: []
            })
        }
    }
    updateState(upData) {
        var sectionData = []

        this.setState({

            divisionUdf: upData.hl1Name,
            sectionUdf: upData.hl2Name,
            departmentUdf: upData.hl3Name,
            hl3Code: upData.hl3Code,
            editDisplayName: upData.editDisplayName

        })
        this.props.chooseDataUdf(upData)
        window.setTimeout(() => {
            document.getElementById("dropdownMenu1").focus();
        }, 0);
    }

    closeSelection(e) {
        this.setState({
            selectionModal: false,
            selectionAnimation: !this.state.selectionAnimation

        });
        window.setTimeout(() => {
            document.getElementById("chooseBtn").focus();
        }, 0);
    }
    openSelection(e) {
        var data = {
            no: 1,
            type: "",
            hl1name: "",
            hl2name: "",
            hl3name: "",
            search: ""
        }
        this.props.divisionSectionDepartmentRequest(data);
        this.setState({
            department: this.state.departmentUdf,
            selectionModal: true,
            selectionAnimation: !this.state.selectionAnimation

        });
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return false;
            }
        }
    }
    // }
    onnChange(name) {

        this.setState({
            udf: name,
            dropdown: false
        })

        let data = {
            udf: name,
        }
        this.props.updateUdf(data)
          document.getElementById("searchBtn").focus();
    }


    getAllDataUdf() {
        if (this.state.codeRadio == "udf") {
            if (this.state.departmentUdf != "" && this.state.udf != "") {
                var data = {

                    type: 1,
                    no: 1,
                    itemUdfType: this.state.udf,
                    ispo: "false",
                    search: "",
                    description: this.state.description,
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCodeUdf,
                    hl4Name: this.props.articleNameUdf,
                    searchBy:this.state.searchBy






                }
              
                this.props.itemUdfMappingRequest(data)
            }
            else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Department and UDF is Mandatory"
                })
            }
        } else {
            if (this.state.departmentUdf != "" && this.state.udfDept != "") {
                var data = {
                    type: 1,
                    no: 1,
                    itemUdfType: this.state.udfDept,
                    ispo: "false",
                    search: "",
                    description: "",
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCodeUdf,
                    hl4Name: this.props.articleNameUdf,
                    searchBy:this.state.searchBy
                }

                this.props.itemUdfMappingRequest(data)
            } else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Department and UDF is Mandatory"
                })
            }
        }

    }
    // __________________________________HANDLECHANGE_______________________


    handleChange(id, e) {
        let r = this.state.itemUdfMapping
        let payloadId = this.state.payloadId
        if (!payloadId.includes(id)) {
            payloadId.push(id)
        }
        this.setState({
            payloadId: payloadId
        })
        if (e.target.id == "changename") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].id == id) {
                    r[i].name = e.target.value
                }


            }
        } else if (e.target.id == "displayUdf") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].id == id) {
                    r[i].displayUdf = e.target.value
                }


            }
        } else if (e.target.id == "udfType") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].id == id) {
                    r[i].udfType = e.target.value
                }


            }
        } else if (e.target.id == "changevalue") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].id == id) {
                    r[i].value = e.target.value
                }


            }
        } else if (e.target.id == "map") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].id == id) {
                    r[i].map = e.target.value
                }


            }
        }else if(e.target.id=="searchByudfmap"){
            this.setState({
                searchBy:e.target.value
            },()=>{
                  let data = {

                    no: 1,
                    type: this.state.type,
                    itemUdfType: this.state.udf,
                    ispo: "false",
                    search: this.props.searchUdf,
                    description: this.state.codeRadio == "udf" ? this.state.description : "",
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCodeUdf,
                    hl4Name: this.props.articleNameUdf,
                    searchBy:this.state.searchBy


                }
                this.props.itemUdfMappingRequest(data)
            })

        }
        this.setState({
            itemUdfMapping: r
        })


    }
    handleCheck(id, value) {
        let r = this.state.itemUdfMapping
        let payloadId = this.state.payloadId
        if (!payloadId.includes(id)) {
            payloadId.push(id)
        }
        this.setState({
            payloadId: payloadId
        })
        if (value == "ext") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    if (r[i].ext == "N") {
                        r[i].ext = "Y"
                    } else {
                        r[i].ext = "N"
                    }
                }
            }
            this.setState({
                itemUdfMapping: r
            })
        } else if (value == "map") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    if (r[i].map == "N") {
                        r[i].map = "Y"
                    } else {
                        r[i].map = "N"
                    }
                }
            }
            this.setState({
                itemUdfMapping: r
            })
        }
    }


    // ____________________-------itemudfmapping_______________________-----------
    addNew() {
        if (this.state.udf == "" || this.state.departmentUdf == "") {
            this.setState({
                toastMsg: "Select Udf & Department",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            this.setState({
                newItemUdf: true
            })
        }

    }
    onClose() {
        this.setState({
            newItemUdf: false,
            displayUdf: "",
            udfValue: "",
            mapUdf: "",
        })

        // this.onClear();
    }
    onSaveNewData() {
        this.udfValueVali();

        this.displayNameVal();

        const t = this;

        setTimeout(function () {
            const { disNameerror, maperror, udfValueerror } = t.state;
            if (!maperror && !udfValueerror) {
                t.setState({
                    newItemUdf: false,

                })
                let itemUdfMappingData = []
                let dataItemUdf = {
                    hl3code: t.state.hl3Code,
                    hl3Name: t.state.departmentUdf,
                    hl4code: t.props.articleCodeUdf,
                    hl4Name: t.props.articleNameUdf,
                    cat_desc_udf: t.state.udf,
                    displayName: t.state.displayUdf,
                    ext: "N",
                    map: "Y",
                    description: t.state.udfValue,
                    code: ""


                }

                itemUdfMappingData.push(dataItemUdf)



                let payload = {
                    isInsert: true,
                    itemUdfMappingData: itemUdfMappingData
                }
                t.props.updateItemUdfRequest(payload);

                t.onClear();
            }
        }, 100)

    }
    handleRadioChange(e) {
        let t = this;
        this.setState({
            codeRadio: e.target.value,
        })
        if (e.target.value == "deptWise") {
            this.setState({
                showSecond: true,
                descriptionUdf: "",
                udf: "",
                addNewImg: false,
                newItemUdf: false
            })

            let data = {
                showSecondCat: true,
                descriptionUdf: "",
                udf: ""
            }

        } else {

            this.setState({
                showSecondCat: false,
                description: "",
                udf: "",
                addNewImg: true
            })
            let data = {
                showSecondCat: false,
                description: "",
                udf: ""
            }
        }


        this.props.updateUdfRadio(e.target.value)
        setTimeout(function () {
            t.onClear(e);

        }, 10)

    }

    // _____________________________ERROR VALIDATION____________________________________


    openDescription() {
        if (this.state.udf != "") {
            let data = {
                no: 1,
                type: 1,
                search: "",
                udfType: this.state.udf,
            }
            this.props.cnameRequest(data);

            this.setState({

                cnameModal: true,
                cnameModalAnimation: !this.state.cnameModalAnimation
            })


        } else {

            this.setState({
                toastLoader: true,
                toastMsg: "select  UDF",
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 2000)

        }
    }
    updateDescriptionUdf(data) {

        this.setState({
            description: data.cname,
            codeUdf: data.code
        })
        let payload = {
            cname: data.cname,
            code: data.code
        }
        this.props.descriptionValueUdf(payload)
        window.setTimeout(() => {
            document.getElementById("chooseBtn").focus()
        })
    }

    closeDescription() {
        this.setState({

            cnameModal: false,
            cnameModalAnimation: !this.state.cnameModalAnimation
        })
        window.setTimeout(() => {
            document.getElementById("description").focus()
        })

    }

    displayNameVal() {
        if (this.state.displayUdf == "") {
            this.setState({
                disNameerror: true
            })
        } else {
            this.setState({
                disNameerror: false
            })
        }
    }

    // mapUdfVal(){
    //     if(this.state.mapUdf == ""){
    //         this.setState({
    //             maperror:true
    //         })
    //     }else{
    //         this.setState({
    //             maperror:false
    //         })
    //     }
    // }

    udfValueVali() {
        if (this.state.udfValue == "") {
            this.setState({
                udfValueerror: true
            })
        } else {
            this.setState({
                udfValueerror: false
            })
        }
    }

    handleChangeUpdate(e) {

        if (e.target.id == "displayUdf") {

            this.setState({
                displayUdf: e.target.value
            },
                () => {
                    this.displayNameVal()
                }
            )



        } else if (e.target.id == "udfValue") {

            this.setState({
                udfValue: e.target.value
            },
                () => {
                    this.udfValueVali()
                }
            )

        }

    }

    onClear() {
        this.setState({
            displayUdf: "",
            udfValue: "",
            mapUdf: "",
            showArticleUdf: false,
            articleCheck: false

        })
    }


    onSubmit() {
        let itemUdfMapping = this.state.itemUdfMapping
        let payloadId = this.state.payloadId
        let dataUdf = []
        if (payloadId.length != 0) {
            itemUdfMapping.forEach(udf => {
                if (payloadId.includes(udf.id.toString())) {
                    let payloadData = {
                        hl3code: this.state.hl3Code,
                        hl3Name: this.state.departmentUdf,
                        hl4code: this.props.articleCodeUdf,
                        hl4Name: this.props.articleNameUdf,
                        cat_desc_udf: this.state.codeRadio === "deptWise" ? this.state.udfDept : this.state.udf,
                        displayName: udf.displayName,
                        ext: udf.ext,
                        map: udf.map,
                        description: udf.description,
                        code: ""

                    }
                    dataUdf.push(payloadData)

                }
            })
            let payloadMapping = {
                isInsert: false,
                itemUdfMappingData: dataUdf
            }

            this.props.updateItemUdfRequest(payloadMapping)
            this.onClear();
        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "No changes in data"
            })
        }
    }

    onCheck() {
        if (this.state.articleCheck) {
            this.setState({
                articleCheck: false,
                // showArticleUdf:false,
                articleCode:"",
                articleName:""
            })
            let data = {
                articleCheck: false,
                showArticleUdf: false
            }
            this.props.checkUdfUpdate(data)
        } else {
            this.setState({
                articleCheck: true,
                showArticleUdf: true

            })
            let data = {
                articleCheck: true,
                showArticleUdf: true
            }
            this.props.checkUdfUpdate(data)
        }


    }


    openArticleSelection() {
        if (this.state.departmentUdf != "") {
            if (this.state.codeRadio === "deptWise" ? this.state.udfDept != "" : this.state.udf != "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    hl3Code: this.state.hl3Code,
                    catDescUDF: this.state.codeRadio === "deptWise" ? this.state.udfDept : this.state.udf
                }
                this.props.articleNameRequest(data);

                this.setState({

                    articleNameModal: true,
                    articleNameModalAnimation: !this.state.articleNameModalAnimation
                })
            } else {

                this.setState({
                    toastLoader: true,
                    toastMsg: "Select  UDF",
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)

            }

        } else {

            this.setState({
                toastLoader: true,
                toastMsg: "Select  Department",
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 2000)

        }



    }
    updateArticleUdf(data) {
    
        this.setState({
            articleName: data.articleName,
            articleCode: data.articleCode
        })
        let a = {
            articleName: data.articleName,
            articleCode: data.articleCode
        }
        this.props.updateArticleUdf(a)
    }


    closeArticleSelection() {

        this.setState({

            articleNameModal: true,
            articleNameModalAnimation: !this.state.articleNameModalAnimation
        })

    }

    onnChangeDept(name) {

        this.setState({
            udfDept: name,

        })


        let data = {
            udfDept: name
        }
        this.props.updateUdfDept(data)


    }

    onSearch(e) {
        if (this.state.search == "" || this.state.search == " ") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {

            this.props.udfTypee(3);
            let data = {

                type: 3,
                no: 1,
                itemUdfType: this.state.udf,
                ispo: "false",
                search: this.props.searchUdf,
                description: this.state.codeRadio == "udf" ? this.state.description : "",
                hl3Code: this.state.hl3Code,
                hl4Code: this.props.articleCodeUdf,
                hl4Name: this.props.articleNameUdf,
                searchBy:this.state.searchBy
            }
            this.props.itemUdfMappingRequest(data)

        }
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }


    onClearSearch() {
        this.props.searchUdfFun("")
        this.props.udfTypee(1)
        var data = {
            type: 1,
            no: 1,
            itemUdfType: this.state.udf,
            ispo: "false",
            search: "",
            description: this.state.codeRadio == "udf" ? this.state.description : "",
            hl3Code: this.state.hl3Code,
            hl4Code: this.props.articleCodeUdf,
            hl4Name: this.props.articleNameUdf,
            searchBy:this.state.searchBy

        }
        this.props.itemUdfMappingRequest(data)
    }

    // __________________________TAB FUNCTIONALITY_____________

    searchFocus(e) {
        if (e.key == "Enter") {
            this.getAllDataUdf();
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("searchBtn").blur();
                document.getElementById("search").focus()
            }, 0);
        }
    }

    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {

            document.getElementById(idd).click();
        }
        if (e.key === "Enter" && id == "description") {

            document.getElementById(idd).click();
        }
        if (e.key == "Enter" && idd == "dropdownMenu1") {
            this.setState({
                udfDept: this.state.udfData[0].name
            })
            window.setTimeout(() => {
                document.getElementById(this.state.udfData[0].name).focus()
            })
        }
        if (id == "description" && e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("addmore").focus()
            })
        } else if (id == "chooseBtn" && e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("dropdownMenu1").focus();
            }, 0);
        }
         if (e.key == "Tab" && idd == "dropdownMenu1") {
              document.getElementById("searchBtn").focus();
        }
    }

    focusSave(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id, "map")
        }
    }
    onExtKeyDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id, "ext")
        }
        if (e.key === "Tab") {
            if (this.state.itemUdfMapping[this.state.itemUdfMapping.length - 1].id == id && this.state.payloadId.length == 0) {
                window.setTimeout(() => {
                    document.getElementById("dropdownMenu1").focus()
                }, 0);
            } else if (this.state.itemUdfMapping[this.state.itemUdfMapping.length - 1].id == id && this.state.payloadId.length != 0) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemUdfMapping[0].id).blur();
                    document.getElementById("saveButton").focus();
                }, 0);
            }
        }
    }
    focusOnCatDesc(e) {
        if (e.key === "Enter") {
            this.onSubmit()
        }
        if (e.key === "Tab") {
            document.getElementById("dropdownMenu1").focus();
            e.preventDefault()
        }
    }
    searchKeyDown(e) {
        if (e.key === "Tab" && this.state.itemUdfMapping.length == 0 || this.state.itemUdfMapping == null) {
            window.setTimeout(function () {
                document.getElementById("dropdownMenu1").focus()
            }, 0)
        } else {
            window.setTimeout(() => {
                document.getElementById(this.state.itemUdfMapping[0].map).focus();
            })
        }
    }
    ArrowDown(id, e) {
        if (e.key === "Enter") {
            this.onnChange(id)
            window.setTimeout(() => {
                document.getElementById("description").focus()
            }, 0);
        }
        if (e.key === "ArrowUp" || e.key === "ArrowDown") {
            let udfData = this.state.udfData
            var index = ""
            var nextId = ""
            for (let i = 0; i < udfData.length; i++) {
                if (id == udfData[i].name) {
                    index = i

                }
                if (index > 0 && e.key === "ArrowUp") {
                    nextId = udfData[index - 1].name

                }
                if (index != udfData[i].length && e.key === "ArrowDown" && id != this.state.udfData[this.state.udfData.length - 1].name) {
                    nextId = udfData[index + 1].name
                }
            }
            if (nextId != "") {
                document.getElementById(nextId).focus()
            }
        }
    }
    openDrop(e) {
        this.setState({
            dropdown: true
        })
    }
    focusOnButton(e) {
        if (e.key === "Enter") {
            this.addNew();
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("chooseBtn").focus()
            })
        }
    }
    handleChange(id, e) {
        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {

            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })
        let itemUdfMapping = this.state.itemUdfMapping
        for (let i = 0; i < itemUdfMapping.length; i++) {
            if (itemUdfMapping[i].id == id) {
                itemUdfMapping[i].displayName = e.target.value
            }

        }
        this.setState({
            itemUdfMapping: itemUdfMapping
        })
    }

    _handleKeyPressSearch = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
     xlscsv(type) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }

        axios.get(`${CONFIG.BASE_URL}/admin/po/export/mappings/data?catDescUdfType=${this.state.udf}&hl1Code=&hl2Code=&hl3Code=${this.state.hl3Code}`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
            });

    }
    render() {
  
        const { disNameerror, displayUdf, udfValue, udfValueerror, description, articleName, articleCheck, showArticleUdf } = this.state
        return (
            <div className="p-lr-47">
                <div className="col-md-12 col-sm-12 m-top-20 pad-0">
                    <div className="col-md-2 pad-0 piFormDiv">
                        <label className="purchaseLabel">
                            Division
                    </label>
                        <input type="text" className="purchaseSelectBox" value={this.state.divisionUdf} placeholder="Select  Division" disabled />
                    </div>
                    <div className="col-md-2 pad-0 piFormDiv">
                        <label className="purchaseLabel">
                            Section
                    </label>
                        <input type="text" className="purchaseSelectBox" value={this.state.sectionUdf} placeholder="Choose Section" disabled />
                    </div>
                    <div className="piFormDiv col-md-2 pad-0">
                        <label className="purchaseLabel">
                            Department
                    </label>
                        <input type="text" className="purchaseSelectBox" value={this.state.departmentUdf} placeholder="Choose Department" disabled />
                    </div>
                    <div className="col-md-1 pad-0 piFormDiv m-top-17">
                        <button className="dataPiBtn width100" type="button" onKeyDown={(e) => this._handleKeyPress(e, "chooseBtn")} id="chooseBtn" onClick={(e) => this.openSelection(e)}>
                            Choose Data
                    </button>
                    </div>
                    <div className="col-md-2 pad-0 piFormDiv udfCheck assortmentCheck m-top-15">
                        <label className="checkBoxLabel0"><input type="checkBox" id="articleCheck" checked={articleCheck} onChange={(e) => this.onCheck(e)} />Article Level Mapping <span className="checkmark1"></span> </label>
                    </div>

                    <div className="col-md-12 pad-0"> {this.state.codeRadio == "deptWise" ? <div>
                        <div className="col-md-2 pad-0 m-top-15 piFormDiv">
                            <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown">
                                <button className="btn btn-default dropdown-toggle userModalSelect" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {this.state.udfDept != "" ? this.state.udfDept : "Select UDF"}
                                    <i className="fa fa-chevron-down"></i>
                                </button>

                                <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    {this.state.udfData.length != 0 ? this.state.udfData.map((data, i) => <li value={data.name} id={data.name} key={i} onClick={(e) => this.onnChangeDept(`${data.name}`)} >
                                        <a >    {data.name}</a>
                                    </li>) : "No Data Found"}

                                </ul>

                            </div>
                        </div>


                    </div> : null}
                        {articleCheck ? <div className="col-md-2 pad-0 piFormDiv m-top-15">
                            <div className="inputTextKeyFucMain" >
                                <input autoComplete="off" type="text" className="inputTextKeyFuc btnFocus" onKeyDown={(e) => this._handleKeyPress(e, "articleName")} id="articleName" value={articleName} placeholder="Choose Article Name" onClick={(e) => this.openArticleSelection(e)} />
                                <span className="modalBlueBtn" onClick={(e) => this.openArticleSelection(e)}>
                                    ▾
                                        </span>

                            </div>
                        </div> : null}</div>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 lineAfterBg">
                    <div className="col-md-8 col-sm-12 m-top-20 pad-0">
                        {this.state.codeRadio == "udf" ? <div className="col-md-3 pad-lft-0">
                            <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown">
                                <button className="btn btn-default dropdown-toggle userModalSelect btnFocus" onClick={(e) => this.openDrop(e)} type="button" onKeyDown={(e) => this._handleKeyPress(e, "dropdownMenu1")} id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {this.state.udf != "" ? this.state.udf : "Select Udf"}
                                    <i className="fa fa-chevron-down"></i>
                                </button>
                                {this.state.dropdown ? <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    {this.state.udfData.length != 0 ? this.state.udfData.map((data, j) =>
                                        <li value={data.name} id={data.name} key={j} tabIndex="-1" onKeyDown={(e) => this.ArrowDown(`${data.name}`, e)} onClick={(e) => this.onnChange(`${data.name}`)} >
                                            <a > {data.name}</a>
                                        </li>
                                    ) : "No Data Found"}
                                </ul> : null}
                            </div>
                        </div> : <div className="col-md-3 pad-lft-0">
                                <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown">
                                    <button className="btn btn-default dropdown-toggle userModalSelect itemUdfDisable pointerNone btnDisabled" type="button" id="dropdownMenu1" >
                                        {this.state.udf != "" ? this.state.udf : "Select Udf"}
                                        <i className="fa fa-chevron-down"></i>
                                    </button>

                                </div>
                            </div>}
                        <div className="col-md-3 pad-lft-0">
                            <div className="inputTextKeyFucMain" >
                                {this.state.codeRadio == "udf" ? <input autoComplete="off" type="text" className="inputTextKeyFuc onFocus" onKeyDown={(e) => this._handleKeyPress(e, "description")} id="description" value={description} placeholder="Choose Description" onClick={(e) => this.openDescription(e)} /> :
                                    <input autoComplete="off" type="text" className="inputTextKeyFuc btnDisabled" onKeyDown={(e) => this._handleKeyPress(e, "description")} id="description" value={description} placeholder="Choose Description" onClick={(e) => this.openDescription(e)} disabled />}
                                {this.state.codeRadio == "udf" ? <span className={this.state.codeRadio == "deptWise" ? "modalBlueBtn pointerNone" : "modalBlueBtn"} onClick={(e) => this.openDescription(e)}>
                                    ▾
                                    </span> : <span className={this.state.codeRadio == "deptWise" ? "modalBlueBtn pointerNone" : "modalBlueBtn"} >
                                        ▾
                                    </span>}
                            </div>
                        </div>
                        <div className="col-md-6 pad-lft-0 selectFrequencyCustom setUdfRadio">
                            <ul className="pad-0 lineAfter">
                                <li className="m0"> <label className="select_modalRadio">
                                    <input type="radio" id="udf" checked={this.state.codeRadio === "udf"} name="udf" value="udf" onChange={e => this.handleRadioChange(e)} /><span>UDF</span>
                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                </label>
                                </li>
                                <li className="m0"><label className="select_modalRadio">
                                    <input type="radio" id="deptWise" checked={this.state.codeRadio === "deptWise"} name="deptWise" value="deptWise" onChange={e => this.handleRadioChange(e)} /><span>Dept Wise</span>
                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                </label></li>
                                {this.state.frequencyerr ? <span className="error m-top-10">Select Frequency</span> : null}

                                {this.state.codeRadio === "udf" ? <li className="m0">
                                    <button onClick={() => this.addNew()} onKeyDown={(e) => this.focusOnButton(e)} id="addmore" className="pad-lft-15 displayPointer addmore" >
                                        <img className="displayPointer" src={require('../../assets/add-green.svg')} /> 
                                        <span className="generic-tooltip">Add New</span>
                                    </button>
                                </li> : null}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-40">
                    {this.state.newItemUdf ? <div className="col-md-12 col-sm-12 pad-0">
                        <div className="col-md-12 pad-0" >
                            {/*<div className="col-md-2 pad-lft-0"> <input type="text" value={displayUdf} id="displayUdf" placeholder="Enter display name" className={disNameerror ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} onChange={(e) => this.handleChangeUpdate(e)} />
                                {this.state.disNameerror ? <span className="error">Enter display Name</span> : null}</div>*/}
                            <div className="col-md-2 pad-lft-0">  
                                <input type="text" value={udfValue} id="udfValue" placeholder="Enter value" className={udfValueerror ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} onChange={(e) => this.handleChangeUpdate(e)} />
                                {this.state.udfValueerror ? <span className="error">Enter value</span> : null}</div>
                        </div>
                        <div className="col-md-4 pad-0 m-top-20">
                            <button type="button" className="save2_button_vendor m-rgt-10" onClick={() => this.onSaveNewData()}>Save</button>
                            <button type="button" className="clear_button_vendor m-rgt-10" onClick={() => this.onClear()}>Clear</button>
                            <button type="button" className="cancel_button_vendor" onClick={() => this.onClose()}>Cancel</button>
                        </div>
                    </div> : null}
                </div>
           



                <div className="col-md-12 col-sm-12 pad-0">
                    <div className="col-md-6 m-top-20 pad-0">
                        <button className="search_button_vendor" id="searchBtn" type="button" onKeyDown={(e) => this.searchFocus(e)} onClick={() => this.getAllDataUdf()}>Search</button>
                    </div>


                </div>

                <div className="col-md-12 pad-0">
                 <div className="col-md-1 pad-0">
                        <div className="exportDropDown settingDrop m-top-35 dropdown displayInline pad-0 width100 udfDropDown" id="settingDrop">
                          {/* { this.state.codeRadio == "udf" && (this.state.udf !="" || this.state.hl3Code!="")?
                                            <button type="button" onClick={(e) => this.xlscsv()} className="button_home">
                                            EXPORT
                                    </button>:  <button type="button" className="button_home btnDisabled">
                                                EXPORT
                                    </button>} */}
                                    <div className="gvpd-download-drop2">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button"  onClick={(e) => this.xlscsv()}>
                                                    {/* <img src={ExportExcel} /> */}
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                        </ul>}
                                </div>
                        </div>
                                    </div>
                    <div className="col-md-5"></div>

                    <div className="col-md-6 col-sm-12 pad-0">

                        <ul className="list-inline search_list manageSearch m-top-80">
                            {/* <form onSubmit={(e) => this.onSearch(e)}> */}
                            <li className="width-60per">
                                        {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByudfmap" name="searchByudfmap" value={this.state.searchBy} onChange={(e) => this.handleChange("searchByudfmap",e)}>
                                                        
                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                <input type="search" id="search" onKeyPress={this._handleKeyPressSearch} onChange={(e) => this.handleSearch(e)} value={this.state.search} onBlur={(e) => this.onSearchBlur(e)} placeholder="Type to Search..." className="search_bar" />
                                <button className={this.state.itemUdfMapping.length == 0 || this.state.itemUdfMapping == null ? "btnDisabled searchWithBar" : "searchWithBar btnFocuss"} onKeyDown={(e) => this.searchKeyDown(e)} onClick={(e) => this.onSearch(e)}> Search
                   <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                        <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                        </path></svg>
                                </button>
                            </li>
                            {/* </form> */}
                        </ul>
                        {this.state.type == 3 ? <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                    </div>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric siteMapppingMain udfMappingTable itemUdfTable">
                    <div className="tableHeadFix pipo-con-table">
                        <div className="scrollableTableFixed table-scroll scrollableOrgansation tableHeadFixHeight">
                            <table className="table zui-table sitemappingTable gen-main-table">
                                <thead >
                                    <tr>

                                        <th><label>Dept. Name</label></th>
                                        <th><label>Display Name</label></th>

                                        <th className="width145"><label>Value</label></th>
                                        <th><label>Map</label></th>
                                        <th><label>Ext</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.itemUdfMapping == null || this.state.itemUdfMapping.length == 0 ?
                                        <tr className="tableNoData">
                                            <td className="border-bot0" colSpan="6"> NO DATA FOUND </td>
                                        </tr>
                                        : this.state.itemUdfMapping.map((data, idxx) => (
                                            <tr id={idxx} key={idxx}>

                                                <td>
                                                    <label>{data.hl3Name}</label>

                                                </td>
                                                <td>
                                                    {this.props.editDisplayName == "true" ? <input autoComplete="off" type="text" className="purchaseOrderSelectBox modal_tableBox borderR0" value={data.displayName == null ? "" : data.displayName} onChange={(e) => this.handleChange(`${data.id}`, e)} />
                                                        : <label>{data.displayName}</label>}


                                                </td>

                                                <td>
                                                    <label>{data.description}</label>

                                                </td>
                                                <td className="col-md-12 col-sm-12 pad-0">
                                                    <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" onKeyDown={(e) => this.focusSave(`${data.id}`, e)} onChange={(e) => this.handleCheck(`${data.id}`, "map")} checked={data.map == "N" ? false : true} name={data.id} id={data.map} /> <span className="checkmark1"></span> </label>
                                                </td>
                                                <td className="col-md-12 col-sm-12 pad-0">
                                                    <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" onKeyDown={(e) => this.onExtKeyDown(`${data.id}`, e)} onChange={(e) => this.handleCheck(`${data.id}`, "ext")} checked={data.ext == "N" ? false : true} name={data.id} id={data.ext} /> <span className="checkmark1"></span> </label>
                                                </td>
                                            </tr>
                                        ))}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" min="1" value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <div className="pad-right-15 wthauto">
                                            <ul className="list-inline pagination width100">
                                                {this.state.current == 1 || this.state.current == 0 ? <li >
                                                    <button className="PageFirstBtn pointerNone" >
                                                        First
                                                    </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                                            First
                                                        </button>
                                                    </li>}
                                                {this.state.prev != 0 ? <li >
                                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                        Prev
                                                    </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn" disabled>
                                                            Prev
                                                        </button>
                                                    </li>}
                                                <li>
                                                    <button className="PageFirstBtn pointerNone">
                                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                                    </button>
                                                </li>
                                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 col-sm-12 pad-0">
                    <div className="footerDivForm">
                        <ul className="list-inline m-lft-0 m-top-10">
                            {this.state.itemUdfMapping == null || this.state.itemUdfMapping.length == 0 || this.state.payloadId.length == 0 ? <li><button type="button" className="save_button_vendor btnDisabled" >Save</button></li> :
                                <li><button type="button" onClick={(e) => this.onSubmit(e)} className="save_button_vendor" onKeyDown={(e) => this.focusOnCatDesc(e)} id="saveButton">Save</button></li>}

                        </ul>
                    </div>
                </div>

                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.selectionModal ? <SectionDataSelection {...this.props} department={this.state.departmentUdf} divisionState={this.state.divisionState} sectionCloseModal={(e) => this.sectionCloseModal(e)} selectionCloseModal={(e) => this.selectionCloseModal(e)} updateState={(e) => this.updateState(e)} selectionAnimation={this.state.selectionAnimation} closeSelection={(e) => this.closeSelection(e)} /> : null}
                {this.state.cnameModal ? <CnameModal {...this.props} {...this.state} updateDescriptionUdf={(e) => this.updateDescriptionUdf(e)} closeDescription={(e) => this.closeDescription(e)} openDescription={(e) => this.openDescription(e)} catDesc={this.state.udf} /> : null}
                {this.state.articleNameModal ? <ArticleName {...this.props} {...this.state} hl4Code={this.state.articleCode} catDescUDF={this.state.codeRadio === "deptWise" ? this.state.udfDept : this.state.udf} updateArticleUdf={(e) => this.updateArticleUdf(e)} articleCat={this.state.articleCat} closeArticleSelection={(e) => this.closeArticleSelection(e)} openArticleSelection={(e) => this.openArticleSelection(e)} department={this.state.departmentUdfUdf} /> : null}
            </div>
        )
    }
}

export default UdfMap;