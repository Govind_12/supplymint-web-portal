import React from "react";
import { CONFIG } from '../../config/index';
import axios from 'axios';
import { throttle, debounce } from 'throttle-debounce';
import UdfMappingModal from "./udfMappingModal";
import SupplierModal from "../purchaseIndent/purchaseVendorModal";
import ToastLoader from "../loaders/toastLoader";
import ArticleModal from "./articleModal";
import PiColorModal from "../purchaseIndent/piColorModal";
import moment from 'moment';
import { getDate } from '../../helper';
import ItemCodeModal from "../purchaseOrder/itemCodeModal";
import PiImageModal from "../purchaseIndent/piImageModal";
import VendorDesignModal from "../purchaseOrder/vendorDesignModal";
import TransporterSelection from "../purchaseIndent/transporterSelection";
import LoadIndent from "./loadIndent";
import PoError from "../loaders/poError";
import AlertNotificationModal from "./alertNotificationModal";
import ItemUdfMappingModal from "./itemUdfMappingModal";
import ConfirmModalPo from "../loaders/confirmModalPo";
import DeleteModalPo from "../loaders/deleteConfirmModal";

import exclaimIcon from "../../assets/exclain.svg";
import _ from 'lodash';
import OrderNumberModal from "./orderNumber";
import SetVendorModal from "./setVendorModal";
import SetDepartmentModal from "./setDepartmentModal";
import ResetAndDelete from "../loaders/resetAndDelete";
import copyIcon from "../../assets/copyNew.svg";
import invoice from "../../assets/invoice.svg";
import RupeeNew from "../../assets/rupeeNew.svg";
import DownIcon from "../../assets/selectIcon.svg";
import HsnCodeModal from "./hsnCodeModal";
import SiteModal from "./siteModal";
import IcodeModalInner from "./poIcodeModal2";
import IcodeModal from "./poIcodeModal"
import NewIcodeModal from "./newIcodemodal";
import CityModal from "./cityModal";
import FilterLoader from '../loaders/filterLoader';
import DiscountModal from "./discountModal";
import PoArticleModal from "./poArticleModal"
import SetModalPI from './setModalPI'
import IdleTimer from 'react-idle-timer'
import ConfirmDraftModal from "../loaders/DraftConfirmModal"
import ConfirmClearForm from "../loaders/confirmClearForm";
import { sizeRequest } from "../../redux/purchaseIndent/action";
import AgentModal from "../purchaseIndent/agentModal";


const emptyColorList = { id: 1, code: "", cname: "NA" }

class GenericPurchaseIndent extends React.Component {
    constructor(props) {
        super(props);
        // this.escChild = React.createRef()

        this.state = {
            calculatedMarginErr: false,
            isCheckMarginRuleValidation: false,
            transporterWithCity: true,
            agentNameWithCity: true,
            isCityChecked: true,
            bufferDays: 14,
            selectedRowIndex: 0,
            headerLevelErrMsg: false,
            isTransporterLovSelect: false,
            isVendorLovSelect: false,
            isSiteLovSelect: false,
            isCityLovSelect: false,
            isMaintainSize: true,
            siteMount: false,
            isCatDescExist: true,
            isDisplayDivision: false,
            isDisplaySection: false,
            isDisplayDepartment: false,
            isContactCalled : false,
            isDraftRequest : true,
            isSubmitEnable: false,
            isMrpFilled: false,
            catRemarkLabel: "",
            mrpValidation: false,
            isDisplayWSP: false,
            isMandateWSP: false,
            wspValidation: false,
            isCopyIndent: false,
            isMandateCatRemark: false,
            isDisplayCatRemark: false,
            copyPi: false,
            copyPiDraft: false,
            successMessageState: false,
            discardedSuccess: false,
            articleClicked: false,
            alertFlag: 1,
            isSetDeliveryDate: false,
            validFromEdited: "",
            validToEdited: "",
            rowItem: '',
            rowIndex: '',
            status: "",
            orderNumberSaveDraft: true,
            isLeadTimeDisplayPo: false,
            isDeliveryDate: false,
            getDraft: false,
            disId: "",
            mrpId: "",
            isSet: true,
            storedSet: true,
            hsnId: "",
            uuid: "",
            discountSearch: "",
            mrpSearch: "",
            hsnSearch: "",
            poArticleSearch: "",
            focusedObj: {
                type: "",
                rowId: "",
                cname: "",
                value: "",
                radio: "",
                finalRate: "",
                colorObj: {
                    color: [],
                    colorList: []
                }

            },

            code: "",

            lineItemRowId: "",
            setModal: false,
            setModalAnimation: false,
            selectedRowId: "",
            sectionName: "",
            divisionName: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            hl3Code: "",
            hl4Name: "",
            hl4Code: "",
            basedOn: "",
            mrpStart: "",
            mrpEnd: "",
            mrpRangeSearch: false,
            startRange: "",
            endRange: "",
            poArticle: false,
            poArticleAnimation: false,


            isVendorDesignNotReq: false,
            isAutoGenerateDesign: false,
            typeOfBuying: "Planned",
            displayOtb: true,
            transporterValidation: true,
            isDispalyTransporter: false,
            addRow: true,
            isRequireSiteId: false,
            itemDetailCode: "",
            dateValidationRes: false,
            validFromValidation: false,
            valtdToValidation: false,
            restrictedStartDate: "",
            restrictedEndDate: "",
            isMRPEditable: false,
            isRSP: false,
            discountMrp: "",
            selectedDiscount: "",
            discountGrid: "",
            discountModal: false,
            loader: false,
            clickedOneTime: false,
            cityModal: false,
            cityModalAnimation: false,
            city: "",
            cityerr: false,
            isCityExist: false,
            onFieldhsn: true,
            // itemudf1Validation: false,
            // itemudf2Validation: false,
            // itemudf3Validation: false,
            // itemudf4Validation: false,
            // itemudf5Validation: false,
            // itemudf6Validation: false,
            // itemudf7Validation: false,
            // itemudf8Validation: false,
            // itemudf9Validation: false,
            // itemudf10Validation: false,
            // itemudf11Validation: false,
            // itemudf12Validation: false,
            // itemudf13Validation: false,
            // itemudf14Validation: false,
            // itemudf15Validation: false,
            // itemudf16Validation: false,
            // itemudf17Validation: false,
            // itemudf18Validation: false,
            // itemudf19Validation: false,
            // itemudf20Validation: false,

            // udf1Validation: false,
            // udf2Validation: false,
            // udf3Validation: false,
            // udf4Validation: false,
            // udf5Validation: false,
            // udf6Validation: false,
            // udf7Validation: false,
            // udf8Validation: false,
            // udf9Validation: false,
            // udf10Validation: false,

            // udf11Validation: false,
            // udf12Validation: false,
            // udf13Validation: false,
            // udf14Validation: false,
            // udf15Validation: false,
            // udf16Validation: false,
            // udf17Validation: false,
            // udf18Validation: false,
            // udf19Validation: false,
            // udf20Validation: false,

            // cat1Validation: false,
            // cat2Validation: false,
            // cat3Validation: false,
            // cat4Validation: false,
            // cat5Validation: false,
            // cat6Validation: false,

            // desc1Validation: false,
            // desc2Validation: false,
            // desc3Validation: false,
            // desc4Validation: false,
            // desc5Validation: false,
            // desc6Validation: false,

            isDiscountAvail: false,
            isDiscountMap: false,
            isArticleSeparated: false,
            oldValue: "",
            multipleErrorpo: false,

            lineError: [],
            focusedQty: {
                id: "",
                qty: ""
            },
            copingLineItem: [],
            deleteGridId: "",
            deleteSetNo: "",
            deleteConfirmModal: false,
            lineItemChange: false,
            saveMarginRule: "",
            poItemBarcodeData: {},
            icodeModalAnimation: false,
            itemBarcodeModal: false,
            itemcodeerr: false,
            itemCodeList: [],
            colorListValue: [],
            isAdhoc: false,
            isIndent: false,
            isSetBased: false,
            isHoldPo: false,
            isPOwithICode: false,
            poWithUpload: false,
            copyColor: "false",
            focusImage: "",
            focusState: "",
            rateFocus: "",
            focusId: "",
            itemFocus: "",
            itemCodeId: "",
            section3ColorId: "",
            mappingId: "",
            onFieldSite: false,
            siteName: "",
            siteCode: "",
            siteNameerr: false,
            siteModal: false,
            siteModalAnimation: false,
            itemUdfExist: "false",
            isUDFExist: "false",
            storeUDFExist: "false",
            isSiteExist: "false",
            isOtbValidationPi: "false",


            markUpYes: false,
            tradeGrpCode: "",
            hsnModal: false,
            hsnModalAnimation: false,


            changeLastIndate: true,
            descSix: "",
            minDate: getDate(),
            maxDate: "",
            slCityName: "",
            deleteMrp: "",
            deleteOtb: "",
            desc6: "",
            loadIndentId: "",
            imageRowId: "",
            imageState: {},
            imageModal: false,
            imageModalAnimation: false,
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            gstInNo: "",
            itemValue: "",
            RadioChange: "",
            gridFirst: false,
            gridSecond: false,
            gridThird: false,
            gridFourth: true,
            gridFivth: false,
            itemUdfName: "",
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: false,
            itemUdfId: "",

            itemUdfType: "",
            vendorId: "",
            articleName: "",
            udfRowMapping: [],
            udfMappingData: [],
            colorNewData: [],
            mrp: "",
            itemName: "",
            trasporterModal: false,
            sizeMapping: [],
            transporterAnimation: false,
            transporterCode: "",
            transporterName: "",
            loadIndentModal: false,
            loadIndentAnimation: false,
            purchaseTermState: [],
            itemModal: false,
            itemModalAnimation: false,
            toastLoader: false,
            poErrorMsg: false,
            toastMsg: "",
            errorMassage: "",
            poItemcodeData: [],
            term: "",
            udfMapping: false,
            udfMappingAnimation: false,
            udfSetting: false,
            udfSettingAnimation: false,
            poItems: false,
            poItemsAnimation: false,
            poColor: false,
            indentRadio: "",
            indentRadioerr: false,
            poDate: "",
            poDateerr: false,
            loadData: this.props.purchaseIndent.loadIndent.data.resource,
            loadIndent: "",
            loadIndenterr: false,
            poValidFrom: "",
            lastInDate: "",
            lastAsnDate: "",

            slCode: "",
            slAddr: "",
            slName: "",
            item: "",
            text: "",
            vendor: "",
            leadDays: "",
            transporter: "",
            agentModal: false,
            poUdf1:"",
            poUdf1err: false,
            poUdf2: "yyyy-mm-dd",
            poUdf2err: false,
            poUdf3:"",
            poUdf3err: false,
            poUdf4:"",
            poUdf4err: false,
            poUdf5:"",
            poUdf5err: false,
            supplier: "",

            supplierModal: false,
            supplierModalAnimation: false,
            articleModalAnimation: false,
            articleModal: false,
            codeRadio: "Adhoc",
            open: false,
            search: "",
            colorData: [],
            colorRow: "",
            indentLoad: true,
            isDescriptionChecked: false,
            isSecTwoDescriptionChecked: false,
            isUdfDescriptionChecked: false,
            isItemUdfDescriptionChecked: false,
            termCode: "",
            termName: "",
            poAmount: 0,
            poQuantity: 0,
            vendorPoState: [],
            udfType: "",
            udfName: "",
            udfRow: "",
            colorCode: "",
            colorModal: false,
            colorModalAnimation: false,
            itemUdfMappingState: [],
            supplierCode: "",
            // __________________________validation error states_________________
            combineRow: false,
            poValidFromerr: false,
            lastInDateerr: false,
            lastAsnDateerr: false,

            itemerr: false,
            vendorerr: false,
            transportererr: false,
            otbStatus: false,
            setValue: "",
            vendorDesignVal: "",
            indentValue: "",
            colorValue: "",
            flag1: 0,
            flag: false,
            otbArray: [],
            otbValue: {},
            otbValueData: "",
            mrpValueData: "",
            itemDetailName: "",
            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: true
                },

                finalRate: 0,
                rate: "",
                netRate: "",
                rsp: "",
                wsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",

                marginRule: "",
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],

                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",
                    image: [],
                    imageUrl: {},
                    imagePath: "",
                    containsImage: false,
                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: [],
                    lineItemChk: false,
                    sizeType: "complex",
                    totalBasic: "" // outer Basic
                }]

            }],

            //last
            currentDate: "",
            sizeRows: [],
            vendorDesignModal: false,
            vendorDesignAnimation: false,
            vendorDesignData: [],

            itemDetailsHeader: [],
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            itemType: "",
            itemId: "",
            itemDetailValue: "",
            setBased: "",
            poSetVendorerr: false,
            departmentSeterr: false,
            departmentSet: "",
            poSetVendor: "",
            orderSet: "",
            orderSeterr: false,
            loadIndentTrue: false,
            fourApi: false,
            piKeyData: [],
            descId: "",
            departmentSetBasedAnimation: false,
            departmentModal: false,
            orderNumber: false,
            orderNumberModalAnimation: false,
            SetVendor: false,
            setVendorModalAnimation: false,
            hl3CodeDepartment: "",
            poSetVendorCode: "",
            setDepartment: "",
            resetAndDelete: false,
            itemArticleCode: "",
            indentNoToShow_pending: '',
            indentNoToShow_drafted: '',
            icodeId: "",
            icodesArray: [],
            openIcodeModal: false,
            icodeValidation: false,
            isMrpRequired: true,
            isRspRequired: true,
            isColorRequired: true,
            isDisplayFinalRate: false,
            isTransporterDependent: true,
            isDisplayMarginRule: true,
            isGSTDisable: false,
            isTaxDisable: false,
            isBaseAmountActive: false,
            isvalidFromAndTo: false,
            typeOfBuyingErr: false,
            isModalShow: false,
            supplierSearch: "",
            transporterSearch: "",
            citySearch: "",
            siteSearch: "",
            loadIndentSearch: "",
            setVendorSearch: "",
            orderSearch: "",
            isMrpRangeDisplay: true,
            isMarginRulePi: false,
            parent: "PI",
            showSaveDraft: false,
            onFieldhsn: true,
            supplierState: [],
            POUdf1Label: '',
            isMandatePOUdf1: false,
            isDisplayPOUdf1: false,
            POUdf2Label: '',
            isMandatePOUdf2: false,
            isDisplayPOUdf2: false,
            isMandateImage: false,
            POUdf3Label: '',
            isMandatePOUdf3: false,
            isDisplayPOUdf3: false,
            POUdf4Label: '',
            isMandatePOUdf4: false,
            isDisplayPOUdf4: false,
            POUdf5Label: '',
            isMandatePOUdf5: false,
            isDisplayPOUdf5: false,

            // for Edit PI
            isEditPi: false,
            indentNo: "",
            indentId: "",
            simpleData: 0,

            // Save draft flag
            saveDraftFlag: false,
            editableRow: 0,
            availCustomPo: null,
            clearForm: false,
            indentNoDraft: "",
            successVar: false
            // dateerr: null,
        }
        this.showSaveDraft = this.showSaveDraft.bind(this);
        this.idleTimer = null
        this.onAction = this._onAction.bind(this)
        this.onActive = this._onActive.bind(this)
        this.onIdle = this._onIdle.bind(this)
    }

    ratioHandleChange = (id, size, event, color, index) => {
        console.log('val', event.target.value, id)
        if(color == '' && this.state.isColorRequired){
            this.setState({
                poErrorMsg: true,
                errorMassage: "Please fill color first!"
            })
        } else {
            let poRows = [...this.state.poRows];
            let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.state.selectedRowId));
            // for (let i = 0; i < poRows[mainIndex].lineItem.length; i++) {
            //     console.log('val', event.target.value)
            //     if (i+1 == id) {
                    console.log('val', event.target.value)
                    poRows[mainIndex].lineItem[index].ratio = [event.target.value]
            //     }
            // }
            this.setState({ poRows })
        }
    }

    showSaveDraft = () => {
        this.closeClearModal()
        this.setState({
            orderNumberSaveDraft: false,

            //showSaveDraft: true,
            showSaveDraft: false,
            // headerMsg: "Save draft will save the document as draft and clear the data!",
            // paraMsg: "Click confirm to continue."
        });
        this.finalDraftSave()

        this.closeDraftConfirmModal()
    }

    closeDraftConfirmModal() {
        this.setState({
            showSaveDraft: !this.state.showSaveDraft,
            saveDraftFlag: !this.state.saveDraftFlag
        });

    }

    finalDraftSave() {
        this.setState({
            saveDraftFlag: true,

        })
        this.saveDraftCall("modal");
    }

    onnRadioChange(cr) {
        if (cr != true && cr != false) {
            this.setState({
                codeRadio: cr,
            })

            setTimeout(() => {
                if (cr === "raisedIndent") {
                    document.getElementById("loadIndentInput").focus()
                } else if (cr === "setBased" || cr === "holdPo") {
                    document.getElementById("poSetVendor").focus()
                } else if (cr === "Adhoc" || cr === "poIcode") {
                    document.getElementById("piValidFrom").focus()
                }
                this.onClear();
                this.setBasedResest();
            }, 10);
        } else if (cr == true || cr == false) {
            this.setState({
                isSet: cr,
                isUDFExist: cr == true ? this.state.storeUDFExist : "false"
            }, () => {

                this.resetPoRows()
            })
        }
    }

    handleRadioChange(dat) {
        let indent = ""
        if (dat == "Adhoc") {
            indent = "adhoc"
        } else if (dat == "raisedIndent") {
            indent = "raised indent"
        } else if (dat == "setBased") {
            indent = "set based"
        }
        else if (dat == "holdPo") {
            indent = "hold PO"
        }
        else if (dat == "poIcode") {
            indent = "PO with Icode"
        }
        this.setState({
            headerMsg: "Are you sure you want to raise purchase order on the basis of " + indent + " ?",
            paraMsg: "Click confirm to continue.",
            radioChange: dat,
            confirmModal: true,
        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })

    }
    Capitalize(str) {
        if (str == undefined) {
            return "";
        } else {

            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    }
    _handleKeyPressDate(e, id) {
        let idd = id;
        if (e.key === "Enter") {
            document.getElementById(idd).click();
        }
    }
    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        } else if (e.key === "Enter") {
            if (id == "vendor") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openSupplier() : null
            }
            else if (id == "city") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openCityModal() : null
            } else if (id == "siteName") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openSiteModal() : null
            } else if (id == "transporter") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openTransporterSelection() : null
            } else if (id == "agentName") {
                this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? this.openAgentHandler() : null
            } else if (id == "loadIndentInput") {
                this.openloadIndentSelection()
            } else if (id == "poSetVendor") {
                this.setVendorModalOpen()
            } else if (id == "orderSet") {
                this.onSetOrderNumber()
            }
        } else {
            document.getElementById(id).focus()
        }
    }

    handleInputChange(e) {
        if (e.target.id == "lastInDate") {
            // let restrictedStartDate = this.state.restrictedStartDate
            // let restrictedEndDate = this.state.restrictedEndDate
            // let flag = false
            // if (restrictedStartDate != "" && restrictedEndDate != "" && e.target.value != "") {
            //     if (restrictedStartDate <= e.target.value && e.target.value <= restrictedEndDate) {
            //         this.setState({
            //             poErrorMsg: true,
            //             errorMassage: "You are unable to save PO(Purchase Order) for the selected date(" + e.target.value + ")",

            //         })
            //         flag = true

            //     } else {
            //         e.target.placeholder = e.target.value;
            //         this.setState({
            //             lastInDate: e.target.value
            //         }, () => {
            //             this.lastInDate()
            //         })
            //     }}

            //     else{
            e.target.placeholder = e.target.value;
            this.setState({
                lastInDate: e.target.value
            }, () => {
                this.lastInDate()
            })
        }
        //    if(!flag){
        if (e.target.value != "") {
            var d = new Date(e.target.value);
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            let dateFrom = new Date(this.state.poValidFrom);
            let yearFrom = dateFrom.getFullYear();
            let monthFrom = dateFrom.getMonth();
            let dayFrom = dateFrom.getDate();
            var lastinDateValue = moment(new Date(year, month, day)).format("YYYY-MM-DD")
            let fromAfterGap = moment(new Date(year, month, day -14)).format("YYYY-MM-DD")
            let fromDataValue = moment(new Date(year, month, day - this.state.bufferDays)).format("YYYY-MM-DD")
            let bufferDate = moment(new Date(yearFrom, monthFrom, dayFrom + this.state.bufferDays)).format("YYYY-MM-DD")
            let isFromChange = false;
            if (new Date(lastinDateValue).getTime() > new Date(bufferDate).getTime()){
                isFromChange = true;
                fromDataValue = moment(new Date(year, month, day - this.state.bufferDays)).format("YYYY-MM-DD")
            }
            if (new Date(fromAfterGap).getTime() < new Date(this.state.poValidFrom).getTime()){
                isFromChange = true;
                fromDataValue = moment(new Date(year, month, day - 14)).format("YYYY-MM-DD")
            }
            console.log('buffer', lastinDateValue, fromDataValue, bufferDate, isFromChange, this.state.poValidFrom, monthFrom, dateFrom, d)
            if (new Date(this.state.poDate).getTime() >= new Date(fromDataValue).getTime()) {
                let d = new Date(this.state.poDate);
                let year = d.getFullYear();
                let month = d.getMonth();
                let day = d.getDate();
                fromDataValue = moment(new Date(year, month, day)).format("YYYY-MM-DD")
            }
            document.getElementById("lastInDate").placeholder = lastinDateValue
            document.getElementById("piValidFrom").placeholder = isFromChange ? fromDataValue : this.state.poValidFrom
            this.setState({
                lastInDate: lastinDateValue,
                maxDate: lastinDateValue,
                poValidFrom: isFromChange ? fromDataValue : this.state.poValidFrom
            }, () => {
                this.lastInDate()
            })
            // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.hl4Code != "") {

            let poRows = [...this.state.poRows]

            if (this.state.isMrpRequired && this.state.displayOtb) {
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];
                const date = new Date(lastinDateValue);
                let year = date.getFullYear()
                let monthName = (monthNames[date.getMonth()]);
                let articleList = []
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].articleCode != "" && poRows[i].vendorMrp != "") {
                        let payload = {
                            rowId: poRows[i].gridOneId,
                            articleCode: poRows[i].articleCode,
                            mrp: poRows[i].vendorMrp
                        }
                        articleList.push(payload)


                    }
                }
                let dataa = {
                    articleList: articleList,
                    month: monthName,
                    year: year
                }
                this.setState({
                    changeLastIndate: true
                })
                if (articleList.length != 0) {
                    this.props.multipleOtbRequest(dataa)
                }
            }
            if (this.state.isDeliveryDate) {
                for (let i = 0; i < poRows.length; i++) {
                    poRows[i].deliveryDate = lastinDateValue
                }
                this.setState({
                    poRows: poRows
                })
            }
        }
        else if (e.target.id == "loadIndent") {
            this.setState({
                loadIndent: e.target.value,
                indentLoad: true
            })
        }
        else if (e.target.id == "Adhoc") {
            this.setState({
                indentLoad: false
            })
        }
    }

    openloadIndentSelection(e) {
        this.onEsc()
        var data = {
            no: 1,
            type: this.state.loadIndent == "" || this.state.isModalShow ? 1 : 3,
            search: this.state.isModalShow ? "" : this.state.loadIndent,
            orderId: "",
            supplier: "",
            cityName: "",
            piFromDate: "",
            piToDate: ""
        }
        this.props.loadIndentRequest(data)
        this.setState({
            loadIndentSearch: this.state.isModalShow ? "" : this.state.loadIndent,
            indentValue: this.state.loadIndentId,
            loadIndentModal: true,
            loadIndentAnimation: !this.state.loadIndentAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    onCloseLoadIndent(e) {
        this.setState({
            loadIndentModal: false,
            loadIndentAnimation: !this.state.loadIndentAnimation
        });


        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("loadIndentInput").focus()
    }
    updateLoadIndentState(code) {

        this.setState({
            getPoDataRequest: code.sCode,
            loadIndent: code.pattern,
            loadIndentId: code.indentId
        })
        let data = {
            indentNo: code.sCode

        }

        this.props.gen_IndentBasedRequest(data);

        document.getElementById("loadIndentInput").focus()



    }

    departmentSetModal(e) {
        this.setState({
            departmentModal: true,
            departmentSetBasedAnimation: !this.state.departmentSetBasedAnimation,
        })
        let data = {
            no: 1,
            type: 1,
            search: "",
        }
        this.props.departmentSetBasedRequest(data)

    }
    onCloseDepartmentModal(e) {
        this.setState({
            departmentModal: false,
            departmentSetBasedAnimation: !this.state.departmentSetBasedAnimation,
        })
        document.getElementById('setDepartment').focus()
    }
    updateDepartment(data) {
        this.setState({
            setDepartment: data.setDepartment,
            hl3CodeDepartment: data.hl3code,
            poSetVendor: "",
            orderSet: ""
        }, () => {

        })
        this.onClear();


    }

    setVendorModalOpen(e, id) {
        // if (this.state.setDepartment != "") {
        this.onEsc()
        let data = {
            no: 1,
            type: this.state.poSetVendor == "" || this.state.isModalShow ? 1 : 3,
            search: this.state.isModalShow ? "" : this.state.poSetVendor,
            hl3code: this.state.hl3CodeDepartment,
        }
        this.props.selectVendorRequest(data)
        this.setState({
            setVendorSearch: this.state.isModalShow ? "" : this.state.poSetVendor,
            focusId: id,
            setVendor: true,
            setVendorModalAnimation: !this.state.setVendorModalAnimation,
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
        // } else {
        //     this.setState({
        //         errorMassage: "Select Department",
        //         poErrorMsg: true,
        //         focusId: "setDepartment"


        //     })
        // }
    }
    onCloseVendor() {
        this.setState({
            setVendor: false,
            setVendorModalAnimation: !this.state.setVendorModalAnimation,
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("poSetVendor").focus()
    }

    updateVendorPo(data) {
        this.setState({
            poSetVendor: data.poSetVendor,
            poSetVendorCode: data.poSetVendorCode,
            orderSet: ""
        })
        this.onClear();
    }

    onSetOrderNumber(e) {
        this.onEsc()
        if (this.state.lastInDate != "") {
            if (this.state.poSetVendor != "") {
                this.setState({
                    orderNumber: true,
                    orderNumberModalAnimation: !this.state.orderNumberModalAnimation,
                    orderSearch: this.state.isModalShow ? "" : this.state.orderSet
                })
                let data = {
                    no: 1,
                    type: this.state.orderSet == "" || this.state.isModalShow ? 1 : 3,

                    search: this.state.isModalShow ? "" : this.state.orderSet,
                    hl3Code: this.state.hl3CodeDepartment,
                    supplierCode: this.state.poSetVendorCode
                }
                this.props.selectOrderNumberRequest(data)

                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            } else {
                this.setState({
                    focusId: "setVendor",
                    errorMassage: "Select Vendor",
                    poErrorMsg: true

                })
            }
        } else {
            this.setState({
                errorMassage: "Select Last In Date",
                poErrorMsg: true

            })
        }

    }

    closeSetNumber() {
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }

        this.setState({
            orderNumber: false,
            orderNumberModalAnimation: !this.state.orderNumberModalAnimation


        })
        document.getElementById("orderSet").focus()

    }

    updateOrderNumber(data) {

        this.setState({
            orderSet: data
        })
        let payload = {
            orderNo: data,
            validTo: moment(this.state.lastInDate).format("DD-MMM-YYYY"),
            poType: this.state.codeRadio
        }

        this.props.gen_SetBasedRequest(payload)
        document.getElementById("orderSet").focus()

    }

    headerLevelInputBlur = (e) => {
        if(e.target.id == "city" && e.target.value != ''  && !this.state.isCityLovSelect && !this.state.cityModal) {
            let data = {
                type: this.state.city == "" || this.state.isModalShow ? 1 : 3,
                no: 1,
                search: this.state.isModalShow ? "" : this.state.city
            }
            this.props.getAllCityRequest(data)
        } else if(e.target.id == "siteName" && e.target.value != ''  && !this.state.isSiteLovSelect && !this.state.siteModal) {
            let data = {
                type: this.state.siteName == "" || this.state.isModalShow ? 1 : 3,
                no: 1,
                search: this.state.isModalShow ? "" : this.state.siteName
            }
            this.props.procurementSiteRequest(data)
        } else if(e.target.id == "vendor" && e.target.value != ''  && !this.state.isVendorLovSelect && !this.state.supplierModal) {
            let data = {
                no: 1,
                type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                slCode: "",
                name: "",
                address: "",
                department: "",
                departmentCode: "",
                siteCode: this.state.siteCode,
                city: this.state.isCityChecked ? this.state.city : "",
                search: this.state.isModalShow ? "" : this.state.supplier,
            }
            this.props.supplierRequest(data);
        } else if(e.target.id == "transporter" && e.target.value != ''  && !this.state.isTransporterLovSelect && !this.state.trasporterModal) {
            let data = {
                no: 1,
                type: this.state.transporter == "" || this.state.isModalShow ? 1 : 3,
                search: this.state.isModalShow ? "" : this.state.transporter,
                transporterCode: "",
                transporterName: "",
                city: (this.state.isTransporterDependent != false && this.state.transporterWithCity) ? this.state.city : ""

            }
            this.props.getTransporterRequest(data)
        }
    }

    rowLevelInputBlur = (e, id, idx, value, item, index) => {
        console.log('idd', e.target.id, value, this.state.itemModal)
        let poRows = [...this.state.poRows]
        let row = poRows[this.state.selectedRowId -1]
        if(value == "articleCode" && !this.state.poArticle) {
            // poRows.forEach(row => {
                if ((row.isArticleLovSelect == false) && row.articleCode != '') {
                    if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
                        || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
                        this.checkErr()
                    } else {
                        let payload = {
                            pageNo: 1,
                            type: this.state.isModalShow || item.articleCode == "" ? 1 : 3,
                            hl2Code: item.sectionName != "" ? item.sectionCode : "",
                            hl1Code: item.divisionName != "" ? item.divisionCode : "",
                            hl4Code: "",
                            hl4Name: "",
                            hl1Name: item.divisionName,
                            hl2Name: item.sectionName,
                            hl3Name: item.departmentName,
                            hl3Code: item.departmentName != "" ? item.departmentCode : "",
                            divisionSearch: "",
                            sectionSearch: "",
                            departmentSearch: "",
                            articleSearch: this.state.isModalShow ? "" : item.articleCode,
                            basedOn: "article",
                            supplier: this.state.slCode,
                            mrpSearch: "",
                            isMrp: false
                        }
                        console.log('selectedDate')
                        this.setState({
                            basedOn: "article",
                            poArticleSearch: this.state.isModalShow ? "" : item.articleCode,
                            uuid: idx,
                            focusId: idx,
                            selectedRowIndex: index,
                            mrpRangeSearch: false,
                            isMrpFilled: false,
                        },() => this.props.poArticleRequest(payload))
                    }
                }
            // })
        } else if (value == "hsnCode" && !this.state.hsnModal) {
            // poRows.forEach(row => {
                if((row.isHsnLovSelect == false) && row.hsnSacCode != '') {
                    if (item.articleCode != "") {
                        this.setState({
                            hsnRowId: "",
                            focusId: "hsnCode" + idx,
                            selectedRowId: id,
                            selectedRowIndex: index,
                            departmentCode: item.departmentCode,
                            hsnSearch: this.state.isModalShow ? "" : e.target.value,
                        }, () => {
        
                        })
                        let data = {
                            rowId: "",
                            code: item.departmentCode,
                            no: 1,
                            type: item.hsnSacCode == "" || this.state.isModalShow ? 1 : 3,
                            search: this.state.isModalShow ? "" : item.hsnSacCode,
                        }
                        this.props.hsnCodeRequest(data)
                    } else {
                        this.setState({
                            focusId: "articleCode" + idx,
                            errorMassage: "Select Article",
                            poErrorMsg: true
                        })
                    }
                }
            // })
        } else if (value == "mrpRange" && !this.state.poArticle) {
            if (item.divisionCode != "" || item.sectionCode != "" || item.departmentCode != "" || item.articleCode != "") {
                // poRows.forEach(row => {
                    if (row.isMrpRangeLovSelect == false && row.mrpRange != "") {
                        this.setState({
                            poArticleSearch: this.state.isModalShow ? "" : item.mrpRange,
                            uuid: idx,
                            focusId: idx,
                            mrpRangeSearch: true,
                            isMrpFilled: true,
                            selectedRowIndex: index,
                        })
                        let payload = {
                            pageNo: 1,
                            type: this.state.isModalShow || item.mrpRange == "" ? 1 : 3,
                            hl2Code: item.sectionName != "" ? item.sectionCode : "",
                            hl1Code: item.divisionName != "" ? item.divisionCode : "",
                            hl4Code: item.articleCode,
                            hl4Name: item.articleName,
                            hl1Name: item.divisionName,
                            hl2Name: item.sectionName,
                            hl3Name: item.departmentName,
                            hl3Code: item.departmentName != "" ? item.departmentCode : "",
                            divisionSearch: "",
                            sectionSearch: "",
                            departmentSearch: "",
                            articleSearch: "",
                            basedOn: "article",
                            supplier: this.state.slCode,
                            mrpSearch: this.state.isModalShow ? "" : item.mrpRange.split(/\b/)[0],
                            isMrp: true
                        }
                        this.props.poArticleRequest(payload)
                    }
                // })
            }
        } else if(value == "vendorMrp" && !this.state.itemModal) {
            if (this.state.supplier != "") {
                if (item.hsnSacCode.toString() != "") {
                    // poRows.forEach(row => {
                        if (row.isVendorMrpLovSelect == false && row.vendorMrp != "") {
                            this.setState({
                                desc6: item.vendorMrp,
                                selectedRowId: index +1,
                                itemCodeId: "vendorMrp" + index,
                                selectedRowIndex: index,
                                startRange: item.mrpStart,
                                endRange: item.mrpEnd,
                                mrpSearch: this.state.isModalShow ? "" : item.vendorMrp,
                            }, () => {
                                var data = {
                                    code: item.articleCode,
                                    type: item.vendorMrp == "" || this.state.isModalShow ? 1 : 3,
                                    search: this.state.isModalShow ? "" : item.vendorMrp,
            
                                    no: 1,
                                    mrpRangeFrom: item.mrpStart,
                                    mrpRangeTo: item.mrpEnd
                                }
                                this.props.poItemcodeRequest(data)
                            })
                        }
                    // })
                } else {
                    this.setState({
                        errorMassage: "Select HSN or SAC code",
                        poErrorMsg: true,
                        focusId: "hsnCode" + index,
                    })
                }
            } else {
                this.setState({
                    errorMassage: "Select Vendor",
                    poErrorMsg: true,
                    focusId: "vendor",
                })
            }
        }
    }

    openSiteModal() {
        this.onEsc()
        setTimeout(() => {
            if (!this.state.headerLevelErrMsg) {
                let data = {
                    type: this.state.siteName == "" || this.state.isModalShow ? 1 : 3,
                    no: 1,
                    search: this.state.isModalShow ? "" : this.state.siteName
                }
                this.props.procurementSiteRequest(data)
                this.setState({
                    siteSearch: this.state.isModalShow ? "" : this.state.siteName,
                    siteModal: true,
                    siteModalAnimation: !this.state.siteModalAnimation
                })
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            }
        }, 10);


    }
    closeSiteModal() {
        this.setState({
            siteModal: false,
            siteModalAnimation: !this.state.siteModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("siteName").focus()
    }

    updateSite(data) {
        this.setState({
            siteCode: data.siteCode,
            siteName: data.siteName
        })
        document.getElementById("siteName").focus()
    }

    openCityModal() {

        this.onEsc();
        setTimeout(() => {
            if(!this.state.headerLevelErrMsg) {
                if (this.state.isSiteExist == 'true') {
                    if (this.state.siteName != "") {
                        console.log('siteif', this.state.siteName)
                        let data = {
                            type: this.state.city == "" || this.state.isModalShow ? 1 : 3,
                            no: 1,
                            search: this.state.isModalShow ? "" : this.state.city
                        }
                        this.props.getAllCityRequest(data)
                        this.setState({
                            citySearch: this.state.isModalShow ? "" : this.state.city,
                            cityModal: true,
                            cityModalAnimation: !this.state.cityModalAnimation
                        })
                        document.onkeydown = function (t) {
                            if (t.which == 9) {
                                return false;
                            }
                        }
                    }
                    else {
                        this.setState({
                            focusId: "site",
                            errorMassage: "Select site",
                            poErrorMsg: true
                        })
                    }
                }
                else {
                    let data = {
                        type: this.state.city == "" || this.state.isModalShow ? 1 : 3,
                        no: 1,
                        search: this.state.isModalShow ? "" : this.state.city
                    }
                    this.props.getAllCityRequest(data)
                    this.setState({
                        citySearch: this.state.isModalShow ? "" : this.state.city,
                        cityModal: true,
                        cityModalAnimation: !this.state.cityModalAnimation
                    })
                    document.onkeydown = function (t) {
                        if (t.which == 9) {
                            return false;
                        }
                    }
                }
            }
        }, 10);
    }

    closeCity() {
        this.setState({
            cityModal: false,
            cityModalAnimation: !this.state.cityModalAnimation
        })
        document.getElementById("city").focus()
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateCity(data) {
        this.setState({
            isCityLovSelect: true,
            city: data.city,
            supplier: "",
            stateCode: "",
            slCode: "",
            slName: "",
            slAddr: "",
            transporterCode: "",
            transporterName: "",
            leadDays: "",
            transporter: "",
            tradeGrpCode: "",
            term: "",
            termCode: "",
            termName: "",
            gstInNo: "",
            slCityName: "",
            poAmount: 0,
            poQuantity: 0,
            supplierCode: ""
        }, () => {
            this.resetPoRows();


        })


    }
    dateVali(e) {

        if (e.target.id == "piValidFrom") {
            let restrictedStartDate = this.state.restrictedStartDate
            let restrictedEndDate = this.state.restrictedEndDate
            let flag = false
            if (restrictedStartDate != "" && restrictedEndDate != "" && e.target.value != "") {
                // if (e.target.value != '' && !this.state.isDeliveryDate) {
                //     let lInDate = ""
                //     let mnth = ""
                //     let day = ""
                //     let yr = ""
                //     let finalLInDate = ""
                //     lInDate = new Date(e.target.value)
                //     lInDate.setDate(lInDate.getDate() + 14)
                //     mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                //     day = ("0" + lInDate.getDate()).slice(-2);
                //     yr = lInDate.getFullYear()
                //     finalLInDate = [yr, mnth, day].join("-")
                //     this.setState({
                //         lastInDate: finalLInDate
                //     })
                // }
                if (restrictedStartDate <= e.target.value && e.target.value <= restrictedEndDate) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "You are unable to save PO(Purchase Order) between " + moment(restrictedStartDate).format("DD-MMM-YYYY") + " to " + moment(restrictedEndDate).format("DD-MMM-YYYY"),

                    })
                    flag = true

                } else {
                    e.target.placeholder = e.target.value,
                        this.setState({
                            poValidFrom: e.target.value
                        }, () => {
                            this.poValid();
                            this.lastInDate();
                        });
                }
            } else {
                e.target.placeholder = e.target.value,
                    this.setState({
                        poValidFrom: e.target.value
                    }, () => {
                        this.poValid();
                        this.lastInDate();
                    });
            }


            if (!flag) {
                let poRows = [...this.state.poRows]
                if (e.target.value != "") {
                    let lInDate = new Date(e.target.value)
                    lInDate.setDate(lInDate.getDate() + 14)
                    let mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                    let day = ("0" + lInDate.getDate()).slice(-2);
                    let yr = lInDate.getFullYear()
                    let finalLInDate = [yr, mnth, day].join("-")
                    var d = new Date(e.target.value);
                    var year = d.getFullYear();
                    var month = d.getMonth();
                    var daay = d.getDate();
                    var c = moment(new Date(year, month, day)).format("YYYY-MM-DD")
                    let dateTo = new Date(this.state.lastInDate);
                    let yearTo = dateTo.getFullYear();
                    let monthTo = dateTo.getMonth();
                    let dayTo = dateTo.getDate();
                    let fromAfterGap = moment(new Date(yearTo, monthTo, dayTo -14)).format("YYYY-MM-DD")
                    let willToChange = false;
                    let bufferDate = moment(new Date(yearTo, monthTo, dayTo - this.state.bufferDays)).format("YYYY-MM-DD")
                    
                    if (new Date(e.target.value).getTime() < new Date(bufferDate).getTime()){
                        willToChange = true;
                        finalLInDate = moment(new Date(year, month, daay + this.state.bufferDays)).format("YYYY-MM-DD")
                    }
                    if (new Date(e.target.value).getTime() > new Date(fromAfterGap).getTime()){
                        willToChange = true;
                        finalLInDate = moment(new Date(year, month, daay +14)).format("YYYY-MM-DD")
                    }
                    document.getElementById("lastInDate").placeholder = willToChange ? finalLInDate : this.state.lastInDate,
                    this.setState({
                        lastInDate: willToChange ? finalLInDate : this.state.lastInDate,
                        minDate: getDate(),
                        maxDate: willToChange ? finalLInDate : this.state.maxDate,
                    }, () => {
                        this.lastInDate();
                    });

                    // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.hl4Code != "") {

                    if (this.state.isMrpRequired && this.state.displayOtb) {
                        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                        ];
                        const date = new Date(finalLInDate);
                        let year = date.getFullYear()
                        let monthName = (monthNames[date.getMonth()]);
                        let articleList = []
                        for (let i = 0; i < poRows.length; i++) {
                            if (poRows[i].articleCode != "" && poRows[i].vendorMrp != "") {
                                let payload = {
                                    rowId: poRows[i].gridOneId,
                                    articleCode: poRows[i].articleCode,
                                    mrp: poRows[i].vendorMrp
                                }
                                articleList.push(payload)


                            }
                        }
                        let dataa = {
                            articleList: articleList,
                            month: monthName,
                            year: year
                        }
                        this.setState({
                            changeLastIndate: true
                        })
                        if (articleList.length != 0) {
                            // this.props.multipleOtbRequest(dataa)
                        }
                    }
                    for (let i = 0; i < poRows.length; i++) {
                        poRows[i].deliveryDate = finalLInDate
                    }
                    this.setState({
                        poRows: poRows
                    })
                    // }

                    // else if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo") && this.state.hl4Code != "") {

                    //     this.onClear();
                    //     this.setState({
                    //         orderSet: ""
                    //     })

                    // }
                }
                else {
                    //  this.onClear();
                    // this.setState({
                    //     lastInDate: "",
                    //      orderSet: ""
                    // })
                    document.getElementById("lastInDate").placeholder = "Last in date"
                }
            }

        }

    }

    checkedHandler = (e) =>{
        if(e.target.id == 'transporterCheck'){
            this.setState(prevState => ({
                transporterWithCity: !prevState.transporterWithCity
            }))
        } else if(e.target.id == 'agentCheck'){
            this.setState(prevState => ({
                agentNameWithCity: !prevState.agentNameWithCity
            }))
        }
    }

    openAgentHandler = () => {
        this.onEsc()
        this.setState({
            agentModal: true,
        })
        let data = {
            type: this.state.poUdf1 ? 3 : 1,
            pageNo:1,
            search: this.state.poUdf1,
            cityName: this.state.agentNameWithCity ? this.state.city : ""
        }
        this.props.getAgentNameRequest(data)
    }


    openSupplier(e, id) {
        this.onEsc()
        setTimeout(() => {
            console.log('headererr', this.state.headerLevelErrMsg)
            if(!this.state.headerLevelErrMsg) {
                if ((this.state.isCityExist == true || this.state.isSiteExist == 'true') && !this.state.headerLevelErrMsg) {
                    if (this.state.isCityExist == true) {
                        if (this.state.city != "") {
        
                            var data = {
                                no: 1,
                                type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                                slCode: "",
                                name: "",
                                address: "",
                                department: "",
                                departmentCode: "",
                                siteCode: this.state.siteCode,
                                city: this.state.isCityChecked ? this.state.city : "",
                                search: this.state.isModalShow ? "" : this.state.supplier,
                            }
                            this.props.supplierRequest(data);
                            this.setState({
                                supplierModal: true,
                                supplierModalAnimation: !this.state.supplierModalAnimation
                            });
                            document.onkeydown = function (t) {
                                if (t.which == 9) {
                                    return false;
                                }
                            }
                        } else {
        
        
                            this.setState({
                                focusId: "city",
                                errorMassage: "Select city",
                                poErrorMsg: true
                            })
                        }
                    }
                    else {
                        if (this.state.siteName != "") {
        
                            var data = {
                                no: 1,
                                type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                                slCode: "",
                                name: "",
                                address: "",
                                department: "",
                                departmentCode: "",
                                siteCode: this.state.siteCode,
                                city: this.state.city,
        
                                search: this.state.isModalShow ? "" : this.state.supplier,
                            }
                            this.props.supplierRequest(data);
                            this.setState({
                                supplierModal: true,
                                supplierModalAnimation: !this.state.supplierModalAnimation
        
                            });
                            document.onkeydown = function (t) {
                                if (t.which == 9) {
                                    return false;
                                }
                            }
                        }
                        else {
        
        
                            this.setState({
                                focusId: "city",
                                errorMassage: "Select city",
                                poErrorMsg: true
                            })
                        }
                    }
                    // else {
                    //     if (this.state.siteName != "") {
        
                    //         var data = {
                    //             no: 1,
                    //             type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                    //             slCode: "",
                    //             name: "",
                    //             address: "",
                    //             department: "",
                    //             departmentCode: "",
                    //             siteCode: this.state.siteCode,
                    //             city: this.state.city,
        
                    //             search: this.state.isModalShow ? "" : this.state.supplier,
                    //         }
                    //         this.props.supplierRequest(data);
                    //         this.setState({
                    //             supplierModal: true,
                    //             supplierModalAnimation: !this.state.supplierModalAnimation
        
                    //         });
                    //         document.onkeydown = function (t) {
                    //             if (t.which == 9) {
                    //                 return false;
                    //             }
                    //         }
                    //     } 
                    //     //ria
                    //     else {
        
        
                    //         this.setState({
                    //             focusId: "site",
                    //             errorMassage: "Select site",
                    //             poErrorMsg: true
                    //         })
                    //     }
                    // }
        
                } else {
        
                    var data = {
                        no: 1,
                        type: this.state.supplier == "" || this.state.isModalShow ? 1 : 3,
                        slCode: "",
                        name: "",
                        address: "",
                        department: "",
                        departmentCode: "",
                        siteCode: this.state.siteCode,
                        city: "",
                        search: this.state.isModalShow ? "" : this.state.supplier,
                    }
                    document.onkeydown = function (t) {
                        if (t.which == 9) {
                            return false;
                        }
                    }
                    this.props.supplierRequest(data);
                    this.setState({
                        supplierModal: true,
                        supplierModalAnimation: !this.state.supplierModalAnimation
        
                    });
                }
            }
        }, 10);



    }

    onCloseAgent(e) {

        this.setState({
            agentModal: false,
        });
        document.getElementById("agentName").focus()
    }


    onCloseSupplier(e) {

        this.setState({
            supplierModal: false,
            supplierModalAnimation: !this.state.supplierModalAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("vendor").focus()
    }

    updateSupplierState(data) {
        this.setState({
            supplier: data.supplier,
            stateCode: data.stateCode,
            slCode: data.slcode,
            slName: data.slName,
            slAddr: data.slAddr,
            transporterCode: data.transporterCode,
            transporterName: data.transporterName,
            flag: this.state.flag + 1,
            transporter: data.transporter,
            tradeGrpCode: data.tradeGrpCode,
            term: data.termName,
            termCode: data.purtermMainCode,
            termName: data.termName,
            gstInNo: data.gstInNo,
            slCityName: data.city,
            poAmount: 0,
            poQuantity: 0,
            supplierCode: data.slcode,
            itemCodeList: [],
            isVendorLovSelect: true,

        }, () => {
            this.supplier();
            this.transporter();
        })
        if (data.stateCode == "" && data.stateCode == undefined) {
            this.setState({
                focusId: "vendor",
                errorMassage: "Supplier GSTIN can't be null",
                poErrorMsg: true
            })
        }
        this.props.leadTimeRequest(data.slcode)

        setTimeout(() => {
            this.resetPoRows();

        }, 10)
        document.getElementById("vendor").focus()
    }
    openTransporterSelection(e, id) {
        this.onEsc()
        setTimeout(() => {
            if (!this.state.headerLevelErrMsg) {
                if (this.state.supplier != "") {
                    var data = {
                        no: 1,
                        type: this.state.transporter == "" || this.state.isModalShow ? 1 : 3,
                        search: this.state.isModalShow ? "" : this.state.transporter,
                        transporterCode: "",
                        transporterName: "",
                        city: (this.state.isTransporterDependent != false && this.state.transporterWithCity) ? this.state.city : ""
        
                    }
                    this.props.getTransporterRequest(data)
                    this.setState({
                        trasporterModal: true,
                        transporterAnimation: !this.state.transporterAnimation,
                        transporterSearch: this.state.isModalShow ? "" : this.state.transporter,
                    });
                    document.onkeydown = function (t) {
                        if (t.which == 9) {
                            return false;
                        }
                    }
                }
                else {
                    this.setState({
                        focusId: "vendor",
                        errorMassage: "Select Vendor ",
                        poErrorMsg: true
                    })
                }
            }
        }, 10);

    }
    onCloseTransporter(e) {
        this.setState({
            trasporterModal: false,
            transporterAnimation: !this.state.transporterAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("transporter").focus()
    }

    updateAgentState = (data) => {
        this.setState({
            poUdf1: data.agentName,
        })
    }

    updateTransporterState(code) {

        this.setState({

            transporterCode: code.transporterCode,
            transporterName: code.transporterName,

            transporter: code.transporterName,
            isTransporterLovSelect: true,
        },
            () => {
                this.transporter();
            })

        document.getElementById("transporter").focus()


    }

    calculateMarginValidation = () =>{
        let poRows = [...this.state.poRows];
        let flag = false;
        for(let i = 0; i< poRows.length; i++){
            if(poRows[i].marginRule != undefined && poRows[i].marginRule != null && poRows[i].marginRule != "" && poRows[i].marginRule != 'N/A'){
                let marginRule = Number(poRows[i].marginRule).toFixed(2)
                let calculatedMargin = Number(poRows[i].calculatedMargin).toFixed(2)
                if (marginRule > calculatedMargin) {
                    flag = true 
                    break
                }
            }
        }
        if(flag){
            this.setState({
                calculatedMarginErr: true,
                poErrorMsg: true,
                errorMassage: "Calculated Margin can not be less than Margin Rule. Please change Rate and retry."
            })
        } else{
            this.setState({
                calculatedMarginErr: false,
            })
        }
    }

    typeOfBuying() {
        if (this.state.typeOfBuying != "") {
            this.setState({ typeOfBuyingErr: false })
        } else {
            this.setState({ typeOfBuyingErr: true })
        }
    }
    handleChange(e) {
        if (e.target.id == "typeofBuying") {
            this.setState({ typeOfBuying: e.target.value })
        }


    }
    //validationn msg

    loadIndent(e) {
        if (this.state.loadIndent == "") {
            this.setState({
                loadIndenterr: true
            });
        } else {
            this.setState({
                loadIndenterr: false
            });
        }

    }
    poValid(e) {
        if (this.state.poValidFrom == "") {
            this.setState({
                poValidFromerr: true
            });
        } else {
            this.setState({
                poValidFromerr: false
            });
        }
    }

    lastInDate(e) {
        if (this.state.lastInDate == "") {
            this.setState({
                lastInDateerr: true
            });
        } else {
            this.setState({
                lastInDateerr: false
            });
        }
    }


    supplier(e) {
        if (this.state.supplier == "") {
            this.setState({
                vendorerr: true
            });
        } else {
            this.setState({
                vendorerr: false
            });
        }
    }

    transporter(e) {
        if (this.state.transporterValidationPi && this.state.isDisplayTransporterPi) {
            if (this.state.transporter == "") {
                this.setState({
                    transportererr: true
                });
            } else {
                this.setState({
                    transportererr: false
                });
            }
        } else {
            this.setState({
                transportererr: false
            });
        }
    }
    poUdf1 = () => {
        if(this.state.isMandatePOUdf1 && this.state.isDisplayPOUdf1){
            if(this.state.poUdf1 == "") {
                this.setState({
                    poUdf1err : true,
                })
            } else {
                this.setState({
                    poUdf1err : false,
                })
            }
        }
    }
    poUdf2 = () => {
        if(this.state.isMandatePOUdf2 && this.state.isDisplayPOUdf2){
            if(this.state.poUdf2 == "yyyy-mm-dd") {
                this.setState({
                    poUdf2err : true,
                })
            } else {
                this.setState({
                    poUdf2err : false,
                })
            }
        }
    }
    poUdf3 = () => {
        if(this.state.isMandatePOUdf3 && this.state.isDisplayPOUdf3){
            if(this.state.poUdf3 == "") {
                this.setState({
                    poUdf3err : true,
                })
            } else {
                this.setState({
                    poUdf3err : false,
                })
            }
        }
    }
    poUdf4 = () => {
        if(this.state.isMandatePOUdf4 && this.state.isDisplayPOUdf4){
            if(this.state.poUdf4 == "") {
                this.setState({
                    poUdf4err : true,
                })
            } else {
                this.setState({
                    poUdf4err : false,
                })
            }
        }
    }
    poUdf5 = () => {
        if(this.state.isMandatePOUdf5 && this.state.isDisplayPOUdf5){
            if(this.state.poUdf5 == "") {
                this.setState({
                    poUdf5err : true,
                })
            } else {
                this.setState({
                    poUdf5err : false,
                })
            }
        }
    }
    site(e) {
        if (this.state.siteName == "") {
            this.setState({
                siteNameerr: true
            });
        } else {
            this.setState({
                siteNameerr: false
            });
        }
    }
    city(e) {
        if (this.state.city == "") {
            this.setState({
                cityerr: true
            });
        } else {
            this.setState({
                cityerr: false
            });
        }
    }


    icode(e) {
        if (this.state.itemCodeList.length == 0) {
            this.setState({
                itemcodeerr: true
            });
        } else {
            this.setState({
                itemcodeerr: false
            });
        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false,
            headerLevelErrMsg: false,
        })
        if (document.getElementById(this.state.focusId) != null) {
            document.getElementById(this.state.focusId).focus()
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }



    openNewIcode(id, articleCode, idx) {

        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            if (this.state.slCode != "") {
                if (articleCode != "") {
                    this.setState({
                        icodeModalAnimation: !this.state.icodeModalAnimation,
                        itemBarcodeModal: true,
                        focusId: "itemCode" + idx,
                        selectedRowId: id,
                        hl4Code: articleCode
                    }, () => {

                    })

                    let payload = {
                        type: 1,
                        no: 1,
                        search: "",
                        articleCode: articleCode,
                        supplierCode: this.state.slCode,
                        siteCode: this.state.siteCode
                    }

                    this.props.getPoItemCodeRequest(payload)
                } else {
                    this.setState({
                        errorMassage: "Article code  is mandatory",
                        poErrorMsg: true,
                        focusId: "articleCode" + idx

                    })
                }
            } else {
                this.setState({
                    errorMassage: "Supplier code  is mandatory",
                    poErrorMsg: true,
                    focusId: "vendor"

                })
            }
        }
    }
    closeNewIcode() {
        this.setState({
            icodeModalAnimation: !this.state.icodeModalAnimation,
            itemBarcodeModal: false,
        })
        document.getElementById(this.state.focusId).focus()
    }
    updateNewIcode(data) {
        let poRows = [...this.state.poRows]
        let articleCode = ""
        poRows.map((e) => {
            if (e.gridOneId == this.state.selectedRowId) {
                e.itemCodeList = data
                articleCode = e.articleCode
            }

        })
        this.setState({
            poRows
        }, () => {

        })
        let payload = {
            validTo: moment(this.state.lastInDate).format("DD-MMM-YYYY"),
            articleCode: articleCode,
            items: data
        }
        this.props.poItemBarcodeRequest(payload)
    }



    openHsnCodeModal(id, articleCode, idx, departmentCode, hsnSacCode, hsnId) {
        this.onEsc()
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            if (articleCode != "") {
                this.setState({
                    hsnRowId: "",
                    hsnModal: true,
                    hsnModalAnimation: !this.state.hsnModalAnimation,
                    focusId: "hsnCode" + idx,
                    selectedRowId: id,
                    departmentCode: departmentCode,
                    hsnSearch: this.state.isModalShow ? "" : hsnSacCode,
                    hsnId: hsnId

                }, () => {

                })
                let data = {
                    rowId: "",
                    code: departmentCode,
                    no: 1,
                    type: hsnSacCode == "" || this.state.isModalShow ? 1 : 3,
                    search: this.state.isModalShow ? "" : hsnSacCode,
                }
                this.props.hsnCodeRequest(data)
            } else {
                this.setState({
                    focusId: "articleCode" + idx,
                    errorMassage: "Select Article",
                    poErrorMsg: true
                })
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }

    }
    closeHsnModal() {
        this.setState({
            hsnModal: false,
            hsnModalAnimation: !this.state.hsnModalAnimation
        })
        document.getElementById(this.state.focusId).focus()
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateHsnCode(data) {
        let poRows = [...this.state.poRows]
        poRows.forEach((e) => {
            if (e.gridOneId == this.state.selectedRowId) {
                e.hsnSacCode = data.hsnSacCode
                e.hsnCode = data.hsnCode
            }

        })


        this.setState({
            poRows
        }, () => {


            let poRows = [...this.state.poRows]

            let lineItemArray = []
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.state.selectedRowId) {

                    for (let j = 0; j < poRows[i].lineItem.length; j++) {
                        if (poRows[i].lineItem[j].setQty != "" && poRows[i].finalRate != "" && poRows[i].finalRate != 0) {
                            let taxData = {
                                hsnSacCode: data.hsnSacCode,
                                qty: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total),
                                rate: poRows[i].finalRate,
                                rowId: poRows[i].lineItem[j].gridTwoId,
                                designRowid: Number(this.state.selectedRowId),
                                basic: Number(poRows[i].lineItem[j].quantity) * Number(poRows[i].finalRate), // lineItem tax data
                                clientGstIn: sessionStorage.getItem('gstin'),
                                piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                supplierGstinStateCode: this.state.stateCode,
                                purtermMainCode: this.state.termCode,
                                siteCode: this.state.siteCode
                            }
                            lineItemArray.push(taxData)
                        }
                    }

                }
            }
            if (lineItemArray.length != 0) {
                this.setState({
                    lineItemChange: true
                }, () => {
                    this.props.multipleLineItemRequest(lineItemArray)

                }
                )

            }

            document.getElementById(this.state.focusId).focus()
        })

    }
    _handleKeyPressRow(e, idName, count) {

        if (e.key === "F7" || e.key === "F2") {
            let idd = idName + count;
            document.getElementById(idd).click();
        }
        if (e.key === "ArrowDown") {
            count++;

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }
        }
        if (e.key === "ArrowUp") {
            count--

            let arrRght = idName + count

            if (document.getElementById(arrRght) != undefined) {
                document.getElementById(arrRght).focus();
            }

        }
        if (e.key == "Enter") {
            document.getElementById(idName + "Div" + count) != null ? document.getElementById(idName + "Div" + count).click() : null
        }

    }
    openItemCode(e, mrp, id, idFocus, idx, hsnSacCode, mrpStart, mrpEnd, articleCode) {
        this.onEsc()
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {
            let idd = id
            let mrpp = mrp
            if (this.state.supplier != "") {
                if (hsnSacCode.toString() != "") {
                    this.setState({
                        desc6: mrpp,
                        descId: idd,
                        itemModal: true,
                        itemModalAnimation: !this.state.itemModalAnimation,
                        itemCodeId: idFocus + idx,
                        hl4Code: articleCode,
                        selectedRowId: id,
                        code: articleCode,
                        startRange: mrpStart,
                        endRange: mrpEnd,
                        mrpSearch: this.state.isModalShow ? "" : mrp,
                        mrpId: idFocus + idx,
                        focusId: idFocus + idx,

                    }, () => {

                    })
                    var data = {
                        code: articleCode,
                        type: mrp == "" || this.state.isModalShow ? 1 : 3,
                        search: this.state.isModalShow ? "" : mrp,

                        no: 1,
                        mrpRangeFrom: mrpStart,
                        mrpRangeTo: mrpEnd
                    }
                    this.props.poItemcodeRequest(data)
                } else {
                    this.setState({
                        errorMassage: "Select HSN or SAC code",
                        poErrorMsg: true,
                        focusId: "hsnCode" + idx,
                    })
                }
            } else {
                this.setState({
                    errorMassage: "Select Vendor",
                    poErrorMsg: true,
                    focusId: "vendor",
                })
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeItemmModal() {
        document.getElementById(this.state.itemCodeId).focus()
        this.setState({
            itemModal: false,
            itemModalAnimation: !this.state.itemModalAnimation
        })

        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }


    updateItem = (data) => {

        document.getElementById(this.state.itemCodeId).focus()
        let poRows = [...this.state.poRows]
        let articleCode = ""
        let calculatedMargin = []

        for (var i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                poRows[i].vendorMrp = data.mrp
                poRows[i].rsp = data.rsp
                poRows[i].mrp = data.mrp
                poRows[i].rate = ""
                poRows[i].wsp = data.wsp === undefined ? "" : data.wsp
                poRows[i].discount = {
                    discountType: "",
                    discountValue: "",
                    discountPer: true
                }
                poRows[i].finalRate = ""
                poRows[i].isVendorMrpLovSelect = true
                articleCode = poRows[i].articleCode
                calculatedMargin = poRows[i].calculatedMargin
            }
        }
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        // const date = new Date(this.state.lastInDate);
        // var year = date.getFullYear()
        // var monthName = (monthNames[date.getMonth()]);
        // const date = new Date(this.state.lastInDate);
        // const date = new Date();
        // +        // const d = date.setDate(date.getDate() + 14);
        function addDays(dateObj, numDays) {
            dateObj.setDate(dateObj.getDate() + numDays);
            return dateObj;
        }
        var d = addDays(new Date(), 14);
        var year = d.getFullYear()
        var monthName = (monthNames[d.getMonth()]);

        let dataa = {
            rowId: data.id,
            articleCode: articleCode,
            mrp: data.mrp,
            month: monthName,
            year: year
        }
        data.mrp != "" && this.state.isMrpRequired && this.state.displayOtb ? this.props.otbRequest(dataa) : null


        this.setState({
            poRows: poRows
        }, () => {

            if (calculatedMargin.length != 0) {
                this.multiMarkUp(this.state.selectedRowId)
            }
            this.gridFirst()
        })
    }

    handleChangeGridOne(id, e, idx) {
        let idd = id
        let r = [...this.state.poRows];

        if (e.target.id == "vendorDesign" + idx) {
            if (this.state.supplier != "") {
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == idd) {
                        r[i].vendorDesign = e.target.value
                    }
                }

                this.setState({
                    poRows: r,

                }, () => {

                })
                const t = this
                setTimeout(function () {
                    t.gridFirst();
                }, 1000)
            } else {
                this.setState({
                    focusId: "vendor",
                    errorMassage: "Supplier is mandatory",
                    poErrorMsg: true
                })
            }
        }
        else if (e.target.id == "rsp" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        r[i].rsp = e.target.value
                    }
                }
                this.setState({
                    poRows: r
                }, () => {

                })
            }
        }
        else if (e.target.id == "mrp" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        r[i].mrp = e.target.value
                    }
                }
                this.setState({
                    poRows: r
                }, () => {

                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "rate" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].vendorDesign == "" && !this.state.isVendorDesignNotReq) {
                            this.setState({
                                focusId: "rate",
                                poErrorMsg: true,
                                errorMassage: "Vendor Design is Complusory "
                            })
                        } else if (r[i].vendorMrp == "" && this.state.isMrpRequired && this.state.mrpValidation == true) {
                            this.setState({
                                focusId: "vendorMrp" + idx,
                                poErrorMsg: true,
                                errorMassage: "mrp is Complusory "
                            })
                        }
                        else if (Number(e.target.value) > Number(r[i].mrp) && this.state.isMRPEditable && this.state.mrpValidation == true) {
                            this.setState({
                                focusId: "rate" + idx,
                                poErrorMsg: true,
                                errorMassage: "Rate can't be greater than mrp!"
                            })
                        } else if (Number(e.target.value) > Number(r[i].vendorMrp) && this.state.isMrpRequired && this.state.mrpValidation ==  true) {
                            this.setState({
                                focusId: "rate" + idx,
                                poErrorMsg: true,
                                errorMassage: "Rate can't be greater than mrp!"
                            })
                        }
                        else {
                            r[i].rate = e.target.value
                            r[i].netRate = e.target.value
                            r[i].wsp = e.target.value
                        }
                    }
                }
                this.setState({
                    poRows: r
                }, () => {

                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "netrate" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].vendorDesign == "") {
                            if (!this.state.isVendorDesignNotReq) {
                                this.setState({
                                    errorId: "netrate",
                                    poErrorMsg: true,
                                    errorMassage: "Vendor Design is Complusory "
                                })
                            }
                        } else if (r[i].vendorMrp == "" && this.state.isMrpRequired && this.state.mrpValidation == true) {
                            this.setState({
                                errorId: "netrate",
                                poErrorMsg: true,
                                errorMassage: "mrp is Complusory "
                            })

                        } else {
                            r[i].rate = e.target.value
                            r[i].netRate = e.target.value
                            r[i].wsp = e.target.value
                        }
                    }
                }
                this.setState({
                    poRows: r
                }, () => {

                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
        else if (e.target.id == "date" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].deliveryDate = e.target.value
                }
                this.setState({
                    poRows: r
                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        } else if (e.target.id == "remarks" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == id) {
                    r[i].remarks = e.target.value
                }
                this.setState({
                    poRows: r
                }, () => {

                })
            }
        }
        else if (e.target.id == "wsp" + idx) {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].gridOneId == idd) {
                        if (r[i].rsp == "") {
                            this.setState({
                                errorId: "wsp",
                                poErrorMsg: true,
                                errorMassage: "RSP is compulsory"
                            })
                        } else if (Number(e.target.value) > Number(r[i].rsp)) {
                            this.setState({
                                errorId: "wsp",
                                poErrorMsg: true,
                                errorMassage: "WSP can't be greater than RSP"
                            })

                        } else {
                            r[i].wsp = e.target.value
                        }
                    }
                }
                this.setState({
                    poRows: r
                }, () => {

                })
            }
            const t = this
            setTimeout(function () {
                t.gridFirst();
            }, 1000)
        }
    }

    mouseOverCmprRate(dataRate) {
        let ellRate = document.getElementById(`toolIdRate${dataRate}`)
        var viewportOffset = ellRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - 55;

        if (!document.getElementById(`moveLeftRate${dataRate}`)) {
        } else {
            document.getElementById(`moveLeftRate${dataRate}`).style.left = left;
            document.getElementById(`moveLeftRate${dataRate}`).style.top = top;
        }


    }
    mouseOverCmprMrp(dataRate) {
        let ellRate = document.getElementById(`toolIdMrp${dataRate}`)
        var viewportOffset = ellRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - 55;

        if (!document.getElementById(`moveLeftMrp${dataRate}`)) {
        } else {
            document.getElementById(`moveLeftMrp${dataRate}`).style.left = left;
            document.getElementById(`moveLeftMrp${dataRate}`).style.top = top;
        }


    }
    rateValueOnFocus(e, rowId, type, finalRate) {

        let focusedObj = this.state.focusedObj
        focusedObj.value = e.target.value,
            focusedObj.finalRate = finalRate
        focusedObj.type = type,
            focusedObj.rowId = rowId,
            focusedObj.cname = "",
            focusedObj.radio = this.state.codeRadio,
            focusedObj.colorObj = {
                color: [],
                colorList: []
            }


        this.setState({
            rateFocus: e.target.value,
            focusedObj: focusedObj
        })

    }
    onBlurRate(idd, e) {
        let r = [...this.state.poRows]
        if (e.target.value != "") {
            if (Number(e.target.value) == 0) {

                this.setState({
                    errorMassage: "Rate value can't be less than or equal to Zero(0)",
                    poErrorMsg: true

                })
                for (let i = 0; i < r.length; i++) {
                    if (r[i].gridOneId == idd) {
                        r[i].rate = ""
                        r[i].finalRate = ""
                    }
                }
                this.setState({
                    poRows: r
                })
            } else {

                if (e.target.value != this.state.rateFocus) {
                    let calculatedMarginValue = ""
                    let finalRate = ""

                    for (let i = 0; i < r.length; i++) {
                        if (r[i].gridOneId == idd) {
                            if (r[i].discount.discountType != "" && this.state.isDiscountAvail) {

                                if (r[i].discount.discountPer) {
                                    let value = e.target.value * (r[i].discount.discountValue / 100)
                                    r[i].finalRate = (e.target.value - value).toFixed(2)
                                    finalRate = (e.target.value - value).toFixed(2)

                                } else {
                                    r[i].finalRate = (e.target.value - r[i].discount.discountValue).toFixed(2)
                                    finalRate = (e.target.value - r[i].discount.discountValue).toFixed(2)
                                }
                            } else {
                                r[i].finalRate = (e.target.value)
                                finalRate = (e.target.value)

                            }
                        }
                    }
                    let hsnSacCode = ""
                    for (let i = 0; i < r.length; i++) {
                        if (r[i].gridOneId == idd) {
                            hsnSacCode = r[i].hsnSacCode
                            if ((r[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                                let lineItemArray = []

                                for (let j = 0; j < r[i].lineItem.length; j++) {

                                    if (r[i].lineItem[j].setQty != "" && r[i].lineItem[j].finalRate != "" && r[i].lineItem[j].finalRate != 0) {
                                        let taxData = {
                                            hsnSacCode: r[i].hsnSacCode,
                                            qty: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total),
                                            rate: finalRate,
                                            rowId: r[i].lineItem[j].gridTwoId,
                                            designRowid: Number(idd),
                                            basic: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total) * Number(finalRate), //line item tax data
                                            clientGstIn: sessionStorage.getItem('gstin'),
                                            piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                            supplierGstinStateCode: this.state.stateCode,
                                            purtermMainCode: this.state.termCode,
                                            siteCode: this.state.siteCode
                                        }
                                        lineItemArray.push(taxData)
                                        
                                    }
                                }
                                if (hsnSacCode != "" || hsnSacCode != null) {
                                    if (lineItemArray.length != 0) {
                                        this.setState({
                                            lineItemChange: true
                                        }, () => {
                                            this.props.multipleLineItemRequest(lineItemArray)
                                            

                                        }
                                        )
                                    }
                                } else {

                                    this.setState({
                                        errorMassage: "HSN code is complusory",
                                        poErrorMsg: true

                                    })
                                }

                            }
                        }
                    }



                    this.setState({
                        poRows: r,

                    })
                }
            }
        } else {
            console.log("came here")
            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == idd) {

                    r[i].finalRate = ""
                }
            }

            this.setState({
                poRows: r,

            })
        }
    }

    openDiscountModal(discount, grid, idx) {
        if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
            || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
            this.checkErr()
        }
        else {

            let poRows = [...this.state.poRows]
            let mrp = ""
            let articleCode = ""
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == grid) {
                    mrp = poRows[i].vendorMrp
                    articleCode = poRows[i].articleCode
                }

            }
            if (mrp != "" || !this.state.isMrpRequired) {
                this.setState({
                    selectedDiscount: discount,
                    discountGrid: grid,
                    focusId: "discount" + idx,
                    // discountModal: true,
                    discountMrp: mrp,
                    disId: idx,
                    discountSearch: discount
                }, () => {

                })
                if (this.state.isDiscountMap) {
                    let payload = {
                        articleCode: articleCode,
                        mrp: this.state.isMrpRequired ? mrp : 0,
                        discountType: "percentage"
                    }
                    this.props.discountRequest(payload)
                }
            } else {
                this.setState({
                    focusId: "mrp" + idx,
                    errorMassage: "Mrp is compulsory",
                    poErrorMsg: true
                })
            }
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeDiscountModal() {
        document.getElementById(this.state.focusId) != null ? document.getElementById(this.state.focusId).focus() : null

        this.setState({
            discountModal: false
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateDiscountModal(discountData) {
        let poRows = [...this.state.poRows]
        let value = ""
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].gridOneId == discountData.discountGrid) {

                if (poRows[j].discount.discountPer) {
                    value = poRows[j].rate * discountData.discount.discountValue / 100

                    if (Number(poRows[j].rate - value) < 0) {
                        poRows[j].discount = {
                            discountType: "",
                            discountValue: "",
                            discountPer: discountData.discount.discountPer
                        }
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true,

                        })

                    } else {
                        poRows[j].finalRate = (poRows[j].rate - value).toFixed(2)
                        poRows[j].discount = discountData.discount
                    }

                } else {
                    value = poRows[j].rate - discountData.discount.discountValue
                    if (value < 0) {
                        poRows[j].discount = {
                            discountType: "",
                            discountValue: "",
                            discountPer: discountData.discount.discountPer
                        }
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true
                        })

                    } else {
                        poRows[j].finalRate = value
                        poRows[j].discount = discountData.discount
                    }

                }
            }
        }
        this.setState({
            poRows
        }, () => {
            let calculatedMarginValue = ""
            let finalRate = ""
            let r = [...this.state.poRows]
            let hsnSacCode = ""


            for (let i = 0; i < r.length; i++) {
                if (r[i].gridOneId == discountData.discountGrid) {
                    hsnSacCode = r[i].hsnSacCode

                    if ((r[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                        let lineItemArray = []

                        for (let j = 0; j < r[i].lineItem.length; j++) {

                            if (r[i].lineItem[j].setQty != "" && r[i].finalRate != "" && r[i].finalRate != 0) {
                                let taxData = {
                                    hsnSacCode: r[i].hsnSacCode,
                                    qty: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total),
                                    rate: r[i].finalRate,
                                    rowId: r[i].lineItem[j].gridTwoId,
                                    designRowid: Number(discountData.discountGrid),
                                    basic: Number(r[i].lineItem[j].setQty) * Number(r[i].lineItem[j].total) * Number(r[i].finalRate), //lineitem taxdata
                                    clientGstIn: sessionStorage.getItem('gstin'),
                                    piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                    supplierGstinStateCode: this.state.stateCode,
                                    purtermMainCode: this.state.termCode,
                                    siteCode: this.state.siteCode
                                }
                                lineItemArray.push(taxData)
                            }
                        }
                        if (lineItemArray.length != 0) {
                            this.setState({
                                lineItemChange: true
                            }, () => {

                                if (hsnSacCode != "" && hsnSacCode != null) {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                } else {

                                    this.setState({
                                        errorMassage: "HSN code is complusory",
                                        poErrorMsg: true

                                    })
                                }
                            }
                            )
                        }

                    }
                }
            }

            this.setState({
                poRows: r,

            })

        })
    }
    // openImageModal(id, image, articleCode, idx) {

    //     if (articleCode == "" || articleCode == undefined) {
    //         this.setState({
    //             errorMassage: "Article code is mandatory",
    //             poErrorMsg: true,
    //             focusId: "articleCode" + idx

    //         })

    //     }
    //     else {
    //         let rows = [...this.state.poRows]

    //         for (var i = 0; i < rows.length; i++) {
    //             if (rows[i].gridOneId == id) {

    //                 this.setState({
    //                     imageState: rows[i].imageUrl
    //                 })
    //             }
    //         }
    //         this.setState({
    //             imageRowId: id,
    //             imageModal: true,
    //             imageModalAnimation: !this.state.imageModalAnimation,
    //             focusId: image
    //         });
    //     }
    //     document.onkeydown = function (t) {
    //         if (t.which == 9) {
    //             return false;
    //         }
    //     }
    // }

    // closePiImageModal() {
    //     document.getElementById(this.state.focusId).focus()
    //     this.setState({
    //         imageModal: false,
    //         imageModalAnimation: !this.state.imageModalAnimation
    //     });
    //     document.onkeydown = function (t) {
    //         if (t.which == 9) {
    //             return true;
    //         }
    //     }
    // }


    // updateImage(data) {
    //     let c = [...this.state.poRows];
    //     for (var i = 0; i < c.length; i++) {
    //         if (c[i].gridOneId.toString() == data.imageRowId) {
    //             c[i].imageUrl = data.file
    //             c[i].image = Object.keys(data.file)
    //             c[i].containsImage = Object.keys(data.file).length != 0 ? true : false
    //         }

    //     }

    //     this.setState({
    //         poRows: c,

    //     })
    //     document.getElementById(this.state.focusId).focus()

    // }
    calculatedMargin(marginId, marginLeft) {


        let marginRate = document.getElementById(marginId)
        var viewportOffset = marginRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - (marginId.length <= 8 ? 345 : 175);


        if (!document.getElementById(marginLeft)) {
        } else {
            document.getElementById(marginLeft).style.left = left;
            document.getElementById(marginLeft).style.top = top;
        }
    }


    componentDidMount() {
        this.props.piSaveDraftClear();
        if (this.props.successMessage == true) {
            console.log("came inside if")
            this.setState({
                successMessageState: true
            })
        }        

        //ria
        this.mountRequest()

    }

    mountRequest = () => {
        let data = {
            type: 1,
            no: 1,
            search: '',
        }
        this.setState({
            siteMount: true,
        }, () => this.props.procurementSiteRequest(data))
        this.props.poRadioValidationRequest('data')
        this.dateCall()

        // this.props.piEditClear()
        this.otbNegative()
    }

    dateCall = () => {
        var current = new Date()
        this.setState({
            poDate: moment(current).format("DD-MMM-YYYY"),
            lastAsnDate: moment(current).format("DD-MMM-YYYY"),
        })

        let lInDate = ""
        let mnth = ""
        let day = ""
        let yr = ""
        let finalLInDate = ""
        if (this.state.codeRadio == "raisedIndent") {
            this.setState({
                poValidFrom: getDate(),
                currentDate: getDate()
            })
            if (document.getElementById("piValidFrom") != null) {
                document.getElementById("piValidFrom").placeholder = getDate();
            }

            lInDate = new Date(getDate())
            lInDate.setDate(lInDate.getDate() + 14)
            mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
            day = ("0" + lInDate.getDate()).slice(-2);
            yr = lInDate.getFullYear()
            finalLInDate = [yr, mnth, day].join("-")
            if (document.getElementById("piValidFrom") != null) {
                document.getElementById("lastInDate").placeholder = finalLInDate
            }
            this.setState({
                lastInDate: finalLInDate,
                minDate: getDate(),
                maxDate: finalLInDate
            })
        } else {
            if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" || this.state.codeRadio === "Adhoc") && sessionStorage.getItem("poValidFrom") != null) {
                this.setState({
                    poValidFrom: sessionStorage.getItem("poValidFrom"),
                    currentDate: getDate()
                })
                if (document.getElementById("piValidFrom") != null) {
                    document.getElementById("piValidFrom").placeholder = sessionStorage.getItem("poValidFrom");
                }
                lInDate = new Date(sessionStorage.getItem("poValidFrom"))
                lInDate.setDate(lInDate.getDate() + 14)
                mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                day = ("0" + lInDate.getDate()).slice(-2);
                yr = lInDate.getFullYear()
                finalLInDate = [yr, mnth, day].join("-")
                if (document.getElementById("piValidFrom") != null) {
                    document.getElementById("lastInDate").placeholder = finalLInDate
                }
                this.setState({
                    lastInDate: finalLInDate,
                    minDate: getDate(),
                    maxDate: finalLInDate
                })
            } else if ((this.state.codeRadio === "setBased" || this.state.codeRadio === "holdPo" || this.state.codeRadio === "Adhoc") && sessionStorage.getItem("poValidFrom") == null) {
                this.setState({
                    poValidFrom: getDate(),
                    currentDate: getDate()
                })
                if (document.getElementById("piValidFrom") != null) {
                    document.getElementById("piValidFrom").placeholder = getDate();
                }
                lInDate = new Date(getDate())
                lInDate.setDate(lInDate.getDate() + 14)
                mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                day = ("0" + lInDate.getDate()).slice(-2);
                yr = lInDate.getFullYear()
                finalLInDate = [yr, mnth, day].join("-")
                if (document.getElementById("piValidFrom") != null) {
                    document.getElementById("lastInDate").placeholder = finalLInDate
                }
                this.setState({
                    lastInDate: finalLInDate,
                    minDate: getDate(),
                    maxDate: finalLInDate
                })

            }
        }
        // if ((this.state.codeRadio === "Adhoc" || this.state.codeRadio === "raisedIndent") && this.state.vendorMrpPo != "") {
        let poRows = [...this.state.poRows]
        for (let i = 0; i < poRows.length; i++) {
            poRows[i].deliveryDate = finalLInDate

        }
        this.setState({
            poRows: poRows
        })
        if (this.state.isvalidFromAndTo && document.getElementById("piValidFrom") != null) {
            document.getElementById("piValidFrom").focus()
        }
    }


    componentDidUpdate(prevProps, prevState) {
        let poRows = [...this.state.poRows];
        let isEnable = true;        
        poRows.map(po => {          
            po.lineItem.map(item => {
                if(item.setQty != ''){
                    isEnable = isEnable && true;
                } else {
                    isEnable = isEnable && false;
                }
            })
        });    
        if(prevState.isSubmitEnable != isEnable) {
            this.setState({
                isSubmitEnable : isEnable
            })
        }
    }


    // getPlaceholder = () => {

    // }
    componentWillUnmount() {
        // this.props.piEditClear()
        // this.props.getDraftClear()
        this.idleTimer.pause();
    }



    openPoArticle(e, id, idx, value, item, index) {
        setTimeout(() => {
            if(!this.state.headerLevelErrMsg) {
                if (value == "articleCode") {
                    this.setState({
                        articleClicked: true,
                        alertFlag: 2
                    })
                }
                this.setState({
                    rowItem: value,
                    rowIndex: index,
                })
                if (this.state.loadIndenterr || this.state.poSetVendorerr || this.state.orderSeterr || this.state.vendorerr
                    || this.state.siteNameerr || this.state.transportererr || this.state.cityerr || this.state.typeOfBuyingErr) {
                    this.checkErr()
                }
                else {
                    this.onEsc()
                    if (this.state.supplier != "") {
                        if (value == "division") {
                            this.setState({
                                poArticle: true,
                                poArticleAnimation: true,
                                hl4Code: "",
                                hl1Code: "",
                                hl2Code: "",
                                hl4Name: "",
                                hl1Name: "",
                                hl2Name: "",
                                hl3Name: "",
                                hl3Code: "",
                                basedOn: "division",
                                mrpStart: item.mrpStart,
                                mrpEnd: item.mrpEnd,
                                poArticleSearch: this.state.isModalShow ? "" : item.divisionName,
                                uuid: idx,
                                focusId: idx,
                                mrpRangeSearch: false
                            })
                            let payload = {
                                pageNo: 1,
                                type: this.state.isModalShow || item.divisionName == "" ? 1 : 3,
                                hl4Code: "",
                                hl1Code: "",
                                hl2Code: "",
                                hl4Name: "",
                                hl1Name: "",
                                hl2Name: "",
                                hl3Name: "",
                                hl3Code: "",
                                divisionSearch: this.state.isModalShow ? "" : item.divisionName,
                                sectionSearch: "",
                                departmentSearch: "",
                                articleSearch: "",
                                basedOn: "division",
                                supplier: this.state.slCode,
                                mrpSearch: "",
                                isMrp: false,
                                mrpRangeSearch: false
        
                            }
                            this.props.poArticleRequest(payload)
        
                        } else if (value == "section") {
                            this.setState({
                                poArticle: true,
                                poArticleAnimation: true,
                                hl4Code: "",
                                hl4Name: "",
                                hl2Code: "",
                                hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                hl1Name: item.divisionName,
                                hl2Name: "",
                                hl3Name: "",
                                hl3Code: "",
                                basedOn: "section",
                                mrpStart: item.mrpStart,
                                mrpEnd: item.mrpEnd,
                                poArticleSearch: this.state.isModalShow ? "" : item.sectionName,
                                uuid: idx,
                                focusId: idx,
                                mrpRangeSearch: false
        
                            })
                            let payload = {
                                pageNo: 1,
                                type: this.state.isModalShow || item.sectionName == "" ? 1 : 3,
        
                                hl4Code: "",
                                hl4Name: "",
                                hl1Name: item.divisionName,
                                hl2Code: "",
                                hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                hl2Name: "",
                                hl3Name: "",
                                hl3Code: "",
                                divisionSearch: "",
                                sectionSearch: this.state.isModalShow ? "" : item.sectionName,
                                departmentSearch: "",
                                articleSearch: "",
                                basedOn: "section",
                                supplier: this.state.slCode,
                                mrpSearch: "",
                                isMrp: false,
                                mrpRangeSearch: false
                            }
                            this.props.poArticleRequest(payload)
                        } else if (value == "department") {
                            this.setState({
                                poArticle: true,
                                poArticleAnimation: true,
                                hl4Code: "",
                                hl4Name: "",
                                hl2Code: item.sectionName != "" ? item.sectionCode : "",
                                hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                hl1Name: item.divisionName,
                                hl2Name: item.sectionName,
                                hl3Name: "",
                                hl3Code: "",
                                basedOn: "department",
                                mrpStart: item.mrpStart,
                                mrpEnd: item.mrpEnd,
                                poArticleSearch: this.state.isModalShow ? "" : item.departmentName,
                                uuid: idx,
                                focusId: idx,
                                mrpRangeSearch: false
        
        
                            })
                            let payload = {
                                pageNo: 1,
                                type: this.state.isModalShow || item.departmentName == "" ? 1 : 3,
        
                                hl4Code: "",
                                hl4Name: "",
                                hl2Code: item.sectionName != "" ? item.sectionCode : "",
                                hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                hl1Name: item.divisionName,
                                hl2Name: item.sectionName,
                                hl3Name: "",
                                hl3Code: "",
                                divisionSearch: "",
                                sectionSearch: "",
                                departmentSearch: this.state.isModalShow ? "" : item.departmentName,
                                articleSearch: "",
                                basedOn: "department",
                                supplier: this.state.slCode,
                                mrpSearch: "",
                                isMrp: false,
                                mrpRangeSearch: false
                            }
                            this.props.poArticleRequest(payload)
                        } else if (value == "articleCode") {
                            this.setState({
                                poArticle: true,
                                poArticleAnimation: true,
                                hl2Code: item.sectionName != "" ? item.sectionCode : "",
                                hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                hl4Code: "",
                                hl4Name: "",
                                hl1Name: item.divisionName,
                                hl2Name: item.sectionName,
                                hl3Name: "",
                                hl3Code: "",
                                basedOn: "article",
                                mrpStart: item.mrpStart,
                                mrpEnd: item.mrpEnd,
                                poArticleSearch: this.state.isModalShow ? "" : item.articleCode,
                                uuid: idx,
                                focusId: idx,
                                mrpRangeSearch: false
        
        
        
                            })
                            let payload = {
                                pageNo: 1,
                                type: this.state.isModalShow || item.articleCode == "" ? 1 : 3,
                                hl2Code: item.sectionName != "" ? item.sectionCode : "",
                                hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                hl4Code: "",
                                hl4Name: "",
                                hl1Name: item.divisionName,
                                hl2Name: item.sectionName,
                                hl3Name: item.departmentName,
                                hl3Code: item.departmentName != "" ? item.departmentCode : "",
                                divisionSearch: "",
                                sectionSearch: "",
                                departmentSearch: "",
                                articleSearch: this.state.isModalShow ? "" : item.articleCode,
                                basedOn: "article",
                                supplier: this.state.slCode,
                                mrpSearch: "",
                                isMrp: false
                            }
                            if(!this.state.isDisplayDivision && !this.state.isDisplaySection && !this.state.isDisplayDepartment){
                                payload = {
                                    pageNo: 1,
                                    type: this.state.isModalShow || item.articleCode == "" ? 1 : 3,
                                    hl2Code: "",
                                    hl1Code: "",
                                    hl4Code: "",
                                    hl4Name: "",
                                    hl1Name: "",
                                    hl2Name: "",
                                    hl3Name: "",
                                    hl3Code: "",
                                    divisionSearch: "",
                                    sectionSearch: "",
                                    departmentSearch: "",
                                    articleSearch: this.state.isModalShow ? "" : item.articleCode,
                                    basedOn: "article",
                                    supplier: this.state.slCode,
                                    mrpSearch: "",
                                    isMrp: false
                                }
                            }
                            this.props.poArticleRequest(payload)
        
                        } else if (value == "mrpRange") {
                            if (item.hl1Code != "" || item.hl2Code != "" || item.hl3Code != "" || item.hl4Code != "") {
        
                                this.setState({
                                    poArticle: true,
                                    poArticleAnimation: true,
                                    hl2Code: item.sectionName != "" ? item.sectionCode : "",
                                    hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                    hl4Code: item.articleName != "" ? item.articleCode : "",
                                    hl4Name: item.articleName,
                                    hl1Name: item.divisionName,
                                    hl2Name: item.sectionName,
                                    hl3Name: item.departmentName,
                                    hl3Code: item.departmentCode,
                                    basedOn: "article",
                                    mrpStart: item.mrpStart,
                                    mrpEnd: item.mrpEnd,
                                    poArticleSearch: this.state.isModalShow ? "" : item.mrpRange,
                                    uuid: idx,
                                    focusId: idx,
                                    mrpRangeSearch: true,
                                    isMrpFilled: true,
        
                                })
                                let payload = {
                                    pageNo: 1,
                                    type: this.state.isModalShow || item.mrpRange == "" ? 1 : 3,
                                    hl2Code: item.sectionName != "" ? item.sectionCode : "",
                                    hl1Code: item.divisionName != "" ? item.divisionCode : "",
                                    hl4Code: item.articleCode,
                                    hl4Name: item.articleName,
                                    hl1Name: item.divisionName,
                                    hl2Name: item.sectionName,
                                    hl3Name: item.departmentName,
                                    hl3Code: item.departmentName != "" ? item.departmentCode : "",
                                    divisionSearch: "",
                                    sectionSearch: "",
                                    departmentSearch: "",
                                    articleSearch: "",
                                    basedOn: this.state.basedOn,
                                    supplier: this.state.slCode,
                                    mrpSearch: this.state.isModalShow ? "" : item.mrpRange.split(/\b/)[0],
                                    isMrp: true
                                }
                                this.props.poArticleRequest(payload)
                            }
        
                        }
                        document.getElementById(idx).focus()
                        this.setState({
                            selectedRowId: id
                        })
                        document.onkeydown = function (t) {
                            if (t.which == 9) {
                                return false;
                            }
                        }
                    } else {
                        this.setState({
                            errorMassage: "Supplier is compulsory",
                            poErrorMsg: true,
                            focusId: "vendor"
        
                        })
                    }
                }
            }
        }, 10);
    }
    closePOArticle() {
        // this.articleErr()
        this.setState({
            poArticle: false,
            poArticleAnimation: false,
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById(this.state.focusId).focus()

    }
    callSizeRequest = (department, divisionName, sectionName, departmentCode, articleName, itemObj) => {
        var dataColor = {
            no: 1,
            type: 1,
            search: "",
            hl3Name: department,
            hl1Name: divisionName,
            hl2Name: sectionName,
            hl3Code: departmentCode,
            hl4Name: articleName,
            itemType: 'size',
            isToggled: "",
            itemUdfObj: itemObj
        }
        this.props.sizeRequest(dataColor)
    }
    updateMrpState(data) {
        // if (this.state.articleClicked) {
        //     this.callSizeRequest(data.department, data.division, data.section, data.departmentCode, data.articleName)
        // }
        console.log(data)
        console.log(this.props.purchaseIndent)

        console.log("selected data i guess")
        //ria chnage
        let lInDate = ""
        let mnth = ""
        let day = ""
        let yr = ""
        let finalLInDate = ""
        lInDate = new Date(getDate())
        lInDate.setDate(lInDate.getDate() + 15)
        mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
        day = ("0" + lInDate.getDate()).slice(-2);
        yr = lInDate.getFullYear()
        finalLInDate = [yr, mnth, day].join("-")
        let deliveryDate = this.state.deliveryDate;
        if (this.state.deliveryDate && this.state.isvalidFromAndTo) {
            deliveryDate = this.state.lastInDate;
        }
        if (this.state.deliveryDate && !this.state.isvalidFromAndTo) {
            deliveryDate = finalLInDate;
        }
        let poRows = [...this.state.poRows]
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                if(data.articleCode == "" && data.mrpEnd != ""){
                    poRows[i].isMrpRangeLovSelect = true
                }
                if( data.articleCode != "" && data.mrpEnd == ""){ 
                    poRows[i].isArticleLovSelect = true 
                    let payload = {
                        pageNo: 1,
                        type: 1,
                        hl2Code: data.sectionCode != "" ? data.sectionCode : poRows[i].sectionCode,
                        hl1Code: data.divisionCode != "" ? data.divisionCode : poRows[i].divisionCode,
                        hl4Code: data.articleCode != "" ? data.articleCode : poRows[i].articleCode,
                        hl4Name: data.articleName != "" ? data.articleName : poRows[i].articleName,
                        hl1Name: data.division != "" ? data.division : poRows[i].divisionName,
                        hl2Name: data.section != "" ? data.section : poRows[i].sectionName,
                        hl3Name: data.department != "" ? data.department : poRows[i].departmentName,
                        hl3Code: data.departmentCode != "" ? data.departmentCode : poRows[i].departmentCode,
                        divisionSearch: "",
                        sectionSearch: "",
                        departmentSearch: "",
                        articleSearch: "",
                        basedOn: this.state.basedOn,
                        supplier: this.state.slCode,
                        mrpSearch: '',
                        isMrp: true
                    }        

                        this.setState({
                            isMrpFilled: true,
                            // poArticle: true,
                            // poArticleAnimation: true,
                            // hl2Code: data.sectionCode != "" ? data.sectionCode : poRows[i].sectionCode,
                            // hl1Code: data.divisionCode != "" ? data.divisionCode : poRows[i].divisionCode,
                            // hl4Code: data.articleCode != "" ? data.articleCode : poRows[i].articleCode,
                            // hl4Name: data.articleName != "" ? data.articleName : poRows[i].articleName,
                            // hl1Name: data.division != "" ? data.division : poRows[i].divisionName,
                            // hl2Name: data.section != "" ? data.section : poRows[i].sectionName,
                            // hl3Name: data.department != "" ? data.department : poRows[i].departmentName,
                            // hl3Code: data.departmentCode != "" ? data.departmentCode : poRows[i].departmentCode,
                            // basedOn: "article",
                            // mrpStart: '',
                            // mrpEnd: '',
                            // // poArticleSearch: this.state.isModalShow ? "" : item.mrpRange,
                            // // uuid: i,
                            // // focusId: i,
                            // mrpRangeSearch: true


                        }, () =>  this.props.poArticleRequest(payload) )                        
                        poRows[i].isHsnDefault = false
                        poRows[i].isMrpRangeDefault = false                  
                }
                    poRows[i].articleCode = data.articleCode != "" ? data.articleCode : poRows[i].articleCode
                    poRows[i].articleName = data.articleName != "" ? data.articleName : poRows[i].articleName
                    poRows[i].divisionCode = data.divisionCode != "" ? data.divisionCode : poRows[i].divisionCode
                    poRows[i].divisionName = data.division != "" ? data.division : poRows[i].divisionName
                    poRows[i].sectionCode = data.sectionCode != "" ? data.sectionCode : poRows[i].sectionCode
                    poRows[i].sectionName = data.section != "" ? data.section : poRows[i].sectionName
                    
                    poRows[i].departmentName = data.department != "" ? data.department : poRows[i].departmentName
                    poRows[i].mrpStart = data.mrpStart == 0 ? 0 : data.mrpStart != undefined ? data.mrpStart : poRows[i].mrpStart
                    poRows[i].mrpRange = data.mrpEnd != "" ? data.mrpStart + " - " + data.mrpEnd : ""
                    poRows[i].mrpEnd = data.mrpEnd != "" ? data.mrpEnd : poRows[i].mrpEnd
                if(poRows[i].quantity == "" || poRows[i].quantity == 0 || ( data.departmentCode != "" ? this.state.poRows[i].departmentCode != data.departmentCode : false)){
                    poRows[i].departmentCode = data.departmentCode != "" ? data.departmentCode : poRows[i].departmentCode
                    poRows[i].itemCodeList = [],
                    poRows[i].itemCodeSearch = "",
                    poRows[i].color = [],
                    poRows[i].size = [],
                    poRows[i].ratio = [],
                    poRows[i].vendorMrp = "",
                    poRows[i].vendorDesign = poRows[i].vendorDesign,
                    poRows[i].mrk = [],
                    poRows[i].discount = {
                        discountType: "",
                        discountValue: "",
                        discountPer: true
                    },

                    poRows[i].finalRate = "",
                    poRows[i].rate = "",
                    poRows[i].netRate = "",
                    poRows[i].rsp = "",
                    poRows[i].wsp = "",
                    poRows[i].mrp = "",
                    poRows[i].quantity = "",
                    poRows[i].amount = "",
                    poRows[i].otb = "",
                    poRows[i].remarks = "",
                    poRows[i].gst = [],
                    poRows[i].finCharges = [],
                    //poRows[i].basic = poRows[i].articleCode == "" ? "" : poRows[i].basic,

                    poRows[i].tax = [],
                    poRows[i].calculatedMargin = [],
                    poRows[i].gridOneId = poRows[i].gridOneId,
                    poRows[i].deliveryDate = deliveryDate,

                    // // poRows[i].marginRule = "",
                    // poRows[i].image = [],
                    // poRows[i].imageUrl = {},
                    // poRows[i].containsImage = false,
                    //new

                    poRows[i].hsnCode = poRows[i].hsnCode != "" ? poRows[i].hsnCode : "",
                    poRows[i].hsnSacCode = poRows[i].hsnSacCode != "" ? poRows[i].hsnSacCode : "",

                    poRows[i].catDescHeader = poRows[i].catDescHeader.length != 0 ? poRows[i].catDescHeader : [],
                    poRows[i].catDescArray = poRows[i].catDescArray.length != 0 ? poRows[i].catDescArray : [],
                    poRows[i].itemUdfHeader = poRows[i].itemUdfHeader.length != 0 ? poRows[i].itemUdfHeader : [],
                    poRows[i].itemUdfArray = poRows[i].itemUdfArray.length != 0 ? poRows[i].itemUdfArray : [],
                    poRows[i].lineItem = [{

                        colorChk: false,
                        icodeChk: false,
                        colorSizeList: [],
                        colorSearch: "",
                        icodes: [],
                        itemBarcode: "",
                        setHeaderId: "",
                        gridTwoId: 1,
                        color: [],
                        colorList: [],
                        sizeSearch: "",
                        sizes: [],
                        sizeList: [],
                        ratio: [],
                        size: "",
                        setRatio: "",
                        option: "",
                        setNo: 1,
                        total: "",
                        setQty: this.state.isSet ? "" : 1,
                        quantity: "",
                        amount: "",
                        rate: "",
                        image: [],
                        imageUrl: {},
                        imagePath: "",
                        containsImage: false,
                        gst: "",
                        finCharges: [],
                        tax: "",
                        otb: "",
                        calculatedMargin: "",
                        mrk: "",
                        setUdfHeader: poRows[i].lineItem[0].setUdfHeader,
                        setUdfArray: poRows[i].lineItem[0].setUdfArray,
                        lineItemChk: false,
                        sizeType: "complex"

                    }]
                } else{
                    poRows[i].vendorMrp = "",
                    poRows[i].rate = ""
                    poRows[i].rsp = ""
                    poRows[i].mrp = ""
                }




            }
        }
        // if(this.state.rowItem == 'division'){
        //         poRows[this.state.rowIndex] = {
        //             ...poRows[this.state.rowIndex],
        //             articleCode : "",
        //             articleName : "",
        //             departmentName : "",
        //             sectionName : "",
        //             divisionCode : "",
        //             hsnCode : ""
        //         }}
        this.setState({

            poRows: poRows
        })
        document.getElementById(this.state.focusId).focus()


        if (data.departmentCode != "" && this.state.mrpRangeSearch == false) {

            let hsnData = {
                rowId: "",
                code: data.departmentCode,
                no: 1,
                type: 1,
                search: ""
            }
            this.props.hsnCodeRequest(hsnData)
            let marginData = {
                slCode: this.state.slCode,
                articleCode: data.articleCode,
                siteCode: this.state.siteCode,
                rId: ""
            }
            this.props.marginRuleRequest(marginData)
            let Payload = {
                hl3Code: data.departmentCode,
                hl3Name: data.department,
            }
            this.props.getItemDetailsRequest(Payload)


        }
        setTimeout(() => {

            if (!this.state.isColorRequired) {
                let poRows = [...this.state.poRows]
                let color = []
                let colorObj = {
                    id: 1,
                    code: "",
                    cname: "NA"

                }
                color.push(colorObj)
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == this.state.selectedRowId) {
                        for (let j = 0; j < poRows[i].lineItem.length; j++) {
                            poRows[i].lineItem[j].color = ["NA"],
                                poRows[i].lineItem[j].colorList = color,
                                poRows[i].lineItem[j].option = 1
                        }
                    }

                }
                this.setState({
                    poRows
                })
            }


        }, 10)

    }

    updatePoAmount() {
        let c = [...this.state.poRows]
        let amount = 0;
        for (var x = 0; x < c.length; x++) {
            amount += c[x].amount;
        }
        this.setState({
            poAmount: amount
        })
    }
    updateNetAmt() {
        let poRows = [...this.state.poRows]
        let quantity = 0
        let rate = ""
        let rsp = ""
        let finalRate = ""
        for (let i = 0; i < poRows.length; i++) {
            quantity = quantity + Number(poRows[i].quantity)

        }
        // let otbArray =this.state.otbArray
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].gridOneId == this.state.selectedRowId) {
                console.log('poRows', poRows[j])
                rate = poRows[j].rate,

                    rsp = this.state.isRspRequired ? (poRows[j].rsp == "" || poRows[j].rsp == 0) ? poRows[j].mrp : poRows[j].rsp : poRows[j].mrp
                finalRate = poRows[j].finalRate
            }
        }
        // if (this.state.isRspRequired) {
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    // if (poRows[i].lineItem[j].gridTwoId == id) {
                    let intakedata = {
                        rate: finalRate,
                        wsp: finalRate,
                        rsp: rsp,
                        rowId: this.state.selectedRowId,
                        designRow: this.state.selectedRowId,
                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
                        hsnSacCode: poRows[i].hsnSacCode
                    }
                    this.setState({
                        markUpYes: true
                    })
                    this.props.markUpRequest(intakedata)
                    // }
                }
            }
        }
        // }
        this.setState({
            poQuantity: quantity
        })
    }
    resetPoRows() {
        let lInDate = ""
        let mnth = ""
        let day = ""
        let yr = ""
        let finalLInDate = ""
        lInDate = new Date(getDate())
        lInDate.setDate(lInDate.getDate() + 15)
        mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
        day = ("0" + lInDate.getDate()).slice(-2);
        yr = lInDate.getFullYear()
        finalLInDate = [yr, mnth, day].join("-")
        let deliveryDate = this.state.deliveryDate;
        if (this.state.deliveryDate && this.state.isvalidFromAndTo) {
            deliveryDate = this.state.lastInDate;
        }
        if (this.state.deliveryDate && !this.state.isvalidFromAndTo) {
            deliveryDate = finalLInDate;
        }
        this.setState({


            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: true
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                wsp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: deliveryDate,

                marginRule: "",
                image: [],
                imageUrl: {},
                imagePath: "",
                containsImage: false,
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeSearch: "",

                    sizeList: [],
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: this.state.isSet ? "" : 1,
                    quantity: "",
                    amount: "",
                    rate: "",

                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: [],
                    lineItemChk: false

                }]


            }]
        }, () => {
            this.updatePoAmountNpoQuantity()
            this.updatePoAmount()
        })
    }

    componentWillReceiveProps(nextProps, nextState) {

        if (nextProps.purchaseIndent.getAllCity.isSuccess){
            if (!this.state.cityModal) {
                if (nextProps.purchaseIndent.getAllCity.data.resource != null) {
                    let value = nextProps.purchaseIndent.getAllCity.data.resource;
                    let cityArray = value.map(item => item.toUpperCase())
                    let cityValue = this.state.city.trim().toUpperCase()
                    if (cityArray.includes(cityValue)) {
                        value.map(item => {
                            if(item.toUpperCase() == cityValue) {
                            this.setState({
                                isCityLovSelect: true,
                                city: item,
                            }) 
                            }
                        })
                    } else {
                        this.setState({
                            focusId: "city",
                            city: "",
                            poErrorMsg: true,
                            errorMassage: "Please select valid input from the drop-down list"
                        })
                    }
                } else if (nextProps.purchaseIndent.getAllCity.data.resource == null) {
                    this.setState({
                        focusId: "city",
                        city: "",
                        poErrorMsg: true,
                        errorMassage: "Please select valid input from the drop-down list"
                    }) 
                }
            }
        }

        if (nextProps.purchaseIndent.getTransporter.isSuccess){
            if (!this.state.trasporterModal) {
                if (nextProps.purchaseIndent.getTransporter.data.resource != null) {
                    let value = nextProps.purchaseIndent.getTransporter.data.resource;
                    let transporterName = [];
                    let transporterCode = [];
                    let transporterValue = this.state.transporter.trim().toUpperCase()
                    value.map(item => {
                        transporterName.push(item.transporterName.toUpperCase())
                        transporterCode.push(item.transporterCode.toUpperCase())
                    })
                    if (transporterName.includes(transporterValue) || transporterCode.includes(transporterValue)) {
                        value.map(item => {
                            if(item.transporterCode.toUpperCase() == transporterValue || item.transporterName.toUpperCase() == transporterValue) {
                            this.setState({
                                isTransporterLovSelect: true,
                                transporterCode: item.transporterCode,
                                transporterName: item.transporterName,
                                transporter: item.transporterName,
                            }) 
                            }
                        })
                    } else {
                        this.setState({
                            focusId: "transporter",
                            transporterCode: "",
                            transporterName: "",
                            transporter: "",
                            poErrorMsg: true,
                            errorMassage: "Please select valid input from the drop-down list"
                        })
                    }
                } else if (nextProps.purchaseIndent.getTransporter.data.resource == null) {
                    this.setState({
                        focusId: "transporter",
                        transporterCode: "",
                        transporterName: "",
                        transporter: "",
                        poErrorMsg: true,
                        errorMassage: "Please select valid input from the drop-down list"
                    }) 
                }
            }
        }

        if (nextProps.purchaseIndent.supplier.isSuccess){
            if (!this.state.supplierModal) {
                if(nextProps.purchaseIndent.supplier.data.resource != null) {
                    let value = nextProps.purchaseIndent.supplier.data.resource;
                    let supplierName = [];
                    let supplierCode = [];
                    let supplierValue = this.state.supplier.trim().toUpperCase()
                    value.map(item => {
                        supplierName.push(item.name.toUpperCase())
                        supplierCode.push(item.code.toUpperCase())
                    })
                    if (supplierName.includes(supplierValue) || supplierCode.includes(supplierValue)) {
                        value.map(item => {
                            if(item.code.toUpperCase() == supplierValue || item.name.toUpperCase() == supplierValue) {
                            this.setState({
                                isVendorLovSelect: true,
                                city: item.city,
                                gstInNo: item.gstInNo,
                                sCode: item.code,
                                slAddr: item.address,
                                slName: item.name,
                                slcode: item.code,
                                stateCode: item.stateCode,
                                supplier: item.name,
                                tradeGrpCode: item.tradeGrpCode,
                                transporterCode: item.transporterCode,
                                transporterName: item.transporterName,
                            }) 
                            }
                        })
                    } else {
                        this.setState({
                            focusId: "vendor",
                            supplier: "",
                            poErrorMsg: true,
                            errorMassage: "Please select valid input from the drop-down list"
                        })
                    }
            } else if (nextProps.purchaseIndent.supplier.data.resource == null) {
                this.setState({
                    focusId: "vendor",
                    supplier: "",
                    poErrorMsg: true,
                    errorMassage: "Please select valid input from the drop-down list"
                })
            }
            }
        }

        if (nextProps.purchaseIndent.poArticle.isSuccess) {
            if (this.state.isAutoGenerateDesign && nextProps.purchaseIndent.poArticle.data.vendorDesignNo !== undefined) {
                let poRows = [...this.state.poRows]
                poRows[this.state.selectedRowIndex].vendorDesign = nextProps.purchaseIndent.poArticle.data.vendorDesignNo;
                this.setState({poRows: poRows});
            }
            if (nextProps.purchaseIndent.poArticle.data.resource != null) {
                let poRows = [...this.state.poRows]
                if(this.state.isMrpFilled && !poRows[this.state.selectedRowId -1].isMrpRangeDefault){
                    let data = nextProps.purchaseIndent.poArticle.data.resource[0];
                    let poRows = [...this.state.poRows]
                    poRows[this.state.selectedRowId - 1] = {...poRows[this.state.selectedRowId - 1], mrpStart: data.mrpStart, mrpEnd: data.mrpEnd, mrpRange: data.mrpStart + ' - ' + data.mrpEnd}
                    poRows[this.state.selectedRowId -1].isMrpRangeDefault = true,
                    poRows[this.state.selectedRowId -1].isMrpRangeLovSelect = true
                    this.setState({
                        poRows: poRows,
                        isMrpFilled: false,
                    })
                } 
                this.props.poArticleClear()               
            }
            if(!this.state.poArticle) {
                let poRows = [...this.state.poRows]
                if (nextProps.purchaseIndent.poArticle.data.resource != null) {
                    if (!this.state.isMrpFilled) {
                        let value = nextProps.purchaseIndent.poArticle.data.resource;
                        let hl4CodeArray = [];
                        let hl4NameArray = [];
                        console.log('vll', this.state.selectedRowIndex)
                        value.map(item => {
                            hl4CodeArray.push(item.hl4Code.toString())
                            hl4NameArray.push(item.hl4Name.toUpperCase())
                        })
                        let articleValue = poRows[this.state.selectedRowIndex].articleCode.toString().toUpperCase().trim();
                        if (hl4CodeArray.includes(articleValue) || hl4NameArray.includes(articleValue)) {
                            value.map(data => {
                                if (data.hl4Code == articleValue || data.hl4Name.toUpperCase() == articleValue) {
                                    poRows[this.state.selectedRowIndex].articleCode = data.hl4Code != "" ? data.hl4Code : poRows[this.state.selectedRowIndex].articleCode;
                                    poRows[this.state.selectedRowIndex].articleName = data.hl4Name != "" ? data.hl4Name : poRows[this.state.selectedRowIndex].articleName;
                                    poRows[this.state.selectedRowIndex].divisionCode = data.hl1Code != "" ? data.hl1Code : poRows[this.state.selectedRowIndex].divisionCode;
                                    poRows[this.state.selectedRowIndex].divisionName = data.hl1Name != "" ? data.hl1Name : poRows[this.state.selectedRowIndex].divisionName;
                                    poRows[this.state.selectedRowIndex].sectionCode = data.hl2Code != "" ? data.hl2Code : poRows[this.state.selectedRowIndex].sectionCode;
                                    poRows[this.state.selectedRowIndex].sectionName = data.hl2Name != "" ? data.hl2Name : poRows[this.state.selectedRowIndex].sectionName;
                                    poRows[this.state.selectedRowIndex].departmentCode = data.hl3Code != "" ? data.hl3Code : poRows[this.state.selectedRowIndex].departmentCode;
                                    poRows[this.state.selectedRowIndex].departmentName = data.hl3Name != "" ? data.hl3Name : poRows[this.state.selectedRowIndex].departmentName;
                                    poRows[this.state.selectedRowIndex].isArticleLovSelect = true;
                                    poRows[this.state.selectedRowIndex].isHsnDefault = false;
                                    poRows[this.state.selectedRowIndex].isMrpRangeDefault = false;

                                // this.callSizeRequest(data.hl3Name, data.hl1Name, data.hl2Name, data.hl3Code, data.hl4Name)
                                let hsnData = {
                                    rowId: "",
                                    code: data.hl3Code,
                                    no: 1,
                                    type: 1,
                                    search: ""
                                }
                                this.props.hsnCodeRequest(hsnData)
                                let marginData = {
                                    slCode: this.state.slCode,
                                    articleCode: data.hl4Code,
                                    siteCode: this.state.siteCode,
                                    rId: ""
                                }
                                this.props.marginRuleRequest(marginData)
                                let Payload = {
                                    hl3Code: data.hl3Code,
                                    hl3Name: data.hl3Name,
                                }
                                this.props.getItemDetailsRequest(Payload)

                                let payload = {
                                    pageNo: 1,
                                    type: 1,
                                    hl2Code: data.hl2Name != "" ? data.hl2Code : "",
                                    hl1Code: data.hl1Name != "" ? data.hl1Code : "",
                                    hl4Code: data.hl4Code,
                                    hl4Name: data.hl4Name,
                                    hl1Name: data.hl1Name,
                                    hl2Name: data.hl2Name,
                                    hl3Name: data.hl3Name,
                                    hl3Code: data.hl3code != "" ? data.hl3code : "",
                                    divisionSearch: "",
                                    sectionSearch: "",
                                    departmentSearch: "",
                                    articleSearch: "",
                                    basedOn: this.state.basedOn,
                                    supplier: this.state.slCode,
                                    mrpSearch: "",
                                    isMrp: true
                                }
                                this.setState({
                                    isMrpFilled: true
                                }, () => this.props.poArticleRequest(payload))
                                setTimeout(() => {

                                    if (!this.state.isColorRequired) {
                                        let poRows = [...this.state.poRows]
                                        let color = []
                                        let colorObj = {
                                            id: 1,
                                            code: "",
                                            cname: "NA"
                        
                                        }
                                        color.push(colorObj)
                                        for (let i = 0; i < poRows.length; i++) {
                                            if (poRows[i].gridOneId == this.state.selectedRowId) {
                                                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                                                    poRows[i].lineItem[j].color = ["NA"],
                                                        poRows[i].lineItem[j].colorList = color,
                                                        poRows[i].lineItem[j].option = 1
                                                }
                                            }
                        
                                        }
                                        this.setState({
                                            poRows
                                        })
                                    }
                        
                        
                                }, 10)
                                }
                            })
                            this.setState({
                                poRows: poRows,
                            })
                        } else {
                            poRows[this.state.selectedRowIndex].articleCode = "";
                            poRows[this.state.selectedRowIndex].articleName = "";
                            this.setState({
                                    focusId: this.state.uuid,
                                    poErrorMsg: true,
                                    poRows: poRows,
                                    errorMassage: "Please select valid input from the drop-down list"
                                })
                        }
                    } else if (this.state.isMrpFilled && poRows[this.state.selectedRowId -1].isMrpRangeDefault) {
                        let value = nextProps.purchaseIndent.poArticle.data.resource;
                        let mrpStartArray = [];
                        let mrpEndArray = [];
                        value.forEach(item => {
                            mrpStartArray.push(item.mrpStart.toString())
                            mrpEndArray.push(item.mrpEnd.toString())
                        });
                        let mrpStartValue = poRows[this.state.selectedRowIndex].mrpRange != undefined ? poRows[this.state.selectedRowIndex].mrpRange.split(/\b/)[0] : ""
                        let mrpEndValue = poRows[this.state.selectedRowIndex].mrpRange != undefined ? poRows[this.state.selectedRowIndex].mrpRange.split(/\b/)[2] : ""
                        console.log('mrpstart', mrpStartArray, mrpStartValue, 'mrpEnd', mrpEndArray, mrpEndValue)
                        if(mrpStartArray.includes(mrpStartValue) && mrpEndArray.includes(mrpEndValue)) {
                            value.forEach(item => {
                                if(item.mrpStart == mrpStartValue && item.mrpEnd == mrpEndValue){
                                    poRows[this.state.selectedRowIndex].mrpRange = item.mrpStart + "-" + item.mrpEnd
                                    poRows[this.state.selectedRowIndex].mrpStart = item.mrpStart;
                                    poRows[this.state.selectedRowIndex].mrpEnd = item.mrpEnd;
                                    poRows[this.state.selectedRowIndex].isMrpRangeLovSelect = true;
                                }
                            })
                            this.setState({
                                poRows: poRows
                            })
                        } else {
                            poRows[this.state.selectedRowIndex].mrpRange = "";
                            this.setState({
                                    focusId: this.state.uuid,
                                    poErrorMsg: true,
                                    poRows: poRows,
                                    errorMassage: "Please select valid input from the drop-down list!"
                                })
                        }
                    }
                } else if (nextProps.purchaseIndent.poArticle.data.resource == null) {
                    if (this.state.isMrpFilled) {
                        poRows[this.state.selectedRowIndex].mrpRange = "";
                    } else {
                        poRows[this.state.selectedRowIndex].articleCode = "";
                        poRows[this.state.selectedRowIndex].articleName = "";
                    }
                    
                    this.setState({
                        focusId: this.state.uuid,
                        poErrorMsg: true,
                        poRows: poRows,
                        errorMassage: "Please select valid input from the drop-down list"
                    })
                }
                this.props.poArticleClear() 
            }
        }
        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
            // if(nextProps.purchaseIndent.poRadioValidation.data.resource!={}){   
            this.setState({
                isDispalyTransporter: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayTransporterPi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayTransporterPi: false,
                transporterValidationPi:  nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidationPi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidationPi : false,
                availCustomPo: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO !== undefined && nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO,
                isAdhoc: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isAdhoc != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isAdhoc : false,
                isIndent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isIndent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isIndent : false,
                isSetBased: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSetBased != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSetBased : false,
                isHoldPo: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isHoldPo != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isHoldPo : false,
                isPOwithICode: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isPOwithICode != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isPOwithICode : false,
                poWithUpload: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.poWithUpload : false,
                isCityExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck : false,
                isMRPEditable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMRPEditable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMRPEditable : false,
                isDisplayWSP: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayWSP != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayWSP : false,
                isMandateWSP: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandateWSP != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandateWSP : false,
                isDiscountAvail: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDiscountAvail != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDiscountAvail : false,
                isDiscountMap: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap : false,
                isRSP: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isRSP != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isRSP : false,
                displayOtb: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb : true,
                transporterValidation: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation : true,
                addRow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow : true,
                isvalidFromAndTo: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.validFromAndTo != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.validFromAndTo : false,
                simpleData: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.ppQty != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.ppQty : 0,
                
                // cat1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 : false,
                // cat2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 : false,
                // cat3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 : false,
                // cat4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 : false,
                // cat5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 : false,
                // cat6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 : false,
                // desc1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 : false,
                // desc2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 : false,
                // desc3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 : false,
                // desc4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 : false,
                // desc5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 : false,
                // desc6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 : false,
                isDisplayCatRemark: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayCatRemark != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayCatRemark : false,
                isMandateCatRemark: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandateCatRemark != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandateCatRemark : false,
                catRemarkLabel: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.catRemarkLabel !== undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.catRemarkLabel : "",
                mrpValidation: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.mrpValidation !== undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.mrpValidation : false,
                // itemudf1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 : false,
                // itemudf2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 : false,
                // itemudf3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 : false,
                // itemudf4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 : false,
                // itemudf5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 : false,
                // itemudf6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 : false,
                // itemudf7Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 : false,
                // itemudf8Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 : false,
                // itemudf9Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 : false,
                // itemudf10Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 : false,
                // itemudf11Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 : false,
                // itemudf12Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 : false,
                // itemudf13Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 : false,
                // itemudf14Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 : false,
                // itemudf15Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 : false,
                // itemudf16Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 : false,
                // itemudf17Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 : false,
                // itemudf18Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 : false,
                // itemudf19Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 : false,
                // itemudf20Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 : false,

                // udf1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 : false,
                // udf2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 : false,
                // udf3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 : false,
                // udf4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 : false,
                // udf5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 : false,
                // udf6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 : false,
                // udf7Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 : false,
                // udf8Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 : false,
                // udf9Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 : false,
                // udf10Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 : false,

                // udf11Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 : false,
                // udf12Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 : false,
                // udf13Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 : false,
                // udf14Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 : false,
                // udf15Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 : false,
                // udf16Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 : false,
                // udf17Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 : false,
                // udf18Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 : false,
                // udf19Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 : false,
                // udf20Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 : false,



                restrictedStartDate: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.startDate : "",
                restrictedEndDate: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.endDate : "",
                isRequireSiteId: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId : false,
                isVendorDesignNotReq: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq : false,

                isAutoGenerateDesign: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isAutoGenerateDesign != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isAutoGenerateDesign : false,
                isMrpRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMrpReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMrpReq : true,
                isRspRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isRspReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isRspReq : true,
                isColorRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq : true,
                isDisplayFinalRate: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayFinalRate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayFinalRate : false,
                isTransporterDependent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent : true,
                isDisplayMarginRule: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule : true,
                isTaxDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable : false,
                isGSTDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable : false,
                isBaseAmountActive: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive : false,
                isOtbValidationPi: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isOtbValidationPi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isOtbValidationPi ? "true" : "false" : "false",
                isSiteExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSiteExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isSiteExist ? "true" : "false" : "false",
                itemUdfExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isItemUdfExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isItemUdfExist ? "true" : "false" : "false",
                copyColor: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCopyColor != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCopyColor ? "true" : "false" : "false",
                isModalshow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isModalshow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isModalshow : "false",
                isUDFExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isUdfExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isUdfExist ? "true" : "false" : "false",
                storeUDFExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isUdfExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isUdfExist ? "true" : "false" : "false",

                isMrpRangeDisplay: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMrpRangeDisplay != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMrpRangeDisplay : true,
                isMarginRulePi: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMarginRulePi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMarginRulePi : false,
                isDisplayDivision: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayDivision != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayDivision : true,
                isDisplaySection: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplaySection != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplaySection : true,
                isDisplayDepartment: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayDepartment != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayDepartment : true,
                isCatDescExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isCatDescExist != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isCatDescExist : true,
                storedSet: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.defaultPoType != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.defaultPoType == "NONSET" ? false : true : true,
                isMaintainSize: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMaintainSize != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMaintainSize : true,
                isMrpRangeDisplay: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpRangeDisplay != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpRangeDisplay : true,
                isMarginRulePi: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMarginRulePi != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMarginRulePi : false,
                isDisplayPOUdf1: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf1 : false,
                isMandatePOUdf1: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf1 : false,
                POUdf1Label: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf1Label != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf1Label : "",
                isDisplayPOUdf2: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf2 : false,
                isMandatePOUdf2: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf2 : false,
                POUdf2Label: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf2Label != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf2Label : "",
                isDisplayPOUdf3: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf3 : false,
                isMandatePOUdf3: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf3 : false,
                POUdf3Label: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf3Label != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf3Label : "",
                isDisplayPOUdf4: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf4 : false,
                isMandatePOUdf4: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf4 : false,
                POUdf4Label: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf4Label != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf4Label : "",
                isDisplayPOUdf5: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isDisplayPOUdf5 : false,
                isMandatePOUdf5: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandatePOUdf5 : false,
                POUdf5Label: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf5Label != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.POUdf5Label : "",
                isMandateImage: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandateImage != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isMandateImage : false,
                bufferDays: nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.bufferDays != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.RESTRICTED_PRO_DATE.bufferDays : 14,
                isCheckMarginRuleValidation: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isCheckMarginRuleValidation != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.isCheckMarginRuleValidation : false,
            })

            if(!this.state.isEditPi){
                this.setState({
                    isSet: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.defaultPoType != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.defaultPoType == "NONSET" ? false : true : true,                    
                })
            }
                

            if(nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.defaultPoType != undefined){
                if(nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PI.defaultPoType == "NONSET"){
                    this.setState({
                        isUDFExist: "false"
                    })
                }
            }

            if (!nextProps.purchaseIndent.get_draft_data.isSuccess) {
                if (nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].deliveryDate && nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.validFromAndTo) {
                    if (!this.state.isSetDeliveryDate) {
                        lInDate = new Date(getDate())
                        lInDate.setDate(lInDate.getDate() + 14)
                        mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                        day = ("0" + lInDate.getDate()).slice(-2);
                        yr = lInDate.getFullYear()
                        finalLInDate = [yr, mnth, day].join("-")
                        let poRows = [...this.state.poRows]
                        poRows.map(row => {
                            row.deliveryDate = finalLInDate;
                        })
                        this.setState({
                            poRows: poRows,
                            deliveryDate: finalLInDate,
                            isSetDeliveryDate: true
                        })
                    }
                }
            }

            if (!nextProps.purchaseIndent.get_draft_data.isSuccess) {
                if (!nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].deliveryDate && nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.validFromAndTo) {
                    if (!this.state.isSetDeliveryDate) {
                        lInDate = new Date(getDate())
                        lInDate.setDate(lInDate.getDate() + 15)
                        mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                        day = ("0" + lInDate.getDate()).slice(-2);
                        yr = lInDate.getFullYear()
                        finalLInDate = [yr, mnth, day].join("-")
                        this.setState({
                            lastInDate: finalLInDate,
                            deliveryDate: finalLInDate,
                            isSetDeliveryDate: true,
                        })
                    }
                }
            }  
        }

        if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {

            this.setState({
              status: undefined
            })

        }

        if(nextProps.purchaseIndent.save_draft_pi.isSuccess && this.state.copyPiDraft){
            this.setState({
                indentNo: nextProps.purchaseIndent.save_draft_pi.data.resource.indentNo,
            })
        }
        if(nextProps.purchaseIndent.save_draft_pi.isLoading){
            this.setState({
                isDraftRequest: false
            })
        }
        if(nextProps.purchaseIndent.save_draft_pi.isSuccess){

            if(this.state.isContactCalled){
                this.setState({
                    isDraftRequest: true,
                }, () => this.contactSubmit())
            } else {
                this.setState({
                    isDraftRequest: true,
                })
            }
        }
        //console.log(nextProps.purchaseIndent)
        let lInDate = ""
        let mnth = ""
        let day = ""
        let yr = ""
        let finalLInDate = ""

        if (nextProps.purchaseIndent.procurementSite.isSuccess) {
            if (nextProps.purchaseIndent.procurementSite.data.defaultSiteKey != null) {
                if (this.state.siteName == '') {
                    this.setState({
                        isSiteLovSelect: true,
                        siteMount: false,
                        siteCode: nextProps.purchaseIndent.procurementSite.data.defaultSiteKey.defaultSiteCode,
                        siteName: nextProps.purchaseIndent.procurementSite.data.defaultSiteKey.defaultSiteName
                    })
                }
            }
            if (!this.state.siteModal && !this.state.siteMount) {

                if(nextProps.purchaseIndent.procurementSite.data.resource != null) {
                        let value = nextProps.purchaseIndent.procurementSite.data.resource;
                        let siteName = [];
                        let siteCode = [];
                        let siteValue = this.state.siteName.trim().toUpperCase()
                        value.map(item => {
                            siteName.push(item.siteName.toUpperCase())
                            siteCode.push(item.siteCode.toUpperCase())
                        })
                        console.log('siteName', siteName, siteCode, siteValue)
                        if (siteName.includes(siteValue) || siteCode.includes(siteValue)) {
                            console.log('include')
                            value.map(item => {
                                if(item.siteCode.toUpperCase() == siteValue || item.siteName.toUpperCase() == siteValue) {
                                    console.log('conditionChecked')
                                this.setState({
                                    isSiteLovSelect: true,
                                    siteCode: item.siteCode,
                                    siteName: item.siteName
                                }) 
                                }
                            })
                        } else {
                            this.setState({
                                focusId: "siteName",
                                siteCode: "",
                                siteName: "",
                                poErrorMsg: true,
                                errorMassage: "Please select valid input from the drop-down list"
                            })
                        }
                } else if (nextProps.purchaseIndent.procurementSite.data.resource == null) {
                    this.setState({
                        focusId: "siteName",
                        siteCode: "",
                        siteName: "",
                        poErrorMsg: true,
                        errorMassage: "Please select valid input from the drop-down list"
                    })
                }
            }
            this.props.procurementSiteClear()
        }
        if (nextProps.purchaseIndent.size.isSuccess) {
            console.log(nextProps.purchaseIndent.size.data.resource)
            if (nextProps.purchaseIndent.size.data.resource == null) {
                if (this.state.alertFlag == 2 && this.state.articleClicked) {
                    alert("No sizes available!!")
                }
                this.setState({
                    alertFlag: 3
                })
            }
            else {

                if(nextProps.purchaseIndent.size.data.resource.length == 1){
                    if (this.state.articleClicked && (nextProps.purchaseIndent.size.data.resource[0].isToggled == "true" || nextProps.purchaseIndent.size.data.resource[0].isToggled == true)) {
                        if(nextProps.purchaseIndent.size.data.resource[0].cname){
                            let data = nextProps.purchaseIndent.size.data.resource[0];
                            let poRow = [...this.state.poRows]
                            poRow[this.state.selectedRowId -1].lineItem[0].sizeList = [{id: data.id, code: data.code, cname:data.cname}]
                            poRow[this.state.selectedRowId -1].lineItem[0].sizes = [data.cname]
                            this.setState({
                                poRows: poRow
                            })
                        }
                    }
                }
                this.setState({
                    alertFlag: 3
                })
            }
        }

        if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {
            this.closeClearModal()
        } else if (nextProps.purchaseIndent.discardPoPiData.isError) {
            this.closeClearModal()
        }
        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
            //console.log('lead', nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PO'].isLeadTimeDisplayPo)
            this.setState({
                isLeadTimeDisplayPo: nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].isLeadTimeDisplay != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].isLeadTimeDisplay : false,
                isDeliveryDate: nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].deliveryDate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].deliveryDate : false,
            })
            let deliveryDate = nextProps.purchaseIndent.poRadioValidation.data.resource['CUSTOM_PI'].deliveryDate;
            if (!nextProps.purchaseIndent.get_draft_data.isSuccess) {
                if (!this.state.isSetDeliveryDate) {
                    if (deliveryDate && !nextState.isvalidFromAndTo) {
                        lInDate = new Date(getDate())
                        lInDate.setDate(lInDate.getDate() + 15)
                        mnth = ("0" + (lInDate.getMonth() + 1)).slice(-2)
                        day = ("0" + lInDate.getDate()).slice(-2);
                        yr = lInDate.getFullYear()
                        finalLInDate = [yr, mnth, day].join("-")
                        let poRows = [...this.state.poRows]
                        poRows.map(row => {
                            row.deliveryDate = finalLInDate;
                        })
                        this.setState({
                            poRows: poRows,
                            deliveryDate: finalLInDate,
                            isSetDeliveryDate: true,
                        })
                    }
                }
            }
        }
        if (nextProps.purchaseIndent.get_draft_data.indentNo != 0) {
            this.setState({
                indentNoToShow_drafted: nextProps.purchaseIndent.get_draft_data.indentNo
            })
        }
        if (nextProps.purchaseIndent.supplier.isSuccess) {

            if (nextProps.purchaseIndent.supplier.data.resource != null) {

                this.setState({
                    supplierState: nextProps.purchaseIndent.supplier.data.resource,
                })
            } else {
                this.setState({
                    supplierState: [],
                })
            }
        }
        if(nextProps.purchaseIndent.poRadioValidation.data.resource != null || nextProps.purchaseIndent.pi_edit_data.isSuccess || !this.state.isEditPi || nextProps.purchaseIndent.gen_SetBased.isSuccess || nextProps.purchaseIndent.gen_IndentBased.isSuccess || nextProps.purchaseIndent.get_draft_data.isSuccess) {
            if (nextProps.purchaseIndent.get_draft_data.isSuccess && this.state.getDraft == false) {
                
                if (nextProps.purchaseIndent.get_draft_data.data.resource != null) {
                    console.log(nextProps.purchaseIndent.get_draft_data)
                    var validFromEdited_var = nextProps.purchaseIndent.get_draft_data.data.resource.draft.validFrom
                    var validToEdited_var = nextProps.purchaseIndent.get_draft_data.data.resource.draft.validTo
                    this.setState({
                        getDraft: true,
                        typeOfBuying: nextProps.purchaseIndent.get_draft_data.data.resource.draft.typeOfBuying != undefined ? nextProps.purchaseIndent.get_draft_data.data.resource.draft.typeOfBuying : 'Planned',
                        indentNoDraft: nextProps.purchaseIndent.get_draft_data.data.resource.draft.indentNo,
                        status: nextProps.purchaseIndent.get_draft_data.data.resource.draft.status,
                        poValidFrom: validFromEdited_var.substring(0, 10),
                        lastInDate: validToEdited_var.substring(0, 10)
                    }, () => {
                        setTimeout(() => {
                            this.updatePoData(nextProps.purchaseIndent.get_draft_data.data.resource);
                        }, 5);
                    })
                    if (this.state.isvalidFromAndTo) {
                        document.getElementById("piValidFrom").placeholder = validFromEdited_var.substring(0, 10);
                        document.getElementById("lastInDate").placeholder = validToEdited_var.substring(0, 10)
                    }
                }
            }
            if (nextProps.purchaseIndent.pi_edit_data.isSuccess && !this.state.isEditPi) {
                console.log("pedning", nextProps.purchaseIndent.pi_edit_data)
                const { resource } = nextProps.purchaseIndent.pi_edit_data.data;
                setTimeout(() => {
                    this.updatePoData(resource);
                }, 5);
                var validFromEdited_var = nextProps.purchaseIndent.pi_edit_data.data.resource.validFrom
                var validToEdited_var = nextProps.purchaseIndent.pi_edit_data.data.resource.validTo
                this.setState({
                    isEditPi: true,
                    editableRow: resource.piDetails.length,
                    indentNoToShow_pending: nextProps.purchaseIndent.pi_edit_data.data.resource.indentNo,
                    typeOfBuying: nextProps.purchaseIndent.pi_edit_data.data.resource.typeOfBuying,
                    poValidFrom: validFromEdited_var.substring(0, 10),
                    lastInDate: validToEdited_var.substring(0, 10), 
                    status:nextProps.purchaseIndent.pi_edit_data.data.resource.status
                })
                if (this.state.isvalidFromAndTo) {
                    document.getElementById("piValidFrom").placeholder = validFromEdited_var.substring(0, 10);
                    document.getElementById("lastInDate").placeholder = validToEdited_var.substring(0, 10)
                }
        }
        if (nextProps.purchaseIndent.gen_IndentBased.isSuccess) {
            this.setState({
                poRows: [],
                loader: false,
            })
            if (nextProps.purchaseIndent.gen_IndentBased.data.resource != null) {

                this.setState({
                    piKeyData: nextProps.purchaseIndent.gen_IndentBased.data.resource,
                    isCopyIndent: true,
                    copyPi: true,
                    copyPiDraft: true,
                    isEditPi: true,
                    editableRow: nextProps.purchaseIndent.gen_IndentBased.data.resource.piDetails.length,
                })
                setTimeout(() => {
                    this.updatePoData(nextProps.purchaseIndent.gen_IndentBased.data.resource);
                }, 5);
            }
        this.props.gen_IndentBasedClear();
        }
        if (nextProps.purchaseIndent.gen_SetBased.isSuccess) {
            this.setState({
                poRows: []
            })
            if (nextProps.purchaseIndent.gen_SetBased.data.resource != null) {

                this.setState({
                    piKeyData: nextProps.purchaseIndent.gen_SetBased.data.resource,


                })
                setTimeout(() => {
                    this.updatePoData(nextProps.purchaseIndent.gen_SetBased.data.resource);
                }, 5);
            }

        }
    }
        if (nextProps.purchaseIndent.viewImages.isSuccess) {
        }
        
        if (nextProps.purchaseIndent.getItemDetails.isSuccess) {            
            if (nextProps.purchaseIndent.getItemDetails.data.essentialProParam != undefined) {
                this.setState({
                    cat1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 : this.state.cat1Validation,
                    cat2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 : this.state.cat2Validation,
                    cat3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 : this.state.cat3Validation,
                    cat4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 : this.state.cat4Validation,
                    cat5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 : this.state.cat5Validation,
                    cat6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 : this.state.cat6Validation,
                    desc1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 : this.state.desc1Validation,
                    desc2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 : this.state.desc2Validation,
                    desc3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 : this.state.desc3Validation,
                    desc4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 : this.state.desc4Validation,
                    desc5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 : this.state.desc5Validation,
                    desc6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 : this.state.desc6Validation,

                    itemudf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 : this.state.itemudf1Validation,
                    itemudf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 : this.state.itemudf2Validation,
                    itemudf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 : this.state.itemudf3Validation,
                    itemudf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 : this.state.itemudf4Validation,
                    itemudf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 : this.state.itemudf5Validation,
                    itemudf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 : this.state.itemudf6Validation,
                    itemudf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 : this.state.itemudf7Validation,
                    itemudf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 : this.state.itemudf8Validation,
                    itemudf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 : this.state.itemudf9Validation,
                    itemudf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 : this.state.itemudf10Validation,
                    itemudf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 : this.state.itemudf11Validation,
                    itemudf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 : this.state.itemudf12Validation,
                    itemudf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 : this.state.itemudf13Validation,
                    itemudf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 : this.state.itemudf14Validation,
                    itemudf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 : this.state.itemudf15Validation,
                    itemudf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 : this.state.itemudf16Validation,
                    itemudf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 : this.state.itemudf17Validation,
                    itemudf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 : this.state.itemudf18Validation,
                    itemudf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 : this.state.itemudf19Validation,
                    itemudf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 : this.state.itemudf20Validation,

                    udf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 : this.state.udf1Validation,
                    udf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 : this.state.udf2Validation,
                    udf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 : this.state.udf3Validation,
                    udf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 : this.state.udf4Validation,
                    udf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 : this.state.udf5Validation,
                    udf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 : this.state.udf6Validation,
                    udf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 : this.state.udf7Validation,
                    udf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 : this.state.udf8Validation,
                    udf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 : this.state.udf9Validation,
                    udf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 : this.state.udf10Validation,

                    udf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 : this.state.udf11Validation,
                    udf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 : this.state.udf12Validation,
                    udf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 : this.state.udf13Validation,
                    udf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 : this.state.udf14Validation,
                    udf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 : this.state.udf15Validation,
                    udf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 : this.state.udf16Validation,
                    udf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 : this.state.udf17Validation,
                    udf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 : this.state.udf18Validation,
                    udf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 : this.state.udf19Validation,
                    udf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 : this.state.udf20Validation,



                })
            }
            if (nextProps.purchaseIndent.getItemDetails.data.resource != null) {
                let poRows = [...this.state.poRows]
                if (nextProps.purchaseIndent.getItemDetails.data.resource.catDescHeader != null) {
                    let poUdfData = []
                    poUdfData = nextProps.purchaseIndent.getItemDetails.data.resource.catDescHeader
                    let c = []
                    for (let j = 0; j < poUdfData.length; j++) {
                        let x = poUdfData[j]
                        x.value = "";
                        x.code = ""
                        let a = x
                        c.push(a)
                        console.log('a', a)

                    }
                    if(this.state.isDisplayCatRemark){
                        c = [...c, {
                            catdesc: "CATREMARK",
                            value: "",
                        }]
                    }
                    console.log('c', c)
                    let obj = [{ catDescId: 1, catDesc: c }]

                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.state.selectedRowId && (poRows[this.state.selectedRowId - 1].quantity == '' || poRows[this.state.selectedRowId - 1].quantity == 0)) {
                            poRows[i].catDescHeader = nextProps.purchaseIndent.getItemDetails.data.resource.catDescHeader
                            poRows[i].catDescArray = obj

                        }
                    }
                    let poRowss = _.map(
                        _.uniq(
                            _.map(poRows, function (obj) {

                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    // this.setState({
                    //     poRows: poRowss
                    // })
                    poRows = poRowss
                }
                

                if (nextProps.purchaseIndent.getItemDetails.data.resource.setUdfHeader != null) {
                    let udfMappingData = nextProps.purchaseIndent.getItemDetails.data.resource.setUdfHeader
                    
                    let c = []
                    for (let j = 0; j < udfMappingData.length; j++) {
                        let x = udfMappingData[j]
                        x.value = "";
                        let a = x
                        c.push(a)
                    }
                    // let poRows = [...this.state.poRows]
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.state.selectedRowId  && (poRows[this.state.selectedRowId - 1].quantity == '' || poRows[this.state.selectedRowId - 1].quantity == 0)) {
                            poRows[i].lineItem[0].setUdfHeader = nextProps.purchaseIndent.getItemDetails.data.resource.setUdfHeader
                            poRows[i].lineItem[0].setUdfArray = c
                        }
                    }
    
                    let poRowss = _.map(
                        _.uniq(
                            _.map(poRows, function (obj) {
    
                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    // this.setState({
                    //     poRows: poRowss
                    // })
                    poRows = poRowss
                    console.log('poRows', poRows)
                }

                if (nextProps.purchaseIndent.getItemDetails.data.resource.itemUdfHeader != null) {
                    let poUdfData = []
                    let c = []
                    let itemObj = {}
                    poUdfData = nextProps.purchaseIndent.getItemDetails.data.resource.itemUdfHeader
                    for (let j = 0; j < poUdfData.length; j++) {    
                        poUdfData[j].value = "";    
                        c.push(poUdfData[j])
                        itemObj[poUdfData[j].cat_desc_udf] = ""
                    }
                    
                    // let poRows = [...this.state.poRows]
                    let obj = [{ itemUdfId: 1, itemUdf: c }]
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].gridOneId == this.state.selectedRowId  && (poRows[this.state.selectedRowId - 1].quantity == '' || poRows[this.state.selectedRowId - 1].quantity == 0)) {
                            poRows[i].itemUdfHeader = nextProps.purchaseIndent.getItemDetails.data.resource.itemUdfHeader
                            poRows[i].itemUdfArray = obj
                            this.callSizeRequest(poRows[i].departmentName, poRows[i].divisionName, poRows[i].sectionName, poRows[i].departmentCode, poRows[i].articleName, itemObj)
                        }
    
                    }
                    let poRowss = _.map(
                        _.uniq(
                            _.map(poRows, function (obj) {
    
                                return JSON.stringify(obj);
                            })
                        ), function (obj) {
                            return JSON.parse(obj);
                        }
                    );
                    // this.setState({
                    //     poRows: poRowss
                    // })
                    poRows = poRowss
    
    
    
                }
                this.setState({
                    poRows: poRows
                })
                console.log('porrr', poRows)

            } else {
                this.setState({

                    poErrorMsg: true,
                    errorMassage: "Categories and Description are not available corresponding department"
                })
            }
        }

        if (nextProps.purchaseIndent.gen_PurchaseIndent.isSuccess) {
            this.setState({
                isContactCalled: false,
                loader: false,
            })
            this.dateCall()
            this.onClear("I came from update API");
            this.setBasedResest();
            this.props.gen_PurchaseIndentClear();
        } else if (nextProps.purchaseIndent.gen_PurchaseIndent.isError) {
            this.setState({
              isContactCalled: false,
              loader: false,
            })
            this.props.gen_PurchaseIndentClear();
        }
        if (Object.keys(nextProps.purchaseIndent.gen_PurchaseIndent.data).length != 0) {

            this.setState({
                loader: false,
                currentDate: getDate(),
                successVar: true,
                orderNumberSaveDraft: false
            })


        } else if (nextProps.purchaseIndent.gen_PurchaseIndent.isError) {
            this.setState({
                loader: false,
                successVar: false
            })

        }
        else {
            this.setState({
                loader: false,
                successVar: false
            })

        }
        if (nextProps.purchaseIndent.save_edited_pi.isError) {
            this.setState({
                isContactCalled: false,
                loader: false,
            })
            this.props.editedPiSaveClear();
        }
        if (nextProps.purchaseIndent.save_edited_pi.isSuccess) {
            this.setState({
                isContactCalled: false,
                loader: false,
            })
            this.dateCall()
            this.onClear("I came from update API");
            this.setBasedResest();
            this.props.editedPiSaveClear();
        }
        else if (Object.keys(nextProps.purchaseIndent.save_edited_pi.data).length != 0) {

            this.setState({
                loader: false,
                currentDate: getDate(),
                successVar: true
            })


        } else if (nextProps.purchaseIndent.save_edited_pi.isError) {
            this.setState({
                loader: false,
                successVar: false
            })
        }
        else {
            this.setState({
                successVar: false
            })
        }

        if (nextProps.purchaseIndent.loadIndent.isSuccess) {

            this.setState({
                loadData: nextProps.purchaseIndent.loadIndent.data.resource
            })

        }

        if (nextProps.purchaseIndent.leadTime.isSuccess) {
            this.setState({
                leadDays: nextProps.purchaseIndent.leadTime.data.resource.leadTime
            })
        }

        if (nextProps.purchaseIndent.markUp.isSuccess) {

            if (nextProps.purchaseIndent.markUp.data.resource != null) {


                let poRows = [...this.state.poRows]
                for (var x = 0; x < poRows.length; x++) {
                    if (poRows[x].gridOneId == this.state.selectedRowId) {
                        let lineItem = [...poRows[x].lineItem]
                        for (let y = 0; y < poRows[x].lineItem.length; y++) {
                            // if (poRows[x].lineItem[y].gridTwoId == nextProps.purchaseIndent.markUp.data.rowId) {
                            let t = this.state.selectedRowId - 1

                            lineItem[y].mrk = nextProps.purchaseIndent.markUp.data.resource.actualMarkUp
                            lineItem[y].calculatedMargin = nextProps.purchaseIndent.markUp.data.resource.calculatedMargin
                            lineItem[y].lineItemChk = false
                            lineItem[y].gst = nextProps.purchaseIndent.markUp.data.resource.gst
                            if (nextProps.purchaseIndent.markUp.data.resource.calculatedMargin > 45 && this.state.isDisplayMarginRule) {
                                this.setState({
                                    focusId: "date" + t,
                                    poErrorMsg: true,
                                    errorMassage: "Margin rule is greater than 45%"
                                })
                            }
                            // }
                        }
                    }
                }

                this.setState({
                    poRows: poRows
                }, () => {

                    this.updatePo()
                    setTimeout(() => {

                        this.gridFirst();
                    }, 10)


                })


            }
            this.setState({
                markUpYes: false
            })

            this.setState({
                lineItemChange: false
            })

        }

        if (nextProps.purchaseIndent.lineItem.isSuccess) {
            if (nextProps.purchaseIndent.lineItem.data.resource != null) {


                let rows = [...this.state.poRows]
                for (let x = 0; x < rows.length; x++) {
                    if (rows[x].gridOneId == this.state.selectedRowId) {
                        for (let y = 0; y < rows[x].lineItem.length; y++) {
                            if (rows[x].lineItem[y].gridTwoId == nextProps.purchaseIndent.lineItem.data.rowId) {

                                rows[x].lineItem[y].amount = Math.round(nextProps.purchaseIndent.lineItem.data.resource.netAmount * 100) / 100
                                rows[x].lineItem[y].gst = nextProps.purchaseIndent.lineItem.data.resource.gst
                                rows[x].lineItem[y].tax = nextProps.purchaseIndent.lineItem.data.resource.tax
                                rows[x].lineItem[y].finCharges = nextProps.purchaseIndent.lineItem.data.resource.finCharges
                                rows[x].lineItem[y].basic = nextProps.purchaseIndent.lineItem.data.resource.basic // lineitem basic
                                rows[x].lineItem[y].lineItemChk = false


                            }
                        }
                    }

                }
                this.setState({
                    poRows: rows,

                }, () => {

                    // this.updateNetAmt(nextProps.purchaseIndent.lineItem.data.rowId, nextProps.purchaseIndent.lineItem.data.designRow)
                    this.updatePoAmount();
                    setTimeout(() => {

                        this.updatePo()
                    }, 10)


                })



                !this.state.isRspRequired ? this.setState({
                    lineItemChange: false
                }) : null

            }

            this.props.lineItemClear();
        } else if (nextProps.purchaseIndent.lineItem.isError) {


            this.setState({
                lineItemChange: false
            })

            let poRows = [...this.state.poRows]
            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == this.state.selectedRowId) {
                    for (let j = 0; j < poRows[i].lineItem.length; j++) {
                        if (poRows[i].lineItem[j].gridTwoId == nextProps.purchaseIndent.lineItem.message.rowId) {
                            poRows[i].lineItem[j].lineItemChk = true
                        }
                    }
                }
            }



            this.setState({
                poRows: poRows
            })


        }
        if (nextProps.purchaseIndent.poItemcode.isSuccess) {
            if (nextProps.purchaseIndent.poItemcode.data.resource != null) {

                let c = []
                for (let i = 0; i < nextProps.purchaseIndent.poItemcode.data.resource.length; i++) {


                    let x = nextProps.purchaseIndent.poItemcode.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    poItemcodeData: c
                })
            }
            if(!this.state.itemModal){
                let poRows = [...this.state.poRows]
                if(nextProps.purchaseIndent.poItemcode.data.resource != null) {
                    let value = nextProps.purchaseIndent.poItemcode.data.resource;
                    let mrpArray = value.map(item => item.mrp.toString())
                    let mrpValue = poRows[this.state.selectedRowIndex].vendorMrp;
                    if (mrpArray.includes(mrpValue)){
                        value.forEach(item => {
                            if (item.mrp == mrpValue) {
                                this.updateItem(item)
                            }
                        })
                    } else {
                        poRows[this.state.selectedRowIndex].vendorMrp = "";
                        poRows[this.state.selectedRowIndex].mrp = "";
                        poRows[this.state.selectedRowIndex].rsp = "";
                        this.setState({
                            focusId: "vendorMrp" + this.state.selectedRowIndex,
                            poRows: poRows,
                            poErrorMsg: true,
                            errorMassage: "Please select valid input from the drop-down list"
                        })
                    }
                } else {
                    poRows[this.state.selectedRowIndex].vendorMrp = "";
                    poRows[this.state.selectedRowIndex].mrp = "";
                    poRows[this.state.selectedRowIndex].rsp = "";
                    this.setState({
                        focusId: "vendorMrp" + this.state.selectedRowIndex,
                        poRows: poRows,
                        poErrorMsg: true,
                        errorMassage: "Please select valid input from the drop-down list"
                    })
                }
            }
        }


        if (nextProps.purchaseIndent.color.isSuccess) {
            if (nextProps.purchaseIndent.color.data.resource != null) {
                this.setState({
                    colorNewData: nextProps.purchaseIndent.color.data.resource
                })
            }
        }
        if (nextProps.purchaseIndent.getMultipleMargin.isSuccess) {
            let lineItem = nextProps.purchaseIndent.getMultipleMargin.data.resource.multipleMarkup
            let poRows = [...this.state.poRows]
            if (lineItem[0].calculatedMargin > 45 && this.state.isDisplayMarginRule) {
                let t = this.state.selectedRowId - 1
                this.setState({
                    focusId: "date" + t,
                    poErrorMsg: true,
                    errorMassage: "Margin rule is greater than 45%"
                })
            }

            for (let i = 0; i < lineItem.length; i++) {
                for (let j = 0; j < poRows.length; j++) {
                    if (poRows[j].gridOneId == lineItem[i].designRowid) {
                        for (let k = 0; k < poRows[j].lineItem.length; k++) {
                            if (poRows[j].lineItem[k].gridTwoId == lineItem[i].rowId) {
                                poRows[j].lineItem[k].mrk = lineItem[i].actualMarkUp
                                poRows[j].lineItem[k].calculatedMargin = lineItem[i].calculatedMargin

                            }
                        }
                    }


                }
            }
            this.setState({
                poRows: poRows,
                lineItemChange: false
            }, () => {

                this.updatePo()
            })

        }

        if (nextProps.purchaseIndent.multipleLineItem.isSuccess) {
            let lineItem = nextProps.purchaseIndent.multipleLineItem.data.resource.multipleLineItem
            let poRows = [...this.state.poRows]
            let designId = lineItem[0].designRowid

            poRows.forEach(data1 => {
                console.log(poRows)
                data1.lineItem.forEach(data2 => {
                    if (data1.gridOneId == this.state.selectedRowId) {
                        if (lineItem.length == 1) {
                                if (data2.gridTwoId == lineItem[0].rowId) {
                                    console.log(data2, lineItem)
                                    data2.amount = Math.round(lineItem[0].netAmount * 100) / 100
                                    data2.gst = lineItem[0].gst
                                    data2.tax = lineItem[0].tax
                                    data2.finCharges = lineItem[0].finCharges
                                    data2.lineItemChk = false
                                    data2.basic = lineItem[0].basic
                                }
                        } else {
                                for (let i = 0; i < lineItem.length; i++) {
                                    if ((data2.gridTwoId) == Number(lineItem[i].rowId)) {
                                        data2.amount = Math.round(lineItem[i].netAmount * 100) / 100
                                        data2.gst = lineItem[i].gst
                                        data2.tax = lineItem[i].tax
                                        data2.finCharges = lineItem[i].finCharges
                                        data2.lineItemChk = false
                                        data2.basic = lineItem[i].basic
                                    }
                                }
                            }
                    }
                })
            })


            this.setState({
                poRows: poRows
            }, () => {
                // this.calculateTotalBasic()
                this.updateSelectedPo()
                setTimeout(() => {
                    this.state.isRspRequired ? this.multiMarkUp(designId) : null
                    !this.state.isRspRequired ? this.setState({
                        lineItemChange: false
                    }) : null
                }, 10)
                this.getOpenToBuy("", "", lineItem)
            })




        } else if (nextProps.purchaseIndent.multipleLineItem.isError) {
            let focusedObj = this.state.focusedObj
            if (focusedObj.type == "rate") {
                let poRows = [...this.state.poRows]
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == focusedObj.rowId) {
                        poRows[i].rate = focusedObj.value
                        poRows[i].finalRate = focusedObj.finalRate
                    }
                }


                this.setState({
                    poRows: poRows,
                    focusedObj: {
                        type: "",
                        rowId: "",
                        cname: "",
                        value: "",
                        radio: "",
                        finalRate: "",
                        colorObj: {
                            color: [],
                            colorList: []
                        }

                    }
                })
            }
        }
        if (nextProps.purchaseIndent.otb.isSuccess) {
            if (nextProps.purchaseIndent.otb.data.resource != null) {
                // let c = this.state.poRowsfor (var x = 0; x < c.length; x++) { if (c[x].gridOneId == nextProps.purchaseIndent.otb.data.rowId) {
                //     c[x].otb = nextProps.purchaseIndent.otb.data.resource.otbthis.setState({    poRows: c})}}
                if (this.state.isOtbValidationPi == "true") {
                    if (nextProps.purchaseIndent.otb.data.resource.otb.toString() == "0") {
                        this.setState({
                            poErrorMsg: true,
                            errorMassage: "OTB is zero for the corresponding Article and MRP"
                        })
                    }
                }
                const t = this
                setTimeout(function () {
                    if (t.state.changeLastIndate) {

                        t.getOtbLastInDate(nextProps.purchaseIndent.otb.data.rowId, nextProps.purchaseIndent.otb.data.resource.otb)
                        t.gridFirst();
                    } else {

                        t.gridFirst();
                        t.getOpenToBuy(nextProps.purchaseIndent.otb.data.rowId, nextProps.purchaseIndent.otb.data.resource.otb);
                    }
                }, 1)
            }
            this.props.otbClear();
        }
        if (nextProps.purchaseIndent.marginRule.isSuccess) {
            if (nextProps.purchaseIndent.marginRule.data.resource != null) {
                let rows = [...this.state.poRows]
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].gridOneId == this.state.selectedRowId) {
                        rows[i].marginRule = nextProps.purchaseIndent.marginRule.data.resource
                        this.setState({
                            poRows: rows,
                            saveMarginRule: nextProps.purchaseIndent.marginRule.data.resource
                        })
                    }
                }
            }
            else {
                if(this.state.isMarginRulePi) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Margin rule is not available for the corresponding Article and Supplier"
                    })
                }
            }
            this.props.marginRuleClear()
        }

        if (nextProps.purchaseIndent.multipleOtb.isSuccess) {
            let poRows = [...this.state.poRows]
            if (nextProps.purchaseIndent.multipleOtb.data.resource != null) {
                let multipleOtb = nextProps.purchaseIndent.multipleOtb.data.resource
                for (let i = 0; i < multipleOtb.length; i++) {
                    for (let j = 0; j < poRows.length; j++) {
                        if (multipleOtb[i].rowId == poRows[j].gridOneId) {
                            poRows[j].lineItem[0].otb = multipleOtb[i].otb
                            poRows[j].otb = multipleOtb[i].otb

                        }
                    }
                }
            }
            this.setState({
                poRows: poRows
            }, () => {
                if (this.state.changeLastIndate) {
                    this.updateMultipleOtb()
                }
            })

        }

        if (nextProps.purchaseIndent.hsnCode.isSuccess && this.state.onFieldhsn == true) {
            if (nextProps.purchaseIndent.hsnCode.data.resource != null  && this.state.poRows[this.state.selectedRowId - 1].isHsnDefault == false) {
                if (this.state.poRows && this.state.poRows.length > 0) {
                    let poRows = [...this.state.poRows]
                    poRows[this.state.selectedRowId - 1].hsnSacCode = nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNSACCode;

                    poRows[this.state.selectedRowId - 1].hsnCode = nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNCode;
                    poRows[this.state.selectedRowId -1].isHsnDefault = true;
                    this.setState({
                        hsnCode: nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNCode == null ? "" : nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNCode,
                        hsnSacCode: nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNSACCode == null ? "" : nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNSACCode,
                        poRows: poRows
                    })
                }

            } else if(nextProps.purchaseIndent.hsnCode.data.resource == null && this.state.poRows[this.state.selectedRowId - 1].isHsnDefault == false) {
                this.setState({
                    hsnCode: "",
                    hsnSacCode: "",
                    onFieldhsn: false
                })
            }
            else if(!this.state.hsnModal && this.state.poRows[this.state.selectedRowId - 1].isHsnDefault) {
                console.log('clicked')
                let poRows = [...this.state.poRows]
                if(nextProps.purchaseIndent.hsnCode.data.resource != null) {
                    let value = nextProps.purchaseIndent.hsnCode.data.resource;
                    let hsnCodeArray = value.map(item => item.hsnSacCode);
                    let hsnValue = poRows[this.state.selectedRowIndex].hsnSacCode.trim();
                    if(poRows[this.state.selectedRowIndex].hsnSacCode != '' && !poRows[this.state.selectedRowIndex].isHsnLovSelect) {
                        if(hsnCodeArray.includes(hsnValue)) {
                            value.map(item => {
                                if (item.hsnSacCode == hsnValue) {
                                    poRows[this.state.selectedRowIndex].hsnSacCode = item.hsnSacCode;
                                    poRows[this.state.selectedRowIndex].hsnCode = item.hsnCode
                                    poRows[this.state.selectedRowIndex].isHsnLovSelect = true
                                    this.setState({
                                        hsnCode: item.hsnCode,
                                        hsnSacCode: item.hsnSacCode,
                                        poRows: poRows
                                    })
                                }
                            })
                        } else {
                            poRows[this.state.selectedRowIndex].hsnSacCode = "";
                            poRows[this.state.selectedRowIndex].hsnCode = "";
                            this.setState({
                                focusId: "hsnCode" + this.state.selectedRowIndex,
                                poRows: poRows,
                                hsnCode: "",
                                hsnSacCode: "",
                                poErrorMsg: true,
                                errorMassage: "Please select valid input from the drop-down list"
                            })
                        }
                    }
                } else if (nextProps.purchaseIndent.hsnCode.data.resource == null) {
                    poRows[this.state.selectedRowIndex].hsnSacCode = "";
                    poRows[this.state.selectedRowIndex].hsnCode = "";
                    this.setState({
                        focusId: "hsnCode" + this.state.selectedRowIndex,
                        poRows: poRows,
                        hsnCode: "",
                        hsnSacCode: "",
                        poErrorMsg: true,
                        errorMassage: "Please select valid input from the drop-down list"
                    })
                }
            }
        }

        if (nextProps.purchaseIndent.save_draft_pi.isSuccess && this.state.indentNo == "") {
            if (nextProps.purchaseIndent.save_draft_pi.data.resource != null) {
                this.setState({
                    indentNo: nextProps.purchaseIndent.save_draft_pi.data.resource.indentNo
                })
            }
            this.closeClearModal()
        }
        if (nextProps.purchaseIndent.save_draft_pi.isError) {
            this.closeClearModal()
        }
        if (nextProps.purchaseIndent.save_draft_pi.isSuccess && this.state.saveDraftFlag == true) {
            if (nextProps.purchaseIndent.save_draft_pi.data.resource != null) {
                this.setState({
                    indentNo: nextProps.purchaseIndent.save_draft_pi.data.resource.indentNo
                })
                if (this.state.saveDraftFlag) {
                    this.onClear()
                }
            }
        }
        if (nextProps.purchaseIndent.gen_IndentBased.isLoading) {
            this.setState({
                loader: true,
            })
        }
    }

    createSet(id, item) {
        const isDeliveryDate = this.state.isDeliveryDate ? item.deliveryDate == "" || (item.deliveryDate < this.state.minDate) : null;
        let flag;

        this.state.mrpValidation == false ? 

            flag = (item.vendorDesign == "" && !this.state.isVendorDesignNotReq) || item.rate == "" || (item.wsp == "" && this.state.isDisplayWSP && this.state.isMandateWSP) || item.marginRule == "" && this.state.isMarginRulePi || item.hsnSacCode == ""
            || isDeliveryDate ? true : false
        :
            flag = (item.vendorMrp == "" && this.state.isMrpRequired) || (item.vendorDesign == "" && !this.state.isVendorDesignNotReq) || item.rate == "" || item.wsp == "" && this.state.isDisplayWSP && this.state.isMandateWSP || item.marginRule == "" && this.state.isMarginRulePi || item.hsnSacCode == ""
            || (item.mrp == "" && this.state.isMRPEditable) || isDeliveryDate ? true : false

        if (flag) {
            this.setState({
                poErrorMsg: true,
                errorMassage: this.state.isDeliveryDate ? "Please fill mandate fields first and correct delivery date for this row." : "Please fill mandate fields first for this row."
            })
        } else {
            this.setState({
                selectedRowId: id,
                setModal: true,
                setModalAnimation: true,
                departmentCode: item.departmentCode,
                department: item.departmentName,
                articleName: item.articleName,
                articleCode: item.articleCode,
                divisionName: item.divisionName,
                sectionName: item.sectionName
            })
        }
    }
    closeSetModal() {
        this.setState({
            setModal: false,
            setModalAnimation: false,
        })
    }
    updatePoRows(data) {
        this.setState({
            poRows: data
        })

    }

    gridFirst() {
        let poRows = [...this.state.poRows]
        let flag = false
        if(this.state.mrpValidation == false){
            poRows.forEach(po => {
                flag = (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) || po.rate == "" || (po.wsp == "" && this.state.isMandateWSP && this.state.isDisplayWSP) || po.marginRule == "" && this.state.isMarginRulePi || po.hsnSacCode == ""
                     || (po.deliveryDate == "" && this.state.deliveryDate) ? true : false
            })
        } 
        else{
            poRows.forEach(po => {
                flag = (po.vendorMrp == "" && this.state.isMrpRequired) || (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) || po.rate == "" || (po.wsp == "" && this.state.isMandateWSP && this.state.isDisplayWSP) || po.marginRule == "" && this.state.isMarginRulePi || po.hsnSacCode == ""
                    || (po.mrp == "" && this.state.isMRPEditable) || (po.deliveryDate == "" && this.state.deliveryDate) ? true : false
            })
        }
        
        if (flag) {
            this.setState({
                gridFirst: false
            })
        } else {
            this.setState({
                gridFirst: true
            })
        }
    }
    gridSecond() {
        let poRows = [...this.state.poRows]
        let flag = false
        for (let i = 0; i < poRows.length; i++) {


            for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                    if ((poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT1" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT2" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT3" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT4" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT5" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT6" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC1" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC2" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC3" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC4" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC5" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC6" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "")) {
                        flag = true

                        break
                    }


                }
            }
        }
        if (flag) {
            this.setState({
                gridSecond: false
            })
        } else {
            this.setState({
                gridSecond: true
            })
        }
    }

    gridThird() {
        let poRows = [...this.state.poRows]
        let flag = true
        for (var i = 0; i < poRows.length; i++) {
            for (var j = 0; j < poRows[i].itemUdfArray[0].itemUdf.length; j++) {

                if ((poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING01" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING02" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING03" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING04" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING05" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING06" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING07" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING08" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING09" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING10" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM01" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM02" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM03" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM04" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM05" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE01" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE02" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE04" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE05" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "")) {
                    flag = false
                    break
                }

            }
        }
        if (flag) {
            this.setState({
                gridThird: true
            })
        } else {
            this.setState({
                gridThird: false
            })
        }
    }
    gridFourth() {

        let poRows = [...this.state.poRows]
        let flag = true
        poRows.forEach(po => {
            po.lineItem.forEach(cd => {
                if (cd.setQty == "" || cd.setQty.toString() == "0" || cd.setQty == 0) {

                    flag = false
                } else if (this.state.isMandateImage && (cd.imagePath == undefined || cd.imagePath == '')) {
                    flag = false
                }
            })

        })

        if (flag) {
            this.setState({
                gridFourth: true
            })
        } else {
            this.setState({
                gridFourth: false
            })
        }

    }

    gridFivth() {
        let poRows = [...this.state.poRows]
        let flag = true
        for (let i = 0; i < poRows.length; i++) {
            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {
                    if ((poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING02" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING03" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING04" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING05" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING06" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING07" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING08" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING09" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING10" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM01" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM02" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM03" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM04" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM05" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE01" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE02" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE03" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE04" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE05" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "")) {
                        flag = false

                        break
                    }
                }
            }
        }

        if (flag) {
            this.setState({
                gridFivth: true
            })
        } else {
            this.setState({
                gridFivth: false
            })
        }
    }

    validateCatDescRow() {
        let poRows = [...this.state.poRows]
        let type = ""
        for (let i = 0; i < poRows.length; i++) {


            for (let j = 0; j < poRows[i].catDescArray.length; j++) {
                for (let k = 0; k < poRows[i].catDescArray[j].catDesc.length; k++) {
                    console.log('mandate field', poRows[i].catDescArray[j].catDesc[k])
                    if ((poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT1" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT2" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT3" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT4" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT5" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CAT6" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC1" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC2" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC3" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC4" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC5" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "DESC6" && ((poRows[i].catDescArray[j].catDesc[k].isDisplayPI == "Y") && poRows[i].catDescArray[j].catDesc[k].isCompulsoryPI == "Y") && poRows[i].catDescArray[j].catDesc[k].value == "") ||
                        (poRows[i].catDescArray[j].catDesc[k].catdesc == "CATREMARK" && this.state.isMandateCatRemark == true && poRows[i].catDescArray[j].catDesc[k].value == "")) {

                        type += + typeof (poRows[i].catDescArray[j].catDesc[k].displayName) == "string" && poRows[i].catDescArray[j].catDesc[k].displayName != "null" ? poRows[i].catDescArray[j].catDesc[k].displayName : poRows[i].catDescArray[j].catDesc[k].catdesc + ","
                        // break
                    }


                }
            }
        }
        if (type != "") {
            this.setState({
                poErrorMsg: true,
                // errorMassage: type.substring(0, type.length - 2) + " is/are compulsory"
                errorMassage: "Please fill mandate fields in CAT DESC First."


            })
        }
    }

    validateItemdesc() {

        let poRows = [...this.state.poRows]
        poRows.forEach(po => {
            if(this.state.mrpValidation == true){
                if (po.vendorMrp == "" && this.state.isMrpRequired ) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "MRP is compulsory"
                    })
                }
            }
            
            else if (po.vendorDesign == "" && !this.state.isVendorDesignNotReq) {
                

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor Design is compulsory"
                })

            }
            else if (po.hsnSacCode == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "HSN  is compulsory"
                })

            }
            else if( this.state.mrpValidation == true ){
                if (po.mrp == "" && this.state.isMRPEditable) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Mrp is compulsory"
                    })
                }
            }

            else if (po.rate == "") {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Rate is compulsory"
                })


            }
            else if (po.wsp == "" && this.state.isDisplayWSP && this.state.isMandateWSP) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "WSP is compulsory"
                })


            } else if (this.state.isDeliveryDate ? po.deliveryDate == "" || po.deliveryDate < this.state.minDate : false) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Delivery date is compulsory"
                })


            } else if (po.marginRule == "" && this.state.isMarginRulePi) {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Margin Rule is compulsory"
                })
            } else if (this.state.isDeliveryDate ? this.state.minDate > po.deliveryDate : false) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Please enter Correct Delivery date"
                })
            }
        })
    }
    validateUdf() {
        let poRows = [...this.state.poRows]
        var cat_desc_udf = ""

        for (var i = 0; i < poRows.length; i++) {
            for (var j = 0; j < poRows[i].itemUdfArray[0].itemUdf.length; j++) {


                if ((poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING01" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING02" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING03" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING04" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING05" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING06" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING07" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING08" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING09" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFSTRING10" && (( poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM01" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM02" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM03" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM04" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFNUM05" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE01" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE02" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE04" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == "") ||
                    (poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf == "UDFDATE05" && ((poRows[i].itemUdfArray[0].itemUdf[j].isDisplayPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y") && poRows[i].itemUdfArray[0].itemUdf[j].value == ""))
                // if (poRows[i].itemUdfArray[0].itemUdf[j].isCompulsoryPI == "Y" && poRows[i].itemUdfArray[0].itemUdf[j].value == "")
                {

                    cat_desc_udf += poRows[i].itemUdfArray[0].itemUdf[j].displayName != null ? poRows[i].itemUdfArray[0].itemUdf[j].displayName : poRows[i].itemUdfArray[0].itemUdf[j].cat_desc_udf + " ,"
                    // break
                }

            }
        }
        console.log('cateee', cat_desc_udf, poRows)
        if (cat_desc_udf != "") {
            this.setState({
                poErrorMsg: true,
                // errorMassage: cat_desc_udf.substring(0, cat_desc_udf.length - 2) + " is/are compulsory"
                errorMassage: "Please fill mandate fields in ITEM UDF First."

            })
        }
    }
    validateLineItem() {
        let poRows = [...this.state.poRows]
        poRows.forEach(po => {
            po.lineItem.forEach(sec => {
                //set quantity is empty and color : ["NA"];
                if (sec.setQty == "" || sec.setQty.toString() == "0" || sec.setQty == 0) {

                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Set quantity is compulsory"
                    })
                }

                else if (sec.color.length == 0) {
                    // color i empty
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Color is compulsory"
                    })

                }

                else if (this.state.isMandateImage && (sec.imagePath == '' || sec.imagePath == undefined)) {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Image is compulsory"
                    })
                }
            })

        })
    }
    //_______________________________________validate set udf___________________________

    validateSetUdf() {
        let poRows = [...this.state.poRows]
        var displayName = ""

        for (let i = 0; i < poRows.length; i++) {
            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                for (let k = 0; k < poRows[i].lineItem[j].setUdfArray.length; k++) {


                    if ((poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING02" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING03" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING04" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING05" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING06" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING07" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING08" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING09" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFSTRING10" && (( poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM01" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM02" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM03" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM04" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFNUM05" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE01" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE02" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE03" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE04" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == "") ||
                        (poRows[i].lineItem[j].setUdfArray[k].udfType == "SMUDFDATE05" && ((poRows[i].lineItem[j].setUdfArray[k].isDisplayPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].isCompulsoryPI == "Y") && poRows[i].lineItem[j].setUdfArray[k].value == ""))
                    // if (poRows[i].lineItem[j].setUdfArray[k].isCompulsary == "Y" && poRows[i].lineItem[j].setUdfArray[k].value == "")
                    {


                        displayName += poRows[i].lineItem[j].setUdfArray[k].displayName != null ? poRows[i].lineItem[j].setUdfArray[k].displayName : poRows[i].lineItem[j].setUdfArray[k].udfType + ", "
                        // break
                    }
                }
            }
        }
        if (displayName != "") {
            this.setState({
                poErrorMsg: true,
                // errorMassage: displayName.substring(0, displayName.length - 2) + " is/are compulsory"
                errorMassage: "Please fill mandate fields in Set UDF First."
            })

        }
    }


    updatePoAmountNpoQuantity() {

        let poRows = [...this.state.poRows]
        let poQuantity = ""
        let poAmount = ""
        for (let j = 0; j < poRows.length; j++) {
            poAmount = Number(poAmount) + Number(poRows[j].amount)
            poQuantity = Number(poQuantity) + Number(poRows[j].quantity)
        }
        this.setState({
            poAmount: poAmount,
            poQuantity: poQuantity

        })

    }
    updateLineItemChange(data) {
        this.setState({
            lineItemChange: data
        })
    }
    getOtbForPoRows() {
        let poRows = [...this.state.poRows]
        let id = ""
        let otbb = ""
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                poRows[i].otb = poRows[i].lineItem[0].otb
            }
        }
        this.setState({
            poRows: poRows
        })
    }
    getOtbLastInDate(rowId, otb) {
        let poRows = [...this.state.poRows]
        let otbb = ""
        for (let i = 0; i < poRows.length; i++) {

            if (poRows[i].gridOneId == rowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (j == 0) {
                        poRows[i].lineItem[j].otb = otb
                        otbb = otb
                    }
                    if (j > 0) {
                        poRows[i].lineItem[j].otb = otbb - poRows[i].lineItem[j - 1].amount
                        otbb = poRows[i].lineItem[j - 1].amount
                    }
                }

            }

        }

        this.setState({
            changeLastIndate: false,
            poRows: poRows
        }, () => {

            this.getOtbForPoRows();
        })
    }
    updateMultipleOtb() {
        let poRows = [...this.state.poRows]
        let otbb = ""

        for (let i = 0; i < poRows.length; i++) {


            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                if (j == 0) {
                    poRows[i].lineItem[j].otb = poRows[i].otb
                    otbb = poRows[i].otb
                }
                if (j > 1) {
                    poRows[i].lineItem[j].otb = otbb - poRows[i].lineItem[j - 1].amount
                    otbb = poRows[i].lineItem[j - 1].amount
                }
            }

        }
        this.setState({
            changeLastIndate: false,
            poRows: poRows
        })



    }

    setBasedResest() {
        this.setState({
            setDepartment: "",
            hl3CodeDepartment: "",
            poSetVendor: "",
            orderSet: "",
        })
    }
    onClear() {
        this.props.piEditClear()
        this.props.piSaveDraftClear()
        this.props.getDraftClear()
        this.setState({
            indentNoToShow_pending: "",
            indentNoToShow_drafted: "",
            indentNo: "",
            loadIndenterr: false,
            poSetVendorerr: false,
            orderSeterr: false,
            vendorerr: false,
            transportererr: false,
            siteNameerr: false,
            cityerr: false,
            typeOfBuyingErr: false,

            isSet: this.state.storedSet,
            isUDFExist: this.state.storedSet ? this.state.storeUDFExist : "false",
            selectedRowId: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            hl3Code: "",
            hl4Name: "",
            hl4Code: "",
            basedOn: "",
            mrpStart: "",
            mrpEnd: "",
            mrpRange: "",
            leadDays: "",
            supplierCode: "",
            itemCodeList: [],
            city: "",

            saveMarginRule: "",

            slCode: "",
            loadIndentId: "",
            loadIndent: "",

            item: "",
            text: "",
            vendor: "",

            transporter: "",

            supplier: "",

            term: "",
            poQuantity: 0,
            poAmount: 0,



            siteName: "",
            siteCode: "",
            showSaveDraft: false,


            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: true
                },

                finalRate: "",
                rate: "",
                netRate: "",
                rsp: "",
                wsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],
                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",
                totalBasic: '',
                marginRule: "",
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                mrpRange: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",
                    image: [],
                    imageUrl: {},
                    imagePath: "",
                    containsImage: false,
                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: []

                }]

            }]
        })
        
    }


    updatePo() {
        let poRows = []
        //console.log(this.escChil)
        if (this.escChil != undefined) {
            console.log(this.escChil.state.poRows[0].lineItem)
            poRows = [...this.escChil.state.poRows]
        }
        else {
            poRows = [...this.state.poRows]
        }

        console.log('update.........')
        for (let k = 0; k < poRows.length; k++) {
            poRows[k].amount = ""
            poRows[k].quantity = ""
            poRows[k].calculatedMargin = []
            poRows[k].mrk = []
            poRows[k].tax = []
            poRows[k].gst = []
            poRows[k].finCharges = []
            poRows[k].totalBasic = ""

            // poRows[k].otb = "" // commenting because it is updating the otb

        }

        for (let i = 0; i < poRows.length; i++) {

            for (let j = 0; j < poRows[i].lineItem.length; j++) {


                poRows[i].amount = Math.round((Number(poRows[i].amount) + Number(poRows[i].lineItem[j].amount)) * 100) / 100 || ""

                poRows[i].quantity = Number(poRows[i].quantity) + Number(poRows[i].lineItem[j].quantity) || ""

                poRows[i].calculatedMargin = this.state.isRspRequired ? [...poRows[i].calculatedMargin, poRows[i].lineItem[j].calculatedMargin.toString()] : []

                poRows[i].mrk = this.state.isRspRequired ? [...poRows[i].mrk, poRows[i].lineItem[j].mrk.toString()] : []

                poRows[i].tax = [...poRows[i].tax, poRows[i].lineItem[j].tax.toString()] || ""

                poRows[i].gst = [...poRows[i].gst, poRows[i].lineItem[j].gst.toString()]

                poRows[i].finCharges = poRows[i].finCharges.concat(poRows[i].lineItem[j].finCharges)

                poRows[i].totalBasic = poRows[i].totalBasic == "undefined" ? "" : Number(poRows[i].totalBasic) + Number(poRows[i].lineItem[j].basic)
                //poRows[i].totalBasic = Number(poRows[i].basic) + Number(Number(poRows[i].rate) * Number(poRows[i].lineItem[j].setQty))

                // poRows[i].otb = poRows[i].lineItem[0].otb
            }

        }

        this.setState({
            poRows: poRows
        }, () => {
            this.updatePoAmountNpoQuantity()
            setTimeout(() => {

                this.gridFirst()
            }, 10)



        })


    }

    updateSelectedPo() {
        let poRows = [...this.state.poRows]

        if (this.state.selectedRowId != '') {
            let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.state.selectedRowId));
            poRows[mainIndex].amount = ""
            poRows[mainIndex].quantity = ""
            poRows[mainIndex].calculatedMargin = []
            poRows[mainIndex].mrk = []
            poRows[mainIndex].tax = []
            poRows[mainIndex].gst = []
            poRows[mainIndex].finCharges = []
            poRows[mainIndex].totalBasic = ""
            for (let j = 0; j < poRows[mainIndex].lineItem.length; j++) {
                poRows[mainIndex].amount = Math.round((Number(poRows[mainIndex].amount) + Number(poRows[mainIndex].lineItem[j].amount)) * 100) / 100 || ""

                poRows[mainIndex].quantity = Number(poRows[mainIndex].quantity) + Number(poRows[mainIndex].lineItem[j].quantity) || ""

                poRows[mainIndex].calculatedMargin = this.state.isRspRequired ? [...poRows[mainIndex].calculatedMargin, poRows[mainIndex].lineItem[j].calculatedMargin.toString()] : []

                poRows[mainIndex].mrk = this.state.isRspRequired ? [...poRows[mainIndex].mrk, poRows[mainIndex].lineItem[j].mrk.toString()] : []

                poRows[mainIndex].tax = [...poRows[mainIndex].tax, poRows[mainIndex].lineItem[j].tax.toString()] || ""

                poRows[mainIndex].gst = [...poRows[mainIndex].gst, poRows[mainIndex].lineItem[j].gst.toString()]

                poRows[mainIndex].finCharges = poRows[mainIndex].finCharges.concat(poRows[mainIndex].lineItem[j].finCharges)

                poRows[mainIndex].totalBasic = poRows[mainIndex].totalBasic == "undefined" ? "" : Number(poRows[mainIndex].totalBasic) + Number(poRows[mainIndex].lineItem[j].basic)
                //poRows[i].totalBasic = Number(poRows[i].basic) + Number(Number(poRows[i].rate) * Number(poRows[i].lineItem[j].setQty))

                // poRows[i].otb = poRows[i].lineItem[0].otb
            }
        }

        this.setState({
            poRows: poRows
        }, () => {
            this.updatePoAmountNpoQuantity()
            setTimeout(() => {

                this.gridFirst()
            }, 10)



        })


    }

    multiMarkUp(designId) {
        let poRows = [...this.state.poRows]

        let multiMarkUp = []
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == designId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].setQty != "") {
                        let payload = {
                            rate: poRows[i].finalRate,
                            rsp: this.state.isRSP ? poRows[i].rsp : poRows[i].vendorMrp,
                            designRowid: designId,
                            rowId: poRows[i].lineItem[j].gridTwoId,
                            gst: poRows[i].lineItem[j].gst
                        }
                        multiMarkUp.push(payload)

                    }

                }
            }
        }
        // this.props.getMultipleMarginRequest(multiMarkUp)


    }


    getOpenToBuy(id, otbb, lineItemValue) {
        let valuess = lineItemValue
        var amountTodeduct;
        if(valuess != undefined){
            valuess.map(el => {
                amountTodeduct  = el.netAmount
                id = Number(el.rowId) + 1
            })
           
        }

        let rows = [...this.state.poRows]
        let mrp = ""
        let count = 0
        let otb = []
        let netAmount = []
        let articleCode = ""
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].gridOneId == id) {

                mrp = rows[i].vendorMrp
                articleCode = rows[i].articleCode

            }
        }

        for (var i = 0; i < rows.length; i++) {
            if (rows[i].vendorMrp == mrp && rows[i].articleCode == articleCode) {
                count++
            }
        }

        if (count > 1 && rows.length > 1 || lineItemValue != undefined) {
            for (var i = 0; i < rows.length; i++) {
                if(amountTodeduct != undefined){
                    if(rows[i].gridOneId == id){
                        rows[i].amount = amountTodeduct
                    }
                }
                
                if (rows[i].vendorMrp == mrp && rows[i].articleCode == articleCode) {
                    if (rows[i].otb != null && rows[i].otb.toString() != "") {
                        otb.push(rows[i].otb)
                    }
                    if (rows[i].amount != "") {
                        netAmount.push(rows[i].amount)
                    }
                }
            }

            let minOtb = Math.min.apply(null, otb)


            let index = ""
            let netAmtFinal = ""
            for (var k = 0; k < otb.length; k++) {
                if (otb[k] == minOtb) {
                    index = k
                }
            }
            for (var l = 0; l < netAmount.length; l++) {
                netAmtFinal = netAmount[index]
            }


            for (var m = 1; m < rows.length; m++) {
                var indexx = m-1;
                if (rows[m].gridOneId == (id+1) || lineItemValue != undefined) {
                    if (rows[m].otb != undefined && rows[m].otb != "") {
                        if(lineItemValue == undefined){
                            rows[m].otb = rows[indexx].otb - rows[indexx].amount
                        }
                        else{
                            rows[m].otb = rows[indexx].otb - rows[indexx].amount
                        }
                    }
                }
            }

            this.setState({
                poRows: rows
            }, () => {

                // valuess == undefined ? this.setOtbAfterDelete(id) : null
            })

        }
        if (count == 1) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].vendorMrp == mrp && rows[i].articleCode == articleCode) {
                    if (otbb == undefined) {
                        rows[i].otb = ""
                    } else {
                        rows[i].otb = otbb
                    }
                }
            }
            this.setState({
                poRows: rows
            })
        }
        // this.getOtbForPoRows()

    }

    copyRow(rowId, otb, item) {
        const isDeliveryDate = this.state.isDeliveryDate ? item.deliveryDate == "" || (item.deliveryDate < this.state.minDate) : null;
        const flag = (item.vendorMrp == "" && this.state.isMrpRequired) || (item.vendorDesign == "" && !this.state.isVendorDesignNotReq) || item.rate == "" || (item.wsp == "" && this.state.isDisplayWSP && this.state.isMandateWSP) || item.marginRule == "" && this.state.isMarginRulePi || item.hsnSacCode == ""
            || (item.mrp == "" && this.state.isMRPEditable && this.state.mrpValidation) || isDeliveryDate ? true : false
        if (flag) {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Please fill the basic details first",
                // errorMassage: this.state.isDeliveryDate ? "Please fill mandate fields first and correct delivery date for this row." : "Please fill mandate fields first for this row."
            })
        } else {
            let poRows = [...this.state.poRows]
            let rowids = []
            let finalId = ""
            let obj = {}
            poRows.forEach(po => {
                rowids.push(po.gridOneId)

            })
            finalId = Math.max(...rowids);
            poRows.forEach(po => {
                if (po.gridOneId == rowId) {
                    let lineItem = [...po.lineItem];
                        let newLineItem = [];
                        lineItem.map(item => {newLineItem.push({...item})})
                    obj = {
                        totalBasic: po.totalBasic,
                        vendorMrp: po.vendorMrp,
                        vendorDesign: po.vendorDesign,
                        mrk: po.mrk,
                        discount: po.discount,
                        finalRate: po.finalRate,
                        rate: po.rate,
                        wsp: po.wsp,
                        netRate: po.netRate,
                        rsp: po.rsp,
                        mrp: po.mrp,
                        quantity: po.quantity,
                        amount: po.amount,
                        otb: isNaN(po.otb) ? "" : po.otb == undefined ? "" : po.otb,
                        remarks: po.remarks,
                        gst: po.gst,
                        finCharges: po.finCharges,
                        //basic: po.basic,  //unknown basic
                        tax: po.tax,
                        calculatedMargin: po.calculatedMargin,
                        gridOneId: finalId + 1,
                        deliveryDate: po.deliveryDate,

                        marginRule: po.marginRule,
                        //new
                        articleCode: po.articleCode,
                        articleName: po.articleName,
                        departmentCode: po.departmentCode,
                        departmentName: po.departmentName,
                        sectionCode: po.sectionCode,
                        sectionName: po.sectionName,
                        divisionCode: po.divisionCode,
                        divisionName: po.divisionName,
                        itemCodeList: po.itemCodeList,
                        itemCodeSearch: po.itemCodeSearch,
                        hsnCode: po.hsnCode,
                        hsnSacCode: po.hsnSacCode,
                        mrpStart: po.mrpStart,
                        mrpEnd: po.mrpEnd,
                        mrpRange: po.mrpStart + ' - ' + po.mrpEnd,
                        catDescHeader: po.catDescHeader,
                        catDescArray: po.catDescArray,
                        itemUdfHeader: po.itemUdfHeader,
                        itemUdfArray: po.itemUdfArray,
                        lineItem: newLineItem

                    }
                }
            })
            poRows.push(obj)
            this.setState({
                poRows,
                selectedRowId: rowId
            }, () => {
                this.getOpenToBuy(rowId, otb)
                this.updatePoAmountNpoQuantity()
            })

        }

    }
    handleRemoveSpecificRow = (idx, otb, mrp) => {
        const rows = [...this.state.poRows]
        if (rows.length > 1) {
            this.setState({
                resetAndDelete: true,
                headerMsg: "Are you sure you want to delete row?",
                paraMsg: "Click confirm to continue.",
                rowDelete: "row Delete",
                rowsId: idx,
                otbValueData: otb,
                mrpValueData: mrp,
                selectedRowId: idx
            })

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })

        }

    }

    DeleteRowPo(idx, otb, mrp) {

        var rows = [...this.state.poRows]
        if (rows.length > 1) {
            for (var z = 0; z < rows.length; z++) {
                if (rows[z].gridOneId == idx) {
                    rows.splice(z, 1)
                }
            }

            this.setState({
                poRows: rows,
                deleteMrp: mrp,
                deleteOtb: otb,

            }, () => {

                this.setOtbAfterDelete(idx)
                setTimeout(() => {
                    this.updatePo()
                    this.updatePoAmount()
                    this.updatePoAmountNpoQuantity()
                }, 10)
            })

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Single row can't be deleted"
            })
        }
        // document.getElementById("addIndentRow").disabled = false;
    }
    //reset function
    reset = () => {
        this.setState({
            resetAndDelete: true,
            headerMsg: "Are you sure to reset the form?",
            paraMsg: "Click confirm to continue.",
            rowDelete: ""
        })
    }
    closeResetDeleteModal() {
        this.setState({
            resetAndDelete: !this.state.resetAndDelete,
        })
    }
    resetRows() {

        this.setState({
            codeRadio: "Adhoc"
        })

        this.onClear();
        this.setBasedResest();
    }
    closeDeleteConfirmModal(e) {
        this.setState({
            deleteConfirmModal: !this.state.deleteConfirmModal,
        })

    }
    deleteLineItem(idxx, setNo) {
        let rows = [...this.state.poRows]
        rows.forEach(ro => {
            if (ro.gridOneId == this.state.selectedRowId) {
                if (ro.lineItem.length > 1) {
                    this.setState({
                        deleteConfirmModal: true,
                        deleteGridId: idxx,
                        deleteSetNo: setNo,
                        headerMsg: "Are you sure to delete the row?",
                        paraMsg: "Click confirm to continue.",

                    })
                }
                else {
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Single row can't be deleted"
                    })
                }


            }
        })

    }
    handleRemoveSpecificSecRow(idxx, setNo) {
        let rows = [...this.state.poRows]
        let poRows = [...this.state.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.state.selectedRowId));
        let index = poRows[mainIndex].lineItem.findIndex((obj => obj.gridTwoId == this.state.deleteGridId));
        poRows[mainIndex].lineItem.splice(index, 1)
        // same as above 5 lines code
        // rows.forEach(ro =>   {
        //     if (ro.gridOneId == this.state.selectedRowId) {
        //         ro.lineItem.forEach(li => {
        //             if (li.gridTwoId == this.state.deleteGridId) {
        //                 ro.lineItem.splice(idxx - 1, 1)
        //             }
        //         })

        //     }
        // })
        rows.forEach(ro => {
            let number = 1
            ro.lineItem.forEach(li => {
                li.setNo = number++

            })


        })
        this.setState({
            poRows: rows
        }, () => {
            this.updateMultipleOtb()
            setTimeout(() => {

                this.updatePo()
                this.getOpenToBuy(this.state.selectedRowId)
            }, 10)


        })

    }
    setOtbAfterDelete(idx) {
        let poRows = [...this.state.poRows]
        let otbb = ""
        let id = ""
        for (let i = 0; i < poRows.length; i++) {
            id = poRows[0].gridOneId
            if (poRows[i].gridOneId == id) {
                poRows[i].otb = poRows[i].otb
                otbb = poRows[i].otb

            }
            if (poRows[i].gridOneId > id) {
                if (poRows[i].otb != undefined && poRows[i].otb != "") {
                    poRows[i].otb = otbb - poRows[i - 1].amount
                    otbb = otbb - poRows[i - 1].amount
                }

            }

        }

        this.setState({
            poRows: poRows,
            deleteMrp: "",
            deleteOtb: ""

        })



    }

    addRow = () => {
        let rowids = []
        let poRows = [...this.state.poRows]
        let netAmount = poRows[poRows.length - 1].amount
        if (netAmount != "") {

            poRows.forEach(po => {
                rowids.push(po.gridOneId)

            })
            let finalId = Math.max(...rowids);
            let poRowsObj = {

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: true
                },

                finalRate: 0,
                rate: "",
                netRate: "",
                rsp: "",
                wsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: finalId + 1,
                deliveryDate: "",

                marginRule: "",
                // image: [],
                // imageUrl: {},
                // containsImage: false,
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorSearch: "",
                    colorList: [],
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: this.state.isSet ? "" : 1,
                    quantity: "",
                    amount: "",
                    rate: "",
                    image: [],
                    imageUrl: {},
                    containsImage: false,
                    imagePath: "",
                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: [],
                    lineItemChk: false

                }]

            }
            this.setState({
                poRows: [...this.state.poRows, poRowsObj]
            })
        } else {
            this.setState({
                errorMassage: "Amount is complusory",
                poErrorMsg: true

            })
        }

    }
    copingFocus(data) {
        let poRows = [...this.state.poRows]
        this.setState({
            copingLineItem: [...poRows]
        })
        let focusedObj = this.state.focusedObj
        focusedObj.value = ""
        focusedObj.finalRate = ""
        focusedObj.type = "copying color"
        focusedObj.rowId = ""
        focusedObj.cname = ""
        focusedObj.radio = this.state.codeRadio
        focusedObj.colorObj = {
            color: [],
            colorList: []
        }

        this.setState({
            focusedObj
        }, () => {
            this.copingColor(data.rowId, data.id, data.color, data.icolorList, data.option, data.total, data.search, data.type)

        })

    }
    copingColor(rowId, idd, color, colorList, option, total, search, type) {
        let poRows = [...this.state.poRows]

        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == this.state.selectedRowId) {
                for (let j = 0; j < poRows[i].lineItem.length; j++) {
                    if (poRows[i].lineItem[j].gridTwoId == rowId) {
                        poRows[i].lineItem[j].colorChk = true
                    } else {
                        let sum = 0

                        for (let n = 0; n < poRows[i].lineItem[j].ratio.length; n++) {

                            sum += Number(poRows[i].lineItem[j].ratio[n]);
                        }

                        if (poRows[i].lineItem[j].sizeType == type) {
                            poRows[i].lineItem[j].colorChk = false
                            poRows[i].lineItem[j].color = color
                            poRows[i].lineItem[j].colorSearch = search
                            poRows[i].lineItem[j].colorList = colorList
                            poRows[i].lineItem[j].option = option
                            poRows[i].lineItem[j].total = Number(sum) * Number(option)
                            poRows[i].lineItem[j].quantity = type == 'simple' ? Number(poRows[i].lineItem[j].ppQty) * Number(poRows[i].lineItem[j].setQty) : Number(sum) * Number(option) * Number(poRows[i].lineItem[j].setQty)
                        }
                    }

                }
            }
        }

        this.setState({

            poRows: poRows
        }, () => {




            let r = [...this.state.poRows]
            let calculatedMarginValue = ""
            let secTwoRows = this.state.secTwoRows
            for (let k = 0; k < r.length; k++) {
                if (r[k].gridOneId == idd && type == 'complex') {
                    if (r[k].hsnSacCode != "" || r[k].hsnSacCode != null) {

                        if ((r[k].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {


                            let lineItemArray = []
                            for (let i = 0; i < r[k].lineItem.length; i++) {

                                if (r[k].gridOneId == idd && r[k].lineItem[i].setQty != "" && r[k].finalRate != "" && r[k].finalRate != 0) {

                                    let taxData = {
                                        hsnSacCode: r[k].hsnSacCode,
                                        qty: r[k].lineItem[i].sizeType == 'simple' ? Math.round(Number(r[k].lineItem[i].ratio) / 6) * 6 : Number(r[k].lineItem[i].setQty) * Number(r[k].lineItem[i].total),
                                        rate: r[k].finalRate,
                                        rowId: r[k].lineItem[i].gridTwoId,
                                        designRowid: Number(idd),
                                        basic: r[k].lineItem[i].sizeType == 'simple' ? (r[k].finalRate) * Math.round(Number(r[k].lineItem[i].ratio) / 6) * 6 : Number(r[k].lineItem[i].setQty) * Number(r[k].lineItem[i].total) * Number(r[k].finalRate), //basic lineitem tax data
                                        clientGstIn: sessionStorage.getItem('gstin'),
                                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                        supplierGstinStateCode: this.state.stateCode,
                                        purtermMainCode: this.state.termCode,
                                        siteCode: this.state.siteCode
                                    }

                                    lineItemArray.push(taxData)
                                }
                            }
                            if (lineItemArray.length != 0) {
                                this.setState({
                                    lineItemChange: true
                                }, () => {
                                    this.props.multipleLineItemRequest(lineItemArray)
                                }
                                )
                            }

                        }
                    }
                    else {

                        this.setState({
                            errorMassage: "HSN code is complusory",
                            poErrorMsg: true

                        })
                    }
                }
            }



        })
    }


    debounceFun() {
        this.idleTimer.pause();
        if (!this.state.loader) {
            this.setState({
                loader: true,
                saveDraftFlag: false
            }, () => {
                this.contactSubmit();
            }
            )
        }

    }
    otbNegative() {
        let flag = false
        let datevalue = false
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].amount > poRows[i].otb) {
                flag = true
                break
            }
        }


        if (this.state.isOtbValidationPi == "true") {
            if (flag) {
                this.setState({
                    loader: false
                })
                this.setState({
                    otbStatus: true,
                    errorMassage: "Otb is less than Net Amount",
                    poErrorMsg: true
                })

            } else {

                this.setState({
                    otbStatus: false
                })

            }
        } else {


            this.setState({
                otbStatus: false
            })
        }
    }
    lineItemChkFun = () => {

        let poRows = this.state.poRows
        let lineError = []
        for (let i = 0; i < poRows.length; i++) {
            for (let j = 0; j < poRows[i].lineItem.length; j++) {
                if (poRows[i].lineItem[j].lineItemChk == true) {

                    let payload = {
                        "sNo": poRows[i].lineItem[j].gridTwoId,
                        "message": "LineItem charges is wrongly calculated .Please Rentered the value"

                    }
                    lineError.push(payload)
                }
            }
        }
        return lineError
    }
    lineItemArray(lineItem, rate, finalRate) {
        let lineItemArray = []

        lineItem.forEach(li => {
            let ratio = []
            let ratioIndex = 1
            let itemObj = {}
            let sumOfRatio = 0


            let sr = li.ratio
            for (let k = 0; k < sr.length; k++) {

                let ra = {
                    id: ratioIndex++,
                    ratio: sr[k]
                }
                sumOfRatio += Number(sr[k])
                ratio.push(ra)
            }
            let setUdfArray = li.setUdfArray
            if (setUdfArray.length != 0) {
                setUdfArray.forEach(uArr => {

                    if(uArr.displayName == "Simple / Complex"){
                        itemObj[uArr.udfType] = li.sizeType == "complex" ? "C" :"S"
                    }
                    else{
                        itemObj[uArr.udfType] = uArr.value
                    }

                })
            }
            let lineObj = {
                itemId: li.itemBarcode,
                setHeaderId: li.setHeaderId,
                sizes: li.sizeList,
                ratios: ratio,
                colors: (!this.state.isColorRequired) ? [emptyColorList] : li.colorList,
                setUdfArray: setUdfArray.length != 0 ? itemObj : {},
                qty: li.quantity,
                sumOfRatio: sumOfRatio,
                noOfSets: li.setQty,
                amount: li.amount,
                gst: li.gst,
                tax: li.tax,
                otb: li.otb,
                finCharge: li.finCharges ? li.finCharges : li.finCharge ? li.finCharge : [],
                calculatedMargin: li.calculatedMargin,
                intakeMargin: li.mrk,
                setNo: li.setNo,
                basic: this.state.isDiscountAvail ? Number(li.quantity) * Number(finalRate) : Number(li.quantity) * Number(rate),  //lineitem basic
                imageUrl: li.imageUrl ? li.imageUrl : {},
                images: li.image ? li.image : [],
                containsImage: li.containsImage,
                imagePath: li.imagePath,
                filePath: li.imagePath,
                sizeType: li.sizeType,
                ppQty: li.sizeType == "complex" && sumOfRatio != "undefined" ? sumOfRatio : this.state.simpleData,
            }
            //basic: this.state.isDiscountAvail ? (li.quantity * finalRate).toFixed(2) : (li.quantity * rate).toFixed(2),  //lineitem basic
            lineItemArray.push(lineObj)
        })
        return lineItemArray

    }
    contactSubmit() {


        this.supplier();

        this.transporter();
        this.poUdf1();
        // this.poUdf2();
        this.poUdf3();
        this.poUdf4();
        this.poUdf5();
        this.typeOfBuying();

        { this.state.isCityExist == true ? this.city() : null }
        { this.state.isCheckMarginRuleValidation && this.state.isRspRequired && this.state.isDisplayMarginRule && this.calculateMarginValidation() }
        var lineError = this.lineItemChkFun()

        this.otbNegative()
        this.gridFirst()
        { this.state.isSiteExist == "true" ? this.site() : null }
        { this.state.isUDFExist == "true" ? this.gridFivth() : null }
        this.gridSecond()
        { this.state.itemUdfExist == "true" ? this.gridThird() : null }
        this.gridFourth()
        const t = this;
        setTimeout(function (e) {
            const { cityerr, typeOfBuyingErr, vendorerr, transportererr, otbStatus, lineItemChange, poUdf1err, poUdf2err, poUdf3err, poUdf4err, poUdf5err, calculatedMarginErr } = t.state;
            if (!cityerr && !vendorerr && !transportererr && !typeOfBuyingErr && !poUdf1err && !poUdf2err && !poUdf3err && !poUdf4err && !poUdf5err && !calculatedMarginErr) {
                // const { poValidFromerr, lastInDateerr, vendorerr, articleerr, transportererr, otbStatus, } = t.state;
                if (!otbStatus) {
                    //console.log("t.state.gridFirst", t.state.gridFirst)
                    if (t.state.gridFirst) {
                        if ((t.state.gridSecond && t.state.isCatDescExist) || !t.state.isCatDescExist) {

                            if ((t.state.gridThird && t.state.itemUdfExist == "true") || t.state.itemUdfExist == "false") {
                                if (t.state.gridFourth) {
                                    if ((t.state.gridFivth && t.state.isUDFExist == "true") || t.state.isUDFExist == "false") {
                                        if (!lineItemChange) {

                                            if (lineError.length == 0) {
                                                let lineItem = []
                                                let poRows = [...t.state.poRows]
                                                poRows.forEach(po => {
                                                    let { cat1, cat2, cat3, cat4, cat5, cat6, desc1, desc2, desc3, desc4, desc5, desc6, catRemark } = ""
                                                    let itemObj = {}
                                                    if (po.catDescArray.length != 0) {
                                                        po.catDescArray.forEach(cdArr => {
                                                            cat1 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT1") {
                                                                    return item
                                                                }
                                                            })
                                                            cat2 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT2") {
                                                                    return item
                                                                }
                                                            })
                                                            cat3 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT3") {
                                                                    return item
                                                                }
                                                            })
                                                            cat4 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT4") {
                                                                    return item
                                                                }
                                                            })
                                                            cat5 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT5") {
                                                                    return item
                                                                }
                                                            })
                                                            cat6 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CAT6") {
                                                                    return item
                                                                }
                                                            })
                                                            desc1 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC1") {
                                                                    return item
                                                                }
                                                            })
                                                            desc2 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC2") {
                                                                    return item
                                                                }
                                                            })
                                                            desc3 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC3") {
                                                                    return item
                                                                }
                                                            })
                                                            desc4 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC4") {
                                                                    return item
                                                                }
                                                            })
                                                            desc5 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC5") {
                                                                    return item
                                                                }
                                                            })
                                                            desc6 = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "DESC6") {
                                                                    return item
                                                                }
                                                            catRemark = cdArr.catDesc.find(item => {
                                                                if (item.catdesc == "CATREMARK") {
                                                                    return item
                                                                }
                                                            })

                                                            })
                                                        })

                                                    }
                                                    if (po.itemUdfArray.length != 0) {
                                                        po.itemUdfArray.forEach(uArr => {
                                                            uArr.itemUdf.forEach(iu => {

                                                                itemObj[iu.cat_desc_udf] = iu.value

                                                            })
                                                        })

                                                    }
                                                    let remark = catRemark != undefined ? catRemark.value : '';
                                                    let lineItemArray = t.lineItemArray(po.lineItem, po.rate, po.finalRate)
                                                    let poDetails = {
                                                        design: po.vendorDesign,
                                                        designWiseRowId: po.gridOneId,
                                                        hsnSacCode: po.hsnSacCode,
                                                        hsnCode: po.hsnCode,
                                                        hl1Name: po.divisionName,
                                                        hl1Code: po.divisionCode,
                                                        hl2Name: po.sectionName,
                                                        hl2Code: po.sectionCode,
                                                        hl3Name: po.departmentName,
                                                        hl3Code: po.departmentCode,
                                                        hl4Name: po.articleName,
                                                        hl4Code: po.articleCode,
                                                        hl4CodeSeq: po.articleCode + "_" + po.gridOneId,
                                                        rate: po.rate,
                                                        finalRate: po.finalRate,
                                                        discountType: po.discount.discountPer ? po.discount.discountType + "%" : po.discount.discountType,
                                                        discountValue: po.discount.discountValue,
                                                        editableMrp: po.mrp,
                                                        rsp: t.state.isRspRequired ? po.rsp : po.mrp,
                                                        wsp: po.wsp,
                                                        mrp: t.state.isMrpRequired ? po.vendorMrp : po.mrp,
                                                        mrpStart: po.mrpStart,
                                                        mrpEnd: po.mrpEnd,
                                                        // images: po.image,
                                                        marginRule: po.marginRule,
                                                        designWiseTotalQty: po.quantity,
                                                        designWiseNetAmountTotal: po.amount,
                                                        otb: po.otb,
                                                        designWiseTotalOtb: po.otb,  // added for
                                                        totalBasic: t.calculateTotalBasic(lineItemArray),
                                                        designWiseTotalGST: po.gst,
                                                        designWiseTotalFinCharges: po.finCharges.length == 0 ? null : po.finCharges.length > 0 ? po.finCharges : po.finCharge.length > 0 ? po.finCharge : [],
                                                        remarks: po.remarks,
                                                        designWiseTotalTax: po.tax,
                                                        deliveryDate: po.deliveryDate + "T00:00:00+05:30",
                                                        // containsImage: po.containsImage,
                                                        designWiseTotalCalculatedMargin: po.calculatedMargin != "" || po.calculatedMargin != [] ? po.calculatedMargin : [],
                                                        designWiseTotalIntakeMargin: po.mrk,
                                                        headers: {
                                                            catDescHeader: po.catDescHeader,
                                                            itemUdfHeader: po.itemUdfHeader,
                                                            setUdfHeader: po.lineItem[0].setUdfHeader

                                                        },
                                                        catDescArray: po.catDescArray.length == 0 ? {} : {
                                                            cat1Code: cat1.code,
                                                            cat1Name: cat1.value,
                                                            cat2Code: cat2.code,
                                                            cat2Name: cat2.value,
                                                            cat3Code: cat3.code,
                                                            cat3Name: cat3.value,
                                                            cat4Code: cat4.code,
                                                            cat4Name: cat4.value,
                                                            cat5Code: cat5.code,
                                                            cat5Name: cat5.value,
                                                            cat6Code: cat6.code,
                                                            cat6Name: cat6.value,
                                                            desc1Code: desc1.code,
                                                            desc1Name: desc1.value,
                                                            desc2Code: desc2.code,
                                                            desc2Name: desc2.value,
                                                            desc3Code: desc3.code,
                                                            desc3Name: desc3.value,
                                                            desc4Code: desc4.code,
                                                            desc4Name: desc4.value,
                                                            desc5Code: desc5.code,
                                                            desc5Name: desc5.value,
                                                            desc6Code: desc6.code,
                                                            desc6Name: desc6.value,
                                                            catRemark: remark,
                                                        },
                                                        itemUdfArray: po.itemUdfArray.length == 0 ? {} : itemObj,
                                                        lineItem: lineItemArray
                                                    }
                                                    lineItem.push(poDetails)
                                                })

                                                console.log('pi', lineItem)
                                                let finalSubmitData = {
                                                    isSet: t.state.isSet,
                                                    typeOfBuying: t.state.typeOfBuying,

                                                    slCode: t.state.slCode,
                                                    slName: t.state.slName,
                                                    slCityName: t.state.isCityExist == true ? t.state.city : t.state.slCityName,
                                                    slAddr: t.state.slAddr,
                                                    leadTime: t.state.leadDays,
                                                    termCode: t.state.termCode,
                                                    termName: t.state.termName,
                                                    transporterCode: t.state.transporterCode,
                                                    transporterName: t.state.transporterName,
                                                    poudf1: t.state.poUdf1,
                                                    poudf2: t.state.poUdf2 == "yyyy-mm-dd" ? t.state.lastInDate : t.state.poUdf2,
                                                    poudf3: t.state.poUdf3,
                                                    poudf4: t.state.poUdf4,
                                                    poudf5: t.state.poUdf5,
                                                    poQuantity: t.state.poQuantity,
                                                    poAmount: t.state.poAmount,
                                                    stateCode: t.state.stateCode,
                                                    itemUdfExist: t.state.itemUdfExist,
                                                    isUDFExist: t.state.isUDFExist,
                                                    enterpriseGstin: sessionStorage.getItem("gstin"),
                                                    piDetails: lineItem,
                                                    siteCode: t.state.siteCode,
                                                    siteName: t.state.siteName,
                                                    indentNo: t.state.copyPi ? "" : t.state.indentNo,
                                                    validFrom: t.state.poValidFrom,
                                                    validTo: t.state.lastInDate,
                                                }
                                                if (t.state.isEditPi && !t.state.isCopyIndent) {
                                                    t.setState({
                                                        isContactCalled : true,
                                                    })
                                                    finalSubmitData.indentId = t.state.indentId;
                                                    if(t.state.isDraftRequest) {
                                                        t.idleTimer.pause();
                                                        t.props.editedPiSaveRequest(finalSubmitData);
                                                    } else if(!t.state.isDraftRequest) {
                                                        t.setState({
                                                            loader: true,
                                                        })
                                                    }
                                                } else {
                                                    t.setState({
                                                        isContactCalled : true
                                                    })
                                                    //console.log('finalSubmit', finalSubmitData)
                                                    if(t.state.isDraftRequest){
                                                        t.idleTimer.pause();
                                                        t.props.gen_PurchaseIndentRequest(finalSubmitData) //Create API
                                                    } else if(!t.state.isDraftRequest) {
                                                        t.setState({
                                                            loader: true,
                                                        })
                                                    }
                                                    // if (vendorGstinValidation == true && gstNumber == null) {
                                                    //     alert("Without GSTin Number PI can't be saved");
                                                    // }
                                                    // else if (vendorGstinValidation == false) {
                                                    //     t.props.gen_PurchaseIndentRequest(finalSubmitData)
                                                    // }
                                                }



                                            } else {
                                                t.setState({
                                                    loader: false,
                                                    multipleErrorpo: true,
                                                    lineError: lineError
                                                })


                                            }
                                        } else {

                                            t.setState({
                                                loader: false
                                            })
                                            t.setState({
                                                toastMsg: "Wait a moment untill charges calculated!!",
                                                toastLoader: true
                                            })

                                            setTimeout(function () {
                                                t.setState({
                                                    toastLoader: false
                                                })
                                            }, 1000)
                                        }
                                    }
                                    else {
                                        t.setState({
                                            loader: false
                                        })
                                        t.validateSetUdf();

                                    }
                                } else {
                                    t.setState({
                                        loader: false
                                    })
                                    t.validateLineItem();


                                }

                            } else {
                                console.log('validate')
                                t.setState({
                                    loader: false
                                })
                                t.validateUdf();

                            }

                        } else {
                            t.setState({
                                loader: false
                            })
                            t.validateCatDescRow();

                        }
                    } else {
                        t.setState({
                            loader: false
                        })

                        t.validateItemdesc();
                    }
                } else {
                    t.setState({
                        loader: false
                    })
                }
            } else {
                t.setState({
                    loader: false
                })
            }
        }, 1000)

    }
    
    catDescValue(catdesc, data) {
        console.log('catdesc',catdesc, data)
        if (catdesc.length != 0) {
            for (let i = 0; i < catdesc.length; i++) {
                if (catdesc[i].catdesc == "CAT1") {
                    catdesc[i].code = data.cat1Code
                    catdesc[i].value = data.cat1Name

                }
                if (catdesc[i].catdesc == "CAT2") {
                    catdesc[i].code = data.cat2Code
                    catdesc[i].value = data.cat2Name

                }
                if (catdesc[i].catdesc == "CAT3") {
                    catdesc[i].code = data.cat3Code
                    catdesc[i].value = data.cat3Name

                }
                if (catdesc[i].catdesc == "CAT4") {
                    catdesc[i].code = data.cat4Code
                    catdesc[i].value = data.cat4Name

                }
                if (catdesc[i].catdesc == "CAT5") {
                    catdesc[i].code = data.cat5Code
                    catdesc[i].value = data.cat5Name

                }
                if (catdesc[i].catdesc == "CAT6") {
                    catdesc[i].code = data.cat6Code
                    catdesc[i].value = data.cat6Name

                }
                if (catdesc[i].catdesc == "DESC1") {
                    catdesc[i].code = data.desc1Code
                    catdesc[i].value = data.desc1Name

                }
                if (catdesc[i].catdesc == "DESC2") {
                    catdesc[i].code = data.desc2Code
                    catdesc[i].value = data.desc2Name

                }
                if (catdesc[i].catdesc == "DESC3") {
                    catdesc[i].code = data.desc3Code
                    catdesc[i].value = data.desc3Name

                }
                if (catdesc[i].catdesc == "DESC4") {
                    catdesc[i].code = data.desc4Code
                    catdesc[i].value = data.desc4Name

                }
                if (catdesc[i].catdesc == "DESC5") {
                    catdesc[i].code = data.desc5Code
                    catdesc[i].value = data.desc5Name

                }
                if (catdesc[i].catdesc == "DESC6") {
                    catdesc[i].code = data.desc6Code
                    catdesc[i].value = data.desc6Name
                }
            }
            console.log('print', this.state.isDisplayCatRemark)
            if(this.state.isDisplayCatRemark){
                
                catdesc = [...catdesc, {
                    catdesc: "CATREMARK",
                    value: data.catRemark,
                }]
            }
        }
        if (catdesc.length != 0) {
            catdesc = [{ catDescId: 1, catDesc: catdesc }]
            console.log('catdesc1', catdesc)
        }
        return catdesc


    }
    itemUdfValue(itemUdf, data) {
        if (itemUdf.length != 0 && itemUdf.forEach) {
            itemUdf.forEach(itemUdfDetails => {
                Object.keys(data).forEach(keys => {
                    if (itemUdfDetails.cat_desc_udf == keys) {
                        itemUdfDetails.value = data[keys]
                    }
                })


            });
        }
        if (itemUdf.length != 0) {
            itemUdf = [{ itemUdfId: 1, itemUdf: itemUdf }]
        }
        return itemUdf

    }
    setUdfValue(setUdf, data) {
        let setUdff = [];
        if (setUdf.length != 0) {
            setUdf.forEach(setUdfDetails => {
                let setUdfDtl = {...setUdfDetails};
                Object.keys(data).forEach(keys => {
                    if (setUdfDtl.udfType == keys) {
                        setUdfDtl.value = data[keys]
                    }
                })
                setUdff.push(setUdfDtl)

            });
        }

        return setUdff

    }
    lineItemValue(data, lineItem) {
        let li_Array = []
        let id = 1;
        let setno = 1
        lineItem.forEach(li => {
            let color = [];
            let size = [];
            let ratio = []
            li.colors.forEach(co => {
                color.push(co.cname)
            });
            // li.sizes.forEach(si => {
            //     size.push(si.cname)
            // })
            if (Array.isArray(li.sizes)) {
                Object.keys(li.sizes).map(si => {
                    if (li.sizes[si] != undefined)
                        size.push(li.sizes[si].cname)
                    else
                        size.push(li.sizes.si)
                })
            } else {
                size.push(li.sizes.cname)
            }
            li.ratios.forEach(si => {
                ratio.push(si.ratio)
            })

            let obj = {
                colorChk: false,
                icodeChk: false,
                colorSizeList: [],
                icodes: [],
                itemBarcode: "",
                setHeaderId: li.setHeaderId,
                gridTwoId: id++,
                color: color,
                colorSearch: li.colorSearch,
                colorList: li.colors,
                sizes: size,
                sizeSearch: li.sizeSearch,
                sizeList: li.sizes,
                ratio: ratio,
                size: "",
                setRatio: "",
                option: li.colors.length,
                setNo: setno++,
                total: li.sumOfRatio * li.colors.length,
                setQty: li.noOfSets,
                quantity: li.qty,
                amount: li.amount,
                basic: (li.basic), //line item
                gst: li.gst,
                finCharges: li.finCharge,
                tax: li.tax,
                otb: li.otb,
                calculatedMargin: li.calculatedMargin,
                mrk: li.intakeMargin,
                setUdfHeader: data,
                setUdfArray: this.setUdfValue(data, li.setUdfArray),
                lineItemChk: false,
                imageUrl: li.imageUrl ? li.imageUrl : {},
                images: li.image ? li.image : [],
                containsImage: li.containsImage ? li.containsImage : false,
                imagePath: li.imagePath,
                filePath: li.imagePath,
                sizeType: li.sizeType,
                ppQty: li.sizeType == "complex" && li.sumOfRatio != "undefined" ? li.sumOfRatio : this.state.simpleData,

            }
            //console.log(obj)
            li_Array.push(obj)

        })
        return li_Array
    }
    updatePoData(data) {
        console.log('copyIndent', data)
        let poRows = []
        let id = 1;

        let designArray = []


        let imageUrl = {};
        // let piDetails = data.piDetails != undefined ? data.piDetails : data.poDetails
        let piDetails = data.piDetails != undefined ? data.piDetails : data.draft.piDetails
        piDetails.length > 0 ? piDetails.forEach(pi => {
            console.log(pi)


            //
            // pi.images.forEach(img => {
            //     if (img != null && img != undefined) {
            //         imageUrl[img.split('/')[9].split('?')[0]] = img
            //     }
            // })

            let piData = {

                setHeaderId: "",

                vendorMrp: pi.mrp,
                vendorDesign: pi.design,
                mrk: pi.designWiseTotalIntakeMargin,
                discount: {
                    discountType: pi.discountType.split("%")[0],
                    discountValue: pi.discountValue,
                    discountPer: pi.discountType.includes("%") ? true : false
                },
                rate: pi.rate,
                finalRate: pi.finalRate,
                netRate: pi.rate,

                // basic: pi.designWiseTotalBasic, //
                rsp: pi.rsp,
                wsp: pi.wsp == undefined ? "" : pi.wsp,
                mrp: pi.mrp ? pi.mrp : pi.editableMrp,
                quantity: pi.designWiseTotalQty,
                amount: pi.designWiseNetAmountTotal,
                remarks: pi.remarks != null ? pi.remarks : "",
                otb: pi.designWiseTotalOtb != undefined ? pi.designWiseTotalOtb : pi.otb != undefined ? pi.otb : "",
                gst: pi.designWiseTotalGST,
                finCharges: pi.designWiseTotalFinCharges,
                finCharge: pi.designWiseTotalFinCharges,
                tax: pi.designWiseTotalTax,
                calculatedMargin: pi.designWiseTotalCalculatedMargin,
                gridOneId: id++,
                deliveryDate: pi.deliveryDate ? moment(pi.deliveryDate.substring(0, 10)).format("YYYY-MM-DD") : this.state.lastInDate,

                marginRule: pi.marginRule,
                // image: Object.keys(imageUrl),

                // imageUrl: imageUrl,
                // containsImage: pi.containsImage,
                articleCode: pi.hl4Code != null ? pi.hl4Code : "",
                articleName: pi.hl4Name != null ? pi.hl4Name : "",
                departmentCode: pi.hl3Code != null ? pi.hl3Code : "",
                departmentName: pi.hl3Name != null ? pi.hl3Name : "",
                sectionCode: pi.hl2Code != null ? pi.hl2Code : "",
                sectionName: pi.hl2Name != null ? pi.hl2Name : "",
                divisionCode: pi.hl1Code != null ? pi.hl1Code : "",
                divisionName: pi.hl1Name != null ? pi.hl1Name : "",
                itemCodeList: [],
                itemCodeSearch: "",
                hsnCode: pi.hsnCode != null ? pi.hsnCode : "",
                hsnSacCode: pi.hsnSacCode != null ? pi.hsnSacCode : "",
                mrpStart: pi.mrpStart != null ? pi.mrpStart : pi.mrpStart != undefined ? pi.mrpStart : "",
                mrpRange: pi.mrpStart + ' - ' + pi.mrpEnd,
                mrpEnd: pi.mrpEnd != null ? pi.mrpEnd : pi.mrpEnd != undefined ? pi.mrpEnd : "",
                catDescHeader: pi.headers.catDescHeader,
                catDescArray: this.catDescValue(pi.headers.catDescHeader, pi.catDescArray),
                itemUdfHeader: pi.headers.itemUdfHeader,
                itemUdfArray: this.itemUdfValue(pi.headers.itemUdfHeader, pi.itemUdfArray),
                lineItem: this.lineItemValue(pi.headers.setUdfHeader, pi.lineItem),
                totalBasic: this.calculateTotalBasic(pi.lineItem),


            }
            poRows.push(piData)
        }) : []

        let poRowss = _.map(
            _.uniq(
                _.map(poRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        this.setState({
            poRows: poRowss
        }, () => {

            let dataa = data.draft ? data.draft : data
            this.setState({

                stateCode: dataa.stateCode,
                isSet: dataa.isSet == true ? true : dataa.isSet == "true" ? true : false,
                isUDFExist: dataa.isSet == true ? this.state.storeUDFExist : dataa.isSet == "true" ? this.state.storeUDFExist : "false",
                slCode: dataa.slCode != null ? dataa.slCode : "",
                slAddr: dataa.slAddr != null ? dataa.slAddr : "",
                slName: dataa.slName != null ? dataa.slName : "",
                city: dataa.city != null ? dataa.slCityName : "",
                slCityName: dataa.slCityName != null ? dataa.slCityName : "",
                city: dataa.slCityName,
                supplier: dataa.slName != null ? dataa.slName + "-" + dataa.slAddr : "",
                leadDays: dataa.leadTime != null ? dataa.leadTime : "",
                termCode: dataa.termCode != null ? dataa.termCode : "",
                termName: dataa.termName != null ? dataa.termName : "",
                transporterCode: dataa.transporterCode != null ? dataa.transporterCode : "",
                transporterName: dataa.transporterName != null ? dataa.transporterName : "",
                transporter: dataa.transporterName != null ? dataa.transporterName : "",
                poUdf1: dataa.poudf1 != null ? dataa.poudf1 : "",
                poUdf2: dataa.poudf2 != null ? dataa.poudf2 : "yyyy-mm-dd",
                poUdf3: dataa.poudf3 != null ? dataa.poudf3 : "",
                poUdf4: dataa.poudf4 != null ? dataa.poudf4 : "",
                poUdf5: dataa.poudf5 != null ? dataa.poudf5 : "",
                term: dataa.termName != null ? dataa.termName : "",
                typeOfBuying: dataa.typeofBuying != null || dataa.typeofBuying != undefined ? data.typeOfBuying : 'Planned',

                poRows: poRowss,
                poQuantity: dataa.poQuantity != null ? dataa.poQuantity : "",
                poAmount: dataa.poAmount != null ? dataa.poAmount : "",
                loadIndentTrue: true,

                siteCode: dataa.siteCode != null ? dataa.siteCode : "",
                siteName: dataa.siteName != null ? dataa.siteName : "",
                indentId: dataa.indentId != null ? dataa.indentId : "",
                indentNo: dataa.indentNo != null ? dataa.indentNo : ""




            }, () => {
                let poRows = [...this.state.poRows]

                if (this.state.isMrpRequired && this.state.displayOtb) {
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                    ];
                    const date = new Date();
                    let year = date.getFullYear()
                    let monthName = (monthNames[date.getMonth()]);
                    let articleList = []
                    for (let i = 0; i < poRows.length; i++) {
                        if (poRows[i].articleCode != "" && poRows[i].vendorMrp != "") {
                            let payload = {
                                rowId: poRows[i].gridOneId,
                                articleCode: poRows[i].articleCode,
                                mrp: poRows[i].vendorMrp
                            }
                            articleList.push(payload)


                        }
                    }
                    let dataa = {
                        articleList: articleList,
                        month: monthName,
                        year: year
                    }
                    this.setState({
                        changeLastIndate: true
                    })
                    if (articleList.length != 0 && this.state.getDraft == false) {
                        this.props.multipleOtbRequest(dataa)
                    }
                }
                this.gridSecond()
                this.gridThird()
                this.gridFourth()
                this.gridFivth()
                this.props.getDraftClear()
                this.props.piEditClear()
            })
        })
    }
    handleInput(e) {

        if (e.target.id == "vendor") {
            this.setState({
                isVendorLovSelect: false,
                supplier: e.target.value,
                supplierSearch: this.state.isModalShow ? "" : e.target.value
            })


        } else if (e.target.id == "city") {
            this.setState({
                isCityLovSelect: false,
                city: e.target.value,
                citySearch: this.state.isModalShow ? "" : e.target.value
            })
        } else if (e.target.id == "siteName") {
            this.setState({
                isSiteLovSelect: false,
                siteName: e.target.value,
                siteSearch: this.state.isModalShow ? "" : e.target.value
            })
        } else if (e.target.id == "transporter") {
            this.setState({
                isTransporterLovSelect: false,
                transporter: e.target.value,
                transporterSearch: this.state.isModalShow ? "" : e.target.value
            })
            
        } else if (e.target.id == "agentName") {
            this.setState({
                poUdf1: e.target.value,
            })
        } else if (e.target.id == "poUdf2") {
            this.setState({
                poUdf2: e.target.value,
            })
        } else if (e.target.id == "poUdf3") {
            this.setState({
                poUdf3: e.target.value,
            })
        } else if (e.target.id == "poUdf4") {
            this.setState({
                poUdf4: e.target.value,
            })
        } else if (e.target.id == "poUdf5") {
            this.setState({
                poUdf5: e.target.value,
            })
        } else if (e.target.id == "loadIndentInput") {
            this.setState({
                loadIndent: e.target.value,
                loadIndentSearch: this.state.isModalShow ? "" : e.target.value
            })


        } else if (e.target.id == "poSetVendor") {
            this.setState({
                poSetVendor: e.target.value,
                setVendorSearch: this.state.isModalShow ? "" : e.target.value


            })
        } else if (e.target.id == "orderSet") {
            this.setState({
                orderSet: e.target.value,
                orderSearch: this.state.isModalShow ? "" : e.target.value
            })
        }
    }

    errFun = (focusId) => {
        this.setState({
            headerLevelErrMsg: true,
            focusId: focusId,
            poErrorMsg: true,
            errorMassage: "Please select a valid input from the drop-down list"
        })
    }

    cityErr = () => {
        if(this.state.cityModal && !this.state.isCityLovSelect && this.state.city != ''){
            this.setState({
                city: '',
            })
            this.errFun('city')
        }
    }

    siteErr = () => {
        if(this.state.siteModal && !this.state.isSiteLovSelect && this.state.siteName != ''){
            this.setState({
                siteName: '',
                siteCode: '',
            })
            this.errFun('siteName')
        }
    }

    supplierErr = () => {
        if(this.state.supplierModal && !this.state.isVendorLovSelect && this.state.supplier != ''){
            this.setState({
                supplier: ''
            })
            this.errFun('vendor')
        }
    }

    transporterErr = () => {
        if(this.state.trasporterModal && !this.state.isTransporterLovSelect && this.state.transporter != ''){
            this.setState({
                transporterCode: "",
                transporterName: "",
                transporter: "",
            })
            this.errFun('transporter')
        }
    }

    headerLevelErrFun = () => {
        this.cityErr();
        this.siteErr();
        this.supplierErr();
        this.transporterErr();
    }

    onEsc = () => {
        this.headerLevelErrFun()
        this.setState({

            poArticle: false,
            poArticleAnimation: false,
            discountModal: false,
            cityModal: false,
            cityModalAnimation: false,
            icodeModalAnimation: false,
            itemBarcodeModal: false,
            siteModal: false,
            siteModalAnimation: false,
            hsnModal: false,
            hsnModalAnimation: false,
            imageModal: false,
            imageModalAnimation: false,
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: false,
            trasporterModal: false,
            transporterAnimation: false,

            loadIndentModal: false,
            loadIndentAnimation: false,
            itemModal: false,
            itemModalAnimation: false,
            udfMapping: false,
            udfMappingAnimation: false,
            udfSetting: false,
            udfSettingAnimation: false,
            supplierModal: false,
            supplierModalAnimation: false,
            articleModalAnimation: false,
            articleModal: false,
            colorModal: false,
            colorModalAnimation: false,
            vendorDesignModal: false,
            vendorDesignAnimation: false,
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            departmentSetBasedAnimation: false,
            departmentModal: false,


            orderNumber: false,
            orderNumberModalAnimation: false,

            SetVendor: false,
            setVendorModalAnimation: false,
            // saveDraftFlag: true,
        })
        //  this.escChild.current.childEsc();
    }
    handleSearch(e, type, id, index) {
        let poRows = [...this.state.poRows]
        if (type == "divisionName") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].divisionName = e.target.value
                        poRows[i].sectionName = "",
                            poRows[i].departmentName = "",
                            poRows[i].articleCode = ""
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value
                })
            }
        } else if (type == "mrpRange") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].mrpRange = e.target.value
                        poRows[i].isMrpRangeLovSelect = false
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value,
                    isMrpFilled: true,
                })
            }
        } else if (type == "sectionName") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].sectionName = e.target.value,
                            poRows[i].departmentName = "",
                            poRows[i].articleCode = ""
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value
                })
            }
        } else if (type == "departmentName") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].departmentName = e.target.value,
                            poRows[i].articleCode = ""
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value
                })
            }
        } else if (type == "articleCode") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].articleCode = e.target.value
                        poRows[i].isArticleLovSelect = false
                    }
                }
                this.setState({
                    poRows: poRows,
                    poArticleSearch: e.target.value,
                })
            }
        } else if (type == "hsnCode") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].hsnSacCode = e.target.value
                        poRows[i].isHsnLovSelect = false
                    }
                }
                this.setState({
                    poRows: poRows,
                    hsnSearch: e.target.value
                })
            }
        } else if (type == "vendorMrp") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].vendorMrp = e.target.value
                        poRows[i].isVendorMrpLovSelect = false
                    }
                }
                this.setState({
                    poRows: poRows,
                    mrpSearch: e.target.value
                })
            }
        }
        else if (type == "discount") {
            if (e.target.id == type + id) {
                var numberPattern = /\d+/g;
                let value = "", checkboxVal = true;
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        if (e.target.value.match(/^[a-zA-Z0-9 ]+$/)) {
                            poRows[i].discount.discountType = e.target.value;
                            value = e.target.value
                            var Numvalue = e.target.value.match(numberPattern)
                            poRows[i].discount.discountValue = Numvalue != null ? Numvalue[0] : "";
                            checkboxVal = poRows[i].discount.discountPer;
                            poRows[i].discount.discountPer = checkboxVal;
                        }
                    }
                }
                this.setState({
                    poRows: poRows,
                    discountSearch: e.target.value
                }, () => {
                    if (!this.state.isDiscountMap) {
                        let data = {
                            discount: {
                                discountType: value,
                                discountValue: Numvalue != null ? Numvalue[0] : "",
                                discountPer: checkboxVal
                            },
                            discountGrid: index
                        }
                        this.updateDiscountModal(data)
                    }
                })
            }
        } else if (type == "itemCode") {
            if (e.target.id == type + id) {
                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == index) {
                        poRows[i].itemCodeSearch = e.target.value
                    }
                }
                this.setState({
                    poRows: poRows,
                    itemCodeSearch: e.target.value

                })
            }
        }
        this.setState({
            focusId: type + id,
            selectedRowId: index
        })
    }
    changeSet() {

        if (this.state.isSet) {

            this.setState({


                headerMsg: "Are you sure you want to raise purchase order on the basis of NON SET ",
                paraMsg: "Click confirm to continue.",
                radioChange: false,
                confirmModal: true,
                // toggler:!this.state.toggler,

            })
        } else {
            this.setState({

                headerMsg: "Are you sure you want to raise purchase order on the basis of  SET ",
                paraMsg: "Click confirm to continue.",
                radioChange: true,
                confirmModal: true,
            })
        }
    }
    closeMultipleError() {
        this.setState({
            multipleErrorpo: false
        })

        document.onkeydown = function (t) {

            return true;

        }
    }

    checkErr() {
        if (this.state.siteNameerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Site is Complusory",

                })
            document.getElementById("siteName").focus()
        } else if (this.state.cityerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "City is Complusory",

                })
            document.getElementById("city").focus()
        }
        else if (this.state.vendorerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Vendor is Complusory",

                })
            document.getElementById("vendor").focus()
        }
        else if (this.state.transportererr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Transporter is Complusory",

                })
            document.getElementById("transporter").focus()
        }
        else if (this.state.typeOfBuyingErr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Type of Buying is Complusory",

                })
            document.getElementById("typeOfBuying").focus()
        }
        else if (this.state.loadIndenterr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "load Indent is Complusory",

                })
            document.getElementById("loadIndentInput").focus()
        }
        else if (this.state.poSetVendorerr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Venoor is Complusory",

                })
            document.getElementById("poSetVendor").focus()
        }
        else if (this.state.orderSeterr) {
            this.setState(
                {
                    poErrorMsg: true,
                    errorMassage: "Order Number is Complusory",

                })
            document.getElementById("orderSet").focus()
        }
    }
    updateFocusId(data) {

        this.setState({
            focusId: data
        })
    }
    discountPer(id, percentage) {
        let poRows = this.state.poRows
        for (let i = 0; i < poRows.length; i++) {
            if (poRows[i].gridOneId == id) {

                if (percentage) {
                    poRows[i].discount.discountPer = false
                } else {
                    poRows[i].discount.discountPer = true

                }

            }
        }
        this.setState({
            poRows
        }, () => {


            let calculatedMarginValue = ""
            let finalRate = ""
            let rate = ""

            for (let i = 0; i < poRows.length; i++) {
                if (poRows[i].gridOneId == id) {
                    if (poRows[i].rate != "") {
                        if (poRows[i].discount.discountType != "" && this.state.isDiscountAvail) {

                            if (poRows[i].discount.discountPer) {
                                let value = poRows[i].rate * (poRows[i].discount.discountValue / 100)
                                poRows[i].finalRate = poRows[i].rate - value
                                finalRate = (poRows[i].rate - value).toFixed(2)

                            } else {
                                poRows[i].finalRate = poRows[i].rate - poRows[i].discount.discountValue
                                finalRate = (poRows[i].rate - poRows[i].discount.discountValue).toFixed(2)
                            }
                        } else {
                            poRows[i].finalRate = poRows[i].rate
                            finalRate = (poRows[i].rate).toFixed(2)

                        }
                    }
                }
            }



            this.setState({
                poRows: poRows

            })
            setTimeout(() => {
                let poRows = this.state.poRows
                let hsnSacCode = ""

                for (let i = 0; i < poRows.length; i++) {
                    if (poRows[i].gridOneId == id) {
                        hsnSacCode = poRows[i].hsnSacCode
                        if ((poRows[i].calculatedMargin.length != 0 && this.state.isRspRequired) || !this.state.isRspRequired) {

                            let lineItemArray = []

                            for (let j = 0; j < poRows[i].lineItem.length; j++) {

                                if (poRows[i].lineItem[j].setQty != "" && poRows[i].finalRate != "" && poRows[i].finalRate != 0) {
                                    let taxData = {
                                        hsnSacCode: poRows[i].hsnSacCode,
                                        qty: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total),
                                        rate: finalRate,
                                        rowId: poRows[i].lineItem[j].gridTwoId,
                                        designRowid: Number(id),
                                        basic: Number(poRows[i].lineItem[j].setQty) * Number(poRows[i].lineItem[j].total) * Number(finalRate), // lineitem taxDtaa
                                        clientGstIn: sessionStorage.getItem('gstin'),
                                        piDate: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),

                                        supplierGstinStateCode: this.state.stateCode,
                                        purtermMainCode: this.state.termCode,
                                        siteCode: this.state.siteCode
                                    }
                                    lineItemArray.push(taxData)
                                }
                            }
                            if (hsnSacCode != "" || hsnSacCode != null) {
                                if (lineItemArray.length != 0) {
                                    this.setState({
                                        lineItemChange: true
                                    }, () => {
                                        this.props.multipleLineItemRequest(lineItemArray)
                                    }
                                    )
                                }
                            } else {

                                this.setState({
                                    errorMassage: "HSN code is complusory",
                                    poErrorMsg: true

                                })
                            }

                        }
                    }
                }

            }, 10)

        })

    }

    _onAction(e) {
        if (this.state.successVar != true && !this.state.isContactCalled) {
            // this.idleTimer.resume();
            this.saveDraftCall("")
        }
        else {
            console.log("no draft pls!")
        }

    }

    _onActive(e) {

        // this.saveDraftCall()
    }

    _onIdle(e) {
    }
    calculateTotalBasic = (lineItemdata) => {
        if(lineItemdata != undefined){
            var totalBasicDataVariable = 0;
            for (var i = 0; i < lineItemdata.length; i++) {
                totalBasicDataVariable = totalBasicDataVariable + Number(lineItemdata[i].basic)
            }
    
            return totalBasicDataVariable
        }
        else{
            let poRows = _.cloneDeep(this.state.poRows);
            var totalBasicDataVariable = 0;
            poRows.forEach(data1 => {
                data1.lineItem.forEach(data2 => {
                    totalBasicDataVariable = totalBasicDataVariable + Number(data2.basic)
                })
                data1.totalBasic = totalBasicDataVariable
            })
            this.setState({
                poRows: poRows
            })
        }
        
    }
    saveDraftCall(data) {
        if (this.state.poRows.length > 0 && this.state.poRows[0].articleName != "") {
            const t = this;
            let lineItem = []
            let poRows = [...t.state.poRows]
            poRows.forEach(po => {
                let { cat1, cat2, cat3, cat4, cat5, cat6, desc1, desc2, desc3, desc4, desc5, desc6, catRemark } = ""
                let itemObj = {}
                if (po.catDescArray.length != 0) {
                    po.catDescArray.forEach(cdArr => {
                        cat1 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CAT1") {
                                return item
                            }
                        })
                        cat2 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CAT2") {
                                return item
                            }
                        })
                        cat3 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CAT3") {
                                return item
                            }
                        })
                        cat4 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CAT4") {
                                return item
                            }
                        })
                        cat5 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CAT5") {
                                return item
                            }
                        })
                        cat6 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CAT6") {
                                return item
                            }
                        })
                        desc1 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "DESC1") {
                                return item
                            }
                        })
                        desc2 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "DESC2") {
                                return item
                            }
                        })
                        desc3 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "DESC3") {
                                return item
                            }
                        })
                        desc4 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "DESC4") {
                                return item
                            }
                        })
                        desc5 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "DESC5") {
                                return item
                            }
                        })
                        desc6 = cdArr.catDesc.find(item => {
                            if (item.catdesc == "DESC6") {
                                return item
                            }


                        })
                        catRemark = cdArr.catDesc.find(item => {
                            if (item.catdesc == "CATREMARK") {
                                return item
                            }
                        })
                    })

                }
                if (po.itemUdfArray.length != 0) {
                    po.itemUdfArray.forEach(uArr => {
                        uArr.itemUdf.forEach(iu => {

                            itemObj[iu.cat_desc_udf] = iu.value

                        })
                    })

                }
                let lineItemArray = t.lineItemArray(po.lineItem, po.rate, po.finalRate)
                let remark = catRemark != undefined ? catRemark.value : '';
                //console.log('po', po)
                let poDetails = {
                    design: po.vendorDesign,
                    designWiseRowId: po.gridOneId,
                    hsnSacCode: po.hsnSacCode,
                    hsnCode: po.hsnCode,
                    hl1Name: po.divisionName,
                    hl1Code: po.divisionCode,
                    hl2Name: po.sectionName,
                    hl2Code: po.sectionCode,
                    hl3Name: po.departmentName,
                    hl3Code: po.departmentCode,
                    hl4Name: po.articleName,
                    hl4Code: typeof po.articleCode == 'string' ? '' : po.articleCode,
                    hl4CodeSeq: po.articleCode + "_" + po.gridOneId,
                    rate: po.rate,
                    finalRate: po.finalRate,
                    discountType: po.discount.discountPer ? po.discount.discountType + "%" : po.discount.discountType,
                    discountValue: po.discount.discountValue,
                    editableMrp: po.mrp,
                    rsp: this.state.isRspRequired ? po.rsp : po.mrp,
                    wsp: po.wsp,
                    mrp: this.state.isMrpRequired ? po.vendorMrp : po.mrp,
                    mrpStart: po.mrpStart || po.mrp,
                    mrpEnd: po.mrpEnd || po.mrp,
                    // images: po.image,
                    marginRule: po.marginRule,
                    designWiseTotalQty: po.quantity,
                    designWiseNetAmountTotal: po.amount,
                    otb: po.otb,
                    totalBasic: this.calculateTotalBasic(lineItemArray), // outer Basic
                    designWiseTotalGST: po.gst,
                    designWiseTotalFinCharges: po.finCharges.length > 0 ? po.finCharges : po.finCharge != undefined ? po.finCharge.length > 0 ? po.finCharge : [] : [],
                    remarks: po.remarks,
                    designWiseTotalTax: po.tax,
                    deliveryDate: po.deliveryDate + "T00:00:00+05:30",
                    // containsImage: po.containsImage,
                    designWiseTotalCalculatedMargin: po.calculatedMargin != "" || po.calculatedMargin != [] ? po.calculatedMargin : [],
                    designWiseTotalIntakeMargin: po.mrk,
                    headers: {
                        catDescHeader: po.catDescHeader,
                        itemUdfHeader: po.itemUdfHeader,
                        setUdfHeader: po.lineItem[0].setUdfHeader

                    },
                    catDescArray: po.catDescArray.length == 0 ? {} : {
                        cat1Code: cat1.code,
                        cat1Name: cat1.value,
                        cat2Code: cat2.code,
                        cat2Name: cat2.value,
                        cat3Code: cat3.code,
                        cat3Name: cat3.value,
                        cat4Code: cat4.code,
                        cat4Name: cat4.value,
                        cat5Code: cat5.code,
                        cat5Name: cat5.value,
                        cat6Code: cat6.code,
                        cat6Name: cat6.value,
                        desc1Code: desc1.code,
                        desc1Name: desc1.value,
                        desc2Code: desc2.code,
                        desc2Name: desc2.value,
                        desc3Code: desc3.code,
                        desc3Name: desc3.value,
                        desc4Code: desc4.code,
                        desc4Name: desc4.value,
                        desc5Code: desc5.code,
                        desc5Name: desc5.value,
                        desc6Code: desc6.code,
                        desc6Name: desc6.value,
                        catRemark: remark,
                    },
                    itemUdfArray: po.itemUdfArray.length == 0 ? {} : itemObj,
                    lineItem: lineItemArray





                }
                //console.log('podetails', poDetails)
                lineItem.push(poDetails)
            })



            let finalSubmitData = {
                isSet: t.state.isSet,
                typeOfBuying: t.state.typeOfBuying,
                indentNo: t.state.copyPi ? "" : t.state.indentNo,
                slCode: t.state.slCode,
                slName: t.state.slName,
                slCityName: t.state.isCityExist == true ? t.state.city : t.state.slCityName,
                slAddr: t.state.slAddr,
                leadTime: t.state.leadDays,
                termCode: t.state.termCode,
                termName: t.state.termName,
                transporterCode: t.state.transporterCode,
                transporterName: t.state.transporterName,
                poudf1: t.state.poUdf1,
                poudf2: t.state.poUdf2 == "yyyy-mm-dd" ? t.state.lastInDate : t.state.poUdf2,
                poudf3: t.state.poUdf3,
                poudf4: t.state.poUdf4,
                poudf5: t.state.poUdf5,
                poQuantity: t.state.poQuantity,
                poAmount: t.state.poAmount,
                stateCode: t.state.stateCode,
                itemUdfExist: t.state.itemUdfExist,
                isUDFExist: t.state.isUDFExist,
                enterpriseGstin: sessionStorage.getItem("gstin"),
                piDetails: lineItem,

                siteCode: t.state.siteCode,
                siteName: t.state.siteName,
                status:  "DRAFTED",
                validFrom: t.state.poValidFrom,
                validTo: t.state.lastInDate,
                showModal: data

            }
            //console.log('final submit', finalSubmitData)
            t.props.piSaveDraftRequest(finalSubmitData);
            t.setState({
                copyPi: false,
            })
        }
    }
    mrpRateBlur = (e, id) => {
        if(e.target.value != '') {
            this.setState({
                selectedRowId: id
            }, () => {
                this.updateNetAmt()
            })
        }
    }
    cancelClearForm = () => {
        this.showSaveDraft()
    }

    confirmClear = () => {
        this.setState({
            discardedSuccess: true
        })
        let finalIndentNo = "";
        let status = this.state.status;
        console.log(status)
        console.log("this.state.status",this.state.status, "this.state.indentNoToShow_drafted", this.state.indentNoToShow_drafted, "this.state.orderNoToShow_pending", this.state.indentNoToShow_pending, "this.state.orderNoDraft", this.state.indentNoDraft )


        if(status == "DRAFTED"){
            if(this.state.indentNoToShow_drafted != "" ){
                finalIndentNo = this.state.indentNoToShow_drafted
            }
            else if(this.state.indentNoDraft != "" ){
            finalIndentNo = this.state.indentNoDraft
            }
            let discardData = [{ indentNo: finalIndentNo, status: status }]
            this.props.discardPoPiDataRequest({ discardData: discardData, value: "pi" })    
            console.log("Drafted")
            this.onClear()
        }
        else if(status == "PENDING"){
            finalIndentNo = this.state.indentNoToShow_pending;
            let discardData = [{ indentNo: finalIndentNo, status: status }]
            this.props.discardPoPiDataRequest({ discardData: discardData, value: "pi" })
            console.log("pending")
            this.onClear()
        }
        else if (status == undefined  ||  this.state.indentNo != ""){
            status = "DRAFTED"
            finalIndentNo = this.state.indentNo;
            let discardData = [{ indentNo: finalIndentNo, status: status }]
            this.props.discardPoPiDataRequest({ discardData: discardData, value: "pi" })
            console.log("New PO")
            this.onClear()
        }

        else if (this.state.indentNoDraft == "" && this.state.indentNoToShow_drafted == "" && this.state.indentNoToShow_pending == "", this.state.indentNo == "") {
            console.log("3")
            this.reset();
        }

    }
    closeClearModalOnclickOutside = () => {
        //on clicking outside
        this.setState({
            clearForm: false
        })
    }

    handleClearModal = () => {
        console.log(status)
        console.log("this.state.indentNo", this.state.indentNo, "this.state.status",this.state.status, "this.state.indentNoDraft", this.state.indentNoDraft,"this.state.indentNoToShow_drafted", this.state.indentNoToShow_drafted, "this.state.indentNoToShow_pending", this.state.indentNoToShow_pending, "this.state.indentNoDraft", this.state.indentNoDraft )
        if (this.state.indentNoDraft == "" && this.state.indentNoToShow_drafted == "" && this.state.indentNoToShow_pending == "" && this.state.indentNo == "") {
            this.reset()
        }
        else {
            this.setState({
                clearForm: true
            })
        }
    }
    closeClearModal = () => {
        this.setState({ clearForm: false })
    }
    handleKey(e) {
        if (e.keyCode === 27) {
            this.setState({ clearForm: false })
        }
    }
    render() {
        console.log('agentName', this.state.poUdf1)
        console.log('set', this.state.isSet, this.state.gridSecond,"oneId", this.state.gridOneId, "selectedId", this.state.selectedRowId)
        //console.log("orderNumberSaveDraft", this.state.orderNumberSaveDraft, "discardedSuccess", this.state.discardedSuccess, "indentNoToShow_pending", this.state.indentNoToShow_pending, "indentNoToShow_drafted", this.state.indentNoToShow_drafted)
        //console.log("validFromEdited: ", this.state.validFromEdited, "validToEdited", this.state.validToEdited, "");
        //console.log("poValidFrom", this.state.poValidFrom, "lastInDate", this.state.lastInDate);
        console.log('poRows', this.state.poRows)
        // console.log("validFromEdited: ", this.state.validFromEdited, "validToEdited", this.state.validToEdited, "");
        // console.log("poValidFrom", this.state.poValidFrom, "lastInDate", this.state.lastInDate);
        const { typeofBuying, city, cityerr, itemcodeerr, itemCodeList, siteNameerr, siteName, hsnSacCode, hsnerr, poDate, loadIndent, loadIndenterr, poValidFrom, poValidFromerr, lastInDate, lastInDateerr, articleerr, division, department, section,
            supplier, vendorerr, leadDays, leadDayserr, transporter, transportererr, startRange, endRange, term, articleName, departmentSeterr, poSetVendorerr, setDepartment, poSetVendor, orderSet, orderSeterr, isEditPi, editableRow } = this.state;

        const taxTotal = (data) => {
            let t = 0;
            data.forEach(d => {
                t = t + Number(d)
            })
            return t
        }
        const DEFAULT_EVENTS = [
            // 'mousemove',
            'keydown',
            // 'wheel',
            // 'DOMMouseScroll',
            // 'mouseWheel',
            'mousedown',
            'touchstart',
            'touchmove',
            'MSPointerDown',
            'MSPointerMove',
            'visibilitychange'
        ]
        return (
            <div className="container_div pors-pg p-lr-0" id="" >
                <div>
                    <IdleTimer
                        ref={ref => { this.idleTimer = ref }}
                        element={document}
                        onActive={this.onActive}
                        onIdle={this.onIdle}
                        onAction={this.onAction}
                        debounce={5000}
                        events={DEFAULT_EVENTS}
                    />
                </div>

                <form name="siteForm" className="organizationForm">
                    <div className="new-purchase-head p-lr-47">
                        {/* <div className="col-lg-3 p-lr-0">
                            <div className="nph-title">
                                <h2>Purchase Indent</h2>
                            </div>
                        </div> */}
                        <div className="col-lg-7 pad-0">
                            <div className="nph-pi-po-quantity">
                                <ul className="nppq-inner">
                                    <li>
                                        <label>PI Quantity</label>
                                        <span>
                                            {this.state.poQuantity}
                                        </span>
                                    </li>
                                    <li>
                                        <label> PI Net Amount </label>
                                        <span><img src={RupeeNew} /> {Math.round(this.state.poAmount * 100) / 100} </span>
                                    </li>

                                    <li>
                                        {sessionStorage.getItem('currentPageName') == "Create Indent" ? <label></label>
                                            : this.state.orderNumberSaveDraft == true && !this.state.discardedSuccess ?
                                                this.state.indentNoToShow_pending != "" || this.state.indentNoToShow_drafted != "" ?
                                                    <label>Indent Number</label> : <label></label> : ""}
                                        {sessionStorage.getItem('currentPageName') == "Create Indent" ? <label></label>
                                            :
                                            this.state.orderNumberSaveDraft == true && !this.state.discardedSuccess ?
                                                this.state.isEditPi == true || this.state.isEditPi == "true" ?
                                                    <span>
                                                        {this.state.indentNoToShow_pending}
                                                    </span> :
                                                    <span>
                                                        {this.state.indentNoToShow_drafted}
                                                    </span>
                                                : <span></span>
                                        }
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-5 pad-0 display-fe">
                            <div className="nph-switch-btn">
                                <label className="tg-switch" >
                                    <input type="checkbox" checked={this.state.isSet} onChange={(e) => this.changeSet(e)} />
                                    <span className="tg-slider tg-round"></span>
                                    {this.state.isSet ? <span className="nph-wbtext nph-wbtextset">Set</span> : <span className="nph-wbtext">Non Set</span>}
                                </label>
                            </div>
                            <div className="nph-btn-sc">
                                {this.state.dateValidationRes || this.state.supplier == "" ? <button className="btnDisabled nphbc-clear" type="reset" disabled>Clear Form</button> : <button className="nphbc-clear" type="button" onKeyDown={(e) => this.handleKey(e)} onClick={this.handleClearModal}>Clear Form</button>}
                                {/* <button type="button" className="nphc-savedraft" onClick={this.showSaveDraft}>Save Draft </button> */}
                                {this.state.dateValidationRes || this.state.supplier == "" || !this.state.isSubmitEnable ?                                 
                                <div className="topToolTip toolTipSupplier">
                                    <button className="btnDisabled" type="button" disabled>Submit</button> 
                                    <span className="topToolTipText topAuto" style={{left: '0px'}}>SetQty is Mandatory</span>
                                </div>
                                : <button className="nphbc-submit" id="saveButton" type="button" onClick={(e) => this.debounceFun(e)} >Submit</button>}
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 border-btm">
                        <div className="pi-new-layout">
                            {this.state.isvalidFromAndTo ? <div className="col-lg-12 col-md-12 col-sm-12 pad-0 mb10">
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0">
                                    <label className="pnl-purchase-label">PI Date<span className="mandatory">*</span></label>
                                    <input autoComplete="off" type="text" className="purchaseOrderTextbox pnl-purchase-read" readOnly id="poDate" name="poDate" value={poDate} onKeyDown={(e) => this._handleKeyPressDate(e, "poDate")} placeholder="Choose PO Date" disabled />
                                </div>
                                <div className="col-lg-2 col-md-3 col-sm-5 pad-lft-0">
                                    <label className="pnl-purchase-label">PI Valid From<span className="mandatory">*</span></label>
                                    <input autoComplete="off" type="date" min={this.state.currentDate} className={poValidFromerr ? "errorBorder pnl-purchase-date" : "purchaseOrderTextbox onFocus pnl-purchase-date"} id="piValidFrom" name="piValidFrom" value={poValidFrom} onKeyDown={(e) => this._handleKeyPressDate(e, "poValidFrom")} onChange={(e) => this.dateVali(e)} placeholder={poValidFrom} />
                                    {poValidFromerr ? (<span className="error"> Enter PO Valid From</span>) : null}
                                </div>
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0">
                                    <label className="pnl-purchase-label">PI Valid To<span className="mandatory">*</span></label>
                                    <input autoComplete="off" type="date" className={lastInDateerr ? "errorBorder pnl-purchase-date" : "pnl-purchase-date onFocus"} min={this.state.poValidFrom} id="lastInDate" name="lastInDate" value={lastInDate} onChange={(e) => this.handleInputChange(e)} placeholder={lastInDate} />
                                    {lastInDateerr ? (<span className="error">Select PO Valid To</span>) : null}
                                </div>
                            </div> : ""}


                            {/* {this.state.isvalidFromAndTo && <div className="col-lg-12 col-md-12 col-sm-12 pad-0 mb10">
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0">
                                    <label className="pnl-purchase-label">PI Date<span className="mandatory">*</span></label>
                                    <input autoComplete="off" type="text" className="purchaseOrderTextbox pnl-purchase-read" readOnly id="poDate" name="poDate" value={poDate} onKeyDown={(e) => this._handleKeyPressDate(e, "poDate")} placeholder="Choose PO Date" disabled />
                                </div>
                                <div className="col-lg-2 col-md-3 col-sm-5 pad-lft-0">
                                    <label className="pnl-purchase-label">PI Valid From<span className="mandatory">*</span></label>
                                    <input autoComplete="off" type="date" min={this.state.currentDate} className={poValidFromerr ? "errorBorder pnl-purchase-date" : "purchaseOrderTextbox onFocus pnl-purchase-date"} id="piValidFrom" name="piValidFrom" value={poValidFrom} onKeyDown={(e) => this._handleKeyPressDate(e, "poValidFrom")} onChange={(e) => this.dateVali(e)} placeholder="" />
                                    {poValidFromerr ? (<span className="error"> Enter PO Valid From</span>) : null}
                                </div>
                                <div className="col-lg-2 col-md-6 col-sm-6 pad-lft-0">
                                    <label className="pnl-purchase-label">PI Valid To<span className="mandatory">*</span></label>
                                    <input autoComplete="off" type="date" className={lastInDateerr ? "errorBorder pnl-purchase-date" : "pnl-purchase-date onFocus"} min={this.state.poValidFrom} id="lastInDate" name="lastInDate" value={lastInDate} onChange={(e) => this.handleInputChange(e)} placeholder="" />
                                    {lastInDateerr ? (<span className="error">Select PO Valid To</span>) : null}
                                </div>
                            </div>} */}


                            {/* <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                                <div className="col-lg-6 col-md-4 col-sm-12 pad-0">
                                    <ul className="list_style">
                                        <li><label className="contribution_mart">PURCHASE INDENT</label></li>
                                        <li><p className="master_para">Create purchase indent</p></li>
                                    </ul>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6 text-right">
                                    <label className="purchaseLabel set">
                                        {this.state.isSet ? "SET" : "NON SET"}</label>
                                    <label className="tg-switch" >
                                        <input type="checkbox" checked={this.state.isSet} onChange={(e) => this.changeSet(e)} />
                                        <span className="tg-slider tg-round"></span>
                                    </label>
                                </div>

                            </div>  */}
                            {this.state.isSiteExist == "true" ? <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                <label className="pnl-purchase-label">Site<span className="mandatory">*</span></label>

                                <div disabled={isEditPi} className={siteNameerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">
                                        {!this.state.isModalShow ?
                                            <input disabled={isEditPi} autoComplete="off" type="text" onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.handleInput(e) : null} className="onFocus pnl-purchase-input" onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" onBlur={(e) => this.headerLevelInputBlur(e)} />

                                            : <input disabled={isEditPi} autoComplete="off" type="text" readOnly className="inputTextKeyFuc" onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openSiteModal(e) : null} />}
                                        {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openSiteModal(e)}>
                                                    ▾
                                                    </span> : null} */}
                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn" onClick={(e) => this.openSiteModal(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                            </svg>
                                        </span> : null}
                                        {!this.state.isModalShow ? this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteName={this.state.siteName} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null : null}

                                        {siteName != "" ? <span className="topToolTipText topAuto">{siteName}</span> : null}

                                    </div>
                                </div>
                                {siteNameerr ? (<span className="error">Enter Site </span>) : null}
                            </div> : null}
                            {this.state.isCityExist == true ? <div className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10 margin-right0">
                                <label className="pnl-purchase-label">City<span className="mandatory">*</span></label>
                                <div className={cityerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div disabled={isEditPi} className="topToolTip toolTipSupplier width100">
                                        {!this.state.isModalShow ?
                                            <input autoComplete="off" disabled={isEditPi} onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={city} placeholder="Choose City" onBlur={(e) => this.headerLevelInputBlur(e)} />

                                            :
                                            <input autoComplete="off" disabled={isEditPi} readOnly type="text" className="inputTextKeyFuc m-b-10" onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={city} placeholder="Choose City" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openCityModal(e) : null} />}
                                        {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openCityModal(e)}>
                                            ▾
                                        </span> : null} */}
                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick={(e) => this.openCityModal(e)}>
                                                <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                            </svg>
                                        </span> : null}
                                        {!this.state.isModalShow ? this.state.cityModal ? <CityModal city={this.state.city} citySearch={this.state.citySearch} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null : null}

                                        {city !== "" && <span className="topToolTipText topAuto">{city}</span>}
                                    </div>

                                </div>
                                {/* <div className="pnlci-check">
                                <label className="checkBoxLabel0">
                                    <input type="checkBox" className="checkBoxTab" checked= {this.state.isCityChecked} id = 'cityCheck' onClick = {(e) => {this.setState({isCityChecked: !this.state.isCityChecked})}}/>
                                    <span className="checkmark1"></span>
                                </label>
                                </div> */}
                                {cityerr ? ( <span className="error">Enter City</span>) : null}
                            </div> : null}
                            <div disabled={isEditPi} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10">
                                <label className="pnl-purchase-label">Vendor<span className="mandatory">*</span></label>
                                <div className={vendorerr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"}  >
                                    <div className="topToolTip toolTipSupplier width100">
                                        {!this.state.isModalShow ?
                                            <input autoComplete="off" disabled={isEditPi} onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" onKeyDown={(e) => this._handleKeyPress(e, "vendor")} id="vendor" value={supplier} placeholder="Choose Vendor" onBlur={(e) => this.headerLevelInputBlur(e)} />
                                            : <input autoComplete="off" disabled={isEditPi} readOnly type="text" className="inputTextKeyFuc m-b-10 onFocus" onKeyDown={(e) => this._handleKeyPress(e, "vendor")} id="vendor" value={supplier} placeholder="Choose Vendor" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openSupplier(e, "vendor") : null} />}
                                        {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openSupplier(e, "vendor")}>
                                            ▾
                                        </span> : null} */}
                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick={(e) => this.openSupplier(e, "vendor")}>
                                                <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                            </svg>
                                        </span> : null}
                                        {!this.state.isModalShow ? this.state.supplierModal ? <SupplierModal {...this.props}{...this.state} city={this.state.city} supplierSearch={this.state.supplierSearch} siteName={this.state.siteName} siteCode={this.state.siteCode} departmentCode={this.state.hl3Code} department={this.state.department} supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null : null}

                                        {supplier != "" ? <span className="topToolTipText topAuto">{supplier}</span> : null}
                                    </div>
                                </div>
                                {vendorerr ? (<span className="error">Enter Vendor</span>) : null}
                            </div>
                            <div disabled={isEditPi} hidden={!this.state.isDispalyTransporter} className="col-lg-2 col-md-2 col-sm-6 mb10 check-input">
                                {this.state.transporterValidationPi ? 
                                    <label className="pnl-purchase-label">Transporter{this.state.transporterWithCity ? " (with city)" : " (without city)"}<span className="mandatory">*</span></label>:
                                    <label className="pnl-purchase-label">Transporter{this.state.transporterWithCity ? " (with city)" : " (without city)"}</label>
                                 }
                                <div className={transportererr ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">
                                        {!this.state.isModalShow ?
                                            <input autoComplete="off" disabled={isEditPi} onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" onBlur={(e) =>this.headerLevelInputBlur(e)}/>
                                            : <input autoComplete="off" disabled={isEditPi} readOnly type="text" className="inputTextKeyFuc m-b-10 onFocus" onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.openTransporterSelection(e, "transporter") : null} />}
                                        {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modalBlueBtn" onClick={(e) => this.openTransporterSelection(e, "transporter")}>
                                            ▾
                                        </span> : null} */}
                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick={(e) => this.openTransporterSelection(e, "transporter")}>
                                                <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                            </svg>
                                        </span> : null}
                                        {!this.state.isModalShow ? this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} isTransporterDependent={this.state.isTransporterDependent} city={this.state.city} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null : null}

                                        {transporter != "" ? <span className="topToolTipText topAuto">{transporter}</span> : null}
                                    </div>
                                </div>
                                <div className="pnlci-check">
                                    <label className="checkBoxLabel0">
                                        <input type="checkbox" checked={this.state.transporterWithCity} onChange= {this.checkedHandler} id="transporterCheck"/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                {transportererr ? (<span className="error">Enter Transporter</span>) : null}
                            </div>
                            <div  hidden={!this.state.isDisplayPOUdf1} className="col-lg-2 col-md-2 col-sm-6 mb10 check-input">
                                {this.state.isMandatePOUdf1 ? 
                                    <label className="pnl-purchase-label">{this.state.POUdf1Label}{this.state.agentNameWithCity ? " (with city)" : " (without city)"}<span className="mandatory">*</span></label>:
                                    <label className="pnl-purchase-label">{this.state.POUdf1Label}{this.state.agentNameWithCity ? " (with city)" : " (without city)"}</label>
                                 }
                                <div className={this.state.poUdf1err ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">                                        
                                        <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" onKeyDown={(e) => this._handleKeyPress(e, "agentName")} id="agentName" value={this.state.poUdf1} placeholder={`Choose ${this.state.POUdf1Label}`} />   
                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? <span className="modal-search-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" onClick={(e) => this.openAgentHandler(e, "agentName")}>
                                                <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                            </svg>
                                        </span> : null}
                                        {!this.state.isModalShow ? this.state.agentModal ? <AgentModal {...this.props} {...this.state} onCloseAgent={(e) => this.onCloseAgent(e)} updateAgentState={(e) => this.updateAgentState(e)} /> : null : null}                                                                                 
                                        {this.state.poUdf1 != "" ? <span className="topToolTipText topAuto">{this.state.poUdf1}</span> : null}
                                    </div>
                                </div>
                                <div className="pnlci-check">
                                    <label className="checkBoxLabel0">
                                        <input type="checkbox" checked={this.state.agentNameWithCity} onChange= {this.checkedHandler} id="agentCheck"/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                {this.state.poUdf1err ? (<span className="error">{`Enter ${this.state.POUdf1Label}`}</span>) : null}
                            </div>
                            <div  hidden={!this.state.isDisplayPOUdf2} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10">
                                {this.state.isMandatePOUdf2 ? 
                                    <label className="pnl-purchase-label">{this.state.POUdf2Label}<span className="mandatory">*</span></label>:
                                    <label className="pnl-purchase-label">{this.state.POUdf2Label}</label>
                                 }
                                <div className={this.state.poUdf2err ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">    
                                        <input autoComplete="off" type="date" min={this.state.poValidFrom} max={this.state.maxDate} className={this.state.poUdf2err ? "errorBorder pnl-purchase-date" : "purchaseOrderTextbox onFocus pnl-purchase-date"} id="poUdf2" value={this.state.poUdf2 == "yyyy-mm-dd" ? this.state.lastInDate : this.state.poUdf2} onKeyDown={(e) => this._handleKeyPressDate(e, "poUdf2")} onChange={(e) => this.handleInput(e)} placeholder={this.state.poUdf2 == "yyyy-mm-dd" ? this.state.lastInDate : this.state.poUdf2} />                                    
                                        {/* <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" onKeyDown={(e) => this._handleKeyPress(e, "agentName")} id="agentName" value={this.state.poUdf2} placeholder={`Choose ${this.state.POUdf2Label}`} />                                                                                     */}
                                        {this.state.poUdf2 != "" ? <span className="topToolTipText topAuto">{this.state.poUdf2}</span> : null}
                                    </div>
                                </div>
                                {this.state.poUdf2err ? (<span className="error">{`Select ${this.state.POUdf2Label}`}</span>) : null}
                            </div>
                            <div  hidden={!this.state.isDisplayPOUdf3} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10">
                                {this.state.isMandatePOUdf3 ? 
                                    <label className="pnl-purchase-label">{this.state.POUdf3Label}<span className="mandatory">*</span></label>:
                                    <label className="pnl-purchase-label">{this.state.POUdf3Label}</label>
                                 }
                                <div className={this.state.poUdf3err ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">                                        
                                        <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" id="poUdf3" value={this.state.poUdf3} placeholder={`Choose ${this.state.POUdf3Label}`} />   
                                        {this.state.poUdf3 != "" ? <span className="topToolTipText topAuto">{this.state.poUdf3}</span> : null}
                                    </div>
                                </div>
                                {this.state.poUdf3err ? (<span className="error">{`Enter ${this.state.POUdf3Label}`}</span>) : null}
                            </div>
                            <div  hidden={!this.state.isDisplayPOUdf4} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10">
                                {this.state.isMandatePOUdf4 ? 
                                    <label className="pnl-purchase-label">{this.state.POUdf4Label}<span className="mandatory">*</span></label>:
                                    <label className="pnl-purchase-label">{this.state.POUdf4Label}</label>
                                 }
                                <div className={this.state.poUdf4err ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">                                        
                                        <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" id="poUdf4" value={this.state.poUdf4} placeholder={`Choose ${this.state.POUdf4Label}`} />   
                                        {this.state.poUdf4 != "" ? <span className="topToolTipText topAuto">{this.state.poUdf4}</span> : null}
                                    </div>
                                </div>
                                {this.state.poUdf4err ? (<span className="error">{`Enter ${this.state.POUdf4Label}`}</span>) : null}
                            </div>
                            <div  hidden={!this.state.isDisplayPOUdf5} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10">
                                {this.state.isMandatePOUdf5 ? 
                                    <label className="pnl-purchase-label">{this.state.POUdf5Label}<span className="mandatory">*</span></label>:
                                    <label className="pnl-purchase-label">{this.state.POUdf5Label}</label>
                                 }
                                <div className={this.state.poUdf5err ? "inputTextKeyFucMain errorBorder" : "inputTextKeyFucMain"} >
                                    <div className="topToolTip toolTipSupplier width100">                                        
                                        <input autoComplete="off" onChange={(e) => this.handleInput(e)} type="text" className="onFocus pnl-purchase-input" id="poUdf5" value={this.state.poUdf5} placeholder={`Choose ${this.state.POUdf5Label}`} />   
                                        {this.state.poUdf5 != "" ? <span className="topToolTipText topAuto">{this.state.poUdf5}</span> : null}
                                    </div>
                                </div>
                                {this.state.poUdf5err ? (<span className="error">{`Enter ${this.state.POUdf5Label}`}</span>) : null}
                            </div>
                            <div disabled={isEditPi} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0 mb10">
                                <label className="pnl-purchase-label">Type Of Buying<span className="mandatory">*</span></label>
                                <select disabled={isEditPi} value={this.state.typeOfBuying} className="pnl-purchase-select onFocus" id="typeofBuying" onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChange(e) : null}>
                                    <option>Select</option>
                                    <option value="Adhoc" > Adhoc</option>
                                    <option value="Planned">Planned </option>
                                </select>
                                {this.state.typeOfBuyingErr ? (
                                    <span className="error">
                                        Select Type of Buying
                                    </span>
                                ) : null}
                            </div>
                            {this.state.isLeadTimeDisplayPo && <div disabled={isEditPi} className="col-lg-1 col-md-1 col-sm-6 pad-lft-0 mb10">
                                <label className="pnl-purchase-label">Lead Days</label>
                                <input autoComplete="off" disabled={isEditPi} readOnly type="text" value={leadDays} id="leadDays" onChange={(e) => this.onChange(e)} className={leadDayserr ? "m-b-10 errorBorder purchaseTextbox pnl-purchase-read" : "purchaseTextbox pnl-purchase-read abc"} disabled />
                            </div>}
                            <div disabled={isEditPi} className="col-lg-2 col-md-2 col-sm-6 pad-lft-0">
                                <label className="pnl-purchase-label">Term</label>
                                <input autoComplete="off" disabled={isEditPi} readOnly type="text" className="pnl-purchase-read" id="termData" value={term} placeholder="" disabled />
                            </div>

                            {/* <div className="col-lg-3 col-md-5 col-sm-12 pr-order-1 pad-0">
                                <div className="poAmtTotal">
                                    <ul className="pad-0">
                                        <li>
                                            <label>PI Quantity</label>
                                            <span>
                                                {this.state.poQuantity}
                                            </span>
                                        </li>
                                        <li>
                                            <label> PI Amount </label>
                                            <span>{Math.round(this.state.poAmount * 100) / 100} </span>
                                        </li>

                                    </ul>
                                </div>
                            </div> */}
                        </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47">
                        <div className="purchase-new-table">
                            <div className="StickyDiv1 overflowVisible">
                                <div className="col-lg-12 col-md-12 col-sm-12 pad-0 pnt-bxsha">
                                    <div className="pad-0 marginLeft_126" >
                                        <table className="table scrollTable zui-table border-bot-table overfYhid pit-new-design">
                                            <thead>
                                                <tr>
                                                    <th className="fixed-side alignMiddle pnt-fixed" >
                                                        <ul className="list-inline">
                                                            <li>
                                                                <label className="lableFixed">
                                                                    {this.state.addRow ? <div className="add-row-btn">
                                                                        {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <button onClick={this.addRow} type="button" id="addIndentRow">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 6.883 6.883">
                                                                                <g id="prefix__noun_Plus_2310783" transform="translate(-17 -73.298)">
                                                                                    <g id="prefix__Group_2204" data-name="Group 2204" transform="translate(-8 -904.064)">
                                                                                        <path fill="#ffb103" id="prefix__Path_434" d="M28.441 977.362a.688.688 0 0 0-.688.688v2.065h-2.065a.688.688 0 0 0 0 1.377h2.065v2.065a.688.688 0 1 0 1.377 0v-2.065h2.065a.688.688 0 0 0 0-1.377H29.13v-2.065a.688.688 0 0 0-.689-.688z" data-name="Path 434" />
                                                                                    </g>
                                                                                </g>
                                                                            </svg>
                                                                        </button> : <button type="button" className="btnDisabled">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 6.883 6.883">
                                                                                    <g id="prefix__noun_Plus_2310783" transform="translate(-17 -73.298)">
                                                                                        <g id="prefix__Group_2204" data-name="Group 2204" transform="translate(-8 -904.064)">
                                                                                            <path fill="#ffb103" id="prefix__Path_434" d="M28.441 977.362a.688.688 0 0 0-.688.688v2.065h-2.065a.688.688 0 0 0 0 1.377h2.065v2.065a.688.688 0 1 0 1.377 0v-2.065h2.065a.688.688 0 0 0 0-1.377H29.13v-2.065a.688.688 0 0 0-.689-.688z" data-name="Path 434" />
                                                                                        </g>
                                                                                    </g>
                                                                                </svg>
                                                                            </button>}
                                                                    </div> : null}
                                                                    <span>Add Row</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </th>
                                                    {this.state.isDisplayDivision && <th><label>Division </label></th>}
                                                    {this.state.isDisplaySection && <th><label>Section</label></th>}
                                                    {this.state.isDisplayDepartment && <th><label>Department</label></th>}
                                                    <th><label>Article Code </label></th>
                                                    <th><label>Article Name</label></th>
                                                    {this.state.codeRadio == "poIcode" ? <th><label>Item Code</label></th> : null}
                                                    <th className="set-tdWidth"><label>HSN</label></th>
                                                    {this.state.isMrpRangeDisplay ? <th><label>MRP range</label></th> : null}
                                                    {this.state.isMrpRequired ? <th className="set-tdWidth">
                                                        {/* {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? <label>
                                                            DESC 6<span className="mandatory">*</span>
                                                        </label> : <label>MRP<span className="mandatory">*</span></label>} */}
                                                        <label>MRP {this.state.mrpValidation ==  true ? <span className="mandatory">*</span> : null }</label>
                                                    </th> : null}
                                                    {!this.state.isVendorDesignNotReq ? <th>
                                                        <label>
                                                            Vendor Design No.<span className="mandatory">*</span>
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isRspRequired ? <th className="set-tdWidth">
                                                        <label>
                                                            RSP
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isMRPEditable ? <th className="set-tdWidth">
                                                        <label>
                                                            MRP{this.state.mrpValidation ==  true ? <span className="mandatory">*</span> : null }
                                                        </label>
                                                    </th> : null}
                                                    <th className="set-tdWidth"><label>Rate<span className="mandatory">*</span></label>
                                                    </th>
                                                    {this.state.isDisplayWSP ? <th className="set-tdWidth">
                                                        <label>
                                                            WSP{this.state.isMandateWSP ==  true ? <span className="mandatory">*</span> : null }
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isDiscountAvail ? <th>
                                                        <label>Discount<span className="mandatory">*</span></label>
                                                    </th> : null}
                                                    {this.state.isDisplayFinalRate ? <th className="set-tdWidth">
                                                        <label>
                                                            Final rate
                                                        </label>
                                                    </th> : null}


                                                    {/* <th>
                                                        <label>
                                                            Image
                                                             </label>
                                                    </th> */}
                                                    {this.state.displayOtb ? <th className="set-tdWidth">
                                                        <label>
                                                            OTB
                                                        </label>
                                                    </th> : null}
                                                    {this.state.isDeliveryDate && <th>
                                                        <label>
                                                            Delivery Date<span className="mandatory">*</span>
                                                        </label>
                                                    </th>}

                                                    {this.state.isDisplayMarginRule ? <th className="set-tdWidth">
                                                        <label>
                                                            Margin Rule
                                                   </label>

                                                    </th> : null}
                                                    <th><label>
                                                        Remarks
                                                    </label></th>
                                                    <th className="set-tdWidth"><label>
                                                        Total Quantity
                                                    </label></th>
                                                    {this.state.isBaseAmountActive ? <th className="set-tdWidth"><label>
                                                        Basic
                                                    </label></th> : null}
                                                    <th className="set-tdWidth"><label>
                                                        Total Net Amount
                                                    </label></th>
                                                    {!this.state.isTaxDisable ? <th className="set-tdWidth"><label>
                                                        Tax
                                                    </label></th> : null}
                                                    {this.state.isRspRequired ? <th className="set-tdWidth"><label>
                                                        Calculated Margin
                                                    </label></th> : null}
                                                    {!this.state.isGSTDisable ? <th className="set-tdWidth"><label>
                                                        GST
                                                    </label></th> : null}

                                                    {this.state.isRspRequired ? <th className="set-tdWidth">

                                                        <label>
                                                            Mark Up / Down
                                                                     </label>
                                                    </th> : null}
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {this.state.poRows.map((item, idx) => (
                                                    <tr id={idx} key={idx}>
                                                        <td className="fixed-side alignMiddle pnt-fixed">
                                                            <ul className="list-inline">
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <li className="pnt-f-icon pnt-bg-red">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 19" onClick={(e) => this.handleRemoveSpecificRow(item.gridOneId, item.otb, item.mrp)}>
                                                                        <path fill={item.checked ? "#FF0000" : "#a4b9dd"} fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                    </svg>
                                                                    {/* <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 16.335 19.602" onClick={(e) => this.DeleteRowPo(item.gridOneId, item.otb, item.mrp)}>
                                                                        <path id="prefix__iconmonstr-trash-can-2" d="M7.717 15.518a.817.817 0 0 1-1.633 0V7.351a.817.817 0 0 1 1.633 0zm3.267 0a.817.817 0 0 1-1.633 0V7.351a.817.817 0 0 1 1.633 0zm3.267 0a.817.817 0 0 1-1.633 0V7.351a.817.817 0 1 1 1.633 0zm4.084-13.885v1.634H2V1.633h4.664C7.4 1.633 8 .736 8 0h4.341c0 .736.6 1.633 1.332 1.633zM15.884 4.9v13.068H4.45V4.9H2.817v14.7h14.7V4.9z" transform="translate(-2)" style="fill:#a4b9dd"/>
                                                                    </svg> */}
                                                                    <span className="generic-tooltip">Delete</span>
                                                                </li> : null}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <li className="pnt-f-icon pnt-bg-yellow" onClick={(e) => this.copyRow(item.gridOneId, item.otb, item)} >
                                                                    {/* <img src={copyIcon} /> */}
                                                                    {/*<svg xmlns="http://www.w3.org/2000/svg" onClick={this.deleteSecTwoRows(itemGridTwo.gridTwoId, itemGridTwo.setNo)} width="17" height="17" viewBox="0 0 17 19">
                                                                        <path fill="#e5e5e5" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                    </svg>*/}
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="17" viewBox="0 0 21 21">
                                                                        <g fill="#a4b9dd" fillRule="nonzero">
                                                                            <path d="M17.705 4.66H15.84V2.794A2.795 2.795 0 0 0 13.045 0H2.795A2.795 2.795 0 0 0 0 2.795v10.25a2.795 2.795 0 0 0 2.795 2.796H4.66v1.864A2.795 2.795 0 0 0 7.455 20.5h10.25a2.795 2.795 0 0 0 2.795-2.795V7.455a2.795 2.795 0 0 0-2.795-2.796zM1.864 13.044V2.795c0-.514.417-.931.931-.931h10.25c.515 0 .932.417.932.931V4.66H7.455a2.795 2.795 0 0 0-2.796 2.796v6.522H2.795a.932.932 0 0 1-.931-.932zm16.772 4.66a.932.932 0 0 1-.931.931H7.455a.932.932 0 0 1-.932-.931V7.455c0-.515.417-.932.932-.932h10.25c.514 0 .931.417.931.932v10.25z" />
                                                                            <path d="M15.375 11.648h-1.864V9.784a.932.932 0 1 0-1.863 0v1.864H9.784a.932.932 0 1 0 0 1.863h1.864v1.864a.932.932 0 1 0 1.863 0v-1.864h1.864a.932.932 0 1 0 0-1.863z" />
                                                                        </g>
                                                                    </svg>
                                                                    <span className="generic-tooltip">Copy</span>
                                                                </li> : null}
                                                                <li className="pnt-f-icon pnt-bg-blue" onClick={(e) => this.createSet(item.gridOneId, item)}>
                                                                    {/* <button className="doneButton create-set-icon" type="button" onClick={(e) => this.createSet(item.gridOneId, item)} > */}
                                                                    {/* <img src={invoice} /> */}
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.604" height="17" viewBox="0 0 15.604 18.724">
                                                                        <path fill={item.checked ? "#23e3eb" : "#a4b9dd"} id="prefix__iconmonstr-note-14" fillRule="nonzero" d="M3.56 17.164V1.56h12.483v9.006c0 3.2-4.681 1.917-4.681 1.917s1.184 4.681-2.058 4.681H3.56zm14.04-5.94V0H2v18.724h7.949c2.468 0 7.651-5.635 7.651-7.5zm-3.9-1.081H5.9v-.78h7.8zm0-3.121H5.9V7.8h7.8zm0-2.341H5.9v.78h7.8z" transform="translate(-2)" />
                                                                    </svg>
                                                                    <span className="generic-tooltip">Item Details</span>
                                                                    {/* </button> */}
                                                                </li>
                                                            </ul>
                                                        </td>

                                                        {this.state.isDisplayDivision && <td className="pad-0 tdFocus tInputText" >
                                                            <div disabled={idx < editableRow} className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ?
                                                                        <input autoComplete="off" type="text"  onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.handleSearch(e, "divisionName", idx, item.gridOneId) : null} className="inputTable" value={item.divisionName} onKeyDown={(e) => this._handleKeyPressRow(e, "divisionName", idx)} id={"divisionName" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly  className="inputTable " value={item.divisionName} onKeyDown={(e) => this._handleKeyPressRow(e, "divisionName", idx)} id={"divisionName" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "divisionName" + idx, "division", item, idx) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"divisionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "divisionName" + idx, "division", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"divisionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "divisionName" + idx, "division", item, idx) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "division" ? <div className="calculatedToolTip set-pi-modal-position" > <PoArticleModal {...this.props}{...this.state} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /></div> : null : null : null}*/}
                                                            </div>

                                                        </td>}

                                                        {this.state.isDisplaySection && <td className="pad-0 tdFocus tInputText" >
                                                            <div disabled={idx < editableRow} className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ? <input autoComplete="off" type="text"  onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.handleSearch(e, "sectionName", idx, item.gridOneId) : null} className="inputTable" value={item.sectionName} onKeyDown={(e) => this._handleKeyPressRow(e, "sectionName", idx)} id={"sectionName" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly  className="inputTable " value={item.sectionName} onKeyDown={(e) => this._handleKeyPressRow(e, "sectionName", idx)} id={"sectionName" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "sectionName" + idx, "section", item, idx) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"sectionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "sectionName" + idx, "section", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"sectionNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "sectionName" + idx, "section", item) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "section" ? <div className="calculatedToolTip  set-pi-modal-position" > <PoArticleModal {...this.props}{...this.state} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> </div> : null : null : null}*/}
                                                            </div>

                                                        </td>}
                                                        {this.state.isDisplayDepartment && <td className="pad-0 tdFocus tInputText" >
                                                            <div disabled={idx < editableRow} className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ? <input autoComplete="off" type="text"  onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.handleSearch(e, "departmentName", idx, item.gridOneId) : null} className="inputTable" value={item.departmentName} onKeyDown={(e) => this._handleKeyPressRow(e, "departmentName", idx)} id={"departmentName" + idx} />

                                                                        : <input autoComplete="off" type="text" readOnly  className="inputTable " value={item.departmentName} onKeyDown={(e) => this._handleKeyPressRow(e, "departmentName", idx)} id={"departmentName" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "departmentName" + idx, "department", item) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"departmentNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "departmentName" + idx, "department", item) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"departmentNameDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "departmentName" + idx, "department", item) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "department" ? <div className="calculatedToolTip  set-pi-modal-position" > <PoArticleModal {...this.props}{...this.state} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> </div> : null : null : null}*/}
                                                            </div>
                                                        </td>}

                                                        <td className="pad-0 tdFocus tInputText" >
                                                            <div disabled={idx < editableRow} className="pos-rel">
                                                                <label className="purchaseTableLabel ">
                                                                    {!this.state.isModalShow ?
                                                                        <input autoComplete="off" type="text"  onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.handleSearch(e, "articleCode", idx, item.gridOneId) : null} className="inputTable" value={item.articleCode} onKeyDown={(e) => this._handleKeyPressRow(e, "articleCode", idx)} id={"articleCode" + idx} onBlur={(e) => this.rowLevelInputBlur(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, idx)} />

                                                                        : <input autoComplete="off" type="text" readOnly  className="inputTable " value={item.articleCode} onKeyDown={(e) => this._handleKeyPressRow(e, "articleCode", idx)} id={"articleCode" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, "articleCode" + idx) : null} />}
                                                                </label>
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"articleCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, "articleCode" + idx) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"articleCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "articleCode" + idx, "articleCode", item, "articleCode" + idx) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/* {!this.state.isModalShow ? this.state.poArticle ? this.state.basedOn == "article" ? <div className="calculatedToolTip  set-pi-modal-position" id="uuid"> <PoArticleModal {...this.props}{...this.state} uuid ={this.state.uuid} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> </div> : null : null : null} */}
                                                            </div>
                                                        </td>
                                                        <td className="pad-0 hoverTable tInputRead" >
                                                            <input autoComplete="off" type="text" readOnly disabled className="inputTable " value={item.articleName} onKeyDown={(e) => this._handleKeyPressRow(e, "articleName", idx)} id={"articleName" + idx} />
                                                        </td>
                                                        {this.state.codeRadio == "poIcode" ? <td className="pad-0 hoverTable openModalBlueBtn" >
                                                            <input autoComplete="off" type="text" readOnly className="inputTable " value={item.itemCodeList} onKeyDown={(e) => this._handleKeyPressRow(e, "itemCode", idx)} id={"itemCode" + idx} onClick={(e) => this.openNewIcode(`${item.gridOneId}`, item.articleCode, idx)} />
                                                            {/* <div className="purchaseTableDiv" onClick={(e) => this.openNewIcode(`${item.gridOneId}`, item.articleCode, idx)} >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg>
                                                            </div> */}
                                                            <div className="modal-search-btn-table" onClick={(e) => this.openNewIcode(`${item.gridOneId}`, item.articleCode, idx)}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                    <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                </svg>
                                                            </div>

                                                        </td> : null}
                                                        <td className="pad-0 tdFocus tInputText set-tdWidth" >
                                                            <div className="pos-rel">
                                                                {!this.state.isModalShow ?
                                                                    <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "hsnCode", idx, item.gridOneId)} className="inputTable" value={item.hsnSacCode} onKeyDown={(e) => this._handleKeyPressRow(e, "hsnCode", idx)} id={"hsnCode" + idx} onBlur={(e) => this.rowLevelInputBlur(e, item.gridOneId, "hsnCode" + idx, "hsnCode", item, idx)} />

                                                                    : <input autoComplete="off" type="text" readOnly className="inputTable " value={item.hsnSacCode} onKeyDown={(e) => this._handleKeyPressRow(e, "hsnCode", idx)} id={"hsnCode" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openHsnCodeModal(item.gridOneId, item.articleCode, idx, item.departmentCode, item.hsnSacCode, "hsnCode" + idx) : null} />}
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"hsnCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openHsnCodeModal(item.gridOneId, item.articleCode, idx, item.departmentCode, item.hsnSacCode, "hsnCode" + idx) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"hsnCodeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openHsnCodeModal(item.gridOneId, item.articleCode, idx, item.departmentCode, item.hsnSacCode, "hsnCode" + idx) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/* {!this.state.isModalShow ? this.state.hsnModal ?<div className="calculatedToolTip set-pi-modal-position" >  <HsnCodeModal {...this.props} {...this.state} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /> </div>: null : null} */}
                                                            </div>

                                                        </td>
                                                        {/* Mrp range change */}
                                                        {this.state.isMrpRangeDisplay ? <td className="pad-0 hoverTable tInputText tdFocus">
                                                            <div className="pos-rel">

                                                                <label className="purchaseTableLabel">
                                                                    {!this.state.isModalShow ?
                                                                        <input autoComplete="off" type="text" onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "poIcode" ? (e) => this.handleSearch(e, "mrpRange", idx, item.gridOneId) : null} className="inputTable" value={item.mrpRange} onKeyDown={(e) => this._handleKeyPressRow(e, "mrpRange", idx)} id={"mrpRange" + idx} onBlur={(e) => this.rowLevelInputBlur(e, `${item.gridOneId}`, "mrpRange" + idx, "mrpRange", item, idx)}/>

                                                                        : <input autoComplete="off" type="text" className="inputTable " value={item.mrpRange} onKeyDown={(e) => this._handleKeyPressRow(e, "mrpRange", idx)} id={"mrpRange" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "mrpRange" + idx, "mrpRange", item, "mrpRange" + idx) : null} />}
                                                                    {/* {item.mrpStart}-{item.mrpEnd} */}
                                                                </label>
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"mrpRangeDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openPoArticle(e, `${item.gridOneId}`, "mrpRange" + idx, "mrpRange", item, "mrpRange" + idx) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                            </div>

                                                        </td> : null}


                                                        {this.state.isMrpRequired ? <td className="pad-0 tdFocus tInputText set-tdWidth" >
                                                            <div className="pos-rel">
                                                                {this.state.isModalShow ?
                                                                    <input autoComplete="off" type="text" className="inputTable " value={item.vendorMrp} onKeyDown={(e) => this._handleKeyPressRow(e, "vendorMrp", idx)} id={"vendorMrp" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openItemCode(e, `${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp", idx, `${item.hsnSacCode}`, `${item.mrpStart}`, `${item.mrpEnd}`, `${item.articleCode}`
                                                                    ) : null}/>
                                                                    : <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "vendorMrp", idx, item.gridOneId)} className="inputTable " value={item.vendorMrp} onKeyDown={(e) => this._handleKeyPressRow(e, "vendorMrp", idx)} id={"vendorMrp" + idx} onBlur={(e) => this.rowLevelInputBlur(e, `${item.gridOneId}`, "vendorMrp" + idx, "vendorMrp", item, idx)}/>}
                                                                {/* {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"vendorMrpDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openItemCode(e, `${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp", idx, `${item.hsnSacCode}`, `${item.mrpStart}`, `${item.mrpEnd}`, `${item.articleCode}`) : null} >
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                        <g fill="none" fillRule="evenodd">
                                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                        </g>
                                                                    </svg>
                                                                </div> : null} */}
                                                                {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="modal-search-btn-table" id={"vendorMrpDiv" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.openItemCode(e, `${item.vendorMrp}`, `${item.gridOneId}`, "vendorMrp", idx, `${item.hsnSacCode}`, `${item.mrpStart}`, `${item.mrpEnd}`, `${item.articleCode}`) : null}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#9b7e00" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </div> : null}
                                                                {/*{!this.state.isModalShow ? this.state.itemModal ?<div className="calculatedToolTip set-pi-modal-position" >  <ItemCodeModal {...this.props} {...this.state} desc6={this.state.desc6} poRows={this.state.poRows} descId={this.state.descId} itemModalAnimation={this.state.itemModalAnimation} startRange={this.state.startRange} endRange={this.state.endRange} code={this.state.hl4Code} openItemCode={() => this.openItemCode()} closeItemmModal={() => this.closeItemmModal()} updateItem={(e) => this.updateItem(e)} /> </div>: "" : null}*/}

                                                            </div>
                                                        </td> : null}
                                                        {!this.state.isVendorDesignNotReq ? this.state.isAutoGenerateDesign ? 
                                                        <td className="pad-0 tInputRead set-tdWidth">
                                                            <input autoComplete="off" readOnly disabled type="text" name="vendorDesign" id={"vendorDesign" + idx} className="inputTable " onKeyDown={(e) => this._handleKeyPressRow(e, "vendorDesign", idx)} value={item.vendorDesign} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                        </td> :
                                                        <td className="pad-0 tdFocus tInputType">
                                                            <input autoComplete="off" type="text" name="vendorDesign" id={"vendorDesign" + idx} className="inputTable " onKeyDown={(e) => this._handleKeyPressRow(e, "vendorDesign", idx)} value={item.vendorDesign} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                        </td> : null}

                                                        {this.state.isRspRequired ? <td className="pad-0 tInputRead set-tdWidth">
                                                            <input autoComplete="off" readOnly disabled type="text" className="inputTable" id={"rsp" + idx} value={item.rsp} onKeyDown={(e) => this._handleKeyPressRow(e, "rsp", idx)} />
                                                        </td> : null}

                                                        {this.state.isMRPEditable ? <td className={this.state.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? "errorBorder pad-0" : "pad-0 tdFocus tInputType set-tdWidth" : "pad-0 tdFocus tInputType set-tdWidth"}>
                                                            <label className="rateHover" id={"toolIdMrp" + item.gridOneId} onMouseOver={this.state.codeRadio != "setBased" ? (e) => this.mouseOverCmprMrp(`${item.gridOneId}`) : null}>
                                                                <input autoComplete="off" type="text" className="inputTable" id={"mrp" + idx} onKeyDown={(e) => this._handleKeyPressRow(e, "mrp", idx)} pattern="[0-9]+([\.][0-9]{0,2})?" value={item.mrp} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                                {this.state.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? <span> <div className="showRate" id={"moveLeftMrp" + item.gridOneId}><p>Rsp cannot be greater than Mrp</p></div></span> : "" : ""}
                                                            </label>
                                                        </td> : null}

                                                        <td className={this.state.isMrpRequired || this.state.isMRPEditable ? this.state.mrpValidation == false ? "pad-0 tdFocus tInputType set-tdWidth" : Number(item.rate) > Number(item.mrp) ? "errorBorder pad-0" : "pad-0 tdFocus tInputType set-tdWidth" : "pad-0 tdFocus tInputType set-tdWidth"}>
                                                            <label className="rateHover asd" onBlur={(e) => this.mrpRateBlur(e, item.gridOneId)} id={"toolIdRate" + item.gridOneId} onMouseOver={this.state.codeRadio != "setBased" ? (e) => this.mouseOverCmprRate(`${item.gridOneId}`) : null}>
                                                                <input autoComplete="off" type="text" className="inputTable" id={"rate" + idx} pattern="[0-9.]*" value={item.rate} onFocus={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.rateValueOnFocus(e, item.gridOneId, "rate", item.finalRate) : null} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} onBlur={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.onBlurRate(`${item.gridOneId}`, e) : null} />
                                                                {this.state.mrpValidation ? this.state.isMrpRequired || this.state.isMRPEditable ? Number(item.rate) > Number(item.mrp) ? <span> <div className="showRate" id={"moveLeftRate" + item.gridOneId}><p>Rate cannot be greater than Mrp</p></div></span> : "" : "": ""}
                                                            </label>
                                                        </td>

                                                        {this.state.isDisplayWSP ? <td className={this.state.wspValidation == false ? "pad-0 tdFocus tInputType set-tdWidth" : Number(item.wsp) > Number(item.rsp) ? "errorBorder pad-0" : "pad-0 tdFocus tInputType set-tdWidth"}>
                                                            <label className="rateHover asd" id={"toolIdWsp" + item.gridOneId}>
                                                                <input autoComplete="off" type="text" className="inputTable" id={"wsp" + idx} pattern="[0-9.]*" value={item.wsp} onChange={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "raisedIndent" || this.state.codeRadio == "setBased" || this.state.codeRadio == "poIcode" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                                {this.state.wspValidation && Number(item.wsp) > Number(item.rsp) ? <span> <div className="showRate" id={"moveLeftRate" + item.gridOneId}><p>WSP cannot be greater than RSP</p></div></span> : ""}
                                                            </label>
                                                        </td> : null}

                                                        {this.state.isDiscountAvail ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus tInputType" >
                                                            {this.state.isModalShow ? <input autoComplete="off" type="text" readOnly className="inputTable " value={item.discount.discountType} onKeyDown={(e) => this._handleKeyPressRow(e, "discount", idx)} id={"discount" + idx} onClick={this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || (this.state.codeRadio == "poIcode") ? (e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.gridOneId}`, "discount" + idx) : null} />

                                                                : <input autoComplete="off" type="text" onChange={(e) => this.handleSearch(e, "discount", idx, item.gridOneId)} className="inputTable dis-input" value={item.discount.discountType} onKeyDown={(e) => this._handleKeyPressRow(e, "discount", idx)} id={"discount" + idx} />}
                                                            {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <div className="purchaseTableDiv" id={"discountDiv" + idx} onClick={(this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" || this.state.codeRadio == "poIcode") && this.state.isDiscountMap ? (e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.gridOneId}`, "discount" + idx) : null} >
                                                                {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg> */}
                                                            </div> : null}
                                                            <div className="srb-content2">
                                                                <label className="clr-name">%
                                                                <input type="checkbox" id={"checkbox-value-" + idx} checked={item.discount.discountPer ? true : false} onChange={(e) => this.discountPer(item.gridOneId, item.discount.discountPer ? true : false)} />
                                                                    <span className="checkmark"></span>
                                                                </label>
                                                            </div>

                                                        </td> : null}
                                                        {this.state.isDisplayFinalRate ? <td className="pad-0 tInputRead set-tdWidth">
                                                            <input autoComplete="off" type="text" readOnly className="inputTable" id="finalRate" value={item.finalRate} />
                                                        </td> : null}
                                                        {/* <td className="pad-0 pad-0 hoverTable openModalBlueBtn openModalBlueBtn tdFocus displayPointer tInputText" onClick={this.state.codeRadio != "setBased" ? (e) =>
                                                            this.openImageModal(`${item.gridOneId}`, "image" + idx, item.articleCode, idx) : null}>
                                                            <label className="piToolTip">
                                                                <div className="topToolTip">
                                                                    <input type="text" className="imageInput" readOnly onKeyDown={this.state.codeRadio != "setBased" ? (e) => this._handleKeyPressRow(e, "image", idx) : null} id={"image" + idx} value={item.image != undefined ? item.image.join(',') : ""} />
                                                                    <label>{item.image != undefined ? item.image.join(',') : ""}</label>
                                                                    {item.image != undefined ? <span className="topToolTipText"> {item.image.join(',')}</span> : null}
                                                                </div>
                                                            </label>
                                                            {this.state.codeRadio != "setBased" ? <div className="modal-search-btn-table" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="34" height="44" viewBox="0 0 33 43">
                                                                    <g fill="none" fillRule="nonzero">
                                                                        <path fill="#fffae7" d="M0 0h33v43H0z" />
                                                                        <path fill="#000" d="M13.143 22.491A2.847 2.847 0 0 1 16 19.635a2.847 2.847 0 0 1 2.857 2.856A2.847 2.847 0 0 1 16 25.348a2.847 2.847 0 0 1-2.857-2.857zm-6.026 4.05v-8.236c0-.94.763-1.703 1.703-1.703h3.404l.156-.685c.255-1.056 1.174-1.8 2.25-1.8h2.7c1.077 0 2.016.744 2.25 1.8l.157.685h3.404c.94 0 1.702.763 1.702 1.702v8.237c0 .998-.802 1.82-1.82 1.82H8.938c-.998 0-1.82-.822-1.82-1.82zm4.676-4.05A4.2 4.2 0 0 0 16 26.698a4.2 4.2 0 0 0 4.207-4.207A4.183 4.183 0 0 0 16 18.304a4.196 4.196 0 0 0-4.207 4.187zm-2.817-3.443c0 .47.391.86.861.86s.86-.39.86-.86-.39-.861-.86-.861c-.49.02-.86.391-.86.86z" />
                                                                    </g>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="17.245" height="14.37" viewBox="0 0 17.245 14.37">
                                                                    <path d="M3.593 1.437H1.437V.719h2.156zm7.853 0l.583.876a2.867 2.867 0 0 0 2.391 1.28h1.387v9.341H1.437V3.593h2.824a2.867 2.867 0 0 0 2.391-1.28l.583-.876zM12.215 0H6.467L5.456 1.515a1.436 1.436 0 0 1-1.2.64H0V14.37h17.245V2.156h-2.824a1.436 1.436 0 0 1-1.2-.64L12.215 0zm-7.9 5.748a.719.719 0 1 0-.719.719.718.718 0 0 0 .715-.719zm5.03 0A2.156 2.156 0 1 1 7.185 7.9a2.158 2.158 0 0 1 2.156-2.152zm0-1.437A3.593 3.593 0 1 0 12.933 7.9a3.593 3.593 0 0 0-3.592-3.589z" />
                                                                </svg>
                                                            </div> : null}
                                                        </td> */}
                                                        {this.state.displayOtb ? <td className={item.otb < item.amount ? "errorBorder pad-0" : "pad-0 tInputRead set-tdWidth"}>
                                                            <input autoComplete="off" readOnly disabled type="text" className="inputTable" id={"otb" + idx} value={item.otb == "null" || item.otb == null || item.otb == ""  || isNaN(item.otb) ? 0 : item.otb} onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                        </td> : null}

                                                        {this.state.isDeliveryDate && <td className={item.deliveryDate != "" ? item.deliveryDate < this.state.minDate ? "errorBorder pad-0 positionRelative editDateFormat tInputDate" : "pad-0 positionRelative tdFocus editDateFormat tInputDate" : "pad-0 positionRelative tdFocus editDateFormat tInputDate"}
                                                        >
                                                            <input type="date" className="inputTable height0" min={this.state.minDate} placeholder="" id={"date" + idx} onChange={(e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx)} name="date" value={item.deliveryDate} />
                                                        </td>}

                                                        {this.state.isDisplayMarginRule ? <td className="typeOfBuying tInputRead set-tdWidth">
                                                            <label> {item.marginRule}</label>
                                                        </td> : null}
                                                        <td className="pad-0 tdFocus tInputType">
                                                            <input autoComplete="off" type="text" className="inputTable" id={"remarks" + idx} value={item.remarks} onChange={this.state.codeRadio != "setBased" ? (e) => this.handleChangeGridOne(`${item.gridOneId}`, e, idx) : null} />
                                                        </td>
                                                        <td className="pad-0 tInputRead set-tdWidth">
                                                            <input autoComplete="off" type="text" disabled className="inputTable" id={"quantity" + idx} value={item.quantity} />
                                                        </td>
                                                        {this.state.isBaseAmountActive ? <td className="pad-0 tInputRead set-tdWidth">
                                                            <input autoComplete="off" type="text" disabled className="inputTable" id={"basic" + idx} value={isNaN(item.totalBasic) ? 0 : item.totalBasic} />
                                                        </td> : null}
                                                        <td className="pad-0 posRelative poToolTipMain tInputRead set-tdWidth">
                                                            <label>
                                                                <input autoComplete="off" type="text" disabled className="inputTable widthauto" id={"amount" + idx} value={item.amount} />
                                                                {/*<span className="netAmtHover netAmtTotal displayInline displayPointer">
                                                                    {item.amount != "" ? <img id={"toolId" + item.gridOneId} className="exclaimIconToolTip" onMouseOver={(e) => this.calculatedMargin(`toolId${item.gridOneId}`, `moveLeft${item.gridOneId}`)} src={exclaimIcon} />
                                                                        : null}
                                                                    <div id={"moveLeft" + item.gridOneId} className="totalAmtDescDrop">
                                                                        <div className="amtContent">
                                                                            <table>
                                                                                <thead>
                                                                                    <tr><th><p>Charge Name</p></th>
                                                                                        <th><p>% / Amount</p></th>
                                                                                        <th><p>Total</p></th></tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    {item.finCharges.map((data, key) => (
                                                                                        <tr key={key}>
                                                                                            <td><p>{data.chgName}</p></td>
                                                                                            <td><p>{data.rates != undefined ? <span>{data.rates.igstRate} </span> : ""}</p>{}</td>
                                                                                            <td><p>{data.charges != undefined ? <span>{data.charges.sign}{data.charges.chargeAmount}</span> : data.sign + data.finChgRate}</p>

                                                                                            </td>
                                                                                        </tr>))}
                                                                                </tbody>

                                                                            </table>
                                                                        </div>
                                                                        <div className="amtBottom">
                                                                            <div className="col-md-12">
                                                                                <div className="col-md-6 pad-lft-7">

                                                                                    <span>Basic</span>
                                                                                </div>
                                                                                <div className="col-lg-6 col-md-6 textRight padRightNone">

                                                                                    <span className="totalAmtt">+{item.basic}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="amtBottom">
                                                                            <div className="col-lg-12 col-md-12 pad-top-5 m-top-5">
                                                                                <div className="col-lg-6 col-md-6">

                                                                                    <p>Grand Total</p>
                                                                                </div>
                                                                                <div className="col-lg-6 col-md-6 textRight padRightNone">

                                                                                    <p className="totalAmt">{item.amount}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </span>*/}
                                                            </label>
                                                        </td>
                                                        {!this.state.isTaxDisable ? <td className="pad-0 tInputRead set-tdWidth">
                                                            <label className="piToolTip m0 poToolTip">
                                                                {item.tax && item.tax.length > 0 && <div className="topToolTip">
                                                                    <label>{item.tax.length != 0 ? taxTotal(item.tax) : ""}</label>
                                                                    <span className="topToolTipText">{item.tax.length != 0 ? taxTotal(item.tax) : ""}</span>
                                                                </div>}
                                                            </label>
                                                            {/*<input autoComplete="off" type="text" className="inputTable" id="tax" value={item.tax.length != 0 ? item.tax.join(',') : ""} />*/}
                                                        </td> : null}

                                                        {this.state.isRspRequired ? <td className="pad-0 posRelative poToolTipMain tInputRead set-tdWidth">
                                                            <label>
                                                                <label className="piToolTip m0 poToolTip">
                                                                    {item.calculatedMargin && item.calculatedMargin.length > 0 && <div className="topToolTip">
                                                                        <label className="max-width80">{item.calculatedMargin.length != 0 ? item.calculatedMargin[0] : ""}</label>
                                                                        <span className="topToolTipText">{item.calculatedMargin.length != 0 ? item.calculatedMargin[0] : ""}</span>
                                                                    </div>}
                                                                </label>
                                                                {/*<input autoComplete="off" type="text" className="inputTable" id="calculatedMargin" value={item.calculatedMargin.length != 0 ? item.calculatedMargin.join(',') : ""} />*/}
                                                                {/*<span className="calculatedMargin displayInline displayPointer ">
                                                                    {item.calculatedMargin != "" ? <img src={exclaimIcon} className="exclaimIconToolTip" id={"marginIdSet" + item.gridOneId} onMouseOver={(e) => this.calculatedMargin(`marginIdSet${item.gridOneId}`, `marginLeft${item.gridOneId}`)} /> : null}
                                                                    <div className="calculatedToolTip totalAmtDescDrop" id={"marginLeft" + item.gridOneId}>
                                                                        <div className="formula">
                                                                            <h3>Calculated Margin</h3>
                                                                            <h5>RSP-COST <span>X</span> <span className="pad-0">100</span><p>RSP</p></h5>
                                                                        </div>
                                                                    </div>
                                                                </span>*/}
                                                            </label>
                                                        </td> : null}

                                                        {!this.state.isGSTDisable ? <td className="pad-0 tInputRead">
                                                            <input autoComplete="off" type="text" disabled className="inputTable" id={"gst" + idx} value={item.gst.length != 0 ? item.gst[0] : ""} />
                                                        </td> : null}
                                                        {this.state.isMrpRequired ? <td className="pad-0 posRelative poToolTipMain tInputRead set-tdWidth">
                                                            <label>
                                                                <label className="piToolTip m0 poToolTip">
                                                                    {item.mrk && item.mrk.length > 0 && <div className="topToolTip">
                                                                        <label className="max-width80">{item.mrk.length != 0 ? item.mrk[0] : ""}</label>
                                                                        <span className="topToolTipText">{item.mrk.length != 0 ? item.mrk[0] : ""}</span>
                                                                    </div>}
                                                                </label>
                                                                {/*<input autoComplete="off" type="text" className="inputTable" id="mrk" value={item.mrk.length != 0 ? item.mrk.join(',') : ""} />*/}
                                                                {/*<span className="calculatedMargin displayInline displayPointer inTakeMargin">
                                                                    {item.mrk != "" ? <img src={exclaimIcon} className="exclaimIconToolTip" id={"intakeMarginSet" + item.gridOneId} onMouseOver={(e) => this.calculatedMargin(`intakeMarginSet${item.gridOneId}`, `inTakeMarginLeft${item.gridOneId}`)} />
                                                                        : null}
                                                                    <div className="calculatedToolTip totalAmtDescDrop" id={"inTakeMarginLeft" + item.gridOneId}>
                                                                        <div className="formula">
                                                                            <h3>Intake Margins</h3>
                                                                            <h5><span>RSP</span> <span>X</span> <span className="pad-0">100</span><p className="pad-lft-0">100+COST</p></h5>
                                                                        </div>
                                                                    </div>
                                                                </span>*/}
                                                            </label>
                                                        </td> : null}

                                                    </tr>
                                                ))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                {/* __________________________________ITEM CODE TABLE END____________________ */}
                                {/* {this.state.addRow ? <div className="newRowAdd m-top-10">
                                    {this.state.codeRadio == "Adhoc" || this.state.codeRadio == "holdPo" ? <button onClick={this.addRow} type="button" id="addIndentRow" className="zuiBtn">
                                        Add Row
                        </button> : <button type="button" className="zuiBtn btnDisabled">
                                            Add Row
                        </button>}
                                </div> : null} */}



                            </div>
                            {/* <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                <div className="footerDivForm displayFlex">
                                    <div className="col-md-6 alignMiddle">
                                        <ul className="list-inline m-lft-0 m-top-10">
                                            <li>{this.state.dateValidationRes || this.state.supplier == "" ? <button className="clear_button_vendor btnDisabled" type="reset" disabled>Clear</button> : <button className="clear_button_vendor" type="reset" onClick={(e) => this.reset(e)}>Clear</button>} </li>
                                            <li>{this.state.dateValidationRes || this.state.supplier == "" ? <button className="save_button_vendor btnDisabled" type="button" disabled>Save</button> : <button className="save_button_vendor" id="saveButton" type="button" onClick={(e) => this.debounceFun(e)} >Save</button>} </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-6 poAmountBottom">
                                        <ul>
                                            <li><label>PI Quantity -</label><span>{this.state.poQuantity}</span></li>
                                            <li><label>PI Net Amount -</label><span>{this.state.poAmount}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> */}
                            {this.state.discountModal ? <DiscountModal {...this.props}{...this.state} disId={this.state.disId} discountSearch={this.state.discountSearch} selectedDiscount={this.state.selectedDiscount} discountGrid={this.state.discountGrid} closeDiscountModal={(e) => this.closeDiscountModal(e)} isDiscountMap={this.state.isDiscountMap} vendorMrpPo={this.state.vendorMrpPo} updateDiscountModal={(e) => this.updateDiscountModal(e)} discountMrp={this.state.discountMrp} /> : null}

                            {this.state.poArticle ? <PoArticleModal {...this.props}{...this.state} uuid={this.state.uuid} copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> : null}

                            {this.state.itemModal ? <ItemCodeModal {...this.props} {...this.state} mrpId={this.state.mrpId} desc6={this.state.desc6} poRows={this.state.poRows} descId={this.state.descId} itemModalAnimation={this.state.itemModalAnimation} startRange={this.state.startRange} endRange={this.state.endRange} code={this.state.hl4Code} openItemCode={() => this.openItemCode()} closeItemmModal={() => this.closeItemmModal()} updateItem={(e) => this.updateItem(e)} /> : null}
                            {this.state.hsnModal ? <HsnCodeModal {...this.props} {...this.state} hsnId={this.state.hsnId} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /> : null}
                        </div>
                    </div>
                </form>
                {this.state.clearForm && <ConfirmClearForm cancelClearForm={this.cancelClearForm} confirmClear={this.confirmClear} closeModal={this.closeClearModalOnclickOutside} />}
                {this.state.isModalShow ? this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} isTransporterDependent={this.state.isTransporterDependent} city={this.state.city} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null : null}
                {this.state.itemBarcodeModal ? <NewIcodeModal {...this.props} {...this.state} icodeModalAnimation={this.state.icodeModalAnimation} articleCode={this.state.hl4Code} supplierCode={this.state.slCode} siteCode={this.state.siteCode} closeNewIcode={(e) => this.closeNewIcode(e)} itemCodeList={this.state.itemCodeList} updateNewIcode={(e) => this.updateNewIcode(e)} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

                {this.state.departmentModal ? <SetDepartmentModal {...this.props} {...this.state} onCloseDepartmentModal={(e) => this.onCloseDepartmentModal(e)} updateDepartment={(e) => this.updateDepartment(e)} departmentSetBasedAnimation={this.state.departmentSetBasedAnimation} setDepartment={this.state.setDepartment} hl3CodeDepartment={this.state.hl3CodeDepartment} /> : null}
                {this.state.isModalShow ? this.state.setVendor ? <SetVendorModal {...this.props} {...this.state} onCloseVendor={(e) => this.onCloseVendor(e)} updateVendorPo={(e) => this.updateVendorPo(e)} /> : null : null}
                {this.state.isModalShow ? this.state.orderNumber ? <OrderNumberModal {...this.props} {...this.state} orderSet={this.state.orderSet} hl3CodeDepartment={this.state.hl3CodeDepartment} poSetVendorCode={this.state.poSetVendorCode} updateOrderNumber={(e) => this.updateOrderNumber(e)} orderSet={this.state.orderSet} departmentSet={this.state.departmentSet} poSetVendor={this.state.poSetVendor} closeSetNumber={(e) => this.closeSetNumber(e)} orderNumberModalAnimation={this.state.orderNumberModalAnimation} /> : null : null}
                {this.state.isModalShow ? this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteName={this.state.siteName} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null : null}
                {this.state.isModalShow ? this.state.cityModal ? <CityModal city={this.state.city} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.confirmModal ? <ConfirmModalPo {...this.props} {...this.state} pi={this.state.pi} radioChange={this.state.radioChange} onnRadioChange={(e) => this.onnRadioChange(e)} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
                {/* {this.state.showSaveDraft ? <ConfirmDraftModal {...this.props} {...this.state} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} finalDraftSave={e => { this.finalDraftSave(e) }} closeDraftConfirmModal={(e) => this.closeDraftConfirmModal(e)} /> : null} */}
                {this.state.isModalShow ? this.state.loadIndentModal ? <LoadIndent {...this.props} {...this.state} indentValue={this.state.indentValue} updateLoadIndentState={(e) => this.updateLoadIndentState(e)} loadIndentCloseModal={(e) => this.openloadIndentSelection(e)} onCloseLoadIndent={(e) => this.onCloseLoadIndent(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} /> : null : null}
                {this.state.isModalShow ? this.state.supplierModal ? <SupplierModal {...this.props} {...this.state} city={this.state.city} departmentCode="" siteCode={this.state.siteCode} department="" supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null : null}
                {/* { this.state.poArticle ? <PoArticleModal {...this.props}{...this.state} uuid ={this.state.uuid}  copingFocus={(e) => this.copingFocus(e)} resetPoRows={(e) => this.resetPoRows(e)} poArticleAnimation={this.state.poArticleAnimation} closePOArticle={(e) => this.closePOArticle(e)} updateMrpState={(e) => this.updateMrpState(e)} /> : null} */}
                {this.state.setModal ? <SetModalPI ratioHandleChange={this.ratioHandleChange} showSimple={true} ref={instance => { this.escChil = instance }} copingFocus={(e) => this.copingFocus(e)} updateFocusId={(e) => this.updateFocusId(e)} validateSetUdf={(e) => this.validateSetUdf(e)} onEsc={this.onEsc} isSet={this.state.isSet} setModalAnimation={this.state.setModalAnimation} {...this.props} {...this.state} updatePo={(e) => this.updatePo(e)} copingColor={(e) => this.copingColor(e)} deleteLineItem={(e) => this.deleteLineItem(e)} closeSetModal={(e) => this.closeSetModal(e)} updatePoRows={(e) => this.updatePoRows(e)} validateCatDescRow={(e) => this.validateCatDescRow(e)} gridSecond={(e) => this.gridSecond(e)}
                    validateItemdesc={(e) => this.validateItemdesc(e)} getOtbForPoRows={(e) => this.getOtbForPoRows(e)} gridThird={(e) => this.gridThird(e)} gridFourth={(e) => this.gridFourth(e)} validateLineItem={(e) => this.validateLineItem(e)} gridFivth={() => this.gridFivth()} updatePoAmountNpoQuantity={(e) => this.updatePoAmountNpoQuantity(e)} updateLineItemChange={(e) => this.updateLineItemChange(e)} /> : null}
                {/* {this.state.imageModal ? <PiImageModal {...this.state} {...this.props} file={this.state.imageState} imageName={this.state.imageName} updateImage={(e) => this.updateImage(e)} imageModalAnimation={this.state.imageModalAnimation} closePiImageModal={(e) => this.closePiImageModal(e)} closeImageModal={(e) => this.openImageModal(e)} imageRowId={this.state.imageRowId} /> : null} */}
                {this.state.resetAndDelete ? <ResetAndDelete {...this.props} {...this.state} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeResetDeleteModal={(e) => this.closeResetDeleteModal(e)} DeleteRowPo={(e) => this.DeleteRowPo(e)} resetRows={(e) => this.resetRows(e)} /> : null}
                {this.state.deleteConfirmModal ? <DeleteModalPo closeDeleteConfirmModal={(e) => this.closeDeleteConfirmModal(e)} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} deleteLineItem={(e) => this.deleteLineItem(e)} handleRemoveSpecificSecRow={(e) => this.handleRemoveSpecificSecRow(e)} deleteGridId={this.state.deleteGridId} deleteSetNo={this.state.deleteSetNo} /> : null}
                {this.state.multipleErrorpo ? <AlertNotificationModal closeMultipleError={(e) => this.closeMultipleError(e)} lineError={this.state.lineError} /> : null}

            </div>
        );
    }
}
export default GenericPurchaseIndent;