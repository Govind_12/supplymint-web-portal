import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";

class ArticleName extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            checked: false,
            articleNameState: this.props.articleNameData,
            selectedId: "",
            id: "",
            addNew: true,
            save: false,
            mrp: "",
           
            rsp: "",
            done: false,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            searchMrp: "",
            toastLoader: false,
            sectionSelection: "",
            toastMsg: "",
            articleCode:this.props.articleCode,
            searchBy:"startWith"

        }

    }
    componentDidMount() {
  if(window.screen.width < 1200){      
           this.textInput.current.blur();   
         }else{  
            this.textInput.current.focus();}
  
    }

    componentWillMount() {
  
        if (this.props.purchaseIndent.articleName.isSuccess) {
            let c = []
            if (this.props.purchaseIndent.articleName.data.resource != null) {
                for (let i = 0; i < this.props.purchaseIndent.articleName.data.resource.length; i++) {


                    let x = this.props.purchaseIndent.articleName.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    articleNameState: c,
                    prev: this.props.purchaseIndent.articleName.data.prePage,
                    current: this.props.purchaseIndent.articleName.data.currPage,
                    next: this.props.purchaseIndent.articleName.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.articleName.data.maxPage,
                })

            }
            else {
                this.setState({
                    articleNameState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.articleName.isSuccess) {
            if (nextProps.purchaseIndent.articleName.data.resource != null) {
                this.setState({
                    articleNameState: nextProps.purchaseIndent.articleName.data.resource,
                    prev: nextProps.purchaseIndent.articleName.data.prePage,
                    current: nextProps.purchaseIndent.articleName.data.currPage,
                    next: nextProps.purchaseIndent.articleName.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.articleName.data.maxPage,
                })

            }
            else {
                this.setState({
                    articleNameState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

        if (nextProps.purchaseIndent.articleName.isSuccess) {

            this.setState({


                loader: false
            })
            this.props.articleNameRequest();

        } else if (nextProps.purchaseIndent.articleName.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.articleName.message.error == undefined ? undefined : nextProps.purchaseIndent.articleName.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.articleName.message.error == undefined ? undefined : nextProps.purchaseIndent.articleName.message.error.errorCode,
                code: nextProps.purchaseIndent.articleName.message.status,
                alert: true,

                loader: false

            })
            this.props.articleNameRequest();
        } else if (!nextProps.purchaseIndent.articleName.isLoading) {
            this.setState({
                loader: false
            })

        }

        if (nextProps.purchaseIndent.articleName.isLoading) {
            this.setState({
                loader: true
            })

        }

    }



    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }



    selectedData(id) {


        let c = this.props.purchaseIndent.articleName.data.resource;

        for (let i = 0; i < c.length; i++) {
            if (c[i].hl4Code == id) {
              this.setState({
                  articleCode:id
              })
              
            }
           
        }
        this.setState({
            articleNameState: c
        })

    }


    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.articleName.data.prePage,
                current: this.props.purchaseIndent.articleName.data.currPage,
                next: this.props.purchaseIndent.articleName.data.currPage + 1,
                maxPage: this.props.purchaseIndent.articleName.data.maxPage,
            })
            if (this.props.purchaseIndent.articleName.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.articleName.data.currPage - 1,
                    search: this.state.searchMrp,
                    hl3Code: this.props.hl3Code,
                    catDescUDF: this.props.catDescUDF,
                     searchBy:this.state.searchBy

                }
                this.props.articleNameRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.articleName.data.prePage,
                current: this.props.purchaseIndent.articleName.data.currPage,
                next: this.props.purchaseIndent.articleName.data.currPage + 1,
                maxPage: this.props.purchaseIndent.articleName.data.maxPage,
            })
            if (this.props.purchaseIndent.articleName.data.currPage != this.props.purchaseIndent.articleName.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.articleName.data.currPage + 1,

                    search: this.state.searchMrp,
                    hl3Code: this.props.hl3Code,
                    catDescUDF: this.props.catDescUDF,
                     searchBy:this.state.searchBy
                }
                this.props.articleNameRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.articleName.data.prePage,
                current: this.props.purchaseIndent.articleName.data.currPage,
                next: this.props.purchaseIndent.articleName.data.currPage + 1,
                maxPage: this.props.purchaseIndent.articleName.data.maxPage,
            })
            if (this.props.purchaseIndent.articleName.data.currPage <= this.props.purchaseIndent.articleName.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,

                    search: this.state.searchMrp,
                    hl3Code: this.props.hl3Code,
                    catDescUDF: this.props.catDescUDF,
                     searchBy:this.state.searchBy
                }
                this.props.articleNameRequest(data)
            }

        }
    }
    onDone() {
        let flag = false;
        let c = this.props.purchaseIndent.articleName.data.resource;
        let idd = "";

        let data = {}
        if (c != undefined) {
            for (let i = 0; i < c.length; i++) {
                if (c[i].hl4Code == this.state.articleCode) {
                                    data = {
                        articleName: c[i].hl4Name,
                        articleCode: c[i].hl4Code,

                    }
                    flag = true
                    break
                }
                else {
                    flag = false
                }

            }
        } else {
            this.setState({
                done: true
            })
        }

        if (flag) {
            this.setState({
                done: true,
                search: "",
            })



        } else {
            this.setState({
                done: false
            })
        }

        for (let i = 0; i < c.length; i++) {


            if (c[i].hl4Code == this.state.articleCode) {




                if (this.props.articleCat) {
                    this.props.updateArticle(data)
                } else {
                    this.props.updateArticleUdf(data)
                }




                this.onMrpCloseModal();
            } else {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }
        }

    }
    onMrpCloseModal(e) {
        this.setState({
            searchMrp: "",
            articleNameState: []
        })

        this.props.closeArticleSelection(e)

    }
    onSearch(e) {

        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    catDescUDF: this.props.catDescUDF,
                    search: this.state.searchMrp,
                     searchBy:this.state.searchBy
                }
                this.props.articleNameRequest(data)
            }
            else {

                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    catDescUDF: this.props.catDescUDF,
                    search: this.state.searchMrp,
                     searchBy:this.state.searchBy
                }
                this.props.articleNameRequest(data)
            }
        }
    }
    onsearchClear() {
        this.setState(
            {
                searchMrp: "",

            })
        if (this.state.type == 3) {

            var data = {

                type: "",
                no: 1,
                hl3Code: this.props.hl3Code,
                catDescUDF: this.props.catDescUDF,
                search: this.state.searchMrp,
                 searchBy:this.state.searchBy
            }
            this.props.articleNameRequest(data)
        }
    }

    handleChange(e) {
        if (e.target.id == "searchMrp") {
            this.setState({
                searchMrp: e.target.value
            })

        }else if(e.target.id == "searchByName"){
            this.setState({
                searchBy:e.target.value
            },()=>{
                if(this.state.searchMrp!=""){

                let data = {
                    type: this.state.type,
                    no: 1,
                    search: this.state.searchMrp,
                    hl3Code: this.props.hl3Code,
                    catDescUDF: this.props.catDescUDF,
                    searchBy:this.state.searchBy

                }
                this.props.articleNameRequest(data);
                }
            })
        }
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
        if(e.key ==='ArrowDown'){
            this.selectedData(this.props.purchaseIndent.articleName.data.resource[0].hl4Code)
        }
    }

    render() {
        const {
      rsp, articleCode, mrp, mrpEnd, mrpStart, searchMrp, sectionSelection
    } = this.state;
        return (

            <div className={this.props.articleNameModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.articleNameModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.articleNameModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.articleNameModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT ARTICLE</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only one articleName from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>


                                                    {/* <button className="btn" type="button" data-toggle="dropdown">Choose Column 
                                        <span className="glyphicon glyphicon-menu-down"></span></button>
                                        <ul className="dropdown-menu">
                                        <li><a href="#">Article Code</a></li>
                                        <li><a href="#">MRP</a></li>
                                        <li><a href="#">RSP</a></li>
                                        </ul> */}
                                                              {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ?     <select id="searchByName" name="searchByName" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                    
                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select>:null}

                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                    <label className="m-lft-15">
                                                        <button type="button" className="clearbutton" onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>



                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover cnameMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Article Code</th>
                                                    <th>Article Name</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.articleNameState == undefined || this.state.articleNameState == null || this.state.articleNameState.length == 0 ? <p className="modalNoData">No Data Found</p> : this.state.articleNameState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.hl4Code}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="articleNameCheck" id={data.hl4Code} checked={data.hl4Code==this.state.articleCode}  readOnly/>
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.hl4Code}</td>
                                                        <td className="pad-lft-8">{data.hl4Name}</td>

                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className={this.state.done ? "doneButton" : "doneButton_disable"} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" data-dismiss="modal" onClick={(e) => this.onMrpCloseModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 ? <li >
                                                <button className="PageFirstBtn" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.next - 1 != this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}


                                            {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>

        );
    }
}

export default ArticleName;
