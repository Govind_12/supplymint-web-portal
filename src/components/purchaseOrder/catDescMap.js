import React from 'react';
import SectionDataSelection from "../../components/purchaseIndent/sectionDataSelection";
import addMore from '../../assets/add.svg';
import PoError from '../loaders/poError';
import ToastLoader from '../loaders/toastLoader';
import CnameModal from './cnameModel';
import { CONFIG } from "../../config/index";
import axios from 'axios';
import ArticleName from './articleName';
class CatDescMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: false,
            filterBar: true,
            udfMappingData: [],
            isCompulsary: "",
            displayName: "",
            udfType: "",
            catDescData: this.props.catDescData,
            catDesc: this.props.catDesc,
            selectionModal: false,
            selectionAnimation: false,
            division: this.props.division,
            section: this.props.section,
            department: this.props.department,
            divisionState: this.props.divisionState,
            itemCatDesc: this.props.itemCatDesc,
            poErrorMsg: false,
            errorMassage: "",
            payloadId: [],
            displayNameCat: "",
            mapUdfCat: "",
            udfValueCat: "",
            ext: "N",
            udfName: "",
            hl3Name: "",
            displayNameCaterror: false,
            maperror: false,
            udfValueCaterror: false,
            itemUdfMappingData: [],
            toastLoader: false,
            toastMsg: "",
            codeRadio: this.props.codeRadioCat,
            description: this.props.descriptionCat,
            codeCat: this.props.codeCat,
            displayCodeCat: "",




            articleName: this.props.articleName,
            articleCode: this.props.articleCode,
            showArticle: this.props.showArticleCat,
            articleCheck: this.props.articleCheckCat,
            showArticleCheck: this.props.showArticleCheckCat,
            cnameModal: false,
            cnameModalAnimation: false,
            cnameData: [],
            catDescDept: this.props.catDescDept,
            showSecondCat: this.props.showSecondCat,
            articleNameModal: false,
            articleNameModalAnimation: false,
            articleCat: true,
            no: 1,
            type: this.props.catDescType,
            search: this.props.searchCatDesc,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            addNewImg: true,
            cnameValue: "",
            hl3Code: this.props.hl3Code,
            addValue: "false",
            dropdown: false,
            searchBy: "contains",
            exportToExcel: false

        };
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }
    componentDidMount() {
        window.setTimeout(function () {
            document.getElementById("chooseBtn").focus()
        }, 0)
    }
    updateState(upData) {
        var sectionData = []
        this.setState({
            division: upData.hl1Name,
            section: upData.hl2Name,
            department: upData.hl3Name,
            hl3Code: upData.hl3Code,
            articleCode: "",
            articleName: "",
            editDisplayName: upData.editDisplayName
        })
        this.props.chooseDataCat(upData)
        window.setTimeout(() => {
            document.getElementById("dropdownMenu1").focus()
        }, 0)
    }
    closeSelection(e) {
        this.setState({
            selectionModal: false,
            selectionAnimation: !this.state.selectionAnimation
        });
        window.setTimeout(() => {
            document.getElementById("chooseBtn").focus();
        }, 0);
    }
    openSelection(e) {
        var data = {
            no: 1,
            type: "",
            hl1name: "",
            hl2name: "",
            hl3name: "",
            search: ""
        }
        this.props.divisionSectionDepartmentRequest(data);
        this.setState({
            department: this.state.department,
            selectionModal: true,
            selectionAnimation: !this.state.selectionAnimation
        });
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return false;
            }
        }
    }
    componentWillMount() {
        this.setState({
            catDescData: this.props.catDescData,
            divisionState: this.props.divisionState,
            division: this.props.division,
            section: this.props.section,
            department: this.props.department,
            itemCatDesc: this.props.itemCatDesc,
            type: this.props.catDescType,
            search: this.props.searchCatDesc,
            hl3Code: this.props.hl3Code
        })

        if (this.props.purchaseIndent.itemCatDescUdf.isSuccess) {
            if (this.props.purchaseIndent.itemCatDescUdf.data.resource != null) {
                this.setState({
                    prev: this.props.purchaseIndent.itemCatDescUdf.data.prePage,
                    current: this.props.purchaseIndent.itemCatDescUdf.data.currPage,
                    next: this.props.purchaseIndent.itemCatDescUdf.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.itemCatDescUdf.data.maxPage,
                })
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        const t = this
        setTimeout(function () {
            t.setState({
                catDescData: t.props.catDescData,
                itemCatDesc: t.props.itemCatDesc,
                type: t.props.catDescType,
                search: t.props.searchCatDesc,
            })
        }, 10)

        if (nextProps.purchaseIndent.cname.isSuccess) {
            if (nextProps.purchaseIndent.cname.data.resource != null) {
                let c = []
                for (let i = 0; i < nextProps.purchaseIndent.cname.data.resource.length; i++) {
                    let x = nextProps.purchaseIndent.cname.data.resource[i];
                    x.checked = false;
                    let a = x
                    c.push(a)
                }
                this.setState({
                    cnameData: c
                })
            }
        }

        if (nextProps.purchaseIndent.itemCatDescUdf.isSuccess) {
            if (nextProps.purchaseIndent.itemCatDescUdf.data.resource != null) {
                this.setState({
                    prev: nextProps.purchaseIndent.itemCatDescUdf.data.prePage,
                    current: nextProps.purchaseIndent.itemCatDescUdf.data.currPage,
                    next: nextProps.purchaseIndent.itemCatDescUdf.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.itemCatDescUdf.data.maxPage,
                })
            }
            else {
                this.setState({
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }
        if (nextProps.purchaseIndent.updateItemCatDesc.isSuccess) {
            this.setState({
                payloadId: []
            })
        }
    }

    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
        this.props.searchCatDescFun(e.target.value)
    }
    onSearch(e) {
        if (this.state.search == "" || this.state.search == " ") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.props.updatePiType(3);
            let data = {
                type: 3,
                description: this.state.codeRadio == "catDesc" ? this.state.description : "",
                itemUdfType: this.state.catDesc,
                ispo: "false",
                no: 1,
                search: this.props.searchCatDesc,
                hl3Code: this.state.hl3Code,
                hl4Code: this.props.articleCode,
                hl4Name: this.props.articleName,
                searchBy: this.state.searchBy
            }
            this.props.itemCatDescUdfRequest(data)
        }
    }

    onnChange(name) {
        this.setState({
            catDesc: name,
            articleName: "",
            articleCode: "",
            dropdown: false
        })
        let data = {
            catDesc: name,

        }
        this.props.updateCat(data)

    }

    onnChangeDept(name) {
        this.setState({
            catDescDept: name,
            articleName: "",
            articleCode: ""
        })
        let data = {
            catDesc: name
        }
        this.props.updateCatDept(data)
    }

    getAllData() {
        if (this.state.codeRadio == "catDesc") {
            if (this.state.department != "" && this.state.catDesc != "") {
                var data = {
                    type: 1,
                    no: 1,
                    description: "",
                    itemUdfType: this.state.catDesc,
                    ispo: "false",
                    search: this.props.searchCatDesc,
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy
                }
                this.props.itemCatDescUdfRequest(data)
            } else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Department and Cat/Desc is Mandatory"
                })
            }
        } else {
            if (this.state.department != "" && this.state.catDescDept != "") {
                var data = {
                    type: 1,
                    no: 1,
                    description: this.state.description,
                    itemUdfType: this.state.catDescDept,
                    ispo: "false",
                    search: this.props.searchCatDesc,
                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy
                }
                this.props.itemCatDescUdfRequest(data)
            } else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Department and Cat/Desc is Mandatory"
                })
            }
        }
    }

    // __________________________________HANDLECHANGE_______________________

    openDescription(value) {
        if (this.state.catDesc != "") {
            let data = {
                no: 1,
                type: 1,
                search: "",
                udfType: this.state.catDesc,
            }
            this.props.cnameRequest(data);
            this.setState({
                cnameValue: value == "true" ? this.state.displayNameCat : this.state.description,
                cnameModal: true,
                cnameModalAnimation: !this.state.cnameModalAnimation,
                addValue: value
            })
        } else {
            this.setState({
                toastLoader: true,
                toastMsg: "select  CAT/DESC",
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 2000)
        }
    }
    updateDescription(data) {
        if (this.state.addValue == "true") {
            this.setState({
                displayNameCat: data.cname,
                displayCodeCat: data.code


            })
        } else {
            this.setState({
                description: data.cname,
                codeCat: data.code
            })
            let payload = {
                cname: data.cname,
                code: data.code
            }
            this.props.descriptionValueCat(payload)
        }
        window.setTimeout(() => {
            document.getElementById("chooseBtn").focus();
        })
    }
    closeDescription() {
        this.setState({
            cnameModal: false,
            cnameModalAnimation: !this.state.cnameModalAnimation
        })
        window.setTimeout(() => {
            document.getElementById("description").focus()
        })
    }
    handleChange(id, e) {
        let r = this.state.itemCatDesc
        let payloadId = this.state.payloadId
        if (!payloadId.includes(id)) {
            payloadId.push(id)
        }
        this.setState({
            payloadId: payloadId
        })
        if (e.target.id == "nameUdf") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    r[i].name = e.target.value
                }
            }
        } else if (e.target.id == "displayNameUdf") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    r[i].displayName = e.target.value
                }
            }
        } else if (e.target.id == "udfType") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    r[i].udfType = e.target.value
                }
            }
        } else if (e.target.id == "value") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    r[i].value = e.target.value
                }
            }
        } else if (e.target.id == "map") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    r[i].map = e.target.value
                }
            }
        }
        this.setState({
            itemCatDesc: r
        })
    }
    handleCheck(id, value) {
        let r = this.state.itemCatDesc
        let payloadId = this.state.payloadId
        if (!payloadId.includes(id)) {
            payloadId.push(id)
        }
        this.setState({
            payloadId: payloadId
        })
        if (value == "ext") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    if (r[i].ext == "N") {
                        r[i].ext = "Y"
                    } else {
                        r[i].ext = "N"
                    }
                }
            }
            this.setState({
                itemCatDesc: r
            })
        } else if (value == "map") {
            for (var i = 0; i < r.length; i++) {
                if (r[i].id == id) {
                    if (r[i].map == "N") {
                        r[i].map = "Y"
                    } else {
                        r[i].map = "N"
                    }
                }
            }
            this.setState({
                itemCatDesc: r
            })
        }
        // }
    }

    // ____________________-------itemudfmapping_______________________-----------
    addNew() {
        if (this.state.catDesc == "" || this.state.department == "") {
            this.setState({
                toastMsg: "Select Cat/Desc and Department",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            this.setState({
                newItemUdf: true
            })
        }

    }
    onClose() {
        this.setState({

            newItemUdf: false,
            displayNameCat: "",
            udfValueCat: "",
            mapUdfCat: "",
        })

    }
    onSaveNewData() {
        this.udfValueCatVali();
        this.displayNameVal();
        const t = this;
        setTimeout(function () {
            const { displayNameCaterror, maperror, udfValueCaterror } = t.state;
            if (!displayNameCaterror) {
                t.setState({
                    newItemUdf: false,
                })
                let itemUdfMappingData = []
                let dataItemUdf = {
                    hl3code: t.state.hl3Code,
                    hl3Name: t.state.department,
                    hl4code: t.props.articleCode,
                    hl4Name: t.props.articleName,
                    cat_desc_udf: t.state.catDesc,
                    displayName: "",
                    ext: "N",
                    map: "Y",
                    description: t.state.displayNameCat,
                    code: t.state.displayCodeCat
                }
                itemUdfMappingData.push(dataItemUdf)
                let payload = {
                    isInsert: true,
                    itemUdfMappingData: itemUdfMappingData
                }
                t.props.updateItemCatDescRequest(payload);
                t.onClear();
            }
        }, 100)
    }

    // _____________________________ERROR VALIDATION____________________________________

    handleRadioChange(e) {
        let t = this;
        this.setState({
            codeRadio: e.target.value,
        })

        this.props.updateCatDescRadio(e.target.value);
        if (e.target.value == "deptWise") {
            this.setState({
                showSecondCat: true,
                description: "",
                catDesc: "",
                newItemUdf: false
            })
            document.getElementById("description").disabled = true
            let data = {
                showSecondCat: true,
                description: "",
                catDesc: ""
            }
            this.props.showSecondCatUpdate(data)
        } else {

            this.setState({
                showSecondCat: false,
                description: "",
                catDesc: "",
                addNewImg: true
            })
            document.getElementById("description").disabled = false
            let data = {
                showSecondCat: false,
                description: "",
                catDesc: ""
            }
            this.props.showSecondCatUpdate(data)
        }

        setTimeout(function () {
            t.onClear(e);

        }, 10)

    }
    displayNameVal() {
        if (this.state.displayNameCat == "") {
            this.setState({
                displayNameCaterror: true
            })
        } else {
            this.setState({
                displayNameCaterror: false
            })
        }
    }

    mapUdfCatVal() {
        if (this.state.mapUdfCat == "") {
            this.setState({
                maperror: true
            })
        } else {
            this.setState({
                maperror: false
            })
        }
    }

    udfValueCatVali() {
        if (this.state.udfValueCat == "") {
            this.setState({
                udfValueCaterror: true
            })
        } else {
            this.setState({
                udfValueCaterror: false
            })
        }
    }

    handleChangeUpdate(e) {

        if (e.target.id == "displayNameCat") {

            this.setState({
                displayNameCat: e.target.value
            },
                () => {
                    this.displayNameVal()
                }
            )

        }
        else if (e.target.id == "mapUdfCat") {

            this.setState({
                mapUdfCat: e.target.value
            },
                () => {
                    this.mapUdfCatVal()
                }
            )

        } else if (e.target.id == "udfValueCat") {

            this.setState({
                udfValueCat: e.target.value
            },
                () => {
                    this.udfValueCatVali()
                }
            )

        }

    }

    onClear() {
        this.setState({
            displayNameCat: "",
            udfValueCat: "",
            mapUdfCat: "",
            showArticle: false,
            articleCheck: false

        })
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }


    onSubmit() {
        let itemCatDesc = this.state.itemCatDesc
        let payloadId = this.state.payloadId
        const t = this

        if (payloadId.length != 0) {
            let dataUdf = []
            itemCatDesc.forEach(udf => {
                if (payloadId.includes(udf.id.toString())) {
                    let payloadData = {
                        hl3code: t.state.hl3Code,
                        hl3Name: t.state.department,
                        hl4code: t.props.articleCode,
                        hl4Name: t.props.articleName,
                        cat_desc_udf: t.state.codeRadio == "deptWise" ? t.state.catDescDept : t.state.catDesc,
                        displayName: udf.displayName,
                        ext: udf.ext,
                        map: udf.map,
                        description: udf.description,
                        code: udf.code != undefined ? udf.code : ""

                    }
                    dataUdf.push(payloadData)
                }

            })
            let payloadMapping = {
                isInsert: false,
                itemUdfMappingData: dataUdf
            }

            this.props.updateItemCatDescRequest(payloadMapping)
            this.onClear();
        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "No changes in data"
            })

        }
    }

    onCheck() {
        if (this.state.articleCheck) {
            this.setState({
                articleCheck: false,
                showArticle: false
            })
            let Adata = {
                articleCheck: false,
                showArticle: false
            }
            this.props.checkCatUpdate(Adata)
        } else {
            this.setState({
                articleCheck: true,
                showArticle: true

            })
            let datta = {
                articleCheck: true,
                showArticle: true
            }
            this.props.checkCatUpdate(datta)
        }
    }

    openArticleSelection() {
        if (this.state.department != "") {
            if (this.state.codeRadio == "deptWise" ? this.state.catDescDept != "" : this.state.catDesc != "") {
                let data = {
                    no: 1,
                    type: 1,
                    search: "",
                    hl3Code: this.state.hl3Code,
                    catDescUDF: this.state.codeRadio == "deptWise" ? this.state.catDescDept : this.state.catDesc
                }
                this.props.articleNameRequest(data);
                this.setState({
                    articleNameModal: true,
                    articleNameModalAnimation: !this.state.articleNameModalAnimation
                })
            } else {
                this.setState({
                    toastLoader: true,
                    toastMsg: "Select  CAT/DESC",
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)
            }
        } else {
            this.setState({
                toastLoader: true,
                toastMsg: "Select  Department",
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 2000)
        }
    }
    onClearSearch() {
        this.props.searchCatDescFun("")
        this.props.updatePiType(1)
        var data = {
            type: 1,
            no: 1,
            description: this.state.codeRadio == "catDesc" ? this.state.description : "",
            itemUdfType: this.state.catDesc,
            ispo: "false",

            search: this.props.searchCatDesc,

            hl3Code: this.state.hl3Code,
            hl4Code: this.props.articleCode,
            hl4Name: this.props.articleName,
            searchBy: this.state.searchBy
        }
        this.props.itemCatDescUdfRequest(data)


    }
    closeArticleSelection() {

        this.setState({

            articleNameModal: true,
            articleNameModalAnimation: !this.state.articleNameModalAnimation
        })

    }
    updateArticle(data) {

        this.setState({
            articleName: data.articleName,
            articleCode: data.articleCode
        })
        let ddata = {
            articleName: data.articleName,
            articleCode: data.articleCode
        }
        this.props.updateArticle(ddata)

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.itemCatDescUdf.data.prePage,
                current: this.props.purchaseIndent.itemCatDescUdf.data.currPage,
                next: this.props.purchaseIndent.itemCatDescUdf.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemCatDescUdf.data.maxPage,
            })
            if (this.props.purchaseIndent.itemCatDescUdf.data.currPage != 0) {
                let data = {

                    no: this.props.purchaseIndent.itemCatDescUdf.data.currPage - 1,
                    type: this.state.type,
                    description: this.state.codeRadio == "catDesc" ? this.state.description : "",
                    itemUdfType: this.state.catDesc,
                    ispo: "false",

                    search: this.props.searchCatDesc,

                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy


                }
                this.props.itemCatDescUdfRequest(data);
            }

        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.itemCatDescUdf.data.prePage,
                current: this.props.purchaseIndent.itemCatDescUdf.data.currPage,
                next: this.props.purchaseIndent.itemCatDescUdf.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemCatDescUdf.data.maxPage,
            })
            if (this.props.purchaseIndent.itemCatDescUdf.data.currPage != this.props.purchaseIndent.itemCatDescUdf.data.maxPage) {
                let data = {

                    no: this.props.purchaseIndent.itemCatDescUdf.data.currPage + 1,
                    type: this.state.type,
                    description: this.state.codeRadio == "catDesc" ? this.state.description : "",
                    itemUdfType: this.state.catDesc,
                    ispo: "false",

                    search: this.state.search,

                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy


                }
                this.props.itemCatDescUdfRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.itemCatDescUdf.data.prePage,
                current: this.props.purchaseIndent.itemCatDescUdf.data.currPage,
                next: this.props.purchaseIndent.itemCatDescUdf.data.currPage + 1,
                maxPage: this.props.purchaseIndent.itemCatDescUdf.data.maxPage,
            })
            if (this.props.purchaseIndent.itemCatDescUdf.data.currPage <= this.props.purchaseIndent.itemCatDescUdf.data.maxPage) {
                let data = {

                    no: 1,
                    type: this.state.type,
                    search: this.props.searchCatDesc,
                    description: this.state.codeRadio == "catDesc" ? this.state.description : "",
                    itemUdfType: this.state.catDesc,
                    ispo: "false",

                    hl3Code: this.state.hl3Code,
                    hl4Code: this.props.articleCode,
                    hl4Name: this.props.articleName,
                    searchBy: this.state.searchBy


                }
                this.props.itemCatDescUdfRequest(data)
            }

        }
    }


    searchFocus(e) {
        if (e.key == "Enter") {
            this.getAllData();
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("searchBtn").blur();
                document.getElementById("search").focus()
            }, 0);
        }
    }

    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {

            document.getElementById(idd).click();
        }
        if (e.key === "Enter" && id == "description") {

            document.getElementById(idd).click();
        }
        if (e.key === "Enter" && id == "chooseval") {

            document.getElementById(idd).click();
        }
        if (e.key == "Enter" && idd == "dropdownMenu1") {
            this.setState({
                catDesc: this.state.catDescData[0].name
            })
            window.setTimeout(() => {
                document.getElementById(this.state.catDescData[0].name).focus()
            })
        }
        if (id == "description" && e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("addmore").focus()
            })
        } else if (id == "chooseBtn" && e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("searchBtn").focus();
            }, 0);
        }

    }

    focusSave(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id, "map")
        }

    }
    onExtKeyDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id, "ext")
        }
        if (e.key === "Tab") {
            if (this.state.itemCatDesc[this.state.itemCatDesc.length - 1].id == id && this.state.payloadId.length == 0) {
                window.setTimeout(() => {
                    document.getElementById("dropdownMenu1").focus()
                }, 0);
            } else if (this.state.itemCatDesc[this.state.itemCatDesc.length - 1].id == id && this.state.payloadId.length != 0) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemCatDesc[0].id).blur();
                    document.getElementById("saveButton").focus();
                }, 0);
            }
        }
    }
    focusOnCatDesc(e) {
        if (e.key === "Enter") {
            this.onSubmit()
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("dropdownMenu1").focus();
                e.preventDefault()
            }, 0)
        }
    }
    searchKeyDown(e) {
        if (e.key === "Tab" && this.state.itemCatDesc == null || this.state.itemCatDesc == "") {
            document.getElementById("dropdownMenu1").focus()
            e.preventDefault()
        } else {
            if (this.state.itemCatDesc != null) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemCatDesc[0].map).focus();
                })
            }
        }
    }
    ArrowDown(id, e) {
        if (e.key === "Enter") {
            this.onnChange(id)
            window.setTimeout(() => {
                document.getElementById("description").focus()
            }, 0);
        }
        if (e.key === "ArrowUp" || e.key === "ArrowDown") {
            let catDescData = this.state.catDescData
            var index = ""
            var nextId = ""
            for (let i = 0; i < catDescData.length; i++) {
                if (id == catDescData[i].name) {
                    index = i
                }
                if (index > 0 && e.key === "ArrowUp") {
                    nextId = catDescData[index - 1].name
                }
                if (index != catDescData[i].length && e.key === "ArrowDown" && id != this.state.catDescData[this.state.catDescData.length - 1].name) {
                    nextId = catDescData[index + 1].name
                }
            }
            if (nextId != "") {
                document.getElementById(nextId).focus()
            }
        }
    }
    openDrop(e) {
        this.setState({
            dropdown: true
        })
    }
    focusOnButton(e) {
        if (e.key === "Enter") {
            this.addNew();
        }
        if (e.key === "Tab") {
            if (this.state.newItemUdf) {
                window.setTimeout(() => {
                    document.getElementById("chooseval").focus()
                })
            } else {
                window.setTimeout(() => {
                    document.getElementById("chooseBtn").focus()
                })
            }
        }
    }

    handleChange(id, e) {
        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {
            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })
        let itemCatDesc = this.state.itemCatDesc
        for (let i = 0; i < itemCatDesc.length; i++) {
            if (itemCatDesc[i].id == id) {
                itemCatDesc[i].displayName = e.target.value
            }
        }
        this.setState({
            itemCatDesc: itemCatDesc
        })
    }

    _handleKeyPressSearch = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    xlscsv(type) {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }

        axios.get(`${CONFIG.BASE_URL}/admin/po/export/mappings/data?catDescUdfType=${this.state.catDesc}&hl1Code=&hl2Code=&hl3Code=${this.state.hl3Code}`, { headers: headers })
            .then(res => {
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
            });

    }
    handleChangeStart(e) {
        if (e.target.id == "searchBycatDesc") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {

                        no: 1,
                        type: this.state.type,
                        description: this.state.codeRadio == "catDesc" ? this.state.description : "",
                        itemUdfType: this.state.catDesc,
                        ispo: "false",

                        search: this.state.search,

                        hl3Code: this.state.hl3Code,
                        hl4Code: this.props.articleCode,
                        hl4Name: this.props.articleName,
                        searchBy: this.state.searchBy


                    }
                    this.props.itemCatDescUdfRequest(data)
                }
            })
        }
    }
    render() {
        const { displayNameCaterror, displayNameCat, description, articleName, articleCheck, showArticle } = this.state
        return (
            <div className="p-lr-47">
                <div className="col-md-12 col-sm-12 m-top-20 pad-0">

                    <div className="col-md-2 pad-0 piFormDiv">
                        <label className="purchaseLabel">
                            Division
                                    </label>
                        <input type="text" className="purchaseSelectBox" value={this.state.division} placeholder="Select  Division" disabled />
                    </div>
                    <div className="col-md-2 pad-0 piFormDiv">
                        <label className="purchaseLabel">
                            Section
                                    </label>
                        <input type="text" className="purchaseSelectBox" value={this.state.section} placeholder="Choose Section" disabled /></div>
                    <div className="piFormDiv col-md-2 pad-0">
                        <label className="purchaseLabel">
                            Department
                                    </label>
                        <input type="text" className="purchaseSelectBox" value={this.state.department} placeholder="Choose Department" disabled />

                    </div>

                    <div className="col-md-1 pad-0 piFormDiv m-top-17">
                        <button className="dataPiBtn width100" type="button" onKeyDown={(e) => this._handleKeyPress(e, "chooseBtn")} id="chooseBtn" onClick={(e) => this.openSelection(e)}>
                            Choose Data
                                </button>
                    </div>
                    <div className="col-md-2 pad-0 piFormDiv udfCheck assortmentCheck m-top-15">
                        <label className="checkBoxLabel0"><input type="checkBox" id="articleCheck" checked={articleCheck} onChange={(e) => this.onCheck(e)} />Article Level Mapping <span className="checkmark1"></span> </label>
                    </div>


                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 lineAfterBg">
                    <div className="col-md-8 col-sm-12 m-top-20 pad-0">
                        <div className="col-md-3 pad-lft-0">
                            {this.state.codeRadio == "catDesc" ? <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown" id="settingDrop">
                                <button className=" dropdown-toggle userModalSelect btnFocus" type="button" onClick={(e) => this.openDrop(e)} onKeyDown={(e) => this._handleKeyPress(e, "dropdownMenu1")} id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {this.state.catDesc != "" ? this.state.catDesc : "Select CAT/DESC"}
                                    <i className="fa fa-chevron-down"></i>
                                </button>
                                {this.state.dropdown ? <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    {this.state.catDescData.length != 0 ? this.state.catDescData.map((data, i) => <li tabIndex="-1" value={data.name} id={data.name} key={i} onKeyDown={(e) => this.ArrowDown(`${data.name}`, e)} onClick={(e) => this.onnChange(`${data.name}`)} >
                                        <a >    {data.name}</a>
                                    </li>) : "No Data Found"}
                                </ul> : null}
                            </div> :
                                <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown">
                                    <button className="btn btn-default dropdown-toggle userModalSelect btnDisabled pointerNone itemUdfDisable" type="button" id="dropdownMenu1" >
                                        Select CAT/DESC
                                        <i className="fa fa-chevron-down"></i>
                                    </button>
                                </div>}
                        </div>
                        <div className="col-md-3 pad-lft-0">
                            <div className="inputTextKeyFucMain" >
                                {this.state.codeRadio == "catDesc" ? <input autoComplete="off" type="text" className="inputTextKeyFuc onFocus" onKeyDown={(e) => this._handleKeyPress(e, "description")} id="description" value={description} placeholder="Choose Description" onClick={(e) => this.openDescription("false")} /> :
                                    <input autoComplete="off" type="text" className="inputTextKeyFuc btnDisabled" value={description} placeholder="Choose Description" disabled />}
                                {this.state.codeRadio == "catDesc" ? <span className={this.state.codeRadio == "deptWise" ? "modalBlueBtn pointerNone" : "modalBlueBtn"} onClick={(e) => this.openDescription("false")}>
                                    ▾
                                </span> : <span className={this.state.codeRadio == "deptWise" ? "modalBlueBtn pointerNone" : "modalBlueBtn"} >
                                        ▾
                                </span>}
                            </div>
                        </div>
                        <div className="col-md-6 pad-lft-0 selectFrequencyCustom setUdfRadio">
                            <ul className="pad-0 lineAfter">
                                <li className="m0"> <label className="select_modalRadio">
                                    <input type="radio" id="catDesc" checked={this.state.codeRadio === "catDesc"} name="catDesc" value="catDesc" onChange={e => this.handleRadioChange(e)} /><span>CAT/DESC</span>
                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                </label>
                                </li>
                                <li className="m0"><label className="select_modalRadio">
                                    <input type="radio" id="deptWise" checked={this.state.codeRadio === "deptWise"} name="deptWise" value="deptWise" onChange={e => this.handleRadioChange(e)} /><span>Dept Wise</span>
                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                </label></li>
                                {this.state.frequencyerr ? <span className="error m-top-10">Select Frequency</span> : null}

                                {this.state.codeRadio === "catDesc" ? <li className="m0">
                                    <button onClick={() => this.addNew()} onKeyDown={(e) => this.focusOnButton(e)} id="addmore" className="pad-lft-15 displayPointer addmore" >
                                        <img className="displayPointer" src={require('../../assets/add-green.svg')} />
                                        <span className="generic-tooltip">Add New</span>
                                    </button>
                                </li> : null}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-40">
                    {this.state.newItemUdf ? <div className="col-md-12 col-sm-12 pad-0">
                        <div className="col-md-12 pad-0">
                            {this.state.catDesc == "CAT1" || this.state.catDesc == "CAT2" || this.state.catDesc == "CAT3"
                                || this.state.catDesc == "CAT4" || this.state.catDesc == "CAT5" || this.state.catDesc == "CAT6" ?
                                <div className="col-md-2 inputTextKeyFucMain pad-0" >
                                    <input autoComplete="off" type="text" className="inputTextKeyFuc" onKeyDown={(e) => this._handleKeyPress(e, "chooseval")} id="chooseval" value={displayNameCat} placeholder="Choose value" onClick={(e) => this.openDescription("true")} />
                                    <span className={this.state.codeRadio == "deptWise" ? "modalBlueBtn pointerNone" : "modalBlueBtn"} onClick={(e) => this.openDescription("true")}>
                                        ▾
                                </span>
                                </div> :
                                <div className="col-md-2 pad-lft-0">
                                    <input type="text" value={displayNameCat} id="displayNameCat" placeholder="Enter name" className={displayNameCaterror ? "errorBorder orgnisationTextbox width100" : "orgnisationTextbox width100"} onChange={(e) => this.handleChangeUpdate(e)} />
                                    {this.state.displayNameCaterror ? <span className="error">Enter Name</span> : null}</div>
                            }
                            <div>
                            </div>
                        </div>
                        <div className="col-md-4 m-top-20 pad-0">
                            <button type="button" className="save2_button_vendor m-rgt-10" onClick={() => this.onSaveNewData()}>Save</button>
                            <button type="button" className="clear_button_vendor m-rgt-10" onClick={() => this.onClear()}>Clear</button>
                            <button type="button" className="cancel_button_vendor" onClick={() => this.onClose()}> Cancel</button>
                        </div>

                    </div> : null}
                </div>

                <div className="col-md-12 col-sm-12 pad-0">
                    {this.state.showSecondCat ? <div className="col-md-2 pad-0 m-top-15 piFormDiv">
                        <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown">
                            <button className="btn btn-default dropdown-toggle userModalSelect" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                {this.state.catDescDept != "" ? this.state.catDescDept : "Select CAT/DESC"}
                                <i className="fa fa-chevron-down"></i>
                            </button>

                            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                {this.state.catDescData.length != 0 ? this.state.catDescData.map((data, i) => <li value={data.name} id={data.name} key={i} onClick={(e) => this.onnChangeDept(`${data.name}`)} >
                                    <a >    {data.name}</a>
                                </li>) : "No Data Found"}

                            </ul>

                        </div>
                    </div> : null}
                    {showArticle ? <div className="col-md-2 pad-0 piFormDiv m-top-15">
                        <div className="inputTextKeyFucMain" >
                            <input autoComplete="off" type="text" className="inputTextKeyFuc btnFocus" onKeyDown={(e) => this._handleKeyPress(e, "articleName")} id="articleName" value={articleName} placeholder="Choose Article Name" onClick={(e) => this.openArticleSelection(e)} />
                            <span className="modalBlueBtn" onClick={(e) => this.openArticleSelection(e)}>
                                ▾
</span>
                        </div>
                    </div> : null}

                </div>
                <div className="col-md-12 col-sm-12 pad-0">
                    <div className="col-md-6 m-top-20 pad-0 ">
                        <button className="search_button_vendor" id="searchBtn" type="button" onKeyDown={(e) => this.searchFocus(e)} onClick={() => this.getAllData()}>Search</button>
                    </div>

                </div>
                <div className="col-md-12 pad-0">
                    <div className="col-md-1 pad-0">
                        <div className="exportDropDown settingDrop m-top-35 dropdown displayInline pad-0 width100 udfDropDown" id="settingDrop">
                            {/* {this.state.codeRadio == "catDesc" && (this.state.catDesc != "" || this.state.hl3Code != "") ?
                                <button type="button" onClick={(e) => this.xlscsv()} className="button_home">
                                    EXPORT
                                    </button> : <button type="button" className="button_home btnDisabled">
                                    EXPORT
                                    </button>} */}
                                    <div className="gvpd-download-drop2">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={(e) => this.xlscsv()}>
                                                    {/* <img src={ExportExcel} /> */}
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                        </ul>}
                                </div>
                            {/*<button className="width145 dropdown-toggle userModalSelect btnFocus" type="button" id="dropExport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Export
                                    <i className="fa fa-chevron-down"></i>
                            </button>
                            <ul className="dropdown-menu width145 left0" aria-labelledby="dropExport">
                                <li tabIndex="-1" className="liFocus">
                                    <label className=" checkBoxLabelPad checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" /> <span className="checkmark1"></span> data.name</label>
                                </li>
                                <li tabIndex="-1" className="liFocus">
                                    <label className=" checkBoxLabelPad checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" /> <span className="checkmark1"></span> data.name</label>
                                </li>
                                <li tabIndex="-1" className="liFocus">
                                    <label className=" checkBoxLabelPad checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" /> <span className="checkmark1"></span> data.name</label>
                                </li>
                                <div className="dropdownFooter">
                                    <button type="button">Download</button>
                                </div>
                            </ul>*/}
                        </div>
                        {/*<button type="button">Export</button>*/}

                    </div>
                    <div className="col-md-5 "></div>
                    <div className="col-md-6 col-sm-12 pad-0">

                        <ul className="list-inline search_list manageSearch">
                            {/* <form onSubmit={(e) => this.onSearch(e)}> */}
                                <li>
                                {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ?    <select id="searchBycatDesc" name="searchBycatDesc" value={this.state.searchBy} onChange={(e) => this.handleChangeStart(e)}>
                                   
                                    <option value="contains">Contains</option>
                                    <option value="startWith"> Start with</option>
                                    <option value="endWith">End with</option>

                                </select>:null}

                                </li>


                            <li className="width-60per">
                              
                                <input type="search" id="search" onKeyPress={this._handleKeyPressSearch} onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                <button className={this.state.itemCatDesc == null || this.state.itemCatDesc.length == 0 ? "btnDisabled searchWithBar" : "searchWithBar btnFocuss"} onKeyDown={(e) => this.searchKeyDown(e)} onClick={(e) => this.onSearch(e)}> Search
                                        <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                            <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                            </path>
                                        </svg>
                                </button>
                            </li>
                            {/* </form> */}
                        </ul>
                        {this.state.type == 3 ? <button className="clearSearchFilter focusClear btnFocuss" id="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</button> : null}
                    </div>
                </div>

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric siteMapppingMain udfMappingTable itemUdfTable">
                    <div className="tableHeadFix pipo-con-table">
                        <div className="scrollableTableFixed table-scroll scrollableOrgansation tableHeadFixHeight">
                            <table className="table zui-table sitemappingTable gen-main-table">
                                <thead >
                                    <tr>
                                        <th><label>Name</label></th>
                                        <th><label>Display Name</label></th>
                                        <th><label>Map</label></th>
                                        <th><label>Ext</label></th>
                                    </tr>
                                </thead>

                                <tbody>

                                    {this.state.itemCatDesc == null || this.state.itemCatDesc.length == 0 ? <tr className="tableNoData"><td colSpan="6" className="border-bot0"> NO DATA FOUND </td></tr> : this.state.itemCatDesc.map((data, idxxx) => (
                                        <tr id={idxxx} key={idxxx}>

                                            <td>
                                                <label>{data.description}</label>
                                            </td>

                                            <td>
                                                {this.props.editDisplayName == "true" ? <input autoComplete="off" type="text" className="purchaseOrderSelectBox modal_tableBox borderR0" value={data.displayName == null ? "" : data.displayName} onChange={(e) => this.handleChange(`${data.id}`, e)} />
                                                    : <label>{data.displayName}</label>}
                                            </td>

                                            <td className="col-md-12 col-sm-12 pad-0">
                                                <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id={data.map} onChange={(e) => this.handleCheck(`${data.id}`, "map")} onKeyDown={(e) => this.focusSave(`${data.id}`, e)} checked={data.map == "N" ? false : true} /> <span className="checkmark1"></span> </label>
                                            </td>

                                            <td className="col-md-12 col-sm-12 pad-0">
                                                <label className={"checkBoxLabel0 displayPointer checkBoxFocus"}><input className="checkBoxTab" type="checkBox" id={data.ext} onChange={(e) => this.handleCheck(`${data.id}`, "ext")} onKeyDown={(e) => this.onExtKeyDown(`${data.id}`, e)} checked={data.ext == "N" ? false : true} /> <span className="checkmark1"></span> </label>
                                            </td>
                                        </tr>))}
                                </tbody>

                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" min="1" value="1" />
                                        {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <div className="pad-right-15 wthauto">
                                            <ul className="list-inline pagination width100">
                                                {this.state.current == 1 || this.state.current == 0 ? <li >
                                                    <button className="PageFirstBtn pointerNone" >
                                                        First
                                                    </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                                            First
                                                    </button>
                                                    </li>}
                                                {this.state.prev != 0 ? <li >
                                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                        Prev
                                                    </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn" disabled>
                                                            Prev
                                                        </button>
                                                    </li>}
                                                <li>
                                                    <button className="PageFirstBtn pointerNone">
                                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                                    </button>
                                                </li>
                                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                        Next
                                                    </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn borderNone" disabled>
                                                            Next
                                                        </button>
                                                    </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                                                    </button>
                                                </li>}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="col-md-12 col-sm-12 pad-0">
                    <div className="footerDivForm">
                        <ul className="list-inline m-lft-0 m-top-10">

                            {this.state.itemCatDesc == null || this.state.itemCatDesc.length == 0 ? <li><button type="button" className="save_button_vendor btnDisabled" id="saveButton">Save</button></li> :
                                <li>{this.state.payloadId.length == 0 ? <button type="button" className=" btnDisabled save_button_vendor " >Save</button>
                                    : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.focusOnCatDesc(e)} className="save_button_vendor " id="saveButton">Save</button>}
                                </li>}

                        </ul>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.selectionModal ? <SectionDataSelection {...this.props} department={this.state.department} divisionState={this.state.divisionState} sectionCloseModal={(e) => this.sectionCloseModal(e)} selectionCloseModal={(e) => this.selectionCloseModal(e)} updateState={(e) => this.updateState(e)} selectionAnimation={this.state.selectionAnimation} closeSelection={(e) => this.closeSelection(e)} /> : null}
                {this.state.cnameModal ? <CnameModal {...this.props} {...this.state} cnameValue={this.state.cnameValue} updateDescription={(e) => this.updateDescription(e)} closeDescription={(e) => this.closeDescription(e)} openDescription={(e) => this.openDescription(e)} /> : null}
                {this.state.articleNameModal ? <ArticleName {...this.props} {...this.state} articleCode={this.state.articleCode} catDescUDF={this.state.codeRadio == "deptWise" ? this.state.catDescDept : this.state.catDesc} updateArticle={(e) => this.updateArticle(e)} articleCat={this.state.articleCat} closeArticleSelection={(e) => this.closeArticleSelection(e)} openArticleSelection={(e) => this.openArticleSelection(e)} /> : null}
            </div>

        )
    }


}

export default CatDescMap;