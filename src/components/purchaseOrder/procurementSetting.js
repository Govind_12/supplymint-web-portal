import React from 'react';
import ItemUdf from './itemUdf';
import SetUDFSetting from './setUdfSetting';
import CatDescItemUdfMapping from './catDescItemUdfMapping';
import UdfMapping from './udfMapping';
import SetUdfMapping from './setUdfMapping';
import ExcelUpload from './excelUploadModal';
import CatDescItemUdfSetting from './catDescItemUdfSetting';
import CatDescUdfTab from '../changeSetting/catDescUdfTab';
import KeyValidation from '../changeSetting/keyValidation';
import PiPoFormate from '../changeSetting/pipoFormateTab';
import EmailScheduler from '../changeSetting/emailScheduler';
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from '../loaders/requestSuccess';
import RequestError from '../loaders/requestError';
import ConfirmMapping from '../loaders/confirmMapping';
import ToastLoader from '../loaders/toastLoader';
import ListBody from 'antd/lib/transfer/ListBody';

class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            isTemplateDownload: false,
            toastMsg: '',
            fileData: [],
            isCatDescMapSave: false,
            isUdfMapSave: false,
            mapTab: '',
            hierarchyCheck: '',
            headerMsg: '',
            paraMsg: '',
            confirmMap: false,
            isSaveButton: false,
            showExcelUpload: false,
            loader: false,
            alert: false,
            message: '',
            errorMessage: '',
            code: null,
        }
        this.catUdf = React.createRef();
        this.childConfirm = React.createRef();
        this.setUdfConfirm = React.createRef();
    }
    openExcelUpload(e) {
        e.preventDefault();
        this.setState({
            showExcelUpload: !this.state.showExcelUpload
        });
    }
    CancelExcelUpload = (e) => {
        this.setState({
            showExcelUpload: false,
        })
    }

    fileUpload = (e) =>{
        this.setState({
            fileData: e.target.files[0]
        })
    }

    clearFileData = () =>{
        this.setState({
            fileData: [],
        })
    }

    uploadExcel = (type) =>{
        const fd = new FormData();        
        fd.append('type', type);
        fd.append('fileData', this.state.fileData)

        if(this.state.fileData == ''){
            this.setState({
                toastMsg: "Please select file!"
            })
            setTimeout(() => {
                this.setState({
                    toastMsg: ''
                })
            }, 2000);
        }
        if(type == ''){
            this.setState({
                errorMessage: "Please select radio button for type!",
                code: "4000",
                status: "4000",
                errorCode: "4000",
            })
        } 
        if(this.state.fileData != '' && type != '') {
            this.props.mappingExcelUploadRequest(fd);
            this.CancelExcelUpload();
        }        
    }

    downloadTemplate = (type) =>{
        if(type == ''){
            this.setState({
                errorMessage: "Please select radio button for type!",
                code: "4000",
                status: "4000",
                errorCode: "4000",
            })
        } 
        if(type != '') {
            let data = {
                template : true,
                type : type
            }
            this.setState({
                isTemplateDownload : true
            }, () => this.props.mappingExcelStatusRequest(data))            
        }  
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.purchaseIndent.poArticle.isSuccess || nextProps.purchaseIndent.cname.isSuccess 
            || nextProps.purchaseIndent.updateItemUdfMapping.isSuccess 
            || nextProps.purchaseIndent.itemCatDescUdf.isSuccess 
            || nextProps.purchaseIndent.poUdfMapping.isSuccess 
            || nextProps.purchaseIndent.mappingExcelUpload.isSuccess
            || nextProps.purchaseIndent.updateUdfMapping.isSuccess){
            this.setState({
                loader: false,
            })
        }
        if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {
            this.setState({
                loader: false,
            })
        }
        if (nextProps.purchaseIndent.updateItemUdfMapping.isSuccess 
            || nextProps.purchaseIndent.mappingExcelUpload.isSuccess
            || nextProps.purchaseIndent.updateUdfMapping.isSuccess){
            this.setState({
                message: nextProps.purchaseIndent.updateItemUdfMapping.data.message
                || nextProps.purchaseIndent.mappingExcelUpload.data.message
                || nextProps.purchaseIndent.updateUdfMapping.data.message,
            })
        }
        if (nextProps.purchaseIndent.createItemUdf.isSuccess) {
                this.setState({
                    message: nextProps.purchaseIndent.createItemUdf.data.message,
                    loader: false,
                    isSaveButton: false,
                })
                this.props.createItemUdfClear()
            }
        if (nextProps.purchaseIndent.createItemUdf.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.createItemUdf.message.status,
                errorCode: nextProps.purchaseIndent.createItemUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.createItemUdf.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.createItemUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.createItemUdf.message.error.errorMessage
              })
            this.props.createItemUdfClear()
        }

        if (nextProps.purchaseIndent.catDescDropdown.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.catDescDropdown.message.status,
                errorCode: nextProps.purchaseIndent.catDescDropdown.message.error == undefined ? undefined : nextProps.purchaseIndent.catDescDropdown.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.catDescDropdown.message.error == undefined ? undefined : nextProps.purchaseIndent.catDescDropdown.message.error.errorMessage
              })
            this.props.catDescDropdownClear()
        }
        if (nextProps.purchaseIndent.cname.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.cname.message.status,
                errorCode: nextProps.purchaseIndent.cname.message.error == undefined ? undefined : nextProps.purchaseIndent.cname.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.cname.message.error == undefined ? undefined : nextProps.purchaseIndent.cname.message.error.errorMessage
              })
            this.props.cnameClear()
        }
        if (nextProps.purchaseIndent.getUdfMapping.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.getUdfMapping.message.status,
                errorCode: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorMessage
              })
            this.props.getUdfMappingClear()
        }
        if (nextProps.purchaseIndent.updateItemUdfMapping.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.updateItemUdfMapping.message.status,
                errorCode: nextProps.purchaseIndent.updateItemUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.updateItemUdfMapping.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.updateItemUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.updateItemUdfMapping.message.error.errorMessage
              })
            this.props.updateItemUdfMappingClear()
        }
        if(nextProps.purchaseIndent.mappingExcelUpload.isSuccess){
            this.props.mappingExcelUploadClear()
        } else if (nextProps.purchaseIndent.mappingExcelUpload.isError){
            console.log(nextProps.purchaseIndent.mappingExcelUpload)
            this.setState({
                code : nextProps.purchaseIndent.mappingExcelUpload.message.status != undefined ? nextProps.purchaseIndent.mappingExcelUpload.message.status : "",
                errorMessage: nextProps.purchaseIndent.mappingExcelUpload.message.error != undefined ? nextProps.purchaseIndent.mappingExcelUpload.message.error.errorMessage : "",
                errorCode: nextProps.purchaseIndent.mappingExcelUpload.message.error != undefined ? nextProps.purchaseIndent.mappingExcelUpload.message.error.errorCode : "",
                loader: false,
            })
            this.props.mappingExcelUploadClear()
        }
        if(nextProps.purchaseIndent.mappingExcelStatus.isSuccess){
            if(nextProps.purchaseIndent.mappingExcelStatus.data.resource != null){
                if(!this.state.isTemplateDownload && (nextProps.purchaseIndent.mappingExcelStatus.data.resource.status != "" || nextProps.purchaseIndent.mappingExcelStatus.data.resource.status != null)){
                    this.setState({
                        status : nextProps.purchaseIndent.mappingExcelStatus.data.resource.status
                    })
                } else {
                    if (nextProps.purchaseIndent.mappingExcelStatus.data.resource.templateUrl != "") {
                        let url = nextProps.purchaseIndent.mappingExcelStatus.data.resource.templateUrl;
                        let elm = document.createElement('a');
                        elm.href = url;
                        elm.innerText = '';
                        elm.target = '_blank';
                        elm.download = true;
                        document.body.appendChild(elm);
                        elm.click();
                        this.setState({
                            isTemplateDownload: false,
                        })
                        this.props.mappingExcelStatusClear();
                    }
                }
            }
        } else if(nextProps.purchaseIndent.mappingExcelStatus.isError) {
            this.setState({
                code : nextProps.purchaseIndent.mappingExcelStatus.message.status != undefined ? nextProps.purchaseIndent.mappingExcelStatus.message.status : "",
                errorMessage: nextProps.purchaseIndent.mappingExcelStatus.message.error != undefined ? nextProps.purchaseIndent.mappingExcelStatus.message.error.errorMessage : "",
                errorCode: nextProps.purchaseIndent.mappingExcelStatus.message.error != undefined ? nextProps.purchaseIndent.mappingExcelStatus.message.error.errorCode : "",
                loader: false,
            })
            this.props.mappingExcelStatusClear();
            this.props.mappingExcelUploadClear()
        }

        if(nextProps.purchaseIndent.mappingExcelExport.isSuccess){
            if(nextProps.purchaseIndent.mappingExcelExport.data.resource != null){
                let url = nextProps.purchaseIndent.mappingExcelExport.data.resource;
                let elm = document.createElement('a');
                elm.href = url;
                elm.innerText = '';
                elm.target = '_blank';
                elm.download = true;
                document.body.appendChild(elm);
                elm.click();
                this.setState({
                    loader: false,
                })
                this.props.mappingExcelExportClear();
            } else if (nextProps.purchaseIndent.mappingExcelExport.data.resource == null) {
                this.setState({
                    toastMsg: 'No data found',
                    loader: false,
                })
                setTimeout(() => {
                    this.setState({
                        toastMsg: ''
                    })
                }, 2000);
                this.props.mappingExcelExportClear();
            }
        } else if(nextProps.purchaseIndent.mappingExcelExport.isError) {
            this.setState({
                code : nextProps.purchaseIndent.mappingExcelExport.message.status != undefined ? nextProps.purchaseIndent.mappingExcelExport.message.status : "",
                errorMessage: nextProps.purchaseIndent.mappingExcelExport.message.error != undefined ? nextProps.purchaseIndent.mappingExcelExport.message.error.errorMessage : "",
                errorCode: nextProps.purchaseIndent.mappingExcelExport.message.error != undefined ? nextProps.purchaseIndent.mappingExcelExport.message.error.errorCode : "",
                loader: false,
            })
            this.props.mappingExcelExportClear();
        }

        if(nextProps.purchaseIndent.poArticle.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.poArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.poArticle.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.poArticle.message.error == undefined ? undefined : nextProps.purchaseIndent.poArticle.message.error.errorCode,
                code: nextProps.purchaseIndent.poArticle.message.status,
                alert: true,
                loader: false
              })
              this.props.poArticleClear();
        }

        if(nextProps.purchaseIndent.updateUdfMapping.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.updateUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.updateUdfMapping.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.updateUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.updateUdfMapping.message.error.errorCode,
                code: nextProps.purchaseIndent.updateUdfMapping.message.status,
                alert: true,
                loader: false
              })
              this.props.updateUdfMappingClear();
        }

        if(nextProps.purchaseIndent.getCatDescUdf.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorCode,
                code: nextProps.purchaseIndent.getCatDescUdf.message.status,
                alert: true,
                loader: false
              })
              this.props.getCatDescUdfClear();
        }

        if(nextProps.purchaseIndent.poArticle.isLoading || nextProps.purchaseIndent.cname.isLoading 
            || nextProps.purchaseIndent.updateItemUdfMapping.isLoading 
            || nextProps.purchaseIndent.itemCatDescUdf.isLoading 
            || nextProps.purchaseIndent.poUdfMapping.isLoading 
            || nextProps.purchaseIndent.mappingExcelUpload.isLoading
            || nextProps.purchaseIndent.mappingExcelExport.isLoading
            || nextProps.purchaseIndent.updateUdfMapping.isLoading){
            this.setState({
                loader: true,
            })
        }
    }

    mappingTabHandler = (e) =>{
        console.log(e.target.id)
        this.setState({
            mapTab: e.target.id
        })
    }

    handleLoader = () =>{
        this.setState({
            loader: true,
        })
    }

    saveCheckHandler = (data) =>{
        console.log(data)
        this.setState({
            isSaveButton: data.hierarchy && data.table != '',
        })
    }

    confirmHandler = (data) =>{
        this.setState({
                headerMsg: data.headerMsg,
                paraMsg: data.paraMsg,
                confirmMap: data.confirmMap,
                hierarchyCheck: data,
        })
    }

    mappingCancelHandler = () =>{
        this.setState ({
            confirmMap: false,
        })
    }

    mappingConfirmHandler = () =>{
        this.setState({
            confirmMap: false
        })
        this.catUdf.current.childConfirmHandler(this.state.hierarchyCheck)
        this.childConfirm.current.childConfirmHandler(this.state.hierarchyCheck)
    }

    closeSuccessLoader = (e) =>{
        this.setState({
            message: '',
        })
    }

        closeErrorLoader = (e) =>{
        this.setState({
            alert: false,
            errorMessage: '',
            code     : "",
            status   : "",
            errorCode: "",
        })
    }

    mappingSaveCheckHandler = (data) =>{
        if(this.state.mapTab == 'catDesc'){
            this.setState({
                isCatDescMapSave: data!= '' ? true : false, 
            })
        }
        if(this.state.mapTab == 'udfMap'){
            this.setState({
                isUdfMapSave: data!= '' ? true : false,
            })
        }
    }

    render() {
        return (
            <div>
                <div className="container-fluid pad-0">
                    <div className="col-lg-12 pad-0 pad-l50">
                        <div className="subscription-tab procurement-setting-tab">
                            <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                                <li className="nav-item active" >
                                    <a className="nav-link st-btn p-l-0" href="#fieldsetting" role="tab" data-toggle="tab">Field Setting</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#datamapping" role="tab" data-toggle="tab" id="catDesc" onClick={this.mappingTabHandler}>Data Mapping</a>
                                </li>
                                {/* <li className="nav-item">
                                    <a className="nav-link st-btn" href="#catdescudftab" role="tab" data-toggle="tab">Cat/Desc & UDF</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#keyvalidations" role="tab" data-toggle="tab">Key Validations</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#pipoformat" role="tab" data-toggle="tab">PI/PO Format</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#emailscheduler" role="tab" data-toggle="tab">Email Scheduler</a>
                                </li> */}
                                {/* <li className="pst-button">
                                    <div className="pstb-inner">
                                        <button type="button" className="excel-export" onClick={(e) => this.openExcelUpload(e)}>
                                            <span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="8.627" height="10" viewBox="0 0 8.627 12.652">
                                                    <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)" />
                                                </svg>
                                            </span>
                                            Excel Upload
                                        </button>
                                        <button type="button" className="pst-save">Save</button>
                                        <button type="button">Clear</button>
                                    </div>
                                </li> */}
                            </ul>
                        </div>
                        <div className="tab-content">
                            <div className="tab-pane fade in active" id="fieldsetting" role="tabpanel">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner p-lr-47" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#itemudfsetting" role="tab" data-toggle="tab">Cat/Desc/Item UDF Setting</a>
                                        </li>
                                        {/* <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#setudfsetting" role="tab" data-toggle="tab">Set UDF Setting</a>
                                        </li> */}
                                        <li className="pst-button">
                                            <div className="pstb-inner">
                                                
                                                {this.state.isSaveButton ? 
                                                <button type="button" className="pst-save"  onClick={() => this.catUdf.current.updateSetting()}>Save</button>
                                                : <button type="button" className="pst-save btnDisabled" disabled >Save</button>}
                                                {this.state.isSaveButton ? <button type="button" onClick={() => this.catUdf.current.saveData()}>Reset Form</button> 
                                                : <button type="button" className="pst-save btnDisabled">Reset Form</button>}
                                                <button type="button" onClick={() => this.catUdf.current.clearForm()}>Clear Form</button>                                                                                          
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="tab-content pad-0">
                                    <div className="tab-pane fade in active" id="itemudfsetting" role="tabpanel">
                                        <CatDescItemUdfSetting {...this.state} {...this.props} ref={this.catUdf} handleLoader={this.handleLoader} saveCheck={this.saveCheckHandler} confirmHandler={this.confirmHandler}/>
                                    </div>
                                    {/* <div className="tab-pane fade" id="setudfsetting" role="tabpanel">
                                        <SetUDFSetting {...this.state} {...this.props} />
                                    </div> */}
                                </div>
                            </div>
                            <div className="tab-pane fade" id="datamapping" role="tabpanel">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner p-lr-47" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#itemudfmapping" role="tab" data-toggle="tab" id='catDesc' onClick={this.mappingTabHandler}>Cat/Desc/Item UDF Mapping</a>
                                        </li>
                                        <li className="nav-item" >
                                            <a className="nav-link gsti-btn" href="#setudfmapping" role="tab" data-toggle="tab" id='udfMap' onClick={this.mappingTabHandler}>Set UDF Mapping</a>
                                        </li>
                                        {this.state.mapTab == 'catDesc' && <li className="pst-button">
                                            <div className="pstb-inner" >
                                                <button type="button" className="excel-export" onClick={(e) => this.openExcelUpload(e)}>
                                                    <span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="8.627" height="10" viewBox="0 0 8.627 12.652">
                                                            <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)" />
                                                        </svg>
                                                    </span>
                                                    Excel Upload
                                                </button>
                                                {this.state.isCatDescMapSave ? <button type="button" className="pst-save" onClick={() => this.childConfirm.current.updateCatDescMapping()}>Save</button> :
                                                <button type="button" className="pst-save btnDisabled">Save</button>}
                                                {this.state.isCatDescMapSave ? <button type="button" onClick={() => this.childConfirm.current.resetCatDescMapping()}>Reset Form</button>
                                                : <button type="button" className="pst-save btnDisabled">Reset Form</button>}
                                                <button type="button" onClick={() => this.childConfirm.current.clearCatDescMapping()}>Clear Form</button>                                                
                                            </div>
                                        </li>}
                                        {this.state.mapTab == 'udfMap' && <li className="pst-button">
                                            <div className="pstb-inner" >
                                                {/* <button type="button" className="excel-export" onClick={(e) => this.openExcelUpload(e)}>
                                                    <span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="8.627" height="10" viewBox="0 0 8.627 12.652">
                                                            <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)" />
                                                        </svg>
                                                    </span>
                                                    Excel Upload
                                                </button> */}
                                                {this.state.isUdfMapSave ? <button type="button" className="pst-save" onClick={this.setUdfConfirm.current.updateSetUdfData}>Save</button> :
                                                <button type="button" className="pst-save btnDisabled">Save</button>}
                                                {this.state.isUdfMapSave ? <button type="button" onClick={this.setUdfConfirm.current.resetSetUdfData}>Reset Form</button>
                                                : <button type="button" className="pst-save btnDisabled">Reset Form</button>}
                                                <button type="button" onClick={() => this.setUdfConfirm.current.clearSetUdfData()}>Clear Form</button>                                                
                                            </div>
                                        </li>}
                                    </ul>
                                </div>
                                <div className="tab-content pad-0">
                                    <div className="tab-pane fade in active" id="itemudfmapping" role="tabpanel">
                                            {this.state.mapTab == 'catDesc' && <CatDescItemUdfMapping  {...this.state} {...this.props} ref={this.childConfirm} handleLoader={this.handleLoader} saveCheckMap={this.mappingSaveCheckHandler} confirmHandler={this.confirmHandler}/> }
                                    </div>
                                    <div className="tab-pane fade" id="setudfmapping" role="tabpanel">
                                            {this.state.mapTab == 'udfMap' && <SetUdfMapping {...this.state} {...this.props} ref={this.setUdfConfirm} saveCheckMap={this.mappingSaveCheckHandler}/> }
                                    </div>
                                </div>
                            </div>
                            {/* <div className="tab-pane fade" id="catdescudftab" role="tabpanel">
                                <CatDescUdfTab />
                            </div>
                            <div className="tab-pane fade" id="keyvalidations" role="tabpanel">
                                <KeyValidation />
                            </div>
                            <div className="tab-pane fade" id="pipoformat" role="tabpanel">
                                <PiPoFormate />
                            </div>
                            <div className="tab-pane fade" id="emailscheduler" role="tabpanel">
                                <EmailScheduler />
                            </div> */}
                        </div>
                    </div>
                    {this.state.showExcelUpload && <ExcelUpload {...this.state} {...this.props} CancelExcelUpload={this.CancelExcelUpload} fileUpload={this.fileUpload} clearFileData={this.clearFileData} uploadExcel={this.uploadExcel} downloadTemplate={this.downloadTemplate}/>}
                </div>
                {this.state.loader && <FilterLoader />}
                {this.state.message && <RequestSuccess successMessage={this.state.message} closeRequest={(e) => this.closeSuccessLoader(e)}/>}
                {this.state.errorMessage && <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.closeErrorLoader(e)}/>}
                {this.state.confirmMap && <ConfirmMapping headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} mappingCancel={this.mappingCancelHandler} mappingConfirm={this.mappingConfirmHandler}/>}
                {this.state.toastMsg && <ToastLoader toastMsg={this.state.toastMsg} />}
            </div>
        )
    }
}
export default Settings;