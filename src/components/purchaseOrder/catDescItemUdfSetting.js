import React from 'react';
import SectionDataSelection from "../../components/purchaseIndent/sectionDataSelection";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import PoError from '../loaders/poError';
import Pagination from '../pagination';
import ConfigModal from './configModal';
import ConfirmMapping from '../loaders/confirmMapping';
import { tuple } from 'antd/lib/_util/type';

class CatDescItemUdfSetting extends React.Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     selectDepartment: false,
        //     selectDivision: false,
        //     selectSection: false,
        //     currentActive: "Division",

        // }
        this.state = {
            mapData: '',
            confirmMap: false,
            headerMsg: '',
            paraMsg: '',
            divisionCheck: false,
            sectionCheck: false,
            departmentCheck: true,
            openFilter: false,
            dashboardType: "pi",
            currentActive: "Division",
            pageDetails: '',
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            selectSite: false,
            hierarchyDataPi: [],
            hierarchyDataPo: [],
            siteDetails: { "siteCode": "", "siteName": "" },
            hierarchyDropHandle: false,
            basedOn: '',
            searchBy: 'startWith',
            type: 1,
            no: 1,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            search: "",
            startDate: "",
            endDate: "",
            selectVendor: false,
            isSave: false,
            categoryUdfData: [],            
            categoryPayload: [],
            catUdfPayload: [],
            tableSearch: '',
        }
    }

    showDropDown = (event) => {
        // event.preventDefault();
        this.setState({ hierarchyDropHandle: true }, () => {
            document.addEventListener('click', this.closeDropDown);
        });
    }

    closeDropDown = () => {
        this.setState({ hierarchyDropHandle: false }, () => {
            document.removeEventListener('click', this.closeDropDown);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.poArticle.isSuccess){
            let pageDetails = {                
                currPage: nextProps.purchaseIndent.poArticle.data.currPage,                
                maxPage: nextProps.purchaseIndent.poArticle.data.maxPage,
            }
            this.setState({
                pageDetails: pageDetails
            })            
            this.props.poArticleClear()
        }
        if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {
            let catUdf = [...nextProps.purchaseIndent.getCatDescUdf.data.resource.categories, ...nextProps.purchaseIndent.getCatDescUdf.data.resource.cat_desc_udf];
            this.setState({
                categoryUdfData : catUdf,                
            })
            this.props.getCatDescUdfClear()
        }
        if (nextProps.purchaseIndent.createItemUdf.isSuccess) {
            this.setState({
                categoryPayload: [],
                catUdfPayload: [],
            })
            console.log(this.state.hierarchy)
            this.saveData();
        } 
    }
    handleSearch = (e) => {
        let dName = e.target.dataset.name

        this.setState({
            hierarchy: {
                ...this.state.hierarchy, [dName]: {
                    name: e.target.value,
                    code: ""
                }
            },
            search: e.target.value
        })
    }
    // setDropData = (e) => {
    // let { startDate, endDate } = this.state
    // let dName = e.target.dataset.name


    //     if (e.keyCode == 13) {
    //         this.setState({ currentActive: dName, hierarchyDropHandle: true })
    //         payload = {
    //             "type": this.state.search.length === 0 ? 1 : 3,
    //             "pageNo": 1,
    //             "isPO": this.state.dashboardType === "pi" ? false : true,
    //             "hl1Name": this.state.search.length === 0 ? this.state.hierarchy["Division"].name : "",
    //             "hl2Name": this.state.search.length === 0 ? this.state.hierarchy["Section"].name : "",
    //             "hl3Name": this.state.search.length === 0 ? this.state.hierarchy["Department"].name : "",
    //             "hl4Name": "",
    //             "divisionSearch":
    //                 "fromDate": startDate === "" ? oneYearFromNow : startDate,
    //             "toDate": endDate == "" ? currFormatDate : endDate,
    //                 "search": this.state.search,
    //                     "siteCode": this.state.siteDetails.siteCode || "",
    //                         "getBy": TRACK_DATA.get(dName).label
    //     }
    //     // if (this.state.dashboardType === "pi") {
    //     this.props.piDashHierarchyRequest(payload)

    // }
    //}

    setDropData = (e, value) => {
        let { startDate, endDate } = this.state
        let dName = e.target.dataset.name
        if (e.keyCode == 13 || e.target.id == 'click') {
            this.setState({ currentActive: dName, hierarchyDropHandle: true })
            if (value == "division") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "division",
                    poArticleSearch: "",
                    mrpRangeSearch: false

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Division'].name == '' ? 1 : 3,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,                    
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.hierarchy['Division'].name,
                    sectionSearch: "",
                    departmentSearch: "",
                    articleSearch: "",
                    basedOn: "division",
                    supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false

                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)


            } else if (value == "section") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "section",
                    poArticleSearch: "",
                    mrpRangeSearch: false
                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Section'].name == '' ? 1 : 3,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,                    
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: this.state.hierarchy['Section'].name,
                    departmentSearch: "",
                    articleSearch: "",
                    basedOn: "section",
                    //supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false
                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)
            } else if (value == "department") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl2Name: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "department",
                    poArticleSearch: "",
                    mrpRangeSearch: false,
                    isSave: true

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Department'].name == '' ? 1 : 3,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,                    
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: "",
                    departmentSearch: this.state.hierarchy['Department'].name,
                    articleSearch: "",
                    basedOn: "department",
                    // supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false
                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)
            }
        }
    }

    page = (e) => {
        console.log(e.target.id)
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.prePage != 0) {
                let data = {
                    type: this.state.hierarchy['Division'].name != '' || this.state.hierarchy['Section'].name != '' || this.state.hierarchy['Department'].name != '' ? 3 : 1,
                    no: this.props.purchaseIndent.poArticle.data.prePage,

                    hl1Name: this.state.hierarchy['Division'].name,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,                    
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl3Code: this.state.hierarchy['Department'].code, 
                    divisionSearch: this.state.basedOn == "division" ? this.state.hierarchy['Division'].name : "",
                    sectionSearch: this.state.basedOn == "section" ? this.state.hierarchy['Section'].name : "",
                    departmentSearch: this.state.basedOn == "department" ? this.state.hierarchy['Department'].name : "",
                    articleSearch: "",
                    basedOn: this.state.basedOn,
                    supplier: this.state.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: false,
                }
                this.props.poArticleRequest(data);
                setTimeout(() => this.showDropDown(), 2);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.currPage != this.props.purchaseIndent.poArticle.data.maxPage) {
                let data = {
                    type: this.state.hierarchy['Division'].name != '' || this.state.hierarchy['Section'].name != '' || this.state.hierarchy['Department'].name != '' ? 3 : 1,
                    no: this.props.purchaseIndent.poArticle.data.currPage + 1,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,                    
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.basedOn == "division" ? this.state.hierarchy['Division'].name : "",
                    sectionSearch: this.state.basedOn == "section" ? this.state.hierarchy['Section'].name : "",
                    departmentSearch: this.state.basedOn == "department" ? this.state.hierarchy['Department'].name : "",
                    articleSearch: "",
                    basedOn: this.state.basedOn,
                    supplier: this.state.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: false
                }
                this.props.poArticleRequest(data)
                setTimeout(() => this.showDropDown(), 2);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }

        }        
    }

    // page = (e) => {
    //     console.log(this.props.purchaseIndent.poArticle)
    //     if (e.target.id == "prev") {
    //         this.setState({
    //             prev: this.props.purchaseIndent.piDashHierarchy.data.prePage,
    //             current: this.props.purchaseIndent.piDashHierarchy.data.currPage,
    //             next: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
    //             maxPage: this.props.purchaseIndent.piDashHierarchy.data.maxPage,
    //         })
    //         if (this.props.purchaseIndent.piDashHierarchy.data.prePage != 0) {
    //             let data = {
    //                 type: this.state.type,
    //                 no: this.props.purchaseIndent.piDashHierarchy.data.prePage,
    //                 isPO: this.state.dashboardType === "pi" ? false : true
    //             }
    //             this.props.piDashHierarchyRequest(data)
    //         }
    //     } else if (e.target.id == "next") {
    //         this.setState({
    //             prev: this.props.purchaseIndent.piDashHierarchy.data.prePage,
    //             current: this.props.purchaseIndent.piDashHierarchy.data.currPage,
    //             next: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
    //             maxPage: this.props.purchaseIndent.piDashHierarchy.data.maxPage,
    //         })
    //         if (this.props.purchaseIndent.piDashHierarchy.data.currPage != this.props.purchaseIndent.piDashHierarchy.data.maxPage) {
    //             let data = {
    //                 type: this.state.type,
    //                 no: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
    //                 isPO: this.state.dashboardType === "pi" ? false : true
    //             }
    //             this.props.piDashHierarchyRequest(data)
    //         }
    //     }
    //     else if (e.target.id == "first") {
    //         this.setState({
    //             prev: this.props.purchaseIndent.piDashHierarchy.data.prePage,
    //             current: this.props.purchaseIndent.piDashHierarchy.data.currPage,
    //             next: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
    //             maxPage: this.props.purchaseIndent.piDashHierarchy.data.maxPage,
    //         })
    //         if (this.props.purchaseIndent.piDashHierarchy.data.currPage <= this.props.purchaseIndent.piDashHierarchy.data.maxPage) {
    //             let data = {
    //                 type: this.state.type,
    //                 no: 1,
    //                 isPO: this.state.dashboardType === "pi" ? false : true
    //             }
    //             this.props.piDashHierarchyRequest(data)
    //         }
    //     }
    // }
    setHierarchySelectedData = (data) => {
        this.setState({
            search: "",
            hierarchy: { ...this.state.hierarchy, Division: { name: data.hl1Name, code: data.hl1Code } || "", Section: { name: data.hl2Name, code: data.hl2Code } || "", Department: { name: data.hl3Name, code: data.hl3Code } || "", Article: { name: data.hl4Name, code: data.hl4Code } || "" },
            hierarchyDropHandle: false
        }, () => {
            if(this.state.hierarchy["Department"].name != ''){
                this.saveData();
            }
        })
    }
    handleHierarchyModal = () => {
        this.setState({ hierarchyDropHandle: !this.state.hierarchyDropHandle })
    };

    saveData() {
        let data = {
            hl3Code: this.state.hierarchy.Department.code,
            ispo: false,
            hl3Name: this.state.hierarchy.Department.name
        }
        console.log(data)
        this.props.getCatDescUdfRequest(data)
        this.props.handleLoader();
    }

    clearForm = () => {
        this.setState({
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            categoryPayload: [],
            catUdfPayload: [],
            categoryUdfData: [],
            departmentCheck: true,
            divisionCheck: false,
            sectionCheck: false,
        }, () =>{
            let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
            let tableCheck = this.state.categoryPayload != '' ? this.state.categoryPayload : this.state.catUdfPayload;
            let data ={hierarchy: hierarchyCheck, table: tableCheck}
            this.props.saveCheck(data)
        })
    }

    changeSettingHandler =(e, data) =>{
        let category = [...this.state.categoryPayload];
        let udf = [...this.state.catUdfPayload];
        let dataVal = {};
        let categoryUdfDisplayData = [];        
        if(data.cat_desc_udf_type == 'U'){
            if(e.target.id == 'displayName'){
                dataVal = {...data, displayName: e.target.value}
            }
            if(e.target.id == 'orderBy'){
                dataVal = {...data, orderBy: e.target.value}
            }
            if(e.target.id == 'isPiComp'){
                let value = data.isCompulsoryPI == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isCompulsoryPI: value}
                if(value == 'Y'){
                    dataVal = {...dataVal, isDisplayPI: value}
                }
            }
            if(e.target.id == 'isPoComp'){
                let value = data.isCompulsoryPO == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isCompulsoryPO: value}
                if(value == 'Y'){
                    dataVal = {...dataVal, isDisplayPO: value}
                }
            }
            
            if(e.target.id == 'isDisplayPI'){
                let value = data.isDisplayPI == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isDisplayPI: value}
                if(value == 'N'){
                    dataVal = {...dataVal, isCompulsoryPI: value}
                }
            }
            if(e.target.id == 'isDisplayPO'){
                let value = data.isDisplayPO == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isDisplayPO: value}
                if(value == 'N'){
                    dataVal = {...dataVal, isCompulsoryPO: value}
                }
            }
            if(e.target.id == 'isLovPI'){
                let value = data.isLovPI == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isLovPI: value}
            }
            if(e.target.id == 'isLovPO'){
                let value = data.isLovPO == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isLovPO: value}
            }
            if(udf == '' || udf.filter(item => item.id == data.id) == ''){
                udf.push(dataVal)
            }
            else{
                udf = this.state.catUdfPayload.map(item =>{                    
                        if(item.id == data.id){                       
                            return dataVal
                        }
                        else{
                            return item
                        }
                    })
            }
        } else{
            if(e.target.id == 'displayName'){
                dataVal = {...data, displayName: e.target.value}
            }
            if(e.target.id == 'orderBy'){
                dataVal = {...data, orderBy: e.target.value}
            }
            if(e.target.id == 'isPiComp'){
                let value = data.isCompulsoryPI == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isCompulsoryPI: value}
                if(value == 'Y'){
                    dataVal = {...dataVal, isDisplayPI: value}
                }
            }
            if(e.target.id == 'isPoComp'){
                let value = data.isCompulsoryPO == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isCompulsoryPO: value}
                if(value == 'Y'){
                    dataVal = {...dataVal, isDisplayPO: value}
                }
            }
            
            if(e.target.id == 'isDisplayPI'){
                let value = data.isDisplayPI == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isDisplayPI: value}
                if(value == 'N'){
                    dataVal = {...dataVal, isCompulsoryPI: value}
                }
            }
            if(e.target.id == 'isDisplayPO'){
                let value = data.isDisplayPO == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isDisplayPO: value}
                if(value == 'N'){
                    dataVal = {...dataVal, isCompulsoryPO: value}
                }
            }
            if(e.target.id == 'isLovPI'){
                let value = data.isLovPI == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isLovPI: value}
            }
            if(e.target.id == 'isLovPO'){
                let value = data.isLovPO == 'Y' ? 'N' : 'Y';
                dataVal = {...data, isLovPO: value}
            }
            if(category == '' || category.filter(item => item.id == data.id) == ''){
                category.push(dataVal)
            }
            else{

                    category = this.state.categoryPayload.map(item =>{                    
                        if(item.id == data.id){                       
                            return dataVal
                        }
                        else{
                            return item
                        }
                    })   

            }            
        }
        categoryUdfDisplayData = this.state.categoryUdfData.map(item =>{
            if(item.id == data.id){                    
                return dataVal
            }
            else{
                return item
            }
        })
        console.log('displayData', categoryUdfDisplayData)
        this.setState({
            categoryPayload: category,
            catUdfPayload: udf,
            categoryUdfData: categoryUdfDisplayData
        }, () =>{
            let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
            let tableCheck = this.state.categoryPayload != '' ? this.state.categoryPayload : this.state.catUdfPayload;
            let data ={hierarchy: hierarchyCheck, table: tableCheck}
            this.props.saveCheck(data)
        })        
    }

    confirmMapCheck = (e, value) =>{
        let data = e.target.id;
        let displayData = data;
        if(data == 'departmentCheck'){
            displayData = 'Department'
        }
        if(data == 'divisionCheck'){
            displayData = 'Division'
        }
        if(data == 'sectionCheck'){
            displayData = 'Section'
        }
        if(!value){
            this.setState({
                mapData: data,            
            }, () => {
                let dataVal = {
                    headerMsg: `All settings will be effective on the selected ${displayData}.`,
                    paraMsg: 'Please Confirm',
                    confirmMap: true,
                    data: data
                }
                this.props.confirmHandler(dataVal)
            })
        }
        else{            
            this.setState({
                divisionCheck: false,
                sectionCheck: false,
                departmentCheck: true,
            }, () =>{
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}
                this.props.saveCheck(data)}
            )
        }
    }

    childConfirmHandler = (data) =>{        
        this.setState({
            mapData: data,
        }, this.mappingConfirmHandler())
    }

    mappingConfirmHandler = () => {
        console.log(this.state.mapData)
        let t = this;
        if(this.state.mapData == 'divisionCheck'){
            this.setState({
                divisionCheck: !this.state.divisionCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}
                t.props.saveCheck(data)
                if(this.state.divisionCheck){
                    this.setState({
                        departmentCheck: false,
                        sectionCheck: false,
                    })
                }
            })
        }
        if(this.state.mapData == 'departmentCheck'){
            this.setState({
                departmentCheck: !this.state.departmentCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}
                t.props.saveCheck(data)
                if(this.state.departmentCheck){
                    this.setState({
                        divisionCheck: false,
                        sectionCheck: false,
                    })
                }
            })
        }
        if(this.state.mapData == 'sectionCheck'){
            this.setState({
                sectionCheck: !this.state.sectionCheck
            }, () => {
                let hierarchyCheck = this.state.divisionCheck || this.state.departmentCheck || this.state.sectionCheck;
                let tableCheck = this.state.categoryPayload || this.state.catUdfPayload;
                let data ={hierarchy: hierarchyCheck, table: tableCheck}
                t.props.saveCheck(data)
                if(this.state.sectionCheck){
                    this.setState({
                        divisionCheck: false,
                        departmentCheck: false,
                    })
                }
            })
        }
    }


    updateSetting = () =>{
        let payload = {
            hl1Code: this.state.divisionCheck ? this.state.hierarchy['Division'].code : '',
            hl2Code: this.state.sectionCheck ? this.state.hierarchy['Section'].code : '',
            hl3Code: this.state.departmentCheck ? this.state.hierarchy['Department'].code : '',
            categories: this.state.categoryPayload,
            cat_desc_udf: this.state.catUdfPayload
          }
        this.props.createItemUdfRequest(payload)
        this.props.handleLoader();
    }

    tableSearchHandler = (e) => {
        this.setState({
            tableSearch: e.target.value
        })
    }

    clearTableSearchHandler = () => {
        this.setState({
            tableSearch: '',
        })
    }

    render() {
        console.log("category: ", this.state.categoryPayload, "catUdf ", this.state.catUdfPayload)
        console.log('cat', this.state.categoryUdfData)
        const { hierarchy, siteDetails, hierarchyDropHandle, categoryUdfData, tableSearch, divisionCheck, departmentCheck, sectionCheck, catUdfPayload, categoryPayload } = this.state;
        
        let filterCatUdf = categoryUdfData.filter(item =>{
            if(tableSearch == ''){
                return item
            }
            else if(item.cat_desc_udf.toLowerCase().includes(tableSearch.toLowerCase()) || item.displayName.toLowerCase().includes(tableSearch.toLowerCase())){
                return item
            }
        })        
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="pi-new-layout">
                        <form>
                            <div className="col-lg-2 check-input">
                                <label className="pnl-purchase-label" fillRule="pddivision">Division</label>
                                <div className="inputTextKeyFucMain">
                                    <input type="text" className="onFocus pnl-purchase-input" data-name="Division" value={hierarchy["Division"].name} onChange={(e) => this.handleSearch(e)} onKeyDown={(e) => this.setDropData(e, "division")} placeholder="Choose Division" />
                                    <span className="modal-search-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" id="click" data-name="Division" onClick={(e) => this.setDropData(e, "division")}>
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                        </svg>
                                    </span>
                                    {hierarchyDropHandle && <ConfigModal {...this.state} page={this.page} setHierarchySelectedData={this.setHierarchySelectedData} handleHierarchyModal={this.handleHierarchyModal} currentActive={this.state.currentActive} hierarchyData={this.props.purchaseIndent.poArticle.data.resource || this.props.purchaseIndent.poArticle.data.resource || []} />}
                                </div>
                                <div className="pnlci-check">
                                    <label className="checkBoxLabel0">
                                        <input type="radio" className="checkBoxTab" checked= {this.state.divisionCheck} id = 'divisionCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.divisionCheck)}/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>

                            <div className="col-lg-2 check-input">
                                <label className="pnl-purchase-label">Section</label>
                                <div className="inputTextKeyFucMain">
                                    <input type="text" className="onFocus pnl-purchase-input" data-name="Section" value={hierarchy["Section"].name} onChange={this.handleSearch} onKeyDown={(e) => this.setDropData(e, "section")} placeholder="Choose Section " />
                                    <span className="modal-search-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" id="click" data-name="Section" onClick={(e) => this.setDropData(e, "section")}>
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                        </svg>
                                    </span>
                                    {/* {this.state.selectSection && <ConfigModal handleDepartmentModal={this.handleDepartmentModal} />} */}
                                </div>
                                <div className="pnlci-check">
                                    <label className="checkBoxLabel0">
                                        <input type="radio" className="checkBoxTab" checked= {this.state.sectionCheck} id = 'sectionCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.sectionCheck)}/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>
                            <div className="col-lg-2 check-input">
                                <label className="pnl-purchase-label">Department</label>
                                <div className="inputTextKeyFucMain">
                                    <input type="text" className="onFocus pnl-purchase-input" data-name="Department" value={hierarchy["Department"].name} onChange={this.handleSearch} onKeyDown={(e) => this.setDropData(e, "department")} placeholder="Choose Department" />
                                    <span className="modal-search-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231"  id="click" data-name="Department" onClick={(e) => this.setDropData(e, "department")}>
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1"/>
                                        </svg>
                                    </span>
                                    {/* {this.state.selectDepartment && <ConfigModal handleDepartmentModal={this.handleDepartmentModal} />} */}
                                </div>
                                <div className="pnlci-check">
                                    <label className="checkBoxLabel0">
                                        <input type="radio" className="checkBoxTab" checked= {this.state.departmentCheck} id = 'departmentCheck' onClick = {(e) => this.confirmMapCheck(e, this.state.departmentCheck)}/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>
                        </form>
                        {/* <div className="col-lg-2 udf-flex">
                            {this.state.isSave ?
                                <button className="udf-search-btn" onClick={(e) => this.saveData(e)} ><img src={require('../../assets/transparentSearch.svg')} />Search</button> :
                                <button className="btnDisabled udf-search-btn"><img src={require('../../assets/transparentSearch.svg')} />Search</button>}
                        </div> */}
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="gen-item-udf">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <span className="gen-info-icon" style={{marginBottom: '-17px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="20" viewBox="0 0 14 14">
                                        <path id="iconmonstr-info-2" fill="#97abbf" d="M7 1.167A5.833 5.833 0 1 1 1.167 7 5.84 5.84 0 0 1 7 1.167zM7 0a7 7 0 1 0 7 7 7 7 0 0 0-7-7zm.583 10.5H6.417V5.833h1.166zM7 3.354a.729.729 0 1 1-.729.729A.729.729 0 0 1 7 3.354z"/>
                                    </svg>
                                    <div className="gii-dropdown">
                                        <label className="checkBoxLabel0 displayPointer">
                                            <input type="checkBox" className="checkBoxTab" checked/>
                                            <span className="checkmark1 checkmark1-red"></span>
                                            Non-editable (Product Level. Config.)
                                        </label>
                                        <label className="checkBoxLabel0 displayPointer">
                                            <input type="checkBox" className="checkBoxTab" checked/>
                                            <span className="checkmark1 checkmark1-disabled"></span>
                                            Non-editable (System mandate config.)
                                        </label>
                                        <label className="checkBoxLabel0 displayPointer">
                                            <input type="checkBox" className="checkBoxTab" checked/>
                                            <span className="checkmark1"></span>
                                            Editable (User can change config.)
                                        </label>
                                        <p>*Contact <span className="bold">Supplymint</span> for more info..</p>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type to Search..." value={this.state.tableSearch} onChange={this.tableSearchHandler}/>
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                    {this.state.tableSearch && <span className="closeSearch" id="clearSearchFilter"><img src={require('../../assets/clearSearch.svg')} onClick={this.clearTableSearchHandler}/></span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="gen-item-udf-table">
                        <div className="giut-headfix" id="table-scroll">
                            {/* <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div> */}
                            <table className="table gen-main-table">
                                <thead >
                                    <tr>
                                        <th className="width145">
                                            <label>Name</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Display Name</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Order By</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Is PI Compulsory</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Is PI Display</label>                                            
                                        </th>
                                        <th>
                                            <label>Is PI LOV (List of Values)</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Is PO Compulsory</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                        <th>
                                            <label>Is PO Display</label>                                            
                                        </th>
                                        <th>
                                            <label>Is PO LOV (List of Values)</label>
                                            {/* <img src={require('../../assets/headerFilter.svg')} /> */}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filterCatUdf.length == 0 ?
                                        <tr>
                                            <td align="center" colSpan="12">
                                                <label>No Data Available</label>
                                            </td>
                                        </tr>
                                        : filterCatUdf.map((data, key) => (
                                            <tr key={key} className={(data.cat_desc_udf_type == 'C' || data.cat_desc_udf_type == 'D') ? "bg-config-orange" : "bg-config-blue"}>
                                                <td className="width145"><label className="bold">{data.cat_desc_udf}</label></td>
                                                <td>
                                                    <input type="text" className="modal_tableBox " value={data.displayName} id='displayName' disabled={(data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP'} onChange={(e) => this.changeSettingHandler(e, data)}/>
                                                </td>
                                                <td><input type="number" pattern="[0-9]*" maxLength="2" className="inputTable " value={data.orderBy} id='orderBy' onChange={(e) => this.changeSettingHandler(e, data)} /></td>
                                                <td >
                                                    <label className="checkBoxLabel0 displayPointer">
                                                        <input type="checkBox" className="checkBoxTab" checked={data.isCompulsoryPI == "N" ? false : true} id='isPiComp' onChange={(e) => this.changeSettingHandler(e, data)} disabled={(data.productLevelRequired == "Y" && data.productLevelCompulsoryPI == "N") ? false : true}/>
                                                        <span className={(data.productLevelRequired == "Y" && data.productLevelCompulsoryPI == "N") ? "checkmark1" : "checkmark1 checkmark1-red"}></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label className="checkBoxLabel0 displayPointer">
                                                        <input type="checkBox" className="checkBoxTab" checked={data.isDisplayPI == "N" ? false : true} id='isDisplayPI' onChange={(e) => this.changeSettingHandler(e, data)} disabled={(data.productLevelRequired == "Y" && data.productLevelDisplayPI == "Y") ? ((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP') : true}/>
                                                        <span className={(data.productLevelRequired == "Y" && data.productLevelDisplayPI == "Y") ? ((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP') ? "checkmark1 checkmark1-disabled" : "checkmark1" : "checkmark1 checkmark1-red"}></span>
                                                    </label>
                                                </td>
                                                <td >
                                                    <label className="checkBoxLabel0 displayPointer">
                                                        <input type="checkBox" className="checkBoxTab" checked={data.isLovPI == "N" ? false : true} id='isLovPI' onChange={(e) => this.changeSettingHandler(e, data)} disabled={((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP' || data.cat_desc_udf_type == 'C')}/>
                                                        <span className={((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP' || data.cat_desc_udf_type == 'C') ? "checkmark1 checkmark1-disabled" : "checkmark1"}></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label className="checkBoxLabel0 displayPointer">
                                                        <input type="checkBox" className="checkBoxTab" checked={data.isCompulsoryPO == "N" ? false : true} id='isPoComp' onChange={(e) => this.changeSettingHandler(e, data)} disabled={(data.productLevelRequired == "Y" && data.productLevelCompulsoryPO == "N") ? false : true}/>
                                                        <span className={(data.productLevelRequired == "Y" && data.productLevelCompulsoryPO == "N") ? "checkmark1" : "checkmark1 checkmark1-red"}></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label className="checkBoxLabel0 displayPointer">
                                                        <input type="checkBox" className="checkBoxTab" checked={data.isDisplayPO == "N" ? false : true} id='isDisplayPO' onChange={(e) => this.changeSettingHandler(e, data)} disabled={(data.productLevelRequired == "Y" && data.productLevelDisplayPO == "Y") ? ((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP') : true}/>
                                                        <span className={(data.productLevelRequired == "Y" && data.productLevelDisplayPO == "Y") ? ((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP') ? "checkmark1 checkmark1-disabled" : "checkmark1" : "checkmark1 checkmark1-red"}></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label className="checkBoxLabel0 displayPointer">
                                                        <input type="checkBox" className="checkBoxTab" checked={data.isLovPO == "N" ? false : true} id='isLovPO' onChange={(e) => this.changeSettingHandler(e, data)} disabled={(data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP' || data.cat_desc_udf_type == 'C'}/>
                                                        <span className={((data.displayName).toUpperCase() == 'SIZE' || (data.displayName).toUpperCase() == 'COLOR' || (data.displayName).toUpperCase() == 'VENDOR_DESIGN' || (data.displayName).toUpperCase() == 'MRP' || data.cat_desc_udf_type == 'C') ? "checkmark1 checkmark1-disabled" : "checkmark1"}></span>
                                                    </label>
                                                </td>
                                            </tr>
                                        ))}                                        
                                    {/* {
                                        this.state.categoryData.length != 0 ? this.state.categoryData.map((item, key) => {
                                            //console.log("Data: ", this.state.categoryData);
                                            <div>ria</div>
                                            // <tr key={key}>
                                            //     <td>ria</td>
                                            //     <td>ria</td>
                                            //     <td>ria</td>
                                            //     <td>ria</td>
                                            //     <td>ria</td>
                                            //     <td>ria</td>
                                            //     <td>ria</td>
                                            // </tr>
                                            // <tr key={key}>
                                            //     <td className="width145"><label className="bold">{item.cat_desc_udf}</label></td>
                                            //     <td>
                                            //         <input type="text" className="modal_tableBox " value={item.displayName} />
                                            //     </td>
                                            //     <td><input pattern="[0-9]*" maxLength="2" className="inputTable " value={item.orderBy} /></td>
                                            //     <td >
                                            //         <label className="checkBoxLabel0 displayPointer">
                                            //             <input type="checkBox" className="checkBoxTab" checked={item.isCompulsoryPI == "N" ? false : true} />
                                            //             <span className="checkmark1"></span>
                                            //         </label>
                                            //     </td>
                                            //     <td>
                                            //         <label className="checkBoxLabel0 displayPointer">
                                            //             <input type="checkBox" className="checkBoxTab" checked={item.isLovPI == "N" ? false : true} />
                                            //             <span className="checkmark1"></span>
                                            //         </label>
                                            //     </td>
                                            //     <td>
                                            //         <label className="checkBoxLabel0 displayPointer">
                                            //             <input type="checkBox" className="checkBoxTab" checked={item.isCompulsoryPO == "N" ? false : true} />
                                            //             <span className="checkmark1"></span>
                                            //         </label>
                                            //     </td>
                                            //     <td>
                                            //         <label className="checkBoxLabel0 displayPointer">
                                            //             <input type="checkBox" className="checkBoxTab" checked={item.isLovPO == "N" ? false : true} />
                                            //             <span className="checkmark1"></span>
                                            //         </label>
                                            //     </td>
                                            // </tr>
                                        }) : <tr>
                                                <td align="center" colspan="12"><label>No Data Found</label></td>
                                            </tr>
                                    } */}

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-12 pad-0" >
                        <div className="new-gen-pagination">
                            {/* <div className="ngp-left">
                                <div className="table-page-no">
                                    <span>Page :</span><input type="number" className="paginationBorder" min="1" value={1} />                                    
                                </div>
                            </div>
                            <div className="ngp-right">
                                <div className="nt-btn">
                                    <Pagination />
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>                
            </div>
        )
    }
}

export default CatDescItemUdfSetting;