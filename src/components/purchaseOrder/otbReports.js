import React from 'react';
import Pagination from '../pagination';
import DivisionModal from './otbDivModal';
import Loader from '../loaders/filterLoader';
import ArticleModal from './otbArticleModal';
import MonthModal from './otbMonthModal';

class OtbReport extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            monthList: [],
            isMonthOpen: false,
            articleSelectedList: [],
            articleId: [],
            articleInput: [],
            aritcleValue: [],
            isArticleOpen: false,
            loader: false,
            showFilters: true,
            hierarchyDropHandle: false,
            pageDetails: '',
            currentActive: "Division",
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            payload: {
                "data": [
                ],
                "monthYearList": [
                ],
                "isExport":false
            },
            otbItems:[],
        }
    }

    componentDidMount() {
        this.props.analyticsOtbRequest(this.state.payload)
    }

    static getDerivedStateFromProps(props, state) {
        if(props.purchaseIndent.analyticsOtb.isSuccess){
            if(props.purchaseIndent.analyticsOtb.data.resource != null){
                if(typeof (props.purchaseIndent.analyticsOtb.data.resource) == 'object'){
                    return {
                        otbItems: props.purchaseIndent.analyticsOtb.data.resource,
                    }
                }
                else if(props.purchaseIndent.analyticsOtb.data.resource.length == 0){
                    return {
                        otbItems: [],
                    }   
                }
                else if(typeof (props.purchaseIndent.analyticsOtb.data.resource) == "string"){
                    let a = document.createElement('a');
                    a.href = props.purchaseIndent.analyticsOtb.data.resource;
                    a.innerText = '';
                    a.download = true;
                    document.body.appendChild(a)
                    a.click()
                } else if(typeof (props.purchaseIndent.analyticsOtb.data.resource) == ""){
                    alert('Report is not generated yet')
                } 
            }
            else{
                return {
                    otbItems: [],
                } 
            }            
        }
        if (props.purchaseIndent.poArticle.isSuccess && !state.isArticleOpen){
            let pageDetails = {                
                currPage: props.purchaseIndent.poArticle.data.currPage,                
                maxPage: props.purchaseIndent.poArticle.data.maxPage,
            }
            props.poArticleClear()
            return{
                pageDetails: pageDetails,
                loader: false,
            }        
        }
        if (props.purchaseIndent.poArticle.isSuccess && state.isArticleOpen){
            props.poArticleClear()
            return{
                loader: false,
            }        
        }
        if (props.purchaseIndent.poArticle.isLoading){
            return{
                loader: true,
            }
        }
    }

    downloadReport = () =>{
        let payload = {
            "data": this.state.aritcleValue,
            "monthYearList": this.state.monthList,
            "isExport":true
        }
        this.props.analyticsOtbRequest(payload)
    }

    filterOtb = () =>{
        let payload = {
            "data": this.state.aritcleValue,
            "monthYearList": this.state.monthList,
            "isExport":false
        }
        this.props.analyticsOtbRequest(payload)
    }

    clearFilter = () =>{
        this.setState({
            articleId: [],
            articleInput: [],
            aritcleValue: [],
            monthList: [],
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
        }, () => this.props.analyticsOtbRequest(this.state.payload))
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            showFilters: !this.state.showFilters
        });
    }

    showDropDown = (event) => {
        // event.preventDefault();
        this.setState({ hierarchyDropHandle: true }, () => {
            document.addEventListener('click', this.closeDropDown);
        });
    }

    closeDropDown = () => {
        this.setState({ hierarchyDropHandle: false }, () => {
            document.removeEventListener('click', this.closeDropDown);
        });
    }

    articleDropDown = (e) => {
            let payload = {
                pageNo: 1,
                type: 1,
                hl4Code: "",
                hl4Name: "",
                hl1Name: this.state.hierarchy['Division'].name,
                hl1Code: this.state.hierarchy['Division'].code,
                hl2Code: this.state.hierarchy['Section'].code,
                hl2Name: this.state.hierarchy['Section'].name,
                hl3Name: this.state.hierarchy['Department'].name,
                hl3Code: this.state.hierarchy['Department'].code,
                divisionSearch: "",
                sectionSearch: "",
                departmentSearch: "",
                articleSearch: "",
                basedOn: "article",
                //supplier: this.state.slCode,
                mrpSearch: "",
                isMrp: false,
                mrpSearch: "",
                mrpRangeSearch: false,
                isArticleWithMrp:true
            }
            this.props.poArticleRequest(payload)
            this.showArticleDropDown()
    }

    showArticleDropDown = (event) => {
        // event.preventDefault();
        this.setState({ isArticleOpen: true });
    }

    closeArticleDropDown = () => {
        this.setState({ isArticleOpen: false });
    }

    articleSubmit = (articleInput, aritcleValue, id, articleSelectedList) => {
        this.setState({
            isArticleOpen: false,
            articleInput: articleInput, 
            aritcleValue: aritcleValue,
            articleId: id,
            articleSelectedList: articleSelectedList,
        })
    }


    showMonthDropDown = () => {
        this.setState({ isMonthOpen: true });
    }

    closeMonthDropDown = () =>{
        this.setState({
            isMonthOpen: false
        })
    }

    monthSubmit = (data) =>{
        this.setState({
            monthList: data,
            isMonthOpen: false
        })
    }

    handleSearch = (e) => {
        let dName = e.target.dataset.name

        this.setState({
            hierarchy: {
                ...this.state.hierarchy, [dName]: {
                    name: e.target.value,
                    code: ""
                }
            },
            search: e.target.value
        })
    }

    setDropData = (e, value) => {
        let { startDate, endDate } = this.state
        let dName = e.target.dataset.name
        if (e.keyCode == 13 || e.target.id == 'click') {
            this.setState({ currentActive: dName, hierarchyDropHandle: value != 'article' ? true :false })
            if (value == "division") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl1Code: "",
                    hl2Name: "",
                    hl2Code: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "division",
                    poArticleSearch: "",
                    mrpRangeSearch: false

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Division'].name == '' ? 1 : 3,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.hierarchy['Division'].name,
                    sectionSearch: "",
                    departmentSearch: "",
                    articleSearch: "",
                    basedOn: "division",
                    supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false

                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)


            } else if (value == "section") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl1Code: "",
                    hl2Name: "",
                    hl2Code: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "section",
                    poArticleSearch: "",
                    mrpRangeSearch: false
                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Section'].name == '' ? 1 : 3,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: this.state.hierarchy['Section'].name,
                    departmentSearch: "",
                    articleSearch: "",
                    basedOn: "section",
                    //supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false
                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)
            } else if (value == "department") {
                this.setState({
                    poArticle: true,
                    poArticleAnimation: true,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: "",
                    hl1Code: "",
                    hl2Name: "",
                    hl2Code: "",
                    hl3Name: "",
                    hl3Code: "",
                    basedOn: "department",
                    poArticleSearch: "",
                    mrpRangeSearch: false,
                    isSave: true

                })
                let payload = {
                    pageNo: 1,
                    type: this.state.hierarchy['Department'].name == '' ? 1 : 3,
                    hl4Code: "",
                    hl4Name: "",
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: "",
                    sectionSearch: "",
                    departmentSearch: this.state.hierarchy['Department'].name,
                    articleSearch: "",
                    basedOn: "department",
                    // supplier: this.state.slCode,
                    mrpSearch: "",
                    isMrp: false,
                    mrpSearch: "",
                    mrpRangeSearch: false
                }
                this.props.poArticleRequest(payload)
                this.showDropDown(e)
            }            
        }
    }

    page = (e) => {
        console.log(e.target.id)
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.prePage != 0) {
                let data = {
                    type: this.state.hierarchy['Division'].name != '' || this.state.hierarchy['Section'].name != '' || this.state.hierarchy['Department'].name != '' ? 3 : 1,
                    no: this.props.purchaseIndent.poArticle.data.prePage,

                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.basedOn == "division" ? this.state.hierarchy['Division'].name : "",
                    sectionSearch: this.state.basedOn == "section" ? this.state.hierarchy['Section'].name : "",
                    departmentSearch: this.state.basedOn == "department" ? this.state.hierarchy['Department'].name : "",
                    articleSearch: "",
                    basedOn: this.state.basedOn,
                    supplier: this.state.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: false,
                }
                this.props.poArticleRequest(data);
                setTimeout(() => this.showDropDown(), 2);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.poArticle.data.prePage,
                current: this.props.purchaseIndent.poArticle.data.currPage,
                next: this.props.purchaseIndent.poArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.poArticle.data.currPage != this.props.purchaseIndent.poArticle.data.maxPage) {
                let data = {
                    type: this.state.hierarchy['Division'].name != '' || this.state.hierarchy['Section'].name != '' || this.state.hierarchy['Department'].name != '' ? 3 : 1,
                    no: this.props.purchaseIndent.poArticle.data.currPage + 1,
                    hl1Name: this.state.hierarchy['Division'].name,
                    hl1Code: this.state.hierarchy['Division'].code,
                    hl2Code: this.state.hierarchy['Section'].code,
                    hl2Name: this.state.hierarchy['Section'].name,
                    hl3Name: this.state.hierarchy['Department'].name,
                    hl3Code: this.state.hierarchy['Department'].code,
                    divisionSearch: this.state.basedOn == "division" ? this.state.hierarchy['Division'].name : "",
                    sectionSearch: this.state.basedOn == "section" ? this.state.hierarchy['Section'].name : "",
                    departmentSearch: this.state.basedOn == "department" ? this.state.hierarchy['Department'].name : "",
                    articleSearch: "",
                    basedOn: this.state.basedOn,
                    supplier: this.state.slCode,
                    searchBy: this.state.searchBy,
                    mrpSearch: "",
                    isMrp: false
                }
                this.props.poArticleRequest(data)
                setTimeout(() => this.showDropDown(), 2);
            }
            { this.state.isModalShow ? this.textInput.current.focus() : null }

        }        
    }


    setHierarchySelectedData = (data) => {
        this.setState({
            search: "",
            hierarchy: { ...this.state.hierarchy, Division: { name: data.hl1Name, code: data.hl1Code } || "", Section: { name: data.hl2Name, code: data.hl2Code } || "", Department: { name: data.hl3Name, code: data.hl3Code } || "", Article: { name: data.hl4Name, code: data.hl4Code } || "" },
            hierarchyDropHandle: false
        })
    }
    handleHierarchyModal = () => {
        this.setState({ hierarchyDropHandle: !this.state.hierarchyDropHandle })
    };

    render() {
        console.log('hierarchy', this.state.hierarchyDropHandle, 'article', this.state.isArticleOpen)
        const { hierarchy, articleInput } = this.state;
        let articleInputVal = "";
        if (articleInput.length != 0){
            articleInputVal = articleInput.map(item => Object.values(item).join("|")).join(',')
        }
        return(
            <div>
                <div className="container-fluid pad-0 pad-l50">
                    <div className="col-lg-12 pad-0">
                        <div className="otb-reports-filter p-lr-47">
                            <div className="orf-head">
                                <div className="orfh-left">
                                    <div className="orfhl-inner">
                                        <h3>Filters</h3> 
                                        {/* <span>2 filters applied</span> */}
                                    </div>
                                    {/* <button type="button">Remove</button> */}
                                </div>
                                {/* <div className="orfh-rgt"> */}
                                <div className="orf-body">
                                    <div className="col-lg-12 pad-0 m-top-20">
                                        <div className="orfb-apply-btn">
                                            {this.state.aritcleValue.length != 0 || this.state.monthList.length != 0 ? <button type="button" className="orfbab-apply" onClick={this.filterOtb}>Apply Filter</button> :
                                            <button type="button" className="orfbab-apply btnDisabled" >Apply Filter</button>}
                                            <button type="button" className="" onClick={this.clearFilter}>Clear</button>
                                        </div>
                                    </div>
                                    {/* <button className={this.state.showFilters === false ? "" : "orfh-btn"} onClick={(e) => this.openFilter(e)}>
                                        <img src={require('../../assets/down-arrow-new.svg')} />
                                    </button> */}
                                </div>
                            </div>
                            {this.state.showFilters && <div className="orf-body m-top-10">
                                <form>
                                    <div className="col-lg-12 pad-0">
                                        <div className="col-lg-2 pad-0 pad-r15">
                                            {/* <label className="pd-filter-label" fillRule="pddivision">Division</label> */}
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input divi" data-name="Division" placeholder="Division" value={hierarchy["Division"].name} onChange={(e) => this.handleSearch(e)} onKeyDown={(e) => this.setDropData(e, "division")} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" data-name="Division" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.hierarchyDropHandle && <DivisionModal {...this.state} page={this.page} setHierarchySelectedData={this.setHierarchySelectedData} handleHierarchyModal={this.handleHierarchyModal} currentActive={this.state.currentActive} hierarchyData={this.props.purchaseIndent.poArticle.data.resource || this.props.purchaseIndent.poArticle.data.resource || []} />}
                                            </div>
                                        </div>
                                        <div className="col-lg-2 pad-0 pad-r15">
                                            {/* <label className="pd-filter-label" fillRule="pddivision">Section</label> */}
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Section " data-name="Section" value={hierarchy["Section"].name} onChange={this.handleSearch} onKeyDown={(e) => this.setDropData(e, "section")}/>
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" data-name="Section" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 pad-0 pad-r15">
                                            {/* <label className="pd-filter-label" fillRule="pddivision">Department</label> */}
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Department" data-name="Department" value={hierarchy["Department"].name} onChange={this.handleSearch} onKeyDown={(e) => this.setDropData(e, "department")}/>
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" data-name="Department" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 pad-0 pad-r15">
                                            {/* <label className="pd-filter-label" fillRule="pddivision">Article</label> */}
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" data-name="Article" placeholder="Article" value={articleInputVal} onChange={(e) => this.handleSearch(e)} onFocus ={(e) => this.articleDropDown(e)} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" data-name="Article" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 pad-0 pad-r15">
                                            {/* <label className="pd-filter-label" fillRule="pddivision">Month</label> */}
                                            <div className="inputTextKeyFucMain set-vend-pos">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Month" value={this.state.monthList.join('|')} onFocus={(e) => this.showMonthDropDown(e)} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="openVendor" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <div className="col-lg-12 pad-0 m-top-20">
                                        <div className="orfb-apply-btn">
                                            <button type="button" className="orfbab-apply">Apply Filter</button>
                                            <button type="button" className="">Clear</button>
                                        </div>
                                    </div> */}
                                </form>
                            </div>}
                        </div>
                    </div>
                    <div className="col-lg-12 p-lr-47 m-top-30">
                        <div className="otb-reports-content">
                            <div className="otbrc-left">
                                <div className="otbrcl-heading">
                                    <h3>OTB Report</h3>
                                    <p>View and Download your OTB reports</p>
                                </div>
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className="nav-item active" >
                                            <a className="nav-link gsti-btn" href="#create" role="tab" data-toggle="tab">Top 5</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#managescheduler" role="tab" data-toggle="tab">Top 10</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#managescheduler20" role="tab" data-toggle="tab">Top 20</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link gsti-btn" href="#managescheduler50" role="tab" data-toggle="tab">Top 50</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="otbrc-right">
                                <button type="button" className="otbrc-email">Email
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 18.938 18.938">
                                            <g id="send_1_" data-name="send (1)">
                                                <path id="Path_945" fill="#fe9923" d="M119.919 16.018v5.835a.555.555 0 0 0 1 .333l2.851-3.8.869-.445 9.409-13.3.175-1.034-1.529.845-12.778 11.087z" data-name="Path 945" transform="translate(-115.481 -3.471)"/>
                                                <path id="Path_946" fill="#fedb41" d="M18.17.044C18.145.054.33 9 .306 9.011a.555.555 0 0 0-.043.969l4.175 2.569L18.74.135a.537.537 0 0 0-.57-.091z" data-name="Path 946"/>
                                                <path id="Path_947" fill="#fc3" d="M234.533 3.631c-.008-.008-.011-.02-.019-.027l-.109.154-10.343 14.628 6.4 3.94a.555.555 0 0 0 .836-.37l3.4-17.828a.552.552 0 0 0-.165-.497z" data-name="Path 947" transform="translate(-215.774 -3.471)"/>
                                            </g>
                                        </svg>
                                    </span>
                                </button>
                                <button type="button" className="otbrc-down-report" onClick = {this.downloadReport}>Download Report
                                    <span className="otbrcdr-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 p-lr-47 m-top-30">
                        <div className="vendor-gen-table" >
                            <div className="manage-table">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                <table className="table gen-main-table">
                                    <thead>
                                        <tr>
                                            <th><label>Division Name</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Section Name</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Department Name</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Article Code</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Name</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>MRP</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Month-Year</label><img src={require('../../assets/headerFilter.svg')}/></th>                                            
                                            <th><label>PO Amount</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Balance OTB</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                            <th><label>Indent Amount</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { this.state.otbItems.length != 0 ?
                                            this.state.otbItems.map(item=>(
                                                <tr key={item.id}>
                                                    <td><label>{item.hl1Name}</label></td>
                                                    <td><label>{item.hl2Name}</label></td>
                                                    <td><label>{item.hl3Name}</label></td> 
                                                    <td><label className="bold">{item.hl4Code}</label></td>
                                                    <td><label>{item.hl4Name}</label></td>
                                                    <td><label><span className="genmt-rupee-sign">&#8377;</span>{item.mrp}</label></td>
                                                    <td><label>{item.monthYear}</label></td>                                                    
                                                    <td><label><span className="genmt-rupee-sign">&#8377;</span>{item.poAmount}</label></td>
                                                    <td><label><span className="genmt-rupee-sign">&#8377;</span>{item.balanceOtb}</label></td>
                                                    <td><label><span className="genmt-rupee-sign">&#8377;</span>{item.piAmount}</label></td>                                                    
                                                </tr>
                                            )):
                                            <tr>
                                                <td align="center" colspan="12"><label>No Data Available</label></td>
                                            </tr>
                                        }                                    
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            {/* <span>Page :</span><input type="number" className="paginationBorder"  min="1" value="01" /> */}
                                            {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.otbItems.length}</span> */}
                                            <span >Total Items </span> <span className="bold">{this.state.otbItems.length}</span>
                                        </div>
                                    </div>
                                    {/* <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.isArticleOpen && <ArticleModal {...this.state} {...this.props} closeArticleDropDown={this.closeArticleDropDown} articleSubmit={this.articleSubmit}/>}
                {this.state.isMonthOpen && <MonthModal {...this.state} {...this.props} closeMonthDropDown={this.closeMonthDropDown} monthSubmit={this.monthSubmit}/>}
                {this.state.loader && <Loader/>}
            </div>
        )
    }
}
export default OtbReport;