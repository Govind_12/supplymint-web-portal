import React from 'react';

import SectionDataSelection from "../../components/purchaseIndent/sectionDataSelection";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import PoError from '../loaders/poError';
import Pagination from '../pagination';


class ItemUdf extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: false,
            filterBar: true,

            division: "",
            section: "",
            department: "",
            hl3Code: "",
            selectionModal: false,
            selectionAnimation: false,
            divisionState: [],
            loader: false,
            success: false,
            alert: false,
            code: "",
            successMessage: "",
            errorCode: "",
            errorMessage: "",

            activeCat: true,
            activeUdf: false,
            itemUdfData: [],
            payloadId: [],
            errorMassage: "",
            poErrorMsg: false,
            previousItem: [],
            catDescModification: "",

        };
    }
    componentDidMount() {
        window.setTimeout(function () {
            document.getElementById("dataPiBtn").focus()
        }, 0)
    }
    onActiveCat() {
        this.setState({
            activeCat: true,
            activeUdf: false
        })

    }
    onActiveUdf() {
        this.setState({
            activeCat: false,
            activeUdf: true
        })

    }
    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.divisionData.isLoading) {
            this.setState(
                {
                    loader: true
                }
            )
        }
        else if (nextProps.purchaseIndent.divisionData.isSuccess) {
            let c = []
            if (nextProps.purchaseIndent.divisionData.data.resource != null) {
                for (let i = 0; i < nextProps.purchaseIndent.divisionData.data.resource.length; i++) {

                    let x = nextProps.purchaseIndent.divisionData.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    divisionState: c,

                })
            }
            this.setState({
                loader: false,
                catDescModification: nextProps.purchaseIndent.divisionData.data.catDescModification
            })

            this.props.divisionSectionDepartmentClear();


        } else if (nextProps.purchaseIndent.divisionData.isError) {
            ("error")
            this.setState({
                errorMessage: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorCode,
                code: nextProps.purchaseIndent.divisionData.message.status,
                alert: true,

                loader: false

            })
            this.props.divisionSectionDepartmentClear();
        }
        if (nextProps.purchaseIndent.divisionData.isLoading || nextProps.purchaseIndent.createItemUdf.isLoading || nextProps.purchaseIndent.getCatDescUdf.isLoading) {
            this.setState(
                {
                    loader: true
                }
            )
        }
        if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {
            this.setState({
                previousItem: nextProps.purchaseIndent.getCatDescUdf.data.resource,
                itemUdfData: nextProps.purchaseIndent.getCatDescUdf.data.resource,

                loader: false
            })
            window.setTimeout(function () {
                document.getElementById("dataPiBtn").focus()
            }, 0)
            this.props.getCatDescUdfClear();


        } else if (nextProps.purchaseIndent.getCatDescUdf.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.getCatDescUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.getCatDescUdf.message.error.errorCode,
                code: nextProps.purchaseIndent.getCatDescUdf.message.status,
                alert: true,
                loader: false
            })
            this.props.getCatDescUdfClear();
        }
        if (nextProps.purchaseIndent.createItemUdf.isSuccess) {
            this.setState({
                success: true,
                loader: false,
                payloadId: [],
                successMessage: nextProps.purchaseIndent.createItemUdf.data.message,
            })
            this.props.createItemUdfClear();
        } else if (nextProps.purchaseIndent.createItemUdf.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.createItemUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.createItemUdf.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.createItemUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.createItemUdf.message.error.errorCode,
                code: nextProps.purchaseIndent.createItemUdf.message.status,
                alert: true,
                loader: false
            })
            this.props.createItemUdfClear();
        }
    }
    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {

        } else {
            this.setState({
                type: 3,
                isCompulsary: "",
                displayName: "",
                udfType: "",
            })
            let data = {
                type: 3,
                no: 1,
                isCompulsary: "",
                displayName: "",
                udfType: "",
                search: this.state.search,
                ispo: false
            }
            this.props.getUdfMappingRequest(data)
        }
    }
    handleChange(id, categories, e) {
        if (e.key != "Tab") {
            let ids = id
            let payloadId = this.state.payloadId
            if (!payloadId.includes(ids)) {

                payloadId.push(ids)
            }
            this.setState({
                payloadId: payloadId
            })

            let itemUdfData = this.state.itemUdfData
            let cat = itemUdfData.categories
            let cat_desc_udf = itemUdfData.cat_desc_udf
            let isChanged = false
            if (categories == "categories") {

                if (e.target.id == "nameCat") {
                    for (var i = 0; i < cat.length; i++) {

                        if (cat[i].id == id) {
                            cat[i].name = e.target.value
                        }
                    }

                } else if (e.target.id == "displayNameCat") {
                    let flag = false
                    for (var i = 0; i < cat.length; i++) {

                        if (cat[i].id == id) {
                            if (cat[i].displayName == e.target.value) {
                                isChanged = false
                            } else {
                                isChanged = true

                                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                                    cat[i].displayName = e.target.value

                                } else {

                                    flag = this.checkDisplayName(e.target.value, id)

                                    if (flag) {
                                        cat[i].displayName = ""
                                    } else {
                                        cat[i].displayName = e.target.value
                                    }
                                }
                            }
                        }
                    }
                } else if (e.target.id == "orderByCat") {
                    for (var i = 0; i < cat.length; i++) {

                        if (cat[i].id == id) {
                            if (e.target.validity.valid) {
                                cat[i].orderBy = e.target.value
                            }
                        }
                    }

                }
                if (isChanged) {

                    this.setState({
                        itemUdfData: itemUdfData
                    })
                }


            } else if (categories == "cat_desc_udf") {
                if (e.target.id == "nameUdf") {
                    for (var i = 0; i < cat_desc_udf.length; i++) {

                        if (cat_desc_udf[i].id == id) {
                            cat_desc_udf[i].name = e.target.value
                        }


                    }
                } else if (e.target.id == "displayNameUdf") {
                    for (var i = 0; i < cat_desc_udf.length; i++) {

                        if (cat_desc_udf[i].id == id) {
                            cat_desc_udf[i].displayName = e.target.value
                        }


                    }
                } else if (e.target.id == "orderByUdf") {
                    let flag = false
                    cat.forEach(c => {
                        cat_desc_udf.forEach(u => {
                            if (c.orderBy == e.target.value && u.orderBy == e.target.value) {
                                flag = true
                            }
                        })
                    })

                    if (flag == false) {
                        for (var i = 0; i < cat_desc_udf.length; i++) {

                            if (cat_desc_udf[i].id == id) {
                                if (e.target.validity.valid) {
                                    cat_desc_udf[i].orderBy = e.target.value
                                }
                            }


                        }
                    } else {
                        for (var i = 0; i < cat_desc_udf.length; i++) {

                            if (cat_desc_udf[i].id == id) {
                                if (e.target.validity.valid) {
                                    cat_desc_udf[i].orderBy = ""
                                }
                            }
                        }
                    }
                }
                this.setState({
                    itemUdfData: itemUdfData
                })
            }
        }
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    closeSelection(e) {
        this.setState({
            selectionModal: false,
            selectionAnimation: !this.state.selectionAnimation
        });
        window.setTimeout(() => {
            document.getElementById("dataPiBtn").focus()
        }, 0)
    }
    openSelection(e) {
        var data = {
            no: 1,
            type: "",
            hl1name: "",
            hl2name: "",
            hl3name: "",
            search: ""
        }
        this.props.divisionSectionDepartmentRequest(data);
        this.setState({
            department: this.state.department,
            selectionModal: true,
            selectionAnimation: !this.state.selectionAnimation

        });
        document.onkeydown = function (t) {

            if (t.which == 9) {
                return false;
            }
        }
    }

    updateState(upData) {
        var sectionData = []

        this.setState({

            division: upData.hl1Name,
            section: upData.hl2Name,
            department: upData.hl3Name,
            hl3Code: upData.hl3Code

        })
        window.setTimeout(() => {
            document.getElementById("getData").focus()
        }, 0)
    }

    getData() {
        let data = {
            hl3Code: this.state.hl3Code,
            ispo: false,
            hl3Name: this.state.department
        }
        console.log(data)
        this.props.getCatDescUdfRequest(data)
    }

    handleCheck(data, categories, id) {

        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {

            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })
        let itemUdfData = this.state.itemUdfData
        let cat = itemUdfData.categories
        let cat_desc_udf = itemUdfData.cat_desc_udf
        if (categories == "cat") {
            if (data == "isCompulsoryPO") {
                for (var i = 0; i < cat.length; i++) {
                    if (cat[i].id == id) {
                        if (cat[i].isCompulsoryPO == "N") {
                            cat[i].isCompulsoryPO = "Y"

                        } else {
                            cat[i].isCompulsoryPO = "N"
                        }
                    }
                }
                this.setState({
                    itemUdfData: itemUdfData
                })
            } else if (data == "isLovPO") {
                for (var i = 0; i < cat.length; i++) {
                    if (cat[i].id == id) {
                        if (cat[i].isLovPO == "N") {
                            cat[i].isLovPO = "Y"
                        } else {
                            cat[i].isLovPO = "N"
                        }


                    }
                }
                this.setState({
                    itemUdfData: itemUdfData
                })
            }
        }
        else if (categories == "cat_desc_udf") {
            if (data == "isCompulsoryPO") {
                for (var i = 0; i < cat_desc_udf.length; i++) {
                    if (cat_desc_udf[i].id == id) {
                        if (cat_desc_udf[i].isCompulsoryPO == "N") {
                            cat_desc_udf[i].isCompulsoryPO = "Y"

                        } else {
                            cat_desc_udf[i].isCompulsoryPO = "N"

                        }
                    }
                }
                this.setState({
                    itemUdfData: itemUdfData
                })
            } else if (data == "isLovPO") {
                for (var i = 0; i < cat_desc_udf.length; i++) {
                    if (cat_desc_udf[i].id == id) {
                        if (cat_desc_udf[i].isLovPO == "N") {
                            cat_desc_udf[i].isLovPO = "Y"
                        } else {
                            cat_desc_udf[i].isLovPO = "N"
                        }
                    }
                }
                this.setState({
                    itemUdfData: itemUdfData
                })
            }



        }


    }

    onSubmit() {
        let itemUdfData = this.state.itemUdfData
        let payloadId = this.state.payloadId

        let catDescKey = {
            categories: [],
            cat_desc_udf: []
        }

        itemUdfData.categories.forEach(cat => {
            if (payloadId.includes(cat.id)) {
                let catPayload = {
                    id: cat.id,
                    cat_desc_udf: cat.cat_desc_udf,
                    displayName: cat.displayName,
                    isCompulsoryPO: cat.isCompulsoryPO,
                    orderBy: cat.orderBy,
                    isLovPO: cat.isLovPO,
                    cat_desc_udf_type: cat.cat_desc_udf_type,
                    hl3code: this.state.hl3Code

                }
                catDescKey.categories.push(catPayload)
            }

        })

        itemUdfData.cat_desc_udf.forEach(cat_desc_udf => {
            if (payloadId.includes(cat_desc_udf.id)) {
                let udfPayload = {
                    id: cat_desc_udf.id,
                    cat_desc_udf: cat_desc_udf.cat_desc_udf,
                    displayName: cat_desc_udf.displayName,
                    isCompulsoryPO: cat_desc_udf.isCompulsoryPO,
                    orderBy: cat_desc_udf.orderBy,
                    isLovPO: cat_desc_udf.isLovPO,
                    cat_desc_udf_type: cat_desc_udf.cat_desc_udf_type,
                    hl3code: this.state.hl3Code

                }
                catDescKey.cat_desc_udf.push(udfPayload)
            }

        })

        if (payloadId.length != 0) {
            let payload = {
                catDescKey: catDescKey,
                hl3Code: this.state.hl3Code
            }
            this.props.createItemUdfRequest(payload)
            this.setState({

            })

        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "There is no change in data..."
            })
        }

    }
    onResetData() {
        let data = {
            hl3Name: this.state.department,
            hl3Code: this.state.hl3Code,
            hl4Code: "",
            ispo: false
        }
        this.setState({
            payloadId: []
        })
        this.props.getCatDescUdfRequest(data)
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    // _________________TAB FUNCTIONALITY__________________________
    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }
    }

    focusItemCat(id, e) {
        if (e.key === "Enter") {
            this.handleCheck("isCompulsoryPO", "cat_desc_udf", id)
        }

    }
    focusLovKeyDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck("isLovPO", "cat_desc_udf", id)
        }
        if (e.key === "Tab") {
            if (this.state.itemUdfData.cat_desc_udf[this.state.itemUdfData.cat_desc_udf.length - 1].id == id && this.state.payloadId.length == 0) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemUdfData.cat_desc_udf[0].id).blur();
                    document.getElementById("dataPiBtn").focus()
                }, 0);
            } else if (this.state.itemUdfData.cat_desc_udf[this.state.itemUdfData.cat_desc_udf.length - 1].id == id && this.state.payloadId.length != 0) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemUdfData.cat_desc_udf[0].id).blur();
                    document.getElementById("reset").focus();
                }, 0);
            }
        }
    }
    focusreset(e) {
        if (e.key == "Enter") {
            this.onResetData();
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("reset").blur();
                document.getElementById("saveButton").focus()
            }, 0);
        }
    }
    focusSave(e) {
        if (e.key === "Enter") {
            this.onSubmit();
        }
        if (e.key === "Tab") {
            document.getElementById("dataPiBtn").focus()
            e.preventDefault()
        }
    }

    isCompulsaryKeyDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck("isCompulsoryPO", "cat", id)
        }
    }
    isLovDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck("isLovPO", "cat", id)
        }
        if (e.key === "Tab") {
            if (this.state.itemUdfData.categories[this.state.itemUdfData.categories.length - 1].id == id && this.state.payloadId.length == 0) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemUdfData.categories[0].id).blur();
                    document.getElementById("dataPiBtn").focus()
                }, 0);
            } else if (this.state.itemUdfData.categories[this.state.itemUdfData.categories.length - 1].id == id && this.state.payloadId.length != 0) {
                window.setTimeout(() => {
                    document.getElementById(this.state.itemUdfData.categories[0].id).blur();
                    document.getElementById("reset").focus();
                }, 0);
            }
        }
    }

    checkDisplayName(data, id) {


        let array = this.state.itemUdfData

        let flag = false
        let count = 0
        for (let i = 0; i < array.categories.length; i++) {
            if (array.categories[i].displayName == data.toUpperCase().trim()) {

                count++

                if (count >= 1) {


                    flag = true
                    this.setState({
                        poErrorMsg: true,
                        errorMassage: "Display name already  exist"
                    })
                }
            }
        }

        return flag


    }

    render() {
        return (
            <div className="container-fluid p-lr-47">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design pad-0">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search"></div>
                                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                {this.state.department != "" ? <span>{this.state.payloadId.length == 0 ? <button className="btnDisabled" type="reset" disabled>Reset</button>
                                    : <button className="gen-clear" type="reset" id="reset" onKeyDown={(e) => this.focusreset(e)} onClick={(e) => this.onResetData(e)}>Reset</button>}
                                </span> : null}
                                {this.state.payloadId.length == 0 ? <button type="button" className="btnDisabled">Save</button>
                                    : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.focusSave(e)} className="gen-save" id="saveButton">Save</button>}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 col-sm-12 col-xs-12 p-lr-47 udfMappingMain itemUdfSet">
                    <div className="container_content heightAuto pipo-con">
                        <div className="col-md-12 pad-0">
                            <div className="col-md-9 col-sm-12 m-top-20 pad-0">
                                <div className="piFormDiv ">
                                    <label className="purchaseLabel">
                                        Department
                                </label>
                                    <input type="text" className="purchaseSelectBox" value={this.state.department} placeholder="Choose Department" disabled />
                                </div>
                                <div className="piFormDiv">
                                    <label className="purchaseLabel">
                                        Division
                                    </label>
                                    <input type="text" className="purchaseSelectBox" value={this.state.division} placeholder="Select  Division" disabled />
                                </div>
                                <div className="piFormDiv ">
                                    <label className="purchaseLabel">
                                        Section
                                    </label>
                                    <input type="text" className="purchaseSelectBox" value={this.state.section} placeholder="Choose Section" disabled /></div>
                                <div className="piFormDiv m0 width_15">
                                    <button className="dataPiBtn" type="button" id="dataPiBtn" onKeyDown={(e) => this._handleKeyPress(e, "dataPiBtn")} onClick={(e) => this.openSelection(e)}>
                                        Choose Data
                                    </button>
                                </div>
                                {this.state.department != "" ? <button className="search_button_vendor" type="button" id="getData" onClick={() => this.getData()}>Search</button> : null}
                            </div>
                            <div className="col-md-3">
                            </div>
                        </div>

                        <div className="changeSettingContain">
                            <div className="col-md-12 col-sm-12 switchTabs pad-0 m-top-30">
                                <ul className="list_style ">
                                    <li className={this.state.activeCat ? "m-rgt-25 activeTab displayPointer" : "m-rgt-25 displayPointer"} onClick={(e) => this.onActiveCat(e)}>
                                        <label className="displayPointer">CAT/DESC </label>
                                    </li>
                                    <li className={this.state.activeUdf ? "displayPointer activeTab" : "displayPointer"} onClick={(e) => this.onActiveUdf(e)}>
                                        <label className="displayPointer">UDF </label>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        {this.state.activeCat ? <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-40 tableGeneric siteMapppingMain udfMappingTable itemUdfTable">
                            <div className="tableHeadFix pipo-con-table">
                                <div className="scrollableTableFixed table-scroll zui-scroller scrollableOrgansation orgScroll m0 tableHeadFixHeight">
                                    <table className="table zui-table sitemappingTable gen-main-table">
                                        <thead >
                                            <tr>
                                                <th><label>Name</label></th>
                                                <th className="width145"><label>Display Name</label></th>
                                                <th><label>Is Compulsory PI</label></th>
                                                <th><label>Is Compulsory PO</label></th>
                                                <th><label>Order By </label></th>
                                                <th><label>Is LOV PI(List of values)</label></th>
                                                <th><label>Is LOV PO(List of values)</label></th>

                                            </tr>
                                        </thead>

                                        <tbody>

                                            {this.state.itemUdfData == null || this.state.itemUdfData.length == 0 ? <tr className="tableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.itemUdfData.categories.map((data, idxxx) => (
                                                (data.displayName == "COLOR" || data.displayName == "SIZE" || data.displayName == "MRP") && sessionStorage.getItem("partnerEnterpriseName") != "VMART" ?
                                                    <tr id={idxxx} key={idxxx} className="bg-disable">
                                                        <td> <div className="col-md-12 col-sm-12 pad-0">

                                                            <input type="text" className="modal_tableBox " id="nameCat" value={data.cat_desc_udf == null ? "" : data.cat_desc_udf} readOnly></input>

                                                        </div></td>

                                                        <td> <div className="col-md-12 col-sm-12 pad-0">

                                                            <input type="text" className="modal_tableBox " id="displayNameCat" value={data.displayName == null ? "" : data.displayName} disabled></input>

                                                        </div></td>

                                                        <td className="col-md-12 col-sm-12 pad-0">
                                                            <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isCompulsoryCatPi" disabled checked={true} name={"isCompulsoryPI" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                        </td>
                                                        <td className="col-md-12 col-sm-12 pad-0">
                                                            <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isCompulsoryCatPo" disabled checked={true} name={"isCompulsoryPO" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                        </td>
                                                        <td> <div className="col-md-12 col-sm-12 pad-0">

                                                            <input type="text" className="modal_tableBox" id="orderByCat" pattern="[0-9]*" value={data.orderBy == null ? "" : data.orderBy} disabled></input>

                                                        </div></td>
                                                        <td className="col-md-12 col-sm-12 pad-0">
                                                            <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" id="isLovCatPi" disabled className="checkBoxTab" checked={true} name={"isLovPI" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                        </td>
                                                        <td className="col-md-12 col-sm-12 pad-0">
                                                            <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" id="isLovCatPo" disabled className="checkBoxTab" checked={true} name={"isLovPO" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                        </td>


                                                    </tr>




                                                    : <tr id={idxxx} key={idxxx}>
                                                        <td> <div className="col-md-12 col-sm-12 pad-0">

                                                            <input type="text" className="modal_tableBox " id="nameCat" onChange={(e) => this.handleChange(`${data.id}`, "categories", e)} value={data.cat_desc_udf == null ? "" : data.cat_desc_udf} readOnly></input>

                                                        </div></td>

                                                        <td> <div className="col-md-12 col-sm-12 pad-0">


                                                            <input type="text" className="modal_tableBox " id="displayNameCat" onChange={(e) => this.handleChange(`${data.id}`, "categories", e)} value={data.displayName == null ? "" : data.displayName}></input>

                                                        </div></td>

                                                        <td className="col-md-12 col-sm-12 pad-0">
                                                            <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isCompulsoryCat" onKeyDown={(e) => this.isCompulsaryKeyDown(`${data.id}`, e)} onChange={(e) => this.handleCheck("isCompulsoryPO", "cat", `${data.id}`)} checked={data.isCompulsoryPO == "N" ? false : true} name={"isCompulsoryPO" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                        </td>
                                                        <td> <div className="col-md-12 col-sm-12 pad-0">

                                                            <input type="text" className="modal_tableBox" id="orderByCat" pattern="[0-9]*" onChange={(e) => this.handleChange(`${data.id}`, "categories", e)} value={data.orderBy == null ? "" : data.orderBy}></input>

                                                        </div></td>
                                                        <td className="col-md-12 col-sm-12 pad-0">
                                                            <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" id="isLovCat" className="checkBoxTab" onKeyDown={(e) => this.isLovDown(`${data.id}`, e)} onChange={(e) => this.handleCheck("isLovPO", "cat", `${data.id}`)} checked={data.isLovPO == "N" ? false : true} name={"isLovPO" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                        </td>


                                                    </tr>))}
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> : null}

                        {this.state.activeUdf ? <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-40 tableGeneric siteMapppingMain udfMappingTable itemUdfTable">
                            <div className="tableHeadFix pipo-con-table">
                                <div className="scrollableTableFixed table-scroll zui-scroller scrollableOrgansation orgScroll m0 tableHeadFixHeight">
                                    <table className="table zui-table sitemappingTable gen-main-table">
                                        <thead >
                                            <tr>
                                                <th><label>Name</label></th>
                                                <th className="width145"><label>Display Name</label></th>
                                                <th><label>Is Compulsory PI</label></th>
                                                <th><label>Is Compulsory PO</label></th>
                                                <th><label>Order By</label></th>
                                                <th><label>Is LOV PI(List of values)</label></th>
                                                <th><label>Is LOV PO(List of values)</label></th>

                                            </tr>
                                        </thead>

                                        <tbody>


                                            {this.state.itemUdfData == null || this.state.itemUdfData.length == 0 ? <tr className="tableNoData"><td colSpan="5"> NO DATA FOUND </td></tr> : this.state.itemUdfData.cat_desc_udf.map((data, idxxx) => (
                                                <tr id={idxxx} key={idxxx}>
                                                    <td> <div className="col-md-12 col-sm-12 pad-0">

                                                        <input type="text" className="modal_tableBox " id="nameUdf" onChange={(e) => this.handleChange(`${data.id}`, "cat_desc_udf", e)} value={data.cat_desc_udf == null ? "" : data.cat_desc_udf} readOnly></input>

                                                    </div></td>

                                                    <td> <div className="col-md-12 col-sm-12 pad-0">

                                                        <input type="text" className="modal_tableBox " id="displayNameUdf" onChange={(e) => this.handleChange(`${data.id}`, "cat_desc_udf", e)} value={data.displayName == null ? "" : data.displayName}></input>

                                                    </div></td>


                                                    <td className="col-md-12 col-sm-12 pad-0">
                                                        <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isCompulsoryUdfPi" onKeyDown={(e) => this.focusItemCat(`${data.id}`, e)} onChange={(e) => this.handleCheck("isCompulsoryPI", "cat_desc_udf", `${data.id}`)} checked={data.isCompulsoryPI == "N" ? false : true} name={"isCompulsoryPI" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                    </td>
                                                    <td className="col-md-12 col-sm-12 pad-0">
                                                        <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isCompulsoryUdfPo" onKeyDown={(e) => this.focusItemCat(`${data.id}`, e)} onChange={(e) => this.handleCheck("isCompulsoryPO", "cat_desc_udf", `${data.id}`)} checked={data.isCompulsoryPO == "N" ? false : true} name={"isCompulsoryPO" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                    </td>
                                                    <td> <div className="col-md-12 col-sm-12 pad-0">

                                                        <input type="text" className="modal_tableBox " id="orderByUdf" pattern="[0-9]*" onChange={(e) => this.handleChange(`${data.id}`, "cat_desc_udf", e)} value={data.orderBy == null ? "" : data.orderBy}></input>

                                                    </div></td>
                                                    <td className="col-md-12 col-sm-12 pad-0">
                                                        <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isLovUdfPi" onKeyDown={(e) => this.focusLovKeyDown(`${data.id}`, e)} onChange={(e) => this.handleCheck("isLovPI", "cat_desc_udf", `${data.id}`)} checked={data.isLovPI == "N" ? false : true} name={"isLovPI" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                    </td>
                                                    <td className="col-md-12 col-sm-12 pad-0">
                                                        <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" id="isLovUdfPo" onKeyDown={(e) => this.focusLovKeyDown(`${data.id}`, e)} onChange={(e) => this.handleCheck("isLovPO", "cat_desc_udf", `${data.id}`)} checked={data.isLovPO == "N" ? false : true} name={"isLovPO" + data.id} id={data.id} /> <span className="checkmark1"></span> </label>
                                                    </td>

                                                </tr>


                                            ))}
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> : null}


                        {/* <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                            <div className="footerDivForm height4per">
                                <ul className="list-inline m-lft-0 m-top-10">
                                    {this.state.department != "" ? <li>{this.state.payloadId.length == 0 ? <button className="textDisable pointerNone clear_button_vendor" type="reset" disabled>Reset</button> : <button className="clear_button_vendor" type="reset" id="reset" onKeyDown={(e) => this.focusreset(e)} onClick={(e) => this.onResetData(e)}>Reset</button>}
                                    </li> : null}
                                    <li>{this.state.payloadId.length == 0 ? <button type="button" className="btnDisabled save_button_vendor">Save</button> : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.focusSave(e)} className="save_button_vendor" id="saveButton">Save</button>}</li>
                                </ul>
                            </div>
                        </div> */}

                    </div>
                </div>
                {this.state.selectionModal ? <SectionDataSelection {...this.props} {...this.state} department={this.state.department} divisionState={this.state.divisionState} updateState={(e) => this.updateState(e)} selectionAnimation={this.state.selectionAnimation} closeSelection={(e) => this.closeSelection(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}

export default ItemUdf;