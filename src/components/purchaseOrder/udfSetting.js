import React from 'react';
import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import SearchImage from '../../assets/searchicon.svg';
import searchIcon from '../../assets/clearSearch.svg';

class UdfSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: false,
            filterBar: true,
            udfMappingData: [],

            id: "",
            type: 1,
            search: "",
            isCompulsary: "",
            displayName: "",
            udfType: "",
            toastLoader: false,
            toastMsg: "",
            payloadId: [],
            poErrorMsg: false,

            errorMassage: "",
            loader: false,

            success: false,
            alert: false,
            code: "",
            successMessage: "",
            errorCode: "",
            errorMessage: "",
            searchBy:"contains"


        };
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }

    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                isCompulsary: "",
                displayName: "",
                udfType: "",
            })
            let data = {
                type: 3,

                isCompulsary: "",
                displayName: "",
                udfType: "",

                search: this.state.search,
                ispo: false,
            searchBy:this.state.searchBy
                
            }
            this.props.getUdfMappingRequest(data)
        }
    }

    componentWillMount() {
        if (!this.props.purchaseIndent.getUdfMapping.isSuccess) {
            let data = {

                type: 1,
                search: "",
                isCompulsary: "",
                displayName: "",
                udfType: "",
                ispo: false,
                searchBy:this.state.searchBy
            }
            this.props.getUdfMappingRequest(data);
        } else {
            let data = {

                type: 1,
                search: "",
                isCompulsary: "",
                displayName: "",
                udfType: "",
                ispo: false,
                searchBy:this.state.searchBy
            }
            this.props.getUdfMappingRequest(data);
        }
        if (this.props.purchaseIndent.getUdfMapping.isSuccess) {

            this.setState({
                udfMappingData: this.props.purchaseIndent.getUdfMapping.data.resource,

            })

        }

    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.udfSetting.isSuccess) {

            this.setState({
                success: true,
                successMessage: nextProps.purchaseIndent.udfSetting.data.message,
                loader: false,
                payloadId: []
            })
            this.props.createUdfSettingRequest();

        } else if (nextProps.purchaseIndent.udfSetting.isError) {

            this.setState({
                errorMessage: nextProps.purchaseIndent.udfSetting.message.error == undefined ? undefined : nextProps.purchaseIndent.udfSetting.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.udfSetting.message.error == undefined ? undefined : nextProps.purchaseIndent.udfSetting.message.error.errorCode,
                code: nextProps.purchaseIndent.udfSetting.message.status,
                alert: true,

                loader: false

            })
            this.props.createUdfSettingRequest();
        } else if (!nextProps.purchaseIndent.udfSetting.isLoading) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {

            this.setState({
                udfMappingData: nextProps.purchaseIndent.getUdfMapping.data.resource,

            })

            window.setTimeout(() => {
                document.getElementById("search").focus()
            })
        }
        else if (nextProps.purchaseIndent.getUdfMapping.isError) {

            this.setState({
                errorMessage: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorCode,
                code: nextProps.purchaseIndent.getUdfMapping.message.status,
                alert: true,

                loader: false

            })
            this.props.getUdfMappingClear();
        }
        else if (!nextProps.purchaseIndent.getUdfMapping.isLoading) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.purchaseIndent.udfSetting.isLoading || nextProps.purchaseIndent.getUdfMapping.isLoading
        ) {
            this.setState({
                loader: true
            })
        }
    }

    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            search: "",
            isCompulsary: "",
            displayName: "",
            udfType: ""
        })
        let data = {
            type: 1,
            search: "",
            isCompulsary: "",
            displayName: "",
            udfType: "",
            ispo: false,
            searchBy:this.state.searchBy
        }
        this.props.getUdfMappingRequest(data);
    }

    onClearFilter(e) {
        e.preventDefault();
        this.setState({
            type: 1,

            isCompulsary: "",
            displayName: "",
            udfType: ""
        })
        let data = {
            type: 1,

            search: "",
            isCompulsary: "",
            displayName: "",
            udfType: "",
            ispo: false,
            searchBy:this.state.searchBy
        }
        this.props.getUdfMappingRequest(data);
    }
    handleChangee(id, e) {
        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {

            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })
        let udfMappingData = this.state.udfMappingData
        for (let i = 0; i < udfMappingData.length; i++) {
            if (udfMappingData[i].id == id) {
                udfMappingData[i].displayName = e.target.value
            }

        }
        this.setState({
            udfMappingData: udfMappingData
        })
    }

    handleChange(id, e) {

        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {

            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })
        let udfMappingData = this.state.udfMappingData



        for (var i = 0; i < udfMappingData.length; i++) {
            if (udfMappingData[i].id == id) {
                if (e.target.validity.valid) {
                    udfMappingData[i].orderBy = e.target.value
                }
            }


        }

        this.setState({
            udfMappingData: udfMappingData
        })


    }
    // checkOrderBy(id, e) {
    //     let udfMappingData = this.state.udfMappingData
    //     let orderByData = []
    //     let flag = false
    //     udfMappingData.forEach(s => {
    //         let size = Number(s.orderBy)

    //         orderByData.push(size)

    //     })
    //     let unique = [];
    //     orderByData.forEach(function (item) {
    //         if (item != 0) {
    //             if (!unique[item])
    //                 unique[item] = 0;
    //             unique[item] += 1;
    //         }
    //     })

    //     for (var prop in unique) {
    //         if (unique[prop] >= 2) {
    //             flag = true
    //         }
    //     }



    //     if (flag) {
    //         for (let i = 0; i < udfMappingData.length; i++) {


    //             if (udfMappingData[i].id == id) {

    //                 udfMappingData[i].orderBy = ""
    //             }
    //         }




    //         this.setState({
    //             udfMappingData: udfMappingData,
    //             poErrorMsg: true,
    //             errorMassage: "order by no. can't be same"
    //         })

    //     }

    // }

    handleCheck(id) {
        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {

            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })

        let udfMappingData = this.state.udfMappingData
        for (let i = 0; i < udfMappingData.length; i++) {
            if (udfMappingData[i].id == id) {
                // if (udfMappingData[i].displayName != null) {
                if (udfMappingData[i].isCompulsary == "Y") {

                    udfMappingData[i].isCompulsary = "N"



                } else {

                    udfMappingData[i].isCompulsary = "Y"

                }

                // } 


            }
        }
        this.setState({
            udfMappingData: udfMappingData
        })

    }
    onResetData() {
        let data = {

            type: 1,
            search: "",
            isCompulsary: "",
            displayName: "",
            udfType: "",
            ispo: false,
            searchBy:this.state.searchBy
        }
        this.setState({
            payloadId: []
        })
        this.props.getUdfMappingRequest(data);
    }

    onSubmit() {

        let udfMappingData = this.state.udfMappingData
        let payloadId = this.state.payloadId

        let submitPayload = []
        if (payloadId.length != 0) {
            udfMappingData.forEach(u => {
                if (payloadId.includes(u.id.toString())) {
                    let catPayload = {
                        checked: u.checked,
                        displayName: u.displayName,
                        id: u.id,
                        isCompulsary: u.isCompulsary,
                        orderBy: u.orderBy,
                        udfType: u.udfType,
                        isLovPO: u.isLovPO
                    }
                    submitPayload.push(catPayload)
                }

            })
            this.props.createUdfSettingRequest(submitPayload)
        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "There is no change in data..."
            })
        }

    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }

    handleCheckOrderBy(id) {
        let ids = id
        let payloadId = this.state.payloadId
        if (!payloadId.includes(ids)) {

            payloadId.push(ids)
        }
        this.setState({
            payloadId: payloadId
        })

        let udfMappingData = this.state.udfMappingData
        for (let i = 0; i < udfMappingData.length; i++) {
            if (udfMappingData[i].id == id) {
                if (udfMappingData[i].isLovPO == "N") {
                    udfMappingData[i].isLovPO = "Y"
                } else {
                    udfMappingData[i].isLovPO = "N"
                }
            }
        }
        this.setState({
            udfMappingData: udfMappingData
        })
    }


    handleSearch(e) {
        this.setState({
            search: e.target.value
        })
    }
    isCompulsaryKeyDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id)
        }
    }
    isLovDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheckOrderBy(id)
        }
        if (e.key === "Tab") {
            if (this.state.udfMappingData[this.state.udfMappingData.length - 1].id == id && this.state.payloadId.length == 0) {
                window.setTimeout(() => {
                    document.getElementById("search").focus()
                }, 0);
            } else if (this.state.udfMappingData[this.state.udfMappingData.length - 1].id == id && this.state.payloadId.length != 0) {
                window.setTimeout(() => {
                    document.getElementById("reset").focus();
                }, 0);
            }
        }
    }
    onresetKeyDown(e) {
        if (e.key === "Enter") {
            this.onResetData()
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById("saveButton").focus()
            })
        }
    }
    onKeyDownSave(e) {
        if (e.key === "Enter") {
            this.onSubmit()
        }
        if (e.key === "Tab") {
            document.getElementById("search").focus()
            e.preventDefault()
        }
    }
    searchKeydown(e) {
        if (e.key === "Tab") {
            if (this.state.udfMappingData == null) {
                window.setTimeout(() => {
                    document.getElementById("clearSearchFilter").focus()
                }, 0)
                e.preventDefault()
            }
        }
    }
    handleChangeSearch(e){
        if(e.target.id=="searchByudfset")
        this.setState({
            searchBy:e.target.value
        },()=>{
            if(this.state.search!=""){
        let data = {
            type: this.state.type,

            search: this.state.search,
            isCompulsary: "",
            displayName: "",
            udfType: "",
            ispo: false,
            searchBy:this.state.searchBy
        }
        this.props.getUdfMappingRequest(data);
            }
        })

    }

    render() {
        return (
            <div className="container-fluid pad-l50">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <form onSubmit={(e) => this.onSearch(e)}>
                                        <input type="search" id="search" onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." />
                                        <img className="search-image" src={SearchImage} onClick={(e) => this.searchKeydown(e)} />
                                        {this.state.type != 3 ? null : <button className="closeSearch" id="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></button>}
                                    </form>
                                </div>
                                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByudfset" name="searchByudfset" value={this.state.searchBy} onChange={(e) => this.handleChangeSearch(e)}>
                                    <option value="contains">Contains</option>
                                    <option value="startWith"> Start with</option>
                                    <option value="endWith">End with</option>
                                </select> : null}
                                {this.state.payloadId.length == 0 ? <button className="btnDisabled" >Reset</button>
                                    : <button className="gen-clear" id="reset" type="reset" onKeyDown={(e) => this.onresetKeyDown(e)} onClick={(e) => this.onResetData(e)}>Reset</button>}
                                {this.state.payloadId.length == 0 ? <button type="button" className="btnDisabled">Save</button>
                                    : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.onKeyDownSave(e)} className="gen-save" id="saveButton">Save</button>}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 p-lr-47 m-top-20 tableGeneric siteMapppingMain udfMappingTable tableScroll tableHeadFix">
                    <div className="container_content pipo-con">
                        <div className="pipo-con-table">
                            <div className="scrollableTableFixed table-scroll scrollableOrgansation itemUdfSet tableHeadFixHeight" id="table-scroll">
                                <table className="table zui-table sitemappingTable itemUdfTable gen-main-table">
                                    <thead >
                                        <tr>
                                            <th className="width145"><label>UDF Type</label></th>
                                            <th><label>Display Name</label></th>

                                            <th><label>Order By</label></th>
                                            <th ><label>Is Compulsory</label></th>
                                            <th><label>Is LOV (List of values)</label></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {this.state.udfMappingData == "" || this.state.udfMappingData == undefined || this.state.udfMappingData == null || this.state.udfMappingData == "" || this.state.udfMappingData.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.udfMappingData.map((data, key) => (

                                            <tr key={key}>

                                                <td className="width145"><label>{data.udfType}</label></td>
                                                <td> <div className="col-md-12 col-sm-12 pad-0">

                                                    <input type="text" className="modal_tableBox " id={data.udfType} onChange={(e) => this.handleChangee(`${data.id}`, e)} value={data.displayName == null ? "" : data.displayName}></input>

                                                </div></td>
                                                <td className="pad-lft-15"><input pattern="[0-9]*" onChange={(e) => this.handleChange(`${data.id}`, e)} value={data.orderBy == null ? "" : data.orderBy} maxLength="2" className="inputTable " /></td>
                                                <td >
                                                    <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" onChange={(e) => this.handleCheck(`${data.id}`)} onKeyDown={(e) => this.isCompulsaryKeyDown(`${data.id}`, e)} checked={data.isCompulsary == "N" ? false : true} name={data.udfType} id={data.id} /> <span className="checkmark1"></span> </label>
                                                </td>
                                                <td>
                                                    <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" onChange={(e) => this.handleCheckOrderBy(`${data.id}`)} onKeyDown={(e) => this.isLovDown(`${data.id}`, e)} checked={data.isLovPO == "N" ? false : true} name={data.udfType} /> <span className="checkmark1"></span> </label>
                                                </td>


                                            </tr>))}
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>


                    {/* <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                        <div className="footerDivForm height4per">
                            <ul className="list-inline m-lft-0 m-top-10">
                                <li>{this.state.payloadId.length == 0 ? <button className="textDisable pointerNone clear_button_vendor" >Reset</button>
                                    : <button className="clear_button_vendor" id="reset" type="reset" onKeyDown={(e) => this.onresetKeyDown(e)} onClick={(e) => this.onResetData(e)}>Reset</button>}
                                </li>
                                <li>{this.state.payloadId.length == 0 ? <button type="button" className="btnDisabled save_button_vendor">Save</button>
                                    : <button type="button" onClick={(e) => this.onSubmit(e)} onKeyDown={(e) => this.onKeyDownSave(e)} className="save_button_vendor" id="saveButton">Save</button>}
                                </li>
                            </ul>
                        </div>
                    </div> */}

                </div>

                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}

                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>

        )
    }
}

export default UdfSetting;