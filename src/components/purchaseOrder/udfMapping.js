import React from 'react';
import RequestError from '../loaders/requestError';
import RequestSuccess from '../loaders/requestSuccess';
import FilterLoader from '../loaders/filterLoader';
import ToastLoader from '../loaders/toastLoader';
import PoError from '../loaders/poError';
import addMore from '../../assets/add.svg';
import SearchImage from '../../assets/searchicon.svg';
import searchIcon from '../../assets/clearSearch.svg';
class UdfMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: false,
            filterBar: true,
            udfMappingData: [],
            displayName: "",
            udfType: "",
            poUdfMapping: [],
            loader: false,
            success: false,
            alert: false,
            code: "",
            successMessage: "",
            errorCode: "",
            errorMessage: "",
            toastLoader: false,
            toastMsg: "",
            poErrorMsg: false,
            errorMassage: "",
            codeUdf: "",
            nameUdf: "",
            codeerror: false,
            nameerror: false,
            newRowAdd: false,
            payloadCode: [],
            ext: "N",
            search: "",
            dropdown: false,
            searchBy:"contains"
        };
    }
    componentDidMount(){
        window.setTimeout(function () {
            document.getElementById("dropdownMenu1").focus()
        }, 0)
    }
    componentWillMount() {
 
        if (!this.props.purchaseIndent.getUdfMapping.isSuccess) {
            let data = {

                type: 1,
                search: "",
                isCompulsary: "",
                displayName: "",
                udfType: "",
                ispo: false
            }
            this.props.getUdfMappingRequest(data);
        } else {
            let data = {

                type: 1,
                search: "",
                isCompulsary: "",
                displayName: "",
                udfType: "",
                ispo: false
            }
            this.props.getUdfMappingRequest(data);
        }
        if (this.props.purchaseIndent.getUdfMapping.isSuccess) {
            this.setState({
                udfMappingData: this.props.purchaseIndent.getUdfMapping.data.resource,
            })
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {
            this.setState({
                udfMappingData: nextProps.purchaseIndent.getUdfMapping.data.resource,
                loader: false
            })

        }

        if (nextProps.purchaseIndent.getUdfMapping.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.getUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.getUdfMapping.message.error.errorCode,
                code: nextProps.purchaseIndent.getUdfMapping.message.status,
                alert: true,
                loader: false
            })
            this.props.getUdfMappingClear();
        }

        else if (!nextProps.purchaseIndent.getUdfMapping.isLoading) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.purchaseIndent.poUdfMapping.isSuccess) {
            this.setState({
                poUdfMapping: nextProps.purchaseIndent.poUdfMapping.data.resource,
                loader: false
            })

            this.props.poUdfMappingRequest()
        }
        else if (!nextProps.purchaseIndent.poUdfMapping.isLoading) {
            this.setState({
                loader: false
            })
        } else if (nextProps.purchaseIndent.poUdfMapping.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.poUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.poUdfMapping.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.poUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.poUdfMapping.message.error.errorCode,
                code: nextProps.purchaseIndent.poUdfMapping.message.status,
                alert: true,

                loader: false

            })
            this.props.poUdfMappingRequest();
        }

        if (nextProps.purchaseIndent.updateUdfMapping.isSuccess) {
            this.setState({
                successMessage: nextProps.purchaseIndent.updateUdfMapping.data.message,
                loader: false,
                success: true
            })
            this.props.updateUdfMappingRequest();
        } else if (nextProps.purchaseIndent.updateUdfMapping.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.updateUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.updateUdfMapping.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.updateUdfMapping.message.error == undefined ? undefined : nextProps.purchaseIndent.updateUdfMapping.message.error.errorCode,
                code: nextProps.purchaseIndent.updateUdfMapping.message.status,
                alert: true,
                loader: false
            })
            this.props.updateUdfMappingRequest();
        } else if (!nextProps.purchaseIndent.updateUdfMapping.isLoading) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.purchaseIndent.getUdfMapping.isLoading || nextProps.purchaseIndent.poUdfMapping.isLoading
            || nextProps.purchaseIndent.updateUdfMapping.isLoading) {
            this.setState({
                loader: true
            })
        }


    }
    // ______________________ERROR,TOAST AND MODAL CLOSE CODE______________________ 
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }

    onnChange(name) {
        let udfMappingData = this.state.udfMappingData
        for (var i = 0; i < udfMappingData.length; i++) {
            if (udfMappingData[i].udfType == name) {
                this.setState({
                    displayName: udfMappingData[i].displayName == null ? "" : udfMappingData[i].displayName,
                    poUdfMapping: []
                })
            }
        }
        this.setState({
            udfType: name,
            dropdown: false
        })

    }

    // __________________________________HANDLECHANGE_______________________


    handleChange(code, e) {
        let r = this.state.poUdfMapping
        let codeId = code
        let payloadCode = this.state.payloadCode
        if (!payloadCode.includes(codeId)) {
            payloadCode.push(codeId)
        }
        this.setState({
            payloadCode: payloadCode
        })

        if (e.target.id == "udfType") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].code == code) {
                    r[i].code = e.target.value
                }

            }
        } else if (e.target.id == "value") {
            for (var i = 0; i < r.length; i++) {

                if (r[i].code == code) {
                    r[i].name = e.target.value
                }
            }
        }

        this.setState({
            poUdfMapping: r,
        })
    }

    handleChangeUpdate(e) {
        if (e.target.id == "codeUdfAdd") {
            if (e.target.validity.valid) {

                this.setState({
                    codeUdf: e.target.value
                },
                    () => {
                        this.codeUdf()
                    }
                )
            }
        } else if (e.target.id == "nameUdfAdd") {

            this.setState({
                nameUdf: e.target.value
            },
                () => {
                    this.nameUdf()
                }
            )

        }
    }
    codeUdf() {
        if (this.state.codeUdf == "") {
            this.setState({
                codeerror: true
            })
        } else {
            this.setState({
                codeerror: false
            })
        }
    }
    nameUdf() {
        if (this.state.nameUdf == "") {
            this.setState({
                nameerror: true
            })
        } else {
            this.setState({
                nameerror: false
            })
        }
    }

    // _________________________________API FOR GET ALL DATA__________________________________________

    getPoUdfMapping() {
        var dataUdf = this.state.udfType
        if (dataUdf == "") {
            this.setState({
                toastMsg: "Select Udf",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            var data = {
                udfType: this.state.udfType,
                no: 1,
                type: 1,
                search: "",
                code: "",
                name: "",
                ext: "",
                ispo: "",
                searchBy:this.state.searchBy
            }

            this.props.poUdfMappingRequest(data)

        }


    }

    // ___________________________ADD NEW INPUT___________

    addNew() {
        var dataUdf = this.state.udfType
        if (dataUdf == "") {
            this.setState({
                toastMsg: "Select Udf",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            this.setState({
                newRowAdd: true
            })
        }

    }

    onSaveNewData() {

        this.nameUdf();

        const t = this;

        setTimeout(function () {
            const { nameerror, codeerror } = t.state;
            if (!nameerror) {
                t.setState({
                    loader: true,
                    alert: false,
                    success: false,

                })
                let udfMappingData = []
                let dataUdf = {
                    name: t.state.nameUdf,
                    code: 0,
                    ext: t.state.ext,

                    udfType: t.state.udfType

                }


                udfMappingData.push(dataUdf)

                let payload = {
                    isInsert: true,
                    udfMappingData: udfMappingData
                }
                t.props.updateUdfMappingRequest(payload);
                t.onClear();
            }
        }, 100)

    }

    onClear() {
        this.setState({
            codeUdf: "",
            nameUdf: "",
        })
    }

    handleCheck(code) {
        let r = this.state.poUdfMapping
        let codeId = code
        let payloadCode = this.state.payloadCode
        if (!payloadCode.includes(codeId)) {
            payloadCode.push(codeId)
        }
        this.setState({
            payloadCode: payloadCode
        })

        for (var i = 0; i < r.length; i++) {
            if (r[i].code == code) {
                if (r[i].ext == "N") {
                    r[i].ext = "Y"
                } else {
                    r[i].ext = "N"
                }
            }
        }
        this.setState({
            poUdfMapping: r
        })

    }

    onSubmitt() {

        if (this.state.payloadCode.length == 0) {
            this.setState({
                errorMassage: "No change found",
                poErrorMsg: true
            })
        } else {
            let poUdfMapping = this.state.poUdfMapping
            let payloadCode = this.state.payloadCode
            let dataUdf = []
            poUdfMapping.forEach(udf => {
                if (payloadCode.includes(udf.code.toString())) {
                    let payloadData = {
                        code: udf.code,
                        name: udf.name,
                        udfType: udf.udfType,
                        ext: udf.ext,

                    }
                    dataUdf.push(payloadData)

                }


            })
            let payloadMapping = {
                isInsert: false,
                udfMappingData: dataUdf
            }

            this.props.updateUdfMappingRequest(payloadMapping)
            this.setState({
                payloadCode: []
            })
        }
        this.onClear();
    }
    // _____________________________________onblur validation_______________________________________


    onClose() {
        this.setState({
            newRowAdd: false
        })
    }

    addName(id, e) {
        if (id == "nameUdfAdd") {
            let poUdfMapping = this.state.poUdfMapping
            let orderByData = []
            let flag = false
            if (poUdfMapping != null) {
                poUdfMapping.forEach(s => {
                    let name = s.name

                    orderByData.push(name)

                })
            }
            let unique = [];
            orderByData.forEach(function (item) {
                if (item == e.target.value.toUpperCase()) {

                    flag = true
                }
            })


            if (flag) {
                this.setState({
                    nameUdf: ""
                })


                this.setState({

                    poErrorMsg: true,
                    errorMassage: "Name can't be same"
                })

            }
        } else if (id == "codeUdfAdd") {
            let poUdfMapping = this.state.poUdfMapping
            let orderByData = []
            let flag = false
            if (poUdfMapping != null) {
                poUdfMapping.forEach(s => {
                    let code = s.code

                    orderByData.push(code)

                })
            }
            let unique = [];
            orderByData.forEach(function (item) {
                if (item == Number(e.target.value)) {
                    flag = true
                }
            })


            if (flag) {
                this.setState({
                    codeUdf: ""
                })

                this.setState({

                    poErrorMsg: true,
                    errorMassage: "Code can't be same"
                })

            }
        }

    }
    handleSearch(e) {
        this.setState({
            search: e.target.value
        })

    }

    onSearch(e) {
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                no: 1
            })
            let data = {
                type: 3,
                search: this.state.search,
                no: 1,
                udfType: this.state.udfType,
                code: "",
                name: "",
                ext: "",
                ispo: "",
                searchBy:this.state.searchBy

            }
            this.props.poUdfMappingRequest(data)
        }
    }


    onClearSearch(e) {

        var dataUdf = this.state.udfType
        if (dataUdf == "") {
            this.setState({
                toastMsg: "Select Udf",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            this.setState({
                type: 1,
                no: 1,
                search: ""
            })
            var data = {
                udfType: this.state.udfType,
                no: 1,
                type: 1,
                search: "",
                code: "",
                name: "",
                ext: "",
                ispo: "",
                searchBy:this.state.searchBy
            }
            this.props.poUdfMappingRequest(data)
        }
    }

    // __________________________TAB FUNCTIONALITY_____________

    focusOnButton(e) {
        if (e.key === "Enter") {
            this.addNew();
        }
    }
    searchFocus(e) {
        if (e.key == "Enter") {
            this.getPoUdfMapping();
            document.getElementById("addmore").focus();
            e.preventDefault()
        }
        if (e.key === "Tab") {
            window.setTimeout(() => {

                document.getElementById("addmore").focus();
            }, 0);
        }
    }

    _handleKeyPress(e, id) {
        let idd = id;
        if (e.key == "Enter" && idd == "dropdownMenu1") {
            this.setState({
                udfType: this.state.udfMappingData[0].udfType,
                displayName: this.state.udfMappingData[0].displayName
            })
            window.setTimeout(() => {
                document.getElementById(this.state.udfMappingData[0].udfType).focus()
            })
        }
    }

    focusSave(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id, "map")
        }
    }
    onExtKeyDown(id, e) {
        if (e.key === "Enter") {
            this.handleCheck(id)
        }
        if (e.key === "Tab") {
            if (this.state.poUdfMapping[this.state.poUdfMapping.length - 1].code == id && this.state.payloadCode.length == 0) {
                // window.setTimeout(() => {
                    document.getElementById("dropdownMenu1").focus()
                // }, 0);
                e.preventDefault()
            } else if (this.state.poUdfMapping[this.state.poUdfMapping.length - 1].code == id && this.state.payloadCode.length != 0) {
                window.setTimeout(() => {
                    document.getElementById("saveButton").focus();
                }, 0);
            }
        }
    }
    focusOnCatDesc(e) {
        if (e.key === "Enter") {
            this.onSubmitt()
            window.setTimeout(function () {
                document.getElementById("dropdownMenu1").focus()
            }, 0)
        }
        if (e.key === "Tab") {
            document.getElementById("dropdownMenu1").focus()
            e.preventDefault()
        }
    }
    ArrowDown(id, e) {
        if (e.key === "Enter") {
            this.onnChange(id)
            window.setTimeout(() => {
                document.getElementById("getData").focus()
            })
        }
        if (e.key === "ArrowUp" || e.key === "ArrowDown") {
            let udfMappingData = this.state.udfMappingData
            var index = ""
            var nextId = ""
            for (let i = 0; i < udfMappingData.length; i++) {
                if (id == udfMappingData[i].udfType) {
                    index = i

                }
                if (index > 0 && e.key === "ArrowUp") {
                    nextId = udfMappingData[index - 1].udfType

                }
                if (index != udfMappingData[i].length && e.key === "ArrowDown" && id != this.state.udfMappingData[this.state.udfMappingData.length - 1].udfType) {
                    nextId = udfMappingData[index + 1].udfType
                }
            }
            if (nextId != "") {
                document.getElementById(nextId).focus()
            }
        }
    }
    searchKeyDown(e) {
        if (e.key === "Enter") {
            this.onSearch(e)
        }
        if (e.key === "Tab" && this.state.poUdfMapping.length == 0) {
            document.getElementById("dropdownMenu1").focus()
            e.preventDefault()
        }
        this.handleSearch(e)
    }
    openDrop(e) {
        this.setState({
            dropdown: true
        })
    }
    _handleKeyPressSearch = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    handleChangeSearch(e){
        if(e.target.id=="searchBymap"){
            this.setState({
                searchBy:e.target.value
            },()=>{
                if(this.state.search!=""){
                      var data = {
                udfType: this.state.udfType,
                no: 1,
                type: this.state.type,
                search: this.state.search,
                code: "",
                name: "",
                ext: "",
                ispo: "",
                searchBy:this.state.searchBy
            }

            this.props.poUdfMappingRequest(data)
                }

            })

        }

    }

    render() {
        const { codeUdf, nameUdf, nameerror, codeerror } = this.state
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <input type="search" onKeyPress={this._handleKeyPressSearch} onChange={(e) => this.handleSearch(e)} id="search" value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                    <img className="search-image" onKeyDown={(e) => this.searchKeyDown(e)} src={SearchImage} />
                                    {this.state.type == 3 ? <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></span> : null}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBymap" name="searchBymap" value={this.state.searchBy} onChange={(e) => this.handleChangeSearch(e)}>
                                    <option value="contains">Contains</option>
                                    <option value="startWith"> Start with</option>
                                    <option value="endWith">End with</option>
                                </select> : null}
                                {this.state.payloadCode.length != 0 ? <button type="button" onClick={(e) => this.onSubmitt(e)} className="gen-save" onKeyDown={(e) => this.focusOnCatDesc(e)} id="saveButton">Save</button>
                                    : <button type="button" className="btnDisabled" >Save</button>}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="container-fluid p-lr-47">
                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0 udfMappingMain">
                        <div className="container_content heightAuto pipo-con">
                            <div className="col-md-12 pad-0 m-top-20">
                                <div className="col-md-2 pad-lft-0">
                                    <div className="settingDrop dropdown displayInline pad-0 width100 udfDropDown setUdfMappingMain">
                                        <button className="btn btn-default dropdown-toggle userModalSelect onFocus" type="button" id="dropdownMenu1" onClick={(e) => this.openDrop(e)} onKeyDown={(e) => this._handleKeyPress(e, "dropdownMenu1")} data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            {this.state.udfType == "" ? " Select Udf" : this.state.udfType}
                                            <i className="fa fa-chevron-down"></i>
                                        </button>

                                        {this.state.dropdown ? <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            {this.state.udfMappingData.length != 0 ? this.state.udfMappingData.map((data, j) => <li value={data.udfType} tabindex="-1" id={data.udfType} key={j} onKeyDown={(e) => this.ArrowDown(`${data.udfType}`, e)} onClick={(e) => this.onnChange(`${data.udfType}`)} >
                                                <a>{data.udfType}</a>
                                            </li>) : "No Data Found"}
                                        </ul> : null}

                                    </div>
                                </div>
                                <div className="col-md-2 pad-lft-0">
                                    {/* <input type="text" className=" orgnisationTextbox onFocus" value={this.state.displayName} placeholder="Udf Name" /> */}
                                    <label className=" orgnisationTextbox onFocus labelGlobal displayFlex alignMiddle fontWeightNormal pad-lft-7" >{this.state.displayName == "" ? "Udf Name" : this.state.displayName}</label>
                                </div>
                                <div className="col-md-1 pad-0 searchBtn">
                                    <button className="search_button_vendor" type="button" id="getData" onKeyDown={(e) => this.searchFocus(e)} onClick={() => this.getPoUdfMapping()}>Search</button>
                                </div>
                                <div className="col-md-2 searchBtn">
                                    {/* */}
                                    <button onClick={() => this.addNew()} onKeyDown={(e) => this.focusOnButton(e)} id="addmore" className="pad-lft-15 displayPointer addmore" >
                                        <img className="displayPointer" src={require('../../assets/add-green.svg')} /> 
                                        <span className="generic-tooltip">Add New</span>
                                    </button>
                                </div>
                            </div>
                            {this.state.newRowAdd ? <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-40 lineBefore">
                                <div className="displayLine pad-0" >

                                    <div className="col-md-2 pad-lft-0"><input type="text" placeholder="Enter Value" value={nameUdf} id="nameUdfAdd" className={nameerror ? "errorBorder orgnisationTextbox" : "orgnisationTextbox"} onChange={(e) => this.handleChangeUpdate(e)} onBlur={(e) => this.addName("nameUdfAdd", e)} /></div>
                                    {this.state.nameerror ? <span className="error">Enter value</span> : null}
                                </div>
                                <div className="col-md-12 pad-0 m-top-20">
                                    <button type="button" className="save2_button_vendor m-rgt-10" onClick={() => this.onSaveNewData()}>Save</button>
                                    <button type="button" className="clear_button_vendor m-rgt-10" onClick={() => this.onClear()}>Clear</button>
                                    <button type="button" className="cancel_button_vendor" onClick={() => this.onClose()}>Cancel</button>
                                </div>
                            </div> : null}
                            <div className="col-md-12">
                                <div className="col-md-6"></div>
                                <div className="col-md-6 col-sm-12 pad-0">

                                    <ul className="list-inline search_list manageSearch chooseDataModal">
                                        <li>
                                            {/* <form onSubmit={(e) => this.onSearch(e)}> */}
                                                    
                                                
                                            {/* </form> */}
                                        </li>

                                    </ul>

                                    
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric siteMapppingMain udfMappingTable">
                                <div className="tableHeadFix pipo-con-table">
                                    <div className="scrollableTableFixed table-scroll itemUdfSet scrollableOrgansation tableHeadFixHeight" id="table-scroll">
                                        <table className="table zui-table sitemappingTable itemUdfTable gen-main-table">
                                            <thead>
                                                <tr>

                                                    <th><label>UDF Type</label></th>
                                                    <th><label>Value</label></th>
                                                    <th><label>Ext</label></th>

                                                </tr>
                                            </thead>

                                            <tbody>
                                                {this.state.poUdfMapping == null || this.state.poUdfMapping == "" || this.state.poUdfMapping.length == 0 ? <tr className="tableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.poUdfMapping.map((data, idx) => (
                                                    <tr id={idx} key={idx}>

                                                        <td>
                                                            <label>
                                                                {data.udfType == null ? "" : data.udfType}
                                                            </label>

                                                        </td>

                                                        <td>
                                                            <label>
                                                                {data.value == null ? "" : data.value}
                                                            </label>

                                                        </td>
                                                        <td >
                                                            <ul className="list-inline">
                                                                <li className="text-center width70">
                                                                    <label className="checkBoxLabel0 displayPointer checkBoxFocus"><input type="checkBox" className="checkBoxTab" onChange={(e) => this.handleCheck(`${data.code}`)} onKeyDown={(e) => this.onExtKeyDown(`${data.code}`, e)} checked={data.ext == "N" ? false : true} name={data.code} id={data.code} /> <span className="checkmark1"></span> </label>
                                                                </li>
                                                            </ul>

                                                        </td>

                                                    </tr>))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="col-md-12 col-sm-12 pad-0 m-top-20">
                                <div className="footerDivForm height4per">
                                    <ul className="list-inline m-lft-0 m-top-10">

                                        <li>{this.state.payloadCode.length != 0 ? <button type="button" onClick={(e) => this.onSubmitt(e)} className="save_button_vendor" onKeyDown={(e) => this.focusOnCatDesc(e)} id="saveButton">Save</button>
                                            : <button type="button" className="btnDisabled" >Save</button>}
                                        </li>
                                    </ul>
                                </div>
                            </div> */}

                        </div>
                    </div>
                </div>

                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

            </div>
        )
    }
}
export default UdfMapping;