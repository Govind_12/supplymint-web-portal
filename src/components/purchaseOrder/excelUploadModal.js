import React from 'react';

class ExcelUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileData: [],
            type: '',
            mappingHeader:["HLCode" , "HLName", "HLType", "Mapping Type", "CCode", "CName"],
            preDefinedSetHeader:["HLCode" , "HLName", "HLType", "Mapping Type", "CCode", "CName", "Ratios"]
        }
    }

    typeHandler = (e) =>{
        this.setState({
            type: e.target.id
        })
    }

    render(){
        console.log('fi', this.props.fileData)
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content bulk-upload-modal excel-upload-modal">
                    <div className="bum-head">
                        <h3>Upload a file</h3>
                        <p>You can upload only in .xls format and file size could not be more than 10mb</p>
                        <button type="button" className="bum-close-btn" onClick={this.props.CancelExcelUpload}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="bum-body eum-body">
                        <div className="bum-file-name">
                            <p>{this.props.fileData.name}</p>
                            {this.props.fileData.length != 0 && <span onClick={this.props.clearFileData}>Remove</span>}
                        </div>
                        <label className="file upload-file">
                            <input type="file" id="fileUpload" multiple="multiple" onChange={this.props.fileUpload} accept=".xlsx, .xls, .csv"/>
                            <span >Choose file</span>
                            {/* <img src={require('../../../assets/fileupload')} /> */}
                        </label>
                    </div>
                    <div className="gen-pi-formate">
                        <ul className="gpf-radio-list">
                            <li>
                                <label className="gen-radio-btn">
                                    <input type="radio" name="searchformate" id="mapping" onChange={this.typeHandler}/>
                                    <span className="checkmark"></span>
                                    Mapping
                                </label>
                            </li>
                            <li>
                                <label className="gen-radio-btn">
                                    <input type="radio" name="searchformate" id="preDefinedSet" onChange={this.typeHandler}/>
                                    <span className="checkmark"></span>
                                    PreDefinedSet
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div className="eumb-bottom">
                        <h3>Headers Required</h3>
                        <div className="eumbb-header">
                            {this.state.type == "preDefinedSet" ?
                            this.state.preDefinedSetHeader.map(header => <span>{header}</span>)
                            :this.state.mappingHeader.map(header => <span>{header}</span>)}                            
                        </div>
                    </div>
                    
                    <div className="bum-footer eum-footer">
                        <button type="button" className="bumf-down-temp" onClick={() => this.props.downloadTemplate(this.state.type)}>Download Template</button>
                        <button type="button" className="bumf-upload" onClick={() => this.props.uploadExcel(this.state.type)}>Upload
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.486" height="6.532" viewBox="0 0 13.486 6.532">
                                    <g id="prefix__arrow_1_" data-name="arrow (1)" transform="translate(0 -132)">
                                        <g id="prefix__Group_2565" data-name="Group 2565" transform="translate(0 132)">
                                            <path id="prefix__Path_645" fill="#fff" d="M13.332 134.893l-2.753-2.739a.527.527 0 0 0-.743.747l1.848 1.839H.527a.527.527 0 1 0 0 1.054h11.156l-1.848 1.839a.527.527 0 0 0 .743.747l2.753-2.739a.527.527 0 0 0 .001-.748z" data-name="Path 645" transform="translate(0 -132)"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
export default ExcelUpload;