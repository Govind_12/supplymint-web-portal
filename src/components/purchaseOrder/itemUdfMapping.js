import React from 'react';
import CatDescMap from './catDescMap';
import UdfMap from "./udfMap";

import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

class ItemUdfMapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            codeRadioCat: "catDesc",
            codeRadioUdf: "udf",
            activeCat: true,
            activeUdf: false,
            udfData: [],
            catDescData: [],
            udf: "",
            catDesc: "",
            divisionState: [],
            loader: false,
            success: false,
            alert: false,
            code: "",
            successMessage: "",
            errorCode: "",
            errorMessage: "",
            division: "",
            section: "",
            department: "",
            divisionUdf: "",
            sectionUdf: "",
            departmentUdf: "",
            itemCatDesc: [],
            itemUdfMapping: [],
            showArticleUdf: false,
            articleCheckUdf: false,
            showArticleCat: false,
            articleCheckCat: false,
            descriptionCat: "",
            codeCat:"",
            descriptionUdf: "",
            codeUdf:"",
            catDescDept: "",
            showSecondCat: false,
            articleName: "",
            articleCode: "",
            udfDept: "",
            catDescType: 1,
            searchCatDesc: "",
            searchUdf: "",
            udfType: 1,
            articleNameUdf: "",
            articleCodeUdf: "",
            hl3Code: "",
            catDescModification: "false",
            editDisplayName:"false",
          

        };
    }
    chooseDataCat(data) {
        this.setState({
            division: data.hl1Name,
            section: data.hl2Name,
            department: data.hl3Name,
            hl3Code: data.hl3Code
        })
    }
    updatePiType(data) {
        this.setState({
            catDescType: data
        })
    }
    searchUdf(data) {
       
        this.setState({
            searchUdf: data
        })

    }
    searchCatDescFun(data) {
     
        this.setState({
            searchCatDesc: data
        })

    }
    udfTypee(d) {
        this.setState({
            udfType: d
        })
    }
    chooseDataUdf(data) {
        this.setState({

            divisionUdf: data.hl1Name,
            sectionUdf: data.hl2Name,
            departmentUdf: data.hl3Name,

        })
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }

    updateUdf(data) {
        this.setState({
            udf: data.udf,
        })
    }

    updateCat(dataa) {
        this.setState({
            catDesc: dataa.catDesc
        })
    }

    updateCatDept(dataa) {
        this.setState({
            catDescDept: dataa.catDesc,

        })
    }

    updateUdfDept(payload) {
        this.setState({
            udfDept: payload.udfDept,

        })
    }
    componentWillMount() {
        if (!this.props.purchaseIndent.activeUdf.isSuccess) {
            this.props.activeUdfRequest();
        } else {
            this.props.activeUdfRequest();
        }

        if (!this.props.purchaseIndent.activeCatDesc.isSuccess) {
            this.props.activeCatDescRequest();

        } else {
            this.props.activeCatDescRequest();
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.activeUdf.isSuccess) {
            this.setState({
                udfData: nextProps.purchaseIndent.activeUdf.data.resource!=null? nextProps.purchaseIndent.activeUdf.data.resource:[]
            })
        } else if (!nextProps.purchaseIndent.activeUdf.isLoading) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.purchaseIndent.activeCatDesc.isSuccess) {
            this.setState({
                catDescData: nextProps.purchaseIndent.activeCatDesc.data.resource!=null ?nextProps.purchaseIndent.activeCatDesc.data.resource:[]
            })
        } else if (!nextProps.purchaseIndent.activeCatDesc.isLoading) {
            this.setState({
                loader: false
            })
        }
        if (nextProps.purchaseIndent.itemCatDescUdf.isSuccess) {
            this.setState({
                loader: false,
                itemCatDesc: nextProps.purchaseIndent.itemCatDescUdf.data.resource
            })
        } else if (nextProps.purchaseIndent.itemCatDescUdf.isLoading) {
            this.setState({
                loader: true
            })
        }

        if (nextProps.purchaseIndent.itemUdfMapping.isSuccess) {
            this.setState({
                loader: false,
                itemUdfMapping: nextProps.purchaseIndent.itemUdfMapping.data.resource == null ? [] : nextProps.purchaseIndent.itemUdfMapping.data.resource
            })
        }

        if (nextProps.purchaseIndent.divisionData.isSuccess) {
            let c = []
            if (nextProps.purchaseIndent.divisionData.data.resource != null) {
                for (let i = 0; i < nextProps.purchaseIndent.divisionData.data.resource.length; i++) {

                    let x = nextProps.purchaseIndent.divisionData.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }
                this.setState({
                    divisionState: c,
                      editDisplayName:nextProps.purchaseIndent.divisionData.data.editDisplayName,
                })
            }
            this.setState({
                catDescModification: nextProps.purchaseIndent.divisionData.data.catDescModification,
                editDisplayName:nextProps.purchaseIndent.divisionData.data.editDisplayName,
                loader: false
            })
            this.props.divisionSectionDepartmentRequest();


        } else if (nextProps.purchaseIndent.divisionData.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.divisionData.message.error == undefined ? undefined : nextProps.purchaseIndent.divisionData.message.error.errorCode,
                code: nextProps.purchaseIndent.divisionData.message.status,
                alert: true,

                loader: false

            })
            this.props.divisionSectionDepartmentRequest();
        } else if (!nextProps.purchaseIndent.divisionData.isLoading) {
            this.setState({
                loader: false
            })
        }

        if (nextProps.purchaseIndent.updateItemUdf.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.updateItemUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.updateItemUdf.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.updateItemUdf.message.error == undefined ? undefined : nextProps.purchaseIndent.updateItemUdf.message.error.errorCode,
                code: nextProps.purchaseIndent.updateItemUdf.message.status,
                alert: true,
                loader: false

            })
            this.props.updateItemUdfRequest();
        }

        else if (nextProps.purchaseIndent.updateItemUdf.isSuccess) {
            this.setState({
                success: true,
                loader: false,
                successMessage: nextProps.purchaseIndent.updateItemUdf.data.message,
            })
            this.props.updateItemUdfRequest();
        } else if (!nextProps.purchaseIndent.updateItemUdf.isLoading) {
            this.setState({
                loader: false
            })
        }


        if (nextProps.purchaseIndent.updateItemCatDesc.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.updateItemCatDesc.message.error == undefined ? undefined : nextProps.purchaseIndent.updateItemCatDesc.message.error.errorMessage,

                errorCode: nextProps.purchaseIndent.updateItemCatDesc.message.error == undefined ? undefined : nextProps.purchaseIndent.updateItemCatDesc.message.error.errorCode,
                code: nextProps.purchaseIndent.updateItemCatDesc.message.status,
                alert: true,

                loader: false

            })
            this.props.updateItemCatDescRequest();
        }
        else if (nextProps.purchaseIndent.updateItemCatDesc.isSuccess) {
            this.setState({
                success: true,
                loader: false,
                successMessage: nextProps.purchaseIndent.updateItemCatDesc.data.message,
            })
            this.props.updateItemCatDescRequest();
        } else
            if (!nextProps.purchaseIndent.updateItemCatDesc.isLoading) {
                this.setState({
                    loader: false
                })
            }
        if (nextProps.purchaseIndent.divisionData.isLoading || nextProps.purchaseIndent.itemUdfMapping.isLoading
            || nextProps.purchaseIndent.itemCatDescUdf.isLoading || nextProps.purchaseIndent.activeCatDesc.isLoading
            || nextProps.purchaseIndent.activeUdf.isLoading || nextProps.purchaseIndent.updateItemUdf.isLoading
            || nextProps.purchaseIndent.updateItemCatDesc.isLoading) {
            this.setState(
                {
                    loader: true
                }
            )
        }
    }
    onActiveCat() {
        this.setState({
            editDisplayName:this.state.editDisplayName,
            activeCat: true,
            activeUdf: false
        })

    }
    onActiveUdf() {
        this.setState({
            editDisplayName:this.state.editDisplayName,
            activeCat: false,
            activeUdf: true
        })

    }
    updateCatDescRadio(value) {
        this.setState({
            codeRadioCat: value
        })
    }

    updateUdfRadio(value) {
        this.setState({
            codeRadioUdf: value
        })
    }
    checkUdfUpdate(data) {

        this.setState({
            showArticleUdf: data.showArticle,
            articleCheckUdf: data.articleCheck
        })
        if(data.showArticle == false || data.articleCheck==false){
             this.setState({
            articleNameUdf: "",
            articleCodeUdf: ""
        })
        }
    }
    checkCatUpdate(data) {
        this.setState({
            showArticleCat: data.showArticle,
            articleCheckCat: data.articleCheck
        })
        if(data.showArticle==false || data.articleCheck== false){
             this.setState({
            articleName: "",
            articleCode: ""
        })
        }
    }
    descriptionValueCat(data) {
        this.setState({
            descriptionCat: data.cname,
            codeCat:data.code

        })
    }
    descriptionValueUdf(data) {
        this.setState({
            descriptionUdf: data.cname,
            codeUdf:data.code
        })
    }
    updateArticle(adata) {
        this.setState({
            articleName: adata.articleName,
            articleCode: adata.articleCode
        })
    }
    updateArticleUdf(dataA) {

        this.setState({
            articleNameUdf: dataA.articleName,
            articleCodeUdf: dataA.articleCode
        })
    }
    showSecondCatUpdate(data) {
        this.setState({
            showSecondCat: data.showSecondCat,
            descriptionCat: data.description,
            catDesc: ""
        })
    }
    render() {
        
        return (
            <div className="container-fluid pad-0 pad-l50">
                {/* <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <input type="search" id="search" onKeyPress={this._handleKeyPressSearch} onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar width-60per" />
                                    <button className={this.state.itemCatDesc == null || this.state.itemCatDesc.length == 0 ? "btnDisabled searchWithBar" : "searchWithBar btnFocuss"} onKeyDown={(e) => this.searchKeyDown(e)} onClick={(e) => this.onSearch(e)}> Search
                                            <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                            <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                            </path></svg>
                                    </button>
                                    {this.state.type == 3 ? <span className="closeSearch" id="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">

                            </div>
                        </div>
                    </div>
                </div> */}
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 udfMappingMain setUdfMappingMain itemUdfSet udfMappingHead">
                    <div className="container_content heightAuto pipo-con">
                        {/* <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                            <div className="col-md-6 col-sm-12 pad-0">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            ITEM UDF MAPPING
                                         </label>
                                    </li>
                                    <li>
                                    </li>
                                </ul>
                            </div> 
                        </div> */}
                        <div className="changeSettingContain">
                            <div className="col-md-12 col-sm-12 switchTabs pad-0 p-lr-47 m-top-15">
                                <ul className="list_style ">
                                    <li className={this.state.activeCat ? "m-rgt-25 activeTab displayPointer" : "m-rgt-25 displayPointer"} onClick={(e) => this.onActiveCat(e)}>
                                        <label className="displayPointer">CAT/DESC Mapping</label>
                                    </li>
                                    <li className={this.state.activeUdf ? "displayPointer activeTab" : "displayPointer"} onClick={(e) => this.onActiveUdf(e)}>
                                        <label className="displayPointer">UDF Mapping</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {this.state.activeCat ? <CatDescMap {...this.props}{...this.state} catDescModification={this.state.catDescModification} searchCatDesc={this.state.searchCatDesc} searchCatDescFun={(e) => this.searchCatDescFun(e)} updatePiType={(e) => this.updatePiType(e)} updateArticle={(e) => this.updateArticle(e)} showSecondCatUpdate={(e) => this.showSecondCatUpdate(e)} updateCatDept={(e) => this.updateCatDept(e)} descriptionValueCat={(e) => this.descriptionValueCat(e)} checkCatUpdate={(e) => this.checkCatUpdate(e)} updateCatDescRadio={(e) => this.updateCatDescRadio(e)} chooseDataCat={(e) => this.chooseDataCat(e)} divisionState={this.state.divisionState} catDesc={this.state.catDesc} catDescData={this.state.catDescData} itemCatDesc={this.state.itemCatDesc} updateCat={(e) => this.updateCat(e)} /> : null}
                        {this.state.activeUdf ? <UdfMap  {...this.props}{...this.state} searchUdfFun={(e) => this.searchUdf(e)} articleNameUdf={this.state.articleNameUdf} articleCodeUdf={this.state.articleCodeUdf} searchUdf={this.state.searchUdf} udfTypee={(e) => this.udfTypee(e)} udfType={this.state.udfType} updateUdfDept={(e) => this.updateUdfDept(e)} updateArticleUdf={(e) => this.updateArticleUdf(e)} descriptionValueUdf={(e) => this.descriptionValueUdf(e)} checkUdfUpdate={(e) => this.checkUdfUpdate(e)} updateUdfRadio={(e) => this.updateUdfRadio(e)} chooseDataUdf={(e) => this.chooseDataUdf(e)} divisionState={this.state.divisionState} udf={this.state.udf} udfData={this.state.udfData} itemUdfMapping={this.state.itemUdfMapping} updateUdf={(e) => this.updateUdf(e)} /> : null}
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default ItemUdfMapping;