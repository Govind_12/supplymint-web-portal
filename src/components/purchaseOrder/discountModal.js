import React from "react";
import ToastLoader from "../loaders/toastLoader";

class DiscountModal extends React.Component {
    constructor(props) {

        super(props);
        this.textInput = React.createRef();

        this.state = {
            discountType: "percentage",
            discountData: [],
            selectedDiscount: this.props.selectedDiscount,
            discount: "",
            discountTypeerr: "",
            discountValue: "",
            discountValueerr: "",
            focusedLi: "",
            search: "",
            percent: true,
            modalDisplay: false,
            toastMsg: "",
            toastLoader: false



        }
    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
        if (this.props.disId != "" && this.props.disId != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let windowWidth = window.innerWidth;
            let position = document.getElementById(this.props.disId).getBoundingClientRect();
            let leftPosition = position.left;
            let left = 0;
            let diff = windowWidth - leftPosition;

            if (diff >= modalWidth) {
                left = leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                // let removeleft = diff >= 260 ? diff - 2 : diff;
                // left = leftPosition - removeleft;

                left = (leftPosition - removeWidth) - 20;
            }

            let idNo = parseInt(this.props.disId.replace(/\D/g, '')) + 1;
            // let top = 155 + (35 * idNo);
            let top = this.props.parent == "PI" ? 51 + (35 * idNo) : 155 + (35 * idNo)

            let newLeft = left > 40 ? left - 40 : left;

            console.log('leftPosition: ' + leftPosition)
            console.log('diff: ' + diff)
            console.log('left: ' + left)
            console.log('newLeft: ' + newLeft)



            $('#discountModalPosition').css({ 'left': newLeft, 'top': top });
        }

    }

    componentWillMount() {
        console.log(this.props.isDiscountMap, this.props.isModalShow, this.props.discountSearch)
        this.setState({
            selectedDiscount: this.props.selectedDiscount,
            search: this.props.isDiscountMap ? this.props.isModalShow ? "" : this.props.discountSearch : "",
            discount: !this.props.isDiscountMap ? this.props.isModalShow ? "" : this.props.discountSearch : ""




        }, () => {
            console.log(this.state.discount)
            if (!this.props.isDiscountMap) {
                this.checkper()
            }

        })
        if (!this.props.isModalShow) {
            this.setState({
                modalDisplay: true
            })
        }
    }
    handleChange(data) {
        if (this.state.discountType != data) {
            this.setState({
                discountType: data
            })
            if (this.props.isDiscountMap) {
                let payload = {
                    articleCode: this.props.vendorMrpPo,
                    mrp: this.props.discountMrp,
                    discountType: data
                }
                this.props.discountRequest(payload)
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        if (!this.props.isDiscountMap) {
            this.setState({
                selectedDiscount: this.props.selectedDiscount,
                search: this.props.isDiscountMap ? this.props.isModalShow ? "" : this.props.discountSearch : "",
                discount: !this.props.isDiscountMap ? this.props.isModalShow ? "" : this.props.discountSearch : ""




            }, () => {
                console.log(this.state.discount)
                if (!this.props.isDiscountMap) {
                    this.checkper()
                }

            })
        }

        if (nextProps.purchaseIndent.discount.isSuccess) {
            if (nextProps.purchaseIndent.discount.data.resource != null) {
                this.setState({
                    discountData: nextProps.purchaseIndent.discount.data.resource
                })
            } else {
                this.setState({
                    discountData: []
                })
            }
            if (window.screen.width > 1200) {
                if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.supplier.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.discount.data.resource[0].discount,
                            search: this.props.isDiscountMap ? this.props.isModalShow ? "" : this.props.discountSearch : "",
                            discount: !this.props.isDiscountMap ? this.props.isModalShow ? "" : this.props.discountSearch : ""

                        }, () => {
                            console.log(this.state.discount)
                            if (!this.props.isDiscountMap) {
                                this.checkper()
                            }

                        })
                        document.getElementById(nextProps.purchaseIndent.discount.data.resource[0].discount) != null ? document.getElementById(nextProps.purchaseIndent.discount.data.resource[0].discount).focus() : null
                    }



                }
            }
            this.setState({
                modalDisplay: true
            })
            document.getElementById("discountType") != null ? document.getElementById("discountType").focus() : null

        }

    }
    discountType() {
        if (this.state.discount != "") {
            this.setState({
                discountTypeerr: false
            })
        } else {
            this.setState({
                discountTypeerr: true
            })
        }

    }
    discountValue() {
        if (this.state.discountValue != "") {
            this.setState({
                discountValueerr: false
            })
        } else {
            this.setState({
                discountValueerr: true
            })
        }
    }
    selectedData(data) {


        this.setState({
            selectedDiscount: data
        }, () => {
            if (!this.props.isModalShow) {
                this.onDone()
            }
        })



    }
    inputChange(e) {
        if (e.target.id == "discountType") {
            this.setState({
                discount: e.target.value
            },
                () => {
                    this.discountType();


                }
            )

            var numberPattern = /\d+/g;
            var Numvalue = e.target.value.match(numberPattern)
            this.setState({
                discountValue: Numvalue != null ? Numvalue[0] : ""

            }, () => {
                this.checkper()
            })
        } else if (e.target.id == "discountValue") {
            this.setState({
                discountValue: e.target.value
            },
                () => {
                    this.discountValue();
                }
            )
        }

    }
    checkper() {
        if (this.state.percent) {
            if (this.state.discount != "" && this.state.discount != undefined) {
                if (this.state.discount.match(/^[a-zA-Z0-9% ]+$/)) {
                    if (!this.state.discount.match("%")) {

                        var numberPattern = /\d+/g;
                        var Numvalue = this.state.discount.match(numberPattern)
                        this.setState({
                            discount: this.state.discount + "%",
                            discountValue: Numvalue

                        })
                    }
                } else {
                    this.setState({
                        toastMsg: "Invalid Discount",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1500)
                }
            }
        }
    }

    onDone() {
        if (this.props.isDiscountMap) {
            let discountData = this.state.discountData
            if (this.props.isModalShow ? this.state.selectedDiscount != this.props.selectedDiscount : this.state.selectedDiscount != this.props.selectedDiscount
                || this.state.search == this.props.discountSearch || this.state.search != this.props.discountSearch) {
                for (let i = 0; i < discountData.length; i++) {
                    if (discountData[i].discount == this.state.selectedDiscount) {
                        let data = {
                            discount: {
                                discountType: discountData[i].discount,
                                discountValue: discountData[i].discountValue
                            },
                            discountGrid: this.props.discountGrid
                        }
                        this.props.updateDiscountModal(data)
                        this.props.closeDiscountModal()
                    }
                }
            } else {
                this.props.closeDiscountModal()
            }
        } else {
            this.discountType();
            this.discountValue();
            setTimeout(() => {

                const { discountTypeerr, discountValueerr } = this.state
                if (!discountTypeerr && !discountValueerr) {
                    let data = {
                        discount: {
                            discountType: this.state.discount,
                            discountValue: this.state.discountValue
                        },
                        discountGrid: this.props.discountGrid
                    }
                    this.props.updateDiscountModal(data)
                    this.props.closeDiscountModal()
                }
            }, 10)


        }
    }


    selectLi(e, code) {
        let discountData = this.state.discountData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < discountData.length; i++) {
                if (discountData[i].discount == code) {
                    index = i
                }
            }
            if (index < discountData.length - 1 || index == 0) {
                document.getElementById(discountData[index + 1].discount) != null ? document.getElementById(discountData[index + 1].discount).focus() : null

                this.setState({
                    focusedLi: discountData[index + 1].discount
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < discountData.length; i++) {
                if (discountData[i].discount == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(discountData[index - 1].discount) != null ? document.getElementById(discountData[index - 1].discount).focus() : null

                this.setState({
                    focusedLi: discountData[index - 1].discount
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }

        if (e.key == "Escape") {
            this.props.closeDiscountModal(e)
        }


    }
    changeType() {
        if (this.state.percent) {
            this.setState({

                percent: false
            }, () => {
                this.checkper()
            })
        } else {
            this.setState({
                percent: true
            }, () => {
                this.checkper()
            })
        }
    }

    selectLi(e, code) {
        let discountData = this.state.discountData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < discountData.length; i++) {
                if (discountData[i].discount == code) {
                    index = i
                }
            }
            if (index < discountData.length - 1 || index == 0) {
                document.getElementById(discountData[index + 1].discount) != null ? document.getElementById(discountData[index + 1].discount).focus() : null

                this.setState({
                    focusedLi: discountData[index + 1].discount
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < discountData.length; i++) {
                if (discountData[i].discount == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(discountData[index - 1]).discount != null ? document.getElementById(discountData[index - 1].discount).focus() : null

                this.setState({
                    focusedLi: discountData[index - 1].discount
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next") != null ? document.getElementById("next").focus() : null }

        }
        if (e.key == "Escape") {
            this.props.closeCity(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next") != null ? document.getElementById("next").focus() : null :
                        this.state.discountData.length != 0 ?
                            document.getElementById(this.state.discountData[0]).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.discountData.length != 0) {
                    document.getElementById(this.state.discountData[0]).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.props.closeCity(e)
        }


    }
    handleKeyDown(btn, e) {
        if (e.key == "Enter") {
            document.getElementById(e.target.id).click()
        }
        if (e.key == "Tab") {
            if (btn == "done") {
                document.getElementById("closeButton").focus()
            }
            if (btn == "close") {
                document.getElementById("discountType").focus()


            }
        }

    }
    handleInput(e) {
        if (e.key == "Tab") {
            document.getElementById("donsButton").focus()
        }
    }

    render() {
        console.log(this.props.disId, this.props.isModalShow)
        if (this.props.disId != "" && this.props.disId != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let windowWidth = window.innerWidth;
            let position = document.getElementById(this.props.disId).getBoundingClientRect();
            let leftPosition = position.left;
            let left = 0;
            let diff = windowWidth - leftPosition;

            if (diff >= modalWidth) {
                left = leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                // let removeleft = diff >= 260 ? diff - 2 : diff;
                // left = leftPosition - removeleft;

                left = (leftPosition - removeWidth) - 20;
            }

            let idNo = parseInt(this.props.disId.replace(/\D/g, '')) + 1;
            // let top = 155 + (35 * idNo);
            console.log("this.props.parent")
            let top = this.props.parent == "PI" ? 51 + (35 * idNo) : 155 + (35 * idNo)

            let newLeft = left > 40 ? left - 40 : left;

            console.log('leftPosition: ' + leftPosition)
            console.log('diff: ' + diff)
            console.log('left: ' + left)
            console.log('newLeft: ' + newLeft)



            $('#discountModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');

        }

        const { discountData, search, discountTypeerr, discountValueerr } = this.state
        if (this.props.isDiscountMap) {
            if (this.state.focusedLi != "" || this.state.search == this.props.discountSearch) {
                document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
            }
        }
        if (this.props.isDiscountMap) {
            var result = _.filter(discountData, function (data) {
                return _.startsWith(data.toLowerCase(), search.toLowerCase());
            });

        }

        return (
            this.props.isModalShow ? <div className="modal display_block" id="pocolorModel">
                <div className="backdrop display_block"></div>
                <div className="modal_Indent display_block">
                    <div className="col-md-12 col-sm-12 previousAddortmentModal discountModal modalpoColor modal-content modalShow">
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle pad-0">
                                    <div className="col-md-6 pad-0">
                                        <ul className="list_style width_100 m-top-20">
                                            <li>
                                                <label className="select_name-content">Discount</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                {this.props.isDiscountMap ? <div className="col-md-10 pad-0 procurementSetting">
                                    <div className="card-content newRadioBtns width100 backgroundNone">
                                        <ul className="pad-0">
                                            <li className="m-rgt-30">
                                                <label className="select_modalRadio">
                                                    <input type="radio" name="frequency" id="percentage" value={this.state.discountType} checked={this.state.discountType == "percentage"} onClick={(e) => this.handleChange("percentage")} /><span className="text">Percentage</span>
                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                </label>
                                            </li>
                                            <li className="widthAuto float_initial">
                                                <label className="select_modalRadio">
                                                    <input type="radio" name="frequency" id="amount" value={this.state.discountType} checked={this.state.discountType == "amount"} onClick={(e) => this.handleChange("amount")} /><span className="text">Amount</span>
                                                    <span className="checkradio-select select_all positionCheckbox"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onChange={e => this.handleChange(e)} value={searchMrp} id="discountSearch" placeholder="Type to search" />

                                            </li>
                                        </ul>
                                    </div>
                                </div> : null}
                                {this.props.isDiscountMap ? <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover vendorMrpMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Discount Type</th>
                                                    <th>Discount Value</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {result.length != 0 ? result.map((data, key) => (

                                                    <tr key={key}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.discount} checked={this.state.selectedDiscount == `${data.discount}`} onClick={(e) => this.selectedData(`${data.discount}`)} />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.discount}</td>
                                                        <td>{data.discountValue}</td>

                                                    </tr>
                                                )) : <tr><td colSpan="3"></td></tr>}

                                            </tbody>
                                        </table>
                                    </div>
                                </div> : null}


                                {!this.props.isDiscountMap ? <div className="col-md-12 pad-0">
                                    <div className="col-md-4 col-sm-4 pad-0 piFormDiv">
                                        <label className="purchaseLabel">
                                            Discount
                                         </label>
                                        <input type="text" className="discountInput" id="discountType" onKeyDown={(e) => this.handleInput(e)} value={this.state.discount} onChange={(e) => this.inputChange(e)} />
                                        {discountTypeerr ? (
                                            <span className="error" >
                                                Enter Discount
                                                        </span>
                                        ) : null}
                                    </div>
                                    <div className="col-md-4 col-sm-4 pad-0 piFormDiv">
                                        <label className="purchaseLabel">
                                            Discount Value
                                         </label>
                                        <input type="text" className="discountInput" id="discountValue" value={this.state.discountValue} disabled />
                                        {discountValueerr ? (
                                            <span className="error" >
                                                Enter valid Discount
                                                        </span>
                                        ) : null}
                                    </div>
                                </div> : null}


                                <div className="modal-bottom m-top-20">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.handleKeyDown("done", e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.handleKeyDown("close", e)} onClick={(e) => this.props.closeDiscountModal(e)} >Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}

            </div>
                :

                this.state.modalDisplay ? <div className="poArticleModalPosition" id="discountModalPosition" >
                    <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">

                        {this.props.isDiscountMap ? <ul className="dropdown-menu-city-item">
                            {result != undefined || result.length != 0 ?
                                result.map((data, key) => (
                                    <li key={key} onClick={(e) => this.selectedData(`${data.discount}`)} id={data.discount} className={this.state.supplierCode == `${data.discount}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.discount)}>
                                        <span className="vendor-details">
                                            <span className="vd-name">{data.discount}</span>
                                            <span className="vd-loc">{data.discountValue}</span>

                                        </span>
                                    </li>)) : <li><span>No Data Found</span></li>}

                        </ul> : null}
                        {this.props.isDiscountMap ? <div className="page-close">
                            <button className="btn-close" type="button" onClick={(e) => this.props.closeDiscountModal(e)} id="btn-close">Close</button>
                        </div> : null}
                        {!this.props.isDiscountMap ? <div className="col-md-12 pad-0">
                            <div className="col-md-8">
                                <ul className="dropdown-menu-city-item drop-dis-modal">
                                    <div className="dis-modal-lab">
                                        <span className="purchaseLabel">
                                            Discount
                                                </span>
                                        <input type="text" className="discountInput" id="discountType" onKeyDown={(e) => this.handleInput(e)} value={this.state.discount} onChange={(e) => this.inputChange(e)} />
                                        {discountTypeerr ? (
                                            <span className="error" >
                                                Enter Discount
                                                                </span>
                                        ) : null}
                                    </div>
                                    <div className="dis-modal-lab">
                                        <span className="purchaseLabel">
                                            Discount Value
                                                </span>
                                        <input type="text" className="discountInput" id="discountValue" value={this.state.discountValue} disabled />
                                        {discountValueerr ? (
                                            <span className="error" >
                                                Enter valid Discount
                                                                </span>
                                        ) : null}
                                    </div>
                                </ul>
                            </div>
                            <div className="col-md-4 text-right m-top-10">
                                <label className="purchaseLabel set">
                                    {this.state.percent ? "Percent" : "Value"}</label>
                                <label className="tg-switch" >
                                    <input type="checkbox" checked={this.state.percent} onChange={(e) => this.changeType(e)} />
                                    <span className="tg-slider tg-round"></span>
                                </label>
                            </div>
                        </div> : null}
                        <div className="page-close">
                            <button type="button" className="btn-done" id="doneButton" onKeyDown={(e) => this.handleKeyDown("done", e)} onClick={(e) => this.onDone(e)}>Done</button>
                            <button type="button" className="btn-close" id="closeButton" onKeyDown={(e) => this.handleKeyDown("close", e)} onClick={(e) => this.props.closeDiscountModal(e)} >Close</button>
                        </div>
                    </div>
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}

                </div> : null
        )
    }
}
export default DiscountModal;