import React from "react";
import PoError from "../loaders/poError";
import moment from "moment";
import ToastLoader from '../loaders/toastLoader';
import closeSearch from "../../assets/close-recently.svg"
class LoadIndent extends React.Component {
    constructor(props) {

        super(props);
        this.textInput = React.createRef();
        this.state = {
            LoadIndentState: this.props.LoadIndentState,
            selectedId: "",
            checked: false,
            search: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            pattern: "",
            piQuantity: "",
            sectionSelection: "",
            toastLoader: false,
            toastMsg: "",
            indentValue: this.props.indentValue,
            poErrorMsg: false,
            errorMassage: "",
            cityName: "",
            vendor: "",
            toDate: "",
            fromDate: "",
            toDateerr: false,
            fromDateerr: false,
            selectedOrderId: "",
            selectedOrderDetailId: "",
            searchBy: "startWith",
            focusedLi: "",


        }
    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }

    componentWillMount() {
           this.setState({
           
            search: this.props.isModalShow ? "" : this.props.indentSearch,
                                        type: this.props.isModalShow || this.props.indentSearch == "" ? 1 : 3

        })

        if (this.props.purchaseIndent.loadIndent.isSuccess) {
            if (this.props.purchaseIndent.loadIndent.data.resource != null) {
                let c = []
                for (let i = 0; i < this.props.purchaseIndent.loadIndent.data.resource.length; i++) {
                    let x = this.props.purchaseIndent.loadIndent.data.resource[i];
                    x.checked = false;

                    let a = x
                    c.push(a)
                }

                this.setState({
                    loadIndentState: c,
                    prev: this.props.purchaseIndent.loadIndent.data.prePage,
                    current: this.props.purchaseIndent.loadIndent.data.currPage,
                    next: this.props.purchaseIndent.loadIndent.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.loadIndent.data.maxPage,
                })
            } else {
                this.setState({
                    loadIndentState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            this.textInput.current.focus();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.loadIndent.isSuccess) {
            if (nextProps.purchaseIndent.loadIndent.data.resource != null) {
                let c = []
                for (let i = 0; i < nextProps.purchaseIndent.loadIndent.data.resource.length; i++) {
                    let x = nextProps.purchaseIndent.loadIndent.data.resource[i];
                    x.checked = false;
                    let a = x
                    c.push(a)
                }

                this.setState({
                    loadIndentState: c,
                    prev: nextProps.purchaseIndent.loadIndent.data.prePage,
                    current: nextProps.purchaseIndent.loadIndent.data.currPage,
                    next: nextProps.purchaseIndent.loadIndent.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.loadIndent.data.maxPage,
                })
            } else {
                this.setState({
                    loadIndentState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
                if (window.screen.width > 1200) {
                    if (this.props.isModalShow) {
                        this.textInput.current.focus();

                    }
                    else if (!this.props.isModalShow) {
                        if (nextProps.purchaseIndent.loadIndent.data.resource != null) {
                            this.setState({
                                focusedLi: nextProps.purchaseIndent.loadIndent.data.resource[0].id,
            search: this.props.isModalShow ? "" : this.props.indentSearch,
                                        type: this.props.isModalShow || this.props.indentSearch == "" ? 1 : 3

                                
                            })
                            document.getElementById(nextProps.purchaseIndent.loadIndent.data.resource[0].id) != null ? document.getElementById(nextProps.purchaseIndent.loadIndent.data.resource[0].id).focus() : null
                        }



                    }
                }

            }

        }



        page(e) {
            if (e.target.id == "prev") {
                this.setState({
                    prev: this.props.purchaseIndent.loadIndent.data.prePage,
                    current: this.props.purchaseIndent.loadIndent.data.currPage,
                    next: this.props.purchaseIndent.loadIndent.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.loadIndent.data.maxPage,
                })
                if (this.props.purchaseIndent.loadIndent.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.loadIndent.data.currPage - 1,
                        orderId: "",
                        supplier: this.state.vendor,
                        cityName: this.state.cityName,
                        piFromDate: this.state.fromDate,
                        piToDate: this.state.toDate,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.loadIndentRequest(data);
                }
            } else if (e.target.id == "next") {
                this.setState({
                    prev: this.props.purchaseIndent.loadIndent.data.prePage,
                    current: this.props.purchaseIndent.loadIndent.data.currPage,
                    next: this.props.purchaseIndent.loadIndent.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.loadIndent.data.maxPage,
                })
                if (this.props.purchaseIndent.loadIndent.data.currPage != this.props.purchaseIndent.loadIndent.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.loadIndent.data.currPage + 1,
                        orderId: "",
                        supplier: this.state.vendor,
                        cityName: this.state.cityName,
                        piFromDate: this.state.fromDate,
                        piToDate: this.state.toDate,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.loadIndentRequest(data)
                }
            }
            else if (e.target.id == "first") {
                this.setState({
                    prev: this.props.purchaseIndent.loadIndent.data.prePage,
                    current: this.props.purchaseIndent.loadIndent.data.currPage,
                    next: this.props.purchaseIndent.loadIndent.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.loadIndent.data.maxPage,
                })
                if (this.props.purchaseIndent.loadIndent.data.currPage <= this.props.purchaseIndent.loadIndent.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        orderId: "",
                        supplier: this.state.vendor,
                        cityName: this.state.cityName,
                        piFromDate: this.state.fromDate,
                        piToDate: this.state.toDate,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.loadIndentRequest(data)
                }

            }
        }

        selectedData(e) {
            this.setState({
                selectedId: e
            })
            for (let i = 0; i < this.state.loadIndentState.length; i++) {
                if (this.state.loadIndentState[i].id == e) {

                    this.setState({
                        indentValue: this.state.loadIndentState[i].id
                    }, () => {
                    if (!this.props.isModalShow) {
                        this.ondone()
                    }
                })
                }

            }

        }

        ondone() {
            var sCode = "";
            var pattern = "";
            let data = {}
            if (this.props.isModalShow?this.state.indentValue != "":this.state.indentValue != ""||this.props.indentSearch == this.state.search||this.props.indentSearch != this.state.search) {

                if (this.props.isModalShow?this.state.indentValue == this.props.indentValue:this.props.indentSearch != this.state.search) {
                    this.closeLoadIndent()
                } else {
                    for (let i = 0; i < this.state.loadIndentState.length; i++) {
                        if (this.state.loadIndentState[i].id == this.state.indentValue) {
                            data = {
                                sCode: this.state.loadIndentState[i].orderId,
                                pattern: this.state.loadIndentState[i].orderDetailId,
                                indentId: this.state.indentValue
                            }
                        }
                    }
                    let t = this
                    setTimeout(function () {
                        t.props.updateLoadIndentState(data);
                    }, 100)
                }

                this.setState(
                    {
                        search: "",
                    })

                this.closeLoadIndent()


            } else {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)

            }
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return true;
                }
            }
        }

        closeLoadIndent(e) {
            this.setState({
                search: "",
                sectionSelection: "",
                loadIndentState: [],
                type: "",
                prev: 0,
                current: 0,
                next: 0,
                maxPage: 0,
            })

            this.props.onCloseLoadIndent(e)
        }

        onClear(e) {
            if (this.state.type == 2 || this.state.type == 3) {
                var data = {
                    type: 1,
                    no: 1,
                    orderId: "",
                    supplier: this.state.vendor,
                    cityName: this.state.cityName,
                    piFromDate: this.state.fromDate,
                    piToDate: this.state.toDate,
                    search: "",
                    searchBy: this.state.searchBy
                }
                this.props.loadIndentRequest(data)
            }
            this.setState(
                {

                    type: "",
                    no: 1,
                    vendor: "",
                    cityName: "",
                    fromDate: "",
                    toDate: ""
                })
        }
        onClearSearch() {
            this.setState(
                {
                    search: "",
                    type: "",
                    no: 1,

                })
            if (this.state.type == 2 || this.state.type == 3) {
                var data = {
                    type: 1,
                    no: 1,
                    orderId: "",
                    supplier: this.state.vendor,
                    cityName: this.state.cityName,
                    piFromDate: this.state.fromDate,
                    piToDate: this.state.toDate,
                    search: "",
                    searchBy: this.state.searchBy
                }
                this.props.loadIndentRequest(data)
            }
            document.getElementById("search").focus()
        }



        onSearch(e) {

            if (this.state.search == "") {
                this.setState({
                    toastMsg: "Enter data on search input",
                    toastLoader: true,

                })
                const t = this;
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            } else {
                if (this.state.sectionSelection == "") {
                    this.setState({
                        type: 3,

                    })
                    let data = {
                        type: 3,
                        no: 1,
                        orderId: "",
                        supplier: this.state.vendor,
                        cityName: this.state.cityName,
                        piFromDate: this.state.fromDate,
                        piToDate: this.state.toDate,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.loadIndentRequest(data)

                }
                else {
                    this.setState({
                        type: 2,

                    })
                    let data = {
                        type: 2,
                        no: 1,
                        orderId: "",
                        supplier: this.state.vendor,
                        cityName: this.state.cityName,
                        piFromDate: this.state.fromDate,
                        piToDate: this.state.toDate,
                        search: "",
                        searchBy: this.state.searchBy
                    }
                    this.props.loadIndentRequest(data)
                }
            }

        }

        handleChange(e) {
            if (e.target.id == "search") {

                this.setState(
                    {
                        search: e.target.value
                    },

                );
            } else if (e.target.id == "vendor") {
                this.setState(
                    {
                        vendor: e.target.value
                    },

                );
            } else if (e.target.id == "cityName") {
                this.setState(
                    {
                        cityName: e.target.value
                    },

                );
            } else if (e.target.id == "toDate") {
                e.target.placeholder = e.target.value;
                this.setState({
                    toDate: e.target.value
                }
                    // , () => {
                    //     this.DateTo()
                    // }
                )
            } else if (e.target.id == "fromDate") {
                e.target.placeholder = e.target.value;
                this.setState({
                    fromDate: e.target.value
                }
                )
            } else if (e.target.id == "searchByindent") {
                this.setState({
                    searchBy: e.target.value
                }, () => {
                    if (this.state.search != "") {
                        let data = {
                            type: this.state.type,
                            no: 1,
                            orderId: "",
                            supplier: this.state.vendor,
                            cityName: this.state.cityName,
                            piFromDate: this.state.fromDate,
                            piToDate: this.state.toDate,
                            search: this.state.search,
                            searchBy: this.state.searchBy
                        }
                        this.props.loadIndentRequest(data)
                    }

                })
            }

        }
        DateTo() {
            if (this.state.toDate == "") {
                this.setState({
                    toDateerr: true
                })
            } else {
                this.setState({
                    toDateerr: false
                })
            }
        }
        DateFrom() {
            if (this.state.fromDate == "") {
                this.setState({
                    fromDateerr: true
                })
            } else {
                this.setState({
                    fromDateerr: false
                })
            }
        }

        onApplyFilter(e) {

            e.preventDefault();
            if (this.state.fromDate != "") {
                this.DateTo();
            }
            setTimeout(() => {
                const { toDateerr, fromDateerr } = this.state;
                if (!toDateerr && !fromDateerr) {

                    let data = {
                        type: 2,
                        no: 1,
                        orderId: "",
                        supplier: this.state.vendor,
                        cityName: this.state.cityName,
                        piFromDate: this.state.fromDate,
                        piToDate: this.state.toDate,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.loadIndentRequest(data)
                }
            })
            this.setState({ type: 2 })

        }
        _handleKeyPress = (e) => {
            if (e.key === 'Enter') {
                if (e.target.value != "") {
                    this.onSearch();
                }
            }
        }


        _handleKeyDown = (e) => {

            if (e.key === "Tab") {
                if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                    if (this.state.loadIndentState.length != 0) {

                        this.setState({
                            indentValue: this.state.loadIndentState[0].id
                        })
                        let icode = this.state.loadIndentState[0].id
                        this.selectedData(icode)
                        window.setTimeout(function () {
                            document.getElementById("search").blur()
                            document.getElementById(icode).focus()
                        }, 0)
                    } else {
                        window.setTimeout(function () {
                            document.getElementById("search").blur()
                            document.getElementById("closeButton").focus()
                        }, 0)
                    }
                }
                if (e.target.value != "") {
                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById("findButton").focus()
                    }, 0)
                }
            }
            if (e.key == "ArrowDown") {
                if (this.state.loadIndentState.length != 0) {

                    this.setState({
                        indentValue: this.state.loadIndentState[0].id
                    })
                    let icode = this.state.loadIndentState[0].id
                    this.selectedData(icode)
                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById(icode).focus()
                    }, 0)
                } else {
                    window.setTimeout(function () {
                        document.getElementById("search").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }
            this.handleChange(e)
        }
        findKeyDown(e) {
            if (e.key == "Enter") {
                this.onSearch()
            }
            if (e.key == "Tab") {

                window.setTimeout(function () {

                    document.getElementById("findButton").blur()
                    document.getElementById("clearButton").focus()

                }, 0)
            }

        }
        focusDone(e) {

            if (e.key === "Tab") {
                let icode = this.state.loadIndentState[0].id
                window.setTimeout(function () {


                    document.getElementById(icode).blur()
                    document.getElementById("doneButton").focus()
                }, 0)
            }
            if (e.key === "Enter") {
                this.ondone()
            }
        }
        doneKeyDown(e) {
            if (e.key === "Enter") {
                this.ondone();
            }
            if (e.key === "Tab") {
                window.setTimeout(function () {
                    document.getElementById("doneButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
        closeKeyDown(e) {
            if (e.key === "Enter") {
                this.closeLoadIndent();
            }
            if (e.key === "Tab") {
                window.setTimeout(function () {
                    document.getElementById("closeButton").blur()
                    document.getElementById("search").focus()
                }, 0)
            }

        }

        onClearDown(e) {
            if (e.key == "Enter") {
                this.onClearSearch();
            }
            if (e.key == "Tab") {
                if (this.state.loadIndentState.length != 0) {
                    this.setState({
                        indentValue: this.state.loadIndentState[0].id
                    })
                    let icode = this.state.loadIndentState[0].id
                    this.selectedData(icode)
                    window.setTimeout(function () {

                        document.getElementById("clearButton").blur()
                        document.getElementById(icode).focus()
                    }, 0)

                } else {
                    window.setTimeout(function () {

                        document.getElementById("clearButton").blur()
                        document.getElementById("search").focus()
                    }, 0)
                }
            }
        }


       selectLi(e, code) {
        let loadIndentState = this.state.loadIndentState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < loadIndentState.length; i++) {
                if (loadIndentState[i].id == code) {
                    index = i
                }
            }
            if (index < loadIndentState.length - 1 || index == 0) {
                document.getElementById(loadIndentState[index + 1].id) != null ? document.getElementById(loadIndentState[index + 1].id).focus() : null

                this.setState({
                    focusedLi: loadIndentState[index + 1].id
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < loadIndentState.length; i++) {
                if (loadIndentState[i].id == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(loadIndentState[index - 1].id) != null ? document.getElementById(loadIndentState[index - 1].id).focus() : null

                this.setState({
                    focusedLi: loadIndentState[index - 1].id
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.closeLoadIndent(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.loadIndentState.length!=0?
                document.getElementById(this.state.loadIndentState[0].id).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.loadIndentState.length!=0){
                document.getElementById(this.state.loadIndentState[0].id).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.closeLoadIndent(e)
        }


    }
  

    render() {
            !this.props.isModalShow ? $('.poSearchModal').removeClass('hideSmoothly')
            : null
        if (this.state.focusedLi != "" && this.props.indentSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

            const { search, toDate, fromDate, toDateerr, fromDateerr } = this.state;
            return (

                this.props.isModalShow ? <div className={this.props.loadIndentAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                    <div className={this.props.loadIndentAnimation ? "backdrop display_block" : "display_none"}></div>
                    <div className={this.props.loadIndentAnimation ? "modal_Indent display_block loadIndentModal" : "display_none"}>
                        <div className={this.props.loadIndentAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                            <div className="col-md-12 col-sm-12">
                                <div className="modal_Color">
                                    <div className="modal-top displayBlock">
                                        <ul className="list_style width_100 m-top-20">
                                            <li>
                                                <label className="select_name-content">SELECT LOAD INDENT</label>
                                            </li>
                                            <li>
                                                <p className="para-content">You can select only single entry from below records</p>
                                            </li>
                                        </ul>

                                        <div className="col-md-12 pad-0 modalFilter">
                                            <form onSubmit={(e) => this.onApplyFilter(e)}>
                                                <ul className="pad-0">
                                                    <li className="displayInline">
                                                        <ul className="list-inline">
                                                            <label className="filter_modal displayBlock pad-lft-5 textLeft">FILTERS</label>
                                                            <li>
                                                                <input type="text" onChange={(e) => this.handleChange(e)} id="vendor" value={this.state.vendor} placeholder="Enter Supplier Name" className="organistionFilterModal" />
                                                            </li>

                                                            <li>
                                                                <input type="text" onChange={(e) => this.handleChange(e)} id="cityName" value={this.state.cityName} placeholder="Enter city name" className="organistionFilterModal" />
                                                            </li>

                                                            <li>
                                                                <input type="date" placeholder={this.state.fromDate != "" ? this.state.fromDate : "Date Range From"} id="fromDate" onChange={(e) => this.handleChange(e)} value={this.state.fromDate} className="organistionFilterModal" />
                                                                {fromDateerr ? (
                                                                    <span className="error">
                                                                        Select Date From
                                                                </span>
                                                                ) : null}
                                                            </li>
                                                            <li>
                                                                {fromDate == "" ? <input type="date" placeholder={this.state.toDate != "" ? this.state.toDate : "Delivery Date To"} className="organistionFilterModal" disabled /> :
                                                                    <input type="date" placeholder="Date Range To" id="toDate" onChange={(e) => this.handleChange(e)} value={this.state.toDate} className="organistionFilterModal" />}
                                                                {toDateerr ? (
                                                                    <span className="error">
                                                                        Select Date To
                                                                </span>
                                                                ) : null}
                                                            </li>
                                                        </ul>

                                                    </li>
                                                </ul>
                                            </form>
                                            <ul className="list-inline width_100 m-top-10 chooseDataModal">
                                                <div className="col-md-6 pad-0">
                                                    <li className="displayInline posRelative verticalBot">
                                                        {this.state.vendor == "" && this.state.cityName == "" && this.state.toDate == "" && this.state.fromDate == "" ?
                                                            <button type="button" className="modal_Apply_btn btnDisabled" disabled>
                                                                APPLY
                                                         </button>
                                                            : <button type="button" className="modal_Apply_btn" onClick={(e) => this.onApplyFilter(e)}>
                                                                APPLY
                                                             </button>}
                                                        {this.state.type != 2 ? null : <span className="m-lft-8 displayPointer" onClick={(e) => this.onClear(e)}>Clear Filter</span>}

                                                    </li>
                                                    {/* <li className="displayInline posRelative verticalBot">
                                                    <label>
                                                        <button type="button" onClick={(e) => this.onClear(e)}>CLEAR</button>
                                                    </label>
                                                </li> */}
                                                </div>
                                                <div className="col-md-6 col-sm-6 pad-0 chooseDataModal" >
                                                    <li className="textRight">
                                                        {/* <select id="sectionBasedOn"  name="sectionSelection" value={sectionSelection} onChange={(e) => this.handleChange(e)}>
                                            <option value="">Choose</option>
                                                <option value="pattern">Pattern</option>
                                                <option value="piQuantity">PI Quantity</option>
                                                <option value="piAmount">PI Amount</option>
                                           
                                            </select> */}
                                                        {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByindent" name="searchByindent" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                            <option value="contains">Contains</option>
                                                            <option value="startWith"> Start with</option>
                                                            <option value="endWith">End with</option>

                                                        </select> : null}
                                                        <input type="search" autoComplete="off" autoCorrect="off" id="search" ref={this.textInput} onKeyPress={this._handleKeyPress} onKeyDown={this._handleKeyDown} value={search} onChange={(e) => this.handleChange(e)} className="search-box" placeholder="Type to search" />
                                                        {this.state.search == "" && (this.state.type == "" || this.state.type == 1) ?

                                                            <button type="button" className="clearbutton m-lft-15" id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onClearSearch(e)}>CLEAR</button>

                                                            : <button type="button" className="clearbutton btnDisabled">CLEAR</button>}
                                                        <label className="m-lft-15">
                                                            {search != "" ? <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                    <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                    <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                                </svg>
                                                            </button> : <button type="button" className="findButton btnDisabled btnDisableSvg" >FIND
                                                    <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                        <path fill="#8b8b8b" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                                    </svg>
                                                                </button>}
                                                        </label>
                                                    </li>
                                                </div>


                                            </ul>
                                        </div>

                                    </div>

                                    <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                        <div className="modal_table">
                                            <table className="table tableModal table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Select</th>
                                                        <th>Pattern</th>
                                                        <th>Order Id</th>
                                                        <th>Article code</th>
                                                        <th>Supplier Name</th>
                                                        <th>City Name</th>

                                                        <th>Generated Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {this.state.loadIndentState == undefined ? <tr className="modalTableNoData"><td colSpan="7"> NO DATA FOUND </td></tr> : this.state.loadIndentState.length == 0 ? <tr className="modalTableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr> : this.state.loadIndentState.map((data, key) => (
                                                        <tr key={key} onClick={() => this.selectedData(`${data.id}`)}>
                                                            <td>
                                                                <label className="select_modalRadio">
                                                                    <input id={data.id} type="radio" name="transporter" onKeyDown={(e) => this.focusDone(e)} checked={`${data.id}` == this.state.indentValue} readOnly />
                                                                    <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                                </label>
                                                            </td>
                                                            <td>{data.orderDetailId}</td>
                                                            <td className="pad-lft-8">{data.orderId}</td>
                                                            <td>{data.articleCode}</td>
                                                            <td>{data.supplierName}</td>
                                                            <td className="pad-lft-8">{data.cityName}</td>
                                                            <td>{moment(data.generatedDate).utc().format("DD-MMM-YYYY HH:mm")}</td>

                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="modal-bottom">
                                        <ul className="list-inline width_35 m-top-9 modal-select">

                                            <li className="float_left">
                                                <label className="m-r-15">
                                                    <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Done</button>
                                                </label>
                                                <label>
                                                    <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeLoadIndent(e)}>Close</button>
                                                </label>
                                            </li>
                                        </ul>
                                        <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                            <ul className="list-inline pagination paginationWidth50">
                                                {this.state.current == 1 || this.state.current == 0 ? <li >
                                                    <button className="PageFirstBtn" type="button"  >
                                                        First
                  </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                            First
                  </button>
                                                    </li>}
                                                {this.state.prev != 0 ? <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                        Prev
                  </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn" type="button" disabled>
                                                            Prev
                  </button>
                                                    </li>}
                                                <li>
                                                    <button className="PageFirstBtn pointerNone" type="button">
                                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                                    </button>
                                                </li>
                                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                    <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                        Next
                  </button>
                                                </li> : <li >
                                                        <button className="PageFirstBtn borderNone" type="button" disabled>
                                                            Next
                  </button>
                                                    </li> : <li >
                                                        <button className="PageFirstBtn borderNone" type="button" disabled>
                                                            Next
                  </button>
                                                    </li>}



                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                </div>
                :


                <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                        <div className="dropdown-modal-header">
                            <span className="div-col-5">Order Detail Id</span>
                            <span className="div-col-5">Order Id</span>
                            <span className="div-col-5">Supplier</span>
                            <span className="div-col-5">City</span>
                            <span className="div-col-5">Date</span>
                        </div>

                    <ul className="dropdown-menu-city-item">
                        {this.state.loadIndentState == undefined || this.state.loadIndentState.length == 0 ? <li><span>No Data Found</span></li>
                           : this.state.loadIndentState.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.id}`)} id={data.id} className={this.state.supplierCode == `${data.id}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.id)}>
                                    <span className="vendor-details">
                                        <span className="vd-name div-col-5">{data.orderDetailId}</span>
                                        <span className="vd-loc div-col-5">{data.orderId}</span>
                                        <span className="vd-num div-col-5">{data.supplierName}</span>
                                        <span className="vd-code div-col-5">{data.cityName}</span>
                                        <span className="vd-code div-col-5">{moment(data.generatedDate).utc().format("DD-MMM-YYYY HH:mm")}</span>
                                        
                                    </span>
                                </li>)) }

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.closeLoadIndent(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>


            );
        }
    }

    export default LoadIndent;