import React from "react";

class PDashboardSite extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        console.log(this.props.siteDetails)
        return (
            <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                <div className="dropdown-modal-header">
                    <span className="div-col-2">Site Code</span>
                    <span className="div-col-2">Site Name</span>
                </div>
                <ul className="dropdown-menu-city-item">
                    {this.props.siteDetails.length > 0 ? this.props.siteDetails.map((_, k) => (
                        <li key={k} onClick={() => this.props.selectedSite(_)}>
                            <span className="vendor-details">
                                <span className="vd-name div-col-2">{_.SiteCode}</span>
                                <span className="vd-loc div-col-2">{_.SiteName}</span>
                            </span>
                        </li>
                    )) : <li><span>No Data Found</span></li>}
                </ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={this.props.handleSetModal}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        {this.props.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.props.page(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.props.page(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                        <button className="pnpb-no" type="button">1/1</button>
                        {this.props.maxPage != 0 ? this.props.next <= this.props.maxPage ? <button className="pnpb-next" type="button" onClick={(e) => this.props.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.props.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>
                            : <button className="pnpb-next" type="button" >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                            </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                    </div>
                </div>
            </div>
        )
    }
}

export default PDashboardSite;