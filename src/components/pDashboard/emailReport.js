import React from 'react';
import RequestSuccess from '../loaders/requestSuccess';
import FilterLoader from '../loaders/filterLoader';
import RequestError from "../loaders/requestError";

class EmailReport extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            emailValueTo: '',
            emailValueCc: '',
            emailsTo: [],
            emailsCc: [],
            status: [],
            checkedAll: false,
            checked: true,
            isAttachements: false,
            isMainHeader: false,
            success: false,
            successMessage: "",
            subjectValue: "Email Dashboard",
            appliedFiltersYes: true,
            appliedFiltersNo: false,
            loader: false,
            moduleError: "procurement",
            alert: false,
            code:"",
            errorCode: "",
            errorMessage: "",
            emailCcToPrint: [],
            emailToPrint: []
        }
    }

    componentDidMount(){
        document.addEventListener("keydown", this.handleKey, false);
      }
      componentWillUnmount(){
        document.removeEventListener("keydown", this.handleKey, false);
      }
    componentWillReceiveProps(nextProps) {
        if(nextProps.purchaseIndent.emailDashboardSend.isLoading){
            this.setState({
                loader: true
            })
        }
        console.log(nextProps, nextProps.purchaseIndent)   
        if (nextProps.purchaseIndent.emailDashboardSend.isSuccess) {   
                 
            if (nextProps.purchaseIndent.emailDashboardSend.data.message != null) {
               this.setState({
                   success: true,
                   successMessage: nextProps.purchaseIndent.emailDashboardSend.data.message,
                   loader: false,
                   statusCode: nextProps.purchaseIndent.emailDashboardSend.data.status
               })
            }
        }
        else if (nextProps.purchaseIndent.emailDashboardSend.isError) {
            this.setState({
              loader: false,
              alert: true,
              code: nextProps.purchaseIndent.emailDashboardSend.message.status,
              errorCode: nextProps.purchaseIndent.emailDashboardSend.message.error == undefined ? undefined : nextProps.purchaseIndent.emailDashboardSend.message.error.errorCode,
              errorMessage: nextProps.purchaseIndent.emailDashboardSend.message.error == undefined ? undefined : nextProps.purchaseIndent.emailDashboardSend.message.error.errorMessage
            })
            this.props.emailDashboardSendClear()
          }
          else if(nextProps.purchaseIndent.emailDashboardSend.message.status == 500){
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.emailDashboardSend.message.status,
                errorCode: nextProps.purchaseIndent.emailDashboardSend.message.error == undefined ? undefined : nextProps.purchaseIndent.emailDashboardSend.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.emailDashboardSend.message.error == undefined ? undefined : nextProps.purchaseIndent.emailDashboardSend.message.error.errorMessage
              }) 
          }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
          success: false
        });
        document.getElementById("close").click();
      }

      onError(e) {
        e.preventDefault();
        this.setState({
          alert: false
        });
        document.onkeydown = function (t) {
          if (t.which) {
            return true;
          }
        }
      }
    

    handleEmailChange(e) {
        if (e.target.id == "emailTo") {
            this.setState({
                 emailValueTo: e.target.value,
            })
        }
        else if(e.target.id == "emailCc") {
            this.setState({
                emailValueCc: e.target.value
           })
        }
        
    }
    _handleKeyPressRowEmail = (e, data) => {
        if(data == "keyPress"){
            if (['Enter', 'Tab', ',', ' '].includes(e.key)) {
                e.preventDefault();
                 if(e.target.id == "emailTo"){
                  var email = this.state.emailValueTo.trim();
                  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
                  if (re.test(email)) {
                    this.setState({
                      emailToPrint: [...this.state.emailsTo, email],
                      emailsTo: [...this.state.emailsTo, email],
                      emailValueTo: '',
                    });
                  }
                 } 
                 else if(e.target.id == "emailCc"){
                  var email = this.state.emailValueCc.trim();
                  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
                  if (re.test(email)) {
                    this.setState({
                      emailCcToPrint: [...this.state.emailsCc, email],
                      emailsCc: [...this.state.emailsCc, email],
                      emailValueCc: '',
                    });
                  }
                 }
                
              }
        }
        else if(data == "focusOut"){
                e.preventDefault();
                 if(e.target.id == "emailTo"){
                  var email = this.state.emailValueTo.trim();
                  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
                  if (re.test(email)) {
                    this.setState({
                      emailToPrint: [...this.state.emailsTo, email],
                      emailsTo: [...this.state.emailsTo, email],
                      emailValueTo: '',
                    });
                  }
                 } 
                 else if(e.target.id == "emailCc"){
                  var email = this.state.emailValueCc.trim();
                  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
                  if (re.test(email)) {
                    this.setState({
                      emailCcToPrint: [...this.state.emailsCc, email],
                      emailsCc: [...this.state.emailsCc, email],
                      emailValueCc: '',
                    });
                  }
                }
        }
           
    }
    handleCrossIcon = (toBeRemoved, e) => {
        if(e.target.id == "emailToImg"){
            this.setState({
              emailsTo: this.state.emailsTo.filter(email => email !== toBeRemoved),
              emailToPrint: this.state.emailsTo.filter(email => email !== toBeRemoved)
            });
        }
        else if(e.target.id == "emailCcImg"){
            this.setState({
                emailsCc: this.state.emailsCc.filter(email => email !== toBeRemoved),
                emailCcToPrint: this.state.emailsCc.filter(email => email !== toBeRemoved)
              });
        }
    }
    hasAttachement = () => {
        this.setState({
            isAttachements: !this.state.isAttachements
        }, () => {
            if(this.state.isAttachements == false){
                this.setState({
                    isMainHeader: false,
                    isLineItem: false
                })
            }
            else{
                this.setState({
                    isMainHeader: true,
                    isLineItem: false
                })
            }
        })
    }
    statusCheckbox = (e, data) => {
        var temp = ["PENDING", "DRAFTED", "APPROVED", "REJECTED"]
        var statusVar = []
        if(e.target.name == "allData"){
            this.setState({
                checkedAll: !this.state.checkedAll,
            }, () => {
                if(this.state.checkedAll){
                    statusVar = ["PENDING", "DRAFTED", "APPROVED", "REJECTED"]
                }
                else{
                    statusVar = []
                }
                this.setState({
                    status: statusVar
                })
            })
            statusVar = []
        }

        else {
            var array =  [...this.state.status]
                if(this.state.status.indexOf(data) > -1){
                        const index = array.indexOf(data);
                        if (index > -1) {
                          array.splice(index, 1);
                        }
                        if(array.sort() == temp.sort()){
                            console.log(array, array.sort, array.sort(), temp, temp.sort, temp.sort())

                            this.setState({
                                checkedAll: true,
                                status: array,
                            })
                        }
                        else{
                            this.setState({
                                checkedAll: false,
                                status: array,
                            })
                        }
                }
                else {
                    if (this.state.checked) {
                        array.push(data)
                        console.log(array, array.sort(), temp, temp.sort())
                        var temp1 = array.sort();
                        var temp2 = temp.sort();
                        
                        if (JSON.stringify(temp1)==JSON.stringify(temp2)) {
                            this.setState({
                                checkedAll: true,
                                status: [...this.state.status, data],
                            })
                        }
                        else {
                           this.setState({
                                checkedAll: false,
                                status: [...this.state.status, data],
                            })
                        }
                    }
                }
            statusVar = []
        }
    }

    headerOrLineItemdata = (e) => {
        if(e.target.name == "lineItemLevel"){
            this.setState({
                isLineItem: true,
                isMainHeader: false
            })
        }
        else if(e.target.name == "headerLevel"){
            this.setState({
                isMainHeader: true,
                isLineItem: false,
            })
        }
    }

    handleSubjectChange = (e) => {
        
            this.setState({
                subjectValue: e.target.value,
            })
        
    } 
    handleAppliedFilters = (e) => {
        if(e.target.name == "appliedFilterYes"){
            this.setState({
                appliedFiltersYes: true,
                appliedFiltersNo: false,
            })
        }
        else if(e.target.name == "appliedFilterNo"){
            this.setState({
                appliedFiltersYes: false,
                appliedFiltersNo: true,
            })
        }
    }

    handleKey = (e) => {
        console.log("yooooo", e)

        if (e.keyCode == 27 || e.key ==  "Escape") {
            document.getElementById("close").click();
            // this.props.CloseEmailReport
        }
    }
    clearAll = () => {
        this.setState({
            emailValueTo: '',
            emailValueCc: '',
            emailsTo: [],
            emailsCc: [],
            status: [],
            checkedAll: false,
            checked: true,
            isAttachements: false,
            isMainHeader: false,
            success: false,
            successMessage: "",
            subjectValue: "Email Dashboard",
            appliedFiltersYes: true,
            appliedFiltersNo: false,
            loader: false,
            moduleError: "procurement",
            alert: false,
            code:"",
            errorCode: "",
            errorMessage: "",
            emailCcToPrint: [],
            emailToPrint: []
        })
    }

        sendBtn = (e) => {
            var payload = {
                isPO : this.props.dashboardType == "pi" ? false : true,
                to: this.state.emailsTo.join(),
                cc: this.state.emailsCc.join(),
                bcc: "",
                subject: this.state.subjectValue,
                isAttachmentFile: this.state.isAttachements,
                totalPending: this.props.poCards.totalPendingPO != undefined ? this.props.poCards.totalPendingPO : this.props.poCards.totalPendingPI,
                totalApproved: this.props.poCards.totalApprovedPO != undefined ? this.props.poCards.totalApprovedPO : this.props.poCards.totalApprovedPI,
                totalDrafted: this.props.poCards.totalDraftedPO != undefined ? this.props.poCards.totalDraftedPO : this.props.poCards.totalDraftedPI,
                totalRejected: this.props.poCards.totalRejectedPO != undefined ? this.props.poCards.totalRejectedPO : this.props.poCards.totalRejectedPI,
                pendingAmount: this.props.poCards.pendingPOAmount != undefined ? this.props.poCards.pendingPOAmount : this.props.poCards.pendingPIAmount,
                rejectedAmount: this.props.poCards.rejectedPOAmount != undefined ? this.props.poCards.rejectedPOAmount : this.props.poCards.rejectedPIAmount,
                approvedAmount: this.props.poCards.approvedPOAmount != undefined ? this.props.poCards.approvedPOAmount : this.props.poCards.approvedPIAmount,
                draftedAmount: this.props.poCards.draftedPOAmount != undefined ? this.props.poCards.draftedPOAmount : this.props.poCards.draftedPIAmount,
                status: this.state.status.join(),
                isMainHeader: this.state.isMainHeader,
                fromDate: this.state.appliedFiltersYes ? this.props.startDate : "",
                toDate: this.state.appliedFiltersYes ? this.props.endDate: "",
                siteCode: this.state.appliedFiltersYes ?  this.props.siteDetails.siteCode: "",
                slCode: this.state.appliedFiltersYes ? this.props.siteDetails.siteName: "",
                hl1Code: this.state.appliedFiltersYes ? this.props.hierarchy.Division.code: "",
                hl2Code: this.state.appliedFiltersYes ? this.props.hierarchy.Section.code: "",
                hl3Code: this.state.appliedFiltersYes ? this.props.hierarchy.Department.code: "",
                hl4Code: this.state.appliedFiltersYes ? this.props.hierarchy.Article.code: "",
                expireOnDays: this.props.poCards.expireOnDays != undefined ? this.props.poCards.expireOnDays : "",
            }
            this.props.emailDashboardSendRequest(payload)
        }    
    render() {
        return(
            <div className="modal">
                <div onKeyDown ={(e) => this.handleKey(e)} className="backdrop modal-backdrop-new"></div>
                <div className="modal-content email-report-modal">
                    <div className="erm-head">
                        <h3>EMAIL REPORT</h3>
                        <span className="ermh-close">
                            <span className="ermh-esc">Esc</span>
                            <img id = {"close"} onClick={this.props.CloseEmailReport} src={require('../../assets/clearSearch.svg')} />
                        </span>
                    </div>
                    <div className="erm-body" style = {{padding: "0px 25px"}}>
                    <h3>Subject</h3>
                        <div className="ermb-input-field" style = {{height: "40px", padding: "0px 8px", overflow: "hidden", marginBottom: "10px"}}>
                            <label>
                                <input style = {{ height: "38px"}} autoComplete="off" type="text" onChange={(e) => this.handleSubjectChange(e)} className="inputTable" value={this.state.subjectValue} id={"subject"} >
                                </input>
                            </label>
                        </div>
                        <h3>TO</h3>
                        <div className="ermb-input-field"  style = {{height: "40px", padding: "0px 8px"}}>
                            <label>
                                {this.state.emailToPrint.length != 0 ? this.state.emailToPrint.map(el => (
                                        <span className="ermbif-tag">
                                            <span className="ermbift-mailid">{el}</span>
                                            <img id = {"emailToImg"} onClick = {(e) => this.handleCrossIcon(el, e)} src={require('../../assets/clearSearch.svg')}/>
                                        </span>
                                    )):null}
                                <input autoComplete="off" type="email" onBlur = {(e) => this._handleKeyPressRowEmail(e, "focusOut")} onChange={(e) => this.handleEmailChange(e)} className="inputTable" value={this.state.emailValueTo} onKeyDown={(e) => this._handleKeyPressRowEmail(e, "keyPress")} id={"emailTo"} >
                                </input>
                            </label>
                        </div>
                        <h3>CC</h3>
                        <div className="ermb-input-field" style = {{height: "40px", padding: "0px 8px"}}>
                            <label>
                                {this.state.emailCcToPrint.length != 0 ? this.state.emailCcToPrint.map(el => (
                                        <span className="ermbif-tag">
                                            <span className="ermbift-mailid">{el}</span>
                                            <img id = {"emailCcImg"} onClick = {(e) => this.handleCrossIcon(el, e)} src={require('../../assets/clearSearch.svg')}/>
                                        </span>
                                    )):null}
                                <input autoComplete="off" type="email" onBlur = {(e) => this._handleKeyPressRowEmail(e, "focusOut")} onChange={(e) => this.handleEmailChange(e)} className="inputTable" value={this.state.emailValueCc} onKeyDown={(e) => this._handleKeyPressRowEmail(e, "keyPress")} id={"emailCc"} >
                                </input>
                            </label>
                        </div>
                        <div className="ermb-dashdata">
                            <h3>Dashboard Data</h3>
                            <div className="ermbdd-inner">
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox"  name = "allData" checked = {this.state.checkedAll} onChange = {(e) => this.statusCheckbox(e, "")}/>All Data
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked = {this.state.status.indexOf("PENDING") > -1} name = "pending"  onChange = {(e) => this.statusCheckbox(e, "PENDING")}/>{this.props.dashboardType == "pi" ? "PI Pending for Approval" : "PO Pending for Approval" }
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked = {this.state.status.indexOf("DRAFTED") > -1}  name = "drafted" onChange = {(e) => this.statusCheckbox(e, "DRAFTED")}/>{this.props.dashboardType == "pi" ? "PI Saved as Draft" : "PO Saved as Draft" }
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked = {this.state.status.indexOf("APPROVED") > -1}  name = "approved" onChange = {(e) => this.statusCheckbox(e, "APPROVED")}/>{this.props.dashboardType == "pi" ? "PI Approved" : "PO Approved"}
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked = {this.state.status.indexOf("REJECTED") > -1}  name = "rejected" onChange = {(e) => this.statusCheckbox(e, "REJECTED")}/>{this.props.dashboardType == "pi" ?  "PI Rejected" : "PO Rejected"}
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="ermb-dashdata">
                            <h3>Attachments</h3>
                            <div className="ermbdd-inner">
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked={this.state.isAttachements} onChange = {() => this.hasAttachement()} />Has Attachment
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="gen-pi-formate">
                                    <ul className="gpf-radio-list">
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input disabled = {!this.state.isAttachements} type="radio" name="headerLevel" checked={this.state.isMainHeader} onChange={(e) => this.headerOrLineItemdata(e)} />
                                                <span className="checkmark"></span>
                                                Header Level
                                            </label>
                                        </li>
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input disabled = {!this.state.isAttachements} type="radio" checked={this.state.isLineItem} name="lineItemLevel" onChange={(e) => this.headerOrLineItemdata(e)} />
                                                <span className="checkmark"></span>
                                                Line Item level
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="ermb-dashdata">
                            <h3>Send with applied filter</h3>
                            <div className="ermbdd-inner">
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked = {this.state.appliedFiltersYes} name = "appliedFilterYes" onChange = {(e) => this.handleAppliedFilters(e)} />Yes
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked = {this.state.appliedFiltersNo}  name = "appliedFilterNo" onChange = {(e) => this.handleAppliedFilters(e)} />No
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="erm-footer">
                        <button type="button" className="ermf-send" onClick = {(e) => this.sendBtn(e)}>Send Now</button>
                        <button type="button" className="ermf-clear" onClick = {() => this.clearAll()}>Clear</button>
                    </div>
                </div>
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <RequestError code={this.state.code} moduleError={this.state.moduleError} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>
        )
    }
}

export default EmailReport;