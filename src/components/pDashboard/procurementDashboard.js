import React, { Component } from 'react';
import Rupee from '../../assets/rupee.svg';
import InfoIcon from '../../assets/info-icon.svg';
import InfoWhite from '../../assets/infoWhite.svg';
import PiDashBottom from './piDashBottom';
import Filter from '../../assets/filter1.svg';
import Close from '../../assets/close.svg';
import Rupee2 from '../../assets/rupee-white.svg';
import PoDashBottom from './poDashBottom';
import PDashboardSite from '../../components/pDashboard/pDashboardSite';
import PDashboardArticle from '../../components/pDashboard/pDashboardArticle';
import { DatePicker } from 'antd';
import moment from 'moment';
const { RangePicker } = DatePicker;
import { OrderedMap } from "immutable";
import PDashboardVendor from './pDashboardVendor';
import EmailReport from './emailReport';
import FilterLoader from "../loaders/filterLoader";
import DownloadReport from './downloadReport';


const TRACK_DATA = new OrderedMap({
    "Division": {
        key: "Division",
        label: "HL1GET",
    },
    "Section": {
        key: "Section",
        label: "HL2GET",
    },
    "Department": {
        key: "Department",
        label: "HL3GET",
    },
    "Article": {
        key: "Article",
        label: "HL4GET",
    }
})

const dateFormat = 'YYYY/MM/DD';
const currentData = new Date();
const currFormatDate = moment(new Date).format('YYYY-MM-DD')
const oneYearFromNow = moment(currentData.setFullYear(currentData.getFullYear() - 1)).format('YYYY-MM-DD');

export default class ProcurementDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expiryDate: '',
            dReport: false,
            tagState: true,
            checkedFilters: [],
            filteredValue: {},
            openFilter: false,
            dashboardType: "pi",
            currentActive: "Division",
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            selectSite: false,
            hierarchyDataPi: [],
            hierarchyDataPo: [],
            siteDetails: { "siteCode": "", "siteName": "" },
            vendorDetails: { "vendorCode": "", "vendorName": "" },
            hierarchyDropHandle: false,
            type: 1,
            no: 1,
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            search: "",
            startDate: "",
            endDate: "",
            selectVendor: false,
            emailReport: false,
            isApplyFilter: false
        },
            this.showDropDownSite = this.showDropDownSite.bind(this);
        this.closeDropDownSite = this.closeDropDownSite.bind(this);
    }

    closeReport = (e) =>{
        this.setState({
            dReport: false,
        })
    }

    openReport = (e) =>{
        this.setState({
            dReport: true,
        })
    }

    showDropDownSite(event) {
        event.preventDefault();
        this.setState({ selectSite: true }, () => {
            document.addEventListener('click', this.closeDropDownSite);
        });
    }

    closeDropDownSite() {
        this.setState({ selectSite: false }, () => {
            document.removeEventListener('click', this.closeDropDownSite);
        });
    }

    showDropDownVendor = (event) => {
        console.log('trigger')
        event.preventDefault();
        this.setState({ selectVendor: true }, () => {
            document.addEventListener('click', this.closeDropDownVendor);
        })
    }

    closeDropDownVendor = () => {
        this.setState({ selectVendor: false }, () => {
            document.removeEventListener('click', this.closeDropDownVendor)
        })
    }

    showDropDownHierarchy = (e) => {
        e.preventDefault();
        this.setState({
            hierarchyDropHandle: true,
        }, () => {
            document.addEventListener('click', this.closeDropDownHierarchy)
        })
    }

    closeDropDownHierarchy = () => {
        this.setState({ hierarchyDropHandle: false }, () => {
            document.removeEventListener('click', this.closeDropDownHierarchy)
        })
    }

    openEmailReport(e) {
        e.preventDefault();
        this.setState({
            emailReport: !this.state.emailReport
        });
        //  () => document.addEventListener('click', this.closeEmailReport));
    }
    CloseEmailReport = () => {
        this.setState({
            emailReport: false,
            // document.removeEventListener('click', this.closeEmailReport);
        });
    }

    openCloseSideFilter() {
        this.setState({ openFilter: !this.state.openFilter });
    }
    changeDashBot = () => {
        this.setState({
            expiryDate:'',
            dashboardType: this.state.dashboardType == "pi" ? "po" : "pi",
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            startDate: "",
            endDate: "",
            siteDetails: { siteName: '', siteCode: '' },
            vendorDetails: { vendorName: '', vendorCode: '' },
            checkedFilters: [],
            isApplyFilter: false,
        })
    }
    hanldeSearch = (e) => {
        let dName = e.target.dataset.name

        this.setState({
            isApplyFilter: true,
            hierarchy: {
                ...this.state.hierarchy, [dName]: {
                    name: e.target.value,
                    code: ""
                }
            },
            search: e.target.value
        })
    }

    handleSearchInput = (e) => {
        let id = e.target.id;
        if (id == 'site') {
            this.setState({
                isApplyFilter: true,
                siteDetails: {
                    siteCode: '',
                    siteName: e.target.value
                },
                search: e.target.value
            })
        }
        if (id == 'vendor') {
            this.setState({
                isApplyFilter: true,
                vendorDetails: {
                    vendorCode: '',
                    vendorName: e.target.value
                },
                search: e.target.value
            })
        }
    }
    setDropData = (e) => {
        let { startDate, endDate } = this.state
        let payload = {}
        let dName = e.target.dataset.name
        if (e.keyCode == 13 || e.target.dataset.id == "openHierarchy") {
            this.setState({ currentActive: dName, hierarchyDropHandle: true })
            this.showDropDownHierarchy(e);
            payload = {
                "type": this.state.search.length === 0 ? 1 : 3,
                "pageNo": 1,
                "isPO": this.state.dashboardType === "pi" ? false : true,
                "hl1Name": this.state.search.length === 0 ? this.state.hierarchy["Division"].name : "",
                "hl2Name": this.state.search.length === 0 ? this.state.hierarchy["Section"].name : "",
                "hl3Name": this.state.search.length === 0 ? this.state.hierarchy["Department"].name : "",
                "hl4Name": this.state.search.length === 0 ? this.state.hierarchy["Article"].name : "",
                "fromDate": startDate,
                "toDate": endDate,
                "search": this.state.search,
                "siteCode": this.state.siteDetails.siteCode || "",
                "slCode": this.state.vendorDetails.vendorCode || '',
                "getBy": TRACK_DATA.get(dName).label
            }
            // if (this.state.dashboardType === "pi") {
            this.props.piDashHierarchyRequest(payload)

        }
    }
    siteData = (e) => {
        let payload = {
            isPO: this.state.dashboardType === "pi" ? false : true,
            fromDate: '',
            toDate: '',
        }
        if (e.keyCode == 13 || e.target.id == "openSite") {
            this.setState({
                loader: true,
            })
            this.props.piPoDashSiteRequest(payload)
            this.showDropDownSite(e);
        } else if (e.keyCode === 27) {
            this.setState({ selectSite: false })
        }
    }
    vendorData = (e) => {
        let payload = {
            isPO: this.state.dashboardType === "pi" ? false : true,
            fromDate: '',
            toDate: '',
            type: this.state.search.length == 0 ? 1 : 3,
            pageNo: 1,
            search: this.state.search,
            siteCode: this.state.siteDetails.siteCode || "",
        }
        if (e.keyCode == 13 || e.target.id == "openVendor") {
            this.setState({
                isApplyFilter: true,
                loader: true
            })
            this.props.getSlRequest(payload)
            this.showDropDownVendor(e);
        } else if (e.keyCode === 27) {
            this.setState({ selectVendor: false })
        }
    }
    slRequest = (e, page) => {
        this.setState({
            loader: true,
            selectVendor: true,
        })
        let payload = {
            isPO: this.state.dashboardType === "pi" ? false : true,
            fromDate: '',
            toDate: '',
            type: this.state.search.length == 0 ? 1 : 3,
            pageNo: page,
            search: this.state.search,
            siteCode: this.state.siteDetails.siteCode ? this.state.siteDetails.siteCode : "",
        }
        setTimeout((e) => this.showDropDownVendor(e), 50, e);
        this.props.getSlRequest(payload)
    }

    handleSetModal = () => {
        this.setState({ selectSite: !this.state.selectSite })
    }
    handleVendorModal = () => {
        this.setState({ selectVendor: !this.state.selectVendor })
    }
    handleHierarchyModal = () => {
        this.setState({ hierarchyDropHandle: !this.state.hierarchyDropHandle })
    }
    handleFilter = (date) => {
        this.setState({ startDate: moment(date[0]['_d']).format('YYYY-MM-DD'), endDate: moment(date[1]['_d']).format('YYYY-MM-DD'), isApplyFilter: true })
    }
    selectedSite = (data) => {
        this.setState({
            search: "",
            siteDetails: { ...this.state.siteDetails, siteCode: data.SiteCode, siteName: data.SiteName },
            selectSite: false,
            isApplyFilter: true
        })
    }

    selectedVendor = (data) => {
        this.setState({
            search: "",
            vendorDetails: { ...this.state.vendorDetails, vendorCode: data.slCode, vendorName: data.slName },
            selectVendor: false,
            isApplyFilter: true
        })
    }
    setHierarchySelectedData = (data) => {
        this.setState({
            search: "",
            hierarchy: { ...this.state.hierarchy, Division: { name: data.hl1Name, code: data.hl1Code } || "", Section: { name: data.hl2Name, code: data.hl2Code } || "", Department: { name: data.hl3Name, code: data.hl3Code } || "", Article: { name: data.hl4Name, code: data.hl4Code } || "" },
            hierarchyDropHandle: false,
            isApplyFilter: true
        })
    }
    page = (e) => {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.piDashHierarchy.data.prePage,
                current: this.props.purchaseIndent.piDashHierarchy.data.currPage,
                next: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piDashHierarchy.data.maxPage,
            })
            if (this.props.purchaseIndent.piDashHierarchy.data.prePage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.piDashHierarchy.data.prePage,
                    isPO: this.state.dashboardType === "pi" ? false : true
                }
                this.props.piDashHierarchyRequest(data)
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.piDashHierarchy.data.prePage,
                current: this.props.purchaseIndent.piDashHierarchy.data.currPage,
                next: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piDashHierarchy.data.maxPage,
            })
            if (this.props.purchaseIndent.piDashHierarchy.data.currPage != this.props.purchaseIndent.piDashHierarchy.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
                    isPO: this.state.dashboardType === "pi" ? false : true
                }
                this.props.piDashHierarchyRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.piDashHierarchy.data.prePage,
                current: this.props.purchaseIndent.piDashHierarchy.data.currPage,
                next: this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piDashHierarchy.data.maxPage,
            })
            if (this.props.purchaseIndent.piDashHierarchy.data.currPage <= this.props.purchaseIndent.piDashHierarchy.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    isPO: this.state.dashboardType === "pi" ? false : true
                }
                this.props.piDashHierarchyRequest(data)
            }
        }
    }
    applyFilter = () => {
        let { startDate, endDate } = this.state
        let payload = {
            isPO: this.state.dashboardType === "pi" ? false : true,
            fromDate: startDate,
            toDate: endDate,
            expireDays: "",
            siteCode: this.state.siteDetails.siteCode || '',
            slCode: this.state.vendorDetails.vendorCode || '',
            hl1Code: this.state.hierarchy["Division"].code,
            hl2Code: this.state.hierarchy["Section"].code,
            hl3Code: this.state.hierarchy["Department"].code,
            hl4Code: this.state.hierarchy["Article"].code
        }
        console.log('apply', this.state.vendorDetails.vendorCode)
        this.props.piDashBottomRequest(payload);
        let array = [];
        if (this.state.siteDetails.siteName) {
            let value = 'Site Name'
            array.push(value)
        }
        if (this.state.vendorDetails.vendorName) {
            let value = 'Vendor Name'
            array.push(value)
        }
        if (this.state.startDate && this.state.endDate) {
            let value = this.state.dashboardType === "pi" ? 'Indent Date' : 'Order Date'
            array.push(value)
        }
        if (this.state.hierarchy['Division'].name) {
            let value = 'Division'
            array.push(value)
        }

        if (this.state.hierarchy['Section'].name) {
            let value = 'Section'
            array.push(value)
        }
        if (this.state.hierarchy['Department'].name) {
            let value = 'Department'
            array.push(value)
        }
        if (this.state.hierarchy['Article'].name) {
            let value = 'Article'
            array.push(value)
        }
        this.setState({
            checkedFilters: array
        })
    }

    poExpiryDate = (days) =>{
        console.log(days)
        this.setState({
            expiryDate: days
        })
    }

    clearTag = (e, index) => {
        this.clearForm(this.state.checkedFilters[index]);
        let deleteItem = this.state.checkedFilters;
        deleteItem.splice(index, 1)
        if (deleteItem.length == 0) {
            this.setState({
                isApplyFilter: false,
            })
        }
        this.setState({
            checkedFilters: deleteItem
        })

    }

    clearForm = (item) => {
        if (item == 'Indent Date' || item == 'Order Date') {
            this.setState({
                endDate: '',
                startDate: '',
            })
            setTimeout(() => {
                this.getBottomVal();
            }, 500)
        }
        if (item == 'Site Name') {
            this.setState({
                siteDetails: {
                    siteName: '',
                    siteCode: ''
                }
            })
            setTimeout(() => {
                this.getBottomVal();
            }, 500)
        }
        if (item == 'Vendor Name') {
            this.setState({
                vendorDetails: {
                    vendorName: '',
                    vendorCode: ''
                }
            })
            setTimeout(() => {
                this.getBottomVal();
            }, 500)
        }
        if (item == 'Division' || item == 'Section' || item == 'Department' || item == 'Article') {
            let clearVal = { ...this.state.hierarchy };
            Object.keys(this.state.hierarchy).map(key => {
                clearVal[item].name = '';
                clearVal[item].code = '';
            })
            this.setState({
                hierarchy: clearVal
            })
            setTimeout(() => {
                this.getBottomVal();
            }, 500)
        }
    }

    getBottomVal = () => {
        let { startDate, endDate } = this.state
        let payload = {
            isPO: this.state.dashboardType === "pi" ? false : true,
            fromDate: startDate,
            toDate: endDate,
            expireDays: "",
            siteCode: this.state.siteDetails.siteCode || '',
            slCode: this.state.vendorDetails.vendorCode || '',
            hl1Code: this.state.hierarchy["Division"].code,
            hl2Code: this.state.hierarchy["Section"].code,
            hl3Code: this.state.hierarchy["Department"].code,
            hl4Code: this.state.hierarchy["Article"].code
        }
        this.props.piDashBottomRequest(payload);
    }

    clearFilter = () => {
        let { startDate, endDate } = this.state
        let payload = {
            isPO: this.state.dashboardType === "pi" ? false : true,
            fromDate: '',
            toDate: '',
            expireDays: "",
            siteCode: "",
            hl1Code: "",
            hl2Code: "",
            hl3Code: "",
            hl4Code: ""
        }
        this.props.piDashBottomRequest(payload)
        this.setState({
            hierarchy: { "Division": { name: "", code: "" }, "Section": { name: "", code: "" }, "Department": { name: "", code: "" }, "Article": { name: "", code: "" } },
            startDate: "",
            endDate: "",
            siteDetails: { siteName: '', siteCode: '' },
            vendorDetails: { vendorName: '', vendorCode: '' },
            checkedFilters: [],
            isApplyFilter: false,
        })
    }

    page = (e) => {
        console.log('page clicked', e.target.id)
        let { startDate, endDate } = this.state
        let payload = {}
        if (e.target.id == "prev") {
            this.setState({ hierarchyDropHandle: true })
            payload = {
                "type": this.state.search.length === 0 ? 1 : 3,
                "pageNo": this.props.purchaseIndent.piDashHierarchy.data.currPage - 1,
                "isPO": this.state.dashboardType === "pi" ? false : true,
                "hl1Name": this.state.search.length === 0 ? this.state.hierarchy["Division"].name : "",
                "hl2Name": this.state.search.length === 0 ? this.state.hierarchy["Section"].name : "",
                "hl3Name": this.state.search.length === 0 ? this.state.hierarchy["Department"].name : "",
                "hl4Name": this.state.search.length === 0 ? this.state.hierarchy["Article"].name : "",
                "fromDate": startDate,
                "toDate": endDate,
                "search": this.state.search,
                "siteCode": this.state.siteDetails.siteCode || "",
                "slCode": this.state.vendorDetails.vendorCode || '',
                "getBy": TRACK_DATA.get(this.state.currentActive).label
            }
            // if (this.state.dashboardType === "pi") {
            this.props.piDashHierarchyRequest(payload)

        }
        if (e.target.id == "next") {
            this.setState({ hierarchyDropHandle: true })
            payload = {
                "type": this.state.search.length === 0 ? 1 : 3,
                "pageNo": this.props.purchaseIndent.piDashHierarchy.data.currPage + 1,
                "isPO": this.state.dashboardType === "pi" ? false : true,
                "hl1Name": this.state.search.length === 0 ? this.state.hierarchy["Division"].name : "",
                "hl2Name": this.state.search.length === 0 ? this.state.hierarchy["Section"].name : "",
                "hl3Name": this.state.search.length === 0 ? this.state.hierarchy["Department"].name : "",
                "hl4Name": this.state.search.length === 0 ? this.state.hierarchy["Article"].name : "",
                "fromDate": startDate,
                "toDate": endDate,
                "search": this.state.search,
                "siteCode": this.state.siteDetails.siteCode || "",
                "slCode": this.state.vendorDetails.vendorCode || '',
                "getBy": TRACK_DATA.get(this.state.currentActive).label
            }
            // if (this.state.dashboardType === "pi") {
            this.props.piDashHierarchyRequest(payload)

        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.purchaseIndent.exportDashboardExcel.isSuccess){
           
            if(nextProps.purchaseIndent.exportDashboardExcel.data.resource != null){
                var a = document.createElement('a');
                a.target = '_blank';
                a.href = nextProps.purchaseIndent.exportDashboardExcel.data.resource;
                a.innerText = '';
                a.download = true;
                document.body.appendChild(a);
                a.click();            
            } else {
                alert('pdf is not generated yet!')
            }
            nextProps.exportDashboardExcelClear();
        }
        if (nextProps.replenishment.getHeaderConfigExcel.isSuccess || nextProps.purchaseIndent.exportDashboardExcel.isSuccess){
            return {
                loader: false,
            }
        }
        if (nextProps.replenishment.getHeaderConfigExcel.isLoading || nextProps.purchaseIndent.exportDashboardExcel.isLoading){
            return {
                // loader: true,
            }
        }
        if (nextProps.purchaseIndent.exportDashboardExcel.isError  || nextProps.replenishment.getHeaderConfigExcel.isError){
            return {
                loader: false
            }
        }
        if (nextProps.purchaseIndent.slData.isSuccess) {

            if (nextProps.purchaseIndent.slData.data.resource != null) {
                return {
                    loader: false,
                }
            }
        }
        if (nextProps.purchaseIndent.piPoDashSite.isSuccess) {

            if (nextProps.purchaseIndent.piPoDashSite.data.resource != null) {
                return {
                    loader: false,
                }
            }
        }
    }


    render() {
        const { hierarchy, siteDetails, hierarchyDropHandle, vendorDetails } = this.state;
        let createdTime = this.state.startDate ? this.state.dashboardType == 'pi' ? { indentDate: this.state.startDate + '|' + this.state.endDate } : { createdTime: this.state.startDate + '|' + this.state.endDate } : null;
        let site = siteDetails.siteName ? { siteName: siteDetails.siteName } : null;
        let vendor = vendorDetails.vendorName ? { slName: vendorDetails.vendorName } : null;
        let hl1Name = this.state.hierarchy["Division"].name ? { hl1Name: this.state.hierarchy["Division"].name } : null;
        let hl2Name = this.state.hierarchy["Section"].name ? { hl2Name: this.state.hierarchy["Section"].name } : null;
        let hl3Name = this.state.hierarchy["Department"].name ? { hl3Name: this.state.hierarchy["Department"].name } : null;
        let hl4Name = this.state.hierarchy["Article"].name ? { hl4Name: this.state.hierarchy["Article"].name } : null;
        let filter = {
            ...createdTime,
            ...site,
            ...vendor,

        }
        let filterItems = {
            ...hl1Name,
            ...hl2Name,
            ...hl3Name,
            ...hl4Name,
        }
        return (
            <div className="container-fluid p-lr-0 pad-l50">
                <div className="container-fluid pad-0 ">
                    <div className="col-lg-12 p-lr-47">
                        <div className="pdfilters">
                            <div className="pdf-top">
                                <div className="pdft-left">
                                    <div className="nph-switch-btn">
                                        <label className="tg-switch" >
                                            <input type="checkbox" checked={this.state.dashboardType == "pi"} onChange={(e) => this.changeDashBot(e)} />
                                            <span className="tg-slider tg-round"></span>
                                            {this.state.dashboardType == "pi" ?
                                                <span className="nph-wbtext nph-wbtext-left">PI</span> :
                                                <span className="nph-wbtext">PO</span>}
                                        </label>
                                    </div>
                                    <button type="button" className="" onClick={(e) => this.openEmailReport(e)}><img src={require('../../assets/mail.svg')} /></button>
                                    <button type="button" className="" onClick={(e) => this.openReport(e)}><img src={require('../../assets/dashBDownload.svg')} />
                                        <span className="generic-tooltip">Download Excel</span></button>
                                </div>
                                <div className="pdft-right">
                                    {this.state.isApplyFilter ?
                                        <button className="apply-filter-btn " type="button" onClick={this.applyFilter}>Apply Filter</button>
                                        :
                                        <button className="apply-filter-btn btnDisabled" type="button">Apply Filter</button>
                                    }
                                    <button type="button" onClick={this.clearFilter}>Clear</button>
                                </div>
                            </div>
                            <div className="pdfilters-inner">
                                <div className="pdfilters-inner-filter-heading">
                                    <h3>Filters</h3>
                                </div>
                                {/* <div className="pdfif-no-filter"><span>No Filters</span></div> */}
                                <div className="col-lg-12 p-lr-47">
                                    {this.state.tagState ? this.state.checkedFilters.map((keys, index) => (
                                        <div className="show-applied-filter">
                                            {/* {index === 0 ? <button type="button" className="saf-clear-all" onClick={(e) => this.clearAllTag(e)}>Clear All</button> : null} */}
                                            <button type="button" className="saf-btn">{keys}
                                                <img onClick={(e) => this.clearTag(e, index)} src={require('../../assets/clearSearch.svg')} />
                                                {/* <span className="generic-tooltip">{Object.values(this.state.filteredValue)[index]}</span> */}
                                                {/* <span className="generic-tooltip">{typeof (Object.values(this.state.filteredValue)[index]) == 'object' ? Object.values(Object.values(this.state.filteredValue)[index]).join(',') : Object.values(this.state.filteredValue)[index]}</span> */}
                                            </button>
                                        </div>)) : ''}
                                </div>
                            </div>
                            <form>
                                <div className="col-lg-2 pad-0 pad-r15">
                                    <p>Date Range wise</p>
                                    <div className="col-lg-12 pad-0 pad-r15">
                                        <label className="pd-filter-label">Date Range</label>
                                        {/* <input type="date" name="date" className="pd-date-input" /> */}
                                        <RangePicker picker="date" id="rangePicker" onChange={(e) => this.handleFilter(e)}
                                            defaultValue={[this.state.startDate, this.state.endDate]}
                                            format={dateFormat}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-10 pad-0">
                                    <p>Category wise</p>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" htmlFor="pddivision">Site</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" onKeyDown={this.siteData} value={siteDetails.siteName} className="onFocus pnl-purchase-input" placeholder="Choose Site" id='site' onChange={this.handleSearchInput} />
                                            <span className="modal-search-btn" onClick={this.siteData}>
                                                <svg xmlns="http://www.w3.org/2000/svg" id="openSite" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.selectSite && <PDashboardSite selectedSite={this.selectedSite} siteDetails={this.props.purchaseIndent.piPoDashSite.data.resource || []} handleSetModal={this.handleSetModal} />}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" fillRule="pddivision">Vendor</label>
                                        <div className="inputTextKeyFucMain set-vend-pos">
                                            <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Vendor" value={this.state.vendorDetails.vendorName} onKeyDown={this.vendorData} id='vendor' onChange={this.handleSearchInput} />
                                            <span className="modal-search-btn" onClick={this.vendorData}>
                                                <svg xmlns="http://www.w3.org/2000/svg" id="openVendor" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {this.state.selectVendor && <PDashboardVendor selectedVendor={this.selectedVendor} vendorDetails={this.props.purchaseIndent.slData.data.resource || []} pageDetails={this.props.purchaseIndent.slData.data || []} slRequest={(e, page) => this.slRequest(e, page)} handleVendorModal={this.handleVendorModal} />}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" fillRule="pddivision">Division</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input divi" data-name="Division" value={hierarchy["Division"].name} onChange={this.hanldeSearch} onKeyDown={this.setDropData} placeholder="Choose Division" />
                                            <span className="modal-search-btn" onClick={this.setDropData}>
                                                <svg xmlns="http://www.w3.org/2000/svg" data-name="Division" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {hierarchyDropHandle && <PDashboardArticle  {...this.state} page={(e) => this.page(e)} setHierarchySelectedData={this.setHierarchySelectedData} handleHierarchyModal={this.handleHierarchyModal} currentActive={this.state.currentActive} pageDetails={this.props.purchaseIndent.piDashHierarchy.data || []} hierarchyData={this.props.purchaseIndent.piDashHierarchy.data.resource || this.props.purchaseIndent.piDashHierarchy.data.resource || []} />}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" fillRule="pddivision">Section</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" data-name="Section" value={hierarchy["Section"].name} onChange={this.hanldeSearch} onKeyDown={this.setDropData} placeholder="Choose Section " />
                                            <span className="modal-search-btn" onClick={this.setDropData}>
                                                <svg xmlns="http://www.w3.org/2000/svg" data-name="Section" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {/* <PDashboardArticle /> */}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" fillRule="pddivision">Department</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" data-name="Department" value={hierarchy["Department"].name} onChange={this.hanldeSearch} onKeyDown={this.setDropData} placeholder="Choose Department" />
                                            <span className="modal-search-btn" onClick={this.setDropData}>
                                                <svg xmlns="http://www.w3.org/2000/svg" data-name="Department" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {/* <PDashboardArticle /> */}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label className="pd-filter-label" fillRule="pddivision">Article</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" data-name="Article" value={hierarchy["Article"].name} onChange={this.hanldeSearch} onKeyDown={this.setDropData} placeholder="Choose Article" />
                                            <span className="modal-search-btn" onClick={this.setDropData}>
                                                <svg xmlns="http://www.w3.org/2000/svg" data-name="Article" data-id="openHierarchy" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                            {/* <PDashboardArticle /> */}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="col-lg-12 pad-0">
                        <div className="procurement-dashboard-tab">
                            <ul className="nav nav-tabs pdt-list" role="tablist">
                                <li className="nav-item active">
                                    <a className="nav-link pdt-list-btn active" href="#actionableitems" role="tab" data-toggle="tab">Actionable Items</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link pdt-list-btn" href="#analyticsitems" role="tab" data-toggle="tab">Analytics Items</a>
                                </li>
                            </ul>
                        </div>
                        <div className="tab-content pd-tab-content">
                            <div role="tabpanel" className="tab-pane fade pd-tab-actionable-items in active" id="actionableitems">
                                {this.state.dashboardType == "pi" ? <PiDashBottom {...this.props} filter={filter} filterItems={filterItems} piCards={this.props.purchaseIndent.piDashBottom.data.resource != undefined ? this.props.purchaseIndent.piDashBottom.data.resource : {}} /> : <PoDashBottom  {...this.props} filter={filter} filterItems={filterItems} poCards={this.props.purchaseIndent.piDashBottom.data.resource != undefined ? this.props.purchaseIndent.piDashBottom.data.resource : {}} poExpiryDate={this.poExpiryDate}/>}
                            </div>
                            <div role="tabpanel" className="tab-pane fade pd-tab-actionable-items" id="analyticsitems">
                                <div className="col-lg-12 m-top-20 pd-item-details">
                                    <div className="col-lg-2">
                                        <div className="pd-hdetails pahd-manage">
                                            <div className="pd-hdetails-top">
                                                <span>0</span>
                                            </div>
                                            <div className="pd-hdetails-bot">
                                                <p>Monthly Number of PO</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2">
                                        <div className="pd-hdetails pahd-manage">
                                            <div className="pd-hdetails-top">
                                                <span><img src={Rupee}></img>0</span>
                                            </div>
                                            <div className="pd-hdetails-bot">
                                                <p>Monthly Buying Value</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2">
                                        <div className="pd-hdetails pahd-manage">
                                            <div className="pd-hdetails-top">
                                                <span>0</span>
                                            </div>
                                            <div className="pd-hdetails-bot">
                                                <p>Monthly PO Cancellation</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2">
                                        <div className="pd-hdetails pahd-manage">
                                            <div className="pd-hdetails-top">
                                                <span>0</span>
                                            </div>
                                            <div className="pd-hdetails-bot">
                                                <p>No. Of Suppliers</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2">
                                        <div className="pd-hdetails pahd-manage">
                                            <div className="pd-hdetails-top">
                                                <span>0</span>
                                                <span className="pd-ht-info-icon"><img src={InfoIcon} /></span>
                                            </div>
                                            <div className="pd-hdetails-bot">
                                                <p>Total No. of PO</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2">
                                        <div className="pd-hdetails pahd-manage">
                                            <div className="pd-hdetails-top">
                                                <span>0</span>
                                                <span className="pd-ht-info-icon"><img src={InfoIcon} /></span>
                                            </div>
                                            <div className="pd-hdetails-bot">
                                                <p>PO Amount</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-12 m-top-20">
                                    <div className="col-lg-12">
                                        <div className="pd-content-head">
                                            <div className="pd-top5-articles">
                                                <span>Top 5 Articles</span>
                                            </div>
                                            <ul className="nav nav-tabs pd-tb-list" role="tablist">
                                                <li className="nav-item active">
                                                    <a className="nav-link pd-table-tab-btn active" href="#pdartb5" role="tab" data-toggle="tab">Bottom 5</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link pd-table-tab-btn" href="#pdartt5" role="tab" data-toggle="tab">Top 5</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-content pd-articles-tab">
                                            <div role="tabpanel" className="tab-pane fade pdaatab in active" id="pdartb5">
                                                <table className="pd-tables pd-articles-b5">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Articles Name</label></th>
                                                            <th><label>Amount</label></th>
                                                            <th><label>Units</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label >0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade pdaatab" id="pdartt5">
                                                <table className="pd-tables pd-articles-t5">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Articles Name</label></th>
                                                            <th><label>Amount</label></th>
                                                            <th><label>Units</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label >0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>White-Shirt-xl-men</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="pd-content-head">
                                            <div className="pd-top5-articles">
                                                <span>Top 5 Suppliers</span>
                                            </div>
                                            <ul className="nav nav-tabs pd-tb-list" role="tablist">
                                                <li className="nav-item active">
                                                    <a className="nav-link pd-table-tab-btn active" href="#pdsupb5" role="tab" data-toggle="tab">Bottom 5</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link pd-table-tab-btn" href="#pdsupt5" role="tab" data-toggle="tab">Top 5</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-content pd-articles-tab">
                                            <div role="tabpanel" className="tab-pane fade pdaatab in active" id="pdsupb5">
                                                <table className="pd-tables pd-supplier-b5">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Supplier Name</label></th>
                                                            <th><label>Amount</label></th>
                                                            <th><label>Units</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade pdaatab" id="pdsupt5">
                                                <table className="pd-tables pd-supplier-t5">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Supplier Name</label></th>
                                                            <th><label>Amount</label></th>
                                                            <th><label>Units</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>GV Enterprises</label></td>
                                                            <td className="pd-tables-amount"><label className="clr-red">0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-sm-12 col-md-12">
                                    <div className="col-lg-12">
                                        <div className="pd-graphic-data-head">
                                            <span className="pd-item">
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </span>
                                            <span className="pd-gd-text">Tabular Data</span>
                                        </div>
                                        <div className="pd-gd-head-item">
                                            <ul className="nav nav-tabs pd-tb-list" role="tablist">
                                                <li className="nav-item m-r-15 active">
                                                    <a className="nav-link pd-table-tab-btn active" href="#pdspendanalysis" role="tab" data-toggle="tab">Spend Analysis</a>
                                                </li>
                                                <li className="nav-item m-r-15">
                                                    <a className="nav-link pd-table-tab-btn" href="#pdlastyearcomp" role="tab" data-toggle="tab">Last Year Comparison</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link pd-table-tab-btn" href="#pdfillrate" role="tab" data-toggle="tab">Fill Rate</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-content pd-articles-tab">
                                            <div role="tabpanel" className="tab-pane fade pdaatab in active" id="pdspendanalysis">
                                                <table className="pd-tables pd-tabular-data">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Month Name</label></th>
                                                            <th><label>PO Amount</label></th>
                                                            <th><label>OTB</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>January 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>February 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>March 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>April 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>May 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>June 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>July 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>August 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>September 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>October 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade pdaatab" id="pdlastyearcomp">
                                                <table className="pd-tables pd-tabular-data">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Month Name</label></th>
                                                            <th><label>PO Amount</label></th>
                                                            <th><label>OTB</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>January 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>February 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>March 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>April 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>May 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>June 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>July 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>August 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>September 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>October 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade pdaatab" id="pdfillrate">
                                                <table className="pd-tables pd-tabular-data">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Month Name</label></th>
                                                            <th><label>PO Amount</label></th>
                                                            <th><label>OTB</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>January 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>February 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>March 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>April 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>May 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>June 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>July 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>August 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>September 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>October 2019</label></td>
                                                            <td className="pd-tables-amount"><label>0</label></td>
                                                            <td><label>0</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.emailReport && <EmailReport CloseEmailReport={this.CloseEmailReport}  {...this.state} {...this.props} poCards={this.props.purchaseIndent.piDashBottom.data.resource != undefined ? this.props.purchaseIndent.piDashBottom.data.resource : {}}/>}
                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.dReport ?  <DownloadReport {...this.state} {...this.props} closeReport={this.closeReport}/> : null}
                </div>
            </div>

        )
    }
}
