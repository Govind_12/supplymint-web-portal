import React from "react";
import { orderMap, OrderedMap } from 'immutable';

const HIERARCHY_Level_DATA = new OrderedMap({
    "Division": {
        headers: [{ key: 'hl1Name', label: "Division" }],
        body: [{ key: 'hl1Name', label: "Division" }]
    },
    "Section": {
        headers: [{ key: 'hl1Name', label: "Division" }, { key: "hl2Name", label: "Section" }],
        body: [{ key: 'hl1Name', label: "Division" }, { key: "hl2Name", label: "Section" }]
    },
    "Department": {
        headers: [{ key: 'hl1Name', label: "Division" }, { key: "hl2Name", label: "Section" }, { key: "hl3Name", label: "Department" }],
        body: [{ key: 'hl1Name', label: "Division" }, { key: "hl2Name", label: "Section" }, { key: "hl3Name", label: "Department" }]

    },
    "Article": {
        headers: [{ key: 'hl1Name', label: "Division" }, { key: "hl2Name", label: "Section" }, { key: "hl3Name", label: "Department" }, { key: "hl4Name", label: "Article" }],
        body: [{ key: 'hl1Name', label: "Division" }, { key: "hl2Name", label: "Section" }, { key: "hl3Name", label: "Department" }, { key: "hl4Name", label: "Article" }]
    }
})

class PDashboardArticle extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        console.log(this.props.currentActive);
        return (
            <div className="dropdown-menu-city2 po-article-mod-dropdown posAbsolute" id="pocolorModel">
                <div className="dropdown-modal-header">
                    {HIERARCHY_Level_DATA.get(this.props.currentActive).headers.map((_) => (
                        <span>{_.label}</span>
                    ))}
                </div>

                <ul className="dropdown-menu-city-item">
                    {/* <li><span>No Data Found</span></li> */}
                    {this.props.hierarchyData.length ? this.props.hierarchyData.map((_) => (
                        <li onClick={() => this.props.setHierarchySelectedData(_)}>
                            <span className="vendor-details">
                                {HIERARCHY_Level_DATA.get(this.props.currentActive).body.map((__, k) => (
                                    <span className="vd-name div-col-7">{_[__.key]}</span>
                                ))}
                            </span>
                        </li>
                    )) : <li>No Data Found</li>}
                </ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={this.props.handleHierarchyModal}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        {this.props.pageDetails.currPage > 1 ? <button className="pnpb-prev" type="button" id="prev" onClick={(e)=>this.props.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>:
                        <button className="pnpb-prev" type="button" id="prev" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>}
                        <button className="pnpb-no" type="button">{this.props.pageDetails ? this.props.pageDetails.currPage: 0}/{this.props.pageDetails ? this.props.pageDetails.maxPage: 0}</button>
                        {this.props.pageDetails.currPage < this.props.pageDetails.maxPage ? <button className="pnpb-next" type="button" id="next" onClick={(e)=>this.props.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>:
                        <button className="pnpb-next" type="button" id="next" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                        </button>}
                    </div>
                </div>
            </div >
        )
    }
}

export default PDashboardArticle;