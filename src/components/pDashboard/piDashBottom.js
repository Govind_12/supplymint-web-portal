import React, { useEffect } from 'react';
import Rupee from '../../assets/rupee.svg';
import InfoWhite from '../../assets/infoWhite.svg';
import moment from 'moment';


const currentData = new Date();
const currFormatDate = moment(new Date).format('YYYY-MM-DD')
const oneYearFromNow = moment(currentData.setFullYear(currentData.getFullYear() - 1)).format('YYYY-MM-DD');
const PiDashBottom = (props) => {
    useEffect(() => {
        let payload = {
            isPO: false,
            fromDate: oneYearFromNow,
            toDate: currFormatDate,
            expireDays: "",
            siteCode: "",
            hl1Code: "",
            hl2Code: "",
            hl3Code: "",
            hl4Code: ""
        }
        let mainHeaderPayload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "PI_TABLE_HEADER",
            displayName: "TABLE HEADER"
        }
        console.log(payload)
        props.piDashBottomRequest(payload)
        props.getHeaderConfigRequest(mainHeaderPayload)
    }, [])
    console.log(props.piCards)

    return (
        <div>
            {Object.keys(props.piCards).length > 0 && <div className="col-lg-12 m-top-20 pd-item-details">
                <div className="col-lg-3">
                    <div className="pd-hdetails">
                        <div className="pd-hdetails-top">
                            <span>{props.piCards.totalPendingPI || 0}</span>
                        </div>
                        <div className="pd-hdetails-bot">
                            <p>PI Pending for Approval</p>
                            <span className="pdhb-amount"><span>&#8377;</span> {Number(props.piCards.pendingPOAmount).toFixed(2) || 0}</span>
                            <button onClick={() => props.history.push({ pathname: '/purchase/purchaseIndentHistory', status: "PENDING", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })}>View Indents <img src={require('../../assets/next-arrow1.svg')} /></button>
                        </div>
                    </div>
                </div>

                <div className="col-lg-3">
                    <div className="pd-hdetails">
                        <div className="pd-hdetails-top">
                            <span>{props.piCards.totalDraftedPI || 0}</span>
                        </div>
                        <div className="pd-hdetails-bot">
                            <p>PI Saved as Draft</p>
                            <span className="pdhb-amount"><span>&#8377;</span>{Number(props.piCards.draftedPOAmount).toFixed(2) || 0}</span>
                            <button onClick={() => props.history.push({ pathname: '/purchase/purchaseIndentHistory', status: "DRAFTED", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })}>View Indents <img src={require('../../assets/next-arrow1.svg')} /></button>

                        </div>
                    </div>
                </div>
                <div className="col-lg-3">
                    <div className="pd-hdetails">
                        <div className="pd-hdetails-top">
                            <span>{props.piCards.totalApprovedPI || 0}</span>
                        </div>
                        <div className="pd-hdetails-bot">
                            <p>PI Approved</p>
                            <span className="pdhb-amount"><span>&#8377;</span> {Number(props.piCards.approvedPOAmount).toFixed(2) || 0}</span>
                            <button onClick={() => props.history.push({ pathname: '/purchase/purchaseIndentHistory', status: "APPROVED", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })}>View Indents <img src={require('../../assets/next-arrow1.svg')} /></button>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3">
                    <div className="pd-hdetails">
                        <div className="pd-hdetails-top">
                            {/* <span><img src={Rupee}></img>0</span> */}
                            <span>{props.piCards.totalRejectedPI || 0}</span>
                        </div>
                        <div className="pd-hdetails-bot">
                            <p>PI Rejected</p>
                            <span className="pdhb-amount clr-red"><span>&#8377;</span> {Number(props.piCards.rejectedPOAmount).toFixed(2) || 0}</span>
                            <button onClick={() => props.history.push({ pathname: '/purchase/purchaseIndentHistory', status: "REJECTED", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })}>View Indents <img src={require('../../assets/next-arrow1.svg')} /></button>
                        </div>
                    </div>
                </div>
            </div>}
        </div>
    )
};

export default PiDashBottom