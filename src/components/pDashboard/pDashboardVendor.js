import React from "react";

class PDashboardVendor extends React.Component {
    render() {
        console.log('vendor', this.props.vendorDetails)
        return (
            <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                <div className="dropdown-modal-header">
                    <span className="vd-name div-col-2">Vendor</span>
                    {/* <span className="vd-loc div-col-2">Site</span> */}
                    {/* <span className="vd-num div-col-2">GST No</span> */}
                    <span className="vd-code div-col-2">Code</span>
                </div>

                <ul className="dropdown-menu-city-item">
                    {this.props.vendorDetails.length > 0 ? this.props.vendorDetails.map((_, k) => (
                        <li key={k} onClick={() => this.props.selectedVendor(_)}>
                            <span className="vendor-details">
                                <span className="vd-name div-col-2">{_.slName}</span>
                                <span className="vd-loc div-col-2">{_.slCode}</span>
                            </span>
                        </li>
                    )) : <li><span>No Data Found</span></li>}
                </ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={this.props.handleVendorModal}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">                        
                        {this.props.pageDetails.currPage > 1 ? <button className="pnpb-prev" type="button" id="prev" onClick={(e) => this.props.slRequest(e, this.props.pageDetails.currPage - 1)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button> :
                            <button className="pnpb-prev" type="button" id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg>
                            </button>}
                        <button className="pnpb-no" type="button">{this.props.pageDetails ? this.props.pageDetails.currPage : 0}/{this.props.pageDetails ? this.props.pageDetails.maxPage : 0}</button>
                        {this.props.pageDetails.currPage < this.props.pageDetails.maxPage ?
                            <button className="pnpb-next" type="button" id="next" onClick={(e) => this.props.slRequest(e, this.props.pageDetails.currPage + 1)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg>
                            </button> :
                            <button className="pnpb-next" type="button" id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                </svg>
                            </button>}
                    </div>
                </div>
            </div>
        )
    }
}

export default PDashboardVendor;