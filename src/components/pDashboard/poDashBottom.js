import React, { useEffect, useState } from 'react';
import Rupee from '../../assets/rupee.svg';
import InfoWhite from '../../assets/infoWhite.svg';
import moment from 'moment';

const currentData = new Date();
const currFormatDate = moment(new Date).format('YYYY-MM-DD')
const oneYearFromNow = moment(currentData.setFullYear(currentData.getFullYear() - 1)).format('YYYY-MM-DD');
const PoDashBottom = (props) => {
    const [number, setNumber] = useState('1')
    useEffect(() => {
        let payload = {
            isPO: true,
            fromDate: oneYearFromNow,
            toDate: currFormatDate,
            expireDays: "",
            siteCode: "",
            hl1Code: "",
            hl2Code: "",
            hl3Code: "",
            hl4Code: ""
        }
        let mainHeaderPayload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "PO_TABLE_HEADER",
            displayName: "TABLE HEADER"
        }
        props.getHeaderConfigRequest(mainHeaderPayload)
        props.piDashBottomRequest(payload)
    }, [])
    const changeNumber = (e) => {
        if (e.target.dataset.value !== 1) {
            let payload = {
                isPO: true,
                fromDate: oneYearFromNow,
                toDate: currFormatDate,
                expireDays: e.target.dataset.value,
                siteCode: "",
                hl1Code: "",
                hl2Code: "",
                hl3Code: "",
                hl4Code: ""
            }
            props.poExpiryDate(e.target.dataset.value)
            props.piDashBottomRequest(payload)
        }
        setNumber(e.target.dataset.value)
    }
    return (
        <div>
            {Object.keys(props.poCards).length > 0 && <div className="col-lg-12 m-top-20 pd-item-details">
                <div className="col-lg-7 pad-0">
                    <div className="col-lg-4">
                        <div className="pd-hdetails">
                            <div className="pd-hdetails-top">
                                <span>{props.poCards.totalPendingPO || 0}</span>
                            </div>
                            <div className="pd-hdetails-bot">
                                <p>PO Pending for Approval</p>
                                <span className="pdhb-amount"><span>&#8377;</span> {Number(props.poCards.pendingPOAmount).toFixed(2) || 0}</span>
                                <button onClick={() => props.history.push({ pathname: '/purchase/purchaseOrderHistory', status: "PENDING", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })} type="button">View Orders <img src={require('../../assets/next-arrow1.svg')} /></button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="pd-hdetails">
                            <div className="pd-hdetails-top">
                                {/* <span><img src={Rupee}></img>0</span> */}
                                <span>{props.poCards.totalDraftedPO || 0}</span>
                            </div>
                            <div className="pd-hdetails-bot">
                                <p>PO Saved as Draft</p>
                                <span className="pdhb-amount"><span>&#8377;</span>{Number(props.poCards.draftedPOAmount).toFixed(2) || 0}</span>
                                <button onClick={() => props.history.push({ pathname: '/purchase/purchaseOrderHistory', status: "DRAFTED", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })} type="button">View Orders <img src={require('../../assets/next-arrow1.svg')} /></button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="pd-hdetails">
                            <div className="pd-hdetails-top">
                                <span>{props.poCards.totalApprovedPO || 0}</span>
                            </div>
                            <div className="pd-hdetails-bot">
                                <p>PO Approved</p>
                                <span className="pdhb-amount"><span>&#8377;</span> {Number(props.poCards.approvedPOAmount).toFixed(2) || 0}</span>
                                <button onClick={() => props.history.push({ pathname: '/purchase/purchaseOrderHistory', status: "APPROVED", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })} type="button">View Orders <img src={require('../../assets/next-arrow1.svg')} /></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-5 pad-0">
                    <div className="col-lg-6">
                        <div className="pd-hdetails">
                            <div className="pd-hdetails-top">
                                <span>{props.poCards.totalRejectedPO || 0}</span>
                            </div>
                            <div className="pd-hdetails-bot">
                                <p>PO Rejected</p>
                                <span className="pdhb-amount clr-red"><span>&#8377;</span> {Number(props.poCards.rejectedPOAmount).toFixed(2) || 0}</span>
                                <button onClick={() => props.history.push({ pathname: '/purchase/purchaseOrderHistory', status: "REJECTED", type: 2, filter:{...props.filter}, filterItems:{...props.filterItems}, ...props })} type="button">View Orders <img src={require('../../assets/next-arrow1.svg')} /></button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="pd-hdetails bg-red">
                            <div className="pd-hdetails-top">
                                <span>{number == 1 ? props.poCards.expireToday || 0 : props.poCards.expireOnDays}</span>
                                <span className="pd-ht-info-icon"><img src={InfoWhite} /></span>
                            </div>
                            <div className="pd-hdetails-bot">
                                <p>PO Near Expiry</p>
                                <div className="vdhi-bottom">
                                    <span>PO Expires  {number == 1 ? "Today" : `in next ${number} Days`}  <i className="fa fa-chevron-down"></i>
                                        <ul className="vdhi-dropdown-menu">
                                            <li><span data-show="tenDay" data-value="10" onClick={changeNumber}>10 Days</span></li>
                                            <li><span data-show="sevenDay" data-value="7" onClick={changeNumber}>7 Days</span></li>
                                            <li><span data-show="fiveDay" data-value="5" onClick={changeNumber}>5 Days</span></li>
                                            <li><span data-show="threeDay" data-value="3" onClick={changeNumber}>3 Days</span></li>
                                            <li><span data-show="currentDay" data-value="1" onClick={changeNumber}>Today</span></li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <div className="col-lg-2">
                    <div className="pd-hdetails bg-orange">
                        <div className="pd-hdetails-top">
                            <span>0</span>
                            <span className="pd-ht-info-icon"><img src={InfoWhite} /></span>
                        </div>
                        <div className="pd-hdetails-bot">
                            <p>PO Expiry in 06 Days</p>
                        </div>
                    </div>
                </div> */}
            </div>}
        </div>
    )
}
export default PoDashBottom