import React from 'react';
import Pagination from '../pagination';
import LineItemData from './lineItemData';
import CircleTick from '../../assets/circle-white-tick.svg';

class ViewDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLineItem: false,
        }
    }
    viewLineItem(e) {
        e.preventDefault();
        this.setState({
            showLineItem: !this.showLineItem,
        });
    }
    closeLineItem() {
        this.setState({
            showLineItem: false
        })
    }

    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content line-item-view-data-modal view-details">
                    <div className="livdm-head">
                        <h3>Indent Details</h3>
                        <div className="livdmh-right">
                            <button type="button" className="gen-approved"><img src={CircleTick} />Approve</button>
                            <button type="button" className="gen-cancel">Reject</button>
                            <button type="button" className="livdmh-close" onClick={(e) => this.props.closeDetails(e)}><img src={require('../../assets/clearSearch.svg')} /></button>
                        </div>
                    </div>
                    <div className="vd-body">
                        <div className="vd-modal-table m-top-20">
                            <div className="vdmt-manage">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th className="fix-action-btn"><label></label></th>
                                            <th><label>Division</label></th>
                                            <th><label>Section</label></th>
                                            <th><label>Department</label></th>
                                            <th><label>Article Code</label></th>
                                            <th><label>Article Name</label></th>
                                            <th><label>HSN</label></th>
                                            <th><label>MRP range</label></th>
                                            <th><label>MRP</label></th>
                                            <th><label>Vendor Design No.</label></th>
                                            <th><label>RSP</label></th>
                                            <th><label>MRP</label></th>
                                            <th><label>Rate</label></th>
                                            <th><label>Discount</label></th>
                                            <th><label>Final rate</label></th>
                                            <th><label>OTB</label></th>
                                            <th><label>Delivery Date</label></th>
                                            <th><label>Margin Rule</label></th>
                                            <th><label>Remarks</label></th>
                                            <th><label>Total Quantity</label></th>
                                            <th><label>Basic</label></th>
                                            <th><label>Total Net Amount	</label></th>
                                            <th><label>Tax</label></th>
                                            <th><label>Calculated Margin</label></th>
                                            <th><label>GST</label></th>
                                            <th><label>Mark Up / Down</label></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                        <tr>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner tili-eye" onClick={(e) => this.viewLineItem(e)}>
                                                        <img src={require('../../assets/eye-open.svg')} />
                                                    </li>
                                                </ul>
                                            </td>
                                            <td className="disinput"><label>APPARELS</label></td>
                                            <td className="disinput"><label>MENS</label></td>
                                            <td className="disinput"><label>TOP</label></td>
                                            <td className="disinput"><label>4136</label></td>
                                            <td className="disinput"><label>JACKETS</label></td>
                                            <td className="disinput"><label>01102900</label></td>
                                            <td className="disinput"><label>1500 - 1999</label></td>
                                            <td className="disinput"><label>500</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>52</label></td>
                                            <td className="disinput"><label>200</label></td>
                                            <td className="disinput"><label>400</label></td>
                                            <td className="disinput"><label>5%</label></td>
                                            <td className="disinput"><label>0.00</label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label>12/01/2021</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label>0</label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                            <td className="disinput"><label></label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder" value="01" />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showLineItem && <LineItemData closeLineItem={(e) => this.closeLineItem(e)} />}
            </div>
        )
    }
}

export default ViewDetail;