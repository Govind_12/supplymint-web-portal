import React from "react";
import moment from "moment";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import PoHistoryFilter from "../piOrPoHistory/poHistoryFilter";
import ToastLoader from "../loaders/toastLoader";

class PoHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openRightBar: false,
            rightbar: false,
            poHistoryState: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1,
            no: 1,
            loader:false,
            success: false,
            alert: false,
            successMessage:"",
            errorMessage:"",
            errorCode:"",
            code:"",
            filter:false,
            filterBar:false,
            generatedDate :"",
            orderNo :"",
            validFrom :"",
            validTo:"",
            slCode :"",
            search : "",
            toastLoader:false,
            toastMsg:"",
            hl1Name:"",
            hl2Name:"",
            hl3Name:""

        }
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: true,
            filterBar:!this.state.filterBar
          
        });
    }

handleChange(e){
    if(e.target.id=="search"){
        this.setState({
            search:e.target.value
        })
    }
}

componentWillMount(){
    if (!this.props.purchaseIndent.poHistory.isSuccess) {
       let data={
           no:1,
           type:1,
           orderDate :"",
           orderNo :"",
           validFrom :"",
           validTo:"",
           slCode :"",
           slName:"",
           slCityName:"",
           search : "",
           hl1Name:"",
           hl2Name:"",
           hl3Name:""
        }
        this.props.poHistoryRequest(data);
        this.setState({
  
          loader: false
        })
      } else 
      if(this.props.purchaseIndent.poHistory.isSuccess){
        if(this.props.purchaseIndent.poHistory.data.resource!=null){
     
        this.setState({
            prev: this.props.purchaseIndent.poHistory.data.prePage,
        current: this.props.purchaseIndent.poHistory.data.currPage,
        next: this.props.purchaseIndent.poHistory.data.currPage + 1,
        maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
        loader:false    
        })
      }
    
      else{
        this.setState({
            poHistoryState: this.props.purchaseIndent.poHistory.data.resource,
            prev: this.props.purchaseIndent.poHistory.data.prePage,
        current: this.props.purchaseIndent.poHistory.data.currPage,
        next: this.props.purchaseIndent.poHistory.data.currPage + 1,
        maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
        loader:false    
        })
      }

    }
   }
   onRequest(e) {
    e.preventDefault();
    this.setState({
      success: false
    });
  }
  onError(e) {
    e.preventDefault();
    this.setState({
      alert: false
    });
    document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
  } 

  componentWillReceiveProps(nextProps){
    if(nextProps.purchaseIndent.poHistory.isLoading){
        this.setState({
            loader:true
        })
    }else if (nextProps.purchaseIndent.poHistory.isError) {
        this.setState({
          errorMessage:nextProps.purchaseIndent.poHistory.message.error == undefined ? undefined :nextProps.purchaseIndent.poHistory.message.error.errorMessage,
          errorCode:nextProps.purchaseIndent.poHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.poHistory.message.error.errorCode,
          code: nextProps.purchaseIndent.poHistory.message.status,
          alert: true,
       
          loader: false
        })
        this.props.poHistoryClear()
      } else if (nextProps.purchaseIndent.poHistory.isSuccess) {
      if(nextProps.purchaseIndent.poHistory.data.resource!=null){
     
           this.setState({                
               prev: nextProps.purchaseIndent.poHistory.data.prePage,
           current: nextProps.purchaseIndent.poHistory.data.currPage,
           next: nextProps.purchaseIndent.poHistory.data.currPage + 1,
           maxPage: nextProps.purchaseIndent.poHistory.data.maxPage,
           })
      }
      this.setState({
        loader: false,
        poHistoryState: nextProps.purchaseIndent.poHistory.data.resource,  
        prev: nextProps.purchaseIndent.poHistory.data.prePage,
    current: nextProps.purchaseIndent.poHistory.data.currPage,
    next: nextProps.purchaseIndent.poHistory.data.currPage + 1,
    maxPage: nextProps.purchaseIndent.poHistory.data.maxPage,
      })
      this.props.poHistoryClear()
    }
}

updateFilter(data) {
    this.setState({
        type: data.type,
        no: data.no,
        orderDate :data.orderDate,
           orderNo :data.orderNo,
           validFrom :data.validFrom,
           validTo:data.validTo,
           slCode :data.slCode,
            slName:data.slName,
            slCityName:data.slCityName,
           search : data.search,
           hl1Name:data.hl1Name,
           hl2Name:data.hl2Name,
           hl3Name:data.hl3Name,
    });
}

onClearSearch(e){
    e.preventDefault();
    this.setState({
        type: 1,
        no: 1,
        orderDate :"",
        orderNo :"",
        validFrom :"",
        validTo:"",
        slCode :"",
        slName:"",
        slCityName:"",
        search : ""
    })
    let data = {
        type: 1,
        no: 1,
        orderDate :"",
        orderNo :"",
        validFrom :"",
        validTo:"",
        slCode :"",
        slName:"",
        slCityName:"",
        search : ""
    }
    this.props.poHistoryRequest(data);
}

onSearch(e){
   
    e.preventDefault();
    if (this.state.search == "") {
        this.setState({
            toastMsg:"Enter text on search input ",
            toastLoader: true
        })
        setTimeout(() => {
           this.setState({
            toastLoader:false
           })
        }, 1500);
    } else {
    this.setState({
        type: 3,
        search: this.state.search,
        orderDate :"",
        orderNo :"",
        validFrom :"",
        validTo:"",
        slCode :"",
        slName:"",
        slCityName:"",
     
    })
    let data = {
        no: 1,
        search: this.state.search,
        type: 3,
        orderDate :"",
        orderNo :"",
        validFrom :"",
        validTo:"",
        slCode :"",
        slName:"",
        slCityName:"",
       
    };
    this.props.poHistoryRequest(data);
}
}

page(e) {
    if (e.target.id == "prev") {
        this.setState({
            prev: this.props.purchaseIndent.poHistory.data.prePage,
            current: this.props.purchaseIndent.poHistory.data.currPage,
            next: this.props.purchaseIndent.poHistory.data.currPage + 1,
            maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
        })
        if (this.props.purchaseIndent.poHistory.data.currPage != 0) {
            let data = {
                
                no: this.props.purchaseIndent.poHistory.data.currPage - 1,
                type:this.state.type,
                search:this.state.search,
                orderDate:this.state.orderDate,
                validFrom: this.state.validFrom,
                orderNo :this.state.orderNo,
                validTo :this.state.validTo,
                slCode :this.state.slCode,
                slCityName:this.state.slCityName,       
                slName:this.state.slName,
                hl1Name:this.state.hl1Name,
                hl2Name:this.state.hl2Name,
                hl3Name:this.state.hl3Name,

            }
            this.props.poHistoryRequest(data);
        }
            
    } else if (e.target.id == "next") {
       
        this.setState({
            prev: this.props.purchaseIndent.poHistory.data.prePage,
            current: this.props.purchaseIndent.poHistory.data.currPage,
            next: this.props.purchaseIndent.poHistory.data.currPage + 1,
            maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
        })
        if (this.props.purchaseIndent.poHistory.data.currPage != this.props.purchaseIndent.poHistory.data.maxPage) {
            let data = {
               
                no: this.props.purchaseIndent.poHistory.data.currPage + 1,
                type:this.state.type,
                search :this.state.search,
                orderDate:this.state.orderDate,
                validFrom: this.state.validFrom,
                orderNo :this.state.orderNo,
                validTo :this.state.validTo,
                slCode :this.state.slCode,
                slCityName:this.state.slCityName,       
                slName:this.state.slName,
                hl1Name:this.state.hl1Name,
                hl2Name:this.state.hl2Name,
                hl3Name:this.state.hl3Name,

            }
            this.props.poHistoryRequest(data)
        }
    }
    else if (e.target.id == "first") {
        this.setState({
            prev: this.props.purchaseIndent.poHistory.data.prePage,
            current: this.props.purchaseIndent.poHistory.data.currPage,
            next: this.props.purchaseIndent.poHistory.data.currPage + 1,
            maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
        })
        if (this.props.purchaseIndent.poHistory.data.currPage <= this.props.purchaseIndent.poHistory.data.maxPage) {
            let data = {
              
                no: 1,
                type:this.state.type,
                search:this.state.search,
                orderDate:this.state.orderDate,
                validFrom: this.state.validFrom,
                orderNo :this.state.orderNo,
                validTo :this.state.validTo,
                slCode :this.state.slCode,
                slCityName:this.state.slCityName,       
                slName:this.state.slName,
                hl1Name:this.state.hl1Name,
                hl2Name:this.state.hl2Name,
                hl3Name:this.state.hl3Name,
            }
            this.props.poHistoryRequest(data)
        }

    }
}


    render() {
        const {search}= this.state
        return (
            <div className="container-fluid">
           
        
                <div className="container_div poh-pg" id="vendor_manage">
                <div className="container-fluid">


                    <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                        <div className="container_content heightAuto">
                            <div className="col-md-6 col-sm-6 pad-0">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            PURCHASE ORDER HISTORY
                            </label>
                                    </li>
                                    <li>
                                        
                                    </li>
                                    <li>
                                        
                                    </li>
                                    <li className="m-top-50">
                                      
                                        <ul className="list-inline m-top-10">
                                         
                                            <li>
                                                <button className="filter_button" onClick={(e)=> this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                                    FILTER
    
                                            <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                        <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                    </svg>
                                                </button>
                                            </li>
                                            {this.state.type != 2 ? null : <span className="clearFilterBtn filterBtnLft" onClick={(e) => this.onClearSearch(e)} >Clear Filter</span>}
                                        
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-md-6 col-sm-6 pad-0">
                      
                                <ul className="list-inline search_list manageSearch m-top18per">
                                <form onSubmit={(e) => this.onSearch(e)}>
                                    <li>
                                        <input type="search" id="search" onChange={(e)=>this.handleChange(e)} value={search} placeholder="Type to Search..." className="search_bar" />
                                        <button className="searchWithBar" onClick={(e)=>this.onSearch(e)}> Search
                                            <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                </path></svg>
                                        </button>
                                    </li>
                                    </form>
                                </ul>
                                {this.state.type == 3 ?<span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>: null}
                            </div>

                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 poHistoryMain tableGeneric tableHeadFix">
                                <div className="scrollableOrgansation oflxAuto">
                                    <table className="table poHistoryTable scrollTable tableOddColor zui-table">
                                       
                                        <thead>
                                            <tr>
                                                 <th>
                                                  {sessionStorage.getItem("partnerEnterpriseName") != "VMART"  ? <label> Supplymint PO No.</label>: <label> Order No.</label>}
                                                </th>
                                                {sessionStorage.getItem("partnerEnterpriseName") != "VMART" ?  <th> <label>Ginesys PO No.</label>
                                                </th>:null}
                                                 <th>
                                                    <label>PO Type</label>
                                                </th>
                                                <th>
                                                    <label>Order Date</label>
                                                </th>
                                                                                             
                                                <th>
                                                    <label>Valid From</label>
                                                </th>
                                                <th>
                                                    <label>Valid To</label>
                                                </th>
                                            <th>

                                                    <label>Division</label>
                                                </th>
                                                <th>
                                                    <label>Section</label>
                                                </th>
                                                <th>
                                                    <label>Department</label>
                                                </th>

                                                <th>
                                                    <label>Supplier Name</label>
                                                </th>
                                                <th>
                                                <label>Supplier Cityname</label>
                                                </th>
                                                <th>
                                                    <label>Supplier Address</label>
                                                </th>
                                                <th>
                                                    <label>Supplier Code</label>
                                                </th>
                                                
                                               
                                      
                                               
                                              
                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                        {this.state.poHistoryState == undefined ||this.state.poHistoryState.length==0 || this.state.poHistoryState==null? <tr className="tableNoData"><td colSpan="13"> NO DATA FOUND </td></tr>  : this.state.poHistoryState.map((data, key) => (
                                                <tr key={key}>
                                                    <td>
                                                    <label>{data.orderNo}</label>
                                                </td>
                                                   {sessionStorage.getItem("partnerEnterpriseName") != "VMART" ?  <td> <label>{data.gensysOrderNo!=undefined?data.gensysOrderNo:" Not generated "}</label>
                                                </td>:null}

                                                <td>
                                                    <label>{data.poType=="raisedIndent"?"Load Indent":data.poType}</label>
                                                </td>
                                                <td>
                                                    <label>{data.orderDate}</label>
                                                </td>
                                                
                                                <td>
                                                    <label>{data.validFrom}
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>{data.validTO}</label>
                                                
                                                </td>
                                                     
                                                    <td>
                                                    <label>{data.hl1Name}</label>
                                                </td>
                                                <td>
                                                    <label>{data.hl2Name}</label>
                                                </td>
                                                <td>
                                                    <label>{data.hl3Name}</label>
                                                </td>
                                                <td>
                                                    <label>{data.slName}</label>
                                                </td>
                                                <td>
                                                    <label>{data.slCityName}</label>
                                                </td>
                                                <td>
                                                    <label>{data.slAddr}</label>
                                                </td>
                                                <td>
                                                    <label>{data.slCode}</label>
                                                </td>
                                                
                                                
                                            

                                               
                                               

                                            </tr>))}
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <div className="pagerDiv">
                            <ul className="list-inline pagination">
                                {this.state.current == 1 || this.state.current==0 ? <li >
                                    <button className="PageFirstBtn pointerNone">
                                        First
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                            First
                  </button>
                                    </li>}
                                {this.state.prev != 0 ? <li >
                                    <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                        Prev
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn" disabled>
                                            Prev
                  </button>
                                    </li>}
                                <li>
                                    <button className="PageFirstBtn pointerNone">
                                        <span>{this.state.current}/{this.state.maxPage}</span>
                                    </button>
                                </li>
                                {this.state.maxPage!=0 ? this.state.next <= this.state.maxPage ? <li >
                                    <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                        Next
                  </button>
                                </li> : <li >
                                        <button className="PageFirstBtn borderNone" disabled>
                                            Next
                  </button>
                                    </li>: <li >
                                        <button className="PageFirstBtn borderNone" disabled>
                                            Next
                  </button>
                                    </li>}


                            </ul>
                        </div>
                        </div>

                    </div>
                        {this.state.loader ? <FilterLoader /> : null}
                        {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null }
{this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
  {this.state.filter?<PoHistoryFilter {...this.props} filterBar={this.state.filterBar} closeFilter ={(e)=>this.openFilter(e)} updateFilter={(e)=>this.updateFilter(e)}/>:null}
                        {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                </div>
               
            </div>
</div>
        );
    }
}

export default PoHistory;
