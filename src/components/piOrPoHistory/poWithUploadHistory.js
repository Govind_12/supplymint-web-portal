import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import ToastLoader from "../loaders/toastLoader";
import PoUploadHistoryFilter from "./poUploadHistoryFilter";
import refreshIcon from "../../assets/refresh.svg";
import { CONFIG } from "../../config/index";
import axios from 'axios';
import Pagination from "../pagination";
import VendorFilter from "../vendorPortal/vendorFilter";
import ArsFilter from "../inventoryPlanning/arsFilter";

class PoWithUploadHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fromDateP:'YYYY-MM-DD',
            toDateP:'YYYY-MM-DD',
            fromDate: '',
            toDate: '',
            historyTable: [],
            tableHeaders: {},
            totalRecord: 0,
            currPage: 0,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1,
            no: 1,
            jobList: [],
            runningJobs: false,
            activeJobId : '',
            activeJobName: '',
            openRightBar: false,
            rightbar: false,
            poHistoryWithUpload: [],            
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            toastLoader: false,
            toastMsg: "",
            search: "",
            uploadedDate: "",
            fileName: "",
            fileRecord: "",
            status: "",

            filter: false,
            filterBar: false,
            filterItems: {},
            appliedFilters: {},
            showFilters: false,
        }
    }
    openShowFilters =(e)=> {
        e.preventDefault();
        this.setState({
            showFilters: !this.state.showFilters
        });
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    componentDidMount(){
        this.props.getAllJobRequest('data');
    }

    componentWillMount() {
        // let data = {
        //     no: 1,
        //     type: 1,
        //     search: "",
        //     uploadedDate: "",
        //     fileName: "",
        //     fileRecord: "",
        //     status: ""
        // }
        // this.props.fmcgGetHistoryRequest(data)
    }

 //----------------------------- Showing job list----------------------------------
    openRunningJobs = () =>{
        this.setState(prevState => ({
            runningJobs: !prevState.runningJobs,
        }))
    }

    setJobHandler = (job) =>{
        this.setState({
            activeJobId: job.id,
            activeJobName: job.jobName,
            runningJobs: false
        })
        let payload = {
            no: 1,
            type: this.state.type,
            from: this.state.fromDate,
            to: this.state.toDate,
            jobId: job.id,
            search: this.state.search,
            filter: this.state.appliedFilters
        }
        this.props.jobHistoryRequest(payload)
    }

    //-----------------------------------------------

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }

    componentWillReceiveProps(nextProps) {
        // if (nextProps.purchaseIndent.fmcgGetHistory.isLoading) {
        //     this.setState({
        //         loader: true
        //     })
        // } else if (nextProps.purchaseIndent.fmcgGetHistory.isError) {
        //     this.setState({
        //         errorMessage: nextProps.purchaseIndent.fmcgGetHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.fmcgGetHistory.message.error.errorMessage,
        //         errorCode: nextProps.purchaseIndent.fmcgGetHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.fmcgGetHistory.message.error.errorCode,
        //         code: nextProps.purchaseIndent.fmcgGetHistory.message.status,
        //         alert: true,

        //         loader: false
        //     })
        //     this.props.fmcgGetHistoryClear()
        // } else if (nextProps.purchaseIndent.fmcgGetHistory.isSuccess) {
        //     if (nextProps.purchaseIndent.fmcgGetHistory.data.resource != null) {

        //         this.setState({
        //             prev: nextProps.purchaseIndent.fmcgGetHistory.data.prePage,
        //             current: nextProps.purchaseIndent.fmcgGetHistory.data.currPage,
        //             next: nextProps.purchaseIndent.fmcgGetHistory.data.currPage + 1,
        //             maxPage: nextProps.purchaseIndent.fmcgGetHistory.data.maxPage,
        //         })
        //     }
        //     this.setState({
        //         loader: false,
        //         poHistoryWithUpload: nextProps.purchaseIndent.fmcgGetHistory.data.resource,
        //         prev: nextProps.purchaseIndent.fmcgGetHistory.data.prePage,
        //         current: nextProps.purchaseIndent.fmcgGetHistory.data.currPage,
        //         next: nextProps.purchaseIndent.fmcgGetHistory.data.currPage + 1,
        //         maxPage: nextProps.purchaseIndent.fmcgGetHistory.data.maxPage,
        //     })
        //     this.props.fmcgGetHistoryClear()
        // }
        if(nextProps.replenishment.getAllJob.isSuccess){
            if(nextProps.replenishment.getAllJob.data.resource != null && nextProps.replenishment.getAllJob.data.resource.length != 0){
                let payload = {
                    no:  1,
                    type: 1,
                    from: '',
                    to: '',
                    jobId: nextProps.replenishment.getAllJob.data.resource[0].id,
                }
                this.setState({
                    jobList: nextProps.replenishment.getAllJob.data.resource,
                    activeJobId: nextProps.replenishment.getAllJob.data.resource[0].id,
                    activeJobName: nextProps.replenishment.getAllJob.data.resource[0].jobName,
                }, () => this.props.jobHistoryRequest(payload));
                this.props.getHeaderConfigRequest({
                    enterpriseName: "TURNINGCLOUD",
                    attributeType: "TABLE HEADER",
                    displayName: "REPLENISHMENT_HISTORY",
                });
            }
            this.props.getAllJobClear()
        }
        // if(nextProps.replenishment.getAllJob.isError){
        //     this.setState({
        //         errorMessage: nextProps.replenishment.getAllJob.error == undefined ? '' : nextProps.replenishment.getAllJob.error,
        //         alert: true,
        //         code: nextProps.replenishment.getAllJob.status,
        //         errorCode: nextProps.replenishment.getAllJob.status == undefined ? '' : nextProps.replenishment.getAllJob.status
        //     })
        // }
        if(nextProps.replenishment.jobHistory.isSuccess){
            if(nextProps.replenishment.jobHistory.data.resource != null){
                this.setState({
                    historyTable: nextProps.replenishment.jobHistory.data.resource,
                    totalRecord: nextProps.replenishment.jobHistory.data.totalRecord,
                    current: nextProps.replenishment.jobHistory.data.currPage,
                    currPage: nextProps.replenishment.jobHistory.data.currPage,
                    maxPage: nextProps.replenishment.jobHistory.data.maxPage,               
                    prev: nextProps.replenishment.jobHistory.data.prePage,
                    next: nextProps.replenishment.jobHistory.data.maxPage == nextProps.replenishment.jobHistory.data.currPage ? nextProps.replenishment.jobHistory.data.currPage : nextProps.replenishment.jobHistory.data.currPage +1,
                })
            } else if(nextProps.replenishment.jobHistory.data.resource == null){
                this.setState({
                    historyTable: [],
                    totalRecord: 0,
                    current: 0,
                    currPage: 0,
                    maxPage: 0,               
                    prev: 0,
                    next: 0,
                })
            }
            this.props.jobHistoryClear();
        }    
        if(nextProps.replenishment.jobHistory.isError){            
            this.setState({
                errorMessage: nextProps.replenishment.jobHistory.message.error.errorMessage == undefined ? '' : nextProps.replenishment.jobHistory.message.error.errorMessage,
                alert: true,
                code: nextProps.replenishment.jobHistory.message.status,
                errorCode: nextProps.replenishment.jobHistory.message.error.errorCode == undefined ? '' : nextProps.replenishment.jobHistory.message.error.errorCode
            })
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loader: false,
                alert: false,
                success: false,
                tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
                filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
            });
            this.props.getHeaderConfigClear();
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            });
        }

        if (nextProps.replenishment.downloadRepReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.replenishment.downloadRepReport.data.message,
                alert: false
            });
            window.location.href = nextProps.replenishment.downloadRepReport.data.resource;
            this.props.downloadRepReportClear();
        }
        else if (nextProps.replenishment.downloadRepReport.isError) {
            this.setState({
                loading: false,
                success: false,
                alert: true,
                code: nextProps.replenishment.downloadRepReport.message.status,
                errorCode: nextProps.replenishment.downloadRepReport.message.error == undefined ? undefined : nextProps.replenishment.downloadRepReport.message.error.errorCode,
                errorMessage: nextProps.replenishment.downloadRepReport.message.error == undefined ? undefined : nextProps.replenishment.downloadRepReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadRepReportClear();
        }

        if(nextProps.replenishment.getAllJob.isSuccess || nextProps.replenishment.jobHistory.isSuccess){
            this.setState({
                loader: false,
            })
        }
        if(nextProps.replenishment.getAllJob.isLoading || nextProps.replenishment.jobHistory.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.replenishment.downloadRepReport.isLoading){
            this.setState({
                loader: true,
            })
        }
        if(nextProps.replenishment.getAllJob.isError || nextProps.replenishment.jobHistory.isError){
            this.setState({
                loader: false,                
            })
        }
    }

    //-----------------Date Filter--------------------------

    dateChangeHandler = (e) => {
        if(e.target.id == 'fromDate'){
            this.setState({
                fromDate: e.target.value,
                fromDateP: e.target.value,
                //type: 2,
            })
        } else if(e.target.id == 'toDate'){
            this.setState({
                toDate: e.target.value,
                toDateP: e.target.value,
                //type: 2,
            })
        }
    }

    clearDateFilter = (e) => {
        let type = 0;
        if (this.state.type == 2) {
            type = 1;
        }
        else {
            if (Object.keys(this.state.appliedFilters).length === 0) {
                type = 3;
            }
            else if (this.state.search == "") {
                type = 4;
            }
            else {
                type = 5;
            }
        }
        let payload = {
            no: 1,
            type: type,
            from: '',
            to: '',
            jobId: this.state.activeJobId,
            search: this.state.search,
            filter: this.state.appliedFilters
        };
        this.props.jobHistoryRequest(payload);
        this.setState({
            fromDateP:'YYYY-MM-DD',
            toDateP:'YYYY-MM-DD',
            fromDate: '',
            toDate: '',
            type: type
        });
    }

    applyDateFilter = (e) =>{
        let payload = {
            no: 1,
            type: this.state.type == 1 || this.state.type == 2 ? 2 : 5,
            from: this.state.fromDate,
            to: this.state.toDate,
            jobId: this.state.activeJobId,
            search: this.state.search,
            filter: this.state.appliedFilters
        }
        this.props.jobHistoryRequest(payload);
        this.setState({
            type: this.state.type == 1 || this.state.type == 2 ? 2 : 5
        });
    }


    //------------------Pagination--------------------------

    pageHandler = (e) =>{
        this.setState({
            currPage: e.target.value
        })        
    }

    jumpToPage = (e) =>{
        if(e.key === 'Enter' && e.target.value <= this.state.maxPage){
            let payload = {
                no: e.target.value,
                type: this.state.type,
                from: this.state.fromDate,
                to: this.state.toDate,
                jobId: this.state.activeJobId,
                search: this.state.search,
                filter: this.state.appliedFilters
            }
            this.props.jobHistoryRequest(payload);
        }
    }

    page = (e) => {
        if(e.target.id == 'next'){
            let payload = {
                no: this.state.next,
                type: this.state.type,
                from: this.state.fromDate,
                to: this.state.toDate,
                jobId: this.state.activeJobId,
                search: this.state.search,
                filter: this.state.appliedFilters
            }
            this.props.jobHistoryRequest(payload)
        } else if(e.target.id == 'prev'){
            let payload = {
                no:  this.state.prev,
                type: this.state.type,
                from: this.state.fromDate,
                to: this.state.toDate,
                jobId: this.state.activeJobId,
                search: this.state.search,
                filter: this.state.appliedFilters
            }
            this.props.jobHistoryRequest(payload)
        } else if(e.target.id == 'first'){
            let payload = {
                no:  1,
                type: this.state.type,
                from: this.state.fromDate,
                to: this.state.toDate,
                jobId: this.state.activeJobId,
                search: this.state.search,
                filter: this.state.appliedFilters
            }
            this.props.jobHistoryRequest(payload)
        } else if(e.target.id == 'last'){
            let payload = {
                no:  this.state.maxPage,
                type: this.state.type,
                from: this.state.fromDate,
                to: this.state.toDate,
                jobId: this.state.activeJobId,
                search: this.state.search,
                filter: this.state.appliedFilters
            }
            this.props.jobHistoryRequest(payload)
        }
    }

    // page(e) {
    //     if (e.target.id == "prev") {
    //         this.setState({
    //             prev: this.props.purchaseIndent.fmcgGetHistory.data.prePage,
    //             current: this.props.purchaseIndent.fmcgGetHistory.data.currPage,
    //             next: this.props.purchaseIndent.fmcgGetHistory.data.currPage + 1,
    //             maxPage: this.props.purchaseIndent.fmcgGetHistory.data.maxPage,
    //         })
    //         if (this.props.purchaseIndent.fmcgGetHistory.data.currPage != 0) {
    //             let data = {
    //                 no: this.props.purchaseIndent.fmcgGetHistory.data.currPage - 1,
    //                 type: this.state.type,
    //                 search: this.state.search,
    //                 uploadedDate: this.state.uploadedDate,
    //                 fileName: this.state.fileName,
    //                 fileRecord: this.state.fileRecord,
    //                 status: this.state.status
    //             }
    //             this.props.fmcgGetHistoryRequest(data);
    //         }

    //     } else if (e.target.id == "next") {

    //         this.setState({
    //             prev: this.props.purchaseIndent.fmcgGetHistory.data.prePage,
    //             current: this.props.purchaseIndent.fmcgGetHistory.data.currPage,
    //             next: this.props.purchaseIndent.fmcgGetHistory.data.currPage + 1,
    //             maxPage: this.props.purchaseIndent.fmcgGetHistory.data.maxPage,
    //         })
    //         if (this.props.purchaseIndent.fmcgGetHistory.data.currPage != this.props.purchaseIndent.fmcgGetHistory.data.maxPage) {
    //             let data = {
    //                 no: this.props.purchaseIndent.fmcgGetHistory.data.currPage + 1,
    //                 type: this.state.type,
    //                 search: this.state.search,
    //                 uploadedDate: this.state.uploadedDate,
    //                 fileName: this.state.fileName,
    //                 fileRecord: this.state.fileRecord,
    //                 status: this.state.status

    //             }
    //             this.props.fmcgGetHistoryRequest(data);
    //         }
    //     }
    //     else if (e.target.id == "first") {
    //         this.setState({
    //             prev: this.props.purchaseIndent.fmcgGetHistory.data.prePage,
    //             current: this.props.purchaseIndent.fmcgGetHistory.data.currPage,
    //             next: this.props.purchaseIndent.fmcgGetHistory.data.currPage + 1,
    //             maxPage: this.props.purchaseIndent.fmcgGetHistory.data.maxPage,
    //         })
    //         if (this.props.purchaseIndent.fmcgGetHistory.data.currPage <= this.props.purchaseIndent.fmcgGetHistory.data.maxPage) {
    //             let data = {
    //                 no: 1,
    //                 type: this.state.type,
    //                 search: this.state.search,
    //                 uploadedDate: this.state.uploadedDate,
    //                 fileName: this.state.fileName,
    //                 fileRecord: this.state.fileRecord,
    //                 status: this.state.status

    //             }
    //             this.props.fmcgGetHistoryRequest(data);
    //         }
    //     }
    // }

    // _________________________-SEARCH UPLOAD HISTORY_____________________________


    // onClearSearch(e) {
    //     e.preventDefault();
    //     this.setState({
    //         type: 1,
    //         no: 1,
    //         search: "",
    //         uploadedDate: "",
    //         fileName: "",
    //         fileRecord: "",
    //         status: ""
    //     })
    //     let data = {
    //         type: 1,
    //         no: 1,
    //         search: "",
    //         uploadedDate: "",
    //         fileName: "",
    //         fileRecord: "",
    //         status: ""
    //     }
    //     this.props.fmcgGetHistoryRequest(data);
    // }

    // onSearch(e) {
    //     e.preventDefault();
    //     if (this.state.search == "") {
    //         this.setState({
    //             toastMsg: "Enter text on search input ",
    //             toastLoader: true
    //         })
    //         setTimeout(() => {
    //             this.setState({
    //                 toastLoader: false
    //             })
    //         }, 1500);
    //     } else {
    //         this.setState({
    //             type: 3,
    //             search: this.state.search,
    //             uploadedDate: "",
    //             fileName: "",
    //             fileRecord: "",
    //             status: ""
    //         })
    //         let data = {
    //             no: 1,
    //             search: this.state.search,
    //             type: 3,
    //             uploadedDate: "",
    //             fileName: "",
    //             fileRecord: "",
    //             status: ""
    //         };
    //         this.props.fmcgGetHistoryRequest(data);
    //     }
    // }

    search = () => {
        if (this.state.search !== "") {
            this.props.jobHistoryRequest({
                no: 1,
                type: this.state.type == 1 || this.state.type == 3 ? 3 : 5,
                search: this.state.search,
                jobId: this.state.activeJobId,
                from: this.state.fromDate,
                toDate: this.state.toDate,
                filter: this.state.appliedFilters
            });
            this.setState({
                type: this.state.type == 1 || this.state.type == 3 ? 3 : 5,
            });
        }
    }

    clearSearch = () => {
        let type = 0;
        if (this.state.type == 3) {
            type = 1;
        }
        else {
            if (Object.keys(this.state.appliedFilters).length === 0) {
                type = 2;
            }
            else if (this.state.fromDate == "" && this.state.toDate == "") {
                type = 4;
            }
            else {
                type = 5;
            }
        }
        let payload = {
            no: 1,
            type: type,
            from: this.state.fromDate,
            to: this.state.toDate,
            jobId: this.state.activeJobId,
            search: "",
            filter: this.state.appliedFilters
        };
        this.props.jobHistoryRequest(payload);
        this.setState({
            search: "",
            type: type
        });
    }

    //--------------------- FILTER FUNCTIONS BEGIN ---------------------//

    openFilter = (e) => {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        },() =>  document.addEventListener('click', this.closeFilterOnClickEvent));
    }

    closeFilterOnClickEvent = (e) => {
        if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
            this.setState({
                filter: false,
                filterBar: false 
            }, () =>
            document.removeEventListener('click', this.closeFilterOnClickEvent));
        }
    }

    closeFilter = () => {
        this.setState({
            filter: false,
            filterBar: false
        }, () =>
        document.removeEventListener('click', this.closeFilterOnClickEvent));
    }

    applyFilter = (checkedFilters, refs) => {
        let filters = {};
        Object.keys(checkedFilters).forEach((key) => {
            refs[key].current != null && refs[key].current.value != undefined && refs[key].current.value != "" ? filters[key] = refs[key].current.value : ""
        });
        this.props.jobHistoryRequest({
            no: 1,
            type: this.state.type == 1 || this.state.type == 4 ? 4 : 5,
            search: this.state.search,
            filter: filters,
            from: this.state.fromDate,
            to: this.state.toDate,
            jobId: this.state.activeJobId
        });
        this.setState({
            type: this.state.type == 1 || this.state.type == 4 ? 4 : 5,
            appliedFilters: filters
        });
        this.closeFilter();
    }

    clearFilter = () => {
        let type = 0;
        if (this.state.type == 4) {
            type = 1;
        }
        else {
            if (this.state.search == "") {
                type = 2;
            }
            else if (this.state.fromDate == "" && this.state.toDate == "") {
                type = 3;
            }
            else {
                type = 5;
            }
        }
        let payload = {
            no: 1,
            type: type,
            from: this.state.fromDate,
            to: this.state.toDate,
            jobId: this.state.activeJobId,
            search: this.state.search,
            filter: {}
        };
        this.props.jobHistoryRequest(payload);
        this.setState({
            appliedFilters: {},
            type: type
        });
        this.closeFilter();
    }

    removeFilter = (key) => {
        let filters = this.state.appliedFilters;
        delete filters[key];
        if (Object.keys(filters).length == 0) {
            this.clearFilter();
        }
        else {
            this.setState({
                appliedFilters: filters
            });
            this.props.jobHistoryRequest({
                pageNo: 1,
                type: this.state.type,
                search: this.state.search,
                filter: filters,
                from: this.state.fromDate,
                to: this.state.toDate,
                jobId: this.state.activeJobId
            });
        }  
    }

    // // ____________________FILTER MODAL CODE_______________________________-

    // openFilter(e) {
    //     e.preventDefault();
    //     this.setState({
    //         // filter: true,
    //         // filterBar: !this.state.filterBar
    //     });
    // }
    // handleChange(e) {
    //     if (e.target.id == "search") {
    //         this.setState({
    //             search: e.target.value
    //         })
    //     }
    // }
    // updateFilter(data) {
    //     this.setState({
    //         type: data.type
    //     })
    // }
    // onRefresh() {
    //     let data = {
    //         no: 1,
    //         type: 1,
    //         search: "",
    //         uploadedDate: "",
    //         fileName: "",
    //         fileRecord: "",
    //         status: ""
    //     }
    //     this.props.fmcgGetHistoryRequest(data)
    // }

    // // _____________________________ DOWNLAOD REPORTS______________________

    // downloadReport(id) {
    //     console.log(id);
    //     let headers = {
    //         'X-Auth-Token': sessionStorage.getItem('token'),
    //         'Content-Type': 'application/json'
    //     }
    //     axios.get(`${CONFIG.BASE_URL}/admin/po/export/fmcg/error/data/${id}`, { headers: headers })
    //         .then(res => {
    //             console.log(res);
                
    //             window.open(`${res.data.data.resource}`)
    //         }).catch((error) => {
    //             console.log(error);
    //         });
    // }

    xlscsv = () => {
        let data = {
            no: 1,
            type: this.state.type,
            from: this.state.fromDate,
            to: this.state.toDate,
            id: this.state.activeJobId,
            search: this.state.search,
            filter: this.state.appliedFilters
        };
        this.props.downloadRepReportRequest({
            module: "REPLENISHMENT_HISTORY",
            data: data
        });
    }

    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                    <div className="otb-reports-filter p-lr-47">
                        <div className="orf-head">
                            <div className="orfh-left">
                                <div className="orfhl-inner">
                                    <h3>Filters</h3>
                                </div>
                            </div>
                            <div className="orfh-rgt">
                                <button className={this.state.showFilters === false ? "" : "orfh-btn"} onClick={(e) => this.openShowFilters(e)}>
                                    <img src={require('../../assets/down-arrow-new.svg')} />
                                </button>
                            </div>
                        </div>
                        {this.state.showFilters &&<div className="orf-body">
                            <form>
                                <div className="col-lg-12 col-md-12 pad-0">
                                    <div className="col-lg-2 col-md-3 pad-0 pad-r15">
                                        <label>From Date</label>
                                        <input type="date" id='fromDate' max={this.state.toDate} placeholder={this.state.fromDateP} value={this.state.fromDate} onChange={this.dateChangeHandler}/> 
                                    </div>
                                    <div className="col-lg-2 col-md-3 pad-0 pad-r15">
                                        <label>To Date</label> 
                                        <input type="date" id='toDate' min={this.state.fromDate} placeholder={this.state.toDateP} value={this.state.toDate} onChange={this.dateChangeHandler}/>  
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12 pad-0 m-top-20">
                                    <div className="orfb-apply-btn">
                                        {(this.state.fromDate && this.state.toDate) ?
                                        <button type="button" className="orfbab-apply" onClick={this.applyDateFilter}>Apply Filter</button>
                                        :<button type="button" className="orfbab-apply btnDisabled">Apply Filter</button>}
                                        {(this.state.fromDate && this.state.toDate) ?
                                        <button type="button" onClick={this.clearDateFilter}>Clear</button>
                                        :<button type="button" className="btnDisabled">Clear</button>}                                            
                                    </div>
                                </div>
                            </form>
                        </div>}
                    </div>
                </div>
                {
                    this.state.jobList.length == 0 ?
                    <div className="col-lg-12 col-md-12 col-sm-12 p-lr-47 m-top-30">
                        <div className="summary-page-status m-top-30">
                            <div className="sps-error-msg">
                                <span className="spsem-msg">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="information" width="14.413" height="14.413" viewBox="0 0 15.413 15.413">
                                        <path fill="#ff8103" id="Path_1092" d="M7.706 0a7.706 7.706 0 1 0 7.706 7.706A7.715 7.715 0 0 0 7.706 0zm0 14.012a6.305 6.305 0 1 1 6.305-6.305 6.312 6.312 0 0 1-6.305 6.305z" class="cls-1"/>
                                        <path fill="#ff8103" id="Path_1093" d="M145.936 70a.934.934 0 1 0 .934.935.935.935 0 0 0-.934-.935z" class="cls-1" transform="translate(-138.23 -66.731)"/>
                                        <path fill="#ff8103" id="Path_1094" d="M150.7 140a.7.7 0 0 0-.7.7v4.2a.7.7 0 0 0 1.4 0v-4.2a.7.7 0 0 0-.7-.7z" class="cls-1" transform="translate(-142.994 -133.461)"/>
                                    </svg>
                                    No Jobs Found!
                                </span>
                                <p>Currently no jobs has been configured to your account. Please contact support at support@supplymint.com</p>
                            </div>
                        </div>
                    </div> :
                    <React.Fragment>
                        <div className="col-lg-12 pad-0 p-lr-47 m-top-20">
                            <div className="auto-confing-head">
                                <div className="ach-left">
                                    <div className="gvpd-search">
                                        <input type="search" placeholder="Type To Search" value={this.state.search} onChange={(e) => e.target.value == "" ? this.clearSearch() : this.setState({search: e.target.value})} onKeyDown={(e) => e.keyCode == 13 ? this.search() : e.keyCode == 27 ? this.clearSearch() : ""} />
                                        <img className="search-image" src={require('../../assets/searchicon.svg')} onClick={this.search} />
                                        {this.state.search == "" ? null : <span className="closeSearch" onClick={this.clearSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                                    </div>
                                </div>
                                <div className="ach-right">
                                    <div className="achjbt-row">
                                        <p>Showing Summary for</p>
                                        <button className="achjbt-jobs" type="button" onClick={(e) => this.openRunningJobs(e)}>{this.state.activeJobName}</button>
                                        {this.state.runningJobs &&
                                        <div className="achjbtj-dropdown">
                                            <ul>
                                                {this.state.jobList.map((job, index)=>{
                                                    return <li key={index} onClick={() => this.setJobHandler(job)}>{job.jobName}</li>
                                                })}                                        
                                            </ul>
                                        </div>}
                                    </div>
                                    <div className="pihitory-download-dropdown">
                                        <button className="pi-download" type="button" onClick={this.xlscsv}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                                <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                            </svg>
                                        </button>
                                    </div>
                                    <div className="gvpd-filter">
                                        <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                                <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                            </svg>
                                        </button>
                                        {this.state.filter && <ArsFilter filterItems={this.state.filterItems} appliedFilters={this.state.appliedFilters} applyFilter={this.applyFilter} clearFilter={this.clearFilter} closeFilter={(e) => this.closeFilter(e)} />}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 p-lr-47">{
                            Object.keys(this.state.appliedFilters).length == 0 ? "" :
                            <div className="show-applied-filter">
                                <button type="button" className="saf-clear-all" onClick={this.clearFilter}>Clear All</button>
                                {
                                    Object.keys(this.state.appliedFilters).map((key) =>
                                        <button type="button" className="saf-btn">{this.state.filterItems[key]}
                                            <img onClick={() => this.removeFilter(key)} src={require('../../assets/clearSearch.svg')} />
                                            <span className="generic-tooltip">{(key.toLowerCase().includes("date") || key.toLowerCase().includes("time")) && this.state.appliedFilters[key] !== undefined ? `From ${this.state.appliedFilters[key].from} to ${this.state.appliedFilters[key].to}` : this.state.appliedFilters[key]}</span>
                                        </button>
                                    )
                                }
                            </div>
                        }</div>
                        <div className="col-lg-12 pad-0 p-lr-47">
                            <div className="generic-history-table">
                                <div className="ght-manage" id="table-scroll">
                                    <table className="table ght-main-table">
                                        <thead>
                                            <tr>
                                                <th className="ght-sticky-col"></th>
                                                {
                                                    Object.keys(this.state.tableHeaders).map((key) => <th><label>{this.state.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)    
                                                }

                                                {/* <th><label>Job ID</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Order Status</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Job Status</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>End Time</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Duration</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Store Count</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Item Count</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Replenish Date</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                <th><label>Transfer Order</label><img src={require('../../assets/headerFilter.svg')} /></th> */}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.historyTable.length == 0 ?
                                            <tr>
                                                <td align="center" colSpan="12">
                                                    <label>No Data Available</label>
                                                </td>
                                            </tr>
                                            :this.state.historyTable.map((item, index)=>(
                                                <tr key={index}>
                                                <td className="ght-sticky-col">
                                                    <ul className="ght-item-list">
                                                        <li className="ghtil-inner">
                                                            <label className="checkBoxLabel0">
                                                                <input type="checkbox" />
                                                                <span className="checkmark1"></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </td>
                                                {
                                                    Object.keys(this.state.tableHeaders).map((key) => 
                                                    key == "jobId" ?
                                                    <td><label className="ghtmtblue">{item.jobId}</label>
                                                    <div className="table-tooltip"><label>{item.jobId}</label></div>
                                                    </td> :
                                                    key == "jobRunState" ?
                                                    <td><label className={item.jobRunState == 'FAILED' ? "ghtmtstatus ghtmtstatuscan" : item.jobRunState == 'SUCCEEDED' ? "ghtmtstatus ghtmtstatusapv" : ""}>{item.jobRunState}</label></td> :
                                                    key == "transferOrder" ?
                                                    <td><label>
                                                        <button type="button" class={item.excelAllocationPath === null ? "transfer-order-btn btnDisabled" : "transfer-order-btn"} disabled={item.excelAllocationPath === null ? "disabled" : ""} onClick={() => {window.location.href = item.excelAllocationPath}}>A</button>{" "}
                                                        <button type="button" class={item.excelRequirementPath === null ? "transfer-order-btn btnDisabled" : "transfer-order-btn"} disabled={item.excelRequirementPath === null ? "disabled" : ""} onClick={() => {window.location.href = item.excelRequirementPath}}>R</button>{" "}
                                                        <button type="button" class={item.excelsZipPath === null ? "transfer-order-btn btnDisabled" : "transfer-order-btn"} disabled={item.excelsZipPath === null ? "disabled" : ""} onClick={() => {window.location.href = item.excelsZipPath}}>Z</button>{" "}
                                                    </label></td> :
                                                    <td><label>{item[key]}</label></td>)
                                                }
                                                {/* <td><label className="ghtmtblue">{items.jobId}</label></td>
                                                <td><label>{items.status}</label></td>
                                                <td><label className={items.jobRunState == 'FAILED' ? "ghtmtstatus ghtmtstatuscan" : items.jobRunState == 'SUCCEEDED' ? "ghtmtstatus ghtmtstatusapv" : ""}>{items.jobRunState}</label></td>
                                                <td><label>{items.updatedOn}</label></td>
                                                <td><label>{items.duration}</label></td>
                                                <td><label>{items.storeCount}</label></td>
                                                <td><label>{items.itemCount}</label></td>
                                                <td><label>{items.repDate}</label></td>
                                                <td><label>{items.generatedTransferOrder}</label></td> */}
                                            </tr>
                                            ))}
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value={this.state.currPage} onChange={this.pageHandler} onKeyPress={this.jumpToPage}/>
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalRecord}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                }
                {/* <div className="container_div" id="vendor_manage">
                    <div className="container-fluid">
                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                            <div className="container_content heightAuto">
                                <div className="col-md-6 col-sm-12 pad-0">
                                    <ul className="list_style">
                                        <li>
                                            <label className="contribution_mart">
                                                PURCHASE ORDER HISTORY WITH UPLOAD
                                        </label>
                                        </li>
                                        <li className="m-top-50">
                                            <ul className="list-inline m-top-10">
                                                <li>
                                                    <button className="filter_button" onClick={(e) => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                                        FILTER
                                                <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                            <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                        </svg>
                                                    </button>
                                                </li>
                                                <li className="refresh-table">
                                                    <img onClick={() => this.onRefresh()} src={refreshIcon} />
                                                    <p className="tooltiptext topToolTipGeneric">
                                                        Refresh
                                            </p>
                                                </li>
                                                {this.state.type != 2 ? null : <span className="clearFilterBtn filterBtnLft" onClick={(e) => this.onClearSearch(e)} >Clear Filter</span>}

                                            </ul>
                                        </li>

                                    </ul>
                                </div>
                                <div className="col-md-6 col-sm-12 pad-0">
                                    <ul className="list-inline search_list manageSearch m-top18per">
                                        <form onSubmit={(e) => this.onSearch(e)}>
                                            <li>
                                                <input type="search" id="search" onChange={(e) => this.handleChange(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                                <button className="searchWithBar" onClick={(e) => this.onSearch(e)}> Search
                                                    <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                        <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                        </path>
                                                    </svg>
                                                </button>
                                            </li>
                                        </form>
                                    </ul>
                                    {this.state.type == 3 ? <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 poHistoryMain tableGeneric tableHeadFix">
                                    <div className="scrollableOrgansation oflxAuto">
                                        <table className="table poHistoryTable scrollTable tableOddColor zui-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <label>Report</label>
                                                    </th>
                                                    <th>
                                                        <label>JobId</label>
                                                    </th>
                                                    <th>
                                                        <label>Upload Date</label>
                                                    </th>
                                                    <th>
                                                        <label>File Name</label>
                                                    </th>
                                                    <th>
                                                        <label>Generated PO</label>
                                                    </th>
                                                    <th>
                                                        <label>Success PO</label>
                                                    </th>
                                                    <th>
                                                        <label>Failed PO</label>
                                                    </th>
                                                    <th>
                                                        <label>Status</label>
                                                    </th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {this.state.poHistoryWithUpload == undefined || this.state.poHistoryWithUpload.length == 0 || this.state.poHistoryWithUpload == null ? <tr className="tableNoData"><td colSpan="11"> NO DATA FOUND </td></tr> : this.state.poHistoryWithUpload.map((data, key) => (
                                                    <tr key={key}>
                                                        <td>
                                                            <label> {data.status=="SUCCEEDED"?<button type="button" className="donwloadReport" onClick={(e) => this.downloadReport(data.id)}>Download</button>:<button type="button" className="donwloadReport btnDisabled" disabled>Download</button>}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.jobId}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.uploadedDate}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.fileName}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.totalRecord}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.successPo}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.failedPo}</label>
                                                        </td>
                                                        <td>
                                                            <label>{data.status}</label>
                                                        </td>
                                                    </tr>))}
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div className="pagerDiv">
                                    <ul className="list-inline pagination">
                                        {this.state.current == 1 || this.state.current == 0 ? <li >
                                            <button className="PageFirstBtn pointerNone">
                                                First
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                                    First
                  </button>
                                            </li>}
                                        {this.state.prev != 0 ? <li >
                                            <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                Prev
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn" disabled>
                                                    Prev
                  </button>
                                            </li>}
                                        <li>
                                            <button className="PageFirstBtn pointerNone">
                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                            </button>
                                        </li>
                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                            <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                Next
                  </button>
                                        </li> : <li >
                                                <button className="PageFirstBtn borderNone" disabled>
                                                    Next
                  </button>
                                            </li> : <li >
                                                <button className="PageFirstBtn borderNone" disabled>
                                                    Next
                  </button>
                                            </li>}


                                    </ul>
                                </div>
                            </div>

                        </div>
                        </div>
                        
                    </div> */}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.setState.toastMsg} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {/* {this.state.filter ? <PoUploadHistoryFilter {...this.state} {...this.props} closeFilter={(e) => this.openFilter(e)} updateFilter={(e) => this.updateFilter(e)} /> : null} */}
            </div>
        );
    }
}

export default PoWithUploadHistory;
