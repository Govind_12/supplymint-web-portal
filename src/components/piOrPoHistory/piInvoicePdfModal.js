import React from 'react';
import errorIcon from "../../assets/error_icon.svg";


class PiInvoicePdfModal extends React.Component{
    constructor(props) {
        super(props);
      
      }
componentWillMount(){
    this.setState({
        adhocModalData:this.props.adhocModalData
    })
}

approveDisapprove(action){
let data={
    status:action,
    pattern:this.props.orderDetailId

}
this.props.piUpdateRequest(data)
}
    render(){
    
        return(
            <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
            <div className="modal-content vendorEditModalContent modalShow adHocModal budgetedSaleModal pdfModalMain">
                 

            
                <div className="col-md-12 col-sm-12 pad-0">
                {this.props.status=="APPROVED"?<button type="button" className="approveBtn btnDisabled" disabled >Approve</button>:<button type="button" className="approveBtn" onClick={()=>this.approveDisapprove("APPROVED")} >Approve</button>}
                   {this.props.status=="REJECTED"? <button type="button "className="rejectBtn btnDisabled" disabled>Reject</button>: <button type="button "className="rejectBtn" onClick={()=>this.approveDisapprove("REJECTED")}>Reject</button>}
                    <div className="downloadPdfBtn float_Right">                    
                        <a href={this.props.piPdf} target="_blank" className="downloadBtn">Download</a>
                    </div>                    
                </div>
                <div className="container">
                    <div className="pdfView m-top-40">
                    <iframe src={this.props.piPdf}></iframe>
                    </div>
                </div>
                
             

                <div className="adHocBottom">
                    <button type="button" className="btnHover" onClick={(e)=>this.props.closePiInvoice(e)}>Close</button>
                </div>
               
                
            </div>
            
      
        </div>
        </div>
        )
    }
}

export default PiInvoicePdfModal;