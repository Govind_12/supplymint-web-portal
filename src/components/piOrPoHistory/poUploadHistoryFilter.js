import React from "react";

class PoUploadHistoryFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: "",
            no: "",
            uploadedDate: "",
            status: "",
            fileName: ""
        };
    }

    handleChange(e) {
        if (e.target.id == "fileName") {
            this.setState({
                fileName: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        } else if (e.target.id == "uploadedDate") {
            e.target.placeholder = e.target.value
            this.setState({
                uploadedDate: e.target.value
            });
        }
    }

    clearFilter(e) {
        this.setState({
            uploadedDate: "",
            status: "",
            fileName: ""
        })
    }

    onSubmit(e) {
        let data = {
            no: 1,
            type: 2,
            uploadedDate: this.state.uploadedDate,
            fileName: this.state.fileName,
            fileRecord: "",
            status: this.state.status,
            search:""
        }
        this.props.fmcgGetHistoryRequest(data)
        this.props.closeFilter(e);
        this.props.updateFilter(data)
        this.setState({
            uploadedDate: "",
            status: "",
            fileName: ""
        })
    }

    render() {
        let count = 0;
        if (this.state.fileName != "") {
            count++;
        }
        if (this.state.uploadedDate != "") {
            count++;
        }
        if (this.state.status != "") {
            count++;
        }
        return (
            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0 poHistoryFilterInput">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="fileName" value={this.state.fileName} placeholder="file name" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="status" value={this.state.status} placeholder="status" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} placeholder={this.state.uploadedDate == "" ? "Uploaded Date" : this.state.uploadedDate} id="uploadedDate" value={this.state.uploadedDate} className="organistionFilterModal" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                            CLEAR FILTER
                                        </button>
                                    </li>
                                    <li>
                                        {this.state.fileName != "" || this.state.status != "" || this.state.uploadedDate != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default PoUploadHistoryFilter;
