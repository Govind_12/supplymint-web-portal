import React from "react";
import CancelIcon from "../../assets/cancel.svg";

export default class RemarkModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reasonsArray: [],
            remark: "",
            reason: "",
            reasonErr: true,
            remarkErr: true,
        }
    }
   
    componentDidMount() {

    }
    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps){

    }

    handleChange =(e)=>{
        let regex = /[\s]/g;
        let result = e.target.value.replace(regex, '');
        console.log(result)
        this.setState({remark: e.target.value},()=>{
            this.setState({
                remarkErr: result.length > 0 ? false : true,
            }, () => {console.log(result.length, result)})                  
        })
    }

    render() {
        return (
           <div className="modal" id="cancelrequestmodal">
              <div className="backdrop modal-backdrop-new"></div>
                 <div className="modal-content cancel-request-modal">
                    <div className="crm-alert-icon">
                        <img src={CancelIcon} />
                    </div>
                    <div className="crm-body">
                        <span className="crmb-alrt-msg">Please add remark and confirm</span>
                        {/* <div className="crmb-reason">
                            <label>Reason <span className="mandatory">*</span></label>
                            <select className="selectionGenric imgFocus" id="reason" value={this.state.reason} onChange={(e) => this.handleChange(e)}>
                                <option key={"0"} value="">Select Reason</option>
                                {this.state.reasonsArray.map( (data, key) => <option key={key} value={data}>{data}</option>)}
                            </select>
                        </div> */}
                        <div className="crmb-input">
                            <textarea id="remark" value={this.state.remark} onChange={this.handleChange} placeholder="Write your reason for rejection…"></textarea>
                        </div>
                    </div>
                    <div className="crm-footer">
                        <button className="crmf-disbtn" type="button" value="check" onClick={this.props.cancelRemarkRequest}>Cancel</button>
                        {!this.state.remarkErr ? <button className="crmf-canbtn" style={{backgroundColor : '#8b77fa', borderColor : '#8b77fa'}} type="button" value="confirm" onClick={() => this.props.statusUpdateConfirm('', this.state.remark, false)}>Confirm</button>
                        : <button className="crmf-canbtn btnDisabled" type="button" value="confirm" >Confirm</button>}
                    </div>
              </div>
          </div>
        )
    }
}

// export const CancelModal = React.forwardRef((props, ref) => (
//     <div className="modal" id="cancelrequestmodal">
//         <div className="backdrop modal-backdrop-new"></div>
//         <div className="modal-content cancel-request-modal">
//             <div className="crm-alert-icon">
//                 <img src={CancelIcon} />
//             </div>
//             <div className="crm-body">
//                 <span>{props.rejectAsnMsg !== undefined ? props.rejectAsnMsg : (props.rejectInvoiceStatus !== undefined && props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED" ? "Are you sure you want to reject the invoice request"
//                  : "Are you sure you want to cancel the request")}</span>
//                 <div className="crmb-input">
//                     <textarea placeholder={props.rejectAsnMsg !== undefined || (props.rejectInvoiceStatus !== undefined && props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ?  "Write your reason for rejection…" : "Write your reason for cancellation…"} ref={ref} ></textarea>
//                 </div>
//             </div>
//             <div className="crm-footer">
//                 <button className="crmf-disbtn" type="button" value="check" onClick={props.cancelRequest}>Discard</button>
//                 <button className="crmf-canbtn" type="button" value="confirm" onClick={props.cancelRequest}>{props.rejectAsnMsg !== undefined || (props.rejectInvoiceStatus !== undefined && props.rejectInvoiceStatus == "SHIPMENT_CONFIRMED") ? "Reject" : "Cancel"}</button>
//             </div>

//         </div>
//     </div>
// )
// )