import React from "react";
import axios from 'axios';
import moment from "moment";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import PoHistoryFilter from "../piOrPoHistory/poHistoryFilter";
import ToastLoader from "../loaders/toastLoader";
import ColoumSetting from "../replenishment/coloumSetting";
import ConfirmationSummaryModal from "../replenishment/confirmationReset";
import DownloadReport from "../piOrPoHistory/downloadReport";
import eyeIcon_closed from "../../assets/eye-closed.svg";
import eyeIcon_open from "../../assets/eye-open.svg";
import ApproveTag from "../../assets/approve-tag.svg";
import RejectTag from "../../assets/reject-tag.svg";
import PendingTag from "../../assets/pending-tag.svg";
import editIcon from "../../assets/edit.svg";
import ExportExcel from '../../assets/exportToExcel.svg';
import PdfDownload from '../../assets/pdfdownload.svg';
import HeaderFilter from '../../assets/headerFilter.svg';
import { CONFIG } from "../../config/index";
import LineItemData from "./lineItemDataPi";
import PoError from "../loaders/poError"
import ConfirmModal from "../loaders/confirmModal"
import filterIcon from '../../assets/headerFilter.svg';
import VendorFilter from "../vendorPortal/vendorFilter";
import CircleTick from '../../assets/circle-white-tick.svg';
import Reload from '../../assets/refresh-block.svg';
import RemarkModal from "./remarkModal";
import RetryModal from "../loaders/retryApproval";
import "../../styles/main.scss"
import ViewDetailPo from "./viewDetailsPo";
import { object } from "prop-types";

const userType = "entpo"

class PoHistoryNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            statusVal: '',
            rightBtnName: 'View',
            leftBtnName: 'Edit',
            isDownloadPdf: true,
            retryStatus: '',
            isRetryEnable: false,
            lineData:{},
            selectedStatus: '',
            selectedNo: '',
            isRemark: false,
            statusType: '',
            avgMargin: 0,
            totalAmout: 0,
            totalQty: 0,
            openRightBar: false,
            rightbar: false,
            poHistoryState: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1,
            no: 1,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            filter: false,
            filterBar: false,
            generatedDate: "",
            orderNo: "",
            validFrom: "",
            validTo: "",
            slCode: "",
            search: "",
            toastLoader: false,
            toastMsg: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],
            dReport: false,
            pushedPo: {},
            lineItemFixedHeaders: {},
            lineItemDefaultHeaders: {},
            lineItemCustomHeaders: {},
            lineItemData: [],
            viewDetails: false,
            poErrorMsg: false,
            errorMassage: "",
            defaultUpdateVersion: "",
            deleteConfirmModal: false,
            deleteOrderNo: "",
            deleteStatus: "",
            filterKey: "",
            filterType: "",
            prevFilter: "",
            filterItems: this.props.replenishment.getHeaderConfig.data.resource != null ? this.props.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO == undefined ? this.props.replenishment.getHeaderConfig.data.resource["Default Headers"].PO : this.props.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO : {},
            checkedFilters: [],
            filteredValue: [],
            applyFilter: false,
            showDownloadDrop: false,
            selectedGnNo: '',
            selectedId: '',
            currentStatus: [],
            enableDeleteButton: false,
            inputBoxEnable: false,
            fromPIQuantityValue: "",
            toPIQuantityValue: "",
            fromPIAmountValue: "",
            toPIAmountValue: "",
            fromValidFromValue: "",
            toValidFromValue: "",
            fromValidToValue: "",
            toValidToValue: "",
            fromCreationDate: "start date",
            toCreationDate: "end date",
            fromValidTo: "start date",
            toValidTo: "end date",
            fromValidFrom: "start date",
            toValidFrom: "end date",
            enableApproveButton: false,
            enableRejectButton: false,
            enableCancelButton: false,
            exportToExcel: false,
            filterNameForDate: "",
            jumpPage: 1,
            totalPendingPo: "",
            statusForPdf: '',
            approvedPO: [],
            searchCheck: false,
            selectedItems: 0,
            selectAll: false,
            active: [],
            totalPendingPo: '',
            tagState: false,
            editFlag: false,
            deleteIndentNo: [],
            //main
            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "PO_TABLE_HEADER",
                displayName: "TABLE HEADER"
            },
            tabVal: 1,
            getMainHeaderConfig: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mainFixedHeaderData: [],
            mainHeaderConfigState: {},
            mainHeaderConfigDataState: {},
            mainCustomHeaders: {},
            mainAvailableHeaders: [],
            saveMainState: [],

            //set (consider as piDetails header except catDesc,itemUdf,setUdf,lineItem)
            getSetHeaderConfig: [],
            setFixedHeader: [],
            setCustomHeadersState: [],
            setCustomHeaders: {},
            setHeaderSummary: [],
            setDefaultHeaderMap: [],
            setFixedHeaderData: [],
            setHeaderConfigState: {},
            setHeaderConfigDataState: {},
            saveSetState: [],
            setHeaderCondition: false,
            setAvailableHeaders: [],

            //catDesc Header::
            getCatDescHeaderConfig: [],
            catDescFixedHeader: [],
            catDescCustomHeadersState: [],
            catDescCustomHeaders: {},
            catDescHeaderSummary: [],
            catDescDefaultHeaderMap: [],
            catDescFixedHeaderData: [],
            catDescHeaderConfigState: {},
            catDescHeaderConfigDataState: {},
            saveCatDescState: [],
            catDescHeaderCondition: false,
            catDescAvailableHeaders: [],
             
            //itemUdf Header::
            getItemUdfHeaderConfig: [],
            itemUdfFixedHeader: [],
            itemUdfCustomHeadersState: [],
            itemUdfCustomHeaders: {},
            itemUdfHeaderSummary: [],
            itemUdfDefaultHeaderMap: [],
            itemUdfFixedHeaderData: [],
            itemUdfHeaderConfigState: {},
            itemUdfHeaderConfigDataState: {},
            saveItemUdfState: [],
            itemUdfHeaderCondition: false,
            itemUdfAvailableHeaders: [],

            //setUdf Header::
            getSetUdfHeaderConfig: [],
            setUdfFixedHeader: [],
            setUdfCustomHeadersState: [],
            setUdfCustomHeaders: {},
            setUdfHeaderSummary: [],
            setUdfDefaultHeaderMap: [],
            setUdfFixedHeaderData: [],
            setUdfHeaderConfigState: {},
            setUdfHeaderConfigDataState: {},
            saveSetUdfState: [],
            setUdfHeaderCondition: false,
            setUdfAvailableHeaders: [],

            //lineItem Header::
            getLineItemHeaderConfig: [],
            lineItemFixedHeader: [],
            lineItemCustomHeadersState: [],
            lineItemCustomHeaders_: {},
            lineItemHeaderSummary: [],
            lineItemDefaultHeaderMap: [],
            lineItemFixedHeaderData: [],
            lineItemHeaderConfigState: {},
            lineItemHeaderConfigDataState: {},
            saveLineItemState: [],
            lineItemHeaderCondition: false,
            lineItemAvailableHeaders: [],

            changesInMainHeaders: false,
            changesInSetHeaders: false,
            changesInCatDescHeaders: false,
            changesInSetUdfHeaders: false,
            changesInItemUdfHeaders: false,
            changesInLineItemHeaders: false,

            threeDotMenu: false,
            isSet: false,
        }
        this.cancelRef = React.createRef();
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDragEnter = this.handleDragEnter.bind(this);
        this.pushColumnData = this.pushColumnData.bind(this);
    }
    openThreeDotMenu(e) {
        e.preventDefault();
        this.setState({
            threeDotMenu: !this.state.threeDotMenu
        }, () => document.addEventListener('click', this.closeThreeDotMenu));
    }
    closeThreeDotMenu = () => {
        this.setState({ threeDotMenu: false }, () => {
            document.removeEventListener('click', this.closeThreeDotMenu);
        });
    }
    // openExportToExcel(e) {
    //     e.preventDefault();
    //     this.setState({
    //         exportToExcel: !this.state.exportToExcel
    //     });
    // }

    //new pdfdownload
    downloadPdf() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }

        // let payload = {
        //     //data: [{ "indentId": this.state.indentIdForPdf }]
        //     data: { "orderNo": this.state.selectedId }
        // }
        let payload = {
            data: [{ "orderNo": this.state.selectedId.join('') }]
            
        }


        this.setState({
            downloading: true
        })
        axios.post(`${CONFIG.BASE_URL}/admin/po/download/pdf`, payload, { headers: headers })
            .then(res => {
                this.setState({
                    downloading: false
                })
                if (res.data.data.resource != null) {
                    if (res.data.data.resource[0].url == null) {
                        if(!this.state.isDownloadPdf){
                            this.setState({
                                isDownloadPdf: true
                            })
                        } else if(this.state.isDownloadPdf){
                            this.setState({
                                toastMsg: "PDF is generating, will download after few sec",
                                toastLoader: true
                            })
                            setTimeout(() => {
                                this.setState({
                                    toastLoader: false
                                })
                            }, 5000);
                            axios.post(`${CONFIG.BASE_URL}/admin/po/regenerate/pdf`, payload, { headers: headers })
                            .then(res => {
                                if (res.data.data.message != '' && res.data.status == 2000){
                                    console.log('regnrate success')
                                    setTimeout(() => {
                                        this.downloadPdf() 
                                    }, 20000);
                                } else {
                                    alert("PDF is not generated yet")
                                }
                            })
                            this.setState({
                                isDownloadPdf: false
                            })
                        }
                        // alert("PDF is not generated yet")
                    }
                    else {
                        let a = document.createElement('a');
                        a.target = '_blank'
                        a.href = res.data.data.resource[0].url;
                        a.innerText = '';
                        a.download = true;
                        document.body.appendChild(a);
                        a.click()
                    }
                } else {

                    this.props.oncloseErr()
                }
                this.props.closeReport()

            }).catch((error) => {
                this.setState({
                    downloading: false
                })

            });
    }

    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });

        if (this.state.selectedId.length == 0) {
            alert("Please select data");
        }
        else {
            if (this.state.statusForPdf == "APPROVED") {
                this.downloadPdf();
            }
            else {
                alert("Status: Not Approved")
            }
        }

    }



    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }

    handleChange(e) {
        if (e.target.id == "search") {
            this.setState({
                search: e.target.value
            })

        }
    }
    closeErrorRequest(e) {
        this.setState({

            poErrorMsg: false
        })
    }

    componentDidMount() {
        this.props.poRadioValidationRequest("ata")
        if (!this.props.purchaseIndent.poHistory.isSuccess) {
            let filterObj = {};
            this.props.location.status != undefined && Object.keys(this.props.location.filter).map(key => {
                if (key == 'createdTime') {
                    filterObj[key] = { from: this.props.location.filter[key].split('|')[0], to: this.props.location.filter[key].split('|')[1] }
                }
                else {
                    filterObj[key] = this.props.location.filter[key];
                }
            })
            let data = {
                no: 1,
                type: this.props.location.type || 1,
                // orderDate: "",
                // orderNo: "",
                // validFrom: "",
                // validTo: "",
                // slCode: "",
                // slName: "",
                // slCityName: "",
                search: "",
                userType: userType,
                filter: { ...filterObj, status: this.props.location.status },
                filterItems: { ...this.props.location.filterItems },
                // hl1Name: "",
                // hl2Name: "",
                // hl3Name: ""
                // expireDays: this.props.location.expireDays
            }
            if(this.props.location.status){
                this.setState({
                    tagState: true,
                })
            }
            this.props.poHistoryRequest(data);
            let filterValue = {};
            let filter = {};
            this.props.location.status != undefined && Object.keys(this.props.location.filter).map(key => {
                if (key == 'createdTime') {
                    filterValue[key] = { from: this.props.location.filter[key].split('|')[0], to: this.props.location.filter[key].split('|')[1] }
                }
                else {
                    filterValue[key] = this.props.location.filter[key]
                }
            })
            let checkValue = this.props.location.status != undefined && Object.keys(this.props.location.filter).map(key => {
                filter[this.state.filterItems[key]] = this.props.location.filter[key];
                return this.state.filterItems[key]
            })
            this.state.Status = this.props.location.status;
            this.setState({
                ...filter,
                loader: false,
                filteredValue: this.props.location.status != undefined ? { ...filterValue, status: this.props.location.status } : {},
                checkedFilters: this.props.location.status != undefined ? [...checkValue, 'Status'] : [],
                applyFilter: this.props.location.status != undefined ? true : false,
                inputBoxEnable: this.props.location.status != undefined ? true : false,
                type: this.props.location.status != undefined ? 2 : 1
            })
        } else
            if (this.props.purchaseIndent.poHistory.isSuccess) {
                if (this.props.purchaseIndent.poHistory.data.resource != null) {

                    this.setState({
                        prev: this.props.purchaseIndent.poHistory.data.prePage,
                        current: this.props.purchaseIndent.poHistory.data.currPage,
                        next: this.props.purchaseIndent.poHistory.data.currPage + 1,
                        maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
                        loader: false,
                        totalPendingPo: this.props.purchaseIndent.poHistory.data.totalRecord,
                        avgMargin: this.props.purchaseIndent.poHistory.data.avgMargin,
                        totalAmout: this.props.purchaseIndent.poHistory.data.totalAmount,
                        totalQty : this.props.purchaseIndent.poHistory.data.totalQty,
                    })
                }

                else {
                    this.setState({
                        poHistoryState: this.props.purchaseIndent.poHistory.data.resource,
                        prev: this.props.purchaseIndent.poHistory.data.prePage,
                        current: this.props.purchaseIndent.poHistory.data.currPage,
                        next: this.props.purchaseIndent.poHistory.data.currPage + 1,
                        maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
                        loader: false,
                        totalPendingPo: this.props.purchaseIndent.poHistory.data.totalRecord,

                    })
                }

            }
        if (!this.props.replenishment.getHeaderConfig.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "PO_TABLE_HEADER",
                displayName: "TABLE HEADER"
            }
            this.props.getHeaderConfigRequest(payload)
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    oncloseErr() {
        this.setState({
            poErrorMsg: true,
            errorMassage: "No data found"
        })
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.purchaseIndent.save_draft_po.isSuccess) {
            nextProps.poSaveDraftClear()
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null) {

                let getHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO) : []
                let fixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO) : []
                let customHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO) : []

                let getMainHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO) : []
                let mainFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO) : []
                let mainCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO) : []
                let headerCondition = mainCustomHeadersState.length == 0 ? true : false
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO).indexOf(obj) == -1 })

                let getSetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails).filter( obj => typeof obj !== 'object') : []
                let setCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails).filter( obj => typeof obj !== 'object') : []
                let setHeaderCondition =  setCustomHeadersState.length == 0 ? true : false
                let setAvailableHeaders = setCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails).filter( obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails).filter( obj => typeof obj !== 'object').indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails).filter( obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails).filter( obj => typeof obj !== 'object').indexOf(obj) == -1 })

                let getSetUdfHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem.setUdfHeader) : []
                let setUdfCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem.setUdfHeader) : [] : []
                let setUdfHeaderCondition = setUdfCustomHeadersState.length == 0 ? true : false
                let setUdfAvailableHeaders = setUdfCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem.setUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem.setUdfHeader).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem.setUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem.setUdfHeader).indexOf(obj) == -1 })

                let getItemUdfHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.itemUdfHeader) : []
                let itemUdfCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.itemUdfHeader) : []
                let itemUdfHeaderCondition = itemUdfCustomHeadersState.length == 0 ? true : false
                let itemUdfAvailableHeaders = itemUdfCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.itemUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.itemUdfHeader).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.itemUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.itemUdfHeader).indexOf(obj) == -1 })

                let getCatDescHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.catDescHeader) : []
                let catDescCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.catDescHeader) : []
                let catDescHeaderCondition = catDescCustomHeadersState.length == 0 ? true : false
                let catDescAvailableHeaders = catDescCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.catDescHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.catDescHeader).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.catDescHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.catDescHeader).indexOf(obj) == -1 })

                let getLineItemHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem).filter(obj => typeof obj !== 'object') : []
                let lineItemCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem).filter(obj => typeof obj !== 'object') : []
                let lineItemHeaderCondition = lineItemCustomHeadersState.length == 0 ? true : false
                let lineItemAvailableHeaders = lineItemCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem).filter(obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem).filter(obj => typeof obj !== 'object').indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem).filter(obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem).filter(obj => typeof obj !== 'object').indexOf(obj) == -1 })
                
                return {
                    // tagState: true,
                    loader: false,
                    customHeaders: prevState.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO,
                    getHeaderConfig,
                    fixedHeader,
                    customHeadersState,
                    filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO : nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO,
                    // tableCustomHeader: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
                    // tableGetHeaderConfig: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
                    headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO : {},
                    fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO : {},
                    headerConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO } : {},
                    headerSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO) : [],
                    defaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO) : [],
                    pushedPo: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO == undefined ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO } : { ...nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO },
                    pushedPi: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO == undefined ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO } : { ...nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO },

                    lineItemDefaultHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails == undefined ? {} : nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails,
                    lineItemFixedHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails == undefined ? {} : nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails,

                    lineItemCustomHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails == undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails,

                    mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    headerCondition,
                    mainCustomHeadersState,
                    mainHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO : {},
                    mainFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].PO : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO } : {},
                    mainHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].PO) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].PO) : [],
                    mandateHeaderMain: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].PO),

                    setCustomHeaders: prevState.setHeaderCondition ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails, ['catDescHeader','itemUdfHeader','lineItem']) : _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails, ['catDescHeader','itemUdfHeader','lineItem']) != {} ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    getSetHeaderConfig,
                    setFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails).filter( obj => typeof obj !== 'object') : [],
                    setCustomHeadersState,
                    setHeaderCondition,
                    setAvailableHeaders,
                    setHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    setFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    setHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails ? _.omit({ ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails}, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    setHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails).filter( obj => obj !== 'catDescHeader' || obj !== 'itemUdfHeader' || obj !== 'lineItem') : [],
                    setDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails).filter( obj => obj !== 'catDescHeader' || obj !== 'itemUdfHeader' || obj !== 'lineItem') : [],
                    mandateHeaderSet: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails).filter( obj => typeof obj !== 'object') : [],

                    setUdfCustomHeaders: prevState.setUdfHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem.setUdfHeader : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem.setUdfHeader != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem.setUdfHeader : {} : {},
                    getSetUdfHeaderConfig,
                    setUdfFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem.setUdfHeader) : [],
                    setUdfCustomHeadersState,
                    setUdfHeaderCondition,
                    setUdfAvailableHeaders,
                    setUdfHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem.setUdfHeader : {},
                    setUdfFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem.setUdfHeader : {},
                    setUdfHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem.setUdfHeader} : {},
                    setUdfHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem.setUdfHeader) : [] : [],
                    setUdfDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem.setUdfHeader) : [],
                    mandateHeaderSetUdf: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails.lineItem.setUdfHeader) : [],

                    itemUdfCustomHeaders: prevState.itemUdfHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.itemUdfHeader : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.itemUdfHeader != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.itemUdfHeader : {} : {},
                    getItemUdfHeaderConfig,
                    itemUdfFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.itemUdfHeader) : [],
                    itemUdfCustomHeadersState,
                    itemUdfHeaderCondition,
                    itemUdfAvailableHeaders,
                    itemUdfHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.itemUdfHeader : {},
                    itemUdfFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.itemUdfHeader : {},
                    itemUdfHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.itemUdfHeader} : {},
                    itemUdfHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.itemUdfHeader) : [],
                    itemUdfDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.itemUdfHeader) : [],
                    mandateHeaderItemUdf: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails.itemUdfHeader) : [],

                    catDescCustomHeaders: prevState.catDescHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.catDescHeader : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.catDescHeader != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.catDescHeader : {} : {},
                    getCatDescHeaderConfig,
                    catDescFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.catDescHeader) : [],
                    catDescCustomHeadersState,
                    catDescHeaderCondition,
                    catDescAvailableHeaders,
                    catDescHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.catDescHeader : {},
                    catDescFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.catDescHeader : {},
                    catDescHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.catDescHeader} : {},
                    catDescHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.catDescHeader) : [],
                    catDescDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.catDescHeader) : [],
                    mandateHeaderCatDesc: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails.catDescHeader) : [],

                    lineItemCustomHeaders_: prevState.lineItemHeaderCondition ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem, ['setUdfHeader']) : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem, ['setUdfHeader']) != {} ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem, ['setUdfHeader']) : {} : {},
                    getLineItemHeaderConfig,
                    lineItemFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem).filter( obj => typeof obj !== 'object') : [],
                    lineItemCustomHeadersState,
                    lineItemHeaderCondition,
                    lineItemAvailableHeaders,
                    lineItemHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem, ['setUdfHeader']) : {},
                    lineItemFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].poDetails.lineItem, ['setUdfHeader']) : {},
                    lineItemHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails ? _.omit({ ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem}, ['setUdfHeader']) : {},
                    lineItemHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].poDetails.lineItem).filter( obj => obj !== 'setUdfHeader' ) : [],
                    lineItemDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].poDetails.lineItem).filter( obj => obj !== 'setUdfHeader') : [],
                    mandateHeaderLineItem: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].poDetails.lineItem).filter( obj => typeof obj !== 'object') : [],
               }
            }
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            }
        }
        if (nextProps.purchaseIndent.poHistory.isError) {
            return {
                errorMessage: nextProps.purchaseIndent.poHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.poHistory.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.poHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.poHistory.message.error.errorCode,
                code: nextProps.purchaseIndent.poHistory.message.status,
                alert: true,
                loader: false
            }
        } else if (nextProps.purchaseIndent.poHistory.isSuccess) {
            if (nextProps.purchaseIndent.poHistory.data.resource != null) {
                return {
                    avgMargin: nextProps.purchaseIndent.poHistory.data.avgMargin,
                    totalAmout: nextProps.purchaseIndent.poHistory.data.totalAmount,
                    totalQty : nextProps.purchaseIndent.poHistory.data.totalQty,
                    poHistoryState: nextProps.purchaseIndent.poHistory.data.resource,
                    loader: false,
                    prev: nextProps.purchaseIndent.poHistory.data.prePage,
                    current: nextProps.purchaseIndent.poHistory.data.currPage,
                    next: nextProps.purchaseIndent.poHistory.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.poHistory.data.maxPage,
                    jumpPage: nextProps.purchaseIndent.poHistory.data.currPage,
                    totalPendingPo: nextProps.purchaseIndent.poHistory.data.totalRecord,
                    selectedItems: nextProps.purchaseIndent.poHistory.data.resultedDataCount,
                    approvedPO: nextProps.purchaseIndent.poHistory.data.resource == null ? [] : nextProps.purchaseIndent.poHistory.data.resource,
                }
            } else {
                return {
                    loader: false,
                    poHistoryState: [],
                    prev: nextProps.purchaseIndent.poHistory.data.prePage,
                    current: nextProps.purchaseIndent.poHistory.data.currPage,
                    next: nextProps.purchaseIndent.poHistory.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.poHistory.data.maxPage,
                }
            }
        }
        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
            return {
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
                loader: false,
                success: true,
                // tagState: true,
            }
        } else if (nextProps.replenishment.createHeaderConfig.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            }
        }
        if (nextProps.purchaseIndent.getLineItem.isSuccess) {
            return {
                loader: false,
                lineItemData: nextProps.purchaseIndent.getLineItem.data.resource != null ? nextProps.purchaseIndent.getLineItem.data.resource : []
            }
        } else if (nextProps.purchaseIndent.getLineItem.isError) {

            return {
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.getLineItem.message.status,
                errorCode: nextProps.purchaseIndent.getLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.getLineItem.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.getLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.getLineItem.message.error.errorMessage
            }
        }
        if (nextProps.purchaseIndent.po_approve_retry.isSuccess) {
            return {
                success: true,
                loader: false,
                successMessage: nextProps.purchaseIndent.po_approve_retry.data.message,
                isRemark: false,
                showLineItem: false,
            }
        }
        else if (nextProps.purchaseIndent.po_approve_retry.isError) {
            return {
                errorMessage: nextProps.purchaseIndent.po_approve_retry.message.error == undefined ? undefined : nextProps.purchaseIndent.po_approve_retry.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.po_approve_retry.message.error == undefined ? undefined : nextProps.purchaseIndent.po_approve_retry.message.error.errorCode,
                code: nextProps.purchaseIndent.po_approve_retry.message.status,
                alert: true,
                loader: false
            }
        }
        if (nextProps.purchaseIndent.po_approve_reject.isSuccess) {
            return {
                success: true,
                loader: false,
                successMessage: nextProps.purchaseIndent.po_approve_reject.data.message,
                isRemark: false,
                showLineItem: false,
            }
        }
        else if (nextProps.purchaseIndent.po_approve_reject.isError) {
            return {
                errorMessage: nextProps.purchaseIndent.po_approve_reject.message.error == undefined ? undefined : nextProps.purchaseIndent.po_approve_reject.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.po_approve_reject.message.error == undefined ? undefined : nextProps.purchaseIndent.po_approve_reject.message.error.errorCode,
                code: nextProps.purchaseIndent.po_approve_reject.message.status,
                alert: true,
                loader: false,
                isRemark: false,
            }
        } else if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {
            return {
                successMessage: nextProps.purchaseIndent.discardPoPiData.data.message,
                loader: false,
                success: true,
                deleteOrderNo: "",
                deleteStatus: ""
            }
        } else if (nextProps.purchaseIndent.discardPoPiData.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.discardPoPiData.message.status,
                errorCode: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorMessage
            }
        }
        if (nextProps.purchaseIndent.po_approve_reject.isLoading || nextProps.purchaseIndent.poHistory.isLoading || nextProps.purchaseIndent.getLineItem.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.replenishment.createHeaderConfig.isLoading || nextProps.purchaseIndent.discardPoPiData.isLoading || nextProps.purchaseIndent.po_approve_retry.isLoading) {
            return {

                loader: true
            }
        }
        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
            return {
                defaultUpdateVersion: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.defaultUpdateVersion != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.defaultUpdateVersion : '',

            }
        }
        return {}
    }


    componentDidUpdate(previousProps, previousState) {
        if (this.props.purchaseIndent.poHistory.isError || this.props.purchaseIndent.poHistory.isSuccess) {
            this.props.poHistoryClear()
            this.setState({selectedId: [], enableApproveButton: false, enableCancelButton: false, enableDeleteButton: false, enableRejectButton: false, currentStatus: []})
        }
        if (this.props.purchaseIndent.po_approve_reject.isError || this.props.purchaseIndent.po_approve_reject.isSuccess) {
            this.props.po_approve_reject_Clear()
            this.setState({selectedId: [], enableApproveButton: false, enableCancelButton: false, enableDeleteButton: false, enableRejectButton: false, currentStatus: []})
        }
        if (this.props.purchaseIndent.po_approve_retry.isError || this.props.purchaseIndent.po_approve_retry.isSuccess) {
            this.props.po_approve_retry_Clear()
        }
        if (this.props.replenishment.getHeaderConfig.isError || this.props.replenishment.getHeaderConfig.isSuccess) {
            this.props.getHeaderConfigClear()
        }
        if (this.props.replenishment.createHeaderConfig.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "PO_TABLE_HEADER",
                displayName: "TABLE HEADER"
            }
            this.props.getHeaderConfigRequest(payload)
            this.props.createHeaderConfigClear()
        } else if (this.props.replenishment.createHeaderConfig.isError) {
            this.props.createHeaderConfigClear()
        }
        if (this.props.purchaseIndent.getLineItem.isError || this.props.purchaseIndent.getLineItem.isSuccess) {
            this.props.getLineItemClear()
        }
        if (this.props.purchaseIndent.po_approve_reject.isSuccess || this.props.purchaseIndent.po_approve_retry.isSuccess) {

            this.setState({
                selectedId: "",
                currentStatus: "",
                selectedGnNo: '',
            })
        }
        if (this.props.purchaseIndent.discardPoPiData.isSuccess || this.props.purchaseIndent.discardPoPiData.isError) {
            this.props.discardPoPiDataClear()
            // let e ={
            //     target : {
            //         value: "reload"
            //     }
            // }
            // this.page(e)
        }
        if (this.props.purchaseIndent.piImageUrl.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (this.props.purchaseIndent.piImageUrl.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: this.props.purchaseIndent.piImageUrl.data.status,
                errorCode: this.props.purchaseIndent.piImageUrl.data.error == undefined ? undefined : this.props.purchaseIndent.piImageUrl.data.error.errorCode,
                errorMessage: this.props.purchaseIndent.piImageUrl.data.error == undefined ? undefined : this.props.purchaseIndent.piImageUrl.data.error.errorMessage
            })
            this.props.piImageUrlClear()
        }
    }
    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            orderDate: data.orderDate,
            orderNo: data.orderNo,
            validFrom: data.validFrom,
            validTo: data.validTo,
            slCode: data.slCode,
            slName: data.slName,
            slCityName: data.slCityName,
            search: data.search,
            hl1Name: data.hl1Name,
            hl2Name: data.hl2Name,
            hl3Name: data.hl3Name,
        });
    }

    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            // orderDate: "",
            // orderNo: "",
            // validFrom: "",
            // validTo: "",
            // slCode: "",
            // slName: "",
            // slCityName: "",
            search: "",
            searchCheck: false
        })
        let data = {
            type: 1,
            no: 1,
            // orderDate: "",
            // orderNo: "",
            // validFrom: "",
            // validTo: "",
            // slCode: "",
            // slName: "",
            // slCityName: "",
            search: ""
        }
        this.props.poHistoryRequest(data);
    }

    cancelActive(e, data) {
        let array = [...this.state.active]
        if (e.target.name == "selectEach" && !this.state.selectAll) {

            let deletedItem = {
                orderNumber: data.orderNumber, orderId: data.orderId, id: data.id, status: data.status == "PARTIAL" ? data.status : "CANCELLED", orgId: data.orgId, oldStatus: data.status, orderCode: data.orderCode,
                documentNumber: data.documentNumber, orderNumber: data.orderNumber,
                documentNumber: data.documentNumber, vendorCode: data.vendorCode,
                vendorName: data.vendorName, orderQty: data.poQty, enterpriseSite: data.siteDetails
                , orderDate: data.createdOnDate, orderAmount: data.poAmount
            }
            if (this.state.active.some((item) => item.id == data.id)) {
                array = array.filter((item) => item.id != data.id)
            } else {
                array.push(deletedItem)
            }
            this.setState({ active: array, enableDeleteButton: true })
        } else if (e.target.name == "selectAll" && array == "") {
            this.setState({ selectAll: !this.state.selectAll, enableDeleteButton: true })
        }
    }
    searchClear = () => {
        if (this.state.type === 3 || this.state.type == 4) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                status: "APPROVED",
                poDate: "",
                createdOn: "",
                poNumber: "",
                vendorName: "",
                userType: userType,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
            }
            this.props.poHistoryRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
        } else { this.setState({ search: "" }) }
    }

    searchIcon = () => {
        if (this.state.search.length) {
            let data = {
                type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                no: 1,
                search: this.state.search,
                filter: this.state.filteredValue,
            }
            this.props.poHistoryRequest(data)
            this.props.poHistoryDataUpdate(data);
            this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true })
        }
    }
    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let data = {
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    no: 1,
                    search: this.state.search,
                    filter: this.state.filteredValue,
                }
                e.preventDefault();
                this.props.poHistoryRequest(data)
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true })
            }
        }
        this.setState({
            search: e.target.value,
            filterCount: 0,
        }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let data = {
                    type: this.state.type == 4 ? 2 : 1,
                    no: 1,
                    search: "",
                    filter: this.state.filteredValue,
                }
                e.preventDefault();
                this.props.poHistoryRequest(data)
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
            }
        })
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.poHistory.data.prePage,
                current: this.props.purchaseIndent.poHistory.data.currPage,
                next: this.props.purchaseIndent.poHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.poHistory.data.currPage != 0) {
                let data = {

                    no: this.props.purchaseIndent.poHistory.data.currPage - 1,
                    type: this.state.type,
                    search: this.state.search,
                    // orderDate: this.state.orderDate,
                    // validFrom: this.state.validFrom,
                    // orderNo: this.state.orderNo,
                    // validTo: this.state.validTo,
                    // slCode: this.state.slCode,
                    // slCityName: this.state.slCityName,
                    // slName: this.state.slName,
                    // hl1Name: this.state.hl1Name,
                    // hl2Name: this.state.hl2Name,
                    // hl3Name: this.state.hl3Name,
                    filter: this.state.filteredValue,

                }
                this.props.poHistoryRequest(data);

            }

        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.poHistory.data.prePage,
                current: this.props.purchaseIndent.poHistory.data.currPage,
                next: this.props.purchaseIndent.poHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.poHistory.data.currPage != this.props.purchaseIndent.poHistory.data.maxPage) {
                let data = {

                    no: this.props.purchaseIndent.poHistory.data.currPage + 1,
                    type: this.state.type,
                    search: this.state.search,
                    // orderDate: this.state.orderDate,
                    // validFrom: this.state.validFrom,
                    // orderNo: this.state.orderNo,
                    // validTo: this.state.validTo,
                    // slCode: this.state.slCode,
                    // slCityName: this.state.slCityName,
                    // slName: this.state.slName,
                    // hl1Name: this.state.hl1Name,
                    // hl2Name: this.state.hl2Name,
                    // hl3Name: this.state.hl3Name,
                    filter: this.state.filteredValue,

                }
                this.props.poHistoryRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.poHistory.data.prePage,
                current: this.props.purchaseIndent.poHistory.data.currPage,
                next: this.props.purchaseIndent.poHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.poHistory.data.currPage <= this.props.purchaseIndent.poHistory.data.maxPage) {
                let data = {

                    no: 1,
                    type: this.state.type,
                    search: this.state.search,
                    // orderDate: this.state.orderDate,
                    // validFrom: this.state.validFrom,
                    // orderNo: this.state.orderNo,
                    // validTo: this.state.validTo,
                    // slCode: this.state.slCode,
                    // slCityName: this.state.slCityName,
                    // slName: this.state.slName,
                    // hl1Name: this.state.hl1Name,
                    // hl2Name: this.state.hl2Name,
                    // hl3Name: this.state.hl3Name,
                    filter: this.state.filteredValue,
                }
                this.props.poHistoryRequest(data)
            }

        } else if (e.target.value == "reload") {
            if (this.props.purchaseIndent.poHistory.data.currPage != undefined || this.props.purchaseIndent.poHistory.data.currPage != 0) {
                let data = {

                    no: this.props.purchaseIndent.poHistory.data.currPage,
                    type: this.state.type,
                    search: this.state.search,
                    // orderDate: this.state.orderDate,
                    // validFrom: this.state.validFrom,
                    // orderNo: this.state.orderNo,
                    // validTo: this.state.validTo,
                    // slCode: this.state.slCode,
                    // slCityName: this.state.slCityName,
                    // slName: this.state.slName,
                    // hl1Name: this.state.hl1Name,
                    // hl2Name: this.state.hl2Name,
                    // hl3Name: this.state.hl3Name,
                    filter: this.state.filteredValue,

                }
                this.props.poHistoryRequest(data);

            }

        }
        else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.purchaseIndent.poHistory.data.prePage,
                    current: this.props.purchaseIndent.poHistory.data.currPage,
                    next: this.props.purchaseIndent.poHistory.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.poHistory.data.maxPage,
                })
                if (this.props.purchaseIndent.poHistory.data.currPage <= this.props.purchaseIndent.poHistory.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.poHistory.data.maxPage,
                        search: this.state.search,
                        status: "APPROVED",
                        poDate: this.state.poDate,
                        createdOn: "",
                        poNumber: this.state.poNumber,
                        vendorName: this.state.vendorName,
                        userType: "vendorpo",
                        vendorId: this.state.vendorId,
                        vendorCity: this.state.vendorCity,
                        vendorState: this.state.vendorState,
                        validTo: this.state.validTo,
                        siteDetail: this.state.siteDetail,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue
                    }
                    this.props.poHistoryRequest(data);
                }
            }
        }
    }

    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1 || tabVal === 2 || tabVal === 3 || tabVal === 4 || tabVal === 5 || tabVal === 6) {
                if (!this.props.replenishment.getHeaderConfig.data.resource) {
                    this.openColoumSetting("true")
                    this.props.getHeaderConfigRequest(this.state.mainHeaderPayload)
                }
            }
            // } else if (tabVal === 2) {
            //     if (!this.props.replenishment.getHeaderConfig.data.resource) {
            //         this.openColoumSetting("true")
            //         this.props.getHeaderConfigRequest(this.state.mainHeaderPayload)
            //     }
            // }
        })
    }


    openColoumSetting(data) {
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }

        }
        else if (this.state.tabVal == 2) {
            if (this.state.setCustomHeadersState.length == 0) {
                this.setState({
                    setHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 3) {
            if (this.state.setUdfCustomHeadersState.length == 0) {
                this.setState({
                    setUdfHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 4) {
            if (this.state.itemUdfCustomHeadersState.length == 0) {
                this.setState({
                    itemUdfHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 5) {
            if (this.state.catDescCustomHeadersState.length == 0) {
                this.setState({
                    catDescHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 6) {
            if (this.state.lineItemCustomHeadersState.length == 0) {
                this.setState({
                    lineItemHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e.target.className.includes("backdrop-transparent")) {
            this.setState({ coloumSetting: false }, () =>
                document.removeEventListener('click', this.closeColumnSetting))
        }
    }

    pushColumnData(e, data) {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let customHeadersState = this.state.setCustomHeadersState
            let headerConfigDataState = this.state.setHeaderConfigDataState
            let customHeaders = this.state.setCustomHeaders
            let saveState = this.state.saveSetState
            let defaultHeaderMap = this.state.setDefaultHeaderMap
            let headerSummary = this.state.setHeaderSummary
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (this.state.setHeaderCondition) {

                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeadersState: customHeadersState,
                setCustomHeaders: customHeaders,
                saveSetState: saveState,
                setDefaultHeaderMap: defaultHeaderMap,
                setHeaderSummary: headerSummary,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
        }// For SET UDF::
        if (this.state.tabVal == 3) {
            let getHeaderConfig = this.state.getSetUdfHeaderConfig
            let customHeadersState = this.state.setUdfCustomHeadersState
            let headerConfigDataState = this.state.setUdfHeaderConfigDataState
            let customHeaders = this.state.setUdfCustomHeaders
            let saveState = this.state.saveSetUdfState
            let defaultHeaderMap = this.state.setUdfDefaultHeaderMap
            let headerSummary = this.state.setUdfHeaderSummary
            let fixedHeaderData = this.state.setUdfFixedHeaderData
            let setUdfAvailableHeaders = this.state.setUdfAvailableHeaders

            if (this.state.setUdfHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(setUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(setUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getsetUdfHeaderConfig: getHeaderConfig,
                setUdfCustomHeadersState: customHeadersState,
                setUdfCustomHeaders: customHeaders,
                saveSetUdfState: saveState,
                setUdfDefaultHeaderMap: defaultHeaderMap,
                setUdfHeaderSummary: headerSummary,
                setUdfAvailableHeaders,
                changesInSetUdfHeaders: true,
            })
        }
        // For ITEM UDF::
        if (this.state.tabVal == 4) {
            let getHeaderConfig = this.state.getItemUdfHeaderConfig
            let customHeadersState = this.state.itemUdfCustomHeadersState
            let headerConfigDataState = this.state.itemUdfHeaderConfigDataState
            let customHeaders = this.state.itemUdfCustomHeaders
            let saveState = this.state.saveItemUdfState
            let defaultHeaderMap = this.state.itemUdfDefaultHeaderMap
            let headerSummary = this.state.itemUdfHeaderSummary
            let fixedHeaderData = this.state.itemUdfFixedHeaderData
            let itemUdfAvailableHeaders = this.state.itemUdfAvailableHeaders

            if (this.state.itemUdfHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(itemUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(itemUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getItemUdfHeaderConfig: getHeaderConfig,
                itemUdfCustomHeadersState: customHeadersState,
                itemUdfCustomHeaders: customHeaders,
                saveItemUdfState: saveState,
                itemUdfDefaultHeaderMap: defaultHeaderMap,
                itemUdfHeaderSummary: headerSummary,
                itemUdfAvailableHeaders,
                changesInItemUdfHeaders: true,
            })
        }
        // FOR CAT DESC HEADER::::
        if (this.state.tabVal == 5) {
            let getHeaderConfig = this.state.getCatDescHeaderConfig
            let customHeadersState = this.state.catDescCustomHeadersState
            let headerConfigDataState = this.state.catDescHeaderConfigDataState
            let customHeaders = this.state.catDescCustomHeaders
            let saveState = this.state.saveCatDescState
            let defaultHeaderMap = this.state.catDescDefaultHeaderMap
            let headerSummary = this.state.catDescHeaderSummary
            let fixedHeaderData = this.state.catDescFixedHeaderData
            let catDescAvailableHeaders = this.state.catDescAvailableHeaders

            if (this.state.catDescHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(catDescAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(catDescAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getCatDescHeaderConfig: getHeaderConfig,
                catDescCustomHeadersState: customHeadersState,
                catDescCustomHeaders: customHeaders,
                saveCatDescState: saveState,
                catDescDefaultHeaderMap: defaultHeaderMap,
                catDescHeaderSummary: headerSummary,
                catDescAvailableHeaders,
                changesInCatDescHeaders: true,
            })
        }
        //FOR LINE ITEM HEADER:::::
        if (this.state.tabVal == 6) {
            let getHeaderConfig = this.state.getLineItemHeaderConfig
            let customHeadersState = this.state.lineItemCustomHeadersState
            let headerConfigDataState = this.state.lineItemHeaderConfigDataState
            let customHeaders = this.state.lineItemCustomHeaders_
            let saveState = this.state.saveLineItemState
            let defaultHeaderMap = this.state.lineItemDefaultHeaderMap
            let headerSummary = this.state.lineItemHeaderSummary
            let fixedHeaderData = this.state.lineItemFixedHeaderData
            let lineItemAvailableHeaders = this.state.lineItemAvailableHeaders

            if (this.state.lineItemHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(lineItemAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(lineItemAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getLineItemHeaderConfig: getHeaderConfig,
                lineItemCustomHeadersState: customHeadersState,
                lineItemCustomHeaders_: customHeaders,
                saveLineItemState: saveState,
                lineItemDefaultHeaderMap: defaultHeaderMap,
                lineItemHeaderSummary: headerSummary,
                lineItemAvailableHeaders,
                changesInLineItemHeaders: true,
            })
        }
    }

    handleDragStart(e, key, dragItem, dragNode) {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({
            dragOn: true
        })
    }

    handleDragEnd(dragItem, dragNode) {
        //this.props.handleDragEnd(this.dragNode)
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({
            dragOn: false
        })
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter(e, key, dragItem, dragNode) {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return {
                            mainHeaderConfigDataState: configheaderData,
                            mainCustomHeaders: configheaderData,
                            changesInMainHeaders: true,
                            mainDefaultHeaderMap: newList,
                            getMainHeaderConfig: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return {
                            changesInMainHeaders: true,
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList,
                        }
                    })

                }
            }
            if (this.state.tabVal == 2) {
                if (this.state.setHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.setDefaultHeaderMap
                        let newSetHeaderList = prevState.getSetHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newSetHeaderList.splice(key, 0, newSetHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newSetHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setCustomHeaders: customheaderData, setHeaderConfigDataState: customheaderData, changesInSetHeaders: true, setDefaultHeaderMap: newList, getSetHeaderConfig: newSetHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.setHeaderSummary
                        let newCustomHeaderList = prevState.setCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setCustomHeaders: customheaderData, changesInSetHeaders: true, setHeaderSummary: newList, setCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            //FOR SET UDF HEADER::
            if (this.state.tabVal == 3) {
                if (this.state.setUdfHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.setUdfDefaultHeaderMap
                        let newSetUdfHeaderList = prevState.getSetUdfHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newSetUdfHeaderList.splice(key, 0, newSetUdfHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setUdfHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newSetUdfHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setUdfCustomHeaders: customheaderData, setUdfHeaderConfigDataState: customheaderData, changesInSetUdfHeaders: true, setUdfDefaultHeaderMap: newList, getSetUdfHeaderConfig: newSetUdfHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.setUdfHeaderSummary
                        let newCustomHeaderList = prevState.setUdfCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setUdfCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setUdfCustomHeaders: customheaderData, changesInSetUdfHeaders: true, setUdfHeaderSummary: newList, setUdfCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            // FOR ITEM UDF HEADER:::
            if (this.state.tabVal == 4) {
                if (this.state.itemUdfHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.itemUdfDefaultHeaderMap
                        let newItemUdfHeaderList = prevState.getItemUdfHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newItemUdfHeaderList.splice(key, 0, newItemUdfHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.itemUdfHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newItemUdfHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { itemUdfCustomHeaders: customheaderData, itemUdfHeaderConfigDataState: customheaderData, changesInItemUdfHeaders: true, itemUdfDefaultHeaderMap: newList, getItemUdfHeaderConfig: newItemUdfHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.itemUdfHeaderSummary
                        let newCustomHeaderList = prevState.itemUdfCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.itemUdfCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { itemUdfCustomHeaders: customheaderData, changesInItemUdfHeaders: true, itemUdfHeaderSummary: newList, itemUdfCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            // FOR CAT DESC HEADER ::
            if (this.state.tabVal == 5) {
                if (this.state.catDescHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.catDescDefaultHeaderMap
                        let newCatDescHeaderList = prevState.getCatDescHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCatDescHeaderList.splice(key, 0, newCatDescHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.catDescHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCatDescHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return {catDescCustomHeaders: customheaderData, catDescHeaderConfigDataState: customheaderData, changesInCatDescHeaders: true, catDescDefaultHeaderMap: newList, getCatDescHeaderConfig: newCatDescHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.catDescHeaderSummary
                        let newCustomHeaderList = prevState.catDescCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.catDescCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { catDescCustomHeaders: customheaderData, changesInCatDescHeaders: true, catDescHeaderSummary: newList, catDescCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            // FOR LINE ITEM::
            if (this.state.tabVal == 6) {
                if (this.state.lineItemHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.lineItemDefaultHeaderMap
                        let newLineItemHeaderList = prevState.getLineItemHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newLineItemHeaderList.splice(key, 0, newLineItemHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.lineItemHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newLineItemHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { lineItemHeaderConfigDataState: customheaderData, changesInLineItemHeaders: true, lineItemDefaultHeaderMap: newList, getLineItemHeaderConfig: newLineItemHeaderList, lineItemCustomHeaders_: customheaderData }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.lineItemHeaderSummary
                        let newCustomHeaderList = prevState.lineItemCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.lineItemCustomHeaders_
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { lineItemCustomHeaders_: customheaderData, changesInLineItemHeaders: true, lineItemHeaderSummary: newList, lineItemCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
        }
    }

    closeColumn(data) {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let headerConfigState = this.state.setHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.setCustomHeadersState
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (!this.state.setHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.setCustomHeadersState.length == 0) {
                    this.setState({
                        setHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            setAvailableHeaders.push(data)
            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeaders: headerConfigState,
                setCustomHeadersState: customHeadersState,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.setFixedHeaderData))[data];
                let saveState = this.state.saveSetState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.setHeaderSummary
                let defaultHeaderMap = this.state.setDefaultHeaderMap
                if (!this.state.setHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    setHeaderSummary: headerSummary,
                    setDefaultHeaderMap: defaultHeaderMap,
                    saveSetState: saveState
                })
            }, 100);
        }
        //FOR SET UDF HEADER:::
        if (this.state.tabVal == 3) {
            let getHeaderConfig = this.state.getSetUdfHeaderConfig
            let headerConfigState = this.state.setUdfHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.setUdfCustomHeadersState
            let fixedHeaderData = this.state.setUdfFixedHeaderData
            let setUdfAvailableHeaders = this.state.setUdfAvailableHeaders

            if (!this.state.setUdfHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.setUdfCustomHeadersState.length == 0) {
                    this.setState({
                        setUdfHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            setUdfAvailableHeaders.push(data)
            this.setState({
                getSetUdfHeaderConfig: getHeaderConfig,
                setUdfCustomHeaders: headerConfigState,
                setUdfCustomHeadersState: customHeadersState,
                setUdfAvailableHeaders,
                changesInSetUdfHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.setUdfFixedHeaderData))[data];
                let saveState = this.state.saveSetUdfState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.setUdfHeaderSummary
                let defaultHeaderMap = this.state.setUdfDefaultHeaderMap
                if (!this.state.setUdfHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    setUdfHeaderSummary: headerSummary,
                    setUdfDefaultHeaderMap: defaultHeaderMap,
                    saveSetUdfState: saveState
                })
            }, 100);
        }
        //FOR ITEM UDF:::
        if (this.state.tabVal == 4) {
            let getHeaderConfig = this.state.getItemUdfHeaderConfig
            let headerConfigState = this.state.itemUdfHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.itemUdfCustomHeadersState
            let fixedHeaderData = this.state.itemUdfFixedHeaderData
            let itemUdfAvailableHeaders = this.state.itemUdfAvailableHeaders

            if (!this.state.itemUdfHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.itemUdfCustomHeadersState.length == 0) {
                    this.setState({
                        itemUdfHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            itemUdfAvailableHeaders.push(data)
            this.setState({
                getItemUdfHeaderConfig: getHeaderConfig,
                itemUdfCustomHeaders: headerConfigState,
                itemUdfCustomHeadersState: customHeadersState,
                itemUdfAvailableHeaders,
                changesInItemUdfHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.itemUdfFixedHeaderData))[data];
                let saveState = this.state.saveItemUdfState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.itemUdfHeaderSummary
                let defaultHeaderMap = this.state.itemUdfDefaultHeaderMap
                if (!this.state.itemUdfHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    itemUdfHeaderSummary: headerSummary,
                    itemUdfDefaultHeaderMap: defaultHeaderMap,
                    saveItemUdfState: saveState
                })
            }, 100);
        }
        //FOR CAT DESC HEADER:::
        if (this.state.tabVal == 5) {
            let getHeaderConfig = this.state.getCatDescHeaderConfig
            let headerConfigState = this.state.catDescHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.catDescCustomHeadersState
            let fixedHeaderData = this.state.catDescFixedHeaderData
            let catDescAvailableHeaders = this.state.catDescAvailableHeaders

            if (!this.state.catDescHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.catDescCustomHeadersState.length == 0) {
                    this.setState({
                        catDescHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            catDescAvailableHeaders.push(data)
            this.setState({
                getCatDescHeaderConfig: getHeaderConfig,
                catDescCustomHeaders: headerConfigState,
                catDescCustomHeadersState: customHeadersState,
                catDescAvailableHeaders,
                changesInCatDescHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.catDescFixedHeaderData))[data];
                let saveState = this.state.saveCatDescState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.catDescHeaderSummary
                let defaultHeaderMap = this.state.catDescDefaultHeaderMap
                if (!this.state.catDescHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    catDescHeaderSummary: headerSummary,
                    catDescDefaultHeaderMap: defaultHeaderMap,
                    saveCatDescState: saveState
                })
            }, 100);
        }
        // FOR LINE ITEM HEADER::
        if (this.state.tabVal == 6) {
            let getHeaderConfig = this.state.getLineItemHeaderConfig
            let headerConfigState = this.state.lineItemHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.lineItemCustomHeadersState
            let fixedHeaderData = this.state.lineItemFixedHeaderData
            let lineItemAvailableHeaders = this.state.lineItemAvailableHeaders

            if (!this.state.lineItemHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.lineItemCustomHeadersState.length == 0) {
                    this.setState({
                        lineItemHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            lineItemAvailableHeaders.push(data)
            this.setState({
                getLineItemHeaderConfig: getHeaderConfig,
                lineItemCustomHeaders_: headerConfigState,
                lineItemCustomHeadersState: customHeadersState,
                lineItemAvailableHeaders,
                changesInLineItemHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.lineItemFixedHeaderData))[data];
                let saveState = this.state.saveLineItemState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.lineItemHeaderSummary
                let defaultHeaderMap = this.state.lineItemDefaultHeaderMap
                if (!this.state.lineItemHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    lineItemHeaderSummary: headerSummary,
                    lineItemDefaultHeaderMap: defaultHeaderMap,
                    saveLineItemState: saveState
                })
            }, 100);
        }
    }
    
    saveColumnSetting(e) {
        let fixedHeadersPiDetail = {};
            fixedHeadersPiDetail = this.state.setFixedHeaderData;
            fixedHeadersPiDetail.catDescHeader = this.state.catDescFixedHeaderData;
            fixedHeadersPiDetail.itemUdfHeader = this.state.itemUdfFixedHeaderData;
            fixedHeadersPiDetail.lineItem = this.state.lineItemFixedHeaderData;
            fixedHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfFixedHeaderData;
         
        let defaultHeadersPiDetail = {};
            defaultHeadersPiDetail = this.state.setHeaderConfigDataState;
            defaultHeadersPiDetail.catDescHeader = this.state.catDescHeaderConfigDataState;
            defaultHeadersPiDetail.itemUdfHeader = this.state.itemUdfHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem = this.state.lineItemHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfHeaderConfigDataState;
            
        let customHeadersPiDetail = {};
            customHeadersPiDetail = Object.keys(this.state.setCustomHeaders).length == 0 ? this.state.setHeaderConfigDataState : this.state.setCustomHeaders;
            customHeadersPiDetail.catDescHeader = Object.keys(this.state.catDescCustomHeaders).length == 0 ? this.state.catDescHeaderConfigDataState : this.state.catDescCustomHeaders;
            customHeadersPiDetail.itemUdfHeader = Object.keys(this.state.itemUdfCustomHeaders).length == 0 ? this.state.itemUdfHeaderConfigDataState : this.state.itemUdfCustomHeaders;
            customHeadersPiDetail.lineItem = Object.keys(this.state.lineItemCustomHeaders_).length == 0 ? this.state.lineItemHeaderConfigDataState : this.state.lineItemCustomHeaders_;
            customHeadersPiDetail.lineItem.setUdfHeader = Object.keys(this.state.setUdfCustomHeaders).length == 0 ?  this.state.setUdfHeaderConfigDataState : this.state.setUdfCustomHeaders;
            
            
        let payload = {
            module: "PROCUREMENT",
            subModule: "PURCHASE ORDER",
            section: "HISTORY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "PO_TABLE_HEADER",
            displayName: "TABLE HEADER",
            fixedHeaders: {
                PO: this.state.mainFixedHeaderData,
                poDetails: fixedHeadersPiDetail
            },
            defaultHeaders: {
                PO: this.state.mainHeaderConfigDataState,
                poDetails: defaultHeadersPiDetail
            },
            customHeaders: {
                PO: Object.keys(this.state.mainCustomHeaders).length == 0 ? this.state.mainHeaderConfigDataState : this.state.mainCustomHeaders,
                poDetails: customHeadersPiDetail
            }
        }
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 2) {
            this.setState({
                coloumSetting: false,
                setHeaderCondition: false,
                saveSetState: [],
                changesInSetHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 3) {
            this.setState({
                coloumSetting: false,
                setUdfHeaderCondition: false,
                saveSetUdfState: [],
                changesInSetUdfHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 4) {
            this.setState({
                coloumSetting: false,
                itemUdfHeaderCondition: false,
                saveItemUdfState: [],
                changesInItemUdfHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 5) {
            this.setState({
                coloumSetting: false,
                catDescHeaderCondition: false,
                saveCatDescState: [],
                changesInCatDescHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 6) {
            this.setState({
                coloumSetting: false,
                lineItemHeaderCondition: false,
                saveLineItemState: [],
                changesInLineItemHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
    }

    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            paraMsg: "Click confirm to continue.",
            confirmModal: true,
        })
    }

    resetColumn() {
        const { getMainHeaderConfig, mainFixedHeader,setFixedHeader, getSetHeaderConfig, setUdfFixedHeader,
            getSetUdfHeaderConfig,getItemUdfHeaderConfig, itemUdfFixedHeader,getCatDescHeaderConfig, catDescFixedHeader,
            getLineItemHeaderConfig, lineItemFixedHeader } = this.state;

        let fixedHeadersPiDetail = {};
            fixedHeadersPiDetail = this.state.setFixedHeaderData;
            fixedHeadersPiDetail.catDescHeader = this.state.catDescFixedHeaderData;
            fixedHeadersPiDetail.itemUdfHeader = this.state.itemUdfFixedHeaderData;
            fixedHeadersPiDetail.lineItem = this.state.lineItemFixedHeaderData;
            fixedHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfFixedHeaderData;
         
        let defaultHeadersPiDetail = {};
            defaultHeadersPiDetail = this.state.setHeaderConfigDataState;
            defaultHeadersPiDetail.catDescHeader = this.state.catDescHeaderConfigDataState;
            defaultHeadersPiDetail.itemUdfHeader = this.state.itemUdfHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem = this.state.lineItemHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfHeaderConfigDataState;
            
        let customHeadersPiDetail = {};
            customHeadersPiDetail = this.state.tabVal == 2 ? this.state.setHeaderConfigDataState : this.state.setCustomHeaders;
            customHeadersPiDetail.catDescHeader = this.state.tabVal == 5 ? this.state.catDescHeaderConfigDataState : this.state.catDescCustomHeaders;
            customHeadersPiDetail.itemUdfHeader = this.state.tabVal == 4 ? this.state.itemUdfHeaderConfigDataState : this.state.itemUdfCustomHeaders;
            customHeadersPiDetail.lineItem = this.state.tabVal == 5 ? this.state.lineItemHeaderConfigDataState : this.state.lineItemCustomHeaders_;
            customHeadersPiDetail.lineItem.setUdfHeader = this.state.tabVal == 3 ? this.state.setUdfHeaderConfigDataState  : this.state.setUdfCustomHeaders;    
        
        let customHeadersPi = {};
            customHeadersPi = this.state.tabVal == 1 ? this.state.mainHeaderConfigDataState : this.state.mainCustomHeaders;

        let payload = {
            module: "PROCUREMENT",
            subModule: "PURCHASE ORDER",
            section: "HISTORY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "PO_TABLE_HEADER",
            displayName: "TABLE HEADER",
            fixedHeaders: {
                PO: this.state.mainFixedHeaderData,
                poDetails: fixedHeadersPiDetail
            },
            defaultHeaders: {
                PO: this.state.mainHeaderConfigDataState,
                poDetails: defaultHeadersPiDetail
            },
            customHeaders: {
                PO: customHeadersPi,
                poDetails: customHeadersPiDetail
            }
        }

        if (this.state.tabVal == 1) {    
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 2) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = setFixedHeader.filter(function (obj) { return getSetHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                setHeaderCondition: true,
                coloumSetting: false,
                saveSetState: [],
                confirmModal: false,
                setAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 3) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = setUdfFixedHeader.filter(function (obj) { return getSetUdfHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                setUdfHeaderCondition: true,
                coloumSetting: false,
                saveSetUdfState: [],
                confirmModal: false,
                setUdfAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 4) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = itemUdfFixedHeader.filter(function (obj) { return getItemUdfHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                itemUdfHeaderCondition: true,
                coloumSetting: false,
                saveItemUdfState: [],
                confirmModal: false,
                itemUdfAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 5) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = catDescFixedHeader.filter(function (obj) { return getCatDescHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                catDescHeaderCondition: true,
                coloumSetting: false,
                saveCatDescState: [],
                confirmModal: false,
                catDescAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 6) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = lineItemFixedHeader.filter(function (obj) { return getLineItemHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                lineItemHeaderCondition: true,
                coloumSetting: false,
                saveLineItemState: [],
                confirmModal: false,
                lineItemAvailableHeaders: availableHeaders
            })
        }
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
            // deleteConfirmModal: !this.state.deleteConfirmModal,
        })
    }
    openDownloadReport() {
        this.setState({
            dReport: true,
            exportToExcel: !this.state.exportToExcel
        })
    }
    closeReport() {
        this.setState({
            dReport: false
        })
    }
    viewDetails(data) {
        this.setState({
            statusVal: data.status,
            viewDetails: true,
            selectedId: data.orderNo,
            isSet: data.isSet == "false" || data.isSet == "FALSE" ? false : true,
        })
        let payload = {
            orderNo: data.orderNo,
            status: data.status == 'DRAFTED' ? true : false,
        }
        this.props.getLineItemRequest(payload)
    }

    closeDetails() {
        this.setState({
            viewDetails: false,
            selectedId: ""
        })
    }

    editPurchaseOrder(orderNo, poType, status, validTo) {
        if (this.state.defaultUpdateVersion == "V1") {
            if (status == "DRAFTED") {
                this.props.getDraftRequest({ orderNo: orderNo, draftType: "PO_DRAFT", validTo: validTo, version: "V1" })
                this.props.history.push("/purchase/purchaseOrder");

            } else if (status == "PENDING") {
                //search
                this.props.poEditRequest({ orderNo: orderNo, poType: poType });
                this.props.history.push("/purchase/purchaseOrder");
            } else {
                this.setState({
                    alert: true,
                    errorMessage: "Cannot edit Approved or Rejected Document",
                    errorCode: "5000",
                    code: "5001"
                })
            }
        } else {
            if (status == "DRAFTED") {
                this.props.getDraftRequest({ orderNo: orderNo, draftType: "PO_DRAFT" })
                this.props.history.push("/purchase/purchaseOrders");

            } else if (status == "PENDING") {
                this.props.poEditRequest({ orderNo: orderNo, poType: poType, version: "V2" });
                this.props.history.push("/purchase/purchaseOrders");

            } else {
                this.setState({
                    alert: true,
                    errorMessage: "Cannot edit Approved or Rejected Document",
                    errorCode: "5000",
                    code: "5001"
                })
            }
        }
    }
    editPurchaseOrderV1(orderNo, version) {
        alert("Order No: ", orderNo, "is not editable because it is ", version)
    }
    deleteForm = () => {
        this.setState({
            headerMsg: "Do you want to delete item(s), If deleted, all data saved in this document will be lost.",
            paraMsg: "Click confirm to continue.",
            deleteConfirmModal: true,
            deleteIndentNo: this.state.selectedId,
            deleteStatus: this.state.currentStatus
        })
    }

    closeDeleteConfirmModal = () => {
        this.setState({
            deleteConfirmModal: !this.state.deleteConfirmModal,
            deleteOrderNo: "",
            deleteStatus: ""
        })
    }

    confirmDeleteForm = () => {
        this.setState({
            deleteConfirmModal: false
        })
        var discardData = [];
        this.state.deleteIndentNo.map(data => {
            discardData.push({ orderNo: data, status: this.state.statusForPdf })
        })
        this.props.discardPoPiDataRequest({ discardData: discardData, value: "po" })
        let data = {
            no: this.state.current,
            type: 1,
            search: this.state.search,
            filter: this.state.filteredValue,
        }
        this.props.poHistoryRequest(data);


        this.setState({
            loader: false,
            deleteIndentNo: [],
            selectedId: '',
            currentStatus: [],
            enableDeleteButton: false,
        })
    }

    filterHeader = (event) => {
        // var data = event.target.dataset.key
        var data = event.target.textContent ? event.target.textContent : event.target.dataset.key;
        var oldActive = document.getElementsByClassName("imgHead");
        // if (event.target.classList.contains('rotate180')) {
        //     event.target.classList.remove("rotate180");
        // } else {
        //     for (var i = 0; i < oldActive.length; i++) {
        //         oldActive[i].classList.remove("rotate180");
        //     }
        //     event.target.classList.add("rotate180");
        // }
        if(document.getElementById(data).classList.contains('rotate180')){
            document.getElementById(data).classList.remove('rotate180')
        }else {
                for (var i = 0; i < oldActive.length; i++) {
                    oldActive[i].classList.remove("rotate180");
                }
                document.getElementById(data).classList.add('rotate180')
            }
        var def = { ...this.state.headerConfigDataState };
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                no: this.state.current,
                type: this.state.type,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue
            }
            this.props.poHistoryRequest(payload);
        })
    }

    openFilter = (e) => {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar

        });
    }

    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            this.setState({ [data]: "" })
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }

    submitFilter = () => {
        let payload = {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] == "" ? "" : payload[data] + "T00:00+05:30")))
        Object.keys(payload).map((data) => ((data.includes("poQuantity") || data.includes("poAmount") || data.includes("createdTime") || data.includes("updationTime"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() })))
        let data = {
            no: 1,
            type: this.state.type == 3 || this.state.type == 4 ? 2 : 4,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: payload
        }
        this.props.poHistoryRequest(data);
        this.props.poHistoryDataUpdate(data)
        this.setState({
            filter: false,
            filteredValue: payload,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            tagState: true
        })
    }

    clearFilter = () => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.poHistoryRequest(payload);

        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
    }
    closeFilter = (e) => {
        // e.preventDefault();
        this.setState({
            filter: false,
            filterBar: false
        });
        this.clearFilter();
    }

    handleInput = (event, filterName) => {
        if (event != undefined && event.length != undefined) {
            this.setState({
                fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toValidToValue: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                fromValidToValue: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                fromValidFromValue: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toValidFromValue: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
        }
        else if (event != null) {
            if (event.target.id === "fromPIQuantity")
                this.setState({ fromPIQuantityValue: event.target.value }, () => this.handleFromAndToValue(event))
            else if (event.target.id == "toPIQuantity")
                this.setState({ toPIQuantityValue: event.target.value }, () => this.handleFromAndToValue(event))
            else if (event.target.id === "fromPIAmount")
                this.setState({ fromPIAmountValue: event.target.value }, () => this.handleFromAndToValue(event))
            else if (event.target.id == "toPIAmount")
                this.setState({ toPIAmountValue: event.target.value }, () => this.handleFromAndToValue(event))
            else
                this.handleFromAndToValue(event);
        }
    }

    handleFromAndToValue = () => {
        var value = "";
        var name = event.target.dataset.value
        if (name == undefined) {
            value = this.state.fromCreationDate + " | " + this.state.toCreationDate
            name = this.state.filterNameForDate;
        }

        else {
            if (event.target.id === "fromPIQuantity" || event.target.id === "toPIQuantity")
                value = this.state.fromPIQuantityValue + " | " + this.state.toPIQuantityValue
            else if (event.target.id === "fromPIAmount" || event.target.id === "toPIAmount")
                value = this.state.fromPIAmountValue + " | " + this.state.toPIAmountValue
            else if (event.target.id === "fromValidFrom" || event.target.id === "toValidFrom")
                value = this.state.fromValidFromValue + " | " + this.state.toValidFromValue
            else if (event.target.id === "fromValidTo" || event.target.id === "toValidTo")
                value = this.state.fromValidToValue + " | " + this.state.toValidToValue
            else
                value = event.target.value
        }


        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }
        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                this.setState({ applyFilter: true })
            }
        })
    }

    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    cancelRemarkRequest = () => {
        this.setState({
            isRemark: false,
        })
    }

    statusUpdate = (status) => {
        if(status == "APPROVED" || status == "PENDING"){
            this.statusUpdateConfirm(status, '',false)
        } else {
            this.setState({
                statusType: status,
                isRemark: true,
            })
        }

    }

    statusUpdateConfirm = (status, remark, retry) => {
        if (this.state.selectedId.length > 0) {
            this.closeDetails();
            let idArray = [];
            idArray.push(this.state.selectedId);
            let payloadForStatus = {
                patterns: Array.isArray(this.state.selectedId) ? this.state.selectedId : idArray,
                status: status == '' ? this.state.statusType : status,
                remark: remark,
            }
            let filter = this.state.checkedFilters.length == 0 ? false : true;
            let search = this.state.search == "" ? false : true;
            let payloadForHistory = {
                no: this.state.current,
                type: filter && search ? 4 : !filter && !search ? 1 : filter && !search ? 2 : 3,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
            }
            let payload = {
                payloadForStatus: payloadForStatus,
                payloadForHistory: payloadForHistory,
            }
            if(retry){
                this.props.po_approve_retry_Request(payload)
            } else{
                this.props.po_approve_reject_Request(payload)
            }
            this.setState({
                selectedId: [],
                currentStatus: [],                
                statusForPdf: '',
                isRemark: false,
            }, () => this.statusIdentify(this.state.currentStatus))
        }
        else {
            this.oncloseErr();
        }
    }

    selectedData = (orderNo, status, data, e) => {
        (e) => this.cancelActive(e, data);
        if (!this.state.selectedId.includes(orderNo)) {
            this.setState({
                selectedGnNo: [...this.state.selectedGnNo, data.gensysOrderNo ],
                // selectedId: [...this.state.selectedId, orderNo],
                selectedId: [...this.state.selectedId, orderNo],
                currentStatus: [...this.state.currentStatus, status],
                statusForPdf: status
            }, () => this.statusIdentify(this.state.currentStatus)
            )
        }
        else {
            var index = this.state.currentStatus.indexOf(status);
            this.state.currentStatus.splice(index, 1);
            this.setState({                
                selectedId: this.state.selectedId.filter(selectedId => {
                    return selectedId !== orderNo
                }),
                selectedGnNo: this.state.selectedGnNo.filter(gnNo => gnNo != data.gensysOrderNo)
            }, () => this.statusIdentify(this.state.currentStatus)
            )
        }
    }

    statusIdentify = () => {
        if (this.state.currentStatus.length == 0 || this.state.currentStatus.includes("APPROVED") || this.state.currentStatus.includes("PENDING")
            || this.state.currentStatus.includes("REJECTED") || this.state.currentStatus.includes("CANCELED")) {
            this.setState({ enableDeleteButton: false })
        }
        else {
            this.setState({ enableDeleteButton: true })
        }
        if (this.state.currentStatus.length == 0 || this.state.currentStatus.includes("APPROVED") || this.state.currentStatus.includes("DRAFTED")
            || this.state.currentStatus.includes("REJECTED") || this.state.currentStatus.includes("CANCELED")) {
            this.setState({ enableApproveButton: false, enableRejectButton: false })
        }
        else {
            this.setState({ enableApproveButton: true, enableRejectButton: true, enableCancelButton: false })
        }
        if (this.state.currentStatus.length == 0 || this.state.currentStatus.includes("DRAFTED") || this.state.currentStatus.includes("REJECTED") || this.state.currentStatus.includes("PENDING")) {
            this.setState({ enableCancelButton: false })
        }
        else {
            this.setState({ enableCancelButton: true })
        }
        if (this.state.currentStatus.length != 0 && (this.state.currentStatus.includes("PENDING") || this.state.currentStatus.includes("APPROVED")) &&
        !this.state.currentStatus.includes("REJECTED") && !this.state.currentStatus.includes("CANCELED") && !this.state.currentStatus.includes("DRAFTED")) {
            let isVisible =  true;
            this.state.selectedGnNo.forEach(item=>{
                if(item == null || item == "") {
                    isVisible =  isVisible && true;
                } else {
                    isVisible = isVisible && false;
                }
            })
            if (this.state.currentStatus.includes('PENDING') && this.state.currentStatus.includes("APPROVED")){
                isVisible = false
            }
            if (this.state.currentStatus.includes('PENDING') && !this.state.currentStatus.includes("APPROVED")) {
                this.setState({
                    retryStatus: 'PENDING'
                })
            }
            if (!this.state.currentStatus.includes('PENDING') && this.state.currentStatus.includes("APPROVED")) {
                this.setState({
                    retryStatus: 'APPROVED'
                })
            }
            this.setState({
                isRetryEnable: isVisible
            })
        } else {
            this.setState({
                isRetryEnable: false
            })
        }
    }
    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        no: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        status: "APPROVED",
                        userType: userType,
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                    }
                    this.props.poHistoryRequest(payload)

                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }
    resetField() {
        this.setState({
            search: "",
            lgtNumber: "",
            lgtDate: "",
            vendorName: "",
            transporterName: "",
            ownerSite: "",
            transportMode: "",
            stationFrom: "",
            stationTo: "",
            lgtReceivedQty: "",
            vehicleNo: "",
            filterCount: 0,
            type: 1,
        })
    }
    onRefresh = () => {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "APPROVED",
            poDate: "",
            createdOn: "",
            poNumber: "",
            vendorName: "",
            userType: "vendorpo",
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [] })
        this.props.poHistoryRequest(payload)
        this.resetField()
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
    }
    clearTag = (e, index) => {
        let deleteItem = this.state.checkedFilters;
        deleteItem.splice(index, 1)
        this.setState({
            checkedFilters: deleteItem
        }, () => {
            if (this.state.checkedFilters.length == 0){
                this.clearFilter();
                this.setState({
                    tagState: false,
                })
            }                
            else
                this.submitFilter();
        })
    }
    clearAllTag = (e) => {
        this.setState({
            checkedFilters: [],
            tagState: false,
        }, () => {
            this.clearFilter();
            this.clearFilterOutside();
        })
    }

    clearFilterOutside = () => {
        this.setState({
            filteredValue: [],
            selectAll: false,
            checkedFilters: []
        })
    }

    openRetryModal = (data) => {
        this.setState({
            isRetry: true,
            headerMsg: (data.erpErrorMessage == '' || data.erpErrorMessage == null) ? "Found some data issues" : data.erpErrorMessage,
            lineData: data,
        })
    }

    closeModalHandler = () => {
        this.setState({
            isRetry: false,
        })
    }

    leftBtnHandler = () => {
        let data = this.state.lineData;
        this.editPurchaseOrder(data.orderNo, data.poType, data.status)
        this.setState({
            isRetry: false,
            headerMsg: "",
        })
    }

    rightBtnHandler = () => {
        let data = this.state.lineData;
        this.viewDetails(data)
        this.setState({
            isRetry: false,
            headerMsg: "",
        })
    }

    render() {
        console.log('catDescState', this.state.catDescHeaderCondition, "1", this.state.catDescHeaderConfigDataState, "2", this.state.catDescHeaderConfigState, "3", this.state.catDescCustomHeaders, "4", this.state.catDescHeaderSummary)
        console.log('lineItemState', this.state.lineItemHeaderCondition, "1", this.state.lineItemHeaderConfigDataState, "2", this.state.lineItemHeaderConfigState, "3", this.state.lineItemCustomHeaders_)
        const { approvedPO, search, sliderDisable } = this.state
        return (
            <div className="container-fluid pad-0 pad-l50" id="vendor_manage">
                <div className="new-purchase-head p-lr-47">
                    <div className="col-lg-6 pad-0">
                    <div className="nph-pi-po-quantity">
                            <ul className="nppq-inner">
                                <li className="nppq-history">
                                    <label>Quantity</label>
                                    <span>{this.state.totalQty}</span>
                                </li>
                                <li className="nppq-history nppqh-borderlft">
                                    <label> Amount </label>
                                    <span><img src={require('../../assets/rupeeNew.svg')} /> {this.state.totalAmout}
                                        <span className="nppqh-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                                <path id="iconmonstr-info-2" fill="#97abbf" d="M7 1.167A5.833 5.833 0 1 1 1.167 7 5.84 5.84 0 0 1 7 1.167zM7 0a7 7 0 1 0 7 7 7 7 0 0 0-7-7zm.583 10.5H6.417V5.833h1.166zM7 3.354a.729.729 0 1 1-.729.729A.729.729 0 0 1 7 3.354z"/>
                                            </svg>
                                            <div className="nppqhi-information">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Status</label></th>
                                                            <th><label>Quantity</label></th>
                                                            <th><label>Amount</label></th>
                                                            <th><label>Margin</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label className="nppqhii-status nppqhiis-drft">Drafted</label></td>
                                                            <td><label>100</label></td>
                                                            <td><label><img src={require('../../assets/rupeeNew.svg')} /> 1,100,55</label></td>
                                                            <td><label>100</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label className="nppqhii-status nppqhiis-apv">Approved</label></td>
                                                            <td><label>100</label></td>
                                                            <td><label><img src={require('../../assets/rupeeNew.svg')} /> 1,100,55</label></td>
                                                            <td><label>100</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label className="nppqhii-status nppqhiis-can">Canceled</label></td>
                                                            <td><label>100</label></td>
                                                            <td><label><img src={require('../../assets/rupeeNew.svg')} /> 1,100,55</label></td>
                                                            <td><label>100</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </span>
                                    </span>
                                </li>
                                <li className="nppq-history nppqh-borderlft">
                                    <label>Margin</label>
                                    <span>{this.state.avgMargin}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-6 pad-0">
                        <div className="gvpd-right">
                            {this.state.enableApproveButton ? <button type="button" className="gen-approved" onClick={(e) => this.statusUpdate("APPROVED")} ><img src={CircleTick} />Approve</button>
                                : <button type="button" className="gen-approved btnDisabled"><img src={CircleTick} />Approve</button>}
                            {this.state.enableRejectButton ? <button type="button" className="gen-cancel" value="check" onClick={(e) => this.statusUpdate("REJECTED")}>Reject</button>
                                : <button type="button" className="gen-cancel btnDisabled" value="check" >Reject</button>}
                            {this.state.enableDeleteButton ? <button type="button" className="pi-download" onClick={(e) => this.deleteForm()}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                    <path fill="#12203c" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                </svg> 
                                <span class="generic-tooltip">Delete</span>
                             </button>
                                : <button type="button" className="pi-download2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                        <path fill="#d8d3d3" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                    </svg> 
                                    <span class="generic-tooltip">Delete</span>
                            </button>}
                            {/* excel download */}
                            <div className="pihitory-download-dropdown">
                                <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openDownloadReport(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                        <g>
                                            <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                            <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                            <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                        </g>
                                    </svg>
                                    <span class="generic-tooltip">EXCEL Download</span>
                                </button>
                                {/* {this.state.exportToExcel &&
                                    <ul className="pi-history-download">
                                        {/* <li>
                                            <button className="export-excel" type="button" onClick={(e) => this.openDownloadReport(e)}  >
                                                <span className="pi-export-svg">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                        <g id="prefix__files" transform="translate(0 2)">
                                                            <g id="prefix__Group_2456" data-name="Group 2456">
                                                                <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                    <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                    <text fontSize="7px" fontFamily="ProximaNova-Bold,Proxima Nova" fontWeight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                        <tspan x="0" y="0">XLS</tspan>
                                                                    </text>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </span>
                                                Export to Excel</button>
                                        </li> */}
                                {/* <li>
                                            <button className="export-excel" type="button">
                                                <span className="pi-export-svg">
                                                    <img src={require('../../assets/downloadAll.svg')} />
                                                </span>
                                                Download All</button>
                                        </li> */}
                                {/* </ul>} */}
                            </div>
                            {/* pdf download */}
                            <div className="pihitory-download-dropdown">
                                <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(event) => this.showDownloadDrop(event)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                        <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                    </svg>
                                    <span class="generic-tooltip">PDF Download</span>
                                </button>

                            </div>
                            <div className="gvpd-filter">
                                <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                    <span class="generic-tooltip">Filter</span>
                                </button>
                                {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                            </div>
                            <div className="gvpd-three-dot-btn">
                                <button className="gvpdtdb-btn" onClick={(e) => this.openThreeDotMenu(e)}><span></span></button>
                                {this.state.threeDotMenu && 
                                <div className="gvpdtd-menu">
                                    <ul className="gvpdtdm-inner">
                                        <li>
                                        {this.state.enableCancelButton && this.state.statusForPdf != 'CANCELED' ? <button type="button" className="gvpdtdm-cancel" value="check" onClick={(e) => this.statusUpdate("CANCELED")}>Cancel Order</button>
                                        : <button type="button" className="gen-cancel btnDisabled" value="check" >Cancel Order</button>}
                                        </li>
                                        <li>
                                        {this.state.isRetryEnable ? <button type="button" className="gvpdtdm-cancel" value="check" onClick={(e) => this.statusUpdateConfirm(this.state.retryStatus, '' , true)}>Retry</button>
                                        : <button type="button" className="gen-cancel btnDisabled" value="check" >Retry</button>}
                                        </li>
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="nph-history-search p-lr-47">
                    <div className="col-lg-6 pad-0">
                        <div className="gen-new-pi-history">
                            <form>
                                <input type="search" id="search" onKeyDown={this.onSearch} onChange={this.onSearch} value={search} placeholder="Type to Search..." className="search_bar" />
                                <button className="searchWithBar" onClick={this.searchIcon}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                    </svg>
                                </button>
                                {search != "" ? <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                            </form>
                        </div>
                    </div>
                </div>
                {/* applied filter / recent filter */}
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState ? this.state.checkedFilters.map((keys, index) => (
                        <div className="show-applied-filter">
                            {index === 0 ? <button type="button" className="saf-clear-all" onClick={(e) => this.clearAllTag(e)}>Clear All</button> : null}
                            <button type="button" className="saf-btn">{keys}
                                <img onClick={(e) => this.clearTag(e, index)} src={require('../../assets/clearSearch.svg')} />
                                {/* <span className="generic-tooltip">{Object.values(this.state.filteredValue)[index]}</span> */}
                                <span className="generic-tooltip">{typeof (Object.values(this.state.filteredValue)[index]) == 'object' ? Object.values(Object.values(this.state.filteredValue)[index]).join(',') : Object.values(this.state.filteredValue)[index]}</span>
                            </button>
                        </div>)) : ''}
                </div>
                <div className="col-lg-12 pad-0 p-lr-47">
                    <div className="generic-history-table">
                        <div className="ght-manage" id="table-scroll">
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                saveSetState={this.state.saveSetState}
                                saveItemState={this.state.saveItemState}
                                saveSetUdfState = {this.state.saveSetUdfState}
                                saveItemUdfState={this.state.saveItemUdfState}
                                saveCatDescState={this.state.saveCatDescState}
                                saveLineItemState={this.state.saveLineItemState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                            <table className="table ght-main-table">
                                <thead>
                                    <tr>
                                        <th className="ght-sticky-col">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                                </li>
                                                <li className="rab-rinner">
                                                    {approvedPO.length != 0 ? (Object.keys(this.state.filteredValue).length != 0 || this.state.searchCheck) && <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.selectAll} name="selectAll" onChange={(e) => this.cancelActive(e, "")} /><span className="checkmark1"></span></label> : ""}
                                                    {approvedPO.length != 0 && this.state.selectAll && <span className="select-all-text">{this.state.totalPendingPo} line items selected</span>}
                                                </li>
                                            </ul>
                                        </th>
                                        {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                            <th onClick={this.filterHeader}>
                                                <label >{data}</label>
                                                <img src={HeaderFilter} className="imgHead" key={key} data-key={data} id={data}/>
                                            </th>
                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                            <th onClick={this.filterHeader}>
                                                <label>{data}</label>
                                                <img src={HeaderFilter} className="imgHead" key={key} data-key={data} id={data}/>
                                            </th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.poHistoryState.length ? this.state.poHistoryState.map((data, key) => (
                                        <tr key={key} className={this.state.selectedId.includes(data.orderNo) ? "vgt-tr-bg" : ""}>
                                            <td className="ght-sticky-col">
                                                <ul className="ght-item-list">
                                                    <li className="ghtil-inner">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkbox" checked={this.state.selectedId.includes(data.orderNo)} name="selectEach" onChange={(e) => this.selectedData(`${data.orderNo}`, data.status, data, e)} />
                                                            <span className="checkmark1"></span>
                                                        </label>
                                                    </li>
                                                    {/* <li className="ghtil-inner ght-eye-btn">
                                                        {data.status != "DRAFTED" ?
                                                            <img src={eyeIcon_open} onClick={() => this.viewLineItem(`${data.orderNo}`, data.status)} />
                                                            : <img src={eyeIcon_closed} />
                                                        }
                                                    </li> */}
                                                    {/* {data.status != "DRAFTED" ?
                                                        <li className="ghtil-inner ght-eye-btn">
                                                            <img src={eyeIcon_open} onClick={() => this.viewLineItem(data.orderNo, data.status)} />
                                                            <span className="generic-tooltip">Data is available</span>
                                                        </li> :
                                                        <li className="ghtil-inner ght-eye-btn">
                                                            <img src={eyeIcon_closed} />
                                                            <span className="generic-tooltip">No Data Available</span>
                                                        </li>
                                                    } */}
                                                    <li className="ghtil-inner ght-eye-btn">
                                                        <img src={eyeIcon_open} onClick={() => this.viewDetails(data)} />
                                                        <span className="generic-tooltip">Data is available</span>
                                                    </li>
                                                    {data.version != "V1" ?
                                                        <li className="ghtil-inner ght-edit" onClick={e => { this.editPurchaseOrder(data.orderNo, data.poType, data.status) }}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit</span>
                                                        </li> :
                                                        <li className="ghtil-inner ght-edit-dis" onClick={e => { this.editPurchaseOrderV1(data.orderNo, data.version) }}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit Disabled</span>
                                                        </li>}
                                                        {/* {data.status=="APPROVED"? <li className="ghtil-inner ght-edit-dis" onClick={e => { this.editPurchaseOrder(data.orderNo, data.poType, data.status) }}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit Disabled</span>
                                                        </li> : null}
                                                        {data.status=="REJECTED"? <li className="ghtil-inner ght-edit-dis" onClick={e => { this.editPurchaseOrder(data.orderNo, data.poType, data.status) }}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit Disabled</span>
                                                        </li> : null}
                                                        {data.status=="CANCELED"? <li className="ghtil-inner ght-edit-dis">
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit Disabled</span>
                                                        </li> : null}
                                                        {data.status=="DRAFTED"? <li className="ghtil-inner ght-edit" onClick={e => { this.editPurchaseOrder(data.orderNo, data.poType, data.status) }}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit</span>
                                                        </li> : null}
                                                        {data.status=="PENDING"? <li className="ghtil-inner ght-edit" onClick={e => { this.editPurchaseOrder(data.orderNo, data.poType, data.status) }}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit</span>
                                                        </li> : null} */}
                                                    {/* <li className="ghtil-inner select_modalRadio">
                                                        <label className="">
                                                            <input id={data.orderNo} type="radio" name="transporter" checked={this.state.selectedData == `${data.orderNo}`} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                        </label>
                                                    </li> */}
                                                    {/* <li className="ghtil-inner apv-rej-btn">
                                                        {data.status=="APPROVED"?  <button className="pohAprv-btn" type="button" ><img src={ApproveTag} /></button>:null}
                                                        {data.status=="REJECTED"?   <button className="pohRejt-btn m-right-8" type="button"><img src={RejectTag} /></button>:null}
                                                        {data.status=="PENDING"?   <button className="pohRejt-btn" type="button"><img src={PendingTag} /></button>:null}
                                                        {data.status=="DRAFTED"?  <span className="pend_badge_pos"> <button className="pohRejt-btn" type="button"> <span style={{fontSize: "10px", backgroundColor: "#f5bf5e", fontWeight: "600", padding: "6px 7px",margin: "0px", borderRadius: "7px",color: "#7b7979"}}> DRAFT </span> </button>  </span>:null}
                                                        {data.status=="DRAFTED"?   <button className="pohRejt-btn" type="button"> <span style={{fontSize: "10px", backgroundColor: "#f5bf5e", fontWeight: "600", padding: "6px 7px",margin: "0px", borderRadius: "7px", color: "#7b7979"}}> DRAFT </span>  </button>:null}
                                                    </li> */}
                                                    {/* <li className="ghtil-inner ght-delete-btn" onClick={(e) => this.deleteForm(data.orderNo, data.status)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                            <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Delete</span>
                                                    </li> */}
                                                </ul>
                                                {/* <label className="select_modalRadio">
                                                    <input id={data.orderNo} type="radio" name="transporter" checked={this.state.selectedData == `${data.orderNo}`} readOnly />
                                                    <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                </label>
                                                {data.status=="APPROVED"?  <button className="pohAprv-btn" type="button" ><img src={ApproveTag} /></button>:null}
                                                {data.status=="REJECTED"?   <button className="pohRejt-btn m-right-8" type="button"><img src={RejectTag} /></button>:null}
                                                {data.status=="PENDING"?   <button className="pohRejt-btn" type="button"><img src={PendingTag} /></button>:null}
                                                {data.status=="DRAFTED"?  <span className="pend_badge_pos"> <button className="pohRejt-btn" type="button"> <span style={{fontSize: "10px", backgroundColor: "#f5bf5e", fontWeight: "600", padding: "6px 7px",margin: "0px", borderRadius: "7px",color: "#7b7979"}}> DRAFT </span> </button>  </span>:null}
                                                {data.status=="DRAFTED"?   <button className="pohRejt-btn" type="button"> <span style={{fontSize: "10px", backgroundColor: "#f5bf5e", fontWeight: "600", padding: "6px 7px",margin: "0px", borderRadius: "7px", color: "#7b7979"}}> DRAFT </span>  </button>:null} */}
                                            </td>
                                            {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                <td key={key} >
                                                    {hdata == "orderNo" && (data.status == "PENDING" || data.status == "APPROVED") && (data.gensysOrderNo == null || data.gensysOrderNo == "") ? <label className="ghtmtError" onClick = {() => this.openRetryModal(data)}>{data["orderNo"]}</label>
                                                    : hdata == "orderNo" ? <label className="ghtmtblue">{data["orderNo"]}</label>
                                                    : hdata === "status" ? (<label className={data.status=="DRAFTED" ? "ghtmtstatus ghtmtstatusdrft" : data.status=="PENDING" ? "ghtmtstatus ghtmtstatuspend" : data.status=="APPROVED" ? "ghtmtstatus ghtmtstatusapv" : data.status=="CANCELED" || data.status=="REJECTED" ? "ghtmtstatus ghtmtstatuscan" : "ghtmtstatus"}>{data["status"]} </label>)
                                                    : <label>{data[hdata]}</label>}
                                                </td>
                                            )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                <td key={keyy} >
                                                    {sdata == "orderNo" && (data.status == "PENDING" || data.status == "APPROVED") && (data.gensysOrderNo == null || data.gensysOrderNo == "") ? <label className="ghtmtError" onClick = {() => this.openRetryModal(data)}>{data["orderNo"]}</label>
                                                    :sdata == "orderNo" ? <label className="ghtmtblue">{data["orderNo"]}</label>
                                                    : sdata === "status" ? (<label className={data.status=="DRAFTED" ? "ghtmtstatus ghtmtstatusdrft" : data.status=="PENDING" ? "ghtmtstatus ghtmtstatuspend" : data.status=="APPROVED" ? "ghtmtstatus ghtmtstatusapv" : data.status=="CANCELED" || data.status=="REJECTED" ? "ghtmtstatus ghtmtstatuscan" : "ghtmtstatus"}>{data["status"]}</label>)
                                                    : <label>{data[sdata]}
                                                        {/* {data.status=="APPROVED"? <img src={ApproveTag} /> : null}
                                                        {data.status=="REJECTED"? <img src={RejectTag} />:null}
                                                        {data.status=="PENDING"? <img src={PendingTag} /> :null} */}
                                                    </label>}
                                                </td>
                                            ))}
                                        </tr>
                                    )) : <tr><td align="center" colSpan="12"><label>No Data Found</label></td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="new-gen-pagination">
                        <div className="ngp-left">
                            <div className="table-page-no">
                                {/* <span>Page :</span><label className="paginationBorder">{this.state.current}</label> */}

                                <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span>
                            </div>
                        </div>
                        <div className="ngp-right">
                            <div className="nt-btn">
                                <div className="pagination-inner">
                                    <ul className="pagination-item">
                                        {/*<li ><button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >First</button></li>*/}
                                        {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? <li >
                                            <button type="button" className="disable-first-btn" >
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="first-btn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        {/*<li><button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">Prev</button></li>*/}
                                        {this.state.prev != 0 && this.state.prev != "" ? <li >
                                            <button className="prev-btn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="dis-prev-btn" type="button" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        <li>
                                            <button className="pi-number-btn">
                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                            </button>
                                        </li>
                                        {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                            <button className="next-btn" onClick={(e) => this.page(e)} id="next">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span> </button>
                                        </li> : <li >
                                                <button className="dis-next-btn" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        {this.state.current != 0 && this.state.next - 1 != this.state.maxPage && this.state.current != undefined && this.state.maxPage != this.state.current ? <li >
                                            <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="dis-last-btn" disabled>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </button>
                                            </li>}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.dReport ? <DownloadReport {...this.state} {...this.props} oncloseErr={(e) => this.oncloseErr(e)} pushedPo={this.state.pushedPo} closeReport={(e) => this.closeReport(e)} /> : null}
                {this.state.viewDetails ?<ViewDetailPo closeDetails={(e) => this.closeDetails(e)}  {...this.state} {...this.props} statusUpdate={this.statusUpdate}/>:null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.poErrorMsg ? <PoError oncloseErr={(e) => this.oncloseErr(e)} errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {/* {this.state.filter ? <PoHistoryFilter {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} updateFilter={(e) => this.updateFilter(e)} /> : null} */}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {/* {this.state.viewDetails ? <LineItemData  {...this.state} {...this.props} lineItem="POLINEITEM" customHeaders={this.state.customHeaders} fixedHeaderData={this.state.fixedHeaderData} headerConfigDataState={this.state.headerConfigDataState} closeLineItem={(e) => this.closeLineItem(e)} lineItemData={this.state.lineItemData} lineItemCustomHeaders={this.state.lineItemCustomHeaders} lineItemDefaultHeaders={this.state.lineItemDefaultHeaders} lineItemFixedHeaders={this.state.lineItemFixedHeaders} /> : null} */}
                {this.state.deleteConfirmModal ? <ConfirmModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeDeleteConfirmModal(e)} confirmDeleteForm={(e) => this.confirmDeleteForm(e)} /> : null}
                {this.state.isRemark && <RemarkModal {...this.state} {...this.props} cancelRemarkRequest={this.cancelRemarkRequest} statusUpdateConfirm={this.statusUpdateConfirm}/>}
                {this.state.isRetry && <RetryModal {...this.state} {...this.props} leftBtnHandler={this.leftBtnHandler} rightBtnHandler={this.rightBtnHandler} closeModalHandler={this.closeModalHandler} />}
            </div>
        );
    }
}

export default PoHistoryNew;