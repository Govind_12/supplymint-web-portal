import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import ToastLoader from "../loaders/toastLoader";
import PoUploadHistoryFilter from "./poUploadHistoryFilter";
import refreshIcon from "../../assets/refresh.svg";
import { CONFIG } from "../../config/index";
import axios from 'axios';
import _ from 'lodash';
import PoError from "../loaders/poError"
import { select } from "redux-saga/effects";
import { object } from "prop-types";
import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;
const dateFormat = 'YYYY/MM/DD';
import moment from 'moment';


class DownloadReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchPushedPo: "",
            searchPushedNotPo: "",
            startDate: "",
            endDate: "",
            startDateerr: false,
            endDateerr: false,
            pushPo: true,
            notPushPo: false,
            pushedHeaders: this.props.pushedPo,
            notPushedHeaders: {},
            downloadPushedHeaders: {},
            downloadNotPushedHeaders: {},
            downloading: false,
            downloadPushedHeaderData: {},
            downloadNotPushedHeaderData: {},
            search: "",
            filters: {},
            headersObject: {},
            lineItem: {}

        }
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    componentDidMount() {
        let payload = {
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: "PO_EXCEL_HEADER",
            displayName: "EXCEL HEADER"
        }

        this.props.getHeaderConfigExcelRequest(payload);
        const data = {};
        Object.keys(this.props.pushedPo).forEach(q => {
            data[q] = this.props.pushedPo[q]
        })
        const data1 = {};
        Object.keys(this.props.pushedPo).forEach(q => {
            data1[q] = this.props.pushedPo[q]
        })
        this.setState({
            pushedHeaders: this.props.pushedPo,
            //notPushedHeaders: this.props.pushedPo,
            downloadPushedHeaders: data,
            headersObject: data,
            downloadPushedHeaderData: { ...data },
            downloadNotPushedHeaders: {}
        })
    }

    componentWillReceiveProps(nextProps) {
        // if (nextProps.replenishment.getHeaderConfigExcel.isSuccess) {
        if (nextProps.replenishment.getHeaderConfigExcel.data.resource != null) {

            this.setState({
                notPushedHeaders: nextProps.replenishment.getHeaderConfigExcel.data.resource["Default Headers"],

            }, () => {
                const data = {};
                Object.keys(this.state.notPushedHeaders).forEach(q => {
                    data[q] = this.state.notPushedHeaders[q]
                })
                if (this.props.notPushPo == true) {
                    this.setState({
                        downloadNotPushedHeaders: data,
                    })
                }
                this.setState({
                    downloadNotPushedHeaderData: { ...data }

                })

            })
        }

        if (nextProps.purchaseIndent.poHistory != null) {

            this.setState({
                search: nextProps.purchaseIndent.poHistory.search,
                filters: nextProps.purchaseIndent.poHistory.filter
            })
            //}
        }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }






    // downloadReport() {
    //     this.startDate();
    //     this.endDate();
    //     setTimeout(() => {

    //         if (!this.state.startDateerr && !this.state.endDateerr) {
    //             let headers = {
    //                 'X-Auth-Token': sessionStorage.getItem('token'),
    //                 'Content-Type': 'application/json'
    //             }
    //             let payload = {
    //                 fromDate: this.state.startDate,
    //                 toDate: this.state.endDate,
    //                 pushHeaders: this.state.downloadpushedHeaders,
    //                 noPushHeaders: this.state.downloadNotPushedHeaders
    //             }
    //             this.setState({
    //                 downloading: true
    //             })
    //             // {"data":[{"orderNo":"290520\/182500033"},{"orderNo":"150620\/141100461"}]}
    //             axios.post(`${CONFIG.BASE_URL}/admin/po/export/pos/data`, payload, { headers: headers })
    //                 .then(res => {

    //                     this.setState({
    //                         downloading: false
    //                     })
    //                     if (res.config.url != null) {
    //                         window.open(`${res.data.data.resource}`)
    //                     } else {
    //                         this.props.oncloseErr()
    //                     }
    //                     this.props.closeReport()
    //                 }).catch((error) => {
    //                     this.setState({
    //                         downloading: false
    //                     })

    //                 });
    //         }
    //     }, 100)
    // }



    // downloadReport() {
    //     this.startDate();
    //     this.endDate();
    //     setTimeout(() => {

    //         if (!this.state.startDateerr && !this.state.endDateerr) {
    //             let headers = {
    //                 'X-Auth-Token': sessionStorage.getItem('token'),
    //                 'Content-Type': 'application/json'
    //             }
    //             let body = {
    //                 "data": [{ "orderNo": "290520\/182500033" }, { "orderNo": "150620\/141100461" }]
    //             }
    //             let payload = {
    //                 fromDate: this.state.startDate,
    //                 toDate: this.state.endDate,
    //                 pushHeaders: this.state.downloadpushedHeaders,
    //                 noPushHeaders: this.state.downloadNotPushedHeaders,
    //                 data: [{ "orderNo": "290520\/182500033" }, { "orderNo": "150620\/141100461" }]
    //             }
    //             this.setState({
    //                 downloading: true
    //             })

    //             axios.post(`${CONFIG.BASE_URL}/admin/po/download/pdf`, payload, { headers: headers })
    //                 .then(res => {

    //                     this.setState({
    //                         downloading: false
    //                     })
    //                     if (res.config.url != null) {
    //                         window.open(`${res.data.data.resource}`)
    //                     } else {
    //                         this.props.oncloseErr()
    //                     }
    //                     this.props.closeReport()
    //                 }).catch((error) => {
    //                     this.setState({
    //                         downloading: false
    //                     })

    //                 });
    //         }
    //     }, 100)
    // }

    // handleRequestedDate = (e) => {
    //     if (e.target.id == "startDate") {
    //         this.setState({
    //             startDate: e.target.value
    //         }, () => {
    //             this.startDate()
    //         })
    //     } else if (e.target.id == "endDate") {
    //         this.setState({
    //             endDate: e.target.value
    //         }, () => {
    //             this.endDate()
    //         })

    //     }


    // }
    // startDate() {
    //     if (this.state.startDate != "") {
    //         this.setState({
    //             startDateerr: false
    //         })
    //     } else {
    //         this.setState({
    //             startDateerr: true
    //         })
    //     }
    // }
    // endDate() {
    //     if (this.state.endDate != "") {
    //         this.setState({
    //             endDateerr: false
    //         })
    //     } else {
    //         this.setState({
    //             endDateerr: true
    //         })
    //     }
    // }
    downloadReport() {

        let payload = {
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            searchData: this.props.search,
            headersdata: (Object.keys(this.state.downloadPushedHeaders).length == 0 ? this.state.downloadNotPushedHeaders : this.state.downloadPushedHeaders),
            filterdata: this.props.filteredValue,
            mainHeader: (Object.keys(this.state.downloadPushedHeaders).length == 0 ? false : true),

        }
        this.props.getExcelDownloadedPoRequest(payload)
        this.props.closeReport()
        console.log("Action fired PI");

    }
    handleChange(e) {
        if (e.target.id == "searchPushedPo") {
            this.setState({
                searchPushedPo: e.target.value
            })

        } else if (e.target.id == "searchPushedNotPo") {
            this.setState({
                searchPushedNotPo: e.target.value
            })
        }

    }
    handleCheck(e) {
        // Left
        if (e.target.id == "pushPo") {
            if (this.state.pushPo) {
                this.setState({
                    pushPo: false, //left select all uncheck
                    downloadPushedHeaders: {}, //left all checkboxes uncheck
                    // headersObject: {}   //object empty
                })
                //console.log("LEFT UNCHECK ", " left select all uncheck", this.state.downloadPushedHeaders)

            } else {
                this.setState({
                    downloadPushedHeaders: { ...this.state.downloadPushedHeaderData },//leftall checkboxes checked
                    pushPo: true, //left select all button check
                    notPushPo: false, //right select all button uncheck
                    downloadNotPushedHeaders: {}, // right all checkboxes uncheck
                })
                //console.log("LEFT CHECK ", " left select all checked, object full ", this.state.downloadPushedHeaders);
            }
        } else {
            //Right
            if (e.target.id == "notPushPo") {
                if (this.state.notPushPo) {
                    this.setState({
                        notPushPo: false, //right select all uncheck
                        downloadNotPushedHeaders: {}, // right call checkboxes unchecked
                        // lineItem: {} // object full

                    })
                    //console.log("RIGHT UNCHECK ", " right select all uncheck, object empty ", this.state.downloadNotPushedHeaders)
                } else {
                    this.setState({
                        downloadNotPushedHeaders: { ...this.state.downloadNotPushedHeaderData }, //right all checkboxes checked
                        notPushPo: true, //right select all button checked
                        pushPo: false, //left select all button unchecked
                        downloadPushedHeaders: {} //left all checkboxes unchecked
                        //lineItem: this.state.downloadNotPushedHeaderData //object empty
                    })
                    //console.log("RIGHT CHECK ", " right select all check, object full ", this.state.downloadNotPushedHeaders);
                    //console.log("RIGHT CHECK ", " right select all check, object full ", this.state.downloadNotPushedHeaderData);
                }
            }
        }

    }

    //left: headers
    onPushedheaderSelect(key, value) {

        if (this.state.downloadPushedHeaders[key] != undefined) {
            //if checkbox is unchecked

            delete this.state.downloadPushedHeaders[key];

            console.log("left checkbox unchecked data DELETED ", key, this.state.downloadPushedHeaders)

            //console.log("Unchecked ", " old ", Object.keys(this.state.downloadPushedHeaders).length + 1, " new ", Object.keys(this.state.downloadPushedHeaderData).length, " key: ", key, " value: ", value, " ")

            if (Object.keys(this.state.downloadPushedHeaders).length != Object.keys(this.state.downloadPushedHeaderData).length) {

                this.setState({
                    pushPo: false,  // if tempered object and original object's length not same then select all button unchecked
                })
            }

        } else {
            //checkbox is checked
            this.setState({
                notPushPo: false, // if i clicked left any checkbox, then right select all button uncheck 
                downloadNotPushedHeaders: {}, //and right all checkboxes uncheck
                //lineItem: {} // and right object empty
            })
            //console.log("checked ", " old ", Object.keys(this.state.downloadPushedHeaders).length + 1, " new ", Object.keys(this.state.downloadPushedHeaderData).length);

            this.state.downloadPushedHeaders[key] = value
            console.log("left checkbox checked data ADD ", this.state.downloadPushedHeaders)

            if (Object.keys(this.state.downloadPushedHeaders).length == Object.keys(this.state.downloadPushedHeaderData).length) {
                this.setState({
                    pushPo: true, //if both are equal left selectall button check,
                    //headersObject: { ...this.state.downloadPushedHeaderData },
                    notPushPo: false, // and right select all button uncheck
                    downloadNotPushedHeaders: {} // right all checkboxes uncheck
                })
                console.log("tempered  = original,so left object full", this.state.downloadPushedHeaders)
            }
        }

    }


    //right:line items
    onHeaderSelect(key, value) {

        if (this.state.downloadNotPushedHeaders[key] != undefined) { //if any checkbox is unchecked

            delete this.state.downloadNotPushedHeaders[key];
            console.log("right checkbox data DELETE ", this.state.downloadNotPushedHeaders)

            // console.log("Unchecked ", " old ", Object.keys(this.state.downloadNotPushedHeaders).length + 1, " new ", Object.keys(this.state.downloadNotPushedHeaderData).length, " key: ", key, " value: ", value, " ")

            if (Object.keys(this.state.downloadNotPushedHeaders).length != Object.keys(this.state.downloadNotPushedHeaderData).length) {

                this.setState({
                    notPushPo: false  //if not equal then right select all button uncheck 
                })
            }

        } else {

            this.setState({
                pushPo: false, //if right any checkbox is checke, left select all checkbox uncheck
                downloadPushedHeaders: {}, // and left all checboxes uncheck
                //headersObject: {} //and left object empty
            })
            //  console.log("checked ", " old ", Object.keys(this.state.downloadNotPushedHeaders).length + 1, " new ", Object.keys(this.state.downloadNotPushedHeaderData).length);
            this.state.downloadNotPushedHeaders[key] = value
            //console.log("right checkbox data ADD ", this.state.downloadNotPushedHeaders)

            if (Object.keys(this.state.downloadNotPushedHeaders).length == Object.keys(this.state.downloadNotPushedHeaderData).length) {
                this.setState({
                    notPushPo: true,  //if they are equal then right select all button checked 
                })
            }
        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    handleFilter = (date) => {
        this.setState({
            startDate: moment(date[0]['_d']).format('YYYY-MM-DD'),
            endDate: moment(date[1]['_d']).format('YYYY-MM-DD')
        })
    }
    render() {
        console.log("start: ", this.state.startDate, "end: ", this.state.endDate)
        const { searchPushedPo, searchPushedNotPo, startDateerr, endDateerr, pushPo, notPushPo } = this.state;
        var result_pushed = _.filter(Object.keys(this.state.pushedHeaders), function (data) {
            return _.startsWith(data, searchPushedPo.toLowerCase())
        });
        var result_notPushed = _.filter(Object.keys(this.state.notPushedHeaders), function (data) {
            return _.startsWith(data, searchPushedNotPo.toLowerCase())
        });
        return (
            <div className="modal  display_block" id="downloadPopiReport">
                <div className="backdrop display_block"></div>
                <div className=" display_block">
                    <div className="modal-content modalRight createShipmentModal width-34 qualityCheckRight text-initial chatModalHeight">
                        <div className="col-md-12 pad-0">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop  text-left">
                                    <div className="col-md-10 pad-0 ht-45">
                                        <h3 className="download_h3"> Download Report</h3>
                                    </div>
                                    <div className="col-md-2 pad-0 ht-45">
                                        <button type="button" onClick={(e) => this.props.closeReport(e)} className="closeButton" id="closeButton"><span>Close</span>
                                        </button>
                                    </div>
                                    <div className="col-md-12 pad-0 ">
                                        <div className="purchase_dR">
                                            <div className="col-md-6 pad-0 left_dr">
                                                <div className="settingCheck m-top-bot-10 m-left-8">
                                                    <label className="checkBoxLabel0 text-middle displayPointer"><input type="checkBox" checked={pushPo} onChange={(e) => this.handleCheck(e)} id="pushPo" />Header Level Data<span className="checkmark1"></span> </label>

                                                </div>
                                            </div>
                                            <div className="col-md-6 pad-0 right_dr">
                                                <div className="settingCheck m-top-bot-10 m-left-8">
                                                    <label className="checkBoxLabel0 text-middle displayPointer"><input type="checkBox" checked={notPushPo} onChange={(e) => this.handleCheck(e)} id="notPushPo" />Line Item level Data<span className="checkmark1"></span> </label>

                                                </div>
                                            </div>

                                            <div className="col-md-6 pad-0 left_dr">
                                                <input type="text" autoComplete="off" onChange={e => this.handleChange(e)} name="searchPushedPo" id="searchPushedPo" value={searchPushedPo} placeholder="Search..." className="orgnisationTextbox m-lft-top-10 " />
                                                <ul className=" width-192 pad-0 ht-330 m-top-10" >
                                                    {result_pushed.length != 0 ? result_pushed.map((data, key) => (
                                                        <li tabIndex="-1" className="liFocus" key={key}>
                                                            <label className=" checkBoxLabelPad checkBoxLabel0 displayPointer checkBoxFocus checkbox_label"><input type="checkBox" className="checkBoxTab" checked={this.state.downloadPushedHeaders[data] != undefined} onClick={(e) => this.onPushedheaderSelect(data, this.state.pushedHeaders[data])} /> <span className="checkmark1"></span> {this.state.pushedHeaders[data]}</label>
                                                        </li>


                                                    )) : <li>
                                                            <label className="m-left-13">No data found</label>

                                                        </li>}


                                                </ul>

                                            </div>
                                            <div className="col-md-6 pad-0 right_dr">
                                                <input type="text" autoComplete="off" onChange={e => this.handleChange(e)} name="searchPushedNotPo" id="searchPushedNotPo" value={searchPushedNotPo} placeholder="Search..." className="orgnisationTextbox m-lft-top-10" />
                                                <ul className=" width-192 pad-0 ht-330 m-top-10" >
                                                    {result_notPushed.length != 0 ? result_notPushed.map((data, key) => (
                                                        <li tabIndex="-1" className="liFocus" key={key}>
                                                            <label className=" checkBoxLabelPad checkBoxLabel0 displayPointer checkBoxFocus checkbox_label"><input type="checkBox" className="checkBoxTab" checked={this.state.downloadNotPushedHeaders[data] != undefined} onClick={(e) => this.onHeaderSelect(data, this.state.notPushedHeaders[data])} /> <span className="checkmark1"></span> {this.state.notPushedHeaders[data]}</label>
                                                        </li>


                                                    )) : <li>
                                                            <label className="m-left-13">No data found</label>

                                                        </li>}

                                                </ul>
                                            </div>
                                            <div className="col-lg-12 pad-0">
                                                <div className="col-lg-12 manage-pad-range">
                                                    <label>Date Range </label>
                                                    {/* <input type="date" name="date" className="pd-date-input" /> */}
                                                    <RangePicker picker="date" id="rangePicker" onChange={(e) => this.handleFilter(e)}
                                                        defaultValue={[this.state.startDate, this.state.endDate]}
                                                        format={dateFormat}
                                                    />
                                                </div>
                                            </div>
                                            {/* <div className="col-md-12 pad-0 ">
                                                <input type="date" className="inputDate input_txt" id="startDate" placeholder={this.state.startDate == "" ? "Select Date" : this.state.startDate} value={this.state.startDate} onChange={this.handleRequestedDate} />
                                                {startDateerr ? (
                                                    <span className="error m-l-20">
                                                        From Date
                                                    </span>
                                                ) : null}
                                            </div> */}

                                            {/* <div className="col-md-6 pad-0 ">
                                                <input type="date" className="inputDate input_txt" min={this.state.startDate} id="endDate" placeholder={this.state.endDate == "" ? "Select Date" : this.state.endDate} value={this.state.endDate} onChange={this.handleRequestedDate} />
                                                {endDateerr ? (
                                                    <span className="error m-l-20">
                                                        To Date
                                                    </span>
                                                ) : null}
                                            </div> */}
                                        </div>
                                        {!this.state.downloading ? <div className="col-md-12 pad-0 m-top-20">

                                            <button className="download_report" type="button" onClick={(e) => this.downloadReport(e)}>
                                                Download Report

                                                  </button>
                                        </div> :
                                            <div className="col-md-12 pad-0 m-top-20">
                                                <button className="downloading_report" type="button" disabled>
                                                    Downloading....

                                                  </button>
                                            </div>}



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}


            </div>
        );
    }
}

export default DownloadReport;
