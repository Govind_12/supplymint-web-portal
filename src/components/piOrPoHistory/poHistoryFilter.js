import React from "react";

class PoHistoryFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: "",
            no: "",
            orderDate: "",
            orderNo: "",
            validFrom: "",
            validTo: "",
            slName: "",
            slCityName: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: "",
            validFromerr: false,
            validToerr: false
        };
    }

    handleChange(e) {
        if (e.target.id == "orderNo") {
            this.setState({
                orderNo: e.target.value
            });
        } else if (e.target.id == "supplierName") {
            this.setState({
                slName: e.target.value
            });
        } else if (e.target.id == "supplierCityName") {
            this.setState({
                slCityName: e.target.value
            });
        } else if (e.target.id == "orderDate") {
            e.target.placeholder = e.target.value
            this.setState({
                orderDate: e.target.value
            });
        } else if (e.target.id == "validFrom") {
            e.target.placeholder = e.target.value
            this.setState({
                validFrom: e.target.value
            }, () => {
                this.validDateFrom();
            });
        } else if (e.target.id == "validTo") {
            e.target.placeholder = e.target.value
            this.setState({
                validTo: e.target.value
            }, () => {
                this.validDateTo();
            });
        } else if (e.target.id == "hl1Name") {
            e.target.placeholder = e.target.value
            this.setState({
                hl1Name: e.target.value
            });
        }
        else if (e.target.id == "hl2Name") {
            e.target.placeholder = e.target.value
            this.setState({
                hl2Name: e.target.value
            });
        }
        else if (e.target.id == "hl3Name") {
            e.target.placeholder = e.target.value
            this.setState({
                hl3Name: e.target.value
            });
        }

    }

    clearFilter(e) {
        this.setState({
            type: "",
            no: "",
            orderDate: "",
            orderNo: "",
            validFrom: "",
            validTo: "",
            slName: "",
            slCityName: "",
            hl1Name: "",
            hl2Name: "",
            hl3Name: ""
        })

    }

    validDateFrom() {
        if (this.state.validFrom == "") {
            this.setState({
                validFromerr: true
            })
        } else {
            this.setState({
                validFromerr: false
            })
        }
    }
    validDateTo() {
        if (this.state.validTo == "") {
            this.setState({
                validToerr: true
            })
        } else {
            this.setState({
                validToerr: false
            })
        }
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.state.validFrom != "") {
            this.validDateTo()
        }
        setTimeout(() => {
            const { validFromerr, validToerr } = this.state
            if (!validFromerr && !validToerr) {

               
                let data = {
                    no: 1,
                    type: 2,
                    orderDate: this.state.orderDate,
                    orderNo: this.state.orderNo.trim(),
                    validFrom: this.state.validFrom,
                    validTo: this.state.validTo,
                    slName: this.state.slName.trim(),
                    slCityName: this.state.slCityName.trim(),
                    hl1Name: this.state.hl1Name.trim(),
                    hl2Name: this.state.hl2Name.trim(),
                    hl3Name: this.state.hl3Name.trim(),
                    slCode: "",
                    search: ""
                }
         
                
                this.props.poHistoryRequest(data);
                this.props.closeFilter(e);
                this.props.updateFilter(data)
                setTimeout(() => {
                    this.setState({
                        orderDate: "",
                        orderNo: "",
                        validFrom: "",
                        validTo: "",
                        slName: "",
                        slCityName: "",
                        hl1Name: "",
                        hl2Name: "",
                        hl3Name: ""
                    })
                }, 10);
            }

        }, 10);
    }

    render() {
        let count = 0;
        if (this.state.orderNo != "") {
            count++;
        }
        if (this.state.orderDate != "") {
            count++;
        }
        if (this.state.slName != "") {
            count++;
        }
        if (this.state.slCityName != "") {
            count++;
        }
        if (this.state.validFrom != "") {
            count++;
        }
        if (this.state.validTo != "") {
            count++;
        }
        if (this.state.hl1Name != "") {
            count++;
        }
        if (this.state.hl2Name != "") {
            count++;
        }
        if (this.state.hl3Name != "") {
            count++;
        }
        const { orderNo, orderDate, slName, slCityName, validFrom, validTo, hl1Name, hl2Name, hl3Name, validFromerr, validToerr } = this.state
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0 poHistoryFilterInput">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="orderNo" value={orderNo} placeholder="Order No." className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="hl1Name" value={hl1Name} placeholder="Division" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="hl2Name" value={hl2Name} placeholder="Section" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="hl3Name" value={hl3Name} placeholder="Department" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} placeholder="Supplier Name" id="supplierName" value={slName} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} placeholder="Supplier City Name" id="supplierCityName" value={slCityName} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} placeholder={this.state.orderDate == "" ? "Order Date" : this.state.orderDate} id="orderDate" value={orderDate} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} placeholder={this.state.validFrom == "" ? "Valid From" : this.state.validFrom} id="validFrom" value={validFrom} className="organistionFilterModal" />
                                        {validFromerr ? (
                                            <span className="error">
                                                Select Valid Date From
                                            </span>
                                        ) : null}
                                    </li>
                                    <li>
                                    {validFrom == "" ? <input type="date" placeholder="Valid To"  className="organistionFilterModal" disabled/> :
                                     <input type="date" onChange={(e) => this.handleChange(e)} placeholder={this.state.validTo == "" ? "Valid To" : this.state.validTo} id="validTo" value={validTo} className="organistionFilterModal" />}
                                        {validToerr ? (
                                            <span className="error">
                                                Select Valid Date From
                                            </span>
                                        ) : null}
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                            CLEAR FILTER
                     </button>
                                    </li>
                                    <li>
                                        {this.state.orderDate != "" || this.state.orderNo != "" || this.state.slName != "" || this.state.slCityName != "" || this.state.validFrom != "" || this.state.validTo != "" || this.state.hl1Name != "" || this.state.hl2Name != "" || this.state.hl3Name != "" ? <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default PoHistoryFilter;
