import React from 'react';


class ViewImage extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            imagePathData: []
        }
    }

    componentDidMount(){
        let payload ={
            path: this.props.imagePath
        }
        this.props.piImageUrlRequest(payload)
    }

    componentDidUpdate() {
        if (this.props.purchaseIndent.piImageUrl.isSuccess) {
            this.setState({
                imagePathData: this.props.purchaseIndent.piImageUrl.data.resource !== null && this.props.purchaseIndent.piImageUrl.data.resource.length > 0 ?
                           this.props.purchaseIndent.piImageUrl.data.resource : [] 
            })  
            this.props.piImageUrlClear()
        }
    }

    viewImage =(path)=>{
        window.location.href = path
    }

    render () {
        const{imagePathData} = this.state;
        return (
            <React.Fragment>
                <div className="backdrop-transparent"></div>
                <div className="item-detail-view-images-modal">
                    <div className="idvim-head">
                        <div className="idvimh-left">
                            <h3>Image Viewer <p>Total images: {imagePathData.length}</p></h3>
                        </div>
                        <button type="button" className="idvimh-close" onClick={this.props.CloseViewImage}><img src={require('../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="idvim-body">
                        <div className="idvimb-manage">
                            {imagePathData.length > 0 ? imagePathData.map( (data,key) => <div key={key} className="idvimb-images">
                                {/* <img src={data.url.split("?")[0]} /> */}
                                <img src={data.url} />
                                <label>{data.fileName}</label>
                                <div className="idvimb-backdrop">
                                    <button type="button" onClick={()=> window.location.href = data.url}>
                                        <svg xmlns="http://www.w3.org/2000/svg" id="down-arrow" width="14" height="14" viewBox="0 0 17.305 17.305">
                                            <g id="Group_3272" transform="translate(0 10.579)">
                                                <g id="Group_3271">
                                                    <path fill="#fff" id="Path_1049" d="M15.953 313v4.7a.677.677 0 0 1-.676.676H2.028a.677.677 0 0 1-.676-.676V313H0v4.7a2.03 2.03 0 0 0 2.028 2.028h13.249a2.03 2.03 0 0 0 2.028-2.028V313z" class="cls-1" transform="translate(0 -313)"/>
                                                </g>
                                            </g>
                                            <g id="Group_3274" transform="translate(4.452)">
                                                <g id="Group_3273">
                                                    <path fill="#fff" id="Path_1050" d="M139.161 7.967l-2.569 2.569V0h-1.352v10.536l-2.569-2.569-.956.956 4.2 4.2 4.2-4.2z" class="cls-1" transform="translate(-131.716)"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </button>
                                </div>
                            </div>) : null }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default ViewImage;