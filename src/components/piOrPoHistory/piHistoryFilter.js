import React from "react";

class PiHistoryFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: "",
            no: "",
            vendorDistrict: "",
            vendorState: "",
            deliveryDateFrom: "",
            deliveryDateTo: "",
            description: "",
            dateFrom: false,
            dateTo: false
        };
    }
    handleChange(e) {
        if (e.target.id == "vendorDistrict") {
            this.setState({
                vendorDistrict: e.target.value
            });
        } else if (e.target.id == "vendorState") {
            this.setState({
                vendorState: e.target.value
            });
        } else if (e.target.id == "description") {
            this.setState({
                description: e.target.value
            });
        }
        else if (e.target.id == "deliveryDateFrom") {
            e.target.placeholder = e.target.value
            this.setState({
                deliveryDateFrom: e.target.value
            }, () => {
                this.deliveryDateFrom();
            });
        }
        else if (e.target.id == "deliveryDateTo") {
            e.target.placeholder = e.target.value
            this.setState({
                deliveryDateTo: e.target.value
            }, () => {
                this.deliveryDateTo();
            });
        }
       
    }
    deliveryDateFrom() {
        if (this.state.deliveryDateFrom == "") {
            this.setState({
                dateFrom: true
            })
        } else {
            this.setState({
                dateFrom: false
            })
        }
    }
    deliveryDateTo() {
        if (this.state.deliveryDateTo == "") {
            this.setState({
                dateTo: true
            })
        } else {
            this.setState({
                dateTo: false
            })
        }
    }

    clearFilter(e) {
        this.setState({
            type: "",
            no: "",
            vendorDistrict: "",
            deliveryDateFrom: "",
            deliveryDateTo: "",
            description: "",
            dateFrom : "",
            dateTo :""
        })
    }
    onSubmit(e) {
        e.preventDefault();
    
        if(this.state.deliveryDateFrom != ""){
            this.deliveryDateTo();
        }
        setTimeout(()=>{
         
            const { dateTo, dateFrom} = this.state;
            if (!dateFrom && !dateTo ) {
               
                let data = {
                    no: 1,
                    type: 2,
                    slCityName: this.state.vendorDistrict,
                    desc2Code: this.state.description,
                    deliveryDateFrom: this.state.deliveryDateFrom,
                    deliveryDateTo: this.state.deliveryDateTo,
                    search: ""
                }
                this.props.piHistoryRequest(data);
                this.props.closeFilter(e);
                this.props.updateFilter(data)
                this.setState({
                    vendorDistrict: "",
                    deliveryDateFrom: "",
                    deliveryDateTo: "",
                    description: "",
                    dateFrom : "",
                    dateTo :""
                })
            }
           
        },10)

    }
    render() {
        let count = 0;
        if (this.state.vendorDistrict != "") {
            count++;
        }
        // if (this.state.vendorState != "") {
        //     count++;
        // } 
        if (this.state.description != "") {
            count++;
        }
        if(this.state.deliveryDateFrom != ""){
            count++;
        }
        if(this.state.deliveryDateTo != ""){
            count++;
        }
        const { vendorDistrict, vendorState, description, deliveryDateFrom, deliveryDateTo, dateFrom,dateTo } = this.state
     
        
        return (
            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS
                                    </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                                    </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0 piHistoryFilterForm">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="vendorDistrict" value={vendorDistrict} placeholder="City Name" className="organistionFilterModal" />
                                    </li>
                                    {/* <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} placeholder="Vendor State" id="vendorState" value={vendorState} className="organistionFilterModal" />
                                    </li> */}
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} placeholder={description!=""?description:"Description 2"} id="description" value={description} className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="date" onChange={(e) => this.handleChange(e)} placeholder={deliveryDateFrom!=""?deliveryDateFrom:"Delivery Date From"} id="deliveryDateFrom" value={deliveryDateFrom} className="organistionFilterModal" />
                                        {dateFrom ? (
                                            <span className="error">
                                                Select delivery Date From
                                            </span>
                                        ) : null}
                                    </li>
                                    <li>
                                    {deliveryDateFrom == "" ? <input type="date" placeholder="Delivery Date To"  className="organistionFilterModal" disabled/> :

                                        <input type="date" onChange={(e) => this.handleChange(e)} placeholder={deliveryDateTo!=""?deliveryDateTo:" Delivery Date To"} id="deliveryDateTo" value={deliveryDateTo} className="organistionFilterModal" />}
                                        {dateTo ? (
                                            <span className="error">
                                                Select delivery Date To
                                            </span>
                                        ) : null}
                                    </li>

                                    {/* <li>
                                        <select id="status" onChange={(e) => this.handleChange(e)} className="organistionFilterModal" value={status} onChange={(e) => this.handleChange(e)}>
                                            <option value="">
                                                Status
                                                </option>
                                            <option value="PENDING">PENDING</option>
                                            <option value="APPROVED">APPROVED</option>
                                            <option value="REJECTED">REJECTED</option>
                                        </select>
                                    </li> */}
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                            CLEAR FILTER
                                        </button>
                                    </li>
                                    <li>
                                    {this.state.vendorDistrict != "" || this.state.deliveryDateFrom != "" || this.state.deliveryDateTo != "" || this.state.description != "" ?  <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button>: <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                            APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default PiHistoryFilter;
