import React from 'react';
import LineItemDataPo from './lineItemDataPo';
import PoError from '../loaders/poError'
import CircleTick from '../../assets/circle-white-tick.svg';
import { update } from 'immutable';



class ViewDetailPo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isRemark : false,
            statusType : '',
            lineItemData: [],
            poErrorMsg: false,
            errorMassage: "",

            poRows: [{

                vendorMrp: "",
                vendorDesign: "",
                mrk: [],
                discount: {
                    discountType: "",
                    discountValue: "",
                    discountPer: true
                },

                finalRate: 0,
                rate: "",
                netRate: "",
                rsp: "",
                mrp: "",
                quantity: "",
                amount: "",
                otb: "",
                remarks: "",
                gst: [],
                finCharges: [],

                tax: [],
                calculatedMargin: [],
                gridOneId: 1,
                deliveryDate: "",

                marginRule: "",
                //new
                articleCode: "",
                articleName: "",
                departmentCode: "",
                departmentName: "",
                sectionCode: "",
                sectionName: "",
                divisionCode: "",
                divisionName: "",
                itemCodeList: [],

                itemCodeSearch: "",
                hsnCode: "",
                hsnSacCode: "",
                mrpStart: "",
                mrpEnd: "",
                catDescHeader: [],
                catDescArray: [],
                itemUdfHeader: [],
                itemUdfArray: [],
                lineItem: [{

                    colorChk: false,
                    icodeChk: false,
                    colorSizeList: [],
                    icodes: [],
                    itemBarcode: "",
                    setHeaderId: "",
                    gridTwoId: 1,
                    color: [],
                    colorList: [],
                    colorSearch: "",
                    sizes: [],
                    sizeList: [],
                    sizeSearch: "",
                    ratio: [],
                    size: "",
                    setRatio: "",
                    option: "",
                    setNo: 1,
                    total: "",
                    setQty: "",
                    quantity: "",
                    amount: "",
                    rate: "",
                    image: [],
                    imageUrl: {},
                    imagePath: "",
                    containsImage: false,
                    gst: "",
                    finCharges: [],
                    tax: "",
                    otb: "",
                    calculatedMargin: "",
                    mrk: "",
                    setUdfHeader: [],
                    setUdfArray: [],
                    lineItemChk: false,
                    sizeType: "complex",
                    totalBasic: "", // outer Basic

                    actionExpand: false,
                    prevId: "",
                    dropOpen: false,
                    expandedId: "",
                    expandedLineItem: [],
                }]

            }],
        }
    }

    componentDidMount(){
        let outerHeadersTemp = {}
        let tempHeaders = _.cloneDeep(this.props.lineItemCustomHeaders)
        outerHeadersTemp = tempHeaders

        delete outerHeadersTemp.catDescHeader
        delete outerHeadersTemp.itemUdfHeader
        delete outerHeadersTemp.lineItem

        this.setState({
            selectedId: this.props.selectedId,
            outerHeaders: outerHeadersTemp
        })
    }

    oncloseErr() {
        this.setState({
            poErrorMsg: true,
            errorMassage: "No data found"
        })
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false
        })
    }
    updateData = () => {
        let lineItemTempdata = []
        let tempData = _.cloneDeep(this.state.lineItemData)
        tempData.forEach((data1, key1) => {
            let updatedLineItem = [];
            if (data1.lineItem.length != 0) {
                data1.lineItem.map(item => {
                    if (item.lineWiseItem.length != 0){
                        item.lineWiseItem.map(lineItm => {
                            updatedLineItem.push(lineItm)
                        })
                    }
                })
            }
                let data = {
                    id: data1.id,
                    discount: {
                        discountType: data1.discountType,
                        discountValue: data1.discountValue,
                        discountPer: true
                    },
                    design: data1.design,
                    itemId: data1.itemId,
                    intakeMargin: data1.intakeMargin,
                    totalBasic: data1.totalBasic,
                    finalRate: data1.finalRate,
                    rate: data1.rate,
                    rsp: data1.rsp,
                    mrp: data1.mrp,
                    totalQty: data1.totalQty,
                    netAmountTotal: data1.netAmountTotal,
                    totalOtb: data1.totalOtb,
                    remarks: data1.remarks,
                    gst: data1.gst,
                    totalTax: data1.totalTax,
                    calculatedMargin: data1.calculatedMargin,
                    gridOneId: 1,
                    deliveryDate: data1.deliveryDate,
        
                    marginRule: data1.marginRule,
                    hl4Code: data1.hl4Code,
                    hl4Name: data1.hl4Name,
                    hl3Code: data1.hl3Code,
                    hl3Name: data1.hl3Name,
                    hl2Code: data1.hl2Code,
                    hl2Name: data1.hl2Name,
                    hl1Code: data1.hl1Code,
                    hl1Name: data1.hl1Name,
                    hsnCode: data1.hsnCode,
                    hsnSacCode: data1.hsnSacCode,
                    mrpStart: data1.mrpStart,
                    mrpEnd: data1.mrpEnd,
                    catDescArray: data1.catDescArray,
                    itemUdfArray: data1.itemUdfArray,
                    lineItem:  updatedLineItem,
            }
            lineItemTempdata.push(data)
        })

    this.setState({
        lineItemRows: lineItemTempdata
    })
}

    componentDidUpdate(prev){
        if (this.props.purchaseIndent.getLineItem.isSuccess) {
            if (this.props.purchaseIndent.getLineItem.data.resource != null) {
                this.setState({
                    loader: false,
                    lineItemData: this.props.purchaseIndent.getLineItem.data.resource.details != null ? this.props.purchaseIndent.getLineItem.data.resource.details : []
                },() => this.updateData())
            }
        } else if (this.props.purchaseIndent.getLineItem.isError) {
            this.oncloseErr();
            this.setState({
                loader: false,
            })
        }
        if (this.props.purchaseIndent.getLineItem.isLoading){

        }
        this.props.getLineItemClear()
    }
    statusUpdate = (status) => {
        this.statusUpdateConfirm(status, '');
    }

    oncloseErr() {
        this.setState({
            poErrorMsg: true,
            errorMassage: "No data found"
        })
    }
    statusUpdateConfirm = (status, remark) => {
        if (this.state.selectedId.length > 0) {
            this.props.closeDetails();
            let idArray = [];
            idArray.push(this.state.selectedId);
            let payloadForStatus = {
                patterns: idArray,
                status: status == '' ? this.state.statusType : status,
                remark: remark
            }
            let payloadForHistory = {
                no: this.props.current,
                type: (this.props.search !== undefined || this.props.search !== "") ? 3 : this.props.type == 3 || this.props.type == 4 ? 4 : 2,
                search: this.props.search,
                sortedBy: this.props.filterKey,
                sortedIn: this.props.filterType,
                filter: this.props.filteredValue,
            }
            let payload = {
                payloadForStatus: payloadForStatus,
                payloadForHistory: payloadForHistory,
            }
            this.props.po_approve_reject_Request(payload)
            this.setState({
                selectedId: [],
                currentStatus: [],
                indentIdForPdf: '',
                statusForPdf: '',
            })
        }
        else {
            this.oncloseErr();
        }
    }

    expandColumn =(e,id)=> {
        if (!this.state.actionExpand || this.state.prevId !== id) {
            let expandedLineItem = this.state.lineItemRows.filter( item => item.id == id)
            this.setState({ actionExpand: true, prevId: id, expandedId: id, dropOpen: true, expandedLineItem })
        }else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false, expandedLineItem:[]})
        }  
    }

    render() {
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content line-item-view-data-modal view-details">
                <div className="livdm-head">
                        <h3>Order Details</h3>
                        <div className="livdmh-right">
                            {this.props.statusVal == "PENDING" ? <button type="button" className="gen-approved" onClick={(e) => this.statusUpdate("APPROVED")} ><img src={CircleTick} />Approve</button> 
                            : <button type="button" className="gen-approved btnDisabled"><img src={CircleTick} />Approve</button>}
                            {this.props.statusVal == "PENDING" ? <button type="button" className="gen-cancel" value="check" onClick={(e) => this.statusUpdate("REJECTED")}>Reject</button> 
                            : <button type="button" className="gen-cancel btnDisabled" value="check" >Reject</button>}
                            <button type="button" className="" onClick={(e) => this.props.closeDetails(e)}>Close</button>
                        </div>
                    </div>
                    <div className="vd-body">
                        <div className="vd-modal-table m-top-20">
                            <div className="vdmt-manage">
                                <table className="table">
                                <thead>
                                        <tr>
                                            <th className="fix-action-btn"><label></label></th>
                                            {
                                                this.state.outerHeaders != undefined ? Object.keys(this.state.outerHeaders).map((data, key) => (
                                                    <th><label>{ this.state.outerHeaders[data]}</label></th>
                                                )): null
                                            }
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.lineItemRows != undefined ? this.state.lineItemRows.length != 0 ? this.state.lineItemRows.map((data, key) => (
                                                <React.Fragment key={key}>
                                                    <tr id={key}>
                                                        <td className="fix-action-btn">
                                                            <ul className="table-item-list">
                                                                {/* {this.state.showLineItem === false ?
                                                                <li className="til-inner til-add-btn" onClick={(e) => this.viewLineItem(e)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z" />
                                                                    </svg>
                                                                </li> :
                                                                <li className="til-inner til-add-btn" onClick={(e) => this.closeLineItem(e)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z" />
                                                                    </svg>
                                                                </li>} */}
                                                                {this.state.dropOpen && this.state.expandedId == data.id ?
                                                                <li className="til-inner til-add-btn" onClick={(e)=>this.expandColumn(e,data.id)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fillRule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z" />
                                                                    </svg> 
                                                                </li>:
                                                                <li className="til-inner til-add-btn" onClick={(e)=>this.expandColumn(e,data.id)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                        <path fill="#a4b9dd" fillRule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z" />
                                                                    </svg>
                                                                </li>}
                                                                <span className="generic-tooltip">Expand</span>
                                                            </ul>
                                                        </td>
                                                        {Object.keys(this.state.outerHeaders).length > 0 ? Object.keys(this.state.outerHeaders).map((hdata, key) => (
                                                            hdata == "discount" ? 
                                                            <td key={key} className="disinput"><label>{data[hdata].discountValue}</label></td>
                                                            :
                                                            <td key={key} className="disinput"><label>{data[hdata]}</label></td>
                                                        )) : null}
                                                    </tr>
                                                    {this.state.dropOpen && this.state.actionExpand && this.state.expandedId == data.id ? <tr><td colSpan="100%" className="pad-0"><LineItemDataPo {...this.props} {...this.state} isSet={this.props.isSet}/></td></tr> : null}
                                                </React.Fragment>
                                            )) : null : <tr className="tableNoData"><td> NO DATA FOUND </td></tr>
                                        }
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                {this.state.poErrorMsg ? <PoError oncloseErr={(e) => this.oncloseErr(e)} errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}

export default ViewDetailPo;