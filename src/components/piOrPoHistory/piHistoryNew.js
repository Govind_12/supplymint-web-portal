import React from "react";
import axios from 'axios';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import eyeIcon_closed from "../../assets/eye-closed.svg";
import eyeIcon_open from "../../assets/eye-open.svg";
import invoice from "../../assets/invoice.svg";
import editIcon from "../../assets/edit.svg";
import PoError from "../loaders/poError"
import rejectIcon from "../../assets/reject.svg";
import ApproveTag from "../../assets/approve-tag.svg";
import RejectTag from "../../assets/reject-tag.svg";
import PendingTag from "../../assets/pending-tag.svg";
import pendingApprove from "../../assets/exclamation-summary.svg";
import approve from "../../assets/approve.svg";
import Filter from "../../assets/filter-icon.svg";
import HeaderFilter from '../../assets/headerFilter.svg';
import ExportExcel from '../../assets/exportToExcel.svg';
import PdfDownload from '../../assets/pdfdownload.svg';
import NonSet from '../../assets/non-set-tag.svg';
import Set from '../../assets/set-tag.svg';
import { CONFIG } from "../../config/index";
import PiHistoryFilter from "./piHistoryFilter";
import PiInvoicePdfModal from "./piInvoicePdfModal";
import moment from "moment";
import ToastLoader from "../loaders/toastLoader";
import ColoumSetting from "../replenishment/coloumSetting";
import DownloadReportPi from "../piOrPoHistory/downloadReportPi";
import ConfirmationSummaryModal from "../replenishment/confirmationReset";
import LineItemData from "./lineItemDataPi"
import ConfirmModal from "../loaders/confirmModal"
import filterIcon from '../../assets/headerFilter.svg';
import VendorFilter from "../vendorPortal/vendorFilter";
import CircleTick from '../../assets/circle-white-tick.svg';
import RemarkModal from "./remarkModal";
const userType = "entpo"
import Reload from '../../assets/refresh-block.svg';
import "../../styles/main.scss";
import ViewDetailPi from "./viewDetailsPi";


class PiHistoryNew extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            isDownloadPdf: true,
            selectedStatus: '',
            selectedNo: '',
            statusType: '',
            remark: false,
            avgMargin: 0,
            totalAmout: 0,
            totalQty: 0,
            openRightBar: false,
            orderDetailId: "",
            rightbar: false,
            piHistoryState: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1,
            no: 1,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            filter: false,
            filterBar: false,

            search: "",

            toastLoader: false,
            toastMsg: "",
            piInvoiceModal: false,
            piStatus: "",
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],

            dReport: false,
            pushedPi: {},
            lineItemFixedHeaders: {},
            lineItemDefaultHeaders: {},
            lineItemCustomHeaders: {},
            lineItemData: [],
            viewDetails: false,
            poErrorMsg: false,
            errorMassage: "",
            deleteConfirmModal: false,
            deleteIndentNo: "",
            deleteStatus: "",
            filterKey: "",
            filterType: "",
            prevFilter: "",
            filterItems: this.props.replenishment.getHeaderConfig.data.resource != null ? this.props.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi == undefined ? this.props.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : this.props.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi : {},
            checkedFilters: [],
            filteredValue: [],
            applyFilter: false,
            showDownloadDrop: false,
            selectedId: [],
            currentStatus: [],
            enableDeleteButton: false,
            inputBoxEnable: false,
            fromPIQuantityValue: "",
            toPIQuantityValue: "",
            fromPIAmountValue: "",
            toPIAmountValue: "",
            fromCreationDate: "start date",
            toCreationDate: "end date",
            enableCopyIndent: false,
            enableApproveButton: false,
            enableRejectButton: false,
            enableCancelButton: false,
            exportToExcel: false,
            filterHistoryData: {},
            jumpPage: 1,
            totalPendingPi: "",
            totalPendingPo: "",
            statusForPdf: '',
            approvedPO: [],
            searchCheck: false,
            selectedItems: 0,
            selectAll: false,
            active: [],
            indentIdForPdf: '',
            editFlag: false,
            tagState: false,
            mainHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "PI_TABLE_HEADER",
                displayName: "TABLE HEADER"
            },
            setHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_ASN_APPROVAL_SET",
                basedOn: "SET"
            },
            itemHeaderPayload: {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "TABLE HEADER",
                displayName: "ENT_ASN_APPROVAL_ITEM",
                basedOn: "ITEM"
            },
            tabVal: "1",
            //main
            getMainHeaderConfig: [],
            mainFixedHeader: [],
            mainCustomHeadersState: [],
            mainHeaderSummary: [],
            mainDefaultHeaderMap: [],
            mainFixedHeaderData: [],
            mainHeaderConfigState: {},
            mainHeaderConfigDataState: {},
            mainCustomHeaders: {},
            mainAvailableHeaders: [],
            saveMainState: [],
            //set (consider as piDetails header except catDesc,itemUdf,setUdf,lineItem)
            getSetHeaderConfig: [],
            setFixedHeader: [],
            setCustomHeadersState: [],
            setCustomHeaders: {},
            setHeaderSummary: [],
            setDefaultHeaderMap: [],
            setFixedHeaderData: [],
            setHeaderConfigState: {},
            setHeaderConfigDataState: {},
            saveSetState: [],
            setHeaderCondition: false,
            setAvailableHeaders: [],
            //item
            itemHeaderSummary: [],
            itemDefaultHeaderMap: [],
            itemFixedHeaderData: [],
            itemHeaderConfigState: {},
            itemHeaderConfigDataState: {},
            itemCustomHeaders: {},
            saveItemState: [],
            itemHeaderCondition: false,
            getItemHeaderConfig: [],
            itemfixedHeader: [],
            itemCustomHeadersState: [],
            itemAvailableHeaders: [],
            
            //catDesc Header::
            getCatDescHeaderConfig: [],
            catDescFixedHeader: [],
            catDescCustomHeadersState: [],
            catDescCustomHeaders: {},
            catDescHeaderSummary: [],
            catDescDefaultHeaderMap: [],
            catDescFixedHeaderData: [],
            catDescHeaderConfigState: {},
            catDescHeaderConfigDataState: {},
            saveCatDescState: [],
            catDescHeaderCondition: false,
            catDescAvailableHeaders: [],
             
            //itemUdf Header::
            getItemUdfHeaderConfig: [],
            itemUdfFixedHeader: [],
            itemUdfCustomHeadersState: [],
            itemUdfCustomHeaders: {},
            itemUdfHeaderSummary: [],
            itemUdfDefaultHeaderMap: [],
            itemUdfFixedHeaderData: [],
            itemUdfHeaderConfigState: {},
            itemUdfHeaderConfigDataState: {},
            saveItemUdfState: [],
            itemUdfHeaderCondition: false,
            itemUdfAvailableHeaders: [],

            //setUdf Header::
            getSetUdfHeaderConfig: [],
            setUdfFixedHeader: [],
            setUdfCustomHeadersState: [],
            setUdfCustomHeaders: {},
            setUdfHeaderSummary: [],
            setUdfDefaultHeaderMap: [],
            setUdfFixedHeaderData: [],
            setUdfHeaderConfigState: {},
            setUdfHeaderConfigDataState: {},
            saveSetUdfState: [],
            setUdfHeaderCondition: false,
            setUdfAvailableHeaders: [],

            //lineItem Header::
            getLineItemHeaderConfig: [],
            lineItemFixedHeader: [],
            lineItemCustomHeadersState: [],
            lineItemCustomHeaders_: {},
            lineItemHeaderSummary: [],
            lineItemDefaultHeaderMap: [],
            lineItemFixedHeaderData: [],
            lineItemHeaderConfigState: {},
            lineItemHeaderConfigDataState: {},
            saveLineItemState: [],
            lineItemHeaderCondition: false,
            lineItemAvailableHeaders: [],

            changesInMainHeaders: false,
            changesInSetHeaders: false,
            changesInCatDescHeaders: false,
            changesInSetUdfHeaders: false,
            changesInItemUdfHeaders: false,
            changesInLineItemHeaders: false,

            dragOn: false,
            threeDotMenu: false,
            isSet: false,
        }
        this.cancelRef = React.createRef();
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDragEnter = this.handleDragEnter.bind(this);
        this.pushColumnData = this.pushColumnData.bind(this);
    }
    openThreeDotMenu(e) {
        e.preventDefault();
        this.setState({
            threeDotMenu: !this.state.threeDotMenu
        }, () => document.addEventListener('click', this.closeThreeDotMenu));
    }
    closeThreeDotMenu = () => {
        this.setState({ threeDotMenu: false }, () => {
            document.removeEventListener('click', this.closeThreeDotMenu);
        });
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }

    //new pdfdownload
    downloadPdf() {
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }

        let payload = {
            data: [{ "indentId": this.state.indentIdForPdf }]
        }

        this.setState({
            downloading: true
        })
        axios.post(`${CONFIG.BASE_URL}/admin/pi/download/pdf`, payload, { headers: headers })
            .then(res => {
                this.setState({
                    downloading: false
                })
                if (res.data.data.resource != null) {
                    if (res.data.data.resource[0].url == null) {
                        if(!this.state.isDownloadPdf){
                            this.setState({
                                isDownloadPdf: true
                            })
                        } else if(this.state.isDownloadPdf){
                            this.setState({
                                toastMsg: "PDF is generating, will download after few sec",
                                toastLoader: true
                            })
                            setTimeout(() => {
                                this.setState({
                                    toastLoader: false
                                })
                            }, 5000);
                            axios.post(`${CONFIG.BASE_URL}/admin/pi/regenerate/pdf`, payload, { headers: headers })
                            .then(res => {
                                if (res.data.data.message != '' && res.data.status == 2000){
                                    console.log('regnrate success')
                                    setTimeout(() => {
                                        this.downloadPdf() 
                                    }, 20000);
                                } else {
                                    alert("PDF is not generated yet")
                                }
                            })
                            this.setState({
                                isDownloadPdf: false
                            })
                        }
                    }
                    else {
                        var a = document.createElement('a');
                        a.target = '_blank'
                        a.href = res.data.data.resource[0].url;
                        a.innerText = '';
                        a.download = true;
                        document.body.appendChild(a);
                        a.click()
                    }


                } else {

                    this.props.oncloseErr()
                }
                this.props.closeReport()

            }).catch((error) => {
                this.setState({
                    downloading: false
                })

            });
    }
    showDownloadDrop(event) {
        event.preventDefault();


        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
        if (this.state.selectedId.length == 0) {
            alert("Please select data");
        }
        else {
            if (this.state.statusForPdf == "APPROVED") {
                this.downloadPdf();
            }
            else {
                alert("Status: Not Approved")
            }
        }
    }

    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    oncloseErr() {
        this.setState({
            poErrorMsg: true,
            errorMassage: "No data found"
        })
    }

    handleChange(e) {
        if (e.target.id == "search") {
            this.setState({
                search: e.target.value
            })
        }
    }
    viewPi(id, path) {


        let c = this.state.piHistoryState
        for (var i = 0; i < c.length; i++) {
            if (c[i].pattern == id) {

                this.setState({
                    orderDetailId: id,
                    piInvoiceModal: true,
                    piStatus: c[i].status
                })
                this.piDownload(path);
            }
        }
    }
    closePiInvoice() {
        this.setState({
            piInvoiceModal: !this.state.piInvoiceModal
        })
    }
    componentDidMount() {
        if (!this.props.replenishment.getHeaderConfig.isSuccess) {
            this.props.getHeaderConfigRequest(this.state.mainHeaderPayload)
        }
        if (!this.props.purchaseIndent.piHistory.isSuccess) {
            let filterObj = {};
            this.props.location.status != undefined && Object.keys(this.props.location.filter).map(key => {
                if (key == 'indentDate') {
                    filterObj['createdTime'] = { from: this.props.location.filter[key].split('|')[0], to: this.props.location.filter[key].split('|')[1] }
                }
                else {
                    filterObj[key] = this.props.location.filter[key];
                }
            })
            let data = {
                no: 1,
                type: this.props.location.type || 1,
                search: "",
                userType: userType,
                filter: { ...filterObj, status: this.props.location.status },
                filterItems: { ...this.props.location.filterItems }
            }
            if(this.props.location.status){
                this.setState({
                    tagState: true,
                })
            }
            this.props.piHistoryRequest(data);
            let filterValue = {};
            let filter = {};
            this.props.location.status != undefined && Object.keys(this.props.location.filter).map(key => {
                if (key == 'indentDate') {
                    filterValue[key] = { from: this.props.location.filter[key].split('|')[0], to: this.props.location.filter[key].split('|')[1] }
                }
                else {
                    filterValue[key] = this.props.location.filter[key]
                }
            })
            let checkValue = this.props.location.status != undefined && Object.keys(this.props.location.filter).map(key => {
                filter[this.state.filterItems[key]] = this.props.location.filter[key];
                return this.state.filterItems[key]
            })
            this.state.Status = this.props.location.status;
            this.setState({
                ...filter,
                loader: false,
                filteredValue: this.props.location.status != undefined ? { ...filterValue, status: this.props.location.status } : {},
                checkedFilters: this.props.location.status != undefined ? [...checkValue, 'Status'] : [],
                applyFilter: this.props.location.status != undefined ? true : false,
                inputBoxEnable: this.props.location.status != undefined ? true : false,
                type: this.props.location.status != undefined ? 2 : 1
            })
        } else
            if (this.props.purchaseIndent.piHistory.isSuccess) {
                if (this.props.purchaseIndent.piHistory.data.resource != null) {
                    this.setState({
                        piHistoryState: this.props.purchaseIndent.piHistory.data.resource,
                        prev: this.props.purchaseIndent.piHistory.data.prePage,
                        current: this.props.purchaseIndent.piHistory.data.currPage,
                        next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                        maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                        loader: false
                    })
                }

                else {
                    this.setState({
                        piHistoryState: this.props.purchaseIndent.piHistory.data.resource,
                        prev: this.props.purchaseIndent.piHistory.data.prePage,
                        current: this.props.purchaseIndent.piHistory.data.currPage,
                        next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                        maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                        loader: false
                    })
                }

            }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.purchaseIndent.piHistory.isSuccess) {

            if (nextProps.purchaseIndent.piHistory.data.resource != null) {
                return {
                    loader: false,
                    piHistoryState: nextProps.purchaseIndent.piHistory.data.resource,
                    prev: nextProps.purchaseIndent.piHistory.data.prePage,
                    current: nextProps.purchaseIndent.piHistory.data.currPage,
                    next: nextProps.purchaseIndent.piHistory.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.piHistory.data.maxPage,
                    jumpPage: nextProps.purchaseIndent.piHistory.data.currPage,
                    totalPendingPi: nextProps.purchaseIndent.piHistory.data.totalRecord,
                    approvedPO: nextProps.purchaseIndent.piHistory.data.resource == null ? [] : nextProps.purchaseIndent.piHistory.data.resource,
                    selectedItems: nextProps.purchaseIndent.poHistory.data.resultedDataCount,
                    avgMargin: nextProps.purchaseIndent.piHistory.data.avgMargin,
                    totalAmout: nextProps.purchaseIndent.piHistory.data.totalAmount,
                    totalQty : nextProps.purchaseIndent.piHistory.data.totalQty,
                }
            } else {
                return {
                    loader: false,
                    piHistoryState: nextProps.purchaseIndent.piHistory.data.resource,
                    prev: nextProps.purchaseIndent.piHistory.data.prePage,
                    current: nextProps.purchaseIndent.piHistory.data.currPage,
                    next: nextProps.purchaseIndent.piHistory.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.piHistory.data.maxPage,
                }
            }
        } else if (nextProps.purchaseIndent.piHistory.isError) {
            return {
                errorMessage: nextProps.purchaseIndent.piHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.piHistory.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.piHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.piHistory.message.error.errorCode,
                code: nextProps.purchaseIndent.piHistory.message.status,
                alert: true,
                loader: false
            }
        }
        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null) {

                let getHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi) : []
                let fixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi) : []
                let customHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi) : []

                let getMainHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi) : []
                let mainFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi) : []
                let mainCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi) : []
                let headerCondition = mainCustomHeadersState.length == 0 ? true : false
                let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi).indexOf(obj) == -1 })

                let getSetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail).filter( obj => typeof obj !== 'object') : []
                let setCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail).filter( obj => typeof obj !== 'object') : []
                let setHeaderCondition = setCustomHeadersState.length == 0 ? true : false
                let setAvailableHeaders = setCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail).filter( obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail).filter( obj => typeof obj !== 'object').indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail).filter( obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail).filter( obj => typeof obj !== 'object').indexOf(obj) == -1 })

                let getSetUdfHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem.setUdfHeader) : []
                let setUdfCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem.setUdfHeader) : [] : []
                let setUdfHeaderCondition = setUdfCustomHeadersState.length == 0 ? true : false
                let setUdfAvailableHeaders = setUdfCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem.setUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem.setUdfHeader).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem.setUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem.setUdfHeader).indexOf(obj) == -1 })

                let getItemUdfHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.itemUdfHeader) : []
                let itemUdfCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.itemUdfHeader) : []
                let itemUdfHeaderCondition = itemUdfCustomHeadersState.length == 0 ? true : false
                let itemUdfAvailableHeaders = itemUdfCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.itemUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.itemUdfHeader).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.itemUdfHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.itemUdfHeader).indexOf(obj) == -1 })

                let getCatDescHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.catDescHeader) : []
                let catDescCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.catDescHeader) : []
                let catDescHeaderCondition = catDescCustomHeadersState.length == 0 ? true : false
                let catDescAvailableHeaders = catDescCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.catDescHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.catDescHeader).indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.catDescHeader).filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.catDescHeader).indexOf(obj) == -1 })

                let getLineItemHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem).filter(obj => typeof obj !== 'object') : []
                let lineItemCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem).filter(obj => typeof obj !== 'object') : []
                let lineItemHeaderCondition = lineItemCustomHeadersState.length == 0 ? true : false
                let lineItemAvailableHeaders = lineItemCustomHeadersState.length !== 0 ?
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem).filter(obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem).filter(obj => typeof obj !== 'object').indexOf(obj) == -1 }) :
                    Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem).filter(obj => typeof obj !== 'object').filter(function (obj) { return Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem).filter(obj => typeof obj !== 'object').indexOf(obj) == -1 })
                return {
                    loader: false,
                    customHeaders: prevState.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi : {},
                    getHeaderConfig,
                    fixedHeader,
                    customHeadersState,
                    filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi,
                    // tableCustomHeader: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]),
                    // tableGetHeaderConfig: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]),
                    headerConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : {},
                    fixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi : {},
                    headerConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi } : {},
                    headerSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi) : [],
                    defaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi) : [],
                    pushedPi: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi == undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi,

                    lineItemDefaultHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail == undefined ? {} : nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail,
                    lineItemFixedHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail == undefined ? {} : nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail,

                    lineItemCustomHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail == undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail,

                    filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi : nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi,
                    mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi : {},
                    getMainHeaderConfig,
                    mainAvailableHeaders,
                    mainFixedHeader,
                    mainCustomHeadersState,
                    headerCondition,
                    mainHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi : {},
                    mainFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].pi : {},
                    mainHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi } : {},
                    mainHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].pi) : [],
                    mainDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].pi) : [],
                    mandateHeaderMain: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].pi != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].pi) : [],

                    setCustomHeaders: prevState.setHeaderCondition ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail, ['catDescHeader','itemUdfHeader','lineItem']) : _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail, ['catDescHeader','itemUdfHeader','lineItem']) != {} ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    getSetHeaderConfig,
                    setFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail).filter( obj => typeof obj !== 'object') : [],
                    setCustomHeadersState,
                    setHeaderCondition,
                    setAvailableHeaders,
                    setHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    setFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    setHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail ? _.omit({ ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail}, ['catDescHeader','itemUdfHeader','lineItem']) : {},
                    setHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail).filter( obj => obj !== 'catDescHeader' || obj !== 'itemUdfHeader' || obj !== 'lineItem') : [],
                    setDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail).filter( obj => obj !== 'catDescHeader' || obj !== 'itemUdfHeader' || obj !== 'lineItem') : [],
                    mandateHeaderSet: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail).filter( obj => typeof obj !== 'object') : [],

                    setUdfCustomHeaders: prevState.setUdfHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem.setUdfHeader : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem.setUdfHeader != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem.setUdfHeader : {} : {},
                    getSetUdfHeaderConfig,
                    setUdfFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem.setUdfHeader) : [],
                    setUdfCustomHeadersState,
                    setUdfHeaderCondition,
                    setUdfAvailableHeaders,
                    setUdfHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem.setUdfHeader : {},
                    setUdfFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem.setUdfHeader : {},
                    setUdfHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem.setUdfHeader} : {},
                    setUdfHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem.setUdfHeader) : [] : [],
                    setUdfDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem.setUdfHeader) : [],
                    mandateHeaderSetUdf: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail.lineItem != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail.lineItem.setUdfHeader) : [],

                    itemUdfCustomHeaders: prevState.itemUdfHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.itemUdfHeader : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.itemUdfHeader != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.itemUdfHeader : {} : {},
                    getItemUdfHeaderConfig,
                    itemUdfFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.itemUdfHeader) : [],
                    itemUdfCustomHeadersState,
                    itemUdfHeaderCondition,
                    itemUdfAvailableHeaders,
                    itemUdfHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.itemUdfHeader : {},
                    itemUdfFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.itemUdfHeader : {},
                    itemUdfHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.itemUdfHeader} : {},
                    itemUdfHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.itemUdfHeader) : [],
                    itemUdfDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.itemUdfHeader) : [],
                    mandateHeaderItemUdf: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail.itemUdfHeader) : [],

                    catDescCustomHeaders: prevState.catDescHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.catDescHeader : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail!= undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.catDescHeader != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.catDescHeader : {} : {},
                    getCatDescHeaderConfig,
                    catDescFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.catDescHeader) : [],
                    catDescCustomHeadersState,
                    catDescHeaderCondition,
                    catDescAvailableHeaders,
                    catDescHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.catDescHeader : {},
                    catDescFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.catDescHeader : {},
                    catDescHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.catDescHeader} : {},
                    catDescHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.catDescHeader) : [],
                    catDescDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.catDescHeader) : [],
                    mandateHeaderCatDesc: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail.catDescHeader) : [],

                    lineItemCustomHeaders_: prevState.lineItemHeaderCondition ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem, ['setUdfHeader']) : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem, ['setUdfHeader']) != {} ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem, ['setUdfHeader']) : {} : {},
                    getLineItemHeaderConfig,
                    lineItemFixedHeader: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem).filter( obj => typeof obj !== 'object') : [],
                    lineItemCustomHeadersState,
                    lineItemHeaderCondition,
                    lineItemAvailableHeaders,
                    lineItemHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem, ['setUdfHeader']) : {},
                    lineItemFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail != undefined ? _.omit(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"].piDetail.lineItem, ['setUdfHeader']) : {},
                    lineItemHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail ? _.omit({ ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem}, ['setUdfHeader']) : {},
                    lineItemHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"].piDetail.lineItem).filter( obj => obj !== 'setUdfHeader' ) : [],
                    lineItemDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"].piDetail.lineItem).filter( obj => obj !== 'setUdfHeader') : [],
                    mandateHeaderLineItem: nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail != undefined ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"].piDetail.lineItem).filter( obj => typeof obj !== 'object') : [],
                }


            }
        } else if (nextProps.replenishment.getHeaderConfig.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            }

        }
        // if (nextProps.replenishment.getMainHeaderConfig.isSuccess) {
        //     if (nextProps.replenishment.getMainHeaderConfig.data.resource != null &&
        //         nextProps.replenishment.getMainHeaderConfig.data.basedOn == "ALL") {
        //         let getMainHeaderConfig = nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : []
        //         let mainFixedHeader = nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]) : []
        //         let mainCustomHeadersState = nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : []
        //         let mainAvailableHeaders = mainCustomHeadersState.length !== 0 ? 
        //         Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
        //         Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
        //         return {
        //             filterItems: Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"],
        //             mainCustomHeaders: prevState.headerCondition ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] : {},
        //             getMainHeaderConfig,
        //             mainAvailableHeaders,
        //             mainFixedHeader,
        //             mainCustomHeadersState,
        //             mainHeaderConfigState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] : {},
        //             mainFixedHeaderData: nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getMainHeaderConfig.data.resource["Fixed Headers"] : {},
        //             mainHeaderConfigDataState: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] } : {},
        //             mainHeaderSummary: nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Custom Headers"]) : [],
        //             mainDefaultHeaderMap: nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getMainHeaderConfig.data.resource["Default Headers"]) : [],
        //             mandateHeaderMain: Object.values(nextProps.replenishment.getMainHeaderConfig.data.resource["Mandate Headers"])
        //         };
        // }
        // }
        // if (nextProps.replenishment.getSetHeaderConfig.isSuccess) {
        //     if (nextProps.replenishment.getSetHeaderConfig.data.resource != null &&
        //         nextProps.replenishment.getSetHeaderConfig.data.basedOn == "SET") {
        //         let getSetHeaderConfig = nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : []
        //         let setCustomHeadersState = nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]) : []
        //         let setAvailableHeaders = setCustomHeadersState.length !== 0 ? 
        //         Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
        //         Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
        //         return {
        //             filterItems: Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"],
        //             setCustomHeaders: prevState.setHeaderCondition ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] : {},
        //             getSetHeaderConfig,
        //             setFixedHeader: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"]) : [],
        //             setCustomHeadersState,
        //             setAvailableHeaders,
        //             setHeaderConfigState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] : {},
        //             setFixedHeaderData: nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getSetHeaderConfig.data.resource["Fixed Headers"] : {},
        //             setHeaderConfigDataState: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] } : {},
        //             setHeaderSummary: nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Custom Headers"]) : [],
        //             setDefaultHeaderMap: nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getSetHeaderConfig.data.resource["Default Headers"]) : [],
        //             mandateHeaderSet: Object.values(nextProps.replenishment.getSetHeaderConfig.data.resource["Mandate Headers"])
        //         };
        //     }
        // }
        // if (nextProps.replenishment.getItemHeaderConfig.isSuccess) {
        //     if (nextProps.replenishment.getItemHeaderConfig.data.resource != null &&
        //         nextProps.replenishment.getItemHeaderConfig.data.basedOn == "ITEM") {
        //         let getItemHeaderConfig = nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]) : []
        //         let itemCustomHeadersState = nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]) : []
        //         let itemAvailableHeaders = itemCustomHeadersState.length !== 0 ?
        //         Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]).indexOf(obj) == -1 }):
        //         Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"]).filter(function (obj) { return Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]).indexOf(obj) == -1 })
        //         return {
        //             filterItems: Object.keys(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]).length == 0 ? nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"],
        //             itemCustomHeaders: prevState.itemHeaderCondition ? nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] : {},
        //             getItemHeaderConfig,
        //             itemFixedHeader: nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"]) : [],
        //             itemCustomHeadersState,
        //             itemAvailableHeaders,
        //             itemHeaderConfigState: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] : {},
        //             itemFixedHeaderData: nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getItemHeaderConfig.data.resource["Fixed Headers"] : {},
        //             itemHeaderConfigDataState: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] } : {},
        //             itemHeaderSummary: nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getItemHeaderConfig.data.resource["Custom Headers"]) : [],
        //             itemDefaultHeaderMap: nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getItemHeaderConfig.data.resource["Default Headers"]) : [],
        //             mandateHeaderItem: Object.values(nextProps.replenishment.getItemHeaderConfig.data.resource["Mandate Headers"])
        //         };
        //     }
        // }
        if (nextProps.purchaseIndent.piUpdate.isSuccess) {
            return {
                loader: false,
                successMessage: nextProps.purchaseIndent.piUpdate.data.message,
                alert: false,
                success: true,
                isRemark: false,
                showLineItem: false,
            }
        } else if (nextProps.purchaseIndent.piUpdate.isError) {
            return {
                errorMessage: nextProps.purchaseIndent.piUpdate.message.error.errorMessage == undefined ? undefined : nextProps.purchaseIndent.piUpdate.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.piUpdate.message.error.errorCode == undefined ? undefined : nextProps.purchaseIndent.piUpdate.message.error.errorCode,
                code: nextProps.purchaseIndent.piUpdate.message.status,
                alert: true,
                success: false,
                loader: false
            }

        }

        if (nextProps.replenishment.createHeaderConfig.isSuccess) {
            return {
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
                loader: false,
                success: true,
            }

        } else if (nextProps.replenishment.createHeaderConfig.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            }

        }

        if (nextProps.purchaseIndent.getPiLineItem.isSuccess) {
            if (nextProps.purchaseIndent.getPiLineItem.data.resource != null) {
                return {
                    loader: false,
                    lineItemData: nextProps.purchaseIndent.getPiLineItem.data.resource != null ? nextProps.purchaseIndent.getPiLineItem.data.resource : []

                }
            }

        } else if (nextProps.purchaseIndent.getPiLineItem.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.getPiLineItem.message.status,
                errorCode: nextProps.purchaseIndent.getPiLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.getPiLineItem.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.getPiLineItem.message.error == undefined ? undefined : nextProps.purchaseIndent.getPiLineItem.message.error.errorMessage
            }

        } else if (nextProps.purchaseIndent.discardPoPiData.isSuccess) {

            return {
                successMessage: nextProps.purchaseIndent.discardPoPiData.data.message,
                loader: false,
                success: true,
                deleteIndentNo: "",
                deleteStatus: ""
            }
        } else if (nextProps.purchaseIndent.discardPoPiData.isError) {
            return {
                loader: false,
                alert: true,
                code: nextProps.purchaseIndent.discardPoPiData.message.status,
                errorCode: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorCode,
                errorMessage: nextProps.purchaseIndent.discardPoPiData.message.error == undefined ? undefined : nextProps.purchaseIndent.discardPoPiData.message.error.errorMessage
            }
        }


        if (nextProps.purchaseIndent.piHistory.isLoading ||
            nextProps.purchaseIndent.getPiLineItem.isLoading ||
            nextProps.replenishment.createHeaderConfig.isLoading ||
            nextProps.replenishment.getHeaderConfig.isLoading ||
            nextProps.purchaseIndent.piUpdate.isLoading ||
            nextProps.purchaseIndent.discardPoPiData.isLoading)
            return {
                loader: true
            }
        return {}
    }

    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            generatedDate: data.generatedDate,
            status: data.status,
            indentId: data.indentId,
            supplierName: data.supplierName,
            division: data.division,
            section: data.section,
            department: data.department,
            search: data.search,
            articleCode: data.articleCode
        });
    }
    onClearSearch(e) {
        e.preventDefault();
        if (this.state.type == 3 || this.state.type == 4) {
            let data = {
                no: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
            }
            this.props.piHistoryRequest(data);
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, no: 1, searchCheck: false })
        } else {
            this.setState({ search: "" })
        }
    }
    cancelActive(e, data) {
        let array = [...this.state.active]
        if (e.target.name == "selectEach" && !this.state.selectAll) {

            let deletedItem = {
                orderNumber: data.orderNumber, orderId: data.orderId, id: data.id, status: data.status == "PARTIAL" ? data.status : "CANCELLED", orgId: data.orgId, oldStatus: data.status, orderCode: data.orderCode,
                documentNumber: data.documentNumber, orderNumber: data.orderNumber,
                documentNumber: data.documentNumber, vendorCode: data.vendorCode,
                vendorName: data.vendorName, orderQty: data.poQty, enterpriseSite: data.siteDetails
                , orderDate: data.createdOnDate, orderAmount: data.poAmount
            }
            if (this.state.active.some((item) => item.id == data.id)) {
                array = array.filter((item) => item.id != data.id)
            } else {
                array.push(deletedItem)
            }
            this.setState({ active: array, enableDeleteButton: true })
        } else if (e.target.name == "selectAll" && array == "") {
            this.setState({ selectAll: !this.state.selectAll, enableDeleteButton: true })
        }
    }
    searchClear = () => {
        if (this.state.type === 3 || this.state.type == 4) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 2 : 1,
                search: "",
                status: "APPROVED",
                poDate: "",
                createdOn: "",
                poNumber: "",
                vendorName: "",
                userType: userType,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
            }
            this.props.piHistoryRequest(payload)
            this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })

        } else { this.setState({ search: "" }) }
    }
    searchIcon = () => {
        if (this.state.search.length) {
            let data = {
                type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                no: 1,
                search: this.state.search,
                filter: this.state.filteredValue,
            };
            this.props.piHistoryRequest(data);
            this.props.piHistoryDataUpdate(data)
            this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true })
        }
    }

    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let data = {
                    type: this.state.type == 2 || this.state.type == 4 ? 4 : 3,
                    no: 1,
                    search: this.state.search,
                    filter: this.state.filteredValue,
                };
                e.preventDefault();
                this.props.piHistoryRequest(data);
                this.setState({ type: this.state.type == 2 || this.state.type == 4 ? 4 : 3, searchCheck: true })
            }
        }
        this.setState({
            search: e.target.value,
            filterCount: 0,
        }, () => {
            if (this.state.search == "" && (this.state.type == 3 || this.state.type == 4)) {
                let data = {
                    type: this.state.type == 4 ? 2 : 1,
                    no: 1,
                    search: "",
                    filter: this.state.filteredValue,
                }
                e.preventDefault();
                this.props.piHistoryRequest(data);
                this.setState({ search: "", type: this.state.type == 4 ? 2 : 1, searchCheck: false, selectAll: false })
            }
        })
    }
    piDownload(path) {
        this.setState({
            loader: true,
            piPdf: ""
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        if (path != null) {
            axios.get(`${CONFIG.BASE_URL}/admin/pi/download/pdf?savedPath=${path}`, { headers: headers })
                .then(res => {
                    this.setState({
                        loader: false
                    })
                    if (res.data.data.resource != null) {
                        this.setState({
                            piPdf: res.data.data.resource
                        })

                    } else {
                        this.setState({
                            piPdf: ""
                        })
                    }
                }).catch((error) => {
                })
        }
    }
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.purchaseIndent.piHistory.data.prePage,
                    current: this.props.purchaseIndent.piHistory.data.currPage,
                    next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                })
                if (this.props.purchaseIndent.piHistory.data.currPage != 0) {
                    let data = {
                        no: this.props.purchaseIndent.piHistory.data.currPage - 1,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.filteredValue,

                    }
                    this.props.piHistoryRequest(data);
                }

            }

        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.piHistory.data.prePage,
                current: this.props.purchaseIndent.piHistory.data.currPage,
                next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.piHistory.data.currPage != this.props.purchaseIndent.piHistory.data.maxPage) {
                let data = {

                    no: this.props.purchaseIndent.piHistory.data.currPage + 1,
                    type: this.state.type,
                    search: this.state.search,
                    filter: this.state.filteredValue,


                }
                this.props.piHistoryRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.piHistory.data.prePage,
                current: this.props.purchaseIndent.piHistory.data.currPage,
                next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.piHistory.data.currPage <= this.props.purchaseIndent.piHistory.data.maxPage) {
                let data = {

                    no: 1,
                    type: this.state.type,
                    search: this.state.search,
                    filter: this.state.filteredValue,

                }
                this.props.piHistoryRequest(data)
            }

        } else if (e.target.value == "reload") {
            if (this.props.purchaseIndent.poHistory.data.currPage != undefined || this.props.purchaseIndent.poHistory.data.currPage != 0) {
                let data = {

                    no: this.props.purchaseIndent.poHistory.data.currPage,
                    type: this.state.type,
                    search: this.state.search,
                    // orderDate: this.state.orderDate,
                    // validFrom: this.state.validFrom,
                    // orderNo: this.state.orderNo,
                    // validTo: this.state.validTo,
                    // slCode: this.state.slCode,
                    // slCityName: this.state.slCityName,
                    // slName: this.state.slName,
                    // hl1Name: this.state.hl1Name,
                    // hl2Name: this.state.hl2Name,
                    // hl3Name: this.state.hl3Name,
                    filter: this.state.filteredValue,

                }
                this.props.piHistoryRequest(data);

            }

        }
        else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({

                    prev: this.props.purchaseIndent.piHistory.data.prePage,
                    current: this.props.purchaseIndent.piHistory.data.currPage,
                    next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                })
                if (this.props.purchaseIndent.piHistory.data.currPage <= this.props.purchaseIndent.piHistory.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.piHistory.data.maxPage,
                        search: this.state.search,
                        status: "APPROVED",
                        poDate: this.state.poDate,
                        createdOn: "",
                        poNumber: this.state.poNumber,
                        vendorName: this.state.vendorName,
                        userType: "vendorpo",
                        vendorId: this.state.vendorId,
                        vendorCity: this.state.vendorCity,
                        vendorState: this.state.vendorState,
                        validTo: this.state.validTo,
                        siteDetail: this.state.siteDetail,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                        filter: this.state.filteredValue
                    }
                    this.props.piHistoryRequest(data)
                }
            }
        }
    }



    componentDidUpdate(previousProps, previousState) {
        if (this.props.purchaseIndent.piHistory.isError || this.props.purchaseIndent.piHistory.isSuccess) {
            this.props.piHistoryClear()
            this.setState({selectedId: [], enableApproveButton: false, enableCancelButton: false, enableDeleteButton: false, enableRejectButton: false, currentStatus: []})
        }
        if (this.props.replenishment.getHeaderConfig.isError || this.props.replenishment.getHeaderConfig.isSuccess) {
            this.props.getHeaderConfigClear()
        }
        if (this.props.purchaseIndent.piUpdate.isSuccess) {
            this.props.piUpdateClear()
            this.closePiInvoice()
            this.setState({selectedId: [], enableApproveButton: false, enableCancelButton: false, enableDeleteButton: false, enableRejectButton: false, currentStatus: []})
        } else if (this.props.purchaseIndent.piUpdate.isError) {
            this.props.piUpdateClear()
        }
        if (this.props.replenishment.createHeaderConfig.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: "PI_TABLE_HEADER",
                displayName: "TABLE HEADER"
            }
            this.props.getHeaderConfigRequest(payload)
            this.props.createHeaderConfigClear()
        } else if (this.props.replenishment.createHeaderConfig.isSuccess || this.props.replenishment.createHeaderConfig.isError) {
            this.props.createHeaderConfigClear()
        }
        if (this.props.purchaseIndent.getLineItem.isError || this.props.purchaseIndent.getLineItem.isSuccess) {
            this.props.getLineItemClear()
        }
        if (this.props.purchaseIndent.discardPoPiData.isSuccess || this.props.purchaseIndent.discardPoPiData.isError) {
            this.props.discardPoPiDataClear()
            this.setState({selectedId: ''})
            // let e ={
            //     target : {
            //         value: "reload"
            //     }
            // }
            // this.page(e)
        }
        if (this.props.purchaseIndent.piImageUrl.isSuccess) {
            this.setState({
                loader: false
            })
        } else if (this.props.purchaseIndent.piImageUrl.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: this.props.purchaseIndent.piImageUrl.data.status,
                errorCode: this.props.purchaseIndent.piImageUrl.data.error == undefined ? undefined : this.props.purchaseIndent.piImageUrl.data.error.errorCode,
                errorMessage: this.props.purchaseIndent.piImageUrl.data.error == undefined ? undefined : this.props.purchaseIndent.piImageUrl.data.error.errorMessage
            })
            this.props.piImageUrlClear()
        }

    }

    onHeadersTabClick = (tabVal) => {
        this.setState({ tabVal: tabVal }, () => {
            if (tabVal === 1 || tabVal === 2 || tabVal === 3 || tabVal === 4 || tabVal === 5 || tabVal === 6) {
                if (!this.props.replenishment.getHeaderConfig.data.resource) {
                    this.openColoumSetting("true")
                    this.props.getHeaderConfigRequest(this.state.mainHeaderPayload)
                }
            }
            // } else if (tabVal === 2) {
            //     if (!this.props.replenishment.getHeaderConfig.data.resource) {
            //         this.openColoumSetting("true")
            //         this.props.getHeaderConfigRequest(this.state.mainHeaderPayload)
            //     }
            // }
        })
    }

    openColoumSetting(data) {
        if (this.state.tabVal == 1) {
            if (this.state.mainCustomHeadersState.length == 0) {
                this.setState({
                    headerCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }

        }
        else if (this.state.tabVal == 2) {
            if (this.state.setCustomHeadersState.length == 0) {
                this.setState({
                    setHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 3) {
            if (this.state.setUdfCustomHeadersState.length == 0) {
                this.setState({
                    setUdfHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 4) {
            if (this.state.itemUdfCustomHeadersState.length == 0) {
                this.setState({
                    itemUdfHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 5) {
            if (this.state.catDescCustomHeadersState.length == 0) {
                this.setState({
                    catDescHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }else if (this.state.tabVal == 6) {
            if (this.state.lineItemCustomHeadersState.length == 0) {
                this.setState({
                    lineItemHeaderCondition: true
                }, () => document.addEventListener('click', this.closeColumnSetting))
            }
            if (data == "true") {
                this.setState({
                    coloumSetting: true,
                }, () => document.addEventListener('click', this.closeColumnSetting))
            } else {
                this.setState({
                    coloumSetting: false
                }, () => document.removeEventListener('click', this.closeColumnSetting))
            }
        }
    }

    closeColumnSetting = (e) => {
        if (e.target.className.includes("backdrop-transparent")) {
            this.setState({ coloumSetting: false }, () =>
                document.removeEventListener('click', this.closeColumnSetting))
        }
    }

    pushColumnData(e, data) {
        if (this.state.tabVal == 1) {
            e.preventDefault();
            let getHeaderConfig = this.state.getMainHeaderConfig
            let customHeadersState = this.state.mainCustomHeadersState
            let headerConfigDataState = this.state.mainHeaderConfigDataState
            let customHeaders = this.state.mainCustomHeaders
            let saveState = this.state.saveMainState
            let defaultHeaderMap = this.state.mainDefaultHeaderMap
            let headerSummary = this.state.mainHeaderSummary
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (this.state.headerCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig = getHeaderConfig
                    getHeaderConfig.push(data)
                    var even = (_.remove(mainAvailableHeaders), function (n) {
                        return n == data
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(mainAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeadersState: customHeadersState,
                mainCustomHeaders: customHeaders,
                saveMainState: saveState,
                mainDefaultHeaderMap: defaultHeaderMap,
                mainHeaderSummary: headerSummary,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let customHeadersState = this.state.setCustomHeadersState
            let headerConfigDataState = this.state.setHeaderConfigDataState
            let customHeaders = this.state.setCustomHeaders
            let saveState = this.state.saveSetState
            let defaultHeaderMap = this.state.setDefaultHeaderMap
            let headerSummary = this.state.setHeaderSummary
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (this.state.setHeaderCondition) {

                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {

                        let invert = _.invert(fixedHeaderData)

                        let keyget = invert[data];

                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }

                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];


                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {

                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {

                    customHeadersState.push(data)
                    var even = _.remove(setAvailableHeaders, function (n) {
                        return n == data;
                    });

                    if (!customHeadersState.includes(headerConfigDataState)) {

                        let keyget = (_.invert(fixedHeaderData))[data];


                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)

                    }

                    if (!Object.keys(customHeaders).includes(headerSummary)) {

                        let keygetvalue = (_.invert(fixedHeaderData))[data];

                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeadersState: customHeadersState,
                setCustomHeaders: customHeaders,
                saveSetState: saveState,
                setDefaultHeaderMap: defaultHeaderMap,
                setHeaderSummary: headerSummary,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
        }
        // For SET UDF::
        if (this.state.tabVal == 3) {
            let getHeaderConfig = this.state.getSetUdfHeaderConfig
            let customHeadersState = this.state.setUdfCustomHeadersState
            let headerConfigDataState = this.state.setUdfHeaderConfigDataState
            let customHeaders = this.state.setUdfCustomHeaders
            let saveState = this.state.saveSetUdfState
            let defaultHeaderMap = this.state.setUdfDefaultHeaderMap
            let headerSummary = this.state.setUdfHeaderSummary
            let fixedHeaderData = this.state.setUdfFixedHeaderData
            let setUdfAvailableHeaders = this.state.setUdfAvailableHeaders

            if (this.state.setUdfHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(setUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(setUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getsetUdfHeaderConfig: getHeaderConfig,
                setUdfCustomHeadersState: customHeadersState,
                setUdfCustomHeaders: customHeaders,
                saveSetUdfState: saveState,
                setUdfDefaultHeaderMap: defaultHeaderMap,
                setUdfHeaderSummary: headerSummary,
                setUdfAvailableHeaders,
                changesInSetUdfHeaders: true,
            })
        }
        // For ITEM UDF::
        if (this.state.tabVal == 4) {
            let getHeaderConfig = this.state.getItemUdfHeaderConfig
            let customHeadersState = this.state.itemUdfCustomHeadersState
            let headerConfigDataState = this.state.itemUdfHeaderConfigDataState
            let customHeaders = this.state.itemUdfCustomHeaders
            let saveState = this.state.saveItemUdfState
            let defaultHeaderMap = this.state.itemUdfDefaultHeaderMap
            let headerSummary = this.state.itemUdfHeaderSummary
            let fixedHeaderData = this.state.itemUdfFixedHeaderData
            let itemUdfAvailableHeaders = this.state.itemUdfAvailableHeaders

            if (this.state.itemUdfHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(itemUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(itemUdfAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getItemUdfHeaderConfig: getHeaderConfig,
                itemUdfCustomHeadersState: customHeadersState,
                itemUdfCustomHeaders: customHeaders,
                saveItemUdfState: saveState,
                itemUdfDefaultHeaderMap: defaultHeaderMap,
                itemUdfHeaderSummary: headerSummary,
                itemUdfAvailableHeaders,
                changesInItemUdfHeaders: true,
            })
        }
        // FOR CAT DESC HEADER::::
        if (this.state.tabVal == 5) {
            let getHeaderConfig = this.state.getCatDescHeaderConfig
            let customHeadersState = this.state.catDescCustomHeadersState
            let headerConfigDataState = this.state.catDescHeaderConfigDataState
            let customHeaders = this.state.catDescCustomHeaders
            let saveState = this.state.saveCatDescState
            let defaultHeaderMap = this.state.catDescDefaultHeaderMap
            let headerSummary = this.state.catDescHeaderSummary
            let fixedHeaderData = this.state.catDescFixedHeaderData
            let catDescAvailableHeaders = this.state.catDescAvailableHeaders

            if (this.state.catDescHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(catDescAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(catDescAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getCatDescHeaderConfig: getHeaderConfig,
                catDescCustomHeadersState: customHeadersState,
                catDescCustomHeaders: customHeaders,
                saveCatDescState: saveState,
                catDescDefaultHeaderMap: defaultHeaderMap,
                catDescHeaderSummary: headerSummary,
                catDescAvailableHeaders,
                changesInCatDescHeaders: true,
            })
        }
        //FOR LINE ITEM HEADER:::::
        if (this.state.tabVal == 6) {
            let getHeaderConfig = this.state.getLineItemHeaderConfig
            let customHeadersState = this.state.lineItemCustomHeadersState
            let headerConfigDataState = this.state.lineItemHeaderConfigDataState
            let customHeaders = this.state.lineItemCustomHeaders_
            let saveState = this.state.saveLineItemState
            let defaultHeaderMap = this.state.lineItemDefaultHeaderMap
            let headerSummary = this.state.lineItemHeaderSummary
            let fixedHeaderData = this.state.lineItemFixedHeaderData
            let lineItemAvailableHeaders = this.state.lineItemAvailableHeaders

            if (this.state.lineItemHeaderCondition) {
                if (!data.includes(getHeaderConfig) || getHeaderConfig.length == 0) {
                    getHeaderConfig.push(data)
                    var even = _.remove(lineItemAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!data.includes(Object.values(headerConfigDataState))) {
                        let invert = _.invert(fixedHeaderData)
                        let keyget = invert[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(defaultHeaderMap)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        defaultHeaderMap.push(keygetvalue)
                    }
                }
            } else {
                if (!data.includes(customHeadersState) || customHeadersState.length == 0) {
                    customHeadersState.push(data)
                    var even = _.remove(lineItemAvailableHeaders, function (n) {
                        return n == data;
                    });
                    if (!customHeadersState.includes(headerConfigDataState)) {
                        let keyget = (_.invert(fixedHeaderData))[data];
                        Object.assign(customHeaders, { [keyget]: data })
                        saveState.push(keyget)
                    }
                    if (!Object.keys(customHeaders).includes(headerSummary)) {
                        let keygetvalue = (_.invert(fixedHeaderData))[data];
                        headerSummary.push(keygetvalue)
                    }
                }
            }

            this.setState({
                getLineItemHeaderConfig: getHeaderConfig,
                lineItemCustomHeadersState: customHeadersState,
                lineItemCustomHeaders_: customHeaders,
                saveLineItemState: saveState,
                lineItemDefaultHeaderMap: defaultHeaderMap,
                lineItemHeaderSummary: headerSummary,
                lineItemAvailableHeaders,
                changesInLineItemHeaders: true,
            })
        }
    }

    handleDragStart(e, key, dragItem, dragNode) {
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', this.handleDragEnd(dragItem, dragNode))
        dragItem.current = key;
        this.setState({
            dragOn: true
        })
    }

    handleDragEnd(dragItem, dragNode) {
        //this.props.handleDragEnd(this.dragNode)
        dragNode.current.removeEventListener('dragend', this.HandleDragEnd)
        this.setState({
            dragOn: false
        })
        dragItem.current = null
        dragNode.current = null
    }
    handleDragEnter(e, key, dragItem, dragNode) {
        const currentItem = dragItem.current
        if (e.target !== dragItem.current) {
            if (this.state.tabVal == 1) {
                if (this.state.headerCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainDefaultHeaderMap
                        let newMainHeaderConfigList = prevState.getMainHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newMainHeaderConfigList.splice(key, 0, newMainHeaderConfigList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newMainHeaderConfigList))
                        let configheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return {
                            mainHeaderConfigDataState: configheaderData,
                            mainCustomHeaders: configheaderData,
                            changesInMainHeaders: true,
                            mainDefaultHeaderMap: newList,
                            getMainHeaderConfig: newMainHeaderConfigList,
                        }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.mainHeaderSummary
                        let newCustomList = prevState.mainCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomList.splice(key, 0, newCustomList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.mainCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return {
                            changesInMainHeaders: true,
                            mainCustomHeaders: customheaderData,
                            mainHeaderSummary: newList,
                            mainCustomHeadersState: newCustomList,
                        }
                    })

                }
            }
            if (this.state.tabVal == 2) {
                if (this.state.setHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.setDefaultHeaderMap
                        let newSetHeaderList = prevState.getSetHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newSetHeaderList.splice(key, 0, newSetHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newSetHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setCustomHeaders: customheaderData, setHeaderConfigDataState: customheaderData, changesInSetHeaders: true, setDefaultHeaderMap: newList, getSetHeaderConfig: newSetHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.setHeaderSummary
                        let newCustomHeaderList = prevState.setCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setCustomHeaders: customheaderData, changesInSetHeaders: true, setHeaderSummary: newList, setCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            //FOR SET UDF HEADER::
            if (this.state.tabVal == 3) {
                if (this.state.setUdfHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.setUdfDefaultHeaderMap
                        let newSetUdfHeaderList = prevState.getSetUdfHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newSetUdfHeaderList.splice(key, 0, newSetUdfHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setUdfHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newSetUdfHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setUdfCustomHeaders: customheaderData, setUdfHeaderConfigDataState: customheaderData, changesInSetUdfHeaders: true, setUdfDefaultHeaderMap: newList, getSetUdfHeaderConfig: newSetUdfHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.setUdfHeaderSummary
                        let newCustomHeaderList = prevState.setUdfCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.setUdfCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { setUdfCustomHeaders: customheaderData, changesInSetUdfHeaders: true, setUdfHeaderSummary: newList, setUdfCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            // FOR ITEM UDF HEADER:::
            if (this.state.tabVal == 4) {
                if (this.state.itemUdfHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.itemUdfDefaultHeaderMap
                        let newItemUdfHeaderList = prevState.getItemUdfHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newItemUdfHeaderList.splice(key, 0, newItemUdfHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.itemUdfHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newItemUdfHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { itemUdfCustomHeaders: customheaderData, itemUdfHeaderConfigDataState: customheaderData, changesInItemUdfHeaders: true, itemUdfDefaultHeaderMap: newList, getItemUdfHeaderConfig: newItemUdfHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.itemUdfHeaderSummary
                        let newCustomHeaderList = prevState.itemUdfCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.itemUdfCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { itemUdfCustomHeaders: customheaderData, changesInItemUdfHeaders: true, itemUdfHeaderSummary: newList, itemUdfCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            // FOR CAT DESC HEADER ::
            if (this.state.tabVal == 5) {
                if (this.state.catDescHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.catDescDefaultHeaderMap
                        let newCatDescHeaderList = prevState.getCatDescHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCatDescHeaderList.splice(key, 0, newCatDescHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.catDescHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCatDescHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { catDescHeaderConfigDataState: customheaderData, catDescCustomHeaders: customheaderData, changesInCatDescHeaders: true, catDescDefaultHeaderMap: newList, getCatDescHeaderConfig: newCatDescHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.catDescHeaderSummary
                        let newCustomHeaderList = prevState.catDescCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.catDescCustomHeaders
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { catDescCustomHeaders: customheaderData, changesInCatDescHeaders: true, catDescHeaderSummary: newList, catDescCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
            // FOR LINE ITEM::
            if (this.state.tabVal == 6) {
                if (this.state.lineItemHeaderCondition) {
                    this.setState((prevState, props) => {
                        let newList = prevState.lineItemDefaultHeaderMap
                        let newLineItemHeaderList = prevState.getLineItemHeaderConfig
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newLineItemHeaderList.splice(key, 0, newLineItemHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.lineItemHeaderConfigDataState
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newLineItemHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { lineItemHeaderConfigDataState: customheaderData, lineItemCustomHeaders_: customheaderData, changesInLineItemHeaders: true, lineItemDefaultHeaderMap: newList, getLineItemHeaderConfig: newLineItemHeaderList }
                    })
                } else {
                    this.setState((prevState, props) => {
                        let newList = prevState.lineItemHeaderSummary
                        let newCustomHeaderList = prevState.lineItemCustomHeadersState
                        newList.splice(key, 0, newList.splice(dragItem.current, 1)[0])
                        newCustomHeaderList.splice(key, 0, newCustomHeaderList.splice(dragItem.current, 1)[0])
                        let headerData = prevState.lineItemCustomHeaders_
                        const swapCustomHeaderData = Object.keys(headerData)
                            .reduce((obj, key) => Object.assign({}, obj, { [headerData[key]]: key }), {});
                        let val = JSON.parse(JSON.stringify(swapCustomHeaderData, newCustomHeaderList))
                        let customheaderData = Object.keys(val)
                            .reduce((obj, key) => Object.assign({}, obj, { [val[key]]: key }), {});
                        dragItem.current = key;
                        return { lineItemCustomHeaders_: customheaderData, changesInLineItemHeaders: true, lineItemHeaderSummary: newList, lineItemCustomHeadersState: newCustomHeaderList }
                    })

                }
            }
        }

    }

    closeColumn(data) {
        if (this.state.tabVal == 1) {
            let getHeaderConfig = this.state.getMainHeaderConfig
            let headerConfigState = this.state.mainHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.mainCustomHeadersState
            let fixedHeaderData = this.state.mainFixedHeaderData
            let mainAvailableHeaders = this.state.mainAvailableHeaders
            if (!this.state.headerCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.mainCustomHeadersState.length == 0) {
                    this.setState({
                        headerCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            mainAvailableHeaders.push(data)
            this.setState({
                getMainHeaderConfig: getHeaderConfig,
                mainCustomHeaders: headerConfigState,
                mainCustomHeadersState: customHeadersState,
                mainAvailableHeaders,
                changesInMainHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.mainFixedHeaderData))[data];
                let saveState = this.state.saveMainState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.mainHeaderSummary
                let defaultHeaderMap = this.state.mainDefaultHeaderMap
                if (!this.state.headerCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }
                this.setState({
                    mainHeaderSummary: headerSummary,
                    mainDefaultHeaderMap: defaultHeaderMap,
                    saveMainState: saveState
                })
            }, 100);
        }
        if (this.state.tabVal == 2) {
            let getHeaderConfig = this.state.getSetHeaderConfig
            let headerConfigState = this.state.setHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.setCustomHeadersState
            let fixedHeaderData = this.state.setFixedHeaderData
            let setAvailableHeaders = this.state.setAvailableHeaders

            if (!this.state.setHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.setCustomHeadersState.length == 0) {
                    this.setState({
                        setHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            setAvailableHeaders.push(data)
            this.setState({
                getSetHeaderConfig: getHeaderConfig,
                setCustomHeaders: headerConfigState,
                setCustomHeadersState: customHeadersState,
                setAvailableHeaders,
                changesInSetHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.setFixedHeaderData))[data];
                let saveState = this.state.saveSetState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.setHeaderSummary
                let defaultHeaderMap = this.state.setDefaultHeaderMap
                if (!this.state.setHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    setHeaderSummary: headerSummary,
                    setDefaultHeaderMap: defaultHeaderMap,
                    saveSetState: saveState
                })
            }, 100);
        }
        //FOR SET UDF HEADER:::
        if (this.state.tabVal == 3) {
            let getHeaderConfig = this.state.getSetUdfHeaderConfig
            let headerConfigState = this.state.setUdfHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.setUdfCustomHeadersState
            let fixedHeaderData = this.state.setUdfFixedHeaderData
            let setUdfAvailableHeaders = this.state.setUdfAvailableHeaders

            if (!this.state.setUdfHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.setUdfCustomHeadersState.length == 0) {
                    this.setState({
                        setUdfHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            setUdfAvailableHeaders.push(data)
            this.setState({
                getSetUdfHeaderConfig: getHeaderConfig,
                setUdfCustomHeaders: headerConfigState,
                setUdfCustomHeadersState: customHeadersState,
                setUdfAvailableHeaders,
                changesInSetUdfHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.setUdfFixedHeaderData))[data];
                let saveState = this.state.saveSetUdfState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.setUdfHeaderSummary
                let defaultHeaderMap = this.state.setUdfDefaultHeaderMap
                if (!this.state.setUdfHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    setUdfHeaderSummary: headerSummary,
                    setUdfDefaultHeaderMap: defaultHeaderMap,
                    saveSetUdfState: saveState
                })
            }, 100);
        }
        //FOR ITEM UDF:::
        if (this.state.tabVal == 4) {
            let getHeaderConfig = this.state.getItemUdfHeaderConfig
            let headerConfigState = this.state.itemUdfHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.itemUdfCustomHeadersState
            let fixedHeaderData = this.state.itemUdfFixedHeaderData
            let itemUdfAvailableHeaders = this.state.itemUdfAvailableHeaders

            if (!this.state.itemUdfHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.itemUdfCustomHeadersState.length == 0) {
                    this.setState({
                        itemUdfHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            itemUdfAvailableHeaders.push(data)
            this.setState({
                getItemUdfHeaderConfig: getHeaderConfig,
                itemUdfCustomHeaders: headerConfigState,
                itemUdfCustomHeadersState: customHeadersState,
                itemUdfAvailableHeaders,
                changesInItemUdfHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.itemUdfFixedHeaderData))[data];
                let saveState = this.state.saveItemUdfState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.itemUdfHeaderSummary
                let defaultHeaderMap = this.state.itemUdfDefaultHeaderMap
                if (!this.state.itemUdfHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    itemUdfHeaderSummary: headerSummary,
                    itemUdfDefaultHeaderMap: defaultHeaderMap,
                    saveItemUdfState: saveState
                })
            }, 100);
        }
        //FOR CAT DESC HEADER:::
        if (this.state.tabVal == 5) {
            let getHeaderConfig = this.state.getCatDescHeaderConfig
            let headerConfigState = this.state.catDescHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.catDescCustomHeadersState
            let fixedHeaderData = this.state.catDescFixedHeaderData
            let catDescAvailableHeaders = this.state.catDescAvailableHeaders

            if (!this.state.catDescHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.catDescCustomHeadersState.length == 0) {
                    this.setState({
                        catDescHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            catDescAvailableHeaders.push(data)
            this.setState({
                getCatDescHeaderConfig: getHeaderConfig,
                catDescCustomHeaders: headerConfigState,
                catDescCustomHeadersState: customHeadersState,
                catDescAvailableHeaders,
                changesInCatDescHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.catDescFixedHeaderData))[data];
                let saveState = this.state.saveCatDescState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.catDescHeaderSummary
                let defaultHeaderMap = this.state.catDescDefaultHeaderMap
                if (!this.state.catDescHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    catDescHeaderSummary: headerSummary,
                    catDescDefaultHeaderMap: defaultHeaderMap,
                    saveCatDescState: saveState
                })
            }, 100);
        }
        // FOR LINE ITEM HEADER::
        if (this.state.tabVal == 6) {
            let getHeaderConfig = this.state.getLineItemHeaderConfig
            let headerConfigState = this.state.lineItemHeaderConfigState
            let customHeaders = []
            let customHeadersState = this.state.lineItemCustomHeadersState
            let fixedHeaderData = this.state.lineItemFixedHeaderData
            let lineItemAvailableHeaders = this.state.lineItemAvailableHeaders

            if (!this.state.lineItemHeaderCondition) {
                for (let j = 0; j < customHeadersState.length; j++) {
                    if (data == customHeadersState[j]) {
                        customHeadersState.splice(j, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!customHeadersState.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
                if (this.state.lineItemCustomHeadersState.length == 0) {
                    this.setState({
                        lineItemHeaderCondition: false
                    })
                }
            } else {
                for (var i = 0; i < getHeaderConfig.length; i++) {
                    if (data == getHeaderConfig[i]) {
                        getHeaderConfig.splice(i, 1)
                    }
                }
                for (var key in fixedHeaderData) {
                    if (!getHeaderConfig.includes(fixedHeaderData[key])) {
                        customHeaders.push(key)
                    }
                }
            }
            customHeaders.forEach(e => delete headerConfigState[e]);
            lineItemAvailableHeaders.push(data)
            this.setState({
                getLineItemHeaderConfig: getHeaderConfig,
                lineItemCustomHeaders_: headerConfigState,
                lineItemCustomHeadersState: customHeadersState,
                lineItemAvailableHeaders,
                changesInLineItemHeaders: true,
            })
            setTimeout(() => {
                let keygetvalue = (_.invert(this.state.lineItemFixedHeaderData))[data];
                let saveState = this.state.saveLineItemState
                data != "undefined" ? saveState.push(data) : null
                let headerSummary = this.state.lineItemHeaderSummary
                let defaultHeaderMap = this.state.lineItemDefaultHeaderMap
                if (!this.state.lineItemHeaderCondition) {
                    for (let j = 0; j < headerSummary.length; j++) {
                        if (keygetvalue == headerSummary[j]) {
                            headerSummary.splice(j, 1)
                        }
                    }
                } else {
                    for (let i = 0; i < defaultHeaderMap.length; i++) {
                        if (keygetvalue == defaultHeaderMap[i]) {
                            defaultHeaderMap.splice(i, 1)
                        }
                    }
                }

                this.setState({
                    lineItemHeaderSummary: headerSummary,
                    lineItemDefaultHeaderMap: defaultHeaderMap,
                    saveLineItemState: saveState
                })
            }, 100);
        }
    }
    saveColumnSetting(e) {
        let fixedHeadersPiDetail = {};
            fixedHeadersPiDetail = this.state.setFixedHeaderData;
            fixedHeadersPiDetail.catDescHeader = this.state.catDescFixedHeaderData;
            fixedHeadersPiDetail.itemUdfHeader = this.state.itemUdfFixedHeaderData;
            fixedHeadersPiDetail.lineItem = this.state.lineItemFixedHeaderData;
            fixedHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfFixedHeaderData;
         
        let defaultHeadersPiDetail = {};
            defaultHeadersPiDetail = this.state.setHeaderConfigDataState;
            defaultHeadersPiDetail.catDescHeader = this.state.catDescHeaderConfigDataState;
            defaultHeadersPiDetail.itemUdfHeader = this.state.itemUdfHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem = this.state.lineItemHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfHeaderConfigDataState;
            
        let customHeadersPiDetail = {};
            customHeadersPiDetail = Object.keys(this.state.setCustomHeaders).length == 0 ? this.state.setHeaderConfigDataState : this.state.setCustomHeaders;
            customHeadersPiDetail.catDescHeader = Object.keys(this.state.catDescCustomHeaders).length == 0 ? this.state.catDescHeaderConfigDataState : this.state.catDescCustomHeaders;
            customHeadersPiDetail.itemUdfHeader = Object.keys(this.state.itemUdfCustomHeaders).length == 0 ? this.state.itemUdfHeaderConfigDataState : this.state.itemUdfCustomHeaders;
            customHeadersPiDetail.lineItem = Object.keys(this.state.lineItemCustomHeaders_).length == 0 ? this.state.lineItemHeaderConfigDataState : this.state.lineItemCustomHeaders_;
            customHeadersPiDetail.lineItem.setUdfHeader = Object.keys(this.state.setUdfCustomHeaders).length == 0 ?  this.state.setUdfHeaderConfigDataState : this.state.setUdfCustomHeaders;
            
            
        let payload = {
            module: "PROCUREMENT",
            subModule: "PURCHASE INDENT",
            section: "HISTORY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "PI_TABLE_HEADER",
            displayName: "TABLE HEADER",
            fixedHeaders: {
                pi: this.state.mainFixedHeaderData,
                piDetail: fixedHeadersPiDetail
            },
            defaultHeaders: {
                pi: this.state.mainHeaderConfigDataState,
                piDetail: defaultHeadersPiDetail
            },
            customHeaders: {
                pi: Object.keys(this.state.mainCustomHeaders).length == 0 ? this.state.mainHeaderConfigDataState : this.state.mainCustomHeaders,
                piDetail: customHeadersPiDetail
            }
        }
        if (this.state.tabVal == 1) {
            this.setState({
                coloumSetting: false,
                headerCondition: false,
                saveMainState: [],
                changesInMainHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 2) {
            this.setState({
                coloumSetting: false,
                setHeaderCondition: false,
                saveSetState: [],
                changesInSetHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 3) {
            this.setState({
                coloumSetting: false,
                setUdfHeaderCondition: false,
                saveSetUdfState: [],
                changesInSetUdfHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 4) {
            this.setState({
                coloumSetting: false,
                itemUdfHeaderCondition: false,
                saveItemUdfState: [],
                changesInItemUdfHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 5) {
            this.setState({
                coloumSetting: false,
                catDescHeaderCondition: false,
                saveCatDescState: [],
                changesInCatDescHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
        if (this.state.tabVal == 6) {
            this.setState({
                coloumSetting: false,
                lineItemHeaderCondition: false,
                saveLineItemState: [],
                changesInLineItemHeaders: false,
            })
            this.props.createHeaderConfigRequest(payload)
        }
    }

    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // paraMsg: "Click confirm to continue.",
            confirmModal: true,
        })
    }
    resetColumn() {
        const { getMainHeaderConfig, mainFixedHeader,setFixedHeader, getSetHeaderConfig, setUdfFixedHeader,
            getSetUdfHeaderConfig,getItemUdfHeaderConfig, itemUdfFixedHeader,getCatDescHeaderConfig, catDescFixedHeader,
            getLineItemHeaderConfig, lineItemFixedHeader } = this.state;

        let fixedHeadersPiDetail = {};
            fixedHeadersPiDetail = this.state.setFixedHeaderData;
            fixedHeadersPiDetail.catDescHeader = this.state.catDescFixedHeaderData;
            fixedHeadersPiDetail.itemUdfHeader = this.state.itemUdfFixedHeaderData;
            fixedHeadersPiDetail.lineItem = this.state.lineItemFixedHeaderData;
            fixedHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfFixedHeaderData;
         
        let defaultHeadersPiDetail = {};
            defaultHeadersPiDetail = this.state.setHeaderConfigDataState;
            defaultHeadersPiDetail.catDescHeader = this.state.catDescHeaderConfigDataState;
            defaultHeadersPiDetail.itemUdfHeader = this.state.itemUdfHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem = this.state.lineItemHeaderConfigDataState;
            defaultHeadersPiDetail.lineItem.setUdfHeader = this.state.setUdfHeaderConfigDataState;
            
        let customHeadersPiDetail = {};
            customHeadersPiDetail = this.state.tabVal == 2 ? this.state.setHeaderConfigDataState : this.state.setCustomHeaders;
            customHeadersPiDetail.catDescHeader = this.state.tabVal == 5 ? this.state.catDescHeaderConfigDataState : this.state.catDescCustomHeaders;
            customHeadersPiDetail.itemUdfHeader = this.state.tabVal == 4 ? this.state.itemUdfHeaderConfigDataState : this.state.itemUdfCustomHeaders;
            customHeadersPiDetail.lineItem = this.state.tabVal == 5 ? this.state.lineItemHeaderConfigDataState : this.state.lineItemCustomHeaders_;
            customHeadersPiDetail.lineItem.setUdfHeader = this.state.tabVal == 3 ? this.state.setUdfHeaderConfigDataState  : this.state.setUdfCustomHeaders;    
        
        let customHeadersPi = {};
            customHeadersPi = this.state.tabVal == 1 ? this.state.mainHeaderConfigDataState : this.state.mainCustomHeaders;

        let payload = {
            module: "PROCUREMENT",
            subModule: "PURCHASE INDENT",
            section: "HISTORY",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "PI_TABLE_HEADER",
            displayName: "TABLE HEADER",
            fixedHeaders: {
                pi: this.state.mainFixedHeaderData,
                piDetail: fixedHeadersPiDetail
            },
            defaultHeaders: {
                pi: this.state.mainHeaderConfigDataState,
                piDetail: defaultHeadersPiDetail
            },
            customHeaders: {
                pi: customHeadersPi,
                piDetail: customHeadersPiDetail
            }
        }

        if (this.state.tabVal == 1) {    
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = mainFixedHeader.filter(function (obj) { return getMainHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                headerCondition: true,
                coloumSetting: false,
                saveMainState: [],
                confirmModal: false,
                mainAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 2) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = setFixedHeader.filter(function (obj) { return getSetHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                setHeaderCondition: true,
                coloumSetting: false,
                saveSetState: [],
                confirmModal: false,
                setAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 3) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = setUdfFixedHeader.filter(function (obj) { return getSetUdfHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                setUdfHeaderCondition: true,
                coloumSetting: false,
                saveSetUdfState: [],
                confirmModal: false,
                setUdfAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 4) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = itemUdfFixedHeader.filter(function (obj) { return getItemUdfHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                itemUdfHeaderCondition: true,
                coloumSetting: false,
                saveItemUdfState: [],
                confirmModal: false,
                itemUdfAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 5) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = catDescFixedHeader.filter(function (obj) { return getCatDescHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                catDescHeaderCondition: true,
                coloumSetting: false,
                saveCatDescState: [],
                confirmModal: false,
                catDescAvailableHeaders: availableHeaders
            })
        }
        if (this.state.tabVal == 6) {
            this.props.createHeaderConfigRequest(payload)
            let availableHeaders = lineItemFixedHeader.filter(function (obj) { return getLineItemHeaderConfig.indexOf(obj) == -1 })
            this.setState({
                lineItemHeaderCondition: true,
                coloumSetting: false,
                saveLineItemState: [],
                confirmModal: false,
                lineItemAvailableHeaders: availableHeaders
            })
        }
    }

    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }
    viewDetails(data) {
        this.setState({
            selectedId: [...this.state.selectedId, data.indentNo],
            viewDetails: true,
            isSet: data.isSet == "false" || data.isSet == "FALSE" ? false : true,
        })

        let payload = {
            orderNo: data.id,
            status: data.status == 'DRAFTED' ? true : false,
        }
        this.props.getPiLineItemRequest(payload)


    }
    closeDetails() {
        this.setState({
            viewDetails: false,
            selectedId: [],
        })
    }
    openDownloadReport() {
        this.setState({
            dReport: true,
            exportToExcel: !this.state.exportToExcel
        })
    }
    closeReport() {
        this.setState({
            dReport: false
        })
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false
        })
    }

    editPurchaseIndent(id, status, indentNo) {
        if (status == "DRAFTED") {
            this.props.getDraftRequest({ orderNo: indentNo, draftType: "PI_DRAFT" })
            this.props.history.push("/purchase/purchaseIndents");
        } else if (status == "PENDING") {
            this.props.piEditRequest({ indentId: id });
            this.props.history.push("/purchase/purchaseIndents");

        } else if (status == "APPROVED" || status == "REJECTED") {
            this.setState({
                alert: true,
                errorMessage: "Cannot edit Approved or Rejected Document",
                errorCode: "5000",
                code: "5001"
            })
        }
    }

    deleteForm() {
        // this.props.discardPoPiDataRequest({indentNo : indentNo, status : status,value: "pi"})
        this.setState({
            headerMsg: "Do you want to delete this item, If deleted, all data saved in this document will be lost.",
            paraMsg: "Click confirm to continue.",
            deleteConfirmModal: true,
            deleteIndentNo: this.state.selectedId,
            deleteStatus: this.state.currentStatus
        })
    }

    closeDeleteConfirmModal() {
        this.setState({
            deleteConfirmModal: !this.state.deleteConfirmModal,
            deleteIndentNo: "",
            deleteStatus: ""
        })
    }

    confirmDeleteForm() {
        this.setState({
            deleteConfirmModal: false
        })
        var discardData = [];
        this.state.deleteIndentNo.map(data => {
            discardData.push({ indentNo: data, status: this.state.statusForPdf })
        })
        this.props.discardPoPiDataRequest({ discardData: discardData, value: "pi" })
        let data = {
            no: this.state.current,
            type: 1,
            search: this.state.search,
            filter: this.state.filteredValue,
        }
        this.props.piHistoryRequest(data);
        this.setState({
            loader: false,
            deleteIndentNo: "",
            selectedId: [],
            currentStatus: [],
            enableDeleteButton: false,
        })
    }

    filterHeader = (event) => {
        // var data = event.target.dataset.key        
        var data = event.target.textContent ? event.target.textContent : event.target.dataset.key;
        var oldActive = document.getElementsByClassName("imgHead");
        // if (event.target.classList.contains('rotate180')) {
        //     event.target.classList.remove("rotate180");
        // } else {
        //     for (var i = 0; i < oldActive.length; i++) {
        //         oldActive[i].classList.remove("rotate180");
        //     }
        //     event.target.classList.add("rotate180");
        // }
        if(document.getElementById(data).classList.contains('rotate180')){
            document.getElementById(data).classList.remove('rotate180')
        }else {
            for (var i = 0; i < oldActive.length; i++) {
                oldActive[i].classList.remove("rotate180");
            }
            document.getElementById(data).classList.add('rotate180')
        }
        var def = { ...this.state.headerConfigDataState };
        var filterKey = ""
        Object.keys(def).some(function (k) {
            if (def[k] == data) {
                filterKey = k
            }
        })
        if (this.state.prevFilter == data) {
            this.setState({ filterKey, filterType: this.state.filterType == "ASC" ? "DESC" : "ASC" })
        } else {
            this.setState({ filterKey, filterType: "ASC" })
        }
        this.setState({ prevFilter: data }, () => {
            let payload = {
                no: this.state.current,
                type: this.state.type,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue
            }
            this.props.piHistoryRequest(payload);
        })
    }

    openFilter = (e) => {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar

        });
    }

    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            this.setState({ [data]: "" })
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check, inputBoxEnable: true })
    }

    submitFilter = () => {
        let payload = {}
        this.state.checkedFilters.map((data) => {
            return (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data])
        })
        Object.keys(payload).map((data) => (data.includes("Date") && (payload[data] = payload[data] == "" ? "" : payload[data] + "T00:00+05:30")))
        Object.keys(payload).map((data) => ((data.includes("poQuantity") || data.includes("poAmount") || data.includes("indentDate") || data.includes("createdTime") || data.includes("updationTime"))
            && (payload[data] = payload[data] == "" ? "" : { from: payload[data].split("|")[0].trim(), to: payload[data].split("|")[1].trim() })))
        let data = {
            no: 1,
            type: this.state.type == 3 || this.state.type == 4 ? 2 : 4,
            search: this.state.search,
            sortedBy: this.state.filterKey,
            sortedIn: this.state.filterType,
            filter: payload
        }
        this.props.piHistoryRequest(data);
        this.props.piHistoryDataUpdate(data)

        this.setState({
            filter: false,
            filteredValue: payload,
            type: this.state.type == 3 || this.state.type == 4 ? 4 : 2,
            tagState: true
        })
    }

    clearFilter = (e) => {
        if (this.state.type == 3 || this.state.type == 4 || this.state.type == 2) {
            let payload = {
                no: 1,
                type: this.state.type == 4 ? 3 : 1,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
            }
            this.props.piHistoryRequest(payload);

        }
        this.setState({
            filteredValue: [],
            type: this.state.type == 4 ? 3 : 1,
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
    }
    closeFilter = (e) => {
        // e.preventDefault();
        this.setState({
            filter: false,
            filterBar: false
        });
        this.clearFilter();
    }

    handleInput = (event, filterName) => {
        if (event != undefined && event.length != undefined) {
            this.setState({
                fromCreationDate: moment(event[0]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
            this.setState({
                toCreationDate: moment(event[1]._d).format('YYYY-MM-DD'),
                filterNameForDate: filterName
            }, () => this.handleFromAndToValue(event))
        }
        else if (event != null) {
            if (event.target.id === "fromPIQuantity")
                this.setState({ fromPIQuantityValue: event.target.value }, () => this.handleFromAndToValue(event))
            else if (event.target.id == "toPIQuantity")
                this.setState({ toPIQuantityValue: event.target.value }, () => this.handleFromAndToValue(event))
            else if (event.target.id === "fromPIAmount")
                this.setState({ fromPIAmountValue: event.target.value }, () => this.handleFromAndToValue(event))
            else if (event.target.id == "toPIAmount")
                this.setState({ toPIAmountValue: event.target.value }, () => this.handleFromAndToValue(event))
            else
                this.handleFromAndToValue(event);
        }
    }

    handleFromAndToValue = () => {
        var value = "";
        var name = event.target.dataset.value
        if (name == undefined) {
            value = this.state.fromCreationDate + " | " + this.state.toCreationDate
            name = this.state.filterNameForDate;
        }
        else {
            if (event.target.id === "fromPIQuantity" || event.target.id === "toPIQuantity")
                value = this.state.fromPIQuantityValue + " | " + this.state.toPIQuantityValue
            else if (event.target.id === "fromPIAmount" || event.target.id === "toPIAmount")
                value = this.state.fromPIAmountValue + " | " + this.state.toPIAmountValue
            else
                value = event.target.value
        }

        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }
        this.setState({ [name]: value, applyFilter: true }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                this.setState({ applyFilter: true })
            }
        })
    }

    handleInputBoxEnable = (e, data) => {
        this.setState({ inputBoxEnable: true })
        this.handleCheckedItems(e, this.state.filterItems[data])
    }

    copyIndentHandler = () =>{
        this.props.gen_IndentBasedRequest({indentNo: this.state.indentIdForPdf });
        this.props.history.push('/purchase/purchaseIndents')
    }

    cancelRemarkRequest = () =>{
        this.setState({
            isRemark: false,
        })
    }

    statusUpdate = (status) => {
        if (status == "APPROVED") {
            this.statusUpdateConfirm(status, '');
        } else {
            this.setState({
                isRemark : true,
                statusType : status
            })  
        } 
    }

    statusUpdateConfirm = (status, remark) => {
        if (this.state.selectedId.length > 0) {
            this.closeDetails();
            let payloadForStatus = {
                patterns: this.state.showLineItem ? [this.state.selectedNo] : this.state.selectedId,
                status: status == '' ? this.state.statusType : status,
                remark: remark
            }
            let filter = this.state.checkedFilters.length == 0 ? false : true;
            let search = this.state.search == "" ? false : true;
            let payloadForHistory = {
                no: this.state.current,
                type: filter && search ? 4 : !filter && !search ? 1 : filter && !search ? 2 : 3,
                search: this.state.search,
                sortedBy: this.state.filterKey,
                sortedIn: this.state.filterType,
                filter: this.state.filteredValue,
            }
            let payload = {
                payloadForStatus: payloadForStatus,
                payloadForHistory: payloadForHistory,
            }
            this.props.piUpdateRequest(payload)
            this.setState({
                selectedId: [],
                currentStatus: [],
                indentIdForPdf: '',
                statusForPdf: '',
            }, () => this.statusIdentify(this.state.currentStatus))
        }
        else {
            this.oncloseErr();
        }
    }

    selectedData = (indentNo, status, id) => {
        if (!this.state.selectedId.includes(indentNo)) {
            this.setState({
                // indentIdForPdf: [...this.state.indentIdForPdf, id],
                indentIdForPdf: id,
                selectedId: [...this.state.selectedId, indentNo],
                currentStatus: [...this.state.currentStatus, status],
                statusForPdf: status
            }, () => this.statusIdentify(this.state.currentStatus)
            )
        }
        else {
            var index = this.state.currentStatus.indexOf(status);
            this.state.currentStatus.splice(index, 1);
            this.setState({
                // indentIdForPdf: [...this.state.indentIdForPdf, id],
                indentIdForPdf: id,
                selectedId: this.state.selectedId.filter(selectedId => {
                    return selectedId !== indentNo
                }),
            }, () => this.statusIdentify(this.state.currentStatus)
            )
        }
    }

    statusIdentify = () => {
        if (this.state.currentStatus.length == 0 || this.state.currentStatus.includes("APPROVED") || this.state.currentStatus.includes("PENDING")
            || this.state.currentStatus.includes("REJECTED") || this.state.currentStatus.includes("CANCELED")) {
            this.setState({ enableDeleteButton: false })
        }
        else {
            this.setState({ enableDeleteButton: true })
        }
        if (this.state.currentStatus.length == 0 || this.state.currentStatus.includes("APPROVED") || this.state.currentStatus.includes("DRAFTED")
            || this.state.currentStatus.includes("REJECTED") || this.state.currentStatus.includes("CANCELED")) {
            this.setState({ enableApproveButton: false, enableRejectButton: false })
        }
        else {
            this.setState({ enableApproveButton: true, enableRejectButton: true, enableCancelButton: false })
        }
        if (this.state.currentStatus.length == 0 || this.state.currentStatus.includes("DRAFTED") || this.state.currentStatus.includes("REJECTED") || this.state.currentStatus.includes("PENDING")) {
            this.setState({ enableCancelButton: false })
        }
        else {
            this.setState({ enableCancelButton: true })
        }
        if (this.state.currentStatus.length == 1 && (this.state.currentStatus.includes("APPROVED")  || this.state.currentStatus.includes("PENDING"))) {
            this.setState({ enableCopyIndent: true})
        }
        else{
            this.setState({ enableCopyIndent: false})
        }
    }


    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        no: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        status: "APPROVED",
                        userType: userType,
                        filter: this.state.filteredValue,
                        sortedBy: this.state.filterKey,
                        sortedIn: this.state.filterType,
                    }
                    this.props.piHistoryRequest(payload)

                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }
    resetField() {
        this.setState({
            search: "",
            lgtNumber: "",
            lgtDate: "",
            vendorName: "",
            transporterName: "",
            ownerSite: "",
            transportMode: "",
            stationFrom: "",
            stationTo: "",
            lgtReceivedQty: "",
            vehicleNo: "",
            filterCount: 0,
            type: 1,
        })
    }
    onRefresh = () => {
        let payload = {
            no: 1,
            type: 1,
            search: "",
            status: "APPROVED",
            poDate: "",
            createdOn: "",
            poNumber: "",
            vendorName: "",
            userType: "vendorpo",
        }
        this.setState({ filteredValue: [], selectAll: false, checkedFilters: [] })
        this.props.piHistoryRequest(payload)
        this.resetField()
        if (document.getElementsByClassName('rotate180')[0] != undefined) {
            document.getElementsByClassName('rotate180')[0].classList.remove('rotate180')
        }
    }

    clearTag = (e, index) => {
        let deleteItem = this.state.checkedFilters;
        deleteItem.splice(index, 1)
        this.setState({
            checkedFilters: deleteItem
        }, () => {
            if (this.state.checkedFilters.length == 0){
                this.clearFilter();
                this.setState({
                    tagState: false,
                })
            }                
            else
                this.submitFilter();
        })
    }
    clearAllTag = (e) => {
        this.setState({
            checkedFilters: [],
            tagState: false,
        }, () => {
            this.clearFilter();
            this.clearFilterOutside();
        })
    }

    clearFilterOutside = () => {
        this.setState({
            filteredValue: [],
            selectAll: false,
            checkedFilters: []
        })
    }

    render() {
        if (this.state.mainCustomHeadersState) {
        }
        const { approvedPO, search, sliderDisable } = this.state
        return (
            <div className="container-fluid pad-0 pad-l50" id="vendor_manage">
                <div className="new-purchase-head p-lr-47">
                    <div className="col-lg-6 pad-0">
                        <div className="nph-pi-po-quantity">
                            <ul className="nppq-inner">
                                <li className="nppq-history">
                                    <label>Quantity</label>
                                    <span>{this.state.totalQty}</span>
                                </li>
                                <li className="nppq-history nppqh-borderlft">
                                    <label> Amount </label>
                                    <span><img src={require('../../assets/rupeeNew.svg')} /> {this.state.totalAmout}
                                        <span className="nppqh-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                                <path id="iconmonstr-info-2" fill="#97abbf" d="M7 1.167A5.833 5.833 0 1 1 1.167 7 5.84 5.84 0 0 1 7 1.167zM7 0a7 7 0 1 0 7 7 7 7 0 0 0-7-7zm.583 10.5H6.417V5.833h1.166zM7 3.354a.729.729 0 1 1-.729.729A.729.729 0 0 1 7 3.354z"/>
                                            </svg>
                                            <div className="nppqhi-information">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th><label>Status</label></th>
                                                            <th><label>Quantity</label></th>
                                                            <th><label>Amount</label></th>
                                                            <th><label>Margin</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label className="nppqhii-status nppqhiis-drft">Drafted</label></td>
                                                            <td><label>100</label></td>
                                                            <td><label><img src={require('../../assets/rupeeNew.svg')} /> 1,100,55</label></td>
                                                            <td><label>100</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label className="nppqhii-status nppqhiis-apv">Approved</label></td>
                                                            <td><label>100</label></td>
                                                            <td><label><img src={require('../../assets/rupeeNew.svg')} /> 1,100,55</label></td>
                                                            <td><label>100</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label className="nppqhii-status nppqhiis-can">Canceled</label></td>
                                                            <td><label>100</label></td>
                                                            <td><label><img src={require('../../assets/rupeeNew.svg')} /> 1,100,55</label></td>
                                                            <td><label>100</label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </span>
                                    </span>
                                </li>
                                <li className="nppq-history nppqh-borderlft">
                                    <label>Margin</label>
                                    <span>{this.state.avgMargin}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-6 pad-0">
                        <div className="gvpd-right">
                            {this.state.enableApproveButton ? <button type="button" className="gen-approved" onClick={(e) => this.statusUpdate("APPROVED")} ><img src={CircleTick} />Approve</button>
                                : <button type="button" className="gen-approved btnDisabled"><img src={CircleTick} />Approve</button>}
                            {this.state.enableRejectButton ? <button type="button" className="gen-cancel" value="check" onClick={(e) => this.statusUpdate("REJECTED")}>Reject</button>
                                : <button type="button" className="gen-cancel btnDisabled" value="check" >Reject</button>}
                            {this.state.enableDeleteButton ? <button type="button" className="pi-download" onClick={(e) => this.deleteForm()}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                    <path fill="#12203c" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                </svg>
                                <span class="generic-tooltip">Delete</span>
                             </button>
                                : <button type="button" className="pi-download2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                        <path fill="#d8d3d3" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                    </svg>
                                    <span class="generic-tooltip">Delete</span>
                            </button>}
                            {/* excel download */}
                            <div className="pihitory-download-dropdown">
                                <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openDownloadReport(e)} >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                        <g>
                                            <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                            <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                            <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                        </g>
                                    </svg>
                                    <span class="generic-tooltip">Excel Download</span>
                                </button>

                                {/* {this.state.exportToExcel &&
                                    <ul className="pi-history-download">
                                        {/* <li>
                                            <button className="export-excel" type="button" onClick={(e) => this.openDownloadReport(e)}  >
                                                <span className="pi-export-svg">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                        <g id="prefix__files" transform="translate(0 2)">
                                                            <g id="prefix__Group_2456" data-name="Group 2456">
                                                                <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                    <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                    <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                        <tspan x="0" y="0">XLS</tspan>
                                                                    </text>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </span>
                                                Export to Excel</button>
                                        </li> */}
                                {/* <li>
                                            <button className="export-excel" type="button">
                                                <span className="pi-export-svg">
                                                    <img src={require('../../assets/downloadAll.svg')} />
                                                </span>
                                            Download All</button>
                                        </li> */}
                                {/* </ul>  */}
                            </div>
                            {/* pdf button */}
                            <div className="pihitory-download-dropdown">
                                <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                        <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                    </svg>
                                    <span class="generic-tooltip">PDF Download</span>
                                </button>

                            </div>
                            <div className="gvpd-filter">
                                <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                    <span class="generic-tooltip">Filter</span>
                                </button>
                                {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                {/* {this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null} */}
                            </div>
                            <div className="gvpd-three-dot-btn">
                                <button className="gvpdtdb-btn" onClick={(e) => this.openThreeDotMenu(e)}><span></span></button>
                                {this.state.threeDotMenu && 
                                <div className="gvpdtd-menu">
                                    <ul className="gvpdtdm-inner">
                                        <li>
                                        {this.state.enableCancelButton && this.state.statusForPdf != 'CANCELED' ? <button type="button" className="gvpdtdm-cancel" value="check" onClick={(e) => this.statusUpdate("CANCELED")}>Cancel Order</button>
                                        : <button type="button" className="gen-cancel btnDisabled" value="check" >Cancel Order</button>}
                                        </li>
                                        <li>
                                        {this.state.enableCopyIndent ? <button type="button" className="gvpdtdm-cancel" value="check" onClick={(e) => this.copyIndentHandler()}>Copy Indent</button>
                                        : <button type="button" className="gen-cancel btnDisabled" value="check" >Copy Indent</button>}
                                        </li>
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="nph-history-search p-lr-47">
                    <div className="col-lg-6 pad-0">
                        <div className="gen-new-pi-history">
                            <form>
                                <input type="search" id="search" onKeyDown={this.onSearch} onChange={this.onSearch} value={search} placeholder="Type to Search..." className="search_bar" />
                                <button type="button" className="searchWithBar" onClick={this.searchIcon}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                    </svg>
                                </button>
                                {search != "" ? <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                            </form>
                        </div>
                    </div>
                </div>
                {/* applied filter / recent filter */}
                <div className="col-lg-12 p-lr-47">
                    {this.state.tagState ? this.state.checkedFilters.map((keys, index) => (
                        <div className="show-applied-filter">
                            {index === 0 ? <button type="button" className="saf-clear-all" onClick={(e) => this.clearAllTag(e)}>Clear All</button> : null}
                            <button type="button" className="saf-btn">{keys}
                                <img onClick={(e) => this.clearTag(e, index)} src={require('../../assets/clearSearch.svg')} />
                                {/* <span className="generic-tooltip">{Object.values(this.state.filteredValue)[index]}</span> */}
                                <span className="generic-tooltip">{typeof (Object.values(this.state.filteredValue)[index]) == 'object' ? Object.values(Object.values(this.state.filteredValue)[index]).join(',') : Object.values(this.state.filteredValue)[index]}</span>
                            </button>
                        </div>)) : ''}
                </div>
                <div className="col-lg-12 pad-0 p-lr-47">
                    <div className="generic-history-table">
                        <div className="ght-manage" id="table-scroll">
                            <ColoumSetting {...this.props} {...this.state}
                                handleDragStart={this.handleDragStart}
                                handleDragEnter={this.handleDragEnter}
                                onHeadersTabClick={this.onHeadersTabClick}
                                getMainHeaderConfig={this.state.getMainHeaderConfig}
                                closeColumn={(e) => this.closeColumn(e)}
                                saveMainState={this.state.saveMainState}
                                saveSetState={this.state.saveSetState}
                                saveItemState={this.state.saveItemState}
                                saveSetUdfState = {this.state.saveSetUdfState}
                                saveItemUdfState={this.state.saveItemUdfState}
                                saveCatDescState={this.state.saveCatDescState}
                                saveLineItemState={this.state.saveLineItemState}
                                tabVal={this.state.tabVal}
                                pushColumnData={this.pushColumnData}
                                openColoumSetting={(e) => this.openColoumSetting(e)}
                                resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)}
                                saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                            <table className="table ght-main-table">
                                <thead>
                                    <tr>
                                        <th className="ght-sticky-col">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={Reload} onClick={this.onRefresh}></img></span>
                                                </li>
                                                <li className="rab-rinner">
                                                    {approvedPO.length != 0 ? (Object.keys(this.state.filteredValue).length != 0 || this.state.searchCheck) && <label className="checkBoxLabel0"><input type="checkBox" checked={this.state.selectAll} name="selectAll" onChange={(e) => this.cancelActive(e, "")} /><span className="checkmark1"></span></label> : ""}
                                                    {approvedPO.length != 0 && this.state.selectAll && <span className="select-all-text">{this.state.totalPendingPo} line items selected</span>}
                                                </li>
                                            </ul>
                                        </th>
                                        {this.state.mainCustomHeadersState.length == 0 ? this.state.getMainHeaderConfig.map((data, key) => (
                                            <th key={key} onClick={this.filterHeader} >
                                                <label>{data}</label>
                                                <img src={HeaderFilter} className="imgHead" key={key} data-key={data} id={data}/>
                                            </th>
                                        )) : this.state.mainCustomHeadersState.map((data, key) => (
                                            <th key={key} onClick={this.filterHeader}>
                                                <label>{data}</label>
                                                <img src={HeaderFilter} className="imgHead" key={key} data-key={data} id={data}/>
                                            </th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.piHistoryState && this.state.piHistoryState.length > 0 ? this.state.piHistoryState.map((data, key) => (
                                        <tr key={key} className={this.state.selectedId.includes(data.indentNo) ? "vgt-tr-bg" : ""}>
                                            <td className="ght-sticky-col">
                                                <ul className="ght-item-list">
                                                    <li className="ghtil-inner">
                                                        <label className="checkBoxLabel0">
                                                            <input type="checkbox" checked={this.state.selectedId.includes(data.indentNo)} onChange={(e) => this.selectedData(`${data.indentNo}`, data.status, data.id)} />
                                                            <span className="checkmark1"></span>
                                                        </label>
                                                    </li>
                                                    {/* {data.status != "DRAFTED" ?
                                                        <li className="ghtil-inner ght-eye-btn">
                                                            <img src={eyeIcon_open} onClick={() => this.viewLineItem(data.id, data.status)} />
                                                            <span className="generic-tooltip">Data is available</span>
                                                        </li> :
                                                        <li className="ghtil-inner ght-eye-btn">
                                                            <img src={eyeIcon_closed} />
                                                            <span className="generic-tooltip">No Data Available</span>
                                                        </li>
                                                    } */}
                                                    <li className="ghtil-inner ght-eye-btn">
                                                        <img src={eyeIcon_open} onClick={() => this.viewDetails(data)} />
                                                        <span className="generic-tooltip">Data is available</span>
                                                    </li>

                                                    {data.status == "APPROVED" ?
                                                        <li className="ghtil-inner ght-edit-dis" onClick={() => this.editPurchaseIndent(data.id, data.status, data.indentNo)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                                <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                                <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                            </svg>
                                                            <span className="generic-tooltip">Edit Disabled</span>
                                                        </li>
                                                        : null}
                                                    {data.status == "REJECTED" ? <li className="ghtil-inner ght-edit-dis" onClick={() => this.editPurchaseIndent(data.id, data.status, data.indentNo)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Edit Disabled</span>
                                                    </li> : null}
                                                    {data.status == "PENDING" ? <li className="ghtil-inner ght-edit enb" onClick={() => this.editPurchaseIndent(data.id, data.status, data.indentNo)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Edit</span>
                                                    </li> : null}
                                                    {data.status == "DRAFTED" ? <li className="ghtil-inner ght-edit enb" onClick={() => this.editPurchaseIndent(data.id, data.status, data.indentNo)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Edit</span>
                                                    </li> : null}
                                                    {data.status == "CANCELED" ? <li className="ghtil-inner ght-edit-dis">
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                            <path fill="#d8d3d3" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                            <path fill="#d8d3d3" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                            <path fill="#d8d3d3" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                            <path fill="#d8d3d3" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                        </svg>
                                                        <span className="generic-tooltip">Edit Disabled</span>
                                                    </li> : null}
                                                    {/* <li className="ghtil-inner ght-invoice-btn" onClick={() => this.viewPi(`${data.pattern}`, `${data.path}`)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__signs_4_" width="17.126" height="18.592" data-name="signs (4)" viewBox="0 0 17.126 18.592">
                                                            <path fill="#a4b9dd" id="prefix__Path_591" d="M117.113 0h-8.43a2.016 2.016 0 0 0-2.016 2.016v9.9a2.016 2.016 0 0 0 2.016 2.016h8.43a2.016 2.016 0 0 0 2.016-2.016v-9.9A2.016 2.016 0 0 0 117.113 0zm0 0" className="prefix__cls-1" data-name="Path 591" transform="translate(-102.003)" />
                                                            <g id="prefix__Group_2450" data-name="Group 2450" transform="translate(7.289 3.735)">
                                                                <path fill="#fff" id="prefix__Path_592" d="M198.612 257.1H192.6a.552.552 0 1 1 0-1.1h6.011a.552.552 0 1 1 0 1.1zm0 0" className="prefix__cls-2" data-name="Path 592" transform="translate(-192 -249.601)" />
                                                                <path fill="#fff" id="prefix__Path_593" d="M198.612 193.1H192.6a.552.552 0 1 1 0-1.1h6.011a.552.552 0 1 1 0 1.1zm0 0" className="prefix__cls-2" data-name="Path 593" transform="translate(-192 -188.801)" />
                                                                <path fill="#fff" id="prefix__Path_594" d="M198.612 129.1H192.6a.552.552 0 1 1 0-1.1h6.011a.552.552 0 1 1 0 1.1zm0 0" className="prefix__cls-2" data-name="Path 594" transform="translate(-192 -128)" />
                                                            </g>
                                                            <path fill="#a4b9dd" id="prefix__Path_595" d="M5.681 67.363a3.222 3.222 0 0 1-3.115-3.313V53.527c0-.067.015-.129.018-.195h-.568A2.084 2.084 0 0 0 0 55.476v12.472a2.084 2.084 0 0 0 2.016 2.144h8.8a2.084 2.084 0 0 0 2.016-2.144v-.585zm0 0" className="prefix__cls-1" data-name="Path 595" transform="translate(0 -51.5)" />
                                                        </svg>
                                                        <span className="generic-tooltip">PI PDF</span>
                                                    </li> */}
                                                    {/* <li className="ghtil-inner ght-status">
                                                        {data.status=="APPROVED"? <span><img src={ApproveTag} /></span>:null}
                                                        {data.status=="REJECTED"? <span><img src={RejectTag} /></span>:null}
                                                        {data.status=="PENDING"?  <span> <img src={PendingTag} /> </span>:null}
                                                        {data.status=="DRAFTED"?  <span><span style={{fontSize: "10px", backgroundColor: "#f5bf5e", fontWeight: "600", padding: "6px 7px",margin: "0px", borderRadius: "7px",color: "#7b7979"}}> DRAFT </span></span>:null}
                                                    </li> */}
                                                </ul>
                                                {/* <span className="pad-top-3">
                                                    <img src={invoice} className="img__pos width-16" onClick={() => this.viewPi(`${data.pattern}`, `${data.path}`)} />
                                                </span>
                                                <button  className="edit_button">
                                                    <img src={editIcon} />
                                                </button> */}
                                            </td>
                                            {this.state.mainHeaderSummary.length == 0 ? this.state.mainDefaultHeaderMap.map((hdata, key) => (
                                                <td key={key} >
                                                    {hdata == "indentNo" ? <label className="ghtmtblue">{data["indentNo"]}</label>
                                                    : hdata === "status" ? (<label className={data.status=="DRAFTED" ? "ghtmtstatus ghtmtstatusdrft" : data.status=="PENDING" ? "ghtmtstatus ghtmtstatuspend" : data.status=="APPROVED" ? "ghtmtstatus ghtmtstatusapv" : data.status=="CANCELED" || data.status=="REJECTED" ? "ghtmtstatus ghtmtstatuscan" : "ghtmtstatus"}>{data["status"]} </label>)
                                                    : <label>{data[hdata]}</label>}
                                                    {/* <img src={NonSet} /> */}
                                                </td>
                                            )) : this.state.mainHeaderSummary.map((sdata, keyy) => (
                                                <td key={keyy} >
                                                    {sdata == "indentNo" ? <label className="ghtmtblue">{data["indentNo"]}</label>
                                                    : sdata === "status" ? (<label className={data.status=="DRAFTED" ? "ghtmtstatus ghtmtstatusdrft" : data.status=="PENDING" ? "ghtmtstatus ghtmtstatuspend" : data.status=="APPROVED" ? "ghtmtstatus ghtmtstatusapv" : data.status=="CANCELED" || data.status=="REJECTED" ? "ghtmtstatus ghtmtstatuscan" : "ghtmtstatus"}>{data["status"]}</label>)
                                                    : <label>{data[sdata]}
                                                        {/* <img src={NonSet} /> */}
                                                        {/* <img src={Set} /> */}
                                                    </label>}
                                                </td>
                                            ))}
                                        </tr>
                                    )) : <tr><td align="center" colSpan="12"><label>No Data Found</label></td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div className="new-gen-pagination">
                        <div className="ngp-left">
                            <div className="table-page-no">
                                {/* <span>Page :</span><label className="paginationBorder">{this.state.current}</label> */}

                                <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPi}</span>
                            </div>
                        </div>
                        <div className="ngp-right">
                            <div className="nt-btn">
                                <div className="pagination-inner">
                                    <ul className="pagination-item">
                                        {/*<li ><button className={this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >First</button></li>*/}
                                        {this.state.current == 1 || this.state.current == undefined || this.state.current == "" ? <li>
                                            <button className="disable-first-btn" type="button">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="first-btn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        {/*<li><button className={this.state.prev != 0 && this.state.prev != "" && this.state.current != 1 && this.state.current != "" && this.state.current != undefined ? "PageFirstBtn" : " PageFirstBtn pointerNone"} onClick={(e) => this.page(e)} id="prev">Prev</button></li>*/}
                                        {this.state.prev != 0 && this.state.prev != "" ? <li >
                                            <button className="prev-btn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="dis-prev-btn" type="button" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        <li>
                                            <button className="pi-number-btn">
                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                            </button>
                                        </li>
                                        {this.state.current != "" && this.state.next - 1 != this.state.maxPage && this.state.current != undefined ? <li >
                                            <button className="next-btn" onClick={(e) => this.page(e)} id="next">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="dis-next-btn" disabled>
                                                    <span className="page-item-btn-inner">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>}
                                        {this.state.current != 0 && this.state.next - 1 != this.state.maxPage && this.state.current != undefined && this.state.maxPage != this.state.current ? <li >
                                            <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                <span className="page-item-btn-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </span>
                                            </button>
                                        </li> : <li >
                                                <button className="dis-last-btn" disabled>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                    </svg>
                                                </button>
                                            </li>}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.dReport ? <DownloadReportPi {...this.state} {...this.props} oncloseErr={(e) => this.oncloseErr(e)} pushedPi={this.state.pushedPi} closeReport={(e) => this.closeReport(e)} /> : null}
                {this.state.viewDetails ? <ViewDetailPi {...this.state} {...this.props} closeDetails={(e) => this.closeDetails(e)} statusUpdate={this.statusUpdate}/>:null}

                {/* {this.state.viewDetails ? <LineItemData  {...this.state} {...this.props} lineItem="PILINEITEM" customHeaders={this.state.customHeaders} fixedHeaderData={this.state.fixedHeaderData} headerConfigDataState={this.state.headerConfigDataState} closeLineItem={(e) => this.closeLineItem(e)} lineItemData={this.state.lineItemData} lineItemCustomHeaders={this.state.lineItemCustomHeaders} lineItemDefaultHeaders={this.state.lineItemDefaultHeaders} lineItemFixedHeaders={this.state.lineItemFixedHeaders} /> : null} */}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
                {this.state.poErrorMsg ? <PoError oncloseErr={(e) => this.oncloseErr(e)} errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {/* {this.state.piInvoiceModal ? <PiInvoicePdfModal {...this.props} status={this.state.piStatus} orderDetailId={this.state.orderDetailId} piPdf={this.state.piPdf} closePiInvoice={(e) => this.closePiInvoice(e)} /> : null} */}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {/* {this.state.filter ? <PiHistoryFilter {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} updateFilter={(e) => this.updateFilter(e)} /> : null} */}
                {this.state.deleteConfirmModal ? <ConfirmModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeDeleteConfirmModal(e)} confirmDeleteForm={(e) => this.confirmDeleteForm(e)} /> : null}
                {this.state.isRemark && <RemarkModal {...this.state} {...this.props} cancelRemarkRequest={this.cancelRemarkRequest} statusUpdateConfirm={this.statusUpdateConfirm}/>}
                {/* {this.state.filter ? <PiHistoryFilter {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} updateFilter={(e) => this.updateFilter(e)} /> : null} */}
                {/* </div> */}
            </div>
        );
    }
}

export default PiHistoryNew;
