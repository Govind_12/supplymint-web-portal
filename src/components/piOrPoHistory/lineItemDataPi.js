import React from "react";
import moment from "moment";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import PoHistoryFilter from "./poHistoryFilter";
import ToastLoader from "../loaders/toastLoader";
import LineItemColoumSetting from "./lineItemColoumSetting"
import ConfirmationSummaryModal from "../replenishment/confirmationReset";
import Pagination from '../pagination';
import ViewImage from "./viewImageModal";

class LineItemDataPi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmModal: false,
            setUdfDetails:false,
            viewImage: false,
            lineItemData: this.props.expandedLineItem,
            lineItem: [],
            imagePath: "",
        }
    }
    filtercatDescArray = (data1) => {
        var filteredObject = {}
        Object.keys(data1).forEach(el => (
            (el.includes("Name") || el.includes("catRemark")) ?
                filteredObject[el] = data1[el]
                : null
        ))
        return filteredObject
    }
    componentDidMount(){
        let lineItemTemp = []
        let catDescTemp = []
        let itemUdfTemp = []
        let setUdfArrayTemp = []
        let catDescHeaderTemp = {}
        let itemUdfHeadertemp = {}
        let setUdfHeaderTemp = {}
        let lineItemHeaderTemp = {}
        let tempHeaders = _.cloneDeep(this.props.lineItemCustomHeaders)

        this.state.lineItemData.map((data) => {
            lineItemTemp = data.lineItem
            catDescTemp.push(data.catDescArray) 
            itemUdfTemp.push(data.itemUdfArray)
            data.lineItem.map(data1 => {
                setUdfArrayTemp.push(data1.setUdfArray)
            })
        })
        catDescHeaderTemp = this.filtercatDescArray(tempHeaders.catDescHeader)
        itemUdfHeadertemp = tempHeaders.itemUdfHeader
        setUdfHeaderTemp = tempHeaders.lineItem.setUdfHeader
        lineItemHeaderTemp = tempHeaders.lineItem
        delete lineItemHeaderTemp.setUdfHeader


        this.setState({
            lineItem: lineItemTemp,
            catDescArray: catDescTemp,
            itemUdfArray: itemUdfTemp,
            setUdfArray: setUdfArrayTemp,
            catDescHeader: catDescHeaderTemp,
            itemUdfHeader: itemUdfHeadertemp,
            setUdfHeader: setUdfHeaderTemp,
            lineItemHeader: lineItemHeaderTemp
        })
    }

    openSetUdfDetails(e) {
        e.preventDefault();
        this.setState({
            setUdfDetails: !this.state.setUdfDetails
        });
    }

    closeSetUdfDetails = () => {
        this.setState({ setUdfDetails: false 
        });
    }
    openViewImage =(e, imagePath)=> {
        e.preventDefault();
        this.setState({
            viewImage: !this.state.viewImage,
            imagePath: imagePath
        });
    }
    CloseViewImage = () => {
        this.setState({ viewImage: false 
        });
    }



    render() {
        return (
            <React.Fragment>
                {/* <div className="backdrop-transparent"></div> */}
                <div className="vd-sticky-table">
                    <div className="vdst-expand">
                        {/* <div className="livdm-head">
                            <h3>Line Item Details</h3>
                            <button type="button" className="livdmh-close" onClick={(e) => this.props.closeLineItem(e)}><img src={require('../../assets/clearSearch.svg')} /></button>
                        </div> */}
                        <div className="vdste-tab">
                            <div className="lidm-tab">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item active" >
                                        <a className="nav-link p-l-0" href="#lineitem" role="tab" data-toggle="tab">Line Item</a>
                                    </li>
                                    {this.state.catDescHeader !== null && this.state.catDescHeader !== undefined && Object.keys(this.state.catDescHeader).length ? <li className="nav-item" >
                                        <a className="nav-link" href="#catdesc" role="tab" data-toggle="tab">Cat/Desc</a>
                                    </li>: null}
                                    {this.state.itemUdfHeader !== null && this.state.itemUdfHeader !== undefined && Object.keys(this.state.itemUdfHeader).length ? <li className="nav-item" >
                                        <a className="nav-link" href="#itemudf" role="tab" data-toggle="tab">Item Udf</a>
                                    </li> : null}
                                </ul>
                            </div>
                        </div>
                        <div className="tab-content">
                            <div className="tab-pane fade in active" id="lineitem" role="tabpanel">
                                <div className="vst-manage">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                {this.state.lineItemHeader != null ? Object.keys(this.state.lineItemHeader).map((data, key) => (
                                                        <th><label>{this.state.lineItemHeader[data]}</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                )): null }
                                                {this.state.setUdfHeader != undefined ? Object.keys(this.state.setUdfHeader).map((data1, key1) => (
                                                        this.state.setUdfDetails && <th key = {key1}><label>{this.state.setUdfHeader[data1]}</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                )): null}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.lineItem != undefined ? this.state.lineItem.length != 0 ? this.state.lineItem.map((data, lkey) => (
                                                    <tr key={lkey}>
                                                        {Object.keys(this.state.lineItemHeader).length > 0 ? Object.keys(this.state.lineItemHeader).map((hdata, key) => (
                                                            hdata === "imagePath" ?
                                                            (data[hdata] !== "" && data[hdata] !== null ? <td key={key}><label className="lidm-view-img" onClick={(e) => this.openViewImage(e, data[hdata])}>View Images</label></td>
                                                                : <td key={key}><label className="bold">No Images</label></td>)
                                                            : <td key={key} ><label className="bold">{data[hdata]}</label></td>
                                                        )) : null }
                                                        
                                                        {this.state.setUdfDetails ?
                                                            this.state.setUdfArray.length != 0 ? this.state.setUdfArray.map((data, skey) => (
                                                                Object.keys(this.state.setUdfHeader).length > 0 ? Object.keys(this.state.setUdfHeader).map((hdata, key) =>(  
                                                                    lkey == skey && <td key={key} className="sudetail"><label>{data[hdata]}</label></td>))
                                                                : null
                                                            )) : null
                                                        : null }
                                                    </tr>)) 
                                                : null 
                                            : null}
                                        </tbody>
                                    </table>
                                    {this.props.isSet && this.state.setUdfHeader != undefined && this.state.setUdfHeader !== null && Object.keys(this.state.setUdfHeader).length ? <div className={this.state.setUdfDetails === false ? "lidmt-button" : "lidmt-button lidmt-button-fff"}>
                                        <button className="lidmt-arrow" type="button" onClick={(e) => this.openSetUdfDetails(e)}><img src={require('../../assets/right-arrow.svg')} /></button>
                                    </div> : null}
                                </div>
                            </div>
                            <div className="tab-pane fade" id="catdesc" role="tabpanel">
                                <div className="vst-manage">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                {this.state.catDescHeader != null ? Object.keys(this.state.catDescHeader).map((data, key) => (
                                                    <th key = {key}><label>{this.state.catDescHeader[data]}</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                )): null}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.catDescArray != undefined ? this.state.catDescArray.length != 0 ? this.state.catDescArray.map((data, key) => (
                                                <tr key={key}>
                                                    {Object.keys(this.state.catDescHeader).length > 0 ? Object.keys(this.state.catDescHeader).map((hdata, key) =>(  
                                                        <td key={key} className="sudetail"><label>{data[hdata]}</label></td>))
                                                    : null}
                                                </tr>)) : null 
                                            : null }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="itemudf" role="tabpanel">
                                <div className="vst-manage">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                {this.state.itemUdfHeader != null ? Object.keys(this.state.itemUdfHeader).map((data, key) => (
                                                        <th key = {key}><label>{this.state.itemUdfHeader[data]}</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                                )): null}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.itemUdfArray != undefined ? this.state.itemUdfArray.length != 0 ? this.state.itemUdfArray.map((data, key) => (
                                                <tr key={key}>
                                                    {Object.keys(this.state.itemUdfHeader).length > 0 ? Object.keys(this.state.itemUdfHeader).map((hdata, key) =>(  
                                                        <td key={key} className="sudetail"><label>{data[hdata]}</label></td>))
                                                    : null}
                                                </tr>)) : null 
                                            : null }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} {...this.props} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.liresetColumn(e)} /> : null}
                    {this.state.viewImage && <ViewImage {...this.props} {...this.state} CloseViewImage={this.CloseViewImage} imagePath={this.state.imagePath}/>}
                </div>
            </React.Fragment>
        );
    }
}

export default LineItemDataPi;
