import React from "react";
import axios from 'axios';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import eyeIcon from "../../assets/actions-buttons.svg";
import rejectIcon from "../../assets/reject.svg";
import pendingApprove from "../../assets/exclamation-summary.svg";
import approve from "../../assets/approve.svg";
import { CONFIG } from "../../config/index";
import PiHistoryFilter from "./piHistoryFilter";
import PiInvoicePdfModal from "./piInvoicePdfModal";
import moment from "moment";
import ToastLoader from "../loaders/toastLoader";

class PiHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openRightBar: false,
            piStatus: "",
            rightbar: false,
            piHistoryState: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: 1,
            no: 1,
            loader: false,
            success: false,
            alert: false,
            successMessage: "",
            errorMessage: "",
            errorCode: "",
            code: "",
            filter: false,
            filterBar: false,
            piInvoiceModal: false,
            piPdf: "",
            orderDetailId: "",
            search: "",
            articleCode: "",
            generatedDate: "",
            status: "",
            indentId: "",
            supplierName: "",
            division: "",
            department: "",
            section: "",
            vendorDistrict: "",
            deliveryDateFrom: "",
            deliveryDateTo: "",
            description: "",
            toastLoader: false,
            toastMsg: ""
        }
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: true,
            filterBar: !this.state.filterBar

        });
    }

    handleChange(e) {
        if (e.target.id == "search") {
            this.setState({
                search: e.target.value
            })
        }
    }
    viewPi(id, path) {


        let c = this.state.piHistoryState
        for (var i = 0; i < c.length; i++) {
            if (c[i].orderDetailId == id) {

                this.setState({
                    orderDetailId: id,
                    piInvoiceModal: true,
                    piStatus: c[i].status
                })
                this.piDownload(path);
            }
        }
    }
    closePiInvoice() {
        this.setState({
            piInvoiceModal: !this.state.piInvoiceModal
        })
    }

    componentWillMount() {
        if (!this.props.purchaseIndent.piHistory.isSuccess) {
            let data = {
                no: 1,
                type: 1,
                articleCode: "",
                search: "",
                generatedDate: "",
                status: "",
                indentId: "",
                supplierName: "",
                division: "",
                department: "",
                section: "",
                slCityName: "",
                deliveryDateFrom: "",
                deliveryDateTo: "",
                desc2Code: "",
            }
            this.props.piHistoryRequest(data);
            this.setState({
                loader: false
            })
        } else
            if (this.props.purchaseIndent.piHistory.isSuccess) {
                if (this.props.purchaseIndent.piHistory.data.resource != null) {
                    this.setState({
                        piHistoryState: this.props.purchaseIndent.piHistory.data.resource,
                        prev: this.props.purchaseIndent.piHistory.data.prePage,
                        current: this.props.purchaseIndent.piHistory.data.currPage,
                        next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                        maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                        loader: false
                    })
                }

                else {
                    this.setState({
                        piHistoryState: this.props.purchaseIndent.piHistory.data.resource,
                        prev: this.props.purchaseIndent.piHistory.data.prePage,
                        current: this.props.purchaseIndent.piHistory.data.currPage,
                        next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                        maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                        loader: false
                    })
                }

            }
    }
    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.piHistory.isLoading) {
            this.setState({
                loader: true
            })
        }

        else if (nextProps.purchaseIndent.piHistory.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.piHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.piHistory.message.error.errorMessage,
                errorCode: nextProps.purchaseIndent.piHistory.message.error == undefined ? undefined : nextProps.purchaseIndent.piHistory.message.error.errorCode,
                code: nextProps.purchaseIndent.piHistory.message.status,
                alert: true,
                loader: false
            })
            this.props.piHistoryClear()
        } else if (nextProps.purchaseIndent.piHistory.isSuccess) {
            if (nextProps.purchaseIndent.piHistory.data.resource != null) {
                this.setState({
                    piHistoryState: nextProps.purchaseIndent.piHistory.data.resource,
                    prev: nextProps.purchaseIndent.piHistory.data.prePage,
                    current: nextProps.purchaseIndent.piHistory.data.currPage,
                    next: nextProps.purchaseIndent.piHistory.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.piHistory.data.maxPage,
                })
            }
            this.setState({
                loader: false,
                piHistoryState: nextProps.purchaseIndent.piHistory.data.resource,
                prev: nextProps.purchaseIndent.piHistory.data.prePage,
                current: nextProps.purchaseIndent.piHistory.data.currPage,
                next: nextProps.purchaseIndent.piHistory.data.currPage + 1,
                maxPage: nextProps.purchaseIndent.piHistory.data.maxPage,
            })
            this.props.piHistoryClear()
        }
        if (nextProps.purchaseIndent.piUpdate.isLoading) {
            this.setState({
                loader: true
            })
        }
        else if (nextProps.purchaseIndent.piUpdate.isSuccess) {
            this.setState({
                loader: false,
                successMessage: nextProps.purchaseIndent.piUpdate.data.message,
                alert: false,
                success: true,
            })
            this.props.piUpdateRequest()
            this.closePiInvoice();
        } else if (nextProps.purchaseIndent.piUpdate.isError) {
            this.setState({
                errorMessage: nextProps.purchaseIndent.piUpdate.message.error == undefined ? undefined : nextProps.purchaseIndent.piUpdate.message.errorMessage,
                errorCode: nextProps.purchaseIndent.piUpdate.message.error == undefined ? undefined : nextProps.purchaseIndent.piUpdate.message.error.errorCode,
                code: nextProps.purchaseIndent.piUpdate.message.status,
                alert: true,
                success: false,
                loader: false
            })
            this.props.piUpdateRequest()
        }
    }
    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,
            generatedDate: data.generatedDate,
            status: data.status,
            indentId: data.indentId,
            supplierName: data.supplierName,
            division: data.division,
            section: data.section,
            department: data.department,
            search: data.search,
            articleCode: data.articleCode
        });
    }
    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            articleCode: "",
            search: "",
            generatedDate: "",
            status: "",
            indentId: "",
            supplierName: "",
            division: "",
            department: "",
            section: ""
        })
        let data = {
            no: 1,
            type: 1,
            articleCode: "",
            search: "",
            generatedDate: "",
            status: "",
            indentId: "",
            supplierName: "",
            division: "",
            department: "",
            section: "",
            slCityName: "",
            deliveryDateFrom: "",
            deliveryDateTo: "",
            desc2Code: "",
        }
        this.props.piHistoryRequest(data);
    }
    onSearch(e) {
        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false,
                })
            }, 1500)
        } else {
            this.setState({
                type: 3,
                no: 1,
                articleCode: "",
                search: this.state.search,
                generatedDate: "",
                status: "",
                indentId: "",
                supplierName: "",
                division: "",
                department: "",
                section: "",
                slCityName: "",
                deliveryDateFrom: "",
                deliveryDateTo: "",
                desc2Code: "",
            })
            let data = {
                type: 3,
                no: 1,
                articleCode: "",
                search: this.state.search,
                generatedDate: "",
                status: "",
                indentId: "",
                supplierName: "",
                division: "",
                department: "",
                section: "",
                slCityName: "",
                deliveryDateFrom: "",
                deliveryDateTo: "",
                desc2Code: "",
            };
            this.props.piHistoryRequest(data);
        }
    }

    piDownload(path) {


        this.setState({
            loader: true,
            piPdf: ""
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        if (path != null) {
            axios.get(`${CONFIG.BASE_URL}/admin/pi/pdf?savedPath=${path}`, { headers: headers })
                .then(res => {
                    this.setState({
                        loader: false
                    })
                    if (res.data.data.resource != null) {
                        this.setState({
                            piPdf: res.data.data.resource
                        })

                    } else {
                        this.setState({
                            piPdf: ""
                        })
                    }
                }).catch((error) => {
                })
        }
    }
    page(e) {
        if (e.target.id == "prev") {
            if (this.state.current == undefined) {

            } else {
                this.setState({
                    prev: this.props.purchaseIndent.piHistory.data.prePage,
                    current: this.props.purchaseIndent.piHistory.data.currPage,
                    next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
                })
                if (this.props.purchaseIndent.piHistory.data.currPage != 0) {
                    let data = {
                        no: this.props.purchaseIndent.piHistory.data.currPage - 1,
                        type: this.state.type,
                        search: this.state.search,
                        articleCode: this.state.articleCode,
                        generatedDate: this.state.generatedDate,
                        status: this.state.status,
                        indentId: this.state.indentId,
                        supplierName: this.state.indentId,
                        division: this.state.division,
                        item: this.state.item,
                        department: this.state.department,
                        section: this.state.section,
                        slCityName: this.state.vendorDistrict,
                        desc2Code: this.state.description,
                        deliveryDateFrom: this.state.deliveryDateFrom,
                        deliveryDateTo: this.state.deliveryDateTo

                    }
                    this.props.piHistoryRequest(data);
                }

            }

        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.piHistory.data.prePage,
                current: this.props.purchaseIndent.piHistory.data.currPage,
                next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.piHistory.data.currPage != this.props.purchaseIndent.piHistory.data.maxPage) {
                let data = {

                    no: this.props.purchaseIndent.piHistory.data.currPage + 1,
                    type: this.state.type,
                    search: this.state.search,
                    articleCode: this.state.articleCode,

                    generatedDate: this.state.generatedDate,
                    status: this.state.status,
                    indentId: this.state.indentId,
                    supplierName: this.state.indentId,
                    division: this.state.division,
                    item: this.state.item,
                    department: this.state.department,
                    section: this.state.section,
                    slCityName: this.state.vendorDistrict,
                    desc2Code: this.state.description,
                    deliveryDateFrom: this.state.deliveryDateFrom,
                    deliveryDateTo: this.state.deliveryDateTo


                }
                this.props.piHistoryRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.piHistory.data.prePage,
                current: this.props.purchaseIndent.piHistory.data.currPage,
                next: this.props.purchaseIndent.piHistory.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piHistory.data.maxPage,
            })
            if (this.props.purchaseIndent.piHistory.data.currPage <= this.props.purchaseIndent.piHistory.data.maxPage) {
                let data = {

                    no: 1,
                    type: this.state.type,
                    search: this.state.search,
                    articleCode: this.state.articleCode,

                    generatedDate: this.state.generatedDate,
                    status: this.state.status,
                    indentId: this.state.indentId,
                    supplierName: this.state.indentId,
                    division: this.state.division,
                    item: this.state.item,
                    department: this.state.department,
                    section: this.state.section,
                    slCityName: this.state.vendorDistrict,
                    desc2Code: this.state.description,
                    deliveryDateFrom: this.state.deliveryDateFrom,
                    deliveryDateTo: this.state.deliveryDateTo

                }
                this.props.piHistoryRequest(data)
            }

        }
    }


    render() {
        const { search } = this.state
        return (
            <div className="container-fluid">

                <div className="container_div pih-pg" id="vendor_manage">

                    <div className="col-md-12 col-sm-12 col-xs-12 piHistoryHead">
                        <div className="container_content heightAuto">
                            <div className="col-md-6 col-sm-6 pad-0">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            PURCHASE INDENT HISTORY
                            </label>
                                    </li>
                                    <li>

                                    </li>

                                    <li className="m-top-30">

                                        <ul className="list-inline m-top-10">

                                            <li>
                                                <button className="filter_button" onClick={(e) => this.openFilter(e)} data-toggle="modal" data-target="#myModal">
                                                    FILTER

                                            <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                        <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z" />
                                                    </svg>
                                                </button>
                                            </li>
                                            {this.state.type != 2 ? null : <span className="clearFilterBtn filterBtnLft" onClick={(e) => this.onClearSearch(e)} >Clear Filter</span>}
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-md-6 col-sm-6 pad-0">

                                <ul className="list-inline search_list manageSearch m-top-80">
                                    <form onSubmit={(e) => this.onSearch(e)}>
                                        <li>
                                            <input type="search" id="search" onChange={(e) => this.handleChange(e)} value={search} placeholder="Type to Search..." className="search_bar" />
                                            <button className="searchWithBar" onClick={(e) => this.onSearch(e)}> Search
                                            <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                    <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                    </path></svg>
                                            </button>
                                        </li>
                                    </form>
                                </ul>
                                {this.state.type == 3 ? <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span> : null}
                            </div>

                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3 piHistoryMain">
                                <div className="scrollableOrgansation scrollableTableFixed roleTableHeight">
                                    <table className="table table zui-table roleTable  border-bot-table tableOddColor m-bot-0">
                                        <thead>
                                            <tr>
                                                <th className="fixed-side-history fixed-side1">
                                                    <label> Action</label>
                                                </th>
                                                <th>
                                                    <label>STATUS</label>
                                                </th>
                                                <th>
                                                    <label >INDENT ID</label>
                                                </th>
                                                <th>
                                                    <label >ARTICLE CODE</label>
                                                </th>
                                                <th>
                                                    <label>SUPPLIER NAME</label>
                                                </th>
                                                <th>
                                                    <label>CITY NAME</label>
                                                </th>
                                                <th>
                                                    <label >DIVISION</label>
                                                </th>
                                                <th>
                                                    <label >SECTION</label>
                                                </th>
                                                <th>
                                                    <label >DEPARTMENT</label>
                                                </th>
                                                <th>
                                                    <label>GENERATED DATE</label>
                                                </th>
                                                <th>
                                                    <label>DESCRIPTION 2</label>
                                                </th>
                                                <th>
                                                    <label>DELIVERY DATE</label>
                                                </th>

                                            </tr>
                                        </thead>

                                        <tbody>

                                            {this.state.piHistoryState == undefined || this.state.piHistoryState.length == 0 || this.state.piHistoryState == null ? <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr> : this.state.piHistoryState.map((data, key) => (
                                                <tr key={key}>
                                                    <td className="fixed-side-history fixed-side1">
                                                        <span className="pad-top-3">

                                                            <img src={eyeIcon} onClick={() => this.viewPi(`${data.orderDetailId}`, `${data.pdfPath}`)} />
                                                        </span>

                                                    </td>
                                                    <td>
                                                        {data.status == "PENDING" ? <img className="imgHeight" src={pendingApprove} /> : data.status == "APPROVED" ? <img src={approve} className="imgHeight" /> : data.status == "REJECTED" ? <img src={rejectIcon} className="imgHeight" /> : null}

                                                        <label className="pad-lft-10">{data.status}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.orderDetailId}</label>
                                                    </td>

                                                    <td><label>{data.hl4Code}</label></td>


                                                    <td>
                                                        <label>{data.slName}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.slCityName}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.hl1Name}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.hl2Name}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.hl3Name}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.createdTime}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.desc2Name}</label>
                                                    </td>
                                                    <td>
                                                        <label>{data.deliveryDate != null ? data.deliveryDate : ""}</label>
                                                    </td>




                                                </tr>))}
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <div className="pagerDiv">
                                <ul className="list-inline pagination">
                                    {this.state.current == 1 || this.state.current == 0 || this.state.current == undefined ? <li >
                                        <button className="PageFirstBtn pointerNone" >
                                            First
                                    </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="first" >
                                                First
                                        </button>
                                        </li>}
                                    {this.state.prev != 0 && this.state.current != undefined ? <li >
                                        <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                            Prev
                                    </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn" disabled>
                                                Prev
                                        </button>
                                        </li>}
                                    <li>
                                        <button className="PageFirstBtn pointerNone">
                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                        </button>
                                    </li>
                                    {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                        <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                            Next
                  </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn borderNone" disabled>
                                                Next
                  </button>
                                        </li> : <li >
                                            <button className="PageFirstBtn borderNone" disabled>
                                                Next
                  </button>
                                        </li>}

                                </ul>
                            </div>

                        </div>
                    </div>



                    {this.state.loader ? <FilterLoader /> : null}
                    {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                    {this.state.piInvoiceModal ? <PiInvoicePdfModal {...this.props} status={this.state.piStatus} orderDetailId={this.state.orderDetailId} piPdf={this.state.piPdf} closePiInvoice={(e) => this.closePiInvoice(e)} /> : null}
                    {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                    {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                    {this.state.filter ? <PiHistoryFilter {...this.props} filterBar={this.state.filterBar} closeFilter={(e) => this.openFilter(e)} updateFilter={(e) => this.updateFilter(e)} /> : null}
                </div>
            </div>
        );
    }
}

export default PiHistory;
