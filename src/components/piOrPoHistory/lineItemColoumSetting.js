import React from 'react';
import { Link } from 'react-router-dom';
import successIcon from '../../assets/greencheck.png'
import closeSearch from "../../assets/close-recently.svg"
import _ from 'lodash';
import cross from '../../assets/group-4.svg';

class LineItemColoumSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            liColoumSetting: this.props.liColoumSetting,
            search:""
        }
    }

    openLIColoumSetting(data) {
        this.props.openLIColoumSetting(data)
    }

    liCloseColumn(data) {
        this.props.liCloseColumn(data)
    }
    handleChange(e){
        this.setState({
            search:e.target.value
        })
    }
    clearSearch(){
        this.setState({
            search:""
        })
    }
    render() {
       
        const {search} = this.state
        var result = _.filter(this.props.liFixedHeader, function (data) {
            return _.startsWith(data.toLowerCase(), search.toLowerCase());
        });
        return (
            <div>
                {this.props.liColoumSetting ? <div className="columnFilterGeneric" >
                    <span className="glyphicon glyphicon-menu-right" onClick={(e) => this.openLIColoumSetting("false")}></span>
                    <div className="columnSetting" onClick={(e) => this.openLIColoumSetting("false")}>Columns Setting</div>
                    <div className="columnFilterDropDown">
                        <div className="col-md-12 pad-0 filterHeader">
                            <div className="col-md-7 pad-0">
                                <input type="search" className="width100 inputSearch" value={this.state.search} onChange={(e)=>this.handleChange(e)} placeholder="Search Columns…" />
                               {this.state.search!=""? <div className="crossIcon cr-lf-bot"><img src={cross} onClick={(e)=>this.clearSearch(e)}/></div>:null}
                            </div>
                            <div className="col-md-5 pad-right-0 columns">
                                <div className="col-md-7 pad-0">
                                    <h3>Visible Columns <span>{this.props.liHeaderCondition ? this.props.liGetHeaderConfig.length: this.props.liCustomHeadersState.length}</span></h3>
                                </div>
                                <div className="col-md-5 pad-0 textCenter">
                                    <button className="resetBtn" onClick={(e) => this.props.liresetColumnConfirmation(e)}>Reset to default</button>
                                    {!this.props.liHeaderCondition ?  <button className={this.props.liCustomHeadersState.length == 0 || this.props.liSaveState.length == 0 ? "opacity saveBtn": "saveBtn" } onClick={(e) =>this.props.liCustomHeadersState.length == 0 || this.props.liSaveState.length == 0 ?null : this.props.lisaveColumnSetting(e)}>Save</button>:
                                    <button className={this.props.liGetHeaderConfig.length == 0 || this.props.liSaveState.length == 0 ? "opacity saveBtn": "saveBtn" } onClick={(e) =>this.props.liGetHeaderConfig.length == 0 || this.props.liSaveState.length == 0 ? null : this.props.lisaveColumnSetting(e)}>Save</button>}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-7 pad-0">
                            <div className="filterLeftHead">
                                <div className="col-md-12 identifier">
                                    <h2>Fixed</h2>
                                    <ul className="list-inline">
                                        {result.length!=0?result.map((data, key) => (<div key={key} className="inlineBlock col-md-4 pad-lft-0">
                                            {this.props.liHeaderCondition ? <li className={!this.props.liGetHeaderConfig.includes(`${data}`) ? "active" : "opacity active"} onClick={(e) => !this.props.liGetHeaderConfig.includes(`${data}`) ? this.props.liPushColumnData(data) : null}>{data}</li> :
                                                <li className={!this.props.liCustomHeadersState.includes(`${data}`) ? "active" : "opacity active"} onClick={(e) => !this.props.liCustomHeadersState.includes(`${data}`) ? this.props.liPushColumnData(data) : null}>{data}</li>}
                                        </div>
                                        )):"No Data Found"}
                                        {/* <li>Assortment</li>
                                <li>Updated MBQ</li>
                                <li>Closing Stock</li>
                                <li className="active">Opening Stock</li>
                                <li className="active">Page Views</li>
                                <li>Requirements</li> */}
                                    </ul>
                                </div>
                                {/* <div className="col-md-12 identifier">
                                    <h2>Custom</h2>
                                    <ul className="list-inline">
                                        {this.props.liCustomHeadersState.map((data, key) => (
                                            <li key={key} className="active">{data}</li>
                                        ))}
                                    </ul>
                                </div> */}
                            </div>
                        </div>
                        <div className="col-md-5 pad-0">
                            <div className="filterRightHead">
                                {/* <div className="col-md-12 savedColumn">
                            <h3>Saved Column presets</h3>
                            <label>My setting</label>
                            <label>Setting 2</label>
                        </div> */}
                                <div className="col-md-12 visibleColumn">
                                    <h3>All Visible Columns</h3>
                                <div className="row">
                                    {this.props.liHeaderCondition ? this.props.liGetHeaderConfig.map((data, key) => (
                                        <div key={key} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter"><label className="alignMiddle displayFlex"><span className="eachVisibleColumn">{data}</span>
                                            <span onClick={(e) => this.liCloseColumn(data)}> <img src={closeSearch} /></span></label></div>
                                    )) : this.props.liCustomHeadersState.map((dataa, keyy) => (
                                        <div key={keyy} className="col-md-6 pad-0 m-top-3 displayFlex justifyCenter">
                                            <label className="alignMiddle displayFlex">
                                            <span className="eachVisibleColumn">{dataa}</span><span onClick={(e) => this.liCloseColumn(dataa)}> <img className="float_Right displayPointer" src={closeSearch} /></span>
                                            </label>
                                        </div>
                                    ))}
                                    </div>
                                    {/* <div className="col-md-6 pad-0 m-top-3"><label>MBQ</label></div>
                            <div className="col-md-6 pad-0"> <label>Updated MBQ</label></div>
                            <div className="col-md-6 pad-0"><label>Closing Stock</label></div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : <div className="columnFilterGeneric" onClick={(e) => this.openLIColoumSetting("true")}>
                        <span className="glyphicon glyphicon-menu-left"></span></div>}
            </div>
        )
    }
}

export default LineItemColoumSetting;