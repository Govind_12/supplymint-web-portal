import React from 'react'
import Pagination from '../../components/pagination';
import ExpandAdhocPendingRequest from './expandAdhocPendingRequest';

class AdhocPendingRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expandRow: '',
            selectedItems: [],
            expandedRowData: []
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getAdhoc.isLoading) {
            this.setState({
                expandRow: '', //REMOVE COMMENT TO MAKE THE ROW COLLAPSE AFTER THE CHANGES ARE SAVED.
                selectedItems: [],
                expandedRowData: []
            });
        }

        if (nextProps.seasonPlanning.getAdhocItemsPerWO.isSuccess) {
            this.setState({expandedRowData: nextProps.seasonPlanning.getAdhocItemsPerWO.data.resource.response});
        }
        else if (nextProps.seasonPlanning.getAdhocItemsPerWO.isError) {
            this.setState({expandedRowData: []});
        }
    }

    expandColumn(e, id) {
        e.preventDefault();
        this.props.expandColumn(e, id);
        this.setState({
            expandRow: this.state.expandRow == id ? '' : id
        });
    }

    selectAll = () => {
        if (this.state.selectedItems.length !== this.props.adhocData.length) {
            let selectedItems = [];
            this.props.adhocData.forEach((item) => selectedItems.push(item.workOrder));
            this.setState({selectedItems});
            this.props.getSelectedItems(selectedItems);
        }
        else {
            this.setState({selectedItems: []});
            this.props.getSelectedItems([]);
        }
    }

    render () {
        console.log("props", this.props);
        console.log("state", this.state);
        return (
            <div className="expand-new-table">
                <div className="manage-table">
                    <div className="columnFilterGeneric">
                        <span className="glyphicon glyphicon-menu-left"></span>
                    </div>
                    <table className="table gen-main-table">
                        <thead>
                            <tr>
                                <th className="fix-action-btn width90">
                                    <ul className="rab-refresh">
                                        <li className="rab-rinner">{
                                            // this.state.selectedItems.length !== 0 && this.state.selectedItems.length === this.props.adhocData.length ?
                                            // <label className="checkBoxLabel0"><input type="checkBox" name="selectAll" checked onChange={() => {this.setState({selectedItems: []}); this.props.getSelectedItems([])}} /><span className="checkmark1"></span></label> :
                                            // <label className="checkBoxLabel0"><input type="checkBox" name="selectAll" onChange={() => {let selectedItems = []; this.props.adhocData.forEach((item) => selectedItems.concat(item.workOrder)); this.props.getSelectedItems(selectedItems); this.setState({selectedItems})}} /><span className="checkmark1"></span></label>
                                        }
                                            <label className="checkBoxLabel0"><input type="checkBox" name="selectAll" checked={this.state.selectedItems.length !== 0 && this.state.selectedItems.length === this.props.adhocData.length} onChange={this.selectAll} /><span className="checkmark1"></span></label>
                                            {/* <span className="select-all-text">10 line items selected</span> */}
                                        </li>
                                        <li className="rab-rinner">
                                            <span><img src={require('../../assets/reload.svg')} /></span>
                                        </li>
                                    </ul>
                                </th>
                                {
                                    Object.keys(this.props.tableHeaders).map((key) => <th key={key} className={this.props.requestData.sortedBy == key && this.props.requestData.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.props.sortData(key)}><label>{this.props.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)
                                }
                            </tr>
                        </thead>
                        <tbody>{
                            this.props.loading ?
                            (<td><label>Loading data...</label></td>) :
                            this.props.adhocData.length == 0 || Object.keys(this.props.tableHeaders).length == 0 ?
                            (<td><label>No data found!</label></td>) :
                            this.props.adhocData.map((item) => (
                                <React.Fragment>
                                    <tr>
                                        <td className="fix-action-btn width90">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">{
                                                        this.state.selectedItems.indexOf(item.workOrder) == -1 ?
                                                        <input type="checkbox"
                                                            onChange={() => {
                                                                this.props.getSelectedItems(this.state.selectedItems.concat(item.workOrder));
                                                                this.setState({
                                                                    selectedItems: this.state.selectedItems.concat(item.workOrder)
                                                                });
                                                            }}
                                                        /> :
                                                        <input type="checkbox" checked
                                                            onChange={() => {
                                                                let newItems = this.state.selectedItems;
                                                                newItems.splice(this.state.selectedItems.indexOf(item.workOrder), 1);
                                                                this.props.getSelectedItems(newItems);
                                                                this.setState({
                                                                    selectedItems: newItems
                                                                });
                                                            }}
                                                        />
                                                    }<span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-delete-btn" onClick={() => this.props.deleteItem(item.workOrder)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                                <li className="til-inner til-add-btn" onClick={(e) => this.expandColumn(e, item.workOrder)}>
                                                    {this.state.expandRow == item.workOrder ? <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20 20">
                                                        <path fill="#fff" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"></path>
                                                    </svg> :
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20 20">
                                                        <path fill="#fff" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                    </svg>}
                                                    <span className="generic-tooltip">Expand</span>
                                                </li>
                                            </ul>
                                        </td>
                                        {
                                            Object.keys(this.props.tableHeaders).map((key) => <td><label>{item[key]}</label></td>)
                                        }
                                    </tr>
                                    {this.state.expandRow == item.workOrder ? <tr><td colSpan="100%" className="pad-0"><ExpandAdhocPendingRequest data={this.state.expandedRowData} headers={this.props.expandColumnHeaders} save={this.props.saveExpandedRow} /> </td></tr> : null}
                                </React.Fragment>
                            ))
                        }</tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default AdhocPendingRequest;