import React from 'react';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import Confirm from '../loaders/arsConfirm';

class CreateEventMappingPlanning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            toastLoader: false,
            toastMsg: "",
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',

            sitePlanningFilters: [],
            newEvent: [{
                id: this.props.location.state == undefined ? 0 : this.props.location.state.id,
                storeCode: this.props.location.state == undefined ? '' : this.props.location.state.storeCode,
                storeName: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.storeName,
                eventName: this.props.location.state == undefined ? '' : this.props.location.state.eventName,
                multiplier: this.props.location.state == undefined ? '' : this.props.location.state.multiplier,
                eventStartDate: this.props.location.state == undefined ? '' : this.props.location.state.eventStartDate,
                eventEndDate: this.props.location.state == undefined ? '' : this.props.location.state.eventEndDate,
                eventDays: this.props.location.state == undefined ? '' : this.props.location.state.eventDays
            }],
            errorSite: [false],
            errorEventName: [false],
            errorEventStartDate: [false],
            errorEventEndDate: [false],
            errorEventDays: [false],
            errorMultiplier: [false],
            siteName: "",
            searchSite: ""
        }
    }

    openSiteName = (e, index) => {
        e.preventDefault();
        this.setState({
            siteName: index
        }, () => document.addEventListener('click', this.closeSiteName));
    }
    closeSiteName = (e) => {
        if (e === undefined || (document.getElementById("siteNames") != null && !document.getElementById("siteNames").contains(e.target))) {
            this.setState({ siteName: "", searchSite: "" }, () => {
                document.removeEventListener('click', this.closeSiteName);
            });
        }
    }

    componentDidMount() {
        this.props.getSitePlanningFiltersRequest();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getSitePlanningFilters.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                sitePlanningFilters: nextProps.seasonPlanning.getSitePlanningFilters.data.resource
            });
            this.props.getSitePlanningFiltersClear();
        }
        else if (nextProps.seasonPlanning.getSitePlanningFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSitePlanningFilters.message.status,
                errorCode: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorMessage
            });
            this.props.getSitePlanningFiltersClear();
        }

        if (nextProps.seasonPlanning.saveEventPlanning.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveEventPlanning.data.resource.message, //nextProps.seasonPlanning.saveEventPlanning.data.message,
                error: false
            });
            this.props.saveEventPlanningClear();
        }
        else if (nextProps.seasonPlanning.saveEventPlanning.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveEventPlanning.message.status,
                errorCode: nextProps.seasonPlanning.saveEventPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveEventPlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveEventPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveEventPlanning.message.error.errorMessage
            });
            this.props.saveEventPlanningClear();
        }

        if (nextProps.seasonPlanning.getSitePlanningFilters.isLoading || nextProps.seasonPlanning.saveEventPlanning.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false
            });
        }
        // if (nextProps.seasonPlanning.getSitePlanningFilters.isSuccess) {
        //     this.setState({
        //         loading: false,
        //         sitePlanningFilters: nextProps.seasonPlanning.getSitePlanningFilters.data.resource
        //     })
        // }
        // else if (nextProps.seasonPlanning.getSitePlanningFilters.isLoading) {
        //     this.setState({
        //         loading: true
        //     });
        //     console.log("isLoading");
        // }
        // else if (nextProps.seasonPlanning.getSitePlanningFilters.isError) {
        //     this.setState({
        //         loading: false
        //     });
        //     console.log("Site Planning Filters Error");
        // }
    }

    validateSite = (site, index) => {
        if (site == 'Select Option') {
            let errorSite = this.state.errorSite;
            errorSite[index] = true;
            this.setState({
                errorSite
            });
            return true;
        }
        else {
            let errorSite = this.state.errorSite;
            errorSite[index] = false;
            this.setState({
                errorSite
            });
            return false;
        }
    }

    validateEventName = (eventName, index) => {
        if (eventName == '') {
            let errorEventName = this.state.errorEventName;
            errorEventName[index] = true;
            this.setState({
                errorEventName
            });
            return true;
        }
        else {
            let errorEventName = this.state.errorEventName;
            errorEventName[index] = false;
            this.setState({
                errorEventName
            });
            return false;
        }
    }

    validateEventStartDate = (eventStartDate, index) => {console.log("sdate");
        if (eventStartDate == '') {
            let errorEventStartDate = this.state.errorEventStartDate;
            errorEventStartDate[index] = true;
            this.setState({
                errorEventStartDate
            });
            return true;
        }
        else {
            let errorEventStartDate = this.state.errorEventStartDate;
            errorEventStartDate[index] = false;
            this.setState({
                errorEventStartDate
            });
            return false;
        }
    }

    validateEventEndDate = (eventEndDate, index) => {console.log("edate");
        if (eventEndDate == '') {
            let errorEventEndDate = this.state.errorEventEndDate;
            errorEventEndDate[index] = true;
            this.setState({
                errorEventEndDate
            });
            return true;
        }
        else {
            let errorEventEndDate = this.state.errorEventEndDate;
            errorEventEndDate[index] = false;
            this.setState({
                errorEventEndDate
            });
            return false;
        }
    }

    validateEventDays = (eventDays, index) => {
        if (eventDays <= 0) {
            let errorEventDays = this.state.errorEventDays;
            errorEventDays[index] = true;
            this.setState({
                errorEventDays
            });
            return true;
        }
        else {
            let errorEventDays = this.state.errorEventDays;
            errorEventDays[index] = false;
            this.setState({
                errorEventDays
            });
            return false;
        }
    }

    validateMultiplier = (multiplier, index) => {
        if (!/^\d+$/.test(multiplier)) {
            let errorMultiplier = this.state.errorMultiplier;
            errorMultiplier[index] = true;
            this.setState({
                errorMultiplier
            });
            return true;
        }
        else {
            let errorMultiplier = this.state.errorMultiplier;
            errorMultiplier[index] = false;
            this.setState({
                errorMultiplier
            });
            return false;
        }
    }

    saveEventPlanning = (e) => {
        const errorSite = this.state.newEvent.filter((item, index) => this.validateSite(item.storeName, index));
        const errorEventName = this.state.newEvent.filter((item, index) => this.validateEventName(item.eventName, index));
        const errorEventStartDate = this.state.newEvent.filter((item, index) => this.validateEventStartDate(item.eventStartDate, index));
        const errorEventEndDate = this.state.newEvent.filter((item, index) => this.validateEventEndDate(item.eventEndDate, index));
        const errorEventDays = this.state.newEvent.filter((item, index) => this.validateEventDays(item.eventDays, index));
        const errorMultiplier = this.state.newEvent.filter((item, index) => this.validateMultiplier(item.multiplier, index));

        if (errorSite.length == 0 && errorEventName.length == 0 && errorEventStartDate.length == 0 && errorEventEndDate.length == 0 && errorEventDays.length == 0 && errorMultiplier.length == 0) {
            this.props.saveEventPlanningRequest(this.state.newEvent);
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
        this.props.history.push("/inventoryPlanning/event-mapping")
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    addNewRow = () => {
        let newRow = {
            id: 0,
            storeCode: '',
            storeName: 'Select Option',
            eventName: '',
            multiplier: '',
            eventStartDate: '',
            eventEndDate: '',
            eventDays: ''
        };
        this.setState({
            newEvent: this.state.newEvent.concat(newRow),
            errorSite: this.state.errorSite.concat(false),
            errorEventName: this.state.errorEventName.concat(false),
            errorEventStartDate: this.state.errorEventStartDate.concat(false),
            errorEventEndDate: this.state.errorEventEndDate.concat(false),
            errorEventDays: this.state.errorEventDays.concat(false),
            errorMultiplier: this.state.errorMultiplier.concat(false)
        });
    }

    deleteRow = (index) => {
        let updateRow = this.state.newEvent;
        if (updateRow.length == 1) {
            this.setState({
                toastMsg: "Single row cannot be deleted!",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 3000);
        }
        else {
            let newErrorSite = this.state.errorSite;
            let newErrorEventName = this.state.errorEventName;
            let newErrorEventStartDate = this.state.errorEventStartDate;
            let newErrorEventEndDate = this.state.errorEventEndDate;
            let newErrorEventDays = this.state.errorEventDays;
            let newErrorMultiplier = this.state.errorMultiplier;
            updateRow.splice(index, 1);
            newErrorSite.splice(index, 1);
            newErrorEventName.splice(index, 1);
            newErrorEventStartDate.splice(index, 1);
            newErrorEventEndDate.splice(index, 1);
            newErrorEventDays.splice(index, 1);
            newErrorMultiplier.splice(index, 1);

            this.setState({
                newEvent: updateRow,

                errorSite: newErrorSite,
                errorEventName: newErrorEventName,
                errorEventStartDate: newErrorEventStartDate,
                errorEventEndDate: newErrorEventEndDate,
                errorEventDays: newErrorEventDays,
                errorMultiplier: newErrorMultiplier
            });
        }
    }

    clearRows = () => {
        let emptyRow = [{
            id: 0,
            storeCode: '',
            storeName: 'Select Option',
            eventName: '',
            multiplier: '',
            eventStartDate: '',
            eventEndDate: '',
            eventDays: ''
        }];
        this.setState({
            newEvent: emptyRow,

            errorSite: [false],
            errorEventName: [false],
            errorEventStartDate: [false],
            errorEventEndDate: [false],
            errorEventDays: [false],
            errorMultiplier: [false],

            confirmClearModal: false
        });
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        })
    }

    handleSiteName = (siteName, siteCode, index) => {
        let updateRow = [...this.state.newEvent];
        updateRow[index].storeCode = parseInt(siteCode);
        updateRow[index].storeName = siteName;
        this.setState({newEvent: updateRow});
        this.validateSite(siteName, index);
        this.closeSiteName();
    }
    
    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/inventoryPlanning/event-mapping")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="excel-export">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="8.627" height="10" viewBox="0 0 8.627 12.652">
                                            <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)"/>
                                        </svg>
                                    </span>
                                    Excel Upload
                                </button>
                                <button type="button" className="get-details" onClick={this.saveEventPlanning}>Save</button>
                                <button type="button" disabled={this.props.location.state == undefined ? "" : "disabled"} onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="add-more-button">
                        <button className="amb-add" type="button" disabled={this.props.location.state == undefined ? "" : "disabled"} onClick={this.addNewRow}>
                            <span className="amba-inner">
                                <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="20" height="20" viewBox="0 0 19.846 19.846">
                                    <g id="Group_3035">
                                        <g id="Group_3034">
                                            <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1"/>
                                        </g>
                                    </g>
                                    <g id="Group_3037" transform="translate(5.311 5.242)">
                                        <g id="Group_3036">
                                            <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                            Add More
                        </button>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="event-master-table">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="fix-action-btn"><label></label></th>
                                    <th><label>Site Name</label></th>
                                    <th><label>Site ID</label></th>
                                    <th><label>Event Name</label></th>
                                    <th><label>Start Date</label></th>
                                    <th><label>Last Date</label></th>
                                    <th><label>Event Days</label></th>
                                    <th><label>Mutiplier</label></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.newEvent.map ((item, index) => (
                                        <tr key={index}>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner til-delete-btn" onClick={() => this.deleteRow(index)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                            <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                        </svg>
                                                        {/* <span className="generic-tooltip">Delete</span> */}
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                {/* <select className={this.state.errorSite[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newEvent;
                                                    updateRow[index].storeCode = event.target.value;
                                                    updateRow[index].storeName = event.target.options[event.target.selectedIndex].text;
                                                    this.setState({newEvent: updateRow});
                                                    this.validateSite(updateRow[index].storeName, index);
                                                }}>
                                                    {item.storeName == '' ? <option selected disabled value=''>Select Option</option> : <option disabled value=''>Select Option</option>}
                                                    {this.state.sitePlanningFilters.map((innerItem) => (
                                                        innerItem.siteName === item.storeName ? <option value={innerItem.siteCode} selected>{innerItem.siteName}</option> : <option value={innerItem.siteCode}>{innerItem.siteName}</option>
                                                    ))}
                                                </select> */}
                                                <div className="gen-custom-select">
                                                    <button type="button" className={this.state.errorSite[index] ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openSiteName(e, index)}>{item.storeName}</button>
                                                    {this.state.siteName === index &&
                                                    <div className="gcs-dropdown" id="siteNames">
                                                        <ul>
                                                            <li className="gcsd-search">
                                                                <input type="search" placeholder="Search...." onChange={(e) => this.setState({searchSite: e.target.value})} />
                                                                <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                                            </li>
                                                            {this.state.sitePlanningFilters.map((innerItem) => (
                                                                innerItem.siteCode.toString().includes(this.state.searchSite) || innerItem.siteName.includes(this.state.searchSite) ?
                                                                <li onClick={() => this.handleSiteName(innerItem.siteName, innerItem.siteCode, index)}>{innerItem.siteCode} - {innerItem.siteName}</li> :
                                                                null
                                                            ))}
                                                        </ul>
                                                    </div>}
                                                </div>
                                                {this.state.errorSite[index] ? <span className="error">Select Site Name</span> : null}
                                            </td>
                                            <td><input disabled type="text" value={item.storeCode}/></td>
                                            <td><input type="text" value={item.eventName} className={this.state.errorEventName[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newEvent;
                                                    updateRow[index].eventName = event.target.value;
                                                    this.setState({newEvent: updateRow});
                                                    this.validateEventName(event.target.value, index);
                                                }}/>
                                                {this.state.errorEventName[index] ? <span className="error">Enter Event Name</span> : null}
                                            </td>
                                            <td><input type="date" value={item.eventStartDate} placeholder={item.eventStartDate == '' ? "YYYY-MM-DD" : ""} className={this.state.errorEventStartDate[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newEvent;
                                                    updateRow[index].eventStartDate = event.target.value;
                                                    if(updateRow[index].eventEndDate != '') {
                                                        let startDate = new Date(updateRow[index].eventStartDate);
                                                        let endDate = new Date(updateRow[index].eventEndDate);
                                                        let diff = Math.floor((Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()) - Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) ) /(1000 * 60 * 60 * 24));
                                                        updateRow[index].eventDays = diff + 1;
                                                        this.validateEventDays(diff + 1, index);
                                                    }
                                                    this.setState({newEvent: updateRow});
                                                    this.validateEventStartDate(event.target.value, index);
                                                }}/>
                                                {this.state.errorEventStartDate[index] ? <span className="error">Select Event Start Date</span> : null}
                                            </td>
                                            <td><input type="date" value={item.eventEndDate} placeholder={item.eventEndDate == '' ? "YYYY-MM-DD" : ""} className={this.state.errorEventEndDate[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newEvent;
                                                    updateRow[index].eventEndDate = event.target.value;
                                                    if(updateRow[index].eventStartDate != '') {
                                                        let startDate = new Date(updateRow[index].eventStartDate);
                                                        let endDate = new Date(updateRow[index].eventEndDate);
                                                        let diff = Math.floor((Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()) - Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) ) /(1000 * 60 * 60 * 24));
                                                        updateRow[index].eventDays = diff + 1;
                                                        this.validateEventDays(diff + 1, index);
                                                    }
                                                    this.setState({newEvent: updateRow});
                                                    this.validateEventEndDate(event.target.value, index);
                                                }}/>
                                                {this.state.errorEventEndDate[index] ? <span className="error">Select Event Last Date</span> : null}
                                            </td>
                                            <td>
                                                <input disabled type="text" className={this.state.errorEventDays[index] ? "errorBorder" : ""} value={item.eventDays}/>
                                                {this.state.errorEventDays[index] ? <span className="error">Select valid Event Dates</span> : null}
                                            </td>
                                            <td><input type="text" value={item.multiplier} className={this.state.errorMultiplier[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newEvent;
                                                    updateRow[index].multiplier = /^\d+$/.test(event.target.value) ? parseFloat(event.target.value) : event.target.value;
                                                    this.setState({newEvent: updateRow});
                                                    this.validateMultiplier(event.target.value, index);
                                                }}/>
                                                {this.state.errorMultiplier[index] ? <span className="error">Enter valid Multiplier</span> : null}
                                            </td>
                                        </tr>
                                    ))
                                }
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.clearRows} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
             </div>
        )
    }
}

export default CreateEventMappingPlanning;