import React from 'react';
import FilterLoader from '../loaders/filterLoader';
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import AdHocModal from './adHocModal';
import { CONFIG } from "../../config/index";
import axios from 'axios';
import AdhocFilter from './adHocFilter';
import AdhocConfirmModal from '../loaders/adhocConfirmModal';
import ToastLoader from '../loaders/toastLoader';
class AdHocRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            adhocModalData: [],
            type: 1,
            search: "",
            no: 1,
            loader: true,
            alert: false,
            success: false,
            errorCode: "",
            errorMessage: "",
            successMessage: "",
            code: "",
            adHockData: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            adhocModal: false,
            adhocFilter: false,
            filterBar: false,
            workOrder: "",
            site: "",
            status: "",
            createdOn: "",
            workOrderId: "",
            confirmModal: false,
            headerMsg: "",
            paraMsgAd: "",
            orderId: "",
            toastLoader: false,
            toastMsg: ""
        }
    }

    xlscsv(type) {
        this.setState({
            loader: true
        })
        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'application/json'
        }
        axios.get(`${CONFIG.BASE_URL}/download/module/${type}/AdHocRequest`, { headers: headers })
            .then(res => {
                this.setState({
                    loader: false
                })
                window.open(`${res.data.data.resource}`)
            }).catch((error) => {
                this.setState({
                    loader: false
                })

            });

    }
    openAdhocModal(id) {
        let data = this.state.adHockData
        for (var i = 0; i < data.length; i++) {
            if (data[i].workOrder == id) {
                this.setState({
                    workOrderId: id,
                    adhocModalData: data[i].item,
                    adhocModal: !this.state.adhocModal
                })
            }
        }

    }
    closeAdhocModal() {
        this.setState({
            adhocModal: !this.state.adhocModal
        })

    }

    onCancel(id) {
        this.setState({
            confirmModal: !this.state.confirmModal,
            headerMsg: "Are you sure you want to cancel it?",
            paraMsgAd: "",
            orderId: id
        })
    }

    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }
    openFilter(e) {
        this.setState({
            adhocFilter: true,
            filterBar: !this.state.filterBar
        })
    }


    componentWillMount() {

        if (!this.props.inventoryManagement.getAdHock.isSuccess) {
            let data = {
                type: 1,
                no: 1,
                search: "",
                workOrder: "",
                status: "",
                site: "",
                createdOn: ""

            }
            this.props.getAdHockRequest(data);
        } else {
            let data = {
                type: 1,
                no: 1,
                search: "",
                workOrder: "",
                status: "",
                site: "",
                createdOn: ""
            }
            this.props.getAdHockRequest(data);
            this.setState({
                loader: false
            })
            if (this.props.inventoryManagement.getAdHock.data.resource == null) {
                this.setState({
                    adHockData: this.props.inventoryManagement.getAdHock.data.resource,

                })
            } else {
                this.setState({
                    adHockData: this.props.inventoryManagement.getAdHock.data.resource

                })
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.inventoryManagement.getAdHock.isSuccess) {
            this.setState({
                loader: false,
                adHockData: nextProps.inventoryManagement.getAdHock.data.resource == null ? [] : nextProps.inventoryManagement.getAdHock.data.resource,

                prev: nextProps.inventoryManagement.getAdHock.data.prePage,
                current: nextProps.inventoryManagement.getAdHock.data.currPage,
                next: nextProps.inventoryManagement.getAdHock.data.currPage + 1,
                maxPage: nextProps.inventoryManagement.getAdHock.data.maxPage,
            })
        } else if (nextProps.inventoryManagement.getAdHock.isError) {
            this.setState({
                code: nextProps.inventoryManagement.getAdHock.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.inventoryManagement.getAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.getAdHock.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.getAdHock.message.error.errorMessage,
            })
            this.props.getAdHockClear()
        }
        if (nextProps.inventoryManagement.editAdHoc.isSuccess) {
            this.setState({
                loader: false,
                adHockData: nextProps.inventoryManagement.editAdHoc.data.resource == null ? [] : nextProps.inventoryManagement.editAdHoc.data.resource,

                successMessage: nextProps.inventoryManagement.editAdHoc.data.message,
                success: true,
                loader: false,

            })
            this.props.editAdhocRequest();
        } else if (nextProps.inventoryManagement.editAdHoc.isError) {
            this.setState({
                code: nextProps.inventoryManagement.editAdHoc.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.inventoryManagement.editAdHoc.message.error == undefined ? undefined : nextProps.inventoryManagement.editAdHoc.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.editAdHoc.message.error == undefined ? undefined : nextProps.inventoryManagement.editAdHoc.message.error.errorMessage,
            })
            this.props.editAdhocRequest();
        }

        if (nextProps.inventoryManagement.cancelAdhoc.isSuccess) {
            this.setState({
                loader: false,

                successMessage: nextProps.inventoryManagement.cancelAdhoc.data.message,
                success: true,
                loader: false,

            })
            this.props.cancelAdhocRequest();
        } else if (nextProps.inventoryManagement.cancelAdhoc.isError) {
            this.setState({
                code: nextProps.inventoryManagement.cancelAdhoc.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.inventoryManagement.cancelAdhoc.message.error == undefined ? undefined : nextProps.inventoryManagement.cancelAdhoc.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.cancelAdhoc.message.error == undefined ? undefined : nextProps.inventoryManagement.cancelAdhoc.message.error.errorMessage,
            })
            this.props.editAdhocRequest();

        }


        if (nextProps.inventoryManagement.deleteItemCode.isSuccess) {
            this.setState({
                loader: false,


                successMessage: nextProps.inventoryManagement.deleteItemCode.data.message,
                success: true,
                loader: false,

            })
            this.props.deleteItemCodeRequest();
        } else if (nextProps.inventoryManagement.deleteItemCode.isError) {
            this.setState({
                code: nextProps.inventoryManagement.deleteItemCode.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.inventoryManagement.deleteItemCode.message.error == undefined ? undefined : nextProps.inventoryManagement.deleteItemCode.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.deleteItemCode.message.error == undefined ? undefined : nextProps.inventoryManagement.deleteItemCode.message.error.errorMessage,
            })
            this.props.deleteItemCodeRequest();
        }


        if (nextProps.inventoryManagement.getItemAdHock.isSuccess) {
            this.setState({

                loader: false,
                alert: false
            }
            )
        } else if (nextProps.inventoryManagement.getItemAdHock.isError) {
            this.setState({
                code: nextProps.inventoryManagement.getItemAdHock.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.inventoryManagement.getItemAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.getItemAdHock.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getItemAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.getItemAdHock.message.error.errorMessage,
            })

        }


        if (nextProps.inventoryManagement.getItemAdHock.isLoading || nextProps.inventoryManagement.cancelAdhoc.isLoading || nextProps.inventoryManagement.deleteItemCode.isLoading ||
            nextProps.inventoryManagement.editAdHoc.isLoading || nextProps.inventoryManagement.getAdHock.isLoading) {
            this.setState({
                loader: true
            })
        }


    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.inventoryManagement.getAdHock.data.prePage,
                current: this.props.inventoryManagement.getAdHock.data.currPage,
                next: this.props.inventoryManagement.getAdHock.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getAdHock.data.maxPage,
            })
            if (this.props.inventoryManagement.getAdHock.data.currPage != 0) {
                let data = {
                    no: this.props.inventoryManagement.getAdHock.data.currPage - 1,
                    search: this.state.search,
                    type: this.state.type,
                    workOrder: this.state.workOrder,
                    status: this.state.status,
                    site: this.state.site,
                    createdOn: this.state.createdOn
                };
                this.props.getAdHockRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.inventoryManagement.getAdHock.data.prePage,
                current: this.props.inventoryManagement.getAdHock.data.currPage,
                next: this.props.inventoryManagement.getAdHock.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getAdHock.data.maxPage,
            })
            if (this.props.inventoryManagement.getAdHock.data.currPage != this.props.inventoryManagement.getAdHock.data.maxPage) {
                let data = {
                    no: this.props.inventoryManagement.getAdHock.data.currPage + 1,
                    search: this.state.search,
                    type: this.state.type,
                    workOrder: this.state.workOrder,
                    status: this.state.status,
                    site: this.state.site,
                    createdOn: this.state.createdOn
                };
                this.props.getAdHockRequest(data)
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1) {

            } else {
                this.setState({
                    prev: this.props.inventoryManagement.getAdHock.data.prePage,
                    current: this.props.inventoryManagement.getAdHock.data.currPage,
                    next: this.props.inventoryManagement.getAdHock.data.currPage + 1,
                    maxPage: this.props.inventoryManagement.getAdHock.data.maxPage,
                })
                if (this.props.inventoryManagement.getAdHock.data.currPage <= this.props.inventoryManagement.getAdHock.data.maxPage) {
                    let data = {
                        no: 1,
                        search: this.state.search,
                        type: this.state.type,
                        workOrder: this.state.workOrder,
                        status: this.state.status,
                        site: this.state.site,
                        createdOn: this.state.createdOn
                    };
                    this.props.getAdHockRequest(data)
                }

            }
        }
    }

    onSearch(e) {

        e.preventDefault();
        if (this.state.search == "") {
            this.setState({
                toastLoader: true,
                toastMsg: "Enter text on search input"
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false,
                })
            }, 1500);
        } else {
            this.setState({
                type: 3,
                workOrder: "",
                status: "",
                site: "",
                createdOn: ""
            })
            let data = {
                no: 1,

                search: this.state.search,
                type: 3,
                workOrder: "",
                status: "",
                site: "",
                createdOn: ""
            };
            this.props.getAdHockRequest(data);
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
    }
    onClearFilter() {

        this.setState({
            type: 1,
            no: 1,
            search: "",
            workOrder: "",
            status: "",
            site: "",
            createdOn: ""
        })

        let data = {
            type: 1,
            no: 1,
            search: "",
            workOrder: "",
            status: "",
            site: "",
            createdOn: ""

        }
        this.props.getAdHockRequest(data);
    }
    updateFilter(data) {
        this.setState({
            type: data.type,
            no: data.no,

            search: data.search,
            workOrder: data.workOrder,
            status: data.status,
            site: data.orgName,
            createdOn: data.createdOn,
        });
    }


    onClearSearch(e) {
        e.preventDefault();
        this.setState({
            type: 1,
            no: 1,
            search: "",
            workOrder: "",
            status: "",
            site: "",
            createdOn: ""
        })

        if (this.state.type == 3) {
            let data = {
                type: 1,
                no: 1,
                search: "",
                workOrder: "",
                status: "",
                site: "",
                createdOn: ""

            }
            this.props.getAdHockRequest(data);
        }
    }

    onActive(id) {
        let adhoc = this.state.adHockData;
        for (let i = 0; i < adhoc.length; i++) {
            if (adhoc[i].workOrder == id) {
                adhoc[i].status = adhoc[i].status == "Active" ? "InActive" : "Active";
                let data = {
                    workOrder: adhoc[i].workOrder,
                    site: adhoc[i].site,
                    createdOn: adhoc[i].createdOn,
                    reqDate: adhoc[i].reqDate,
                    item: adhoc[i].item,
                    status: adhoc[i].status

                }
                this.props.editAdhocRequest(data)
            }
        }
    }

    render() {
        return (
            <div className="container_div">
                {/* <div className="container_div m-top-100 " id="">
                <div className="col-md-12 col-sm-12">
                    <div className="menu_path">
                    <ul className="list-inline width_100 pad20">
                        <BraedCrumps />
                    </ul>
                    </div>
                </div>
                </div> */}

                <div className="col-md-12 col-sm-12 col-xs-12 ">
                    <div className="container-fluid">
                        <div className="container_content heightAuto adHocRequestMain">
                            <div className="col-md-6 col-sm-12 pad-0">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            AD-HOC REQUEST
                                    </label>
                                    </li>
                                    <li>
                                        <p className="master_para">Create & manage ad-hoc request</p>
                                    </li>

                                    <li className="m-top-50">
                                        {/* <label className="export_data_div">
                                            Export Data
                                        </label> */}
                                        <ul className="list-inline m-top-10">
                                            <li>
                                                <button type="button" className="button_home" onClick={(e) => this.xlscsv("CSV")}>
                                                    CSV
                                            </button>
                                            </li>
                                            <li>
                                                <button type="button" className="button_home" onClick={(e) => this.xlscsv("XLS")}>
                                                    XLS
                                            </button>
                                            </li>
                                            <li>
                                                <button className="filter_button" data-toggle="modal" data-target="#myUserManage" onClick={(e) => this.openFilter(e)}>
                                                    FILTER

                                            <svg className="filter_control" xmlns="http://www.w3.org/2000/svg" width="17" height="15" viewBox="0 0 17 15">
                                                        <path fill="#FFF" fillRule="nonzero" d="M1.285 2.526h9.79a1.894 1.894 0 0 0 1.79 1.263c.82 0 1.515-.526 1.789-1.263h1.368a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632h-1.368A1.894 1.894 0 0 0 12.864 0c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631zM12.865.842c.589 0 1.052.463 1.052 1.053 0 .59-.463 1.052-1.053 1.052-.59 0-1.053-.463-1.053-1.052 0-.59.464-1.053 1.053-1.053zm3.157 5.684h-9.79a1.894 1.894 0 0 0-1.789-1.263c-.821 0-1.516.526-1.79 1.263H1.286a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.631h1.369a1.894 1.894 0 0 0 1.789 1.264c.821 0 1.516-.527 1.79-1.264h9.789a.62.62 0 0 0 .632-.631.62.62 0 0 0-.632-.632zM4.443 8.211c-.59 0-1.053-.464-1.053-1.053 0-.59.464-1.053 1.053-1.053.59 0 1.053.463 1.053 1.053 0 .59-.464 1.053-1.053 1.053zm11.579 3.578h-5.579a1.894 1.894 0 0 0-1.79-1.263c-.82 0-1.515.527-1.789 1.263H1.285a.62.62 0 0 0-.631.632.62.62 0 0 0 .631.632h5.58a1.894 1.894 0 0 0 1.789 1.263c.82 0 1.515-.527 1.789-1.263h5.579a.62.62 0 0 0 .632-.632.62.62 0 0 0-.632-.632zm-7.368 1.685c-.59 0-1.053-.463-1.053-1.053 0-.59.463-1.053 1.053-1.053.589 0 1.052.464 1.052 1.053 0 .59-.463 1.053-1.052 1.053z"
                                                        />
                                                    </svg>
                                                </button>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                            </div>

                            <div className="col-md-6 col-sm-12 pad-0">
                                <ul className="list-inline circle_list">

                                    <li>
                                        <div className="tooltip" onClick={() => this.props.history.push('/inventoryPlanning/Ad-Hoc')}>
                                            {/* <div className="tooltip" onClick={() => this.props.history.push('/administration/user/addUser')}> */}
                                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                                                <g fill="none" fillRule="evenodd">
                                                    <circle cx="17.5" cy="17.5" r="17.5" fill="#6D6DC9" />
                                                    <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7" />
                                                </g>
                                            </svg>

                                            <p className="tooltiptext tooltip-left">
                                                <span>Create New Ad-Hoc Request
                                            </span>
                                            </p>
                                        </div>

                                    </li>
                                </ul>
                                <ul className="list-inline search_list manageSearch m-top-55">
                                    <li>
                                        <form onSubmit={(e) => this.onSearch(e)}>
                                            <input type="search" onChange={(e) => this.setState({ search: e.target.value })} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                            <button onClick={(e) => this.onSearch(e)} className="searchWithBar">Search
                                             <svg className="search_img" xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 18 18">
                                                    <path fill="#ffffff" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z">
                                                    </path></svg>
                                            </button>
                                        </form>
                                    </li>

                                </ul>
                                {this.state.type != 3 ? null : <span className="clearSearchFilter" onClick={(e) => this.onClearSearch(e)}>Clear Search Filter</span>}
                            </div>


                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 tableGeneric bordere3e7f3 adHocMain">
                                <div className="Zui-wrapper">
                                    <div className="scrollableOrgansation zui-scrollerHistory table-scroll scrollableTableFixed heightAuto">
                                        <table className="table zui-table UserManageTable manageUserTable border-bot-table tableOddColor">
                                            {/* UserManageTable */}
                                            <thead>
                                                <tr>
                                                    <th className="fixed-side-adHoc fixed-side1">
                                                        <label className="width65 pad-lft-5">Action</label>
                                                    </th>
                                                    <th className="positionRelative">
                                                        <label>Work Order</label>
                                                    </th>
                                                    <th className="positionRelative">
                                                        <label>Site</label>
                                                    </th>

                                                    <th className="positionRelative">
                                                        <label>Created On</label>
                                                    </th>
                                                    <th className="positionRelative">
                                                        <label>Requested Date</label>
                                                    </th>
                                                    <th className="positionRelative">
                                                        <label>Status</label>
                                                    </th>


                                                </tr>
                                            </thead>

                                            <tbody>

                                                {this.state.adHockData == null || this.state.adHockData.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> :
                                                    this.state.adHockData.map((data, key) => (<tr key={key}>
                                                        {/* ---------------------------------------------Active Incative toggle------------------------- */}
                                                        {/* <td className="fixed-side-adHoc fixed-side1 ">
                                                    <ul className="list-inline m-0 float_None alignMiddle">
                                                        <li className={ data.status == "Active" ? "addToggleSwitch mainToggle" : "mainToggle"}>                                                           
                                                            <label className="switchToggle">
                                                                <input type="checkbox" id="myDiv" onClick={(e) => this.onActive(`${data.workOrder}`)}/>
                                                                <span className="sliderToggle round">
                                                                </span>                                                                
                                                                </label>
                                                                <p className="onActive">Active</p>
                                                                <p className="onInActive">InActive</p>
                                                        </li>

                                                    </ul>
                                                </td> */}
                                                        {/* -----------------------------------------------------------Active Inactive Toggle End---------------------------- */}

                                                        <td className="fixed-side-adHoc fixed-side1">
                                                            <ul className="list-inline m-0">
                                                                <li>
                                                                    {data.status == "Inprogress" ? <button className="saveBtnBorder" onClick={() => this.onCancel(`${data.workOrder}`)}>
                                                                        CANCEL
                                                        </button> : <button className="saveBtnBorder btnDisabled" disabled>
                                                                            CANCEL
                                                        </button>}
                                                                </li>
                                                            </ul>
                                                        </td>

                                                        {/* <td className="fixed-side1 historyJobStatus">
                                                    <ul className="list-inline m-0">
                                                        <li>
                                                            <button className="edit_button" onClick={() => this.onUserEditModal(`${data.userId}`)}>
                                                                EDIT
                                                        </button>
                                                        </li>
                                                        <li>
                                                            <button type="button" onClick={(e) => this.onActive(`${data.userId}`)} className={ data.status == "Active" ? "btnSummary" : "active_button"}>
                                                                {data.status}
                                                            </button>
                                                        </li>

                                                    </ul>
                                                </td> */}
                                                        <td className="openModalRequest">
                                                            <label className="displayPointer clickableBtn" onClick={(e) => this.openAdhocModal(`${data.workOrder}`)}>
                                                                {data.workOrder}
                                                            </label>
                                                        </td>


                                                        <td>
                                                            <label >
                                                                {data.site}
                                                            </label>
                                                        </td>

                                                        <td>
                                                            <label>
                                                                {data.createdOn}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.reqDate}
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                                {data.status}
                                                            </label>
                                                        </td>


                                                    </tr>
                                                    ))}


                                            </tbody>

                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div className="pagerDiv">
                                <ul className="list-inline pagination">
                                    <li >
                                        <button className={this.state.current == 1 || this.state.current == undefined || this.state.current == 0 ? "PageFirstBtn pointerNone" : "PageFirstBtn"} onClick={(e) => this.page(e)} id="first" >
                                            First
                  </button>
                                    </li>
                                    {this.state.prev == 0 || this.state.prev == undefined ?
                                        <li >
                                            <button className="PageFirstBtn" disabled>
                                                Prev
               </button>
                                        </li> :
                                        <li >

                                            <button className="PageFirstBtn" onClick={(e) => this.page(e)} id="prev">
                                                Prev
                  </button>
                                        </li>}
                                    <li>
                                        <button className="PageFirstBtn pointerNone">
                                            <span>{this.state.current}/{this.state.maxPage}</span>
                                        </button>
                                    </li>
                                    {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                        <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                            Next
                  </button>
                                    </li> : <li >
                                            <button className="PageFirstBtn borderNone" disabled>
                                                Next
                  </button>
                                        </li> : <li >
                                            <button className="PageFirstBtn borderNone" disabled>
                                                Next
                  </button>
                                        </li>}


                                    {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                </ul>
                            </div>
                            {/* <div className="footerDivForm height4per m-top-20">
                            <ul className="list-inline m-lft-0 m-top-10">
                                <li><button className="clear_button_vendor" >Clear</button>
                                </li>
                                <li><button type="button" className="save_button_vendor">Save</button></li>
                            </ul>
                        </div> */}
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.adhocModal ? <AdHocModal {...this.props} workOrderId={this.state.workOrderId} adhocModalData={this.state.adhocModalData} closeAdhocModal={(e) => this.closeAdhocModal(e)} closeModal={(e) => this.openAdhocModal(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.adhocFilter ? <AdhocFilter {...this.props} updateFilter={(e) => this.updateFilter(e)} closeFilter={(e) => this.openFilter(e)} filterBar={this.state.filterBar} /> : null}
                {this.state.confirmModal ? <AdhocConfirmModal {...this.props} {...this.state} orderId={this.state.orderId} headerMsg={this.state.headerMsg} paraMsgAd={this.state.paraMsgAd} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}

            </div>

        );
    }
}

export default AdHocRequest;