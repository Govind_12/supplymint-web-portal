import React from 'react';
import Pagination from '../pagination';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import Confirm from '../loaders/arsConfirm';
import ItemFilter from './itemFilterModal';
import LocationFilter from './locationFilterModal';
import InventorySetting from './inventorySettingModal';
import ConfirmationSaveModal from '../loaders/confirmationModal';

class CoreConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            submit: false,
            openSetting: '',
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',
            confirmSaveModal: false,

            currentTab: 'item',
            settingsValues: [], //Options that appear in the setting modal
            selectedSetting: {}, //Options that are already selected in the setting modal
            fieldNames: [],
            fieldValues: [],
            selectedValues: [],
            userDefinedValues: [],

            
            selectDepartment: false,
            selectDivision: false,
            selectSection: false,
            selectArticle: false,
            selectSite: false,
            settingModal: false, //get api isSuccess
        }
    }

    // openSetting(e, type) {
    //     e.preventDefault();
    //     this.props.getSiteAndItemFilterRequest(type);
    //     this.props.getSiteAndItemFilterConfigurationRequest(type);
    //     this.setState({openSetting: type});
    //     // if (id == 'item') {
    //     //     console.log("in");
    //     //     this.props.getSiteAndItemFilterRequest("item");
    //     // }
    //     // else if (id == 'site') {

    //     // }
    //     // this.setState({
    //     //     settingModal: !this.state.settingModal
    //     // });
        
    // }
    // closeSetting = () => {
    //     this.setState({
    //         settingModal: false,
    //         openSetting: ''
    //     });
    // }

    // departmentData = (e) => {
    //     if (e.keyCode == 13) {
    //         this.setState({ selectDepartment: true })
    //     } else if (e.keyCode === 27) {
    //         this.setState({ selectDepartment: false })
    //     }
    // }
    // divisionData = (e) => {
    //     if (e.keyCode == 13) {
    //         this.setState({ selectDivision: true })
    //     } else if (e.keyCode === 27) {
    //         this.setState({ selectDivision: false })
    //     }
    // }
    // sectionData = (e) => {
    //     if (e.keyCode == 13) {
    //         this.setState({ selectSection: true })
    //     } else if (e.keyCode === 27) {
    //         this.setState({ selectSection: false })
    //     }
    // }
    // articleData = (e) => {
    //     if (e.keyCode == 13) {
    //         this.setState({ selectArticle: true })
    //     } else if (e.keyCode === 27) {
    //         this.setState({ selectArticle: false })
    //     }
    // }
    // siteData = (e) => {
    //     if (e.keyCode == 13) {
    //         this.setState({ selectSite: true })
    //     } else if (e.keyCode === 27) {
    //         this.setState({ selectSite: false })
    //     }
    // }

    componentDidMount() {
        this.props.getItemConfigurationRequest(this.state.currentTab);
        // this.props.getItemConfigurationRequest("site");
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.seasonPlanning.getItemConfiguration.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                fieldNames: JSON.parse(nextProps.seasonPlanning.getItemConfiguration.data.resource.displayName),
                fieldValues: nextProps.seasonPlanning.getItemConfiguration.data.resource.fieldValues,
                selectedValues: JSON.parse(nextProps.seasonPlanning.getItemConfiguration.data.resource.selectedValues),
                userDefinedValues: JSON.parse(nextProps.seasonPlanning.getItemConfiguration.data.resource.UserDefinedNames)
            });
        }
        else if(nextProps.seasonPlanning.getItemConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getItemConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.getItemConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getItemConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getItemConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getItemConfiguration.message.error.errorMessage
            });
        }

        // if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isSuccess) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: false,
        //         settingsValues: nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter),
        //         settingModal: true
        //     });
        // }
        // else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isError) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: true,
        //         code: nextProps.seasonPlanning.getSiteAndItemFilter.message.status,
        //         errorCode: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorCode,
        //         errorMessage: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorMessage
        //     });
        // }

        // if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: false,
        //         selectedSetting: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter)
        //     }, () => this.setState({settingModal: true}));
        // }
        // else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isError) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: true,
        //         code: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.status,
        //         errorCode: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorCode,
        //         errorMessage: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorMessage
        //     });
        // }

        if(this.state.submit && nextProps.seasonPlanning.saveItemConfiguration.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveItemConfiguration.data.resource.successMessage, //nextProps.seasonPlanning.saveSeasonPlanning.data.message,
                error: false
            });
        }
        else if(this.state.submit && nextProps.seasonPlanning.saveItemConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveItemConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.saveItemConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.saveItemConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveItemConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.saveItemConfiguration.message.error.errorMessage
            });
        }

        // if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isSuccess) {
        //     this.setState({
        //         loading: false,
        //         success: true,
        //         successMessage: "Configuration saved successfully!", //nextProps.seasonPlanning.saveSeasonPlanning.data.message,
        //         error: false
        //     });
        // }
        // else if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isError) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: true,
        //         code: nextProps.seasonPlanning.saveSiteAndItemFilter.message.status,
        //         errorCode: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorCode,
        //         errorMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorMessage
        //     });
        // }

        if(nextProps.seasonPlanning.getItemConfiguration.isLoading || nextProps.seasonPlanning.saveItemConfiguration.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false
            });
        }
    }

    setSubmit = () => {
        this.setState({submit: true});
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false,
            openSetting: '',
            settingModal: false
        }, () => {

        });
        if(this.state.currentTab == 'item' || this.state.currentTab == 'site' ) this.props.getItemConfigurationRequest(this.state.currentTab);
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false,
            openSetting: '',
            settingModal: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    confirmClear = () => {
        let values = this.state.selectedValues;
        Object.keys(values).forEach((item) => values[item] = "''");
        this.setState({selectedValues: values, confirmClearModal: false});
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        })
    }

    confirmSave = () => {
        this.setState({
            confirmSaveModal: true
        })
    }

    cancelSave = () => {
        this.setState({
            confirmSaveModal: false
        })
    }

    saveItemConfiguration = () => {
        this.setState({submit: true, confirmSaveModal: false});
        if (this.state.currentTab == 'general') {

        }
        else if (this.state.currentTab == 'item') {
            this.props.saveItemConfigurationRequest({
                filterType: 'item',
                data: this.state.selectedValues,
                userDefineNames: this.state.userDefinedValues
            });
        }
        else if (this.state.currentTab == 'site') {
            this.props.saveItemConfigurationRequest({
                filterType: 'site',
                data: this.state.selectedValues,
                userDefineNames: this.state.userDefinedValues
            });
        }
        
    }

    render() {
        console.log(this.state.selectedValues);
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            {/* <li className="nav-item active" >
                                <a className="nav-link p-l-0 st-btn" href="#general" role="tab" data-toggle="tab" onClick={() => this.setState({currentTab: "general"})}>General</a>
                            </li> */}
                            <li className="nav-item active">
                                <a className="nav-link st-btn" href="#itemconfiguration" role="tab" data-toggle="tab" onClick={() => {this.props.getItemConfigurationRequest("item"); this.setState({currentTab: "item"})}}>Item Configuration</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#siteconfiguration" role="tab" data-toggle="tab" onClick={() => {this.props.getItemConfigurationRequest("site"); this.setState({currentTab: "site"})}}>Site Configuration</a>
                            </li>
                            <li className="pst-button">
                                <div className="pstb-inner">
                                    <button type="button" className="pst-save" onClick={this.confirmSave}>Save</button>
                                    <button type="button" onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="col-lg-12 p-lr-47">
                    <div className="tab-content">
                        {/* <div className="tab-pane fade in active" id="general" role="tabpanel">
                            <div className="inventory-configuration-filter m-top-30">
                                <div className="icf-item-filter">
                                    <div className="icfif-head">
                                        <h3>Item Filter</h3>
                                        <div className="nph-switch-btn">
                                            <label className="tg-switch">
                                                <input type="checkbox" checked />
                                                <span className="tg-slider tg-round"></span>
                                                <span className="ccbd-text advance">Enable</span>
                                                {/* <span className="ccbd-text">Disabled</span> }
                                            </label>
                                        </div>
                                        <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "item")}><img src={require('../../assets/settings-icon.svg')} /></button>
                                    </div>
                                    {/* <div className="pi-new-layout">
                                        <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="pnl-purchase-label">Division</label>
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Division" onKeyDown={this.divisionData} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.selectDivision && <ItemFilter />}
                                            </div>
                                        </div>
                                        <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="pnl-purchase-label">Section</label>
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Section" onKeyDown={this.sectionData} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.selectSection && <ItemFilter />}
                                            </div>
                                        </div>
                                        <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="pnl-purchase-label">Department</label>
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Department" onKeyDown={this.departmentData}  />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.selectDepartment && <ItemFilter />}
                                            </div>
                                        </div>
                                        <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="pnl-purchase-label">Article</label>
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Article" onKeyDown={this.articleData} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.selectArticle && <ItemFilter />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="icf-item-filter m-top-30">
                                    <div className="icfif-head">    
                                        <h3>Location Filter</h3>
                                        <div className="nph-switch-btn">
                                            <label className="tg-switch">
                                                <input type="checkbox" />
                                                <span className="tg-slider tg-round"></span>
                                                {/* <span className="ccbd-text advance">Enable</span> 
                                                <span className="ccbd-text">Disabled</span>
                                            </label>
                                        </div>
                                        <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "site")}><img src={require('../../assets/settings-icon.svg')} /></button>
                                    </div>
                                    {/* <div className="pi-new-layout">
                                        <div className="col-md-2 col-sm-6 pad-lft-0">
                                            <label className="pnl-purchase-label">Site</label>
                                            <div className="inputTextKeyFucMain">
                                                <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Site" onKeyDown={this.siteData} />
                                                <span className="modal-search-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                    </svg>
                                                </span>
                                                {this.state.selectSite && <LocationFilter />}
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div> */}
                        <div className="tab-pane fade active" id="itemconfiguration" role="tabpanel">
                            <div className="col-lg-8 pad-0">
                                <div className="expand-new-table m-top-30">
                                    <div className="manage-table">
                                        <table className="table gen-main-table">
                                            <thead>
                                                <tr>
                                                    <th><label>Column Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                    <th><label>Mapping With Item Master</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                    <th><label>Field Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                Object.keys(this.state.selectedValues).sort().map((item) => (
                                                    <tr>
                                                        <td><label>{this.state.fieldNames[item]}</label></td>
                                                        <td>
                                                            <select onChange={(e) => {
                                                                let newValues = this.state.selectedValues;
                                                                newValues[item] = e.target.value;
                                                                this.setState({selectedValues: newValues});
                                                            }}>
                                                                {
                                                                    this.state.selectedValues[item] == "''" ? (<option selected disabled>Select Option</option>) : (<option disabled>Select Option</option>)
                                                                }
                                                                {
                                                                    this.state.fieldValues.map((innerItem) => (
                                                                        this.state.selectedValues[item] == innerItem ?
                                                                        (<option selected>{innerItem}</option>) :
                                                                        (<option>{innerItem}</option>)
                                                                    ))
                                                                }
                                                            </select>
                                                        </td>
                                                        <td><input type="text" className="widthAuto" value={this.state.userDefinedValues[item]}
                                                            onChange={(e) => {
                                                                let newValues = this.state.userDefinedValues;
                                                                newValues[item] = e.target.value;
                                                                this.setState({userDefinedValues: newValues});
                                                            }}/>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="col-md-12 pad-0" >
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    {/* <span>Page :</span><label className="paginationBorder">01</label> */}
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    {/* <Pagination /> */}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="siteconfiguration" role="tabpanel">
                            <div className="col-lg-8 pad-0">
                                <div className="expand-new-table m-top-30">
                                    <div className="manage-table">
                                        <table className="table gen-main-table">
                                            <thead>
                                                <tr>
                                                    <th><label>Column Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                    <th><label>Mapping With Site Master</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                    <th><label>Field Name</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                Object.keys(this.state.selectedValues).sort().map((item) => (
                                                    <tr>
                                                        <td><label>{this.state.fieldNames[item]}</label></td>
                                                        <td>
                                                            <select onChange={(e) => {
                                                                let newValues = this.state.selectedValues;
                                                                newValues[item] = e.target.value;
                                                                this.setState({selectedValues: newValues});
                                                            }}>
                                                                {
                                                                    this.state.selectedValues[item] == "''" ? (<option selected disabled>Select Option</option>) : (<option disabled>Select Option</option>)
                                                                }
                                                                {
                                                                    this.state.fieldValues.map((innerItem) => (
                                                                        this.state.selectedValues[item] == innerItem ?
                                                                        (<option selected>{innerItem}</option>) :
                                                                        (<option>{innerItem}</option>)
                                                                    ))
                                                                }
                                                            </select>
                                                        </td>
                                                        <td><input type="text" className="widthAuto" value={this.state.userDefinedValues[item]}
                                                            onChange={(e) => {
                                                                let newValues = this.state.userDefinedValues;
                                                                newValues[item] = e.target.value;
                                                                this.setState({userDefinedValues: newValues});
                                                            }}/>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    {/* <div className="col-md-12 pad-0" >
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><label className="paginationBorder">01</label>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination />
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.confirmSaveModal && <ConfirmationSaveModal discard={this.cancelSave} confirm={this.saveItemConfiguration} />}
                {/* {this.state.settingModal && <InventorySetting closeSetting={this.closeSetting} data={this.state.settingsValues} selectedData={this.state.selectedSetting} save={this.props.saveSiteAndItemFilterRequest} type={this.state.openSetting} setSubmit={this.setSubmit} />} */}
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.confirmClear} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default CoreConfiguration;