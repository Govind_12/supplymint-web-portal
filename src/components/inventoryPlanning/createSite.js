import React from 'react';
import NewSideBar from '../newSidebar';
import SideBar from '../sidebar';
import BraedCrumps from "../breadCrumps";
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import Confirm from '../loaders/arsConfirm';

class CreateSiteMappingPlanning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            toastLoader: false,
            toastMsg: "",
            confirmClearModal: false,
            confirmUpdate: false,
            updateId: 0,
            headerMsg: '',
            paraMsg: '',

            sitePlanningFilters: [],
            id: this.props.location.state == undefined ? 0 : this.props.location.state.id,
            fromSite: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.fromSite,
            fromSiteCode: this.props.location.state == undefined ? '' : this.props.location.state.fromSiteCode,
            toSite: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.toSite,
            toSiteCode: this.props.location.state == undefined ? '' : this.props.location.state.toSiteCode,
            leadDays: this.props.location.state == undefined ? '' : this.props.location.state.leadDays,
            status: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.status,
            storeToStoreAllocation: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.storeToStoreAllocation,

            errorFromSite: false,
            errorToSite: false,
            errorLeadDays: false,
            errorStatus: false,
            errorAllocation: false,
            fromSiteOpen: false,
            toSiteOpen: false,
            setStatus: false,
            siteAllocation: false,
            searchSite: ""
        }
    }

    openFromSite = (e) => {
        e.preventDefault();
        this.setState({
            fromSiteOpen: !this.state.fromSiteOpen
        }, () => document.addEventListener('click', this.closeFromSite));
    }
    closeFromSite = (e) => {
        if (e === undefined || (document.getElementById("fromSiteNames") != null && !document.getElementById("fromSiteNames").contains(e.target))) {
            this.setState({ fromSiteOpen: false, searchSite: "" }, () => {
                document.removeEventListener('click', this.closeFromSite);
            });
        }
    }

    openToSite = (e) => {
        e.preventDefault();
        this.setState({
            toSiteOpen: !this.state.toSiteOpen
        }, () => document.addEventListener('click', this.closeToSite));
    }
    closeToSite = (e) => {
        if (e === undefined || (document.getElementById("toSiteNames") != null && !document.getElementById("toSiteNames").contains(e.target))) {
            this.setState({ toSiteOpen: false, searchSite: "" }, () => {
                document.removeEventListener('click', this.closeToSite);
            });
        }
    }

    openSetStatus = (e) => {
        e.preventDefault();
        this.setState({
            setStatus: !this.state.setStatus
        }, () => document.addEventListener('click', this.closeSetStatus));
    }
    closeSetStatus = (e) => {
        if (e === undefined || (document.getElementById("statusOptions") != null && !document.getElementById("statusOptions").contains(e.target))) {
            this.setState({ setStatus: false }, () => {
                document.removeEventListener('click', this.closeSetStatus);
            });
        }
    }

    openSiteAllocation = (e) => {
        e.preventDefault();
        this.setState({
            siteAllocation: !this.state.siteAllocation
        }, () => document.addEventListener('click', this.closeSiteAllocation));
    }
    closeSiteAllocation = (e) => {
        if (e === undefined || (document.getElementById("allocationOptions") != null && !document.getElementById("allocationOptions").contains(e.target))) {
            this.setState({ siteAllocation: false }, () => {
                document.removeEventListener('click', this.closeSiteAllocation);
            });
        }
    }

    componentDidMount() {
        this.props.getSitePlanningFiltersRequest();
        console.log(this.props.location);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getSitePlanningFilters.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                sitePlanningFilters: nextProps.seasonPlanning.getSitePlanningFilters.data.resource
            });
            this.props.getSitePlanningFiltersClear();
        }
        else if (nextProps.seasonPlanning.getSitePlanningFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSitePlanningFilters.message.status,
                errorCode: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorMessage
            });
            this.props.getSitePlanningFiltersClear();
        }

        if (nextProps.seasonPlanning.saveSitePlanning.isSuccess) {
            if (nextProps.seasonPlanning.saveSitePlanning.data.resource.isSuccess) {
                this.setState({
                    loading: false,
                    success: true,
                    successMessage: nextProps.seasonPlanning.saveSitePlanning.data.resource.message,
                    error: false
                });
            }
            else {
                this.setState({
                    loading: false,
                    success: false,
                    error: false,
                    id: nextProps.seasonPlanning.saveSitePlanning.data.resource.id,
                    headerMsg: nextProps.seasonPlanning.saveSitePlanning.data.resource.message,
                    paraMsg: "Click confirm to continue.",
                    confirmUpdate: true
                });
            }
            
            this.props.saveSitePlanningClear();
        }
        else if (nextProps.seasonPlanning.saveSitePlanning.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSitePlanning.message.status,
                errorCode: nextProps.seasonPlanning.saveSitePlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSitePlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSitePlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSitePlanning.message.error.errorMessage
            });
            this.props.saveSitePlanningClear();
        }

        if (nextProps.seasonPlanning.getSitePlanningFilters.isLoading || nextProps.seasonPlanning.saveSitePlanning.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false
            });
        }
    }

    validateFromSite = (site) => {
        if (site == 'Select Option') {
            this.setState({
                errorFromSite: true
            });
            return true;
        }
        else {
            this.setState({
                errorFromSite: false
            });
            return false;
        }
    }

    validateToSite = (site) => {
        if (site == 'Select Option') {
            this.setState({
                errorToSite: true
            });
            return true;
        }
        else {
            this.setState({
                errorToSite: false
            });
            return false;
        }
    }

    validateLeadDays = (leadDays) => {
        if (!/^\d+$/.test(leadDays) || leadDays == '') {
            this.setState({
                errorLeadDays: true
            });
            return true;
        }
        else {
            this.setState({
                errorLeadDays: false
            });
            return false;
        }
    }

    validateStatus = (status) => {
        if (status == 'Select Option') {
            this.setState({
                errorStatus: true
            });
            return true;
        }
        else {
            this.setState({
                errorStatus: false
            });
            return false;
        }
    }

    validateAllocation = (allocation) => {
        if (allocation == 'Select Option') {
            this.setState({
                errorAllocation: true
            });
            return true;
        }
        else {
            this.setState({
                errorAllocation: false
            });
            return false;
        }
    }

    clearData = () => {
        this.setState({
            fromSite: 'Select Option',
            fromSiteCode: '',
            toSite: 'Select Option',
            toSiteCode: '',
            leadDays: '',
            status: 'Select Option',
            storeToStoreAllocation: 'Select Option',

            errorFromSite: false,
            errorToSite: false,
            errorLeadDays: false,
            errorStatus: false,
            errorAllocation: false,

            confirmClearModal: false
        });
    }

    saveSitePlanning = (e) => {
        const errorFromSite = this.validateFromSite(this.state.fromSite);
        const errorToSite = this.validateToSite(this.state.toSite);
        const errorLeadDays = this.validateLeadDays(this.state.leadDays);
        const errorStatus = this.validateStatus(this.state.status);
        const errorAllocation = this.validateAllocation(this.state.storeToStoreAllocation);

        if (!errorFromSite && !errorToSite && !errorLeadDays && !errorStatus && !errorAllocation) {
            this.setState({submit: true});
            this.props.saveSitePlanningRequest({
                id: this.state.id,
                fromSiteName: this.state.fromSite,
                fromSiteCode: this.state.fromSiteCode,
                toSiteName: this.state.toSite,
                toSiteCode: this.state.toSiteCode,
                leadDays: this.state.leadDays,
                status: this.state.status,
                siteToSiteAllocation: this.state.storeToStoreAllocation 
            });
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false
        }, () => {

        });
        this.props.history.push("/inventoryPlanning/siteMapping");
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        });
    }

    closeUpdate = () => {
        this.setState({
            confirmUpdate: false,
            id: 0
        });
    }

    handleSiteName = (siteName, siteCode, type) => {
        if (type === "from") {
            if(this.state.toSiteCode == siteCode) {
                this.setState({
                    toastMsg: "From Site and To Site locations cannot be same!",
                    toastLoader: true,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000);
            }
            else {
                this.setState({fromSite: siteName, fromSiteCode: siteCode});
                this.validateFromSite(siteName);
                this.closeFromSite();
            }
        }
        else if (type === "to") {
            if(this.state.fromSiteCode == siteCode) {
                this.setState({
                    toastMsg: "From Site and To Site locations cannot be same!",
                    toastLoader: true,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 3000);
            }
            else {
                this.setState({toSite: siteName, toSiteCode: siteCode});
                this.validateToSite(siteName);
                this.closeToSite();
            }
        }
    }

    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/inventoryPlanning/siteMapping")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="excel-export">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="8.627" height="10" viewBox="0 0 8.627 12.652">
                                            <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)"/>
                                        </svg>
                                    </span>
                                    Excel Upload
                                </button>
                                <button type="button" className="get-details" onClick={this.saveSitePlanning}>Save</button>
                                <button type="button" disabled={this.props.location.state == undefined ? "" : "disabled"} onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 m-top-30 p-lr-47">
                    <div className="site-mapping-page">
                        <div className="col-lg-3 pad-lft-0">
                            <div className="smp-inner">
                                <label>From Site</label>
                                {/* <select className={this.state.errorFromSite ? "errorBorder" : ""} value={this.state.fromSiteCode} onChange={(e) => {
                                    if(this.state.toSiteCode == e.target.value) {
                                        this.setState({
                                            toastMsg: "From Site and To Site locations cannot be same!",
                                            toastLoader: true,
                                        })
                                        setTimeout(() => {
                                            this.setState({
                                                toastLoader: false
                                            })
                                        }, 5000);
                                        e.target.options.selectedIndex = 0;
                                    }
                                    else {
                                        this.setState({fromSite: e.target.options[e.target.selectedIndex].text, fromSiteCode: e.target.value});
                                        this.validateFromSite(e.target.value);
                                    }
                                        
                                }}>
                                    <option disabled selected value=''>Select Site</option>
                                    {this.state.sitePlanningFilters.map((item) => <option value={item.siteCode}>{item.siteName}</option>)}
                                </select> */}
                                <div className="gen-custom-select">
                                    <button type="button" className={this.state.errorFromSite ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openFromSite(e)}>{this.state.fromSite}</button>
                                    {this.state.fromSiteOpen &&
                                    <div className="gcs-dropdown" id="fromSiteNames">
                                        <ul>
                                            <li className="gcsd-search">
                                                <input type="search" placeholder="Search...." onChange={(e) => this.setState({searchSite: e.target.value})} />
                                                <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                            </li>
                                            {this.state.sitePlanningFilters.map((innerItem) => (
                                                innerItem.siteCode.toString().includes(this.state.searchSite) || innerItem.siteName.includes(this.state.searchSite) ?
                                                <li onClick={() => this.handleSiteName(innerItem.siteName, innerItem.siteCode, "from")}>{innerItem.siteCode} - {innerItem.siteName}</li> :
                                                null
                                            ))}
                                        </ul>
                                    </div>}
                                </div>
                                {this.state.errorFromSite ? <span className="error">Select From Site</span> : null}
                            </div>
                        </div>
                        <div className="col-lg-3">
                            <div className="smp-inner">
                                <label>To Site</label>
                                {/* <select className={this.state.errorToSite ? "errorBorder" : ""} value={this.state.toSiteCode} onChange={(e) => {
                                    if(this.state.fromSiteCode == e.target.value) {
                                        this.setState({
                                            toastMsg: "From Site and To Site locations cannot be same!",
                                            toastLoader: true,
                                        })
                                        setTimeout(() => {
                                            this.setState({
                                                toastLoader: false
                                            })
                                        }, 3000);
                                        e.target.options.selectedIndex = 0;
                                    }
                                    else {
                                        this.setState({toSite: e.target.options[e.target.selectedIndex].text, toSiteCode: e.target.value});
                                        this.validateToSite(e.target.value);
                                    }
                                }}>
                                    <option disabled selected value=''>Select Site</option>
                                    {this.state.sitePlanningFilters.map((item) => <option value={item.siteCode}>{item.siteName}</option>)}
                                </select> */}
                                <div className="gen-custom-select">
                                    <button type="button" className={this.state.errorToSite ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openToSite(e)}>{this.state.toSite}</button>
                                    {this.state.toSiteOpen &&
                                    <div className="gcs-dropdown" id="toSiteNames">
                                        <ul>
                                            <li className="gcsd-search">
                                                <input type="search" placeholder="Search...." onChange={(e) => this.setState({searchSite: e.target.value})} />
                                                <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                            </li>
                                            {this.state.sitePlanningFilters.map((innerItem) => (
                                                innerItem.siteCode.toString().includes(this.state.searchSite) || innerItem.siteName.includes(this.state.searchSite) ?
                                                <li onClick={() => this.handleSiteName(innerItem.siteName, innerItem.siteCode, "to")}>{innerItem.siteCode} - {innerItem.siteName}</li> :
                                                null
                                            ))}
                                        </ul>
                                    </div>}
                                </div>
                                {this.state.errorToSite ? <span className="error">Select To Site</span> : null}
                            </div>
                        </div>
                        <div className="col-lg-2">
                            <div className="smp-inner">
                                <label>Lead Days</label>
                                <input className={this.state.errorLeadDays ? "errorBorder" : ""} type="text" value={this.state.leadDays} onChange={(event) => {this.setState({leadDays: event.target.value}); this.validateLeadDays(event.target.value)}} />
                                {this.state.errorLeadDays ? <span className="error">Enter valid Lead Days</span> : null}
                            </div>
                        </div>
                        <div className="col-lg-2">
                            <div className="smp-inner">
                                <label>Set Status</label>
                                {/* <select value={this.state.status} className={this.state.errorStatus ? "errorBorder" : ""} onChange={(event) => {
                                    this.setState({status: event.target.value});
                                    this.validateStatus(event.target.value);
                                }}>
                                    <option disabled selected value=''>Select Option</option>
                                    <option>Active</option>
                                    <option>Inactive</option>
                                </select> */}
                                <div className="gen-custom-select">
                                    <button type="button" className={this.state.errorStatus ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openSetStatus(e)}>{this.state.status}</button>
                                    {this.state.setStatus &&
                                    <div className="gcs-dropdown" id="statusOptions">
                                        <ul>
                                            <li onClick={() => {this.setState({status: "Active"}); this.validateStatus("Active"); this.closeSetStatus();}}>Active</li>
                                            <li onClick={() => {this.setState({status: "Inactive"}); this.validateStatus("Inactive"); this.closeSetStatus();}}>Inactive</li>
                                        </ul>
                                    </div>}
                                </div>
                                {this.state.errorStatus ? <span className="error">Select Status</span> : null}
                            </div>
                        </div>
                        <div className="col-lg-2 pad-lft-0">
                            <div className="smp-inner">
                                <label>Site to Site Allocation</label>
                                {/* <select value={this.state.storeToStoreAllocation} className={this.state.errorAllocation ? "errorBorder" : ""} onChange={(event) => {
                                    this.setState({storeToStoreAllocation: event.target.value});
                                    this.validateAllocation(event.target.value);
                                }}>
                                    <option selected disabled value=''>Select Option</option>
                                    <option>Yes</option>
                                    <option>No</option>
                                </select> */}
                                <div className="gen-custom-select">
                                    <button type="button" className={this.state.errorAllocation ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openSiteAllocation(e)}>{this.state.storeToStoreAllocation}</button>
                                    {this.state.siteAllocation &&
                                    <div className="gcs-dropdown" id="allocationOptions">
                                        <ul>
                                            <li onClick={() => {this.setState({storeToStoreAllocation: "Yes"}); this.validateStatus("Yes"); this.closeSiteAllocation();}}>Yes</li>
                                            <li onClick={() => {this.setState({storeToStoreAllocation: "No"}); this.validateStatus("No"); this.closeSiteAllocation();}}>No</li>
                                        </ul>
                                    </div>}
                                </div>
                                {this.state.errorAllocation ? <span className="error">Select Site to Site Allocation</span> : null}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.clearData} /> : null}
                {this.state.confirmUpdate ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeUpdate} afterConfirm={this.saveSitePlanning} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>       
        )
    }
}

export default CreateSiteMappingPlanning; 