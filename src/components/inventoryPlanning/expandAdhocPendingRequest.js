import React from 'react';

class ExpandAdhocPendingRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            headers: this.props.headers
        }
    }

    handleSubmit = () => {
        let payload = this.state.data.map((item) => ({workOrder: item.workOrder, icode: item.icode, requestedQuantity: item.requestedQuantity, approvedQuantity: item.approvedQuantity, rejectedQuantity: item.rejectedQuantity, id: item.id}));
        this.props.save(payload);
    }

    render () {
        return (
            <div className="gen-sticky-tab sticky-table-width">
                <div className="gen-table-form">
                    <form>
                        {
                            Object.keys(this.state.headers).map((key) =>
                                <div className="gtf-col">
                                    <label>{this.state.headers[key]}</label>
                                    {this.state.data.map((item, index) =>
                                        key == "icode" ? <label className="gtfc-label">{item.icode}</label> :
                                        key == "requestedQuantity" ? <input className="gtf-qty" type="text" value={item.requestedQuantity} disabled /> :
                                        key == "pendingQuantity" ? <input className="gtf-qty" type="text" value={item.requestedQuantity - item.approvedQuantity - item.rejectedQuantity} disabled /> :
                                        key == "approvedQuantity" ? <input className="gtf-qty" type="text" value={item.approvedQuantity} onChange={(e) => {let update = this.state.data; update[index].approvedQuantity = e.target.value; this.setState({data: update});}} /> :
                                        key == "rejectedQuantity" ? <input className="gtf-qty" type="text" value={item.rejectedQuantity} onChange={(e) => {let update = this.state.data; update[index].rejectedQuantity = e.target.value; this.setState({data: update});}} /> :
                                        <label className="gtfc-label">{item[key]}</label>
                                    )}
                                </div>
                            )
                        }
                        {/* <div className="gtf-col">
                            <label>Item Code</label>
                            {this.state.data.map((item) => (<label className="gtfc-label">{item.icode}</label>))}
                        </div>
                        <div className="gtf-col">
                            <label>Requested Qty</label>
                            {this.state.data.map((item) => (<input className="gtf-qty" type="text" value={item.requestedQuantity} disabled />))}
                        </div>
                        <div className="gtf-col">
                            <label>Pending Qty</label>
                            {this.state.data.map((item) => (<input className="gtf-qty" type="text" value={item.requestedQuantity - item.approvedQuantity - item.rejectedQuantity} disabled />))}
                        </div>
                        <div className="gtf-col">
                            <label>Approved Qty</label>
                            {this.state.data.map((item, index) => (<input className="gtf-qty" type="text" value={item.approvedQuantity}
                            onChange={(e) => {
                                let update = this.state.data;
                                update[index].approvedQuantity = e.target.value;
                                this.setState({data: update});
                            }} />))}
                        </div>
                        <div className="gtf-col">
                            <label>Rejected Qty</label>
                            {this.props.data.map((item, index) => (<input className="gtf-qty" type="text" value={item.rejectedQuantity}
                            onChange={(e) => {
                                let update = this.state.data;
                                update[index].rejectedQuantity = e.target.value;
                                this.setState({data: update});
                            }}
                            />))}
                        </div> */}
                        <div className="gtf-action-btn">
                            {/* <button type="button" className="gtf-edit-btn">Edit Request</button> */}
                            <button type="button" className="gtf-save-btn" onClick={this.handleSubmit}>Save Changes</button>
                        </div>
                    </form>
                </div>
                {/* <div className="gen-sticky-tab-inner">
                    <div className="columnFilterGeneric">
                        <span className="glyphicon glyphicon-menu-left"></span>
                    </div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th className="fix-action-btn width40"><label></label></th>
                                <th><label>Work Order</label></th>
                                <th><label>Site</label></th>
                                <th><label>Created On</label></th>
                                <th><label>Requested On</label></th>
                                <th><label>Status</label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="fix-action-btn width40">
                                    <ul className="table-item-list">
                                        <li className="til-inner til-add-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20 20">
                                                <path fill="#fff" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                            </svg>
                                        </li>
                                    </ul>
                                </td>
                                <td><label>WDS017298</label></td>
                                <td><label>WDS017298</label></td>
                                <td><label>29 dec 2019</label></td>
                                <td><label>29 dec 2019</label></td>
                                <td><label>Pending</label></td>
                            </tr>
                            <tr>
                                <td className="fix-action-btn width40">
                                    <ul className="table-item-list">
                                        <li className="til-inner til-add-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20 20">
                                                <path fill="#fff" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                            </svg>
                                        </li>
                                    </ul>
                                </td>
                                <td><label>WDS017298</label></td>
                                <td><label>WDS017298</label></td>
                                <td><label>29 dec 2019</label></td>
                                <td><label>29 dec 2019</label></td>
                                <td><label>Pending</label></td>
                            </tr>
                            <tr>
                                <td className="fix-action-btn width40">
                                    <ul className="table-item-list">
                                        <li className="til-inner til-add-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20 20">
                                                <path fill="#fff" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                            </svg>
                                        </li>
                                    </ul>
                                </td>
                                <td><label>WDS017298</label></td>
                                <td><label>WDS017298</label></td>
                                <td><label>29 dec 2019</label></td>
                                <td><label>29 dec 2019</label></td>
                                <td><label>Pending</label></td>
                            </tr>
                        </tbody>
                    </table>
                </div> */}
            </div>
        )
    }
}

export default ExpandAdhocPendingRequest;