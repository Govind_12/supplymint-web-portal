import React from 'react';
import fileIcon from '../../assets/uploadNew.svg';
import AddGreen from '../../assets/add-green.svg';
import Close from '../../assets/close-black.svg';
import FilterLoader from "../loaders/filterLoader";

class PlanningParameter extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
            showStoreWise: false,
            loading: true,
            planningParameter: {
                saleDayForRequirement: '',
                saleDayForStoreRanking: '',
                saleDaysForAssortmentRanking: '',
                saleDayForItemRanking: '',
                storeWiseBaseStock: '',
                inventoryAgeing: '',
                sellThroughCapping: '',
                sellThoroughCalculation: '',
                saleContributionA: '',
                marginA: '',
                saleContributionB: '',
                marginB: '',
                saleContributionC: '',
                marginC: ''
            },
            errors: {
                saleContributionB: false,
                marginB: false
            }
		};
    }
    showStoreWise(e) {
        e.preventDefault();
        this.setState({ showStoreWise: true },
        () => document.addEventListener('click', this.closeStoreWise));
    }

    closeStoreWise = () => {
        this.setState({ showStoreWise: false }, () => {
          document.removeEventListener('click', this.closeStoreWise);
        });
    }

    componentDidMount() {
        this.props.getPlanningParameterRequest();
    }

    componentWillReceiveProps(nextProps) {
        console.log("componentDidUpdate", this.props);
        if (nextProps.seasonPlanning.getPlanningParameter.isSuccess) {
            let response = nextProps.seasonPlanning.getPlanningParameter.data.resource.response;
            this.setState({
                loading: false,
                planningParameter: {
                    saleDayForRequirement: response.saleDayForRequirement,
                    saleDayForStoreRanking: response.saleDayForStoreRanking,
                    saleDaysForAssortmentRanking: response.saleDaysForAssortmentRanking,
                    saleDayForItemRanking: response.saleDayForItemRanking,
                    storeWiseBaseStock: response.storeWiseBaseStock,
                    inventoryAgeing: response.inventoryAgeing,
                    sellThroughCapping: response.sellThroughCapping,
                    sellThoroughCalculation: response.sellThoroughCalculation,
                    saleContributionA: response.saleContributionA,
                    marginA: response.marginA,
                    saleContributionB: response.saleContributionB,
                    marginB: response.marginB,
                    saleContributionC: response.saleContributionC,
                    marginC: response.marginC
                },
            });
        }
        else if (nextProps.seasonPlanning.getPlanningParameter.isLoading) {
            this.setState({
                loading: true
            });
            console.log("getPlanningParameter Loading");
        }
        else if (nextProps.seasonPlanning.getPlanningParameter.isError) {
            this.setState({
                loading: false
            });
            console.log("getPlanningParameter Error");
        }
    }

    handleChange = (e) => {
        let elem = e.target;
        let pp = this.state.planningParameter;

        if (elem.id == "saleContributionB") {
            this.setState(
                {planningParameter: {...this.state.planningParameter, saleContributionB: elem.value}},
                () => {
                    if (pp.saleContributionA && (parseInt(pp.saleContributionA) < parseInt(elem.value))) {
                        this.setState({errors: {...this.state.errors, saleContributionB: true}});
                    }
                    else {
                        this.setState({errors: {...this.state.errors, saleContributionB: false}});
                    }
                }
            );
        }
        else if (elem.id == "marginB") {
            this.setState(
                {planningParameter: {...this.state.planningParameter, marginB: elem.value}},
                () => {
                    if (pp.marginB && (parseInt(pp.marginA) < parseInt(elem.value))) {
                        this.setState({errors: {...this.state.errors, marginB: true}});
                    }
                    else {
                        this.setState({errors: {...this.state.errors, marginB: false}});
                    }
                }
            );
        }
    }

    savePlanningParameter = () => { //VALIDATE BEFORE SAVING
        this.props.savePlanningParameterRequest(this.state.planningParameter);
    }

    clearPlanningParameter = () => {
        this.setState({
            planningParameter: {
                saleDayForRequirement: '',
                saleDayForStoreRanking: '',
                saleDaysForAssortmentRanking: '',
                saleDayForItemRanking: '',
                storeWiseBaseStock: '',
                inventoryAgeing: '',
                sellThroughCapping: '',
                sellThoroughCalculation: '',
                saleContributionA: '',
                marginA: '',
                saleContributionB: '',
                marginB: '',
                saleContributionC: '',
                marginC: ''
            }
        });
    }

    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <div className="nph-title">
                                    <h2>Planning Parameter</h2>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="get-details" onClick={this.savePlanningParameter}>Save</button>
                                <button type="button" onClick={this.clearPlanningParameter}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ars-planning-parameter p-lr-47">
                    <div className="app-head-section">
                        <div className="app-hs-col">
                            <label>Sale Days for Requirement</label>
                            <input onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, saleDayForRequirement: e.target.value}})}
                                value={this.state.planningParameter.saleDayForRequirement} type="text" list="requirement"/>
                            <datalist id="requirement">
                                <option>7</option>
                                <option>15</option>
                                <option>30</option>
                                <option>45</option>
                                <option>60</option>
                                <option>90</option>
                            </datalist>
                        </div>
                        <div className="app-hs-col">
                            <label>Sale Days for Store Ranking</label>
                            <input onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, saleDayForStoreRanking: e.target.value}})}
                                value={this.state.planningParameter.saleDayForStoreRanking} type="text" list="requirement"/>
                            <datalist id="requirement">
                                <option>7</option>
                                <option>15</option>
                                <option>30</option>
                                <option>45</option>
                                <option>60</option>
                                <option>90</option>
                            </datalist>
                        </div>
                        <div className="app-hs-col">
                            <label>Sale Days for Assortment Ranking</label>
                            <input onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, saleDaysForAssortmentRanking: e.target.value}})}
                                value={this.state.planningParameter.saleDaysForAssortmentRanking} type="text" list="requirement"/>
                            <datalist id="requirement">
                                <option>7</option>
                                <option>15</option>
                                <option>30</option>
                                <option>45</option>
                                <option>60</option>
                                <option>90</option>
                            </datalist>
                        </div>
                        <div className="app-hs-col">
                            <label>Sale Days for Item Ranking</label>
                            <input onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, saleDayForItemRanking: e.target.value}})}
                                value={this.state.planningParameter.saleDayForItemRanking} type="text" list="requirement"/>
                            <datalist id="requirement">
                                <option>7</option>
                                <option>15</option>
                                <option>30</option>
                                <option>45</option>
                                <option>60</option>
                                <option>90</option>
                            </datalist>
                        </div>
                        <div className="app-hs-col">
                            <label>Inventory Ageing (days) for Store to Store Transfer</label>
                            <input onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, inventoryAgeing: e.target.value}})}
                                value={this.state.planningParameter.inventoryAgeing} type="text" list="requirement"/>
                            <datalist id="requirement">
                                <option>7</option>
                                <option>15</option>
                                <option>30</option>
                                <option>45</option>
                                <option>60</option>
                                <option>90</option>
                            </datalist>
                        </div>
                        <div className="app-hs-col">
                            <label>Store wise Base stock MBQ</label>
                            <div className="app-hs-col-inner">
                                <select onClick={(e) => this.showStoreWise(e)}>
                                    <option hidden>Select Option</option>
                                </select>
                                {this.state.showStoreWise ? (<div className="app-hsc-store-wise-modal">
                                    <div className="app-hsc-swm-top">
                                        <div className="app-hsc-swm-top-inner">
                                            <span className="app-stw-store-name">
                                                <label>Store Name</label>
                                                <select></select>
                                            </span>
                                            <span className="app-stw-base-qty">
                                                <label>Base Qty</label>
                                                <input type="text" />
                                            </span>
                                            <span className="app-stw-add-icon"><img src={AddGreen} /></span>
                                        </div>
                                    </div>
                                    <div className="app-hsc-swm-bottom">
                                        <div className="app-hsc-bottom-inner">
                                            <div className="bottomToolTip">
                                                <label className="customInput">
                                                    <input type="file" id="fileUpload" onChange={(e) => this.onFileUpload(e, "INVOICE", data)} multiple="multiple" />
                                                    <span >Attach File</span>
                                                    <img src={fileIcon} />
                                                </label>
                                                <div className="fileName">
                                                    <label>Bulk_data_Mar_2020.xls</label><label className="widthAuto"><img src={Close} /></label>
                                                </div>
                                                <div className="fileName" id="fileName"></div>
                                            </div>
                                            <div className="app-hsc-bi-select-temp">
                                                <label>Sample Template</label>
                                                <button type="button">Download</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>) : (null) }
                            </div>
                        </div>
                        <div className="app-hs-col">
                            <label>Sell Through Capping for Store to Store Item Transfer</label>
                            <input type="text" value={this.state.planningParameter.sellThroughCapping}
                                onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, sellThroughCapping: e.target.value}})}></input>
                        </div>
                        <div className="app-hs-col">
                            <label>Sell Through Calculation Sale Days</label>
                            <input onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, sellThoroughCalculation: e.target.value}})}
                                value={this.state.planningParameter.sellThoroughCalculation} type="text" list="requirement"/>
                            <datalist id="requirement">
                                <option>7</option>
                                <option>15</option>
                                <option>30</option>
                                <option>45</option>
                                <option>60</option>
                                <option>90</option>
                            </datalist>
                        </div>
                    </div>
                    <div className="app-bottom-section">
                        <h5>ABC Criteria</h5>
                        <div className="app-bs-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><label>Criteria</label></th>
                                        <th><label>Operator</label></th>
                                        <th><label>Sale Contribution</label><img src={require('../../assets/info-icon.svg')} /></th>
                                        <th><label>Operator</label></th>
                                        <th><label>Gross Margin</label><img src={require('../../assets/info-icon-new.svg')} /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label>A</label></td>
                                        <td><label><input type="text" value="&#62;" disabled/></label></td>
                                        <td><label><input type="text" value={this.state.planningParameter.saleContributionA} onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, saleContributionA: e.target.value}})}/></label></td>
                                        <td><label><input type="text" value="&#62;" disabled/></label></td>
                                        <td><label><input type="text" value={this.state.planningParameter.marginA} onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, marginA: e.target.value}})}/></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>B</label></td>
                                        <td><label><input type="text" value="&#62;" disabled/></label></td>
                                        <td><label>
                                            <input id="saleContributionB" className={this.state.errors.saleContributionB ? "errorBorder" : ""}
                                                type="text" value={this.state.planningParameter.saleContributionB}
                                                onChange={(e) => this.handleChange(e)}/>
                                            {this.state.errors.saleContributionB ?
                                                <span className="error">
                                                    Must be less than that of A
                                                </span> :
                                                null
                                            }
                                        </label></td>
                                        <td><label><input type="text" value="&#62;" disabled/></label></td>
                                        <td><label>
                                            <input id="marginB" className={this.state.errors.marginB ? "errorBorder" : ""}
                                                type="text" value={this.state.planningParameter.marginB}
                                                onChange={(e) => this.handleChange(e)}/>
                                            {this.state.errors.marginB ?
                                                <span className="error">
                                                    Must be less than that of A
                                                </span> :
                                                null
                                            }
                                        </label></td>
                                    </tr>
                                    <tr>
                                        <td><label>C</label></td>
                                        <td>
                                        </td>
                                        <td><label><input type="text" placeholder="Rest" value={this.state.planningParameter.saleContributionC}
                                            onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, saleContributionC: e.target.value}})}/></label></td>
                                        <td>
                                        </td>
                                        <td><label><input type="text" placeholder="Rest" value={this.state.planningParameter.marginC}
                                            onChange={(e) => this.setState({planningParameter: {...this.state.planningParameter, marginC: e.target.value}})}/></label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
            </div>
        )
    }
}

export default PlanningParameter;
