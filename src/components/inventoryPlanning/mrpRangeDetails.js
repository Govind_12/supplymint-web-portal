import React from "react";

class MrpRangeDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            MRPFrom: "",
            MRPTo: "",

            errors: {
                MRPFrom: false,
                MRPTo: false
            }
        }
    }

    validate = () => {
        let err = this.state.errors;
        if (this.state.MRPFrom == "") {
            err.MRPFrom = true;
        }
        else {
            err.MRPFrom = false;
        }

        if (this.state.MRPTo == "") {
            err.MRPTo = true;
        }
        else {
            err.MRPTo = false;
        }

        this.setState({errors: err});
    }

    saveMRPRange = (e) => {
        e.preventDefault();
        this.validate();
        if (this.state.MRPFrom != "" && this.state.MRPTo != "") {
            let payload = {};
            Object.keys(this.props.data).forEach((key) => {
                payload[key] = this.props.appliedParams[key] == undefined ? [] : this.props.appliedParams[key]
            });
            this.props.save({
                hl1Name: payload.hl1_name,
                hl2Name: payload.hl2_name,
                hl3Name: payload.hl3_name,
                hl4Name: payload.hl4_name,
                brand: payload.brand,
                color: payload.color,
                pattern: payload.pattern,
                material: payload.material,
                design: payload.design,
                sleeve: payload.sleeve_length,
                newMrpRangeFrom: this.state.MRPFrom,
                newMrpRangeTo: this.state.MRPTo
            });
        }
    }

    render() {
        console.log(this.props);
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content grc-confirmation-modal mrp-range-detail">
                    <div className="grccm-head ">
                        <h3>MRP Range Details</h3>
                        <div className="grccmh-btn">
                            <button type="button" className="grc-confirm-btn" onClick={(e) => this.saveMRPRange(e)}>Save Changes</button>
                            <button type="button" onClick={this.props.CloseGetDetails}>Cancel</button>
                        </div>
                    </div>
                    <div className="grccm-body mrd-body">
                        <h3>Applied Parameters</h3>
                        <div className="mrdb-section">{
                            Object.keys(this.props.appliedParams).length == 0 ?
                            <i>No parameters applied</i> :
                            Object.keys(this.props.data).map((key) => (
                                this.props.appliedParams[key] == undefined ?
                                null :
                                <div className="mrdbs-inner">
                                    <p>{this.props.data[key]}</p>{
                                        this.props.appliedParams[key].map((item) => (
                                            <button type="button">{item}</button>
                                        ))
                                    }
                                </div>
                            ))
                        }</div>
                        <div className="mrdb-details">
                            <div className="mrdbd-top">
                                <label>Total Icodes</label>
                                <p>{this.props.response.totalRecord}</p>
                            </div>
                            <div className="mrdbd-bottom">
                                <label>New MRP Range</label>
                                <input className={this.state.errors.MRPFrom ? "errorBorder onFocus mrl-input" : "onFocus mrl-input"} type="text" placeholder="&#8377; 0.00" value={this.state.MRPFrom} onChange={(e) => {this.setState({MRPFrom: e.target.value}); this.validate}} />
                                <input className={this.state.errors.MRPTo ? "errorBorder onFocus mrl-input1" : "onFocus mrl-input1"} type="text" placeholder="&#8377; 0.00" value={this.state.MRPTo} onChange={(e) => {this.setState({MRPTo: e.target.value}); this.validate}} />
                            </div>
                            {this.state.errors.MRPFrom || this.state.errors.MRPTo ? <span className="error">Enter From MRP - To MRP</span> : null}
                        </div>
                        {/* <div className="grccmb-table mrd-table">
                            <table className="table">
                                <thead>
                                    <tr>  
                                        <th><label>MRP Range</label></th>
                                        <th><label>New MRP Range</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label>467 - 679</label></td>
                                        <td><label><input type="text" value="0" /> <input type="text" className="mrdt-input" value="0" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>467 - 679</label></td>
                                        <td><label><input type="text" value="0" /> <input type="text" className="mrdt-input" value="0" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>467 - 679</label></td>
                                        <td><label><input type="text" value="0" /> <input type="text" className="mrdt-input" value="0" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>467 - 679</label></td>
                                        <td><label><input type="text" value="0" /> <input type="text" className="mrdt-input" value="0" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>467 - 679</label></td>
                                        <td><label><input type="text" value="0" /> <input type="text" className="mrdt-input" value="0" /></label></td>
                                    </tr>
                                    <tr>
                                        <td><label>467 - 679</label></td>
                                        <td><label><input type="text" value="0" /> <input type="text" className="mrdt-input" value="0" /></label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="grcm-bottom mrd-footer">
                            <div className="grcmb-inner">
                                <span className="grcmb-inner-total-no">Page : <span className="bold mrd-jump">01</span></span>
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default MrpRangeDetails;