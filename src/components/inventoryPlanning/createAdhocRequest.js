import React from 'react';
import ToastLoader from "../loaders/toastLoader";
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import SiteDataModal from './siteDataModal';
import Confirm from '../loaders/arsConfirm';

class CreateAdhocRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: '',
            error: false,
            code: '',
            errorCode: '',
            errorMessage: '',
            toastLoader: false,
            toastMsg: '',
            submit: false,
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',

            searchSite: '',
            siteModal: false,
            siteData: '',
            submitSite: false,

            searchItem: [""],
            itemModal: false,
            itemModalRow: '',
            itemData: '',
            submitItem: false,

            newRequestSite: '',
            newRequestDate: (new Date()).toISOString().split("T")[0],
            newRequestRows: [{
                itemCode: '',
                quantity: ''
            }],

            errorDate: false,
            errorSite: false,
            errorItemCode: [false],
            errorQuantity: [false],
            errorQuantityNum: [false]
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps", nextProps);
        if (this.state.submitSite && nextProps.seasonPlanning.searchAdhocSite.isSuccess) {
            this.setState({
                siteData: nextProps.seasonPlanning.searchAdhocSite.data,
                loading: false,
                error: false,
                success: false,
                siteModal: true
            });
        }
        else if (this.state.submitSite && nextProps.seasonPlanning.searchAdhocSite.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.seasonPlanning.searchAdhocSite.message.status,
                errorCode: nextProps.seasonPlanning.searchAdhocSite.message.error == undefined ? undefined : nextProps.seasonPlanning.searchAdhocSite.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.searchAdhocSite.message.error == undefined ? undefined : nextProps.seasonPlanning.searchAdhocSite.message.error.errorMessage,
                siteModal: false
            });
        }

        if (this.state.submitItem && nextProps.seasonPlanning.searchAdhocItem.isSuccess) {
            this.setState({
                itemData: nextProps.seasonPlanning.searchAdhocItem.data,
                loading: false,
                error: false,
                success: false,
                itemModal: true
            });
        }
        else if (this.state.submitItem && nextProps.seasonPlanning.searchAdhocItem.isError) {
            this.setState({
                loading: false,
                error: true,
                code: nextProps.seasonPlanning.searchAdhocItem.message.status,
                errorCode: nextProps.seasonPlanning.searchAdhocItem.message.error == undefined ? undefined : nextProps.seasonPlanning.searchAdhocItem.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.searchAdhocItem.message.error == undefined ? undefined : nextProps.seasonPlanning.searchAdhocItem.message.error.errorMessage,
                itemModal: false
            });
        }

        if (this.state.submit && nextProps.seasonPlanning.saveAdhoc.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveAdhoc.data.resource.message,
                error: false
            });
        }
        else if (this.state.submit && nextProps.seasonPlanning.saveAdhoc.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveAdhoc.message.status,
                errorCode: nextProps.seasonPlanning.saveAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.saveAdhoc.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.saveAdhoc.message.error.errorMessage
            });
        }

        if (nextProps.seasonPlanning.searchAdhocSite.isLoading || nextProps.seasonPlanning.searchAdhocItem.isLoading || nextProps.seasonPlanning.saveAdhoc.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false,
                siteModal: false,
                itemModal: false
            });
        }
    }

    addNewRow = () => {
        let newRow = {
            itemCode: '',
            quantity: ''
        };
        this.setState({
            searchItem: this.state.searchItem.concat(""),
            newRequestRows: this.state.newRequestRows.concat(newRow),
            errorItemCode: this.state.errorItemCode.concat(false),
            errorQuantity: this.state.errorQuantity.concat(false),
            errorQuantityNum: this.state.errorQuantityNum.concat(false)
        });
    }

    deleteRow = (index) => {
        let updateRow = this.state.newRequestRows;
        if (updateRow.length == 1) {
            this.setState({
                toastMsg: "Single row cannot be deleted!",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 3000);
        }
        else {
            let newSearchItem = this.state.searchItem;
            let newErrorItemCode = this.state.errorItemCode;
            let newErrorQuantity = this.state.errorQuantity;
            let newErrorQuantityNum = this.state.errorQuantityNum;
            updateRow.splice(index, 1);
            newSearchItem.splice(index, 1);
            newErrorItemCode.splice(index, 1);
            newErrorQuantity.splice(index, 1);
            newErrorQuantityNum.splice(index, 1);
            this.setState({
                searchItem: newSearchItem,
                newRequestRows: updateRow,
                errorItemCode: newErrorItemCode,
                errorQuantity: newErrorQuantity,
                errorQuantityNum: newErrorQuantityNum
            });
        }
    }

    validateDate = () => {
        if (this.state.newRequestDate == '') {
            this.setState({errorDate: true});
            return false;
        }
        else {
            return true;
        }
    }

    validateSite = () => {
        if (this.state.newRequestSite == '') {
            this.setState({errorSite: true});
            return false;
        }
        else {
            return true;
        }
    }

    validateItemCode = (value, index) => {
        if (value == '') {
            let newErrorItemCode = this.state.errorItemCode;
            newErrorItemCode[index] = true;
            this.setState({errorItemCode: newErrorItemCode});
            return false;
        }
        else {
            let newErrorItemCode = this.state.errorItemCode;
            newErrorItemCode[index] = false;
            this.setState({errorItemCode: newErrorItemCode});
            return true;
        }
    }

    validateQuantity = (value, index) => {
        if (value == '') {
            let newErrorQuantity = this.state.errorQuantity;
            newErrorQuantity[index] = true;
            this.setState({errorQuantity: newErrorQuantity});
            return false;
        }
        else if (!/^\d+$/.test(value)) {
            let newErrorQuantityNum = this.state.errorQuantityNum;
            newErrorQuantityNum[index] = true;
            this.setState({errorQuantityNum: newErrorQuantityNum});
            return false;
        }
        else {
            let newErrorQuantity = this.state.errorQuantity;
            newErrorQuantity[index] = false;
            let newErrorQuantityNum = this.state.errorQuantityNum;
            newErrorQuantityNum[index] = false;
            this.setState({errorQuantityNum: newErrorQuantityNum, errorQuantity: newErrorQuantity});
            return true;
        }
    }

    saveNewRequest = () => {
        this.validateDate();
        this.validateSite();
        this.state.newRequestRows.forEach((item, index) => this.validateItemCode(item.itemCode, index));
        this.state.newRequestRows.forEach((item, index) => this.validateQuantity(item.quantity, index));

        if (this.validateDate() && this.validateSite() && this.state.newRequestRows.every((item, index) => this.validateItemCode(item.itemCode, index)) && this.state.newRequestRows.every((item, index) => this.validateQuantity(item.quantity, index))) {
            let itemCodeArray = this.state.newRequestRows.map((item) => item.itemCode);
            let quantityArray = this.state.newRequestRows.map((item) => item.quantity);
            this.setState({
                submit: true
            })
            this.props.saveAdhocRequest({
                site: this.state.newRequestSite,
                requestedDate: this.state.newRequestDate,
                itemCode: itemCodeArray.join(),
                requestedQuantity: quantityArray.join()
            });
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false
        }, () => {

        });
        this.props.history.push("/inventoryPlanning/manageAdhocRequest");
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    siteData = (e) => {
        if (e.keyCode == 13) {
            this.searchSiteRequest();
        } else if (e.keyCode === 27) {
            this.closeSiteModal();
        }
    }

    selectSite = (site) => {
        this.setState({searchSite: site, newRequestSite: site, errorSite: false});
        this.closeSiteModal();
    }

    closeSiteModal = () => {
        this.setState({siteModal: false, submitSite: false});
        document.removeEventListener("click", this.closeSiteModalClick);
    }

    closeSiteModalClick = (e) => {
        if(!document.getElementById("siteDataModal").contains(e.target)) {
            this.setState({siteModal: false, submitSite: false});
            document.removeEventListener("click", this.closeSiteModalClick);
        }
    }

    searchSiteRequest = () => {
        this.props.searchAdhocSiteRequest({search: this.state.searchSite, pageNo: 1});
        this.setState({submitSite: true});
        document.addEventListener("click", this.closeSiteModalClick);
    }

    itemData = (e, index) => {
        if (e.keyCode == 13) {
            this.searchItemRequest(index);
        } else if (e.keyCode === 27) {
            this.closeItemModal();
        }
    }

    selectItem = (item, index) => {
        let updateRow = this.state.newRequestRows;
        updateRow[index].itemCode = item;
        let newErrorItemCode = this.state.errorItemCode;
        newErrorItemCode[index] = false;
        let newSearchItem = this.state.searchItem;
        newSearchItem[index] = item;
        this.setState({searchItem: newSearchItem, newRequestRows: updateRow, errorItemCode: newErrorItemCode});
        this.closeItemModal();
    }

    closeItemModal = () => {
        this.setState({itemModal: false, submitItem: false});
        document.removeEventListener("click", this.closeItemModalClick);
    }

    closeItemModalClick = (e) => {
        if(!document.getElementById("siteDataModal").contains(e.target)) {
            this.setState({itemModal: false, submitItem: false});
            document.removeEventListener("click", this.closeItemModalClick);
        }
    }

    searchItemRequest = (index) => {
        this.props.searchAdhocItemRequest({search: this.state.searchItem[index], pageNo: 1});
        this.setState({itemModalRow: index, submitItem: true});
        document.addEventListener("click", this.closeItemModalClick);
    }

    clearData = () => {
        this.setState({
            searchSite: '',
            searchItem: [""],

            newRequestSite: '',
            newRequestDate: '',
            newRequestRows: [{
                itemCode: '',
                quantity: ''
            }],

            errorDate: false,
            errorSite: false,
            errorItemCode: [false],
            errorQuantity: [false],
            errorQuantityNum: [false],

            confirmClearModal: false
        });
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        })
    }

    render () {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/inventoryPlanning/manageAdhocRequest")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="get-details" onClick={this.saveNewRequest}>Save</button>
                                <button type="button" onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="create-adhoc-request">
                        <div className="col-lg-12 pad-0">
                            <div className="col-lg-2 pad-lft-0">
                                <label>Request Date<span className="mandatory">*</span></label>
                                <input type="date" className={this.state.errorDate? "errorBorder" : ""}
                                    placeholder={this.state.newRequestDate == '' ? "YYYY-MM-DD" : ""}
                                    value={this.state.newRequestDate}
                                    onChange={(e) => this.setState({newRequestDate: e.target.value, errorDate: false})}
                                />
                                {this.state.errorDate ? <span className="error">Date is required!</span> : null}
                            </div>
                            <div className="col-lg-3">
                                <label>Site<span className="mandatory">*</span></label>
                                <div className="inputTextKeyFucMain">
                                    <input type="text" className={this.state.errorSite ? "onFocus pnl-purchase-input errorBorder" : "onFocus pnl-purchase-input"} placeholder="Search Site" onKeyDown={this.siteData} onChange={(e) => this.setState({searchSite: e.target.value})} value={this.state.searchSite} />
                                    <span className="modal-search-btn" onClick={this.searchSiteRequest}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                        </svg>
                                    </span>
                                    {this.state.siteModal && <SiteDataModal data={this.state.siteData} close={this.closeSiteModal} search={this.state.searchSite} action={this.props.searchAdhocSiteRequest} type="site" select={this.selectSite} />}
                                </div>
                                {this.state.errorSite ? <span className="error">Site is required!</span> : null}
                            </div>
                        </div>
                        <div className="col-lg-5 pad-0">
                            <div className="car-table">
                                <h3>Select Item Code and Quantity</h3>
                                <div className="cart-inner m-top-30">
                                    <div className="add-more-button">
                                        <button className="amb-add" type="button" onClick={this.addNewRow}>
                                            <span className="amba-inner">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="16" height="16" viewBox="0 0 19.846 19.846">
                                                    <g id="Group_3035">
                                                        <g id="Group_3034">
                                                            <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1"/>
                                                        </g>
                                                    </g>
                                                    <g id="Group_3037" transform="translate(5.311 5.242)">
                                                        <g id="Group_3036">
                                                            <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                                            Add More
                                        </button>
                                    </div>
                                    <div className="event-master-table">
                                        <table className="table">
                                            <thead>
                                                <tr>
                                                    <th className="fix-action-btn"><label></label></th>
                                                    <th><label>Item Code</label></th>
                                                    <th><label>Quantity</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>{
                                                this.state.newRequestRows.map((item, index) => (
                                                    <tr>
                                                        <td className="fix-action-btn">
                                                            <ul className="table-item-list">
                                                                <li className="til-inner til-delete-btn" onClick={() => this.deleteRow(index)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                                    </svg>
                                                                    {/* <span className="generic-tooltip">Delete</span> */}
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>
                                                            <div className="inputTextKeyFucMain">
                                                                <input type="text" className={this.state.errorItemCode[index] ? "onFocus pnl-purchase-input errorBorder" : "onFocus pnl-purchase-input"} placeholder="Search item" onKeyDown={(e) => this.itemData(e, index)} onChange={(e) => {let updateItems = this.state.searchItem; updateItems[index] = e.target.value; this.setState({searchItem: updateItems})}} value={this.state.searchItem[index]} />
                                                                <span className="modal-search-btn" onClick={() => this.searchItemRequest(index)}>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                                    </svg>
                                                                </span>
                                                                {this.state.itemModal && this.state.itemModalRow == index && <SiteDataModal data={this.state.itemData} close={this.closeItemModal} search={this.state.searchItem[index]} action={this.props.searchAdhocItemRequest} type="itemCode" select={this.selectItem} index={index} />}
                                                            </div>
                                                        {this.state.errorItemCode[index] ? <span className="error">Item Code is required!</span> : null}
                                                        </td>
                                                        <td><input type="text" className={this.state.errorQuantity[index] || this.state.errorQuantityNum[index] ? "errorBorder" : ""} value={item.quantity}
                                                            onChange={(e) => {
                                                                let updateRow = this.state.newRequestRows;
                                                                updateRow[index].quantity = e.target.value;
                                                                this.setState({newRequestRows: updateRow});
                                                                this.validateQuantity(e.target.value, index);
                                                            }} />
                                                        {this.state.errorQuantityNum[index] ? <span className="error">Quantity must be a number!</span> : null}
                                                        {this.state.errorQuantity[index] ? <span className="error">Quantity is required!</span> : null}
                                                        </td>
                                                    </tr>
                                                )) 
                                            }</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.clearData} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default CreateAdhocRequest;