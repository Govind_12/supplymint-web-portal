import React from 'react';
import Pagination from '../pagination';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../redux/actions';
import HeaderFilter from '../../assets/headerFilter.svg';
import ColumnSetting from '../analytics/columnSetting';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from '../loaders/filterLoader';
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import GenericDataModal from '../replenishment/genericDataModal';
import Confirm from '../loaders/arsConfirm';

class AssortmentPlanning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //loading: false, HANDLED BY LOADER
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            submit: false,
            //openSetting: '',
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',

            genericDataModalShow: false,
            genericDataModalShowRow: {},
            genericSearchData: {},
            data: {
                hl1_name: "Division",
                hl2_name: "Section",
                hl3_name: "Department",
                hl4_name: "Article",
                brand: "Brand",
                color: "Color",
                pattern: "Pattern",
                material: "Material",
                fit: "Design",
                sleeve_length: "Sleeve",
                article_name: "Article Name",
                mrp: "MRP"
            },
            genericSearchRefs: {},
            selectedItems: {},

            requirementCheckedItems: [],

            type: 1,
            assortmentPlanningRequirement: [],
            tableHeaders: {},
            //ABOVE ITEMS ADDED LATELY

            exportToExcel: false,
            division: [],
            section: [],
            department: [],
            category: [],
            article: [],
            color: [],
            pattern: [],
            material: [],
            neck: [],
            brand: [],
            sleeve: [],
            mrpRange: [],

            currPageR: 0,
            prePageR: 0,
            maxPageR: 0,
            nextPageR: 0,
            pageJumpR: 1,
            totalCountR: 0,

            divisionR : "",
            sectionR: "",
            departmentR: "",
            categoryR: "",
            colorR: "",
            patternR: "",
            materialR: "",
            designR: "",
            brandR: "",
            sleeveR: "",
            mrpRangeR: "",
            articleR: "",

            // divisionCheckR: false,
            // sectionCheckR: false,
            // departmentCheckR: false,
            // categoryCheckR: false,
            // colorCheckR: false,
            // patternCheckR: false,
            // materialCheckR: false,
            // designCheckR: false,
            // brandCheckR: false,
            // sleeveCheckR: false,
            // mrpRangeCheckR: false,
            // articleCheckR: false, 

            currPageA: 0,
            prePageA: 0,
            maxPageA: 0,
            nextPageA: 0,
            pageJumpA: 1,
            totalCountA: 0,

            divisionA: "", 
            sectionA : "",
            departmentA : "",
            colorA: "",
            patternA : "",
            materialA :"",
            designA:"",
            brandA : "",
            articleA : "",

            divisionCheckA: false,
            sectionCheckA: false,
            departmentCheckA: false,
            colorCheckA: false,
            patternCheckA: false,
            materialCheckA: false,
            designCheckA: false,
            brandCheckA: false,
            articleCheckA: false, 

            requirementFlag: false,
            allocationFlag: false,
            totalRequirement : 0,
            totalAllocation: 0,
            allocationBasedItemFlag: false,
            pageTitle: "#requirementAssortment",
            
            requirementAssortment: {},
            allocationAssortment: {},
            //header start::
            getHeaderConfig: ["Icode", "Assortment Name"],
            fixedHeader: ["Icode", "Assortment Name"],
            customHeadersState: ["Icode", "Assortment Name"],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],           
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            headerCondition: false,
            saveState: [],
            //header end::

            toastMsg: "",
            toastLoader: false,
            loader: false,
       
        }
    }
    componentDidMount(){
        // this.setState({ loader: true })
        // this.props.getAssortmentPlanningFiltersRequest();
        this.props.getAllocationBasedOnItemRequest();
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "PLANNING_SETTINGS_ASSORTMENT",
        });
        this.props.getAssortmentPlanningRequest({pageNo: 1, type: 1});
        document.addEventListener("keydown", this.escFunction, false);
    }
    componentWillUnmount(){
        document.removeEventListener("keydown", this.escFunction, false);
    }
    componentWillReceiveProps(nextProps){
        // if( nextProps.inventoryManagement.getAssortmentPlanningFilters.isSuccess ){
        //    this.setState({
        //         division: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.division,
        //         section: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.section,
        //         department: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.department,
        //         category: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.category,
        //         article: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.article,
        //         color: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.color,
        //         pattern: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.pattern,
        //         material: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.material,
        //         neck: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.neck,
        //         brand: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.brand,
        //         sleeve: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.sleeve,
        //         mrpRange: nextProps.inventoryManagement.getAssortmentPlanningFilters.data.resource.MRPRange,
        //         loader: false
        //    })
        //    this.props.getAssortmentPlanningFiltersClear();
        // }
        // if( nextProps.inventoryManagement.getAssortmentPlanningCount.isSuccess && this.state.requirementFlag ){
        //     this.setState({
        //         totalRequirement: nextProps.inventoryManagement.getAssortmentPlanningCount.data.resource.totalCount,
        //         requirementFlag: false,
        //         loader: false
        //     })
        //     this.props.getAssortmentPlannigCountClear();
        // }
        // if( nextProps.inventoryManagement.getAssortmentPlanningCount.isSuccess && this.state.allocationFlag ){
        //     this.setState({
        //         totalAllocation: nextProps.inventoryManagement.getAssortmentPlanningCount.data.resource.totalCount,
        //         allocationFlag: false,
        //         loader: false
        //     })
        //     this.props.getAssortmentPlannigCountClear();
        // }
        // if( nextProps.inventoryManagement.saveAssortmentPlanningDetail.isSuccess ){
        //     switch(this.state.pageTitle){
        //         case "#requirementAssortment" :
        //                 let payloadR = {
        //                     pageNo: 1,
        //                     totalRecord: this.state.totalRequirement,
        //                     type: 1
        //                 } 
        //                 this.props.getAssortmentPlanningDetailRequest(payloadR);
        //                 break;
        //         case "#allocationAssortment" :
        //                let payloadA = {
        //                    pageNo: 1,
        //                    totalRecord: this.state.totalAllocation,
        //                    type: 2
        //                }  
        //                this.props.getAssortmentPlanningDetailRequest(payloadA);
        //                break;      
        //     }
        //     this.setState({loader: false})
        //     this.props.saveAssortmentPlannigDetailClear();
        // }
        // if( nextProps.inventoryManagement.getAssortmentPlanningDetail.isSuccess ){
        //    switch(this.state.pageTitle){
        //        case "#requirementAssortment" :
        //              this.setState({
        //                 requirementAssortment: nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response,
        //                 customHeadersState: Object.keys(nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response[0]), 
        //                 getHeaderConfig: Object.keys(nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response[0]), 
        //                 fixedHeader: Object.keys(nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response[0]),
                       
        //                 currPageR: this.state.pageJumpR,
        //                 prePageR: nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.previousPage,
        //                 maxPageR: nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.maxPage,
        //                 nextPageR: this.state.pageJumpR+1,
        //                 loader: false,
        //              })
        //              break;
        //         case "#allocationAssortment" :
        //             this.setState({
        //                 allocationAssortment: nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response,
        //                 customHeadersState: Object.keys(nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response[0]), 
        //                 getHeaderConfig: Object.keys(nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response[0]), 
        //                 fixedHeader: Object.keys(nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.response[0]),
                        
        //                 currPageA: this.state.pageJumpA,
        //                 prePageA: nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.previousPage,
        //                 maxPageA: nextProps.inventoryManagement.getAssortmentPlanningDetail.data.resource.maxPage,
        //                 nextPageA: this.state.currPageA+1,
        //                 loader: false,
        //              })
        //              break;    
        //    }
        //    this.props.getAssortmentPlanningDetailClear();
        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loader: false,
                success: false,
                error: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
            this.props.getArsGenericFiltersClear();
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loader: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getArsGenericFiltersClear();
        }

        if(nextProps.seasonPlanning.getAllocationBasedOnItem.isSuccess) {
            this.setState({
                loader: false,
                success: false,
                error: false,
                allocationBasedItemFlag: nextProps.seasonPlanning.getAllocationBasedOnItem.data.resource.isEnableDisabled == "true" ? true : false
            });
            this.props.getAllocationBasedOnItemClear();
        }
        else if (nextProps.seasonPlanning.getAllocationBasedOnItem.isError) {
            this.setState({
                loader: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getAllocationBasedOnItem.message.status,
                errorCode: nextProps.seasonPlanning.getAllocationBasedOnItem.message.error == undefined ? undefined : nextProps.seasonPlanning.getAllocationBasedOnItem.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getAllocationBasedOnItem.message.error == undefined ? undefined : nextProps.seasonPlanning.getAllocationBasedOnItem.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getAllocationBasedOnItemClear();
        }

        if(nextProps.seasonPlanning.saveAllocationBasedOnItem.isSuccess) {
            this.setState({
                loader: false,
                success: false,
                error: false,
                allocationBasedItemFlag: !this.state.allocationBasedItemFlag
            });
            this.props.saveAllocationBasedOnItemClear();
        }
        else if (nextProps.seasonPlanning.saveAllocationBasedOnItem.isError) {
            this.setState({
                loader: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveAllocationBasedOnItem.message.status,
                errorCode: nextProps.seasonPlanning.saveAllocationBasedOnItem.message.error == undefined ? undefined : nextProps.seasonPlanning.saveAllocationBasedOnItem.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveAllocationBasedOnItem.message.error == undefined ? undefined : nextProps.seasonPlanning.saveAllocationBasedOnItem.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.saveAllocationBasedOnItemClear();
        }

        if(nextProps.seasonPlanning.getAssortmentPlanning.isSuccess) {
            this.setState({
                loader: false,
                success: false,
                error: false,
                assortmentPlanningRequirement: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.response,
                currPageR: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.currPage,
                prePageR: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.previousPage,
                maxPageR: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.maxPage,
                nextPageR: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.currPage + 1,
                pageJumpR: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.currPage,
                totalCountR: nextProps.seasonPlanning.getAssortmentPlanning.data.resource.totalCount,
            });
            this.props.getAssortmentPlanningClear();
        }
        else if (nextProps.seasonPlanning.getAssortmentPlanning.isError) {
            this.setState({
                loader: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getAssortmentPlanning.message.status,
                errorCode: nextProps.seasonPlanning.getAssortmentPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.getAssortmentPlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getAssortmentPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.getAssortmentPlanning.message.error.errorMessage,
                currPageR: '',
                prePageR: '',
                maxPageR: '',
                nextPageR: '',
                pageJumpR: '',
                totalCountR: ''
                //confirmDelete: false
            });
            this.props.getAssortmentPlanningClear();
        }

        if(nextProps.seasonPlanning.findFilterAssortment.isSuccess) {
            this.setState({
                loader: false,
                success: false,
                error: false,
                //allocationBasedItemFlag: !this.state.allocationBasedItemFlag
                assortmentPlanningRequirement: nextProps.seasonPlanning.findFilterAssortment.data.resource.response,
                currPageR: nextProps.seasonPlanning.findFilterAssortment.data.resource.currPage,
                prePageR: nextProps.seasonPlanning.findFilterAssortment.data.resource.previousPage,
                maxPageR: nextProps.seasonPlanning.findFilterAssortment.data.resource.maxPage,
                nextPageR: nextProps.seasonPlanning.findFilterAssortment.data.resource.currPage + 1,
                pageJumpR: nextProps.seasonPlanning.findFilterAssortment.data.resource.currPage,
                totalCountR: nextProps.seasonPlanning.findFilterAssortment.data.resource.totalCount,

            });
            console.log("----------- GET DETAILS -----------\n", nextProps.seasonPlanning.findFilterAssortment.data);
            this.props.findFilterAssortmentClear();
        }
        else if (nextProps.seasonPlanning.findFilterAssortment.isError) {
            this.setState({
                loader: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.findFilterAssortment.message.status,
                errorCode: nextProps.seasonPlanning.findFilterAssortment.message.error == undefined ? undefined : nextProps.seasonPlanning.findFilterAssortment.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.findFilterAssortment.message.error == undefined ? undefined : nextProps.seasonPlanning.findFilterAssortment.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.findFilterAssortmentClear();
        }

        if(nextProps.seasonPlanning.saveAssortmentPlanning.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveAssortmentPlanning.data.resource.message,
                error: false,
            });
            console.log("----------- GET DETAILS -----------\n", nextProps.seasonPlanning.saveAssortmentPlanning.data);
            this.props.saveAssortmentPlanningClear();
        }
        else if (nextProps.seasonPlanning.saveAssortmentPlanning.isError) {
            this.setState({
                loader: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveAssortmentPlanning.message.status,
                errorCode: nextProps.seasonPlanning.saveAssortmentPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveAssortmentPlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveAssortmentPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveAssortmentPlanning.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.saveAssortmentPlanningClear();
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                success: false,
                tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
            });
            this.props.getHeaderConfigClear();
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            });
        }

        if (nextProps.seasonPlanning.getArsGenericFilters.isLoading || nextProps.seasonPlanning.getAllocationBasedOnItem.isLoading || nextProps.seasonPlanning.saveAllocationBasedOnItem.isLoading || nextProps.seasonPlanning.getAssortmentPlanning.isLoading || nextProps.seasonPlanning.findFilterAssortment.isLoading || nextProps.seasonPlanning.saveAssortmentPlanning.isLoading || nextProps.replenishment.getHeaderConfig.isLoading) {
            this.setState({
                loader: true
            });
        }
    }

    openGetDetails = (e) => {
        console.log(this.state.requirementCheckedItems);
        e.preventDefault();
        if (this.state.requirementCheckedItems.length == 0) {
            this.setState({
                toastMsg: "At least one column must be checked!",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 3000);
        }
        else {
            this.props.findFilterAssortmentRequest({
                ...this.state.selectedItems,
                pageNo: 1,
                type: 1,
                columns: this.state.requirementCheckedItems.join()
            });
        }
    }

    siteData = (e, entity, key, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, search, pageNo) => {
        if (this.state.genericDataModalShowRow != key) this.closeGenericDataModal();
        this.props.getArsGenericFiltersRequest({
            entity: entity,
            key: key,
            search: search,
            pageNo: pageNo
        });
        this.setState({genericSearchData: {payload: {entity: entity, key: key, search: search}}, genericDataModalShowRow: key});
    }

    closeGenericDataModal = () => {
        let newRefs = this.state.genericSearchRefs;
        if (newRefs[this.state.genericDataModalShowRow] !== undefined) {
            newRefs[this.state.genericDataModalShowRow].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                genericSearchRefs: newRefs
            });
        }
    }

    selectItems = (entity, key, items) => {
        let updateItems = {...this.state.selectedItems};
        if (items.length !== 0) {
            updateItems[key] = items;
        }
        else if (items.length === 0 && updateItems[key] !== undefined) {
            delete updateItems[key];
        }
        this.setState({
            selectedItems: updateItems
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false
        }, () => {

        });
        this.onClear();
        this.props.getAssortmentPlanningRequest({pageNo: 1, type: 1});
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    openExportToExcel = (e) => {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }

    page = (e) => {
        if (this.state.pageTitle == "#requirementAssortment") {
            if (e.target.id == "prev") {
                if (this.state.currPageR == "" || this.state.currPageR == undefined || this.state.currPageR == 1) {
                } else {
                    this.setState({
                        prePageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.previousPage,
                        currPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage,
                        nextPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage + 1,
                        maxPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage
                    })
                    if (this.props.seasonPlanning.getAssortmentPlanning.data.resource.previousPage != 0) {
                        this.props.getAssortmentPlanningRequest({
                            pageNo: this.props.seasonPlanning.getAssortmentPlanning.data.resource.previousPage,
                            type: this.state.type
                        });
                    }
                }
            } else if (e.target.id == "next") {
                this.setState({
                        prePageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.previousPage,
                        currPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage,
                        nextPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage + 1,
                        maxPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage
                    })
                if (this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage != this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage) {
                    this.props.getAssortmentPlanningRequest({
                        pageNo: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage + 1,
                        type: this.state.type
                    });
                }
            }
            else if (e.target.id == "first") {
                if (this.state.currPageR == 1 || this.state.currPageR == "" || this.state.currPageR == undefined) {
                }
                else {
                    this.setState({
                        prePageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.previousPage,
                        currPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage,
                        nextPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage + 1,
                        maxPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage
                    })
                    if (this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage <= this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage) {
                        this.props.getAssortmentPlanningRequest({pageNo: 1, type: this.state.type});
                    }
                }
            } else if (e.target.id == "last") {
                if (this.state.currPageR == this.state.maxPageR || this.state.currPageR == undefined) {
                }
                else {
                    this.setState({
                        prePageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.previousPage,
                        currPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage,
                        nextPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage + 1,
                        maxPageR: this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage
                    })
                    if (this.props.seasonPlanning.getAssortmentPlanning.data.resource.currPage <= this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage) {
                        this.props.getAssortmentPlanningRequest({
                            pageNo: this.props.seasonPlanning.getAssortmentPlanning.data.resource.maxPage,
                            type: this.state.type
                        });
                    }
                }
            }
        }
    }

    getAnyPage = _ => {
        if (this.state.pageTitle == "#requirementAssortment") {
            if (_.target.validity.valid) {
                this.setState({ pageJumpR: _.target.value })
                if (_.key == "Enter" && _.target.value != this.state.currPageR) {
                    if (_.target.value != "") {
                        this.props.getAssortmentPlanningRequest({
                            pageNo: _.target.value,
                            type: this.state.type
                        });
                    }
                    else {
                        this.setState({
                            toastMsg: "Page number can not be empty!",
                            toastLoader: true
                        })
                        setTimeout(() => {
                            this.setState({
                                toastLoader: false
                            })
                        }, 3000);
                    }
                }
            }
        }
    }

    // page = (e) => {
    //     e.persist();
    //     if( this.state.pageTitle == "#requirementAssortment"){
    //         if(e.target.id == "first") {
    //             if(this.state.currPageR == 1 || this.state.currPageR == 0 ||this.state.currPageR == undefined || this.state.maxPageR == 1) {
    //             } else {
    //             let payloadR = {
    //                 pageNo: 1,
    //                 totalRecord: this.state.totalRequirement,
    //                 type: 1
    //             }
    //             this.setState({ loader: true, pageJumpR: 1},() => 
    //              this.props.getAssortmentPlanningDetailRequest(payloadR))
    //            }
    //         }
    //         if(e.target.id == "prev") {
    //             if(this.state.currPageR == 0 ||this.state.currPageR == undefined || this.state.currPageR == 1 || this.state.maxPageR == 1) {
    //             } else {
    //             let payloadR = {
    //                 pageNo: this.state.currPageR-1,
    //                 totalRecord: this.state.totalRequirement,
    //                 type: 1
    //             } 
    //             this.setState({ loader: true, pageJumpR: Number(this.state.currPageR)-1}, ()=>
    //             this.props.getAssortmentPlanningDetailRequest(payloadR));
                
    //            }
    //         }
    //         if(e.target.id == "next") {
    //             if(this.state.currPageR == 0 ||this.state.currPageR == undefined || this.state.currPageR == this.state.maxPageR) {
    //             } else {
    //             let payloadR = {
    //                 pageNo: this.state.currPageR+1,
    //                 totalRecord: this.state.totalRequirement,
    //                 type: 1
    //             } 
    //             this.setState({ loader: false,pageJumpR: Number(this.state.currPageR)+1 }, ()=>
    //             this.props.getAssortmentPlanningDetailRequest(payloadR));
                
    //             }
    //         }
    //         if(e.target.id == "last") {
    //             if(this.state.currPageR == 0 ||this.state.currPageR == undefined || this.state.currPageR == this.state.maxPageR) {
    //             } else {
    //             let payloadR = {
    //                 pageNo: this.state.maxPageR,
    //                 totalRecord: this.state.totalRequirement,
    //                 type: 1
    //             }
    //             this.setState({ loader: true, pageJumpR: this.state.maxPageR },()=> 
    //             this.props.getAssortmentPlanningDetailRequest(payloadR))
                
    //             }
    //         }
    //     }
    //     else if( this.state.pageTitle == "#allocationAssortment"){
    //         if(e.target.id == "first") {
    //             if(this.state.currPageA == 1 || this.state.currPageA == 0 ||this.state.currPageA == undefined || this.state.maxPageA == 1) {
    //             } else {
    //             let payloadA = {
    //                 pageNo: 1,
    //                 totalRecord: this.state.totalAllocation,
    //                 type: this.state.allocationBasedItemFlag ? 1 : 2,
    //             } 
    //             this.setState({loader: true, pageJumpA: 1},()=>
    //             this.props.getAssortmentPlanningDetailRequest(payloadA))
                
    //            }
    //         }
    //         if(e.target.id == "prev") {
    //             if(this.state.currPageA == 0 ||this.state.currPageA == undefined || this.state.currPageA == 1 || this.state.maxPageA == 1) {
    //             } else {
    //             let payloadA = {
    //                 pageNo: this.state.currPageA-1,
    //                 totalRecord: this.state.totalAllocation,
    //                 type: this.state.allocationBasedItemFlag ? 1 : 2,
    //             } 
    //             this.setState({ loader:true,pageJumpA: Number(this.state.currPageA)-1}, ()=>
    //             this.props.getAssortmentPlanningDetailRequest(payloadA))
                
    //            }
    //         }
    //         if(e.target.id == "next") {
    //             if(this.state.currPageA == 0 ||this.state.currPageA == undefined || this.state.currPageA == this.state.maxPageA) {
    //             } else {
    //             let payloadA = {
    //                 pageNo: this.state.currPageA+1,
    //                 totalRecord: this.state.totalAllocation,
    //                 type: this.state.allocationBasedItemFlag ? 1 : 2,
    //             } 
    //             this.setState({ loader: true,pageJumpA: Number(this.state.currPageA)+1 },()=>
    //             this.props.getAssortmentPlanningDetailRequest(payloadA))
                
    //             }
    //         }
    //         if(e.target.id == "last") {
    //             if(this.state.currPageA == 0 ||this.state.currPageA == undefined || this.state.currPageA == this.state.maxPageA) {
    //             } else {
    //             let payloadA = {
    //                 pageNo: this.state.maxPageA,
    //                 totalRecord: this.state.totalAllocation,
    //                 type: this.state.allocationBasedItemFlag ? 1 : 2,
    //             } 
    //             this.setState({ loader: true,pageJumpA: this.state.maxPageA },()=>
    //             this.props.getAssortmentPlanningDetailRequest(payloadA))
                
    //             }
    //         }
    //     }
    // }

    // pageChange = (e) =>{
    //     if( this.state.pageTitle == "#requirementAssortment" && this.state.totalRequirement > 0){
    //         this.setState({ pageJumpR : e.target.value, currPageR: e.target.value, loader: true }, ()=>{
    //             if( this.state.currPageR != "" && this.state.currPageR != 0 && this.state.currPageR <= this.state.maxPageR ){
    //                 let payloadR = {
    //                     pageNo: this.state.pageJumpR,
    //                     totalRecord: this.state.totalRequirement,
    //                     type: 1
    //                 } 
    //                 this.props.getAssortmentPlanningDetailRequest(payloadR);
    //             }else{
    //                 this.setState({
    //                     toastMsg: "Page No should not be empty Or greater than max value..",
    //                     toastLoader: true
    //                 })
    //                 setTimeout(() => {
    //                     this.setState({
    //                         toastLoader: false
    //                     })
    //                 }, 3000);
    //             }
    //         })
    //     }
    //     else if(this.state.pageTitle == "#allocationAssortment" && this.state.totalAllocation > 0 ){
    //         this.setState({ pageJumpA : e.target.value, currPageA: e.target.value, loader: true }, ()=>{
    //             if( this.state.currPageA != "" && this.state.currPageA != 0 && this.state.currPageA <= this.state.maxPageA ){
    //                 let payloadA = {
    //                     pageNo: this.state.pageJumpA,
    //                     totalRecord: this.state.totalAllocation,
    //                     type: this.state.allocationBasedItemFlag ? 1 : 2,
    //                 } 
    //                 this.props.getAssortmentPlanningDetailRequest(payloadA);
    //             }else{
    //                 this.setState({
    //                     toastMsg: "Page No should not be empty Or greater than max value..",
    //                     toastLoader: true
    //                 })
    //                 setTimeout(() => {
    //                     this.setState({
    //                         toastLoader: false
    //                     })
    //                 }, 3000);
    //             }
    //         })
    //     }
        
    // }

    allocationBasedItem = () => {
        this.props.saveAllocationBasedOnItemRequest(!this.state.allocationBasedItemFlag);
    //   this.setState({ 
    //     allocationBasedItemFlag: !this.state.allocationBasedItemFlag,
    //   }, () => {
    //       if( this.state.allocationBasedItemFlag )
    //        this.setState({ totalAllocation: this.state.totalRequirement,
    //               //allocationAssortment: this.state.requirementAssortment,
    //               //pageJumpA: this.state.pageJumpR, currPageA: this.state.currPageR,
    //               //maxPageA: this.state.maxPageR
    //             })
    //       else
    //         this.setState({ totalAllocation: 0, allocationAssortment: {},pageJumpA: 1, currPageA: 0,
    //             maxPageA: 0 })
    //     }
    //   );
    }

    filterSelectionR = (e) =>{
      let id = e.target.id;
      let value = e.target.value;
      switch(id){
            case "divisionR":
                this.setState({ divisionR: value }, ()=> this.getCountForR());
                break; 
            case "sectionR":
                this.setState({ sectionR: value }, ()=> this.getCountForR());
                break;
            case "departmentR":
                this.setState({ departmentR: value }, ()=> this.getCountForR());
                break;  
            case "categoryR":
                this.setState({ categoryR: value }, ()=> this.getCountForR());
                break; 
            case "colorR":
                this.setState({ colorR: value }, ()=> this.getCountForR());
                break; 
            case "patternR":
                this.setState({ patternR: value }, ()=> this.getCountForR());
                break;
            case "materialR":
                this.setState({ materialR: value }, ()=> this.getCountForR());
                break;  
            case "designR":
                this.setState({ designR: value }, ()=> this.getCountForR());
                break;
            case "brandR":
                this.setState({ brandR: value }, ()=> this.getCountForR());
                break; 
            case "sleeveR":
                this.setState({ sleeveR: value }, ()=> this.getCountForR());
                break;
            case "mrpRangeR":
                this.setState({ mrpRangeR: value }, ()=> this.getCountForR());
                break;  
            case "articleR":
                this.setState({ articleR: value }, ()=> this.getCountForR());
                break;  
        }
    }

    // handleCheckedR = (e) =>{
    //     let isChecked = e.target.checked;
    //     let id = e.target.id;  
    //     if( id == "divisionCheckR"){
    //         if( isChecked )
    //            this.setState({ divisionCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ divisionCheckR: false, divisionR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "sectionCheckR"){
    //         if( isChecked )
    //            this.setState({ sectionCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ sectionCheckR: false, sectionR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "departmentCheckR"){
    //         if( isChecked )
    //            this.setState({ departmentCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ departmentCheckR: false, departmentR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "categoryCheckR"){
    //         if( isChecked )
    //            this.setState({ categoryCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ categoryCheckR: false, categoryR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "colorCheckR"){
    //         if( isChecked )
    //            this.setState({ colorCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ colorCheckR: false, colorR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "patternCheckR"){
    //         if( isChecked )
    //            this.setState({ patternCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ patternCheckR: false, patternR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "materialCheckR"){
    //         if( isChecked )
    //            this.setState({ materialCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ materialCheckR: false, materialR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "designCheckR"){
    //         if( isChecked )
    //            this.setState({ designCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ designCheckR: false, designR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "brandCheckR"){
    //         if( isChecked )
    //            this.setState({ brandCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ brandCheckR: false, brandR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "sleeveCheckR"){
    //         if( isChecked )
    //            this.setState({ sleeveCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ sleeveCheckR: false, sleeveR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "mrpRangeCheckR"){
    //         if( isChecked )
    //            this.setState({ mrpRangeCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ mrpRangeCheckR: false, mrpRangeR: ""}, ()=> this.getCountForR())   
    //     }
    //     if( id == "articleCheckR"){
    //         if( isChecked )
    //            this.setState({ articleCheckR: true }, ()=> this.getCountForR())
    //         else
    //            this.setState({ articleCheckR: false, articleR: ""}, ()=> this.getCountForR())   
    //     }
    // }

    handleRequirementCheck = () => {

    }

    getCountForR = () =>{ 
        let payload = {
            division: this.state.divisionCheckR ? this.state.divisionR : null,
            section: this.state.sectionCheckR ? this.state.sectionR : null,
            department: this.state.departmentCheckR ? this.state.departmentR : null,
            category: this.state.categoryCheckR ? this.state.categoryR : null,
            color: this.state.colorCheckR ? this.state.colorR : null,
            pattern: this.state.patternCheckR ? this.state.patternR : null,
            material: this.state.materialCheckR ? this.state.materialR : null,
            neck: this.state.designCheckR ? this.state.designR : null,
            article : this.state.articleCheckR ? this.state.articleR : null,
            brand : this.state.brandCheckR ? this.state.brandR : null,
            sleeve : this.state.sleeveCheckR ? this.state.sleeveR : null,
            mrp : this.state.mrpRangeCheckR ? this.state.mrpRangeR : null,
        }
        if( this.state.divisionCheckR || this.state.sectionCheckR || this.state.departmentCheckR || this.state.categoryCheckR ||
            this.state.patternCheckR || this.state.materialCheckR || this.state.designCheckR || this.state.articleCheckR ||
            this.state.brandCheckR || this.state.sleeveCheckR || this.state.mrpRangeCheckR ){
                this.setState({ requirementFlag: true, loader: true}, () =>
                  this.props.getAssortmentPlannigCountRequest(payload)
                )
        }else{
            this.setState({totalRequirement : 0})
        }
    }
    
    filterSelectionA = (e) =>{
        let id = e.target.id;
        let value = e.target.value;
        switch(id){
            case "divisionA":
                this.setState({ divisionA: value }, ()=> this.getCountForA());
                break; 
            case "sectionA":
                this.setState({ sectionA: value }, ()=> this.getCountForA());
                break;
            case "departmentA":
                this.setState({ departmentA: value }, ()=> this.getCountForA());
                break;  
            case "colorA":
                this.setState({ colorA: value }, ()=> this.getCountForA());
                break; 
            case "patternA":
                this.setState({ patternA: value }, ()=> this.getCountForA());
                break;
            case "materialA":
                this.setState({ materialA: value }, ()=> this.getCountForA());
                break;  
            case "designA":
                this.setState({ designA: value }, ()=> this.getCountForA());
                break;
            case "brandA":
                this.setState({ brandA: value }, ()=> this.getCountForA());
                break;   
            case "articleA":
                this.setState({ articleA: value }, ()=> this.getCountForA());
                break;
        }
    }

    handleCheckedA = (e) =>{
        let isChecked = e.target.checked;
        let id = e.target.id;  
        if( id == "divisionCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.divisionCheckR ){
                this.setState({ divisionCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ divisionCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ divisionCheckA: false, divisionA: ""}, ()=> this.getCountForA())   
            }
        }
        if( id == "sectionCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.sectionCheckR ){
                this.setState({ sectionCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ sectionCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ sectionCheckA: false, sectionA: ""}, ()=> this.getCountForA()) 
            }  
        }
        if( id == "departmentCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.departmentCheckR ){
                this.setState({ departmentCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ departmentCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ departmentCheckA: false, departmentA: ""}, ()=> this.getCountForA())
            }   
        }
        if( id == "colorCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.colorCheckR ){
                this.setState({ colorCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ colorCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ colorCheckA: false, colorA: ""}, ()=> this.getCountForA()) 
            }  
        }
        if( id == "patternCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.patternCheckR ){
                this.setState({ patternCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ patternCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ patternCheckA: false, patternA: ""}, ()=> this.getCountForA()) 
            }  
        }
        if( id == "materialCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.materialCheckR ){
                this.setState({ materialCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ materialCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ materialCheckA: false, materialA: ""}, ()=> this.getCountForA()) 
            }  
        }
        if( id == "designCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.designCheckR ){
                this.setState({ designCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ designCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ designCheckA: false, designA: ""}, ()=> this.getCountForA())  
            } 
        }
        if( id == "brandCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.brandCheckR ){
                this.setState({ brandCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ brandCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ brandCheckA: false, brandA: ""}, ()=> this.getCountForA())
            }   
        }
        if( id == "articleCheckA"){
            if( this.state.allocationBasedItemFlag && this.state.articleCheckR ){
                this.setState({ articleCheckA: false })
            }
            else{
                if( isChecked )
                this.setState({ articleCheckA: true }, ()=> this.getCountForA())
                else
                this.setState({ articleCheckA: false, articleA: ""}, ()=> this.getCountForA()) 
            }  
        }
    }

    getCountForA = () =>{
        let payload = {

            division: this.state.allocationBasedItemFlag && this.state.divisionCheckR ? this.state.divisionR :
                       this.state.divisionCheckA ? this.state.divisionA : null,
            section: this.state.allocationBasedItemFlag && this.state.sectionCheckR ? this.state.sectionR :
                       this.state.sectionCheckA ? this.state.sectionA : null,
            department: this.state.allocationBasedItemFlag && this.state.departmentCheckR ? this.state.departmentR :
                       this.state.departmentCheckA ? this.state.departmentA : null,
            color: this.state.allocationBasedItemFlag && this.state.colorCheckR ? this.state.colorR :
                       this.state.colorCheckA ? this.state.colorA : null,
            pattern: this.state.allocationBasedItemFlag && this.state.patternCheckR ? this.state.patternR :
                       this.state.patternCheckA ? this.state.patternA : null,
            material: this.state.allocationBasedItemFlag && this.state.materialCheckR ? this.state.materialR :
                       this.state.materialCheckA ? this.state.materialA : null, 
            neck: this.state.allocationBasedItemFlag && this.state.designCheckR ? this.state.designR :
                       this.state.designCheckA ? this.state.designA : null,
            article: this.state.allocationBasedItemFlag && this.state.articleCheckR ? this.state.articleR :
                       this.state.articleCheckA ? this.state.articleA : null,
            brand: this.state.allocationBasedItemFlag && this.state.brandCheckR ? this.state.brandR :
                       this.state.brandCheckA ? this.state.brandA : null,                                 

        }
        if( this.state.divisionCheckA || this.state.sectionCheckA || this.state.departmentCheckA || this.state.patternCheckA || 
             this.state.materialCheckA || this.state.designCheckA || this.state.articleCheckA || this.state.brandCheckA){
                this.setState({ allocationFlag: true, loader: true}, () =>
                    this.props.getAssortmentPlannigCountRequest(payload))
        }
        else if( this.state.allocationBasedItemFlag && ( this.state.divisionCheckR || this.state.sectionCheckR || this.state.departmentCheckR || this.state.categoryCheckR ||
            this.state.patternCheckR || this.state.materialCheckR || this.state.designCheckR || this.state.articleCheckR ||
            this.state.brandCheckR || this.state.sleeveCheckR || this.state.mrpRangeCheckR )){
              this.setState({totalAllocation: this.state.totalRequirement })
        }else{
            this.setState({ totalAllocation: 0})
        }

    }

    getAssortmentType = (e) =>{
        let title = e.target.hash;
        this.setState({pageTitle: title});
    }

    saveAssortment = () =>{
        console.log(this.state);
        switch(this.state.pageTitle){
           case "#requirementAssortment" :
                this.props.saveAssortmentPlanningRequest({
                    ...this.state.selectedItems,
                    type: 1,
                    columns: this.state.requirementCheckedItems.join()
                });
                // let payloadR = {
                //     division: this.state.divisionCheckR ? this.state.divisionR : null,
                //     section: this.state.sectionCheckR ? this.state.sectionR : null,
                //     department: this.state.departmentCheckR ? this.state.departmentR : null,
                //     category: this.state.categoryCheckR ? this.state.categoryR : null,
                //     color: this.state.colorCheckR ? this.state.colorR : null,
                //     pattern: this.state.patternCheckR ? this.state.patternR : null,
                //     material: this.state.materialCheckR ? this.state.materialR : null,
                //     neck: this.state.designCheckR ? this.state.designR : null,
                //     article : this.state.articleCheckR ? this.state.articleR : null,
                //     brand : this.state.brandCheckR ? this.state.brandR : null,
                //     sleeve : this.state.sleeveCheckR ? this.state.sleeveR : null,
                //     mrp : this.state.mrpRangeCheckR ? this.state.mrpRangeR : null,
                //     type: 1,
                // }
                // if( this.state.totalRequirement > 0 )
                //    this.props.saveAssortmentPlannigDetailRequest(payloadR);
                break;

            case "#allocationAssortment" : 
                let payloadA = {
                    division: this.state.allocationBasedItemFlag && this.state.divisionCheckR ? this.state.divisionR :
                       this.state.divisionCheckA ? this.state.divisionA : null,
                    section: this.state.allocationBasedItemFlag && this.state.sectionCheckR ? this.state.sectionR :
                            this.state.sectionCheckA ? this.state.sectionA : null,
                    department: this.state.allocationBasedItemFlag && this.state.departmentCheckR ? this.state.departmentR :
                            this.state.departmentCheckA ? this.state.departmentA : null,
                    color: this.state.allocationBasedItemFlag && this.state.colorCheckR ? this.state.colorR :
                            this.state.colorCheckA ? this.state.colorA : null,
                    pattern: this.state.allocationBasedItemFlag && this.state.patternCheckR ? this.state.patternR :
                            this.state.patternCheckA ? this.state.patternA : null,
                    material: this.state.allocationBasedItemFlag && this.state.materialCheckR ? this.state.materialR :
                            this.state.materialCheckA ? this.state.materialA : null, 
                    neck: this.state.allocationBasedItemFlag && this.state.designCheckR ? this.state.designR :
                            this.state.designCheckA ? this.state.designA : null,
                    article: this.state.allocationBasedItemFlag && this.state.articleCheckR ? this.state.articleR :
                            this.state.articleCheckA ? this.state.articleA : null,
                    brand: this.state.allocationBasedItemFlag && this.state.brandCheckR ? this.state.brandR :
                            this.state.brandCheckA ? this.state.brandA : null, 
                    type: 2,
                }
                if( this.state.totalAllocation > 0 )
                   this.props.saveAssortmentPlannigDetailRequest(payloadA);
                break;                               
        }
    }

    onClear = () => {
        this.setState({
            genericDataModalShow: false,
            genericDataModalShowRow: {},
            genericSearchData: {},
            selectedItems: {},
            requirementCheckedItems: [],

            confirmClearModal: false
        });
        // this.setState({
        //     division: [],
        //     section: [],
        //     department: [],
        //     category: [],
        //     article: [],
        //     color: [],
        //     pattern: [],
        //     material: [],
        //     neck: [],
        //     brand: [],
        //     sleeve: [],
        //     mrpRange: [],
        //     allocationBasedItemFlag: false,
        //     loader: true
        // }, ()=>{
        //     this.props.getAssortmentPlanningFiltersRequest();
        //     switch(this.state.pageTitle){
        //         case "#requirementAssortment" :
        //             this.setState({ requirementFlag: false, 
        //                             totalRequirement : 0,
        //                             requirementAssortment: {},
        //                             allocationBasedItemFlag: false,
        //                             pageTitle: "#requirementAssortment",
        //                             divisionR : "",
        //                             sectionR: "",
        //                             departmentR: "",
        //                             categoryR: "",
        //                             colorR: "",
        //                             patternR: "",
        //                             materialR: "",
        //                             designR: "",
        //                             brandR: "",
        //                             sleeveR: "",
        //                             mrpRangeR: "",
        //                             articleR: "",
        //                             divisionCheckR: false,
        //                             sectionCheckR: false,
        //                             departmentCheckR: false,
        //                             categoryCheckR: false,
        //                             colorCheckR: false,
        //                             patternCheckR: false,
        //                             materialCheckR: false,
        //                             designCheckR: false,
        //                             brandCheckR: false,
        //                             sleeveCheckR: false,
        //                             mrpRangeCheckR: false,
        //                             articleCheckR: false,
        //                             currPageR: 0,
        //                             maxPageR: 0,
        //                             pageJumpR:1,
        //             })
        //             break;
    
        //         case "#allocationAssortment" : 
        //             this.setState({ allocationFlag: false,
        //                             totalAllocation: 0,
        //                             allocationAssortment: {},
        //                             allocationBasedItemFlag: false,
        //                             pageTitle: "#requirementAssortment",
        //                             divisionA: "", 
        //                             sectionA : "",
        //                             departmentA : "",
        //                             colorA: "",
        //                             patternA : "",
        //                             materialA :"",
        //                             designA:"",
        //                             brandA : "",
        //                             articleA : "",
        //                             divisionCheckA: false,
        //                             sectionCheckA: false,
        //                             departmentCheckA: false,
        //                             colorCheckA: false,
        //                             patternCheckA: false,
        //                             materialCheckA: false,
        //                             designCheckA: false,
        //                             brandCheckA: false,
        //                             articleCheckA: false, 
        //                             currPageA: 0,
        //                             maxPageA: 0,
        //                             pageJumpA:1,
        //             })
        //             break;                               
        //     }
        // });
        
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        })
    }

    // Dynamic Header Code Start Here:: 
    openColumnSetting(data) {
        if (this.state.customHeadersState.length == 0) {
            this.setState({
                headerCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                ColumnSetting: true
            })
        } else {
            this.setState({
                ColumnSetting: false
            })
        }
    }
    pushColumnData(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let customHeadersState = this.state.customHeadersState
        let headerConfigDataState = this.state.headerConfigDataState
        let customHeaders = this.state.customHeaders
        let saveState = this.state.saveState
        if (this.state.headerCondition) {
            if (!data.includes(getHeaderConfig) || this.state.getHeaderConfig.length == 0) {
                getHeaderConfig.push(data)
                if (!getHeaderConfig.includes(headerConfigDataState)) {
                    let keyget = (_.invert(headerConfigDataState))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(this.state.defaultHeaderMap)) {
                    let keygetvalue = (_.invert(headerConfigDataState))[data];
                    this.state.defaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(customHeadersState) || this.state.customHeadersState.length == 0) {
                customHeadersState.push(data)
                if (!customHeadersState.includes(headerConfigDataState)) {
                    let keyget = (_.invert(headerConfigDataState))[data];
                    Object.assign(customHeaders, { [keyget]: data })
                    saveState.push(keyget)
                }
                if (!Object.keys(customHeaders).includes(this.state.headerSummary)) {
                    let keygetvalue = (_.invert(headerConfigDataState))[data];
                    this.state.headerSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            getHeaderConfig,
            customHeadersState,
            customHeaders,
            saveState
        })
    }
    closeColumn(data) {
        let getHeaderConfig = this.state.getHeaderConfig
        let headerConfigState = this.state.headerConfigState
        let customHeaders = []
        let customHeadersState = this.state.customHeadersState
        if (!this.state.headerCondition) {
            for (let j = 0; j < customHeadersState.length; j++) {
                if (data == customHeadersState[j]) {
                    customHeadersState.splice(j, 1)
                }
            }
            for (var key in headerConfigState) {
                if (!customHeadersState.includes(headerConfigState[key])) {
                    customHeaders.push(key)
                }
            }
            if (this.state.customHeadersState.length == 0) {
                this.setState({
                    headerCondition: false
                })
            }
        } else {
            for (var i = 0; i < getHeaderConfig.length; i++) {
                if (data == getHeaderConfig[i]) {
                    getHeaderConfig.splice(i, 1)
                }
            }
            for (var key in headerConfigState) {
                if (!getHeaderConfig.includes(headerConfigState[key])) {
                    customHeaders.push(key)
                }
            }
        }
        customHeaders.forEach(e => delete headerConfigState[e]);
        this.setState({
            getHeaderConfig,
            customHeaders: headerConfigState,
            customHeadersState,
        })
        setTimeout(() => {
            let keygetvalue = (_.invert(this.state.headerConfigDataState))[data];
            let saveState = this.state.saveState
            saveState.push(keygetvalue)
            let headerSummary = this.state.headerSummary
            let defaultHeaderMap = this.state.defaultHeaderMap
            if (!this.state.headerCondition) {
                for (let j = 0; j < headerSummary.length; j++) {
                    if (keygetvalue == headerSummary[j]) {
                        headerSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < defaultHeaderMap.length; i++) {
                    if (keygetvalue == defaultHeaderMap[i]) {
                        defaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                headerSummary,
                defaultHeaderMap,
                saveState
            })
        }, 100);
    }
    saveColumnSetting(e) {
        this.setState({
            ColumnSetting: false,
            headerCondition: false,
            saveState: []
        })
    }
    resetColumnConfirmation() {
        this.setState({
            headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // paraMsg: "Click confirm to continue.",
            confirmModal: true,
        })
    }
    resetColumn() {
        this.setState({
            customHeadersState: this.state.getHeaderConfig
        })
    }

    //Dynamic header end here:::

    escFunction = (event) => {
        if (event.keyCode === 27) {
             this.setState({ confirmModal: false, ColumnSetting: false, headerCondition: false})
        }
    }
    
    render() {  
        console.log(this.state);
        const { division,section, department,category,color,pattern, material,neck, brand,
               sleeve, mrpRange,article,totalRequirement, totalAllocation,requirementAssortment,
               allocationAssortment, divisionCheckR,sectionCheckR, departmentCheckR,
               categoryCheckR,colorCheckR,patternCheckR,materialCheckR,designCheckR,brandCheckR,sleeveCheckR,mrpRangeCheckR,
               articleCheckR,divisionCheckA,sectionCheckA,departmentCheckA,colorCheckA,patternCheckA,materialCheckA,
               designCheckA, brandCheckA, articleCheckA  } = this.state;
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-7 pad-0">
                            <div className="new-gen-left">
                                <div className="nph-title">
                                    <h2>Assortments</h2>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 pad-0">
                            <div className="new-gen-right">
                                <button type="button" onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                                <button type="button" className={this.state.requirementCheckedItems.length == 0 ? "get-details btnDisabled" : "get-details"} onClick={(e) => this.openGetDetails(e)} disabled={this.state.requirementCheckedItems.length == 0 ? "disabled" : ""}>Get Details</button>
                                <button type="button" className={this.state.requirementCheckedItems.length == 0 ? "get-details btnDisabled" : "get-details"} onClick={this.saveAssortment} disabled={this.state.requirementCheckedItems.length == 0 ? "disabled" : ""}>Create {this.state.pageTitle === "#requirementAssortment" ? "Requirement" : "Allocation"} Assortment</button>
                                <button type="button" className="tree-dot-btn" onClick={(e) => this.openExportToExcel(e)}></button>
                                {/* {this.state.exportToExcel &&
                                <ul className="tdm-drop-down">
                                    <li className="tdm-title-line">Track History</li>
                                    <li>View Activities</li>
                                </ul>} */}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active" >
                                <a className="nav-link st-btn p-l-0" href="#requirementAssortment" role="tab" data-toggle="tab" onClick={this.getAssortmentType}>Requirement</a>
                            </li>
                            <li className="nav-item">{
                                this.state.allocationBasedItemFlag ?
                                <a className="nav-link st-btn" style={{cursor: "default"}}>Allocation</a> :
                                <a className="nav-link st-btn" href="#allocationAssortment" role="tab" data-toggle="tab" onClick={this.getAssortmentType}>Allocation</a>
                            }
                            </li>
                            <li className="allcation-tab-item">
                                <div className="ap-check">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" checked={this.state.allocationBasedItemFlag} onChange={this.allocationBasedItem}/>
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <p>Allocation based on item</p>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content p-lr-47">
                        <div className="tab-pane fade in active assortment-page m-top-20" id="requirementAssortment" role="tabpanel">
                            <div className="col-lg-12 pad-0">
                                <div className="pi-new-layout">
                                    <form>{
                                        Object.keys(this.state.data).map((key) => {
                                            this.state.genericSearchRefs[key] = React.createRef();
                                            return (
                                                <div className="col-lg-2 check-input assortment-dropdown">
                                                    <label className="pnl-purchase-label">{this.state.data[key]}</label>
                                                    <div className="inputTextKeyFucMain">
                                                        {/* <input type="text" className="onFocus pnl-purchase-input" placeholder={"Search " + this.state.data[key]} ref={this.state.genericSearchRefs[key]} onKeyDown={(e) => this.siteData(e, "item", key, e.target.value, 1)} /> */}
                                                            <input type="text" className="onFocus pnl-purchase-input"
                                                                id={key + "Focus"}
                                                                placeholder={this.state.selectedItems[key] === undefined || this.state.selectedItems[key].length === 0 ? "No " + this.state.data[key] + " selected" : this.state.selectedItems[key].length === 1 ? this.state.selectedItems[key][0] + " selected" : this.state.selectedItems[key][0] + " + " + (this.state.selectedItems[key].length - 1) + " selected"}
                                                                ref={this.state.genericSearchRefs[key]}
                                                                onFocus={(e) => {if (this.state.genericDataModalShow === false || this.state.genericDataModalShowRow != key) this.openGenericDataModal("item", key, e.target.value, 1)}}
                                                                //onBlur={(e) => {if (this.state.genericDataModalShowRow != this.state.data[key]) e.target.value=""}}
                                                                onKeyDown={(e) => this.siteData(e, "item", key, e.target.value, 1)}
                                                            />
                                                        <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("item", key, this.state.genericSearchRefs[key].current.value, 1)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                                <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                            </svg>
                                                        </span>
                                                        {this.state.genericDataModalShow && this.state.genericDataModalShowRow == key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems[key]} /> : null}
                                                    </div>
                                                    <div className="pnlci-check">
                                                        <label className="checkBoxLabel0">{
                                                            this.state.requirementCheckedItems.indexOf(key) == -1 ?
                                                            <input type="checkBox" onChange={() => this.setState({requirementCheckedItems: this.state.requirementCheckedItems.concat(key)})} /> :
                                                            <input type="checkBox" checked onChange={() => {
                                                                let newItems = this.state.requirementCheckedItems;
                                                                newItems.splice(this.state.requirementCheckedItems.indexOf(key), 1);
                                                                this.setState({requirementCheckedItems: newItems});
                                                            }}/>
                                                        }<span className="checkmark1"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                        {/* <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Division</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Division" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="divisionCheckR" checked={divisionCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Section</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Section" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" >
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="sectionCheckR" checked={sectionCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Department</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Department" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="departmentCheckR" checked={departmentCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Category</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Category" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="categoryCheckR" checked={categoryCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Color</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Color" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="colorCheckR" checked={colorCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-30">
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Pattern</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Pattern" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="patternCheckR" checked={patternCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Material</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Material" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" >
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="materialCheckR" checked={materialCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Design</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Design" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="designCheckR" checked={designCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Brand</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Brand" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="brandCheckR" checked={brandCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Sleeve</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Sleeve" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="sleeveCheckR" checked={sleeveCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-30">
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">MRP Range</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose MRP Range" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="mrpRangeCheckR" checked={mrpRangeCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Article</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Article" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="articleCheckR" checked={articleCheckR} onChange={this.handleCheckedR}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div> */}
                                    </form>
                                </div>
                                {/* <div className="col-lg-10 pad-0">
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Division</label>
                                            <select id="divisionR" onChange={this.filterSelectionR}>
                                                <option value="">Select Division</option>
                                                {divisionCheckR ? division.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="sectionCheckR" checked={sectionCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Section</label>
                                            <select id="sectionR" onChange={this.filterSelectionR}>
                                                <option value="">Select Section</option>
                                                {sectionCheckR ? section.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="departmentCheckR" checked={departmentCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Department</label>
                                            <select id="departmentR" onChange={this.filterSelectionR}>
                                                <option value="">Select Department</option>
                                                {departmentCheckR ? department.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="categoryCheckR" checked={categoryCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Choose Category</label>
                                            <select id="categoryR" onChange={this.filterSelectionR}>
                                                <option value="">Choose Category</option>
                                                {categoryCheckR ? category.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-2 pad-0">
                                    <div className="col-lg-12 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="colorCheckR" checked={colorCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Color</label>
                                            <select className="select-as-color" id="colorR" onChange={this.filterSelectionR}>
                                                <option value="">Select Color</option>
                                                {colorCheckR ? color.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-10 pad-0 m-top-20">
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="patternCheckR" checked={patternCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Pattern</label>
                                            <select id="patternR" onChange={this.filterSelectionR}>
                                                <option value="">Select Pattern</option>
                                                {patternCheckR ? pattern.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="materialCheckR" checked={materialCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Material</label>
                                            <select id="materialR" onChange={this.filterSelectionR}>
                                                <option value="">Select Material</option>
                                                {materialCheckR ? material.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="designCheckR" checked={designCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Design</label>
                                            <select id="designR" onChange={this.filterSelectionR}>
                                                <option value="">Select Design</option>
                                                {designCheckR ? neck.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="brandCheckR" checked={brandCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Brand</label>
                                            <select id="brandR" onChange={this.filterSelectionR}>
                                                <option value="">Select Brand</option>
                                                {brandCheckR ? brand.map( key =>
                                                    <option value={key}>{key}</option>
                                                ):null}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-2 pad-0 m-top-20">
                                    <div className="col-lg-12 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="sleeveCheckR" checked={sleeveCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Sleeve</label>
                                            <select className="select-as-color" id="sleeveR" onChange={this.filterSelectionR}>
                                                <option value="">Select Sleeve</option>
                                                {sleeveCheckR ? sleeve.map( key =>
                                                    <option value={key}>{key}</option>
                                                ):null}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-10 pad-0 m-top-20">
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="mrpRangeCheckR" checked={mrpRangeCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">MRP Range</label>
                                            <select id="mrpRangeR" onChange={this.filterSelectionR}>
                                                <option value="">Select MPR Range</option>
                                                {mrpRangeCheckR ? mrpRange.map( key =>
                                                    <option value={key}>{key}</option>
                                                ):null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="articleCheckR" checked={articleCheckR} onChange={this.handleCheckedR}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Article</label>
                                            <select id="articleR" onChange={this.filterSelectionR}>
                                                <option value="">Select Article</option>
                                                {articleCheckR ? article.map( key =>
                                                    <option value={key}>{key}</option>
                                                ) : null }
                                            </select>
                                        </div>
                                    </div>
                                </div> */}
                            </div>

                            <div className="col-lg-12 pad-0 m-top-30">
                                <div className="ap-details">
                                    <div className="apd-title">
                                        <h2>Assortment Details</h2>
                                    </div>
                                    <div className="apd-numbers">
                                        <p>Total Assortment</p>
                                        <h3>{this.state.totalCountR}</h3>
                                    </div>
                                </div>
                                <div className="asp-table m-top-30">
                                    <div className="asp-manage-table">
                                        {/* <div className="columnFilterGeneric">
                                            <span className="glyphicon glyphicon-menu-left"></span>
                                        </div>
                                        <ColumnSetting {...this.props} {...this.state} ColumnSetting={this.state.ColumnSetting} getHeaderConfig={this.state.getHeaderConfig} openColumnSetting={(e) => this.openColumnSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} /> */}
                                        <table className="table asp-gen-table">
                                            <thead>
                                                {/* <tr>
                                                    {/* <th><label>Item Code</label></th>
                                                    <th><label>Assortment Details</label></th>}
                                                    {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                                      <th key={key} onClick={this.filterHeader}>
                                                       <label>{data}</label>
                                                       <img src={HeaderFilter} className="imgHead" data-key={data}/>
                                                      </th>
                                                    )) : this.state.customHeadersState.map((data, key) => (
                                                       <th key={key} onClick={this.filterHeader}>
                                                          <label>{data}</label>
                                                          <img src={HeaderFilter} className="imgHead" data-key={data}/>
                                                       </th>
                                                    ))}
                                                </tr> */}
                                                <tr>{
                                                    Object.keys(this.state.tableHeaders).map((key) => <th><label>{this.state.tableHeaders[key]}</label><img src={HeaderFilter} className="imgHead" /></th>)    
                                                }</tr>
                                                {/* <tr>
                                                    <th>
                                                        <label>Icode</label>
                                                        <img src={HeaderFilter} className="imgHead" />
                                                    </th>
                                                    <th>
                                                        <label>Item Name</label>
                                                        <img src={HeaderFilter} className="imgHead" />
                                                    </th>
                                                    <th>
                                                        <label>Assortment Name</label>
                                                        <img src={HeaderFilter} className="imgHead" />
                                                    </th>
                                                </tr> */}
                                            </thead>
                                            {/* <tbody>
                                                {/* { requirementAssortment && requirementAssortment.length > 0 ? requirementAssortment.map( keys => (
                                                    <tr key={Math.random()}>
                                                      {this.state.customHeadersState.map((data, key) => (
                                                        <td key={Math.random()}> <label> {keys[data]}</label> </td>
                                                      ))}
                                                    </tr>
                                                  )) : <tr><td><label>NO DATA FOUND</label></td></tr>
                                                } 
                                                {
                                                    this.state.loading ?
                                                    (<td><label>Loading data...</label></td>) :
                                                    this.state.error ?
                                                    (<td><label>Failed to load data!</label></td>) :
                                                    this.state.assortmentPlanningRequirement.length == 0 ?
                                                    (<td><label>No data found!</label></td>) :
                                                    this.state.assortmentPlanningRequirement.map((item) => (
                                                        <tr>
                                                            <td><label>{item.itemCode}</label></td>
                                                            <td><label>{item.itemName}</label></td>
                                                            <td><label>{item.assortmentDetail}</label></td>
                                                        </tr>
                                                    ))
                                                }
                                            </tbody> */}
                                            <tbody>{
                                                Object.keys(this.state.tableHeaders).length == 0 || this.state.assortmentPlanningRequirement.length == 0 ?
                                                <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                                this.state.assortmentPlanningRequirement.map((item) =>
                                                    <tr>{
                                                        Object.keys(this.state.tableHeaders).map((key) => <td><label>{item[key]}</label></td>)
                                                    }</tr>
                                                )  
                                            }</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPageR} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.pageJumpR} />
                                            {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span> */}
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination {...this.state} {...this.props} page={this.page}
                                                prev={this.state.prePageR} current={this.state.currPageR} maxPage={this.state.maxPageR} next={this.state.nextPageR} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span>
                                            <span className="adb-pageNo"><input type="number" max={this.state.maxPageR} min="1" className="paginationBorder" onChange={(e) => this.pageChange(e)} value={this.state.currPageR}/></span>
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <div className="pagination-inner">
                                                <ul className="pagination-item">
                                                {this.state.currPageR == 1 || this.state.currPageR == undefined || this.state.currPageR == "" ? 
                                                    <li >
                                                        <button className="disable-first-btn" disabled>
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> :
                                                    <li >
                                                        <button className="first-btn" onClick={(e) => this.page(e)} id="first" >
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    {this.state.prePageR != 0 && this.state.prePageR != "" && this.state.currPageR != 1 && this.state.currPageR != "" && this.state.currPageR != undefined ?
                                                    <li>
                                                        <button className="prev-btn" onClick={(e) => this.page(e)} id="prev">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> :
                                                    <li>
                                                        <button className="dis-prev-btn">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    <li>
                                                        <button className="pi-number-btn">
                                                            <span>{this.state.currPageR}/{this.state.maxPageR}</span>
                                                        </button>
                                                    </li>
                                                    {this.state.currPageR != "" && this.state.nextPageR - 1 != this.state.maxPageR && this.state.currPageR != undefined ? <li >
                                                        <button className="next-btn" onClick={(e) => this.page(e)} id="next">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                            <button className="dis-next-btn" disabled>
                                                                <span className="page-item-btn-inner">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </li>}
                                                    {this.state.currPageR != 0 && this.state.nextPageR - 1 != this.state.maxPageR && this.state.currPageR != undefined && this.state.maxPageR != this.state.currPageR ? <li >
                                                        <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                            <button className="dis-last-btn" disabled>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </button>
                                                        </li>}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                        </div>
                        <div className="tab-pane fade assortment-page m-top-20" id="allocationAssortment" role="tabpanel">
                            <div className="col-lg-12 pad-0">
                                <div className="pi-new-layout">
                                    <form>
                                        <div className="col-lg-12 col-md-12 col-sm-12 pad-0">
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Division</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Division" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="divisionCheckA" checked={divisionCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Section</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Section" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" >
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="sectionCheckA" checked={sectionCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Department</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Department" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="departmentCheckA" checked={departmentCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Article</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Article" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="articleCheckA" checked={articleCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Color</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Color" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="colorCheckA" checked={colorCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-30">
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Pattern</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Pattern" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="patternCheckA" checked={patternCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Material</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Material" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231" >
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="materialCheckA" checked={materialCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Design</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Design" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="designCheckA" checked={designCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-2 check-input">
                                                <label className="pnl-purchase-label">Brand</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" placeholder="Choose Brand" />
                                                    <span className="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="pnlci-check">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" id="brandCheckA" checked={brandCheckA} onChange={this.handleCheckedA}/>
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {/* <div className="col-lg-10 pad-0">
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="divisionCheckA" checked={divisionCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Division</label>
                                            <select id="divisionA" onChange={this.filterSelectionA}>
                                                <option value="">Select Division</option>
                                                {divisionCheckA ? division.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="sectionCheckA" checked={sectionCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Section</label>
                                            <select id="sectionA" onChange={this.filterSelectionA}>
                                                <option value="">Select Section</option>
                                                {sectionCheckA ? section.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="departmentCheckA" checked={departmentCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Department</label>
                                            <select id="departmentA" onChange={this.filterSelectionA}>
                                                <option value="">Select Department</option>
                                                {departmentCheckA ?department.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="articleCheckA" checked={articleCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Choose Article</label>
                                            <select id="articleA" onChange={this.filterSelectionA}>
                                                <option value="">Choose Article</option>
                                                {articleCheckA ? article.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-2 pad-0">
                                    <div className="col-lg-12 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="colorCheckA" checked={colorCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Color</label>
                                            <select className="select-as-color" id="colorA" onChange={this.filterSelectionA}>
                                                <option value="">Select Color</option>
                                                {colorCheckA ?color.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-10 pad-0 m-top-20">
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="patternCheckA" checked={patternCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Pattern</label>
                                            <select id="patternA" onChange={this.filterSelectionA}>
                                                <option value="">Select Pattern</option>
                                                {patternCheckA ? pattern.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="materialCheckA" checked={materialCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Material</label>
                                            <select id="materialA" onChange={this.filterSelectionA}>
                                                <option value="">Select Material</option>
                                                {materialCheckA ? material.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="designCheckA" checked={designCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Design</label>
                                            <select id="designA" onChange={this.filterSelectionA}>
                                                <option value="">Select Design</option>
                                                {designCheckA ? neck.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 pad-lft-0">
                                        <div className="ap-check">
                                            <label className="checkBoxLabel0">
                                                <input type="checkBox" id="brandCheckA" checked={brandCheckA} onChange={this.handleCheckedA}/>
                                                <span className="checkmark1"></span>
                                            </label>
                                        </div>
                                        <div className="ap-dorp-field">
                                            <label className="assort-label">Brand</label>
                                            <select id="brandA" onChange={this.filterSelectionA}>
                                                <option value="">Select Brand</option>
                                                {brandCheckA ? brand.map( key =>
                                                    <option value={key}>{key}</option>
                                                ): null}
                                            </select>
                                        </div>
                                    </div>
                                </div> */}
                            </div>

                            <div className="col-lg-12 pad-0 m-top-30">
                                <div className="ap-details">
                                    <div className="apd-title">
                                        <h2>Assortment Details</h2>
                                    </div>
                                    <div className="apd-numbers">
                                        <p>Total Assortment</p>
                                        <h3>{totalAllocation}</h3>
                                    </div>
                                </div>
                                <div className="asp-table m-top-30">
                                    <div className="asp-manage-table">
                                        <div className="columnFilterGeneric">
                                            <span className="glyphicon glyphicon-menu-left"></span>
                                        </div>
                                        <ColumnSetting {...this.props} {...this.state} ColumnSetting={this.state.ColumnSetting} getHeaderConfig={this.state.getHeaderConfig} openColumnSetting={(e) => this.openColumnSetting(e)} closeColumn={(e) => this.closeColumn(e)} resetColumnConfirmation={(e) => this.resetColumnConfirmation(e)} pushColumnData={(e) => this.pushColumnData(e)} saveColumnSetting={(e) => this.saveColumnSetting(e)} />
                                        <table className="table asp-gen-table">
                                            <thead>
                                                <tr>
                                                    {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                                      <th key={key} onClick={this.filterHeader}>
                                                       <label>{data}</label>
                                                       <img src={HeaderFilter} className="imgHead" data-key={data}/>
                                                      </th>
                                                    )) : this.state.customHeadersState.map((data, key) => (
                                                       <th key={key} onClick={this.filterHeader}>
                                                          <label>{data}</label>
                                                          <img src={HeaderFilter} className="imgHead" data-key={data}/>
                                                       </th>
                                                    ))}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                { allocationAssortment && allocationAssortment.length > 0 ? allocationAssortment.map( keys => (
                                                    <tr key={Math.random()}>
                                                      {this.state.customHeadersState.map((data, key) => (
                                                        <td key={Math.random()}> <label> {keys[data]}</label> </td>
                                                      ))}
                                                    </tr>
                                                  )) : <tr><td><label>NO DATA FOUND</label></td></tr>
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                        <span>Page :</span>
                                        <span className="adb-pageNo"><input type="number" max={this.state.maxPageA} className="paginationBorder" onChange={(e) => this.pageChange(e)} value={this.state.currPageA}/></span>
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <div className="pagination-inner">
                                                <ul className="pagination-item">
                                                {this.state.currPageA == 1 || this.state.currPageA == undefined || this.state.currPageA == "" ? 
                                                    <li >
                                                        <button className="disable-first-btn" disabled>
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> :
                                                    <li >
                                                        <button className="first-btn" onClick={(e) => this.page(e)} id="first" >
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    {this.state.prePageA != 0 && this.state.prePageA != "" && this.state.currPageA != 1 && this.state.currPageA != "" && this.state.currPageA != undefined ?
                                                    <li>
                                                        <button className="prev-btn" onClick={(e) => this.page(e)} id="prev">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> :
                                                    <li>
                                                        <button className="dis-prev-btn">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li>}
                                                    <li>
                                                        <button className="pi-number-btn">
                                                            <span>{this.state.currPageA}/{this.state.maxPageA}</span>
                                                        </button>
                                                    </li>
                                                    {this.state.currPageA != "" && this.state.nextPageA - 1 != this.state.maxPageA && this.state.currPageA != undefined ? <li >
                                                        <button className="next-btn" onClick={(e) => this.page(e)} id="next">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                            <button className="dis-next-btn" disabled>
                                                                <span className="page-item-btn-inner">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </li>}
                                                    {this.state.currPageA != 0 && this.state.nextPageA - 1 != this.state.maxPageA && this.state.currPageA != undefined && this.state.maxPageA != this.state.currPageA ? <li >
                                                        <button className="last-btn" onClick={(e) => this.page(e)} id="last">
                                                            <span className="page-item-btn-inner">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </li> : <li >
                                                            <button className="dis-last-btn" disabled>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                </svg>
                                                            </button>
                                                        </li>}
                                                </ul>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.onClear} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
            </div>
        )
    }
}

export default AssortmentPlanning; 