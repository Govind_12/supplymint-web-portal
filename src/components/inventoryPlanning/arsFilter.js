import React, { Component } from 'react';
import Close from '../../assets/close-red.svg';
import Search from '../../assets/searchicon.svg';
import Checkbox from '../../assets/checkbox.svg';
import Recent from '../../assets/recent.svg';
import Check from '../../assets/check.svg';
import Delete from '../../assets/delete.svg';
import moment from "moment";
import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;

export default class ArsFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterItems: {...this.props.filterItems},
            search: "",
            checkedFilters: {...this.props.appliedFilters},
            refs: {},
            applyFilterButton: false
        }
    }

    componentDidMount() {
        let refs = this.state.refs;
        Object.keys(this.state.checkedFilters).forEach((key) =>
            refs[key].current.value = this.state.checkedFilters[key]
        );
        this.setState({
            refs,
            applyFilterButton: Object.keys(this.state.checkedFilters).some((key) => this.state.refs[key].current != null && this.state.refs[key].current.value != undefined && this.state.refs[key].current.value != "")
        });
    }

    handleApplyFilterButton = () => {
        this.setState({
            applyFilterButton: Object.keys(this.state.checkedFilters).some((key) => this.state.refs[key].current != null && this.state.refs[key].current.value != undefined && this.state.refs[key].current.value != "")
        });
    }

    checkFilter = (key) => {
        let checkedFilters = this.state.checkedFilters;
        Object.keys(this.state.checkedFilters).indexOf(key) == -1 ? checkedFilters[key] = "" : delete checkedFilters[key];
        this.setState({
            checkedFilters,
            applyFilterButton: Object.keys(this.state.checkedFilters).some((key) => this.state.refs[key].current != null && this.state.refs[key].current.value != undefined && this.state.refs[key].current.value != "")
        });
    }

    handleDateInput = (e, key) => {
        console.log("handleDate", e);
        let refs = this.state.refs;
        if (e == null) {
            refs[key].current.value = ""
        }
        else {
            refs[key].current.value = {
                from: moment(e[0]._d).format('YYYY-MM-DD'),
                to: moment(e[1]._d).format('YYYY-MM-DD'),
            }
        }
        this.setState({
            refs,
            applyFilterButton: Object.keys(this.state.checkedFilters).some((key) => this.state.refs[key].current != null && this.state.refs[key].current.value != undefined && this.state.refs[key].current.value != "")
        });
    }

    render() {
        console.log(this.props);
        return (
            <div className="asn-filter textLeft">
                <div className="backdrop-transparent"></div>
                <div className="asn-filter-inner">
                    <div className="asnf-head">
                        <div className="asnfh-top">
                            <span>Filter</span>
                            <span className="asnfht-input"><input type="search" onChange={(e) => this.setState({search: e.target.value})} placeholder="Type To Search"></input></span>
                            <span className="asn-filter-close displayPointer"><img src={Close} onClick={(e) => this.props.closeFilter(e)}></img></span>
                        </div>
                    </div>
                    <div className="asnf-body">
                        <div className="asnfb-head">
                            <span className="asnfb-left">Header</span><span className="asnfb-right">Value</span>
                        </div>
                        <div className="asnfb-body">
                        {
                            Object.keys(this.state.filterItems).length == 0 ?
                            <span>No filters available!</span> :
                            Object.keys(this.state.filterItems).map((key) => {
                                this.state.refs[key] = React.createRef();
                                if (this.state.filterItems[key].toString().toLowerCase().includes(this.state.search.toLowerCase())) {
                                    return (
                                        <div key={key} className="asnfb-item">
                                            <span className="asnfbb-left">
                                                <span className="asn-check">
                                                    <div className="checkSeperate checkBoxBorder">{
                                                        this.state.checkedFilters[key] == undefined ?
                                                        <label className="checkBoxLabel0"><input type="checkBox" onChange={() => this.checkFilter(key)} />{this.state.filterItems[key]}<span className="checkmark1"></span></label> :
                                                        <label className="checkBoxLabel0"><input type="checkBox" checked onChange={() => this.checkFilter(key)} />{this.state.filterItems[key]}<span className="checkmark1"></span></label>
                                                    }</div>
                                                </span>
                                            </span>
                                            {
                                                this.state.checkedFilters[key] == undefined ?
                                                <span className="asnfbb-right not-checked" onClick={() => this.checkFilter(key)}></span> :
                                                key.toLowerCase().includes("date") || key.toLowerCase().includes("time") ?
                                                <span className="asnfbb-right">
                                                    <RangePicker
                                                        format="YYYY-MM-DD"
                                                        defaultValue={this.state.checkedFilters[key].from == undefined ? null : [moment(this.state.checkedFilters[key].from), moment(this.state.checkedFilters[key].to)]}
                                                        ref={this.state.refs[key]}
                                                        onChange={(e) => this.handleDateInput(e, key)}
                                                    />
                                                </span> :
                                                <span className="asnfbb-right">
                                                    <input 
                                                        type="text"
                                                        ref={this.state.refs[key]}
                                                        placeholder="Enter Value"
                                                        onChange={this.handleApplyFilterButton}
                                                    />
                                                </span>
                                            }
                                        </div>
                                    )
                                }
                            })
                        }
                        </div>
                    </div>
                    <div className="asnf-bottom">
                    {
                        this.state.applyFilterButton == true ?
                        <a className="m-r-10" onClick={() => this.props.applyFilter(this.state.checkedFilters, this.state.refs)}>Apply Filter</a> :
                        <a className="m-r-10 btnDisabled" >Apply Filter</a>
                    }
                    {
                        this.state.applyFilterButton == true ?
                        <a onClick={this.props.clearFilter}>Clear</a> :
                        <a className="btnDisabled">Clear</a>
                    }
                    </div>
                </div>
            </div>
        )
    }
}