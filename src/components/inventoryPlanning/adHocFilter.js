import React from "react";

class AdhocFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: "",
            search: "",
            no: "",
            workOrder: "",
            site: "",
            status: "",
            createdOn: ""
        };
    }

    handleChange(e) {
        if (e.target.id == "workOrder") {
            this.setState({
                workOrder: e.target.value
            });
        } else if (e.target.id == "site") {
            this.setState({
                site: e.target.value
            });
        } else if (e.target.id == "status") {
            this.setState({
                status: e.target.value
            });
        } else if (e.target.id == "createdOn") {
            e.target.placeholder = e.target.value
            this.setState({
                createdOn: e.target.value
            });
        }
    }

    clearFilter(e) {
        this.setState({
            type: "",
            no: "",
            workOrder: "",
            site: "",
            status: "",
            createdOn: ""
        })
        document.getElementById("createdOn").placeholder = "Created On"
    }

    onSubmit(e) {
        e.preventDefault();
 
            let data = {
                no: 1,
                type: 2,
                search: "",
                workOrder: this.state.workOrder,
                site: this.state.site,
                status: this.state.status,
                createdOn: this.state.createdOn
            }
            this.props.getAdHockRequest(data);
            this.props.closeFilter(e);
            this.props.updateFilter(data)
            this.clearFilter()
        }
    

    render() {
        let count = 0;
        if (this.state.workOrder != "") {
            count++;
        }
        if (this.state.site != "") {
            count++;
        } if (this.state.status != "") {
            count++;
        } if (this.state.createdOn != "") {
            count++;
        }
        const { workOrder, site, createdOn, status } = this.state
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={(e) => this.onSubmit(e)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS

                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                     </label>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="container_modal">

                                <ul className="list-inline m-top-20">

                                    <li>
                                        <select id="status" className="organistionFilterModal" value={status} onChange={(e) => this.handleChange(e)}>
                                            <option value="">
                                                Status
                                                </option>
                                            <option value="Removed">Removed</option>
                                            <option value="Canceled">Cancelled</option>
                                            <option value="Inprogress">Inprogress</option>
                                        </select>
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="workOrder" value={workOrder} placeholder="WorkOrder" className="organistionFilterModal" />
                                    </li>
                                    <li>
                                        <input type="text" onChange={(e) => this.handleChange(e)} id="site" placeholder="Site" value={site} className="organistionFilterModal" />
                                    </li>

                                    <li>
                                        <input type="date" value={createdOn} onChange={(e) => this.handleChange(e)} id="createdOn" placeholder="Created On" className="organistionFilterModal" />
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        <button type="button" onClick={(e) => this.clearFilter(e)} className="modal_clear_btn">
                                            CLEAR FILTER
                     </button>
                                    </li>
                                    <li>
                                       {this.state.workOrder != "" || this.state.site != "" || this.state.status != "" || this.state.createdOn != "" ?  <button type="submit" className="modal_Apply_btn">
                                            APPLY
                                        </button>: <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                            APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}

export default AdhocFilter;
