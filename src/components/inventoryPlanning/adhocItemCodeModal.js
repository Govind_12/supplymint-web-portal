import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";

class AdhocItemCodeModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            getItemAdHockData: [],
            sCode: this.props.getItemAdHockCode,
            search: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: 1,
            no: 1,
            toastLoader: false,
            toastMsg: "",
            itemValue: this.props.itemValue,
        }
    }

    handleChange(e) {
        if (e.target.id = "adhocSearch") {
            this.setState(
                {
                    search: e.target.value,
                }
            );

        }
    }

    onsearchClear() {
        this.setState(
            {
                search: "",
                type: 1,
                no: 1

            })
        if (this.state.type == 3) {
            let data = {
                no: 1,
                type: 1,
                search: ""
            }
            this.props.getItemAdHockRequest(data);
        }
    }

    onSelectedData(id) {
        let sdata = this.state.getItemAdHockData
        for (var i = 0; i < sdata.length; i++) {
            if (sdata[i].itemCode == id) {
                
                this.setState({
                    itemValue: sdata[i].itemCode
                })
            }
        }
    }
    onDone() {
   
        var checkedData = "";
        var checkedId = "";
   
    
  

        if (this.state.itemValue == undefined || this.state.itemValue == "") {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {

            checkedData =this.state.itemValue
            checkedId = this.props.adhocId
     
            let finalData = {
                checkedData: checkedData,
                checkedId: checkedId,
            }
                    this.props.updateItem(finalData);
                    this.onCloseSection();
             
        }
    
    }

    onCloseSection(e) {
        this.setState({
            search: "",
            type: 1,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            getItemAdHockData: []

        })

        this.props.closeAdhocModal();
    }


    onSearch(e) {
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,
                search: this.state.search,
            }
            this.props.getItemAdHockRequest(data)
        }
    }

    page(e) {

        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.inventoryManagement.getItemAdHock.data.prePage,
                current: this.props.inventoryManagement.getItemAdHock.data.currPage,
                next: this.props.inventoryManagement.getItemAdHock.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getItemAdHock.data.maxPage,
            })
            if (this.props.inventoryManagement.getItemAdHock.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.inventoryManagement.getItemAdHock.data.currPage - 1,

                    id: this.props.adhocId,
                    search: this.state.search,
                }
                this.props.getItemAdHockRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.inventoryManagement.getItemAdHock.data.prePage,
                current: this.props.inventoryManagement.getItemAdHock.data.currPage,
                next: this.props.inventoryManagement.getItemAdHock.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getItemAdHock.data.maxPage,
            })
            if (this.props.inventoryManagement.getItemAdHock.data.currPage != this.props.inventoryManagement.getItemAdHock.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.inventoryManagement.getItemAdHock.data.currPage + 1,

                    id: this.props.adhocId,
                    search: this.state.search,
                }
                this.props.getItemAdHockRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.inventoryManagement.getItemAdHock.data.prePage,
                current: this.props.inventoryManagement.getItemAdHock.data.currPage,
                next: this.props.inventoryManagement.getItemAdHock.data.currPage + 1,
                maxPage: this.props.inventoryManagement.getItemAdHock.data.maxPage,
            })
            if (this.props.inventoryManagement.getItemAdHock.data.currPage <= this.props.inventoryManagement.getItemAdHock.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,

                    id: this.props.adhocId,
                    search: this.state.search,
                }
                this.props.getItemAdHockRequest(data)
            }

        }
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    componentWillMount() {
        this.setState({
            itemValue: this.props.itemValue,
        })
        if (this.props.inventoryManagement.getItemAdHock.isSuccess) {
            if (this.props.inventoryManagement.getItemAdHock.data.resource != null) {
                this.setState({
                    getItemAdHockData: this.props.inventoryManagement.getItemAdHock.data.resource,
                    id: this.props.adhocId,
                    prev: this.props.inventoryManagement.getItemAdHock.data.prePage,
                    current: this.props.inventoryManagement.getItemAdHock.data.currPage,
                    next: this.props.inventoryManagement.getItemAdHock.data.currPage + 1,
                    maxPage: this.props.inventoryManagement.getItemAdHock.data.maxPage
                })
            } else {
                this.setState({
                    getItemAdHockData: this.props.inventoryManagement.getItemAdHock.data.resource,
                    id: this.props.adhocId,
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })

            }
        }
    }
    componentWillReceiveProps(nextProps) {

  
        if (nextProps.inventoryManagement.getItemAdHock.isSuccess) {
            if (nextProps.inventoryManagement.getItemAdHock.data.resource != null) {
                this.setState({
                    getItemAdHockData: nextProps.inventoryManagement.getItemAdHock.data.resource,

                    id: this.props.adhocId,
                    prev: nextProps.inventoryManagement.getItemAdHock.data.prePage,
                    current: nextProps.inventoryManagement.getItemAdHock.data.currPage,
                    next: nextProps.inventoryManagement.getItemAdHock.data.currPage + 1,
                    maxPage: nextProps.inventoryManagement.getItemAdHock.data.maxPage

                })
            } else {
                this.setState({
                    getItemAdHockData: nextProps.inventoryManagement.getItemAdHock.data.resource,

                    id: this.props.adhocId,
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0

                })
            }
        }
    }

    render() {

        
        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });

        const {
            search
        } = this.state;
        return (

            <div className={this.props.adhocModalAnimation ? "modal display_block" : "display_none"} id="piopensizeModel">
                <div className={this.props.adhocModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.adhocModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.adhocModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Size">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT ITEM CODE</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-5">

                                        <div className="col-md-8 col-sm-8 pad-0 modalDropBtn">
                                            <li>
                                                <input type="search" className="search-box" onKeyPress={this._handleKeyPress} value={search} id="adhocSearch" onChange={e => this.handleChange(e)} placeholder="Type to search" />
                                                <label className="m-lft-15">
                                                    <button type="button" className="findButton" onClick={(e) => this.onSearch(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                            <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                        </svg>
                                                    </button>
                                                </label>
                                            </li>

                                        </div>
                                        <li className="float_right">
                                            <label>
                                                <button type="button" className="clearbutton" onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Article Name</th>
                                                    <th>Item Code </th>
                                                    <th>Item Name</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.getItemAdHockData != null ? this.state.getItemAdHockData.length != 0 ? this.state.getItemAdHockData.map((data, key) => (
                                                    <tr key={key}>
                                                        <td>  <label className="select_modalRadio" onChange={() => this.onSelectedData(`${data.itemCode}`)}>
                                                            <input type="radio" name="vendorMrpCheck " id={data.itemCode} checked={`${data.itemCode}` == this.state.itemValue} onChange={() => this.onSelectedData(`${data.itemCode}`)} />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.articleName}</td>
                                                        <td>{data.itemCode}</td>
                                                        <td>{data.itemName}</td>
                                                    </tr>
                                                )) : <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        {/* <li>
                                            <label className="select_modalRadio">SELECT ALL
                                            <input type="radio" name="radio" />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label className="select_modalRadio">DESELECT ALL
                                            <input type="radio" name="radio" />
                                                <span className="checkradio-select deselect_all"></span>
                                            </label>
                                        </li> */}
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onClick={() => this.onDone()}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" onClick={(e) => this.onCloseSection(e)}>Close</button>
                                            </label>
                                        </li>

                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn" type="button" disabled>
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" disabled>
                                                        Next
                  </button>
                                                </li>}


                                            {/* {this.state.prev != 0 ? <li onClick={(e) => this.page(e)} id="prev">{this.state.prev}</li> : <li />}
                <li onClick={(e) => this.page(e)} style={{ background: "blue", color: "white" }} id="current">{this.state.current}</li>
                {this.state.next - 1 != this.state.maxPage ? <li onClick={(e) => this.page(e)} id="next">{this.state.next}</li> : <li />} */}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>


        );
    }
}

export default AdhocItemCodeModal;
