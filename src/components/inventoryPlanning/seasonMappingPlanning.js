import React from 'react';
import Reload from '../../assets/reload.svg';
import Pagination from '../pagination';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import Confirm from "../loaders/arsConfirm";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import ArsFilter from "./arsFilter";

class SeasonMappingPlanning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exportToExcel: false,
            loading: true,
            confirmDelete: false,
            headerMsg: '',
            paraMsg: '',
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            
            tableHeaders: {},
            seasonPlanningData: [],
            type: 1,
            prev: '',
            current: 1,
            next: '',
            maxPage: 0,
            totalItems: 0,
            jumpPage: 1,
            toastMsg: "",
            toastLoader: false,
            selectedItems: [],
            search: "",
            sortedBy: "",
            sortedIn: "DESC",

            filter: false,
            filterBar: false,
            filterItems: {},
            appliedFilters: {}
        }
    }

    componentDidMount() {
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "PLANNING_SETTINGS_SEASON_MAPPING",
        });
        this.props.getSeasonPlanningRequest({pageNo: 1, type: 1});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getSeasonPlanning.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                success: false,
                seasonPlanningData: nextProps.seasonPlanning.getSeasonPlanning.data.resource.response,
                prev: nextProps.seasonPlanning.getSeasonPlanning.data.resource.previousPage,
                current: nextProps.seasonPlanning.getSeasonPlanning.data.resource.currPage,
                next: nextProps.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1,
                maxPage: nextProps.seasonPlanning.getSeasonPlanning.data.resource.maxPage,
                totalItems: nextProps.seasonPlanning.getSeasonPlanning.data.resource.totalCount,
                jumpPage: nextProps.seasonPlanning.getSeasonPlanning.data.resource.currPage
            });
            this.props.getSeasonPlanningClear();
        }
        else if (nextProps.seasonPlanning.getSeasonPlanning.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.seasonPlanning.getSeasonPlanning.message.status,
                errorCode: nextProps.seasonPlanning.getSeasonPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.getSeasonPlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSeasonPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.getSeasonPlanning.message.error.errorMessage,
                seasonPlanningData: [],
                prev: '',
                current: '',
                next: '',
                maxPage: '',
                jumpPage: ''
            });
            this.props.getSeasonPlanningClear();
        }

        if (nextProps.seasonPlanning.deleteSeasonPlanning.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.deleteSeasonPlanning.data.resource.message,
                error: false,
                confirmDelete: false,
                selectedItems: []
            });
            this.props.deleteSeasonPlanningClear();
        }
        else if (nextProps.seasonPlanning.deleteSeasonPlanning.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.deleteSeasonPlanning.message.status,
                errorCode: nextProps.seasonPlanning.deleteSeasonPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.deleteSeasonPlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.deleteSeasonPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.deleteSeasonPlanning.message.error.errorMessage,
                confirmDelete: false
            });
            this.props.deleteSeasonPlanningClear();
        }

        if (nextProps.seasonPlanning.downloadReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.downloadReport.data.message,
                error: false
            });
            window.location.href = nextProps.seasonPlanning.downloadReport.data.resource;
            this.props.downloadReportClear();
        }
        else if (nextProps.seasonPlanning.downloadReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.downloadReport.message.status,
                errorCode: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadReportClear();
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null) {
                this.setState({
                    loading: false,
                    error: false,
                    success: false,
                    tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
                    filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
                });
            }
            else {
                this.setState({loading: false});
            }
            this.props.getHeaderConfigClear();
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            });
            this.props.getHeaderConfigClear();
        }

        if (nextProps.seasonPlanning.getSeasonPlanning.isLoading || nextProps.seasonPlanning.deleteSeasonPlanning.isLoading || nextProps.seasonPlanning.downloadReport.isLoading || nextProps.replenishment.getHeaderConfig.isLoading) {
            this.setState({
                loading: true,
                error: false,
                success: false,
                //seasonPlanningData: []
            });
        }
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }

    xlscsv = () => {
        let data = {
            pageNo: 1,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.appliedFilters
        };
        this.props.downloadReportRequest({
            module: "SEASON_MAPPING",
            data: data
        });
        this.setState({exportToExcel: false});
    }

    createNew = () => {
        this.props.history.push('/inventoryPlanning/createSeason');
    }

    editItem = (item) => {
        console.log(item);
        this.props.history.push({
            pathname: '/inventoryPlanning/createSeason',
            state: {
                id: item.id,
                storeCode: item.storeCode,
                storeName: item.storeName,
                seasonName: item.seasonName,
                seasonStartDate: item.seasonStartDate,
                seasonEndDate: item.seasonEndDate,
                seasonDays: item.seasonDays
            }
        });
    }

    page = (e) => {
        let payload = {
            type: this.state.type,
            search: this.state.search,
            filter: this.state.appliedFilters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        };
        
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage,
                    current: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage,
                    next: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage != 0) {
                    this.props.getSeasonPlanningRequest({pageNo: this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage, ...payload});
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage,
                    current: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage,
                    next: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage
                })
            if (this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage != this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage) {
                this.props.getSeasonPlanningRequest({pageNo: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1, ...payload});
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage,
                    current: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage,
                    next: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage <= this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage) {
                    this.props.getSeasonPlanningRequest({pageNo: 1, ...payload});
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage,
                    current: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage,
                    next: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage <= this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage) {
                    this.props.getSeasonPlanningRequest({pageNo: this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage, ...payload});
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.appliedFilters,
                        sortedBy: this.state.sortedBy,
                        sortedIn: this.state.sortedIn
                    };
                    this.props.getSeasonPlanningRequest(payload);
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    deleteItems = () => {
        this.setState({
            headerMsg: "Are you sure you want to delete the selected row(s)?",
            paraMsg: "Click confirm to continue.",
            confirmDelete: true
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.appliedFilters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        };
        this.props.getSeasonPlanningRequest(payload);
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    closeConfirmDelete = () => {
        this.setState({
            confirmDelete: false,
        });
    }

    afterConfirmDelete = () => {
        this.setState({
            confirmDelete: false
        });
    }

    search = () => {
        if (this.state.search !== "") {
            this.props.getSeasonPlanningRequest({
                pageNo: 1,
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type,
                search: this.state.search,
                filter: this.state.appliedFilters,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn
            });
            this.setState({
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type
            });
        }
    }

    clearSearch = () => {
        this.props.getSeasonPlanningRequest({
            pageNo: 1,
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
            search: "",
            filter: this.state.appliedFilters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            search: "",
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type
        });
    }

    onRefresh = () => {
        this.setState({
            type: 1,
            search: "",
            appliedFilters: {},
            sortedBy: "",
            sortedIn: "DESC"
        });
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "PLANNING_SETTINGS_SEASON_MAPPING",
        });
        this.props.getSeasonPlanningRequest({pageNo: 1, type: 1});
    }

    //--------------------- FILTER FUNCTIONS BEGIN ---------------------//

    openFilter = (e) => {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        },() =>  document.addEventListener('click', this.closeFilterOnClickEvent));
    }

    closeFilterOnClickEvent = (e) => {
        if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
            this.setState({
                filter: false,
                filterBar: false 
            }, () =>
            document.removeEventListener('click', this.closeFilterOnClickEvent));
        }
    }

    closeFilter = () => {
        this.setState({
            filter: false,
            filterBar: false
        }, () =>
        document.removeEventListener('click', this.closeFilterOnClickEvent));
    }

    applyFilter = (checkedFilters, refs) => {
        let filters = {};
        Object.keys(checkedFilters).forEach((key) => {
            refs[key].current != null && refs[key].current.value != undefined && refs[key].current.value != "" ? filters[key] = refs[key].current.value : ""
        });
        this.props.getSeasonPlanningRequest({
            pageNo: 1,
            type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type,
            search: this.state.search,
            filter: filters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type,
            appliedFilters: filters
        });
        this.closeFilter();
    }

    clearFilter = () => {
        this.props.getSeasonPlanningRequest({
            pageNo: 1,
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type,
            search: this.state.search,
            filter: {},
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            appliedFilters: {},
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type
        });
        this.closeFilter();
    }

    removeFilter = (key) => {
        let filters = this.state.appliedFilters;
        delete filters[key];
        if (Object.keys(filters).length == 0) {
            this.clearFilter();
        }
        else {
            this.setState({
                appliedFilters: filters
            });
            this.props.getSeasonPlanningRequest({
                pageNo: 1,
                type: this.state.type,
                search: this.state.search,
                filter: filters,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn
            });
        }  
    }

    sortData = (key) => {
        this.props.getSeasonPlanningRequest({
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.appliedFilters,
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        });
        this.setState({
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        });
    }
    
    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <div className="ngl-search">
                                    <input type="search" placeholder="Type To Search" value={this.state.search} onChange={(e) => e.target.value == "" ? this.clearSearch() : this.setState({search: e.target.value})} onKeyDown={(e) => e.keyCode == 13 ? this.search() : e.keyCode == 27 ? this.clearSearch() : ""} />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} onClick={this.search} />
                                    {this.state.search == "" ? null : <span className="closeSearch" onClick={this.clearSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="new-trigger" onClick={this.createNew}>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="16" height="16" viewBox="0 0 19.846 19.846">
                                        <g id="Group_3035">
                                            <g id="Group_3034">
                                                <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1" />
                                            </g>
                                        </g>
                                        <g id="Group_3037" transform="translate(5.311 5.242)">
                                            <g id="Group_3036">
                                                <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)" />
                                            </g>
                                        </g>
                                    </svg>
                                    Create New
                                </button>
                                <button type="button" className={(this.state.selectedItems.length === 0) ? "pi-download2" : "pi-download"} disabled={(this.state.selectedItems.length === 0) ? "disabled" : ""} onClick={this.deleteItems} >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                        <path fill="#12203c" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                    </svg>
                                    <span class="generic-tooltip">Delete</span>
                                </button>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Excel Download</span>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                        </ul>}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {this.state.filter && <ArsFilter filterItems={this.state.filterItems} appliedFilters={this.state.appliedFilters} applyFilter={this.applyFilter} clearFilter={this.clearFilter} closeFilter={(e) => this.closeFilter(e)} />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">{
                    Object.keys(this.state.appliedFilters).length == 0 ? "" :
                    <div className="show-applied-filter">
                        <button type="button" className="saf-clear-all" onClick={this.clearFilter}>Clear All</button>
                        {
                            Object.keys(this.state.appliedFilters).map((key) =>
                                <button type="button" className="saf-btn">{this.state.filterItems[key]}
                                    <img onClick={() => this.removeFilter(key)} src={require('../../assets/clearSearch.svg')} />
                                    <span className="generic-tooltip">{key.toLowerCase().includes("date") && this.state.appliedFilters[key] !== undefined ? `From ${this.state.appliedFilters[key].from} to ${this.state.appliedFilters[key].to}` : this.state.appliedFilters[key]}</span>
                                </button>
                            )
                        }
                    </div>
                }</div>
                <div className="col-lg-12 p-lr-47">
                    <div className="expand-new-table m-top-30">
                        <div className="manage-table">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn width80">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner"><img src={require('../../assets/reload.svg')} onClick={this.onRefresh} /></li>
                                            </ul>
                                        </th>
                                        {
                                            Object.keys(this.state.tableHeaders).map((key) => <th key={key} className={this.state.sortedBy == key && this.state.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.sortData(key)}><label>{this.state.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)
                                        }
                                    </tr>
                                </thead>
                                <tbody>{
                                    Object.keys(this.state.tableHeaders).length == 0 || this.state.seasonPlanningData.length == 0 ?
                                    <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                    this.state.seasonPlanningData.map((item) =>
                                    <tr className={this.state.selectedItems.indexOf(item.id) == -1 ? "" : "vgt-tr-bg"}>
                                        <td className="fix-action-btn width80">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">{
                                                        this.state.selectedItems.indexOf(item.id) == -1 ?
                                                        <input type="checkbox" onChange={() => this.setState({selectedItems: this.state.selectedItems.concat(item.id)})} /> :
                                                        <input type="checkbox" checked onChange={() => {
                                                            let newItems = this.state.selectedItems;
                                                            newItems.splice(this.state.selectedItems.indexOf(item.id), 1);
                                                            this.setState({
                                                                selectedItems: newItems
                                                            })
                                                        }} />
                                                    }<span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn" onClick={() => this.editItem(item)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                                        <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                                    </svg>
                                                </li>
                                            </ul>
                                        </td>
                                        {
                                            Object.keys(this.state.tableHeaders).map((key) => <td><label>{item[key]}</label></td>)
                                        }
                                        </tr>
                                    )
                                }</tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.confirmDelete ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeConfirmDelete} action={this.props.deleteSeasonPlanningRequest} payload={this.state.selectedItems} afterConfirm={this.afterConfirmDelete} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default SeasonMappingPlanning; 