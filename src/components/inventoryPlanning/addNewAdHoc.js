import React from 'react';
import addMore from "../../assets/add.svg";
import deleteItems from "../../assets/remove.svg";
import FilterLoader from "../loaders/filterLoader";
import AdHocModal from './adHocModal';
import AdhocItemCodeModal from './adhocItemCodeModal';
import RequestError from '../loaders/requestError';
import RequestSuccess from '../loaders/requestSuccess';
import ToastLoader from '../loaders/toastLoader';
import { getDate, FilterData } from '../../helper';
class AddNewAdHoc extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toastLoader: false,
            toastMsg: "",
            errorBorder: false,
            siteerr: false,
            currentDate: "",
            itemCount: false,
            dateerr: false,
            adhocId: "",
            itemData: [],
            loader: true,
            liLength: "",
            scrollTop: "",
            scrollValue: [],
            pageno: 1,
            nextPage: "",
            maxPage: "",
            alert: false,
            success: false,
            errorCode: "",
            errorMessage: "",
            successMessage: "",
            code: "",
            siteData: [],
            site: "",
            date: "",
            itemCode: [],
            adhocModal: false,
            adhocModalAnimation: false,
            itemCodeData: [],
            edit: false,
            slot: "slot1",
            itemValue: "",
            item: [{ itemCode: "", quantity: "", id: 1 }],
            itemArray: {},
            dropDownShow: false,
            filteredData: ""
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        this.setState({
            currentDate: getDate()
        })
    }
    site() {
        if (
            this.state.site == "" || this.state.site == "Select Site"
        ) {
            this.setState({
                siteerr: true
            });
        } else {
            this.setState({
                siteerr: false
            });
        }
    }
    date() {
        if (
            this.state.date == "") {
            this.setState({
                dateerr: true
            });
        } else {
            this.setState({
                dateerr: false
            });
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
      if (t.which) {
        return true;
      }
    }
    }
    onClear() {
        // e.preventDefault();
        // let initial =this.state.item
        // for(var i=0;i<initial.length;i++){
        //      initial[i].itemCode="",
        //      initial[i].quantity=""
        // }
        this.setState({
            site: "",
            date: "",
            item: [
                { itemCode: "", quantity: "", id: 1 }
            ]

        })
        document.getElementById("date").placeholder = "Select Date"
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
          this.setState({
            dropDownShow: false
          })
        }
      }
    handleChange(id, e) {
        let r = this.state.item;
        if (e.target.id == "quantity") {
            for (let i = 0; i < r.length; i++) {
                if (e.target.validity.valid) {
                    if (r[i].id == id) {
                        r[i].quantity = e.target.value
                    }
                }
            }
            this.setState(
                {
                    item: r
                }
            )
        }

        if (e.target.id == "selectSite") {
            this.setState({
                dropDownShow: true,
                site: e.target.value,
                filteredData: FilterData(e.target.value, this.state.siteData)
            })
        }
    }

    onChange(e) {
        if (e.target.id == "site") {
            // this.setState(  {
            //         site: e.target.value
            //     },
            //     () => {
            //         this.site();
            //     }
            // );
        }
        else if (e.target.id == "date") {
            e.target.placeholder = e.target.value
            this.setState(
                {
                    date: e.target.value
                },
                () => {
                    this.date();
                }
            );
        }
    }

    componentWillMount() {

        this.props.getSiteAdHockRequest();

    }

    OnEdit() {
        this.setState({
            edit: true
        })
    }


    componentWillReceiveProps(nextProps) {

        if (nextProps.inventoryManagement.getSiteAdHock.isSuccess) {
            this.setState({
                siteData: nextProps.inventoryManagement.getSiteAdHock.data.resource,
                loader: false,
            })
            this.props.getSiteAdHockClear();
        } else if (nextProps.inventoryManagement.getSiteAdHock.isError) {
            this.setState({
                code: nextProps.inventoryManagement.getSiteAdHock.message.status,
                alert: true,
                loader: false,
                errorCode: nextProps.inventoryManagement.getSiteAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.getSiteAdHock.message.error.errorCode,
                errorMessage: nextProps.inventoryManagement.getSiteAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.getSiteAdHock.message.error.errorMessage,
            })
            this.props.getSiteAdHockClear();
        }
        if (nextProps.inventoryManagement.getItemAdHock.isLoading) {
            this.setState({

                loader: true,
            })
        } else if (nextProps.inventoryManagement.getItemAdHock.isSuccess) {
            this.setState({

                loader: false,
            })
            // if (nextProps.inventoryManagement.getItemAdHock.data.resource != null) {

            //     let c = []
            //     for (let i = 0; i < nextProps.inventoryManagement.getItemAdHock.data.resource.length; i++) {


            //         let x = nextProps.inventoryManagement.getItemAdHock.data.resource[i];
            //         x.checked = false;

            //         let a = x
            //         c.push(a)
            //     }
            //     this.setState({
            //         itemData: c
            //     })

            // }
        }

        if (nextProps.inventoryManagement.createAdHock.isLoading) {
            this.setState({
                loader: true
            })

        } else if (nextProps.inventoryManagement.createAdHock.isSuccess) {
            if (nextProps.inventoryManagement.createAdHock.data.resource != null) {
                this.setState({
                    successMessage: nextProps.inventoryManagement.createAdHock.data.message,
                    success: true,
                    loader: false,
                    alert: false,
                })
            }
            this.props.createAdhocRequest();
            this.onClear();
        }

        else if (nextProps.inventoryManagement.createAdHock.isError) {
            this.setState({
                errorMessage: nextProps.inventoryManagement.createAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.createAdHock.message.error.errorMessage,
                alert: true,
                success: false,
                errorCode: nextProps.inventoryManagement.createAdHock.message.error == undefined ? undefined : nextProps.inventoryManagement.createAdHock.message.error.errorCode,
                code: nextProps.inventoryManagement.createAdHock.message.status,
                loader: false,

            })
            this.props.createAdhocRequest();
            this.onClear();
        }

    }
    onAdd() {
        let initial = this.state.item;
        let len = initial.length;
        let id = initial[len - 1].id
        let flag = false
        for (var i = 0; i < initial.length; i++) {
            if (initial[i].itemCode == "") {
                flag = true

            }
            else if (initial[i].quantity == "") {
                flag = true
            } else {
                flag = false
            }
        }

        if (flag == false) {
            if (len < 5) {
                this.setState({
                    item: initial.concat({ itemCode: "", quantity: "", id: id + 1 })
                })
            }
            else {

                let itemArray = this.state.itemArray

                if (Object.keys(itemArray).length == 0) {

                    itemArray.slot1 = this.state.item

                    this.setState({
                        itemArray: itemArray,
                        itemCodeData: this.state.item,

                        item: [
                            { itemCode: "", quantity: "", id: 6 }
                        ],

                    })
                } else if (Object.keys(itemArray).length == 1) {

                    itemArray.slot2 = this.state.item

                    this.setState({
                        itemArray: itemArray,
                        itemCodeData: this.state.itemCodeData.concat(this.state.item),

                        item: [
                            { itemCode: "", quantity: "", id: 11 }
                        ],

                    })
                } else if (Object.keys(itemArray).length == 2) {

                    itemArray.slot3 = this.state.item

                    this.setState({
                        itemArray: itemArray,
                        itemCodeData: this.state.itemCodeData.concat(this.state.item),

                        item: [
                            { itemCode: "", quantity: "", id: 16 }
                        ],

                    })

                } else if (Object.keys(itemArray).length == 3) {

                    this.setState({

                        toastMsg: "You can not select more item code",
                        toastLoader: true
                    })

                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 2000)


                }


            }
        } else {
            this.setState({

                toastMsg: "First fill item code  and quantity then click add row",
                toastLoader: true
            })

            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 2000)
        }

        setTimeout(() => {
            let itemDataa = this.state.itemCodeData.concat(this.state.item)
            let flag = false
            itemDataa.forEach(function (item) {
                if (item.itemCode != "") {
                    flag = true
                } else {
                    flag = false
                }

            });
            if (flag) {
                this.setState({
                    itemCount: true
                })
            } else {
                this.setState({
                    itemCount: false
                })
            }
        }, 10)
    }
    openAdhocModal(id, itemValue) {
        let data = {
            no: 1,
            type: 1,
            search: ""
        }
        this.props.getItemAdHockRequest(data);
        this.setState({
            itemValue: itemValue,
            adhocId: id,
            adhocModal: true,
            adhocModalAnimation: !this.state.adhocModalAnimation
        })

    }

    closeAdhocModal() {
        this.setState({

            adhocModal: true,
            adhocModalAnimation: !this.state.adhocModalAnimation
        })
    }
    onDel(id) {
        let initial = this.state.item;
        if (initial.length > 1) {

            for (let i = 0; i < initial.length; i++) {
                if (initial[i].id == id) {
                    initial.splice(i, 1)
                }
            }
            this.setState({
                item: initial
            })
        }
    }
    onItemDel(id, row) {
        let itemArray = this.state.itemArray
        let itemCodeData = this.state.itemCodeData
        if (itemArray[row.data].length != 1) {
            for (var r = 0; r < itemArray[row.data].length; r++) {
                if (itemArray[row.data][r].id == id) {
                    itemArray[row.data].splice(r, 1)
                }

            }
        } else {

            if (row.data == "slot1") {
                if (itemArray["slot2"] != undefined) {
                    itemArray["slot1"] = itemArray["slot2"]
                }
                if (itemArray["slot3"] != undefined) {
                    itemArray["slot2"] = itemArray["slot3"]
                }
                if (itemArray["slot2"] == undefined && itemArray["slot3"] == undefined) {
                    delete itemArray[row.data];
                } else if (itemArray["slot3"] == undefined && itemArray["slot2"] != undefined) {
                    delete itemArray["slot2"];
                } else if (itemArray["slot3"] != undefined) {
                    delete itemArray["slot3"];
                }

            } else if (row.data == "slot2") {
                if (itemArray["slot3"] != undefined) {
                    itemArray["slot2"] = itemArray["slot3"]
                }
                if (itemArray["slot3"] == undefined && itemArray["slot2"] != undefined) {
                    delete itemArray["slot2"];
                } else if (itemArray["slot3"] != undefined) {
                    delete itemArray["slot3"];
                }


            } else if (row.data == "slot3") {


                delete itemArray["slot3"];


            }

        }
        for (var j = 0; j < itemCodeData.length; j++) {
            if (itemCodeData[j].id == id) {
                itemCodeData.splice(j, 1)
            }

        }

        this.setState({
            itemArray: itemArray,
            itemCodeData: itemCodeData
        })

    }

    quantityChange(id, row, e) {
        let itemArray = this.state.itemArray
        let itemCodeData = this.state.itemCodeData

        for (var r = 0; r < itemArray[row.data].length; r++) {
            if (itemArray[row.data][r].id == id) {
                itemArray[row.data][r].quantity = e.target.value
            }

        }

        for (var j = 0; j < itemCodeData.length; j++) {
            if (itemCodeData[j].id == id) {
                itemCodeData[j].quantity = e.target.value
            }
        }

        this.setState({
            itemArray: itemArray,
            itemCodeData: itemCodeData
        })
    }

    updateItem(data) {
        let initial = this.state.item;

        let itemCodeDatas = this.state.itemCodeData.concat(this.state.item)

        let itemCodeData = [];
        for (var i = 0; i < itemCodeDatas.length; i++) {
            itemCodeData.push(itemCodeDatas[i].itemCode)
        }
        for (var i = 0; i < initial.length; i++) {

            if (initial[i].id == data.checkedId) {

                if (!itemCodeData.includes(data.checkedData)) {
                    initial[i].itemCode = data.checkedData
                } else {

                    this.setState({
                        errorBorder: true,
                        toastMsg: "This Item code is already use ,choose another Item Code",
                        toastLoader: true
                    })

                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 2000)

                    set
                }

            }

        }
        setTimeout(() => {
            let itemDataa = this.state.itemCodeData.concat(this.state.item)
            let flag = false
            itemDataa.forEach(function (item) {
                if (item.itemCode != "") {
                    flag = true
                } else {
                    flag = false
                }

            });
            if (flag) {
                this.setState({
                    itemCount: true
                })
            } else {
                this.setState({
                    itemCount: false
                })
            }
        }, 10)
    }


    contactSubmit() {
        this.site();
        this.date();
        let initial = this.state.itemCodeData.concat(this.state.item);
        var reCollectData = [];

        initial.forEach(fetchItem => {
            //(size.code,size.cname)
            if (fetchItem.itemCode != "" && fetchItem.quantity != "") {
                let adhocData = {
                    itemCode: fetchItem.itemCode,
                    quantity: fetchItem.quantity
                }
                reCollectData.push(adhocData);
            }
        })

        let flag = false;
        for (var i = 0; i < initial.length; i++) {
            if (initial[i].itemCode != "" && initial[i].quantity != "") {
                flag = true
            }
        }
        const t = this;
        setTimeout(function (e) {
            const { siteerr, dateerr } = t.state;

            if (!siteerr && !dateerr && flag == true) {
                t.setState({
                    loader: true,
                    alert: false,
                    success: false
                })

                let Data = {

                    site: t.state.site,
                    requestedDate: t.state.date,
                    item: reCollectData,



                }
                t.props.createAdhocRequest(Data);
            }
            else if (flag == false) {
                t.setState({
                    toastMsg: "Select Item code and quantity",
                    toastLoader: true
                })

                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            }

        }, 100)


    }
    rotateIcon(id) {
        if (this.state.slot != id) {
            this.setState({
                slot: id
            })
        } else {
            this.setState({
                slot: ""
            })
        }

    }
    OnSave() {
        this.setState({
            edit: false
        })
    }
    setWrapperRef(node) {
        this.wrapperRef = node;
      }
    handleSite(site) {
        this.setState({ site :site,
            dropDownShow:false
        }, () => { this.site() })
    }

    render() {
        const { siteerr, dateerr } = this.state;
        return (
            <div className="container_div">
                <div className="col-md-12 col-sm-12 col-xs-12 addNewAdHOcMain">
                    <div className="container-fluid">
                        <div className="container_content heightAuto pad-top-0 padRightNone">
                            <div className="col-md-6 pad-top-20">
                                <div className="col-md-6 col-sm-12 pad-0">
                                    <ul className="list_style">
                                        <li>
                                            <label className="contribution_mart">
                                                AD-HOC REQUEST
                                        </label>
                                        </li>
                                        <li>
                                            <p className="master_para">Create & manage ad-hoc request</p>
                                        </li>
                                    </ul>
                                    {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                                </div>


                                {/* <form  name="adhocForm" onSubmit={(e) => this.contactSubmit((e))
                                                }
                                            >  */}
                                <div className="col-md-12 col-sm-12 adHocSiteSelect pad-0 m-top-30">

                                    <ul className="pad-0">
                                        <li>
                                            <label>Select Date</label>
                                            <input type="date" value={this.state.date} min={this.state.currentDate} id="date" onChange={(e) => this.onChange(e)} placeholder="Select Date" className={dateerr ? "errorBorder organistionFilterModal pad-right-8" : "organistionFilterModal pad-right-8"} />
                                            {dateerr ? (
                                                <span className="error">
                                                    Select Date
                                                                </span>
                                            ) : null}
                                        </li>
                                        <li ref={(e) => this.setWrapperRef(e)}>
                                            <label>Site</label>
                                            {/* <select onChange={(e) => this.onChange(e)} id="site" className={siteerr ? " errorBorder form-box displayPointer" : "form-box displayPointer"} value={this.state.site}>
                                                <option>Select Site</option>
                                                {this.state.siteData == null ? null : this.state.siteData.map((data, key) => (<option key={key} value={data.site}>{data.site}</option>))}
                                            </select>
                                            {siteerr ? (
                                                <span className="error displayBlock">
                                                    Select Site
                                                                </span>
                                            ) : null} */}
                                            <input type="search" className={siteerr ? " errorBorder form-box displayPointer" : "form-box displayPointer"} id="selectSite" value={this.state.site} placeholder="Select Site" onFocus={(e) => this.handleChange(`${"selectSite"}`, e)} onChange={(e) => this.handleChange(`${"selectSite"}`, e)} />
                                            {this.state.dropDownShow ? <div className="storeSearchDrop m-top-7 userModalSelect adHocDropDown form-box displayPointer" ><span>{this.state.filteredData != "" ? this.state.filteredData.map((data, key) => (<label key={key} onClick={() => this.handleSite(data.site)}>{data.site}</label>)) : <label>No Data Found</label>}</span></div> : ""}
                                            {siteerr ? (
                                                <span className="error displayBlock">
                                                    Select Site
                                                                </span>
                                            ) : null}
                                        </li>
                                    </ul>

                                    <div className="col-md-6">
                                    </div>
                                </div>
                                <div className="col-md-12 pad-0 m-top-15">
                                    <div className="pad-0 addItemsMain">
                                        <div className="selectItemCodeTitle">
                                            <label>Select Item code and Quantity </label>
                                            <p onClick={() => this.onAdd()} className="displayPointer">Add More <img src={addMore} /></p>
                                        </div>
                                        <div className="addMoreItemsAdHoc">
                                            {this.state.item.length > 1 ? this.state.item.map((data, i) => (<div className="addItemsDiv" key={i}> <ul>
                                                <li>

                                                    <div className="userModalSelect adHocDropDown form-box displayPointer" onClick={(e) => this.openAdhocModal(`${data.id}`, `${data.itemCode}`)}>

                                                        <label className="displayPointer">{data.itemCode == "" ? "Select Item Code" : data.itemCode}
                                                            <span >
                                                                ▾
                                                            </span>
                                                        </label>
                                                        {/* {selectederr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                        {/* <div className="dropdownDiv hiddenDiv" onScroll={()=>this.myFunction()}>

                                                                <input type="search" className="searchFilterModal" placeholder="Type to Search" />
                                                                <ul className="dropdownFilterOption" id="dpfilterOption" >
                                                                    {this.state.itemCode.length == 0 ? "No data" : this.state.itemCode.map((data,key) => (<li key={key}>
                                                                        {data.itemCode}
                                                                    </li>))}
                                                                </ul>

                                                            </div> */}

                                                    </div>

                                                </li>
                                                <li>
                                                    <input type="text" pattern="[0-9]*" value={data.quantity} className="form-box" id="quantity" placeholder="Quantity" onChange={(e) => this.handleChange(`${data.id}`, e)} />

                                                </li>
                                                <li>
                                                    <div onClick={() => this.onDel(`${data.id}`)} className="deleteAddedItem displayPointer" id={data.id}>
                                                        <img src={deleteItems} />
                                                    </div>
                                                </li>
                                            </ul></div>)) : this.state.item.map((data, j) => (<div className="addItemsDiv" key={j}> <ul >
                                                <li>

                                                    <div className="userModalSelect adHocDropDown form-box displayPointer" onClick={(e) => this.openAdhocModal(`${data.id}`, `${data.itemCode}`)}>

                                                        <label className="displayPointer">{data.itemCode == "" ? "Select Item Code" : data.itemCode}
                                                            <span >
                                                                ▾
                                                            </span>
                                                        </label>
                                                        {/* {selectederr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                                        {/* <div className="dropdownDiv hiddenDiv" onScroll={()=>this.myFunction()}>

                                                                <input type="search" className="searchFilterModal" placeholder="Type to Search" />
                                                                <ul className="dropdownFilterOption" id="dpfilterOption" >
                                                                    {this.state.itemCode.length == 0 ? "No data" : this.state.itemCode.map((data,key) => (<li key={key}>
                                                                        {data.itemCode}
                                                                    </li>))}
                                                                </ul>

                                                            </div> */}

                                                    </div>

                                                </li>
                                                <li>
                                                    <input type="text" pattern="[0-9]*" value={data.quantity} className="form-box" id="quantity" placeholder="Quantity" onChange={(e) => this.handleChange(`${data.id}`, e)} />

                                                </li>

                                            </ul></div>))}

                                        </div>


                                    </div>
                                    {/* <div className="col-md-7"></div> */}
                                </div>

                            </div>
                            {Object.keys(this.state.itemArray).length != 0 ? <div className="col-md-5 col-md-offset-1 adHocAccordion pad-top-20">

                                <div className="col-md-6 pad-lft-0">
                                    <label className="contribution_mart">
                                        AD-HOC ITEM LISTS
                                            </label>
                                </div>
                                <div className="col-md-6">
                                    <label className="totalItemAdd contribution_mart">
                                        Total Item’s Added <span>{this.state.itemCount ? this.state.itemCodeData.concat(this.state.item).length : this.state.itemCodeData.concat(this.state.item).length - 1}</span>
                                    </label>
                                </div>

                                <div className="accordionItems">
                                    <div className="bs-example">
                                        <div className="panel-group" id="accordion">
                                            {Object.keys(this.state.itemArray).map((data, key) => (


                                                <div className="panel panel-default" key={key}>
                                                    <div className="panel-heading">

                                                        <h4 className="panel-title">
                                                            {data}
                                                        </h4>
                                                        <a data-toggle="collapse" data-parent="#accordion" href={"#collapseOne" + data}>
                                                            <div onClick={(e) => this.rotateIcon(data)} id={"rotate" + data} className={this.state.slot == data ? "glyphicon glyphicon-chevron-up" : "glyphicon glyphicon-chevron-down"}></div></a>

                                                    </div>
                                                    {this.state.slot == data ? <div id={"collapseOne" + data} className="panel-collapse collapse in">
                                                        <div className="panel-body">
                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th>Item Code</th>
                                                                        <th>Quantity</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.itemArray[data].map((Adata, Akey) => (
                                                                        <tr key={Akey}>
                                                                            <td>{Adata.itemCode}</td>
                                                                            <td><label>{this.state.edit ? <input type="text" pattern="[0-9]*" value={Adata.quantity} onChange={(e) => this.quantityChange(`${Adata.id}`, { data }, e)} /> : Adata.quantity}</label><img src={deleteItems} onClick={() => this.onItemDel(`${Adata.id}`, { data })} id={Adata.id} /> </td>

                                                                        </tr>
                                                                    ))}
                                                                </tbody>
                                                            </table>


                                                            {this.state.edit == false ? <button type="button" onClick={(e) => this.OnEdit(e)}>Edit</button> : <button type="button" onClick={(e) => this.OnSave(e)}>Ok</button>}
                                                        </div>
                                                    </div> : null}
                                                </div>))}



                                        </div>
                                    </div>
                                </div>

                            </div> : null}
                            <div className="footerDivForm height4per m-top-100">
                                <ul className="list-inline m-lft-0 m-top-10">
                                    <li><button className="clear_button_vendor" type="button" onClick={(e) => this.onClear(e)}>Clear</button>
                                    </li>
                                    <li><button type="button" className="save_button_vendor" onClick={(e) => this.contactSubmit(e)} >Save</button></li>
                                </ul>
                            </div>
                            {/* </form> */}
                        </div>

                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.userFilter ? <UserFilter updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} filter={(e) => this.onFilter(e)} /> : null}
                {this.state.userModal ? <UserModal summaryModalAnimation={this.state.summaryModalAnimation} modal={(e) => this.onModal(e)} /> : null}
                {this.state.userEditModal ? <UserEditModal {...this.state} {...this.props} userId={this.state.userId} userModalAnimation={this.state.userModalAnimation} userEditModal={(e) => this.onUserEditModal(e)} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.adhocModal ? <AdhocItemCodeModal {...this.props} {...this.state} itemValue={this.state.itemValue} closeAdhocModal={(e) => this.closeAdhocModal(e)} updateItem={(e) => this.updateItem(e)} adhocModalAnimation={this.state.adhocModalAnimation} adhocId={this.state.adhocId} closeModal={(e) => this.openAdhocModal(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>

        );
    }
}

export default AddNewAdHoc;