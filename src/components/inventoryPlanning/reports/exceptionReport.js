import React from 'react';
import Pagination from '../../pagination';

class Exceptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showFilters: false,
            exportToExcel: false,
        }
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            showFilters: !this.state.showFilters
        });
    }

    render () {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="otb-reports-filter p-lr-47">
                        <div className="orf-head">
                            <div className="orfh-left">
                                <div className="orfhl-inner">
                                    <h3 className={this.state.showFilters === false ? "h3After" : ""}>Filters</h3>
                                    {/* {this.state.showFilters === false ? <span>{Object.keys(this.state.filters).length} filters applied</span>: null} */}
                                </div>
                                {this.state.showFilters === false && <button type="button" onClick={this.clearFilters}>Remove</button>}
                            </div>
                            <div className="orfh-rgt">
                                <button className={this.state.showFilters === false ? "" : "orfh-btn"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.806" height="8.403" viewBox="0 0 14.806 8.403">
                                        <path id="Path_943" fill="none" stroke="#3a5074" strokeLinecap="round" strokeLinecap="round" strokeWidth="2px" d="M6 9l5.989 5.989L17.978 9" data-name="Path 943" transform="translate(-4.586 -7.586)"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        {this.state.showFilters && <div className="orf-body m-top-10">
                            <form>
                                <div className="col-lg-12 pad-0">
                                    <div className="col-lg-2 pad-lft-0">
                                        <label className="pnl-purchase-label">Store Name</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" placeholder="Search " />
                                            <span className="modal-search-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-lft-0">
                                        <label className="pnl-purchase-label">Division</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" placeholder="Search " />
                                            <span className="modal-search-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-lft-0">
                                        <label className="pnl-purchase-label">Product Category</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" placeholder="Search " />
                                            <span className="modal-search-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-lft-0">
                                        <label className="pnl-purchase-label">Zone</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" placeholder="Search " />
                                            <span className="modal-search-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 pad-lft-0">
                                        <label className="pnl-purchase-label">Inv Status</label>
                                        <div className="inputTextKeyFucMain">
                                            <input type="text" className="onFocus pnl-purchase-input" placeholder="Search " />
                                            <span className="modal-search-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                    <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    <div className="orfb-submit-btn">
                                        <button type="button" className="orfbab-submit">Submit</button>
                                        <button type="button" className="">Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>}
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="otb-reports-content">
                        <div className="otbrc-left">
                            <div className="otbrc-search">
                                <input type="search" placeholder="Type To Search" />
                                <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span>
                            </div>
                        </div>
                        <div className="otbrc-right">
                            <div className="gvpd-filter">
                                <button type="button" className="gvpd-filter-inner">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                    <span className="generic-tooltip">Filter</span>
                                </button>
                                {/* {this.state.filter && <VendorFilter />} */}
                            </div>
                            <button type="button" className="otbrc-email">Email
                                <span>
                                    <img src={require('../../../assets/email.svg')} />
                                </span>
                            </button>
                            <div className="gvpd-download-drop">
                                <button type="button" className="otbrc-down-report">Download Report
                                    <span className="otbrcdr-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table vgtbg-fcfdfe" >
                        <div className="manage-table">
                            <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th><label>Store Code</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Store Name</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Barcode</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Zone</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>City Class</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Article Name</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Product Category</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Total Stock</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Lead Days</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Final MBQ</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Total Sales</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Sales Per Day</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>STR</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>PLC</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Days Stock At Store</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>MRP</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Final Requirement</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Inv Status</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Excess Days</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Excess Inv </label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Excess Cogs Value</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Lost Days</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Less Inv</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        <th><label>Lost Value</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                    <tr>
                                        <td><label>mfn0252</label></td>
                                        <td><label>RUCHI LIFESTYLE PVT LTD - MOTERA	</label></td>
                                        <td><label>883646301386</label></td>
                                        <td><label>west</label></td>
                                        <td><label>Class 4</label></td>
                                        <td><label>65093-blk</label></td>
                                        <td><label>SLIPPER</label></td>
                                        <td><label>0</label></td>
                                        <td><label>11</label></td>
                                        <td><label>1</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>45</label></td>
                                        <td><label>0</label></td>
                                        <td><label>3799</label></td>
                                        <td><label>1</label></td>
                                        <td><label>No Sale, Replenish, Stock Less than MBQ	</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                        <td><label>0</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value="01" />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">10</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Exceptions;