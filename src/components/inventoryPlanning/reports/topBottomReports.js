import React from 'react';
import Reload from '../../../assets/reload.svg';
import Pagination from '../../pagination';
import ToastLoader from '../../loaders/toastLoader';
import FilterLoader from "../../loaders/filterLoader";
import Confirm from "../../loaders/arsConfirm";
import RequestError from "../../loaders/requestError";
import RequestSuccess from "../../loaders/requestSuccess";
import GenericDataModal from '../../replenishment/genericDataModal';
import InventorySetting from '../inventorySettingModal';
import moment from "moment";
import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;

class SellThruPerformance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showFilters: false,
            exportToExcel: false,
            topBottom: false,
            topBottomValue: "Top Styles",
            sellThru: false,
            sellThruValue: "Sales",
            loading: true,
            confirmDelete: false,
            headerMsg: '',
            paraMsg: '',
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",

            type: 1,
            prev: '',
            current: 1,
            next: '',
            maxPage: 0,
            totalItems: 0,
            jumpPage: 1,
            toastMsg: "",
            toastLoader: false,

            headers: [],
            data: [],

            genericDataModalShow: false,
            genericDataModalShowRow: {},
            genericSearchData: {},
            genericSearchRefs: {},
            selectedItems: {},
            filters: {},
            date: {},
            search: "",
            sortedBy: "sales_qty",
            sortedIn: "DESC",

            //CONFIGURATION
            settingsValues: [],
            selectedSetting: {},
            openSetting: '',
            modalHeading: ""
        }
    }

componentDidMount() {
        this.props.getSellThruPerformanceReportRequest({pageNo: 1, type: 1, sortedBy: "sales_qty", sortedIn: "DESC"});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getSellThruPerformanceReport.isSuccess) {
            if (nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource !== null) {
                this.setState({
                    loading: false,
    
                    prev: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage,
                    current: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage,
                    next: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage + 1,
                    maxPage: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage,
                    totalItems: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.totalCount,
                    jumpPage: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage,
    
                    headers: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.headers === undefined || nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.headers === null ? [] : Object.keys(nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.headers).length == 0 ? [] : JSON.parse(nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.headers),
                    date: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.filterDates === undefined || nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.filterDates === null ? {} : {fromDate: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.filterDates.fromDate, toDate: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.filterDates.toDate},
                    data: nextProps.seasonPlanning.getSellThruPerformanceReport.data.resource.data
                });
            }
            this.props.getSellThruPerformanceReportClear();
        }
        else if (nextProps.seasonPlanning.getSellThruPerformanceReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSellThruPerformanceReport.message.status,
                errorCode: nextProps.seasonPlanning.getSellThruPerformanceReport.message.error == undefined ? undefined : nextProps.seasonPlanning.getSellThruPerformanceReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSellThruPerformanceReport.message.error == undefined ? undefined : nextProps.seasonPlanning.getSellThruPerformanceReport.message.error.errorMessage,

                prev: '',
                current: '',
                next: '',
                maxPage: '',
                jumpPage: '',

                headers: [],
                data: []
            }, this.props.getSellThruPerformanceReportClear);
        }

        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loading: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
            this.props.getArsGenericFiltersClear();
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getArsGenericFiltersClear();
        }

        // if (nextProps.seasonPlanning.updateInventoryPlanningReport.isSuccess) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: false
        //     });
        //     this.props.updateInventoryPlanningReportClear();
        //     let payload = {
        //         pageNo: this.state.current,
        //         type: this.state.type,
        //         search: this.state.search,
        //         filter: this.state.filters,
        //         sortedBy: this.state.sortedBy,
        //         sortedIn: this.state.sortedIn
        //     }
        //     this.props.getSellThruPerformanceReportRequest(payload);
        // }
        // else if (nextProps.seasonPlanning.updateInventoryPlanningReport.isError) {
        //     this.setState({
        //         loading: false,
        //         success: false,
        //         error: true,
        //         code: nextProps.seasonPlanning.updateInventoryPlanningReport.message.status,
        //         errorCode: nextProps.seasonPlanning.updateInventoryPlanningReport.message.error == undefined ? undefined : nextProps.seasonPlanning.updateInventoryPlanningReport.message.error.errorCode,
        //         errorMessage: nextProps.seasonPlanning.updateInventoryPlanningReport.message.error == undefined ? undefined : nextProps.seasonPlanning.updateInventoryPlanningReport.message.error.errorMessage,
        //         //confirmDelete: false
        //     });
        //     this.props.updateInventoryPlanningReportClear();
        // }

        if (nextProps.seasonPlanning.downloadReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.downloadReport.data.message,
                error: false
            });
            window.location.href = nextProps.seasonPlanning.downloadReport.data.resource;
            this.props.downloadReportClear();
        }
        else if (nextProps.seasonPlanning.downloadReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.downloadReport.message.status,
                errorCode: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadReportClear();
        }

        if(nextProps.seasonPlanning.saveSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.data.resource.message,
                error: false
            });
            this.props.getSellThruPerformanceReportRequest({
                pageNo: this.state.current,
                type: this.state.type,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn,
                search: this.state.search,
                filter: this.state.filters
            });
            this.props.saveSiteAndItemFilterClear();
        }
        else if(nextProps.seasonPlanning.saveSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorMessage
            });
            this.props.saveSiteAndItemFilterClear();
        }

        if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                settingsValues: Object.keys(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource) == 0 ? {} : nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter),
                //settingModal: true
            }, () => this.props.getSiteAndItemFilterConfigurationRequest(this.state.openSetting));
            this.props.getSiteAndItemFilterClear();
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorMessage
            });
            this.props.getSiteAndItemFilterClear();
        }

        if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess) {
            this.setState({
                loading: false,
                selectedSetting: Object.keys(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter) == 0 ? {} : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter)
            }, () => this.setState({settingModal: true}));
            this.props.getSiteAndItemFilterConfigurationClear();
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorMessage
            });
            this.props.getSiteAndItemFilterConfigurationClear();
        }

        if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.data.resource.message,
                error: false
            });
            this.props.getSellThruPerformanceReportRequest({
                pageNo: this.state.current,
                type: this.state.type,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn,
                search: this.state.search,
                filter: this.state.filters
            });
            this.props.saveSiteAndItemFilterClear();
        }
        else if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorMessage
            });
            this.props.saveSiteAndItemFilterClear();
        }

        if (nextProps.seasonPlanning.getSellThruPerformanceReport.isLoading || nextProps.seasonPlanning.getArsGenericFilters.isLoading || nextProps.seasonPlanning.updateInventoryPlanningReport.isLoading || nextProps.seasonPlanning.downloadReport.isLoading || nextProps.seasonPlanning.saveSiteAndItemFilter.isLoading || nextProps.seasonPlanning.getSiteAndItemFilter.isLoading || nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isLoading) {
            this.setState({
                loading: true
            });
        }
    }

    openTopBottom (e) {
        e.preventDefault ();
        this.setState ({
            topBottom: !this.state.topBottom
        }, () => document.addEventListener('click', this.closeTopBottom));
    }
    closeTopBottom = () => {
        this.setState({ topBottom: false }, () => {
            document.removeEventListener('click', this.closeTopBottom);
        });
    }

    openSellThru (e) {
        e.preventDefault ();
        this.setState ({
            sellThru: !this.state.sellThru
        }, () => document.addEventListener('click', this.closeSellThru));
    }
    closeSellThru = () => {
        this.setState({ sellThru: false }, () => {
            document.removeEventListener('click', this.closeSellThru);
        });
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            showFilters: !this.state.showFilters
        });
    }

    page = (e) => {
        let payload = {
            type: this.state.type,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn,
            search: this.state.search,
            filter: this.state.filters
        };

        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage != 0) {
                    this.props.getSellThruPerformanceReportRequest({pageNo: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage, ...payload});
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage
                })
            if (this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage != this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage) {
                this.props.getSellThruPerformanceReportRequest({pageNo: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage + 1, ...payload});
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage <= this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage) {
                    this.props.getSellThruPerformanceReportRequest({pageNo: 1, ...payload});
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.currPage <= this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage) {
                    this.props.getSellThruPerformanceReportRequest({pageNo: this.props.seasonPlanning.getSellThruPerformanceReport.data.resource.maxPage, ...payload});
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: this.state.type,
                        sortedBy: this.state.sortedBy,
                        sortedIn: this.state.sortedIn,
                        search: this.state.search,
                        filter: this.state.filters
                    }
                    this.props.getSellThruPerformanceReportRequest(payload);
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    siteData = (e, entity, key, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, search, pageNo) => {
        if (this.state.genericDataModalShowRow != key) this.closeGenericDataModal();
        if (key === "name1") {
            this.props.getArsGenericFiltersRequest({
                entity: entity,
                key: "name1",
                code: "site_code",
                search: search,
                pageNo: pageNo
            });
        }
        else {
            this.props.getArsGenericFiltersRequest({
                entity: entity,
                key: key,
                search: search,
                pageNo: pageNo
            });
        }
        this.setState({genericSearchData: {payload: {entity: entity, key: key, search: search}}, genericDataModalShowRow: key});
    }

    closeGenericDataModal = () => {
        let newRefs = this.state.genericSearchRefs;
        if (newRefs[this.state.genericDataModalShowRow] !== undefined) {
            newRefs[this.state.genericDataModalShowRow].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                genericSearchRefs: newRefs
            });
        }
    }

    selectItems = (entity, key, items) => {
        let updateItems = {...this.state.selectedItems};
        if (items.length !== 0) {
            updateItems[key] = items;
        }
        else if (items.length === 0 && updateItems[key] !== undefined) {
            delete updateItems[key];
        }
        this.setState({
            selectedItems: updateItems
        });
    }

    submitFilters = () => {
        let filters = {};
        if(Object.keys(this.state.selectedItems).length != 0) {
            Object.keys(this.state.selectedItems).forEach((key) => {
                if(this.state.selectedItems[key].length != 0) filters[key] = this.state.selectedItems[key].join();
            });
        }
        this.props.getSellThruPerformanceReportRequest({
            search: this.state.search,
            filter: filters,
            pageNo: 1,
            type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type, filters: filters});
    }

    clearFilters = () => {
        this.props.getSellThruPerformanceReportRequest({
            pageNo: 1,
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type,
            filter: {},
            search: this.state.search,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            // fromDate: "",
            // toDate: "",
            selectedItems: {},
            filters: {},
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type
        });
    }

    search = () => {
        if (this.state.search !== "") {
            this.props.getSellThruPerformanceReportRequest({
                pageNo: 1,
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type,
                search: this.state.search,
                filter: this.state.filters,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn
            });
            this.setState({
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type
            });
        }
    }

    clearSearch = () => {
        this.props.getSellThruPerformanceReportRequest({
            pageNo: 1,
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
            search: "",
            filter: this.state.filters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            search: "",
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
        this.closeSetting();
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    downloadReport = () => {
        this.props.downloadReportRequest({
            module: "SELL_THRU_PERFORMANCE_MAPPING",
            data: {
                pageNo: 1,
                type: this.state.type,
                search: this.state.search,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn,
                filter: this.state.filters,
                filterDate: Object.keys(this.state.date).length === 0 ? {fromDate: "", toDate: ""} : this.state.date
            }
        });
    }

    sortData = (key) => {
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.filters,
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        };
        this.props.getSellThruPerformanceReportRequest(payload);
        this.setState({
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        });
    }

    onReload = () => {
        this.props.getSellThruPerformanceReportRequest({pageNo: 1, type: 1});
        this.setState({
            type: 1,
            search: "",
            // fromDate: "",
            // toDate: "",
            selectedItems: {},
            filters: {},
            sortedBy: "",
            sortedIn: "DESC"
        });
    }

    handleDateInput = (e) => {
        var date = {};
        if (e !== null) {
            date = {
                fromDate: moment(e[0]._d).format('YYYY-MM-DD'),
                toDate: moment(e[1]._d).format('YYYY-MM-DD'),
            }
        }
        this.setState({
            date,
        });
    }

    generateReport = () => {
        this.props.saveSiteAndItemFilterRequest({
            filterType: "sellThruPerformance",
            date: {
                fromDate: this.state.date.fromDate,
                toDate: this.state.date.toDate
            }
        });
    }

    handleTopBottom = (value) => {
        this.setState({
            topBottomValue: value,
            sortedBy: this.state.sellThruValue === "Sales" ? "sales_qty" : "sell_trhu",
            sortedIn: value === "Top Styles" ? "DESC" : "ASC"
        });
        this.props.getSellThruPerformanceReportRequest({
            pageNo: 1,
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
            search: this.state.search,
            filter: this.state.filters,
            sortedBy: this.state.sellThruValue === "Sales" ? "sales_qty" : "sell_trhu",
            sortedIn: value === "Top Styles" ? "DESC" : "ASC"
        });
        // else if (sortedBy === "Bottom Styles" && this.state.sellThruValue === "Sales") {
        //     this.setState({
        //         sortedBy: "sales_qty",
        //         sortedIn: this.state.sellThruValue === "Sales" ? "DESC" : "ASC"
        //     });
        //     this.props.getSellThruPerformanceReportRequest({
        //         pageNo: 1,
        //         type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
        //         search: this.state.search,
        //         filter: this.state.filters,
        //         sortedBy: "sales_qty",
        //         sortedIn: this.state.sellThruValue === "Sales" ? "DESC" : "ASC"
        //     });
        // }
    }

    handleSellThru = (value) => {
        this.setState({
            sellThruValue: value,
            sortedBy: value === "Sales" ? "sales_qty" : "sell_trhu",
            sortedIn: this.state.topBottomValue === "Top Styles" ? "DESC" : "ASC"
        });
        this.props.getSellThruPerformanceReportRequest({
            pageNo: 1,
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
            search: this.state.search,
            filter: this.state.filters,
            sortedBy: value === "Sales" ? "sales_qty" : "sell_trhu",
            sortedIn: this.state.topBottomValue === "Top Styles" ? "DESC" : "ASC"
        });
    }

    openSetting(e, type, heading) {
        e.preventDefault();
        this.setState({
            openSetting: type,
            modalHeading: heading,
            settingValues: [],
            selectedSetting: {}
        }, () => {
            type == "site" ? this.props.getSiteAndItemFilterRequest("site") : type == "basestock" ? this.props.getSiteAndItemFilterRequest("basestock") : this.props.getSiteAndItemFilterRequest("item");
            //this.props.getSiteAndItemFilterConfigurationRequest(type);
        });
    }

    closeSetting = () => {
        this.setState({
            settingModal: false,
            openSetting: ''
        });
    }

    setSubmit = () => {
        this.setState({submit: true});
    }

    render () {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="otb-reports-filter p-lr-47">
                        <div className="orf-head">
                            <div className="orfh-left">
                                <div className="orfhl-inner">
                                    <h3 className={Object.keys(this.state.filters).length !== 0 && this.state.showFilters === false ? "h3After" : ""}>Filters</h3>
                                    {/* {this.state.showFilters === false ? <span>{Object.keys(this.state.filters).length} filters applied</span>: null} */}
                                </div>
                                {this.state.showFilters === false && Object.keys(this.state.filters).length !== 0 ? <button type="button" onClick={this.clearFilters}>Remove</button>: null}
                            </div>
                            <div className="orfh-rgt">
                                <button className={this.state.showFilters === false ? "" : "orfh-btn"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.806" height="8.403" viewBox="0 0 14.806 8.403">
                                        <path id="Path_943" fill="none" stroke="#3a5074" strokeLinecap="round" strokeLinecap="round" strokeWidth="2px" d="M6 9l5.989 5.989L17.978 9" data-name="Path 943" transform="translate(-4.586 -7.586)"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        {this.state.showFilters && <div className="orf-body m-top-10">
                            <form>
                                <div className="col-lg-5 pad-0">
                                    <h3>Sales Date Range</h3>
                                    <div className="col-lg-5 pad-0 pad-r15">
                                        <RangePicker
                                            format="YYYY-MM-DD"
                                            defaultValue={Object.keys(this.state.date).length === 0 ? null : [moment(this.state.date.fromDate), moment(this.state.date.toDate)]}
                                            onChange={(e) => this.handleDateInput(e)}
                                        />
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <div className="orfb-submit-btn">
                                            <button type="button" className={Object.keys(this.state.date).length === 0 ? "orfbab-submit btnDisabled" : "orfbab-submit"} disabled={Object.keys(this.state.date).length === 0 ? "disabled" : ""} onClick={this.generateReport}>Generate</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 pad-0">
                                {
                                    this.state.headers.map((item) => {
                                        item.isFilter == "Y" ? this.state.genericSearchRefs[item.key] = React.createRef() : "";
                                        return (
                                            item.isFilter == "Y" ?
                                            <div className="col-lg-2 pad-lft-0 m-top-20">
                                                <label className="pnl-purchase-label">{item.value}</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input"
                                                        id={item.key + "Focus"}
                                                        placeholder={this.state.selectedItems[item.key] === undefined || this.state.selectedItems[item.key].length === 0 ? "No " + item.value + " selected" : this.state.selectedItems[item.key].length === 1 ? this.state.selectedItems[item.key][0] + " selected" : this.state.selectedItems[item.key][0] + " + " + (this.state.selectedItems[item.key].length - 1) + " selected"}
                                                        ref={this.state.genericSearchRefs[item.key]}
                                                        onFocus={(e) => {if (this.state.genericDataModalShow === false || this.state.genericDataModalShowRow != item.key) this.openGenericDataModal(item.entity, item.key, e.target.value, 1)}}
                                                        onKeyDown={(e) => this.siteData(e, item.entity, item.key, e.target.value, 1)}
                                                    />
                                                    <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal(item.entity, item.key, this.state.genericSearchRefs[item.key].current.value, 1)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {this.state.genericDataModalShow && this.state.genericDataModalShowRow == item.key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems[item.key]} /> : null}
                                                </div>
                                            </div> :
                                            null
                                        )
                                    })
                                }
                                </div>
                                <div className="col-lg-12 pad-0 m-top-30">
                                    <div className="orfb-submit-btn">
                                        <button type="button" className={Object.keys(this.state.selectedItems).length == 0 ? "orfbab-submit btnDisabled" : "orfbab-submit"} disabled={Object.keys(this.state.selectedItems).length == 0 ? "disabled" : ""} onClick={this.submitFilters}>Submit</button>
                                        <button type="button" className="" onClick={this.clearFilters}>Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>}
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="otb-reports-content">
                        <div className="otbrc-left">
                            <div className="otbrc-search">
                                <input type="search" placeholder="Type To Search" value={this.state.search} onChange={(e) => e.target.value == "" ? this.clearSearch() : this.setState({search: e.target.value})} onKeyDown={(e) => e.keyCode == 13 ? this.search() : e.keyCode == 27 ? this.clearSearch() : ""} />
                                <img className="search-image" src={require('../../../assets/searchicon.svg')} onClick={this.search} />
                                {this.state.search == "" ? null : <span className="closeSearch" onClick={this.clearSearch}><img src={require('../../../assets/clearSearch.svg')} /></span>}
                            </div>
                        </div>
                        <div className="otbrc-right">
                            <div className="sales-through">
                                <button type="button" className="" onClick={(e) => this.openTopBottom(e)}>{this.state.topBottomValue}</button>
                                {this.state.topBottom && <div className="sathrst-dropdown">
                                    <ul className="sathrstd-inner">
                                        <li onClick={() => this.handleTopBottom("Top Styles")}>Top Styles</li>
                                        <li onClick={() => this.handleTopBottom("Bottom Styles")}>Bottom Styles</li>
                                    </ul>
                                </div>}
                            </div>
                            <div className="sales-through">
                                <button type="button" className="" onClick={(e) => this.openSellThru(e)}>{this.state.sellThruValue}</button>
                                {this.state.sellThru &&
                                <div className="sathrst-dropdown">
                                    <ul className="sathrstd-inner">
                                        <li onClick={() => this.handleSellThru("Sales")}>Sales</li>
                                        <li onClick={() => this.handleSellThru("Sell Thru")}>Sell Thru</li>
                                    </ul>
                                </div>}
                            </div>
                            <div className="gvpd-filter">
                                <button type="button" className="gvpd-filter-inner">
                                    <img src={Reload} onClick={this.onReload} />
                                    <span className="generic-tooltip">Refresh</span>
                                </button>
                                {this.state.filter && <VendorFilter />}
                            </div>
                            <button type="button" className="otbrc-email">Email
                                <span>
                                    <img src={require('../../../assets/email.svg')} />
                                </span>
                            </button>
                            <div className="gvpd-download-drop">
                                <button type="button" className="otbrc-down-report" onClick={this.downloadReport}>Download Report
                                    <span className="otbrcdr-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table vgtbg-fcfdfe" >
                        <div className="manage-table">
                            <div className="columnFilterGeneric" onClick={(e) => this.openSetting(e, "sellThruPerformance", "Sell Thru Performance Mapping")}>
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                    {
                                        this.state.headers.length != 0 ?
                                        this.state.headers.map((item) => (
                                            <th key={item.key} className={this.state.sortedBy == item.key && this.state.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.sortData(item.key)}><label>{item.value}</label><img src={require('../../../assets/headerFilter.svg')}/></th>
                                        )) :
                                        <th></th>
                                    }  
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.data.length == 0 ?
                                    <tr style={{textAlign: "center"}}><td><label>NO DATA FOUND!</label></td></tr> :
                                    this.state.data.map((item, index) => (
                                        <tr key={index}>
                                        {
                                            this.state.headers.map((innerItem) => <td><label>{item[innerItem.key]}</label></td>)
                                        }
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.settingModal && <InventorySetting closeSetting={this.closeSetting} data={this.state.settingsValues} selectedData={this.state.selectedSetting} save={this.props.saveSiteAndItemFilterRequest} type={this.state.openSetting} heading={this.state.modalHeading} setSubmit={this.setSubmit} />}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default SellThruPerformance;