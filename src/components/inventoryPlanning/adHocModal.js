import React from 'react';
import errorIcon from "../../assets/error_icon.svg";
import deleteIconModal from "../../assets/remove.svg";
import AdhocConfirmModal from '../loaders/adhocConfirmModal';


class AdHocModal extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            adhocModalData:[],
            confirmModal:false,
            headerMsg:"",
            paraMsg:"",
            itemCode:""
        };
      }
componentWillMount(){
    this.setState({
        adhocModalData:this.props.adhocModalData
    })
}
componentWillReceiveProps(){

   this.setState({
        adhocModalData:this.props.adhocModalData
    })
}
onItemDel(itemCode){
    var adhoc = this.props.adhocModalData
    
    let count = 0 
    for(var i = 0; i < adhoc.length; ++i){
        if(adhoc[i].status == "Active"){
            count++;
        }
    }
    if(count == 1 || this.props.adhocModalData.length==1){
        this.setState({
            confirmModal:!this.state.confirmModal,
            headerMsg:"Are you sure to revoke the Item Code?",
            paraMsg:"Revoking the Item Code will revoke the Work Order. Click confirm to continue.",
            itemCode:itemCode
        })
    }else{
        this.setState({
            confirmModal:!this.state.confirmModal,
            headerMsg:"Are you sure to revoke the Item Code?",
            paraMsg:"",
            itemCode:itemCode
        })
    }
}
closeConfirmModal(e){
    this.setState({
        confirmModal:!this.state.confirmModal,
    })
}

    render(){
  
        $("body").on("keyup",".numbersOnly",function(){
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
               this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
   
        return(
            <div className="modal  display_block" id="editVendorModal">
            <div className="backdrop display_block"></div>
     
            <div className=" display_block">
            <div className="modal-content vendorEditModalContent modalShow adHocModal adHocRequestModal">
           
                <div className="col-md-12 col-sm-12 pad-0">
                    <div className="col-md-7 mainAdHoc"> 
                        <ul className="list_style">
                            <li><label className="contribution_mart">Ad-Hoc Request Details</label>
                            
                            </li>

                        </ul>
                    </div>
                    
                </div>
                <div className="col-md-12 col-sm-12 m-top-10">
                    <div className="adHocModalTable">
                       <table>
                            <thead>
                                <th>Item Code</th>
                                <th>Quantity</th> 
                                <th>Action</th>                               
                            </thead>
                            
                            <tbody>
                            {this.state.adhocModalData.map((data,key) => ( <tr  key={key}>
                                    <td>{data.itemData}</td>
                                    <td>{data.quantity}</td>
                                    <td className="alignMiddle">{data.status=="Active"?<img src={deleteIconModal}  onClick={(e)=>this.onItemDel(`${data.itemData}`)}/>:data.status}</td>
                                </tr>
                            ))}
                            </tbody>
                        </table> 
                    </div>
                </div>

                <div className="adHocBottom">
                    <button type="button" onClick={(e)=>this.props.closeAdhocModal(e)}>CLOSE</button>
                </div>
               
                
            </div>
            
      
        </div>
        {this.state.confirmModal?<AdhocConfirmModal {...this.props} {...this.state} itemCode={this.state.itemCode} headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirmModal={(e)=>this.closeConfirmModal(e)} />:null}
        </div>
        )
    }
}

export default AdHocModal;