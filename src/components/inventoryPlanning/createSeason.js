import React from 'react';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import Confirm from '../loaders/arsConfirm';

class CreateSeasonMappingPlanning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            toastLoader: false,
            toastMsg: "",
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',

            sitePlanningFilters: [],
            newSeason: [{//FOR EDIT
                id: this.props.location.state == undefined ? 0 : this.props.location.state.id,
                storeCode: this.props.location.state == undefined ? '' : this.props.location.state.storeCode,
                storeName: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.storeName,
                seasonName: this.props.location.state == undefined ? 'Select Option' : this.props.location.state.seasonName,
                seasonStartDate: this.props.location.state == undefined ? '' : this.props.location.state.seasonStartDate,
                seasonEndDate: this.props.location.state == undefined ? '' : this.props.location.state.seasonEndDate,
                seasonDays: this.props.location.state == undefined ? '' : this.props.location.state.seasonDays
            }],
            errorSite: [false],
            errorSeasonName: [false],
            errorSeasonStartDate: [false],
            errorSeasonEndDate: [false],
            errorSeasonDays: [false],
            siteName: "",
            seasonName: "",
            searchSite: ""
        }
    }

    openSiteName = (e, index) => {
        e.preventDefault();
        this.setState({
            siteName: index
        }, () => document.addEventListener('click', this.closeSiteName));
    }
    closeSiteName = (e) => {
        if (e === undefined || (document.getElementById("siteNames") != null && !document.getElementById("siteNames").contains(e.target))) {
            this.setState({ siteName: "", searchSite: "" }, () => {
                document.removeEventListener('click', this.closeSiteName);
            });
        }
    }

    openSeasonName = (e, index) => {
        e.preventDefault();
        this.setState({
            seasonName: index
        }, () => document.addEventListener('click', this.closeSeasonName));
    }
    closeSeasonName = (e) => {
        if (e === undefined || (document.getElementById("seasonNames") != null && !document.getElementById("seasonNames").contains(e.target))) {
            this.setState({ seasonName: "" }, () => {
                document.removeEventListener('click', this.closeSeasonName);
            });
        }
    }

    componentDidMount() {
        this.props.getSitePlanningFiltersRequest();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getSitePlanningFilters.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                sitePlanningFilters: nextProps.seasonPlanning.getSitePlanningFilters.data.resource
            });
            this.props.getSitePlanningFiltersClear();
        }
        else if (nextProps.seasonPlanning.getSitePlanningFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSitePlanningFilters.message.status,
                errorCode: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSitePlanningFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getSitePlanningFilters.message.error.errorMessage
            });
            this.props.getSitePlanningFiltersClear();
        }

        if (nextProps.seasonPlanning.saveSeasonPlanning.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveSeasonPlanning.data.resource.message, //nextProps.seasonPlanning.saveSeasonPlanning.data.message,
                error: false
            });
            this.props.saveSeasonPlanningClear();
        }
        else if (nextProps.seasonPlanning.saveSeasonPlanning.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSeasonPlanning.message.status,
                errorCode: nextProps.seasonPlanning.saveSeasonPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSeasonPlanning.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSeasonPlanning.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSeasonPlanning.message.error.errorMessage
            });
            this.props.saveSeasonPlanningClear();
        }

        if (nextProps.seasonPlanning.getSitePlanningFilters.isLoading || nextProps.seasonPlanning.saveSeasonPlanning.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false
            });
        }
    }

    validateSite = (site, index) => {
        if (site == 'Select Option') {
            let errorSite = this.state.errorSite;
            errorSite[index] = true;
            this.setState({
                errorSite
            });
            return true;
        }
        else {
            let errorSite = this.state.errorSite;
            errorSite[index] = false;
            this.setState({
                errorSite
            });
            return false;
        }
    }

    validateSeasonName = (seasonName, index) => {
        if (seasonName == 'Select Option') {
            let errorSeasonName = this.state.errorSeasonName;
            errorSeasonName[index] = true;
            this.setState({
                errorSeasonName
            });
            return true;
        }
        else {
            let errorSeasonName = this.state.errorSeasonName;
            errorSeasonName[index] = false;
            this.setState({
                errorSeasonName
            });
            return false;
        }
    }

    validateSeasonStartDate = (seasonStartDate, index) => {
        if (seasonStartDate == '') {
            let errorSeasonStartDate = this.state.errorSeasonStartDate;
            errorSeasonStartDate[index] = true;
            this.setState({
                errorSeasonStartDate
            });
            return true;
        }
        else {
            let errorSeasonStartDate = this.state.errorSeasonStartDate;
            errorSeasonStartDate[index] = false;
            this.setState({
                errorSeasonStartDate
            });
            return false;
        }
    }

    validateSeasonEndDate = (seasonEndDate, index) => {
        if (seasonEndDate == '') {
            let errorSeasonEndDate = this.state.errorSeasonEndDate;
            errorSeasonEndDate[index] = true;
            this.setState({
                errorSeasonEndDate
            });
            return true;
        }
        else {
            let errorSeasonEndDate = this.state.errorSeasonEndDate;
            errorSeasonEndDate[index] = false;
            this.setState({
                errorSeasonEndDate
            });
            return false;
        }
    }

    validateSeasonDays = (seasonDays, index) => {
        if (seasonDays <= 0) {
            let errorSeasonDays = this.state.errorSeasonDays;
            errorSeasonDays[index] = true;
            this.setState({
                errorSeasonDays
            });
            return true;
        }
        else {
            let errorSeasonDays = this.state.errorSeasonDays;
            errorSeasonDays[index] = false;
            this.setState({
                errorSeasonDays
            });
            return false;
        }
    }

    saveSeasonPlanning = () => {
        const errorSite = this.state.newSeason.filter((item, index) => this.validateSite(item.storeName, index));
        const errorSeasonName = this.state.newSeason.filter((item, index) => this.validateSeasonName(item.seasonName, index));
        const errorSeasonStartDate = this.state.newSeason.filter((item, index) => this.validateSeasonStartDate(item.seasonStartDate, index));
        const errorSeasonEndDate = this.state.newSeason.filter((item, index) => this.validateSeasonEndDate(item.seasonEndDate, index));
        const errorSeasonDays = this.state.newSeason.filter((item, index) => this.validateSeasonDays(item.seasonDays, index));

        if (errorSite.length == 0 && errorSeasonName.length == 0 && errorSeasonStartDate.length == 0 && errorSeasonEndDate.length == 0 && errorSeasonDays.length == 0) {
            this.props.saveSeasonPlanningRequest(this.state.newSeason);
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
        this.props.history.push("/inventoryPlanning/season-mapping");
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    addNewRow = () => {
        let newRow = {
            id: 0,
            storeCode: '',
            storeName: 'Select Option',
            seasonName: 'Select Option',
            seasonStartDate: '',
            seasonEndDate: '',
            seasonDays: ''
        };
        this.setState({
            newSeason: this.state.newSeason.concat(newRow),
            errorSite: this.state.errorSite.concat(false),
            errorSeasonName: this.state.errorSeasonName.concat(false),
            errorSeasonStartDate: this.state.errorSeasonStartDate.concat(false),
            errorSeasonEndDate: this.state.errorSeasonEndDate.concat(false),
            errorSeasonDays: this.state.errorSeasonDays.concat(false)
        });
    }

    deleteRow = (index) => {
        let updateRow = this.state.newSeason;
        if (updateRow.length == 1) {
            this.setState({
                toastMsg: "Single row cannot be deleted!",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 3000);
        }
        else {
            let newErrorSite = this.state.errorSite;
            let newErrorSeasonName = this.state.errorSeasonName;
            let newErrorSeasonStartDate = this.state.errorSeasonStartDate;
            let newErrorSeasonEndDate = this.state.errorSeasonEndDate;
            let newErrorSeasonDays = this.state.errorSeasonDays;
            updateRow.splice(index, 1);
            newErrorSite.splice(index, 1);
            newErrorSeasonName.splice(index, 1);
            newErrorSeasonStartDate.splice(index, 1);
            newErrorSeasonEndDate.splice(index, 1);
            newErrorSeasonDays.splice(index, 1);

            this.setState({
                newSeason: updateRow,

                errorSite: newErrorSite,
                errorSeasonName: newErrorSeasonName,
                errorSeasonStartDate: newErrorSeasonStartDate,
                errorSeasonEndDate: newErrorSeasonEndDate,
                errorSeasonDays: newErrorSeasonDays
            });
        }
    }

    clearRows = () => {
        let emptyRow = [{
            id: 0,
            storeCode: '',
            storeName: 'Select Option',
            seasonName: 'Select Option',
            seasonStartDate: '',
            seasonEndDate: '',
            seasonDays: ''
        }]
        this.setState({
            newSeason: emptyRow,

            errorSite: [false],
            errorSeasonName: [false],
            errorSeasonStartDate: [false],
            errorSeasonEndDate: [false],
            errorSeasonDays: [false],

            confirmClearModal: false
        });
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        })
    }

    handleSiteName = (siteName, siteCode, index) => {
        let updateRow = [...this.state.newSeason];
        updateRow[index].storeCode = parseInt(siteCode);
        updateRow[index].storeName = siteName;
        this.setState({newSeason: updateRow});
        this.validateSite(siteName, index);
        this.closeSiteName();
    }

    handleSeasonName = (seasonName, index) => {
        let updateRow = [...this.state.newSeason];
        updateRow[index].seasonName = seasonName;
        this.setState({newSeason: updateRow});
        this.validateSeasonName(seasonName, index);
        this.closeSeasonName();
    }

    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-left">
                                <button className="ngl-back" type="button" onClick={() => this.props.history.push("/inventoryPlanning/season-mapping")}>
                                    <span className="back-arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10">
                                            <path fill="#000" fillRule="evenodd" d="M12.283 4.232H2.987l2.612-2.733a.83.83 0 0 0 0-1.149.745.745 0 0 0-1.098 0L.563 4.47a.83.83 0 0 0 0 1.149l3.924 4.105a.764.764 0 0 0 .55.238c.2 0 .401-.084.549-.238a.83.83 0 0 0 0-1.149L2.987 5.857h9.296c.428 0 .777-.364.777-.813 0-.448-.349-.812-.777-.812z"/>
                                        </svg>
                                    </span>
                                    Back
                                </button>
                            </div>    
                        </div> 
                        <div className="col-lg-6 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className="excel-export">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="8.627" height="10" viewBox="0 0 8.627 12.652">
                                            <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)"/>
                                        </svg>
                                    </span>
                                    Excel Upload
                                </button>
                                <button type="button" className="get-details" onClick={this.saveSeasonPlanning}>Save</button>
                                <button type="button" disabled={this.props.location.state == undefined ? "" : "disabled"} onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="add-more-button">
                        <button className="amb-add" type="button" disabled={this.props.location.state == undefined ? "" : "disabled"} onClick={this.addNewRow}>
                            <span className="amba-inner">
                                <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="20" height="20" viewBox="0 0 19.846 19.846">
                                    <g id="Group_3035">
                                        <g id="Group_3034">
                                            <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" className="cls-1"/>
                                        </g>
                                    </g>
                                    <g id="Group_3037" transform="translate(5.311 5.242)">
                                        <g id="Group_3036">
                                            <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" className="cls-1" transform="translate(-137.022 -135.238)"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                            Add More
                        </button>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="event-master-table">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="fix-action-btn"><label></label></th>
                                    <th><label>Site Name</label></th>
                                    <th><label>Site ID</label></th>
                                    <th><label>Season Name</label></th>
                                    <th><label>Season Start Date</label></th>
                                    <th><label>Season Last Date</label></th>
                                    <th><label>Season Days</label></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.newSeason.map ((item, index) => (
                                        <tr key={index}>
                                            <td className="fix-action-btn">
                                                <ul className="table-item-list">
                                                    <li className="til-inner til-delete-btn" onClick={() => this.deleteRow(index)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                            <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                        </svg>
                                                        {/* <span className="generic-tooltip">Delete</span> */}
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                {/* <select className={this.state.errorSite[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newSeason;
                                                    updateRow[index].storeCode = parseInt(event.target.value);
                                                    updateRow[index].storeName = event.target.options[event.target.selectedIndex].text;
                                                    this.setState({newSeason: updateRow});
                                                    this.validateSite(updateRow[index].storeName, index);
                                                }}>
                                                    {item.storeName == '' ? <option selected disabled>Select Option</option> : <option disabled>Select Option</option>}
                                                    {this.state.sitePlanningFilters.map((innerItem) => (
                                                        innerItem.siteName === item.storeName ? <option value={innerItem.siteCode} selected>{innerItem.siteName}</option> : <option value={innerItem.siteCode}>{innerItem.siteName}</option>
                                                    ))}
                                                </select> */}
                                                <div className="gen-custom-select">
                                                    <button type="button" className={this.state.errorSite[index] ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openSiteName(e, index)}>{item.storeName}</button>
                                                    {this.state.siteName === index &&
                                                    <div className="gcs-dropdown" id="siteNames">
                                                        <ul>
                                                            <li className="gcsd-search">
                                                                <input type="search" placeholder="Search...." onChange={(e) => this.setState({searchSite: e.target.value})} />
                                                                <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                                            </li>
                                                            {this.state.sitePlanningFilters.map((innerItem) => (
                                                                innerItem.siteCode.toString().includes(this.state.searchSite) || innerItem.siteName.includes(this.state.searchSite) ?
                                                                <li onClick={() => this.handleSiteName(innerItem.siteName, innerItem.siteCode, index)}>{innerItem.siteCode} - {innerItem.siteName}</li> :
                                                                null
                                                            ))}
                                                        </ul>
                                                    </div>}
                                                </div>
                                                {this.state.errorSite[index] ? <span className="error">Select Site Name</span> : null}
                                            </td>
                                            <td><input disabled type="text" value={item.storeCode}/></td>
                                            <td>
                                                {/* <select value={item.seasonName} className={this.state.errorSeasonName[index] ? "errorBorder" : ""} onChange={(event) => {
                                                    let updateRow = this.state.newSeason;
                                                    updateRow[index].seasonName = event.target.value;
                                                    this.setState({newSeason: updateRow});
                                                    this.validateSeasonName(event.target.value, index);
                                                }}>
                                                    {item.seasonName == '' ? <option selected disabled value=''>Select Option</option> : <option disabled value=''>Select Option</option>}
                                                    {item.seasonName == "Autumn" ? <option value="Autumn" selected>Autumn</option> : <option value="Autumn">Autumn</option>}
                                                    {item.seasonName == "Spring" ? <option value="Spring" selected>Spring</option> : <option value="Spring">Spring</option>}
                                                    {item.seasonName == "Summer" ? <option value="Summer" selected>Summer</option> : <option value="Summer">Summer</option>}
                                                    {item.seasonName == "Winter" ? <option value="Winter" selected>Winter</option> : <option value="Winter">Winter</option>}
                                                </select> */}
                                                <div className="gen-custom-select" >
                                                    <button type="button" className={this.state.errorSeasonName[index] ? "gcs-select-btn errorBorder" : "gcs-select-btn"} onClick={(e) => this.openSeasonName(e, index)}>{item.seasonName}</button>
                                                    {this.state.seasonName === index &&
                                                    <div className="gcs-dropdown" id="seasonNames">
                                                        <ul>
                                                            <li onClick={() => this.handleSeasonName("Autumn", index)}>Autumn</li>
                                                            <li onClick={() => this.handleSeasonName("Spring", index)}>Spring</li>
                                                            <li onClick={() => this.handleSeasonName("Summer", index)}>Summer</li>
                                                            <li onClick={() => this.handleSeasonName("Winter", index)}>Winter</li>
                                                        </ul>
                                                    </div>}
                                                </div>
                                                {this.state.errorSeasonName[index] ? <span className="error">Select Season Name</span> : null}
                                            </td>
                                            <td><input type="date" placeholder={item.seasonStartDate == '' ? "YYYY-MM-DD" : ""} value={item.seasonStartDate} className={this.state.errorSeasonStartDate[index] ? "errorBorder" : ""}
                                                onChange={(event) => {
                                                    let updateRow = this.state.newSeason;
                                                    updateRow[index].seasonStartDate = event.target.value;
                                                    if(updateRow[index].seasonEndDate != '') {
                                                        let startDate = new Date(updateRow[index].seasonStartDate);
                                                        let endDate = new Date(updateRow[index].seasonEndDate);
                                                        let diff = Math.floor((Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()) - Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) ) /(1000 * 60 * 60 * 24));
                                                        updateRow[index].seasonDays = diff + 1;
                                                        this.validateSeasonDays(diff + 1, index);
                                                    }
                                                    this.setState({newSeason: updateRow});
                                                    this.validateSeasonStartDate(event.target.value, index);
                                                }}/>
                                                {this.state.errorSeasonStartDate[index] ? <span className="error">Select Season Start Date</span> : null}
                                            </td>
                                            <td><input type="date" placeholder={item.seasonEndDate == '' ? "YYYY-MM-DD" : ""} value={item.seasonEndDate} className={this.state.errorSeasonEndDate[index] ? "errorBorder" : ""}
                                                onChange={(event) => {
                                                    let updateRow = this.state.newSeason;
                                                    updateRow[index].seasonEndDate = event.target.value;
                                                    if(updateRow[index].seasonStartDate != '') {
                                                        let startDate = new Date(updateRow[index].seasonStartDate);
                                                        let endDate = new Date(updateRow[index].seasonEndDate);
                                                        let diff = Math.floor((Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()) - Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) ) /(1000 * 60 * 60 * 24));
                                                        updateRow[index].seasonDays = diff + 1;
                                                        this.validateSeasonDays(diff + 1, index);
                                                    }
                                                    this.setState({newSeason: updateRow});
                                                    this.validateSeasonEndDate(event.target.value, index);
                                                }}/>
                                                {this.state.errorSeasonEndDate[index] ? <span className="error">Select Season Last Date</span> : null}
                                            </td>
                                            <td>
                                                <input disabled className={this.state.errorSeasonDays[index] ? "errorBorder" : ""} type="text" value={item.seasonDays} />
                                                {this.state.errorSeasonDays[index] ? <span className="error">Select valid Season Dates</span> : null}
                                            </td>
                                        </tr>
                                    ))
                                }
                                {/* <tr>
                                    <td className="fix-action-btn">
                                        <ul className="table-item-list">
                                            <li className="til-inner til-delete-btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                    <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                </svg>
                                                {/* <span className="generic-tooltip">Delete</span>}
                                            </li>
                                        </ul>
                                    </td>
                                    <td><input type="text" onChange={(event) => this.setState({storeCode: event.target.value})}/></td>
                                    <td><input type="text" onChange={(event) => this.setState({storeName: event.target.value})}/></td>
                                    <td>
                                        <select onChange={(event) => this.setState({seasonName: event.target.value})}>
                                            <option>Autumn</option>
                                            <option>Spring</option>
                                            <option>Summer</option>
                                            <option>Winter</option>
                                        </select>
                                    </td>
                                    <td><input type="date" placeholder="DD/MM/YY" onChange={(event) => this.setState({seasonStartDate: event.target.value})}/></td>
                                    <td><input type="date" placeholder="DD/MM/YY" onChange={(event) => this.setState({seasonEndDate: event.target.value})}/></td>
                                    <td><input type="text" onChange={(event) => this.setState({seasonDays: event.target.value})}/></td>
                                </tr> */}
                            </tbody>
                        </table>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.clearRows} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default CreateSeasonMappingPlanning; 