import React from 'react';
import SearchImage from '../../assets/searchicon.svg';
import MrpRangeDetails from './mrpRangeDetails';
import Pagination from '../pagination';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import GenericDataModal from '../replenishment/genericDataModal';
import Confirm from '../loaders/arsConfirm';
import ArsFilter from "./arsFilter";

class MrpRangePlanning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',
            toastMsg: "",
            toastLoader: false,
            exportToExcel: false,
            getDetails: false,

            genericDataModalShow: false,
            genericDataModalShowRow: {},
            genericSearchData: {},
            data: {
                hl1_name: "Division",
                hl2_name: "Section",
                hl3_name: "Department",
                hl4_name: "Article",
                brand: "Brand",
                color: "Color",
                pattern: "Pattern",
                material: "Material",
                design: "Design",
                sleeve_length: "Sleeve"
            },
            MRPFrom: "",
            MRPTo: "",
            genericSearchRefs: {},
            selectedItems: {},

            tableHeaders: {},
            history: [],
            type: 1,
            search: "",
            sortedBy: "",
            sortedIn: "DESC",

            filter: false,
            filterBar: false,
            filterItems: {},
            appliedFilters: {},

            errors: {
                MRPFrom: false,
                MRPTo: false
            },

            response: {} //FINAL RESPONSE AFTER THE "GET DETAILS" BUTTON IS CLICKED
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
            this.props.getArsGenericFiltersClear();
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getArsGenericFiltersClear();
        }

        if(nextProps.seasonPlanning.getMRPRange.isSuccess) {
            if (nextProps.seasonPlanning.getMRPRange.data.resource == null) {
                this.setState({
                    loading: false,
                    success: false,
                    error: false,
                    toastMsg: nextProps.seasonPlanning.getMRPRange.data.message,
                    toastLoader: true
                });
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 3000);
                this.props.getMRPRangeClear();
            }
            else {
                this.setState({
                    loading: false,
                    success: false,
                    error: false,
                    response: nextProps.seasonPlanning.getMRPRange.data
                }, () => this.setState({getDetails: true}));
                this.props.getMRPRangeClear();
            }
        }
        else if (nextProps.seasonPlanning.getMRPRange.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getMRPRange.message.status,
                errorCode: nextProps.seasonPlanning.getMRPRange.message.error == undefined ? undefined : nextProps.seasonPlanning.getMRPRange.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getMRPRange.message.error == undefined ? undefined : nextProps.seasonPlanning.getMRPRange.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getMRPRangeClear();
        }

        if(nextProps.seasonPlanning.updateMRPRange.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.updateMRPRange.data.message,
                error: false
            });
            this.props.updateMRPRangeClear();
        }
        else if (nextProps.seasonPlanning.updateMRPRange.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.updateMRPRange.message.status,
                errorCode: nextProps.seasonPlanning.updateMRPRange.message.error == undefined ? undefined : nextProps.seasonPlanning.updateMRPRange.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.updateMRPRange.message.error == undefined ? undefined : nextProps.seasonPlanning.updateMRPRange.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getMRPRangeClear();
        }

        if (nextProps.seasonPlanning.getMRPHistory.isSuccess) {
            this.setState({
                loading: false,
                error: false,
                success: false,
                history: nextProps.seasonPlanning.getMRPHistory.data.resource.searchResult,
                prev: nextProps.seasonPlanning.getMRPHistory.data.resource.previousPage,
                current: nextProps.seasonPlanning.getMRPHistory.data.resource.currPage,
                next: nextProps.seasonPlanning.getMRPHistory.data.resource.currPage + 1,
                maxPage: nextProps.seasonPlanning.getMRPHistory.data.resource.maxPage,
                totalItems: nextProps.seasonPlanning.getMRPHistory.data.resource.totalCount,
                jumpPage: nextProps.seasonPlanning.getMRPHistory.data.resource.currPage
            });
            this.props.getMRPHistoryClear();
        }
        else if (nextProps.seasonPlanning.getMRPHistory.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.seasonPlanning.getMRPHistory.message.status,
                errorCode: nextProps.seasonPlanning.getMRPHistory.message.error == undefined ? undefined : nextProps.seasonPlanning.getMRPHistory.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getMRPHistory.message.error == undefined ? undefined : nextProps.seasonPlanning.getMRPHistory.message.error.errorMessage,
                history: [],
                prev: '',
                current: '',
                next: '',
                maxPage: '',
                jumpPage: ''
            });
            this.props.getMRPHistoryClear();
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null) {
                this.setState({
                    loading: false,
                    error: false,
                    success: false,
                    tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
                    filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
                });
            }
            else {
                this.setState({loading: false});
            }
            this.props.getHeaderConfigClear();
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            });
            this.props.getHeaderConfigClear();
        }

        if (nextProps.seasonPlanning.downloadReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.downloadReport.data.message,
                error: false
            });
            window.location.href = nextProps.seasonPlanning.downloadReport.data.resource;
            this.props.downloadReportClear();
        }
        else if (nextProps.seasonPlanning.downloadReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.downloadReport.message.status,
                errorCode: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadReportClear();
        }

        if (nextProps.seasonPlanning.getArsGenericFilters.isLoading || nextProps.seasonPlanning.getMRPRange.isLoading || nextProps.seasonPlanning.updateMRPRange.isLoading || nextProps.seasonPlanning.getMRPHistory.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.seasonPlanning.downloadReport.isLoading) {
            this.setState({
                loading: true,
                getDetails: false
            });
        }
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }

    xlscsv = () => {
        let data = {
            pageNo: 1,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.appliedFilters
        };
        this.props.downloadReportRequest({
            module: "PLANNING_SETTINGS_MRP_RANGE",
            data: data
        });
        this.setState({exportToExcel: false});
    }

    page = (e) => {
        let payload = {
            type: this.state.type,
            search: this.state.search,
            filter: this.state.appliedFilters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        };

        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.seasonPlanning.getMRPHistory.data.resource.previousPage,
                    current: this.props.seasonPlanning.getMRPHistory.data.resource.currPage,
                    next: this.props.seasonPlanning.getMRPHistory.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getMRPHistory.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getMRPHistory.data.resource.previousPage != 0) {
                    this.props.getMRPHistoryRequest({pageNo: this.props.seasonPlanning.getSeasonPlanning.data.resource.previousPage, ...payload});
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.seasonPlanning.getMRPHistory.data.resource.previousPage,
                    current: this.props.seasonPlanning.getMRPHistory.data.resource.currPage,
                    next: this.props.seasonPlanning.getMRPHistory.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getMRPHistory.data.resource.maxPage
                })
            if (this.props.seasonPlanning.getMRPHistory.data.resource.currPage != this.props.seasonPlanning.getMRPHistory.data.resource.maxPage) {
                this.props.getMRPHistoryRequest({pageNo: this.props.seasonPlanning.getSeasonPlanning.data.resource.currPage + 1, ...payload});
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getMRPHistory.data.resource.previousPage,
                    current: this.props.seasonPlanning.getMRPHistory.data.resource.currPage,
                    next: this.props.seasonPlanning.getMRPHistory.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getMRPHistory.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getMRPHistory.data.resource.currPage <= this.props.seasonPlanning.getMRPHistory.data.resource.maxPage) {
                    this.props.getMRPHistoryRequest({pageNo: 1, ...payload});
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getMRPHistory.data.resource.previousPage,
                    current: this.props.seasonPlanning.getMRPHistory.data.resource.currPage,
                    next: this.props.seasonPlanning.getMRPHistory.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getMRPHistory.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getMRPHistory.data.resource.currPage <= this.props.seasonPlanning.getMRPHistory.data.resource.maxPage) {
                    this.props.getMRPHistoryRequest({pageNo: this.props.seasonPlanning.getSeasonPlanning.data.resource.maxPage, ...payload});
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: this.state.type,
                        search: this.state.search,
                        filter: this.state.appliedFilters,
                        sortedBy: this.state.sortedBy,
                        sortedIn: this.state.sortedIn
                    };
                    this.props.getMRPHistoryRequest(payload);
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    validate = () => {
        let err = this.state.errors;
        if (this.state.MRPFrom == "") {
            err.MRPFrom = true;
        }
        else {
            err.MRPFrom = false;
        }

        if (this.state.MRPTo == "") {
            err.MRPTo = true;
        }
        else {
            err.MRPTo = false;
        }

        this.setState({errors: err});
    }
    
    openGetDetails = (e) => {
        e.preventDefault();
        this.validate();
        if (!(this.state.errors.MRPFrom || this.state.errors.MRPTo)) {
            let payload = {};
            Object.keys(this.state.data).forEach((key) => {
                payload[key] = this.state.selectedItems[key] == undefined ? [] : this.state.selectedItems[key]
            });
            this.props.getMRPRangeRequest({
                type: 1,
                pageNo: 1,
                search: "",
                hl1Name: payload.hl1_name,
                hl2Name: payload.hl2_name,
                hl3Name: payload.hl3_name,
                hl4Name: payload.hl4_name,
                brand: payload.brand,
                color: payload.color,
                pattern: payload.pattern,
                material: payload.material,
                design: payload.design,
                sleeve: payload.sleeve_length,
                mrpFrom: this.state.MRPFrom,
                mrpTo: this.state.MRPTo
            });
        }
    }

    CloseGetDetails = () => {
        this.setState({getDetails: false });
    }

    siteData = (e, entity, key, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, search, pageNo) => {
        if (this.state.genericDataModalShowRow != key) this.closeGenericDataModal();
        this.props.getArsGenericFiltersRequest({
            entity: entity,
            key: key,
            search: search,
            pageNo: pageNo
        });
        this.setState({genericSearchData: {payload: {entity: entity, key: key, search: search}}, genericDataModalShowRow: key});
    }

    closeGenericDataModal = () => {
        let newRefs = this.state.genericSearchRefs;
        if (newRefs[this.state.genericDataModalShowRow] !== undefined) {
            newRefs[this.state.genericDataModalShowRow].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                genericSearchRefs: newRefs
            });
        }
    }

    selectItems = (entity, key, items) => {
        let updateItems = {...this.state.selectedItems};
        if (items.length !== 0) {
            updateItems[key] = items;
        }
        else if (items.length === 0 && updateItems[key] !== undefined) {
            delete updateItems[key];
        }
        this.setState({
            selectedItems: updateItems
        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false,
            getDetails: false
        }, () => {

        });
    }

    clearData = () => {
        this.setState({
            genericDataModalShow: false,
            genericDataModalShowRow: {},
            genericSearchData: {},
            MRPFrom: "",
            MRPTo: "",
            selectedItems: {},

            errors: {
                MRPFrom: false,
                MRPTo: false
            },

            confirmClearModal: false
        });
    }

    closeModal = () => {
        this.setState({
            confirmClearModal: false
        })
    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }
    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
            this.setState({ getDetails: false, confirmClearModal: false, })
        }
    }

    getHistoryData = () => {
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "PLANNING_SETTINGS_MRP_RANGE",
        });
        this.props.getMRPHistoryRequest({
            pageNo: 1,
            type: 1
        });
    }

    search = () => {
        if (this.state.search !== "") {
            this.props.getMRPHistoryRequest({
                pageNo: 1,
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type,
                search: this.state.search,
                filter: this.state.appliedFilters,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn
            });
            this.setState({
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type
            });
        }
    }

    clearSearch = () => {
        this.props.getMRPHistoryRequest({
            pageNo: 1,
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
            search: "",
            filter: this.state.appliedFilters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            search: "",
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type
        });
    }

    //--------------------- FILTER FUNCTIONS BEGIN ---------------------//

    openFilter = (e) => {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        },() =>  document.addEventListener('click', this.closeFilterOnClickEvent));
    }

    closeFilterOnClickEvent = (e) => {
        if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
            this.setState({
                filter: false,
                filterBar: false 
            }, () =>
            document.removeEventListener('click', this.closeFilterOnClickEvent));
        }
    }

    closeFilter = () => {
        this.setState({
            filter: false,
            filterBar: false
        }, () =>
        document.removeEventListener('click', this.closeFilterOnClickEvent));
    }

    applyFilter = (checkedFilters, refs) => {
        let filters = {};
        Object.keys(checkedFilters).forEach((key) => {
            refs[key].current != null && refs[key].current.value != undefined && refs[key].current.value != "" ? filters[key] = refs[key].current.value : ""
        });
        this.props.getMRPHistoryRequest({
            pageNo: 1,
            type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type,
            search: this.state.search,
            filter: filters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type,
            appliedFilters: filters
        });
        this.closeFilter();
    }

    clearFilter = () => {
        this.props.getMRPHistoryRequest({
            pageNo: 1,
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type,
            search: this.state.search,
            filter: {},
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            appliedFilters: {},
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type
        });
        this.closeFilter();
    }

    removeFilter = (key) => {
        let filters = this.state.appliedFilters;
        delete filters[key];
        if (Object.keys(filters).length == 0) {
            this.clearFilter();
        }
        else {
            this.setState({
                appliedFilters: filters
            });
            this.props.getMRPHistoryRequest({
                pageNo: 1,
                type: this.state.type,
                search: this.state.search,
                filter: filters,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn
            });
        }  
    }

    sortData = (key) => {
        this.props.getMRPHistoryRequest({
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        });
        this.setState({
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        });
    }

    render() {
        console.log(this.state);
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active" >
                                <a className="nav-link p-l-0 st-btn" href="#createmrprange" role="tab" data-toggle="tab">Create MRP Range</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#history" role="tab" data-toggle="tab" onClick={this.getHistoryData}>History</a>
                            </li>
                            {/* <li className="pst-button">
                                <div className="pstb-inner">
                                    <button type="button" className="pst-save">Get Details</button>
                                    <button type="button">Clear</button>
                                </div>
                            </li> */}
                        </ul>
                    </div>
                </div>

                <div className="col-lg-12 pad-0">
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="createmrprange" role="tabpanel">
                            <div className="col-lg-12 pad-0">
                                <div className="new-gen-head p-lr-47">
                                    <div className="col-lg-7 pad-0">
                                    </div>
                                    <div className="col-lg-5 pad-0">
                                        <div className="new-gen-right">
                                            <button type="button" className="get-details" onClick={(e) => this.openGetDetails(e)}>Get Details</button>
                                            <button type="button" onClick={() => this.setState({confirmClearModal: true, headerMsg: "Are you sure you want to clear the form?", paraMsg: "Click confirm to continue."})}>Clear</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 p-lr-47 m-top-20">
                                <div className="col-lg-2 pad-lft-0">
                                    <label className="mrp-label">From MRP - To MRP<span className="mandatory">*</span></label>
                                    <div className="inputTextKeyFucMain">
                                        <input className={this.state.errors.MRPFrom ? "errorBorder onFocus pnl-purchase-input mrl-input" : "onFocus pnl-purchase-input mrl-input"} type="text" placeholder="&#8377; 0.00" onChange={(e) => {this.setState({MRPFrom: e.target.value}); this.validate}} value={this.state.MRPFrom} />
                                        <input className={this.state.errors.MRPTo ? "errorBorder onFocus pnl-purchase-input mrl-input1" : "onFocus pnl-purchase-input mrl-input1"} type="text" placeholder="&#8377; 0.00" onChange={(e) => {this.setState({MRPTo: e.target.value}); this.validate}} value={this.state.MRPTo} />
                                    </div>
                                    {this.state.errors.MRPFrom || this.state.errors.MRPTo ? <span className="error">Enter From MRP - To MRP</span> : null}
                                </div>
                            </div>
                            <div className="pi-new-layout p-lr-47 m-top-20">
                                <form>{
                                    Object.keys(this.state.data).map((key) => {
                                        this.state.genericSearchRefs[key] = React.createRef();
                                        return (
                                            <div className="col-lg-2 pad-lft-0 mrp-range-item-detail">
                                                <label className="pnl-purchase-label">{this.state.data[key]}</label>
                                                <div className="inputTextKeyFucMain">
                                                    {/* <input type="text" className="onFocus pnl-purchase-input" placeholder={"Search " + this.state.data[key]} ref={this.state.genericSearchRefs[key]} onKeyDown={(e) => this.siteData(e, "item", key, e.target.value, 1)} /> */}
                                                    <input type="text" className="onFocus pnl-purchase-input"
                                                        id={key + "Focus"}
                                                        placeholder={this.state.selectedItems[key] === undefined || this.state.selectedItems[key].length === 0 ? "No " + this.state.data[key] + " selected" : this.state.selectedItems[key].length === 1 ? this.state.selectedItems[key][0] + " selected" : this.state.selectedItems[key][0] + " + " + (this.state.selectedItems[key].length - 1) + " selected"}
                                                        ref={this.state.genericSearchRefs[key]}
                                                        onFocus={(e) => {if (this.state.genericDataModalShow === false || this.state.genericDataModalShowRow != key) this.openGenericDataModal("item", key, e.target.value, 1)}}
                                                        //onBlur={(e) => {if (this.state.genericDataModalShowRow != this.state.data[key]) e.target.value=""}}
                                                        onKeyDown={(e) => this.siteData(e, "item", key, e.target.value, 1)}
                                                    />
                                                    <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal("item", key, this.state.genericSearchRefs[key].current.value, 1)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {this.state.genericDataModalShow && this.state.genericDataModalShowRow == key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems[key]} /> : null}
                                                </div>
                                            </div>
                                        )
                                    })
                                }</form>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="history" role="tabpanel">
                            <div className="col-lg-12 pad-0">
                                <div className="new-gen-head p-lr-47">
                                    <div className="col-lg-7 pad-0">
                                        <div className="new-gen-left">
                                            <div className="ngl-search">
                                                <input type="search" placeholder="Type To Search" value={this.state.search} onChange={(e) => e.target.value == "" ? this.clearSearch() : this.setState({search: e.target.value})} onKeyDown={(e) => e.keyCode == 13 ? this.search() : e.keyCode == 27 ? this.clearSearch() : ""} />
                                                <img className="search-image" src={require('../../assets/searchicon.svg')} onClick={this.search} />
                                                {this.state.search == "" ? null : <span className="closeSearch" onClick={this.clearSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 pad-0">
                                        <div className="new-gen-right">
                                            <div className="gvpd-download-drop">
                                                <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                                        <g>
                                                            <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                            <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                            <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                                        </g>
                                                    </svg>
                                                    <span className="generic-tooltip">Excel Download</span>
                                                </button>
                                                {this.state.exportToExcel &&
                                                    <ul className="pi-history-download">
                                                        <li>
                                                            <button className="export-excel" type="button" onClick={this.xlscsv}>
                                                                <span className="pi-export-svg">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                                        <g id="prefix__files" transform="translate(0 2)">
                                                                            <g id="prefix__Group_2456" data-name="Group 2456">
                                                                                <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                                    <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                                    <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                                        <tspan x="0" y="0">XLS</tspan>
                                                                                    </text>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                                Export to Excel
                                                            </button>
                                                        </li>
                                                    </ul>}
                                            </div>
                                            <div className="gvpd-filter">
                                                <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Filter</span>
                                                </button>
                                                {this.state.filter && <ArsFilter filterItems={this.state.filterItems} appliedFilters={this.state.appliedFilters} applyFilter={this.applyFilter} clearFilter={this.clearFilter} closeFilter={(e) => this.closeFilter(e)} />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 p-lr-47">{
                                Object.keys(this.state.appliedFilters).length == 0 ? "" :
                                <div className="show-applied-filter">
                                    <button type="button" className="saf-clear-all" onClick={this.clearFilter}>Clear All</button>
                                    {
                                        Object.keys(this.state.appliedFilters).map((key) =>
                                            <button type="button" className="saf-btn">{this.state.filterItems[key]}
                                                <img onClick={() => this.removeFilter(key)} src={require('../../assets/clearSearch.svg')} />
                                                <span className="generic-tooltip">{key.toLowerCase().includes("date") && this.state.appliedFilters[key] !== undefined ? `From ${this.state.appliedFilters[key].from} to ${this.state.appliedFilters[key].to}` : this.state.appliedFilters[key]}</span>
                                            </button>
                                        )
                                    }
                                </div>
                            }</div>
                            <div className="col-lg-12 p-lr-47">
                                <div className="expand-new-table m-top-30">
                                    <div className="manage-table">
                                        <table className="table gen-main-table">
                                            <thead>
                                                <tr>{
                                                    Object.keys(this.state.tableHeaders).map((key) => <th key={key} className={this.state.sortedBy == key && this.state.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.sortData(key)}><label>{this.state.tableHeaders[key]}</label><img src={require('../../assets/headerFilter.svg')} /></th>)
                                                }</tr>
                                            </thead>
                                            <tbody>{
                                                Object.keys(this.state.tableHeaders).length == 0 || this.state.history.length == 0 ?
                                                <tr><td><label>NO DATA FOUND!</label></td></tr> :
                                                this.state.history.map((item) =>
                                                    <tr>{
                                                        Object.keys(this.state.tableHeaders).map((key) => <td><label>{item[key]}</label></td>)
                                                    }</tr>
                                                )  
                                            }</tbody>
                                        </table>
                                    </div>
                                    <div className="col-md-12 pad-0" >
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <Pagination {...this.state} {...this.props} page={this.page}
                                                        prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.getDetails && <MrpRangeDetails CloseGetDetails={this.CloseGetDetails} appliedParams={this.state.selectedItems} data={this.state.data} response={this.state.response} save={this.props.updateMRPRangeRequest} />}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.clearData} /> : null}
            </div>
        )
    }
}

export default MrpRangePlanning; 