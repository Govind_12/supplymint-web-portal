import React from 'react';

class SiteDataModal extends React.Component {
    constructor(props) {
        console.log("constr");
        super(props);
        this.state = {
            // loading: true,
            // error: false,
            sites: this.props.data.resource.response,
            //type: 1,
            prev: this.props.data.resource.previousPage,
            current: this.props.data.resource.currPage,
            next: this.props.data.resource.currPage + 1,
            maxPage: this.props.data.resource.maxPage,
            totalItems: this.props.data.resource.totalCount,
        }
    }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.data.resource.previousPage,
                    current: this.props.data.resource.currPage,
                    next: this.props.data.resource.currPage + 1,
                    maxPage: this.props.data.resource.maxPage
                })
                if (this.props.data.resource.previousPage != 0) {
                    this.props.action({
                        search: this.props.search,
                        pageNo: this.props.data.resource.previousPage
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.data.resource.previousPage,
                    current: this.props.data.resource.currPage,
                    next: this.props.data.resource.currPage + 1,
                    maxPage: this.props.data.resource.maxPage
                })
            if (this.props.data.resource.currPage != this.props.data.resource.maxPage) {
                this.props.action({
                    search: this.props.search,
                    pageNo: this.props.data.resource.currPage + 1
                });
            }
        }
    }

    render() {
        console.log("render:", this.state);
        return (
            <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown zi999" id="siteDataModal">
                <div className="dropdown-modal-header">
                    <span className="vd-name div-col-2">{this.props.type == "site" ? "Site Code" : "Item Code"}</span>
                    <span className="vd-loc div-col-2">{this.props.type == "site" ? "Site Name" : "Item Name"}</span>
                </div>
                <ul className="dropdown-menu-city-item">{
                    this.state.sites.map((item) => (
                        <li onClick={this.props.type == "site" ? () => this.props.select(item.siteName) : () => this.props.select(item.itemCode, this.props.index)}>
                            <span className="vendor-details displayBlock">
                                <span className="vd-name div-col-2">{this.props.type == "site" ? item.siteCode : item.itemCode}</span>
                                <span className="vd-name div-col-2">{this.props.type == "site" ? item.siteName : item.itemName}</span>
                            </span>
                        </li>
                    ))
                }</ul>
                <div className="gen-dropdown-pagination">
                    <div className="page-close">
                        <button className="btn-close" type="button" id="btn-close" onClick={this.props.close}>Close</button>
                    </div>
                    <div className="page-next-prew-btn">
                        <button className="pnpb-prev" type="button" id="prev" onClick={(e) => this.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                        <button className="pnpb-no" type="button" disabled>{this.state.current}/{this.state.maxPage}</button>
                        <button className="pnpb-next" type="button" id="next" onClick={(e) => this.page(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default SiteDataModal;