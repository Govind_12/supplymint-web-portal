import React from 'react';
import ReactDragListView from 'react-drag-listview';

class InventorySetting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checkedItems: this.props.selectedData,
            data: {...this.props.data}
        }
    }

    checkItem = (key) => {
        let items = this.state.checkedItems;
        if (items[key] == undefined) {
            items[key] = this.props.data[key];
        }
        else {
            delete items[key];
        }
        this.setState({
            checkedItems: items
        });
    }

    handleSubmit = () => {
        let data = this.state.data;
        let checkedItems = {};
        Object.keys(data).forEach((key) => this.state.checkedItems[key] === undefined ? "" : checkedItems[key] = this.state.checkedItems[key]);
        this.props.save({
            filterType: this.props.type,
            data: this.state.checkedItems
        });
        this.props.setSubmit();
    }

    render () {
        const that = this;
        const dragProps = {
            onDragEnd(fromIndex, toIndex) {
                const data = Object.keys(that.state.checkedItems);
                const item = data.splice(fromIndex, 1)[0];
                data.splice(toIndex, 0, item);
                let dataObject = {};
                data.forEach((item) => {
                    dataObject[item] = that.state.checkedItems[item]
                });
                that.setState({ checkedItems: dataObject });
                // const data = Object.keys(that.state.data);
                // const item = data.splice(fromIndex, 1)[0];
                // data.splice(toIndex, 0, item);
                // let dataObject = {};
                // data.forEach((item) => {
                //     dataObject[item] = that.state.data[item]
                // });
                // that.setState({ data: dataObject, checkedItems: that.state.checkedItems });
            },
            nodeSelector: 'li',
            handleSelector: 'a',
            lineClassName: 'drag-line'
        };

        console.log(this.state);
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content invetory-setting-modal">
                    <div className="ism-head">
                        <h3>{this.props.heading} Configuration</h3>
                        <div className="ismh-btn">
                            <button type="button" onClick={this.props.closeSetting}><img src={require('../../assets/clearSearch.svg')} /></button>
                        </div>
                    </div>
                    <div className="ism-body">
                        <div className="ismb-left">
                            <div className="ismbl-head">
                                <h3>Available Items</h3>
                                <p>You can select multiple items from the below list.</p>
                            </div>
                            <ul className="chmbl-headers">{
                                Object.keys(this.state.data).map((item) => (
                                    this.state.data[item] == '' || this.state.data[item] == null || this.state.checkedItems[item] !== undefined ?
                                    null :
                                    <li className="chmblh-items">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" checked={this.state.checkedItems[item] !== undefined} onClick={() => this.checkItem(item)} />
                                            <span className="checkmark1"></span>
                                            {this.state.data[item]}
                                        </label>
                                    </li>
                                ))
                            }</ul>
                        </div>
                        <div className="ismb-right">
                            <div className="ismbr-head">
                                <h3>Selected Items</h3>
                                <p>You can drag and drop the below items to arrange the order.</p>
                            </div>
                            <ReactDragListView {...dragProps}>
                                <ul>{
                                    Object.keys(this.state.checkedItems).map((item) => (
                                        <li>
                                            <a><span className="chmblh-drag"></span></a>
                                            <span className="chmblhsl-text">{this.state.checkedItems[item]}</span>
                                            <span className="chmblhsl-close"><img src={require('../../assets/clearSearch.svg')} onClick={() => this.checkItem(item)} /></span>
                                        </li>
                                    ))
                                }</ul>
                            </ReactDragListView>
                        </div>
                    </div>
                    <div className="ism-footer">
                        <button type="button" className={Object.keys(this.state.checkedItems).length === 0 ? "ismhb-save btnDisabled" : "ismhb-save"} disabled={Object.keys(this.state.checkedItems).length === 0 ? "disabled" : ""} onClick={this.handleSubmit}>Save Settings</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default InventorySetting;