import React from 'react';
import AddGreen from '../../assets/add-green.svg';
import Filter from '../../assets/filter-icon.svg';
import Pagination from '../../components/pagination';
import Reload from '../../assets/reload.svg';
import SearchImage from '../../assets/searchicon.svg';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import Confirm from "../loaders/arsConfirm";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import AdhocPendingRequest from './adhocPendingRequest';
import AdhocApprovedRequest from './adhocApprovedRequest';
import AdhocRejectedRequest from './adhocRejectedRequest';
import ArsFilter from "./arsFilter";

class ManageAdhocRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exportToExcel: false,
            tableHeaders: {},
            expandColumnHeaders: {},
            expandColumn: "",
            adhocData: [],
            type: 1,
            prev: '',
            current: 1,
            next: '',
            maxPage: 0,
            totalItems: 0,
            jumpPage: 1,
            toastMsg: "",
            toastLoader: false,
            selectedItems: [],
            currentTab: 'Pending',

            loading: true,
            confirmDelete: false,
            submit: false,
            headerMsg: '',
            paraMsg: '',
            payload: '',
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            confirmUpdate: false,
            confirmUpdatePerWO: false,
            updateType: 1,
            failedToLoad: false,

            requestData: {
                pageNo: 1,
                type: 1,
                status: 'Pending',
                search: '',
                filter: '',
                sortedBy: "",
                sortedIn: "DESC",
            },

            filter: false,
            filterBar: false,
            filterItems: {}
        }
    }

    componentDidMount() {
        this.props.getHeaderConfigRequest({
            enterpriseName: "TURNINGCLOUD",
            attributeType: "TABLE HEADER",
            displayName: "AD_HOC_REQUEST_TABLE_PENDING",
        });
        this.props.getAdhocRequest(this.state.requestData);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getAdhoc.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                failedToLoad: false,
                confirmDelete: false,
                adhocData: nextProps.seasonPlanning.getAdhoc.data.resource.response,
                prev: nextProps.seasonPlanning.getAdhoc.data.resource.previousPage,
                current: nextProps.seasonPlanning.getAdhoc.data.resource.currPage,
                next: nextProps.seasonPlanning.getAdhoc.data.resource.currPage + 1,
                maxPage: nextProps.seasonPlanning.getAdhoc.data.resource.maxPage,
                totalItems: nextProps.seasonPlanning.getAdhoc.data.resource.totalCount,
                jumpPage: nextProps.seasonPlanning.getAdhoc.data.resource.currPage
            });
            this.props.getAdhocClear();
        }
        else if (nextProps.seasonPlanning.getAdhoc.isError) {
            this.setState({
                loading: false,
                success: false,
                failedToLoad: true,
                error: true,
                confirmDelete: false,
                code: nextProps.seasonPlanning.getAdhoc.message.status,
                errorCode: nextProps.seasonPlanning.getAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.getAdhoc.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.getAdhoc.message.error.errorMessage,
                adhocData: [],
                prev: '',
                current: '',
                next: '',
                maxPage: '',
                jumpPage: ''
            });
            this.props.getAdhocClear();
        }

        if (nextProps.seasonPlanning.deleteAdhoc.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.deleteAdhoc.data.resource.message,
                error: false,
                confirmDelete: false,
            });
            this.props.deleteAdhocClear();
        }
        else if (nextProps.seasonPlanning.deleteAdhoc.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.deleteAdhoc.message.status,
                errorCode: nextProps.seasonPlanning.deleteAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.deleteAdhoc.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.deleteAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.deleteAdhoc.message.error.errorMessage,
                confirmDelete: false
            });
            this.props.deleteAdhocClear();
        }

        if (nextProps.seasonPlanning.updateAdhoc.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.updateAdhoc.data.resource.message,
                error: false,
                confirmDelete: false,
                confirmUpdate: false,
                confirmUpdatePerWO: false
            });
            this.props.updateAdhocClear();
        }
        else if (nextProps.seasonPlanning.updateAdhoc.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.updateAdhoc.message.status,
                errorCode: nextProps.seasonPlanning.updateAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.updateAdhoc.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.updateAdhoc.message.error == undefined ? undefined : nextProps.seasonPlanning.updateAdhoc.message.error.errorMessage,
                confirmUpdate: false,
                confirmUpdatePerWO: false
            });
            this.props.updateAdhocClear();
        }

        if (nextProps.seasonPlanning.getAdhocItemsPerWO.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                failedToLoad: false,
                confirmDelete: false
            });
            this.props.getAdhocItemsPerWOClear();
        }
        else if (nextProps.seasonPlanning.getAdhocItemsPerWO.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                confirmDelete: false,
                code: nextProps.seasonPlanning.getAdhocItemsPerWO.message.status,
                errorCode: nextProps.seasonPlanning.getAdhocItemsPerWO.message.error == undefined ? undefined : nextProps.seasonPlanning.getAdhocItemsPerWO.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getAdhocItemsPerWO.message.error == undefined ? undefined : nextProps.seasonPlanning.getAdhocItemsPerWO.message.error.errorMessage
            });
            this.props.getAdhocItemsPerWOClear();
        }

        if (nextProps.seasonPlanning.updateAdhocItemsPerWO.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.updateAdhocItemsPerWO.data.resource.message,
                error: false,
                confirmDelete: false,
                confirmUpdate: false,
                confirmUpdatePerWO: false
            });
            this.props.updateAdhocItemsPerWOClear();
        }
        else if (nextProps.seasonPlanning.updateAdhocItemsPerWO.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.updateAdhocItemsPerWO.message.status,
                errorCode: nextProps.seasonPlanning.updateAdhocItemsPerWO.message.error == undefined ? undefined : nextProps.seasonPlanning.updateAdhocItemsPerWO.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.updateAdhocItemsPerWO.message.error == undefined ? undefined : nextProps.seasonPlanning.updateAdhocItemsPerWO.message.error.errorMessage,
                confirmUpdate: false,
                confirmUpdatePerWO: false
            });
            this.props.updateAdhocItemsPerWOClear();
        }

        if (nextProps.seasonPlanning.downloadReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.downloadReport.data.message,
                error: false
            });
            window.location.href = nextProps.seasonPlanning.downloadReport.data.resource;
            this.props.downloadReportClear();
        }
        else if (nextProps.seasonPlanning.downloadReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.downloadReport.message.status,
                errorCode: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadReportClear();
        }

        if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (this.state.expandColumn == "") {
                this.setState({
                    loading: false,
                    error: false,
                    success: false,
                    tableHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"],
                    filterItems: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
                });
                this.props.getHeaderConfigClear();
            }
            else {
                this.setState({
                    loading: false,
                    error: false,
                    success: false,
                    expandColumnHeaders: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]
                });
                this.props.getHeaderConfigClear();
            }
        }
        else if (nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loading: false,
                error: true,
                success: false,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage,
                tableHeaders: {}
            });
        }

        if (nextProps.seasonPlanning.getAdhoc.isLoading || nextProps.seasonPlanning.deleteAdhoc.isLoading || nextProps.seasonPlanning.updateAdhoc.isLoading || nextProps.seasonPlanning.getAdhocItemsPerWO.isLoading || nextProps.seasonPlanning.updateAdhocItemsPerWO.isLoading || nextProps.seasonPlanning.downloadReport.isLoading || nextProps.replenishment.getHeaderConfig.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false,
                confirmDelete: false,
                confirmUpdate: false,
                confirmUpdatePerWO: false
            });
        }
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }

    xlscsv = () => {
        // let data = {
        //     pageNo: 1,
        //     type: this.state.requestData.type,
        //     search: this.state.requestData.search,
        //     filter: this.state.appliedFilters
        // };
        let module = "";
        switch(this.state.requestData.status) {
            case "Pending": module = "AD_HOC_REQUEST_PENDING"; break;
            case "Approved": module = "AD_HOC_REQUEST_APPROVED"; break;
            case "Rejected": module = "AD_HOC_REQUEST_REJECTED"; break;
        }
        this.props.downloadReportRequest({
            module: module,
            data: this.state.requestData
        });
        this.setState({exportToExcel: false});
    }

    createNew = () => {
        this.props.history.push('/inventoryPlanning/createAdhocRequest')
    }

    expandColumn = (e, id) => {
        e.preventDefault();
        if (this.state.expandColumn == id) {
            this.setState({
                expandColumn: ""
            });
        }
        else {
            this.setState({
                expandColumn: id
            },
            () => {
                this.props.getHeaderConfigRequest({
                    enterpriseName: "TURNINGCLOUD",
                    attributeType: "TABLE HEADER",
                    displayName: this.state.currentTab == "Pending" ? "AD_HOC_REQUEST_TABLE_SUB_PENDING" : this.state.currentTab == "Approved" ? "AD_HOC_REQUEST_TABLE_SUB_APPROVED" : "AD_HOC_REQUEST_TABLE_SUB_REJECTED",
                });
                this.props.getAdhocItemsPerWORequest({pageNo: 1, workOrder: id, status: this.state.currentTab});
            });
        }
    }

    // saveExpandedRow = (payload) => {
    //     this.props.updateAdhocItemsPerWORequest(payload);
    // }

    page = (e) => {
        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.seasonPlanning.getAdhoc.data.resource.previousPage,
                    current: this.props.seasonPlanning.getAdhoc.data.resource.currPage,
                    next: this.props.seasonPlanning.getAdhoc.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getAdhoc.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getAdhoc.data.resource.previousPage != 0) {
                    this.props.getAdhocRequest({
                        ...this.state.requestData,
                        pageNo: this.props.seasonPlanning.getAdhoc.data.resource.previousPage
                    });
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.seasonPlanning.getAdhoc.data.resource.previousPage,
                    current: this.props.seasonPlanning.getAdhoc.data.resource.currPage,
                    next: this.props.seasonPlanning.getAdhoc.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getAdhoc.data.resource.maxPage
                })
            if (this.props.seasonPlanning.getAdhoc.data.resource.currPage != this.props.seasonPlanning.getAdhoc.data.resource.maxPage) {
                this.props.getAdhocRequest({
                    ...this.state.requestData,
                    pageNo: this.props.seasonPlanning.getAdhoc.data.resource.currPage + 1
                });
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getAdhoc.data.resource.previousPage,
                    current: this.props.seasonPlanning.getAdhoc.data.resource.currPage,
                    next: this.props.seasonPlanning.getAdhoc.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getAdhoc.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getAdhoc.data.resource.currPage <= this.props.seasonPlanning.getAdhoc.data.resource.maxPage) {
                    this.props.getAdhocRequest({
                        ...this.state.requestData,
                        pageNo: 1
                    });
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getAdhoc.data.resource.previousPage,
                    current: this.props.seasonPlanning.getAdhoc.data.resource.currPage,
                    next: this.props.seasonPlanning.getAdhoc.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getAdhoc.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getAdhoc.data.resource.currPage <= this.props.seasonPlanning.getAdhoc.data.resource.maxPage) {
                    this.props.getAdhocRequest({
                        ...this.state.requestData,
                        pageNo: this.props.seasonPlanning.getAdhoc.data.resource.maxPage
                    });
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    this.props.getAdhocRequest({
                        ...this.state.requestData,
                        pageNo: _.target.value
                    });
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    handleTabChange = (tabName) => {
        let filters = {...this.state.requestData.filter};
        filters.approvedQuantity === undefined ? "" : delete filters.approvedQuantity;
        if (this.state.currentTab != tabName) {
            this.setState({
                requestData: {...this.state.requestData, status: tabName, filter: filters, sortedBy: "", sortedIn: "DESC"},
                currentTab: tabName,
                selectedItems: [],
                expandColumn: ""
            });
            this.props.getHeaderConfigRequest({
                enterpriseName: "TURNINGCLOUD",
                attributeType: "TABLE HEADER",
                displayName: tabName == "Pending" ? "AD_HOC_REQUEST_TABLE_PENDING" : tabName == "Approved" ? "AD_HOC_REQUEST_TABLE_APPROVED" : "AD_HOC_REQUEST_TABLE_REJECTED",
            });
            this.props.getAdhocRequest({...this.state.requestData, status: tabName, filter: filters});
        }
    }

    deleteItem = (id) => {
        this.setState({
            payload: id,
            headerMsg: "Are you sure you want to delete this row?",
            paraMsg: "Click confirm to continue.",
            confirmDelete: true
        });
    }

    afterConfirmDelete = () => {
        this.setState({
            //submit: true,
            confirmDelete: false
        });
        //this.props.getAdhocRequest({...this.state.requestData, status: this.state.currentTab});
    }

    closeConfirmDelete = () => {
        this.setState({
            // submit: false,
            confirmDelete: false,
        });
    }

    getSelectedItems = (items) => {
        this.setState({
            selectedItems: items
        })
    }

    updateItems = (type) => {
        this.setState({
            payload: {type: type, keys: this.state.selectedItems},
            headerMsg: type == "Approved" ? "Are you sure you want to approve the selected request(s)?" : "Are you sure you want to reject the selected request(s)?",
            paraMsg: "Click confirm to continue.",
            confirmUpdate: true
        });
    }

    updateItemsPerWO = (payload) => {
        this.setState({
            payload: payload,
            headerMsg: "Are you sure you want to save the changes?",
            paraMsg: "Click confirm to continue.",
            confirmUpdatePerWO: true
        });
    }

    afterConfirmUpdate = () => {
        this.setState({
            confirmUpdate: false,
            selectedItems: []
        });
    }

    afterConfirmUpdatePerWO = () => {
        this.setState({
            confirmUpdatePerWO: false
        });
    }

    closeConfirmUpdate = () => {
        this.setState({
            confirmUpdate: false
        });
    }

    closeConfirmUpdatePerWO = () => {
        this.setState({
            confirmUpdatePerWO: false
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false,
            expandColumn: ""
        }, () => {

        });
        this.props.getAdhocRequest(this.state.requestData);
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    search = () => {
        if (this.state.requestData.search !== "") {
            this.props.getAdhocRequest({...this.state.requestData, pageNo: 1, type: this.state.requestData.type == 1 ? 3 : this.state.requestData.type == 2 ? 4 : this.state.requestData.type});
            this.setState({
                requestData: {...this.state.requestData, type: this.state.requestData.type == 1 ? 3 : this.state.requestData.type == 2 ? 4 : this.state.requestData.type}
            });
        }
    }

    clearSearch = () => {
        this.props.getAdhocRequest({
            ...this.state.requestData,
            pageNo: 1,
            type: this.state.requestData.type == 3 ? 1 : this.state.requestData.type == 4 ? 2 : this.state.requestData.type,
            search: ""
        });
        this.setState({
            requestData: {
                ...this.state.requestData,
                type: this.state.requestData.type == 3 ? 1 : this.state.requestData.type == 4 ? 2 : this.state.requestData.type,
                search: ""
            }
        });
    }

    // onRefresh = () => {
    //     this.setState({
    //         requestData: {
    //             ...this.state.requestData,
    //             type: 1,
    //             search: "",
    //             filter: {}
    //         }
    //     });
    //     this.props.getAdhocRequest({
    //         ...this.state.requestData,
    //         pageNo: 1,
    //         type: 1,
    //         search: "",
    //         filter: {}
    //     });
    // }

    //--------------------- FILTER FUNCTIONS BEGIN ---------------------//

    openFilter = (e) => {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        },() =>  document.addEventListener('click', this.closeFilterOnClickEvent));
    }

    closeFilterOnClickEvent = (e) => {
        if (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent")) {
            this.setState({
                filter: false,
                filterBar: false 
            }, () =>
            document.removeEventListener('click', this.closeFilterOnClickEvent));
        }
    }

    closeFilter = () => {
        this.setState({
            filter: false,
            filterBar: false
        }, () =>
        document.removeEventListener('click', this.closeFilterOnClickEvent));
    }

    applyFilter = (checkedFilters, refs) => {
        let filters = {};
        Object.keys(checkedFilters).forEach((key) => {
            refs[key].current != null && refs[key].current.value != undefined && refs[key].current.value != "" ? filters[key] = refs[key].current.value : ""
        });
        this.props.getAdhocRequest({
            ...this.state.requestData,
            pageNo: 1,
            type: this.state.requestData.type == 1 ? 2 : this.state.requestData.type == 3 ? 4 : this.state.requestData.type,
            filter: filters
        });
        this.setState({
            requestData: {
                ...this.state.requestData,
                type: this.state.requestData.type == 1 ? 2 : this.state.requestData.type == 3 ? 4 : this.state.requestData.type,
                filter: filters
            }
        });
        this.closeFilter();
    }

    clearFilter = () => {
        this.props.getAdhocRequest({
            ...this.state.requestData,
            pageNo: 1,
            type: this.state.requestData.type == 2 ? 1 : this.state.requestData.type == 4 ? 3 : this.state.requestData.type,
            filter: {}
        });
        this.setState({
            requestData: {
                ...this.state.requestData,
                filter: {},
                type: this.state.requestData.type == 2 ? 1 : this.state.requestData.type == 4 ? 3 : this.state.requestData.type
            }
        });
        this.closeFilter();
    }

    removeFilter = (key) => {
        let filters = this.state.requestData.filter;
        delete filters[key];
        if (Object.keys(filters).length == 0) {
            this.clearFilter();
        }
        else {
            this.setState({
                requestData: {
                    ...this.state.requestData,
                    filter: filters
                }
            });
            this.props.getAdhocRequest({
                ...this.state.requestData,
                pageNo: 1,
                filter: filters
            });
        }  
    }

    sortData = (key) => {
        this.props.getAdhocRequest({
            ...this.state.requestData,
            sortedBy: key,
            sortedIn: this.state.requestData.sortedBy == key && this.state.requestData.sortedIn === "ASC" ? "DESC" : "ASC"
        });
        this.setState({
            requestData: {...this.state.requestData, sortedBy: key, sortedIn: this.state.requestData.sortedBy == key && this.state.requestData.sortedIn === "ASC" ? "DESC" : "ASC"}
        });
    }

    render() {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="new-gen-head p-lr-47">
                        <div className="col-lg-5 pad-0">
                            <div className="new-gen-left">
                                <div className="ngl-search">
                                    <input type="search" placeholder="Type To Search" value={this.state.requestData.search} onChange={(e) => e.target.value == "" ? this.clearSearch() : this.setState({requestData: {...this.state.requestData, search: e.target.value}})} onKeyDown={(e) => e.keyCode == 13 ? this.search() : e.keyCode == 27 ? this.clearSearch() : ""} />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} onClick={this.search} />
                                    {this.state.requestData.search === "" ? null : <span className="closeSearch" onClick={this.clearSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-7 pad-0">
                            <div className="new-gen-right">
                                <button type="button" className={this.state.selectedItems.length == 0 ? "btnDisabled" : "aprv-req-btn"} onClick={() => this.updateItems("Approved")} disabled={this.state.selectedItems.length == 0 ? "disabled" : ""}>Approve Request</button>
                                <button type="button" className={this.state.selectedItems.length == 0 ? "btnDisabled" : "reject-btn"} onClick={() => this.updateItems("Rejected")} disabled={this.state.selectedItems.length == 0 ? "disabled" : ""}>Reject Request</button>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.exportToExcel === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 21.5 17.917">
                                            <g>
                                                <path d="M8.735 12.526h1.12v2.911a.9.9 0 1 0 1.792 0v-2.911h1.116a.672.672 0 0 0 .475-1.147l-2.013-2.012a.67.67 0 0 0-.95 0L8.263 11.38a.672.672 0 0 0 .472 1.146z" data-name="Path 632" transform="translate(0 -2)" />
                                                <path d="M13.438 19.916H8.063a1.793 1.793 0 0 1-1.792-1.791v-.9a.9.9 0 0 1 1.792 0v.9h5.375v-.9a.9.9 0 0 1 1.792 0v.9a1.793 1.793 0 0 1-1.792 1.791z" data-name="Path 633" transform="translate(0 -2)" />
                                                <path d="M18.813 18.349h-1.344a.9.9 0 0 1 0-1.792h1.344a.9.9 0 0 0 .9-.9V6.479a.9.9 0 0 0-.9-.9H9.182a.892.892 0 0 1-.633-.262L7.02 3.792H2.688a.9.9 0 0 0-.9.9v10.969a.9.9 0 0 0 .9.9h1.343a.9.9 0 0 1 0 1.792H2.688A2.691 2.691 0 0 1 0 15.661V4.688A2.691 2.691 0 0 1 2.688 2h4.7a.892.892 0 0 1 .633.262l1.532 1.53h9.259A2.691 2.691 0 0 1 21.5 6.479v9.182a2.691 2.691 0 0 1-2.687 2.688z" data-name="Path 634" transform="translate(0 -2)" />
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Export Excel</span>
                                    </button>
                                    {this.state.exportToExcel &&
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button" onClick={() => this.xlscsv()}>
                                                    {/* <img src={ExportExcel} /> */}
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel</button>
                                            </li>
                                        </ul>}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        <span className="generic-tooltip">Filter</span>
                                    </button>
                                    {this.state.filter && <ArsFilter filterItems={this.state.filterItems} appliedFilters={this.state.requestData.filter} applyFilter={this.applyFilter} clearFilter={this.clearFilter} closeFilter={(e) => this.closeFilter(e)} />}
                                </div>
                                <button className="add-btn" onClick={this.createNew}><img src={AddGreen} />
                                    <span className="generic-tooltip">Add New Request</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">{
                    Object.keys(this.state.requestData.filter).length == 0 ? "" :
                    <div className="show-applied-filter">
                        <button type="button" className="saf-clear-all" onClick={this.clearFilter}>Clear All</button>
                        {
                            Object.keys(this.state.requestData.filter).map((key) =>
                                <button type="button" className="saf-btn">{this.state.filterItems[key]}
                                    <img onClick={() => this.removeFilter(key)} src={require('../../assets/clearSearch.svg')} />
                                    <span className="generic-tooltip">{key.toLowerCase().includes("date") && this.state.requestData.filter[key] !== undefined ? `From ${this.state.requestData.filter[key].from} to ${this.state.requestData.filter[key].to}` : this.state.requestData.filter[key]}</span>
                                </button>
                            )
                        }
                    </div>
                }</div>
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active" >
                                <a className="nav-link st-btn p-l-0" href="#pendingrequest" role="tab" data-toggle="tab" onClick={() => this.handleTabChange('Pending')}>Pending Request</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#approvedrequest" role="tab" data-toggle="tab" onClick={() => this.handleTabChange('Approved')}>Approved Request</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link st-btn" href="#rejectedrequest" role="tab" data-toggle="tab" onClick={() => this.handleTabChange('Rejected')}>Rejected Request</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content p-lr-47">
                        <div className="tab-pane fade in active" id="pendingrequest" role="tabpanel">
                            <AdhocPendingRequest loading={this.state.loading} error={this.state.failedToLoad} adhocData={this.state.adhocData} deleteItem={this.deleteItem} getSelectedItems={this.getSelectedItems} expandColumn={this.expandColumn} saveExpandedRow={this.updateItemsPerWO} tableHeaders={this.state.tableHeaders} expandColumnHeaders={this.state.expandColumnHeaders} sortData={this.sortData} requestData={this.state.requestData} {...this.props} />
                        </div>
                        <div className="tab-pane fade" id="approvedrequest" role="tabpanel">
                            <AdhocApprovedRequest loading={this.state.loading} error={this.state.failedToLoad} adhocData={this.state.adhocData} deleteItem={this.deleteItem} expandColumn={this.expandColumn} tableHeaders={this.state.tableHeaders} expandColumnHeaders={this.state.expandColumnHeaders} sortData={this.sortData} requestData={this.state.requestData} {...this.props} />
                        </div>
                        <div className="tab-pane fade" id="rejectedrequest" role="tabpanel">
                            <AdhocRejectedRequest loading={this.state.loading} error={this.state.failedToLoad} adhocData={this.state.adhocData} deleteItem={this.deleteItem} expandColumn={this.expandColumn} tableHeaders={this.state.tableHeaders} expandColumnHeaders={this.state.expandColumnHeaders} sortData={this.sortData} requestData={this.state.requestData} {...this.props} />
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.confirmDelete ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeConfirmDelete} action={this.props.deleteAdhocRequest} payload={this.state.payload} afterConfirm={this.afterConfirmDelete} /> : null}
                {this.state.confirmUpdate ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeConfirmUpdate} action={this.props.updateAdhocRequest} payload={this.state.payload} afterConfirm={this.afterConfirmUpdate} /> : null}
                {this.state.confirmUpdatePerWO ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeConfirmUpdatePerWO} action={this.props.updateAdhocItemsPerWORequest} payload={this.state.payload} afterConfirm={this.afterConfirmUpdatePerWO} /> : null}
            </div>
        )
    }
}

export default ManageAdhocRequest;