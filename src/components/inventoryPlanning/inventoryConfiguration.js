import React from 'react';
import Pagination from '../pagination';
import FilterLoader from "../loaders/filterLoader";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import Confirm from '../loaders/arsConfirm';
import ItemFilter from './itemFilterModal';
import LocationFilter from './locationFilterModal';
import InventorySetting from './inventorySettingModal';

class InventoryConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",
            submit: false,
            openSetting: '',
            confirmClearModal: false,
            headerMsg: '',
            paraMsg: '',

            currentTab: 'general',
            settingsValues: [], //Options that appear in the setting modal
            selectedSetting: {}, //Options that are already selected in the setting modal
            modalHeading: "",
            fieldNames: [],
            fieldValues: [],
            selectedValues: [],
            userDefinedValues: [],
            
            selectDepartment: false,
            selectDivision: false,
            selectSection: false,
            selectArticle: false,
            selectSite: false,
            settingModal: false, //get api isSuccess
        }
    }

    openSetting(e, type, heading) {
        e.preventDefault();
        this.setState({
            openSetting: type,
            modalHeading: heading,
            settingValues: [],
            selectedSetting: {}
        }, () => {
            type == "site" ? this.props.getSiteAndItemFilterRequest("site") : type == "basestock" ? this.props.getSiteAndItemFilterRequest("basestock") : this.props.getSiteAndItemFilterRequest("item");
            //this.props.getSiteAndItemFilterConfigurationRequest(type);
        });
    }

    closeSetting = () => {
        this.setState({
            settingModal: false,
            openSetting: ''
        });
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                settingsValues: Object.keys(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource) == 0 ? {} : nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter),
                //settingModal: true
            }, () => this.props.getSiteAndItemFilterConfigurationRequest(this.state.openSetting));
            this.props.getSiteAndItemFilterClear();
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorMessage
            });
            this.props.getSiteAndItemFilterClear();
        }

        if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess) {
            this.setState({
                loading: false,
                success: false,
                error: false,
                selectedSetting: Object.keys(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter) == 0 ? {} : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter)
            }, () => this.setState({settingModal: true}));
            this.props.getSiteAndItemFilterConfigurationClear();
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorMessage
            });
            this.props.getSiteAndItemFilterConfigurationClear();
        }

        if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.data.resource.message,
                error: false
            });
            //this.props.saveSiteAndItemFilterClear();
        }
        else if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorMessage
            });
            //this.props.saveSiteAndItemFilterClear();
        }

        if(nextProps.seasonPlanning.getSiteAndItemFilter.isLoading || nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isLoading ||  nextProps.seasonPlanning.saveSiteAndItemFilter.isLoading) {
            this.setState({
                loading: true,
                success: false,
                error: false
            });
        }
    }

    setSubmit = () => {
        this.setState({submit: true});
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false,
            openSetting: '',
            settingModal: false
        }, () => {

        });
        //if(this.state.currentTab == 'item' || this.state.currentTab == 'site' ) this.props.getItemConfigurationRequest(this.state.currentTab);
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false,
            openSetting: '',
            settingModal: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    
    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }
    
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }
    
    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop"))){
            this.setState({ settingModal: false, })
        }
    }

    render() {
        console.log(this.state.selectedValues);
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="subscription-tab procurement-setting-tab">
                        <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                            <li className="nav-item active">
                                <a className="nav-link st-btn p-l-0" href="#general" role="tab" data-toggle="tab">General Configuration</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="col-lg-12 p-lr-47">
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="general" role="tabpanel">
                            <div className="inventory-configuration-filter m-top-30">
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">
                                            <h3>Item Filters</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "item", "Item Filters")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Selected attributes will be presented as filters, these filters will be presented at different pages.</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Site Filters</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "site", "Site Filters")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Selected attributes will be presented as filters, these filters will be presented at different pages.</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Inventory Planning Report Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "basestock", "Inventory Planning Report Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Inventory Planning Report in Analytics, same group will be considered for your base stock mapping.</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Sales Inventory Report Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "saleinventory", "Sales Inventory Report Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Sales Inventory Report in Analytics Section.</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">
                                            <h3>Sales Contribution Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "saleContribution", "Sales Contribution Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Sales Contribution Report in Reports, same group will be considered for your sales contribution mapping.</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Sales Comparison Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "saleComparision", "Sales Comparison Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Sales Comparison Report in Reports, same group will be considered for your sales comparison mapping.</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Sell Thru Performance Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "sellThruPerformance", "Sell Thru Performance Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Sell Thru Performance Report in Reports, same group will be considered for your sell thru performance mapping.</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Top Moving Items Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "topMovingItems", "Top Moving Items Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Top Moving Items Report in Reports, same group will be considered for your top moving items mapping.</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-3 pad-lft-0">
                                    <div className="icf-item-filter">
                                        <div className="icfif-head">    
                                            <h3>Category Size Wise Mapping</h3>
                                            <button type="button" className="icfif-setting" onClick={(e) => this.openSetting(e, "categorySizeWise", "Category Size Wise Mapping")}>Edit</button>
                                        </div>
                                        <div className="icfif-description">
                                            <p>Description :</p>
                                            <span className="icfifd-text">Select attributes from the given list to generate Category Size Wise Report in Reports, same group will be considered for your category size wise mapping.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.settingModal && <InventorySetting closeSetting={this.closeSetting} data={this.state.settingsValues} selectedData={this.state.selectedSetting} save={this.props.saveSiteAndItemFilterRequest} type={this.state.openSetting} heading={this.state.modalHeading} setSubmit={this.setSubmit} />}
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.confirmClearModal ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeModal} afterConfirm={this.confirmClear} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}

export default InventoryConfiguration;