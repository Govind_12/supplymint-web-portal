import React from 'react';
import Reload from '../../assets/reload.svg';
import Pagination from '../pagination';
import ToastLoader from '../loaders/toastLoader';
import FilterLoader from "../loaders/filterLoader";
import Confirm from "../loaders/arsConfirm";
import RequestError from "../loaders/requestError";
import RequestSuccess from "../loaders/requestSuccess";
import GenericDataModal from '../replenishment/genericDataModal';
import InventorySetting from './inventorySettingModal';

class ArsInventoryReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showFilters: false,
            exportToExcel: false,
            loading: true,
            confirmDelete: false,
            submit: false,
            headerMsg: '',
            paraMsg: '',
            success: false,
            successMessage: "",
            error: false,
            code: "",
            errorCode: "",
            errorMessage: "",

            type: 1,
            prev: '',
            current: 1,
            next: '',
            maxPage: 0,
            totalItems: 0,
            jumpPage: 1,
            toastMsg: "",
            toastLoader: false,

            header: [],
            data: [],
            growth: 10,
            totalSum: {},

            genericDataModalShow: false,
            genericDataModalShowRow: {},
            genericSearchData: {},
            genericSearchRefs: {},
            selectedItems: {},
            filters: {},
            fromDate: "",
            toDate: "",
            search: "",
            sortedBy: "",
            sortedIn: "DESC",

            //CONFIGURATION
            settingsValues: [],
            selectedSetting: {},
            openSetting: '',
            modalHeading: ""
        }
    }

    componentDidMount() {
        this.props.getSalesInventoryReportRequest({pageNo: 1, type: 1});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.seasonPlanning.getSalesInventoryReport.isSuccess) {
            if (nextProps.seasonPlanning.getSalesInventoryReport.data.resource !== null) {
                this.setState({
                    loading: false,
    
                    prev: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.prevPage,
                    current: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.currPage,
                    next: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.currPage + 1,
                    maxPage: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.maxPage,
                    totalItems: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.totalCount,
                    jumpPage: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.currPage,
    
                    header: Object.keys(nextProps.seasonPlanning.getSalesInventoryReport.data.resource.headers).length == 0 ? [] : JSON.parse(nextProps.seasonPlanning.getSalesInventoryReport.data.resource.headers),
                    data: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.data,
                    fromDate: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.filterDates === undefined || nextProps.seasonPlanning.getSalesInventoryReport.data.resource.filterDates === null ? "" : nextProps.seasonPlanning.getSalesInventoryReport.data.resource.filterDates.fromDate,
                    toDate: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.filterDates === undefined || nextProps.seasonPlanning.getSalesInventoryReport.data.resource.filterDates === null ? "" : nextProps.seasonPlanning.getSalesInventoryReport.data.resource.filterDates.toDate,
                    totalSum: nextProps.seasonPlanning.getSalesInventoryReport.data.resource.totalSum === null ? {} : nextProps.seasonPlanning.getSalesInventoryReport.data.resource.totalSum
                });
            }
            this.props.getSalesInventoryReportClear();
        }
        else if (nextProps.seasonPlanning.getSalesInventoryReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSalesInventoryReport.message.status,
                errorCode: nextProps.seasonPlanning.getSalesInventoryReport.message.error == undefined ? undefined : nextProps.seasonPlanning.getSalesInventoryReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSalesInventoryReport.message.error == undefined ? undefined : nextProps.seasonPlanning.getSalesInventoryReport.message.error.errorMessage,

                prev: '',
                current: '',
                next: '',
                maxPage: '',
                jumpPage: '',

                header: [],
                data: [],
                totalSum: {}
            }, this.props.getSalesInventoryReportClear);
        }

        if(nextProps.seasonPlanning.getArsGenericFilters.isSuccess) {
            this.setState({
                loading: false,
                genericSearchData: {...this.state.genericSearchData, data: nextProps.seasonPlanning.getArsGenericFilters.data},
                genericDataModalShow: true
            });
            this.props.getArsGenericFiltersClear();
        }
        else if (nextProps.seasonPlanning.getArsGenericFilters.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getArsGenericFilters.message.status,
                errorCode: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getArsGenericFilters.message.error == undefined ? undefined : nextProps.seasonPlanning.getArsGenericFilters.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.getArsGenericFiltersClear();
        }

        if (nextProps.seasonPlanning.downloadReport.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.downloadReport.data.message,
                error: false
            });
            window.location.href = nextProps.seasonPlanning.downloadReport.data.resource;
            this.props.downloadReportClear();
        }
        else if (nextProps.seasonPlanning.downloadReport.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.downloadReport.message.status,
                errorCode: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.downloadReport.message.error == undefined ? undefined : nextProps.seasonPlanning.downloadReport.message.error.errorMessage,
                //confirmDelete: false
            });
            this.props.downloadReportClear();
        }

        if(nextProps.seasonPlanning.saveSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.data.resource.message,
                error: false
            });
            this.props.getSalesInventoryReportRequest({
                pageNo: this.state.current,
                type: this.state.type,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn,
                search: this.state.search,
                filter: this.state.filters
            });
            this.props.saveSiteAndItemFilterClear();
        }
        else if(nextProps.seasonPlanning.saveSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorMessage
            });
            this.props.saveSiteAndItemFilterClear();
        }

        if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                settingsValues: Object.keys(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource) == 0 ? {} : nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilter.data.resource.itemFilter),
                //settingModal: true
            }, () => this.props.getSiteAndItemFilterConfigurationRequest(this.state.openSetting));
            this.props.getSiteAndItemFilterClear();
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilter.message.error.errorMessage
            });
            this.props.getSiteAndItemFilterClear();
        }

        if(this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isSuccess) {
            this.setState({
                loading: false,
                selectedSetting: Object.keys(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter) == 0 ? {} : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter == undefined ? JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.siteFilter) : JSON.parse(nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.data.resource.itemFilter)
            }, () => this.setState({settingModal: true}));
            this.props.getSiteAndItemFilterConfigurationClear();
        }
        else if (this.state.openSetting != '' && nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.status,
                errorCode: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error == undefined ? undefined : nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.message.error.errorMessage
            });
            this.props.getSiteAndItemFilterConfigurationClear();
        }

        if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isSuccess) {
            this.setState({
                loading: false,
                success: true,
                successMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.data.resource.message,
                error: false
            });
            this.props.getSalesInventoryReportRequest({
                pageNo: this.state.current,
                type: this.state.type,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn,
                search: this.state.search,
                filter: this.state.filters
            });
            this.props.saveSiteAndItemFilterClear();
        }
        else if(this.state.submit && nextProps.seasonPlanning.saveSiteAndItemFilter.isError) {
            this.setState({
                loading: false,
                success: false,
                error: true,
                code: nextProps.seasonPlanning.saveSiteAndItemFilter.message.status,
                errorCode: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorCode,
                errorMessage: nextProps.seasonPlanning.saveSiteAndItemFilter.message.error == undefined ? undefined : nextProps.seasonPlanning.saveSiteAndItemFilter.message.error.errorMessage
            });
            this.props.saveSiteAndItemFilterClear();
        }

        if (nextProps.seasonPlanning.getSalesInventoryReport.isLoading || nextProps.seasonPlanning.getArsGenericFilters.isLoading || nextProps.seasonPlanning.downloadReport.isLoading || nextProps.seasonPlanning.saveSiteAndItemFilter.isLoading || nextProps.seasonPlanning.getSiteAndItemFilter.isLoading || nextProps.seasonPlanning.getSiteAndItemFilterConfiguration.isLoading) {
            this.setState({
                loading: true
            });
        }
    }

    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        }, () => document.addEventListener('click', this.closeExportToExcel));
    }
    closeExportToExcel = () => {
        this.setState({ exportToExcel: false }, () => {
            document.removeEventListener('click', this.closeExportToExcel);
        });
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            showFilters: !this.state.showFilters
        });
    }

    page = (e) => {
        let payload = {
            type: this.state.type,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn,
            search: this.state.search,
            filter: this.state.filters
        };

        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.seasonPlanning.getSalesInventoryReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSalesInventoryReport.data.resource.prevPage != 0) {
                    this.props.getSalesInventoryReportRequest({pageNo: this.props.seasonPlanning.getSalesInventoryReport.data.resource.prevPage, ...payload});
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.seasonPlanning.getSalesInventoryReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage
                })
            if (this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage != this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage) {
                this.props.getSalesInventoryReportRequest({pageNo: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage + 1, ...payload});
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getSalesInventoryReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage <= this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage) {
                    this.props.getSalesInventoryReportRequest({pageNo: 1, ...payload});
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.seasonPlanning.getSalesInventoryReport.data.resource.prevPage,
                    current: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage,
                    next: this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage + 1,
                    maxPage: this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage
                })
                if (this.props.seasonPlanning.getSalesInventoryReport.data.resource.currPage <= this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage) {
                    this.props.getSalesInventoryReportRequest({pageNo: this.props.seasonPlanning.getSalesInventoryReport.data.resource.maxPage, ...payload});
                }
            }
        }
    }

    getAnyPage = _ => {
        if (_.target.validity.valid) {
            this.setState({ jumpPage: _.target.value })
            if (_.key == "Enter" && _.target.value != this.state.current) {
                if (_.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        type: this.state.type,
                        sortedBy: this.state.sortedBy,
                        sortedIn: this.state.sortedIn,
                        search: this.state.search,
                        filter: this.state.filters
                    }
                    this.props.getSalesInventoryReportRequest(payload);
                }
                else {
                    this.setState({
                        toastMsg: "Page number can not be empty!",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 3000);
                }
            }
        }
    }

    siteData = (e, entity, key, search, pageNo) => {
        if (e.keyCode == 13) {
            this.openGenericDataModal(entity, key, search, pageNo);
        }
    }

    openGenericDataModal = (entity, key, search, pageNo) => {
        if (this.state.genericDataModalShowRow != key) this.closeGenericDataModal();
        if (key === "name1") {
            this.props.getArsGenericFiltersRequest({
                entity: entity,
                key: "name1",
                code: "site_code",
                search: search,
                pageNo: pageNo
            });
        }
        else {
            this.props.getArsGenericFiltersRequest({
                entity: entity,
                key: key,
                search: search,
                pageNo: pageNo
            });
        }
        this.setState({genericSearchData: {payload: {entity: entity, key: key, search: search}}, genericDataModalShowRow: key});
    }

    closeGenericDataModal = () => {
        let newRefs = this.state.genericSearchRefs;
        if (newRefs[this.state.genericDataModalShowRow] !== undefined) {
            newRefs[this.state.genericDataModalShowRow].current.value = "";
            this.setState({
                genericDataModalShowRow: '',
                genericDataModalShow: false,
                genericSearchRefs: newRefs
            });
        }
    }

    selectItems = (entity, key, items) => {
        let updateItems = {...this.state.selectedItems};
        if (items.length !== 0) {
            updateItems[key] = items;
        }
        else if (items.length === 0 && updateItems[key] !== undefined) {
            delete updateItems[key];
        }
        this.setState({
            selectedItems: updateItems
        });
    }

    submitFilters = () => {
        let filters = {};
        if(Object.keys(this.state.selectedItems).length != 0) {
            Object.keys(this.state.selectedItems).forEach((key) => {
                if(this.state.selectedItems[key].length != 0) filters[key] = this.state.selectedItems[key].join();
            });
        }
        this.props.getSalesInventoryReportRequest({
            search: this.state.search,
            filter: filters,
            pageNo: 1,
            type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({type: this.state.type == 1 ? 2 : this.state.type == 3 ? 4 : this.state.type, filters: filters});
    }

    clearFilters = () => {
        this.props.getSalesInventoryReportRequest({
            pageNo: 1,
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type,
            filter: {},
            search: this.state.search,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            // fromDate: "",
            // toDate: "",
            selectedItems: {},
            filters: {},
            type: this.state.type == 2 ? 1 : this.state.type == 4 ? 3 : this.state.type
        });
    }

    search = () => {
        if (this.state.search !== "") {
            this.props.getSalesInventoryReportRequest({
                pageNo: 1,
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type,
                search: this.state.search,
                filter: this.state.filters,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn
            });
            this.setState({
                type: this.state.type == 1 ? 3 : this.state.type == 2 ? 4 : this.state.type
            });
        }
    }

    clearSearch = () => {
        this.props.getSalesInventoryReportRequest({
            pageNo: 1,
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type,
            search: "",
            filter: this.state.filters,
            sortedBy: this.state.sortedBy,
            sortedIn: this.state.sortedIn
        });
        this.setState({
            search: "",
            type: this.state.type == 3 ? 1 : this.state.type == 4 ? 2 : this.state.type
        });
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            success: false
        }, () => {

        });
        this.closeSetting();
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            submit: false,
            error: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    downloadReport = () => {
        // let filters = {};
        // if(Object.keys(this.state.selectedItems).length != 0) {
        //     Object.keys(this.state.selectedItems).forEach((key) => {
        //         if(this.state.selectedItems[key].length != 0) filters[key] = this.state.selectedItems[key].join();
        //     });
        // }
        this.props.downloadReportRequest({
            module: "SALES_INVENTORY_REPORT",
            data: {
                pageNo: 1,
                type: this.state.type,
                search: this.state.search,
                sortedBy: this.state.sortedBy,
                sortedIn: this.state.sortedIn,
                filter: this.state.filters,
                filterDate: {
                    fromDate: this.state.fromDate,
                    toDate: this.state.toDate,
                    ...this.state.totalSum
                }
            }
        });
    }

    sortData = (key) => {
        let payload = {
            pageNo: this.state.current,
            type: this.state.type,
            search: this.state.search,
            filter: this.state.filters,
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        };
        this.props.getSalesInventoryReportRequest(payload);
        this.setState({
            sortedBy: key,
            sortedIn: this.state.sortedBy == key && this.state.sortedIn === "ASC" ? "DESC" : "ASC"
        });
    }

    onReload = () => {
        this.props.getSalesInventoryReportRequest({pageNo: 1, type: 1});
        this.setState({
            type: 1,
            search: "",
            // fromDate: "",
            // toDate: "",
            selectedItems: {},
            filters: {},
            sortedBy: "",
            sortedIn: "DESC"
        });
    }

    generateReport = () => {
        this.props.saveSiteAndItemFilterRequest({
            filterType: "saleinventory",
            date: {
                fromDate: this.state.fromDate,
                toDate: this.state.toDate
            }
        });
    }

    openSetting(e, type, heading) {
        e.preventDefault();
        this.setState({
            openSetting: type,
            modalHeading: heading,
            settingValues: [],
            selectedSetting: {}
        }, () => {
            type == "site" ? this.props.getSiteAndItemFilterRequest("site") : type == "basestock" ? this.props.getSiteAndItemFilterRequest("basestock") : this.props.getSiteAndItemFilterRequest("item");
            //this.props.getSiteAndItemFilterConfigurationRequest(type);
        });
    }

    closeSetting = () => {
        this.setState({
            settingModal: false,
            openSetting: ''
        });
    }

    setSubmit = () => {
        this.setState({submit: true});
    }

    render () {
        return (
            <div className="container-fluid pad-0 pad-l50">
                <div className="col-lg-12 pad-0">
                    <div className="otb-reports-filter p-lr-47">
                        <div className="orf-head">
                            <div className="orfh-left">
                                <div className="orfhl-inner">
                                    <h3 className={Object.keys(this.state.filters).length !== 0 && this.state.showFilters === false ? "h3After" : ""}>Filters</h3>
                                    {/* {this.state.showFilters === false ? <span>{Object.keys(this.state.filters).length} filters applied</span>: null} */}
                                </div>
                                {this.state.showFilters === false && (Object.keys(this.state.filters).length !== 0) ? <button type="button" onClick={this.clearFilters}>Remove</button>: null}
                            </div>
                            <div className="orfh-rgt">
                                <button className={this.state.showFilters === false ? "" : "orfh-btn"} onClick={(e) => this.openFilter(e)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.806" height="8.403" viewBox="0 0 14.806 8.403">
                                        <path id="Path_943" fill="none" stroke="#3a5074" strokeLinecap="round" strokeLinecap="round" strokeWidth="2px" d="M6 9l5.989 5.989L17.978 9" data-name="Path 943" transform="translate(-4.586 -7.586)"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        {this.state.showFilters && <div className="orf-body m-top-10">
                            <form>
                                <h3>Sales Date Range</h3>
                                <div className="col-lg-12 pad-0">
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>From Date</label>
                                        <input type="date" className="onFocus" placeholder={this.state.fromDate === "" ? "DD-MM-YYYY" : this.state.fromDate} value={this.state.fromDate} onChange={(e) => this.setState({fromDate: e.target.value})} />
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15">
                                        <label>To Date</label>
                                        <input type="date" className="onFocus" placeholder={this.state.toDate === "" ? "DD-MM-YYYY" : this.state.toDate} value={this.state.toDate} onChange={(e) => this.setState({toDate: e.target.value})} />
                                    </div>
                                    <div className="col-lg-2 pad-0 pad-r15 m-top-25">
                                        <div className="orfb-submit-btn">
                                            <button type="button" className={this.state.fromDate === "" || this.state.toDate === "" ? "orfbab-submit btnDisabled" : "orfbab-submit"} disabled={this.state.fromDate === "" || this.state.toDate === "" ? "disabled" : ""} onClick={this.generateReport}>Generate</button>
                                        </div>
                                    </div>
                                </div>
                                {
                                    this.state.header.map((item) => {
                                        item.isFilter == "Y" ? this.state.genericSearchRefs[item.key] = React.createRef() : "";
                                        return (
                                            item.isFilter == "Y" ?
                                            <div className="col-lg-2 pad-lft-0 m-top-20">
                                                <label className="pnl-purchase-label">{item.value}</label>
                                                <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input"
                                                        id={item.key + "Focus"}
                                                        placeholder={this.state.selectedItems[item.key] === undefined || this.state.selectedItems[item.key].length === 0 ? "No " + item.value + " selected" : this.state.selectedItems[item.key].length === 1 ? this.state.selectedItems[item.key][0] + " selected" : this.state.selectedItems[item.key][0] + " + " + (this.state.selectedItems[item.key].length - 1) + " selected"}
                                                        ref={this.state.genericSearchRefs[item.key]}
                                                        onFocus={(e) => {if (this.state.genericDataModalShow === false || this.state.genericDataModalShowRow != item.key) this.openGenericDataModal(item.entity, item.key, e.target.value, 1)}}
                                                        //onBlur={(e) => {if (this.state.genericDataModalShowRow != item.key) e.target.value=""}}
                                                        onKeyDown={(e) => this.siteData(e, item.entity, item.key, e.target.value, 1)}
                                                    />
                                                    {/* {(Object.keys(this.state.filters).length !== 0) ? <span className="closeSearch newCloseSearch" onClick={this.clearFilters}><img src={require('../../assets/clearSearch.svg')} /></span> : */}
                                                    <span className="modal-search-btn" onClick={(e) => this.openGenericDataModal(item.entity, item.key, this.state.genericSearchRefs[item.key].current.value, 1)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                                        </svg>
                                                    </span>
                                                    {/* } */}
                                                    {this.state.genericDataModalShow && this.state.genericDataModalShowRow == item.key ? <GenericDataModal data={this.state.genericSearchData} action={this.props.getArsGenericFiltersRequest} close={this.closeGenericDataModal} select={this.selectItems} selectedItems={this.state.selectedItems[item.key]} /> : null}
                                                </div>
                                            </div> :
                                            null
                                        )
                                    })
                                }
                                <div className="col-lg-12 pad-0 m-top-30">
                                    <div className="orfb-submit-btn">
                                        <button type="button" className={Object.keys(this.state.selectedItems).length == 0 ? "orfbab-submit btnDisabled" : "orfbab-submit"} disabled={Object.keys(this.state.selectedItems).length == 0 ? "disabled" : ""} onClick={this.submitFilters}>Submit</button>
                                        <button type="button" className="" onClick={this.clearFilters}>Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>}
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="otb-reports-content">
                        <div className="otbrc-left">
                            <div className="otbrc-search">
                            <input type="search" placeholder="Type To Search" value={this.state.search} onChange={(e) => e.target.value == "" ? this.clearSearch() : this.setState({search: e.target.value})} onKeyDown={(e) => e.keyCode == 13 ? this.search() : e.keyCode == 27 ? this.clearSearch() : ""} />
                                <img className="search-image" src={require('../../assets/searchicon.svg')} onClick={this.search} />
                                {this.state.search == "" ? null : <span className="closeSearch" onClick={this.clearSearch}><img src={require('../../assets/clearSearch.svg')} /></span>}
                            </div>
                        </div>
                        <div className="otbrc-right">
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-20">
                    <div className="otb-reports-content">
                        <div className="otbrc-left">
                            <div className="otbrc-details">
                                <div className="otbrcd-col">
                                    <p>Sales Qty</p>
                                    <span>{this.state.totalSum.totalsaleqty !== undefined && this.state.totalSum.totalsaleqty !== null ? this.state.totalSum.totalsaleqty : "NA"}</span>
                                </div>
                                <div className="otbrcd-col">
                                    <p>Stock Qty</p>
                                    <span>{this.state.totalSum.totalstockqty !== undefined && this.state.totalSum.totalstockqty !== null ? this.state.totalSum.totalstockqty : "NA"}</span>
                                </div>
                            </div>
                        </div>
                        <div className="otbrc-right">
                            <button type="button" className="otbrc-setting" onClick={(e) => this.openSetting(e, "saleinventory", "Sales Inventory Report Mapping")}>
                                <svg id="list-settings-line" xmlns="http://www.w3.org/2000/svg" width="18.051" height="18.051" viewBox="0 0 21.051 21.051">
                                    <path id="Path_1617" data-name="Path 1617" d="M0,0H21.051V21.051H0Z" fill="none"/>
                                    <path className="otbrc-fill-2" id="Path_1618" data-name="Path 1618" d="M2,16.28H8.14v1.754H2Zm0-6.14H9.894v1.754H2ZM2,4H19.543V5.754H2Zm16.38,7.916,1.014-.343.877,1.519-.8.706a3.524,3.524,0,0,1,0,1.454l.8.706-.877,1.519-1.014-.343a3.5,3.5,0,0,1-1.259.728l-.21,1.048H15.157l-.211-1.049a3.505,3.505,0,0,1-1.258-.728l-1.014.344L11.8,15.959l.8-.706a3.524,3.524,0,0,1,0-1.454l-.8-.706.877-1.519,1.014.343a3.5,3.5,0,0,1,1.259-.728l.21-1.048h1.754l.211,1.049a3.487,3.487,0,0,1,1.258.728ZM16.034,16.28a1.754,1.754,0,1,0-1.754-1.754A1.754,1.754,0,0,0,16.034,16.28Z" transform="translate(-0.246 -0.491)"/>
                                </svg>  
                                <span className="generic-tooltip">Configuration</span>
                            </button>
                            <div className="gvpd-filter">
                                <button type="button" className="gvpd-filter-inner">
                                    {/* <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg> */}
                                    <img src={Reload} onClick={this.onReload} />
                                    <span className="generic-tooltip">Refresh</span>
                                </button>
                                {this.state.filter && <VendorFilter />}
                            </div>
                            <button type="button" className="otbrc-email">Email
                                <span>
                                    <img src={require('../../assets/email.svg')} />
                                </span>
                            </button>
                            <div className="gvpd-download-drop">
                                <button type="button" className="otbrc-down-report" onClick={this.downloadReport}>Download Report
                                    <span className="otbrcdr-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="vendor-gen-table vgtbg-fcfdfe">
                        <div className="manage-table">
                            <div className="columnFilterGeneric">
                                <span className="glyphicon glyphicon-menu-left"></span>
                            </div>
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        {/* <th><label>Site</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Division</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Section</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Department</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>MRP</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Vendor</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Cat1</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Cat2</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Cat3</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Cat4</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Cat5</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Cat6</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Desc1</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Desc2</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Desc3</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Desc4</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Desc5</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Desc6</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Base Stock</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Current Stock </label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Sales Qty</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Growth /De-growth %</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Forecasted Qty</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        <th><label>Requirement Stock or OverStock</label><img src={require('../../assets/headerFilter.svg')}/></th> */}
                                    {
                                        this.state.header.length != 0 ?
                                        this.state.header.map((item) => (
                                            <th key={item.key} className={this.state.sortedBy == item.key && this.state.sortedIn == "ASC" ? "rotate180" : ""} onClick={() => this.sortData(item.key)}><label>{item.value}</label><img src={require('../../assets/headerFilter.svg')}/></th>
                                        )) :
                                        <th></th>
                                    }    
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.data.length == 0 ?
                                    <tr style={{textAlign: "center"}}><td><label>NO DATA FOUND!</label></td></tr> :
                                    this.state.data.map((item, index) => (
                                        <tr>{
                                            this.state.header.map((innerItem) => (
                                                // innerItem.key == "sale_qty" && item.sale_qty == null ?
                                                // <td><label>{0}</label></td> :
                                                // innerItem.key == "stock_qty" && item.stock_qty == null ?
                                                // <td><label>{0}</label></td> :
                                                <td><label>{item[innerItem.key]}</label></td>
                                        ))}</tr>
                                    ))
                                }
                                    {/* <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr>
                                    <tr>
                                        <td><label className="bold">Site1</label></td>
                                        <td><label>MEN</label></td>
                                        <td><label>Lower</label></td>
                                        <td><label>Jeans</label></td>
                                        <td><label>598</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><label>All</label></td>
                                        <td><input type="text" value="500" /></td>
                                        <td><input type="text" value="400" /></td>
                                        <td><input type="text" value="100" /></td>
                                        <td><input type="text" value="10%" /></td>
                                        <td><input type="text" value="110" /></td>
                                        <td><input type="text" value="210" /></td>
                                    </tr> */}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalItems}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination {...this.state} {...this.props} page={this.page}
                                            prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loading ? <FilterLoader /> : null}
                {this.state.settingModal && <InventorySetting closeSetting={this.closeSetting} data={this.state.settingsValues} selectedData={this.state.selectedSetting} save={this.props.saveSiteAndItemFilterRequest} type={this.state.openSetting} heading={this.state.modalHeading} setSubmit={this.setSubmit} />}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.error ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {/* {this.state.confirmDelete ? <Confirm headerMsg={this.state.headerMsg} paraMsg={this.state.paraMsg} closeConfirm={this.closeConfirmDelete} action={this.props.deleteSeasonPlanningRequest} payload={this.state.selectedItems} afterConfirm={this.afterConfirmDelete} /> : null} */}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default ArsInventoryReport;