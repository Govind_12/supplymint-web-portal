import React from 'react';

class ExpandAdhocRejectedRequest extends React.Component {
    render () {
        return (
            <div className="gen-sticky-tab">
                <div className="gen-sticky-tab-inner">
                    <table className="table">
                        <thead>
                            <tr>{
                                Object.keys(this.props.headers).map((key) => <th><label>{this.props.headers[key]}</label></th>)
                            }</tr>
                        </thead>
                        <tbody>{
                            this.props.data.map((item) => (
                                <tr>{
                                    Object.keys(this.props.headers).map((key) => <td><label>{item[key]}</label></td>)
                                }</tr>
                            ))
                        }</tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default ExpandAdhocRejectedRequest;