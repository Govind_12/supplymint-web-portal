import React from 'react';

export default class CustomerAddUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            errorMessage: "",
            successMessage: "",
            first: "",
            middle: "",
            last: "",
            user: "",
            email: "",
            mobile: "",
            active: "Active",
            phone: "",
            access: "UI",
            address: "",
            firsterr: false,
            errorCode: "",
            // middleerr: false,
            lasterr: false,
            usererr: false,
            emailerr: false,
            mobileerr: false,
            activeerr: false,
            phoneerr: false,
            // accesserr: false,
            // addresserr: false,
            data: [
                { "id": 0, "text": "Administartion" },
                { "id": 1, "text": "Manage Role" },
                { "id": 2, "text": "Admin" },
                { "id": 3, "text": "Manage Site" },
                { "id": 4, "text": "Site Mapping" },
                { "id": 5, "text": "Site" }
            ],
            roleData: [],
            selected: [],
            selectederr: "",
            current: "",
            open: false,
            search: "",
            loader: false,
            success: false,
            alert: false,
            rightbar: false,
            openRightBar: false,
            code: ""
        }
    }

    componentDidMount() {
        this.props.allRetailerCustomerRequest()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.allRetailerCustomer.isSuccess) {
            this.setState({ roleData: nextProps.administration.allRetailerCustomer.data.resource.length > 0 ? nextProps.administration.allRetailerCustomer.data.resource : [] })
            this.props.allRetailerCustomerClear()
        }
        if (nextProps.administration.addUserCustomer.isSuccess) {
            this.props.handleAddUserModal();
            this.props.addUserCustomerClear();
            let data = {
                type: 1,
                no: 1,
            }
            this.props.customerUserRequest(data);
        }
    }
    onClear() {
        this.setState({
            first: "",
            search: "",
            middle: "",
            last: "",
            user: "",
            email: "",
            mobile: "",
            active: "",
            phone: "",
            access: "UI",
            address: "",
            selected: [],
            roleData: this.state.roleData.concat(this.state.selected)
        })
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    onDelete =(e)=> {
        let match = this.state.selected.filter( data => data.id == e )
        this.setState(preState=>({
            roleData: preState.roleData.concat(match)
        }));

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            //open: false,
        })
    }
    Change(e) {
        this.setState({
            search: e.target.value
        })
    }

    onnChange =(e)=> {
        e.preventDefault();
        this.setState({
            current: e.target.value,
        })

        let match = this.state.roleData.filter( data => 
            data.id == e.target.value
        )
        this.setState({
            selected: this.state.selected.concat(match)
        });
        let idd = this.state.roleData.filter( data => data.id == e.target.value)[0] !== undefined ? this.state.roleData.filter( data => data.id == e.target.value)[0].id
                  : "";
        var array = this.state.roleData;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            roleData: array,
            selectederr: false,
            //open: false,
        })

    }

    handleChange(e) {
        e.preventDefault();
        if (e.target.id == "first") {
            this.setState(
                {
                    first: e.target.value
                },
                () => {
                    this.first();
                }
            );
        } else if (e.target.id == "last") {
            this.setState(
                {
                    last: e.target.value
                },
                () => {
                    this.last();
                }
            );
        } else if (e.target.id == "middle") {
            this.setState(
                {
                    middle: e.target.value
                },
                () => {
                    this.middle();
                }
            );
        } else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );
        } else if (e.target.id == "mobile") {
            this.setState(
                {
                    mobile: e.target.value
                },
                () => {
                    this.mobile();
                }
            );
        } else if (e.target.id == "phone") {
            this.setState(
                {
                    phone: e.target.value
                },
                () => {
                    this.phone();
                }
            );
        } else if (e.target.id == "access") {
            this.setState(
                {
                    access: e.target.value
                },
                () => {
                    // this.access();
                }
            );
        } else if (e.target.id == "address") {
            this.setState(
                {
                    address: e.target.value
                },
                () => {
                    // this.address();
                }
            );
        } else if (e.target.id == "active") {
            this.setState(
                {
                    active: e.target.value
                },
                () => {
                    this.active();
                }
            );
        } else if (e.target.id == "user") {
            this.setState(
                {
                    user: e.target.value
                },
                () => {
                    this.user(e);
                }
            );
        }
    }
    first() {
        if (
            this.state.first == "" || this.state.first.trim() == "" || !this.state.first.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                firsterr: true
            });
        } else {
            this.setState({
                firsterr: false
            });
        }
    }
    middle() {
        if (this.state.middle == "") {
            this.setState({
                middleerr: false
            })
        }
        else if (!this.state.middle.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                middleerr: true
            });
        }
    }
    active() {
        if (
            this.state.active == "" || this.state.active.trim() == "") {
            this.setState({
                activeerr: true
            });
        } else {
            this.setState({
                activeerr: false
            });
        }
    }
    access() {
        if (
            this.state.access == "" || this.state.access.trim() == "") {
            this.setState({
                accesserr: true
            });
        } else {
            this.setState({
                accesserr: false
            });
        }
    }
    email() {
        if (
            this.state.email == "" || !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )) {
            this.setState({
                emailerr: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }

    phone() {
        if (this.state.phone == "" || this.state.phone.trim() == "") {
            this.setState({
                phoneerr: false
            })
        }
        else if (this.state.phone.length < 10) {
            this.setState({
                phoneerr: true
            });
        }
        else {
            this.setState({
                phoneerr: false
            });
        }
    }
    mobile() {
        if (
            this.state.mobile == "" || !this.state.mobile.match(/^\d{10}$/) ||
            !this.state.mobile.match(/^[1-9]\d+$/) ||
            !this.state.mobile.match(/^.{10}$/)) {
            this.setState({
                mobileerr: true
            });
        } else {
            this.setState({
                mobileerr: false
            });
        }
    }
    address() {
        if (
            this.state.address == "" || this.state.address.trim() == "") {
            this.setState({
                addresserr: true
            });
        } else {
            this.setState({
                addresserr: false
            });
        }
    }
    user(e) {
        if (
            this.state.user.includes(" ") || this.state.user == "" || this.state.user.trim() == "" || !this.state.user.match(/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/)) {
            this.setState({
                usererr: true
            });
        } else {
            this.setState({
                usererr: false
            });
        }
    }
    last() {
        if (
            this.state.last == "" || this.state.last.trim() == "" || !this.state.last.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                lasterr: true
            });
        } else {
            this.setState({
                lasterr: false
            });
        }
    }
    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true
            });
        } else {
            this.setState({
                selectederr: false
            })
        }
    }
    onFormSubmit(e) {
        e.preventDefault();
        this.first();
        this.last();
        this.middle();
        this.email();
        this.phone();
        this.mobile();
        // this.access();
        this.active();
        // this.address();
        this.selected();
        this.user();
        const t = this;
        var uType = sessionStorage.getItem('uType')
        setTimeout(function () {
            const { selected, first, firsterr, middle, middleerr, last, lasterr, email, emailerr, mobile, mobileerr, phone, phoneerr, access, accesserr, active, activeerr, address, addresserr, user, usererr, selectederr } = t.state;
            if (!selectederr && !firsterr && !middleerr && !lasterr && !emailerr && !mobileerr && !phoneerr && !activeerr && !usererr) {
                let data = {
                    first: first,
                    middle: middle,
                    last: last,
                    email: email,
                    mobile: mobile,
                    phone: phone,
                    access: access == "" ? "NA" : access,
                    active: active,
                    address: address == "" ? "NA" : address,
                    user: user,
                    selected: selected,
                    uType,
                    userAsAdmin: false
                }
                t.props.addUserCustomerRequest(data);
            }
        }, 100)
    }
    onRightSideBar() {
        this.setState({
            openRightBar: true,
            rightbar: !this.state.rightbar
        });
    }
    openClose(e) {
        e.preventDefault();
        this.setState({
            open: !this.state.open
        },()=>{
            if(this.state.open)
             document.addEventListener("click", this.closeRoleModal)
            else
              document.removeEventListener("click", this.closeRoleModal)
        })
    }

    closeRoleModal =(e)=>{
        if( e !== null && e.target !== undefined && !e.target.parentElement.className.includes("dropdownDiv")){
            this.setState({ open: false},()=>{
               document.removeEventListener("click", this.closeRoleModal)
            })
        }
    }

    render() {
        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        $("body").on("keyup", ".numbersOnlywithhighpn", function () {
            if (this.value != this.value.replace(/[^0-9-]/g, '')) {
                this.value = this.value.replace(/[^0-9-]/g, '');
            }
        });
        // var result = _.filter(roleData, function (data) {
        //     return _.startsWith(data.name.toLowerCase(), search.toLowerCase());
        // });
        const { search, roleData, first, firsterr, middle, middleerr, last, lasterr, email, emailerr, mobile, mobileerr, phone, phoneerr, access, accesserr, active, activeerr, address, addresserr, user, usererr, selectederr } = this.state;
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content add-vendor-user">
                    <form onSubmit={(e) => this.onFormSubmit(e)}>
                        <div className="avu-head">
                            <div className="avuh-left">
                                <h3>Add New User</h3>
                                <p>Add Basic Details of User</p>
                            </div>
                            <div className="avuh-right">
                                <button type="button" className="cancelBtn" onClick={this.props.handleAddUserModal}>Cancel</button>
                                <button className="save-button-vendor" type="submit">Save</button>
                            </div>
                        </div>
                        <div className="avu-body">
                            <div className="col-md-12 pad-0 m-top-20">
                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="turning"></label>
                                    <input type="text" disabled className="form-box" id="turning" value={`${sessionStorage.getItem('partnerEnterpriseName')}`} />
                                </div>
                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="first">First Name</label>
                                    <input type="text" maxLength="80" className={firsterr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} value={first} id="first" />
                                    {/* {firsterr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                    {firsterr ? (
                                        <span className="error">
                                            Enter first name
                                        </span>
                                    ) : null}
                                </div>
                                {/*<div className="col-md-2 col-sm-2 pad-lft-0">
                                    <label htmlFor="middle"></label>
                                    <input type="text" className={middleerr ? "errorBorder form-box" : "form-box"} onChange={(e) => this.handleChange(e)} id="middle" value={middle} placeholder="Middle Name" />
                                    { {middleerr ? <img src={errorIcon} className="error_icon" /> : null} }
                                    {middleerr ? (
                                        <span className="error">
                                            Enter middle name
                                        </span>
                                    ) : null}
                                </div>*/}
                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="last">Last Name</label>
                                    <input type="text" maxlength="80" className={lasterr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} id="last" value={last} />
                                    {/* {lasterr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                    {lasterr ? (
                                        <span className="error">
                                            Enter last name
                                        </span>
                                    ) : null}
                                </div>

                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="user">Username</label>
                                    <input type="text" maxlength="80" className={usererr ? "errorBorder form-box" : "onFocus form-box"} id="user" onChange={(e) => this.handleChange(e)} value={user} />
                                    {/* {usererr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                    {usererr ? (
                                        <span className="error">
                                            Enter username
                                        </span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 ">
                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="email">Email</label>
                                    <input type="email" maxlength="200" className={emailerr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} id="email" value={email} />
                                    {/* {emailerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                    {emailerr ? (
                                        <span className="error">
                                            Enter valid email
                                        </span>
                                    ) : null}
                                </div>
                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="mobile">Mobile Number</label>
                                    <input type="text" maxLength="10" className={mobileerr ? "errorBorder form-box numbersOnly" : "onFocus form-box numbersOnly"} onChange={(e) => this.handleChange(e)} id="mobile" value={mobile} />
                                    {/* {mobileerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                    {mobileerr ? (
                                        <span className="error">
                                            Enter valid mobile number
                                        </span>
                                    ) : null}
                                </div>
                                <div className="col-md-3 col-sm-2 pad-lft-0">
                                    <label htmlFor="active">Status</label>
                                    <select className={activeerr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} value={active} id="active" placeholder="Active">
                                        <option value="">Status</option>
                                        <option value="true" >Active</option>
                                        <option value="false" >Inactive</option>
                                    </select>
                                    {/* {activeerr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                    {activeerr ? (
                                        <span className="error">
                                            Select active
                                        </span>
                                    ) : null}
                                </div>
                                {/*<div className="col-md-2 col-sm-2 pad-lft-0">
                                    <label htmlFor="phone"></label>
                                    <input type="text" maxLength="15" className={phoneerr ? "errorBorder form-box numbersOnlywithhighpn" : "form-box numbersOnlywithhighpn"} onChange={(e) => this.handleChange(e)} value={phone} id="phone" placeholder="Work Phone" />
                                        {phoneerr ? <img src={errorIcon} className="error_icon" /> : null} 
                                    {phoneerr ? (
                                        <span className="error">
                                            Enter valid phone number
                                        </span>
                                    ) : null}
                                </div>*/}
                                <div className="col-md-3 col-sm-2 pad-0">
                                    <label htmlFor="access" >Accessmode</label>
                                    <select className={accesserr ? "errorBorder form-box displayPointer" : "onFocus form-box"} placeholder="Access Mode" id="access" onChange={(e) => this.handleChange(e)} value={access} aria-placeholder="Accessmode" >

                                        <option value="UI">UI</option>
                                        <option value="API">API</option>
                                        {/* {accesserr ? <img src={errorIcon} className="error_icon" /> : null} */}
                                        {accesserr ? (
                                            <span className="error">
                                                Select access mode
                                            </span>
                                        ) : null}
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 ">
                                <div className="col-md-6 col-sm-4 pad-lft-0">
                                    <label htmlFor="address">Address</label>
                                    <textarea className={addresserr ? "errorBorder formtextarea" : "onFocus formtextarea"} onChange={(e) => this.handleChange(e)} value={address} id="address" ></textarea>
                                    {/* {addresserr ? <img src={errorIcon} className="user_error_icon-txtarea" /> : null} */}
                                    {addresserr ? (
                                        <span className="error">
                                            Enter valid address
                                        </span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20 ">
                                <ul className="avu-choose-retailer">
                                    <h3>Choose Retailers</h3>
                                    <p>You can choose multiple retailers</p>
                                </ul>
                                <div className="col-md-12 col-sm-12 pad-0">
                                    <ul className="list-inline m-top-10 width_100 m-bot-10">
                                        <li>
                                            {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.id} className="role-user-name">
                                                <span type="button" className="role-user-name-inner">{data.name} </span>
                                                <span onClick={(e) => this.onDelete(data.id)} value={data.id} className="roleun-close" aria-hidden="true">&times;</span>
                                            </div>)}

                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-3 col-xs-12 pad-0">
                                    <div className={selectederr ? "errorBorder userModalSelect displayPointer" : "onFocus userModalSelect displayPointer"} onClick={(e) => this.openClose(e)}>
                                        <label className="displayPointer">Choose Retailer
                                        {/* <span>
                                            ▾
                                        </span> */}
                                        </label>
                                        {/* {selectederr ? <img src={errorIcon} className="error_icon_purchase1" /> : null} */}
                                        {selectederr ? (
                                            <span className="error">
                                                Select Retailer
                                            </span>
                                        ) : null}

                                    </div>
                                    {this.state.open ? <div className="dropdownDiv m-top-10">

                                        {/* <input type="search" className="searchFilterModal" value={search} onChange={(e) => this.Change(e)} /> */}
                                        <ul className="dropdownFilterOption">
                                            {this.state.roleData.map((data, i) => <li value={data.id} key={data.id} onClick={(e) => this.onnChange(e)} >
                                                {data.name}
                                            </li>)}
                                        </ul>

                                    </div> : null}
                                </div>
                                {/* <AsyncSelect isMulti cacheOptions defaultOptions loadOptions={promiseOptions} />   */}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}