import React from "react";
import AlertPopUp from '../errorPage/alertPopUp';

import ToastLoader from "../loaders/toastLoader";
class SupplierModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            checked: false,
            supplierState: [],
            selectedId: "",
            id: "",
            addNew: true,
            save: false,
            mrp: "",
            articleCode: "",
            rsp: "",
            done: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            vendorSearch: "",
            name: "",
            address: "",
            toastLoader: false,
            slCode: "",
            findError: false,
            errorFind: "",
            sectionSelection: "",

            toastMsg: "",
            supplierCode: this.props.supplierCode,
            selectedsCode: "",
            selectedsupplier: "",
            supplier: false,

            selectedstateCode: "",
            selectedslcode: "",
            selectedslName: "",
            selectedslAddr: "",
            selectedtransporterCode: "",
            selectedtransporterName: "",
            selectedtransporter: "",
            selectedtradeGrpCode: "",
            selecteditem: "",
            selectedpurtermMainCode: "",
            selectedtermName: "",
            selectedgstInNo: "",
            selectedcity: "",
            searchBy: "startWith",
            selectVendor: "Choose Vendor",

            focusedLi: "",
        }

    }

    // Vendor

    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }

    componentWillMount() {
        console.log(this.props.purchaseIndent)
        console.log("this.props.supplierCode", this.props.supplierCode, "this.props.isModalShow", this.props.isModalShow, "this.props.supplierSearch", this.props.supplierSearch)
        this.setState({
            supplierCode: this.props.supplierCode,
            vendorSearch: this.props.isModalShow ? "" : this.props.supplierSearch,
            type: this.props.isModalShow || this.props.supplierSearch == "" ? 1 : 3
        })
        if (this.props.purchaseIndent.supplier.isSuccess) {

            if (this.props.purchaseIndent.supplier.data.resource != null) {

                this.setState({
                    supplierState: this.props.purchaseIndent.supplier.data.resource,
                    prev: this.props.purchaseIndent.supplier.data.prePage,
                    current: this.props.purchaseIndent.supplier.data.currPage,
                    next: this.props.purchaseIndent.supplier.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.supplier.data.maxPage,
                })
            } else {
                this.setState({
                    supplierState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }

        if (this.props.purchaseIndent.selectVendor.isSuccess) {
            if (this.props.purchaseIndent.selectVendor.data.resource != null) {

                this.setState({
                    supplierState: this.props.purchaseIndent.selectVendor.data.resource,
                    prev: this.props.purchaseIndent.selectVendor.data.prePage,
                    current: this.props.purchaseIndent.selectVendor.data.currPage,
                    next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
                })
            } else {
                this.setState({
                    supplierState: [],

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(this.props.purchaseIndent.selectVendor)
        this.setState({
            supplierCode: this.props.supplierCode
        })
        if (nextProps.purchaseIndent.supplier.isSuccess) {

            if (nextProps.purchaseIndent.supplier.data.resource != null) {

                this.setState({
                    supplier: false,
                    supplierState: nextProps.purchaseIndent.supplier.data.resource,
                    prev: nextProps.purchaseIndent.supplier.data.prePage,
                    current: nextProps.purchaseIndent.supplier.data.currPage,
                    next: nextProps.purchaseIndent.supplier.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.supplier.data.maxPage,

                })
            } else {
                this.setState({
                    supplierState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("vendorSearch").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.supplier.data.resource != null) {
                        console.log("fffffffffffffffffffffffffff")
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.supplier.data.resource[0].code,
                            vendorSearch: this.props.isModalShow ? "" : this.props.supplierSearch,
                            type: this.props.isModalShow || this.props.supplierSearch == "" ? 1 : 3

                        })
                        document.getElementById(nextProps.purchaseIndent.supplier.data.resource[0].code) != null ? document.getElementById(nextProps.purchaseIndent.supplier.data.resource[0].code).focus() : null
                    }


                }
            }
        }


        if (nextProps.purchaseIndent.selectVendor.isSuccess) {

            if (nextProps.purchaseIndent.selectVendor.data.resource != null) {
                console.log((nextProps.purchaseIndent.selectVendor.data.resource[0].supplierCode) != null ? nextProps.purchaseIndent.selectVendor.data.resource[0].supplierCode: null)
                document.getElementById(nextProps.purchaseIndent.selectVendor.data.resource[0].supplierCode) != null ? document.getElementById(nextProps.purchaseIndent.selectVendor.data.resource[0].supplierCode).focus() : null

                this.setState({
                    supplier: true,
                    supplierState: nextProps.purchaseIndent.selectVendor.data.resource,
                    prev: nextProps.purchaseIndent.selectVendor.data.prePage,
                    current: nextProps.purchaseIndent.selectVendor.data.currPage,
                    next: nextProps.purchaseIndent.selectVendor.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.selectVendor.data.maxPage,

                })
            } else {
                this.setState({
                    supplierState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("vendorSearch").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.selectVendor.data.resource != null) {
                        console.log(nextProps.purchaseIndent.selectVendor)
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.selectVendor.data.resource[0].code,
                            vendorSearch: this.props.isModalShow ? "" : this.props.supplierSearch,
                            type: this.props.isModalShow || this.props.supplierSearch == "" ? 1 : 3

                        })
                        document.getElementById(nextProps.purchaseIndent.selectVendor.data.resource[0].supplierCode) != null ? document.getElementById(nextProps.purchaseIndent.selectVendor.data.resource[0].supplierCode).focus() : null
                    }


                }
            }
        }
    }
    page(e) {
       
        if(this.props.purchaseIndent.supplier.data.resource != null){
            if (e.target.id == "prev") {
                this.setState({
                    prev: this.props.purchaseIndent.supplier.data.prePage,
                    current: this.props.purchaseIndent.supplier.data.currPage,
                    next: this.props.purchaseIndent.supplier.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.supplier.data.maxPage,
                })
                if (this.props.purchaseIndent.supplier.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.supplier.data.currPage - 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy
                    }
                    this.props.supplierRequest(data);
                }
            } else if (e.target.id == "next") {
                this.setState({
                    prev: this.props.purchaseIndent.supplier.data.prePage,
                    current: this.props.purchaseIndent.supplier.data.currPage,
                    next: this.props.purchaseIndent.supplier.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.supplier.data.maxPage,
                })
                if (this.props.purchaseIndent.supplier.data.currPage != this.props.purchaseIndent.supplier.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.supplier.data.currPage + 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy
                    }
                    this.props.supplierRequest(data)
                }
            }
            else if (e.target.id == "first") {
                this.setState({
                    prev: this.props.purchaseIndent.supplier.data.prePage,
                    current: this.props.purchaseIndent.supplier.data.currPage,
                    next: this.props.purchaseIndent.supplier.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.supplier.data.maxPage,
                })
                if (this.props.purchaseIndent.supplier.data.currPage <= this.props.purchaseIndent.supplier.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy
    
                    }
                    this.props.supplierRequest(data)
                }
    
            }
        }
        else if( nextProps.purchaseIndent.selectVendor.isSuccess){
            if (e.target.id == "prev") {
                this.setState({
                    prev: this.props.purchaseIndent.selectVendor.data.prePage,
                    current: this.props.purchaseIndent.selectVendor.data.currPage,
                    next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
                })
                if (this.props.purchaseIndent.selectVendor.data.currPage != 0) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.selectVendor.data.currPage - 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy
                    }
                    this.props.selectVendorRequest(data);
                }
            } else if (e.target.id == "next") {
                this.setState({
                    prev: this.props.purchaseIndent.selectVendor.data.prePage,
                    current: this.props.purchaseIndent.selectVendor.data.currPage,
                    next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
                })
                if (this.props.purchaseIndent.selectVendor.data.currPage != this.props.purchaseIndent.selectVendor.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy
                    }
                    this.props.selectVendorRequest(data)
                }
            }
            else if (e.target.id == "first") {
                this.setState({
                    prev: this.props.purchaseIndent.selectVendor.data.prePage,
                    current: this.props.purchaseIndent.selectVendor.data.currPage,
                    next: this.props.purchaseIndent.selectVendor.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.selectVendor.data.maxPage,
                })
                if (this.props.purchaseIndent.selectVendor.data.currPage <= this.props.purchaseIndent.selectVendor.data.maxPage) {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy
    
                    }
                    this.props.selectVendorRequest(data)
                }
    
            }
        }

    }
    onAddNew(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: false,
                save: true
            })
        this.onsearchClear();
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    selectedData = (code, gst) => {
        console.log("code", code, "gst", gst)
        const hash = window.location.hash;
        if (hash == "#/purchase/purchaseOrder") {
            console.log("This is V1")
        }
        if(!this.state.supplier){
            if (gst == undefined || gst == "" && hash != "#/purchase/purchaseOrder") {
                console.log("this is V2 and Pi");
                alert("GST not available for this Vendor. Do you want to proceed?")
            }
        }
        

        this.setState({
            selectedId: code
        })

        if(this.state.supplier){
            for (let i = 0; i < this.state.supplierState.length; i++) {
                if (this.state.supplierState[i].supplierCode == code) {
    
                    this.setState({
                        supplierCode: this.state.supplierState[i].supplierCode,
                        selectedsCode: this.state.supplierState[i].supplierCode,
                        selectedsupplier: this.state.supplierState[i].supplierName,
                        selectedslcode: this.state.supplierState[i].supplierCode,
                        selectedslName: this.state.supplierState[i].supplierName,
                    }, () => {
                        if (!this.props.isModalShow) {
                            this.ondone()
                        }
                    })
                }
            }
        }
        else{
            for (let i = 0; i < this.state.supplierState.length; i++) {
                if (this.state.supplierState[i].code == code) {
    
                    this.setState({
                        supplierCode: this.state.supplierState[i].code,
                        selectedsCode: this.state.supplierState[i].code,
                        selectedsupplier: this.state.supplierState[i].name,
                        selectedstateCode: this.state.supplierState[i].stateCode,
                        selectedslcode: this.state.supplierState[i].code,
                        selectedslName: this.state.supplierState[i].name,
                        selectedslAddr: this.state.supplierState[i].address,
                        selectedtransporterCode: this.state.supplierState[i].transporterCode != null ? this.state.supplierState[i].transporterCode : "",
                        selectedtransporterName: this.state.supplierState[i].transporterName != null ? this.state.supplierState[i].transporterName : "",
                        selectedtransporter: this.state.supplierState[i].transporterName != null ? this.state.supplierState[i].transporterName : "",
                        selectedtradeGrpCode: this.state.supplierState[i].tradeGrpCode,
                        selecteditem: this.state.supplierState[i].purchaseTermName != null ? this.state.supplierState[i].purchaseTermName : "",
                        selectedpurtermMainCode: this.state.supplierState[i].purchaseTermCode != null ? this.state.supplierState[i].purchaseTermCode : "",
                        selectedtermName: this.state.supplierState[i].purchaseTermName != null ? this.state.supplierState[i].purchaseTermName : "",
                        selectedgstInNo: this.state.supplierState[i].gstInNo != null ? this.state.supplierState[i].gstInNo : "",
                        selectedcity: this.state.supplierState[i].city != null ? this.state.supplierState[i].city : "",
                    }, () => {
                        if (!this.props.isModalShow) {
                            this.ondone()
                        }
                    })
                }
            }
        }

    }
    ondone(e) {

        var data = "";
        if (this.props.isModalShow ? this.state.supplierCode != this.props.supplierCode : this.state.supplierCode != this.props.supplierCode || this.props.supplierSearch == this.state.vendorSearch || this.props.supplierSearch != this.state.vendorSearch) {
            if(this.state.supplier){
                data = {
                    sCode: this.state.selectedsCode,
                    supplier: this.state.selectedsupplier,
                    stateCode: this.state.selectedstateCode,
                    slcode: this.state.selectedslcode,
                    slName: this.state.selectedslName,
                    slAddr: this.state.selectedslAddr,
                    transporterCode: this.state.selectedtransporterCode,
                    transporterName: this.state.selectedtransporterName,
                    transporter: this.state.selectedtransporter,
                    tradeGrpCode: this.state.selectedtradeGrpCode,
                    item: this.state.selecteditem,
                    purtermMainCode: this.state.selectedpurtermMainCode,
                    termName: this.state.selectedtermName,
                    gstInNo: this.state.selectedgstInNo,
                    city: this.state.selectedcity,
                }
                console.log("updatestate")
                let t = this
                setTimeout(function () {
                    t.props.updateSupplierState(data);
                }, 100)
    
                this.closeSupplier(e);
    
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return true;
                    }
                }
            }
            else{
                data = {
                    sCode: this.state.selectedsCode,
                    supplier: this.state.selectedsupplier,
                    stateCode: this.state.selectedstateCode,
                    slcode: this.state.selectedslcode,
                    slName: this.state.selectedslName,
                    slAddr: this.state.selectedslAddr,
                    transporterCode: this.state.selectedtransporterCode,
                    transporterName: this.state.selectedtransporterName,
                    transporter: this.state.selectedtransporter,
                    tradeGrpCode: this.state.selectedtradeGrpCode,
                    item: this.state.selecteditem,
                    purtermMainCode: this.state.selectedpurtermMainCode,
                    termName: this.state.selectedtermName,
                    gstInNo: this.state.selectedgstInNo,
                    city: this.state.selectedcity,
                }
                console.log("updatestate")
                let t = this
                setTimeout(function () {
                    t.props.updateSupplierState(data);
                }, 100)
    
                this.closeSupplier(e);
    
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return true;
                    }
                }
            }

            
        } else {
            if (this.state.supplierCode == undefined || this.state.supplierCode == "") {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            } else {
                this.closeSupplier(e);
            }

        }
    }

    handleChange(e) {

        if (e.target.id == "vendorSearch") {

            this.setState(
                {
                    vendorSearch: e.target.value
                },

            );

            // if (document.getElementById("vendorBasedOn").value == "slCode") {
            // this.setState(
            // {
            // slCode: e.target.value,
            // name: "",
            // address: ""
            // },

            // );
            // }
            // else if (document.getElementById("vendorBasedOn").value == "name") {
            // this.setState(
            // {
            // slCode: "",
            // name: e.target.value,
            // address: ""
            // },

            // );
            // } else if (document.getElementById("vendorBasedOn").value == "address") {
            // this.setState(
            // {
            // slCode: "",
            // name: "",
            // address: e.target.value
            // },

            // );displayModal
            // name: "",
            // address: "",
            // vendorSearch: e.target.value
            // },

            // );

            // }
            // } else if (e.target.id == "vendorBasedOn") {
            // this.setState(
            // {
            // vendorSearch: "",
            // sectionSelection: e.target.value
            // },
            // );

        } else if (e.target.id == "searchByvendor") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.vendorSearch != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        slCode: this.state.slCode,
                        name: this.state.name,
                        address: this.state.address,
                        search: this.state.vendorSearch,
                        department: this.props.department,
                        departmentCode: this.props.departmentCode,
                        siteCode: this.props.siteCode,
                        city: this.props.city,
                        searchBy: this.state.searchBy

                    }
                    this.props.supplierRequest(data)
                }

            })
        }

    }

    onSearch(e) {

        if (this.state.vendorSearch == "") {
            this.setState({
                toastMsg: "Enter data on search input",
                toastLoader: true,
                // type=""
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,
                    slCode: this.state.slCode,
                    name: this.state.name,
                    address: this.state.address,
                    search: this.state.vendorSearch,
                    department: this.props.department,
                    departmentCode: this.props.departmentCode,
                    siteCode: this.props.siteCode,
                    city: this.props.city,
                    searchBy: this.state.searchBy
                }
                this.props.supplierRequest(data)

            }
            else {
                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    slCode: this.state.slCode,
                    name: this.state.name,
                    address: this.state.address,
                    search: "",
                    department: this.props.department,
                    departmentCode: this.props.departmentCode,
                    siteCode: this.props.siteCode,
                    city: this.props.city,
                    searchBy: this.state.searchBy
                }
                this.props.supplierRequest(data)
            }
        }

    }

    errorMassage(e) {
        if (
            this.state.errorFind == "") {
            this.setState({
                findError: true
            });
        } else {
            this.setState({
                findError: false
            });
        }
    }
    closeSupplier(e) {

        this.props.onCloseSupplier()
        const t = this

        t.setState({
            supplierState: [],
            vendorSearch: "",
            sectionSelection: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

        // document.getElementById("vendorBasedOn").value = ""

    }
    onsearchClear() {

        if (this.state.type == 2 || this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                slCode: "",
                name: "",
                address: "",
                search: "",
                department: this.props.department,
                departmentCode: this.props.departmentCode,
                siteCode: this.props.siteCode,
                city: this.props.city,
                searchBy: this.state.searchBy
            }
            this.props.supplierRequest(data)
        }
        this.setState(
            {
                vendorSearch: "",
                sectionSelection: "",
                type: "",
                no: 1
            })
        document.getElementById("vendorSearch").focus()
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.supplierState.length != 0) {
                    this.setState({

                        supplierCode: this.state.supplierState[0].code,

                    })
                    this.selectedData(this.state.supplierState[0].code)
                    // let scode= this.state.supplierState[0].code
                    window.setTimeout(() => {
                        document.getElementById("vendorSearch").blur()
                        document.getElementById(this.state.supplierState[0].code).focus()
                    }, 0)
                } else {

                    window.setTimeout(() => {
                        document.getElementById("vendorSearch").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            } else if (e.target.value != "") {
                window.setTimeout(() => {
                    document.getElementById("vendorSearch").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if (e.key == "ArrowDown") {
            if (this.state.supplierState.length != 0) {
                this.setState({

                    supplierCode: this.state.supplierState[0].code,

                })
                this.selectedData(this.state.supplierState[0].code)
                // let scode= this.state.supplierState[0].code
                window.setTimeout(() => {
                    document.getElementById("vendorSearch").blur()
                    document.getElementById(this.state.supplierState[0].code).focus()
                }, 0)
            } else {

                window.setTimeout(() => {
                    document.getElementById("vendorSearch").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById(this.state.supplierState[0].code).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }

        if (e.key === "Enter") {
            this.ondone();
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.ondone(e);
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.closeSupplier();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("closeButton").blur()
                document.getElementById("vendorSearch").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.supplierState.length != 0) {
                this.setState({

                    supplierCode: this.state.supplierState[0].code
                })
                this.selectedData(this.state.supplierState[0].code)
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById(this.state.supplierState[0].code).focus()
                }, 0)
            } else {
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }
    selectLi(e, code, gst) {
        let supplierState = this.state.supplierState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < supplierState.length; i++) {
                if (supplierState[i].code == code) {
                    index = i
                }
            }
            if (index < supplierState.length - 1 || index == 0) {
                document.getElementById(supplierState[index + 1].code) != null ? document.getElementById(supplierState[index + 1].code).focus() : null

                this.setState({
                    focusedLi: supplierState[index + 1].code
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < supplierState.length; i++) {
                if (supplierState[i].code == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(supplierState[index - 1].code) != null ? document.getElementById(supplierState[index - 1].code).focus() : null

                this.setState({
                    focusedLi: supplierState[index - 1].code
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code, gst)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.closeSupplier(e)
        }

    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.supplierState.length != 0 ?
                            document.getElementById(this.state.supplierState[0].code).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.supplierState.length != 0) {
                    document.getElementById(this.state.supplierState[0].code).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeSupplier(e)
        }

    }

    render() {
        console.log("this.state.supplier", this.state.supplier, "this.state.supplierState", this.state.supplierState)
        !this.props.isModalShow ? $('.poSearchModal').removeClass('hideSmoothly')
            : null

        if (this.state.focusedLi != "" && this.props.supplierSearch == this.state.vendorSearch) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

        const { vendorSearch, findError, errorFind, sectionSelection } = this.state
        return (

            this.props.isModalShow ? <div className={this.props.supplierModalAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.supplierModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.supplierModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.supplierModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color selectVendorPopUp supplierSelectModal">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT SUPPLIER</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only single entry from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10">
                                        <div className="col-md-9 col-sm-9 float-left pad-0">
                                            <div className="mrpSelectCode">
                                                <li>

                                                    {/* __________________--DON'T REMOVE THIS COMMENTED CODE_____________________ */}

                                                    {/* <select id="vendorBasedOn" name="sectionSelection" value={sectionSelection} onChange={e => this.handleChange(e)}>
                                                        <option value="">Choose</option>
                                                        <option value="slCode">CODE</option>
                                                        <option value="name">NAME</option>
                                                        <option value="address">ADDRESS</option>
                                                    </select> */}
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByvendor" name="searchByvendor" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}

                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyPress={this._handleKeyPress} onKeyDown={this._handleKeyDown} onChange={(e) => this.handleChange(e)} value={vendorSearch} id="vendorSearch" placeholder="Type to search" />

                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                        <li className="float_right">

                                            <label>
                                                <button type="button" className={this.state.vendorSearch == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                    {
                                            this.state.supplier ? 
                                       
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Name</th>
                                                    <th>Supplier Id</th>
                                                    <th>Code</th>
                                                    <th>Address</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.supplierState == undefined || this.state.supplierState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.supplierState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.supplierState.map((data, key) => (
                                                    < tr key={key} onClick={(e) => this.selectedData(`${data.code}`)} >
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={data.code} type="radio" name="supplier" checked={this.state.supplierCode == `${data.code}`} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.name}</td>
                                                        <td>{data.slId}</td>
                                                        <td>{data.code}</td>
                                                        <td className="toolTipDefault"><label title={data.address}>{data.address}</label></td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>

                                                    :
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Vendor</th>
                                                    <th>Vendor Code</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.supplierState == undefined || this.state.supplierState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.supplierState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.supplierState.map((data, key) => (
                                                    console.log(data),
                                                    < tr key={key} onClick={(e) => this.selectedData(`${data.supplierCode}`)} >
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={data.supplierCode} type="radio" name="supplier" checked={this.state.supplierCode == `${data.supplierCode}`} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.supplierName}</td>
                                                        <td>{data.supplierCode}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    }
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeSupplier(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button" >
                                                    First
</button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
</button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
</button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
</button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
</button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
</button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
</button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div > :

                <div className="poSearchModal">
                    {console.log("whille rendering this.state.supplier", this.state.supplier)}
                    {
                        !this.state.supplier ? 
                            <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                                <div className="dropdown-modal-header">
                                    <span className="vd-name div-col-4">Vendor</span>
                                    <span className="vd-loc div-col-4">Vendor Id</span>
                                    <span className="vd-num div-col-4">GST No</span>
                                    <span className="vd-code div-col-4">Vendor Code</span>
                                </div>

                                <ul className="dropdown-menu-city-item">
                                    {this.state.supplierState == undefined || this.state.supplierState.length == 0 ?
                                        <li><span className="vendor-details"><span className="vd-name">No Data Found</span></span></li> :
                                        this.state.supplierState.map((data, key) => (
                                            console.log(data.code, data.gstInNo),
                                            < li key={key} onClick={() => this.selectedData(data.code, data.gstInNo)} id={data.code} className={this.state.supplierCode == `${data.code}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.code, data.gstInNo)}>
                                                <span className="vendor-details">
                                                    <span className="vd-name div-col-4">{data.name}</span>
                                                    <span className="vd-loc div-col-4">{data.slId}</span>
                                                    <span className="vd-num div-col-4">{data.gstInNo}</span>
                                                    <span className="vd-code div-col-4">{data.code}</span>
                                                </span>
                                            </li>))}
                                </ul>
                                <div className="gen-dropdown-pagination">
                                    <div className="page-close">
                                        <button className="btn-close" type="button" onClick={(e) => this.closeSupplier(e)} id="btn-close">Close</button>
                                    </div>
                                    <div className="page-next-prew-btn">
                                        {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg>
                                        </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                </svg></button>}
                                        <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg>
                                        </button>
                                            : <button className="pnpb-next" type="button" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                </svg>
                                            </button> : <button className="pnpb-next" type="button" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                </svg></button>}
                                    </div>
                                </div>
                            </div>
                        :
                            <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                                <div className="dropdown-modal-header">
                                    <span className="vd-name div-col-4">Vendor</span>
                                    <span className="vd-loc div-col-4">Vendor Id</span>
                                </div>

                                <ul className="dropdown-menu-city-item">
                                    {this.state.supplierState == undefined || this.state.supplierState.length == 0 ?
                                        <li><span className="vendor-details"><span className="vd-name">No Data Found</span></span></li> :
                                        this.state.supplierState.map((data, key) => (
                                            < li key={key} onClick={() => this.selectedData(data.supplierCode)} id={data.supplierCode} className={this.state.supplierCode == `${data.supplierCode}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.supplierCode, data.gstInNo)}>
                                                <span className="vendor-details">
                                                    <span className="vd-name div-col-4">{data.supplierName}</span>
                                                    <span className="vd-loc div-col-4">{data.supplierCode}</span>
                                                </span>
                                            </li>))}
                                </ul>
                                <div className="gen-dropdown-pagination">
                                    <div className="page-close">
                                        <button className="btn-close" type="button" onClick={(e) => this.closeSupplier(e)} id="btn-close">Close</button>
                                    </div>
                                    <div className="page-next-prew-btn">
                                        {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg>
                                        </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                </svg></button>}
                                        <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg>
                                        </button>
                                            : <button className="pnpb-next" type="button" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                </svg>
                                            </button> : <button className="pnpb-next" type="button" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                                </svg></button>}
                                    </div>
                                </div>
                            </div>
                    }
                </div >
        );
    }
}

export default SupplierModal;