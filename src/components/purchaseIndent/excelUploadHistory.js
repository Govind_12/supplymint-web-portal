import React from 'react';
import Pagination from '../pagination';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import ExcelUpload from './excelUpload'

class ExcelUploadHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDownloadDrop: false,
            filter: false,
            isStartNewUpload: false
        };
    }
    // componentDidMount(){
    //     console.log(this.props.excelUpload.excelSearch)
    // }
    // componentWillReceiveProps(nextProps) {
    //     console.log(nextProps.excelUpload)
    // }
    startNewUpload = () => {
        // this.setState({
        //     isStartNewUpload: true
        // })
        this.props.history.push('/digiproc/excelUpload')
    }
    
    render () {
        const hash = window.location.hash;
        return (
            <div>
                <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0 ">
                    <div className="gen-vendor-potal-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                    <span className="closeSearch"><img src={require('../../assets/clearSearch.svg')} /></span>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gvpd-right">
                                <button type="button" className="gen-new-upload" onClick={this.startNewUpload}>
                                    <svg xmlns="http://www.w3.org/2000/svg" id="plus_4_" width="16" height="16" viewBox="0 0 19.846 19.846">
                                        <g id="Group_3035">
                                            <g id="Group_3034">
                                                <path fill="#51aa77" id="Path_948" d="M9.923 0a9.923 9.923 0 1 0 9.923 9.923A9.934 9.934 0 0 0 9.923 0zm0 18.308a8.386 8.386 0 1 1 8.386-8.386 8.4 8.4 0 0 1-8.386 8.386z" class="cls-1"></path>
                                            </g>
                                        </g>
                                        <g id="Group_3037" transform="translate(5.311 5.242)">
                                            <g id="Group_3036">
                                                <path fill="#51aa77" id="Path_949" d="M145.477 139.081H142.4v-3.074a.769.769 0 0 0-1.537 0v3.074h-3.074a.769.769 0 0 0 0 1.537h3.074v3.074a.769.769 0 1 0 1.537 0v-3.074h3.074a.769.769 0 0 0 0-1.537z" class="cls-1" transform="translate(-137.022 -135.238)"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    Start New Upload</button>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className="gvpd-filter-inner">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                        {/* {this.state.filterCount != 0 && <p className="noOfFilter">{this.state.filterCount}</p>} */}
                                    </button>
                                    {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} handleInputBoxEnable={(e, data) => this.handleInputBoxEnable(e, data)} />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-lr-47">
                    <div className="vendor-gen-table" >
                        <div className="manage-table" id="scrollDiv">
                            <table className="table gen-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                </li>
                                            </ul>
                                        </th>
                                        <th><label>Uploaded Date</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Total Number of PO</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Total Number of Item Uploaded</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Amount</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                        <th><label>Status</label><img src={require('../../assets/headerFilter.svg')} /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>23,897.00</label></td>
                                        <td><label>3 PO created and 1 discarded</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>23,897.00</label></td>
                                        <td><label>3 PO created and 1 discarded</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>23,897.00</label></td>
                                        <td><label>3 PO created and 1 discarded</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>23,897.00</label></td>
                                        <td><label>3 PO created and 1 discarded</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>23,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>3657</label></td>
                                        <td><label>23,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>536</label></td>
                                        <td><label>536</label></td>
                                        <td><label>63,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>536</label></td>
                                        <td><label>536</label></td>
                                        <td><label>63,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>536</label></td>
                                        <td><label>536</label></td>
                                        <td><label>63,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>536</label></td>
                                        <td><label>536</label></td>
                                        <td><label>63,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>536</label></td>
                                        <td><label>536</label></td>
                                        <td><label>63,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><label>23/09/2020</label></td>
                                        <td><label>536</label></td>
                                        <td><label>536</label></td>
                                        <td><label>63,897.00</label></td>
                                        <td><label>6 PO created</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0">
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder" value="01" />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">10</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.isStartNewUpload ? <ExcelUpload  {...this.state} {...this.props}  /> : null}
            </div>
            {/* {hash == "/digiproc/excelUpload" ? (
                <ExcelUpload {...this.props} />
            ) : null} */}
            </div>

            
            
        )
    }
}

export default ExcelUploadHistory;