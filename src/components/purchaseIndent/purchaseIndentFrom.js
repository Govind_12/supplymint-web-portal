import React from "react";
import PurchaseIndentTable from "./purchaseIndentTable";
import pIData from "../../json/pIData.json";
import ConfirmModal from '../loaders/confirmModal';
import TransporterSelection from "./transporterSelection";
import SectionDataSelection from "./sectionDataSelection";
import SupplierModal from "./purchaseVendorModal";
import ToastLoader from "../loaders/toastLoader";
import SiteModal from "../purchaseOrder/siteModal"
import CityModal from "../purchaseOrder/cityModal";

class PurchaseIndentForm extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            displayOtb: true,
            transporterValidation: true,
            addRow: false,
            itemUdfExist: "false",
            isDiscountAvail: false,
            isDiscountMap: false,
            cityModal: false,
            cityModalAnimation: false,
            city: "",
            cityerr: false,
            isCityExist: false,
            cat1Validation: false,
            cat2Validation: false,
            cat3Validation: false,
            cat4Validation: false,
            cat5Validation: false,
            cat6Validation: false,
            desc1Validation: false,
            desc2Validation: false,
            desc3Validation: false,
            desc4Validation: false,
            desc5Validation: false,
            desc6Validation: false,
            itemudf1Validation: false,
            itemudf2Validation: false,
            itemudf3Validation: false,
            itemudf4Validation: false,
            itemudf5Validation: false,
            itemudf6Validation: false,
            itemudf7Validation: false,
            itemudf8Validation: false,
            itemudf9Validation: false,
            itemudf10Validation: false,
            itemudf11Validation: false,
            itemudf12Validation: false,
            itemudf13Validation: false,
            itemudf14Validation: false,
            itemudf15Validation: false,
            itemudf16Validation: false,
            itemudf17Validation: false,
            itemudf18Validation: false,
            itemudf19Validation: false,
            itemudf20Validation: false,

            udf1Validation: false,
            udf2Validation: false,
            udf3Validation: false,
            udf4Validation: false,
            udf5Validation: false,
            udf6Validation: false,
            udf7Validation: false,
            udf8Validation: false,
            udf9Validation: false,
            udf10Validation: false,

            udf11Validation: false,
            udf12Validation: false,
            udf13Validation: false,
            udf14Validation: false,
            udf15Validation: false,
            udf16Validation: false,
            udf17Validation: false,
            udf18Validation: false,
            udf19Validation: false,
            udf20Validation: false,

            onFieldSite: false,
            siteName: "",
            siteCode: "",
            siteNameerr: false,
            siteModal: false,
            siteModalAnimation: false,
            isUDFExist: "false",
            isSiteExist: "false",
            onFieldHsn: false,
            slCityName: "",
            divisionState: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            radioChange: "",
            supplierState: [],
            trasporterModal: false,
            purchaseTermState: [],
            transporterAnimation: false,
            supplierModal: false,
            supplierModalAnimation: false,
            selectionModal: false,
            selectionAnimation: false,
            radio: "article",
            transporterState: [],
            stateCode: "",
            tradeGrpCode: "",
            purtermMainCode: "",
            selected: "",
            current: "",
            supplier: "",
            supplierText: "",


            department: "",
            division: "",
            section: "",
            hl1Code: "",
            hl2Code: "",
            hl3Code: "",
            vendor: "",
            leadDays: "",
            item: "",
            transport: "",
            transporter: "",


            departmenterr: false,
            divisionerr: false,
            sectionerr: false,
            vendorerr: false,
            leadDayserr: false,
            itemerr: false,
            transporterr: false,

            checked: false,
            slCode: "",
            slName: "",
            slAddr: "",


            piQuantity: 0,
            piAmount: 0,
            flag: 0,
            supplierCode: "",

            toastLoader: false,
            toastMsg: "",
            selectedDepartment: "",
            // ______________________

            isArticleSeparated: false,
            isMRPEditable: false,
            isRSP: false,
            isRequireSiteId: false,
            isVendorDesignNotReq: false,
            isMrpRequired: true,
            isRspRequired: true,
            isColorRequired: true,
            isDisplayFinalRate: false,
            isTransporterDependent: true,
            isDisplayMarginRule: true,
            isGSTDisable: false,
            isTaxDisable: false,
            isBaseAmountActive: false,
            typeOfBuying: "",
            typeOfBuyingErr: false,

            isModalShow: true,
            supplierSearch: "",
            citySearch: "",
            siteSearch: "",
            transporterSearch: ""

        };


    }


    openTransporterSelection(e) {
        if (this.state.supplier != "") {
            var data = {
                no: 1,
                type: this.state.isModalShow ? 1 : 3,
                search: this.state.isModalShow ? "" : this.state.transporter,
                transporterCode: "",
                transporterName: "",
                city: this.state.isTransporterDependent != false ? this.state.city : ""
            }
            this.props.getTransporterRequest(data)
            // e==undefined? e.preventDefault():null
            this.setState({
                transporterSearch: this.state.transporter,
                transporter: this.state.transporter,
                trasporterModal: true,
                transporterAnimation: !this.state.transporterAnimation

            });
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }
        } else {
            this.setState({
                toastMsg: "Select Supplier",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)

        }
    }
    onCloseTransporter(e) {
        this.setState({
            trasporterModal: false,
            transporterAnimation: !this.state.transporterAnimation

        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("transporter").focus()
    }
    openSupplier(e) {
        // if(e==undefined){
        // e.preventDefault();}
        if (this.state.department == "") {
            this.setState({
                toastMsg: "Select Department",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            if (this.state.isCityExist) {
                if (this.state.city != "") {
                    var data = {
                        no: 1,
                        type: this.state.isModalShow ? 1 : 3,

                        slCode: "",
                        name: "",
                        address: "",
                        search: this.state.isModalShow ? "" : this.state.supplier,
                        department: this.state.department,
                        departmentCode: this.state.hl3Code,
                        siteCode: this.state.siteCode,
                        city: this.state.city


                    }
                    this.props.supplierRequest(data);
                    this.setState({
                        supplierSearch: this.state.isModalShow ? "" : this.state.supplier,
                        supplierCode: this.state.slcode,
                        supplierModal: true,
                        supplierModalAnimation: !this.state.supplierModalAnimation

                    });
                    document.onkeydown = function (t) {
                        if (t.which == 9) {
                            return false;
                        }
                    }
                } else {
                    this.setState({
                        toastMsg: "Select City",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1000)
                }
            } else {
                var data = {
                    no: 1,
                    type: this.state.isModalShow ? 1 : 3,

                    slCode: "",
                    name: "",
                    address: "",
                    department: this.state.department,
                    departmentCode: this.state.hl3Code,
                    siteCode: this.state.siteCode,
                    city: "",
                    search: this.state.supplier,


                }
                this.props.supplierRequest(data);
                this.setState({
                    supplierSearch: this.state.supplier,

                    supplierCode: this.state.slcode,
                    supplierModal: true,
                    supplierModalAnimation: !this.state.supplierModalAnimation

                });
                document.onkeydown = function (t) {
                    if (t.which == 9) {
                        return false;
                    }
                }
            }
        }
    }
    onCloseSupplier(e) {
        this.setState({
            supplierModal: false,
            supplierModalAnimation: !this.state.supplierModalAnimation

        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }

        document.getElementById("supplier").focus()
    }
    closeSelection(e) {
        this.setState({
            selectionModal: false,
            selectionAnimation: !this.state.selectionAnimation
        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("siteName") != null ?
            document.getElementById("siteName").focus() : null
    }


    _handleKeyPress(e, id) {

        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }else  if (e.key === "Enter") {
            if (id == "supplier") {
                this.openSupplier()
            }
            else if (id == "city") {
                this.openCityModal()
            } else if (id == "siteName") {
                this.openSiteModal()
            } else if (id == "transporter") {
                this.openTransporterSelection()
            }
        }else {
            document.getElementById(id).focus()
        }
        
    }

    openSelection(e) {
        e.preventDefault();
        var data = {
            no: 1,
            type: "",
            hl1name: "",
            hl2name: "",
            hl3name: "",
            search: ""
        }
        this.props.divisionSectionDepartmentRequest(data);
        this.setState({
            department: this.state.department,
            selectionModal: true,
            selectionAnimation: !this.state.selectionAnimation

        });
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }

    onhChange(dat) {
        this.setState({
            headerMsg: "Are you sure to make assortment on the basis of " + dat + " ?",
            paraMsg: "Click confirm to continue.",
            radioChange: dat,
            confirmModal: true

        })
    }
    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }

    handleChange(radio) {
        const t = this;
        setTimeout(function () {
            t.setState({
                radio: radio,
                flag: t.state.flag + 1,
                piAmount: 0,
                piQuantity: 0,
                supplier: "",
                item: "",
                transporter: "",
                leadDays: "",
                division: "",
                section: "",
                department: "",
                city: ""
            });
        }, 10)
    }
    updateState(upData) {
        this.setState({
            supplier: "",
            item: "",
            transporter: "",
            leadDays: "",
            slcode: "",

            division: upData.hl1Name,
            section: upData.hl2Name,
            department: upData.hl3Name,
            hl1Code: upData.hl1Code,
            hl2Code: upData.hl2Code,
            hl3Code: upData.hl3Code,
            piQuantity: 0,
            piAmount: 0,
            itemUdfExist: upData.itemUdfExist,

            isUDFExist: upData.isUDFExist,
            isSiteExist: upData.isSiteExist,
            flag: this.state.flag + 1,
            selectedDepartment: upData.hl1Name + upData.hl2Name + upData.hl3Name
        }, () => {
            this.division();
            this.section();
            this.department();
        })
        this.props.leadTimeClear();
        this.props.lineItemClear();
        this.props.otbClear();


        let Payload = {
            hl3Code: upData.hl3Code,
            hl3Name: upData.hl3Name,
        }
        this.props.getItemDetailsRequest(Payload)

        this.setState({
            onFieldHsn: true
        })
        if (upData.isUDFExist == "true") {

            let udfdata = {
                type: 1,
                search: "",
                isCompulsary: "",
                displayName: "",
                udfType: "",
                ispo: true
            }
            this.props.getUdfMappingRequest(udfdata);

        }
        if (upData.itemUdfExist == "true") {
            let uData = {
                hl3Code: upData.hl3Code,
                ispo: true,
                hl3Name: upData.hl3Name

            }

            this.props.getCatDescUdfRequest(uData)
        }


        if (upData.isSiteExist == "true" || this.state.isRequireSiteId == true) {
            let site = {
                type: 1,
                no: 1,
                search: ""
            }
            this.props.procurementSiteRequest(site)
        }
        this.textInput.current.focus();
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }

    updateSupplierState(data) {
        this.setState({
            supplier: data.supplier,
            stateCode: data.stateCode,
            slcode: data.slcode,
            slName: data.slName,
            slAddr: data.slAddr,
            transporterCode: data.transporterCode,
            transporterName: data.transporterName,
            flag: this.state.flag + 1,
            transporter: data.transporter,
            tradeGrpCode: data.tradeGrpCode,
            item: data.item,
            purtermMainCode: data.purtermMainCode,
            termName: data.termName,
            slCityName: data.city,
            piAmount: 0,
            piQuantity: 0,
            onFieldSite: true


        }, () => {
            this.vendor();
            this.item();
            this.transport();
        })
        this.props.leadTimeRequest(data.sCode)

        // let site = {
        //     type: 1,
        //     no: 1,
        //     search: ""
        // }
        // this.props.procurementSiteRequest(site)

        // let hsnData = {
        //     rowId: 0,
        //     code: this.state.hl3Code,
        //     no: 1,
        //     type: 1,
        //     search: "" 
        // }
        // this.props.hsnCodeRequest(hsnData)
        let hsnData = {
            rowId: 0,
            code: this.state.hl3Code,
            no: 1,
            type: 1,
            search: ""
        }
        this.props.hsnCodeRequest(hsnData)
        document.getElementById("supplier").focus()
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    componentWillMount() {

        this.props.poRadioValidationRequest('data')
    }
    componentDidMount() {
        this.textInput.current.focus();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.poRadioValidation.isSuccess) {
            this.setState({
                cat1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat1 : false,
                cat2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat2 : false,
                cat3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat3 : false,
                cat4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat4 : false,
                cat5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat5 : false,
                cat6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.cat6 : false,
                desc1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc1 : false,
                desc2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc2 : false,
                desc3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc3 : false,
                desc4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc4 : false,
                desc5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc5 : false,
                desc6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.desc6 : false,
                isArticleSeparated: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isArticleSeparated != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isArticleSeparated : false,
                isMRPEditable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMRPEditable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMRPEditable : false,
                isDiscountAvail: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountAvail != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountAvail : false,
                isDiscountMap: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDiscountMap : false,
                isRSP: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRSP != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRSP : false,
                isCityExist: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isCityCheck : false,
                isRequireSiteId: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRequireSiteId : false,
                isVendorDesignNotReq: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isVendorDesignNotReq : false,

                displayOtb: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.displayOtb : true,
                transporterValidation: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.transporterValidation : true,
                addRow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.addRow : false,


                itemudf1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf1 : false,
                itemudf2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf2 : false,
                itemudf3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf3 : false,
                itemudf4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf4 : false,
                itemudf5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf5 : false,
                itemudf6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf6 : false,
                itemudf7Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf7 : false,
                itemudf8Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf8 : false,
                itemudf9Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf9 : false,
                itemudf10Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf10 : false,
                itemudf11Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf11 : false,
                itemudf12Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf12 : false,
                itemudf13Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf13 : false,
                itemudf14Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf14 : false,
                itemudf15Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf15 : false,
                itemudf16Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf16 : false,
                itemudf17Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf17 : false,
                itemudf18Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf18 : false,
                itemudf19Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf19 : false,
                itemudf20Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.itemudf20 : false,

                udf1Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf1 : false,
                udf2Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf2 : false,
                udf3Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf3 : false,
                udf4Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf4 : false,
                udf5Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf5 : false,
                udf6Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf6 : false,
                udf7Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf7 : false,
                udf8Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf8 : false,
                udf9Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf9 : false,
                udf10Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf10 : false,

                udf11Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf11 : false,
                udf12Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf12 : false,
                udf13Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf13 : false,
                udf14Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf14 : false,
                udf15Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf15 : false,
                udf16Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf16 : false,
                udf17Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf17 : false,
                udf18Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf18 : false,
                udf19Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf19 : false,
                udf20Validation: nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.ESSENTIAL_PRO_PARAM.udf20 : false,

                isMrpRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isMrpReq : true,
                isRspRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRspReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isRspReq : true,
                isColorRequired: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isColorReq : true,
                isDisplayFinalRate: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayFinalRate != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayFinalRate : false,
                isTransporterDependent: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTransporterDependent : true,
                isDisplayMarginRule: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isDisplayMarginRule : true,
                isTaxDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isTaxDisable : false,
                isGSTDisable: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isGSTDisable : false,
                isBaseAmountActive: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isBaseAmountActive : false,
                // isModalShow: nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isModalShow != undefined ? nextProps.purchaseIndent.poRadioValidation.data.resource.CUSTOM_PO.isModalShow : false

            })

        }
        if (nextProps.purchaseIndent.procurementSite.isSuccess) {

            if (nextProps.purchaseIndent.procurementSite.data.resource != null) {
                this.setState({
                    siteCode: nextProps.purchaseIndent.procurementSite.data.defaultSiteKey.defaultSiteCode,
                    siteName: nextProps.purchaseIndent.procurementSite.data.defaultSiteKey.defaultSiteName,
                    onFieldSite: false

                })
            } else {
                this.setState({
                    siteCode: "",
                    siteName: "",
                    onFieldSite: false


                })
            }
        }
        if (nextProps.purchaseIndent.leadTime.isSuccess) {
            this.setState({
                leadDays: nextProps.purchaseIndent.leadTime.data.resource != null ? nextProps.purchaseIndent.leadTime.data.resource.leadTime : ""
            }, () => {
                this.leadDays()
            })
        }
        if (nextProps.purchaseIndent.supplier.isSuccess) {
            this.setState({
                supplierState: nextProps.purchaseIndent.supplier.data.resource,

            })
        }

        if (nextProps.purchaseIndent.divisionData.isSuccess) {
            this.setState({
                divisionState: this.props.purchaseIndent.divisionData.data.resource,    
            })
        }
        if (nextProps.purchaseIndent.getItemDetails.isSuccess) {
            if (nextProps.purchaseIndent.getItemDetails.data.essentialProParam != undefined) {
                this.setState({
                    cat1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat1 : this.state.cat1Validation,
                    cat2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat2 : this.state.cat2Validation,
                    cat3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat3 : this.state.cat3Validation,
                    cat4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat4 : this.state.cat4Validation,
                    cat5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat5 : this.state.cat5Validation,
                    cat6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.cat6 : this.state.cat6Validation,
                    desc1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc1 : this.state.desc1Validation,
                    desc2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc2 : this.state.desc2Validation,
                    desc3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc3 : this.state.desc3Validation,
                    desc4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc4 : this.state.desc4Validation,
                    desc5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc5 : this.state.desc5Validation,
                    desc6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.desc6 : this.state.desc6Validation,

                    itemudf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf1 : this.state.itemudf1Validation,
                    itemudf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf2 : this.state.itemudf2Validation,
                    itemudf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf3 : this.state.itemudf3Validation,
                    itemudf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf4 : this.state.itemudf4Validation,
                    itemudf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf5 : this.state.itemudf5Validation,
                    itemudf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf6 : this.state.itemudf6Validation,
                    itemudf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf7 : this.state.itemudf7Validation,
                    itemudf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf8 : this.state.itemudf8Validation,
                    itemudf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf9 : this.state.itemudf9Validation,
                    itemudf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf10 : this.state.itemudf10Validation,
                    itemudf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf11 : this.state.itemudf11Validation,
                    itemudf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf12 : this.state.itemudf12Validation,
                    itemudf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf13 : this.state.itemudf13Validation,
                    itemudf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf14 : this.state.itemudf14Validation,
                    itemudf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf15 : this.state.itemudf15Validation,
                    itemudf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf16 : this.state.itemudf16Validation,
                    itemudf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf17 : this.state.itemudf17Validation,
                    itemudf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf18 : this.state.itemudf18Validation,
                    itemudf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf19 : this.state.itemudf19Validation,
                    itemudf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.itemudf20 : this.state.itemudf20Validation,

                    udf1Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf1 : this.state.udf1Validation,
                    udf2Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf2 : this.state.udf2Validation,
                    udf3Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf3 : this.state.udf3Validation,
                    udf4Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf4 : this.state.udf4Validation,
                    udf5Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf5 : this.state.udf5Validation,
                    udf6Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf6 : this.state.udf6Validation,
                    udf7Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf7 : this.state.udf7Validation,
                    udf8Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf8 : this.state.udf8Validation,
                    udf9Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf9 : this.state.udf9Validation,
                    udf10Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf10 : this.state.udf10Validation,

                    udf11Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf11 : this.state.udf11Validation,
                    udf12Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf12 : this.state.udf12Validation,
                    udf13Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf13 : this.state.udf13Validation,
                    udf14Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf14 : this.state.udf14Validation,
                    udf15Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf15 : this.state.udf15Validation,
                    udf16Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf16 : this.state.udf16Validation,
                    udf17Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf17 : this.state.udf17Validation,
                    udf18Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf18 : this.state.udf18Validation,
                    udf19Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf19 : this.state.udf19Validation,
                    udf20Validation: nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 != undefined ? nextProps.purchaseIndent.getItemDetails.data.essentialProParam.udf20 : this.state.udf20Validation,



                })
            }
        }
    }

    getPiQuantity(data) {
        this.setState({
            piQuantity: data.toFixed(2)
        })
    }
    getPiAmount(data) {
        this.setState({
            piAmount: data != "" ? data.toFixed(2) : 0
        })
    }

    onChange(e) {
        if (e.target.id == "vendor") {
            this.setState(
                {
                    vendor: e.target.value
                },
                () => {
                    this.vendor();
                }
            );

        } else if (e.target.id == "item") {
            this.setState(
                {
                    item: e.target.value
                },
                () => {
                    this.item();
                }
            );
            this.updatePurchaseTerm(e.target.value);

        } else if (e.target.id == "transporterData") {
            this.setState(
                {
                    transport: e.target.value
                },
                () => {
                    this.transport();
                }
            );

        }

    }


    transport() {
        if (this.state.transporterValidation) {
            if (
                this.state.transporter == "") {
                this.setState({
                    transporterr: true
                });
            } else {
                this.setState({
                    transporterr: false
                });
            }
        } else {
            this.setState({
                transporterr: false
            });
        }
    }
    item() {
        if (
            this.state.item == "" || this.state.item.trim() == "") {
            this.setState({
                itemerr: true
            });
        } else {
            this.setState({
                itemerr: false
            });
        }
    }
    leadDays() {

        if (
            this.state.leadDays == "") {
            this.setState({
                leadDayserr: true
            });
        } else {
            this.setState({
                leadDayserr: false
            });
        }
    }
    vendor() {

        if (
            this.state.supplier == "" || this.state.supplier.trim() == "") {
            this.setState({
                vendorerr: true
            });
        } else {
            this.setState({
                vendorerr: false
            });
        }
    }
    section() {
        if (
            this.state.section == "" || this.state.section.trim() == "") {
            this.setState({
                sectionerr: true
            });
        } else {
            this.setState({
                sectionerr: false
            });
        }
    }
    department() {
        if (
            this.state.department == "" || this.state.department.trim() == "") {
            this.setState({
                departmenterr: true
            });
        } else {
            this.setState({
                departmenterr: false
            });
        }
    }
    site(e) {
        if (this.state.siteName == "") {
            this.setState({
                siteNameerr: true
            });
        } else {
            this.setState({
                siteNameerr: false
            });
        }
    }
    division() {
        if (
            this.state.division == "" || this.state.division.trim() == "") {
            this.setState({
                divisionerr: true
            });
        } else {
            this.setState({
                divisionerr: false
            });
        }
    }

    sectionCloseModal() {
        this.setState({
            selectionModal: false,

            selectionAnimation: !this.state.selectionAnimation
        })
        document.getElementById("supplier").focus()
    }


    onSubmit() {
        this.vendor();
        this.leadDays();
        this.division();
        this.department();
        this.section();
        this.item();
        this.transport();
        { this.state.isSiteExist == "true" ? this.site() : null }

    }

    resetForm(e) {
        this.setState({
            division: "",
            section: "",
            department: "",
            supplier: "",
            leadDays: "",
            item: "",
            transporter: "",
            piQuantity: 0,
            piAmount: 0,
            radio: "article",
            slcode: "",
            siteName: "",
            siteCode: "",
            selectedDepartment: "",
            city: "",
            departmenterr: false,
            divisionerr: false,
            sectionerr: false,
            vendorerr: false,
            leadDayserr: false,
            itemerr: false,
            transporterr: false,
            cityerr: false,
            siteNameerr: false,


        })
    }
    updatePurchaseTerm(item) {
        var pTerm = this.state.purchaseTermState
        if (pTerm != null) {
            for (var i = 0; i < pTerm.length; i++) {
                if (pTerm[i].name == item) {
                    this.setState({
                        item: pTerm[i].name,
                        purtermMainCode: pTerm[i].code,
                        termName: pTerm[i].name
                    })
                }
            }
        }
    }

    updateTransporterState(code) {
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        this.setState({

            transporterCode: code.transporterCode,
            transporterName: code.transporterName,

            transporter: code.transporterName,

        }, () => {
            this.transport();
        })

        document.getElementById("transporter").focus()

    }
    resetData() {
        this.refs.indentTable.resetData()
    }
    deleteRows(idx) {

        this.refs.indentTable.deleteRows(idx)
    }
    updateHsnvalue() {
        this.setState({
            onFieldHsn: false
        })

    }
    updateSite(data) {
        this.setState({
            siteCode: data.siteCode,
            siteName: data.siteName
        })
        document.getElementById("siteName").focus()
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }

    }
    openSiteModal() {
        this.setState({
            siteModal: true,
            siteModalAnimation: !this.state.siteModalAnimation
        })

        let data = {
            type: this.state.isModalShow ? 1 : 3,
            no: 1,
            search: this.state.isModalShow ? "" : this.state.siteName
        }
        this.props.procurementSiteRequest(data)
        this.setState({
            siteSearch: this.state.siteName
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }

    }
    closeSiteModal() {
        this.setState({
            siteModal: false,
            siteModalAnimation: !this.state.siteModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    choosedataKeydown(e) {

        if (e.key == "Enter") {
            document.getElementById("chooseDsd").click();

        }


    }
    openCityModal() {
        if (this.state.department != "") {
            this.setState({
                cityModal: true,
                cityModalAnimation: !this.state.cityModalAnimation
            })

            let data = {
                type: this.state.isModalShow ? 1 : 3,
                no: 1,
                search: this.state.isModalShow ? "" : this.state.city
            }
            this.setState({
                citySearch: this.state.isModalShow ? "" : this.state.city
            })
            this.props.getAllCityRequest(data)
            document.onkeydown = function (t) {
                if (t.which == 9) {
                    return false;
                }
            }
        } else {
            this.setState({
                toastMsg: "Select Department",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return false;
            }
        }
    }
    closeCity() {
        this.setState({
            cityModal: false,
            cityModalAnimation: !this.state.cityModalAnimation
        })
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    updateCity(data) {
        console.log(data)
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        document.getElementById("city").focus()
        this.setState({
            city: data.city,
            flag: this.state.flag + 1,
            supplier: "",
            stateCode: "",
            slcode: "",
            slName: "",
            slAddr: "",
            transporterCode: "",
            transporterName: "",

            transporter: "",
            tradeGrpCode: "",
            item: "",
            purtermMainCode: "",
            termName: "",
            slCityName: "",
            piAmount: 0,
            piQuantity: 0,

        })

    }
    handleInput(e) {

        if (e.target.id == "supplier") {
            this.setState({
                supplier: e.target.value,
                supplierSearch:this.state.isModalShow ?"":e.target.value
            })


        } else if (e.target.id == "city") {
            this.setState({
                city: e.target.value,
                citySearch:this.state.isModalShow?"":e.target.value
            })
        } else if (e.target.id == "siteName") {
            this.setState({
                siteName: e.target.value,
                siteSearch:this.state.isModalShow?"":e.target.value
            })
        } else if (e.target.id == "transporter") {
            this.setState({
                transporter: e.target.value,
                transporterSearch:this.state.isModalShow?"":e.target.value
            })

        }
    }


    render() {
        const { city, cityerr, siteName, siteNameerr, supplier, supplierText, search, transporter, leadDays, leadDayserr, item, itemerr } = this.state;
        const { transport, transporterr, vendorerr, division, divisionerr, department, departmenterr, section, sectionerr } = this.state;

        return (
            <div className="container_div pires-pg" id="">
                <form>
                    <div className=" col-lg-12 col-md-12 col-sm-12 pad-0 p-lr-47">
                        <div className="purchasedContainer piInputContain">
                            <div className="col-lg-9 col-md-6 col-sm-12 float_Left">
                                <ul className="list_style">
                                    <li>
                                        <label className="contribution_mart">
                                            PURCHASE INDENT
                                        </label>
                                    </li>
                                    <li>
                                        <p className="master_para">Create purchase indent</p>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-12 float_Left">
                                <ul className="list_style purchaseList">
                                    <li>
                                        <label className="contribution_mart m-lft-30">
                                            ASSORTMENT ON THE BASIS OF
                                        </label>
                                    </li>
                                    <li>
                                        <ul className="pad-0 list-inline m-top-5 text-left purchaseList">
                                            <li>
                                                <label className="containerPurchaseRadio displayPointer">ARTICLE
                                            <input type="radio" name="radio" id="article" checked={this.state.radio === "article"} onChange={(e) => this.onhChange('article')} />
                                                    <span className="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label className="containerPurchaseRadio m-l-20  textDisable pointerNone">DESIGN
                                            <input type="radio" name="radio" id="design" checked={this.state.radio === "design"} onChange={(e) => this.onhChange('design')} disabled />
                                                    <span className="checkmark"></span>
                                                </label>
                                            </li>


                                            <li>
                                                <label className="containerPurchaseRadio textDisable pointerNone">SET BASED
                                            <input type="radio" name="radio" id="sets" className="btnDisabled pointerNone" checked={this.state.radio === "sets"} onChange={(e) => this.onhChange('sets')} disabled />
                                                    <span className="checkmark"></span>
                                                </label>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-10 mo-change">
                                <div className="col-lg-9 col-md-8 col-sm-12 pr-order-2">
                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0 alignEnd">
                                        <div className="piFormDiv  piInputWid">
                                            <label className="purchaseLabel">
                                                Division<span className="mandatory">*</span>
                                            </label>

                                            <input type="text" className={divisionerr ? "errorBorder purchaseSelectBox onFocus" : "purchaseSelectBox onFocus"} value={division} disabled />
                                            {divisionerr ? (
                                                <span className="error">Select division</span>) : null}
                                        </div>
                                        <div className="piFormDiv piInputWid">
                                            <label className="purchaseLabel">
                                                Section<span className="mandatory">*</span>
                                            </label>
                                            <input type="text" className={sectionerr ? "errorBorder purchaseSelectBox onFocus" : "purchaseSelectBox onFocus"} value={section} disabled />
                                            {sectionerr ? (
                                                <span className="error">Select section</span>) : null}
                                        </div>

                                        <div className="piFormDiv piInputWid">
                                            <label className="purchaseLabel">
                                                Department<span className="mandatory">*</span>
                                            </label>
                                            <input type="text" className={departmenterr ? "errorBorder purchaseSelectBox onFocus" : "purchaseSelectBox onFocus"} value={department} disabled />
                                            {departmenterr ? (
                                                <span className="error">Select department</span>) : null}
                                        </div>
                                        <div className="piFormDiv piInputWid">
                                            <button className="dataPiBtn onFocus" type="button" ref={this.textInput} onKeyDown={(e) => this.choosedataKeydown(e)} id="chooseDsd" onClick={(e) => this.openSelection(e)}>
                                                Choose Data
                                             </button>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12 col-sm-12 pad-0 m-top-25 m-top-0">
                                        {sessionStorage.getItem("partnerEnterpriseName") == "VMART" || this.state.isSiteExist == "true" ? <div className="piFormDiv piInputWid verticalBot">
                                            <label className="purchaseLabel">
                                                Site{this.state.isSiteExist == "true" ? <span className="mandatory">*</span> : null}
                                            </label>
                                            <div onClick={(e) => this.openSiteModal(e)} className={siteNameerr ? "errorBorder userModalSelect piChooseSupplier displayPointer tdFocus" : " userModalSelect piChooseSupplier displayPointer tdFocus"}>
                                                <div className="topToolTip toolTipSupplier">
                                                    {/*<label className="chooseSupplyWidth displayPointer" value={siteName}>{siteName == "" ? "Choose Site" : siteName}
                                                    </label>*/}
                                                    {!this.state.isModalShow ?
                                                        <input type="text" onChange={(e) => this.handleInput(e)} className="chooseSupplyWidth displayPointer inputLabel inputlimitText " onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" />

                                                        : <input type="text" readOnly onClick={(e) => this.openSiteModal(e)} className="chooseSupplyWidth displayPointer inputLabel inputlimitText " onKeyDown={(e) => this._handleKeyPress(e, "siteName")} id="siteName" value={siteName} placeholder="Choose Site" />}

                                                    {siteName != "" && siteName != "NA" ? <span className="topToolTipText">{siteName}</span> : null}

                                                </div>

                                                <span className="modalBlueBtn" onClick={(e) => this.openSiteModal(e)} >▾</span>
                                                {!this.state.isModalShow ? this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null : null}

                                            </div>
                                            {siteNameerr ? (<span className="error">Select Site</span>) : null}
                                        </div> : null}
                                        {this.state.isCityExist ? <div className="piFormDiv piInputWid verticalBot">
                                            <label className="purchaseLabel">
                                                City<span className="mandatory">*</span>
                                            </label>
                                            <div className="userModalSelect piChooseSupplier displayPointer tdFocus">
                                                <div className="topToolTip toolTipSupplier">
                                                    {/*<label className="chooseSupplyWidth displayPointer" value={siteName}>{siteName == "" ? "Choose Site" : siteName}
                                                    </label>*/}
                                                    {!this.state.isModalShow ?
                                                        <input type="text" onChange={(e) => this.handleInput(e)} className="chooseSupplyWidth displayPointer inputLabel inputlimitText displayInline " onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={this.state.city} placeholder="Choose City" />

                                                        : <input type="text" readOnly onClick={(e) => this.openCityModal(e)} className="chooseSupplyWidth displayPointer inputLabel inputlimitText displayInline " onKeyDown={(e) => this._handleKeyPress(e, "city")} id="city" value={this.state.city} placeholder="Choose City" />}

                                                    {city != "" && city != "NA" ? <span className="topToolTipText">{city}</span> : null}
                                                </div>
                                                <span onClick={(e) => this.openCityModal(e)} className={cityerr ? "errorBorder  piChooseSupplier displayPointer" : " piChooseSupplier displayPointer tdFocus modalBlueBtn"} >▾</span>

                                                {!this.state.isModalShow ? this.state.cityModal ? <CityModal city={this.state.city} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null : null}

                                            </div>
                                            {cityerr ? (<span className="error">Select city</span>) : null}
                                        </div> : null}
                                        <div className="piFormDiv piInputWid verticalBot ">
                                            <label className="purchaseLabel">
                                                Vendor<span className="mandatory">*</span>
                                            </label>
                                            <div className={vendorerr ? "errorBorder userModalSelect piChooseSupplier displayPointer tdFocus" : " userModalSelect piChooseSupplier displayPointer tdFocus"} >
                                                <div className="topToolTip toolTipSupplier">
                                                    {/*<label className="chooseSupplyWidth displayPointer" value={supplier}>{supplier == "" ? "Choose Vendor" : supplier}
                                                    </label>*/}

                                                    {!this.state.isModalShow ? <input type="text" id="supplier" onChange={(e) => this.handleInput(e)} value={supplier} placeholder="Choose Vendor" onKeyDown={(e) => this._handleKeyPress(e, "supplier")} className="chooseSupplyWidth displayPointer inputLabel inputlimitText displayInline" />
                                                        : <input type="text" id="supplier" readOnly onClick={(e) => this.openSupplier(e)} value={supplier == "" ? "Choose Vendor" : supplier} onKeyDown={(e) => this._handleKeyPress(e, "supplier")} className="chooseSupplyWidth displayPointer inputLabel inputlimitText displayInline" />}
                                                    {supplier != "" ? <span className="topToolTipText">{supplier}</span> : ""}
                                                </div>
                                                <span onClick={(e) => this.openSupplier(e)} className="modalBlueBtn" id="supplier">▾</span>

                                                {!this.state.isModalShow ? this.state.supplierModal ? <SupplierModal {...this.props}{...this.state} city={this.state.city} supplierSearch={this.state.supplierSearch} siteName={this.state.siteName} siteCode={this.state.siteCode} departmentCode={this.state.hl3Code} department={this.state.department} supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null : null}

                                            </div>
                                            {vendorerr ? (<span className="error">Select vendor</span>) : null}
                                        </div>
                                        <div className="piFormDiv piInputWid tdFocus">
                                            <label className="purchaseLabel">
                                                Lead Day(s)<span className="mandatory">*</span>
                                            </label>
                                            <input type="text" readOnly value={leadDays} id="leadDays" className={leadDayserr ? " errorBorder purchaseTextbox " : "purchaseTextbox "} disabled />
                                            {leadDayserr ? (
                                                <span className="error">Select leadDays</span>) : null}
                                        </div>
                                        <div className="piFormDiv piInputWid ">
                                            <label className="purchaseLabel">
                                                Term<span className="mandatory">*</span>
                                            </label>
                                            <input type="text" readOnly className={itemerr ? "errorBorder purchaseSelectBox " : "purchaseSelectBox"} id="termData" value={item} placeholder="" disabled />
                                            {itemerr ? (
                                                <span className="error">Select Term</span>) : null}
                                        </div>
                                        <div className="piFormDiv piInputWid verticalBot ">
                                            <label className="purchaseLabel">
                                                Transporter{this.state.transporterValidation ? <span className="mandatory">*</span> : null}
                                            </label>
                                            <div className={transporterr ? "errorBorder userModalSelect piChooseSupplier displayPointer tdFocus " : " userModalSelect piChooseSupplier displayPointer tdFocus"}  >
                                                <div className="topToolTip toolTipSupplier">


                                                    {!this.state.isModalShow ?
                                                        <input type="text" onChange={(e) => this.handleInput(e)} className="chooseSupplyWidth displayPointer inputLabel inputlimitText " onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" />
                                                        : <input type="text" readOnly className="chooseSupplyWidth displayPointer inputLabel inputlimitText " onKeyDown={(e) => this._handleKeyPress(e, "transporter")} id="transporter" value={transporter} placeholder="Choose transporter" onClick={(e) => this.openTransporterSelection(e)} />}


                                                    {transporter != "" && transporter != "NA" ? <span className="topToolTipText">{transporter}</span> : null}
                                                </div>
                                                <span onClick={(e) => this.openTransporterSelection(e)} className="modalBlueBtn" >▾</span>
                                                {!this.state.isModalShow ? this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} transporterState={this.state.transporterState} isTransporterDependent={this.state.isTransporterDependent} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} city={this.state.city} /> : null : null}

                                            </div>
                                            {transporterr ? (<span className="error">Select transporter</span>) : null}
                                        </div>

                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-12 pad-0 pr-order-1">
                                    <div className="rightSideDiv">
                                        <ul>
                                            <li>
                                                <label>
                                                    PI Quantity
                                                </label>
                                                <span>
                                                    {this.state.piQuantity}
                                                </span>
                                            </li>
                                            <li>
                                                <label>
                                                    PI Amount
                                                </label>
                                                <span>
                                                    &#8377;
                                            {this.state.piAmount}
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <PurchaseIndentTable  {...this.props} {...this.state} isDisplayMarginRule={this.state.isDisplayMarginRule} displayOtb={this.state.displayOtb} addRow={this.state.addRow} isVendorDesignNotReq={this.state.isVendorDesignNotReq} ref="indentTable" isCityExist={this.state.isCityExist} city={this.state.city} siteCode={this.state.siteCode} isDiscountAvail={this.state.isDiscountAvail} isDiscountMap={this.state.isDiscountMap} siteName={this.state.siteName} updateHsnvalue={(e) => this.updateHsnvalue(e)} onResetData={(e) => this.props.onResetData(e)} isUDFExist={this.state.isUDFExist} itemUdfExist={this.state.itemUdfExist} isSiteExist={this.state.isSiteExist} hl1Code={this.state.hl1Code} hl2Code={this.state.hl2Code} hl3Code={this.state.hl3Code} slcode={this.state.slcode} hl3Code={this.state.hl3Code} department={this.state.department} division={this.state.division} section={this.state.section} getRows={(e) => this.props.getRows(e)} resetForm={(e) => this.resetForm(e)} flag={this.state.flag} pIData={pIData} getPiAmount={(e) => this.getPiAmount(e)} getPiQuantity={(e) => this.getPiQuantity(e)}
                        cat1Validation={this.state.cat1Validation} cat2Validation={this.state.cat2Validation} cat3Validation={this.state.cat3Validation} cat4Validation={this.state.cat4Validation} cat5Validation={this.state.cat5Validation} cat6Validation={this.state.cat6Validation}
                        desc1Validation={this.state.desc1Validation} desc2Validation={this.state.desc2Validation} desc3Validation={this.state.desc3Validation} desc4Validation={this.state.desc4Validation} desc5Validation={this.state.desc5Validation} desc6Validation={this.state.desc6Validation}
                        purtermMainCode={this.state.purtermMainCode} slCityName={this.state.slCityName} stateCode={this.state.stateCode} radio={this.state.radio} onSubmit={(e) => this.onSubmit(e)} confirmModal={(e) => this.props.confirmModal(e)}

                        itemudf1Validation={this.state.itemudf1Validation} itemudf2Validation={this.state.itemudf2Validation} itemudf3Validation={this.state.itemudf3Validation} itemudf4Validation={this.state.itemudf4Validation} itemudf5Validation={this.state.itemudf5Validation}
                        itemudf6Validation={this.state.itemudf6Validation} itemudf7Validation={this.state.itemudf7Validation} itemudf8Validation={this.state.itemudf8Validation} itemudf9Validation={this.state.itemudf9Validation} itemudf10Validation={this.state.itemudf10Validation}
                        itemudf11Validation={this.state.itemudf11Validation} itemudf12Validation={this.state.itemudf12Validation} itemudf13Validation={this.state.itemudf13Validation} itemudf14Validation={this.state.itemudf14Validation} itemudf15Validation={this.state.itemudf15Validation}
                        itemudf16Validation={this.state.itemudf16Validation} itemudf17Validation={this.state.itemudf17Validation} itemudf18Validation={this.state.itemudf18Validation} itemudf19Validation={this.state.itemudf19Validation} itemudf20Validation={this.state.itemudf20Validation}

                        udf1Validation={this.state.udf1Validation} udf2Validation={this.state.udf2Validation} udf3Validation={this.state.udf3Validation} udf4Validation={this.state.udf4Validation} udf5Validation={this.state.udf5Validation}
                        udf6Validation={this.state.udf6Validation} udf7Validation={this.state.udf7Validation} udf8Validation={this.state.udf8Validation} udf9Validation={this.state.udf9Validation} udf10Validation={this.state.udf10Validation}
                        udf11Validation={this.state.udf11Validation} udf12Validation={this.state.udf12Validation} udf3Validation={this.state.udf13Validation} udf14Validation={this.state.udf14Validation} udf15Validation={this.state.udf15Validation}
                        udf16Validation={this.state.udf16Validation} udf17Validation={this.state.udf17Validation} udf18Validation={this.state.udf18Validation} udf19Validation={this.state.udf19Validation} udf20Validation={this.state.udf20Validation}
                        isMrpRequired={this.state.isMrpRequired} isDisplayFinalRate={this.state.isDisplayFinalRate}
                        isRspRequired={this.state.isRspRequired}
                        isColorRequired={this.state.isColorRequired}
                        isGSTDisable={this.state.isGSTDisable}
                        isTaxDisable={this.state.isTaxDisable}
                        isBaseAmountActive={this.state.isBaseAmountActive}
                    />
                    {this.state.isModalShow ? this.state.supplierModal ? <SupplierModal {...this.props}{...this.state} city={this.state.city} supplierSearch={this.state.supplierSearch} siteName={this.state.siteName} siteCode={this.state.siteCode} departmentCode={this.state.hl3Code} department={this.state.department} supplierCode={this.state.supplierCode} closeSupplier={(e) => this.openSupplier(e)} supplierModalAnimation={this.state.supplierModalAnimation} supplierState={this.state.supplierState} updateSupplierState={(e) => this.updateSupplierState(e)} onCloseSupplier={(e) => this.onCloseSupplier(e)} /> : null : null}

                    {this.state.isModalShow ? this.state.trasporterModal ? <TransporterSelection {...this.props} {...this.state} transporterState={this.state.transporterState} isTransporterDependent={this.state.isTransporterDependent} transporter={this.state.transporter} transportCloseModal={(e) => this.openTransporterSelection(e)} onCloseTransporter={(e) => this.onCloseTransporter(e)} updateTransporterState={(e) => this.updateTransporterState(e)} transporterAnimation={this.state.transporterAnimation} closetransporterSelection={(e) => this.openTransporterSelection(e)} city={this.state.city} /> : null : null}

                    {this.state.selectionModal ? <SectionDataSelection {...this.props} {...this.state} selectedDepartment={this.state.selectedDepartment} divisionState={this.state.divisionState} departmentCode={this.state.hl3Code} sectionCloseModal={(e) => this.sectionCloseModal(e)} selectionCloseModal={(e) => this.selectionCloseModal(e)} updateState={(e) => this.updateState(e)} selectionAnimation={this.state.selectionAnimation} closeSelection={(e) => this.closeSelection(e)} /> : null}
                </form>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.isModalShow ? this.state.siteModal ? <SiteModal {...this.props} {...this.state} siteModal={this.state.siteModal} siteModalAnimation={this.state.siteModalAnimation} closeSiteModal={(e) => this.closeSiteModal(e)} updateSite={(e) => this.updateSite(e)} /> : null : null}
                {this.state.isModalShow ? this.state.cityModal ? <CityModal {...this.props} {...this.state} city={this.state.city} updateCity={(e) => this.updateCity(e)} closeCity={(e) => this.closeCity(e)} {...this.props} cityModalAnimation={this.state.cityModalAnimation} /> : null : null}

            </div>

        );
    }

}


export default PurchaseIndentForm;