import React from 'react';
import ToastLoader from '../loaders/toastLoader';
import ItemDetailsModal from './itemDetailsModal'
import PoError from '../loaders/poError';


// old code
class PurchaseItemTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cat1Validation: this.props.cat1Validation,
            cat2Validation: this.props.cat2Validation,
            cat3Validation: this.props.cat3Validation,
            cat4Validation: this.props.cat4Validation,
            cat5Validation: this.props.cat5Validation,
            cat6Validation: this.props.cat6Validation,
            desc1Validation: this.props.desc1Validation,
            desc2Validation: this.props.desc2Validation,
            desc3Validation: this.props.desc3Validation,
            desc4Validation: this.props.desc4Validation,
            desc5Validation: this.props.desc5Validation,
            desc6Validation: this.props.desc6Validation,
            toastLoader: false,
            toastMsg: "",
            itemDetailsModal: false,
            itemDetailsModalAnimation: false,
            itemType: "",
            itemId: "",
            itemDetailValue: "",
            poErrorMsg: false,
            errorMassage: "",
            itemDetailName: "",
            focusId:"",
            itemDetailCode:""
        }
    }
    componentWillMount(){
        this.setState({
            cat1Validation: this.props.cat1Validation,
            cat2Validation: this.props.cat2Validation,
            cat3Validation: this.props.cat3Validation,
            cat4Validation: this.props.cat4Validation,
            cat5Validation: this.props.cat5Validation,
            cat6Validation: this.props.cat6Validation,

            desc1Validation: this.props.desc1Validation,
            desc2Validation: this.props.desc2Validation,
            desc3Validation: this.props.desc3Validation,
            desc4Validation: this.props.desc4Validation,
            desc5Validation: this.props.desc5Validation,
            desc6Validation: this.props.desc6Validation,
        })
    }

    openItemModal(type, id, value, displayName, idcat,code) {
      
        var rows = this.props.rows

        var flag = false
        for (var i = 0; i < rows.length; i++) {
            if ((rows[i].vendorDesign == "" && !this.props.isVendorDesignNotReq) || rows[i].date == "" || rows[i].articleCode == "") {
                flag = false
            } else {
                flag = true
            }
        }
        if (flag) {
            var data = {
                hl3Code: this.props.hl3Code,
                hl3Name: this.props.department,
                itemType: type,
                id: id,
                no: 1,
                type: 1,
                search: ""
            }
            this.props.getItemDetailsValueRequest(data);
            this.setState({
                focusId: idcat,
                itemDetailsModal: true,
                itemDetailsModalAnimation: !this.state.itemDetailsModalAnimation,
                itemId: id,
                itemDetailValue: value,
                itemDetailCode:code,
                itemType: type,
                itemDetailName: typeof (displayName) == "string" && displayName != "null" ? displayName : type

            })
        } else {
            this.validationRows()
        }
    }

    validationRows() {
        let rows = this.props.rows
        rows.forEach(pi => {

            if (pi.vendorDesign == "" && !this.props.isVendorDesignNotReq) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor Design is compulsory"
                })

            } else if (pi.date == "") {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Select delivery date from indent description"
                })
            } else if (pi.articleCode == "") {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor mrp is compulsory"
                })
            }
        })
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    closeItemModal() {
        this.setState({
            itemDetailsModal: false,
            itemDetailsModalAnimation: !this.state.itemDetailsModalAnimation,
        })
        window.setTimeout(()=>{
            document.getElementById(this.state.focusId).focus();
        },0)
    }
    updateItemDetailValue() {
        this.setState({
            itemDetailValue: ""
        })
          window.setTimeout(()=>{
            document.getElementById(this.state.focusId).focus();
        },0)
    }
      _handleKeyPress(e, id) {

        let idd = id;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }
    }

    render() {
       
        return (
            <div>
                <div className="col-md-12 col-sm-12 pad-0 m-top-35">
                    <ul className="list_style">
                        <li>
                            <label className="contribution_mart">
                                ITEM DETAILS
                            </label>
                        </li>
                        <li>
                            <p className="master_para">Manage Item details for all product</p>
                        </li>
                    </ul>
                </div>

                <div className="col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionOneTable bordere3e7f3 poTableMain">
                    <div className="zui-scroller pad-0 m0">
                        <table className="table scrollTable table scrollTable main-table zui-table border-bot-table displayTable" >
                            <thead>
                                <tr>
                                {!this.props.isVendorDesignNotReq?    <th className="positionRelative">
                                        <label>
                                            Vendor Design
                            </label>
                                    </th>:null}
                                    <th className="positionRelative">
                                        <label>
                                            Article Code
                            </label>
                                    </th>

                                    <th className="positionRelative">
                                        <label>
                                            Delivery Date
                            </label>
                                    </th>
                                    {this.props.itemDetailsHeader.length != 0 ? this.props.itemDetailsHeader.map((data, keyyy) => (
                                        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ?
                                            data.catdesc != "CAT5" && data.catdesc != "CAT6" && data.catdesc != "DESC1" && data.catdesc != "DESC4" && data.catdesc != "DESC6" ? <th id={keyyy} key={keyyy} >
                                                <label>
                                                    {data.displayName != null ? data.displayName : data.catdesc}{data.isCompulsory=="Y"?<span className="mandatory">*</span>:null}
                                                </label>
                                            </th> : null
                                            :
                                             ((data.catdesc == "CAT1" && (this.props.cat1Validation == true || data.isLov == "Y")) || (data.catdesc == "CAT2" && (this.props.cat2Validation == true || data.isLov == "Y")) || (data.catdesc == "CAT3" && (this.props.cat3Validation == true || data.isLov == "Y")) || (data.catdesc == "CAT4" && (this.props.cat4Validation == true || data.isLov == "Y")) || (data.catdesc == "CAT5" && (this.props.cat5Validation == true || data.isLov == "Y")) || (data.catdesc == "CAT6" && (this.props.cat6Validation == true || data.isLov == "Y")) || (data.catdesc == "DESC1" && (this.props.desc1Validation == true || data.isLov == "Y")) || (data.catdesc == "DESC2" && (this.props.desc2Validation == true || data.isLov == "Y")) || (data.catdesc == "DESC3" && (this.props.desc3Validation == true || data.isLov == "Y")) || (data.catdesc == "DESC4" && (this.props.desc4Validation == true || data.isLov == "Y")) || (data.catdesc == "DESC5" && (this.props.desc5Validation == true || data.isLov == "Y")) || (data.catdesc == "DESC6" && (this.props.desc6Validation == true || data.isLov == "Y")) ?
                                                <th key={keyyy}>
                                                    <label>
                                                        {typeof (data.displayName) == "string" ? data.displayName : data.catdesc}{data.isCompulsory=="Y"?<span className="mandatory">*</span>:null}
                                                        {/* {data.displayName!=null? data.catdesc:data.displayName} */}
                                                    </label>
                                                </th> : null
                                            ))) : null}
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.itemDetails.length != 0 ? this.props.itemDetails.map((itemGrid, idx) => (
                                    <tr id={idx} key={idx} >
                                  {!this.props.isVendorDesignNotReq?   <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input autoComplete="off" type="text" name="vendordesign" className="inputTable " readOnly value={itemGrid.vendorDesign} />

                                        </td>:null}
                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input autoComplete="off" type="text" name="article" className="inputTable " readOnly value={itemGrid.articleCode} />

                                        </td>
                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input type="text" className="inputTable" placeholder="" id="date" name="date" readOnly value={itemGrid.deliveryDate} />
                                        </td>   
                                        {itemGrid.itemList.map((data, kkey) => (
                                            sessionStorage.getItem("partnerEnterpriseName") == "VMART" ?
                                                data.catdesc != "CAT5" && data.catdesc != "CAT6" && data.catdesc != "DESC1" && data.catdesc != "DESC4" && data.catdesc != "DESC6" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={kkey}  onClick={(e) => this.openItemModal(`${data.catdesc}`, `${itemGrid.itemGridId}`, `${data.value}`, `${data.displayName}`,`${data.catdesc+itemGrid.itemGridId}`,data.code)}>
                                                    <input autoComplete="off" type="text" name="" className="inputTable " value={data.value} id={ data.catdesc+itemGrid.itemGridId} onKeyDown={(e) => this._handleKeyPress(e,data.catdesc+itemGrid.itemGridId)} />
                                                    <div className="purchaseTableDiv" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                            <g fill="none" fillRule="evenodd">
                                                                <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </td> : null
                                                :
                                                 ((data.catdesc == "CAT1" && (this.props.cat1Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "CAT2" && (this.props.cat2Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "CAT3" && (this.props.cat3Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "CAT4" && (this.props.cat4Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "CAT5" && (this.props.cat5Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "CAT6" && (this.props.cat6Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "DESC1" && (this.props.desc1Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "DESC2" && (this.props.desc2Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "DESC3" && (this.props.desc3Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "DESC4" && (this.props.desc4Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "DESC5" && (this.props.desc5Validation == true || data.isLov == "Y")) ||
                                                    (data.catdesc == "DESC6" && (this.props.desc6Validation == true || data.isLov == "Y"))) ?
                                                    <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={kkey}  >
                                                        <input autoComplete="off" type="text" name="" className="inputTable " value={data.value} id={data.catdesc+itemGrid.itemGridId} id={ data.catdesc+itemGrid.itemGridId} onClick={(e) => this.openItemModal(`${data.catdesc}`, `${itemGrid.itemGridId}`, `${data.value}`, `${data.displayName}`, `${data.catdesc+itemGrid.itemGridId}`,data.code)} onKeyDown={(e) => this._handleKeyPress(e,data.catdesc+itemGrid.itemGridId)}/>
                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>

                                                        </div>

                                                    </td> : null
                                        ))}
                                    </tr>

                                )) : <tr className="modalTableNoData genericNodata"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                            </tbody>

                        </table>            </div>
                    {/* </div> */}
                </div>

                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.itemDetailsModal ? <ItemDetailsModal {...this.props} {...this.state} updateItemDetailValue={(e) => this.updateItemDetailValue(e)} itemDetailName={this.state.itemDetailName} itemDetailValue={this.state.itemDetailValue} itemDetailCode= {this.state.itemDetailCode} closeItemModal={(e) => this.closeItemModal(e)} itemDetailsModalAnimation={this.state.itemDetailsModalAnimation} itemDetailsModal={this.state.itemDetailsModal} openItemModal={(e) => this.openItemModal(e)} itemType={this.state.itemType} itemId={this.state.itemId} /> : null}

            </div>

        );
    }
}

export default PurchaseItemTable;