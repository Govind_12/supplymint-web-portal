import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class PiMrpModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            checked: false,
            vendorMrpState: this.props.vendorMrpState,
            selectedId: "",
            id: "",
            addNew: true,
            save: false,
            mrp: "",
            articleName: "",

            rsp: "",
            done: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            searchMrp: "",
            toastLoader: false,
            sectionSelection: "",
            toastMsg: "",
            rows: this.props.rows,
            iCode: "",
            idx: this.props.idx,
            searchBy: "startWith",
            focusedLi: ""
        }
    }

    componentDidMount() {
         if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }

    componentWillMount() {

        this.setState({
            id: this.props.rowId,
            rows: this.props.rows,
            idx: this.props.idx,
            searchMrp: this.props.isModalShow ? "" : this.props.mrpSearch

        })
        if (this.props.purchaseIndent.vendorMrp.isSuccess) {

            if (this.props.purchaseIndent.vendorMrp.data.resource != null) {

                this.setState({
                    vendorMrpState: this.props.purchaseIndent.vendorMrp.data.resource,
                    prev: this.props.purchaseIndent.vendorMrp.data.prePage,
                    current: this.props.purchaseIndent.vendorMrp.data.currPage,
                    next: this.props.purchaseIndent.vendorMrp.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.vendorMrp.data.maxPage,
                })

            }
            else {
                this.setState({
                    vendorMrpState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

    }

    componentWillReceiveProps(nextProps) {
        //(nextProps)
        this.setState({
            id: nextProps.rowId,
            rows: this.props.rows,
            idx: this.props.idx
        })
        if (nextProps.purchaseIndent.vendorMrp.isSuccess) {

            if (nextProps.purchaseIndent.vendorMrp.data.resource != null) {

                this.setState({
                    vendorMrpState: nextProps.purchaseIndent.vendorMrp.data.resource,
                    prev: nextProps.purchaseIndent.vendorMrp.data.prePage,
                    current: nextProps.purchaseIndent.vendorMrp.data.currPage,
                    next: nextProps.purchaseIndent.vendorMrp.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.vendorMrp.data.maxPage,
                })

            }
            else {
                this.setState({
                    vendorMrpState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

            if (window.screen.width > 1200) {
                if (this.props.isModalShow || this.props.isModalShow==undefined) {

                    document.getElementById("searchMrp").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.vendorMrp.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.vendorMrp.data.resource[0].cname
                        })
                        document.getElementById(nextProps.purchaseIndent.vendorMrp.data.resource[0].cname) != null ? document.getElementById(nextProps.purchaseIndent.vendorMrp.data.resource[0].cname).focus() : null
                    }


                } 
            }
        }
    }

    onAddNew(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: false,
                save: true,
                searchMrp: "",
            })
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        });
    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }


    onSave(e) {
        var array = this.state.vendorMrpState;
        for (let i = 0; i < c.length; i++) {
            if (c[i].id == id) {
                this.state.rows[this.props.idx].iCode = c[i].id
            }

        }
        this.setState({
            vendorMrpState: c
        })

        let c = {
            id: this.state.vendorMrpState.length + 1,
            articleName: this.state.articleName,

            mrp: this.state.mrp,
            rsp: this.state.rsp,
            mrpStart: this.state.mrpStart,
            mrpEnd: this.state.mrpEnd


        };

        array.push(c);
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false,
                vendorMrpState: array
            }
        )
    }
    onCancel(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false
            }
        )
    }

    selectedData(id) {
        let c = this.state.vendorMrpState;
        for (let i = 0; i < c.length; i++) {
            if (c[i].id == id) {
                this.state.rows[this.props.idx].iCode = c[i].id
            }
        }
        this.setState({

            vendorMrpState: c
       } , () => {
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
                })
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.vendorMrp.data.prePage,
                current: this.props.purchaseIndent.vendorMrp.data.currPage,
                next: this.props.purchaseIndent.vendorMrp.data.currPage + 1,
                maxPage: this.props.purchaseIndent.vendorMrp.data.maxPage,
            })
            if (this.props.purchaseIndent.vendorMrp.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.vendorMrp.data.currPage - 1,

                    code: this.props.slcode,
                    articleName: this.state.articleName,
                    mrp: this.state.mrp,
                    rsp: this.state.rsp,
                    search: this.state.searchMrp,
                    department: this.props.department,
                    searchBy: this.state.searchBy
                }
                this.props.vendorMrpRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.vendorMrp.data.prePage,
                current: this.props.purchaseIndent.vendorMrp.data.currPage,
                next: this.props.purchaseIndent.vendorMrp.data.currPage + 1,
                maxPage: this.props.purchaseIndent.vendorMrp.data.maxPage,
            })
            if (this.props.purchaseIndent.vendorMrp.data.currPage != this.props.purchaseIndent.vendorMrp.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.vendorMrp.data.currPage + 1,
                    code: this.props.slcode,
                    articleName: this.state.articleName,
                    mrp: this.state.mrp,
                    rsp: this.state.rsp,
                    search: this.state.searchMrp,
                    department: this.props.department,
                    searchBy: this.state.searchBy
                }
                this.props.vendorMrpRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.vendorMrp.data.prePage,
                current: this.props.purchaseIndent.vendorMrp.data.currPage,
                next: this.props.purchaseIndent.vendorMrp.data.currPage + 1,
                maxPage: this.props.purchaseIndent.vendorMrp.data.maxPage,
            })
            if (this.props.purchaseIndent.vendorMrp.data.currPage <= this.props.purchaseIndent.vendorMrp.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    code: this.props.slcode,
                    articleName: this.state.articleName,
                    mrp: this.state.mrp,
                    rsp: this.state.rsp,
                    search: this.state.searchMrp,
                    department: this.props.department,
                    searchBy: this.state.searchBy
                }
                this.props.vendorMrpRequest(data)
            }

        }
    }
    onDone() {
        let flag = false;
        let c = this.state.vendorMrpState;
        let idd = "";
        let articleCode = ""
        let articleName = ""
        let mrp = ""
        let rsp = ""
        let mrpStart = ""
        let mrpEnd = ""
        let vendorMrp = ""
        let itemName = ""
        let iCode = ""
        let hsnSacCode = ""
        let data = {}

        if (c != undefined) {
            for (let i = 0; i < c.length; i++) {
                if (c[i].id == this.state.rows[this.props.idx].iCode) {
                    idd = c[i].id
                    articleCode = c[i].articleCode
                    articleName = c[i].articleName
                    mrp = c[i].mrp
                    rsp = c[i].rsp
                    mrpStart = c[i].mrpStart
                    mrpEnd = c[i].mrpEnd
                    iCode = c[i].iCode
                    hsnSacCode = c[i].id
                    flag = true
                    break
                }
                else {
                    flag = false
                }

            }
        } else {
            this.setState({
                done: true
            })
        }

        if (flag) {
            this.setState({
                done: true,
                search: "",
            })

            data = {
                id: idd,
                rId: this.state.id,
                articleCode: articleCode,
                articleName: articleName,
                mrp: mrp,
                rsp: rsp,
                mrpStart: mrpStart,
                mrpEnd: mrpEnd,
                iCode: iCode,
                hsnSacCode: hsnSacCode
            }

        } else {
            this.setState({
                done: false
            })
        }
        if (Number(this.props.iCode) != this.state.rows[this.props.idx].iCode) {

            for (let i = 0; i < c.length; i++) {
                if (this.state.rows[this.props.idx].iCode != "") {
                    if (c[i].id == this.state.rows[this.props.idx].iCode) {
                        this.props.updateMrpState(data);
                    } this.onMrpCloseModal();
                }
            }
        } else {
            for (let i = 0; i < c.length; i++) {
                if (this.state.rows[this.props.idx].iCode == "") {
                    this.setState({
                        toastMsg: "select data",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1000)
                } else { this.onMrpCloseModal(); }
            }
        }
    }
    onMrpCloseModal(e) {
        this.setState({
            searchMrp: "",
            sectionSelection: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            vendorMrpState: []
        })

        this.props.mrpCloseModal(e)
        // document.getElementById("mrpBasedOn").value=""
    }
    onSearch(e) {

        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,

            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false,

                })
            })
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,
                    code: this.props.slcode,
                    articleName: this.state.articleName,
                    mrp: this.state.mrp,
                    rsp: this.state.rsp,
                    search: this.state.searchMrp,
                    department: this.props.department,
                    searchBy: this.state.searchBy
                }
                this.props.vendorMrpRequest(data)
            }
            else {

                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    code: this.props.slcode,
                    articleName: this.state.articleName,
                    mrp: this.state.mrp,
                    rsp: this.state.rsp,
                    search: "",
                    department: this.props.department,
                    searchBy: this.state.searchBy
                }
                this.props.vendorMrpRequest(data)
            }
        }
    }
    onsearchClear() {

        if (this.state.type == 2 || this.state.type == 3) {

            var data = {
                code: this.props.slcode,
                type: "",
                no: 1,
                articleName: "",
                mrp: "",
                rsp: "",
                search: "",
                department: this.props.department,
                searchBy: this.state.searchBy
            }
            this.props.vendorMrpRequest(data)
        }

        this.setState(
            {
                searchMrp: "",
                sectionSelection: "",
                type: "",
                no: 1

            })
    }

    handleChange(e) {
        if (e.target.id == "rsp") {
            this.setState(
                {
                    rsp: e.target.value
                },

            );
        } else if (e.target.id == "articleName") {
            this.setState(
                {
                    articleName: e.target.value
                },

            );
        } else if (e.target.id == "mrp") {
            this.setState(
                {
                    mrp: e.target.value
                },

            );
        }
        else if (e.target.id == "mrpStart") {
            this.setState(
                {
                    mrpStart: e.target.value
                },

            );
        } else if (e.target.id == "mrpEnd") {
            this.setState(
                {
                    mrpEnd: e.target.value
                },

            );
        } else if (e.target.id == "searchMrp") {

            this.setState(
                {
                    searchMrp: e.target.value
                },

            );

            //     if(document.getElementById("mrpBasedOn").value=="articleName"){
            //         this.setState(
            //             {
            //                 articleName: e.target.value,
            //                 mrp:"",
            //                 rsp:""
            //             },

            //         );
            //     }
            //    else if(document.getElementById("mrpBasedOn").value=="mrp"){
            //         this.setState(
            //             {
            //                 articleName:"" ,
            //                 mrp:e.target.value,
            //                 rsp:""
            //             },

            //         );
            //     }    else if(document.getElementById("mrpBasedOn").value=="rsp"){
            //         this.setState(
            //             {
            //                 articleName:"" ,
            //                 mrp:"",
            //                 rsp:e.target.value
            //             },

            //         );
            //     }
            //     else if(document.getElementById("mrpBasedOn").value==""){
            //         this.setState(
            //             {
            //                 articleName:"" ,
            //                 mrp:"",
            //                 rsp:"",
            //                 searchMrp:e.target.value
            //             },


            //         );

            // }} else if (e.target.id == "mrpBasedOn") {
            //     this.setState(
            //         {
            //             searchMrp: "",
            //             sectionSelection:e.target.value
            //         }

            //     );

        } else if (e.target.id == "searchBymrp") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.searchMrp != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        code: this.props.slcode,
                        articleName: this.state.articleName,
                        mrp: this.state.mrp,
                        rsp: this.state.rsp,
                        search: this.state.searchMrp,
                        department: this.props.department,
                        searchBy: this.state.searchBy
                    }
                    this.props.vendorMrpRequest(data)
                }
            })
        }


    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
        if (e.key == "ArrowDown") {
            this.selectedData(this.state.vendorMrpState[0].id)
        }
    }
    checkedID() {
        let rows = this.state.rows
        for (i = 0; i < rows.length; i++) {
            if (rows[i].rowId == this.props.rowId) {
                this.setState({
                    iCode: rows[i].iCode
                })

            }
        }
    }




    //     _handleKeyDown = (e) => {

    //     if (e.key === "Tab") {
    //         if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
    //             if (this.state.vendorMrpState.length != 0) {
    //                 this.setState({

    //                     checkedArticle: this.state.piArticle[0].ARTICLECODE,


    //                 })
    //                 this.selectedData(this.state.piArticle[0].ARTICLECODE)
    //                 // let scode= this.state.supplierState[0].code
    //                 window.setTimeout(() => {
    //                     document.getElementById("searchArticle").blur()
    //                     document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
    //                 }, 0)
    //             } else {

    //                 window.setTimeout(() => {
    //                     document.getElementById("searchArticle").blur()
    //                     document.getElementById("closeButton").focus()
    //                 }, 0)
    //             }
    //         } else if (e.target.value != "") {
    //             window.setTimeout(() => {
    //                 document.getElementById("searchArticle").blur()
    //                 document.getElementById("findButton").focus()
    //             }, 0)
    //         }
    //     }
    //     if (e.key == "ArrowDown") {
    //         if (this.state.piArticle.length != 0) {
    //             this.setState({

    //                 checkedArticle: this.state.piArticle[0].ARTICLECODE,


    //             })
    //             this.selectedData(this.state.piArticle[0].ARTICLECODE)
    //             // let scode= this.state.supplierState[0].code
    //             window.setTimeout(() => {
    //                 document.getElementById("searchArticle").blur()
    //                 document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
    //             }, 0)
    //         } else {

    //             window.setTimeout(() => {
    //                 document.getElementById("searchArticle").blur()
    //                 document.getElementById("closeButton").focus()
    //             }, 0)
    //         }

    //     }
    //     this.handleChange(e)
    // }
    // findKeyDown(e) {
    //     if (e.key == "Enter") {
    //         this.onSearch()
    //     }
    //     if (e.key == "Tab") {
    //         window.setTimeout(() => {
    //             document.getElementById("findButton").blur()
    //             document.getElementById("clearButton").focus()
    //         }, 0)
    //     }

    // }
    // focusDone(e) {
    //     if (e.key === "Tab") {

    //         window.setTimeout(() => {
    //             document.getElementById(this.state.piArticle[0].ARTICLECODE).blur()
    //             document.getElementById("doneButton").focus()
    //         }, 0)
    //     }
    //     if (e.key === "Enter") {
    //         this.onDone()
    //     }
    // }
    // doneKeyDown(e) {
    //     if (e.key == "Enter") {
    //         this.onDone(e);
    //     }
    //     if (e.key == "Tab") {
    //         window.setTimeout(() => {
    //             document.getElementById("doneButton").blur()
    //             document.getElementById("closeButton").focus()
    //         }, 0)
    //     }
    // }
    // closeKeyDown(e) {
    //     if (e.key == "Enter") {
    //         this.closeArticleModal();
    //     }
    //     if (e.key == "Tab") {
    //         window.setTimeout(() => {
    //             document.getElementById("closeButton").blur()
    //             document.getElementById("searchArticle").focus()
    //         }, 0)
    //     }

    // }

    // onClearDown(e) {
    //     if (e.key == "Enter") {
    //         this.onsearchClear();
    //     }
    //     if (e.key == "Tab") {
    //         if (this.state.piArticle.length != 0) {
    //             this.setState({

    //                 checkedArticle: this.state.piArticle[0].ARTICLECODE,
    //             })
    //             this.selectedData(this.state.piArticle[0].ARTICLECODE)
    //             window.setTimeout(() => {
    //                 document.getElementById("clearButton").blur()
    //                 document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
    //             }, 0)
    //         } else {
    //             window.setTimeout(() => {
    //                 document.getElementById("clearButton").blur()
    //                 document.getElementById("closeButton").focus()
    //             }, 0)
    //         }
    //     }
    // }



selectLi(e, code) {
        let vendorMrpState = this.state.vendorMrpState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < vendorMrpState.length; i++) {
                if (vendorMrpState[i].id == code) {
                    index = i
                }
            }
            if (index < vendorMrpState.length - 1 || index == 0) {
                document.getElementById(vendorMrpState[index + 1].id) != null ? document.getElementById(vendorMrpState[index + 1].id).focus() : null

                this.setState({
                    focusedLi: vendorMrpState[index + 1].id
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < vendorMrpState.length; i++) {
                if (vendorMrpState[i].id == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(vendorMrpState[index - 1].id) != null ? document.getElementById(vendorMrpState[index - 1].id).focus() : null

                this.setState({
                    focusedLi: vendorMrpState[index - 1].id
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.onMrpCloseModal(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.vendorMrpState.length!=0?
                document.getElementById(this.state.vendorMrpState[0].id).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.vendorMrpState.length!=0){
                document.getElementById(this.state.vendorMrpState[0].id).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.onMrpCloseModal(e)
        }


    }

    render() {
 if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        const {
            rsp, articleName, mrp, mrpEnd, mrpStart, searchMrp, sectionSelection
        } = this.state;
        return (

           this.props.isModalShow ?  <div className={this.props.mrpModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.mrpModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.mrpModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.mrpModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT MRP</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select Mrp from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">
                                        {this.state.save ? <div className="col-md-7 col-sm-7 pad-0 mrpLi vendorMrpList">
                                            <li>
                                                <input type="text" className="" onChange={e => this.handleChange(e)} id="rsp" name="rsp" value={rsp} placeholder="Rsp" /> </li>
                                            <li> <input type="text" className="" onChange={e => this.handleChange(e)} id="articleName" name="articleName" value={articleName} placeholder="Article Code" /> </li>
                                            <li>  <input type="text" className="" onChange={e => this.handleChange(e)} id="mrp" value={mrp} name="mrp" placeholder="Mrp" /> </li>
                                            <li> <input type="text" className="" onChange={e => this.handleChange(e)} id="mrpStart" name="mrpStart" value={mrpStart} placeholder="Mrp End" /> </li>
                                            <li>  <input type="text" className="" onChange={e => this.handleChange(e)} id="mrpEnd" value={mrpEnd} name="mrpEnd" placeholder="Mrp Start" /> </li>

                                        </div> : null}
                                        {this.state.addNew ? <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>

                                                    {/* <select id="mrpBasedOn" name="sectionSelection" value={sectionSelection} onChange={e => this.handleChange(e)}>
                                    <option value="">Choose</option>
                                        <option value="articleName">Article Code</option>
                                        <option value="mrp">MRP</option>
                                        <option value="rsp">RSP</option>
                                    </select> */}
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBymrp" name="searchBymrp" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}


                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={searchMrp} id="searchMrp" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div> : null}
                                        {this.state.addNew ? <li className="float_right" >

                                            <label>
                                                <button type="button" className={this.state.searchMrp == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li> : null}


                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover vendorMrpMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>RSP</th>
                                                    <th>MRP</th>
                                                    <th>Article Name</th>
                                                    <th>MRP Start</th>
                                                    <th>MRP End</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.vendorMrpState == undefined || this.state.vendorMrpState == null || this.state.vendorMrpState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.vendorMrpState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.id}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.id} checked={this.state.rows[this.props.idx].iCode == `${data.id}`} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.rsp}</td>
                                                        <td>{data.mrp}</td>

                                                        <td>{data.articleName}</td>
                                                        <td>{data.mrpStart}</td>
                                                        <td>{data.mrpEnd}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" data-dismiss="modal" onClick={(e) => this.onMrpCloseModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>:
             <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.vendorMrpState != undefined || this.state.vendorMrpState.length != 0 ?
                            this.state.vendorMrpState.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.id}`)} id={data.id} className={this.state.rows[this.props.idx].iCode == `${data.id}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.id)}>
                                    <span className="vendor-details">
                                           <span className="vd-name">{data.rsp}</span>
                                        <span className="vd-loc">{data.mrp}</span>
                                        <span className="vd-num"> {data.articleName}</span>
                                        <span className="vd-code">{data.mrpStart}</span>
                                        <span className="vd-code">{data.mrpEnd}</span>
                                        
                                      
                                    </span>
                                </li>)) : <li><span>No Data Found</span></li>}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.onMrpCloseModal(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>

        );
    }
}

export default PiMrpModal;
