import React from 'react';
import _ from 'lodash';

class MapColumn extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            fixedFieldNames: [],
            uploadedHeaders: [],
            mappedHeaders: {},
            data: [],
            dataToDisplay:[],
            displayCounter: false,
            displayCounter2: false,
            selectMapping: false
        }
    }

    componentDidMount(){
        this.props.excelHeadersRequest()
        console.log(this.props.excelData[0])
        this.setState({
            uploadedHeaders: this.props.excelData[0]
        })
        if(Object.keys(this.props.mappedHeaders).length != 0){
            var data = []
            var tempOptions = _.cloneDeep(this.props.excelData)
            Object.keys(this.props.mappedHeaders).forEach((data2, key2) => {
                this.props.fixedFieldNames.forEach((data1, key1) => {
                    if(data1 == data2) {
                        let obj =  {
                            id: data1,
                            dropDownData: tempOptions[0],
                            selectedValue:  this.props.mappedHeaders[data2] != "" ?  this.props.mappedHeaders[data2] : "Blank",
                            mandate: this.props.mandateHeader.indexOf(data1) > -1 ? true : false
                        }
                        data.push(obj)
                    }
                })
            })
            this.setState({
                displayCounter2: true,
                mappedHeaders: _.cloneDeep(this.props.mappedHeaders),
                dataToDisplay: data
            })
        }
    }

    makeData = (datas) => {
        console.log(this.props.keyNames)
        if(this.state.displayCounter == false && this.state.displayCounter2 == false){
            var data = [];
            var temp 
            var tempOptions = _.cloneDeep(this.props.excelData)
            temp = _.cloneDeep(tempOptions[0])
            // datas.forEach((data1, key1) => {
            //     if(tempOptions[0].indexOf(data1) > -1){
            //         const index = temp.indexOf(data1);
            //         if (index > -1) {
            //             temp.splice(index, 1);
            //         }
            //     } 
            // })

            datas.forEach((data1, key1) => {
                let temp3

                tempOptions[0].map(data11 => {
                    if(data11.toLowerCase() === data1.toLowerCase()){
                        console.log(data11.toLowerCase(), data1.toLowerCase())
                        temp3 = data1
                    }
                })
                let obj =  {
                    id: data1,
                    dropDownData: temp,
                    selectedValue:  temp3 != undefined  ? temp3 : 'Blank',
                    mandate: this.state.mandateHeader.indexOf(data1) > -1 ? true : false
                }
                data.push(obj)
            })

            

            this.setState({
                displayCounter: true,
                dataToDisplay: data
            }, () => {
                this.mapping()
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.excelUpload)
        if (nextProps.excelUpload.excelHeaders.isSuccess) {
            if (nextProps.excelUpload.excelHeaders.data.resource != null) {
                this.setState({
                    excelHeaderData: nextProps.excelUpload.excelHeaders.data.resource.excelHeader,
                    fixedFieldNames: Object.values(nextProps.excelUpload.excelHeaders.data.resource.excelHeader),
                    mandateHeader: Object.values(nextProps.excelUpload.excelHeaders.data.resource.mandateHeader)
                }, () => {
                    this.makeData(Object.values(nextProps.excelUpload.excelHeaders.data.resource.excelHeader))
                })
            }
        }
        this.props.excelHeadersClear()
    }

    // handleSelectedValue(e, data) {
    //     let mappingObj = _.cloneDeep(this.state.mappedHeaders);

    //     console.log(e.target.value, data)
    //     mappingObj[data] =  e.target.value;

    //     if(this.props.stepCounter == 3 && this.props.isMapColumn){
    //         this.props.finalMappedHeaders(mappingObj)
    //     }
    //     this.setState({
    //         mappedHeaders: mappingObj
    //     })
    //     console.log(mappingObj)
    // }

    handleSelectedValue1 = (data, val) => {
        var temp = val.target.value
        let clonedState = _.cloneDeep(this.state.dataToDisplay)
        clonedState.forEach((obj)=>{
            if(obj.id === data.id){
                obj.selectedValue = temp;
            }
        })
        
        this.setState({
            dataToDisplay: clonedState
        })

        this.mapping(data, temp, clonedState)
    }

    mapping = (data, val, clonedState) => {
        let clonedState1, mappingObj;
        if(clonedState == undefined) {
           clonedState1 = _.cloneDeep(this.state.dataToDisplay)
           mappingObj = _.cloneDeep(this.state.mappedHeaders);
           clonedState1.forEach((obj) => {
               if (obj.selectedValue != "Blank") {
                   mappingObj[obj.id] = obj.selectedValue
               }
           })
        }
        else{
           clonedState1 = _.cloneDeep(clonedState)

           mappingObj = _.cloneDeep(this.state.mappedHeaders);
           clonedState1.forEach((obj) => {
               if (obj.id === data.id) {
                   mappingObj[obj.id] = val;
               }
           })
        }
        if (this.props.stepCounter == 3 && this.props.isMapColumn) {
            this.props.finalMappedHeaders(mappingObj, this.state.displayCounter)
        }
        this.setState({
            mappedHeaders: mappingObj
        })
        console.log(mappingObj)
    }

    selectMapping = (data, val) => {
        var temp = val.target.value
        let clonedState = _.cloneDeep(this.state.dataToDisplay)
        clonedState.forEach((obj)=>{
            if(obj.id === data.id){
                obj.selectedValue = temp;
            }
        })
        
        this.setState({
            dataToDisplay: clonedState,
            selectMapping: true

        })

        this.mapping(data, temp, clonedState)
    }
   
    render(){
        console.log(this.state.dataToDisplay, this.state.mappedHeaders)
        return(
            <div className="col-lg-7 pad-0 m-top-10">
            <div className="excel-table-header">
                <div className="eth-manage">
                    <table className="table">
                        <thead>
                            <tr>
                                <th><label>Column Name</label></th>
                                <th><label>Mapping With Excel Headers</label></th>
                            </tr>
                        </thead>
                        <tbody>
                                {
                                    this.state.dataToDisplay.length != 0 ? this.state.dataToDisplay.map((obj) => (
                                        <tr>
                                            <td><label>{obj.id} {obj.mandate ? <span className="mandatory">*</span> : null}</label></td>
                                            <td>
                                                {console.log(obj.dropDownData.filter(word => word.toLowerCase() === obj.selectedValue.toLowerCase()))}
                                                <select value={(obj.dropDownData.filter(word => word.toLowerCase() === obj.selectedValue.toLowerCase())).length != 0 ? obj.dropDownData.filter(word => word.toLowerCase() === obj.selectedValue.toLowerCase())[0] : obj.selectedValue} onChange = {(e) => this.handleSelectedValue1(obj, e)}>
                                                    <option>Blank</option>
                                                    {obj.dropDownData.length != 0 ? obj.dropDownData.map((data2, key2) => (
                                                        <option key = {key2} value = {data2}>{data2}</option>
                                                    )) : null}
                                                </select>

                                                {/* <div className="inputTextKeyFucMain">
                                                    <input type="text" className="onFocus pnl-purchase-input" value={obj.selectedValue} onKeyDown={this.selectMapping}  />
                                                    <span class="modal-search-btn">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" class="cls-1"></path>
                                                        </svg>
                                                    </span>
                                                    {this.state.selectMapping && <div className="dropdown-menu-city1">
                                                        <ul className="dropdown-menu-city-item">
                                                            {obj.dropDownData.length != 0 ? obj.dropDownData.map((data2, key2) => (
                                                            <li key = {key2} value = {data2}>
                                                                <span className="vendor-details">
                                                                    <span className="vd-name div-col-1">{data2}</span>
                                                                </span>
                                                            </li>
                                                            )) : null}
                                                        </ul>
                                                        <div className="gen-dropdown-pagination">
                                                            <div class="page-next-prew-btn">
                                                                <button class="pnpb-prev" type="button" id="prev">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"></path>
                                                                    </svg>
                                                                </button>
                                                                <button class="pnpb-no" type="button">1/1</button>
                                                                <button class="pnpb-next" type="button">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"></path>
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>}
                                                </div> */}
                                            </td>
                                        </tr>
                                    )): null
                                }
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
        )        
    }
}
export default MapColumn;



