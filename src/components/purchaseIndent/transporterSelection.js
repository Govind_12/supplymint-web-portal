import React from "react";

class TransporterSelection extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            transporterState: [],
            selectedId: "",
            checked: false,
            search: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            transporterCode: "",
            transporterName: "",
            sectionSelection: "",
            toastLoader: false,
            toastMsg: "",
            selectedData: this.props.transporter,
            selectedTransporterCode: "",
            selectedTransporterName: "",
            searchBy: "startWith",
            focusedLi: "",
        }

    }
    componentDidMount() {
        if (this.props.isModalShow) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    componentWillMount() {
        this.setState({
            search:this.props.isModalShow?"":this.props.transporterSearch,
                                        type: this.props.isModalShow || this.props.transporterSearch == "" ? 1 : 3

        })

        if (this.props.purchaseIndent.getTransporter.isSuccess) {
            if (this.props.purchaseIndent.getTransporter.data.resource != null) {
                this.setState({
                    transporterState: this.props.purchaseIndent.getTransporter.data.resource,
                    prev: this.props.purchaseIndent.getTransporter.data.prePage,
                    current: this.props.purchaseIndent.getTransporter.data.currPage,
                    next: this.props.purchaseIndent.getTransporter.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.getTransporter.data.maxPage,
                    selectedData: this.props.transporter
                })
            } else {

                this.setState({
                    transporterState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
        }

    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.getTransporter.isSuccess) {
            if (nextProps.purchaseIndent.getTransporter.data.resource != null) {

                this.setState({
                    transporterState: nextProps.purchaseIndent.getTransporter.data.resource,
                    prev: nextProps.purchaseIndent.getTransporter.data.prePage,
                    current: nextProps.purchaseIndent.getTransporter.data.currPage,
                    next: nextProps.purchaseIndent.getTransporter.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getTransporter.data.maxPage,
                    selectedData: nextProps.transporter
                })
            } else {

                this.setState({
                    transporterState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
           
               if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("search").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.getTransporter.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.getTransporter.data.resource[0].transporterName,
                                        search:this.props.isModalShow?"":this.props.transporterSearch,
                            type: this.props.isModalShow || this.props.transporterSearch == "" ? 1 : 3

                        })
                        document.getElementById(nextProps.purchaseIndent.getTransporter.data.resource[0].transporterName) != null ? document.getElementById(nextProps.purchaseIndent.getTransporter.data.resource[0].transporterName).focus() : null
                    }


                
                }
            }

        }

    }



    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.getTransporter.data.prePage,
                current: this.props.purchaseIndent.getTransporter.data.currPage,
                next: this.props.purchaseIndent.getTransporter.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getTransporter.data.maxPage,
            })
            if (this.props.purchaseIndent.getTransporter.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getTransporter.data.currPage - 1,
                    transporterCode: this.state.transporterCode,
                    transporterName: this.state.transporterName,

                    search: this.state.search,
                    city: this.props.isTransporterDependent != false ? this.props.city : "",
                    searchBy: this.state.searchBy
                }
                this.props.getTransporterRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.getTransporter.data.prePage,
                current: this.props.purchaseIndent.getTransporter.data.currPage,
                next: this.props.purchaseIndent.getTransporter.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getTransporter.data.maxPage,
            })
            if (this.props.purchaseIndent.getTransporter.data.currPage != this.props.purchaseIndent.getTransporter.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getTransporter.data.currPage + 1,
                    transporterCode: this.state.transporterCode,
                    transporterName: this.state.transporterName,

                    search: this.state.search,
                    city: this.props.isTransporterDependent != false ? this.props.city : "",
                    searchBy: this.state.searchBy
                }
                this.props.getTransporterRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.getTransporter.data.prePage,
                current: this.props.purchaseIndent.getTransporter.data.currPage,
                next: this.props.purchaseIndent.getTransporter.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getTransporter.data.maxPage,
            })
            if (this.props.purchaseIndent.getTransporter.data.currPage <= this.props.purchaseIndent.getTransporter.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    transporterCode: this.state.transporterCode,
                    transporterName: this.state.transporterName,

                    search: this.state.search,
                    city: this.props.isTransporterDependent != false ? this.props.city : "",
                    searchBy: this.state.searchBy
                }
                this.props.getTransporterRequest(data)
            }

        }
    }

    selectedData(e) {

        for (let i = 0; i < this.state.transporterState.length; i++) {
            if (this.state.transporterState[i].transporterName == e) {
                this.setState({
                    selectedData: this.state.transporterState[i].transporterName,
                    selectedTransporterCode: this.state.transporterState[i].transporterCode,
                    selectedTransporterName: this.state.transporterState[i].transporterName
                }, () => {
                    if (!this.props.isModalShow) {
                        this.ondone()
                    }
                })
            }

        }

    }

    ondone() {
        var sCode = {};

        if (this.props.isModalShow?this.props.transporter != this.state.selectedData:this.props.transporter != this.state.selectedData || this.props.transporterSearch == this.state.search ||this.props.transporterSearch != this.state.search) {
            sCode = {
                transporterCode: this.state.selectedTransporterCode,
                transporterName: this.state.selectedTransporterName,
            }

            let t = this
            setTimeout(function () {
                t.props.updateTransporterState(sCode);
            }, 100)
        }

        if (this.state.selectedData != "") {

            this.setState(
                {
                    search: "",
                })


            this.closeTransporter()
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }

        document.onkeydown = function (t) {

            if (t.which == 9) {
                return true;
            }
        }
    }

    closeTransporter(e) {
        this.props.onCloseTransporter(e)
        const t = this
        t.setState({
            search: "",
            sectionSelection: "",
            transporterState: [],
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
        })

    }

    onClear(e) {
        if (this.state.type == 2 || this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                transporterName: "",
                transporterCode: "",
                search: "",
                city: this.props.isTransporterDependent != false ? this.props.city : "",
                searchBy: this.state.searchBy

            }
            this.props.getTransporterRequest(data)
        }
        this.setState({
            search: "",
            sectionSelection: "",
            type: "",
            no: 1
        })
        document.getElementById("search").focus()
    }



    onSearch(e) {

        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,

                })
                let data = {
                    type: 3,
                    no: 1,
                    transporterCode: this.state.transporterCode,
                    transporterName: this.state.transporterName,

                    search: this.state.search,
                    city: this.props.isTransporterDependent != false ? this.props.city : "",
                    searchBy: this.state.searchBy
                }
                this.props.getTransporterRequest(data)

            }
            else {
                this.setState({
                    type: 2,

                })
                let data = {
                    type: 2,
                    no: 1,
                    transporterCode: this.state.transporterCode,
                    transporterName: this.state.transporterName,
                    search: "",
                    city: this.props.city,
                    searchBy: this.state.searchBy
                }
                this.props.getTransporterRequest(data)
            }
        }
        document.getElementById("search").focus()

    }

    handleChange(e) {
        if (e.target.id == "search") {

            this.setState(
                {
                    search: e.target.value
                },

            );

            //     if (document.getElementById("sectionBasedOn").value == "transporterCode") {
            //         this.setState(
            //             {
            //                 transporterCode: e.target.value,
            //                 transporterName: "",

            //             },

            //         );
            //     }
            //     else if (document.getElementById("sectionBasedOn").value == "transporterName") {
            //         this.setState(
            //             {
            //                 transporterCode: "",
            //                 transporterName: e.target.value,

            //             },

            //         );
            //     }
            //     else if (document.getElementById("sectionBasedOn").value == "") {
            //         this.setState(
            //             {
            //                 transporterCode: "",

            //                 transporterName: "",
            //                 search: e.target.value
            //             },

            //         );

            //     }
            // } else if (e.target.id == "sectionBasedOn") {
            //     this.setState(
            //         {
            //             search: "",
            //             sectionSelection: e.target.value
            //         },
            //     );


        } else if (e.target.id == "searchBytransporter") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        transporterCode: this.state.transporterCode,
                        transporterName: this.state.transporterName,

                        search: this.state.search,
                        city: this.props.isTransporterDependent != false ? this.props.city : "",
                        searchBy: this.state.searchBy
                    }
                    this.props.getTransporterRequest(data)
                }
            })
        }

    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }

    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == 1 || this.state.type == "")) {
                if (this.state.transporterState.length != 0) {
                    this.setState({
                        // selectedId: this.state.transporterState[0].transporterName,
                        selectedData: this.state.transporterState[0].transporterName
                    })
                    this.selectedData(this.state.transporterState[0].transporterName)
                    let da = this.state.transporterState[0].transporterName
                    window.setTimeout(() => {
                        document.getElementById("search").blur()

                        document.getElementById(da).focus()
                    }, 0)
                } else {
                    window.setTimeout(() => {
                        document.getElementById("search").blur()

                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }

            if (e.target.value != "") {
                window.setTimeout(() => {
                    document.getElementById("search").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if(e.key === "ArrowDown"){
                 if (this.state.transporterState.length != 0) {
                    this.setState({
                        // selectedId: this.state.transporterState[0].transporterName,
                        selectedData: this.state.transporterState[0].transporterName
                    })
                    this.selectedData(this.state.transporterState[0].transporterName)
                    let da = this.state.transporterState[0].transporterName
                    window.setTimeout(() => {
                        document.getElementById("search").blur()

                        document.getElementById(da).focus()
                    }, 0)
                } else {
                    window.setTimeout(() => {
                        document.getElementById("search").blur()

                        document.getElementById("closeButton").focus()
                    }, 0)
                }
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            window.setTimeout(() => {
                document.getElementById(this.state.transporterState[0].transporterName).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if(e.key ==="Enter"){
            this.ondone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.ondone();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.closeTransporter();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("closeButton").blur()
                document.getElementById("search").focus()
            }, 0)
        }
    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onClear();
        }
        if (e.key == "Tab") {
            if (this.state.transporterState.length != 0) {
                this.setState({
                    // selectedId: this.state.transporterState[0].transporterName,
                    selectedData: this.state.transporterState[0].transporterName
                })
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById(this.state.transporterState[0].transporterName).focus()
                }, 0)
            } else {
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }

selectLi(e, code) {
        let transporterState = this.state.transporterState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < transporterState.length; i++) {
                if (transporterState[i].transporterName == code) {
                    index = i
                }
            }
            if (index < transporterState.length - 1 || index == 0) {
                document.getElementById(transporterState[index + 1].transporterName) != null ? document.getElementById(transporterState[index + 1].transporterName).focus() : null

                this.setState({
                    focusedLi: transporterState[index + 1].transporterName
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < transporterState.length; i++) {
                if (transporterState[i].transporterName == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(transporterState[index - 1].transporterName) != null ? document.getElementById(transporterState[index - 1].transporterName).focus() : null

                this.setState({
                    focusedLi: transporterState[index - 1].transporterName
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.closeTransporter(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.transporterState.length!=0?
                document.getElementById(this.state.transporterState[0].transporterName).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.transporterState.length!=0){
                document.getElementById(this.state.transporterState[0].transporterName).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.closeTransporter(e)
        }


    }
  

    render() {
            !this.props.isModalShow ? $('.poSearchModal').removeClass('hideSmoothly')
            : null
        if (this.state.focusedLi != ""&& this.props.transporterSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

        const { sectionSelection, search } = this.state;
        return (

          this.props.isModalShow ?  <div className={this.props.transporterAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.transporterAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.transporterAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.transporterAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT TRANSPORTER</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only single entry from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10">
                                        <div className="col-md-9 col-sm-9 pad-0 chooseDataModal" >
                                            <li>

                                                {/* <select id="sectionBasedOn" name="sectionSelection" value={sectionSelection} onChange={(e) => this.handleChange(e)}>
                                                    <option value="">Choose</option>
                                                    <option value="transporterCode">Transporter Code</option>
                                                    <option value="transporterName">Transporter Name</option>

                                                </select> */}
                                                {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBytransporter" name="searchBytransporter" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                
                                                    <option value="contains">Contains</option>
                                                    <option value="startWith"> Start with</option>
                                                    <option value="endWith">End with</option>

                                                </select> : null}
                                                <input type="search" autoCorrect="off" autoComplete="off" id="search" ref={this.textInput} onKeyDown={this._handleKeyDown} value={search} onKeyPress={this._handleKeyPress} onChange={(e) => this.handleChange(e)} className="search-box" placeholder="Type to search" />
                                                <label className="m-lft-15">
                                                    <button className="findButton" type="button" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                            <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                        </svg>
                                                    </button>
                                                </label>
                                            </li>
                                        </div>
                                        <li className="float_right">

                                            <label>
                                                <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onClear(e)}>CLEAR</button>
                                            </label>
                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Transporter Code</th>
                                                    <th>Transporter Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.transporterState == undefined ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.transporterState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.transporterState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.transporterName}`)}>
                                                        <td>
                                                            <label className="select_modalRadio">
                                                                <input id={data.transporterName} type="radio" name="transporter" checked={this.state.selectedData == `${data.transporterName}`} onKeyDown={(e) => this.focusDone(e)}  readOnly/>
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.transporterCode}</td>
                                                        <td className="pad-lft-8">{data.transporterName}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeTransporter(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}



                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> :


                <div className="dropdown-menu-city1 dropdown-menu-vendor" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.transporterState == undefined || this.state.transporterState.length == 0 ?
                        <li><span>No Data Found</span></li>:
                            this.state.transporterState.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.transporterName}`)} id={data.transporterName} className={this.state.selectedData == `${data.transporterName}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.transporterName)}>
                                    <span className="vendor-details">
                                        <span className="vd-name div-col-1">{data.transporterName}</span>
                                    
                                    </span>
                                </li>)) }

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                            <button className="btn-close" type="button" onClick={(e) => this.closeTransporter(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>


        );
    }
}

export default TransporterSelection;