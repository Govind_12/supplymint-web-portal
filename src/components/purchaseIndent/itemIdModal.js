import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";
class ItemIdModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef()
        this.state = {
            isCross: false,
            value: [],
            colorState: [],
            loader: false,
            success: false,
            alert: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            search: "",
            toastMsg: "",
            toastLoader: false,
            save: true,
            selectedData: true
        }
    }

    componentDidMount() {
        console.log(this.props.poRows)
        let poRows = [...this.props.poRows];
        let mainIndex = poRows.findIndex((obj => obj.gridOneId == this.props.selectedRowId));
        let payload = {
            type: 1,
            no: 1,
            search: "",
            articleCode: "",
            supplierCode: this.props.slCode,
            siteCode: this.props.siteCode,
            searchBy: "",
        }
        this.props.getPoItemCodeRequest(payload)
    }
    onClear() {
        this.setState({
            code: "",
            catOne: ""
        })
    }
    onsearchClear() {

        if (this.state.type == 3) {
            let payload = {
                type: "",
                no: 1,
                search: "",
                articleCode: "",
                supplierCode: this.props.slCode,
                siteCode: this.props.siteCode,
                searchBy: "",
            }
            this.props.getPoItemCodeRequest(payload)
        }
        this.setState(
            {
                isCross: false,
                search: "",
                type: "",
                no: 1
            })
        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null
        this.textInput.current.focus();

    }


    componentWillMount() {

        this.setState({
            colorRow: this.props.colorRow,
            colorCode: this.props.colorCode,
            // search:this.props.isModalShow ? "" : this.props.colorSearch,
            // type: this.props.isModalShow || this.props.colorSearch == "" ? 1 : 3
            search: "",
            type: 1
        })

        if (this.props.purchaseIndent.color.isSuccess) {
            if (this.props.purchaseIndent.color.data.resource != null) {
                this.setState({
                    colorState: this.props.purchaseIndent.color.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? this.props.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: this.props.purchaseIndent.color.data.prePage,
                    current: this.props.purchaseIndent.color.data.currPage,
                    next: this.props.purchaseIndent.color.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.color.data.maxPage
                })
            }
            else {
                this.setState({
                    colorState: this.props.purchaseIndent.color.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? this.props.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })
            }
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.getPoItemCode.isSuccess) {
            if (nextProps.purchaseIndent.getPoItemCode.data.resource != null) {
                this.setState({
                    colorState: nextProps.purchaseIndent.getPoItemCode.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? nextProps.purchaseIndent.color.data.resource == null ? JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color")) : nextProps.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: nextProps.purchaseIndent.getPoItemCode.data.prePage,
                    current: nextProps.purchaseIndent.getPoItemCode.data.currPage,
                    next: nextProps.purchaseIndent.getPoItemCode.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getPoItemCode.data.maxPage,
                })
            }
        }

    }

    selectall(e) {

        // if(!e.key=="ArrowUp" || !e.key=="ArrowDown" || !e.key == "ArrowRight" || !e.key =="ArrowLeft"){
        var c = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
        let values = this.state.value
        let colorList = this.state.colorList

        if (c == null) {


            document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = true : null

        } else {
            for (var i = 0; i < c.length; i++) {
                if (!values.includes(c[i].cname)) {
                    values.push(c[i].cname)
                    let payload = {
                        code: c[i].code,
                        cname: c[i].cname,
                        id: this.props.colorRow
                    }
                    colorList.push(payload)
                }
            }
            this.setState({
                value: values,
                colorList: colorList

            })

            if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
                const s = this
                setTimeout(function () {
                    s.setState({
                        coData: c
                    })
                }, 10)
            } else {

                const s = this
                setTimeout(function () {
                    s.setState({
                        colorState: c
                    })
                }, 10)

            }
            var session = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
            if (session != null) {

                sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(session))
                this.setState({
                    sessionState: session
                })
            }
        }

        document.getElementById('selectAll') != null ? document.getElementById('selectAll').focus() : null
        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = true : null

        // }
    }
    deselectall(e) {

        // if(!e.key=="ArrowUp" || !e.key=="ArrowDown" || !e.key == "ArrowRight" || !e.key =="ArrowLeft"){
        var c = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
        var value = this.state.value
        let colorList = this.state.colorList

        if (c == null) {

            document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

        } else {

            // for (let j = 0; j < c.length; j++) {
            //     for (var i = 0; i < value.length; i++) {
            //         if (c[j].cname == value[i] || c[j].cname != value[i])
            //             value.splice(i)
            //     }

            // }

            // for (let j = 0; j < c.length; j++) {
            //     for (var i = 0; i < colorList.length; i++) {
            //         if (c[j].cname == colorList[i].cname || c[j].cname != colorList[i].cname)
            //             colorList.splice(i)
            //     }
            // }
            this.setState({
                value: [],
                colorList: [],
            }, () => {
                let data = {
                    value: this.state.value,
                    colorList: this.state.colorList
                }

                this.props.deselectallProp(data)
            })

            if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
                const s = this
                setTimeout(function () {
                    s.setState({
                        coData: c
                    })
                }, 10)
            } else {
                const s = this
                setTimeout(function () {
                    s.setState({
                        colorState: c
                    })
                }, 10)

            }

            var session = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
            if (session != null) {


                sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(session))
                this.setState({
                    sessionState: session
                })
            }

        }


        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').focus() : null

        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = true : null

        // }

    }


    code() {

        if (
            this.state.code == "" || this.state.code.trim() == "") {
            this.setState({
                codeerr: true
            });
        } else {
            this.setState({
                codeerr: false
            });
        }

    }
    catSix() {

        if (
            this.state.catSix == "" || this.state.catSix.trim() == "") {
            this.setState({
                catSixerr: true
            });
        } else {
            this.setState({
                catSixerr: false
            });
        }

    }


    handleChange(e) {
         if (e.target.id == "itemIDSearch") {

            this.setState(
                {
                    search: e.target.value
                }

            );
            if (e.target.value != '') {
                this.setState({
                    isCross: true,
                })
            }
            else {
                this.setState({
                    isCross: false,
                })
            }

        } 
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            if (e.target.value != "") {
                this.searchValue();
            }
        }
    }

    onSave(e) {

        this.code();
        this.catSix();

        e.preventDefault();

        const t = this;
        setTimeout(function () {


            const { code, codeerr, catSix, catSixerr } = t.state;

            if (!codeerr && !catSixerr) {

                let saveData = {
                    isExist: "COLOR",
                    hl4Code: t.props.colorCode,
                    code: code,
                    categoryName: catSix,

                    hl1Name: "",
                    hl2Name: "",

                    hl3Name: ""
                }
                t.props.piAddNewRequest(saveData);

            }
        }, 10)

    }
    onFinalSave() {
        let idd = 0
        const t = this;
        var colorArray = []
        let r = JSON.parse(sessionStorage.getItem(t.props.colorCode + '-' + "color"))
        var colorCat = []
        if (r != null) {
            for (var i = 0; i < r.length; i++) {
                colorCat.push(r[i].cname.replace(/\s+/g, ""))
            }
        }
        if (colorCat.includes(t.state.catSix.replace(/\s+/g, ""))) {

            t.setState({
                errorMassage: "CAT6 already exist",
                poErrorMsg: true
            })

        }

        else {

            if (sessionStorage.getItem(t.props.colorCode + '-' + "color") != null) {

                colorArray = JSON.parse(sessionStorage.getItem(t.props.colorCode + '-' + "color"))

                let c = {

                    code: t.state.code,

                    cname: t.state.catSix,
                    hl3Name: this.props.hl3Name
                }

                colorArray.push(c)
                sessionStorage.setItem(t.props.colorCode + '-' + "color", JSON.stringify(colorArray));

                t.setState({
                    coData: t.state.colorState == null ? colorArray : t.state.colorState.concat(colorArray),
                    toastMsg: "Cat6 added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)
                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null


            } else {

                let c = {

                    code: t.state.code,

                    cname: t.state.catSix,

                    hl3Name: this.props.hl3Name
                }

                colorArray.push(c)
                sessionStorage.setItem(t.props.colorCode + '-' + "color", JSON.stringify(colorArray));

                t.setState({
                    coData: t.state.colorState == null ? colorArray : t.state.colorState.concat(colorArray),
                    toastMsg: "Cat6 added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)
                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

            }
        }
        t.setState(
            {
                addNew: true,
                save: false,

            }
        )
        t.onClear();

    }
    onClear() {
        this.setState({
            code: "",
            catSix: ""
        })

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,

            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getPoItemCode.data.currPage - 1,
                    search: this.state.search,
                    articleCode: "",
                    supplierCode: this.props.slCode,
                    siteCode: this.props.siteCode,
                    searchBy: this.state.searchBy
                }

                this.props.getPoItemCodeRequest(data)

                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

            }
        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,

            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage != this.props.purchaseIndent.getPoItemCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                    search: this.state.search,
                    articleCode: "",
                    supplierCode: this.props.slCode,
                    siteCode: this.props.siteCode,
                    searchBy: this.state.searchBy
                }

                this.props.getPoItemCodeRequest(data)

            }

            document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

            document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

        } else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.getPoItemCode.data.prePage,
                current: this.props.purchaseIndent.getPoItemCode.data.currPage,
                next: this.props.purchaseIndent.getPoItemCode.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getPoItemCode.data.maxPage,

            })
            if (this.props.purchaseIndent.getPoItemCode.data.currPage <= this.props.purchaseIndent.getPoItemCode.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no:  1,
                    search: this.state.search,
                    articleCode: "",
                    supplierCode: this.props.slCode,
                    siteCode: this.props.siteCode,
                    searchBy: this.state.searchBy
                }

                this.props.getPoItemCodeRequest(data)
            }

        }

        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

    }

    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.colorState.length != 0 ?
                            document.getElementById(this.state.colorState[0].cname).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.colorState.length != 0) {
                    document.getElementById(this.state.colorState[0].cname).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.onClose(e)
        }


    }
    searchValue = () => {
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,
                // type=""
            })
            setTimeout(function () {
                this.setState({
                    toastLoader: false
                })
            }, 1000)
            document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
            document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null
        }
        else{
            this.setState({
                type: 3,
                colorState: []
            })
            let payload = {
                type: 3,
                no: 1,
                search: this.state.search,
                articleCode: "",
                supplierCode: this.props.slCode,
                siteCode: this.props.siteCode,
                searchBy: "",
            }
            this.props.getPoItemCodeRequest(payload)
        }
        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

    }


    render() {
        console.log(this.props.poRows);

        if (this.state.focusedLi != "" && this.props.colorSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        const { code, codeerr, catSix, catSixerr, search, selectedData } = this.state;
        return (
            <div className={this.props.itemIdAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.itemIdAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.itemIdAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.itemIdAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12 piClorModal">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list-inline width_100 m-top-15 modelHeader">
                                        <li>
                                            <label className="select_name-content">SELECT ITEM ID</label>
                                            <p className="para-content">You can select multiple site Id from below records</p>
                                        </li>


                                    </ul>

                                    <ul className="list-inline width_100 chooseDataModal">

                                            {/* { <li className="width100">
                                              
                                                <form>
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} value={search} id="itemIDSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                    {this.state.isCross ? <span className="closeSearch" onClick={(e) => this.onsearchClear(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                                                </form>
                                                
                                                <label className="m-lft-15">
                                                    <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.searchValue(e)} onClick={(e) => this.searchValue(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                            <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                        </svg>
                                                    </button>
                                                </label>
                                            </li>
                                            
                                                } */}
                                                <li>
                                                    <div className="gen-new-pi-history">
                                                        <form>
                                                            <input type="search" autoComplete="off" autoCorrect="off" className="search_bar" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} value={search} id="itemIDSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                            <button className="searchWithBar" onKeyDown={(e) => this.searchValue(e)} onClick={(e) => this.searchValue(e)}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                                                    <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                                                </svg>
                                                            </button>
                                                            {this.state.isCross ? <span className="closeSearch" onClick={(e) => this.onsearchClear(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                                                        </form>
                                                    </div>
                                                </li>
                                                
                                        {this.state.addNew ? <li className="float_right">
                                          
                                            <label>
                                                <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li> : null}
                                        {this.props.sizeType == "complex" && <div className="nph-switch-btn">
                                            <label className="tg-switch" >
                                                <input type="checkbox" checked={selectedData} onChange={this.changeSelectedData} />
                                                <span className="tg-slider tg-round"></span>
                                            </label>
                                            {selectedData && <label className="nph-wbtext">Selected data</label>}
                                            {!selectedData && <label className="nph-wbtext"> selected data</label>}
                                        </div>}
                                    </ul>
                                </div>
                                {<div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Item Id</th>
                                                    <th>Article Name</th>
                                                    <th>MRP</th>
                                                    <th>Design</th>
                                                </tr>
                                            </thead>
                                            <tbody className="rowPad">
                                                {this.state.colorState.length ? this.state.colorState.map((data, key) => (
                                                    <tr key={key} className="rowFocus" onClick={(e) => this.props.onCheckbox(`${data.itemId}`, `${data}`)}>

                                                        <td>
                                                            <div className="checkBoxRowClick pi-c3">

                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" checked={this.props.value.includes(`${data.itemId}`)} id={data.itemId} onKeyDown={(e) => this.focusDone(e, `${data.itemId}`)} readOnly />
                                                                    <label className="checkmark1" htmlFor={data.itemId} ></label>
                                                                </label>

                                                            </div>
                                                        </td>
                                                        <td>{data.itemId}</td>
                                                        <td>{data.hl4Name}</td>
                                                        <td>{data.mrp}</td>
                                                        <td>{data.design}</td>
                                                    </tr>
                                                )) : <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr>
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>}
                                <div className="modal-bottom">
                                    <ul className="list-inline m-top-10 modal-select">
                                        <li className="">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onClick={(e) => this.props.ondone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onClick={this.props.closeKeyDown}>Close</button>
                                            </label>
                                        </li>
                                        {this.props.sizeType == "complex" && <React.Fragment> <li className="selectAllFocus">
                                            <label className="select_modalRadio">SELECT ALL
                                                    <input type="radio" name="colorRadio" id="selectAll" onKeyDown={(e) => this.selectallKeyDown(e)} onClick={(e) => this.selectall(e)} />
                                                <span className="checkradio-select select_all"></span>
                                            </label>
                                        </li>
                                            <li className="selectAllFocus">
                                                <label className="select_modalRadio">DESELECT ALL
                                                    <input type="radio" name="colorRadio" id="deselectAll" onKeyDown={(e) => this.deselectallKeyDown(e)} onClick={(e) => this.deselectall(e)} />
                                                    <span className="checkradio-select deselect_all"></span>
                                                </label>
                                            </li>
                                        </React.Fragment>}

                                    </ul>
                                    <div className="pagerDiv pagerWidth75 m0 modalPagination sizeModalPagination">
                                        <ul className="list-inline pagination paginationWidth40">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                        </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                        </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                        </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" disabled>
                                                        Prev
                        </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                        </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                        </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                        </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>

        );
    }
}

export default ItemIdModal;