import React from "react";

import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";

class ItemDetailsModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {

            addNew: true,
            save: false,
            itemData: [],
            sessionData: [],
            search: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            toastLoader: false,
            toastMsg: "",
            errorMassage: "",
            poErrorMsg: false,
            itemDetailValue: this.props.itemDetailValue,
            itemDetailCode: this.props.itemDetailCode,
            searchBy: "startWith",
            focusedLi: "",
            modalDisplay: false,

        }
    }
    componentDidMount() {
        if (this.props.isModalShow || this.props.isModalShow == undefined) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
        document.addEventListener('mousedown', this.handleClickOutsideCat)
    }

    componentWillUnmount(){
        document.removeEventListener('mousedown', this.handleClickOutsideCat)
    }

    handleClickOutsideCat = (e) => {
        if (this.textInput && !this.textInput.current.contains(e.target) && e.target.id != this.props.focusId) {
            this.props.closeItemModal();
        }
    }

    code() {

        if (
            this.state.code == "") {
            this.setState({
                codeerr: true
            });
        } else {
            this.setState({
                codeerr: false
            });
        }

    }


    cname() {

        if (
            this.state.cname == "") {
            this.setState({
                cnameerr: true
            });
        } else {
            this.setState({
                cnameerr: false
            });
        }

    }
    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }



    handleChange(e) {
        if (e.target.id == "code") {
            if (e.target.validity.valid) {


                this.setState(
                    {
                        code: e.target.value
                    },
                    () => {
                        this.code();
                    }

                );
            }
        } else if (e.target.id == "cname") {
            this.setState(
                {
                    cname: e.target.value
                },
                () => {
                    this.cname();
                }

            );
        } else if (e.target.id == "itemSearch") {
            this.setState(
                {
                    search: e.target.value,
                    searchData: []
                });

        } else if (e.target.id == "searchByitemD") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        hl3Code: this.props.hl3Code,
                        hl3Name: this.props.department,

                        itemType: this.props.itemType,
                        search: this.state.search,
                        searchBy: this.state.searchBy
                    }
                    this.props.getItemDetailsValueRequest(data)
                }
            })
        }

    }
    onsearchClear() {
        this.setState(
            {
                search: "",
                type: "",
                no: 1
            })
        if (this.state.type == 3) {
            let data = {
                type: "",
                no: 1,

                hl3Code: this.props.hl3Code,
                hl3Name: this.props.department,

                itemType: this.props.itemType,
                search: "",
                searchBy: this.state.searchBy
            }
            this.props.getItemDetailsValueRequest(data)


        }
        document.getElementById("itemSearch").focus()
    }
    onSearch(e) {


        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,

                hl3Code: this.props.hl3Code,
                hl3Name: this.props.department,

                itemType: this.props.itemType,
                search: this.state.search,
                searchBy: this.state.searchBy
            }
            this.props.getItemDetailsValueRequest(data)

        }
    }


    onAddNew(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: false,
                save: true

            })
        this.onsearchClear(e)
    }



    onSave(e) {
        this.code();
        this.cname();

        e.preventDefault();

        const t = this;
        setTimeout(function (e) {
            const { code, codeerr, cname, cnameerr } = t.state;

            if (!codeerr && !cnameerr) {
                let saveData = {
                    isExist: t.props.itemType,
                    hl3Code: t.props.hl3Code,
                    code: code,
                    categoryName: cname,

                    hl1Name: "",
                    hl2Name: "",

                    hl3Name: ""
                }
                t.props.piAddNewRequest(saveData);
            }
        }, 100)




    }

    onFinalSave() {

        const t = this;
        var sessionArray = []
        var idd = 0
        let r = JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType))
        var itemCatDesc = []
        if (r != null) {
            for (var i = 0; i < r.length; i++) {
                itemCatDesc.push(r[i].cname.replace(/\s+/g, ""))
            }
        }
        if (itemCatDesc.includes(t.state.cname.replace(/\s+/g, ""))) {
            t.setState({
                errorMassage: this.props.itemType + " already exist",
                poErrorMsg: true
            })

        }

        else {
            if (sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) != null) {

                sessionArray = JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType))
                let c = {

                    code: t.state.code,

                    cname: t.state.cname,
                    hl3Name: this.props.hl3Name
                }

                sessionArray.push(c)
                sessionStorage.setItem(this.props.hl3Code + '-' + this.props.itemType, JSON.stringify(sessionArray));

                t.setState({
                    sessionData: t.state.itemData == null ? sessionArray : t.state.itemData.concat(sessionArray),
                    toastMsg: this.props.itemType + "added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)

            } else {
                let c = {
                    id: t.state.itemData == null ? idd++ : t.state.itemData.length + 1,
                    code: t.state.code,

                    cname: t.state.cname,
                    hl3Name: this.props.hl3Name

                }
                //(c)

                sessionArray.push(c)
                sessionStorage.setItem(this.props.hl3Code + '-' + this.props.itemType, JSON.stringify(sessionArray));

                t.setState({
                    sessionData: t.state.itemData == null ? sessionArray : t.state.itemData.concat(sessionArray),

                    toastMsg: this.props.itemType + "added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)

            }
        }

        t.setState(
            {
                addNew: true,
                save: false,

            }
        )
        t.onClear();

    }

    onCancel(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false,
                code: "",
                cname: ""

            }
        )
    }
    onClear() {
        this.setState({
            code: "",
            cname: ""
        })
    }

    onSelectedData(id) {

        let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData

        for (var i = 0; i < pdata.length; i++) {
            if (pdata[i].cname == id) {

                this.setState({
                    itemDetailValue: pdata[i].cname,
                    itemDetailCode: pdata[i].code
                }, () => {
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
                })

            }

        }
        if (sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) != null) {
            const s = this
            setTimeout(function () {
                s.setState({
                    sessionData: pdata
                })
            }, 10)
        }
        else {
            const s = this
            setTimeout(function () {
                s.setState({
                    itemData: pdata
                })
            }, 10)
        }
    }
    onDone() {
        let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData

        if (this.props.isModalShow ? this.state.itemDetailValue != undefined || this.state.itemDetailValue != "" : this.state.itemDetailValue != "" || this.props.catdescSearch == this.state.search || this.props.catdescSearch != this.state.search) {
            // for (var i = 0; i < pdata.length; i++) {
            // if (pdata[i].cname == this.state.itemDetailValue) {
            let finalData = {
                checkedData: this.state.itemDetailValue,
                checkedId: this.props.itemId,
                type: this.props.itemType,
                checkedCode: this.state.itemDetailCode,
                focusId: this.props.focusId,
            }

            this.props.updateItemDetails(finalData)
            this.onCloseSection();
            // }
            // }
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
        } else {

            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)

        }

        // document.onkeydown = function (t) {
        //     if(t.which == 9){
        //      return true;
        //     }
        // }
    }
    onCloseSection(e) {
        this.props.closeItemModal();

        // this.setState({
        //     search: "",
        //     addNew:true,
        //     save:false,
        //     codeerr:false,
        //     cnameerr:false,
        //     code:"",
        //     cname:"",
        //     type:"",
        //     prev: 0,
        //     current: 0,
        //     next: 0,
        //     maxPage: 0,
        //     itemData:[],
        //     sessionData:[]
        // })
    }


    componentWillMount() {
        setTimeout(() => {
            this.setState({

                itemDetailValue: this.props.itemDetailValue,
                search: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.catdescSearch,
                type: this.props.isModalShow || this.props.isModalShow == undefined || this.props.catdescSearch == "" ? 1 : 3

            })
        }, 10)

        if (this.props.purchaseIndent.getItemDetailsValue.isSuccess) {
            if (this.props.purchaseIndent.getItemDetailsValue.data.resource != null) {
                this.setState({
                    itemData: this.props.purchaseIndent.getItemDetailsValue.data.resource,
                    sessionData: sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) != null ? this.props.itemData.concat(JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType))) : "",

                    prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                    current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                    next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage
                })
            } else {
                this.setState({
                    itemData: this.props.purchaseIndent.getItemDetailsValue.data.resource,
                    sessionData: sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) != null ? this.props.itemData.concat(JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType))) : "",

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })

            }


        }

    }
    componentWillReceiveProps(nextProps) {


        if (nextProps.purchaseIndent.getItemDetailsValue.isSuccess) {
            if (nextProps.purchaseIndent.getItemDetailsValue.data.resource != null) {
                this.setState({
                    itemData: nextProps.purchaseIndent.getItemDetailsValue.data.resource,
                    sessionData: sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) != null ? nextProps.purchaseIndent.getItemDetailsValue.data.resource == null ? JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType)) : nextProps.purchaseIndent.getItemDetailsValue.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType))) : "",

                    prev: nextProps.purchaseIndent.getItemDetailsValue.data.prePage,
                    current: nextProps.purchaseIndent.getItemDetailsValue.data.currPage,
                    next: nextProps.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getItemDetailsValue.data.maxPage

                })
            } else {
                this.setState({

                    itemData: nextProps.purchaseIndent.getItemDetailsValue.data.resource,
                    sessionData: sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) != null ? nextProps.purchaseIndent.getItemDetailsValue.data.resource == null ? JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType)) : nextProps.purchaseIndent.getItemDetailsValue.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType))) : "",

                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })


            }

            if (window.screen.width > 1200) {
                if (this.props.isModalShow || this.props.isModalShow == undefined) {

                    document.getElementById("itemSearch").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.getItemDetailsValue.data.resource != null) {
                        this.setState({
                            focusedLi: "itemUdf" + nextProps.purchaseIndent.getItemDetailsValue.data.resource[0].cname,
                            search: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.catdescSearch,
                            type: this.props.isModalShow || this.props.isModalShow == undefined || this.props.catdescSearch == "" ? 1 : 3


                        })
                        document.getElementById(  "itemUdf" +nextProps.purchaseIndent.getItemDetailsValue.data.resource[0].cname) != null ? document.getElementById( "itemUdf" +nextProps.purchaseIndent.getItemDetailsValue.data.resource[0].cname).focus() : null
                    }


                }
            }
            this.setState({
                modalDisplay: true,

            })
        }
        if (nextProps.purchaseIndent.piAddNew.isSuccess) {
            this.onFinalSave();
            this.props.piAddNewRequest();
        }
    }
    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage,
            })
            if (this.props.purchaseIndent.getItemDetailsValue.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getItemDetailsValue.data.currPage - 1,
                    hl3Code: this.props.hl3Code,
                    hl3Name: this.props.department,

                    itemType: this.props.itemType,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.getItemDetailsValueRequest(data);
            }
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage,
            })
            if (this.props.purchaseIndent.getItemDetailsValue.data.currPage != this.props.purchaseIndent.getItemDetailsValue.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                    hl3Code: this.props.hl3Code,
                    hl3Name: this.props.department,

                    itemType: this.props.itemType,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.getItemDetailsValueRequest(data)
            }
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.getItemDetailsValue.data.prePage,
                current: this.props.purchaseIndent.getItemDetailsValue.data.currPage,
                next: this.props.purchaseIndent.getItemDetailsValue.data.currPage + 1,
                maxPage: this.props.purchaseIndent.getItemDetailsValue.data.maxPage,
            })
            if (this.props.purchaseIndent.getItemDetailsValue.data.currPage <= this.props.purchaseIndent.getItemDetailsValue.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    hl3Name: this.props.department,

                    itemType: this.props.itemType,
                    search: this.state.search,
                    searchBy: this.state.searchBy
                }
                this.props.getItemDetailsValueRequest(data)
            }

        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }
    deselectValue() {

        let finalData = {
            checkedData: "",
            checkedId: this.props.itemId,
            type: this.props.itemType,
            checkedCode: ""
        }
        this.props.updateItemDetailValue()
        this.props.updateItemDetails(finalData)
        window.setTimeout(() => {
            document.getElementById("itemSearch").focus()
        }, 0)
    }

    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.itemDetailValue != "") {

                    window.setTimeout(function () {
                        document.getElementById("itemSearch").blur()
                        document.getElementById("deselectButton").focus()
                    }, 0)
                } else {
                    let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData
                    if (pdata != null) {
                        if (pdata.length != 0) {
                            this.setState({
                                itemDetailValue: pdata[0].cname
                            })
                            this.onSelectedData(pdata[0].cname)
                            let icode = "itemUdf"+pdata[0].cname
                            window.setTimeout(function () {
                                document.getElementById("itemSearch").blur()
                                document.getElementById(icode).focus()
                            }, 0)
                        }
                    } else {
                        window.setTimeout(function () {
                            document.getElementById("itemSearch").blur()
                            document.getElementById("closeButton").focus()
                        }, 0)
                    }
                }
            }
            if (e.target.value != "") {
                window.setTimeout(function () {

                    document.getElementById("itemSearch").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }

        if (e.key == "ArrowDown") {
            let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData
            if (pdata != null) {
                if (pdata.length != 0) {
                    this.setState({
                        itemDetailValue: pdata[0].cname
                    })
                    this.onSelectedData(pdata[0].cname)
                    let icode = "itemUdf"+pdata[0].cname
                    window.setTimeout(function () {
                        document.getElementById("itemSearch").blur()
                        document.getElementById(icode).focus()
                    }, 0)
                }
            } else {
                window.setTimeout(function () {
                    document.getElementById("itemSearch").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }

        }



        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            const t = this
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                t.state.itemDetailValue == "" ? document.getElementById("clearButton").focus()
                    : document.getElementById("deselectButton").focus()
            }, 0)
        }
    }
    focusDone(e) {

        if (e.key === "Tab") {
            let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData
            let icode ="itemUdf"+ pdata[0].cname

            window.setTimeout(() => {

                document.getElementById(icode).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.onDone();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {
            this.onCloseSection();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("itemSearch").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData
            if (pdata != null) {
                if (pdata.length != 0) {
                    this.setState({
                        itemDetailValue: pdata[0].cname
                    })
                    let icode ="itemUdf"+ pdata[0].cname
                    this.onSelectedData(pdata[0].cname)
                    window.setTimeout(function () {

                        document.getElementById("clearButton").blur()
                        document.getElementById(icode).focus()
                    }, 0)
                }
            } else {
                window.setTimeout(function () {

                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }

    onDeselectValue(e) {
        if (e.key === "Enter") {
            this.deselectValue();
        }
        if (e.key === "Tab") {
            if (this.state.search == "" && (this.state.type == "" || this.state.type == 1)) {

                let pdata = sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData : this.state.sessionData
                if (pdata != null) {
                    if (pdata.length != 0) {
                        this.setState({
                            itemDetailValue: pdata[0].cname
                        })
                        this.onSelectedData(pdata[0].cname)
                        let icode ="itemUdf"+ pdata[0].cname
                        window.setTimeout(function () {
                            document.getElementById("deselectButton").blur()
                            document.getElementById(icode).focus()
                        }, 0)
                    }
                } else {

                    window.setTimeout(function () {
                        document.getElementById("deselectButton").blur()
                        document.getElementById("itemSearch").focus()
                    }, 0)
                }

            } else {
                window.setTimeout(function () {
                    document.getElementById("deselectButton").blur()
                    document.getElementById("clearButton").focus()
                }, 0)
            }
        }
    }

    selectLi(e, code) {
        let itemData = this.state.itemData
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < itemData.length; i++) {
                if (itemData[i].cname == code) {
                    index = i
                }
            }
            if (index < itemData.length - 1 || index == 0) {
                document.getElementById("itemUdf"+itemData[index + 1].cname) != null ? document.getElementById("itemUdf"+itemData[index + 1].cname).focus() : null

                this.setState({
                    focusedLi:"itemUdf"+ itemData[index + 1].cname
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < itemData.length; i++) {
                if (itemData[i].cname == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById("itemUdf"+itemData[index - 1].cname) != null ? document.getElementById("itemUdf"+itemData[index - 1].cname).focus() : null

                this.setState({
                    focusedLi: "itemUdf"+itemData[index - 1].cname
                })

            }
        }
        if (e.which === 13) {
            this.onSelectedData(code)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.onCloseSection(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.itemData.length != 0 ?
                            document.getElementById("itemUdf"+this.state.itemData[0].cname).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.itemData.length != 0) {
                    document.getElementById("itemUdf"+this.state.itemData[0].cname).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.onCloseSection(e)
        }


    }

    render() {
        if (this.state.focusedLi != "" && this.props.catdescSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        const hash = window.location.hash;
        if (this.props.catDescId != "" && this.props.catDescId != undefined && !this.props.isModalShow) {
            let modalWidth = 500;
            let modalWindowWidth = document.getElementById('itemSetModal').getBoundingClientRect();
            let position = document.getElementById(this.props.catDescId).getBoundingClientRect();
            let leftPosition = position.left;
            let top = 176;
            let newLeft = 0;
            let diff = modalWindowWidth.width - leftPosition;

            if (diff >= modalWidth) {
                newLeft = leftPosition > 140 ? leftPosition - 140 : leftPosition;
            }
            else {
                let removeWidth = modalWidth - diff;
                newLeft = leftPosition <= 700 ? (leftPosition - removeWidth - 90) : (leftPosition - removeWidth - 5);
            }
            console.log("top:" + top, "leftPosition:" + leftPosition, "newLeft:" + newLeft)

            $('#catDescModalPosition').css({ 'left': newLeft, 'top': top });
            $('.poArticleModalPosition').removeClass('hideSmoothly');

        }
        const {
            code, codeerr, cname, cnameerr, search
        } = this.state;
        return (

            this.props.isModalShow || this.props.isModalShow == undefined ? <div className={this.props.itemDetailsModalAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.itemDetailsModalAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.itemDetailsModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.itemDetailsModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT {this.props.itemDetailName}</label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 chooseDataModal">
                                        {this.state.save ? <div className="col-md-6 col-sm-6 pad-0 mrpLi">
                                            <li>
                                                <input type="text" pattern="[0-9]*" className={codeerr ? "errorBorder " : ""} onChange={e => this.handleChange(e)} id="code" name="code" value={code} placeholder="Enter Code" />

                                                {codeerr ? (
                                                    <span className="error">
                                                        Enter  Code
                          </span>
                                                ) : null}
                                            </li>
                                            <li> <input type="text" className={cnameerr ? "errorBorder" : ""} onChange={e => this.handleChange(e)} id="cname" name="cname" value={cname} placeholder={this.props.itemType} />

                                                {cnameerr ? (
                                                    <span className="error">
                                                        Enter  {this.props.itemType}
                                                    </span>
                                                ) : null}
                                            </li>

                                        </div> : null}
                                        {this.state.addNew ? <div className="col-md-8 col-sm-8 pad-0 modalDropBtn">
                                            <li>
                                                {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByitemD" name="searchByitemD" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                    <option value="contains">Contains</option>
                                                    <option value="startWith"> Start with</option>
                                                    <option value="endWith">End with</option>

                                                </select> : null}

                                                <input type="search" className="search-box " autoComplete="off" autoCorrect="off" ref={this.textInput} onKeyPress={this._handleKeyPress} onKeyDown={this._handleKeyDown} value={search} id="itemSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                <label className="m-lft-15">
                                                    <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                            <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                        </svg>
                                                    </button>
                                                </label>
                                            </li>
                                        </div> : null}
                                        {this.state.addNew ? <li className="float_right">
                                            {/* <label className="m-r-15">
                                            { hash == "#/purchase/purchaseIndent" ? <button type="button" className="findButton" onClick={(e)=>this.onAddNew(e)}>ADD NEW
                                            <span> + </span>
                                                </button>:null}
                                            </label> */}
                                            <label>
                                                <button type="button" className={this.state.itemDetailValue == "" ? "btnDisabled clearbutton widthAuto m-rgt-20" : "clearbutton widthAuto m-rgt-20"} id="deselectButton" onKeyDown={(e) => this.onDeselectValue(e)} onClick={(e) => this.deselectValue(e)}>DESELECT</button>
                                            </label>
                                            <label>
                                                <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li> : null}
                                        {this.state.save ? <li className="float_right">
                                            <label className="m-r-15">
                                                <button type="button" className="findButton" onClick={(e) => this.onSave(e)}>SAVE

                                            </button>
                                            </label>
                                            <label>
                                                <button type="button" className="findButton" onClick={(e) => this.onCancel(e)}>CANCEL</button>
                                            </label>
                                        </li> : null}
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>{this.props.itemDetailName}</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {this.state.type == 3 ? this.state.itemData == undefined || this.state.itemData == "" || this.state.itemData.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.itemData.map((data, key) => (

                                                    <tr key={key} onClick={(e) => this.onSelectedData(`${data.cname}`)}>
                                                        <td>  <label className="select_modalRadio radioModalCheckBox">
                                                            <input type="radio" name="patternCheck" checked={`${data.cname}` == this.state.itemDetailValue} id={"itemUdf"+data.cname} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.cname}</td>
                                                    </tr>
                                                )) : sessionStorage.getItem(this.props.hl3Code + '-' + this.props.itemType) == null ? this.state.itemData == undefined || this.state.itemData == "" || this.state.itemData.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.itemData.map((data, key) => (

                                                    <tr key={key} onClick={(e) => this.onSelectedData(`${data.cname}`)}>
                                                        <td>  <label className="select_modalRadio radioModalCheckBox">
                                                            <input type="radio" name="patternCheck" checked={`${data.cname}` == this.state.itemDetailValue} id={"itemUdf"+data.cname} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.cname}</td>
                                                    </tr>
                                                )) : this.state.sessionData.map((data, key) => (
                                                    <tr key={key} onClick={(e) => this.onSelectedData(`${data.cname}`)}>
                                                        <td>  <label className="select_modalRadio radioModalCheckBox">
                                                            <input type="radio" name="patternCheck" checked={`${data.cname}` == this.state.itemDetailValue} id={"itemUdf"+data.cname} onKeyDown={(e) => this.focusDone(e)} readOnly />
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.cname}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onCloseSection(e)}>Close</button>
                                            </label>
                                        </li>

                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}



                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

            </div> :
                this.state.modalDisplay ? <div ref={this.textInput} className="poArticleModalPosition" id="catDescModalPosition">

                    <div className="dropdown-menu-city dropdown-menu-vendor" id="pocolorModel">

                        <ul className="dropdown-menu-city-item">
                            {this.state.itemData == undefined || this.state.itemData.length == 0 ? <li><span>No Data Found</span></li> :
                                this.state.itemData.map((data, key) => (
                                    <li key={key} onClick={(e) => this.onSelectedData(`${data.cname}`)} id={"itemUdf"+data.cname} className={this.state.itemDetailValue == `${data.cname}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.cname)}>
                                        <span className="vendor-details">
                                            <span className="vd-name div-col-1">{data.cname}</span>

                                        </span>
                                    </li>))}

                        </ul>
                        <div className="gen-dropdown-pagination">
                            <div className="page-close">
                                <button className="btn-close" type="button" onClick={(e) => this.onCloseSection(e)} id="btn-close">Close</button>
                                {/* <button className="btn-clear" type="button">Clear</button> */}
                            </div>
                            <div className="page-next-prew-btn">
                                {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                                </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                                <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                                </button>
                                    : <button className="pnpb-next" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                        </svg>
                                    </button> : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                            </div>
                        </div>
                    </div>
                </div> : null


        );
    }
}

export default ItemDetailsModal;