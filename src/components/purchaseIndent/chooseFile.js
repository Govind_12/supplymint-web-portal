import React from 'react';
import _ from 'lodash';

class ChooseFile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isPi: true,
            isPo: false,
            fileName: "",
        }
    }
    
    componentDidMount(){
        this.setState({
            isPi: this.props.isPi,
            isPo: this.props.isPo,
            fileName: this.props.fileName
        })
    }
    componentDidUpdate(prevProps) {
        if (this.props.fileName !== prevProps.fileName) {
            this.setState({
                fileChange: this.props.fileName
            })
        }
        if (this.props.excelUpload.downloadTemplate.isSuccess) {
            if (this.props.excelUpload.downloadTemplate.data.resource != null) {
                var a = document.createElement('a');
                a.target = '_blank'
                a.href = this.props.excelUpload.downloadTemplate.data.resource;
                a.innerText = '';
                a.download = true;
                document.body.appendChild(a);
                a.click()
                this.setState({
                    loader: false,
                    success: true,
                    successMessage: this.props.excelUpload.downloadTemplate.data.message
                })
                this.props.downloadTemplateClear()
            }

        }
      }
    handleRadioChange(e){
        if(e.target.name == "PI"){
            this.setState({
                isPi: true,
                isPo: false
            }, () => {
                this.props.isPiPo(this.state.isPo, this.state.isPi)
            })
        }
        else if(e.target.name == "PO"){
            this.setState({
                isPi: false,
                isPo: true
            }, () => {
                this.props.isPiPo(this.state.isPo, this.state.isPi)
            })
        }
    }
    handleCrossBtn(){
        this.setState({
            fileName: "",
            file: {}
        })
        this.props.handleCrossBtnParent()
    }
    
    onInputClick = (event) => {
        event.target.value = ''
    }

    downloadTemplate1 = () => {
        var payload = {
            "templateType":"DIGIPROC_EXCEL_UPLOAD"
        }
        this.props.downloadTemplateRequest(payload)
    }
    render() {
        console.log(this.state.fileName, this.props.fileName)
        return (
            <div className="excel-upload-page-bottom m-top-10">
                <div className="gen-pi-formate">
                    <div className="gpf-dbs">
                        <h3>Choose Type</h3>
                        <ul className="gpf-radio-list m-top-20">
                            <li>
                                <label className="gen-radio-btn">
                                    <input type="radio" name="PI" checked={this.state.isPi} onChange={(e) => this.handleRadioChange(e)} />
                                    <span className="checkmark"></span>
                                                Purchase Indent
                                            </label>
                            </li>
                            <li>
                                <label className="gen-radio-btn">
                                    <input type="radio" name="PO" checked={this.state.isPo} onChange={(e) => this.handleRadioChange(e)} />
                                    <span className="checkmark"></span>
                                                Purchase Order
                                            </label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="eupb-download m-top-10">
                    <div className="eupbd-btn">
                        <button type="button" onClick = {() => this.downloadTemplate1()}>
                            <span className="eupbdb-downi">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="15" viewBox="0 0 21.5 19.926">
                                    <g data-name="download (3)">
                                        <g data-name="Group 2115">
                                            <path fill="#69e384" d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424" />
                                            <path fill="#69e384" d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425" />
                                        </g>
                                    </g>
                                </svg>
                            </span>
                                        Download Template
                                    </button>
                    </div>
                </div>
                <div className="eupb-upload m-top-15">
                    <div className="piumb-upload-area">
                        <svg xmlns="http://www.w3.org/2000/svg" id="file" width="20.452" height="25.565" viewBox="0 0 20.452 25.565">
                            <path fill="#90a5c7" id="Path_1070" d="M82.322 41.565a2.133 2.133 0 0 0 2.13-2.13v-17.9a.426.426 0 0 0-.125-.3l-5.113-5.113a.426.426 0 0 0-.3-.125H66.13A2.133 2.133 0 0 0 64 18.13v21.3a2.133 2.133 0 0 0 2.13 2.13zm-2.983-24.11L83 21.113h-2.38a1.28 1.28 0 0 1-1.278-1.278zm-14.487 21.98V18.13a1.28 1.28 0 0 1 1.278-1.278h12.357v2.983a2.133 2.133 0 0 0 2.13 2.13H83.6v17.47a1.28 1.28 0 0 1-1.278 1.278H66.13a1.28 1.28 0 0 1-1.278-1.278z" class="cls-1" transform="translate(-64 -16)"></path>
                            <path fill="#90a5c7" id="Path_1071" d="M219.835 376h-3.409a.426.426 0 1 0 0 .852h3.409a.426.426 0 1 0 0-.852z" class="cls-1" transform="translate(-207.904 -356.826)"></path>
                            <path fill="#90a5c7" id="Path_1072" d="M168.426 157.965h2.13v4.687a.426.426 0 0 0 .426.426h3.409a.426.426 0 0 0 .426-.426v-4.687h2.13a.426.426 0 0 0 .327-.7l-4.261-5.113a.426.426 0 0 0-.655 0l-4.261 5.113a.426.426 0 0 0 .327.7zm4.261-4.873l3.351 4.021h-1.647a.426.426 0 0 0-.426.426v4.687h-2.557v-4.687a.426.426 0 0 0-.426-.426h-1.647z" class="cls-1" transform="translate(-162.461 -144.757)"></path>
                        </svg>
                        <h4>Drag &amp; Drop files here</h4>
                        <p>Files supported : XLSX, XLS, CSV</p>
                        <label class="piumbua-upload">
                            <input type="file" class="imageChooser" id="fileChoosen" multiple="" size="5" onChange={(e) => this.props.fileChange(e)} onClick={(e) => this.onInputClick(e)} />
                                        Choose File
                                    </label>
                        <span class="piumbua-message">Maximum Size : Upto 10 MB</span>
                        <span className="piumbuau-filename">{this.state.fileName == "" ? this.props.fileName == "" ? "" : this.props.fileName : this.state.fileName}
                            {this.state.fileName == "" ? this.props.fileName == "" ? null : <img onClick={() => this.handleCrossBtn()} src={require('../../assets/clearUpload.svg')} />  : <img onClick={() => this.handleCrossBtn()} src={require('../../assets/clearUpload.svg')} />}</span>
                    </div>
                </div>
            </div>
        )
    }
}
export default ChooseFile;




