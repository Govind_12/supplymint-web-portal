import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

import ToastLoader from "../loaders/toastLoader"
import PoError from "../loaders/poError";
class PiSizeModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            checkedSizeId: '',
            sizeId: this.props.sizeId,
            selectedSizeState: [],
            indentSizeQty: 0,
            orderSizeQty: 0,
            blanceSizeQty: 0,
            refPoWithIndent: false,
            isMaintainSize: true,
            sizes: this.props.sizes,
            checked: false,
            siData: [],
            sizeState: this.props.sizeState,
            ratio: "",
            sizeSelected: [],
            sizeId: "",
            size: "",
            id: "",
            selectedId: "",
            selectall: false,
            deselectall: false,
            checkedData: [],
            ratioError: false,
            isChecked: false,
            loader: false,
            toastLoader: false,
            toastMsg: "",
            success: false,
            alert: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            search: "",
            addNew: true,
            save: false,
            code: "",
            catFive: "",
            codeerr: false,
            catFiveerr: false,

            sizeValue: this.props.sizeValue,
            ratioValue: this.props.ratioValue,
            sizeArray: this.props.sizeArray,
            errorMassage: "",
            poErrorMsg: false,
            sizeDateValue: [...this.props.sizeDataList],
            searchBy: "startWith",
            // searchBy: "",
            modalDisplay: false,
            isToggled: this.props.isToggled,
            checkedSizeValue: "",
            isPredinedSetAvailable: this.props.predefinedSetAvailable,
            selectedData: false,
            callUpdate: true
        }
    }

    componentDidMount() {
        
        this.setState({
            sizeId: this.props.sizeId
        })
        setTimeout(() => {
            if(!this.props.isSet && this.state.refPoWithIndent && this.state.sizes[0] == "All Size"){
                this.setState({
                    sizeId: []
                })
            }
        }, 100);
        // if (this.props.isModalShow) {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
        console.log(this.props,this.props.sizeState , this.state.sizeState)
        // let sizeState = [...this.props.sizeState]
        // if(Object.keys(this.props.sizeDataList).length){
        //     let index = sizeState.findIndex((_) => _.code == this.props.sizeDataList)
        //         sizeState[index] = { ...sizeState[index], ration: this.props.sizeDataList.ratio }
            
        //         this.setState({sizeState})
        // }
        // }
        if(this.props.isMaintainSize != undefined){
            this.setState({
                isMaintainSize: this.props.isMaintainSize
            })
        }
        if(this.props.refPoWithIndent != undefined){
            this.setState({
                refPoWithIndent: this.props.refPoWithIndent
            })
        }
        if(this.state.sizes[0] == 'All Size'){
            this.setState({
                indentSizeQty : this.state.ratioValue,
                blanceSizeQty : this.state.ratioValue
            })
        }
    }

    // componentDidUpdate(prevState, prevProps){
    //     console.log('prev', prevState.sizeState, 'curr', this.state.sizeState)
    //     if(this.state.sizeState != prevState.sizeState && this.state.sizeState != undefined){
    //         let sizeState = [...this.state.sizeState];
    //         let sizeDateValue = [...this.state.sizeDateValue]
    //         if(sizeDateValue.length != 0){
    //             sizeDateValue.forEach(item => {
    //                 sizeState.forEach((size, index) => {
    //                     if(item.id == size.id){
    //                         sizeState[index] = {...sizeState[index], ratio: item.code} 
    //                     }
    //                 })
    //             })
    //         }
    //         this.setState({
    //             sizeState: sizeState
    //         })
    //     }
    // }


    onAddNew(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: false,
                save: true,
                search: "",

            })
        this.onsearchClear();

    }

    onClear() {
        this.setState({
            code: "",
            catFive: ""
        })
    }
    onsearchClear() {

        if (this.state.type == 3) {
            let data = {
                type: 1,
                no: 1,
                // code: this.props.sizeCode,
                id: this.props.sizeRow,
                search: "",
                hl3Name: this.props.department,
                hl3Code: this.props.hl3Code,
                hl1Name: this.props.division,
                hl2Name: this.props.section,
                hl4Name: this.props.articleName,
                itemType: 'size',
                searchBy: this.state.searchBy,
                isToggled: this.state.isToggled,
                itemUdfObj: this.props.itemObj,
            }

            this.props.sizeRequest(data)
        }
        this.setState(
            {
                search: "",
                type: "",
                no: 1,
                isCross: false,
                //  sizeValue:this.props.sizes

            })

        document.getElementById("selectedId").checked = false;
        document.getElementById("deselectedId").checked = false;

    }

    onSearch(e) {
        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,
                // type:""
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {
            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,
                // code: this.props.sizeCode,
                id: this.props.sizeRow,
                search: this.state.search != undefined ? this.state.search : "",
                hl3Name: this.props.department,
                hl3Code: this.props.hl3Code,
                hl1Name: this.props.division,
                hl2Name: this.props.section,
                hl4Name: this.props.articleName,
                itemType: 'size',
                searchBy: this.state.searchBy,
                isToggled: this.state.isToggled,
                itemUdfObj: this.props.itemObj,
            }

            this.props.sizeRequest(data)
            document.getElementById("selectedId").checked = false;
            document.getElementById("deselectedId").checked = false;
        }
    }

    onRemoveCheck = (e, id, data) => {
        console.log('iddd', id)
        var le = this.state.sizeState
        let values = this.state.sizeValue
        let idd = this.state.sizeId
        let sizeDateValue = this.state.sizeDateValue

        let index = idd.indexOf(id);
        idd.splice(index, 1);
        values.splice(index, 1);
        sizeDateValue = sizeDateValue.filter(item => item.id != id);
        le.forEach((item, index) => {
            if(item.id == id) {
                le[index].ratio = ''
            }
        })
        this.setState({
            sizeState: le,
            sizeValue: values,
            sizeId: idd,
            sizeDateValue: sizeDateValue
        })
    }

    onCheckbox(id) {
        var le = this.state.sizeState
        let values = this.state.sizeValue
        let idd = this.state.sizeId
        let sizeDateValue = this.state.sizeDateValue
        var session = this.state.sessionState

        for (var i = 0; i < le.length; i++) {
            if (le[i].id == id) {
                if (le[i].ratio != "" && le[i].ratio !== null && le[i].ratio !== undefined) {
                    if (idd.includes(le[i].id)) {
                        console.log('sizeData', sizeDateValue, id)
                        var index = idd.indexOf(le[i].id);
                        let filterData = sizeDateValue.filter((item) => item.id != id)
                        console.log('filter data', filterData)
                        if (index > -1) {
                            // values[index].ratio = ""
                            le[i].ratio = ""
                            values.splice(index, 1);
                            idd.splice(index, 1);
                            this.setState({
                                sizeState: le,
                                sizeValue: values,
                                sizeDateValue: filterData,
                                sizeId: idd
                            }, () => {
                                // if (!this.props.isModalShow) {
                                //     this.saveDone()
                                // }
                            })
                        }
                    }
                    else {
                        values.push(le[i].cname)
                        idd.push(le[i].id)
                        let obj = {
                            id: le[i].id,
                            code: le[i].code,
                            cname: le[i].cname,
                            ratio: le[i].ratio
                        }
                        sizeDateValue.push(obj)

                        this.setState({
                            sizeId: idd,
                            sizeValue: values,
                            sizeDateValue
                        }, () => {
                            // if (!this.props.isModalShow) {
                            //     this.saveDone()
                            // }
                        })

                    }
                }
                else {

                    this.setState({
                        toastMsg: "fill ratio",
                        toastLoader: true
                    })
                    const t = this;
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false,

                        })
                    }, 1000)
                }
            }

        }

        setTimeout(() => {
            this.setState({
                sizeState: le,

            })
        }, 10)

        let array = this.state.sizeState;
        let sArray = []
        array.forEach(a => {
            sArray.push(a.code)
        })

        if (sArray.length == this.state.sizeValue.length) {
            document.getElementById('selectedId') != null ? document.getElementById('selectedId').checked = true : null
            document.getElementById('deselectedId') != null ? document.getElementById('deselectedId').checked = false : null
        } else if (sArray.length != this.state.sizeValue.length) {
            document.getElementById('selectedId') != null ? document.getElementById('selectedId').checked = false : null
            document.getElementById('deselectedId') != null ? document.getElementById('deselectedId').checked = false : null
        }

    }
    closeToastSize(e) {
        this.setState({
            toastLoader: false
        })
    }
    saveDone() {
        console.log("in save done")
        let s = this.state.sizeDateValue
        const cat5Ratio = [];
        const catFive = [];
        const code = [];
        let sizeList = [];

        if (s != null) {
            let sizeId = {}
            for (let d = 0; d < s.length; d++) {
                if (this.state.sizeValue.includes(s[d].code)) {
                    catFive.push(s[d].cname)
                    cat5Ratio.push(s[d].ratio)
                    let sizeData = {};
                    sizeData = {
                        code: s[d].code,
                        cname: s[d].cname,
                        id: s[d].id,
                        lineId: this.props.sizeRow
                    }
                    sizeList.push(sizeData);
                }
            }
            let finalPasteData = {
                catFive: catFive,
                cat5Ratio: cat5Ratio,
                sizerId: this.props.sizeRow
            }
            console.log("asdafsfrwe5qc",this.state.sizeValue.length != 0)
            if (this.state.sizeValue.length != 0) {
                console.log("update sizs")
                this.props.updateSizeState(finalPasteData);
                this.props.updateSizeList(sizeList);
            }
        }
    }


    ondone() {
        console.log("i'm inside onDone function", this.props.sizeType)        
        if (this.props.size === "old") {
                        let sizeState = [...this.state.sizeState]
                        this.props.closePiSizeModal()
                    } else {
                        let s = this.state.sizeDateValue
                        let cat5Ratio = [];
                        let catFive = [];
                        const code = [];
                        let sizeList = [];
                        let selectedDate = []
                        if (s != null) {

                            console.log(s,this.state.sizeDateValue)
                            let sizeId = {}
                            for (let d = 0; d < s.length; d++) {
                                if (this.state.sizeId.includes(s[d].id)) {

                                    catFive.push(s[d].cname)
                                    cat5Ratio.push(s[d].ratio)
                                    let sizeData = {};

                                    sizeData = {
                                        code: s[d].code,
                                        cname: s[d].cname,
                                        id: s[d].id,
                                        lineId: this.props.sizeRow
                                    }
                                    sizeList.push(sizeData);                                    
                                }
                            }
                            if(!this.props.isSet && catFive[0] == 'All Size' && !this.state.isMaintainSize && this.state.refPoWithIndent) {
                                    catFive.splice(0,1)
                                    cat5Ratio.splice(0,1)
                                    sizeList.splice(0,1)
                            }
                            let finalPasteData = {
                                catFive: catFive,
                                cat5Ratio: cat5Ratio,
                                sizerId: this.props.sizeRow,
                                predefinedSetAvailable: this.state.isPredinedSetAvailable,
                                isToggled: this.state.isToggled
                            }

                            console.log('finaldata', finalPasteData, sizeList)
                            if (this.state.isToggled) {
                                console.log('predefined')
                                if (this.state.sizeValue.length != 0) {
                                    if (this.props.sizeType === "complex") {
                                        console.log('nonSet')
                                        // setTimeout(() => (this.props.updateSimpleSize(sizeList)), 100)
                                        selectedDate = [...finalPasteData]
                                        this.props.updateSizeState(finalPasteData);
                                        this.props.updateSizeList(sizeList);
                                        this.onCloseSection();
                                    }
                                } else {
                                    this.setState({
                                        toastMsg: "select data",
                                        toastLoader: true
                                    })
                                    const t = this
                                    setTimeout(function () {
                                        t.setState({
                                            toastLoader: false
                                        })
                                    }, 1000)
                                }
                            } else {
                                console.log('checkedSize outer')
                                if (this.state.checkedSizeValue !== "") {
                                    console.log('checkedSize inner')
                                    this.props.updateSizeState(finalPasteData);
                                    this.props.updateSizeList(sizeList);
                                    this.onCloseSection();
                                } else {
                                    this.setState({
                                        toastMsg: "select data",
                                        toastLoader: true
                                    })
                                    const t = this
                                    setTimeout(function () {
                                        t.setState({
                                            toastLoader: false
                                        })
                                    }, 1000)
                                }
                            }
                        }
                        if (this.props.sizeType === "simple") {
                            console.log("this.props.sizeType", this.props.sizeType)        

                            if (this.state.sizeDateValue.length == 0) {
                                console.log("1")
                                this.props.updateSingleAmount(this.state.sizeValue)
                            } else {
                                console.log("2")
                                this.props.updateSimpleSize(this.state.sizeValue, this.state.sizeDateValue)
                            }
                            this.props.closePiSizeModal()
                        }
                        document.getElementById("selectedId") != null ? document.getElementById("selectedId").checked = false : null;
                        document.getElementById("deselectedId") != null ? document.getElementById("deselectedId").checked = false : null;
                        document.onkeydown = function (t) {
                            if (t.which == 9) {
                                return true;
                            }
                        }
                    }
                }
                onCloseSection(e) {
                    this.props.closePiSizeModal();
                    let s = this.state.siData == "" ? this.state.sizeState : this.state.siData;
                    if (s != null) {
                        if (s.length != 0) {
                            for (let d = 0; d < s.length; d++) {
                                if (s[d].checked == true) {
                                    s[d].checked = false
                                    s[d].ratio = ""
                                }
                            }
                        }
                    }
                    const t = this

                    t.setState({
                        search: "",
                        addNew: true,
                        save: false,
                        codeerr: false,
                        catFiveerr: false,
                        code: "",
                        catFive: "",
                        type: "",
                        prev: 0,
                        current: 0,
                        next: 0,
                        maxPage: 0,
                        siData: [],
                        sizeState: []
                    })


                    document.getElementById("selectedId") != null ? document.getElementById("selectedId").checked = false : null
                    document.getElementById("deselectedId") != null ? document.getElementById("deselectedId").checked = false : null
                }

                componentWillMount() {

                    this.setState({
                        sizeRow: this.props.sizeRow,
                        sizeCode: this.props.sizeCode,
                        sizeValue: this.props.sizeValue,
                        ratioValue: this.props.ratioValue,
                        sizeArray: this.props.sizeArray,
                        sizes: this.props.sizes,
                        sizeDateValue: this.props.sizeDataList,
                        search: !this.props.isModalShow ? "" : this.props.sizeSearch,
                        type: !this.props.isModalShow || this.props.sizeSearch == "" ? 1 : 3,
                        // checkedSizeValue: this.props.sizeDataList ? this.props.sizeDataList.length > 0 ? this.props.sizeDataList[0].code : "" : ""

                    })
                    if (this.props.sizeDataList && this.props.sizeDataList.length > 0) {
                        const temp = [];
                        let id = [];
                        this.props.sizeDataList.forEach(i => {
                            temp.push(i.code);
                            id.push(i.id);
                        })
                        this.setState({
                            checkedSizeValue: temp.join(),
                            checkedSizeId: id,
                        })
                    }


                    // if (this.props.purchaseIndent.size.isSuccess) {

                    //     if (this.props.purchaseIndent.size.data.resource != null) {

                    //         let sdata = [];
                    //         let sizeArray = this.props.sizeArray
                    //         for (let i = 0; i < this.props.purchaseIndent.size.data.resource.length; i++) {
                    //             let x = this.props.purchaseIndent.size.data.resource[i];
                    //             if (sizeArray != null && sizeArray.length != 0) {
                    //                 sizeArray.forEach(element => {
                    //                     if (element.size == x.code) {
                    //                         x.ratio = element.ratio
                    //                     } else {
                    //                         x.ratio = ""
                    //                     }
                    //                 });

                    //                 let a = x
                    //                 sdata.push(a)
                    //             } else {
                    //                 x.ratio = ""
                    //                 let a = x
                    //                 sdata.push(a)

                    //             }
                    //         }

                    //         if (sdata.length != 0) {
                    //             this.props.purchaseIndent.size.data.resource = sdata
                    //         }





                    //         this.setState({
                    //             sizeState: sdata,
                    //             id: this.props.sizeRow,
                    //             prev: this.props.purchaseIndent.size.data.prePage,
                    //             current: this.props.purchaseIndent.size.data.currPage,
                    //             next: this.props.purchaseIndent.size.data.currPage + 1,
                    //             maxPage: this.props.purchaseIndent.size.data.maxPage
                    //         })
                    //     }
                    //     else {
                    //         this.setState({
                    //             sizeState: [],
                    //             id: this.props.sizeRow,
                    //             prev: 0,
                    //             current: 0,
                    //             next: 0,
                    //             maxPage: 0
                    //         })
                    //     }
                    // }
                }
                updateSizeValue() {
                    let values = this.state.sizeValue
                    var c = this.state.sizeState
                    for (var i = 0; i < c.length; i++) {
                        if (c[i].ratio == "") {
                            if (values.includes(c[i].code)) {
                                var index = values.indexOf(c[i].code);
                                if (index > -1) {
                                    values.splice(index, 1);
                                    this.setState({
                                        sizeValue: values
                                    })
                                }
                            }
                        }
                    }
                }
                updatesize() {
                    let sizeState = this.state.sizeState
                    let sizeArray = this.state.sizeDateValue
                    for (let i = 0; i < sizeState.length; i++) {
                        for (let j = 0; j < sizeArray.length; j++) {
                            if (sizeArray[j].code + sizeArray[j].cname == sizeState[i].code + sizeState[i].cname) {
                                sizeState[i].ratio = sizeArray[j].ratio
                            }
                        }
                    }
                    this.setState({
                        sizeState: sizeState
                    })
                }
                componentWillReceiveProps(nextProps) {
                    this.setState({
                        // id: nextProps.sizeRow,
                        // // sizeValue:this.props.sizeValue,
                        // // ratioValue:this.props.ratioValue,
                        // // sizeArray:this.props.sizeArray
                    })
                    if (nextProps.purchaseIndent.size.isSuccess) {
                        if (nextProps.purchaseIndent.size.data.resource != null) {
                            let sdata = [];
                            let sizeArray = this.props.sizeDataList
                            let resource = nextProps.purchaseIndent.size.data.resource
                            if (resource[0].isToggled == "false" && this.state.isToggled == '' && this.props.predefinedSetAvailable != true) {
                                this.setState({
                                    isPredinedSetAvailable: true,
                                    isToggled: false
                                })
                            } else if (resource[0].isToggled == "true" && this.state.isPredinedSetAvailable != true && this.state.isToggled == '') {
                                this.setState({
                                    isPredinedSetAvailable: false,
                                    isToggled: true
                                })
                            }
                            for (let i = 0; i < resource.length; i++) {
                                if (this.state.isToggled) {
                                    resource[i].ratio = ""
                                }
                                let a = resource[i]
                                sdata.push(a)
                            }
                            if (window.screen.width > 1200) {
                                // if (this.props.isModalShow) {
                                this.textInput.current.focus();
                                // }
                                // else if (!this.props.isModalShow) {
                                //     if (nextProps.purchaseIndent.size.data.resource != null) {
                                //         this.setState({
                                //             focusedLi: nextProps.purchaseIndent.size.data.resource[0].code,
                                //             search: !this.props.isModalShow ? "" : this.props.sizeSearch,
                                //             type: !this.props.isModalShow || this.props.sizeSearch == "" ? 1 : 3

                                //         })
                                //         document.getElementById(nextProps.purchaseIndent.size.data.resource[0].code) != null ? document.getElementById(nextProps.purchaseIndent.size.data.resource[0].code).focus() : null
                                //     } else {
                                //         this.setState({
                                //             focusedLi: "closeButton",
                                //             search: !this.props.isModalShow ? "" : this.props.sizeSearch,
                                //             type: !this.props.isModalShow || this.props.sizeSearch == "" ? 1 : 3

                                //         })

                                //     }



                                // }
                            }
                            console.log('sizeList', this.props.sizeDataList)
                            let sizeDataList = this.state.sizeDateValue != undefined ? this.state.sizeDateValue : this.props.sizeDataList
                                if(Object.keys(sizeDataList).length){
                                    let index = sdata.findIndex((_) => _.id == sizeDataList.id)
                                        sdata[index] = { ...sdata[index], ratio: sizeDataList.ratio }
                                }
                                if(this.state.sizeState != undefined && this.props.sizeDataList.length == 0){
                                    let sizeDateValue = this.state.sizeDateValue;
                                    if(sizeDateValue.length != 0){
                                        sizeDateValue.forEach((item, idx) => {
                                            sdata.forEach((size, index) => {
                                                if(item.id == size.id){
                                                    sdata[index] = {...sdata[index], ratio: item.ratio} 
                                                }
                                            })
                                        })
                                    }
                                }
                            this.setState({
                                sizeState: sdata,
                                id: this.props.sizeRow,
                                prev: nextProps.purchaseIndent.size.data.prePage,
                                current: nextProps.purchaseIndent.size.data.currPage,
                                next: nextProps.purchaseIndent.size.data.currPage + 1,
                                maxPage: nextProps.purchaseIndent.size.data.maxPage
                            }, () => {
                                if (sizeArray != null && sizeArray.length != 0) {
                                    this.updatesize()
                                }

                            })
                        }
                        else {
                            this.setState({
                                sizeState: [],
                                id: this.props.sizeRow,
                                prev: 0,
                                current: 0,
                                next: 0,
                                maxPage: 0
                            })
                        }
                        // setTimeout(() => {
                        //     this.updateSizeValue();
                        // }, 1)
                        this.setState({
                            modalDisplay: true,

                        })
                    }
                    if (nextProps.purchaseIndent.piAddNew.isSuccess) {
                        this.onFinalSave();
                        this.props.piAddNewRequest();
                    }
                }
                onRequest(e) {
                    e.preventDefault();
                    this.setState({
                        success: false
                    });
                }
                onError(e) {
                    e.preventDefault();
                    this.setState({
                        alert: false
                    });
                    document.onkeydown = function (t) {
                        if (t.which) {
                            return true;
                        }
                    }
                }
                handleRadioChange(e, ratioId, data) {
                    const c = this.state.sizeState
                    let values = this.state.sizeValue;
                    let idd = this.state.sizeId;
                    let id = [];
                    let sizeDateValue = this.state.sizeDateValue;
                    let ratioArray = [];
                    let cNameArray = [];
                    let codeArray = [];
                    sizeDateValue.forEach(x => {
                        codeArray.push(x.code)
                        id.push(x.id)
                    })
                    if (data.id == id[0]) {

                    } else {
                        for (var i = 0; i < c.length; i++) {
                            if (!idd.includes(data.id)) {
                                if (data.id == c[i].id) {
                                    values = c[i].cname
                                    // c[i].ratio = ratioId;
                                    let id = [];
                                    id.push(c[i].id)
                                    ratioArray = c[i].ratio.split(',')
                                    cNameArray = c[i].cname.split(',')
                                    codeArray = c[i].code.split(',')
                                    this.setState({
                                        checkedSizeId: id,
                                        sizeId: id,
                                        checkedSizeValue: c[i].code,
                                        focusedLi: c[i].id
                                    })
                                    sizeDateValue = [];
                                    ratioArray.forEach((element, k) => {
                                        sizeDateValue.push({
                                            id: c[i].id,
                                            code: codeArray[k],
                                            cname: cNameArray[k],
                                            ratio: element
                                        })
                                        this.setState({
                                            sizeValue: values,
                                            sizeDateValue,
                                            sizeState: c
                                        })

                                    });
                                }
                            }
                        }
                    }
                }

                handleChangee(ratioId, e, data) {
                    console.log('sizeee', this.state.sizeState)
                    var c = this.state.sizeState
                    var ratioId = ratioId;
                    let selectedSize = [...this.state.selectedSizeState]
                    if(data != null || data != undefined){
                        if(selectedSize.length == 0 || selectedSize.filter(item => item.id == data.id).length == 0){
                            selectedSize.push({id: data.id, value: e.target.value})
                        } else {
                            selectedSize = selectedSize.map(item => {
                                if(item.id == data.id){
                                    return {id: data.id, value: e.target.value}
                                } else {
                                    return item
                                }
                            })
                        }
                    }
                    for (let i = 0; i < c.length; i++) {
                        if (c[i].id == data.id) {
                            if (c[i].ratio == null) {
                                // let obj = {
                                //     code: c[k].code,
                                //     cname: c[k].cname,
                                //     ratio: c[k].ratio
                                // }
                                // changeData.push(obj)
                                // this.setState({
                                //     sizeState: changeData
                                // })
                            } else {
                                let index = c.findIndex((obj => obj.id == data.id));
                                c[index] = { ...c[index], ration: e.target.value }
                                this.setState({
                                    sizeState: c,
                                    selectedSizeState: selectedSize
                                },() => this.calculateSizeQty())

                            }
                        }
                    }
                    if (this.props.sizeType == "simple") {
                        let changeData = {}
                        this.props.updateSizeDetails(e.target.value, ratioId)
                        for (let i = 0; i < c.length; i++) {
                            if (c[i].id == data.id) {
                                if (c[i].ratio == null) {
                                    // let obj = {
                                    //     code: c[i].code,
                                    //     cname: c[i].cname,
                                    //     ratio: c[i].ratio
                                    // }
                                    // changeData.push(obj)
                                    // this.setState({
                                    //     sizeState: changeData
                                    // })
                                } else {
                                    let index = c.findIndex((obj => obj.id == data.id));
                                    c[index] = { ...c[index], ration: e.target.value }
                                    this.setState({
                                        sizeState: c
                                    })

                                }
                            }
                        }
                    }
                    // else {
                    // if(this.isToggled) {}
                    // if (e.target.validity.valid == true) {

                    for (var i = 0; i < c.length; i++) {
                        if (data.id == c[i].id) {

                            c[i].ratio = e.target.validity.valid ? e.target.value : ''
                            this.setState({
                                focusedLi: c[i].code
                            })

                        }
                    }
                    // }

                    if (e.target.value == "" || !Number(e.target.value)) {
                        console.log("asdbahdoyb")
                        let values = [...this.state.sizeValue]
                        let idd = [...this.state.sizeId]
                        let sizeDateValue = [...this.state.sizeDateValue]
                        let filterData = sizeDateValue.filter((item) => item.id !== data.id)
                        for (let i = 0; i < c.length; i++) {
                            if (data.id == c[i].id) {
                                if (idd.includes(c[i].id)) {
                                    var index = idd.indexOf(c[i].id);
                                    if (index > -1) {
                                        values.splice(index, 1);
                                        idd.splice(index, 1);
                                        this.setState({
                                            sizeValue: values,
                                            sizeId: idd,
                                            sizeDateValue: filterData
                                        }, () => {
                                            // if (!this.props.isModalShow) {
                                            //     this.saveDone()
                                            // }
                                        })
                                    }
                                }

                            }
                        }
                    } else if (e.target.value != "" && Number(e.target.value)) {
                        let values = [...this.state.sizeValue]
                        let idd = [...this.state.sizeId]
                        let sizeDateValue = [...this.state.sizeDateValue]
                        for (var k = 0; k < c.length; k++) {
                            if (data.id == c[k].id) {
                                if (!idd.includes(c[k].id)) {
                                    values.push(c[k].cname)
                                    idd.push(c[k].id)
                                    let obj = {
                                        id: c[k].id,
                                        code: c[k].code,
                                        cname: c[k].cname,
                                        ratio: c[k].ratio
                                    }
                                    sizeDateValue.push(obj)
                                    this.setState({
                                        sizeValue: values,
                                        sizeId: idd,
                                        sizeDateValue,
                                        sizeState: c
                                    }, () => {
                                        // if (!this.props.isModalShow) {
                                        //     this.saveDone()
                                        // }
                                    })
                                } else {
                                    for (let i = 0; i < sizeDateValue.length; i++) {
                                        if (sizeDateValue[i].id == data.id) {
                                            if(sizeDateValue[i].cname.toUpperCase() == "ALL SIZE") {
                                                sizeDateValue[i].cname = c[k].cname
                                                sizeDateValue[i].code = c[k].code
                                                sizeDateValue[i].ratio = c[k].ratio
                                            }
                                            else {
                                                sizeDateValue[i].ratio = c[k].ratio
                                            }
                                        }
                                    }
                                    this.setState({
                                        sizeDateValue,
                                    }, () => {
                                        // if (!this.props.isModalShow) {
                                        //     this.saveDone()
                                        // }
                                    })
                                }
                            }
                        }
                        // }
                    }




                    const s = this;
                    setTimeout(function () {

                        s.setState({
                            sizeState: c,

                        })
                    }, 10)


                    let array = this.state.sizeState;
                    let sArray = []
                    array.forEach(a => {

                        sArray.push(a.code)
                    })

                    if (sArray.length == this.state.sizeValue.length) {
                        document.getElementById('selectedId') != null ? document.getElementById('selectedId').checked = true : null
                        document.getElementById('deselectedId') != null ? document.getElementById('deselectedId').checked = false : null
                    } else if (sArray.length != this.state.sizeValue.length) {
                        document.getElementById('selectedId') != null ? document.getElementById('selectedId').checked = false : null
                        document.getElementById('deselectedId') != null ? document.getElementById('deselectedId').checked = false : null

                    }


                }

                calculateSizeQty = () =>{
                    if(!this.props.isSet && this.state.refPoWithIndent && !this.state.isMaintainSize && this.state.sizes[0] == "All Size"){
                            let ratio = 0;
                            this.state.selectedSizeState.forEach(size => {
                                console.log(size)
                                if(size.value != ''){
                                    ratio = ratio + parseInt(size.value);
                                }                    
                            })
                            console.log('ratio', ratio)
                            this.setState({
                                orderSizeQty: ratio,
                                blanceSizeQty: this.state.indentSizeQty - ratio
                            })
                    }
                }

                onSelectAll() {
                    let c = this.state.sizeState;
                    var session = this.state.sessionState
                    let values = this.state.sizeValue


                    if (c == "") {
                        document.getElementById("selectedId").checked = false
                    } else {
                        for (var j = 0; j < c.length; j++) {
                            if ((c[j].ratio != "")) {
                                values.push(c[j].code)

                                this.setState({
                                    sizeValue: values
                                })
                            } else if (c[j].ratio == "") {

                                document.getElementById("selectedId").checked = false
                                this.setState({
                                    toastLoader: true,
                                    toastMsg: "Fill all ratio",

                                })

                                const t = this
                                setTimeout(function () {
                                    t.setState({
                                        toastLoader: false
                                    })
                                }, 1000)
                            }



                            this.setState({
                                sizeState: c
                            })

                        }

                    }


                }
                onDeselectAll() {

                    let c = this.state.sizeState;
                    var session = this.state.sessionState
                    let values = this.state.sizeValue
                    this.setState({
                        sizeValue: [],
                    })

                    if (c == "") {
                        document.getElementById("deselectedId").checked = false
                    }





                }

                code() {

                    if (
                        this.state.code == "" || this.state.code.trim() == "") {
                        this.setState({
                            codeerr: true
                        });
                    } else {
                        this.setState({
                            codeerr: false
                        });
                    }

                }
                catFive() {

                    if (
                        this.state.catFive == "" || this.state.catFive.trim() == "") {
                        this.setState({
                            catFiveerr: true
                        });
                    } else {
                        this.setState({
                            catFiveerr: false
                        });
                    }

                }



                handleChange(e) {
                    e.preventDefault()
                    if (e.target.id == "code") {
                        if (e.target.validity.valid) {
                            this.setState(
                                {
                                    code: e.target.value
                                },
                                () => {
                                    this.code();
                                }

                            );
                        }
                    } else if (e.target.id == "catFive") {

                        this.setState(
                            {
                                catFive: e.target.value
                            },
                            () => {
                                this.catFive();
                            }

                        );
                    } else if (e.target.id == "sizeSearch") {

                        this.setState(
                            {
                                search: e.target.value
                            }

                        );
                        if(e.target.value != ''){
                            this.setState({
                                isCross: true,
                            })
                        }
                        else{
                            this.setState({
                                isCross: false,
                            }) 
                        }

                    } else if (e.target.id == "searchBysize") {
                        this.setState({
                            searchBy: e.target.value
                        }, () => {
                            if (this.state.search != "") {
                                let data = {
                                    type: this.state.type,
                                    no: 1,
                                    // code: this.props.sizeCode,
                                    id: this.props.sizeRow,
                                    search: this.state.search != undefined ? this.state.search : "",
                                    hl3Name: this.props.department,
                                    hl3Code: this.props.hl3Code,
                                    hl1Name: this.props.division,
                                    hl2Name: this.props.section,
                                    hl4Name: this.props.articleName,
                                    itemType: 'size',
                                    searchBy: this.state.searchBy,
                                    isToggled: this.state.isToggled,
                                    itemUdfObj: this.props.itemObj,
                                }

                                this.props.sizeRequest(data)
                            }

                        })
                    }


                }



                page(e) {
                    if (e.target.id == "prev") {
                        this.setState({
                            prev: this.props.purchaseIndent.size.data.prePage,
                            current: this.props.purchaseIndent.size.data.currPage,
                            next: this.props.purchaseIndent.size.data.currPage + 1,
                            maxPage: this.props.purchaseIndent.size.data.maxPage,
                        })
                        if (this.props.purchaseIndent.size.data.currPage != 0) {
                            let data = {
                                type: this.state.type,
                                no: this.props.purchaseIndent.size.data.currPage - 1,
                                // code: this.props.sizeCode,
                                id: this.props.sizeRow,
                                search: this.state.search != undefined ? this.state.search : "",
                                hl3Name: this.props.department,
                                hl3Code: this.props.hl3Code,
                                hl1Name: this.props.division,
                                hl2Name: this.props.section,
                                hl4Name: this.props.articleName,
                                itemType: 'size',
                                searchBy: this.state.searchBy,
                                isToggled: this.state.isToggled,
                                itemUdfObj: this.props.itemObj,

                            }

                            this.props.sizeRequest(data);
                        }
                    } else if (e.target.id == "next") {
                        this.setState({
                            prev: this.props.purchaseIndent.size.data.prePage,
                            current: this.props.purchaseIndent.size.data.currPage,
                            next: this.props.purchaseIndent.size.data.currPage + 1,
                            maxPage: this.props.purchaseIndent.size.data.maxPage,
                        })
                        if (this.props.purchaseIndent.size.data.currPage != this.props.purchaseIndent.size.data.maxPage) {
                            let data = {
                                type: this.state.type,
                                no: this.props.purchaseIndent.size.data.currPage + 1,
                                // code: this.props.sizeCode,
                                id: this.props.sizeRow,
                                search: this.state.search != undefined ? this.state.search : "",
                                hl3Name: this.props.department,
                                hl3Code: this.props.hl3Code,
                                hl1Name: this.props.division,
                                hl2Name: this.props.section,
                                hl4Name: this.props.articleName,
                                itemType: 'size',
                                searchBy: this.state.searchBy,
                                isToggled: this.state.isToggled,
                                itemUdfObj: this.props.itemObj,
                            }

                            this.props.sizeRequest(data)
                        }
                    }
                    else if (e.target.id == "first") {
                        this.setState({
                            prev: this.props.purchaseIndent.size.data.prePage,
                            current: this.props.purchaseIndent.size.data.currPage,
                            next: this.props.purchaseIndent.size.data.currPage + 1,
                            maxPage: this.props.purchaseIndent.size.data.maxPage,
                        })
                        if (this.props.purchaseIndent.size.data.currPage <= this.props.purchaseIndent.size.data.maxPage) {
                            let data = {
                                type: this.state.type,
                                no: 1,
                                // code: this.props.sizeCode,
                                id: this.props.sizeRow,
                                search: this.state.search != undefined ? this.state.search : "",
                                hl3Name: this.props.department,
                                hl3Code: this.props.hl3Code,
                                hl1Name: this.props.division,
                                hl2Name: this.props.section,
                                hl4Name: this.props.articleName,
                                itemType: 'size',
                                searchBy: this.state.searchBy,
                                isToggled: this.state.isToggled,
                                itemUdfObj: this.props.itemObj,
                            }

                            this.props.sizeRequest(data)
                        }

                    }
                }

                onCancel(e) {
                    e.preventDefault();
                    this.setState(
                        {
                            addNew: true,
                            save: false,
                            code: "",
                            catFive: ""

                        }
                    )
                }

                closeErrorRequest(e) {
                    this.setState({
                        poErrorMsg: !this.state.poErrorMsg
                    })
                }
                _handleKeyPress = (e) => {
                    if (e.key === 'Enter') {
                        e.preventDefault()
                        this.onSearch();
                    }
                }
                downArrow(e, cname) {

                    if (e.key == 'ArrowDown') {
                        var c = sessionStorage.getItem(this.props.sizeCode + '-' + "size") == null ? this.state.sizeState : this.state.siData.length == 0 ? this.state.sizeState.concat(JSON.parse(sessionStorage.getItem(this.props.sizeCode + '-' + "size"))) : this.state.siData;
                        let nextId = ""
                        let index = ""
                        for (let i = 0; i < c.length; i++) {
                            if (cname == c[i].code) {
                                index = i

                            }
                            if (index != c.length - 1) {
                                nextId = c[index + 1].code
                            }
                        }
                        if (nextId != "") {
                            document.getElementById(nextId).focus()
                        }

                    }
                    if (e.key == 'ArrowUp') {
                        var c = sessionStorage.getItem(this.props.sizeCode + '-' + "size") == null ? this.state.sizeState : this.state.siData.length == 0 ? this.state.sizeState.concat(JSON.parse(sessionStorage.getItem(this.props.sizeCode + '-' + "size"))) : this.state.siData;
                        let nextId = ""
                        let index = ""
                        for (let i = 0; i < c.length; i++) {
                            if (cname == c[i].code) {
                                index = i

                            }
                            if (index != 0) {
                                nextId = c[index - 1].code
                            }
                        }
                        if (nextId != "") {
                            document.getElementById(nextId).focus()
                        }


                    }
                    if (e.key == "Tab") {
                        window.setTimeout(function () {
                            document.getElementById("selectedId").focus()
                        }, 0)
                    }

                }
                paginationKey(e) {
                    if (e.target.id == "prev") {
                        if (e.key == "Enter") {
                            this.page(e)
                        }
                        if (e.key == "Tab") {

                            {
                                this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                                    this.state.sizeState.length != 0 ?
                                        document.getElementById(this.state.sizeState[0].code).focus() : null
                            }

                        }
                    }
                    if (e.target.id == "next") {
                        if (e.key == "Enter") {
                            this.page(e)
                        }
                        if (e.key == "Tab") {
                            if (this.state.sizeState.length != 0) {
                                document.getElementById(this.state.sizeState[0].code).focus()
                            }
                        }
                    }
                    if (e.key == "Escape") {
                        this.onCloseSection(e)
                    }


                }
                doneKeyDown(e) {
                    if (e.key === "Enter") {
                        this.ondone();
                    }
                    if (e.key === "Tab") {
                        window.setTimeout(function () {
                            document.getElementById("doneButton").blur()
                            document.getElementById("closeButton").focus()
                        }, 0)
                    }
                }
                closeKeyDown(e) {
                    if (e.key === "Enter") {
                        this.onCloseSection();
                    }
                    if (e.key === "Tab") {
                        window.setTimeout(function () {
                            document.getElementById("closeButton").blur()
                            // this.props.isModalShow ? 
                            document.getElementById("sizeSearch").focus()
                            //  :
                            //     this.state.sizeState.length != 0 ?

                            //         this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus()

                            //         : null
                        }, 0)
                    }

                }
                selectallKeyDown(e) {
                    if (e.key == "Tab") {
                        window.setTimeout(function () {
                            document.getElementById('deselectedId') != null ? document.getElementById('deselectedId').focus() : null

                            document.getElementById('selectedId') != null ? document.getElementById('selectedId').blur() : null



                        }, 0)
                    }
                    else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                        e.preventDefault()
                    }
                    else if (e.key == "Enter") {
                        this.onSelectAll()
                    }
                }
                deselectallKeyDown(e) {
                    if (e.key == "Tab") {
                        window.setTimeout(function () {

                            document.getElementById("deselectedId").blur()
                            document.getElementById("doneButton").focus()

                        }, 0)
                    }
                    else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                        e.preventDefault()
                    }
                    else if (e.key == "Enter") {
                        this.onDeselectAll()
                    }
                }

                changeToggle() {
                    // e.persist
                    this.setState({
                        sizeId: [],
                        checkedSizeId: [],
                        isToggled: !this.state.isToggled,
                        checkedData: "",
                        sizeDateValue: [],
                        selectedSizeState:[],
                        checkedSizeValue: "",
                        sizeValue: [],
                        ratioValue: []
                    }, () => {
                        // this.onSearch();
                        let data = {
                            type: this.state.type,
                            no: 1,
                            // code: this.props.sizeCode,
                            id: this.props.sizeRow,
                            search: this.state.search != undefined ? this.state.search : "",
                            hl3Name: this.props.department,
                            hl3Code: this.props.hl3Code,
                            hl1Name: this.props.division,
                            hl2Name: this.props.section,
                            hl4Name: this.props.articleName,
                            itemType: 'size',
                            searchBy: this.state.searchBy,
                            isToggled: this.state.isToggled,
                            itemUdfObj: this.props.itemObj,
                        }

                        this.props.sizeRequest(data)
                    })
                }

                changeSelectedData = (event) => {
                    this.setState({
                        selectedData: !this.state.selectedData
                    })
                }



                render() {
                    console.log('sizeState', this.state.sizeState, 'id', this.state.sizeId, 'props', this.props.sizeId, 'sizeDate', this.state.sizeDateValue)
                    console.log('sizevalue', this.state.sizeValue)
                    console.log("selectedSize", this.state.selectedSizeState)
                    // if (this.state.focusedLi != "" && this.props.sizeSearch == this.state.search) {
                    //     document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
                    // }


                    //open modal for dropdown code start here
                    // if (this.props.sizePosId != "" && this.props.sizePosId != undefined && !this.props.isModalShow) {
                    //     let modalWidth = 500;
                    //     let modalWindowWidth = document.getElementById('itemSetModal').getBoundingClientRect();
                    //     let position = document.getElementById(this.props.sizePosId).getBoundingClientRect();
                    //     let leftPosition = position.left;
                    //     let idNo = parseInt(this.props.sizePosId.replace(/\D/g, '')) + 1;
                    //     let top = 57 + (35 * idNo);

                    //     let newLeft = leftPosition > 0 ? leftPosition - 156 : 0;


                    //     $('#piSizeModalPosition').css({ 'left': newLeft, 'top': top });
                    //     $('.poArticleModalPosition').removeClass('hideSmoothly');

                    // }
                    //open modal for dropdown code end here



                    const hash = window.location.hash;
                    const {

                        code, codeerr, catFive, catFiveerr, search
                    } = this.state;

                    const { ratioError, isToggled, isPredinedSetAvailable, selectedData } = this.state;

                    return (

                        // this.props.isModalShow ?
                        <div className={this.props.sizeModalAnimatiion ? "modal display_block" : "display_none"} id="piopensizeModel">
                            <div className={this.props.sizeModalAnimatiion ? "backdrop modal-backdrop-new" : "display_none"}></div>
                            <div className={this.props.sizeModalAnimatiion ? "o" : "display_none"}>
                                <div className={this.props.sizeModalAnimatiion ? "modal-content modalpoColor modalShow gen-color-modal" : "modalHide"}>
                                    <div className="gcm-head">
                                        <div className="gcmh-top">
                                            <div className="gcmht-left">
                                                <h3>Select Size</h3>
                                                {isToggled && <p>You can select multiple size from below records</p>}
                                                {!isToggled && <p>You can select one predifined set from below records</p>}
                                            </div>
                                            <div className="gcmht-right">
                                                {console.log("clicking on Done now!!")}
                                                <button type="button" className="gcmht-close" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onCloseSection(e)}>Close</button>
                                                {!this.props.isSet && this.state.refPoWithIndent && !this.state.isMaintainSize && this.state.sizes[0] == "All Size" && this.state.blanceSizeQty < 0 ?
                                                <button type="button" className="gcmht-done btnDisabled" id="doneButton" >Done</button>
                                                : <button type="button" className="gcmht-done" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={() => this.ondone()}>Done</button>}
                                            </div>
                                        </div>
                                        <div className="gcmh-bottom">
                                            <div className="gcmhb-left">
                                                {this.state.addNew ?<div className="gcmhb-search">
                                                    <input type="search" autoComplete="off" autoCorrect="off" ref={this.textInput} value={search} onKeyPress={this._handleKeyPress} id="sizeSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                    <button className="search-image" onClick={(e) => this.onSearch(e)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                                            <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                                        </svg>
                                                    </button>
                                                    {this.state.isCross ? <span className="closeSearch" onClick={(e) => this.onsearchClear(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                                                </div>: null}
                                            </div>
                                            <div className="gcmhb-right">
                                                {this.props.sizeType == "complex" ? <div className="nph-switch-btn">
                                                    <label className="tg-switch" >
                                                        <input type="checkbox" disabled={(!isPredinedSetAvailable && isToggled == "false")} checked={!isToggled} onChange={() => this.changeToggle()} />
                                                        <span className="tg-slider tg-round"></span>
                                                    </label>
                                                    {isToggled && <label className="nph-wbtext">New Set</label>}
                                                    {!isToggled && <label className="nph-wbtext">Predifined Set</label>}
                                                </div> :
                                                <div className="nph-switch-btn">
                                                    <label className="tg-switch" >
                                                        <input type="checkbox" disabled={true} checked={false} />
                                                        <span className="tg-slider tg-round"></span>
                                                    </label>
                                                    {<label className="nph-wbtext">New Set</label>}
                                                    {/* {!isToggled && <label className="nph-wbtext">Predifined Set</label>} */}
                                                </div>}

                                                <div className="nph-switch-btn">
                                                    <label className="tg-switch" >
                                                        <input type="checkbox" checked={selectedData} onChange={this.changeSelectedData} />
                                                        <span className="tg-slider tg-round"></span>
                                                    </label>
                                                    {selectedData && <label className="nph-wbtext">Selected data</label>}
                                                    {!selectedData && <label className="nph-wbtext"> Selected data</label>}
                                                </div>
                                            </div>
                                        </div>
                                        <ul className="list-inline width_100 m-top-5">
                                            <div className="col-md-5 col-sm-5 pad-0 modalDropBtn addNewWidth chooseDataModal">
                                                {this.state.save ? <div className="col-md-8 col-sm-8 pad-0 mrpLi">
                                                    <li>
                                                        <input type="text" autoComplete="off" autoCorrect="off" pattern="[0-9]*" className={codeerr ? "errorBorder " : ""} onChange={(e) => this.handleChange(e)} id="code" name="code" value={code} placeholder="Enter Code" />
                                                        {/* {codeerr ? <img src={errorIcon} className="error_pop_up" /> : null} */}
                                                        {codeerr ? (
                                                            <span className="error">
                                                                Enter Code
                                                </span>
                                                        ) : null}
                                                    </li>
                                                    <li> <input type="text" className={catFiveerr ? "errorBorder" : ""} onChange={(e) => this.handleChange(e)} id="catFive" name="catFive" value={catFive} placeholder="Enter Size" />

                                                        {catFiveerr ? (
                                                            <span className="error">
                                                                Enter Size
                                                </span>
                                                        ) : null}
                                                    </li>

                                                </div> : null}
                                                {this.state.addNew ? <li className="width100">
                                                {/* {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBysize" name="searchBysize" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>
                                                    <option value="contains">Contains</option>
                                                    <option value="startWith"> Start with</option>
                                                    <option value="endWith">End with</option>
                                                </select> : null} */}
                                                {/* <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} value={search} onKeyPress={this._handleKeyPress} id="sizeSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" onClick={(e) => this.onSearch(e)}>FIND
                                                            <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label> */}
                                                </li>
                                                    : null}
                                            </div>
                                            {/* __________________--DON'T REMOVE THIS COMMENTED CODE_____________________ */}
                                            {/* {this.state.addNew ? <li className="float_right">
                                                <label className="m-r-15">
                                                { hash == "#/purchase/purchaseIndent" ? <button type="button" className="findButton" onClick={(e)=>this.onAddNew(e)}>ADD NEW
                                                <span> + </span>
                                                    </button>:null}
                                                </label>
                                                <label>
                                                    <button style={{ width: "auto" }} type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} onClick={(e) => this.onsearchClear(e)}>CLEAR SEARCH</button>
                                                </label>
                                            </li> : null} */}
                                            {this.state.save ? <li className="float_right">
                                                <label className="m-r-15">
                                                    <button type="button" className="findButton" onClick={(e) => this.onSave(e)}>SAVE</button>
                                                </label>
                                                <label>
                                                    <button type="button" className="findButton" onClick={(e) => this.onCancel(e)}>CANCEL</button>
                                                </label>
                                            </li> : null}
                                        </ul>
                                    </div>

                                    {!this.props.isSet && !this.state.isMaintainSize && this.state.refPoWithIndent && this.state.sizes[0] == 'All Size' && <table className="table tableModal table-hover">
                                        <tbody className="rowPad">
                                            <tr>
                                                <td>Indent Qty: {this.state.indentSizeQty}</td>
                                                <td>Order Qty: {this.state.orderSizeQty}</td>
                                                <td>Blance Qty: {this.state.blanceSizeQty}</td>
                                            </tr>
                                        </tbody>
                                    </table>}

                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <div className="gcm-body">
                                            <div className="gcm-table">
                                                {!selectedData && <div className="gcmt-manage">
                                                    <table className="table gcmt-inner">
                                                        <thead>
                                                            <tr>
                                                                <th className="fix-action-btn"></th>
                                                                <th><label>Size</label></th>
                                                                <th><label>{this.props.isSet && this.props.sizeType == "simple" ? 'Quantity' : this.props.isSet ? 'Set Ratio' : 'Quantity'}</label></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody className="rowPad">
                                                            {this.props.size == "old" ?
                                                            this.state.sizeState.map((data, key) => (<tr key={key} >
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0">
                                                                                <input type="checkBox" checked={this.state.sizeId.includes(data.id)} onClick={() => this.onCheckbox(data.id)} />
                                                                                <span className="checkmark1" htmlFor={data.id}></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                                <td>
                                                                    <input type="text" autoComplete="off" autoCorrect="off" onKeyDown={(e) => this.downArrow(e, data.code)} pattern="^-?[0-9]\d*\.?\d*$"
                                                                        onKeyPress={e => /[\+\-\.\,]$/.test(e.key) && e.preventDefault()}
                                                                        className="modal_tableBox " id={data.code} onChange={(e) => this.handleChangee(`${data.code}`, e, data)} value={data.ratio}></input>
                                                                </td>
                                                            </tr>
                                                            ))
                                                            : this.props.sizeType == "simple" ?
                                                            this.state.sizeState == "" || this.state.sizeState == undefined || this.state.sizeState == null || this.state.sizeState.length == 0 ? <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr> : this.state.sizeState.map((data, key) => (
                                                            <tr key={key} >
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0">
                                                                                <input type="checkBox" checked={this.state.sizeId.includes(data.id)} onClick={() => this.onCheckbox(data.id)} />
                                                                                <span className="checkmark1" htmlFor={data.id}></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                                <td>
                                                                    <input type="text" autoComplete="off" autoCorrect="off" onKeyDown={(e) => this.downArrow(e, data.code)} pattern="^-?[0-9]\d*\.?\d*$"
                                                                        onKeyPress={e => /[\+\-\.\,]$/.test(e.key) && e.preventDefault()}
                                                                        // this.props.sizeType == "simple" ? this.state.sizeDateValue.code == data.code ? this.state.sizeDateValue.ratio : null : data.ratio
                                                                        className="modal_tableBox " id={data.code} onChange={(e) => this.handleChangee(`${data.code}`, e, data)} value={data.ratio}></input>
                                                                </td>
                                                            </tr>)) :
                                                            this.state.type == 3 ? this.state.sizeState == "" || this.state.sizeState == undefined || this.state.sizeState == null || this.state.sizeState.length == 0 ? <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr> : isToggled == true ? this.state.sizeState.map((data, key) => (
                                                            <tr key={key} >
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0">
                                                                                <input type="checkBox" checked={this.state.sizeId.includes(data.id)} onClick={() => this.onCheckbox(data.id)} />
                                                                                <span className="checkmark1" htmlFor={data.id}></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                                <td>
                                                                    <input autoComplete="off" autoCorrect="off" type="text" pattern="^-?[0-9]\d*\.?\d*$" className="modal_tableBox " id={data.code} onChange={(e) => this.handleChangee(`${data.code}`, e, data)} value={data.ratio}></input>
                                                                </td>
                                                            </tr>
                                                            )) :
                                                            this.state.sizeState.map((data, key) => (
                                                            <tr className="" key={key} style={{ cursor: "pointer" }} onClick={(e) => { this.handleRadioChange(e, data.code, data) }}>
                                                                <td >
                                                                    <input style={{ position: 'relative' }} className="predifineSetRadio" type="radio" checked={this.state.checkedSizeId.includes(data.code)} onChange={e => this.handleRadioChange(e, data.code, data)} />
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                                <td><label>{data.ratio}</label></td>
                                                            </tr>
                                                            )) : this.state.sizeState == "" || this.state.sizeState == undefined || this.state.sizeState == null || this.state.sizeState.length == 0 ? <tr className="modalTableNoData"><td colSpan="3"> NO DATA FOUND </td></tr> : isToggled == true ? this.state.sizeState.map((data, key) => (
                                                            <tr key={key} >
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0">
                                                                            <input type="checkBox" checked={this.state.sizeId.includes(data.id)} onClick={() => this.onCheckbox(data.id)} />
                                                                                <span className="checkmark1" htmlFor={data.id}></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                                <td>
                                                                    <input type="text" autoComplete="off" autoCorrect="off" onKeyDown={(e) => this.downArrow(e, data.code)} pattern="^-?[0-9]\d*\.?\d*$"
                                                                        onKeyPress={e => /[\+\-\.\,]$/.test(e.key) && e.preventDefault()}
                                                                        className="modal_tableBox " id={data.code} onChange={(e) => this.handleChangee(`${data.code}`, e, data)} value={data.ratio}></input>
                                                                </td>
                                                            </tr>
                                                            )) :
                                                            this.state.sizeState.map((data, key) => (
                                                            <tr key={key} style={{ cursor: "pointer" }} onClick={(e) => { this.handleRadioChange(e, data.code, data) }}>
                                                                {/* <td>
                                                                    <label className="">
                                                                        <input style={{ position: 'relative' }} type="radio" checked={this.state.checkedSizeValue.includes(`${data.code}`)} onChange={(e) => { this.handleRadioChange(e, data.code) }} />
                                                                    </label>
                                                                </td> */}
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0">
                                                                                <input type="radio" checked={this.state.checkedSizeId.includes(data.id)} onChange={(e) => { this.handleRadioChange(e, data.code, data) }} />
                                                                                <span className="checkmark1" htmlFor={data.code}></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                                <td><label>{data.ratio}</label></td>
                                                            </tr>))
                                                            }
                                                        </tbody>
                                                    </table>
                                                </div>}

                                                {selectedData && <div className="gcmt-manage">
                                                        <table className="table gcmt-inner">
                                                            <thead>
                                                                <tr>
                                                                    <th><label>Id</label></th>
                                                                    <th><label>Size</label></th>
                                                                    <th><label>{this.props.isSet ? 'Set Ratio' : 'Quantity'}</label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody className="rowPad">
                                                                {this.state.isToggled ? this.state.sizeDateValue.map((t, i) => (
                                                                    <React.Fragment>
                                                                        {/* {this.state.sizeValue.includes(`${t.cname}`) &&  */}
                                                                        <tr key={i}>
                                                                        <td className="fix-action-btn">
                                                                            <ul className="table-item-list">
                                                                                <li className="til-inner">
                                                                                    <label className="checkBoxLabel0">
                                                                                        <input type="radio" checked onClick={(e) => { this.onRemoveCheck(e, t.id, t) }} />
                                                                                        <span className="checkmark1" htmlFor={t.id}></span>
                                                                                    </label>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                            <td><label>{t.cname}</label></td>
                                                                            <td><label>{t.ratio}</label></td>

                                                                        </tr>
                                                                        {/* } */}
                                                                    </React.Fragment>
                                                                ))
                                                                    : this.state.sizeState.map((t, i) => (
                                                                        <React.Fragment>
                                                                            {this.state.checkedSizeId.includes(t.id) && <tr key={i}>
                                                                                <td><label>{i}</label></td>
                                                                                <td><label>{t.cname}</label></td>
                                                                                <td><label>{t.ratio}</label></td>

                                                                            </tr>}
                                                                        </React.Fragment>

                                                                    ))}
                                                            </tbody>
                                                        </table>
                                                </div>}
                                                <div className="col-md-12 pad-0" >
                                                    <div className="new-gen-pagination">
                                                        <div className="ngp-left">
                                                            <div className="table-page-no">
                                                                <span>Page :</span><input type="number" className="paginationBorder" value="01" />
                                                                <span className="ngp-total-item">Total Items </span> <span className="bold">10</span>
                                                                {!this.props.isSet && this.state.refPoWithIndent && !this.state.isMaintainSize && this.state.sizes[0] == "All Size" && this.state.blanceSizeQty < 0 && <span className="error">Order Qty can't be greater than Indent Qty</span>}
                                                            </div>
                                                        </div>
                                                        <div className="ngp-right">
                                                            <div className="nt-btn">
                                                                <div className="pagination-inner">
                                                                    <ul className="pagination-item">
                                                                        {this.state.current == 1 || this.state.current == 0 ? <li >
                                                                            <button className="disable-first-btn" disabled >
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li> : 
                                                                        <li>
                                                                            <button className="first-btn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li>}
                                                                        {this.state.prev != 0 ? <li >
                                                                            <button className="prev-btn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                                        <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li> : 
                                                                        <li>
                                                                            <button className="dis-prev-btn" disabled>
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li>}
                                                                        <li>
                                                                            <button className="pi-number-btn" type="button">
                                                                                <span>{this.state.current}/{this.state.maxPage}</span>
                                                                            </button>
                                                                        </li>
                                                                        {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                                            <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li> : 
                                                                        <li >
                                                                            <button className="dis-next-btn" disabled>
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li> :
                                                                        <li>
                                                                            <button className="dis-next-btn" disabled>
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li>}
                                                                        {this.props.current != 0 && this.next - 1 != this.maxPage && this.current != undefined && this.maxPage != this.current ? <li >
                                                                            <button className="last-btn" type="button" onClick={(e) => this.page(e)} id="last">
                                                                                <span className="page-item-btn-inner">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                    </svg>
                                                                                </span>
                                                                            </button>
                                                                        </li> : 
                                                                        <li >
                                                                            <button className="dis-last-btn" disabled>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                </svg>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                                </svg>
                                                                            </button>
                                                                        </li>}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {/* <div className="modal-bottom">
                                        <ul className="list-inline width_100 modal-select m-top-5">
                                            {isToggled && <React.Fragment> <li className="selectAllFocus">
                                                <label className="select_modalRadio" >SELECT ALL
                                                    <input type="radio" name="sizeRadio" id="selectedId" onKeyDown={(e) => this.selectallKeyDown(e)} onClick={(e) => this.onSelectAll(e)} />
                                                    <span className="checkradio-select select_all"></span>
                                                </label>
                                            </li>
                                            <li className="selectAllFocus">
                                                <label className="select_modalRadio" >DESELECT ALL
                                                <input type="radio" name="sizeRadio" id="deselectedId" onKeyDown={(e) => this.deselectallKeyDown(e)} onClick={(e) => this.onDeselectAll(e)} />
                                                    <span className="checkradio-select deselect_all"></span>
                                                </label>
                                            </li> </React.Fragment> }
                                        </ul>
                                    </div> */}
                                </div>
                            </div>
                            {this.state.loader ? <FilterLoader /> : null}
                            {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                            {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

                            {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                            {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

                        </div>
                    );
                }
            }

            export default PiSizeModal;
