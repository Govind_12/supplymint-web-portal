import React from 'react';
import ItemUdfMappingModal from '../purchaseOrder/itemUdfMappingModal';
import UdfMappingModal from '../purchaseOrder/udfMappingModal';
import PoError from '../loaders/poError';

export default class UdfComponet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemUdfMappingModal: false,
            UdfMappingModal: false,
            itemUdfMappingAnimation: false,
            udfMappingAnimation: false,
            itemUdfName: "",
            itemUdfType: "",
            itemValue: "",
            itemUdfId: "",
            setValue: "",
            udfType: "",
            udfName: "",
            udfRow: "",
            poError: false,
            errorMassage: "",
            itemArticleCode: "",
            ItemarticleName: "",
            focusId: "",
        }
    }
    // ____________________________openItemUdfModal___________________________

    openItemUdfModal(udfType, name, id, value, articleCode) {
        var rows = this.props.rows
        var flag = false
        let articleName = ""
        for (var i = 0; i < rows.length; i++) {
            if ((rows[i].vendorDesign == "" && !this.props.isVendorDesignNotReq) || rows[i].date == "" || rows[i].articleCode == "") {
                flag = false
            } else {
                if (rows[i].articleCode == articleCode) {
                    articleName = rows[i].articleName
                }
                flag = true
            }
        }
        if (flag) {
            let data = {
                type: 1,
                no: 1,
                search: "",
                description: "",
                itemUdfType: udfType,
                ispo: true,
                hl3Code: this.props.hl3Code,
                hl4Code: articleCode,
                hl4Name: articleName
            }
            this.props.itemUdfMappingRequest(data)
            this.setState({
                itemUdfId: id,
                focusId: udfType + id,
                itemUdfType: udfType,
                itemUdfName: name,
                itemValue: value,
                ItemarticleName: articleName,
                itemArticleCode: articleCode,
                itemUdfMappingModal: true,
                itemUdfMappingAnimation: !this.state.itemUdfMappingAnimation,
            })
        } else {
            this.validationRows()
        }
    }
    closeItemUdfModal() {
        this.setState({
            itemUdfMappingModal: false,
            itemUdfMappingAnimation: !this.state.itemUdfMappingAnimation,

        })
            document.getElementById(this.state.focusId).focus()
    }

    validationRows() {
        let rows = this.props.rows
        rows.forEach(pi => {

            if (pi.vendorDesign == "" && !this.props.isVendorDesignNotReq) {

                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor Design is compulsory"
                })

            } else if (pi.date == "") {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Select delivery date from indent description"
                })
            } else if (pi.articleCode == "") {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Vendor mrp is compulsory"
                })
            }
        })
    }
    // _________________________openUdfMappingModal_______________________________

    openUdfMappingModal(code, name, id, value) {
        var rows = this.props.rows
        var flag = false
        for (var i = 0; i < rows.length; i++) {
            if ((rows[i].vendorDesign == "" && !this.props.isVendorDesignNotReq) || rows[i].date == "" || rows[i].articleCode == "") {
                flag = false
            } else {
                flag = true
            }
        }
        if (flag) {

            let udfType = code;
            let udfName = name;
            let idd = id;
            var data = {
                no: 1,
                type: 1,
                search: "",
                udfType: udfType,
                code: "",
                name: "",
                ispo: true
            }
            this.props.udfTypeRequest(data);
            this.setState({
                focusId: udfType + id,
                setValue: value,
                udfType: udfType,
                udfName: udfName,
                udfRow: idd,
                UdfMappingModal: true,
                udfMappingAnimation: !this.state.udfMappingAnimation,
            })
        } else {
            this.validationRows()
        }
    }

    closeUdf() {
        this.setState({
            udfMapping: false,
            udfMappingAnimation: !this.state.udfMappingAnimation
        });
        document.getElementById(this.state.focusId).focus()

    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    updateUdfSetvalue() {
        this.setState({
            setValue: ""
        })
            document.getElementById(this.state.focusId).focus()
    }
    
    updateItemValue() {
        this.setState({
            itemValue: ""

        })
            document.getElementById(this.state.focusId).focus()
    }
    _handleKeyPress(e, id) {

        let idd = id;

        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }
    }
    render() {

        return (
            <div>
                <div className="col-md-12 col-sm-12 col-xs-12 pad-0">
                    <div className="col-md-12 col-sm-8 pad-0 m-top-35">
                        <ul className="list_style">
                            <li>
                                <label className="contribution_mart">
                                    SECTION 2
                                 </label>
                            </li>
                        </ul>
                    </div>
                </div>

                {this.props.itemUdfExist == "true" ? <div className="col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper oflHidden sectionOneTable bordere3e7f3 poTableMain">
                    <div className="zui-scroller pad-0 m0">
                        <table className="table scrollTable table scrollTable main-table zui-table border-bot-table displayTable" >
                            <thead>
                                <tr>
                                    {!this.props.isVendorDesignNotReq ? <th >
                                        <label> Vendor Design  </label>
                                    </th> : null}
                                    <th >
                                        <label> Article Code  </label>
                                    </th>
                                    <th >
                                        <label> Delivery Date  </label>
                                    </th>
                                    {this.props.udfItemHeadData.length != 0 ? this.props.udfItemHeadData.map((data, keyy) => (
                                        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.isLovPO == "Y" ? <th key={keyy} >
                                            <label> {data.displayName != null ? data.displayName : data.cat_desc_udf} </label>
                                            {/*{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null}*/}
                                        </th> : null
                                            : ((data.cat_desc_udf == "UDFSTRING01" && (this.props.itemudf1Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING02" && (this.props.itemudf2Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING03" && (this.props.itemudf3Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING04" && (this.props.itemudf4Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING05" && (this.props.itemudf5Validation == true || data.isLovPO == "Y")) ||
                                                (data.cat_desc_udf == "UDFSTRING06" && (this.props.itemudf6Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING07" && (this.props.itemudf7Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING08" && (this.props.itemudf8Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING09" && (this.props.itemudf9Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFSTRING10" && (this.props.itemudf10Validation == true || data.isLovPO == "Y")) ||
                                                (data.cat_desc_udf == "UDFNUM01" && (this.props.itemudf11Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM02" && (this.props.itemudf12Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM03" && (this.props.itemudf13Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM04" && (this.props.itemudf14Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFNUM05" && (this.props.itemudf15Validation == true || data.isLovPO == "Y")) ||
                                                (data.cat_desc_udf == "UDFDATE01" && (this.props.itemudf16Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE02" && (this.props.itemudf17Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE03" && (this.props.itemudf18Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE04" && (this.props.itemudf19Validation == true || data.isLovPO == "Y")) || (data.cat_desc_udf == "UDFDATE05" && (this.props.itemudf20Validation == true || data.isLovPO == "Y"))) ?
                                                <th key={keyy} >
                                                    <label> {data.displayName != null ? data.displayName : data.cat_desc_udf} </label>
                                                    {/*{data.isCompulsoryPO == "Y" ? <span className="mandatory">*</span> : null}*/}
                                                </th> : null)) : null}

                                </tr>
                            </thead>

                            <tbody>
                                {this.props.itemDetails.length != 0 ? this.props.itemDetails.map((data, key) => (
                                    <tr id={key} key={key}>
                                        {!this.props.isVendorDesignNotReq ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input autoComplete="off" type="text" readOnly name="vendordesign" className="inputTable " value={data.vendorDesign} />

                                        </td> : null}
                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input autoComplete="off" type="text" readOnly name="article" className="inputTable " value={data.articleCode} />

                                        </td>
                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input type="text" className="inputTable " readOnly placeholder="" id="date" name="date" value={data.deliveryDate} />

                                        </td>
                                        {data.udfList != undefined ? data.udfList.map((dataa, keyy) => (
                                            sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? dataa.isLovPO == "Y" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={keyy} >
                                                <input autoComplete="off" type="text" name="" className="inputTable " value={dataa.value} onKeyDown={(e) => this._handleKeyPress(e, dataa.cat_desc_udf + data.itemGridId)} id={dataa.cat_desc_udf + data.itemGridId} onClick={(e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemGridId}`, `${dataa.value}`, `${data.articleCode}`)} />
                                                <div className="purchaseTableDiv" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                        <g fill="none" fillRule="evenodd">
                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                        </g>
                                                    </svg>

                                                </div>

                                            </td> : null :
                                                ((dataa.cat_desc_udf == "UDFSTRING01" && (this.props.itemudf1Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING02" && (this.props.itemudf2Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING03" && (this.props.itemudf3Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING04" && (this.props.itemudf4Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING05" && (this.props.itemudf5Validation == true || dataa.isLovPO == "Y")) ||
                                                    (dataa.cat_desc_udf == "UDFSTRING06" && (this.props.itemudf6Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING07" && (this.props.itemudf7Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING08" && (this.props.itemudf8Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING09" && (this.props.itemudf9Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFSTRING10" && (this.props.itemudf10Validation == true || dataa.isLovPO == "Y")) ||
                                                    (dataa.cat_desc_udf == "UDFNUM01" && (this.props.itemudf11Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM02" && (this.props.itemudf12Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM03" && (this.props.itemudf13Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM04" && (this.props.itemudf14Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFNUM05" && (this.props.itemudf15Validation == true || dataa.isLovPO == "Y")) ||
                                                    (dataa.cat_desc_udf == "UDFDATE01" && (this.props.itemudf16Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE02" && (this.props.itemudf17Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE03" && (this.props.itemudf18Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE04" && (this.props.itemudf19Validation == true || dataa.isLovPO == "Y")) || (dataa.cat_desc_udf == "UDFDATE05" && (this.props.itemudf20Validation == true || dataa.isLovPO == "Y"))) ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={keyy} >
                                                        <input autoComplete="off" type="text" name="" className="inputTable " value={dataa.value} onKeyDown={(e) => this._handleKeyPress(e, dataa.cat_desc_udf + data.itemGridId)} id={dataa.cat_desc_udf + data.itemGridId} onClick={(e) => this.openItemUdfModal(`${dataa.cat_desc_udf}`, `${dataa.displayName}`, `${data.itemGridId}`, `${dataa.value}`, `${data.articleCode}`)} />
                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>

                                                        </div>

                                                    </td> : null)) : null}
                                    </tr>
                                )) : <tr className="modalTableNoData genericNodata"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                            </tbody>

                        </table>
                    </div>
                </div> : null}
                {this.props.isUDFExist == "true" ? <div className="col-md-12 col-sm-12 pad-0 m-top-20 tableWidthAuto sectionOneTable dynamicTable poTableMain">
                    <div className="scrollableTableFixed">
                        <table className="table scrollTable border-bot-table">
                            <thead>

                                <tr>
                                    {!this.props.isVendorDesignNotReq ? <th >
                                        <label> Vendor Design  </label>
                                    </th> : null}
                                    <th >
                                        <label> Article Code  </label>
                                    </th>
                                    <th >
                                        <label> Delivery Date  </label>
                                    </th>

                                    {this.props.setUdfRowsData.length != 0 ? this.props.setUdfRowsData.map((data, keyyy) => (
                                        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.isLovPO == "Y" ? <th id={keyyy} key={keyyy} >
                                            <label>
                                                {data.displayName != null ? data.displayName : data.udfType}
                                                {/*{data.isCompulsary == "Y" ? <span className="mandatory">*</span> : null}*/}
                                            </label>
                                        </th> : null :
                                            ((data.udfType == "SMUDFSTRING01" && (this.props.udf1Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING02" && (this.props.udf2Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING03" && (this.props.udf3Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING04" && (this.props.udf4Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING05" && (this.props.udf5Validation == true || data.isLovPO == "Y")) ||
                                                (data.udfType == "SMUDFSTRING06" && (this.props.udf6Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING07" && (this.props.udf7Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING08" && (this.props.udf8Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING09" && (this.props.udf9Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRIN10" && (this.props.udf10Validation == true || data.isLovPO == "Y")) ||
                                                (data.udfType == "SMUDFNUM01" && (this.props.udf11Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM02" && (this.props.udf12Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM03" && (this.props.udf13Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM04" && (this.props.udf14Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM05" && (this.props.udf15Validation == true || data.isLovPO == "Y")) ||
                                                (data.udfType == "SMUDFDATE01" && (this.props.udf16Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE02" && (this.props.udf17Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE03" && (this.props.udf18Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE04" && (this.props.udf19Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE05" && (this.props.udf20Validation == true || data.isLovPO == "Y"))) ?
                                                <th id={keyyy} key={keyyy} >
                                                    <label>
                                                        {data.displayName != null ? data.displayName : data.udfType}
                                                        {/*{data.isCompulsary == "Y" ? <span className="mandatory">*</span> : null}*/}
                                                    </label>
                                                </th> : null
                                    )) : null}
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.itemDetails.length != 0 ? this.props.itemDetails.map((udfGrid, idx) => (
                                    <tr id={idx} key={idx} >
                                        {!this.props.isVendorDesignNotReq ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input autoComplete="off" readOnly type="text" name="vendordesign" className="inputTable " value={udfGrid.vendorDesign} />

                                        </td> : null}
                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input autoComplete="off" readOnly type="text" name="article" className="inputTable " value={udfGrid.articleCode} />

                                        </td>
                                        <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >
                                            <input type="text" readOnly className="inputTable" placeholder="" id="date" name="date" value={udfGrid.deliveryDate} />
                                        </td>

                                        {udfGrid.setUdfList != undefined ? udfGrid.setUdfList.map((data, kkey) => (
                                            sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? data.isLovPO == "Y" ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={kkey} >
                                                <input autoComplete="off" type="text" name="" className="inputTable " value={data.value} id={data.udfType + udfGrid.itemGridId} onKeyDown={(e) => this._handleKeyPress(e, data.udfType + udfGrid.itemGridId)} onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.itemGridId}`, `${data.value}`)} />
                                                <div className="purchaseTableDiv" onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.itemGridId}`, `${data.value}`)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                        <g fill="none" fillRule="evenodd">
                                                            <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                            <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                        </g>
                                                    </svg>

                                                </div>

                                            </td> : null :
                                                ((data.udfType == "SMUDFSTRING01" && (this.props.udf1Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING02" && (this.props.udf2Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING03" && (this.props.udf3Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING04" && (this.props.udf4Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING05" && (this.props.udf5Validation == true || data.isLovPO == "Y")) ||
                                                    (data.udfType == "SMUDFSTRING06" && (this.props.udf6Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING07" && (this.props.udf7Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING08" && (this.props.udf8Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRING09" && (this.props.udf9Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFSTRIN10" && (this.props.udf10Validation == true || data.isLovPO == "Y")) ||
                                                    (data.udfType == "SMUDFNUM01" && (this.props.udf11Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM02" && (this.props.udf12Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM03" && (this.props.udf13Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM04" && (this.props.udf14Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFNUM05" && (this.props.udf15Validation == true || data.isLovPO == "Y")) ||
                                                    (data.udfType == "SMUDFDATE01" && (this.props.udf16Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE02" && (this.props.udf17Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE03" && (this.props.udf18Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE04" && (this.props.udf19Validation == true || data.isLovPO == "Y")) || (data.udfType == "SMUDFDATE05" && (this.props.udf20Validation == true || data.isLovPO == "Y"))) ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" key={kkey} >
                                                        <input autoComplete="off" type="text" name="" className="inputTable " value={data.value} id ={data.udfType + udfGrid.itemGridId} onKeyDown={(e) => this._handleKeyPress(e, data.udfType + udfGrid.itemGridId)} onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.itemGridId}`, `${data.value}`)} />

                                                        <div className="purchaseTableDiv" onClick={(e) => this.openUdfMappingModal(`${data.udfType}`, `${data.displayName}`, `${udfGrid.itemGridId}`, `${data.value}`)}>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>

                                                        </div>

                                                    </td> : null)) : null}
                                    </tr>
                                )) : <tr className="modalTableNoData genericNodata"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                            </tbody>

                        </table>
                    </div>
                </div> : null}
                {this.state.itemUdfMappingModal ? <ItemUdfMappingModal {...this.props} departmentCode={this.props.departmentCode} itemUdfType={this.state.itemUdfType} updateItemValue={(e) => this.updateItemValue(e)} itemUdfMappingAnimation={this.state.itemUdfMappingAnimation} itemUdfMappingModal={this.state.itemUdfMappingModal} itemUdfId={this.state.itemUdfId} itemValue={this.state.itemValue} itemUdfName={this.state.itemUdfName} closeItemUdfModal={(e) => this.closeItemUdfModal()} itemArticleCode={this.state.itemArticleCode} articleName={this.state.ItemarticleName} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.UdfMappingModal ? <UdfMappingModal {...this.props} {...this.state} departmentCode={this.props.departmentCode} udfType={this.state.udfType} udfName={this.state.udfName} updateUdfSetvalue={(e) => this.updateUdfSetvalue(e)} setValue={this.state.setValue} udfRow={this.state.udfRow} udfMappingAnimation={this.state.udfMappingAnimation} closeUdfMappingModal={(e) => this.openUdfMappingModal(e)} closeUdf={(e) => this.closeUdf(e)} /> : null}
            </div>
        )
    }
}