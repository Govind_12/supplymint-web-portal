import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";

import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";

class PiColorModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef()
        this.state = {            
            isCross: false,
            focusedLi: "",
            checked: false,
            colorState: this.props.colorState,
            loader: false,
            success: false,
            alert: false,
            addNew: true,
            save: false,
            code: "",
            catSix: "",
            codeerr: false,
            catSixerr: false,
            coData: [],
            colorCode: this.props.colorCode,
            colorRow: this.props.colorRow,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            search: "",
            toastMsg: "",
            toastLoader: false,

            sessionState: [],
            value: this.props.colorValue,
            errorMassage: "",
            poErrorMsg: false,
            colorList: this.props.colorListValue,
            checkBoxEnter: "",
            searchBy: "startWith",
            modalDisplay: false,
            selectedData: false,
            selectedColor: ""

        }

    }

    componentDidMount() {
        // if (this.props.isModalShow) {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
        // }

    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    onAddNew(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: false,
                save: true,
                search: "",

            })
        //   this.onsearchClear();


    }

    onClear() {
        this.setState({
            code: "",
            catOne: ""
        })
    }
    onsearchClear() {

        if (this.state.type == 3) {

            let data = {
                type: "",
                no: 1,
                // code: this.props.colorCode,
                id: this.props.colorRow,
                search: "",
                hl3Name: this.props.department,
                hl3Code: this.props.hl3Code,

                itemType: 'color',
                searchBy: this.state.searchBy
            }
            this.props.colorRequest(data)
        }
        this.setState(
            {
                search: "",
                type: "",
                no: 1,
                isCross: false,
            })
        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null
        this.textInput.current.focus();

    }
    onSearch(e) {


        if (this.state.search == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true,
                // type=""
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
            document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
            document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null
        }
        else {
            this.setState({
                type: 3,

            })
            let data = {
                type: 3,
                no: 1,
                // code: this.props.colorCode,
                id: this.props.colorRow,
                search: this.state.search,
                hl3Name: this.props.department,
                hl3Code: this.props.hl3Code,

                itemType: 'color',
                searchBy: this.state.searchBy
            }
            this.props.colorRequest(data)
        }

        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null
    }


    onCheckbox(id, code) {
        var le = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
        let values = this.props.colorType == "simple" ? [] : this.state.value
        let colorList = this.state.colorList
        this.state.clr = id;
        var colorIndex = ""


        if (values.includes(id)) {
            var index = values.indexOf(id);

            for (var j = 0; j < colorList.length; j++) {
                if (colorList[j].cname == id) {
                    colorIndex = j
                }
            }

            if (index > -1) {
                values.splice(index, 1);
                this.setState({
                    value: values
                })

            }
            if (colorIndex > -1) {
                colorList.splice(colorIndex, 1);
                this.setState({
                    colorList: colorList
                }, () => {
                    // if(!this.props.isModalShow){
                    //     this.doneSave()

                    // }
                })
            }
        } else {
            values.push(id)
            let payload = {
                code: code,
                cname: id,

            }
            colorList.push(payload)

            this.setState({
                value: values,
                colorList: colorList
            }, () => {
                // if(!this.props.isModalShow){
                //     this.doneSave()

                // }
            })

        }

        if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
            const s = this
            setTimeout(function () {
                s.setState({
                    coData: le
                })
            }, 10)
        } else {
            const s = this
            setTimeout(function () {
                s.setState({
                    colorState: le
                })
            }, 10)
        }
        let array = this.state.coData == "" ? this.state.colorState : this.state.coData;
        for (let i = 0; i < array.length; i++) {
            let cArray = []
            array.forEach(a => {
                cArray.push(a.cname)
            });
            if (cArray.length == this.state.value.length) {

                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = true : null

            } else if (cArray.length != this.state.value.length) {

                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null
                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

            }

        }

        var session = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
        if (session != null) {


            sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(session))
            this.setState({
                sessionState: session
            })
        }
    }


    componentWillMount() {

        this.setState({
            colorRow: this.props.colorRow,
            colorCode: this.props.colorCode,
            // search:this.props.isModalShow ? "" : this.props.colorSearch,
            // type: this.props.isModalShow || this.props.colorSearch == "" ? 1 : 3
            search: "",
            type: 1
        })

        if (this.props.purchaseIndent.color.isSuccess) {
            if (this.props.purchaseIndent.color.data.resource != null) {
                this.setState({
                    colorState: this.props.purchaseIndent.color.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? this.props.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: this.props.purchaseIndent.color.data.prePage,
                    current: this.props.purchaseIndent.color.data.currPage,
                    next: this.props.purchaseIndent.color.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.color.data.maxPage
                })
            }
            else {
                this.setState({
                    colorState: this.props.purchaseIndent.color.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? this.props.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })
            }
        }

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            colorRow: this.props.colorRow,
            colorCode: this.props.colorCode,
            // value: this.props.colorValue
        })
        if (nextProps.purchaseIndent.color.isSuccess) {
            if (nextProps.purchaseIndent.color.data.resource != null) {
                this.setState({
                    colorState: nextProps.purchaseIndent.color.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? nextProps.purchaseIndent.color.data.resource == null ? JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color")) : nextProps.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: nextProps.purchaseIndent.color.data.prePage,
                    current: nextProps.purchaseIndent.color.data.currPage,
                    next: nextProps.purchaseIndent.color.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.color.data.maxPage,

                })
            }
            else {
                this.setState({
                    colorState: nextProps.purchaseIndent.color.data.resource,
                    coData: sessionStorage.getItem(this.props.colorCode + '-' + "color") != null ? nextProps.purchaseIndent.color.data.resource == null ? JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color")) : nextProps.purchaseIndent.color.data.resource.concat(JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))) : "",
                    id: this.props.colorRow,
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,

                })
            }

            if (window.screen.width > 1200) {
                // if (this.props.isModalShow) {
                this.textInput.current.focus();


                // }
                // else if (!this.props.isModalShow) {
                //     if (nextProps.purchaseIndent.color.data.resource != null) {
                //         this.setState({
                //             focusedLi: nextProps.purchaseIndent.color.data.resource[0].cname,
                //             search:  this.props.colorSearch,
                //             type:  this.props.colorSearch == "" ? 1 : 3

                //         })
                //         document.getElementById(nextProps.purchaseIndent.color.data.resource[0].cname) != null ? document.getElementById(nextProps.purchaseIndent.color.data.resource[0].cname).focus() : null
                //     }



                // }
            }
            this.setState({
                modalDisplay: true,

            })
        }
        if (nextProps.purchaseIndent.piAddNew.isSuccess) {
            this.onFinalSave();
            this.props.piAddNewRequest();
        }
    }

    selectall(e) {

        // if(!e.key=="ArrowUp" || !e.key=="ArrowDown" || !e.key == "ArrowRight" || !e.key =="ArrowLeft"){
        var c = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
        let values = this.state.value
        let colorList = this.state.colorList

        if (c == null) {


            document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = true : null

        } else {
            for (var i = 0; i < c.length; i++) {
                if (!values.includes(c[i].cname)) {
                    values.push(c[i].cname)
                    let payload = {
                        code: c[i].code,
                        cname: c[i].cname,
                        id: this.props.colorRow
                    }
                    colorList.push(payload)
                }
            }
            this.setState({
                value: values,
                colorList: colorList

            })

            if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
                const s = this
                setTimeout(function () {
                    s.setState({
                        coData: c
                    })
                }, 10)
            } else {

                const s = this
                setTimeout(function () {
                    s.setState({
                        colorState: c
                    })
                }, 10)

            }
            var session = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
            if (session != null) {

                sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(session))
                this.setState({
                    sessionState: session
                })
            }
        }

        document.getElementById('selectAll') != null ? document.getElementById('selectAll').focus() : null
        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = true : null

        // }
    }
    deselectall(e) {

        // if(!e.key=="ArrowUp" || !e.key=="ArrowDown" || !e.key == "ArrowRight" || !e.key =="ArrowLeft"){
        var c = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
        var value = this.state.value
        let colorList = this.state.colorList

        if (c == null) {

            document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

        } else {


            // for (let j = 0; j < c.length; j++) {
            //     for (var i = 0; i < value.length; i++) {
            //         if (c[j].cname == value[i] || c[j].cname != value[i])
            //             value.splice(i)
            //     }

            // }

            // for (let j = 0; j < c.length; j++) {
            //     for (var i = 0; i < colorList.length; i++) {
            //         if (c[j].cname == colorList[i].cname || c[j].cname != colorList[i].cname)
            //             colorList.splice(i)
            //     }
            // }
            this.setState({
                value: [],
                colorList: [],
            }, () => {
                let data = {
                    value: this.state.value,
                    colorList: this.state.colorList
                }

                this.props.deselectallProp(data)
            })

            if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
                const s = this
                setTimeout(function () {
                    s.setState({
                        coData: c
                    })
                }, 10)
            } else {
                const s = this
                setTimeout(function () {
                    s.setState({
                        colorState: c
                    })
                }, 10)

            }

            var session = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
            if (session != null) {


                sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(session))
                this.setState({
                    sessionState: session
                })
            }

        }


        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').focus() : null

        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = true : null

        // }

    }


    code() {

        if (
            this.state.code == "" || this.state.code.trim() == "") {
            this.setState({
                codeerr: true
            });
        } else {
            this.setState({
                codeerr: false
            });
        }

    }
    catSix() {

        if (
            this.state.catSix == "" || this.state.catSix.trim() == "") {
            this.setState({
                catSixerr: true
            });
        } else {
            this.setState({
                catSixerr: false
            });
        }

    }


    handleChange(e) {
        if (e.target.id == "code") {
            if (e.target.validity.valid) {
                this.setState(
                    {
                        code: e.target.value
                    },
                    () => {
                        this.code();
                    }

                );
            }
        } else if (e.target.id == "catSix") {

            this.setState(
                {
                    catSix: e.target.value
                },
                () => {
                    this.catSix();
                }

            );
        } else if (e.target.id == "colorSearch") {

            this.setState(
                {
                    search: e.target.value
                }
            );
            if (e.target.value != '') {
                this.setState({
                    isCross: true,
                })
            }
            else {
                this.setState({
                    isCross: false,
                })
            }

        } else if (e.target.id == "searchBycolor") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.search != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        itemType: 'color',
                        id: this.props.colorRow,
                        search: this.state.search,
                        hl3Name: this.props.department,
                        hl3Code: this.props.hl3Code,
                        searchBy: this.state.searchBy
                    }
                    this.props.colorRequest(data)
                }
            })
        }


    }

    onSave(e) {

        this.code();
        this.catSix();

        e.preventDefault();

        const t = this;
        setTimeout(function () {


            const { code, codeerr, catSix, catSixerr } = t.state;

            if (!codeerr && !catSixerr) {

                let saveData = {
                    isExist: "COLOR",
                    hl4Code: t.props.colorCode,
                    code: code,
                    categoryName: catSix,

                    hl1Name: "",
                    hl2Name: "",

                    hl3Name: ""
                }
                t.props.piAddNewRequest(saveData);

            }
        }, 10)

    }
    onFinalSave() {
        let idd = 0
        const t = this;
        var colorArray = []
        let r = JSON.parse(sessionStorage.getItem(t.props.colorCode + '-' + "color"))
        var colorCat = []
        if (r != null) {
            for (var i = 0; i < r.length; i++) {
                colorCat.push(r[i].cname.replace(/\s+/g, ""))
            }
        }
        if (colorCat.includes(t.state.catSix.replace(/\s+/g, ""))) {

            t.setState({
                errorMassage: "CAT6 already exist",
                poErrorMsg: true
            })

        }

        else {

            if (sessionStorage.getItem(t.props.colorCode + '-' + "color") != null) {

                colorArray = JSON.parse(sessionStorage.getItem(t.props.colorCode + '-' + "color"))

                let c = {

                    code: t.state.code,

                    cname: t.state.catSix,
                    hl3Name: this.props.hl3Name
                }

                colorArray.push(c)
                sessionStorage.setItem(t.props.colorCode + '-' + "color", JSON.stringify(colorArray));

                t.setState({
                    coData: t.state.colorState == null ? colorArray : t.state.colorState.concat(colorArray),
                    toastMsg: "Cat6 added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)
                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null


            } else {

                let c = {

                    code: t.state.code,

                    cname: t.state.catSix,

                    hl3Name: this.props.hl3Name
                }

                colorArray.push(c)
                sessionStorage.setItem(t.props.colorCode + '-' + "color", JSON.stringify(colorArray));

                t.setState({
                    coData: t.state.colorState == null ? colorArray : t.state.colorState.concat(colorArray),
                    toastMsg: "Cat6 added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)
                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

            }
        }
        t.setState(
            {
                addNew: true,
                save: false,

            }
        )
        t.onClear();

    }
    onClear() {
        this.setState({
            code: "",
            catSix: ""
        })

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.color.data.prePage,
                current: this.props.purchaseIndent.color.data.currPage,
                next: this.props.purchaseIndent.color.data.currPage + 1,
                maxPage: this.props.purchaseIndent.color.data.maxPage,

            })
            if (this.props.purchaseIndent.color.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.color.data.currPage - 1,
                    itemType: 'color',
                    id: this.props.colorRow,
                    search: this.state.search,
                    hl3Name: this.props.department,
                    hl3Code: this.props.hl3Code,
                    searchBy: this.state.searchBy
                }
                this.props.colorRequest(data);

                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

            }
        } else if (e.target.id == "next") {

            this.setState({
                prev: this.props.purchaseIndent.color.data.prePage,
                current: this.props.purchaseIndent.color.data.currPage,
                next: this.props.purchaseIndent.color.data.currPage + 1,
                maxPage: this.props.purchaseIndent.color.data.maxPage,

            })
            if (this.props.purchaseIndent.color.data.currPage != this.props.purchaseIndent.color.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.color.data.currPage + 1,
                    itemType: 'color',
                    id: this.props.colorRow,
                    search: this.state.search,
                    hl3Name: this.props.department,
                    hl3Code: this.props.hl3Code,
                    searchBy: this.state.searchBy
                }

                this.props.colorRequest(data)

            }

            document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

            document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

        } else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.color.data.prePage,
                current: this.props.purchaseIndent.color.data.currPage,
                next: this.props.purchaseIndent.color.data.currPage + 1,
                maxPage: this.props.purchaseIndent.color.data.maxPage,

            })
            if (this.props.purchaseIndent.color.data.currPage <= this.props.purchaseIndent.color.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    itemType: 'color',
                    id: this.props.colorRow,
                    search: this.state.search,
                    hl3Name: this.props.department,
                    hl3Code: this.props.hl3Code,
                    searchBy: this.state.searchBy
                }
                this.props.colorRequest(data)
            }

        }

        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

    }

    onCancel(e) {
        e.preventDefault();
        this.setState(
            {
                addNew: true,
                save: false,
                code: "",
                catSix: ""

            }
        )
    }
    doneSave() {
        let colorList = this.state.colorList;
        let value = this.state.value

        if (this.state.value.length != 0) {

            let finalColorData = {
                colorData: value,
                colorrId: this.props.colorRow,
                section3ColorId: this.props.section3ColorId
            }            
            let payload = {
                colorList: this.state.colorList,
                colorrId: this.props.colorRow,
            }

            this.props.updateColorState(finalColorData);
            this.props.updateColorList(payload);

        }
    }
    ondone() {
        // if(this.state.value.length == 0){
        //     this.props.handleColor
        // }
        let colorList = this.state.colorList;
        console.log('cl1', colorList)
        if(this.props.colorType == "simple"){
            let color = colorList.filter(item=>{
                if(item.cname == this.state.clr){
                    return item
                }else{
                    return
                }
            })
            colorList = color;
            console.log('cl2', color)
        }
        let value = this.state.value

        if (this.state.value.length != 0 || this.state.selectedColor.length !== 0) {

            let finalColorData = {
                colorData: value.length == 0 ? this.state.selectedColor : value,
                colorrId: this.props.colorRow,
                section3ColorId: this.props.section3ColorId
            }
            console.log('colorData', value.length == 0 ? this.state.selectedColor : value,)
            let payload = {
                colorList: colorList,
                colorrId: this.props.colorRow,
            }
            this.props.updateColorState(finalColorData);
            this.props.updateColorList(payload);

            this.setState({
                addNew: true,
                save: false,
                search: "",
                codeerr: false,
                catSixerr: false,
                codata: "",
                // colorState:"",
                code: "",
                catSix: "",
                type: "",
                prev: 0,
                current: 0,
                next: 0,
                maxPage: 0,
                coData: [],
                colorState: []
            })
            this.props.closePiColorModal()
        } else {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }

        console.log('Colorrow1', this.props.poRows)
        if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
            let s = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
            for (var i = 0; i < s.length; i++) {

                s[i].ratio = ""

            }
            sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(s))

        }
        console.log('Colorrow2', this.props.poRows)
        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
        console.log('Colorrow3', this.props.poRows)
    }
    onClose(e) {
        let s = this.state.coData == "" ? this.state.colorState : this.state.coData;
        debugger
        this.props.closePiColorModal()
        console.log('closeRow', this.props.poRows)
        // this.props.colorClear() 
        this.setState({
            addNew: true,
            save: false,
            search: "",
            codeerr: false,
            catSixerr: false,
            codata: "",
            // colorState:"",
            code: "",
            catSix: "",
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            coData: [],
            colorState: []
        })
        if (sessionStorage.getItem(this.props.colorCode + '-' + "color") != null) {
            let s = JSON.parse(sessionStorage.getItem(this.props.colorCode + '-' + "color"))
            for (var i = 0; i < s.length; i++) {

                s[i].ratio = ""

            }
            sessionStorage.setItem(this.props.colorCode + '-' + "color", JSON.stringify(s))

        }
        document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').checked = false : null

        document.getElementById('selectAll') != null ? document.getElementById('selectAll').checked = false : null

        if(this.props.colorType == "simple"){
            let clr = this.props.poRows[this.props.selectedRowId -1].lineItem[this.props.colorRow-1].color;
            console.log('lastcolor', clr, this.props.colorRow, this.props.poRows);
            let colorList = this.state.colorList;
            let color = colorList.filter(item=>{
                if(item.cname == clr){
                    return item
                }else{
                    return
                }
            })            
            console.log('lastcolor2', color, this.props.poRows)
            let payload = {
                colorList: color,
                colorrId: this.props.colorRow,
            } 
            let finalColorData = {
                colorData: clr,
                colorrId: this.props.colorRow,
                section3ColorId: this.props.section3ColorId
            }
            console.log('colPay', payload, 'colFinal', finalColorData, 'row', this.props.poRows)
            this.props.updateColorState(finalColorData);           
            this.props.updateColorList(payload);
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            if (e.target.value != "") {
                this.onSearch();
            }
        }
    }



    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {

                var le = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
                if (le != null) {
                    let cname = le[0].cname
                    let code = le[0].code
                    this.onCheckbox(cname, code)
                    window.setTimeout(function () {
                        document.getElementById("colorSearch").blur()
                        document.getElementById(cname).focus()
                    }, 0)
                }
                else {
                    window.setTimeout(function () {
                        document.getElementById("colorSearch").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            }
            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("colorSearch").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if (e.key == "ArrowDown") {

            var le = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
            if (le != null) {
                let cname = le[0].cname
                let code = le[0].code
                this.onCheckbox(cname, code)
                window.setTimeout(function () {
                    document.getElementById("colorSearch").blur()
                    document.getElementById(cname).focus()
                }, 0)
            }
            else {
                window.setTimeout(function () {
                    document.getElementById("colorSearch").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }




        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {

            window.setTimeout(function () {

                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e, cname) {

        let code = ""
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("selectAll").focus()
            }, 0)
        }
        if (e.key === "ArrowDown") {
            let index = 0
            var le = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
            for (let i = 0; i < le.length; i++) {
                if (le[i].cname == cname) {
                    index = i
                }
            }

            if (index < le.length - 1) {

                this.setState({
                    checkBoxEnter: le[index + 1].cname
                })
                code = le[index + 1].code

                document.getElementById(le[index + 1].cname).focus()
            }
        }
        if (e.key === "ArrowUp") {
            let index = 0
            var le = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
            for (let i = 0; i < le.length; i++) {
                if (le[i].cname == cname) {
                    index = i
                }
            }
            if (index > 0) {

                this.setState({
                    checkBoxEnter: le[index - 1].cname
                })
                code = le[index - 1].code

                document.getElementById(le[index - 1].cname).focus()
            }
        }
        if (e.key === "Enter") {
            if (this.state.checkBoxEnter != "") {
                this.onCheckbox(this.state.checkBoxEnter, code)
            }

        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.ondone();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {
            this.onClose();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                // this.props.isModalShow ? 
                document.getElementById("colorSearch").focus()
                //: this.state.colorState.length != 0 ?

                //     this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus()

                //     : null
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            var le = sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState : this.state.coData
            if (le != null) {
                let cname = le[0].cname
                let code = le[0].code
                this.onCheckbox(cname, code)
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(cname).focus()
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }

    selectallKeyDown(e) {
        if (e.key == "Tab") {
            window.setTimeout(function () {
                document.getElementById('deselectAll') != null ? document.getElementById('deselectAll').focus() : null

                document.getElementById('selectAll') != null ? document.getElementById('selectAll').blur() : null



            }, 0)
        }
        else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
            e.preventDefault()
        }
        else if (e.key == "Enter") {
            this.selectall()
        }
    }
    deselectallKeyDown(e) {
        if (e.key == "Tab") {
            window.setTimeout(function () {

                document.getElementById("deselectAll").blur()
                document.getElementById("doneButton").focus()

            }, 0)
        }
        else if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
            e.preventDefault()
        }
        else if (e.key == "Enter") {
            this.deselectall()
        }
    }



    selectLi(e, code) {
        let colorState = this.state.colorState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < colorState.length; i++) {
                if (colorState[i].cname == code) {
                    index = i
                }
            }
            if (index < colorState.length - 1 || index == 0) {
                document.getElementById(colorState[index + 1].cname) != null ? document.getElementById(colorState[index + 1].cname).focus() : null

                this.setState({
                    focusedLi: colorState[index + 1].cname
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < colorState.length; i++) {
                if (colorState[i].cname == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(colorState[index - 1].cname) != null ? document.getElementById(colorState[index - 1].cname).focus() : null

                this.setState({
                    focusedLi: colorState[index - 1].cname
                })

            }
        }
        // if (e.which === 13) {
        //     this.onCheckbox(code)
        // }
        if (e.which === 9) {
            document.getElementById("doneButton").focus()


        }
        if (e.key == "Escape") {
            this.onClose(e)
        }


    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.colorState.length != 0 ?
                            document.getElementById(this.state.colorState[0].cname).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.colorState.length != 0) {
                    document.getElementById(this.state.colorState[0].cname).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.onClose(e)
        }


    }

    changeSelectedData = (event) => {
        this.setState({
            selectedData: !this.state.selectedData
        })
    }
    handleRadioCheck = (color) => {
        this.setState({
            selectedColor: color
        })
    }
    removeSelected = (cname) => {
        let colorList = [...this.state.colorList]
        let value = [...this.state.value]
        let index = colorList.findIndex((_) => _.cname == cname)
        let vIndex = value.findIndex((_) => _ == cname)
        colorList.splice(index, 1)
        value.splice(vIndex, 1)
        this.setState({ colorList, value })
    }

    render() {        
        console.log('rowId', this.props.selectedRowId, this.props.poRows)
        if (this.state.focusedLi != "" && this.props.colorSearch == this.state.search) {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }
        //open modal for dropdown code start here
        // if (this.props.colorPosId != "" && this.props.colorPosId != undefined && !this.props.isModalShow) {
        //     let modalWidth = 500;
        //     let modalWindowWidth = document.getElementById('itemSetModal').getBoundingClientRect();
        //     let position = document.getElementById(this.props.colorPosId).getBoundingClientRect();
        //     let leftPosition = position.left > 16 ? position.left - 16 : position.left;
        //     // let top = 214;
        //     let idNo = parseInt(this.props.colorPosId.replace(/\D/g, '')) + 1;
        //     let top = 57 + (35 * idNo);

        //     let newLeft = 0;
        //     let diff = modalWindowWidth.width - leftPosition;

        //     if (diff >= modalWidth) {
        //         newLeft = leftPosition > 140 ? leftPosition - 140 : leftPosition;
        //     }
        //     else {
        //         let removeWidth = modalWidth - diff;
        //         newLeft = leftPosition - removeWidth - 5;
        //     }
        //     console.log("top:" + top, "leftPosition:" + leftPosition, "newLeft:" + newLeft)

        //     $('#piColorModalPosition').css({ 'left': newLeft, 'top': top });
        //                 $('.poArticleModalPosition').removeClass('hideSmoothly');

        // }
        //open modal for dropdown code end here


        const { code, codeerr, catSix, catSixerr, search, selectedData } = this.state;

        return (

            // this.props.isModalShow ? 
            <div className={this.props.colorModalAnimation ? "modal" : "display_none"} id="pocolorModel">
                <div className={this.props.colorModalAnimation ? "backdrop modal-backdrop-new" : "display_none"}></div>
                <div className={this.props.colorModalAnimation ? "" : "display_none"}>
                    <div className={this.props.colorModalAnimation ? "modal-content modalpoColor modalShow gen-color-modal" : "modalHide"}>
                        <div className="gcm-head">
                            <div className="gcmh-top">
                                <div className="gcmht-left">
                                    <h3>Select Colour</h3>
                                    <p>You can select multiple colors from below records</p>
                                </div>
                                <div className="gcmht-right">
                                    <button type="button" className="gcmht-close" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.onClose(e)}>Close</button>
                                    <button type="button" className="gcmht-done" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.ondone(e)}>Done</button>
                                </div>
                            </div>
                            <div className="gcmh-bottom">
                                <div className="gcmhb-left">
                                    {this.state.addNew ? <div className="gcmhb-search">
                                        <input type="search" autoComplete="off" autoCorrect="off" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} value={search} id="colorSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                        <button className="search-image" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.094 17.231">
                                                <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" />
                                            </svg>
                                        </button>
                                        {this.state.isCross ? <span className="closeSearch" onClick={(e) => this.onsearchClear(e)}><img src={require('../../assets/clearSearch.svg')} /></span> : null}
                                    </div>: null}
                                </div>
                                <div className="gcmhb-right">
                                    {this.props.sizeType == "complex" && <div className="nph-switch-btn">
                                        <label className="tg-switch" >
                                            <input type="checkbox" checked={selectedData} onChange={this.changeSelectedData} />
                                            <span className="tg-slider tg-round"></span>
                                        </label>
                                        {selectedData && <label className="nph-wbtext">Selected data</label>}
                                        {!selectedData && <label className="nph-wbtext"> Selected data</label>}
                                    </div>}
                                </div>
                            </div>
                            <ul className="list-inline width_100 chooseDataModal">
                                <div className="col-md-8 col-sm-8 pad-0 modalDropBtn addNewWidth m-top-5">
                                    {this.state.save ? <div className="col-md-8 col-sm-8 pad-0 mrpLi">
                                        <li>
                                            <input type="text" pattern="[0-9]*" className={codeerr ? "errorBorder " : ""} onChange={(e) => this.handleChange(e)} id="code" name="code" value={code} placeholder="Enter code" />

                                            {codeerr ? (
                                                <span className="error">
                                                    Enter Code
                                                </span>
                                            ) : null}
                                        </li>
                                        <li> <input type="text" className={catSixerr ? "errorBorder" : ""} onChange={(e) => this.handleChange(e)} id="catSix" name="catSix" value={catSix} placeholder="Enter Color" />

                                            {catSixerr ? (
                                                <span className="error">
                                                    Enter Color
                                                </span>
                                            ) : null}
                                        </li>

                                    </div> : null}
                                    {/* {this.state.addNew ? <li className="width100">
                                        {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" && sessionStorage.getItem("partnerEnterpriseCode") != "TURNINGCLOUD" ? <select id="searchBycolor" name="searchBycolor" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                            <option value="contains">Contains</option>
                                            <option value="startWith"> Start with</option>
                                            <option value="endWith">End with</option>

                                        </select> : null}
                                        <input style={{paddingRight: '26px'}} type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} onKeyDown={this._handleKeyDown} onKeyPress={this._handleKeyPress} value={search} id="colorSearch" onChange={(e) => this.handleChange(e)} placeholder="Type to search" />
                                            {this.state.isCross && <div onClick={(e) => this.onsearchClear(e)} style={{position:'absolute', zIndex:'100', top:'8px', left:'36%', cursor:'pointer', height:'20px', width:'20px'}} ><img src={require('../../assets/clearSearch.svg')} alt='cross' /></div>}
                                        <label className="m-lft-15">
                                            <button type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                                <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                    <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                </svg>
                                            </button>
                                        </label>
                                    </li>
                                        : null}
                                    {this.state.addNew ? <li className="float_right">
                                    <label className="m-r-15">
                                    { hash == "#/purchase/purchaseIndent" ? <button type="button" className="findButton" onClick={(e)=>this.onAddNew(e)}>ADD NEW
                                    <span> + </span>
                                        </button>:null}
                                    </label>
                                    <label>
                                        <button type="button" className={this.state.search == "" && (this.state.type == "" || this.state.type == 1) ? "btnDisabled clearbutton" : "clearbutton"} id="clearButton" onKeyDown={(e) => this.onClearDown(e)} onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                    </label>
                                </li> : null} */}
                                </div>
                                {this.state.save ? <li className="float_right">
                                    <label className="m-r-15">
                                        <button type="button" className="findButton" onClick={(e) => this.onSave(e)}>SAVE

                                    </button>
                                    </label>
                                    <label>
                                        <button type="button" className="findButton" onClick={(e) => this.onCancel(e)}>CANCEL</button>
                                    </label>
                                </li> : null}
                            </ul>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="gcm-body">
                                <div className="gcm-table">
                                    {!selectedData && 
                                    <div className="gcmt-manage">
                                        <table className="table gcmt-inner">
                                            <thead>
                                                <tr>
                                                    <th className="fix-action-btn"></th>
                                                    <th><label>Color</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.props.colorType == "simple" ?
                                                    (this.state.colorState == undefined || this.state.colorState == null || this.state.colorState == "" ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.colorState.map((data, key) => (
                                                        // <React.Fragment>
                                                        <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)} >
                                                            {/* onKeyDown={() => this.handleRadioCheck(data.cname)} */}
                                                            <td className="fix-action-btn">
                                                                <ul className="table-item-list">
                                                                    <li className="til-inner">
                                                                        <label className="checkBoxLabel0" >
                                                                            <input type="radio" checked={this.state.value[0] === (`${data.cname}`)} id={data.cname}  readOnly />
                                                                            <span className="checkmark1" htmlFor={data.cname} ></span>
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td><label>{data.cname}</label></td>
                                                        </tr>
                                                    ))) : this.state.type == 3 ? this.state.colorState == undefined || this.state.colorState == null || this.state.colorState == "" || this.state.colorState.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.colorState.map((data, key) => (
                                                        // <React.Fragment>
                                                        <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)}>
                                                            <td className="fix-action-btn">
                                                                <ul className="table-item-list">
                                                                    <li className="til-inner">
                                                                        <label className="checkBoxLabel0" >
                                                                            <input type="checkBox" checked={this.state.value.includes(`${data.cname}`)} id={data.cname} onKeyDown={(e) => this.focusDone(e, `${data.cname}`)} readOnly />
                                                                            <span className="checkmark1" htmlFor={data.cname} ></span>
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td><label>{data.cname}</label></td>
                                                        </tr>
                                                    )) :
                                                        sessionStorage.getItem(this.props.colorCode + '-' + "color") == null ? this.state.colorState == undefined || this.state.colorState == null || this.state.colorState == "" || this.state.colorState.length == 0 ? <tr className="modalTableNoData"><td colSpan="2"> NO DATA FOUND </td></tr> : this.state.colorState.map((data, key) => (
                                                            <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)}>
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0" onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)} >
                                                                                <input type="checkBox" checked={this.state.value.includes(`${data.cname}`)} id={data.cname} onKeyDown={(e) => this.focusDone(e, `${data.cname}`)} readOnly />
                                                                                <span className="checkmark1" htmlFor={data.cname} onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)} ></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                            </tr>
                                                        )) : this.state.coData.map((data, key) => (
                                                            <tr key={key} className="rowFocus" onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)}>
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0" onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)} >
                                                                                <input type="checkBox" checked={this.state.value.includes(`${data.cname}`)} id={data.cname} onKeyDown={(e) => this.focusDone(e, `${data.cname}`)} readOnly />
                                                                                <span className="checkmark1" htmlFor={data.cname} onClick={(e) => this.onCheckbox(`${data.cname}`, `${data.code}`)}></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{data.cname}</label></td>
                                                            </tr>
                                                        ))
                                                    //  {/* </React.Fragment> */}
                                                }
                                            </tbody>
                                        </table>
                                    </div>}
                                    {selectedData &&
                                    <div className="gcmt-manage">
                                        <table className="table gcmt-inner">
                                            <thead>
                                                <tr>
                                                    <th className="fix-action-btn"></th>
                                                    <th><label>Color</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.colorList.map((c, i) => (
                                                    <React.Fragment key={i}>
                                                        {this.state.value.includes(`${c.cname}`) &&
                                                            <tr onClick={() => this.removeSelected(`${c.cname}`)}>
                                                                <td className="fix-action-btn">
                                                                    <ul className="table-item-list">
                                                                        <li className="til-inner">
                                                                            <label className="checkBoxLabel0"  >
                                                                                <input type="checkBox" checked={true} id={c.cname} onKeyDown={(e) => this.removeSelected(e, `${c.cname}`)} readOnly />
                                                                                <span className="checkmark1" htmlFor={c.cname} ></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td><label>{c.cname}</label></td>
                                                            </tr>
                                                        }

                                                    </React.Fragment>
                                                ))}

                                            </tbody>
                                        </table>
                                    </div>}
                                    <div className="col-md-12 pad-0" >
                                        <div className="new-gen-pagination">
                                            <div className="ngp-left">
                                                <div className="table-page-no">
                                                    <span>Page :</span><input type="number" className="paginationBorder" value="01" />
                                                    <span className="ngp-total-item">Total Items </span> <span className="bold">10</span>
                                                </div>
                                            </div>
                                            <div className="ngp-right">
                                                <div className="nt-btn">
                                                    <div className="pagination-inner">
                                                        <ul className="pagination-item">
                                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                                <button className="disable-first-btn" disabled >
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li> : 
                                                            <li>
                                                                <button className="first-btn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="first">
                                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li>}
                                                            {this.state.prev != 0 ? <li >
                                                                <button className="prev-btn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="prev">
                                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li> : 
                                                            <li>
                                                                <button className="dis-prev-btn" disabled>
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li>}
                                                            <li>
                                                                <button className="pi-number-btn" type="button">
                                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                                </button>
                                                            </li>
                                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="next">
                                                                            <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li> : 
                                                            <li >
                                                                <button className="dis-next-btn" disabled>
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li> :
                                                            <li>
                                                                <button className="dis-next-btn" disabled>
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li>}
                                                            {this.props.current != 0 && this.next - 1 != this.maxPage && this.current != undefined && this.maxPage != this.current ? <li >
                                                                <button className="last-btn" type="button" onClick={(e) => this.page(e)} id="last">
                                                                    <span className="page-item-btn-inner">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                            <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} id="last">
                                                                            <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </li> : 
                                                            <li >
                                                                <button className="dis-last-btn" disabled>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                    </svg>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                                    </svg>
                                                                </button>
                                                            </li>}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className="modal-bottom">
                            <ul className="list-inline m-top-10 modal-select">
                                {this.props.sizeType == "complex" && <React.Fragment> <li className="selectAllFocus">
                                    <label className="select_modalRadio">SELECT ALL
                                    <input type="radio" name="colorRadio" id="selectAll" onKeyDown={(e) => this.selectallKeyDown(e)} onClick={(e) => this.selectall(e)} />
                                        <span className="checkradio-select select_all"></span>
                                    </label>
                                </li>
                                    <li className="selectAllFocus">
                                        <label className="select_modalRadio">DESELECT ALL
                                    <input type="radio" name="colorRadio" id="deselectAll" onKeyDown={(e) => this.deselectallKeyDown(e)} onClick={(e) => this.deselectall(e)} />
                                            <span className="checkradio-select deselect_all"></span>
                                        </label>
                                    </li>
                                </React.Fragment>}
                            </ul>
                        </div> */}
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>

        );
    }
}

export default PiColorModal;