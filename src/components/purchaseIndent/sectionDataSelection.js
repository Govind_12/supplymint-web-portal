import React from "react";
import ToastLoader from "../loaders/toastLoader";
import PoError from "../loaders/poError";
class SectionDataSelection extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            isUDFExist: "false",
            isSiteExist: "true",
            itemUdfExist: "false",
            editDisplayName: "",
            divisionState: this.props.divisionState,
            selectedId: "",
            checked: false,
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            sectionSearch: "",
            hl1name: "",
            hl2name: "",
            hl3name: "",
            toastLoader: false,
            sessionData: [],
            save: false,
            addNew: true,
            hl1Nameerr: false,
            hl2Nameerr: false,
            hl3Nameerr: false,
            division: "",
            section: "",
            department: "",
            selecterr: false,
            sectionSelection: "",

            toastMsg: "",
            newSessionData: [],
            selectedData: this.props.selectedDepartment,
            errorMassage: "",
            poErrorMsg: false,
            selectedHl1Name: "",
            selectedHl1Code: "",
            selectedHl2Name: "",
            selectedHl2Code: "",
            selectedHl3Name: "",
            selectedHl3Code: "",
            searchBy: "startWith",
            focusedLi: "",

        }

    }

    componentDidMount() {
        if (this.props.isModalShow || this.props.isModalShow == undefined) {
            if (window.screen.width < 1200) {
                this.textInput.current.blur();
            } else {
                this.textInput.current.focus();
            }
        }
    }
    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }
    handleChange(e) {
        if (e.target.id == "division") {
            this.setState(
                {
                    division: e.target.value
                },
                () => {
                    this.hl1Name();
                }

            );
        } else if (e.target.id == "section") {
            this.setState(
                {
                    section: e.target.value
                },
                () => {
                    this.hl2Name();
                }

            );
        }
        else if (e.target.id == "department") {
            this.setState(
                {
                    department: e.target.value
                },
                () => {
                    this.hl3Name();
                }

            );
        }
        else if (e.target.id == "sectionSearch") {
            this.setState(
                {
                    sectionSearch: e.target.value
                },

            );

            // __________________________DON'T REMOVE THIS COMMENTED CODE ____________________________

            //     if(document.getElementById("sectionBasedOn").value=="hl1name"){
            //           this.setState(
            //               {
            //                 hl1name: e.target.value,
            //                 hl2name:"",
            //                 hl3name:"",

            //               },

            //           );
            //       }
            //      else if(document.getElementById("sectionBasedOn").value=="hl2name"){
            //           this.setState(
            //               {
            //                   hl1name:"",
            //                   hl2name: e.target.value,
            //                   hl3name:"",
            //               },


            //           );
            //       }    else if(document.getElementById("sectionBasedOn").value=="hl3name"){
            //           this.setState(
            //               {
            //                   hl1name:"",
            //                   hl2name:"",
            //                   hl3name: e.target.value,
            //               },


            //           );
            //       }else if(document.getElementById("sectionBasedOn").value==""){
            //         this.setState(
            //             {
            //                 hl1name:"",
            //                 hl2name:"",
            //                 hl3name:"",
            //                 sectionSearch:e.target.value
            //             },


            //         );

            // }
        }
        else if (e.target.id == "searchBydivision") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.sectionSearch != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        hl1name: this.state.hl1name,
                        hl2name: this.state.hl2name,
                        hl3name: this.state.hl3name,
                        search: this.state.sectionSearch,
                        searchBy: this.state.searchBy

                    }
                    this.props.divisionSectionDepartmentRequest(data)
                }
            })
        }
        // else if (e.target.id == "sectionBasedOn") {
        //       this.setState(
        //           {
        //             sectionSearch: "",
        //             sectionSelection:e.target.value
        //           },

        //       ); 


        //   }

    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.divisionData.data.prePage,
                current: this.props.purchaseIndent.divisionData.data.currPage,
                next: this.props.purchaseIndent.divisionData.data.currPage + 1,
                maxPage: this.props.purchaseIndent.divisionData.data.maxPage,
            })
            if (this.props.purchaseIndent.divisionData.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.divisionData.data.currPage - 1,
                    hl1name: this.state.hl1name,
                    hl2name: this.state.hl2name,
                    hl3name: this.state.hl3name,
                    search: this.state.sectionSearch,
                    searchBy: this.state.searchBy
                }
                this.props.divisionSectionDepartmentRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.divisionData.data.prePage,
                current: this.props.purchaseIndent.divisionData.data.currPage,
                next: this.props.purchaseIndent.divisionData.data.currPage + 1,
                maxPage: this.props.purchaseIndent.divisionData.data.maxPage,
            })
            if (this.props.purchaseIndent.divisionData.data.currPage != this.props.purchaseIndent.divisionData.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.divisionData.data.currPage + 1,
                    hl1name: this.state.hl1name,
                    hl2name: this.state.hl2name,
                    hl3name: this.state.hl3name,
                    search: this.state.sectionSearch,
                    searchBy: this.state.searchBy
                }
                this.props.divisionSectionDepartmentRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.divisionData.data.prePage,
                current: this.props.purchaseIndent.divisionData.data.currPage,
                next: this.props.purchaseIndent.divisionData.data.currPage + 1,
                maxPage: this.props.purchaseIndent.divisionData.data.maxPage,
            })
            if (this.props.purchaseIndent.divisionData.data.currPage <= this.props.purchaseIndent.divisionData.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    hl1name: this.state.hl1name,
                    hl2name: this.state.hl2name,
                    hl3name: this.state.hl3name,
                    search: this.state.sectionSearch,
                    searchBy: this.state.searchBy

                }
                this.props.divisionSectionDepartmentRequest(data)
            }
            this.textInput.current.focus();
        }
    }

    onSearch() {
        if (this.state.sectionSearch == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        } else {
            if (this.state.sectionSelection == "") {

                this.setState({
                    type: 3,
                    divisionData: []
                })
                let data = {
                    type: 3,
                    no: 1,
                    hl1name: this.state.hl1name,
                    hl2name: this.state.hl2name,
                    hl3name: this.state.hl3name,
                    search: this.state.sectionSearch,
                    searchBy: this.state.searchBy
                }
                this.props.divisionSectionDepartmentRequest(data)
            }
            else {
                this.setState({
                    type: 2,
                    divisionData: []

                })
                let data = {
                    type: 2,
                    no: 1,
                    hl1name: this.state.hl1name,
                    hl2name: this.state.hl2name,
                    hl3name: this.state.hl3name,
                    search: "",
                    searchBy: this.state.searchBy
                }
                this.props.divisionSectionDepartmentRequest(data)

            }
            let dSection = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData

        }
        this.textInput.current.focus();
    }


    onsearchClear() {

        if (this.state.type == 2 || this.state.type == 3) {
            var data = {
                type: "",
                no: 1,
                hl1name: "",
                hl2name: "",
                hl3name: "",
                search: "",
                searchBy: this.state.searchBy
            }
            this.props.divisionSectionDepartmentRequest(data)
            let dSection = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData
            this.setState(
                {
                    sectionSearch: "",
                    type: "",
                    no: 1,
                    sectionSelection: ""
                })
        }

    }
    closeSelection(e) {

        this.props.closeSelection(e)
        let dSection = this.state.sessionData == "" ? this.state.divisionState : this.state.sessionData


        const t = this


        t.setState({
            sectionSearch: "",
            sectionSelection: "",
            addNew: true,
            save: false,
            hl1Nameerr: false,
            hl2Nameerr: false,
            hl3Nameerr: false,
            division: "",
            no: 1,
            section: "",
            department: "",
            divisionState: [],
            sessionData: [],
            type: "",
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

    }
    onDone(e) {

        let dSection = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData
        var upData = {}
        if (this.state.selectedData == this.props.selectedDepartment) {
            if (this.state.selectedData == "" || this.state.selectedData == undefined) {
                this.setState({
                    toastMsg: "select data",
                    toastLoader: true,
                })
                const t = this
                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 1000)
            } else {
                this.closeSelection(e)
            }
        } else {

            upData = {
                hl1Name: this.state.selectedHl1Name,
                hl2Name: this.state.selectedHl2Name,
                hl3Name: this.state.selectedHl3Name,
                hl1Code: this.state.selectedHl1Code,
                hl2Code: this.state.selectedHl2Code,
                hl3Code: this.state.selectedHl3Code,
                itemUdfExist: this.state.itemUdfExist,
                isUDFExist: this.state.isUDFExist,
                isSiteExist: this.state.isSiteExist,
                editDisplayName: this.state.editDisplayName

            }
            // document.getElementById("sectionBasedOn").value=""
            setTimeout(() => {
                this.props.updateState(upData);

            }, 100)
            this.closeSelection(e)


        }
        document.onkeydown = function (t) {
            if (t.which == 9) {
                return true;
            }
        }
    }
    componentWillMount() {
        this.setState({
            selectedData: this.props.selectedDepartment,
            sectionSearch: this.props.isModalShow || this.props.isModalShow == undefined ? "" : this.props.hlsearch


        })

    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.divisionData.isSuccess) {

            if (nextProps.purchaseIndent.divisionData.data.resource != null) {

                this.setState({
                    divisionState: nextProps.purchaseIndent.divisionData.data.resource,
                    sessionData: sessionStorage.getItem("divisionSection") != null ? nextProps.purchaseIndent.divisionData.data.resource == null ? JSON.parse(sessionStorage.getItem("divisionSection")) : nextProps.purchaseIndent.divisionData.data.resource.concat(JSON.parse(sessionStorage.getItem("divisionSection"))) : "",
                    prev: nextProps.purchaseIndent.divisionData.data.prePage,
                    current: nextProps.purchaseIndent.divisionData.data.currPage,
                    next: nextProps.purchaseIndent.divisionData.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.divisionData.data.maxPage,
                    itemUdfExist: nextProps.purchaseIndent.divisionData.data.itemUdfExist,
                    isUDFExist: nextProps.purchaseIndent.divisionData.data.isUDFExist,
                    isSiteExist: nextProps.purchaseIndent.divisionData.data.isSiteExist,
                    editDisplayName: nextProps.purchaseIndent.divisionData.data.editDisplayName
                })
            }
            else {
                this.setState({
                    divisionState: [],
                    sessionData: sessionStorage.getItem("divisionSection") != null ? JSON.parse(sessionStorage.getItem("divisionSection")) : "",
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0
                })
            }
                 if (window.screen.width > 1200) {
                if (this.props.isModalShow || this.props.isModalShow==undefined) {

                    document.getElementById("sectionSearch").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.divisionData.data.resource != null) {
                        let divisionState = nextProps.purchaseIndent.divisionData.data.resource
                        this.setState({
                            focusedLi: divisionState[0].hl1Name + divisionState[0].hl2Name + divisionState[0].hl3Name
                        })
                        document.getElementById(divisionState[0].hl1Name + divisionState[0].hl2Name + divisionState[0].hl3Name) != null ? document.getElementById(divisionState[0].hl1Name + divisionState[0].hl2Name + divisionState[0].hl3Name).focus() : null
                    }


                } 
            }
        }
        if (nextProps.purchaseIndent.piAddNew.isSuccess) {
            this.onFinalSave();
            this.props.piAddNewRequest();
        }

    }
    selectedData(data) {
        let sectionData = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData

        for (var i = 0; i < sectionData.length; i++) {
            if (sectionData[i].hl1Name + sectionData[i].hl2Name + sectionData[i].hl3Name == data) {
                this.setState({
                    selectedData: sectionData[i].hl1Name + sectionData[i].hl2Name + sectionData[i].hl3Name,
                    selectedHl1Name: sectionData[i].hl1Name,
                    selectedHl1Code: sectionData[i].hl1Code,
                    selectedHl2Name: sectionData[i].hl2Name,
                    selectedHl2Code: sectionData[i].hl2Code,
                    selectedHl3Name: sectionData[i].hl3Name,
                    selectedHl3Code: sectionData[i].hl3Code
                }, () => {
                    if (!this.props.isModalShow || this.props.isModalShow != undefined) {
                        this.onDone()
                    }
                })
            }
        }
        if (sessionStorage.getItem("divisionSection") != null) {
            const s = this
            setTimeout(function () {
                s.setState({
                    sessionData: sectionData
                })
            }, 10)
        } else {
            const s = this
            setTimeout(function () {
                s.setState({
                    divisionState: sectionData
                })
            }, 10)
        }

    }

    onAdd(e) {
        e.preventDefault();
        let dSection = this.state.sessionData == "" ? this.state.divisionState : this.state.sessionData

        this.setState(
            {
                addNew: false,
                save: true,
                sectionSearch: "",
            })
        this.onsearchClear();
    }

    hl1Name(e) {
        if (this.state.division == "" || this.state.division.trim() == "") {
            this.setState({
                hl1Nameerr: true
            });
        } else {
            this.setState({
                hl1Nameerr: false
            });
        }
    }


    hl2Name(e) {
        if (this.state.section == "" || this.state.section.trim() == "") {
            this.setState({
                hl2Nameerr: true
            });
        } else {
            this.setState({
                hl2Nameerr: false
            });
        }
    }

    hl3Name(e) {
        if (this.state.department == "" || this.state.department.trim() == "") {
            this.setState({
                hl3Nameerr: true
            });
        } else {
            this.setState({
                hl3Nameerr: false
            });
        }
    }

    onSave(e) {
        this.hl1Name();
        this.hl2Name();
        this.hl3Name();
        e.preventDefault();

        const t = this;
        setTimeout(function () {

            const { hl1Nameerr, hl2Nameerr, hl3Nameerr } = t.state;

            if (!hl1Nameerr && !hl2Nameerr && !hl3Nameerr) {
                let saveData = {
                    isExist: "HIERARACHY",
                    hl4Code: "",
                    code: "",
                    categoryName: "",
                    description: "",
                    hl1Name: t.state.division,
                    hl2Name: t.state.section,

                    hl3Name: t.state.department
                }
                t.props.piAddNewRequest(saveData);

            }
        }, 10)
    }
    onFinalSave() {
        var idd = 0
        const t = this;
        var sessionArray = []
        let r = JSON.parse(sessionStorage.getItem("divisionSection"))
        var sectionDivision = []
        var sectionSection = []
        var sectionDepartment = []

        if (r != null) {
            for (var i = 0; i < r.length; i++) {
                sectionDivision.push(r[i].hl1Name.replace(/\s+/g, ""))
                sectionSection.push(r[i].hl2Name.replace(/\s+/g, ""))
                sectionDepartment.push(r[i].hl3Name.replace(/\s+/g, ""))
            }
        }
        if (sectionDivision.includes(t.state.division.replace(/\s+/g, "")) && sectionSection.includes(t.state.section.replace(/\s+/g, ""))
            && sectionDepartment.includes(t.state.department.replace(/\s+/g, ""))) {

            t.setState({
                errorMassage: "This section already exist",
                poErrorMsg: true
            })

        }

        else {

            if (sessionStorage.getItem("divisionSection") != null) {

                sessionArray = JSON.parse(sessionStorage.getItem("divisionSection"))
                let c = {
                    id: t.state.sessionData.length + 1,

                    hl1Name: t.state.division,
                    hl2Name: t.state.section,
                    hl3Name: t.state.department,
                }

                sessionArray.push(c)
                sessionStorage.setItem("divisionSection", JSON.stringify(sessionArray));

                t.setState({
                    sessionData: t.state.divisionState == null ? sessionArray : t.state.divisionState.concat(sessionArray),
                    toastMsg: "Department added successfully",
                    toastLoader: true,
                })

                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)


            } else {
                let c = {
                    id: t.state.divisionState == null ? idd++ : t.state.divisionState.length + 1,

                    hl1Name: t.state.division,
                    hl2Name: t.state.section,
                    hl3Name: t.state.department,
                }
                sessionArray.push(c)
                sessionStorage.setItem("divisionSection", JSON.stringify(sessionArray));

                t.setState({
                    sessionData: t.state.divisionState == null ? sessionArray : t.state.divisionState.concat(sessionArray),
                    toastMsg: "Department added successfully",
                    toastLoader: true,
                })


                setTimeout(function () {
                    t.setState({
                        toastLoader: false
                    })
                }, 2000)
            }
        }
        t.setState(
            {
                addNew: true,
                save: false,
            }
        )
        t.onClear();
    }
    onCancel(e) {
        this.setState(
            {
                addNew: true,
                save: false,
                division: "",
                section: "",
                department: "",

            }
        )
        this.onClear();
    }
    onClear() {
        this.setState({
            division: "",
            section: "",
            department: ""
        })
    }

    _handleKeyDown = (e) => {
        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                let sectionData = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData

                this.setState({
                    selectedData: sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name
                })
                this.selectedData(sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name)
                let dataselect = sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name
                window.setTimeout(function () {
                    document.getElementById("sectionSearch").blur()
                    document.getElementById(dataselect).focus()
                }, 0)
            }

            if (e.target.value != "") {
                window.setTimeout(function () {
                    document.getElementById("sectionSearch").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if (e.key === "ArrowDown") {
            let sectionData = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData

            this.setState({
                selectedData: sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name
            })
            this.selectedData(sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name)
            let dataselect = sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name
            window.setTimeout(function () {
                document.getElementById("sectionSearch").blur()
                document.getElementById(dataselect).focus()
            }, 0)
        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key === "Enter") {
            this.onSearch()
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {
            let dataselect = this.state.divisionState[0].hl1Name + this.state.divisionState[0].hl2Name + this.state.divisionState[0].hl3Name
            window.setTimeout(function () {
                document.getElementById(dataselect).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key === "Enter") {
            this.onDone(e);
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key === "Enter") {

            this.closeSelection();
        }
        if (e.key === "Tab") {
            window.setTimeout(function () {
                document.getElementById("closeButton").blur()
                document.getElementById("sectionSearch").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key === "Enter") {
            this.onsearchClear();
        }
        if (e.key === "Tab") {
            let sectionData = sessionStorage.getItem("divisionSection") == null ? this.state.divisionState : this.state.sessionData
            if (sectionData.length != 0) {
                this.setState({
                    selectedData: sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name
                })
                let dataselect = sectionData[0].hl1Name + sectionData[0].hl2Name + sectionData[0].hl3Name
                this.selectedData(dataselect)
                window.setTimeout(function () {
                    document.getElementById("clearButton").blur()
                    document.getElementById(dataselect).focus()
                }, 0)
            } else {
                window.setTimeout(function () {
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }
selectLi(e, code) {
        let divisionState = this.state.divisionState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < divisionState.length; i++) {
                if (divisionState[i].hl1Name + divisionState[i].hl2Name + divisionState[i].hl3Name == code) {
                    index = i
                }
            }
            if (index < divisionState.length - 1 || index == 0) {
                document.getElementById(divisionState[index + 1].hl1Name + divisionState[index + 1].hl2Name + divisionState[index + 1].hl3Name ) != null ? document.getElementById(divisionState[index + 1].hl1Name + divisionState[index + 1].hl2Name + divisionState[index +  1].hl3Name).focus() : null

                this.setState({
                    focusedLi: divisionState[index + 1].hl1Name + divisionState[index + 1].hl2Name + divisionState[index + 1].hl3Name
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < divisionState.length; i++) {
                if (divisionState[i].hl1Name + divisionState[i].hl2Name + divisionState[i].hl3Name == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(divisionState[index - 1].hl1Name + divisionState[index - 1].hl2Name + divisionState[index - 1].hl3Name) != null ? document.getElementById(divisionState[index - 1].hl1Name + divisionState[index - 1].hl2Name + divisionState[index - 1].hl3Name).focus() : null

                this.setState({
                    focusedLi: divisionState[index - 1].hl1Name + divisionState[index - 1].hl2Name + divisionState[index - 1].hl3Name
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.closeSelection(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.divisionState.length!=0?
                document.getElementById(divisionState[0].hl1Name + divisionState[0].hl2Name + divisionState[0].hl3Name).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.divisionState.length!=0){
                document.getElementById(divisionState[0].hl1Name + divisionState[0].hl2Name + divisionState[0].hl3Name).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.closeSelection(e)
        }


    }

    render() {
          if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

        const hash = window.location.hash;
        const { sectionSearch, hl1Nameerr, hl2Nameerr, hl3Nameerr, division, section, department, selecterr, sectionSelection } = this.state
        return (

          this.props.isModalShow  || this.props.isModalShow==undefined?   <div className={this.props.selectionAnimation ? "modal display_block" : "display_none"} id="pocolorModel">
                <div className={this.props.selectionAnimation ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.selectionAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.selectionAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>
                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT DEPARTMENT
                                    </label>
                                        </li>
                                        <li>
                                            <p className="para-content">You can select only single entry from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-5">

                                        {this.state.save ? <div className="col-md-8 col-sm-8 pad-0 addNewBtns mrpLi">
                                            <li>
                                                <input type="text" className={hl1Nameerr ? "errorBorder" : ""} onChange={e => this.handleChange(e)} id="division" name="division" value={division} placeholder="Enter Division" />
                                                {hl1Nameerr ? (
                                                    <span className="error">
                                                        Enter  Division
                                                        </span>
                                                ) : null}
                                            </li>
                                            <li> <input type="text" className={hl2Nameerr ? "errorBorder" : ""} onChange={e => this.handleChange(e)} id="section" name="section" value={section} placeholder="Enter Section" />
                                                {hl2Nameerr ? (
                                                    <span className="error">
                                                        Enter  Section
                                                        </span>
                                                ) : null}
                                            </li>
                                            <li> <input type="text" className={hl3Nameerr ? "errorBorder" : ""} onChange={e => this.handleChange(e)} id="department" name="department" value={department} placeholder="Enter Department" />
                                                {hl3Nameerr ? (
                                                    <span className="error">
                                                        Enter  Department
                                                        </span>
                                                ) : null}
                                            </li>

                                        </div> : null}


                                        {this.state.addNew ? <div className="col-md-8 col-sm-8 float-left pad-0">
                                            <div className="chooseDataModal">
                                                <li>
                                                    {/* __________________--DON'T REMOVE THIS COMMENTED CODE_____________________ */}
                                                    {/* <select id="sectionBasedOn"  name="sectionSelection" value={sectionSelection} onChange={(e) => this.handleChange(e)}>
                                            <option value="">Choose</option>
                                                <option value="hl1name">Division</option>
                                                <option value="hl2name">Section</option>
                                                <option value="hl3name">Department</option>
                                            </select>
                                            {selecterr ? (
                                                <span className="error">
                                                    select data
                                                </span>
                                            ) : null}

                                  */}
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchBydivision" name="searchBydivision" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" autoComplete="off" autoCorrect="off" className="search-box" ref={this.textInput} tabIndex="-1" onKeyDown={this._handleKeyDown} id="sectionSearch" value={sectionSearch} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} placeholder="Type to search" autoFocus />
                                                    <label className="m-lft-15">
                                                        <button id="find" type="button" className="findButton" id="findButton" onKeyDown={(e) => this.findKeyDown(e)} onClick={(e) => this.onSearch(e)}>FIND
                                        <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>

                                                </li>
                                            </div>
                                        </div> : null}
                                        {this.state.addNew ? <li className="float_right">
                                            {/* <label className="m-r-15">
                                        { hash == "#/purchase/purchaseIndent" ?   <button type="button" className="findButton" onClick={(e)=> this.onAdd(e)}>ADD NEW
                                            <span> + </span>
                                                </button>:null}
                                            </label> */}
                                            <label>
                                                <button type="button" className={this.state.sectionSearch == "" && (this.state.type == 1 || this.state.type == "") ? "btnDisabled clearbutton" : "clearbutton"} onKeyDown={(e) => this.onClearDown(e)} id="clearButton" onClick={(e) => this.state.sectionSearch == "" && (this.state.type == 1 || this.state.type == "") ? null : this.onsearchClear(e)}>CLEAR</button>
                                            </label>
                                        </li> : null}
                                        {this.state.save ? <li className="float_right">
                                            <label className="m-r-15">
                                                <button type="button" className="findButton" onClick={(e) => this.onSave(e)}>SAVE

                                            </button>
                                            </label>
                                            <label>
                                                <button type="button" className="findButton" onClick={(e) => this.onCancel(e)}>CANCEL</button>
                                            </label>
                                        </li> : null}
                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal transporterTable table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Division</th>
                                                    <th>Section</th>
                                                    <th>Department</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.type == 3 || this.state.type == 2 ? this.state.divisionState == undefined || this.state.divisionState == null || this.state.divisionState == "" || this.state.divisionState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.divisionState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`)}>
                                                        <td >
                                                            <label className="select_modalRadio radioModalCheckBox">
                                                                <input id={data.hl1Name + data.hl2Name + data.hl3Name} type="radio" onKeyDown={(e) => this.focusDone(e)} checked={this.state.selectedData == `${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`} name="divisionRadio" readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.hl1Name}</td>
                                                        <td>{data.hl2Name}</td>
                                                        <td>{data.hl3Name}</td>
                                                    </tr>
                                                )) : sessionStorage.getItem("divisionSection") == null ? this.state.divisionState == undefined || this.state.divisionState == null || this.state.divisionState == "" || this.state.divisionState.length == 0 ? <tr className="modalTableNoData"><td colSpan="4"> NO DATA FOUND </td></tr> : this.state.divisionState.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`)}>
                                                        <td>
                                                            <label className="select_modalRadio radioModalCheckBox">
                                                                <input id={data.hl1Name + data.hl2Name + data.hl3Name} onKeyDown={(e) => this.focusDone(e)} type="radio" checked={this.state.selectedData == `${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`} name="divisionRadio" readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox displayPointer"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.hl1Name}</td>
                                                        <td>{data.hl2Name}</td>
                                                        <td>{data.hl3Name}</td>
                                                    </tr>
                                                )) : this.state.sessionData.map((data, key) => (

                                                    <tr key={key} onClick={() => this.selectedData(`${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`)}>
                                                        <td>
                                                            <label className="select_modalRadio radioModalCheckBox">
                                                                <input id={data.hl1Name + data.hl2Name + data.hl3Name} type="radio" onKeyDown={(e) => this.focusDone(e)} checked={this.state.selectedData == `${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`} name="divisionRadio" readOnly />
                                                                <span className="checkradio-select select_all positionCheckbox"></span>
                                                            </label>
                                                        </td>
                                                        <td>{data.hl1Name}</td>
                                                        <td>{data.hl2Name}</td>
                                                        <td>{data.hl3Name}</td>
                                                    </tr>

                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">

                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" onKeyDown={(e) => this.closeKeyDown(e)} onClick={(e) => this.closeSelection(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 || this.state.current == undefined ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 && this.state.current != undefined ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn pointerNone" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}

            </div>:
                  <div className="dropdown-menu-city dropdown-menu-vendor marginTop-30" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.divisionState != undefined || this.state.divisionState.length != 0 ?
                            this.state.divisionState.map((data, key) => (
                                <li key={key} onClick={() => this.selectedData(`${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`)} id={data.hl1Name + data.hl2Name + data.hl3Name} className={this.state.selectedData == `${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e,`${data.hl1Name}` + `${data.hl2Name}` + `${data.hl3Name}`)}>
                                    <span className="vendor-details">
                                         <span className="vd-name">{data.hl1Name}</span>
                                        <span className="vd-loc">{data.hl2Name}</span>
                                        <span className="vd-num"> {data.hl3Name}</span>
                                        
                                      
                                    </span>
                                </li>)) : <li><span>No Data Found</span></li>}

                    </ul>
                    <div className="gen-dropdown-pagination">
                        <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.closeSelection(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn margin-180">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>


        );
    }
}

export default SectionDataSelection;