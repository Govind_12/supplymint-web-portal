import React from 'react';
import _ from 'lodash';
import SpreadsheetComponent from 'react-spreadsheet-component'
var Dispatcher = require('../../../node_modules/react-spreadsheet-component/lib/dispatcher.js');
// import Dispatcher from '../../../node_modules/react-spreadsheet-component/lib/dispatcher.js' 
import FilterLoader from '../loaders/smallLoader';
import PoError from '../loaders/poError';
import moment from 'moment';

class DataValidation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            unique: [],
            isCollapse: false,
            mapColData: 0,
            statusMessage: [],
            status: [],
            counter: 0,
            validateData: [],
            showStatus: false,
            cellErrors: [],
            arraysOfErorrs: [],
            excelEdit: false,
            poNumber: [],
            errorMessage: "",
            alert: false,
            spreadSheetData: []
        }
        this.method = this.method.bind(this)
    }

    componentDidMount() {
        this.setState({
            counter: _.cloneDeep(this.props.counter)
        })
        this.props.onRef(this)
        Dispatcher.subscribe('dataChanged',  (data) => {
            console.log("spreadsheet" + this.state.mapColData, data)
            this.replaceEditedData(data)
    
        }, ("spreadsheet-" + this.state.mapColData))

        let tempStatusMessageArray = []
        let array = []
        let arrayy = [];
        var index;
        this.setState({
            excelData: this.props.excelData
        }, () => {
    
                let spreadsheetdata = _.cloneDeep(this.state.excelData)
                let distinctHeaders = {}
                let repeatingHeaders = {}
                let counter = 0
                let distinctHeadersKeys = {}
                let repeatingHeadersKeys = {}
                let tempSpreadSheetRows = _.cloneDeep(this.props.fixedFieldNames)
                let tempSpreadSheetData = []
                let index
                let tempSpreadSheet = []
                console.log(this.props.mappedHeaders)
                let tempMappedHeaders = _.cloneDeep(this.props.mappedHeaders)
                // spreadsheetdata.map((dataI, keyI) => {
                        counter = 0
                        tempSpreadSheet = []
                        tempSpreadSheet.push(tempSpreadSheetRows)
                        // for(var k = 0; k < dataI.length; k++){
                            let tempHeadersRow = spreadsheetdata[0];
                            let tempRows = _.cloneDeep(spreadsheetdata);
                            Object.keys(tempMappedHeaders).forEach((data, key) => {
                                index = tempSpreadSheetRows.indexOf(data) // sitename
                                if (tempMappedHeaders[data] != "") {
                                    if(distinctHeaders.hasOwnProperty(tempMappedHeaders[data]) == false){
                                        distinctHeaders[tempMappedHeaders[data]] = tempSpreadSheetRows.indexOf(data);
                                    
                                        distinctHeadersKeys[data] = tempHeadersRow.indexOf(tempMappedHeaders[data]) != -1 ? tempHeadersRow.indexOf(tempMappedHeaders[data]):
                                        tempHeadersRow.indexOf((tempHeadersRow.filter(word => word.toLowerCase() === (tempMappedHeaders[data].toLowerCase())))[0])
                                    }
                                    else{
                                        repeatingHeaders[tempMappedHeaders[data]] = tempSpreadSheetRows.indexOf(data);
                                        repeatingHeadersKeys[data] = tempHeadersRow.indexOf(tempMappedHeaders[data]) != -1 ? tempHeadersRow.indexOf(tempMappedHeaders[data]):
                                        tempHeadersRow.indexOf((tempHeadersRow.filter(word => word.toLowerCase() === (tempMappedHeaders[data].toLowerCase())))[0])
                                    }
                                }

                            })
                            var values = Object.values(distinctHeaders)
                            var keyss = Object.keys(distinctHeaders)
                            var repeatedKeyss = Object.keys(repeatingHeaders)

                            if(counter === 0){
                                tempRows.map((data4, key4) => {
                                    counter = 1
                                    tempSpreadSheetData = []
                                    if (key4 != 0) {
                                        Object.keys(distinctHeadersKeys).map((el, keyy) => {
                                            if(el == keyss[keyy]){
                                                tempSpreadSheetData[distinctHeaders[el]] = data4[distinctHeadersKeys[el]]
                                            }
                                            else{
                                                tempSpreadSheetData[distinctHeaders[keyss[keyy]]] = data4[distinctHeadersKeys[el]]
                                            }
                                        })

                                        Object.keys(repeatingHeadersKeys).map((el, keyy) => {
                                            if(el == repeatedKeyss[keyy]){
                                                tempSpreadSheetData[repeatingHeaders[el]] = data4[repeatingHeadersKeys[el]]
                                            }
                                            else{
                                                // tempSpreadSheetData.splice(repeatingHeaders[repeatedKeyss[keyy]], 0, data4[repeatingHeadersKeys[el]]); 
                                                tempSpreadSheetData[repeatingHeaders[repeatedKeyss[keyy]]] = data4[repeatingHeadersKeys[el]]
                                            }
                                        })

                                   
                                    for (var i = 0; i < tempSpreadSheetData.length; i++) {
                                        if (tempSpreadSheetData[i] == undefined || i == undefined) {
                                            tempSpreadSheetData[i] = ""
                                        }
                                    }
                                    var temppp = tempSpreadSheetRows.length - tempSpreadSheetData.length
                                    if (tempSpreadSheetData.length < tempSpreadSheetRows.length) {
                                        for (var j = 0; j < temppp; j++) {
                                            tempSpreadSheetData.push("")
                                        }
                                    }
                                    tempSpreadSheet.push(tempSpreadSheetData)
                                }
                                })
                            }
    
    
                        // }

                        // spreadsheetdata.map((el1, key5) => {
                        //     el1 = tempSpreadSheet[key5]
                        // })
    
    
                // })

                tempSpreadSheet.map(el => {
                    // array.push(el[2])
                    if (el.indexOf("Break Point") != -1 ) {
                        index = el.indexOf("Break Point")
                        array.push(el[index])
                    }
                    else {
                        if(el[index] != ""){
                            array.push(el[index])
                        }
                    }
                })

                array = array.map(x => {if (isNaN(parseInt(x))) return x; else return parseInt(x);})

                let unique = array.filter((item, i, ar) => ar.indexOf(item) === i);
                let tempUnique = array.filter((item, i, ar) => ar.indexOf(item) === i);
                unique.map(data => {
                    if(data == "Break Point"){
                        unique.shift()
                        if(unique.length == 0 && tempUnique.length == 1){
                            unique.push("NA")
                            tempUnique.push("NA")
                        }
                    }
                })

                let spreadsheetdata1 = []
                var result = {}
                result = tempSpreadSheet.reduce(function (r, a) {
                    r[a[2]] = r[a[2]] || [];
                    r[a[2]].push(a);
                    return r;
                }, Object.create(null));
                Object.keys(result).map((data) => {
                    if(data != "Break Point"){
                        let obj = {
                            initialData: {
                                rows: result[data]
                            },
                            config: {
                                rows: 10,
                                columns: 20,
                                hasHeadColumn: false,
                                isHeadColumnString: false,
                                hasHeadRow: true,
                                isHeadRowString: false,
                                canAddRow: false,
                                canAddColumn: false,
                                emptyValueSymbol: '-',
                                hasLetterNumberHeads: false
                            },
                            cellClasses: {
                                rows: [
                                    ['', '', '', '', '', '', '', ''],
                                    ['', '', '', '', '', '', '', ''],
                                    ['', '', '', '', '', '', '', ''],
                                    ['', '', '', '', '', '', '', ''],
                                ]
                            }
                        }
                        spreadsheetdata1.push(obj)
                        tempStatusMessageArray.push("Data not validated yet")
                    }

                })
                for(var j = 0; j < spreadsheetdata1.length; j++){
                    spreadsheetdata1[j].initialData.rows.unshift(tempSpreadSheetRows)
                }
                this.props.getNumberOfDatafromValidation(unique)
    

                //////////
                Object.keys(spreadsheetdata1).forEach((datas, keys) => {
                    Object.keys(spreadsheetdata1[datas].initialData).forEach((datas1, keys1) => {
                        // numberOfRows = spreadsheetdata1[datas].initialData[datas1].length;
                        spreadsheetdata1[datas].initialData[datas1].map((datas2, keys2) => {
                            let tempHeaders = spreadsheetdata1[datas].initialData[datas1][0];
                            tempHeaders.forEach((data1, key1) => {
                                if (keys2 != 0) {
                                    if (data1 == "ORDER DATE" || data1 == "DELIVERY DATE" || data1 == "PO Valid From" || data1 == "PO Valid To") {
                                        var check = this.validatedate(datas2[key1])
                                        if (check == false) {
                                            var tempDate = this.ExcelDateToJSDate(datas2[key1])
                                            console.log(tempDate)

                                            var date = new Date(tempDate),
                                            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                                            day = ("0" + date.getDate()).slice(-2);
                                            var finalDate = [date.getFullYear(), mnth, day].join("-");

                                            datas2[key1] = finalDate
                                        }
                                        else{
                                            let some = moment(datas2[key1], ['YYYY/MM/DD', 'YYYY/DD/MM', "DD/MM/YYYY", "D/M/YYYY", "DD.MM.YYYY", "D.M.YYYY", "DD. MM. YYYY", "D. M. YYYY", "DD-MM-YYYY", "YYYY-MM-DD", "MM/DD/YYYY", "MM-DD-YYYY"]).format("YYYY-MM-DD");
                                            datas2[key1] = some
                                        }

                                    }
                                }
                            })
                        })
                    })
                })
                //////////
    
            this.setState({
                poNumber: tempUnique,
                unique: unique,
                spreadSheetdata: spreadsheetdata1,
                statusMessage: tempStatusMessageArray
            }, () => {
                this.calculateTotalLineItem()
            })
        })
    
    }
    componentWillUnmount() {
        this.props.onRef(undefined)

      }

      closeErrorRequest(e) {
        this.setState({
            alert: false
        })
    }

    ExcelDateToJSDate = (serial) => {
        // var utc_days  = Math.floor(serial - 25569);
        // var utc_value = utc_days * 86400;                                        
        // var date_info = new Date(utc_value * 1000);
     
        // var fractional_day = serial - Math.floor(serial) + 0.0000001;
     
        // var total_seconds = Math.floor(86400 * fractional_day);
     
        // var seconds = total_seconds % 60;
     
        // total_seconds -= seconds;
     
        // var hours = Math.floor(total_seconds / (60 * 60));
        // var minutes = Math.floor(total_seconds / 60) % 60;
     
        // return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);

        return new Date(Math.round((serial - 25569)*86400*1000));


     }


      replaceEditedData = (data) => {
          console.log(data, "spreadsheet" + this.state.mapColData, this.state.spreadSheetdata)
            let spreadSheetData = _.cloneDeep(this.state.spreadSheetdata)
            spreadSheetData.forEach((data1, key) => {
                if(key == this.state.mapColData){
                    Object.keys(data1).forEach(data2 => {
                        if(data2 == "initialData"){
                            data1[data2] = data
                        }
                    })
                }
            })
          this.setState({
            spreadSheetdata: spreadSheetData,
            excelEdit: true,
            counter: 0
        })
      }

      setErrorClasses = (datas) => {

        let tempData = _.cloneDeep(this.state.spreadSheetdata)
        // datas.forEach(some => {
            tempData.forEach((data, key) => {
                Object.keys(data).forEach((data1) => {
                    if(data1 == "cellClasses"){
                        data[data1].rows = datas[key]
                    }
                })
            })
        // })

            this.setState({
                spreadSheetdata: tempData,
            })
            
    }

      componentDidUpdate(prevProps){
        if(this.props.excelUpload.handleValidate.isSuccess){
            console.log()
            if(this.props.excelUpload.handleValidate.data.resource != null){
                let temp = [...this.state.validateData]
                let statusMessageArray = _.cloneDeep(this.state.statusMessage)
                        if(statusMessageArray.indexOf("Data not validated yet") > -1){
                            statusMessageArray = []
                        }
                            let cellErrors = _.cloneDeep(this.state.cellErrors)
                            
                            let statusArray = _.cloneDeep(this.state.status)
                            temp.push(this.props.excelUpload.handleValidate.data.resource)
                            let data = this.props.excelUpload.handleValidate.data.resource
                            let arraysOfErorrs = _.cloneDeep(this.state.arraysOfErorrs)
                                Object.keys(data).forEach((data2, key2) => {
                                    if(data[data2].address != null){
                                        cellErrors.push(data[data2].address)

                                        let tempData = _.cloneDeep(this.state.spreadSheetdata)
                                        var arraySize = []
                                        
                                        let tempSize;
                                        tempData.forEach((data, key) => {
                                            Object.keys(data).forEach((data1, key1) => {
                                                if(data1 == "initialData"){
                                                    arraySize.push(data[data1].rows) 
                                                arraySize.push(data[data1].rows) 
                                                    arraySize.push(data[data1].rows) 
                                                arraySize.push(data[data1].rows) 
                                                    arraySize.push(data[data1].rows) 
                                                arraySize.push(data[data1].rows) 
                                                    arraySize.push(data[data1].rows) 
                                                }
                                            })
                                        })
                                        var numberOfRows = arraySize[key2].length;
        
                                            tempSize = arraySize[key2][0].length
    
                                        let returnedData = this.getExcelCssArray(tempSize,numberOfRows, data[data2].address)
                                        arraysOfErorrs.push(returnedData)
    
    
                                        statusArray.push(data[data2].validation)
                                        statusMessageArray.push(data[data2].msg)
                                        this.setState({
                                            loader: false,
                                            validateData: temp,
                                            statusMessage: statusMessageArray,
                                            status: statusArray,
                                            showStatus: true,
                                            cellErrors: cellErrors,
                                            arraysOfErorrs: arraysOfErorrs
                                        }, () => {
                                            this.method()   
                                            this.setErrorClasses(arraysOfErorrs)
                                        })
                                    }
                                    else{
                                        this.setState({
                                            loader: false,
                                            validateData: temp,
                                            statusMessage: statusMessageArray,
                                            status: statusArray,
                                            showStatus: true,
                                            cellErrors: cellErrors,
                                            arraysOfErorrs: arraysOfErorrs
                                        }, () => {
                                            this.method()
                                            this.setErrorClasses(arraysOfErorrs)
                                        })
                                    }
                                    
                                })
                              
            

                           
                            this.props.handleValidateClear()
                            this.props.getDatafromValidation(this.state.spreadSheetdata, temp, this.state.unique)
            }
            
        }
        else if (this.props.excelUpload.handleValidate.isError) {
            console.log(this.props.excelUpload.handleValidate),
            this.setState({
              errorMessage: this.props.excelUpload.handleValidate.message.error == undefined ? undefined : this.props.excelUpload.handleValidate.message.error.errorMessage,
              alert: true,
              loader: false
            })
            this.props.handleValidateClear()

    }
}

    openMapColData(e, key, data) {
        e.preventDefault();
        this.setState({
            mapColData: key,
            isCollapse: !this.state.isCollapse
        });
    }
    calculateTotalLineItem = () => {
        let finalLength = []
        let spreadsheetTemp = _.cloneDeep(this.state.spreadSheetdata)
        spreadsheetTemp.forEach(data => {
            Object.keys(data).map(data1 => {
                if(data1 == "initialData"){
                    finalLength.push(data[data1].rows.length - 1)
                }
            })
        })
        this.setState({
            totalLineItems: finalLength
        })
        this.props.getInitialDatafromValidation(finalLength)
    }
    arrToObject = (arr) =>{
       var keys = arr.rows[0];
       var newArr = arr.rows.slice(1, arr.rows.length);
       var formatted = [],
       data = newArr,
       cols = keys,
       l = cols.length;
       for (var i=0; i<data.length; i++) {
               var d = data[i],
                       o = {};
               for (var j=0; j<l; j++)
                       o[cols[j]] = d[j];
               formatted.push(o);
       }
       let keyNames = this.props.keyNames
       let fixedFieldNames = this.props.fixedFieldNames

       formatted.map((data, key) => {
           Object.keys(data).map((data1, key1) => {
                if(fixedFieldNames.indexOf(data1) > -1){
                        let index = fixedFieldNames.indexOf(data1)
                        if(keyNames[index] == data1){
                            formatted[key][keyNames[index]] = formatted[key][data1]
                        }
                       else{
                        formatted[key][keyNames[index]] = formatted[key][data1]
                        delete formatted[key][data1]
                       }
                }
           })
       })
       this.setState({
           excelData2:formatted
       })
       return formatted;
   }

   getExcelCssArray = (columns,rows, json)  => {
    Object.keys(json).forEach(data => {
        json[data] = json[data].split(',')
    })
    var arr = new Array(columns);
    for(var i=0;i<rows;i++) {
        arr[i] = new Array(rows);
        for(var j=0;j<columns;j++) {
            var colname = this.numberToCol(j+1);

            var cell = colname + (i+1);
            var value = "";
            if(json.mandate.indexOf(cell)!=-1) {
                value = "red";
            }
            if(json.mismatch.indexOf(cell)!=-1) {
                value = "orange";
            }
            if(json.ext.indexOf(cell)!=-1) {
                value = "yellow";
            }
            if(json.dataIssue.indexOf(cell)!=-1) {
                value = "Brown";
            }
            
            arr[i][j]= value;
        }
    }

    var arr1 = new Array(columns);

    arr.unshift(arr1)
    return arr
}

        numberToCol = (num) => {
        var str = '', q, r;
        while (num > 0) {
        q = (num-1) / 26;
        r = (num-1) % 26
        num = Math.floor(q)
        str = String.fromCharCode(65 + r) + str;
        }
        return str;
        }

    validatedate = (inputText) => {
        if(moment(inputText.toString(), ['YYYY/MM/DD', 'YYYY/DD/MM', 'DD/MM/YYYY', 'D/M/YYYY', 'DD.MM.YYYY', 'D.M.YYYY', 'DD. MM. YYYY', 'D. M. YYYY', 'DD-MM-YYYY', 'YYYY-MM-DD', 'MM/DD/YYYY', 'MM-DD-YYYY'], true).isValid()) {
            return true
        }
        else{
            return false
        }
    }


    
    method() {
        var counter = 0;
        var finalPaylod = [];
        var tempHeaders = []
        var returnedData = {}
        var array = [];
        let total = 0;
        let arraysOfErorrss = []
        var tempSize, numberOfRows;
        finalPaylod = _.cloneDeep(this.state.spreadSheetdata)
        console.log(finalPaylod)
        
        Object.keys(finalPaylod).forEach((datas, keys) => {
            numberOfRows = finalPaylod[datas].initialData["rows"].length;
            Object.keys(finalPaylod[datas].initialData).forEach((datas1, keys1) => {
                // numberOfRows = finalPaylod[datas].initialData[datas1].length;
                finalPaylod[datas].initialData[datas1].map((datas2, keys2) => {
                    numberOfRows = finalPaylod[datas].initialData[datas1].length;
                    tempSize = finalPaylod[datas].initialData[datas1][0].length
                    tempHeaders = finalPaylod[datas].initialData[datas1][0];
                    tempHeaders.forEach((data1, key1) => {
                        if (keys2 != 0) {
                            if (data1 == "ORDER DATE" || data1 == "DELIVERY DATE" || data1 == "PO Valid From" || data1 == "PO Valid To") {
                                var check = this.validatedate(datas2[key1])
                                if (check == false) {
                                    array.push(this.numberToCol(key1 + 1) + keys2)
                                }
                                else{
                                    counter ++;
                                    let some = moment(datas2[key1], ['YYYY/MM/DD', 'YYYY/DD/MM', "DD/MM/YYYY", "D/M/YYYY", "DD.MM.YYYY", "D.M.YYYY", "DD. MM. YYYY", "D. M. YYYY", "DD-MM-YYYY", "YYYY-MM-DD", "MM/DD/YYYY", "MM-DD-YYYY"]).format("YYYY-MM-DD");
                                    datas2[key1] = some
                                }

                            }
                        }
                    })
                })
                let objArray = {}
                objArray["invalidDates"] = array
                let returnData = this.getDateCss(tempSize, numberOfRows, objArray)
                arraysOfErorrss.push(returnData)
            })
            total = total + (numberOfRows - 1) * 4 
        })
        console.log(total)

        this.setErrorClasses(arraysOfErorrss)
        
        var valuess = Object.values(returnedData)
        var valuesToPrint = []
        Object.keys(returnedData).forEach((data7, key7) => {
            if (returnedData[data7] == false) {
                valuesToPrint.push(data7)
            }
        })
        console.log("returnedData", returnedData)
        if (counter == total) {
            if (this.state.counter < Object.keys(finalPaylod).length || this.state.excelEdit) {
                let payload = {}
                let tempPayload = {}
                let tempp, temp;
                temp = finalPaylod[this.state.counter].initialData
                tempp = this.arrToObject(temp)
                tempPayload["Sheet" + (this.state.counter + 1)] = tempp
                payload = {
                    "isPO": this.props.isPo,
                    data: tempPayload
                }
                this.setState({
                    counter: this.state.counter + 1,
                    loader: true
                })
                if (Object.keys(finalPaylod).length == 1) {
                    this.setState({
                        excelEdit: false,
                    })
                }
                this.props.handleValidateRequest(payload)
            }
        }
        else {
            alert("Please enter valid dates!")
        }
    }



getDateCss = (columns,rows, json)  => {
    // Object.keys(json).forEach(data => {
    //     json[data] = json[data].split(',')
    // })
    var arr = new Array(columns);
    for(var i=0;i<rows;i++) {
        arr[i] = new Array(rows);
        for(var j=0;j<columns;j++) {
            var colname = this.numberToCol(j+1);
            var cell = colname + (i+1);
            var value = "";
            if(json.invalidDates.indexOf(cell)!=-1) {
                value = "red";
            }
            arr[i][j]= value;
        }
    }

    var arr1 = new Array(columns);

    arr.unshift(arr1)
    return arr
}
        
        numberToCol = (num) => {
        var str = '', q, r;
        while (num > 0) {
        q = (num-1) / 26;
        r = (num-1) % 26
        num = Math.floor(q)
        str = String.fromCharCode(65 + r) + str;
        }
        return str;
        }

    render() {
        console.log(this.props.mappedHeaders)

        console.log(this.state.unique, this.state.poNumber)
        const testVars = this.state.spreadSheetdata
        console.log(testVars)
        return (
            <div className="excel-upload-page-bottom m-top-30">
                {
                    this.state.unique.map((data, key) => (
                        <div key = {key} className="eupb-collapse">
                            <div className="eupb-row">
                                <div className="eupbr-left">
                                    <div className="eupbrl-left">
                                        <p>{this.state.poNumber[0]}</p>
                                        <h5>{data}</h5>
                                    </div>
                                    <div className="eupbrl-right">
                                        <div className="eupbrlr-inner">
                                            <p>No. of Lineitems uploaded</p>
                                            <h5>{this.state.totalLineItems != undefined ? this.state.totalLineItems[key] : ""}</h5>
                                        </div>
                                        <div className="eupbrlr-inner">
                                            <p>Quantity</p>
                                            <span className="eupbrlr-amnt"> {Object.keys(this.props.totalQty).length == 0 ? "-" :this.props.totalQty["Sheet" + (key+1)]}</span>
                                        </div>
                                        <div className="eupbrlr-inner">
                                            <p>Total Amount</p>
                                            <span className="eupbrlr-amnt">{Object.keys(this.props.totalAmt).length != 0 ? <span className="repee">&#x20B9; </span> : null }{Object.keys(this.props.totalAmt).length == 0 ? "-" : this.props.totalAmt["Sheet" + (key+1)]}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="eupbr-right">
                                    <div className="eupbrr-left">
                                        {
                                            this.state.showStatus ? 
                                                <div className="eupbrrl-img">
                                                {
                                                    this.state.status[key] != undefined ? 
                                                    this.state.status[key] == "failed" ?
                                                    <img src={require('../../assets/close-red.svg')} />:
                                                    <img src={require('../../assets/correct.svg')} />
                                                    : null
                                                }
                                                </div>
                                            :null
                                        }
                                        <div className="eupbrrl-inner">
                                            <h3>{this.state.loader ? <FilterLoader /> : this.state.statusMessage[key] != undefined ? this.state.statusMessage[key] : null}</h3>
                                        </div>
                                    </div>
                                    <div className="eupbrr-right">
                                        <button type="button" className="eupbrr-arrow" onClick={(e) => this.openMapColData(e, key, data)}><img src={require('../../assets/downArrowNew.svg')} /></button>
                                    </div>
                                </div>
                            </div>
                                {
                                    this.state.mapColData == key && this.state.isCollapse ?
                                    <div className="eupbc-bottom-sec">
                                        <div className="eupbc-table">
                                            <SpreadsheetComponent
                                                initialData={testVars[key].initialData}
                                                config={testVars[key].config}
                                                cellClasses={testVars[key].cellClasses}
                                                spreadsheetId = {"spreadsheet-" + this.state.mapColData}
                                            />
                                        </div>
                                    </div> : null
                                }
                        </div>
                    ))
                }

                {this.state.alert ? <PoError errorMassage={this.state.errorMessage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}
export default DataValidation;




