import React from 'react';
import _ from 'lodash';
import FilterLoader from '../loaders/smallLoader';
import PoError from '../loaders/poError';
import RequestSuccess from "../loaders/requestSuccess";

class Summary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            summaryData: _.cloneDeep(this.props.spreadSheetData),
            validateData: this.props.validateDataa,
            success: false,
            successMessage: "",
            errorMessage: "",
            showRedirectButton: false
        }
        this.method1 = this.method1.bind(this)

    }

    componentDidMount() {
        this.props.onRef(this)
        
        console.log(this.props)
        let finalData = _.cloneDeep(this.state.summaryData)
        Object.keys(finalData).forEach((data, key) => {
            console.log(data, finalData[data])
        })

            let successfull = 0
            let failed = 0;
            console.log(this.state.validateData)
            this.state.validateData.map(data1 => {
                Object.keys(data1).forEach((data2, key) => {
                    if(data1[data2].validation == "failed"){
                        failed = failed + 1
                    }
                    else if(data1[data2].validation == "success"){
                        successfull = successfull + 1
                    }
                })
            })
            this.setState({
                totalFailed: failed,
                totalSuccess: successfull
            })
        
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    componentDidUpdate(prevProps){
        if(this.props.excelUpload.excelSubmit.isSuccess){
            if(this.props.excelUpload.excelSubmit.data.message != null){
                                        this.setState({
                                            loader: false,
                                            success: true,
                                            successMessage: this.props.excelUpload.excelSubmit.data.message
                                        })
                           
                            this.props.excelSubmitClear()
            }
            
        }
        else if (this.props.excelUpload.excelSubmit.isError) {
            console.log(this.props.excelUpload.excelSubmit),
            this.setState({
              errorMessage: this.props.excelUpload.excelSubmit.message.error == undefined ? undefined : this.props.excelUpload.excelSubmit.message.error.errorMessage,
              alert: true,
              loader: false
            })
            this.props.excelSubmitClear()
    }
}

closeErrorRequest(e) {
    this.setState({
        alert: false
    })
}

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false,
            showRedirectButton: true
        });
        this.props.disableProceedBtn(true)
    }

  validatedate = (inputText) => {
    if(moment(inputText.toString(), ['DD/MM/YYYY', 'D/M/YYYY', 'DD.MM.YYYY', 'D.M.YYYY', 'DD. MM. YYYY', 'D. M. YYYY', 'DD-MM-YYYY', 'YYYY-MM-DD', 'MM/DD/YYYY', 'MM-DD-YYYY'], true).isValid()) {
        return true
    }
    else{
        return false
    }
}

    method1 = () => {

        // let temp1 = _.cloneDeep(this.state.summaryData)

        // Object.keys(temp1).forEach((datas, keys) => {
        //     Object.keys(temp1[datas].initialData).forEach((datas1, keys1) => {
        //         // numberOfRows = temp1[datas].initialData[datas1].length;
        //         temp1[datas].initialData[datas1].map((datas2, keys2) => {
        //             tempHeaders = temp1[datas].initialData[datas1][0];
        //             tempHeaders.forEach((data1, key1) => {
        //                 if (keys2 != 0) {
        //                     if (data1 == "ODER DATE" || data1 == "DELIVERY DATE" || data1 == "PO Valid From" || data1 == "PO Valid To") {
        //                         var check = this.validatedate(datas2[key1])
        //                         if (check) {
        //                             let some = moment(datas2[key1], ["DD/MM/YYYY", "D/M/YYYY", "DD.MM.YYYY", "D.M.YYYY", "DD. MM. YYYY", "D. M. YYYY", "DD-MM-YYYY", "YYYY-MM-DD", "MM/DD/YYYY", "MM-DD-YYYY"]).format("YYYY-MM-DD");
        //                             datas2[key1] = some
        //                         }

        //                     }
        //                 }
        //             })
        //         })
        //     })
        // })
        let tempSummaryData = _.cloneDeep(this.state.summaryData)
        var finalArray = {}
        tempSummaryData.map((data1, key) => {
            Object.keys(data1).forEach((data2) => {
                let temp;
                if(data2 == "initialData"){
                    temp = this.arrToObject(data1[data2])
                    finalArray["Sheet" + (key + 1)] = temp
                }
            })
        })

        let payload = {}
        let tempArray = []
        let tempArray1 = []
        let tempObj = {}
        let tempObj1 = {}
        let tempValidate = _.cloneDeep(this.state.validateData)
        tempValidate.forEach(data => {
            Object.keys(data).forEach(data6 => {
                if (data[data6].validation == "success" && finalArray.hasOwnProperty(data6)) {
                    tempArray = finalArray[data6]
                    tempArray1 = data[data6]
                }
                tempObj[data6] = tempArray
                tempObj1[data6] = tempArray1
            })
        })

        payload = {
            "isPO": this.props.isPo,
            "totalPOCount": this.props.uniquee.length,
            "totalItemCount": this.props.totalCalculatedQty,
            "totalAmount": this.props.totalCalculatedAmt,
            "data": tempObj,
            "additionalData" : tempObj1
        }

        if (tempArray.length != 0) {
            this.props.excelSubmitRequest(payload)
            this.setState({
                loader: true
            })
        }
        else{
            var  message =  this.props.isPo ? "No successfull PO's" : "No successfull PI's"
            this.setState({
                errorMessage: message,
                alert: true
            })
        }
    }



    arrToObject = (arr) =>{

        console.log(arr)
       var keys = arr.rows[0];
       var newArr = arr.rows.slice(1, arr.rows.length);
       var formatted = [],
       data = newArr,
       cols = keys,
       l = cols.length;
       for (var i=0; i<data.length; i++) {
               var d = data[i],
                       o = {};
               for (var j=0; j<l; j++)
                       o[cols[j]] = d[j];
               formatted.push(o);
       }
       let keyNames = this.props.keyNames
       let fixedFieldNames = this.props.fixedFieldNames

       formatted.map((data, key) => {
        Object.keys(data).map((data1, key1) => {
            if(fixedFieldNames.indexOf(data1) > -1){
                let index = fixedFieldNames.indexOf(data1)
                if(keyNames[index] == data1){
                    formatted[key][keyNames[index]] = formatted[key][data1]
                }
               else{
                formatted[key][keyNames[index]] = formatted[key][data1]
                delete formatted[key][data1]
               }


        }
        })

    })
       console.log(formatted)

       this.setState({
           excelData2:formatted
       })
       return formatted;
   }
    render() {
        console.log(this.state.totalFailed, this.state.isPo, this.state.isPi)
        return (
            <div className="col-lg-5 pad-0 m-top-20">
                <div className="eup-summary-upload-status">
                    <div className="eupsus-inner">
                        <h3>Upload Status</h3>
                        <div className="eupsusi-bot">
                            <div className="eupsusib-sucess">
                                {this.state.totalSuccess >= 1 ? <img src={require('../../assets/correct.svg')} /> : null }
                                <h5>{this.state.totalSuccess ? this.props.isPo ? this.state.totalSuccess + " PO Created Successfully" : this.state.totalSuccess + " PI Created Successfully" : null} </h5>
                            </div>
                            <div className="eupsusib-sucess">
                                {this.state.totalFailed == 0 || this.state.totalFailed == undefined ? null : <img src={require('../../assets/close-red.svg')} />}
                                <h5>{this.state.totalFailed ? this.props.isPo ? this.state.totalFailed +  " PO has been rejected" : this.state.totalFailed + " PI has been rejected" : null}</h5>
                            </div>
                        </div>
                    </div>
                    {
                        this.state.showRedirectButton ? 
                            <div className="eupsus-bottom">
                                <p>Click below to view recently purchase orders</p>
                                <div className="eupsusb-btn">
                                    <button type="button" className="eupsusb-vpo" onClick = {this.props.isPo ? () => this.props.history.push("/purchase/purchaseOrderHistory") : () => this.props.history.push("/purchase/purchaseIndentHistory")}> {this.props.isPo ? "View Purchase Orders" : "View Purchase Indents"}
                                                            <span className="eupsusb-arrow-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13.486" height="6.532" viewBox="0 0 13.486 6.532">
                                                <g>
                                                    <path fill="#fff" d="M13.332 134.893l-2.753-2.739a.527.527 0 0 0-.743.747l1.848 1.839H.527a.527.527 0 1 0 0 1.054h11.156l-1.848 1.839a.527.527 0 0 0 .743.747l2.753-2.739a.527.527 0 0 0 .001-.748z" transform="translate(0 -132)" />
                                                </g>
                                            </svg>
                                        </span>
                                    </button>
                                    <button type="button" className="eupsusb-vuh" onClick={() => this.props.history.push("/digiproc/excelUploadHistory")}>View Upload History</button>
                                </div>
                            </div>: null
                    }

                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.alert ? <PoError errorMassage={this.state.errorMessage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
            </div>
        )
    }
}
export default Summary;




