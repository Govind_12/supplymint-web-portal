import React from "react";
import PiImageModal from "./piImageModal";
import PiSizeModal from "./piSizeModal";
import PiColorModal from "./piColorModal";
import PiMrpModal from "./piMrpModal.js";
import PurchaseItemTable from "./purchaseItemTable";
import moment from 'moment';
import ToastLoader from "../loaders/toastLoader";
import ConfirmModal from '../loaders/confirmModal';
import exclaimIcon from "../../assets/exclain.svg";
import { getDate, getYear } from '../../helper';
import copyIcon from "../../assets/copy-icon-copy.svg";
import UdfComponet from './udfComponent';
import PoError from "../loaders/poError";
import _ from 'lodash';
import HsnCodeModal from "../purchaseOrder/hsnCodeModal";
import ItemCodeModal from "../purchaseOrder/itemCodeModal";
import DiscountModal from "../purchaseOrder/discountModal";
import ArticleNewModal from "./articleNewModal";
class PurchaseIndentTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchLeft:"",
            prevSearchLeft:"",
            searchTop:"",
            prevSearchTop:"",
            searchModal:"",
            prevSearchModal:"",
            articleSearch:"",
            focusId: "",
            sizeDataList: [],
            discountMrp: "",
            selectedDiscount: "",
            discountGrid: "",
            discountModal: false,
            vendorMrpPo: "",



            colorListValue: [],
            hsnModal: false,
            hsnModalAnimation: false,

            hsnRowId: "",
            hsnvalue: "",
            deleteOtb: "",
            deleteArticleCode: "",
            deleteMrp: "",
            sizes: [],
            articleMrpArray: [],
            otb: [],
            idx: "",
            ratioValue: [],
            sizeValue: [],
            sizeArray: [],
            colorValue: [],
            imageState: {},
            imageName: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            pi: "",
            rowDelete: "",
            piResource: [],
            chargesData: [],
            colorCode: "",
            sizeCode: "",
            colorNewData: [],
            sizeNewData: [],
            checked: false,
            quantityId: "",
            imageRowId: "",
            colorState: [],
            sizeState: [],
            poQuantity: 0,
            poAmount: 0,
            rowIdentity: "",
            sizeRow: "",
            colorRow: "",
            sizeData: [],
            colorData: [],
            imageModal: false,
            imageModalAnimation: false,
            mrpModal: false,
            mrpModalAnimation: false,
            sizeModal: false,
            sizeModalAnimatiion: false,
            colorModal: false,
            colorModalAnimation: false,
            articleCode: "",
            articleName: "",
            mrp: "",
            rsp: "",
            slCode: this.props.slcode,
            department: this.props.department,
            resetForm: this.props.resetForm,
            toastLoader: false,
            toastMsg: "",
            flag: false,
            iCode: "",
            rows: [{
                articleCode: "",
                articleName: "",
                vendorMrp: "",
                typeOfBuy: "",
                iCode: "",
                vendorDesign: "",
                image: [],
                showImage: "",
                imageUrl: {},
                containsImage: false,
                rsp: "",
                mrp: "",
                rate: "",
                finalRate: "",
                discount: {
                    discountType: "",
                    discountValue: ""
                },
                size: [],
                sizeList: [],
                ratio: [],
                color: [],
                colorList: [],
                numOfsets: "",
                date: "",
                remarks: "",
                quantity: "",
                netAmountTotal: "",
                calculateMargin: "",
                gst: "",
                otb: "",
                intakeMargin: "",
                mrpStart: "",
                mrpEnd: "",
                rowId: 0,
                hsnSacCode: "",
                hsnCode: "",
                tax: "",
                finCharges: [],
                basic: "",
                marginRule: "",
                totalAmount: ""
            }],
            hsnSacCode: "",
            flag1: 0,
            currentDate: "",
            maxDate: "",
            setUdfRowsData: [],
            udfItemHeadData: [],
            itemDetailsHeader: [],
            itemDetails: [],
            validateMsg: false,
            rowsId: "",

            active: false,
            rateDisable: false,
            gridSecond: false,

            // ____________________________________

            descId: "",
            desc6: "",
            itemModal: false,
            itemModalAnimation: false,
            startRange: "",
            endRange: "",
            articleNewModalAnimation: false,
            articleNew: false,
            checkedArticle: "",
            newArticleName: "",
            articleRowId: "",
            articleId:"",
            hsnSearch:""
        }
    }

    closeErrorRequest(e) {
        this.setState({
            validateMsg: false,
            poErrorMsg: !this.state.poErrorMsg
        })
    }
    updateColorState(data2) {
        var cat6 = data2.colorData;
        var array = this.state.rows
        for (var x = 0; x < array.length; x++) {
            if (array[x].rowId == data2.colorrId) {
                array[x].color = cat6.join(',');
                if (array[x].size != "" && array[x].ratio != "" && array[x].numOfsets != "" && array[x].color != "" && array[x].rate != "" && this.props.purtermMainCode != "") {
                    this.getQuantity(data2.colorrId);
                }
            }
        }
        this.setState({
            rows: array
        })
    }

    closeConfirmModal(e) {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }

    onResetData() {
        this.props.onResetData()
    }

    updateSizeList(sizeList) {

        let index = 1
        var reCollectData = [];
        sizeList.forEach(size => {


            let sizeData = {
                id: index++,
                code: size.code,
                cname: size.cname
            }
            reCollectData.push(sizeData);
        })

        var array = this.state.rows
        for (var x = 0; x < array.length; x++) {
            if (array[x].rowId == sizeList[0].Id) {
                array[x].sizeList = reCollectData;
            }
        }
        this.setState({
            rows: array
        })
    }
    updateColorList(udata) {
        //(colorListt)



        let colorL = [...udata.colorList]
        let colorA = []
        var reCollectData = [];
        let index = 0

        colorL.forEach(color => {
            if (!colorA.includes(color.code)) {


                let colorData = {
                    id: index++,
                    code: color.code,
                    cname: color.cname,

                }
                colorA.push(color.code)
                reCollectData.push(colorData);
            }
        })

        var array = this.state.rows
        for (var x = 0; x < array.length; x++) {
            if (colorL.length != 0) {
                if (array[x].rowId == udata.colorrId) {
                    array[x].colorList = reCollectData
                }
            }
        }
        this.setState({
            rows: array
        })
    }

    // ____________________________openItemUdfModal___________________________


    // _________________________openUdfMappingModal_______________________________

    openUdfMappingModal(code, name, id, value) {
        let udfType = code;
        let udfName = name;
        let idd = id;
        var data = {
            no: 1,
            type: 1,
            search: "",
            udfType: udfType,
            code: "",
            name: "",
            ispo: true
        }
        this.props.udfTypeRequest(data);
        this.setState({
            setValue: value,
            udfType: udfType,
            udfName: udfName,
            udfRow: idd,
            UdfMappingModal: true,
            udfMappingAnimation: !this.state.udfMappingAnimation,
        })
    }

    closeUdf() {
        this.setState({
            udfMapping: false,
            udfMappingAnimation: !this.state.udfMappingAnimation
        });

    }
    getOtb(id) {

        let rows = this.state.rows
        let article = ""
        let mrp = ""
        let count = 0
        let otb = []
        let netAmount = []
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].rowId == id) {
                article = rows[i].articleCode
                mrp = rows[i].vendorMrp

            }
        }

        for (var i = 0; i < rows.length; i++) {
            if (rows[i].articleCode == article && rows[i].vendorMrp == mrp) {
                count++
            }
        }

        if (count > 1 && rows.length > 1) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].articleCode == article && rows[i].vendorMrp == mrp) {
                    otb.push(rows[i].otb)
                    netAmount.push(rows[i].netAmountTotal)
                }

            }



            let minOtb = Math.min.apply(null, otb)


            let index = ""
            let netAmtFinal = ""
            for (var k = 0; k < otb.length; k++) {
                if (otb[k] == minOtb) {
                    index = k
                }
            }
            for (var l = 0; l < netAmount.length; l++) {
                netAmtFinal = netAmount[index]
            }


            for (var m = 0; m < rows.length; m++) {
                if (rows[m].rowId == id) {
                    rows[m].otb = minOtb - netAmtFinal
                }
            }


            this.setState({
                rows: rows
            })

        }

        setTimeout(() => {
            this.setOtbAfterDelete(id)
        }, 10)

    }
    onChangeNetAmt(id, articleCode, mrp) {

        let rows = this.state.rows
        let otb = ""
        let netAmount = ""
        let idd = id


        for (var i = 0; i < rows.length; i++) {
            if (rows[i].rowId == idd) {
                otb = rows[i].otb
                netAmount = rows[i].netAmountTotal

            }
            if (rows[i].rowId > idd) {

                if (rows[i].articleCode == articleCode && rows[i].vendorMrp == mrp) {
                    rows[i].otb = otb - netAmount,
                        // idd = rows[i].rowId
                        // if(rows[i].rowId==idd){
                        otb = otb - netAmount
                    netAmount = rows[i].netAmountTotal

                    // }
                }

            }
        }
        this.setState({
            rows: rows
        })

    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.purchaseIndent.hsnCode.isSuccess && this.props.onFieldHsn == true) {
            if (nextProps.purchaseIndent.hsnCode.data.resource != null) {
                let rows = this.state.rows
                for (let i = 0; i < rows.length; i++) {
                    if (rows[i].rowId == nextProps.purchaseIndent.hsnCode.data.rowId) {
                        rows[i].hsnSacCode = nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNSACCode,
                            rows[i].hsnCode = nextProps.purchaseIndent.hsnCode.data.defaultHSNKEy.defaultHSNCode

                    }
                }
                this.setState({
                    rows: rows
                })
                this.props.updateHsnvalue()
            }
            else {

                let rows = this.state.rows
                for (let i = 0; i < rows.length; i++) {
                    if (rows[i].rowId == nextProps.purchaseIndent.hsnCode.data.rowId) {
                        rows[i].hsnSacCode = ""
                        rows[i].hsnCode = ""

                    }
                }
                this.setState({
                    rows: rows
                })
                this.props.updateHsnvalue()
            }
            this.props.hsnCodeClear()
        }
        if (nextProps.purchaseIndent.getCatDescUdf.isSuccess) {
            let piUdfData = []
            let c = []
            if (nextProps.purchaseIndent.getCatDescUdf.data.resource != null) {
                piUdfData = nextProps.purchaseIndent.getCatDescUdf.data.resource.cat_desc_udf
            }

            for (let j = 0; j < piUdfData.length; j++) {
                let x = piUdfData[j]
                x.value = "";

                c.push(x)

            }

            this.setState({

                udfItemHeadData: c,
            })


        }
        if (nextProps.purchaseIndent.getUdfMapping.isSuccess) {
            if (nextProps.purchaseIndent.getUdfMapping.data.resource != null) {
                let setUdfSettingHead = nextProps.purchaseIndent.getUdfMapping.data.resource
                let c = []
                setTimeout(() => {

                    for (let j = 0; j < setUdfSettingHead.length; j++) {
                        let x = setUdfSettingHead[j]
                        x.value = "";

                        c.push(x)

                    }

                    this.setState({
                        setUdfRowsData: c,
                    })
                }, 10)
            }
        }
        if (nextProps.purchaseIndent.marginRule.isSuccess) {
            if (nextProps.purchaseIndent.marginRule.data.resource != null) {
                let rows = this.state.rows
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].rowId == nextProps.purchaseIndent.marginRule.data.rowId) {
                        rows[i].marginRule = nextProps.purchaseIndent.marginRule.data.resource
                        this.setState({
                            rows: rows
                        })
                    }
                }
            } else {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: "Margin rule is not available for the corresponding Article and Supplier"
                })
            }
            this.props.marginRuleClear();
        }
        if (nextProps.purchaseIndent.quantity.isSuccess) {
            if (nextProps.purchaseIndent.quantity.data.resource != null) {
                let c = this.state.rows
                let poQuantity = []
                var total = 0;
                for (var x = 0; x < c.length; x++) {
                    if (c[x].rowId == nextProps.purchaseIndent.quantity.data.rowId) {
                        c[x].quantity = nextProps.purchaseIndent.quantity.data.resource.qty

                        this.setState({
                            rows: c
                        })
                    }
                    poQuantity.push(c[x].quantity);
                }
                for (var i = 0; i < poQuantity.length; i++) {
                    if (isNaN(poQuantity[i])) {
                        continue;
                    }
                    total += Number(poQuantity[i]);
                }
            }
            this.setState({
                poQuantity: total
            })
            this.updateQuantity(nextProps.purchaseIndent.quantity.data.rowId);
            this.props.getPiQuantity(total)
            this.props.quantityClear();
        }
        if (nextProps.purchaseIndent.lineItem.isSuccess) {
            if (nextProps.purchaseIndent.lineItem.data.resource != null) {
                let c = this.state.rows
                let poAmount = []
                var total = 0;
                const t = this;
                for (var x = 0; x < c.length; x++) {
                    if (c[x].rowId == nextProps.purchaseIndent.lineItem.data.rowId) {
                        c[x].gst = nextProps.purchaseIndent.lineItem.data.resource.gst != undefined ? nextProps.purchaseIndent.lineItem.data.resource.gst.toString() : "",
                            c[x].netAmountTotal = nextProps.purchaseIndent.lineItem.data.resource.netAmount
                        c[x].tax = nextProps.purchaseIndent.lineItem.data.resource.tax,
                            c[x].finCharges = nextProps.purchaseIndent.lineItem.data.resource.finCharges
                        c[x].basic = nextProps.purchaseIndent.lineItem.data.resource.basic
                        c[x].totalAmount = Number(nextProps.purchaseIndent.lineItem.data.resource.netAmount)
                        setTimeout(function () {
                            t.setState({
                                rows: c,
                                taxx: nextProps.purchaseIndent.lineItem.data.resource.tax,
                                chargesData: nextProps.purchaseIndent.lineItem.data.resource.finCharges
                            }, () => {
                                for (var x = 0; x < c.length; x++) {
                                    poAmount.push(c[x].netAmountTotal)
                                }

                                for (var i = 0; i < poAmount.length; i++) {
                                    if (isNaN(poAmount[i])) {
                                        continue;
                                    }
                                    total += Number(poAmount[i]);
                                }

                                t.setState({
                                    poAmount: total
                                })
                            })
                            t.props.getPiAmount(total);
                        }, 10)
                        this.props.isMrpRequired ? this.onChangeNetAmt(nextProps.purchaseIndent.lineItem.data.rowId, nextProps.purchaseIndent.lineItem.data.articleCode, nextProps.purchaseIndent.lineItem.data.mrp) : null
                    }
                }



                this.props.isRspRequired ? this.updateMarkUp(nextProps.purchaseIndent.lineItem.data.rowId) : null


            }
            this.props.lineItemClear();



        }
        if (nextProps.purchaseIndent.otb.isSuccess) {
            if (nextProps.purchaseIndent.otb.data.resource != null) {

                let c = this.state.rows
                for (var x = 0; x < c.length; x++) {


                    if (c[x].rowId == nextProps.purchaseIndent.otb.data.rowId) {
                        //    if( c.length==1 || c[x].articleCode!=)
                        {

                            c[x].otb = nextProps.purchaseIndent.otb.data.resource.otb.toFixed(2)
                            this.setState({
                                rows: c
                            })




                        }


                    }
                }
            }
            setTimeout(() => {
                this.getOtb(nextProps.purchaseIndent.otb.data.rowId)
            }, 1)
            this.props.otbClear()
        }
        if (nextProps.purchaseIndent.markUp.isSuccess) {
            if (nextProps.purchaseIndent.markUp.data.resource != null) {
                let c = this.state.rows
                for (var x = 0; x < c.length; x++) {
                    if (c[x].rowId == nextProps.purchaseIndent.markUp.data.rowId) {
                        c[x].intakeMargin = nextProps.purchaseIndent.markUp.data.resource.actualMarkUp
                        c[x].calculateMargin = nextProps.purchaseIndent.markUp.data.resource.calculatedMargin

                        this.setState({
                            rows: c
                        })
                    }
                }
            }
        }
        if (nextProps.purchaseIndent.size.isSuccess) {
            var array = this.state.c;
            if (nextProps.purchaseIndent.size.data.resource != null) {
                let sdata = [];
                for (let i = 0; i < nextProps.purchaseIndent.size.data.resource.length; i++) {
                    let x = nextProps.purchaseIndent.size.data.resource[i];

                    x.ratio = ""
                    let a = x
                    sdata.push(a)
                }
                this.setState({
                    sizeNewData: sdata
                })
            }
        }
        if (nextProps.purchaseIndent.getItemDetails.isSuccess) {
            if (nextProps.purchaseIndent.getItemDetails.data.resource != null) {
                let cdata = [];
                for (let i = 0; i < nextProps.purchaseIndent.getItemDetails.data.resource.length; i++) {
                    let x = nextProps.purchaseIndent.getItemDetails.data.resource[i];
                    x.value = "";
                    x.code = ""
                    let a = x
                    cdata.push(a)
                }
                this.setState({
                    itemDetailsHeader: cdata
                })
            }
        }
        if (nextProps.purchaseIndent.piCreate.isSuccess) {
            this.resetData();
            this.props.leadTimeClear();
            this.props.lineItemClear();
            this.props.markUpClear();
            this.props.otbClear();
            this.props.purchaseTermClear();
        }
        if (nextProps.flag > this.state.flag1) {
            this.setState(
                {
                    rows: [{
                        articleCode: "",
                        articleName: "",
                        vendorMrp: "",
                        typeOfBuy: "",
                        vendorDesign: "",
                        image: [],
                        showImage: "",
                        imageUrl: {},
                        containsImage: false,
                        rsp: "",
                        mrp: "",
                        finalRate: "",
                        discount: {
                            discountType: "",
                            discountValue: ""
                        },
                        rate: "",
                        size: [],
                        sizeList: [],
                        ratio: [],
                        color: [],
                        colorList: [],
                        numOfsets: "",
                        date: "",
                        remarks: "",
                        quantity: "",
                        netAmountTotal: "",
                        calculateMargin: "",
                        gst: "",
                        otb: "",
                        intakeMargin: "",
                        mrpStart: "",
                        mrpEnd: "",
                        rowId: 0,
                        hsnSacCode: "",
                        hsnCode: "",
                        tax: "",
                        finCharges: [],
                        basic: "",
                        marginRule: "",
                        totalAmount: "",
                        iCode: "",
                    }],

                    itemDetails: [],
                    flag1: nextProps.flag,
                    gridSecond: false
                })
            // document.getElementById("date").placeholder = ""
        }
    }
    handleChange(id, e) {
        let r = this.state.rows;

        if (e.target.id == "vendorDesign") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == id) {
                    r[i].vendorDesign = e.target.value
                }
            }
            this.setState(
                {
                    rows: r
                }
            )
            setTimeout(() => {

                this.groupByFunctions()
            }, 100)

        } else if (e.target.id == "rate") {

            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == id) {
                    if (e.target.validity.valid) {
                        r[i].rate = e.target.value

                        if (Number(r[i].rate) > Number(r[i].mrp)) {

                            this.setState({
                                rateDisable: true
                            })

                        } else {
                            this.setState({
                                rateDisable: false
                            })
                        }

                    }
                }
            }

            this.setState(
                {
                    rows: r
                }
            )
        } else if (e.target.id == "numOfsets") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == id) {
                    if (e.target.validity.valid) {
                        r[i].numOfsets = e.target.value
                        if (r[i].numOfsets > 500) {
                            this.setState({
                                rateDisable: true
                            })
                        } else {
                            this.setState({
                                rateDisable: false
                            })
                        }
                    }
                }
            }
            this.setState(
                {
                    rows: r
                }
            )
        } else if (e.target.id == "date") {
            // e.target.placeholder = e.target.value;
            for (let i = 0; i < r.length; i++) {

                if (r[i].rowId == id) {
                    r[i].date = e.target.value
                }
            }
            this.setState(
                {
                    rows: r
                }
            )
            setTimeout(() => {

                this.groupByFunctions()
            }, 100)
        } else if (e.target.id == "remarks") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == id) {
                    r[i].remarks = e.target.value
                }
            }
            this.setState(
                {
                    rows: r
                }
            )

        } else if (e.target.id == "mrp") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == id) {
                    if (e.target.validity.valid) {
                        r[i].mrp = e.target.value
                    }
                }
            }
            this.setState(
                {
                    rows: r
                }
            )

        } else if (e.target.id == "typeofBuying") {
            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == id) {

                    r[i].typeOfBuy = e.target.value

                }
            }
            this.setState(
                {
                    rows: r
                }
            )
        }
    };
    handleAddRow() {
        var array = this.state.rows;
        var idd = []
        for (var i = 0; i < array.length; i++) {
            idd.push(array[i].rowId)
        }
        var finalId = Math.max(...idd);
        let x = false;
        if (array.length > 0) {
            for (var i = 0; i < array.length; i++) {

                if (array[i].articleCode != "") {

                    x = true;
                } else {

                    x = false
                }

            }
            if (x) {

                this.fun(finalId);
            }
        } else {
            this.fun(finalId);
        }

    };

    fun(finalId) {
        const id = finalId + 1
        const item = {
            articleCode: "",
            articleName: "",
            vendorMrp: "",
            typeOfBuy: "",
            iCode: "",
            vendorDesign: "",
            image: [],
            showImage: "",
            imageUrl: {},
            containsImage: false,
            rsp: "",
            mrp: "",
            finalRate: "",
            discount: {
                discountType: "",
                discountValue: ""
            },
            rate: "",
            size: [],
            sizeList: [],
            ratio: [],
            color: [],
            colorList: [],
            numOfsets: "",
            date: "",
            remarks: "",
            quantity: "",
            netAmountTotal: "",
            calculateMargin: "",
            gst: "",
            otb: "",
            intakeMargin: "",
            mrpStart: "",
            mrpEnd: "",
            rowId: 0,
            hsnSacCode: "",
            hsnCode: "",
            tax: "",
            finCharges: [],
            basic: "",
            marginRule: "",
            totalAmount: ""
        };
        this.setState({
            rows: [...this.state.rows, item],
        });
    }

    handleRemoveSpecificRow(idx, articleCode, mrp, otb) {

        const rows = this.state.rows
        if (rows.length > 1) {
            this.props.confirmModal(idx)
            this.setState({
                deleteArticleCode: articleCode,
                deleteMrp: mrp,
                deleteOtb: otb
            })
        } else {
            this.setState({
                toastMsg: "Can't delete single row",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500)
        }
    }

    deleteRows(idx) {

        let rows = this.state.rows

        if (rows.length > 1) {
            for (var z = 0; z < rows.length; z++) {
                if (rows[z].rowId == idx) {
                    rows.splice(z, 1)
                }
            }
        }



        this.setOtbAfterDelete(idx)
        setTimeout(() => {
            this.groupByFunctions();
        }, 100)
    }
    setOtbAfterDelete(idx) {
        let otb = ""
        let netAmount = ""
        let rows = this.state.rows
        for (let j = 0; j < rows.length; j++) {
            if (rows[j].rowId > idx) {
                if (rows[j].articleCode == this.state.deleteArticleCode && rows[j].vendorMrp == this.state.deleteMrp) {
                    let finalotb = ""
                    if (otb == "") {
                        finalotb = this.state.deleteOtb
                    } else {
                        finalotb = otb - netAmount
                    }

                    rows[j].otb = finalotb
                    otb = finalotb

                    // idd = rows[i].rowId
                    // if(rows[i].rowId==idd){

                    netAmount = rows[j].netAmountTotal
                }
            }
        }
        this.setState({
            rows: rows,
            deleteArticleCode: "",

            deleteMrp: "",
            deleteOtb: ""

        })
    }

    openImageModal(code, id) {

        if (code == "" || code == undefined) {
            this.setState({
                toastMsg: "Select Vendor and Vendor MRP",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            let rows = this.state.rows
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].rowId == id) {
                    this.setState({
                        imageState: rows[i].imageUrl
                    })
                }
            }
            this.setState({
                imageRowId: id,
                imageModal: true,
                imageModalAnimation: !this.state.imageModalAnimation
            });
        }
    }

    closePiImageModal() {
        this.setState({
            imageModal: false,
            imageModalAnimation: !this.state.imageModalAnimation
        });
    }

    openMrpModal(id, idx, iCode, articleCode, mrp, otb, articleName) {
        if (this.props.supplier == "" || this.props.department == "") {
            this.setState({
                toastMsg: "Select Vendor and Department",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            if (this.props.isArticleSeparated) {
                if (articleCode != "") {
                    var data = {
                        code: articleCode,
                        search: "",
                        type: 1,
                        no: 1,
                        mrpRangeFrom: this.state.startRange,
                        mrpRangeTo: this.state.endRange
                    }
                    this.props.poItemcodeRequest(data)

                    this.setState({
                        deleteArticleCode: articleCode,
                        deleteOtb: otb,
                        newArticleName: articleName,
                        iCode: iCode,
                        rowIdentity: id,
                        desc6: mrp,
                        descId: id,
                        itemModal: true,
                        itemModalAnimation: !this.state.itemModalAnimation,
                        focusId: "vendorMrp" + id
                    })
                } else {
                    this.setState({
                        toastMsg: "Select Article Code",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1000)
                }
            } else {
                var data = {
                    code: this.props.slcode,
                    type: "",
                    no: 1,
                    articleName: "",
                    mrp: "",
                    rsp: "",
                    department: this.props.department
                }
                this.props.vendorMrpRequest(data)

                this.setState({
                    focusId: "vendorMrp" + id,
                    deleteArticleCode: articleCode,
                    deleteMrp: mrp,
                    deleteOtb: otb,
                    idx: idx,
                    iCode: iCode,
                    rowIdentity: id,
                    mrpModal: true,
                    mrpModalAnimation: !this.state.mrpModalAnimation
                });
                this.poSearch("vendorMrp"+id, "vendorMrpNew"+id)
            }
        }
    }
    openPiSizeModal(code, id, sizeValue, ratioValue, articleName, sizeList) {

        let sizeArray = []
        if (code == "" || code == undefined) {
            this.setState({
                toastMsg: "Select Vendor and Vendor MRP",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            if (sessionStorage.getItem("partnerEnterpriseCode") == "STYLEBAZAAR") {
                let sizeRatioArray = []
                if (articleName != "") {
                    if (sizeValue != "") {
                        sizeValue = sizeValue.split(',')
                        ratioValue = ratioValue.split(',')

                        for (var i = 0; i < sizeValue.length; i++) {
                            for (var j = 0; j < ratioValue.length; j++) {

                                if (i == j) {
                                    let data = {
                                        size: sizeValue[i],
                                        ratio: ratioValue[j]
                                    }
                                    sizeArray.push(data)
                                }
                            }
                        }
                    }
                    if (sizeList.length != 0) {
                        for (var i = 0; i < sizeList.length; i++) {
                            for (var j = 0; j < ratioValue.length; j++) {

                                if (i == j) {
                                    sizeList[i].ratio = ratioValue[j]

                                }
                            }
                        }

                    }
                    var dataColor = {
                        id: id,
                        // code: code,
                        type: "",
                        no: 1,
                        search: "",
                        hl3Name: this.props.department,
                        hl1Name: this.props.division,

                        hl2Name: this.props.section,

                        hl3Code: this.props.hl3Code,
                        hl4Name: articleName,

                        itemType: 'size'

                    }


                    this.props.sizeRequest(dataColor)
                    this.setState({
                        sizes: sizeValue == "" ? [] : sizeValue,
                        sizeArray: sizeArray,
                        sizeValue: sizeValue == "" ? [] : sizeValue,
                        ratioValue: ratioValue == "" ? [] : ratioValue,
                        sizeRow: id,
                        sizeCode: code,
                        sizeModal: true,
                        articleName: articleName,
                        sizeModalAnimatiion: !this.state.sizeModalAnimatiion,
                        sizeDataList: sizeList,
                        focusId: "size" + id
                    });
                } else {
                    this.setState({
                        toastMsg: "Select Article",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1000)
                }

            } else {
                let sizeRatioArray = []
                if (sizeValue != "") {
                    sizeValue = sizeValue.split(',')
                    ratioValue = ratioValue.split(',')

                    for (var i = 0; i < sizeValue.length; i++) {
                        for (var j = 0; j < ratioValue.length; j++) {

                            if (i == j) {
                                let data = {
                                    size: sizeValue[i],
                                    ratio: ratioValue[j]
                                }
                                sizeArray.push(data)
                            }
                        }
                    }
                }
                if (sizeList.length != 0) {
                    for (var i = 0; i < sizeList.length; i++) {
                        for (var j = 0; j < ratioValue.length; j++) {

                            if (i == j) {
                                sizeList[i].ratio = ratioValue[j]

                            }
                        }
                    }

                }

                var dataColor = {
                    id: id,
                    // code: code,
                    type: "",
                    no: 1,
                    search: "",
                    hl3Name: this.props.department,
                    hl1Name: "",

                    hl2Name: "",

                    hl3Code: this.props.hl3Code,
                    hl4Name: "",

                    itemType: 'size'

                }
                this.props.sizeRequest(dataColor)

                this.setState({
                    sizes: sizeValue == "" ? [] : sizeValue,
                    sizeArray: sizeArray,
                    sizeValue: sizeValue == "" ? [] : sizeValue,
                    ratioValue: ratioValue == "" ? [] : ratioValue,
                    sizeRow: id,
                    sizeCode: code,
                    articleName: "",
                    sizeModal: true,
                    sizeModalAnimatiion: !this.state.sizeModalAnimatiion,
                    sizeDataList: sizeList

                });

            }
        }
    }
    //closePIsize
    closePiSizeModal() {
        this.setState({
            sizeModal: false,
            sizeModalAnimatiion: !this.state.sizeModalAnimatiion
        });
        document.getElementById(this.state.focusId).focus()
    }
    //pi-color
    openPiColorModal(code, id, checkValue, colorList) {

        if (code == "" || code == undefined) {
            this.setState({
                toastMsg: "Select Vendor and Vendor MRP",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        }
        else {
            var dataColor = {
                id: id,
                type: "",
                no: 1,
                search: "",
                hl3Name: this.props.department,
                hl3Code: this.props.hl3Code,
                itemType: 'color',
                colorValue: checkValue == "" ? [] : checkValue.split(',')
            }
            this.props.colorRequest(dataColor)
            this.setState({
                colorValue: checkValue == "" ? [] : checkValue.split(','),
                colorRow: id,
                colorCode: code,
                colorModal: true,
                colorModalAnimation: !this.state.colorModalAnimation,
                colorListValue: colorList == "" ? [] : colorList,
                focusId: "color" + id
            });
        }
    }
    deselectallProp(data) {

        this.setState({
            colorValue: data.value
        })
    }
    closePiColorModal() {
        this.setState({
            colorModal: false,
            colorModalAnimation: !this.state.colorModalAnimation
        });
        document.getElementById(this.state.focusId).focus()
    }

    updateItem(data) {
        var rId = data.rId
        var array = this.state.rows

        for (var x = 0; x < array.length; x++) {
            if (array[x].rowId == data.rId) {


                array[x].mrp = data.mrp;

                array[x].rsp = data.mrp;
                array[x].vendorMrp = data.mrp;
                array[x].iCode = data.iCode;
                array[x].image = [];
                array[x].imageUrl = {};
                array[x].containsImage = false
                array[x].rate = "";
                array[x].finalRate = "",
                    array[x].discount = {
                        discountType: "",
                        discountValue: ""
                    },
                    array[x].numOfsets = "";
                array[x].date = "";
                array[x].remarks = "";
                array[x].quantity = "";
                array[x].netAmountTotal = "";
                array[x].calculateMargin = "";
                array[x].gst = "";
                array[x].otb = "";
                array[x].intakeMargin = "";
                array[x].vendorDesign = "";
                array[x].typeOfBuy = "";
                array[x].totalAmount = "";
                array[x].marginRule = "";
                array[x].finCharges = [];
                array[x].tax = "";
                array[x].basic = "";

            }

        }
        this.setState({
            rows: array,
            // hsnSacCode: data.hsnSacCode
        })
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        const d = new Date();
        var monthName = (monthNames[d.getMonth()]);
        var dataa = {
            rowId: data.rId,
            articleCode: data.articleCode,
            mrp: data.mrp,
            month: monthName,
            year: d.getFullYear()
        }
        { this.props.isMrpRequired ? this.props.otbRequest(dataa) : null }
        setTimeout(() => {
            this.groupByFunctions()
        }, 100)

        // _____________________________MARGINRULE API_________________________

        let marginData = {
            slCode: this.props.slcode,
            articleCode: data.articleCode,
            rId: data.rId,
            siteCode: this.props.siteCode
        }
        this.props.marginRuleRequest(marginData)
        document.getElementById(this.state.focusId).focus()


    }

    updateMrpState(data) {
        var rId = data.rId
        var id = data.id;
        var array = this.state.rows

        for (var x = 0; x < array.length; x++) {
            if (array[x].rowId == rId) {
                array[x].articleCode = data.articleCode;
                array[x].articleName = data.articleName;
                array[x].mrp = data.mrp.toString();
                array[x].mrpEnd = data.mrpEnd.toString();
                array[x].mrpStart = data.mrpStart.toString();
                array[x].rsp = data.rsp.toString();
                array[x].vendorMrp = data.mrp.toString();
                // array[x].hsnSacCode = data.hsnSacCode;
                array[x].iCode = data.id;
                array[x].image = [];
                array[x].imageUrl = {};
                array[x].containsImage = false
                array[x].rate = "";
                array[x].finalRate = "",
                    array[x].discount = {
                        discountType: "",
                        discountValue: ""
                    },
                    array[x].numOfsets = "";
                array[x].date = "";
                array[x].remarks = "";
                array[x].quantity = "";
                array[x].netAmountTotal = "";
                array[x].calculateMargin = "";
                array[x].gst = "";
                array[x].otb = "";
                array[x].intakeMargin = "";
                array[x].vendorDesign = "";
                array[x].typeOfBuy = "";
                array[x].totalAmount = "";
                array[x].marginRule = "";
                array[x].finCharges = [];
                array[x].tax = "";
                array[x].basic = "";
                array[x].color = this.props.isColorRequired ? array[x].color : ["NA"]
                array[x].colorList = this.props.isColorRequired ? array[x].colorList : [{ id: 1, code: "", cname: "NA" }]

            }

        }
        this.setState({
            rows: array,
            // hsnSacCode: data.hsnSacCode
        })
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        const d = new Date();
        var monthName = (monthNames[d.getMonth()]);
        var dataa = {
            rowId: rId,
            articleCode: data.articleCode,
            mrp: data.mrp,
            month: monthName,
            year: d.getFullYear()
        }
        { this.props.isMrpRequired ? this.props.otbRequest(dataa) : null }
        setTimeout(() => {
            this.groupByFunctions()
        }, 100)

        // _____________________________MARGINRULE API_________________________

        let marginData = {
            slCode: this.props.slcode,
            articleCode: data.articleCode,
            rId: rId,
            siteCode: this.props.siteCode
        }
        this.props.marginRuleRequest(marginData)
        document.getElementById(this.state.focusId).focus()

        // document.getElementById("date").placeholder = ""
    }
    mrpCloseModal() {

        this.setState({

            mrpModal: false,
            mrpModalAnimation: !this.state.mrpModalAnimation
        })
        document.getElementById(this.state.focusId).focus()

    }
    getQuantity(id) {
        let c = this.state.rows;
        let numOfColor = "";
        let ratioo = "";
        let set = "";
        this.setState({
            quantityId: id
        })
        for (var i = 0; i < c.length; i++) {
            if (c[i].rowId == id) {

                numOfColor = this.props.isColorRequired ? c[i].color.split(',').length : 1
                ratioo = c[i].ratio,
                    set = c[i].numOfsets
            }
        }
        let quantityData = {
            numOfColor: numOfColor,
            ratioo: ratioo,
            set: set,
            id: id
        }
        this.props.quantityRequest(quantityData)
    }
    updateQuantity(id) {
        let c = this.state.rows;
        let taxData = {}
        for (var i = 0; i < c.length; i++) {
            if (c[i].rowId == id) {
                if (c[i].hsnSacCode != "" && Number(c[i].hsnSacCode) != 0) {
                    taxData = {
                        hsnSacCode: c[i].hsnSacCode,
                        qty: c[i].quantity,
                        rate: c[i].finalRate == "" ? 1 : c[i].finalRate,
                        rowId: c[i].rowId,
                        designRow: "",
                        articleCode: c[i].articleCode,
                        mrp: c[i].vendorMrp,
                        piDate: new Date(),
                        supplierGstinStateCode: this.props.stateCode,
                        purtermMainCode: this.props.purtermMainCode,
                    }
                    this.props.lineItemRequest(taxData)
                } else {
                    this.setState({
                        toastMsg: "HSN is Zero or not selected",
                        toastLoader: true
                    })
                    const t = this
                    setTimeout(function () {
                        t.setState({
                            toastLoader: false
                        })
                    }, 1000)
                }



            }

        }

    }
    updateMarkUp(id) {
        let c = this.state.rows;
        let markUpData = {}
        for (var i = 0; i < c.length; i++) {
            if (c[i].rowId == id) {
                markUpData = {

                    rate: c[i].finalRate == "" ? 1 : c[i].finalRate,
                    rowId: c[i].rowId,
                    articleCode: c[i].articleCode,
                    gst: c[i].gst,
                    rsp: this.props.isRSP ? c[i].rsp : c[i].vendorMrp,
                    designRow: ""
                }

            }
        }
        this.props.markUpRequest(markUpData)
    }
    updateSizeState(data1, sizeList) {

        var cat5 = data1.catFive;
        var catFiveRatio = data1.cat5Ratio;
        var array = this.state.rows

        for (var x = 0; x < array.length; x++) {
            if (array[x].rowId == data1.sizerId) {
                array[x].size = cat5.join(',');
                array[x].ratio = catFiveRatio.join(',');
                if (array[x].size != "" && array[x].ratio != "" && array[x].numOfsets != "" && array[x].color != "" && array[x].rate != "" && this.props.purtermMainCode != "") {
                    this.getQuantity(data1.sizerId);
                }
            }

        }
        this.setState({
            rows: array
        })
        document.getElementById(this.state.focusId).focus()

    }
    copyIndentDescription(id) {
        this.props.onSubmit()
        let addedRow = []
        let otb = this.state.otb
        let netAmountTotal = ""
        let rows = this.state.rows
        let articleMrpArray = this.state.articleMrpArray
        if (this.state.active == false) {
            setTimeout(() => {

                const { transporterr, siteNameerr, leadDayserr, vendorerr, divisionerr, departmenterr, sectionerr } = this.props;
                if (!transporterr && !siteNameerr && !leadDayserr && !vendorerr && !divisionerr && !departmenterr && !sectionerr) {

                    for (let k = 0; k < rows.length; k++) {
                        if (articleMrpArray.length == 0) {

                            let array = {
                                articleMrp: rows[k].articleCode + "-" + rows[k].vendorMrp,
                                netAmountTotal: rows[k].netAmountTotal,
                                otb: rows[k].otb - rows[k].netAmountTotal

                            }
                            otb[rows[k].articleCode + "-" + rows[k].vendorMrp] = rows[k].otb - rows[k].netAmountTotal
                            articleMrpArray.push(array)
                            this.setState({
                                articleMrpArray: articleMrpArray,
                                otb: otb

                            })
                        } else {
                            for (var j = 0; j < articleMrpArray.length; j++) {
                                if (rows[k].rowId == id) {
                                    if (articleMrpArray[j].articleMrp == (rows[k].articleCode + "-" + rows[k].vendorMrp)) {

                                        articleMrpArray[j].netAmountTotal = rows[k].netAmountTotal + netAmountTotal
                                        articleMrpArray[j].otb = rows[k].netAmountTotal + netAmountTotal

                                        otb[rows[k].articleCode + "-" + rows[k].vendorMrp] = otb[rows[k].articleCode + "-" + rows[k].vendorMrp] - (rows[k].netAmountTotal + netAmountTotal)

                                        this.setState({
                                            articleMrpArray: articleMrpArray,
                                            otb: otb
                                        })

                                    } else {

                                        let array = {
                                            articleMrp: rows[k].articleCode + "-" + rows[k].vendorMrp,
                                            netAmountTotal: rows[k].netAmountTotal,
                                            otb: rows[k].otb - rows[k].netAmountTotal

                                        }
                                        otb[rows[k].articleCode + "-" + rows[k].vendorMrp] = rows[k].otb - rows[k].netAmountTotal
                                        articleMrpArray.push(array)
                                        this.setState({
                                            articleMrpArray: articleMrpArray,
                                            otb: otb
                                        })
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    let rowids = []
                    let finalId = ""

                    for (let i = 0; i < rows.length; i++) {
                        rowids.push(rows[i].rowId)

                    }
                    finalId = Math.max(...rowids);
                    for (let i = 0; i < rows.length; i++) {
                        if (rows[i].rowId == id) {
                            if (rows[i].vendorDesign != "" || this.props.isVendorDesignNotReq) {
                                if (rows[i].netAmountTotal != "") {

                                    addedRow = {
                                        articleCode: rows[i].articleCode,
                                        articleName: rows[i].articleName,
                                        vendorMrp: rows[i].vendorMrp,
                                        typeOfBuy: rows[i].typeOfBuy,
                                        vendorDesign: sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? "" : rows[i].vendorDesign,
                                        image: [],

                                        showImage: "",
                                        imageUrl: {},
                                        containsImage: false,
                                        rsp: rows[i].rsp,
                                        mrp: rows[i].mrp,
                                        rate: rows[i].rate,
                                        finalRate: rows[i].finalRate,
                                        discount: rows[i].discount,
                                        size: rows[i].size,
                                        sizeList: rows[i].sizeList,
                                        ratio: rows[i].ratio,
                                        color: rows[i].color,
                                        colorList: rows[i].colorList,
                                        numOfsets: rows[i].numOfsets,
                                        date: rows[i].date,
                                        remarks: rows[i].remarks,
                                        quantity: rows[i].quantity,
                                        netAmountTotal: rows[i].netAmountTotal,
                                        calculateMargin: rows[i].calculateMargin,
                                        gst: rows[i].gst,
                                        otb: otb[rows[i].articleCode + "-" + rows[i].vendorMrp],
                                        intakeMargin: rows[i].intakeMargin,
                                        mrpStart: rows[i].mrpStart == 0 || rows[i].mrpStart == "" ? 0 : rows[i].mrpStart,
                                        mrpEnd: rows[i].mrpEnd == 0 || rows[i].mrpEnd == "" ? 0 : rows[i].mrpEnd,
                                        rowId: finalId + 1,
                                        marginRule: rows[i].marginRule,
                                        tax: rows[i].tax,
                                        finCharges: rows[i].finCharges,
                                        basic: rows[i].basic,
                                        totalAmount: rows[i].totalAmount,
                                        hsnSacCode: rows[i].hsnSacCode,
                                        iCode: rows[i].iCode,
                                    }

                                    this.setState({
                                        rows: [...this.state.rows, addedRow],
                                        active: true
                                    });
                                    setTimeout(() => {
                                        this.updatePiAmount()
                                    }, 10)

                                }
                                else {
                                    this.setState({
                                        poErrorMsg: true,
                                        errorMassage: "Net Amount is mandatory to copy indent"
                                    })
                                }
                            } else {
                                this.setState({
                                    poErrorMsg: true,
                                    errorMassage: "VendorDesign is mandatory to copy indent"
                                })
                            }

                        }
                    }

                }


            }, 10)
        } else {
            this.setState({
                poErrorMsg: true,
                errorMassage: "Data Copied....."
            })
        }



    }



    updatePiAmount() {

        setTimeout(() => {
            let rows = this.state.rows
            let poAmount = []
            let total = 0
            let poQuantity = []
            let totalPiQuantity = 0
            for (let i = 0; i < rows.length; i++) {
                poAmount.push(rows[i].netAmountTotal);
                poQuantity.push(rows[i].quantity);
            }
            for (var j = 0; j < poAmount.length; j++) {
                if (isNaN(poAmount[j])) {
                    continue;
                }
                total += Number(poAmount[j]);
            }
            this.setState({
                poAmount: total,


            })
            for (var k = 0; k < poQuantity.length; k++) {
                if (isNaN(poQuantity[k])) {
                    continue;
                }
                totalPiQuantity += Number(poQuantity[k]);
            }
            this.props.getPiAmount(total);
            this.props.getPiQuantity(totalPiQuantity)

        }, 10);
        setTimeout(() => {
            this.setState({
                active: false
            })

        }, 1000)

    }
    componentDidMount() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        var c = moment(new Date(year + 1, month, day)).format("YYYY-MM-DD")
        this.setState({
            maxDate: c,
            currentDate: getDate()
        })
      
    }
    
    resetData(e) {

        let rows = [{
            articleCode: "",
            articleName: "",
            vendorMrp: "",
            typeOfBuy: "",

            vendorDesign: "",
            image: [],
            showImage: "",
            imageUrl: {},
            containsImage: false,
            rsp: "",
            mrp: "",
            finalRate: "",
            discount: {
                discountType: "",
                discountValue: ""
            },
            rate: "",
            size: [],
            sizeList: [],
            ratio: [],
            color: [],
            colorList: [],
            iCode: "",
            numOfsets: "",
            date: "",
            remarks: "",
            quantity: "",
            netAmountTotal: "",
            calculateMargin: "",
            gst: "",
            otb: "",
            intakeMargin: "",
            mrpStart: "",
            mrpEnd: "",
            rowId: 0,
            hsnSacCode: "",
            hsnCode: "",
            tax: "",
            finCharges: [],
            basic: "",
            marginRule: "",
            totalAmount: ""
        }]


        setTimeout(() => {
            this.setState({
                rows: rows,
                setUdfRowsData: [],
                udfItemHeadData: [],
                itemDetailsHeader: [],
                itemDetails: [],
            })

            this.props.otbClear();

            this.props.resetForm();
        }, 10)
        // document.getElementById("date").value = ""
        // document.getElementById("date").placeholder = ""

    }


    calculatedMargin(marginId, marginLeft) {
        console.log(marginId, marginLeft)
        let marginRate = document.getElementById(marginId)
        var viewportOffset = marginRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - (marginId.length <= 8 ? 345 : 175);


        if (!document.getElementById(marginLeft)) {
        } else {
            document.getElementById(marginLeft).style.left = left;
            document.getElementById(marginLeft).style.top = top;
        }
    }

    poSearch(marginId, marginLeft) {
        console.log(marginId, marginLeft)
        let marginRate = document.getElementById(marginId)
        var viewportOffset = marginRate.getBoundingClientRect();
        var top = viewportOffset.top + 40;
        var left = viewportOffset.left - (marginId.length <= 8 ? 345 : 175);
                
        
        if (!document.getElementById(marginLeft)) {
        } else {
            this.setState((prevState, props) => ({
                 prevSearchLeft:prevState.prevSearchLeft,
                 prevSearchTop:prevState.prevSearchTop,
                 searchTop:top,
                 searchLeft:left,

                searchField: marginId,
                  searchModal:marginLeft,
      
                }));
            document.getElementById(marginLeft).style.left = left;
            document.getElementById(marginLeft).style.top = top;
        }
    }
    updseateImage(data) {
        let c = this.state.rows;
        for (var i = 0; i < c.length; i++) {
            if (c[i].rowId.toString() == data.imageRowId) {
                c[i].imageUrl = data.file
                c[i].image = Object.keys(data.file)
                c[i].containsImage = Object.keys(data.file).length != 0 ? true : false
            }

        }

        this.setState({
            rows: c,

        })
        document.getElementById(this.state.focusId).focus()

    }


    itemUdfUpdate(data) {


        let itemDetailsArr = this.state.itemDetails
        if (itemDetailsArr != undefined) {
            itemDetailsArr.forEach(iDetails => {

                if (iDetails.itemGridId == Number(data.itemUdfId)) {
                    iDetails.udfList.forEach(iList => {

                        if (iList.cat_desc_udf == data.itemUdfType) {
                            iList.value = data.value
                        }
                    })
                }
            })

        }
        this.setState({
            itemDetails: itemDetailsArr
        })

        document.getElementById(this.state.focusId).focus()
    }


    updateUdf(data) {

        let itemDetailsArr = this.state.itemDetails
        if (itemDetailsArr != undefined) {
            itemDetailsArr.forEach(iDetails => {

                if (iDetails.itemGridId == Number(data.udfRow)) {
                    iDetails.setUdfList.forEach(iList => {

                        if (iList.udfType == data.udfType) {
                            iList.value = data.name
                        }

                    })
                }
            })
        }

        this.setState({
            itemDetails: itemDetailsArr
        })
        document.getElementById(this.state.focusId).focus()

    }
    updateItemDetails(data) {


        let itemDetailsArr = this.state.itemDetails
        if (itemDetailsArr != undefined) {
            itemDetailsArr.forEach(iDetails => {

                if (iDetails.itemGridId == Number(data.checkedId)) {

                    iDetails.itemList.forEach(iList => {
                        if (iList.catdesc == data.type) {

                            iList.value = data.checkedData
                            iList.code = data.checkedCode
                        }
                    })
                }

            })
        }
        this.setState({
            itemDetails: itemDetailsArr
        })
        window.setTimeout(() => {
            document.getElementById(data.focusId).focus()
        }, 0)

        // setTimeout(() => {
        //     this.gridSecond()
        // }, 1000)
    }

    // ___________________________GROUPBYFUNCTION____________________
    // groupByFunctions() {

    //     var itemIndex = 1;
    //     let udfList = [];
    //     let setUdfList = [];
    //     let iDetails = [];
    //     let finalItemDetails = []
    //     let finalitemUdfDetails = []
    //     let finalSetDetails = []
    //     let udfItemHeadData = this.state.udfItemHeadData
    //     let itemDetailsHeader = this.state.itemDetailsHeader
    //     let setUdfRowsData = this.state.setUdfRowsData
    //     //_______________________ @Author Manoj_____________________________
    //     let updateItemDetils = this.state.itemDetails;
    //     finalItemDetails = iDetails,
    //         finalSetDetails = setUdfList,
    //         finalitemUdfDetails = udfList

    //     let dRows = []
    //     let a = false;
    //     let b = false;
    //     let c = false;
    //     let d = false;
    //     let count = 0;
    //     let listKey = [];

    //     let valueCount = this.state.rows.length;
    //     this.state.rows.forEach((r) => {
    //         let success = false;
    //         if (this.state.itemDetails.length != 0) {
    //             let breakCount = 0;
    //             this.state.itemDetails.forEach(udetails => {
    //                 if (breakCount <= valueCount && !success) {
    //                     if ((r.articleCode == udetails.articleCode) && (r.date == udetails.deliveryDate) && (r.vendorDesign == udetails.vendorDesign)) {
    //                         success = true;
    //                         if (udetails.itemList.length == 0) {
    //                             a = true;
    //                         } else {
    //                             // if(count==0)
    //                             iDetails = udetails.itemList;
    //                             d = false;
    //                         }
    //                         // if (this.props.isUDFExist == "true") {  
    //                             if (udetails.setUdfList.length == 0) {
    //                                 b = true;

    //                             } else {
    //                                 // if(count==0)
    //                                 setUdfList = udetails.setUdfList;
    //                                 d = false;
    //                             }
    //                             if (udetails.udfList.length == 0) {
    //                                 c = true;
    //                             } else {
    //                                 // if(count==0)
    //                                 udfList = udetails.udfList;
    //                                 d = false;
    //                             }
    //                         // }
    //                         count++;

    //                     } else {
    //                         d = true;

    //                     }
    //                     breakCount++;
    //                 }
    //             });
    //         } 
    //         if (this.state.itemDetails.length == 0 || a || d) {
    //             iDetails = [];
    //             itemDetailsHeader.forEach(f => {
    //                 let catDesc = {
    //                     catdesc: f.catdesc,
    //                     code: "",
    //                     displayName: f.displayName,
    //                     isLov: f.isLov,
    //                     isCompulsory: f.isCompulsory,
    //                     value: ""

    //                 }
    //                 iDetails.push(catDesc)
    //             })


    //         }

    //         // if (this.props.isUDFExist == "true") {
    //              udfList = [];
    //              setUdfList = [];
    //             if (this.state.itemDetails.length == 0 || b || d) {

    //                 udfItemHeadData.forEach(c => {
    //                     let item = {
    //                         cat_desc_udf: c.cat_desc_udf,
    //                         cat_desc_udf_type: c.cat_desc_udf_type,
    //                         displayName: c.displayName,
    //                         id: c.id,
    //                         isCompulsory: c.isCompulsory,
    //                         isLov: c.isLov,
    //                         orderBy: c.orderBy,
    //                         value: ""
    //                     }
    //                     udfList.push(item)
    //                 })
    //             }
    //             if (this.state.itemDetails.length == 0 || c || d) {

    //                 setUdfRowsData.forEach(d => {
    //                     let set = {
    //                         displayName: d.displayName,
    //                         id: d.id,
    //                         isCompulsary: d.isCompulsary,
    //                         isLov: d.isLov,
    //                         orderBy: d.orderBy,
    //                         udfType: d.udfType,
    //                         value: ""

    //                     }
    //                     setUdfList.push(set)
    //                 })
    //             }
    //         // }
    //         // if (this.props.isUDFExist == "true") {
    //             let demorows = {
    //                 articleCode: r.articleCode,
    //                 deliveryDate: r.date,
    //                 vendorDesign: r.vendorDesign,
    //                 itemGridId: itemIndex++,
    //                 itemList: iDetails,
    //                 setUdfList: setUdfList,
    //                 udfList: udfList
    //             }
    //             dRows.push(demorows)
    //         // } else {
    //         //     let demorows = {
    //         //         articleCode: r.articleCode,
    //         //         deliveryDate: r.date,
    //         //         vendorDesign: r.vendorDesign,
    //         //         itemGridId: itemIndex++,
    //         //         itemList: iDetails,

    //         //     }
    //         //     dRows.push(demorows)
    //         // }
    //     })


    //     let rowssss = _.map(
    //         _.uniq(
    //             _.map(dRows, function (obj) {

    //                 return JSON.stringify(obj);
    //             })
    //         ), function (obj) {
    //             return JSON.parse(obj);
    //         }
    //     );
    //     let finalRows = [];
    //     let indexPosition = 0;
    //     rowssss.forEach(data => {
    //         let key = data.articleCode + data.deliveryDate + data.vendorDesign.toUpperCase();
    //         if (finalRows.includes(key)) {
    //             delete rowssss[indexPosition];
    //         } else {
    //             finalRows.push(key);
    //         }
    //         indexPosition++;
    //     })

    //     this.setState({
    //         itemDetails: rowssss
    //     })

    // }
    groupByFunctions() {


        var itemIndex = 1;
        let udfList = [];
        let setUdfList = [];
        let iDetails = [];
        let finalItemDetails = []
        let finalitemUdfDetails = []
        let finalSetDetails = []


        let udfItemHeadData = this.state.udfItemHeadData
        let itemDetailsHeader = this.state.itemDetailsHeader
        let setUdfRowsData = this.state.setUdfRowsData
        // @Author Manoj
        let updateItemDetils = this.state.itemDetails;

        finalItemDetails = iDetails,
            finalSetDetails = setUdfList,
            finalitemUdfDetails = udfList




        let dRows = []

        let a = false;
        let b = false;
        let c = false;
        let d = false;
        let count = 0;
        let listKey = [];

        let valueCount = this.state.rows.length;
        this.state.rows.forEach((r) => {
            let success = false;
            if (this.state.itemDetails.length != 0) {
                let breakCount = 0;
                this.state.itemDetails.forEach(udetails => {
                    if (breakCount <= valueCount && !success) {
                        if ((r.articleCode == udetails.articleCode) && (r.date == udetails.deliveryDate) && (r.vendorDesign == udetails.vendorDesign)) {
                            success = true;
                            if (udetails.itemList.length == 0) {
                                a = true;
                            } else {
                                // if(count==0)
                                iDetails = udetails.itemList;
                                d = false;
                            }
                            if (udetails.setUdfList.length == 0) {
                                if (this.props.isUDFExist == 'true')
                                    b = false;
                                else
                                    b = true;

                            } else {
                                // if(count==0)
                                setUdfList = udetails.setUdfList;
                                d = false;
                            }
                            if (udetails.udfList.length == 0) {
                                if (this.props.itemUdfExist == 'true')
                                    c = false;
                                else
                                    c = true;
                            } else {
                                // if(count==0)
                                udfList = udetails.udfList;
                                d = false;
                            }
                            count++;

                        } else {
                            d = true;

                        }
                        breakCount++;
                    }
                });
            } if (this.state.itemDetails.length == 0 || a || d) {
                iDetails = [];
                itemDetailsHeader.forEach(f => {
                    let catDesc = {
                        catdesc: f.catdesc,
                        code: "",
                        displayName: f.displayName,
                        isLov: f.isLov,
                        isCompulsory: f.isCompulsory,
                        value: ""

                    }
                    iDetails.push(catDesc)
                })
            } if (this.state.itemDetails.length == 0 || b || d) {
                udfList = [];
                udfItemHeadData.forEach(c => {
                    let item = {
                        cat_desc_udf: c.cat_desc_udf,
                        cat_desc_udf_type: c.cat_desc_udf_type,
                        displayName: c.displayName,
                        id: c.id,
                        isCompulsory: c.isCompulsory,
                        isLov: c.isLov,
                        orderBy: c.orderBy,
                        value: ""
                    }
                    udfList.push(item)
                })
            }
            if (this.state.itemDetails.length == 0 || c || d) {
                setUdfList = [];
                setUdfRowsData.forEach(d => {
                    let set = {
                        displayName: d.displayName,
                        id: d.id,
                        isCompulsary: d.isCompulsary,
                        isLov: d.isLov,
                        orderBy: d.orderBy,
                        udfType: d.udfType,
                        value: ""

                    }
                    setUdfList.push(set)
                })
            }


            let demorows = {
                articleCode: r.articleCode,
                deliveryDate: r.date,
                vendorDesign: r.vendorDesign,
                itemGridId: itemIndex++,
                itemList: iDetails,
                setUdfList: setUdfList,
                udfList: udfList
            }
            dRows.push(demorows)
        })


        let rowssss = _.map(
            _.uniq(
                _.map(dRows, function (obj) {

                    return JSON.stringify(obj);
                })
            ), function (obj) {
                return JSON.parse(obj);
            }
        );
        let finalRows = [];
        let indexPosition = 0;
        rowssss.forEach(data => {
            let key = data.articleCode + data.deliveryDate + data.vendorDesign;
            if (finalRows.includes(key)) {
                delete rowssss[indexPosition];
            } else {
                finalRows.push(key);
            }
            indexPosition++;
        })

        this.setState({
            itemDetails: rowssss
        })

    }
    validationMsgRows() {
        var rows = this.state.rows
        rows.forEach(rows => {
            if (rows.vendorMrp == "" && this.props.isMrpRequired) {
                let mrp = sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? "Desc 6" : "MRP"
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,

                    errorMassage: mrp + " is compulsory in indent description"
                })
            } else if (rows.vendorDesign == "" && !this.props.isVendorDesignNotReq) {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Vendor Design is compulsory in indent description"
                })
            } else if (rows.rsp == "" && this.props.isRspRequired) {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "RSP is compulsory in indent description"
                })
            } else if (rows.mrp == "" && this.props.isMrpRequired) {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "MRP is compulsory in indent description"
                })
            } else if (rows.hsnSacCode == "" || rows.hsnSacCode == null) {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "HSN is compulsory in indent description"
                })
            } else if (rows.rate == "") {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Rate is compulsory in indent description"
                })
            }

            else if (rows.size.length == 0) {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Size is compulsory in indent description"
                })
            } else if (rows.color.length == 0) {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Color is compulsory in indent description"
                })
            } else if (rows.numOfsets == "") {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "No of sets is compulsory in indent description"
                })
            } else if (rows.date == "") {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Delivery date is compulsory in indent description"
                })
            } else if (rows.typeOfBuy == "") {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Type of buying is compulsory in indent description"
                })
            } else if (rows.marginRule == "") {
                this.setState({
                    poErrorMsg: true,
                    validateMsg: true,
                    errorMassage: "Margin Rule is compulsory in indent description"
                })
            }
        });
    }


    validateCatDescRow() {
        let catDescRows = this.state.itemDetails
        var type = ""
        for (var i = 0; i < catDescRows.length; i++) {
            for (var j = 0; j < catDescRows[i].itemList.length; j++) {
                if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
                    // if ((catDescRows[i].itemList[j].catdesc == "CAT2" || catDescRows[i].itemList[j].catdesc == "CAT3" || catDescRows[i].itemList[j].catdesc == "CAT4" || catDescRows[i].itemList[j].catdesc == "DESC3") && catDescRows[i].itemList[j].value == "") {
                    if ((catDescRows[i].itemList[j].catdesc == "CAT1" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT2" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT3" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT4" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC1" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC2" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC3" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC5" && catDescRows[i].itemList[j].isLov == "Y" && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "")) {

                        type = typeof (catDescRows[i].itemList[j].displayName) == "string" && catDescRows[i].itemList[j].displayName != "null" ? catDescRows[i].itemList[j].displayName : catDescRows[i].itemList[j].catdesc
                        break
                    }
                } else {
                    if ((catDescRows[i].itemList[j].catdesc == "CAT1" && (this.props.cat1Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT2" && (this.props.cat2Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT3" && (this.props.cat3Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT4" && (this.props.cat4Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT5" && (this.props.cat5Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "CAT6" && (this.props.cat6Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC1" && (this.props.desc1Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC2" && (this.props.desc2Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC3" && (this.props.desc3Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC4" && (this.props.desc4Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC5" && (this.props.desc5Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "") ||
                        (catDescRows[i].itemList[j].catdesc == "DESC6" && (this.props.desc6Validation == true || catDescRows[i].itemList[j].isLov == "Y") && catDescRows[i].itemList[j].isCompulsory == "Y" && catDescRows[i].itemList[j].value == "")) {

                        type = typeof (catDescRows[i].itemList[j].displayName) == "string" && catDescRows[i].itemList[j].displayName != "null" ? catDescRows[i].itemList[j].displayName : catDescRows[i].itemList[j].catdesc
                        break
                    }
                }
            }

            if (type != "") {
                this.setState({
                    poErrorMsg: true,
                    errorMassage: type + " is compulsory"
                })
            }
        }

    }
    gridSecond() {
        let catDescRows = this.state.itemDetails

        let flag = false

        sessionStorage.getItem("partnerEnterpriseName") == "VMART" ?
            catDescRows.forEach(po => {
                po.itemList.forEach(val => {
                    // if (val.value == "" && (val.catdesc == "CAT2" || val.catdesc == "CAT3" || val.catdesc == "CAT4" || val.catdesc == "DESC3")) {
                    if ((val.catdesc == "CAT1" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT2" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT3" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT4" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC1" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC2" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC3" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC5" && val.isLov == "Y" && val.isCompulsory == "Y" && val.value == "")) {

                        flag = true
                        return flag
                    }
                })
            }) :
            catDescRows.forEach(po => {
                po.itemList.forEach(val => {
                    if ((val.catdesc == "CAT1" && (this.props.cat1Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT2" && (this.props.cat2Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT3" && (this.props.cat3Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT4" && (this.props.cat4Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT5" && (this.props.cat5Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "CAT6" && (this.props.cat6Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC1" && (this.props.desc1Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC2" && (this.props.desc2Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC3" && (this.props.desc3Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC4" && (this.props.desc4Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC5" && (this.props.desc5Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "") ||
                        (val.catdesc == "DESC6" && (this.props.desc6Validation == true || val.isLov == "Y") && val.isCompulsory == "Y" && val.value == "")) {

                        flag = true
                        return flag
                    }
                })
            })

        if (flag) {
            this.setState({
                gridSecond: false
            })
        } else {
            this.setState({
                gridSecond: true

            })
        }


    }


    onSubmit() {
        this.props.onSubmit();
        this.validationMsgRows()
        this.gridSecond()
        setTimeout(() => {
            const { transporterr, siteNameerr, leadDayserr, vendorerr, divisionerr, departmenterr, sectionerr } = this.props;
            const { rows, itemDetails, validateMsg } = this.state
            if (!transporterr && !siteNameerr && !leadDayserr && !vendorerr && !divisionerr && !departmenterr && !sectionerr) {


                // setTimeout(() => {

                if (validateMsg == false) {
                    if (this.state.gridSecond) {
                        // if(this.state.gridSecond){
                        let rows = this.state.rows

                        let itemDetails = this.state.itemDetails

                        let finalDataList = []
                        rows.forEach(article => {
                            itemDetails.forEach(itemDetail => {

                                if (article.articleCode == itemDetail.articleCode && article.vendorDesign.toUpperCase() == itemDetail.vendorDesign.toUpperCase() && article.date == itemDetail.deliveryDate) {
                                    let c = article.finCharges
                                    let finChargesData = [];
                                    var finalData = {}
                                    let cat1 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "CAT1") {
                                            return item
                                        }
                                    })
                                    let cat2 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "CAT2") {
                                            return item
                                        }
                                    })
                                    let cat3 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "CAT3") {
                                            return item
                                        }
                                    })
                                    let cat4 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "CAT4") {
                                            return item
                                        }
                                    })
                                    let cat5 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "CAT5") {
                                            return item
                                        }
                                    })
                                    let cat6 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "CAT6") {
                                            return item
                                        }
                                    })
                                    let desc1 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "DESC1") {
                                            return item
                                        }
                                    })
                                    let desc2 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "DESC2") {
                                            return item
                                        }
                                    })
                                    let desc3 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "DESC3") {
                                            return item
                                        }
                                    })
                                    let desc4 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "DESC4") {
                                            return item
                                        }
                                    })
                                    let desc5 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "DESC5") {
                                            return item
                                        }
                                    })
                                    let desc6 = itemDetail.itemList.find(item => {
                                        if (item.catdesc == "DESC6") {
                                            return item
                                        }
                                    })
                                    let itemudf1 = {}, itemudf2 = {}, itemudf3 = {}, itemudf4 = {}, itemudf5 = {}, itemudf6 = {}, itemudf7 = {}, itemudf8 = {}, itemudf9 = {}, itemudf10 = {}
                                    let itemudf11 = {}, itemudf12 = {}, itemudf13 = {}, itemudf14 = {}, itemudf15 = {}, itemudf16 = {}, itemudf17 = {}, itemudf18 = {}, itemudf19 = {}, itemudf20 = {}
                                    let udf1 = {}, udf2 = {}, udf3 = {}, udf4 = {}, udf5 = {}, udf6 = {}, udf7 = {}, udf8 = {}, udf9 = {}, udf10 = {}
                                    let udf11 = {}, udf12 = {}, udf13 = {}, udf14 = {}, udf15 = {}, udf16 = {}, udf17 = {}, udf18 = {}, udf19 = {}, udf20 = {}

                                    if (this.props.itemUdfExist == "true") {
                                        itemudf1 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING01") {
                                                return item
                                            }
                                        })

                                        itemudf2 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING02") {
                                                return item
                                            }
                                        })
                                        itemudf3 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING03") {
                                                return item
                                            }
                                        })
                                        itemudf4 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING04") {
                                                return item
                                            }
                                        })
                                        itemudf5 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING05") {
                                                return item
                                            }
                                        })
                                        itemudf6 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING06") {
                                                return item
                                            }
                                        })
                                        itemudf7 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING07") {
                                                return item
                                            }
                                        })

                                        itemudf8 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING08") {
                                                return item
                                            }
                                        })
                                        itemudf9 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING09") {
                                                return item
                                            }
                                        })
                                        itemudf10 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFSTRING10") {
                                                return item
                                            }
                                        })
                                        itemudf11 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFNUM01") {
                                                return item
                                            }
                                        })
                                        itemudf12 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFNUM02") {
                                                return item
                                            }
                                        })
                                        itemudf13 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFNUM03") {
                                                return item
                                            }
                                        })
                                        itemudf14 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFNUM04") {
                                                return item
                                            }
                                        })
                                        itemudf15 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFNUM05") {
                                                return item
                                            }
                                        })
                                        itemudf16 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFDATE01") {
                                                return item
                                            }
                                        })
                                        itemudf17 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFDATE02") {
                                                return item
                                            }
                                        })
                                        itemudf18 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFDATE03") {
                                                return item
                                            }
                                        })
                                        itemudf19 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFDATE04") {
                                                return item
                                            }
                                        })
                                        itemudf20 = itemDetail.udfList.find(item => {
                                            if (item.cat_desc_udf == "UDFDATE05") {
                                                return item
                                            }
                                        })
                                    }
                                    if (this.props.isUDFExist == "true") {
                                        udf1 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING01") {
                                                return item
                                            }
                                        })
                                        udf2 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING02") {
                                                return item
                                            }
                                        })
                                        udf3 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING03") {
                                                return item
                                            }
                                        })
                                        udf4 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING04") {
                                                return item
                                            }
                                        })
                                        udf5 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING05") {
                                                return item
                                            }
                                        })
                                        udf6 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING06") {
                                                return item
                                            }
                                        })
                                        udf7 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING07") {
                                                return item
                                            }
                                        })
                                        udf8 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING08") {
                                                return item
                                            }
                                        })
                                        udf9 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING09") {
                                                return item
                                            }
                                        })
                                        udf10 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFSTRING10") {
                                                return item
                                            }
                                        })
                                        udf11 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFNUM01") {
                                                return item
                                            }
                                        })
                                        udf12 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFNUM02") {
                                                return item
                                            }
                                        })
                                        udf13 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFNUM03") {
                                                return item
                                            }
                                        })
                                        udf14 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFNUM04") {
                                                return item
                                            }
                                        })
                                        udf15 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFNUM05") {
                                                return item
                                            }
                                        })
                                        udf16 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFDATE01") {
                                                return item
                                            }
                                        })
                                        udf17 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFDATE02") {
                                                return item
                                            }
                                        })
                                        udf18 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFDATE03") {
                                                return item
                                            }
                                        })
                                        udf19 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFDATE04") {
                                                return item
                                            }
                                        })
                                        udf20 = itemDetail.setUdfList.find(item => {
                                            if (item.udfType == "SMUDFDATE05") {
                                                return item
                                            }
                                        })
                                    }


                                    // if (article.finCharges != []) {
                                    //     for (var s = 0; s < c.length; s++) {
                                    //         var dataaa = {
                                    //             chgCode: c[s].chgCode,
                                    //             chgName: c[s].chgName,
                                    //             seq: c[s].seq,
                                    //             finFormula: c[s].finFormula,
                                    //             finChgRate: c[s].finChgRate,
                                    //             finSource: c[s].finSource,
                                    //             gstComponent: c[s].gstComponent,
                                    //             isTax: c[s].isTax,
                                    //             sign: c[s].sign,
                                    //             finChgOperationLevel: c[s].finChgOperationLevel,
                                    //             calculationBasis: c[s].calculationBasis
                                    //         }
                                    //         finChargesData.push(dataaa)
                                    //     }

                                    // } 
                                    let ratio = []
                                    let ratioIndex = 1

                                    let sr = article.ratio.split(',')
                                    for (var k = 0; k < sr.length; k++) {

                                        let ra = {
                                            id: ratioIndex++,
                                            ratio: sr[k]
                                        }
                                        ratio.push(ra)
                                    }

                                    let slist = []
                                    let ss = article.sizeList
                                    let scount = 1
                                    let si = article.size.split(',')
                                    for (var l = 0; l < ss.length; l++) {
                                        for (var j = 0; j < si.length; j++) {
                                            if (si[j] == ss[l].cname) {
                                                let payload = {
                                                    id: scount++,
                                                    code: ss[l].code,
                                                    cname: ss[l].cname
                                                }

                                                slist.push(payload)




                                            }
                                        }
                                    }

                                    finalData = {
                                        hl4Code: article.articleCode,
                                        hl4Name: article.articleName,
                                        hsnSacCode: article.hsnSacCode,
                                        hsnCode: article.hsnCode,
                                        design: article.vendorDesign,
                                        image: Object.values(article.imageUrl),
                                        containsImage: article.containsImage,
                                        rsp: Math.round(article.rsp * 100) / 100,
                                        mrp: Math.round(article.mrp * 100) / 100,
                                        discountType: article.discount.discountType,
                                        discountValue: article.discount.discountValue,

                                        rate: Math.round(article.rate * 100) / 100,
                                        finalRate: (Math.round(article.finalRate * 100) / 100).toString(),
                                        sizeList: slist,
                                        ratioList: ratio,
                                        colorList: article.colorList,
                                        finCharge: article.finCharges,
                                        noOfSets: article.numOfsets,
                                        deliveryDate: article.date == "" ? "" : moment(article.date).format(),
                                        remarks: article.remarks,
                                        quantity: article.quantity,
                                        netAmountTotal: Math.round(article.netAmountTotal * 100) / 100,
                                        calculatedMargin: article.calculateMargin,
                                        gst: article.gst,
                                        otb: Math.round(article.otb * 100) / 100,
                                        intakeMargin: Math.round(article.intakeMargin * 100) / 100,
                                        marginRule: article.marginRule,
                                        typeOfBuying: article.typeOfBuy,
                                        totalAmount: article.totalAmount,
                                        colorName: "",
                                        sizeName: "",
                                        tax: article.tax,
                                        mrpStart: article.mrpStart == "" ? 0 : Math.round(article.mrpStart * 100) / 100,
                                        mrpEnd: article.mrpEnd == "" ? 0 : Math.round(article.mrpEnd * 100) / 100,
                                        cat1Code: cat1 != undefined ? cat1.code : "",
                                        cat1Name: cat1 != undefined ? cat1.value : "",
                                        cat2Code: cat2 != undefined ? cat2.code : "",
                                        cat2Name: cat2 != undefined ? cat2.value : "",
                                        cat3Code: cat3 != undefined ? cat3.code : "",
                                        cat3Name: cat3 != undefined ? cat3.value : "",
                                        cat4Code: cat4 != undefined ? cat4.code : "",
                                        cat4Name: cat4 != undefined ? cat4.value : "",
                                        cat5Code: cat5 != undefined ? cat5.code : "",
                                        cat5Name: cat5 != undefined ? cat5.value : "",
                                        cat6Code: cat6 != undefined ? cat6.code : "",
                                        cat6Name: cat6 != undefined ? cat6.value : "",
                                        desc1Code: desc1 != undefined ? desc1.code : "",
                                        desc1Name: desc1 != undefined ? desc1.value : "",

                                        desc2Code: desc2 != undefined ? desc2.code : "",
                                        desc2Name: desc2 != undefined ? desc2.value : "",
                                        desc3Code: desc3 != undefined ? desc3.code : "",
                                        desc3Name: desc3 != undefined ? desc3.value : "",
                                        desc4Code: desc4 != undefined ? desc4.code : "",
                                        desc4Name: desc4 != undefined ? desc4.value : "",
                                        desc5Code: desc5 != undefined ? desc5.code : "",
                                        desc5Name: desc5 != undefined ? desc5.value : "",
                                        desc6Code: desc6 != undefined ? desc6.code : "",
                                        desc6Name: desc6 != undefined ? desc6.value : "",
                                        udf: this.props.isUDFExist == "true" || this.props.itemUdfExist == "true" ?
                                            {
                                                itemudf1: itemudf1 != undefined ? itemudf1.value : "",
                                                itemudf2: itemudf2 != undefined ? itemudf2.value : "",
                                                itemudf3: itemudf3 != undefined ? itemudf3.value : "",
                                                itemudf4: itemudf4 != undefined ? itemudf4.value : "",
                                                itemudf5: itemudf5 != undefined ? itemudf5.value : "",
                                                itemudf6: itemudf6 != undefined ? itemudf6.value : "",
                                                itemudf7: itemudf7 != undefined ? itemudf7.value : "",
                                                itemudf8: itemudf8 != undefined ? itemudf8.value : "",
                                                itemudf9: itemudf9 != undefined ? itemudf9.value : "",
                                                itemudf10: itemudf10 != undefined ? itemudf10.value : "",
                                                itemudf11: itemudf11 != undefined ? itemudf11.value : "",
                                                itemudf12: itemudf12 != undefined ? itemudf12.value : "",
                                                itemudf13: itemudf13 != undefined ? itemudf13.value : "",
                                                itemudf14: itemudf14 != undefined ? itemudf14.value : "",
                                                itemudf15: itemudf15 != undefined ? itemudf15.value : "",
                                                itemudf16: itemudf16 != undefined ? itemudf16.value : "",
                                                itemudf17: itemudf17 != undefined ? itemudf17.value : "",
                                                itemudf18: itemudf18 != undefined ? itemudf18.value : "",
                                                itemudf19: itemudf19 != undefined ? itemudf19.value : "",
                                                itemudf20: itemudf20 != undefined ? itemudf20.value : "",
                                                udf1: udf1 != undefined ? udf1.value : "",
                                                udf2: udf2 != undefined ? udf2.value : "",
                                                udf3: udf3 != undefined ? udf3.value : "",
                                                udf4: udf4 != undefined ? udf4.value : "",
                                                udf5: udf5 != undefined ? udf5.value : "",
                                                udf6: udf6 != undefined ? udf6.value : "",
                                                udf7: udf7 != undefined ? udf7.value : "",
                                                udf8: udf8 != undefined ? udf8.value : "",
                                                udf9: udf9 != undefined ? udf9.value : "",
                                                udf10: udf10 != undefined ? udf10.value : "",
                                                udf11: udf11 != undefined ? udf11.value : "",
                                                udf12: udf12 != undefined ? udf12.value : "",
                                                udf13: udf13 != undefined ? udf13.value : "",
                                                udf14: udf14 != undefined ? udf14.value : "",
                                                udf15: udf15 != undefined ? udf15.value : "",
                                                udf16: udf16 != undefined ? udf16.value : "",
                                                udf17: udf17 != undefined ? udf17.value : "",
                                                udf18: udf18 != undefined ? udf18.value : "",
                                                udf19: udf19 != undefined ? udf19.value : "",
                                                udf20: udf20 != undefined ? udf20.value : "",
                                            } : null
                                    }
                                    finalDataList.push(finalData);
                                }
                            })

                        })
                        var finalSubmitData = {
                            hl1Code: this.props.hl1Code,
                            hl1Name: this.props.division,
                            hl2Code: this.props.hl2Code,
                            hl2Name: this.props.section,
                            hl3Code: this.props.hl3Code,
                            hl3Name: this.props.department,
                            slCode: this.props.slcode,
                            slName: this.props.slName,
                            slAddr: this.props.slAddr,
                            slCityName: this.props.isCityExist ? this.props.city : this.props.slCityName,
                            leadTime: this.props.leadDays == "" || this.props.leadDays == null || this.props.leadDays == "null" ? "" : this.props.leadDays.toFixed(2),
                            termCode: this.props.purtermMainCode,
                            termName: this.props.termName,
                            transporterCode: this.props.transporterCode,
                            transporterName: this.props.transporterName,
                            poQuantity: this.state.poQuantity.toFixed(2),
                            poAmount: this.state.poAmount.toFixed(2),
                            stateCode: this.props.stateCode,
                            itemUdfExist: this.props.itemUdfExist,
                            isUDFExist: this.props.isUDFExist,
                            isSiteExist: this.props.isSiteExist,
                            piDetails: finalDataList,
                            siteCode: this.props.siteCode,
                            siteName: this.props.siteName
                        }
                        this.props.piCreateRequest(finalSubmitData)
                        this.props.getRows(this.props.hl3Code);
                    }
                    else {
                        this.validateCatDescRow()
                    }
                }

                // }, 10)
            }
        }, 10)
    }

    onBlurVendorDesign(id, e) {
        if (sessionStorage.getItem("partnerEnterpriseName") == "VMART") {
            let idd = id
            let r = this.state.rows
            let flag = false
            let vendorDesignNo = []
            if (e.target.id == "vendorDesign") {
                for (let i = 0; i < r.length; i++) {
                    vendorDesignNo.push(r[i].vendorDesign)
                }

                for (let i = 0; i < r.length; i++) {
                    let unique = [];
                    vendorDesignNo.forEach(s => {
                        if (!unique.includes(s)) {
                            flag = false;
                            unique.push(s)

                        }
                        else {

                            flag = true
                        }
                    });
                }
                if (flag) {
                    for (let i = 0; i < r.length; i++) {
                        if (r[i].rowId == idd) {

                            r[i].vendorDesign = ""

                        }
                    }
                    this.setState({
                        errorMassage: "Same value not allowed",
                        poErrorMsg: true
                    })
                }
            }
        }
    }


    openHsnCodeModal(id, hsn) {

        if (this.props.hl3Code != "") {
            this.setState({
                hsnRowId: id,
                hsnSacCode: hsn,
                hsnModal: true,
                hsnModalAnimation: !this.state.hsnModalAnimation,
                focusId: "hsnCode" + id,
                hsnSearch:hsn


            })
            let data = {
                rowId: id,
                code: this.props.hl3Code,
                no: 1,
                type: 1,
                search: ""
            }
            this.props.hsnCodeRequest(data)
            this.poSearch("hsnCode"+id, "hsnCodeNew"+id)
        } else {


            this.setState({
                errorMassage: "Select Department",
                poErrorMsg: true

            })
        }
    }

    closeHsnModal() {
        this.setState({


            hsnModal: false,
            hsnModalAnimation: !this.state.hsnModalAnimation

        })
        document.getElementById(this.state.focusId).focus()
    }
    updateHsnCode(data) {
        let rows = this.state.rows
        for (let i = 0; i < rows.length; i++) {
            if (rows[i].rowId == data.rowId) {
                rows[i].hsnSacCode = data.hsnSacCode
                rows[i].hsnCode = data.hsnCode
                if (rows[i].size != "" && rows[i].ratio != "" && rows[i].numOfsets != "" && rows[i].color != "" && rows[i].rate != "" && this.props.purtermMainCode != "") {
                    this.getQuantity(data.rowId);
                }
            }
        }
        this.setState({
            rows: rows
        })
        document.getElementById(this.state.focusId).focus()
    }
    closeItemmModal() {
        this.setState({
            itemModal: false,
            itemModalAnimation: !this.state.itemModalAnimation
        })
        document.getElementById(this.state.focusId).focus()
    }

    openArticle(articleCode, id) {


        this.setState({
            articleRowId: id,
            checkedArticle: articleCode,
            articleNew: true,
            articleNewModalAnimation: true,
            focusId: "articleName" + id,
            articleSearch:articleCode,
           

        })
        this.poSearch("articleName"+id, "articleNewId"+id)

        let data = {
            type: 1,
            no: 1,
            hl3Code: this.props.hl3Code,
            search:articleCode
        }
        this.props.piArticleRequest(data)
    }

    updateArticleNew(data) {

        let rows = this.state.rows
        for (let i = 0; i < rows.length; i++) {
            if (rows[i].rowId == data.rowId) {
                rows[i].articleCode = data.articleCode,
                    rows[i].articleName = data.articleName,
                    rows[i].mrpStart = data.mrpStartRange
                rows[i].mrpEnd = data.mrpEndRange

            }
        }
        this.setState({
            startRange: data.mrpStartRange,
            endRange: data.mrpEndRange,
            rows: rows
        }, () => {
            if (!this.props.isMrpRequired) {
                let array = this.state.rows
                for (let x = 0; x < array.length; x++) {
                    if (array[x].rowId == data.rowId) {
                        array[x].mrp = "";
                        array[x].mrpEnd = "";
                        array[x].mrpStart = "";
                        array[x].rsp = "";
                        array[x].vendorMrp = ""
                        // array[x].hsnSacCode = data.hsnSacCode;
                        array[x].iCode = "";
                        array[x].image = [];
                        array[x].imageUrl = {};
                        array[x].containsImage = false
                        array[x].rate = "";
                        array[x].finalRate = "",
                            array[x].discount = {
                                discountType: "",
                                discountValue: ""
                            },
                            array[x].numOfsets = "";
                        array[x].date = "";
                        array[x].remarks = "";
                        array[x].quantity = "";
                        array[x].netAmountTotal = "";
                        array[x].calculateMargin = "";
                        array[x].gst = "";
                        array[x].otb = "";
                        array[x].intakeMargin = "";
                        array[x].vendorDesign = "";
                        array[x].typeOfBuy = "";
                        array[x].totalAmount = "";
                        array[x].marginRule = "";
                        array[x].finCharges = [];
                        array[x].tax = "";
                        array[x].basic = "";
                        array[x].color = this.props.isColorRequired ? array[x].color : ["NA"]
                        array[x].colorList = this.props.isColorRequired ? array[x].colorList : [{ id: 1, code: "", cname: "NA" }]
                    }
                }
                this.setState({
                    rows: array
                })
                let marginData = {
                    slCode: this.props.slcode,
                    articleCode: data.articleCode,
                    rId: data.rowId,
                    siteCode: this.props.siteCode
                }
                this.props.marginRuleRequest(marginData)
            }
        })
        document.getElementById(this.state.focusId).focus()
    }

    closeArticleModal() {
        this.setState({
            articleNew: false,
            articleNewModalAnimation: false
        })
        document.getElementById(this.state.focusId).focus()
    }

    openDiscountModal(discount, grid, idx) {


        let rows = this.state.rows
        let mrp = ""
        let articleCode = ""
        for (let i = 0; i < rows.length; i++) {
            if (rows[i].rowId == grid) {
                mrp = rows[i].vendorMrp
                articleCode = rows[i].articleCode
            }

        }
        if (articleCode != "") {
            if (mrp != "" || !this.props.isMrpRequired) {

                this.setState({
                    selectedDiscount: discount,
                    discountGrid: grid,
                    itemCodeId: idx,
                    discountModal: true,
                    discountMrp: mrp,
                    vendorMrpPo: articleCode,
                    focusId: "discount" + grid
                })
                if (this.props.isDiscountMap) {
                    let payload = {
                        articleCode: articleCode,
                        mrp: mrp,
                        discountType: "percentage"
                    }
                    this.props.discountRequest(payload)
                }
            } else {
                this.setState({

                    errorMassage: "MRP is compulsory",
                    poErrorMsg: true
                })
            }
        } else {
            this.setState({

                errorMassage: "Article is compulsory",
                poErrorMsg: true
            })
        }
    }
    closeDiscountModal() {
        // document.getElementById(this.state.itemCodeId).focus()

        this.setState({
            discountModal: false
        })
        document.getElementById(this.state.focusId).focus()

    }
    updateDiscountModal(discountData) {
        let poRows = this.state.rows
        let value = ""
        let index = 0
        for (let j = 0; j < poRows.length; j++) {
            if (poRows[j].rowId == discountData.discountGrid) {
                index = j
                if (discountData.discount.discountType.includes("%")) {
                    value = poRows[j].rate * discountData.discount.discountValue / 100

                    if (Number(poRows[j].rate - value) < 0) {
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true
                        })

                    } else {
                        poRows[j].finalRate = poRows[j].rate - value
                        poRows[j].discount = discountData.discount
                    }

                } else {
                    value = poRows[j].rate - discountData.discount.discountValue
                    if (value < 0) {
                        this.setState({
                            errorMassage: "This discount will create your final rate negative",
                            poErrorMsg: true
                        })

                    } else {
                        poRows[j].finalRate = value
                        poRows[j].discount = discountData.discount
                    }

                }
            }
        }
        this.setState({
            rows: poRows
        }, () => {
            let r = this.state.rows
            let i = index
            if (r[i].size != "" && r[i].ratio != "" && r[i].numOfsets != "" && r[i].color != "" && r[i].rate != "" && this.props.purtermMainCode != "") {
                this.getQuantity(discountData.discountGrid);
            }


        })

        document.getElementById(this.state.focusId).focus()

    }
    onBlurRate(idd, e) {
        console.log(e)
        var keycode = event.keyCode;
        console.log(keycode)

        let r = this.state.rows
        let index = 0
        if (e.target.value != "" && e.target.value != 0) {

            for (let i = 0; i < r.length; i++) {
                if (r[i].rowId == idd) {
                    index = i
                    if (r[i].discount.discountType != "" && this.props.isDiscountAvail) {
                        if (r[i].discount.discountType.includes("%")) {
                            let value = e.target.value * (r[i].discount.discountValue / 100)
                            r[i].finalRate = e.target.value - value


                        } else {
                            r[i].finalRate = e.target.value - r[i].discount.discountValue

                        }
                    } else {
                        r[i].finalRate = e.target.value


                    }
                }
            }
            this.setState({
                rows: r
            }, () => {
                let r = this.state.rows
                let i = index
                if (r[i].size != "" && r[i].ratio != "" && r[i].numOfsets != "" && r[i].color != "" && r[i].rate != "" && this.props.purtermMainCode != "") {
                    this.getQuantity(idd);
                }


            })
        } else {
            this.setState({
                errorMassage: "Rate can't be zero",
                poErrorMsg: true
            })
        }



    }

    _handleKeyPress(e, data, item) {

        let idd = data + item.rowId;
        if (e.key === "F7" || e.key === "F2") {
            document.getElementById(idd).click();
        }
        if (e.key === "Enter") {
            if ("articleName"+item.rowId == data + item.rowId) {
                console.log("11111111111111111111111111111111")
                this.openArticle(item.articleCode, item.rowId)
            }
            if("hsnCode"+item.rowId == data + item.rowId) {
                console.log("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
                this.openHsnCodeModal(item.rowId, item.hsnSacCode)
            }
        }
    }
    handleInput(data, item, e) {
        let rows = this.state.rows
        if (e.target.id == "articleName" + item.rowId) {
            for (let i = 0; i < rows.length; i++) {
                if (rows[i].rowId == item.rowId) {
                    rows[i].articleCode = e.target.value
                }

            }
            this.setState({
                rows
            })

            
        }else if(e.target.id == "hsnCode" + item.rowId) {
            for (let i = 0; i < rows.length; i++) {
                if (rows[i].rowId == item.rowId) {
                    rows[i].hsnSacCode = e.target.value
                }

            }
            this.setState({
                rows
            })

            
        }
    }

    render() {
        // document.body.addEventListener('scroll', (event) => {
        //     if(this.state.searchModal!=""  && (this.state.prevSearchLeftthis.state.prevSearchLeft!=this.state.searchLeft ||this.state.prevSearchTop!=this.state.searchTop)){
        //         console.log("asdfghjkjhgfdsdfghjklkjhgfdsdfghjhgfdsdfgh")
        //     this.poSearch(this.state.searchField,this.state.searchModal)
        //     }
        //   })
      

        return (
            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 p-lr-47">
                <div className="purchasedContainer1">
                    <div className="StickyDiv2">
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list_style">
                                <li>
                                    <label className="contribution_mart">
                                        INDENT DESCRIPTION
                                    </label>
                                </li>
                                <li>
                                    <p className="master_para">Manage Item description for all products</p>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-12 col-sm-12 pad-0 m-top-20 zui-wrapper tableGeneric sectionOneTable bordere3e7f3 poTableMain piTableIndent">
                            <div className="zui-wrapper">
                                <div className="scrollableTableFixed table-scroll zui-scroller pad-0" id="table-scroll">
                                    <table className="table scrollTable main-table zui-table">
                                        <thead>
                                            <tr>
                                                <th className="fixed-side alignMiddle">
                                                    <ul className="list-inline">

                                                        <li className="width70 pad-lft-0">
                                                            <label className="width-45 lableFixed">
                                                                Action
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Article Code / Name<span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                {this.props.isMrpRequired ? <th className="positionRelative">
                                                    {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? <label>
                                                        DESC 6
                              </label> : <label>MRP</label>}
                                                </th> : null}
                                                {!this.props.isVendorDesignNotReq ? <th className="positionRelative">
                                                    <label>
                                                        Vendor Design No.<span className="mandatory">*</span>
                                                    </label>
                                                </th> : null}
                                                <th className="positionRelative">
                                                    <label>
                                                        HSN code<span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Image
                              </label>
                                                </th>
                                                {this.props.isRspRequired ? <th className="positionRelative">
                                                    <label>
                                                        RSP<span className="mandatory">*</span>
                                                    </label>
                                                </th> : null}
                                                {this.props.isMRPEditable ? <th className="positionRelative">
                                                    <label>
                                                        MRP<span className="mandatory">*</span>
                                                    </label>
                                                </th> : null}
                                                <th className="positionRelative">
                                                    <label>
                                                        Rate<span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                {this.props.isDiscountAvail ? <th>
                                                    <label>
                                                        Discount<span className="mandatory">*</span>
                                                    </label>
                                                </th> : null}
                                                {this.props.isDisplayFinalRate ? <th>
                                                    <label>
                                                        Final Rate
                                                        </label>
                                                </th> : null}

                                                <th className="positionRelative">
                                                    <label>
                                                        Size <span> (Cm / Year)</span><span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Ratio
                              </label>
                                                </th>
                                                {this.props.isColorRequired ? <th className="positionRelative">
                                                    <label>
                                                        Color<span className="mandatory">*</span>
                                                    </label>
                                                </th> : null}
                                                <th className="positionRelative">
                                                    <label>
                                                        No. Of Sets<span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Delivery Date<span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Type Of Buying<span className="mandatory">*</span>
                                                    </label>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Remarks
                              </label>
                                                </th>
                                                <th className="positionRelative">
                                                    <label>
                                                        Quantity
                              </label>
                                                </th>
                                                {this.props.isBaseAmountActive ? <th className="positionRelative">
                                                    <label>
                                                        Basic
                              </label>
                                                </th> : null}
                                                <th className="positionRelative">
                                                    <label>
                                                        Net Amount Total
                              </label>
                                                </th>
                                                {!this.props.isTaxDisable ? <th className="positionRelative">
                                                    <label>
                                                        Tax
                              </label>
                                                </th> : null}
                                                {this.props.isRspRequired ? <th className="positionRelative">
                                                    <label>
                                                        Calculated Margin
                              </label>
                                                </th> : null}
                                                {!this.props.isGSTDisable ? <th className="positionRelative">
                                                    <label>
                                                        GST(%)
                              </label>
                                                </th> : null}
                                                {this.props.displayOtb ? <th className="positionRelative">
                                                    <label>
                                                        OTB
                              </label>
                                                </th> : null}
                                                {this.props.isDisplayMarginRule ? <th className="positionRelative">
                                                    <label>
                                                        Margin Rule
                              </label>
                                                </th> : null}
                                                {this.props.isRspRequired ? <th className="positionRelative">
                                                    <label>
                                                        Intake Margins
                              </label>
                                                </th> : null}

                                                {sessionStorage.getItem("partnerEnterpriseName") != "VMART" && this.props.isMrpRequired ?
                                                    <th className="positionRelative">
                                                        <label>
                                                            MRP Start
                              </label>
                                                    </th> : null}
                                                {sessionStorage.getItem("partnerEnterpriseName") != "VMART" && this.props.isMrpRequired ?
                                                    <th className="positionRelative">
                                                        <label>
                                                            MRP End
                              </label>
                                                    </th> : null}



                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.rows.map((item, idx) => (
                                                <tr id={item.rowId} key={idx} >
                                                    <td className="fixed-side alignMiddle" >
                                                        <ul className="list-inline">
                                                            <li className="text-center width70">
                                                                <img src={copyIcon} onClick={() => this.copyIndentDescription(item.rowId)} />
                                                                <svg xmlns="http://www.w3.org/2000/svg" onClick={(e) => this.handleRemoveSpecificRow(item.rowId, item.articleCode, item.mrp, item.otb)} width="17" height="17" viewBox="0 0 17 19">
                                                                    <path fill="#e5e5e5" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                                                </svg>
                                                            </li>
                                                        </ul>
                                                    </td>

                                                    {!this.props.isArticleSeparated ? <td className="pad-0 pad-0 hoverTable openModalBlueBtn displayPointer tdFocus" >
                                                        <label className="purchaseTableLabel">
                                                            {item.articleCode}
                                                        </label>
                                                    </td> :

                                                        <td className="pad-0 pad-0 hoverTable openModalBlueBtn displayPointer tdFocus">
                                                            <label className="purchaseTableLabel ">
                                                                {!this.state.isModalShow ?
                                                                    <input autoComplete="off" type="text" onChange={(e) => this.handleInput("articleName", item, e)} className="inputTable" id={"articleName" + item.rowId} value={item.articleCode == "" ? null : item.articleCode + "/" + item.articleName} onKeyDown={(e) => this._handleKeyPress(e, "articleName" , item)} />

                                                                    :
                                                                    <input autoComplete="off" type="text" readOnly className="inputTable" id={"articleName" + item.rowId} value={item.articleCode == "" ? null : item.articleCode + "/" + item.articleName} onKeyDown={(e) => this._handleKeyPress(e,"articleName" , item)} onClick={(e) => this.openArticle(`${item.articleCode}`, `${item.rowId}`)} readOnly/>}

                                                            </label>
                                                            <div className="purchaseTableDiv"  onClick={(e) => this.openArticle(`${item.articleCode}`, `${item.rowId}`)} >
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                    <g fill="none" fillRule="evenodd">
                                                                        <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                        <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                        
                                                    {this.state.articleNew ?
                                                     <div className="calculatedToolTip totalAmtDescDrop set-pi-modal-position" id={"articleNewId" + item.rowId}><ArticleNewModal {...this.props} {...this.state} updateArticleNew={(e) => this.updateArticleNew(e)} closeArticleModal={(e) => this.closeArticleModal(e)} /></div>
                                                      : null}
                                                        </td>}
                                                    {/* {this.state.isModalShow? this.state.articleNew ? <div id={"articleNewId" + item.rowId}> <ArticleNewModal {...this.props} {...this.state} updateArticleNew={(e) => this.updateArticleNew(e)} closeArticleModal={(e) => this.closeArticleModal(e)} /></div> : null:null} */}
                                                    {this.props.isMrpRequired ? <td className="pad-0 pad-0 hoverTable openModalBlueBtn displayPointer tdFocus">

                                                        <input autoComplete="off" type="text" className="inputTable" id={"vendorMrp" + item.rowId} value={item.vendorMrp} onKeyDown={(e) => this._handleKeyPress(e, "vendorMrp" , item)} onClick={(e) =>
                                                            this.openMrpModal(`${item.rowId}`, idx, `${item.iCode}`, `${item.articleCode}`, `${item.vendorMrp}`, `${item.otb}`, `${item.articleName}`)} />

                                                        {/*<label className="purchaseTableLabel displayPointer onFocus">
                                                            {item.vendorMrp}
                                                        </label>*/}
                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>
                                                        </div>

                                                        {this.state.mrpModal ? <div className="calculatedToolTip totalAmtDescDrop set-pi-modal-position" id={"vendorMrpNew" + item.rowId}><PiMrpModal {...this.state} {...this.props} idx={this.state.idx} slcode={this.props.slcode} department={this.props.department} rows={this.state.rows} mrpCloseModal={(e) => this.mrpCloseModal(e)} updateMrpState={(e) => this.updateMrpState(e)} mrpModalAnimation={this.state.mrpModalAnimation} rowId={this.state.rowIdentity} closeMrpModal={(e) => this.openMrpModal(e)} /></div> : null}
                                                    </td> : null}


                                                    {!this.props.isVendorDesignNotReq ? <td className="pad-0 positionRelative tdFocus">
                                                        <input type="text" name="vendorDesign" id="vendorDesign" className="inputTable " value={this.state.rows[idx].vendorDesign}
                                                            onChange={(e) => this.handleChange(`${item.rowId}`, e)} autoComplete="off"
                                                        />
                                                    </td> : null}
                                                    <td className="pad-0 pad-0 hoverTable openModalBlueBtn displayPointer tdFocus" >
                                                    {!this.state.isModalShow ?  <input autoComplete="off" type="text"  onChange={(e) => this.handleInput("hsnCode", item, e)} className="inputTable" id={"hsnCode" + item.rowId} value={item.hsnSacCode} onKeyDown={(e) => this._handleKeyPress(e, "hsnCode" , item)}/>
                                                    :
                                                       <input autoComplete="off" readOnly type="text" className="inputTable" id={"hsnCode" + item.rowId} value={item.hsnSacCode} onKeyDown={(e) => this._handleKeyPress(e, "hsnCode" , item)} onClick={(e) =>
                                                            this.openHsnCodeModal(`${item.rowId}`, `${item.hsnSacCode}`)} />}

                                                        {/*<label className="purchaseTableLabel displayPointer onFocus">
                                                            {item.hsnSacCode}
                                                        </label>*/}
                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        {this.state.hsnModal ? <div className="calculatedToolTip totalAmtDescDrop set-pi-modal-position" id={"hsnCodeNew" + item.rowId}><HsnCodeModal {...this.props} {...this.state} departmentCode={this.props.hl3Code} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /></div> : null}
                                                    </td>
                                                    <td className="pad-0 pad-0 hoverTable openModalBlueBtn openModalBlueBtn displayPointer" onClick={(e) =>
                                                        this.openImageModal(`${item.articleCode}`, `${item.rowId}`)}>

                                                        <label className="piToolTip">
                                                            <div className="topToolTip">
                                                                <label>{item.image.join(',')}</label>
                                                                <span className="topToolTipText">{item.image != undefined ? item.image.join(',') : ""}</span>
                                                            </div>

                                                        </label>
                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="44" viewBox="0 0 33 43">
                                                                <g fill="none" fillRule="nonzero">
                                                                    <path fill="#6D6DC9" d="M0 0h33v43H0z" />
                                                                    <path fill="#FFF" d="M13.143 22.491A2.847 2.847 0 0 1 16 19.635a2.847 2.847 0 0 1 2.857 2.856A2.847 2.847 0 0 1 16 25.348a2.847 2.847 0 0 1-2.857-2.857zm-6.026 4.05v-8.236c0-.94.763-1.703 1.703-1.703h3.404l.156-.685c.255-1.056 1.174-1.8 2.25-1.8h2.7c1.077 0 2.016.744 2.25 1.8l.157.685h3.404c.94 0 1.702.763 1.702 1.702v8.237c0 .998-.802 1.82-1.82 1.82H8.938c-.998 0-1.82-.822-1.82-1.82zm4.676-4.05A4.2 4.2 0 0 0 16 26.698a4.2 4.2 0 0 0 4.207-4.207A4.183 4.183 0 0 0 16 18.304a4.196 4.196 0 0 0-4.207 4.187zm-2.817-3.443c0 .47.391.86.861.86s.86-.39.86-.86-.39-.861-.86-.861c-.49.02-.86.391-.86.86z" />
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </td>
                                                    {this.props.isRspRequired ? <td className="pad-0 positionRelative tdFocus">

                                                        <label className="purchaseTableLabel"  >
                                                            {item.rsp}
                                                        </label>
                                                    </td> : null}
                                                    {this.props.isMRPEditable ? <td className={this.props.isMrpRequired ? Number(item.rsp) > Number(item.mrp) ? "errorBorder pad-0 positionRelative tdFocus" : "pad-0 positionRelative tdFocus" : "pad-0 positionRelative tdFocus"}>

                                                        <label className="rateHover" id={"toolIdRate" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`toolIdRate${item.rowId}`, `moveLeftRate${item.rowId}`)}>
                                                            <input type="text" name="mrp" pattern="[0-9]+([\.][0-9]{0,2})?" id="mrp" className="inputTable " value={this.state.rows[idx].mrp}
                                                                onChange={(e) => this.handleChange(`${item.rowId}`, e)} autoComplete="off" />
                                                            {this.props.isMrpRequired ? item.rsp > item.mrp ? <span> <div className="showRate" id={"moveLeftRate" + item.rowId}><p>Rsp cannot be greater then Mrp</p></div></span> : "" : ""}
                                                        </label>
                                                    </td> : null}
                                                    <td className={this.props.isMrpRequired ? Number(item.rate) > Number(item.mrp) ? "errorBorder pad-0 positionRelative tdFocus" : "pad-0 positionRelative" : "pad-0 positionRelative tdFocus"}>
                                                        <label className="rateHover" id={"toolIdRate" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`toolIdRate${item.rowId}`, `moveLeftRate${item.rowId}`)}>
                                                            <input type="text" pattern="[0-9]+([\.][0-9]{0,2})?" className="inputTable " onChange={(e) => this.handleChange(`${item.rowId}`, e)} onBlur={(e) => this.onBlurRate(`${item.rowId}`, e)} id="rate" name="rate" value={item.rate} />
                                                            {this.props.isMrpRequired ? item.rate > item.mrp ? <span> <div className="showRate" id={"moveLeftRate" + item.rowId}><p>Rate cannot be greater then Mrp</p></div></span> : "" : ""}
                                                        </label>
                                                    </td>
                                                    {this.props.isDiscountAvail ? <td className="pad-0 hoverTable openModalBlueBtn tdFocus" >

                                                        <input autoComplete="off" readOnly type="text" className="inputTable" value={item.discount.discountType} onKeyDown={(e) => this._handleKeyPress(e, "discount" , item)} id={"discount" + item.rowId} onClick={(e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.rowId}`, "discount")} />
                                                        <div className="purchaseTableDiv" onClick={(e) => this.openDiscountModal(`${item.discount.discountType}`, `${item.rowId}`, "discount")} >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>

                                                        </div>

                                                    </td> : null}

                                                    {this.props.isDisplayFinalRate ? <td className="pad-0 positionRelative tdFocus">

                                                        <label className="purchaseTableLabel"  >
                                                            {item.finalRate}
                                                        </label>
                                                    </td> : null}
                                                    <td className="pad-0 pad-0 hoverTable openModalBlueBtn displayPointer tdFocus" >

                                                        {/*<label className="piToolTip">
                                                            <div className="topToolTip">
                                                                <label className="displayPointer">{item.size}</label>
                                                                <span className="topToolTipText">{item.size}</span>
                                                            </div>

                                                        </label>*/}
                                                        <div className="topToolTip toolTipSupplier ">

                                                            <input type="text" readOnly className="chooseSupplyWidth displayPointer inputTable inputlimitText displayInline " value={item.size} onKeyDown={(e) => this._handleKeyPress(e, "size" , item)} id={"size" + item.rowId} onClick={e =>
                                                                this.openPiSizeModal(`${this.props.hl3Code}`, `${item.rowId}`, `${item.size}`, `${item.ratio}`, item.articleName, item.sizeList)} />
                                                            {item.size != "" ? <span className="topToolTipText">{item.size}</span> : ""}</div>
                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </td>
                                                    <td className="pad-0 pad-0 hoverTable openModalBlueBtn">
                                                        <label className="purchaseTableLabel">
                                                            {item.ratio}

                                                        </label>
                                                    </td>
                                                    {this.props.isColorRequired ? <td className="pad-0 pad-0 hoverTable openModalBlueBtn openModalBlueBtn displayPointer tdFocus" >

                                                        {/*<label className="piToolTip">
                                                                <div className="topToolTip">
                                                                    <label className="displayPointer">{item.color}</label>
                                                                    <span className="topToolTipText">{item.color}</span>
                                                                </div>
                                                            </label>*/}
                                                        <div className="topToolTip toolTipSupplier ">

                                                            <input type="text" readOnly value={item.color} className="chooseSupplyWidth displayPointer inputTable inputlimitText displayInline " id={"color" + item.rowId}
                                                                onKeyDown={(e) => this._handleKeyPress(e, "color" , item)} onClick={(e) =>
                                                                    this.openPiColorModal(`${this.props.hl3Code}`, `${item.rowId}`, `${item.color}`, item.colorList)} />
                                                            {item.color != "" ? <span className="topToolTipText">{item.color}</span> : ""}</div>


                                                        <div className="purchaseTableDiv" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="44" viewBox="0 0 16 43">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path fill="#6D6DC9" fillRule="nonzero" d="M0 0h16v43H0z" />
                                                                    <path fill="#FFF" d="M3 21l5 5 5-5H3" />
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </td> : null}
                                                    {sessionStorage.getItem("partnerEnterpriseName") == "VMART" ? <td className="pad-0 positisetBasedTrueonRelative tdFocus">
                                                        <label className="noOfSetHover" id={"toolIdSets" + item.rowId} >
                                                            <input type="text" pattern="[0-9]*" className="inputTable" onBlur={() => this.getQuantity(`${item.rowId}`)} onChange={(e) => this.handleChange(`${item.rowId}`, e)} id="numOfsets" name="numOfsets" value={item.numOfsets} />

                                                        </label>
                                                    </td> :
                                                        <td className="pad-0 positisetBasedTrueonRelative tdFocus">
                                                            <label className="noOfSetHover" id={"toolIdSets" + item.rowId} >
                                                                <input type="text" pattern="[0-9]*" className="inputTable" onBlur={() => this.getQuantity(`${item.rowId}`)} onChange={(e) => this.handleChange(`${item.rowId}`, e)} id="numOfsets" name="numOfsets" value={item.numOfsets} />
                                                            </label>
                                                        </td>}
                                                    <td className="pad-0 positionRelative tdFocus editDateFormat">

                                                        <input type="date" className="inputTable height0 " placeholder="" id="date" min={this.state.currentDate} max={this.state.maxDate} onChange={(e) => this.handleChange(`${item.rowId}`, e)} name="date" value={item.date} />
                                                    </td>
                                                    <td className="typeOfBuying tdFocus">
                                                        <select value={item.typeOfBuy} id="typeofBuying" onChange={(e) => this.handleChange(`${item.rowId}`, e)}>
                                                            <option>Select</option>
                                                            <option value="Adhoc" > Adhoc</option>
                                                            <option value="Planned ">Planned</option>
                                                        </select>
                                                    </td>
                                                    <td className="pad-0 tdFocus">
                                                        <input type="text" className="inputTable " id="remarks" onChange={(e) => this.handleChange(`${item.rowId}`, e)} name="remarks" value={item.remarks} />
                                                    </td>
                                                    <td className="pad-0 pad-lft-8 td-disabled">
                                                        <label>{item.quantity}</label>

                                                    </td>
                                                    {this.props.isBaseAmountActive ? <td className="pad-0 pad-lft-8 td-disabled">
                                                        <label>{item.basic}</label>

                                                    </td> : null}
                                                    <td className="pad-0 pad-lft-8 totalAmountDesc td-disabled">
                                                        <label id={"toolIdCmpr" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`toolIdCmpr${item.rowId}`, `moveLeftCmpr${item.rowId}`)} className={this.props.displayOtb ? item.netAmountTotal > item.otb ? "txtColor amtCompareHover displayPointer" : "pad-top-3 amtCompareHover displayPointer" : "pad-top-3 amtCompareHover displayPointer"}>
                                                            {item.netAmountTotal != "" ? Math.round(item.netAmountTotal * 100) / 100 : ""}{item.netAmountTotal > item.otb ? <span>
                                                                {this.props.displayOtb ? <div className="amtCompare" id={"moveLeftCmpr" + item.rowId}><p>Pi amount cannot be greater than OTB value.</p></div> : null}

                                                            </span> : ""}</label>

                                                        {item.netAmountTotal != "" ? <span className="netAmtHover">
                                                            <img id={"toolId" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`toolId${item.rowId}`, `moveLeft${item.rowId}`)} src={exclaimIcon} />

                                                            <div id={"moveLeft" + item.rowId} className="totalAmtDescDrop">
                                                                {item.finCharges != undefined ? item.finCharges.length != 0 ? <div className="amtContent">

                                                                    <table>
                                                                        <thead>
                                                                            <tr><th><p>Charge Name</p></th>
                                                                                <th><p>% / Amount</p></th>
                                                                                <th><p>Total</p></th></tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {item.finCharges.map((data, key) => (
                                                                                <tr key={key}>
                                                                                    <td><p>{data.chgName}</p></td>
                                                                                    <td><p>{data.rates != undefined ? <span>{data.rates.igstRate} % on {item.basic}</span> : ""}</p>{}</td>
                                                                                    <td><p>{data.charges != undefined ? <span>{data.charges.sign}{data.charges.chargeAmount}</span> : data.sign + data.finChgRate}</p>

                                                                                    </td>
                                                                                </tr>))}
                                                                        </tbody>

                                                                    </table>
                                                                </div> : null : null}
                                                                <div className="amtBottom">
                                                                    <div className="col-md-12">
                                                                        <div className="col-md-6 pad-lft-7">

                                                                            <span>Basic</span>
                                                                        </div>
                                                                        <div className="col-md-6 textRight padRightNone">

                                                                            <span className="totalAmtt">+{item.basic}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="amtBottom">
                                                                    <div className="col-md-12 pad-top-5 m-top-5">
                                                                        <div className="col-md-6">

                                                                            <p>Grand Total</p>
                                                                        </div>
                                                                        <div className="col-md-6 textRight padRightNone">

                                                                            <p className="totalAmt">{Math.round(item.netAmountTotal * 100) / 100}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </span> : ""}
                                                    </td>
                                                    {!this.props.isTaxDisable ? <td className="pad-0 pad-lft-8 td-disabled">
                                                        <label>{item.tax}</label>

                                                    </td> : null}
                                                    {this.props.isRspRequired ? <td className="td-disabled" >
                                                        <label className="displayInline line-h-2" >{item.calculateMargin}</label>
                                                        {item.calculateMargin.toString() != "" ? <span className="calculatedMargin displayInline displayPointer">
                                                            <img src={exclaimIcon} className="hoverImg" id={"marginIdSet" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`marginIdSet${+item.rowId}`, `marginLeft${+item.rowId}`)} />
                                                            <div className="calculatedToolTip totalAmtDescDrop" id={"marginLeft" + item.rowId}>
                                                                <div className="formula">
                                                                    <h3>Calculated Margin</h3>
                                                                    <h5>RSP-COST <span>X</span> <span className="pad-0">100</span><p>RSP</p></h5>
                                                                </div>
                                                            </div>
                                                        </span> : null}
                                                    </td> : null}
                                                    {!this.state.isGSTDisable ? <td className="td-disabled">
                                                        <label>{item.gst != "" ? item.gst + "%" : ""} </label>
                                                    </td> : null}
                                                    {this.props.displayOtb ? <td className="td-disabled">
                                                        <label className="purchaseTableLabel">{item.otb} </label>
                                                    </td> : null}
                                                    {this.props.isDisplayMarginRule ? <td className="typeOfBuying td-disabled">
                                                        <label> {item.marginRule}</label>
                                                    </td> : null}
                                                    {this.props.isRspRequired ? <td className={Number(item.calculateMargin) > Number(item.intakeMargin) ? "errorBorder td-disabled" : "td-disabled"}>
                                                        <label className="displayInline line-h-2" id={"toolIdIntakeMargin" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`toolIdIntakeMargin${item.rowId}`, `moveLeftIntakeMargin${item.rowId}`)}>{item.intakeMargin}</label>
                                                        {item.intakeMargin.toString() != "" ? <span className="calculatedMargin displayInline amtCompareHover displayPointer inTakeMargin">
                                                            {/*<img src={exclaimIcon} id={"intakeMarginSet" + item.rowId} onMouseOver={(e) => this.calculatedMargin(`intakeMarginSet${item.rowId}`, `inTakeMarginLeft${item.rowId}`)} />*/}
                                                            <div className="calculatedToolTip totalAmtDescDrop" id={"inTakeMarginLeft" + item.rowId}>
                                                                <div className="formula">
                                                                    <h3>Intake Margins</h3>
                                                                    <h5><span>RSP</span> <span>X</span> <span className="pad-0">100</span><p className="pad-lft-0">100+COST</p></h5>
                                                                </div>
                                                            </div>
                                                            {Number(item.calculateMargin) > Number(item.intakeMargin) ? <span> <div className="showRate" id={"moveLeftIntakeMargin" + item.rowId}><p>Intake Margin not less than calculated Margin</p></div></span> : ""}
                                                        </span> : null}
                                                    </td> : null}
                                                    {sessionStorage.getItem("partnerEnterpriseName") != "VMART" && this.props.isMrpRequired ?
                                                        <td className="td-disabled">
                                                            <label className="purchaseTableLabel">{item.mrpStart} </label>
                                                        </td>
                                                        : null}
                                                    {sessionStorage.getItem("partnerEnterpriseName") != "VMART" && this.props.isMrpRequired ?
                                                        <td className="td-disabled">
                                                            <label className="purchaseTableLabel">{item.mrpEnd} </label>
                                                        </td>
                                                        : null}

                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        {this.props.department != "" ? <PurchaseItemTable isUDFExist={this.props.isUDFExist} itemUdfExist={this.props.itemUdfExist} isVendorDesignNotReq={this.props.isVendorDesignNotReq}  {...this.props} {...this.state} cat1Validation={this.props.cat1Validation} cat2Validation={this.props.cat2Validation} cat3Validation={this.props.cat3Validation} cat4Validation={this.props.cat4Validation} cat5Validation={this.props.cat5Validation} cat6Validation={this.props.cat6Validation}
                            desc1Validation={this.props.desc1Validation} desc2Validation={this.props.desc2Validation} desc3Validation={this.props.desc3Validation} desc4Validation={this.props.desc4Validation} desc5Validation={this.props.desc5Validation} desc6Validation={this.props.desc6Validation} rows={this.state.rows} updateItemDetails={(e) => this.updateItemDetails(e)} itemDetailsHeader={this.state.itemDetailsHeader} itemDetails={this.state.itemDetails} hl3Code={this.props.hl3Code} department={this.props.department} /> : null}
                        {this.props.isUDFExist == "true" || this.props.itemUdfExist == "true" ? <div className="udfComponent">
                            {this.props.department != "" ? <UdfComponet isUDFExist={this.props.isUDFExist} itemUdfExist={this.props.itemUdfExist} {...this.props} {...this.state} isVendorDesignNotReq={this.props.isVendorDesignNotReq} rows={this.state.rows} setUdfRowsData={this.state.setUdfRowsData} departmentCode={this.props.hl3Code}

                                itemudf1Validation={this.props.itemudf1Validation} itemudf2Validation={this.props.itemudf2Validation} itemudf3Validation={this.props.itemudf3Validation} itemudf4Validation={this.props.itemudf4Validation} itemudf5Validation={this.props.itemudf5Validation}
                                itemudf6Validation={this.props.itemudf6Validation} itemudf7Validation={this.props.itemudf7Validation} itemudf8Validation={this.props.itemudf8Validation} itemudf9Validation={this.props.itemudf9Validation} itemudf10Validation={this.props.itemudf10Validation}
                                itemudf11Validation={this.props.itemudf11Validation} itemudf12Validation={this.props.itemudf12Validation} itemudf13Validation={this.props.itemudf13Validation} itemudf14Validation={this.props.itemudf14Validation} itemudf15Validation={this.props.itemudf15Validation}
                                itemudf16Validation={this.props.itemudf16Validation} itemudf17Validation={this.props.itemudf17Validation} itemudf18Validation={this.props.itemudf18Validation} itemudf19Validation={this.props.itemudf19Validation} itemudf20Validation={this.props.itemudf20Validation}

                                udf1Validation={this.props.udf1Validation} udf2Validation={this.props.udf2Validation} udf3Validation={this.props.udf3Validation} udf4Validation={this.props.udf4Validation} udf5Validation={this.props.udf5Validation}
                                udf6Validation={this.props.udf6Validation} udf7Validation={this.props.udf7Validation} udf8Validation={this.props.udf8Validation} udf9Validation={this.props.udf9Validation} udf10Validation={this.props.udf10Validation}
                                udf11Validation={this.props.udf11Validation} udf12Validation={this.props.udf12Validation} udf3Validation={this.props.udf13Validation} udf14Validation={this.props.udf14Validation} udf15Validation={this.props.udf15Validation}
                                udf16Validation={this.props.udf16Validation} udf17Validation={this.props.udf17Validation} udf18Validation={this.props.udf18Validation} udf19Validation={this.props.udf19Validation} udf20Validation={this.props.udf20Validation}
                                udfItemHeadData={this.state.udfItemHeadData} itemUdfUpdate={(e) => this.itemUdfUpdate(e)} updateUdf={(e) => this.updateUdf(e)} itemDetails={this.state.itemDetails} department={this.props.department} /> : null}
                        </div> : null}
                    </div>
                    <div className="col-md-12 col-sm-12 pad-0">
                        <div className="footerDivForm height4per">
                            <ul className="list-inline m-lft-0 m-top-10">
                                {this.props.department == "" ? <li><button className="clear_button_vendor" type="reset" disabled>Clear</button>
                                </li> : <li><button className="clear_button_vendor" type="reset" onClick={(e) => this.onResetData(e)}>Clear</button>
                                    </li>}
                                <li>{this.state.rateDisable ? <button type="button" className="save_button_vendor btnDisabled" id="saveButton">Save</button> : <button type="button" onClick={(e) => this.onSubmit(e)} className="save_button_vendor" id="saveButton">Save</button>}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.imageModal ? <PiImageModal {...this.state} {...this.props} file={this.state.imageState} imageName={this.state.imageName} updateImage={(e) => this.updateImage(e)} imageModalAnimation={this.state.imageModalAnimation} closePiImageModal={(e) => this.closePiImageModal(e)} closeImageModal={(e) => this.openImageModal(e)} imageRowId={this.state.imageRowId} /> : null}
                {/* {this.state.mrpModal ? <PiMrpModal {...this.state} {...this.props} idx={this.state.idx} slcode={this.props.slcode} department={this.props.department} rows={this.state.rows} mrpCloseModal={(e) => this.mrpCloseModal(e)} updateMrpState={(e) => this.updateMrpState(e)} mrpModalAnimation={this.state.mrpModalAnimation} rowId={this.state.rowIdentity} closeMrpModal={(e) => this.openMrpModal(e)} /> : null} */}
                {this.state.sizeModal ? <PiSizeModal  {...this.props} {...this.state} sizeDataList={this.state.sizeDataList} division={this.props.division} section={this.props.section} hl3Code={this.props.hl3Code} sizeArray={this.state.sizeArray} updateSizeList={(e) => this.updateSizeList(e)} closePiSizeModal={(e) => this.closePiSizeModal(e)} updateSizeState={(e) => this.updateSizeState(e)}
                    sizeRow={this.state.sizeRow} sizeCode={this.state.sizeCode} articleName={this.state.articleName} department={this.props.department} sizeValue={this.state.sizeValue} sizes={this.state.sizes} sizeModalAnimatiion={this.state.sizeModalAnimatiion} closePiSizeeModal={(e) => this.openPiSizeModal(e)} /> : null}
                {this.state.colorModal ? <PiColorModal {...this.props} {...this.state} deselectallProp={(e) => this.deselectallProp(e)} colorListValue={this.state.colorListValue} colorCode={this.state.colorCode} colorValue={this.state.colorValue}
                    department={this.props.department} updateColorList={(e) => this.updateColorList(e)} updateColorState={(e) => this.updateColorState(e)} colorModalAnimation={this.state.colorModalAnimation}
                    colorRow={this.state.colorRow} closePiColorModal={(e) => this.closePiColorModal(e)} closePiiColorModal={(e) => this.openPiColorModal(e)} /> : null}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
                {this.state.confirmModal ? <ConfirmModal {...this.props} {...this.state} deleteRows={(e) => this.deleteRows(e)} resetData={(e) => this.resetData(e)} closeConfirmModal={(e) => this.closeConfirmModal(e)} /> : null}
                {/* {this.state.hsnModal ? <HsnCodeModal {...this.props} {...this.state} departmentCode={this.props.hl3Code} hsnModal={this.state.hsnModal} hsnModalAnimation={this.state.hsnModalAnimation} closeHsnModal={(e) => this.closeHsnModal(e)} updateHsnCode={(e) => this.updateHsnCode(e)} /> : null} */}
                {this.state.itemModal ? <ItemCodeModal {...this.props} {...this.state} desc6={this.state.desc6} rowId={this.state.poRows} descId={this.state.descId} itemModalAnimation={this.state.itemModalAnimation} startRange={this.state.startRange} endRange={this.state.endRange} code={this.state.deleteArticleCode} closeItemmModal={() => this.closeItemmModal()} updateItem={(e) => this.updateItem(e)} /> : ""}
                {/* {this.state.articleNew ? <ArticleNewModal {...this.props} {...this.state} updateArticleNew={(e) => this.updateArticleNew(e)} closeArticleModal={(e) => this.closeArticleModal(e)} /> : null} */}
                {this.state.discountModal ? <DiscountModal {...this.props} {...this.state} selectedDiscount={this.state.selectedDiscount} discountGrid={this.state.discountGrid} closeDiscountModal={(e) => this.closeDiscountModal(e)} isDiscountMap={this.props.isDiscountMap} vendorMrpPo={this.state.vendorMrpPo} updateDiscountModal={(e) => this.updateDiscountModal(e)} discountMrp={this.state.discountMrp} /> : null}

            </div>
        );
    }
}
export default PurchaseIndentTable;