import React from 'react';
import ExcelUploadSearch from './excelUploadSearchModal';
import _ from 'lodash';
import merge from 'lodash/merge'
import MapColumn from './mapColumn'
import DataValidation from './dataValidation'
import Summary from './summary'
import ChooseFile from './chooseFile'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import SpreadsheetComponent from 'react-spreadsheet-component'
var Dispatcher = require('../../../node_modules/react-spreadsheet-component/lib/dispatcher.js');
import XLSX from 'xlsx';
import PoError from "../loaders/poError";

class ExcelUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fixedFieldNames:  [],
            mapColData: false,
            searchItem: false,
            totalSteps: ["chooseFile"],
            stepCounter: 1,
            currentStep: "chooseFile",
            file: {},
            isPo: false,
            isPi: true,
            fileName: "",
            excelData: [],
            excelDataOriginal: [],
            // id: this.props.spreadsheetId || Helpers.makeSpreadsheetId(),
            mappedHeaders: {},
            mappingCounter: 0,
            excelData2: [],
            spreadSheetData: [],
            validateDataa: [],
            counter: 0,
            totalQty: {},
            totalAmt: {},
            totalCalculatedAmt: "-",
            totalCalculatedQty: "-",
            totalLineItems: "-",
            disableProceedBtn: false
        }
    }

    componentDidMount(){
        Dispatcher.subscribe('dataChanged',  (data) => {
            console.log("tempooo", data)
            this.setState({
                excelData: data.rows,
                excelDataOriginal: data.rows
            })
            // if(data != undefined || data.lenght != 0) {
            //     this.arrToObject(data)
            // }
        }, "spreadsheet-1")
    }


    componentDidUpdate(nextProps, prevState) {
        console.log(nextProps.excelUpload)
        if(nextProps.excelUpload.excelHeaders.isSuccess){
            if(nextProps.excelUpload.excelHeaders.data.resource != null){
                this.setState({
                    excelHeaderData : nextProps.excelUpload.excelHeaders.data.resource.excelHeader,
                    mandateHeader: Object.values(nextProps.excelUpload.excelHeaders.data.resource.mandateHeader),
                    fixedFieldNames : nextProps.excelUpload.excelHeaders.data.resource.excelHeader != undefined && nextProps.excelUpload.excelHeaders.data.resource.excelHeader != null ? Object.values(nextProps.excelUpload.excelHeaders.data.resource.excelHeader) : [],
                    keyNames : nextProps.excelUpload.excelHeaders.data.resource.excelHeader != undefined && nextProps.excelUpload.excelHeaders.data.resource.excelHeader != null ? Object.keys(nextProps.excelUpload.excelHeaders.data.resource.excelHeader) : []
                }, () => {
                    this.extractSearchHeaders(nextProps.excelUpload.excelHeaders.data.resource.searchHeader)
                })
            }
        }
    }

    extractSearchHeaders = (data1) => {
        let data = _.cloneDeep(data1)
        let mainHeader = data.mainHeader
        let catDescHeaders = data.catDescHeader
        let itemUdfHeaders = data.itemUdfHeader
        let totalHeaders = {...mainHeader, ...catDescHeaders, ...itemUdfHeaders}

        this.setState({
            totalSearchHeader: totalHeaders,
            mainSearchHeader: mainHeader,
            catDescSearchHeader: catDescHeaders,
            itemUdfSearchHeader: itemUdfHeaders
        })
    }

    openMapColData(e) {
        e.preventDefault();
        this.setState({
            mapColData: !this.state.mapColData
        });
    }

    openSearchItem(e) {
        e.preventDefault();
        this.setState({
            searchItem: !this.state.searchItem
        });
    }
    CloseSearchItem = () => {
        this.setState({ searchItem: false
        });
    }
    finalMappedHeaders = (mappingObj, bool) => {
        let tempObj = {}

        console.log(Object.keys(mappingObj).length)
        if (Object.keys(mappingObj).length != this.state.fixedFieldNames.length || bool == false) {
            this.state.fixedFieldNames.forEach(el => {
                if (mappingObj.hasOwnProperty(el)) {
                    tempObj[el] = mappingObj[el]
                }
                else {
                    tempObj[el] = ""
                }
            })
            this.setState({
                mappedHeaders: tempObj
            });
        }
        else{
            if(Object.keys(mappingObj).length == this.state.fixedFieldNames.length ){
                this.setState({
                    mappedHeaders: mappingObj
                });
            }
        }
        console.log("tempObj", tempObj)

    }

    checkMandate = () => {
        let temp = _.cloneDeep(this.state.mappedHeaders)
        let temp1 = Object.keys(temp)
        let counter = 0
        this.state.mandateHeader.map((data, key) => {
            if(temp1.indexOf(data) > -1){
                if(temp[data] == ""){
                    counter++
                }
            }
        })
        if(counter == 0){
            return true
        }
        else{
            return false
        }
    }

    proceedButton() {
        if(this.state.fileName != ""){
            let localCounter = _.cloneDeep(this.state.stepCounter)
            var addStep =_.cloneDeep(this.state.totalSteps);
            if(this.state.stepCounter < 5 || localCounter < 5){
                localCounter++
             this.setState({
                 isMapColumn: true,
                 stepCounter: this.state.stepCounter + 1
             }, () => {
                     if(this.state.stepCounter == 2 || localCounter == 2) {
                         addStep.push("dataPreview")
                     }
                     else if(this.state.stepCounter == 3 || localCounter == 3 ){
                         addStep.push("mapColumn")
                     }
                     else if(this.state.stepCounter == 4 || localCounter == 4){
                         addStep.push("dataValidation")
                     }
                     else if(this.state.stepCounter == 5 || localCounter == 5){
                        if(this.state.validateDataa.length != 0){
                            addStep.push("summary")
                        }
                        else{
                            addStep.push("dataValidation")
                            this.setState({
                                errorMassage: "Please Validate first!",
                                poErrorMsg: true,
                                stepCounter: this.state.stepCounter - 1
                            })
                        }
                        localCounter--
                     }
                 if(addStep[addStep.length-1] == "chooseFile") {
                     this.setState({
                         currentStep: "chooseFile",
                         totalSteps: addStep
                     })
                 }
                 else if(addStep[addStep.length-1] == "dataPreview"){
                     this.setState({
                         currentStep: "dataPreview",
                         totalSteps: addStep
                     })
                 }
                 else if(addStep[addStep.length-1] == "mapColumn"){
                     this.setState({
                         currentStep: "mapColumn",
                         totalSteps: addStep
                     })
                 }
                 else if(addStep[addStep.length-1] == "dataValidation"){
                     var boolean = this.checkMandate()
                     if(boolean){
                        this.setState({
                            currentStep: "dataValidation",
                            totalSteps: addStep
                        })
                     }
                     else{
                        this.setState({
                            errorMassage: "Please map all the Mandate fields*",
                            poErrorMsg: true,
                            stepCounter:  this.state.stepCounter - 1
                        })
                        localCounter--
                     }

                 }
                 else if(addStep[addStep.length-1] == "summary"){
                        this.setState({
                            currentStep: "summary",
                            totalSteps: addStep
                        })
                 }
             })
            }
            else{
                this.handleSubmit()
             }
        }
        else{
            this.setState({
                errorMassage: "Please upload an Excel file first!",
                poErrorMsg: true

            })
        }
    }

    getDatafromValidation = (data1, data2) => {
        let tempQty = _.cloneDeep(this.state.totalQty)
        let tempAmt = _.cloneDeep(this.state.totalAmt)
        let totalCAmt = _.cloneDeep(this.state.totalCalculatedAmt)
        let totalCQty = _.cloneDeep(this.state.totalCalculatedAmt)

        console.log(data1, data2)
        this.setState({
            spreadSheetData: data1,
            validateDataa: data2,
        })

        data2.map(data1 =>{
            Object.keys(data1).map(data2 => {
                if(tempQty.hasOwnProperty(data2) != true && tempAmt.hasOwnProperty(data2) != true){
                    tempQty[data2] = data1[data2].totalQty
                    tempAmt[data2] = data1[data2].totalNetAmount
                }

                // tempQty.push(data1[data2].totalQty)
                // tempAmt.push(data1[data2].totalNetAmount)
            })
        })


         totalCAmt = Object.values(tempAmt).reduce((a, b) => a + b, 0)
         totalCQty = Object.values(tempQty).reduce((a, b) => a + b, 0)

        this.setState({
            totalQty: tempQty,
            totalAmt: tempAmt,
            totalCalculatedAmt: totalCAmt,
            totalCalculatedQty: totalCQty,
        })
    }
    getNumberOfDatafromValidation = (data) => {
        this.setState({
            uniquee: data
        })
    }

    handleBack(){
        var addStep =_.cloneDeep(this.state.totalSteps);

            addStep.pop()
            if(addStep.length >= 1){
                this.setState({
                    stepCounter: this.state.stepCounter - 1
                })
            }

            if(addStep[addStep.length-1] == "chooseFile") {
                this.setState({
                    currentStep: "chooseFile",
                    totalSteps: addStep
                })
            }
            else if(addStep[addStep.length-1] == "dataPreview"){
                this.setState({
                    currentStep: "dataPreview",
                    totalSteps: addStep
                })
            }
            else if(addStep[addStep.length-1] == "mapColumn"){
                this.setState({
                    currentStep: "mapColumn",
                    totalSteps: addStep,
                    mappingCounter: 1
                })
            }
            else if(addStep[addStep.length-1] == "dataValidation"){
                this.setState({
                    currentStep: "dataValidation",
                    totalSteps: addStep
                })
            }
            else if(addStep[addStep.length-1] == "summary"){
                this.setState({
                    currentStep: "summary",
                    totalSteps: addStep
                })
            }
    }

    validatedData = (data) => {
        let temp = data.filter(function(value, index, arr){ return value.length > 0;})
        return temp
    }

    fileChange = (e) => {
        let values = Object.values(e.target.files)
        let fileName = values[0].name

            var validExts = new Array(".xlsx", ".xls", ".csv");
            var fileExt = fileName;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
              alert("Invalid file selected, valid files are of " +
                       validExts.toString() + " types.");
            }
            else{
                const reader = new FileReader();
                const rABS = reader.readAsArrayBuffer(e.target.files[0])
                reader.onload = (e) => {
                    /* Parse data */
                    const bstr = e.target.result;
                    const wb = XLSX.read(bstr, {type:rABS ? 'binary' : 'array'});
                    /* Get first worksheet */
                    const wsname = wb.SheetNames[0];
                    const ws = wb.Sheets[wsname];
                    /* Convert array of arrays */
                    const data = XLSX.utils.sheet_to_json(ws, {header:1});
                    // let data = this.getDateCorrectedData(data)
                    // this.ExcelDateToJSDate()

                    let validatedData = this.validatedData(data)

                    /* Update state */
                    this.setState({
                        excelData: validatedData,
                        excelDataOriginal: validatedData
                    });
                    const wss = XLSX.utils.aoa_to_sheet(validatedData);
                    const wbb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wbb, wss, "SheetJS");
                    // cb(wbb);

                var htmlstr = XLSX.write(wb,{type:'binary',bookType:'html'});
                this.setState({
                    file: htmlstr,
                    fileName: fileName
                })
                // document.getElementById("spreadsheet").dangerouslySetInnerHTML += htmlstr;
                //     /* generate XLSX file and send to client */
                //     // XLSX.writeFile(wbb, "sheetjs.xlsx")
                };
            }


        // let file = { ...this.state.file };
        // let fileName
        // console.log( e.target.value, Object.values(e.target.files))
        // let values = Object.values(e.target.files)

        //                     if (values[i].type == "") {

        //                         file[values[i].name] = values[i]
        //                         fileName = values[i].name
        //                     } else {
        //                         this.setState({
        //                             toastMsg: "Allowed Excel types xlsx, xls, csv",
        //                             toastLoader: true
        //                         })

        //                         e.target.value = ""
        //                         setTimeout(() => {
        //                             this.setState({
        //                                 toastLoader: false
        //                             })
        //                         }, 2500)

        //                         break
        //                     }

        //         e.target.value = ""
        //         setTimeout(() => {
        //             this.setState({
        //                 toastLoader: false
        //             })
        //         }, 2500)

        //         break

        //     console.log('change file', file)
        //     this.setState({
        //         file: file,
        //         fileName: fileName
        //     })
        //     e.target.value = ""
    }


    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false
        })
    }
    handleValidate = () => {
        this.setState({
            counter: 0
        })
        this.child.method()
    }
    handleSubmit = () => {
        this.child1.method1()
    }
    isPiPo = (data, dataPi) => {
        this.setState({
            isPo: data,
            isPi: dataPi
        })
    }

    handleCrossBtnParent = () => {
        this.setState({
            fileName: ""
        })
    }

    getInitialDatafromValidation = (data) => {
        var temp = data.reduce((a, b) => a + b, 0)
        this.setState({
            totalLineItems: temp
        })
    }
    disableProceedBtn = (data) => {
        this.setState({
            disableProceedBtn: data
        })
    }

    render () {
        console.log(this.state.totalLineItems)

        const testVars = {
            initialData: {
                rows: this.state.excelDataOriginal != null ? this.state.excelDataOriginal : []
            },
            config: {
              rows: 10,
              columns: 20,
              hasHeadColumn: false,
              isHeadColumnString: false,
              hasHeadRow: true,
              isHeadRowString: false,
              canAddRow: false,
              canAddColumn: false,
              emptyValueSymbol: '-',
              hasLetterNumberHeads: false
            },
            cellClasses: {
                rows: [
                    ['', '', '', '', '', '', '', ''],
                    ['', '', '', '', '', '', '', ''],
                    ['', '', '', '', '', '', '', ''],
                    ['', '', '', '', '', '', '', ''],
                ]
            }
          };
          console.log(testVars)

        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="excel-upload-page-head">
                        <div className="euph-left">
                            <div className="euphl-dataline euphl-processed">
                                <p>Choose File</p>
                                <span className="euphldl-count">1</span>
                            </div>
                            <div className= { this.state.totalSteps.indexOf("dataPreview") > -1  ? "euphl-dataline euphl-processed" : "euphl-dataline" }>
                                <p>Data Preview</p>
                                <span className="euphldl-count">2</span>
                            </div>
                            <div className= { this.state.totalSteps.indexOf("mapColumn") > -1  ? "euphl-dataline euphl-processed" : "euphl-dataline" }>
                                <p>Map Column</p>
                                <span className="euphldl-count">3</span>
                            </div>
                            <div className= { this.state.totalSteps.indexOf("dataValidation") > -1 ? "euphl-dataline euphl-processed" : "euphl-dataline" }>
                                <p>Data Validation</p>
                                <span className="euphldl-count">4</span>
                            </div>
                            <div className= { this.state.totalSteps.indexOf("summary") > -1 ? "euphl-dataline aftenNone euphl-processed" : "euphl-dataline aftenNone" }>
                                <p>Summary</p>
                                <span className="euphldl-count">5</span>
                            </div>
                        </div>
                        <div className="euph-right">
                            {/* <button type="button" className="euph-back" onClick={() => this.props.history.push("/digiproc/excelUploadHistory")}>Back</button> */}
                            <button type="button" className="euph-back" onClick={() => this.handleBack()}>Back</button>

                            {
                                this.state.currentStep == "dataValidation" ?
                                    <button type="button" className="euph-validate" onClick = {() => this.handleValidate()}>Validate</button>
                                    : null
                            }
                            {
                                this.state.disableProceedBtn == false ?
                                    <button type="button" className="euph-proceed" onClick={() => this.proceedButton()}>{this.state.currentStep == "summary" ? "Final Submit" : "Proceed"}
                                        <span className="euph-arrow-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13.486" height="6.532" viewBox="0 0 13.486 6.532">
                                                <g>
                                                    <path fill="#fff" d="M13.332 134.893l-2.753-2.739a.527.527 0 0 0-.743.747l1.848 1.839H.527a.527.527 0 1 0 0 1.054h11.156l-1.848 1.839a.527.527 0 0 0 .743.747l2.753-2.739a.527.527 0 0 0 .001-.748z" transform="translate(0 -132)" />
                                                </g>
                                            </svg>
                                        </span>
                                    </button>
                                    : null
                            }
                            
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 pad-0">
                    <div className="excel-upload-page-mid p-lr-47">
                        <div className="eupm-left">
                            {
                                this.state.currentStep == "chooseFile" || this.state.currentStep == "dataPreview" || this.state.currentStep == "mapColumn" ?
                                    <div className="eupm-inner">
                                        <h3>Upload Excel Wizard</h3>
                                        <p>You can upload only in .xlsx, .xls or .csv formats and file size should not exceed 10mb</p>
                                        {/* <p>Map your template data</p> */}
                                    </div>
                                :null
                            }
                            {
                                 this.state.currentStep == "dataValidation" || this.state.currentStep == "summary" ?
                                    <div className="eupm-summary">
                                        <h3>Summary</h3>
                                        <div className="eupms-bottom">
                                            <div className="eupmsb-inner">
                                                <p>Total Items Uploaded</p>
                                                <h5>{this.state.totalLineItems}</h5>
                                            </div>
                                            <div className="eupmsb-inner">
                                                {this.state.isPo ? <p>Total PO</p> : <p>Total PI</p>}
                                                <h5>{this.state.uniquee != undefined ? this.state.uniquee.length : "-"}</h5>
                                            </div>
                                            <div className="eupmsb-inner">
                                                <p>Total Quantity</p>
                                                <h5>{this.state.totalCalculatedQty != "-" ? <span className="repee">&#x20B9; </span> : <span></span>}{this.state.totalCalculatedQty}</h5>
                                            </div>
                                            <div className="eupmsb-inner">
                                                <p>Total Amount</p>
                                                <h5>{this.state.totalCalculatedAmt != "-" ? <span className="repee">&#x20B9; </span> : <span></span>}{this.state.totalCalculatedAmt}</h5>
                                            </div>
                                        </div>
                                    </div>
                                :null
                            }
                        </div>
                        {
                           this.state.currentStep == "dataValidation"  ?
                                <div className="eupm-right">
                                    <span className="gen-info-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="35" height="20" viewBox="0 0 14 14">
                                            <path id="iconmonstr-info-2" fill="#97abbf" d="M7 1.167A5.833 5.833 0 1 1 1.167 7 5.84 5.84 0 0 1 7 1.167zM7 0a7 7 0 1 0 7 7 7 7 0 0 0-7-7zm.583 10.5H6.417V5.833h1.166zM7 3.354a.729.729 0 1 1-.729.729A.729.729 0 0 1 7 3.354z"/>
                                        </svg>
                                        <div className="gii-dropdown gii-dropright">
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" checked/>
                                                <span className="checkmark1 checkmark1-red"></span>
                                                Red: Mandatory fields
                                            </label>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" checked/>
                                                <span className="checkmark1 checkmark1-orange"></span>
                                                Orange: Mismatched fields
                                            </label>
                                            <label className="checkBoxLabel0 displayPointer">
                                                <input type="checkBox" className="checkBoxTab" checked/>
                                                <span className="checkmark1 checkmark1-yellow"></span>
                                                Yellow: Extinct fields
                                            </label>
                                            <p>*Contact <span className="bold">Supplymint</span> for more info..</p>
                                        </div>
                                    </span>
                                    <button type="button" className="eupm-search" onClick={(e) => this.openSearchItem(e)}>Search Item <img src={require('../../assets/searchBold.svg')} /></button>
                                </div>
                            :null
                        }
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 execel-upload-overflow-manage">
                    {/* ==================== Step 1 Choose File ======================= */}
                    { this.state.currentStep == "chooseFile" ?
                        <ChooseFile {...this.state} {...this.props} fileChange = {this.fileChange} isPiPo = {this.isPiPo} handleCrossBtnParent = {this.handleCrossBtnParent} />
                        :null
                    }
                    {/* ========================= Step 2 Data Preview ===================== */}

                    { this.state.currentStep == "dataPreview"  ?
                        <div className="excel-upload-page-bottom m-top-20">
                            <div id="spreadsheet" className="eupbc-table">
                                {/* <div id="spreadsheet" >
                                    {this.state.file != null ? this.state.file : null}
                                </div> */}
                                <SpreadsheetComponent
                                initialData={testVars.initialData}
                                config={testVars.config}
                                cellClasses={testVars.cellClasses}
                                spreadsheetId="spreadsheet-1"
                                // onCellValueChange={() => this.handleCellValueChange()}
                                 />
                            </div>
                        </div>
                     : null}
                    {/* ========================= Step 3 Map Column ======================= */}
                   {
                       this.state.currentStep == "mapColumn"  ?
                       <MapColumn {...this.state} {...this.props} finalMappedHeaders = {this.finalMappedHeaders} />
                        : null
                   }
                    {/* ======================== Step 4 Data Validation ========================== */}
                        {
                            this.state.currentStep == "dataValidation" ?
                            <DataValidation {...this.state} {...this.props} getInitialDatafromValidation = {this.getInitialDatafromValidation} handleValidate = {this.handleValidate} onRef={ref => (this.child = ref)} getDatafromValidation = {this.getDatafromValidation} getNumberOfDatafromValidation = {this.getNumberOfDatafromValidation}/>
                            :null
                        }
                    {/* ========================= Step 5 Summary ========================= */}
                        {
                            this.state.currentStep == "summary" ?
                                <Summary {...this.state} {...this.props}  handleSubmit = {this.handleSubmit} onRef={ref => (this.child1 = ref)} disableProceedBtn = {this.disableProceedBtn}/>
                        : null
                        }
                </div>
                {this.state.searchItem &&<ExcelUploadSearch CloseSearchItem={this.CloseSearchItem} {...this.props} {...this.state} />}
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}



export function mapStateToProps(state) {
    console.log(state)
    return {
        excelUpload: state.excelUpload
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExcelUpload);
