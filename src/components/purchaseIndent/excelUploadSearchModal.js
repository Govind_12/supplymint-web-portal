import React from 'react';
import PoError from "../loaders/poError";
import Pagination from "../pagination";

class ExcelUploadSearch extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            fixedFieldNames:  [],
            search: "",
            attributeValue:"SITE_NAME",
            fieldNames: [],
            copiedField: "",
            selectedDpt: "",
            tagState: false,
            jumpPage: 1,
            actiobBtn: "Close"
        }
    }

    componentDidMount(){

        let tempSearch = {}
        tempSearch[this.state.attributeValue] = this.state.search

        let payload = {
            pageNo: 1,
            "hl3Name": "",
            search: tempSearch,
        }
        this.props.excelSearchRequest(payload)  
        this.setState({
            fieldNames: [],
            fixedFieldNames: Object.values(this.props.totalSearchHeader),
            mainSearchHeader: _.cloneDeep(this.props.mainSearchHeader),
            catDescSearchHeader: _.cloneDeep(this.props.catDescSearchHeader),
            itemUdfSearchHeader: _.cloneDeep(this.props.itemUdfSearchHeader)
        })
    }

    componentDidUpdate(nextProps, prevState) {
        console.log(nextProps)
        if(nextProps.excelUpload.excelSearch.isSuccess){
            if(nextProps.excelUpload.excelSearch.data.resource != null){
                this.setState({
                    fieldNames : nextProps.excelUpload.excelSearch.data.resource,
                    prev: nextProps.excelUpload.excelSearch.data.prePage,
                    current: nextProps.excelUpload.excelSearch.data.currPage,
                    next: nextProps.excelUpload.excelSearch.data.currPage + 1,
                    maxPage: nextProps.excelUpload.excelSearch.data.maxPage,
                    selectedItems: nextProps.excelUpload.excelSearch.data.totalRecord == undefined || nextProps.excelUpload.excelSearch.data.totalRecord == null 
                               ? 0 : nextProps.excelUpload.excelSearch.data.totalRecord,
                    jumpPage: nextProps.excelUpload.excelSearch.data.currPage 
                }) 
            }
            else{
                this.setState({
                    noDataMessage: nextProps.excelUpload.excelSearch.data.message,
                    fieldNames : nextProps.excelUpload.excelSearch.data.resource,
                })
            }
        }
        else if(nextProps.excelUpload.excelSearch.isError){ 
            if(nextProps.excelUpload.excelSearch.data == null){
                this.setState({
                    poErrorMsg: true,
                    errorMassage: nextProps.excelUpload.excelSearch.error.errorMessage
                })
            }
        }
        if(nextProps.excelUpload.excelHeaders.isSuccess){
            if(nextProps.excelUpload.excelHeaders.data.resource != null){
                this.setState({
                    excelHeaderData : nextProps.excelUpload.excelHeaders.data.resource.excelHeader,
                    fixedFieldNames : Object.values(nextProps.excelUpload.excelHeaders.data.resource.excelHeader)

                })
            }
        }
        this.props.excelSearchClear()
    }

    handleChange(e) {
        this.setState(
            {
                search: e.target.value
            }
        );
    }
    handleOnKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
    }
    onSearch = () => {
            let tempSearch = {}
            let tempMainHeader = Object.values(this.state.mainSearchHeader)
            let tempCatDescHeader = Object.values(this.state.catDescSearchHeader)
            let tempItemUdfHeader = Object.values(this.state.itemUdfSearchHeader)

            
            if(tempMainHeader.indexOf(this.state.attributeValue) > -1){

                    tempSearch[this.state.attributeValue] = this.state.search
                    this.setState({
                        showRadioButtons: false,
                        actiobBtn: "Close",
                        tagState: false
                    })
                    let payload = {
                        pageNo: 1,
                        "hl3Name": "",
                        search: tempSearch
                    }
                    this.props.excelSearchRequest(payload)    
                
               
            }
            else if(tempCatDescHeader.indexOf(this.state.attributeValue) > -1 || tempItemUdfHeader.indexOf(this.state.attributeValue) > -1 ){
                if(this.state.selectedDpt != ""){
                    this.setState({
                        showRadioButtons: false,
                        actiobBtn: "Close",
                    })
                    this.handleDone()
                }
                else{
                    tempSearch["hl3Name"] = this.state.search
                    this.setState({
                        showRadioButtons: true,
                        actiobBtn: "Done"
                    })
                    let payload = {
                        pageNo: 1,
                        "hl3Name": "",
                        search: tempSearch
                    }
                    this.props.excelSearchRequest(payload) 
                }
            }
        }

    handleAttributeValue(e){
        console.log(e.target.value)
        this.setState({
            attributeValue: e.target.value
        }, () => {
            this.onSearch()
        })
        
    }

    handleCancelSearch(){

        this.setState({
            fieldNames: [],
            showRadioButtons:false,
            actiobBtn: "Close",
            search: "",
            attributeValue:"SITE_NAME"
        }, () => {
            this.onSearch()
        })
        
    }

    handleCopy(data) {
        this.setState({
            copiedField: data
        })
        navigator.clipboard.writeText(data)
    }
    handleEsc(e){
        if (e.keyCode == 27 || e.key ==  "Escape") {
            document.getElementById("closeBtn").click();
        }
    }
    handleRadioChange = (e) => {
        console.log(e.target.value)
        let selectedDpt = e.target.value
        this.setState({
            selectedDpt: selectedDpt,
            actiobBtn: "Done",
            tagState: true
        })
    }

    clearTag = (e) => {
        this.setState({
            tagState: false,
            selectedDpt: ""
        }, () => {
            this.onSearch()
        })
        
    }

    handleDone = () => {
        if(this.state.selectedDpt != ""){
            if(this.state.copiedField != ""){
                this.props.CloseSearchItem()
                this.setState({
                    actiobBtn: "Close"
                })
            }
            else{
                let tempSearch = {};
                tempSearch[this.state.attributeValue] = this.state.search
                let payload = {
                    pageNo: 1,
                    "hl3Name": this.state.selectedDpt,
                    search: tempSearch,
                }
                this.setState({
                    showRadioButtons: false,
                    actionBtn: "Close",
                }, () => {
                    this.props.excelSearchRequest(payload)  
                })
                
            }
        }
        else if(this.state.tagState && this.state.selectedDpt == "" || this.state.showRadioButtons){
            this.setState({
                errorMassage: "Please Select a Department first!",
                poErrorMsg: true,
                actiobBtn: "Close"
            })
        }
        else{
            this.setState({
                actiobBtn: "Close"
            })
            this.props.CloseSearchItem()
        }
    }

    closeErrorRequest(e) {
        this.setState({
            poErrorMsg: false
        })
    }

    page = (e) => {
        let tempSearch = {}
        tempSearch[this.state.attributeValue] = this.state.search

        if (e.target.id == "prev") {
            if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {
            } else {
                this.setState({
                    prev: this.props.excelUpload.excelSearch.data.prePage,
                    current: this.props.excelUpload.excelSearch.data.currPage - 1,
                    next: this.props.excelUpload.excelSearch.data.currPage + 1,
                    maxPage: this.props.excelUpload.excelSearch.data.maxPage
                })
                if (this.props.excelUpload.excelSearch.data.prePage != 0) {
                    let payload = {
                        pageNo: this.props.excelUpload.excelSearch.data.currPage - 1,
                        "hl3Name": this.state.selectedDpt,
                        search: tempSearch,
                    }
                    this.props.excelSearchRequest(payload)  
                }
            }
        } else if (e.target.id == "next") {
            this.setState({
                    prev: this.props.excelUpload.excelSearch.data.prePage,
                    current: this.props.excelUpload.excelSearch.data.currPage + 1,
                    next: this.props.excelUpload.excelSearch.data.currPage + 1,
                    maxPage: this.props.excelUpload.excelSearch.data.maxPage
                })
            if (this.props.excelUpload.excelSearch.data.currPage != this.props.excelUpload.excelSearch.data.maxPage) {
                let payload = {
                    pageNo: this.props.excelUpload.excelSearch.data.currPage + 1,
                    "hl3Name": this.state.selectedDpt,
                    search: tempSearch,
                }
                this.props.excelSearchRequest(payload)  
            }
        }
        else if (e.target.id == "first") {
            if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.excelUpload.excelSearch.data.prePage,
                    current: 1,
                    next: this.props.excelUpload.excelSearch.data.currPage + 1,
                    maxPage: this.props.excelUpload.excelSearch.data.maxPage
                })
                if (this.props.excelUpload.excelSearch.data.currPage <= this.props.excelUpload.excelSearch.data.maxPage) {
                    let payload = {
                        pageNo: 1,
                        "hl3Name": this.state.selectedDpt,
                        search: tempSearch,
                    }
                    this.props.excelSearchRequest(payload)  
                }
            }
        } else if (e.target.id == "last") {
            if (this.state.current == this.state.maxPage || this.state.current == undefined) {
            }
            else {
                this.setState({
                    prev: this.props.excelUpload.excelSearch.data.prePage,
                    current: this.props.excelUpload.excelSearch.data.currPage,
                    next: this.props.excelUpload.excelSearch.data.currPage + 1,
                    maxPage: this.props.excelUpload.excelSearch.data.maxPage
                })
                if (this.props.excelUpload.excelSearch.data.currPage <= this.props.excelUpload.excelSearch.data.maxPage) {
                    let payload = {
                        pageNo: this.props.excelUpload.excelSearch.data.maxPage,
                        "hl3Name": this.state.selectedDpt,
                        search: tempSearch,
                    }
                    this.props.excelSearchRequest(payload)  
                }
            }
        }
    }

    getAnyPage = (e) => {
        if (e.target.validity.valid) {
            this.setState({ 
                jumpPage: e.target.value 
            })
            if (e.key == "Enter" && e.target.value != this.state.current) {
                if (e.target.value != "") {
                    let payload = {
                        pageNo: _.target.value,
                        "hl3Name": this.state.selectedDpt,
                        search: tempSearch,
                    }
                    this.props.excelSearchRequest(payload)  
                }
                else {
                    this.setState({
                        toastMsg: "Page No should not be empty..",
                        toastLoader: true
                    })
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 10000);
                }
            }
        }
    }

    render () {
        console.log(this.state.fieldNames)
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new" onKeyDown = {(e) => this.handleEsc(e)}></div>
                <div className="modal-content excel-search-modal">
                    <div className="esm-head">
                        <h3>Search Items</h3>
                        <button type="button" id = "closeBtn" onClick={this.props.CloseSearchItem}><img src={require('../../assets/clearSearch.svg')} /></button>
                    </div>
                    <div className="esm-body">
                        <div className="esmb-head">
                            <div className="esmbh-left">
                                <label>Attributes</label>
                                <select onChange = {(e) => this.handleAttributeValue(e)}>
                                    {
                                        this.state.fixedFieldNames.map((data1, key) => (
                                            <option key = {key}>{data1}</option>
                                        ))
                                    }
                                    
                                </select>
                            </div>

                            {
                                this.state.tagState ?
                                <div className="col-lg-5">
                                    <div className="show-applied-filter">
                                        <button type="button" className="saf-btn">{this.state.selectedDpt}
                                            <img onClick={(e) => this.clearTag(e)} src={require('../../assets/clearSearch.svg')} />
                                        </button>
                                    </div>
                                </div>: null
                            }

                            <div className="esmbh-right">
                                <div className="esmbh-search">
                                    <input type="search" placeholder="Type to Search"  onKeyDown = {(e) => this.handleOnKeyDown(e)} onChange = {(e) => this.handleChange(e)} value = {this.state.search} />
                                    <img className="search-image" src={require('../../assets/searchicon.svg')} />
                                    <span className="closeSearch" onClick = {() => this.handleCancelSearch()}><img src={require('../../assets/clearSearch.svg')} /></span>
                                </div>
                            </div>
                        </div>
                        <div className="esmb-table">
                            <div className="esmbt-manage">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th className="fix-action-btn"></th>
                                            {
                                                this.state.showRadioButtons ?
                                                <th><label>Please select a department</label></th>:
                                                <th><label>Result</label></th>

                                            }
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.fieldNames != null ? this.state.fieldNames.length != 0 ? this.state.fieldNames.map((data, key1) => (
                                                <tr key = {key1} className = {data == this.state.copiedField && this.state.copiedField != "" ? "copied": ""}>
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            {
                                                                this.state.showRadioButtons ? 
                                                                    <li className="til-inner til-copy" >
                                                                        <label className="gen-radio-btn">
                                                                            <input type="radio" name="PI" value = {data} onChange={(e) => this.handleRadioChange(e)} />
                                                                        </label>
                                                                        {/* <span className="generic-tooltip">Copy Item</span> */}
                                                                    </li>:
                                                                    <li className="til-inner til-copy" onClick = {() => this.handleCopy(data)}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 29.884 35.5">
                                                                            <path id="copy_2_" fill="#a4b9dd" d="M18.79 35.5H5.547A5.553 5.553 0 0 1 0 29.953v-18.79a5.553 5.553 0 0 1 5.547-5.547H18.79a5.553 5.553 0 0 1 5.547 5.547v18.79A5.553 5.553 0 0 1 18.79 35.5zM5.547 8.39a2.777 2.777 0 0 0-2.773 2.773v18.79a2.777 2.777 0 0 0 2.773 2.773H18.79a2.777 2.777 0 0 0 2.773-2.773v-18.79A2.777 2.777 0 0 0 18.79 8.39zm24.337 18.1V5.547A5.553 5.553 0 0 0 24.337 0H8.944a1.387 1.387 0 0 0 0 2.773h15.393a2.777 2.777 0 0 1 2.773 2.774v20.939a1.387 1.387 0 0 0 2.773 0zm0 0" data-name="copy (2)"></path>
                                                                        </svg>
                                                                        <span className="generic-tooltip">Copy Item</span>
                                                                    </li>
                                                            }
                                                           
                                                        </ul>
                                                    </td>
                                                    <td><label>{data}</label></td>
                                                </tr>
                                            )): null : <tr className="tableNoData"><td colSpan="100%">{this.state.noDataMessage}</td></tr>
                                        }
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            {/* <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={(e) => this.getAnyPage(e)} onChange={(e) => this.getAnyPage(e)} value={this.state.jumpPage} /> */}
                                            <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.selectedItems}</span>
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination {...this.state} {...this.props} page={this.page}
                                                prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="esm-footer">
                        <button type="button" onClick={(e) => this.handleDone(e)}>{this.state.actiobBtn}</button>
                    </div>
                </div>
                {this.state.poErrorMsg ? <PoError errorMassage={this.state.errorMassage} closeErrorRequest={(e) => this.closeErrorRequest(e)} /> : null}
            </div>
        )
    }
}

export default ExcelUploadSearch;