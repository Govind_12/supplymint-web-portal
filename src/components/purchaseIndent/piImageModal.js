import React from "react";
import axios, { post } from "axios";
import { CONFIG } from "../../config/index";
import FilterLoader from "../loaders/filterLoader";
import ToastLoader from "../loaders/toastLoader";
import RequestError from "../loaders/requestError";

class PiImageModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            removedImage: "",
            newUpload: false,
            isGetPath: false,
            imgPath: {},
            file: {},
            selectedImage: [],
            imageRowId: this.props.imageRowId,
            _isMounted: false
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (!prevState.isGetPath) {
            if (nextProps.purchaseIndent.piImageUrl.isSuccess) {
                if (nextProps.purchaseIndent.piImageUrl.data.resource != null) {
                    let imgPath = {};
                    let image = nextProps.purchaseIndent.piImageUrl.data.resource;
                    image.map(img => {
                        imgPath[img.fileName] = img.url;
                    })
                    return {
                        file: imgPath,
                        imgPath: imgPath,
                        isGetPath: true,
                    }
                }
            }
        }

    }

    componentWillMount() {
        this._isMounted = true
        setTimeout(() => {
            this.setState({
                imageRowId: this.props.imageRowId,
            })
        }, 10)
    }
    componentDidMount() {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    onClose() {
        let file = Object.values(this.state.file)
        let flag = false
        for (var i = 0; i < file.length; i++) {
            if (!typeof (file[i]) == "string") {
                flag = true
                break
            } else {
                flag = false
            }
        }
        this.setState({
            selectedImage: [],
            file: flag ? this.state.file : this.state.imgPath

        })
        this.props.closePiImageModal();

    }

    fileChange(e) {
        let file = { ...this.state.file };
        console.log('initial file', file)
        let values = Object.values(e.target.files)

        for (var i = 0; i < values.length; i++) {
            this.setState({
                newUpload : true,
            })
            if (Object.values(this.state.file).length < 5) {
                if (values.length <= 5) {
                    if (!Object.keys(this.state.file).includes(values[i].name)) {
                        if (values[i].size < 10485760) {
                            if (values[i].type == "image/jpeg" || values[i].type == "image/png" || values[i].type == "image/jpg") {

                                file[values[i].name] = values[i]
                            } else {
                                this.setState({
                                    toastMsg: "Allowed Image types png, jpg, jpeg",
                                    toastLoader: true
                                })

                                e.target.value = ""
                                setTimeout(() => {
                                    this.setState({
                                        toastLoader: false
                                    })
                                }, 2500)

                                break
                            }
                        } else {
                            this.setState({
                                toastMsg: "Image size must be of >10mb",
                                toastLoader: true
                            })
                            e.target.value = ""
                            setTimeout(() => {
                                this.setState({
                                    toastLoader: false
                                })
                            }, 2500)

                            break
                        }
                    } else {
                        this.setState({
                            toastMsg: "Same image can't be selected",
                            toastLoader: true
                        })
                        e.target.value = ""
                        setTimeout(() => {
                            this.setState({
                                toastLoader: false
                            })
                        }, 2500)
                        break
                    }
                } else {
                    this.setState({
                        toastMsg: "you can select only 5 image in total",
                        toastLoader: true
                    })
                    e.target.value = ""
                    setTimeout(() => {
                        this.setState({
                            toastLoader: false
                        })
                    }, 2500)
                    break
                }
            } else {
                this.setState({
                    toastMsg: "you can select only 5 image",
                    toastLoader: true
                })
                e.target.value = ""
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 2500)

                break

            }
            console.log('change file', file)
            this.setState({
                file: file
            })
            e.target.value = ""

        }
        this.textInput.current.focus();
    }

    deletedImage(image) {
        console.log('image', image)
        let selectedImage = this.state.selectedImage


        if (selectedImage.includes(image)) {

            var index = selectedImage.indexOf(image);
            if (index > -1) {
                selectedImage.splice(index, 1);
                this.setState({
                    selectedImage: selectedImage
                })
            }
        } else {
            selectedImage.push(image)
            this.setState({
                selectedImage: selectedImage
            })
        }

    }
    removeImage() {
        let file = this.state.file
        let keys = []
        let selectedImage = this.state.selectedImage
        for (var key in file) {
            if (selectedImage.includes(file[key])) {
                keys.push(key)
            }
        }
        keys.forEach(e => delete file[e]);

        this.setState({
            file: file,
            selectedImage: [],
            removedImage: keys.join("|")

        })
        let data = {
            file: file,
            imageRowId: this.props.imageRowId,
            imagePath: this.props.imagePth == undefined ? "" : this.props.imagePth,
        }

        this.props.updateImage(data)
    }
    imageSave() {

        let headers = {
            'X-Auth-Token': sessionStorage.getItem('token'),
            'Content-Type': 'multipart/form-data'
        }

        if (Object.values(this.state.file).length == 0 ) {
            this.setState({
                toastMsg: "Select data",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 2500)
        } else if(!this.state.newUpload){
            this.props.closePiImageModal()
        }
        else {
            this.setState({
                loader: true

            })

            const fd = new FormData();
            let count = 0
            let files = this.state.file

            console.log('file upload', files)
            for (var i = 0; i < Object.keys(files).length; i++) {
                console.log(Object.keys(files)[i], files[Object.keys(files)[i]])
                if (typeof (files[Object.keys(files)[i]]) != "string") {
                    count++
                    fd.append("file", files[Object.keys(files)[i]]);                    
                }       
            }
            fd.append('filePath', this.props.imagePth == undefined ? "" : this.props.imagePth)
            fd.append('removeFileName', this.state.removedImage)
            let fileData = this.state.file

            if (count > 0) {
                axios.post(`${CONFIG.BASE_URL}/admin/custom/multiple/file/upload`, fd, { headers: headers })
                    .then(res => {
                        for (var i = 0; i < res.data.data.resource.length; i++) {
                            fileData[res.data.data.resource[i].fileName] = res.data.data.resource[i].url
                        }
                        let filePath = res.data.data.resource[0].filePath
                        let data = {
                            file: fileData,
                            imageRowId: this.props.imageRowId,
                            imagePath: filePath
                        }
                        console.log('imageData', data)
                        this.props.updateImage(data)
                        this.onClose();
                        if (this._isMounted) {
                            this.setState({
                                file: fileData,
                                loader: false

                            })
                        }
                    }).catch((error) => {
                        console.log(error)
                        this.setState({
                            alert: true,
                            loader: false
                        })
                    })

            } else {
                let data = {
                    imagePath: this.props.imagePth == undefined ? "" : this.props.imagePth,
                    file: this.state.file,
                    imageRowId: this.props.imageRowId
                }

                this.props.updateImage(data)
                this.setState({
                    loader: false
                })
                this.onClose()
            }
        }
    }
    keyFileDown(e) {
        if (e.key == 'Tab') {
            this.textInput.current.blur();
            document.getElementById("doneButton").focus()

        }
        if (e.key == 'Enter') {
            this.fileChange(e)
        }
    }
    keyCloseBtn(e) {
        window.setTimeout(function () {
            document.getElementById("closeButtonn").blur()

        }, 0)
        this.textInput.current.focus();
    }
    keyDoneBtn(e) {
        window.setTimeout(function () {
            document.getElementById("doneButton").blur()
            document.getElementById("closeButtonn").focus()
        }, 0)

    }
    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }
    render() {
        console.log('imgPath', this.props)
        // console.log('file' ,Object.values(this.props.file));
        return (
            <div className={this.props.imageModalAnimation ? "modal display_block" : "display_none"}>
                <div className={this.props.imageModalAnimation ? "backdrop modal-backdrop-new" : "display_none"}></div>
                <div className="modal-content modalShow pipo-image-upload-modal">
                    <div className="pium-head">
                        <div className="pium-left">
                            <h3>Upload Images</h3>
                            <p>You can upload multiple images of articles</p>
                        </div>
                        <div className="pium-right">
                            <button type="button" className="pium-close" id="closeButtonn" onClick={(e) => this.onClose(e)}>Close</button>
                            <button type="button" className="pium-done" id="doneButton" onClick={(e) => this.imageSave(e)}>Done</button>
                        </div>
                    </div>
                    <div className="pium-body">
                        <div className="piumb-upload-area">
                            <svg xmlns="http://www.w3.org/2000/svg" id="file" width="20.452" height="25.565" viewBox="0 0 20.452 25.565">
                                <path fill="#90a5c7" id="Path_1070" d="M82.322 41.565a2.133 2.133 0 0 0 2.13-2.13v-17.9a.426.426 0 0 0-.125-.3l-5.113-5.113a.426.426 0 0 0-.3-.125H66.13A2.133 2.133 0 0 0 64 18.13v21.3a2.133 2.133 0 0 0 2.13 2.13zm-2.983-24.11L83 21.113h-2.38a1.28 1.28 0 0 1-1.278-1.278zm-14.487 21.98V18.13a1.28 1.28 0 0 1 1.278-1.278h12.357v2.983a2.133 2.133 0 0 0 2.13 2.13H83.6v17.47a1.28 1.28 0 0 1-1.278 1.278H66.13a1.28 1.28 0 0 1-1.278-1.278z" class="cls-1" transform="translate(-64 -16)"/>
                                <path fill="#90a5c7" id="Path_1071" d="M219.835 376h-3.409a.426.426 0 1 0 0 .852h3.409a.426.426 0 1 0 0-.852z" class="cls-1" transform="translate(-207.904 -356.826)"/>
                                <path fill="#90a5c7" id="Path_1072" d="M168.426 157.965h2.13v4.687a.426.426 0 0 0 .426.426h3.409a.426.426 0 0 0 .426-.426v-4.687h2.13a.426.426 0 0 0 .327-.7l-4.261-5.113a.426.426 0 0 0-.655 0l-4.261 5.113a.426.426 0 0 0 .327.7zm4.261-4.873l3.351 4.021h-1.647a.426.426 0 0 0-.426.426v4.687h-2.557v-4.687a.426.426 0 0 0-.426-.426h-1.647z" class="cls-1" transform="translate(-162.461 -144.757)"/>
                            </svg>
                            <h4>Drag & Drop files here</h4>
                            <p>Files supported : PNG, JPG,JPEG</p>
                            <label className="piumbua-upload">
                                <input type="file" className="imageChooser" id="fileChoosen" multiple="multiple" size="5" ref={this.textInput} onChange={(e) => this.fileChange(e)} />
                                Choose File
                            </label>
                            <span className="piumbua-message">Maximum Size : Upto 10mb</span>
                        </div>
                        {/* <div className="col-md-12 modalContent m-top-30">
                            <div className="chooseFileBtn" id="chooseImage">
                                <div>
                                    <svg className="svgImgPurchase" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 51 51">
                                        <path fill="#6D6DC9" fillRule="nonzero" d="M49.715 12.521C48.64 6.742 44.101 2.323 38.431 1.246A67.167 67.167 0 0 0 25.503 0c-4.367 0-8.734.397-13.042 1.246C6.677 2.323 2.31 6.799 1.233 12.465a68.882 68.882 0 0 0 0 25.949C2.31 44.25 6.79 48.613 12.461 49.69c4.308.905 8.675 1.302 13.042 1.302a66.83 66.83 0 0 0 12.984-1.246c5.785-1.133 10.152-5.552 11.228-11.275a66.606 66.606 0 0 0 0-25.95zm-13.13 14.037H26.558v10.027a1.063 1.063 0 0 1-2.125 0V26.558H14.407a1.063 1.063 0 0 1 0-2.125h10.026V14.407a1.063 1.063 0 0 1 2.125 0v10.026h10.027a1.063 1.063 0 0 1 0 2.125z" />
                                    </svg>
                                    <input type="file" className="imageChooser" id="fileChoosen" multiple="multiple" size="5" ref={this.textInput} onChange={(e) => this.fileChange(e)} />
                                    <label className="chosenImage" >
                                        Choose Image
                                    </label>
                                </div>
                            </div>
                            <p className="noteMaxSize ">Note - Max Image size allowed :<span> 10mb</span></p>
                        </div> */}
                        <div className="piumb-uploaded-images">
                            <h3>Recently Uploaded</h3>
                            {/* <span className="imgesUploaded">{Object.values(this.state.file).length}</span> */}
                            {Object.values(this.state.file).length != 0 ? Object.values(this.state.file).map((data, key) => (
                                <div className="piumbup-inner" key={key} onClick={(e) => this.deletedImage(data)}>
                                    <div className={this.state.selectedImage.includes(data) ? "" : ""}>
                                        <img src={typeof (data) == "string" ? data : URL.createObjectURL(data)} />
                                    </div>
                                    <div className="piumbup-backdrop">
                                        <button type="button">
                                            <svg xmlns="http://www.w3.org/2000/svg" onClick={(e) => this.removeImage(e)} width="20" height="20" viewBox="0 0 17 19">
                                                <path fill="#ffffff" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            )) : null}
                        </div>
                        {/* <div className="col-md-12 m-top-40">
                            <div className="contentBottom">
                                <label>Recently Uploaded Images</label>
                                <div className="col-md-12 pad-0">
                                    {Object.values(this.state.file).length != 0 ? Object.values(this.state.file).map((data, key) => (
                                        <div className="imagesDiv" key={key} onClick={(e) => this.deletedImage(data)}>
                                            <div className={this.state.selectedImage.includes(data) ? "imagesSelected imageBorder" : "imagesSelected"}>
                                                <img src={typeof (data) == "string" ? data : URL.createObjectURL(data)} />
                                            </div>
                                        </div>
                                    )) : null}
                                    <div className="deleteBtn">{this.state.selectedImage.length != 0 ? <svg xmlns="http://www.w3.org/2000/svg" onClick={(e) => this.removeImage(e)} width="20" height="20" viewBox="0 0 17 19">
                                        <path fill="#FF0000" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                    </svg> : <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 17 19">
                                            <path fill="#e5e5e5" fillRule="nonzero" d="M17 3.53c-.02-.43-.35-.796-.794-.796h-3.023v-.333c0-.133.004-.265.002-.398A2.036 2.036 0 0 0 12.61.598a2.028 2.028 0 0 0-1.411-.596L11.054 0H6.103l-.258.002c-.326 0-.621.075-.915.21a1.82 1.82 0 0 0-.552.396 2.14 2.14 0 0 0-.442.715c-.103.254-.121.53-.121.8v.611H.795c-.415 0-.814.365-.794.793.02.43.349.792.793.792h.86v11.61c0 .281-.022.57.008.852.075.719.47 1.373 1.103 1.74.361.207.764.3 1.179.3h9.124c.427 0 .85-.103 1.215-.328a2.26 2.26 0 0 0 1.063-1.793c.006-.094 0-.187 0-.28V4.32h.233c.2 0 .4.006.6.004h.027c.414 0 .811-.365.793-.792zM5.4 1.91l-.014.024.006-.018.01-.024c.002-.018.004-.036.008-.053l-.006.05.04-.096-.026.032.028-.036.02-.046a.357.357 0 0 0-.016.042c.02-.026.042-.053.061-.08a.402.402 0 0 0-.035.027l.037-.03.03-.038-.026.036.08-.062-.042.016a.454.454 0 0 0 .046-.02l.036-.027-.032.026c.032-.014.063-.026.093-.04l-.05.006c.018-.002.036-.004.054-.008l.042-.018-.038.016.113-.016-.05.008h4.795c.212 0 .434-.02.649 0h.02c-.016-.004-.032-.006-.048-.008h-.006-.002c-.014-.002-.026-.008-.04-.01.046.006.092.016.14.018h.003c.006 0 .012.004.018.006l.054.008-.05-.006.095.04c-.01-.01-.021-.018-.031-.026l.035.028c.016.005.03.013.046.02a.358.358 0 0 0-.042-.017l.08.062a.402.402 0 0 0-.026-.036l.03.038.037.03-.035-.026.061.08-.016-.043c.006.016.012.032.02.046l.028.036-.026-.032.04.095-.006-.05a.497.497 0 0 0 .008.054l.018.042-.016-.036.02.153c-.004-.032-.012-.062-.02-.094.018.25-.002.506-.002.753v.024H5.404v-.024c0-.247-.02-.503-.002-.753-.006.03-.014.062-.018.094l.006-.038v-.004l.014-.103-.004.008zm.143 13.024c0 .333-.276.58-.6.595-.32.014-.596-.284-.596-.595l.002-.014c-.006-.322.002-.647.002-.97V6.924c0-.333.273-.58.595-.594.321-.014.595.283.595.594v8.01h.002zM9.095 7.91v7.025c0 .333-.271.58-.595.595-.321.014-.595-.284-.595-.595v-.014c-.006-.322 0-.647 0-.97V6.924c0-.333.274-.58.595-.594.322-.014.595.283.595.594v.014c.006.323 0 .648 0 .971zm3.553-.97c.005.253.002.508 0 .762V14.934c0 .333-.274.58-.596.595-.321.014-.595-.284-.595-.595v-8.01c0-.333.276-.58.6-.594a.512.512 0 0 1 .273.065c.008.004.018.008.026.016.004.002.006.006.01.01h.018c.012.02.021.02.033.03v.002a.616.616 0 0 1 .229.39l.006.042-.002.021-.002.02v.012z" />
                                        </svg>}</div>
                                </div>
                            </div>
                            {Object.values(this.state.file).length != 0 ? <div>
                                <p className="noteMaxSize displayInline m-top-20">Note:- Select image(s) to delete</p>
                            </div> : null}
                        </div> */}
                    </div>
                </div>
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        );
    }
}

export default PiImageModal;

