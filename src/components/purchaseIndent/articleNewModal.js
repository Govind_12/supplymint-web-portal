import React from "react";
import FilterLoader from "../loaders/filterLoader";
import RequestSuccess from "../loaders/requestSuccess";
import RequestError from "../loaders/requestError";
import ToastLoader from "../loaders/toastLoader";
class ArticleNewModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            piArticle: [],
            searchMrp: "",
            prev: "",
            current: "",
            next: "",
            maxPage: "",
            type: "",
            no: 1,
            toastMsg: "",
            toastLoader: false,
            selectedValue: [],
            checkedArticle: this.props.checkedArticle,
            selectedMrpForm: "",
            selectedMrpTo: "",
            selectedArticleName: "",
            searchBy: "startWith",
                focusedLi: "",
        }
    }
    componentDidMount() {
         if (this.props.isModalShow) {
        if (window.screen.width < 1200) {
            this.textInput.current.blur();
        } else {
            this.textInput.current.focus();
        }
         }
    }
    componentWillMount() {
        this.setState({
            checkedArticle: this.props.checkedArticle,
            searchMrp:this.props.isModalShow ? "" :this.props.articleSearch
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.piArticle.isSuccess) {
            if (nextProps.purchaseIndent.piArticle.data.resource != null) {
                this.setState({
                    piArticle: nextProps.purchaseIndent.piArticle.data.resource,
                    prev: nextProps.purchaseIndent.piArticle.data.prePage,
                    current: nextProps.purchaseIndent.piArticle.data.currPage,
                    next: nextProps.purchaseIndent.piArticle.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.piArticle.data.maxPage,
                })
            } else {
                this.setState({
                    piArticle: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }
            if (window.screen.width > 1200) {
                if (this.props.isModalShow) {

                    document.getElementById("searchArticle").focus()
                }
                else if (!this.props.isModalShow) {
                    if (nextProps.purchaseIndent.piArticle.data.resource != null) {
                        this.setState({
                            focusedLi: nextProps.purchaseIndent.piArticle.data.resource[0].ARTICLECODE
                        })
                        document.getElementById(nextProps.purchaseIndent.piArticle.data.resource[0].ARTICLECODE) != null ? document.getElementById(nextProps.purchaseIndent.piArticle.data.resource[0].ARTICLECODE).focus() : null
                    }


                } else {
                    if (!this.props.isModalShow) {
                        if (nextProps.purchaseIndent.piArticle.data.resource != null) {
                            this.setState({
                                focusedLi: nextProps.purchaseIndent.piArticle.data.resource[0].code
                            })
                            document.getElementById(nextProps.purchaseIndent.piArticle.data.resource[0].code) != null ? document.getElementById(nextProps.purchaseIndent.piArticle.data.resource[0].code).focus() : null

                        }
                    }
                }
            }
        }
    }

    onSearch(e) {
        if (this.state.searchMrp == "") {
            this.setState({
                toastMsg: "Enter text on search input ",
                toastLoader: true
            })
            const t = this;
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            })
        } else {
            if (this.state.sectionSelection == "") {
                this.setState({
                    type: 3,
                })
                let data = {
                    type: 3,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.piArticleRequest(data)
            }
            else {
                this.setState({
                    type: 2,
                })
                let data = {
                    type: 2,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    search: "",
                    searchBy: this.state.searchBy
                }
                this.props.piArticleRequest(data)
            }
        }
    }
    onsearchClear() {
        this.setState(
            {
                searchMrp: "",
                type: "",
                no: 1
            })
        if (this.state.type == 3) {
            let data = {
                type: "",
                no: 1,

                hl3Code: this.props.hl3Code,
                hl3Name: this.props.department,

                itemType: this.props.itemType,
                search: ""
            }
            this.props.getItemDetailsValueRequest(data)


        }

        document.getElementById("searchArticle").focus()
    }

    page(e) {
        if (e.target.id == "prev") {
            this.setState({
                prev: this.props.purchaseIndent.piArticle.data.prePage,
                current: this.props.purchaseIndent.piArticle.data.currPage,
                next: this.props.purchaseIndent.piArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.piArticle.data.currPage != 0) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.piArticle.data.currPage - 1,
                    hl3Code: this.props.hl3Code,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.piArticleRequest(data);
            }
            this.textInput.current.focus();
        } else if (e.target.id == "next") {
            this.setState({
                prev: this.props.purchaseIndent.piArticle.data.prePage,
                current: this.props.purchaseIndent.piArticle.data.currPage,
                next: this.props.purchaseIndent.piArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.piArticle.data.currPage != this.props.purchaseIndent.piArticle.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: this.props.purchaseIndent.piArticle.data.currPage + 1,
                    hl3Code: this.props.hl3Code,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.piArticleRequest(data)
            }
            this.textInput.current.focus();
        }
        else if (e.target.id == "first") {
            this.setState({
                prev: this.props.purchaseIndent.piArticle.data.prePage,
                current: this.props.purchaseIndent.piArticle.data.currPage,
                next: this.props.purchaseIndent.piArticle.data.currPage + 1,
                maxPage: this.props.purchaseIndent.piArticle.data.maxPage,
            })
            if (this.props.purchaseIndent.piArticle.data.currPage <= this.props.purchaseIndent.piArticle.data.maxPage) {
                let data = {
                    type: this.state.type,
                    no: 1,
                    hl3Code: this.props.hl3Code,
                    search: this.state.searchMrp,
                    searchBy: this.state.searchBy
                }
                this.props.piArticleRequest(data)
            }
            this.textInput.current.focus();
        }
    }

    handleChange(e) {
        if (e.target.id == "searchArticle") {
            this.setState({
                searchMrp: e.target.value
            });
        } else if (e.target.id == "searchByart") {
            this.setState({
                searchBy: e.target.value
            }, () => {
                if (this.state.searchMrp != "") {
                    let data = {
                        type: this.state.type,
                        no: 1,
                        hl3Code: this.props.hl3Code,
                        search: this.state.searchMrp,
                        searchBy: this.state.searchBy
                    }
                    this.props.piArticleRequest(data)
                }
            })
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }

    selectedData(e) {
        let c = this.state.piArticle;
        for (let i = 0; i < c.length; i++) {
            if (c[i].ARTICLECODE == e) {
                c[i].checked = true;
                this.setState({
                    checkedArticle: c[i].ARTICLECODE,
                    selectedMrpForm: c[i].MRPRANGEFROM,
                    selectedMrpTo: c[i].MRPRANGETO,
                    selectedArticleName: c[i].ARTICLENAME
                })
            } else {
                c[i].checked = false;
            }
        }
        this.setState({
            piArticle: c
        },()=>{
          
                    if (!this.props.isModalShow) {
                        this.onDone()
                    }
            
        }
        )
    }

    onDone(e) {

        if (this.state.checkedArticle == undefined || this.state.checkedArticle == "") {
            this.setState({
                toastMsg: "select data",
                toastLoader: true
            })
            const t = this
            setTimeout(function () {
                t.setState({
                    toastLoader: false
                })
            }, 1000)
        } else {

            if (this.props.checkedArticle != this.state.checkedArticle) {
                let data = {
                    articleCode: this.state.checkedArticle,
                    mrpStartRange: this.state.selectedMrpForm,
                    mrpEndRange: this.state.selectedMrpTo,
                    articleName: this.state.selectedArticleName,
                    rowId: this.props.articleRowId
                }
                this.props.updateArticleNew(data);


            }
            this.props.closeArticleModal(e)
        }
    }

    _handleKeyDown = (e) => {

        if (e.key === "Tab") {
            if (e.target.value == "" && (this.state.type == "" || this.state.type == 1)) {
                if (this.state.piArticle.length != 0) {
                    this.setState({

                        checkedArticle: this.state.piArticle[0].ARTICLECODE,


                    })
                    this.selectedData(this.state.piArticle[0].ARTICLECODE)
                    // let scode= this.state.piArticle[0].ARTICLECODE
                    window.setTimeout(() => {
                        document.getElementById("searchArticle").blur()
                        document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
                    }, 0)
                } else {

                    window.setTimeout(() => {
                        document.getElementById("searchArticle").blur()
                        document.getElementById("closeButton").focus()
                    }, 0)
                }
            } else if (e.target.value != "") {
                window.setTimeout(() => {
                    document.getElementById("searchArticle").blur()
                    document.getElementById("findButton").focus()
                }, 0)
            }
        }
        if (e.key == "ArrowDown") {
            if (this.state.piArticle.length != 0) {
                this.setState({

                    checkedArticle: this.state.piArticle[0].ARTICLECODE,


                })
                this.selectedData(this.state.piArticle[0].ARTICLECODE)
                // let scode= this.state.piArticle[0].ARTICLECODE
                window.setTimeout(() => {
                    document.getElementById("searchArticle").blur()
                    document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
                }, 0)
            } else {

                window.setTimeout(() => {
                    document.getElementById("searchArticle").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }

        }
        this.handleChange(e)
    }
    findKeyDown(e) {
        if (e.key == "Enter") {
            this.onSearch()
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("findButton").blur()
                document.getElementById("clearButton").focus()
            }, 0)
        }

    }
    focusDone(e) {
        if (e.key === "Tab") {

            window.setTimeout(() => {
                document.getElementById(this.state.piArticle[0].ARTICLECODE).blur()
                document.getElementById("doneButton").focus()
            }, 0)
        }
        if (e.key === "Enter") {
            this.onDone()
        }
    }
    doneKeyDown(e) {
        if (e.key == "Enter") {
            this.onDone(e);
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("doneButton").blur()
                document.getElementById("closeButton").focus()
            }, 0)
        }
    }
    closeKeyDown(e) {
        if (e.key == "Enter") {
            this.closeArticleModal();
        }
        if (e.key == "Tab") {
            window.setTimeout(() => {
                document.getElementById("closeButton").blur()
                document.getElementById("searchArticle").focus()
            }, 0)
        }

    }

    onClearDown(e) {
        if (e.key == "Enter") {
            this.onsearchClear();
        }
        if (e.key == "Tab") {
            if (this.state.piArticle.length != 0) {
                this.setState({

                    checkedArticle: this.state.piArticle[0].ARTICLECODE,
                })
                this.selectedData(this.state.piArticle[0].ARTICLECODE)
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
                }, 0)
            } else {
                window.setTimeout(() => {
                    document.getElementById("clearButton").blur()
                    document.getElementById("closeButton").focus()
                }, 0)
            }
        }
    }

  selectLi(e, code) {
        let piArticle = this.state.piArticle
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < piArticle.length; i++) {
                if (piArticle[i].ARTICLECODE == code) {
                    index = i
                }
            }
            if (index < piArticle.length - 1 || index == 0) {
                document.getElementById(piArticle[index + 1].ARTICLECODE) != null ? document.getElementById(piArticle[index + 1].ARTICLECODE).focus() : null

                this.setState({
                    focusedLi: piArticle[index + 1].ARTICLECODE
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < piArticle.length; i++) {
                if (piArticle[i].ARTICLECODE == code) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(piArticle[index - 1].ARTICLECODE) != null ? document.getElementById(piArticle[index - 1].ARTICLECODE).focus() : null

                this.setState({
                    focusedLi: piArticle[index - 1].ARTICLECODE
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(code)
        }
        if (e.which === 9) {
                
           {this.state.prev != 0 ?   document.getElementById("prev").focus():document.getElementById("next").focus()}
                  
        }
         if(e.key =="Escape"){
            this.props.closeArticleModal(e)
        }


    }
    paginationKey(e) {
        if(e.target.id=="prev"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                  
          {this.state.maxPage != 0 && this.state.next <= this.state.maxPage ?   document.getElementById("next").focus():
           this.state.piArticle.length!=0?
                document.getElementById(this.state.piArticle[0].ARTICLECODE).focus():null
                }
              
            }
        }
           if(e.target.id=="next"){
            if(e.key =="Enter"){
                this.page(e)
            }
            if(e.key == "Tab"){
                if(this.state.piArticle.length!=0){
                document.getElementById(this.state.piArticle[0].ARTICLECODE).focus()
                }
            }
        }
        if(e.key =="Escape"){
            this.props.closeArticleModal(e)
        }


    }
  
    render() {

        console.log(this.props.isModalShow)
         if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

        return (

           this.props.isModalShow ? <div className={this.props.articleNewModalAnimation ? "modal  display_block" : "display_none"} id="piselectdesc6Modal">
                <div className={this.props.articleNewModalAnimation ? "backdrop display_block" : "display_none"}></div>

                <div className={this.props.articleNewModalAnimation ? "modal_Indent display_block" : "display_none"}>
                    <div className={this.props.articleNewModalAnimation ? "modal-content modalpoColor modalShow" : "modalHide"}>

                        <div className="col-md-12 col-sm-12">
                            <div className="modal_Color">
                                <div className="modal-top">
                                    <ul className="list_style width_100 m-top-20">
                                        <li>
                                            <label className="select_name-content">SELECT ARTICLE</label>
                                        </li>
                                        <li>
                                            <p className="departmentpara-content">You can select article code from below records</p>
                                        </li>
                                    </ul>

                                    <ul className="list-inline width_100 m-top-10 chooseDataModal">

                                        <div className="col-md-9 col-sm-9 pad-0 modalDropBtn">
                                            <div className="mrpSelectCode">
                                                <li>
                                                    {sessionStorage.getItem("partnerEnterpriseCode") != "VMART" ? <select id="searchByart" name="searchByart" value={this.state.searchBy} onChange={(e) => this.handleChange(e)}>

                                                        <option value="contains">Contains</option>
                                                        <option value="startWith"> Start with</option>
                                                        <option value="endWith">End with</option>

                                                    </select> : null}
                                                    <input type="search" className="search-box" autoComplete="off" autoCorrect="off" ref={this.textInput} onKeyPress={this._handleKeyPress} onChange={e => this.handleChange(e)} value={this.state.searchMrp} id="searchArticle" placeholder="Type to search" />
                                                    <label className="m-lft-15">
                                                        <button type="button" className="findButton" id="findButton" onClick={(e) => this.onSearch(e)}>FIND
                                                    <svg className="search-img" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 18 18">
                                                                <path fill="#4a4a4a" fillRule="nonzero" d="M7.327 1.098A5.968 5.968 0 0 1 13.29 7.06a5.968 5.968 0 0 1-5.962 5.962 5.968 5.968 0 0 1-5.961-5.962A5.968 5.968 0 0 1 7.327 1.1zm0-1.098a7.06 7.06 0 1 0 0 14.119A7.06 7.06 0 0 0 7.327 0zm10.291 16.241l-4.176-4.176a7.96 7.96 0 0 1-1.109 1.11l4.176 4.175a.782.782 0 0 0 1.11 0 .784.784 0 0 0 0-1.109z" />
                                                            </svg>
                                                        </button>
                                                    </label>
                                                </li>
                                            </div>
                                        </div>
                                        <li className="float_right" >
                                            <label>
                                                {this.state.searchMrp != "" && (this.state.type == 1 || this.state.type == "") ?
                                                    <button type="button" className="clearbutton" id="clearButton" onClick={(e) => this.onsearchClear(e)}>CLEAR</button>
                                                    : <button type="button" className="clearbutton btnDisabled" >CLEAR</button>
                                                }
                                            </label>
                                        </li>

                                    </ul>
                                </div>

                                <div className="col-md-12 col-sm-12 pad-0 m-top-10">
                                    <div className="modal_table">
                                        <table className="table tableModal table-hover vendorMrpMain">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Department Code</th>
                                                    <th>Article Code</th>
                                                    <th>Article Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.piArticle == undefined || this.state.piArticle.length == 0 ? <tr className="modalTableNoData"><td colSpan="6"> NO DATA FOUND </td></tr> : this.state.piArticle.map((data, key) => (
                                                    <tr key={key} onClick={() => this.selectedData(`${data.ARTICLECODE}`)}>
                                                        <td>  <label className="select_modalRadio">
                                                            <input type="radio" name="vendorMrpCheck" id={data.ARTICLECODE} checked={this.state.checkedArticle == `${data.ARTICLECODE}`} onKeyDown={(e) => this.focusDone(e)}  readOnly/>
                                                            <span className="checkradio-select select_all positionCheckbox"></span>
                                                        </label>
                                                        </td>
                                                        <td>{data.DEPARTMENTCODE}</td>
                                                        <td>{data.ARTICLECODE}</td>
                                                        <td>{data.ARTICLENAME}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-bottom">
                                    <ul className="list-inline width_35 m-top-9 modal-select">
                                        <li className="float_left">
                                            <label className="m-r-15">
                                                <button type="button" className="doneButton" id="doneButton" onKeyDown={(e) => this.doneKeyDown(e)} onClick={(e) => this.onDone(e)}>Done</button>
                                            </label>
                                            <label>
                                                <button type="button" className="closeButton" id="closeButton" data-dismiss="modal" onClick={(e) => this.props.closeArticleModal(e)}>Close</button>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="pagerDiv pagerWidth65 m0 modalPagination">
                                        <ul className="list-inline pagination paginationWidth50">
                                            {this.state.current == 1 || this.state.current == 0 ? <li >
                                                <button className="PageFirstBtn pointerNone" type="button"  >
                                                    First
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="first" >
                                                        First
                  </button>
                                                </li>}
                                            {this.state.prev != 0 ? <li >
                                                <button className="PageFirstBtn" type="button" onClick={(e) => this.page(e)} id="prev">
                                                    Prev
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn" type="button" disabled>
                                                        Prev
                  </button>
                                                </li>}
                                            <li>
                                                <button className="PageFirstBtn" type="button">
                                                    <span>{this.state.current}/{this.state.maxPage}</span>
                                                </button>
                                            </li>
                                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <li >
                                                <button className="PageFirstBtn borderNone" type="button" onClick={(e) => this.page(e)} id="next">
                                                    Next
                  </button>
                                            </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li> : <li >
                                                    <button className="PageFirstBtn borderNone" type="button" disabled>
                                                        Next
                  </button>
                                                </li>}

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.loader ? <FilterLoader /> : null}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}

            </div>:
             <div className="dropdown-menu-city dropdown-menu-vendor pi-modal-po" id="pocolorModel">

                    <ul className="dropdown-menu-city-item">
                        {this.state.piArticle != undefined || this.state.piArticle.length != 0 ?
                            this.state.piArticle.map((data, key) => (
                                <li key={key} onClick={(e) => this.selectedData(`${data.ARTICLECODE}`)} id={data.ARTICLECODE} className={this.state.checkedArticle == `${data.ARTICLECODE}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data.ARTICLECODE)}>
                                    <span className="vendor-details">
                                        <span className="vd-name">{data.ARTICLENAME}</span>
                                        <span className="vd-loc">{data.ARTICLECODE}</span>
                                        <span className="vd-num"> {data.DEPARTMENTCODE}</span>
                                      
                                    </span>
                                </li>)) : <li><span>No Data Found</span></li>}

                    </ul>
                        <div className="gen-dropdown-pagination">
                        <div className="page-close">
                        <button className="btn-close" type="button" onClick={(e) => this.props.closeArticleModal(e)}  id="btn-close">Close</button>
                        </div>
                        <div className="page-next-prew-btn margin-180">
                            {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                            </svg>
                            </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg></button>}
                            <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                            {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                    <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                </svg>
                            </button>
                                : <button className="pnpb-next" type="button" disabled>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg>
                                </button> : <button className="pnpb-next" type="button" disabled>
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                        <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                    </svg></button>}
                        </div>
                    </div>
                </div>

        );
    }
}

export default ArticleNewModal;
