import React from "react";

class AgentModal extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            agentState: [],
            agentCode: '',
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,
            type: "",
            no: 1,
            focusedLi: "",
        }

    }

    // Vendor

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside)
    }

    handleClickOutside = (e) => {
        if(this.textInput && !this.textInput.current.contains(e.target)){
            this.props.onCloseAgent()
        }
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside)
    }

     componentWillReceiveProps(nextProps) {
        if (nextProps.purchaseIndent.getAgentName.isSuccess) {

            if (nextProps.purchaseIndent.getAgentName.data.resource != null) {

                this.setState({
                    agentState: nextProps.purchaseIndent.getAgentName.data.resource,
                    prev: nextProps.purchaseIndent.getAgentName.data.prePage,
                    current: nextProps.purchaseIndent.getAgentName.data.currPage,
                    next: nextProps.purchaseIndent.getAgentName.data.currPage + 1,
                    maxPage: nextProps.purchaseIndent.getAgentName.data.maxPage,

                }, () => {
                    nextProps.purchaseIndent.getAgentName.data.resource[0].agentCode != null ? document.getElementById(nextProps.purchaseIndent.getAgentName.data.resource[0].agentCode).focus() : null
                })
            } else {
                this.setState({
                    agentState: [],
                    prev: 0,
                    current: 0,
                    next: 0,
                    maxPage: 0,
                })
            }

        }
    }
    page(e) {
       
        if(this.props.purchaseIndent.getAgentName.data.resource != null){
            if (e.target.id == "prev") {
                this.setState({
                    prev: this.props.purchaseIndent.getAgentName.data.prePage,
                    current: this.props.purchaseIndent.getAgentName.data.currPage,
                    next: this.props.purchaseIndent.getAgentName.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.getAgentName.data.maxPage,
                })
                if (this.props.purchaseIndent.getAgentName.data.currPage != 0) {
                    let data = {
                        type: this.props.poUdf1 ? 3 : 1,
                        pageNo: this.props.purchaseIndent.getAgentName.data.currPage - 1,
                        search: this.props.poUdf1,
                        cityName: this.props.isCityChecked ? this.props.city : ""
                    }
                    this.props.getAgentNameRequest(data)
                }
            } else if (e.target.id == "next") {
                this.setState({
                    prev: this.props.purchaseIndent.getAgentName.data.prePage,
                    current: this.props.purchaseIndent.getAgentName.data.currPage,
                    next: this.props.purchaseIndent.getAgentName.data.currPage + 1,
                    maxPage: this.props.purchaseIndent.getAgentName.data.maxPage,
                })
                if (this.props.purchaseIndent.getAgentName.data.currPage != this.props.purchaseIndent.getAgentName.data.maxPage) {
                    let data = {
                        type: this.props.poUdf1 ? 3 : 1,
                        pageNo: this.props.purchaseIndent.getAgentName.data.currPage - 1,
                        search: this.props.poUdf1,
                        cityName: this.props.isCityChecked ? this.props.city : ""
                    }
                    this.props.getAgentNameRequest(data)
                }
            }
        }

    }

    closeAgent(e) {

        this.props.onCloseAgent()
        const t = this

        t.setState({
            agentState: [],
            prev: 0,
            current: 0,
            next: 0,
            maxPage: 0,

        })

        // document.getElementById("vendorBasedOn").value = ""

    }

    selectedData = (data) => {
        this.props.updateAgentState(data)
        this.props.onCloseAgent()
    }

    selectLi(e, data) {
        let agentState = this.state.agentState
        let index = 0

        if (e.which === 40) {
            for (let i = 0; i < agentState.length; i++) {
                if (agentState[i].agentCode == data.agentCode) {
                    index = i
                }
            }
            if (index < agentState.length - 1 || index == 0) {
                document.getElementById(agentState[index + 1].agentCode) != null ? document.getElementById(agentState[index + 1].agentCode).focus() : null

                this.setState({
                    focusedLi: agentState[index + 1].agentCode
                })

            }
        }
        if (e.which === 38) {
            for (let i = 0; i < agentState.length; i++) {
                if (agentState[i].agentCode == data.agentCode) {
                    index = i
                }
            }
            if (index > 0) {
                document.getElementById(agentState[index - 1].agentCode) != null ? document.getElementById(agentState[index - 1].agentCode).focus() : null

                this.setState({
                    focusedLi: agentState[index - 1].agentCode
                })

            }
        }
        if (e.which === 13) {
            this.selectedData(data)
        }
        if (e.which === 9) {

            { this.state.prev != 0 ? document.getElementById("prev").focus() : document.getElementById("next").focus() }

        }
        if (e.key == "Escape") {
            this.closeAgent(e)
        }

    }
    paginationKey(e) {
        if (e.target.id == "prev") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {

                {
                    this.state.maxPage != 0 && this.state.next <= this.state.maxPage ? document.getElementById("next").focus() :
                        this.state.agentState.length != 0 ?
                            document.getElementById(this.state.agentState[0].code).focus() : null
                }

            }
        }
        if (e.target.id == "next") {
            if (e.key == "Enter") {
                this.page(e)
            }
            if (e.key == "Tab") {
                if (this.state.agentState.length != 0) {
                    document.getElementById(this.state.agentState[0].code).focus()
                }
            }
        }
        if (e.key == "Escape") {
            this.closeAgent(e)
        }

    }

    render() {
        if (this.state.focusedLi != "") {
            document.getElementById(this.state.focusedLi) != null ? document.getElementById(this.state.focusedLi).focus() : null
        }

        return (
                <div ref={this.textInput}>
                        <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="pocolorModel">
                            <div className="dropdown-modal-header">
                                <span className="vd-name div-col-2">Code</span>
                                <span className="vd-loc div-col-2">{`${this.props.POUdf1Label}`}</span>
                            </div>

                            <ul className="dropdown-menu-city-item">
                                {this.state.agentState == undefined || this.state.agentState.length == 0 ?
                                    <li><span className="vendor-details"><span className="vd-name">No Data Found</span></span></li> :
                                    this.state.agentState.map((data, key) => (
                                        < li key={key} onClick={() => this.selectedData(data)} id={data.agentCode} className={this.state.agentCode == `${data.agentCode}` ? "selected" : ""} tabIndex="1" onKeyDown={(e) => this.selectLi(e, data)}>
                                            <span className="vendor-details">
                                                <span className="vd-name div-col-2">{data.agentCode}</span>
                                                <span className="vd-loc div-col-2">{data.agentName}</span>
                                            </span>
                                        </li>))}
                            </ul>
                            <div className="gen-dropdown-pagination">
                                <div className="page-close">
                                    <button className="btn-close" type="button" onClick={(e) => this.closeAgent(e)} id="btn-close">Close</button>
                                </div>
                                <div className="page-next-prew-btn">
                                    {this.state.prev != 0 ? <button className="pnpb-prev" type="button" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={(e) => this.page(e)} onKeyDown={(e) => this.paginationKey(e)} id="prev">
                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                        </svg>
                                    </button> : <button className="pnpb-prev" type="button" id="prev" disabled>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg></button>}
                                    <button className="pnpb-no" type="button">{this.state.current}/{this.state.maxPage}</button>
                                    {this.state.maxPage != 0 ? this.state.next <= this.state.maxPage ? <button className="pnpb-next" type="button" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onKeyDown={(e) => this.paginationKey(e)} onClick={(e) => this.page(e)} id="next">
                                            <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                        </svg>
                                    </button>
                                        : <button className="pnpb-next" type="button" disabled>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg>
                                        </button> : <button className="pnpb-next" type="button" disabled>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)" />
                                            </svg></button>}
                                </div>
                            </div>
                        </div>
                </div >
        );
    }
}

export default AgentModal;