import React from 'react';
import CatDescUdfTab from '../changeSetting/catDescUdfTab';
import EmailScheduler from '../changeSetting/emailScheduler';
import KeyValidation from '../changeSetting/keyValidation';
import PiPoFormate from '../changeSetting/pipoFormateTab';
import ProcurementTableHeader from '../changeSetting/procTableHeaders';
import ProcurementSetting from '../changeSetting/procurementSetting';
import FilterLoader from '../loaders/filterLoader';
import RequestError from '../loaders/requestError';
import RequestSuccess from '../loaders/requestSuccess';

export default class DigiProcSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCatDesc: false,
            shipTrackMenu: "procurement",
            tab: "procurement",
            startDate: "",
            endDate: "",
            bufferDays: "",
            dateSelect: false,
            startDateErr: false,
            endDateErr: false,
            bufferDaysErr: false,

            loader: false,
            alert: false,
            code: '',
            errorCode: '',
            errorMessage: '',

            requestKeys: "",
            currentTab: "",
            availableKeysData: {}
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.changeSetting.purchaseOrderDateRange.isSuccess) {
            this.setState({
                successMessage: nextProps.changeSetting.purchaseOrderDateRange.data.message,
                loader: false,
                success: true,
            })
            this.props.purchaseOrderDateRangeClear()
        } else if (nextProps.changeSetting.purchaseOrderDateRange.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.purchaseOrderDateRange.message.error == undefined ? undefined : nextProps.changeSetting.purchaseOrderDateRange.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.purchaseOrderDateRange.message.status,
                errorCode: nextProps.changeSetting.purchaseOrderDateRange.message.error == undefined ? undefined : nextProps.changeSetting.purchaseOrderDateRange.message.error.errorCode
            })
            this.props.purchaseOrderDateRangeClear()
        }

        if (nextProps.changeSetting.getAvailableKeys.isSuccess) {
            this.setState({
                loader: false,
                availableKeysData: nextProps.changeSetting.getAvailableKeys.data.resource,
                currentTab: this.state.requestKeys
            });
            if(nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE != undefined) {
                this.setState({
                    loader: false,
                    startDate: nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE.startDate != undefined ? nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE.startDate : "",
                    endDate: nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE.endDate != undefined ? nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE.endDate : "",
                    bufferDays: nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE.bufferDays != undefined ? nextProps.changeSetting.getAvailableKeys.data.resource.RESTRICTED_PRO_DATE.bufferDays : "",
                    currentTab: 'proDate'
                })
            }
            this.props.getAvailableKeysClear();
        } else if (nextProps.changeSetting.getAvailableKeys.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.getAvailableKeys.message.error == undefined ? undefined : nextProps.changeSetting.getAvailableKeys.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getAvailableKeys.message.status,
                errorCode: nextProps.changeSetting.getAvailableKeys.message.error == undefined ? undefined : nextProps.changeSetting.getAvailableKeys.message.error.errorCode
            });
            this.props.getAvailableKeysClear();
        }

        if (nextProps.changeSetting.updateAvailableKeys.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.changeSetting.updateAvailableKeys.data.message
            });
            this.props.updateAvailableKeysClear();
            this.props.getAvailableKeysRequest(this.state.currentTab);
        } else if (nextProps.changeSetting.updateAvailableKeys.isError) {
            this.setState({
                errorMessage: nextProps.changeSetting.updateAvailableKeys.message.error == undefined ? undefined : nextProps.changeSetting.updateAvailableKeys.message.error.errorMessage,
                loader: false,
                alert: true,
                code: nextProps.changeSetting.updateAvailableKeys.message.status,
                errorCode: nextProps.changeSetting.updateAvailableKeys.message.error == undefined ? undefined : nextProps.changeSetting.updateAvailableKeys.message.error.errorCode
            });
            this.props.updateAvailableKeysClear();
        }

        if(nextProps.changeSetting.getGeneralMapping.isSuccess){
            this.setState({
                loader: false,
            })
        } else if(nextProps.changeSetting.getGeneralMapping.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.changeSetting.getGeneralMapping.message.status,
                errorCode: nextProps.changeSetting.getGeneralMapping.message.error == undefined ? undefined : nextProps.changeSetting.getGeneralMapping.message.error.errorCode,
                errorMessage: nextProps.changeSetting.getGeneralMapping.message.error == undefined ? undefined : nextProps.changeSetting.getGeneralMapping.message.error.errorMessage
            })
        }
        if(nextProps.changeSetting.updateGeneralMapping.isSuccess){
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.changeSetting.updateGeneralMapping.data.message,
            })
            this.props.updateGeneralMappingClear();
        } else if(nextProps.changeSetting.updateGeneralMapping.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.changeSetting.updateGeneralMapping.message.status,
                errorCode: nextProps.changeSetting.updateGeneralMapping.message.error == undefined ? undefined : nextProps.changeSetting.updateGeneralMapping.message.error.errorCode,
                errorMessage: nextProps.changeSetting.updateGeneralMapping.message.error == undefined ? undefined : nextProps.changeSetting.updateGeneralMapping.message.error.errorMessage
            })
        }

        if(nextProps.replenishment.getHeaderConfig.isSuccess) {
            this.setState({
                loader: false,
                availableKeysData: nextProps.replenishment.getHeaderConfig.data.resource,
                currentTab: this.state.requestKeys
            });
            this.props.getHeaderConfigClear();
        } else if(nextProps.replenishment.getHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.getHeaderConfig.message.status,
                errorCode: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.getHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.getHeaderConfig.message.error.errorMessage
            });
            this.props.getHeaderConfigClear();
        }

        if(nextProps.replenishment.createHeaderConfig.isSuccess) {
            this.setState({
                loader: false,
                success: true,
                successMessage: nextProps.replenishment.createHeaderConfig.data.message,
            });
            this.props.createHeaderConfigClear();
            this.props.getHeaderConfigRequest({
                enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                attributeType: this.state.requestKeys === "piHeaders" ? "PI_TABLE_HEADER" : "PO_TABLE_HEADER",
                displayName: "TABLE HEADER"
            });
        } else if(nextProps.replenishment.createHeaderConfig.isError) {
            this.setState({
                loader: false,
                alert: true,
                code: nextProps.replenishment.createHeaderConfig.message.status,
                errorCode: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorCode,
                errorMessage: nextProps.replenishment.createHeaderConfig.message.error == undefined ? undefined : nextProps.replenishment.createHeaderConfig.message.error.errorMessage
            });
            this.props.createHeaderConfigClear();
        }

        if(nextProps.changeSetting.purchaseOrderDateRange.isLoading || nextProps.changeSetting.getAvailableKeys.isLoading || nextProps.changeSetting.getGeneralMapping.isLoading || nextProps.changeSetting.updateGeneralMapping.isLoading || nextProps.changeSetting.updateAvailableKeys.isLoading || nextProps.replenishment.getHeaderConfig.isLoading || nextProps.replenishment.createHeaderConfig.isLoading){
            this.setState({
                loader: true
            })
        }
    }

    onRequest(e) {
        e.preventDefault();
        this.setState({
            success: false
        }, () => {

        });
    }

    onError(e) {
        e.preventDefault();
        this.setState({
            alert: false
        });
        document.onkeydown = function (t) {
            if (t.which) {
                return true;
            }
        }
    }

    proStartDateErr() {
        if (this.state.dateSelect == "restriction") {
            this.setState({
                startDateErr: this.state.startDate == "" ? true : false,
            })
        }
    }
    proEndDateErr() {
        if (this.state.dateSelect == "restriction") {
            this.setState({
                endDateErr: this.state.endDate == "" ? true : false
            })
        }
    }
    
    bufferDaysErr = () => {
        this.setState({
            bufferDaysErr: this.state.bufferDays == "" ? true : false,
        })
    }

    dateHandle = (e) => {
        if (e.target.id == "startDate") {
            this.setState({
                startDate: e.target.value,
                endDate: "",
                startDateErr: false
            }, () => {
                this.proStartDateErr()
            })
        } else if (e.target.id == "endDate") {
            this.setState({
                endDate: e.target.value,
                endDateErr: false
            }, () => {
                this.proEndDateErr()
            })
        }
    }
    onClearProcurement() {
        this.setState({
            startDate: "",
            endDate: "",
            dateSelect: "",
            startDateErr: false,
            endDateErr: false
        })
    }
    handleRestriction = (e) => {
        if (e.target.id == "noRestrict") {
            this.setState({
                dateSelect: "noRestriction"
            })
        } else if (e.target.id == "restriction") {
            this.setState({
                dateSelect: "restriction"
            })
        }
    }
    bufferDaysHandler = (e) => {
        this.setState({
            bufferDays: e.target.value,
        })
        if(e.target.value < 15){
            this.setState({
                invalidBufferDays: true,
                bufferDaysErr: true,
            })
        } else if(e.target.value >= 15){
            this.setState({
                invalidBufferDays: false,
                bufferDaysErr: false,
            })
        }
    }
    saveProcurement() {
        this.proStartDateErr();
        this.proEndDateErr();
        this.bufferDaysErr();
        setTimeout(() => {
            const { startDateErr, endDateErr, bufferDaysErr } = this.state

            if (!startDateErr && !endDateErr && !bufferDaysErr && this.state.dateSelect == "restriction") {
                let data = {
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    bufferDays: this.state.bufferDays
                }
                this.props.updateAvailableKeysRequest(data)
                this.setState({
                    dateSelect: "",
                    startDate: "",
                    endDate: "",
                    startDateErr: false,
                    endDateErr: false,
                    bufferDaysErr: false,
                })
            } else if(!bufferDaysErr && this.state.dateSelect == "noRestriction"){
                let data = {
                    startDate: "",
                    endDate: "",
                    bufferDays: this.state.bufferDays
                }
                this.props.updateAvailableKeysRequest(data)
                this.setState({
                    dateSelect: "",
                    startDate: "",
                    endDate: "",
                    startDateErr: false,
                    endDateErr: false,
                    bufferDaysErr: false,
                })
            }
        }, 100);
    }
    changeTabCS(tab) {
        this.setState({
            tab,
        })
    }

    showCatDescTab = () =>{
        this.setState({
            isCatDesc: true,
        })
    }

    setHeaders = (pipo) => {
        this.setState({
            requestKeys: pipo
        });
        this.props.getHeaderConfigRequest({
            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
            attributeType: pipo === "piHeaders" ? "PI_TABLE_HEADER" : "PO_TABLE_HEADER",
            displayName: "TABLE HEADER"
        });
    }

    render() {
        console.log(this.props)
        return (
            <div className="container-fluid pad-0">
                <div className="container_div" id="home">
                    <div className="col-lg-12 pad-0 ">
                        <div className="subscription-tab procurement-setting-tab">
                            <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                                <li className="nav-item active">
                                    <a className="nav-link st-btn p-l-0" href="#generalSetting" role="tab" data-toggle="tab" onClick={() => this.changeTabCS('procurement')}>Procurement</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#catdescudftab" role="tab" data-toggle="tab" onClick={this.showCatDescTab}>Cat/Desc & UDF</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#keyvalidations" role="tab" data-toggle="tab" onClick={() => {this.setState({requestKeys: "pipo"}); this.props.getAvailableKeysRequest("pipo")}}>Key Validations</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#pipoformat" role="tab" data-toggle="tab" onClick={() => {this.setState({requestKeys: "pdf"}); this.props.getAvailableKeysRequest("pdf")}}>PI/PO Format</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#emailscheduler" role="tab" data-toggle="tab">Email Scheduler</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link st-btn" href="#tableheaders" role="tab" data-toggle="tab" onClick={() => {
                                        this.setState({requestKeys: "piHeaders"});
                                        this.props.getHeaderConfigRequest({
                                            enterpriseName: sessionStorage.getItem('partnerEnterpriseName'),
                                            attributeType: "PI_TABLE_HEADER",
                                            displayName: "TABLE HEADER"
                                        });
                                    }}>Table Headers</a>
                                </li>
                                {/* <li className="pst-button">
                                    {this.state.tab == "procurement" ? <div className="pstb-inner">
                                        <button className={this.state.dateSelect == "" ? "btnDisabled" : "gen-clear"} type="button" onClick={(e) => this.state.dateSelect == "" ? null : this.onClearProcurement(e)}>Clear</button>
                                        <button type="button" className={this.state.dateSelect == "" ? "btnDisabled" : "pst-save"} onClick={(e) => this.state.dateSelect == "" ? null : this.saveProcurement(e)} >Save</button> 
                                    </div>: null}
                                </li> */}
                            </ul>
                        </div>
                            {/* <div className="gen-vendor-potal-design p-lr-47">
                                <div className="col-lg-6 pad-0">
                                    <div className="gvpd-left">
                                    </div>
                                </div>
                                <div className="col-lg-6 pad-0">
                                    {this.state.tab == "procurement" ? <div className="gvpd-right">
                                        <button className={this.state.dateSelect == "" ? "btnDisabled" : "gen-clear"} type="button" onClick={(e) => this.state.dateSelect == "" ? null : this.onClearProcurement(e)}>Clear</button>
                                        <button type="button" className={this.state.dateSelect == "" ? "btnDisabled" : "gen-save"} onClick={(e) => this.state.dateSelect == "" ? null : this.saveProcurement(e)} >Save</button> 
                                    </div>: null}
                                </div>
                            </div> */}
                        <div className="tab-content">
                            <div className="tab-pane m-top-10 fade in active" id="generalSetting" role="tabpanel">
                                <div className="gen-vendor-potal-design p-lr-47 borderNone">
                                    <div className="gvpd-left"></div>
                                    <div className="gvpd-right">
                                        <button type="button" className={this.state.dateSelect == "" ? "btnDisabled" : "gen-save"} onClick={(e) => this.state.dateSelect == "" ? null : this.saveProcurement(e)} >Save</button> 
                                        <button className={this.state.dateSelect == "" ? "btnDisabled" : "gen-clear"} type="button" onClick={(e) => this.state.dateSelect == "" ? null : this.onClearProcurement(e)}>Clear</button>
                                    </div>
                                </div>
                                <ProcurementSetting {...this.props} {...this.state} dateHandle={this.dateHandle} handleRestriction={this.handleRestriction} bufferDaysHandler={this.bufferDaysHandler}/>
                            </div>
                            <div className="tab-pane fade" id="catdescudftab" role="tabpanel">
                                {this.state.isCatDesc && <CatDescUdfTab {...this.state} {...this.props}/>}
                            </div>
                            <div className="tab-pane fade" id="keyvalidations" role="tabpanel">
                                <KeyValidation tab={this.state.currentTab} data={this.state.availableKeysData} save={this.props.updateAvailableKeysRequest} />
                            </div>
                            <div className="tab-pane fade" id="pipoformat" role="tabpanel">
                                <PiPoFormate tab={this.state.currentTab} data={this.state.availableKeysData} save={this.props.updateAvailableKeysRequest} />
                            </div>
                            <div className="tab-pane fade" id="emailscheduler" role="tabpanel">
                                <EmailScheduler />
                            </div>
                            <div className="tab-pane fade" id="tableheaders" role="tabpanel">
                                <ProcurementTableHeader tab={this.state.currentTab} setHeaders={this.setHeaders} data={this.state.availableKeysData} save={this.props.createHeaderConfigRequest}  />
                            </div>
                        </div>
                    </div>
                    {/* <div className="replenishment_container pad-0 pipo-con">
                        <div className="col-md-12 col-sm-12 col-xs-12 itemUdfSet pad-0 changeSettingContain">
                            <div className="switchTabs p-lr-47">
                                <ul className="list_style m0">

                                    <li className={this.state.tab == "asnSetting" ? "displayPointer m-rgt-25 activeTab" : "m-rgt-25 displayPointer"}>
                                        <label className="displayPointer">Procurement</label>
                                    </li>
                                </ul>
                            </div>
                            <div className="subMenu p-lr-47">
                                <ul className="list-inline">
                                    <li className={this.state.shipTrackMenu == "procurement" ? "fontBold activeTab" : ""} name="asnFormat" onClick={() => this.changeTabCS('procurement')}>Procurement</li>
                                </ul>
                            </div>
                        </div>
                    </div> */}
                </div>
                {this.state.loader && <FilterLoader />}
                {this.state.success ? <RequestSuccess successMessage={this.state.successMessage} closeRequest={(e) => this.onRequest(e)} /> : null}
                {this.state.alert ? <RequestError code={this.state.code} errorCode={this.state.errorCode} errorMessage={this.state.errorMessage} closeErrorRequest={(e) => this.onError(e)} /> : null}
            </div>
        )
    }
}