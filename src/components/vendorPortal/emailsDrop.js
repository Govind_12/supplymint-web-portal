import React from 'react';

export const EmailsDrop = (props) => {
    return (
        <div className="mention-person-in-comment-inner">
            <div className="mpic-list-container">
                <ul className="mpic-list">
                    {props.filteredEmails.length != 0 && props.filteredEmails.map((_, k) => (
                        // <span className="mpic-short-name">GS</span>
                        <li key={k} className={props.cursor === k ? 'active' : null} onClick={(_) => props.handleMsg(_)} tabIndex="1" data-eachemail={_.type} id="email" data-username={_.value} ><span className="mpic-full-name">{_.value}</span></li>
                    ))}
                </ul>
                <div className="mpic-help-tools">
                    <span className="mpic-ht-enter">
                        <span className="mpic-ht-enter-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.654" height="8.232" viewBox="0 0 16.654 8.232">
                                <path d="M2.7 4.9l2.01 2.01a.771.771 0 0 1-1.076 1.1l-3.4-3.369a.764.764 0 0 1 0-1.1L3.546.229a.781.781 0 1 1 1.1 1.1l-2.01 2.01h12.49V.795a.764.764 0 1 1 1.529 0v3.34a.764.764 0 0 1-.764.764z" data-name="Path 490" />
                            </svg>
                        </span>
                        to Enter</span>
                    <span className="mpic-ht-esc"><span> esc</span> to dismiss</span>
                </div>
            </div>
        </div>
    )
}