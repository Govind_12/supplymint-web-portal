import React, { Component } from 'react';

import Exchange from '../../../assets/exchange.svg';
import Rupee from '../../../assets/rupee.svg';
import Coupon from "../../../assets/coupon.svg";

export default class InvoiceDetails extends Component {
    constructor(props) {
        super(props);
        // window.location.reload(false);
        this.state = {
            plans:[],
            enterprisedId: sessionStorage.getItem("eid"),
            orgId: sessionStorage.getItem("oid")
            
        }
        if (window.performance) {
            if (performance.navigation.type == 1) {
                this.props.history.push("/subscription/pricingPlans")
            }
        }
    }
    applyCoupon = () => {
        let payload = {
            type: 2,
            planId: this.props.selectedPlanId(),
            noOfUsersPaid: Number(this.props.noOfUsers),
            couponCode: this.props.couponValue
        }
        this.props.getPaymentSummaryRequest(payload)
    }
    componentDidMount() {
        let payload = {
            enterpriseId: this.state.enterprisedId,
            organisationId: this.state.orgId
        }
        this.props.getAllPlansRequest(payload)
    } 
    static getDerivedStateFromProps(nextProps){
        if (nextProps.orders.getAllPlans.isSuccess) {
            return {
                plans: nextProps.orders.getAllPlans.data.resource == null ? [] : nextProps.orders.getAllPlans.data.resource,
            }
        }
    }
    render() {
        const {plans}=this.state
        const users = sessionStorage.getItem("billing-user")
        const totalPrice = plans.length != 0 ? parseInt(plans[0].totalPrice.replace(",", ""))*users : null
        //const percentageValue = plans.length != 0 ? totalPrice ? (totalPrice*parseInt(plans[0].serviceTax))/100 : null :null
        return (
            <div className="invoice-details bg-layers">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-6 col-xs-12 offset-lg-1">
                            <div className="invoice-details-head">
                                <h1>Invoice Details</h1>
                            </div>
                            <div className="invoice-details-form">
                                <form>
                                    <div className="row">
                                        <div className="col-lg-6 col-xs-12">
                                            <label className="name-detail" htmlFor="firstname">Company Name</label>
                                            <input type="text" id="companyname" name="companyName" data-validate="true" value={this.props.companyName} onChange={this.props.handleChange} placeholder="Company Name" />
                                            {this.props.companyNameErr ? (<span className="error">Enter Company Name</span>) : null}
                                        </div>
                                        <div className="col-lg-6 col-xs-12">
                                            <label className="name-detail" htmlFor="lastname">Full Name</label>
                                            <input type="text" id="lastname" name="fullName" data-validate="true" value={this.props.fullName} onChange={this.props.handleChange} placeholder="Full Name" />
                                            {this.props.fullNameErr ? (<span className="error">Enter Full Name</span>) : null}
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="email">Email</label>
                                            <input type="text" id="email" name="email" data-validate="true" value={this.props.email} onChange={this.props.handleChange} placeholder="Email" />
                                            {this.props.emailErr ? (<span className="error">Enter Email</span>) : null}
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="contact-number">Contact Number</label>
                                            <input className="contact-num" type="Number" data-validate="true" name="contactNumber" value={this.props.contactNumber} onChange={this.props.handleChange} id="contact-number" />
                                            <span className="cntry-code">+91</span>
                                            {this.props.contactNumberErr ? (<span className="error">Enter Contact Number</span>) : null}
                                        </div>
                                        <div className="col-lg-5 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="stateId">State</label>
                                            <select name="state" value={this.props.selectedState} data-validate="true" className="states" id="stateId" name="selectedState" onChange={this.props.findCities}>
                                                <option>Select State</option>
                                                {this.props.states.map((data) =>
                                                    <option value={data}>{data}</option>
                                                )}
                                            </select>
                                            {this.props.selectedStateErr ? (<span className="error">Select State</span>) : null}

                                        </div>
                                        <div className="col-lg-4 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="cityId">City</label>
                                            <select name="city" className="cities" id="cityId" data-validate="true" name="selectedCity" value={this.props.selectedCity} onChange={this.props.handleChange}>
                                                <option value="">Select City</option>
                                                {this.props.selectedState !== "" && (this.props.city !== undefined && this.props.city.length > 0) && this.props.city.map((_, k) => (
                                                            <option value={_} key={k}>{_}</option>
                                                        ))}
                                            </select>
                                            {this.props.selectedCityErr ? (<span className="error">Select City</span>) : null}
                                        </div>
                                        <div className="col-lg-3 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="postal-code">Postal Code</label>
                                            <input type="text" id="postal-code" name="postalCode" value={this.props.postalCode} onChange={this.props.handleChange} />
                                            {this.props.postalCodeErr ? (<span className="error">Enter Postal Code</span>) : null}
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="email">GST Number</label>
                                            <input type="text" id="gstNumber" name="gstNumber" value={this.props.gstNumber} onChange={this.props.handleChange} />
                                            {this.props.gstNumberErr ? (<span className="error">Enter GST Number</span>) : null}
                                        </div>
                                        <div className="col-lg-12 col-xs-12 m-top-30">
                                            <label className="name-detail" htmlFor="address-line-1">Billing Address</label>
                                            <input className="id-billing-detail" type="text" id="address-line-1" name="billingAddress" value={this.props.billingAddress} onChange={this.props.handleChange} />
                                            {this.props.billingAddressErr ? (<span className="error">Enter Billing Address</span>) : null}
                                        </div>
                                        {/* <div className="col-lg-12 col-xs-12">
                                            <label className="name-detail" htmlFor="address-line-2">Address line 2</label>
                                            <input type="text" id="address-line-2" />
                                        </div> */}
                                        <div className="col-lg-4 col-xs-8 m-top-30">
                                            <button type="button" onClick={this.props.proceedToPayment} className="submit-btn">Proceed to Payment</button>
                                        </div>
                                        <div className=" col-lg-8 col-lg-4 id-back-btn m-top-30">
                                            <a onClick={() => this.props.history.push('/subscription/pricingPlans')}>Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-5 col-xs-12 p-l-90 bg-fafafa">
                            <div className="col-lg-10 col-xs-12 pad-0">
                                <div className="summary-of-purchase">
                                    <h3>Summary of Purchase</h3>
                                    {/* <span className="switch-plan"><img src={Exchange} />Switch Plan</span> */}
                                </div>
                                <div className="ppp-box sop-box">
                                    <div className="pppb-top">
                                        <div className="rectangle-1">
                                            <span className="standard-plan">Exclusive plan</span>
                                            <span className="for-year">For 3 Year / <span>Pain Annually</span></span>
                                        </div>
                                        <div className="ppgst">
                                            <p>Price plan</p> <p>incl GST</p>
                                        </div>
                                        <div className="price">
                                            {/* <span><img src={Rupee} /></span><p>{this.props.getPlanSummary[0] != undefined && this.props.getPlanSummary[0].basePlanAmount}</p><p>/month</p> */}
                                        <span><img src={Rupee} /></span><p>{plans.length != 0 ? plans[0].basePrice : null}</p><p>/user/month</p>
                                        </div>
                                        <span className="sop-no-of-user">Number of User <span>{users}</span></span>
                                    </div>
                                </div>
                                <div className="subscription-details">
                                    <div className="sd-head">
                                        <p>Next Billing Date</p>
                                        <p className="f-right">{plans.length != 0 ? plans[0].billingMessage : null}</p>
                                    </div>
                                    {/* <div className="sd-time border-b">
                                         <p>{this.props.getPlanSummary[0] != undefined && this.props.getPlanSummary[0].subscriptionValidUpto}</p>
                                        <p>September 31, 2020</p>
                                       <p className="f-right">{this.props.getPlanSummary[0] != undefined && this.props.getPlanSummary[0].nextPaymentDate}</p>
                                        <p className="f-right">September 30, 2023</p>
                                    </div> */}
                                    <div className="sd-base-plan-details border-b">
                                        <div className="sd-base-plan">
                                            <p>Monthly Price</p>
                                            {/* <p className="f-right"><span>(x12 months)</span>  <img src={Rupee}></img> {this.props.getPlanSummary[0] != undefined && this.props.getPlanSummary[0].basePlanAmount}</p> */}
                                <p className="f-right"><span>(x12 months)</span>  <img src={Rupee}></img> {totalPrice}</p>
                                        </div>
                                        {/*<div className="sd-gst">
                                            <p>GST</p>
                                            { <p className="f-right"><span>(18%)</span><img src={Rupee}></img> {this.props.getPlanSummary[0] != undefined && this.props.getPlanSummary[0].serviceTax}</p> }
                                <p className="f-right"><span>({plans.length != 0 ? plans[0].serviceTax : null}%)</span><img src={Rupee}></img> {percentageValue}</p>
                                        </div>*/}
                                    </div>
                                    <div className="sd-grand-total border-b">
                                        <p>Grand Total</p>
                                        {/* <p className="f-right"><img src={Rupee}></img>{this.props.getPlanSummary[0] != undefined && this.props.getPlanSummary[0].grandTotal}</p> */}
                                <p className="f-right"><img src={Rupee}></img> {totalPrice}</p>
                                    </div>
                                </div>
                                {/* <div className="uspm-b-input">
                                    <input type="text" placeholder="DISC15" onChange={this.props.handleChange} /><button type="button">Apply</button>
                                    <span className="uspm-b-close">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.155" height="9.155" viewBox="0 0 9.155 9.155">
                                            <g id="prefix__not_inlcude" data-name="not inlcude" transform="translate(-3.2 -64.15)">
                                                <path fill="#2d3748" id="prefix__Path_137" d="M69.653 68.728l3.46-3.461a.654.654 0 0 0-.925-.925l-3.46 3.458-3.461-3.46a.654.654 0 0 0-.925.925l3.461 3.46-3.46 3.461a.654.654 0 0 0 .924.925l3.461-3.461 3.461 3.46a.654.654 0 0 0 .925-.924z" data-name="Path 137" transform="translate(-60.95)"/>
                                            </g>
                                        </svg>
                                    </span>
                                    <div className="uspm-coupon">
                                        <p>Coupon Applied</p>
                                        <span className="coupon-code">DISC15 <img src={Coupon} /></span> <span className="remove-coupon">Remove</span>
                                    </div>
                                </div>
                                <div className="uspm-apply-couponn">
                                    <img src={Coupon} />
                                    <button type="button" onClick={this.applyCoupon}>Apply Coupon</button>
                                </div> */}
                                <span className="description">Displayed prices are for yearly subscriptions, paid in full at the time of purchase. Plan prices includes GST.
                                The final price can be seen on the purchase page, before payment is completed.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}