import React, { Component } from 'react';

import Rupee from '../../../assets/rupee.svg';
import CurvedArrow from '../../../assets/curved-arrow.svg';
import Check from '../../../assets/check2.svg';
import Uncheck from '../../../assets/uncheck.svg';

export default class PricingPlans extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicks: 1,
            plans:[],
            enterprisedId: sessionStorage.getItem("eid"),
            orgId: sessionStorage.getItem("oid")
            
        }
    }
    IncrementItem = () => {
        this.setState({ clicks: this.state.clicks + 1 });
    }
    DecreaseItem = () => {
        this.setState({ clicks: this.state.clicks == 1 ? 1 : this.state.clicks - 1 });
    }
    componentDidMount() {
        let payload = {
            enterpriseId: this.state.enterprisedId,
            organisationId: this.state.orgId
        }
        this.props.getAllPlansRequest(payload)
    }      
    static getDerivedStateFromProps(nextProps){
        if (nextProps.orders.getAllPlans.isSuccess) {
            return {
                plans: nextProps.orders.getAllPlans.data.resource == null ? [] : nextProps.orders.getAllPlans.data.resource,
            }
        }
    }

    continueToPayment = () => {   
        // let payload = {
        //     type: 1,
        //     planId: this.props.selectedPlanId()                                                                                                                                                                                                                                                                                                                                                                                                                                 ,
        //     noOfUsersPaid: Number(this.props.noOfUsers),
        //     couponCode: ""
        // }
        // this.props.getPaymentSummaryRequest(payload)
        sessionStorage.setItem("billing-user",this.state.clicks )
        //sessionStorage.setItem("billing-plans",planValue)
         this.props.history.push('/subscription/invoiceDetails')
    }
    render() {
        const {plans}=this.state
        return (
            <div className="container pricing-plans-section m-top-30">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="pricing-plans">
                            <h1>Subscription Plan</h1>
                            <p>Supplymint DigiVend</p>
                        </div>
                    </div>
                    {/* <div className="col-lg-12 ">
                        <div className="our-plans">
                            <span className="op-item"><p>1 year plan</p></span>
                            <span className="op-item">
                                <li className={this.props.selectedPlan == "12" ? "mainToggle " : "addToggleSwitch mainToggle"}>
                                    <label className="switchToggle">
                                        <input type="checkbox" onClick={this.props.selectPlan} id="myDiv" />
                                        <span className="sliderToggle round">
                                        </span>
                                    </label>
                                    <p className="onActive">Active</p>
                                    <p className="onInActive">Inactive</p>
                                </li>
                            </span>
                            <span className="op-item"><p>3 year plan</p></span>
                            <span className="curve"><img src={CurvedArrow} /></span>
                            <span className="save-up-to"><p>Save upto 20%</p></span>
                        </div>
                    </div> */}
                    <div className="col-lg-4">
                        <div className="ppp-box">
                            <div className="pppb-top">
                                <div className="rectangle-1">
                                    <span className="standard-plan">Exclusive plan</span>
                                    <span className="for-year">For 3 Year / <span>Pain Annually</span></span>
                                </div>
                                <div className="ppgst">
                                    <p>Offer price</p> <p>incl GST</p>
                                </div>
                                <div className="price">
                                <span><img src={Rupee} /></span><p>{plans.length != 0 ? plans[0].basePrice : null}</p><p>/user/month</p>
                                </div>
                                <div className="ppgst">
                                <span>Billing starts from </span><p>{plans.length != 0 ? plans[0].billingMessage : null}</p>
                                </div>
                            </div>
                            <div className="pppb-bottom">
                                <p>Number of Users</p>
                                <div className="pppbb-input">
                                    <input type="text" placeholder="No of Users" name="noOfUsers" value={this.state.clicks} onChange={this.props.handleChange} />
                                    <div className="pppbbi-button">
                                        <button type="button" className="ippdc-min" onClick={this.DecreaseItem}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 20 20">
                                                <g fill="none" fill-rule="evenodd">
                                                    <rect className="scg-fill1" width="20" height="20" fill="#FFF" rx="4"/>
                                                    <rect className="scg-fill2" width="10" height="2" x="5" y="9" fill="#8a77fa" rx="0"/>
                                                </g>
                                            </svg>
                                        </button>
                                        <button type="button" className="ippdc-min" onClick={this.IncrementItem}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 10.234 10.234">
                                                <path id="prefix__add" fill="#8a77fa" d="M14.984 10.72H10.72v4.264H9.014V10.72H4.75V9.014h4.264V4.75h1.706v4.264h4.264z" transform="translate(-4.75 -4.75)"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 col-lg-offset-1 pad-0">
                        <div className="features">
                            {/* <div className="features-head">
                                <p>Switch to our 3 year plan & save upto <span><img src={Rupee}></img></span><span>9000</span></p>
                            </div> */}
                            <div className="plan-feature">
                                <h3>Plan Features</h3>
                                <li><img src={Check} /><p>Access to Unlimited Retailer</p></li>
                                <li><img src={Check} /><p>Manage Shipments</p></li>
                                <li><img src={Check} /><p>Manage Logistics</p></li>
                                <li><img src={Check} /><p>Get Visibility of your Orders, Shipments etc.</p></li>
                                <li><img src={Check} /><p>Dashboard and Reporting </p></li>
                                <li><img src={Check} /><p>Visibility of Payments and Outstanding</p></li>
                                <li><img src={Check} /><p>Role based access and many more</p></li>
                            </div>
                            <span className="description">Displayed prices are for yearly subscriptions, paid in full at the time of purchase. Plan prices includes GST.
                            The final price can be seen on the purchase page, before payment is completed.</span>
                        </div>
                    </div>
                    <div className="col-lg-12 text-right">
                        <div className="proceed-top-payment-btn">
                            <button type="button" onClick={this.continueToPayment}>Proceed to Payment</button>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

}