import React, { Component } from 'react';

import GridShow from '../../../assets/grid-show.svg';
import Group from '../../../assets/group.svg';
import ExchangeBlack from '../../../assets/exchange-black.svg';
import DoubleChevron from '../../../assets/double-chevron.svg';

export default class MultiEnterprisesAaGridView extends Component {
    render(){
        console.log("ina multi enterprises aa grid view")
        return(
            <div className="enterprises-account-pg grid-view-pg">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-xs-12">
                            <div className="ea-head gv-head">
                                <h3>List of Enterprises linked with this account</h3>
                                <p>Please choose the one to make as your default choice</p>
                            </div>
                            <div className="view-as gv-view-as">
                                <span>Grid View</span><span><img src={GridShow} /></span><span><img src={Group} /></span>
                            </div>
                        </div>
                        <div className="col-lg-12 col-xs-12">
                            <div className="ea-grid-view">
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box">
                                        <h2>VMART</h2>
                                        <p>Active</p>
                                        <a className="main-btn" href="#0">Selected as default</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box">
                                        <h2>Skechers</h2>
                                        <p>Active</p>
                                        <a className="main-btn" href="#0">Selected as default</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box exp-detail">
                                        <h2>RS Brother</h2>
                                        <p>Active</p>
                                        <span className="main-btn ed-btn bg-o">Expire Soon</span><span>30 December 2019</span>
                                        <a className="main-btn" href="#0">Selected as default</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box exp-detail">
                                        <h2>CMR</h2>
                                        <p>Active</p>
                                        <span className="main-btn ed-btn bg-r">Expire Soon</span><span>13 October 2019</span>
                                        <a className="main-btn" href="#0"><img src={ExchangeBlack} />Renew Subscription</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box">
                                        <h2>VMART</h2>
                                        <p>Active</p>
                                        <a className="main-btn" href="#0">Selected as default</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box">
                                        <h2>Skechers</h2>
                                        <p>Active</p>
                                        <a className="main-btn" href="#0">Selected as default</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box exp-detail">
                                        <h2>RS Brother</h2>
                                        <p>Active</p>
                                        <span className="main-btn ed-btn bg-o">Expire Soon</span><span>30 December 2019</span>
                                        <a className="main-btn" href="#0">Selected as default</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-3">
                                    <div className="ea-gv-box exp-detail">
                                        <h2>CMR</h2>
                                        <p>Active</p>
                                        <span className="main-btn ed-btn bg-r">Expire Soon</span><span>13 October 2019</span>
                                        <a className="main-btn" href="#0"><img src={ExchangeBlack} />Renew Subscription</a>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="continue-btn gv-continue-btn">
                                        <a className="main-btn" href="#0">Continue</a>
                                    </div>
                                </div>
                                <div className="prew-next-btn">
                                    <span className="prew"><img src={DoubleChevron} /></span>
                                    <span className="next"><img src={DoubleChevron} /></span>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        )
    }
}