import React, { Component } from 'react';

import Rupee from '../../../assets/rupee.svg';
import Uncheck from '../../../assets/uncheck.svg';
import Check from '../../../assets/check2.svg';
import Rupee2 from '../../../assets/rupee-white.svg';
import Revert from '../../../assets/revert.svg';
import Sad from '../../../assets/sad.svg';

export default class subscriptionRenew extends Component {
    render(){
        console.log("ina subscription renew")
        return(
            <div className="current-plan-pg s-renew-pg bg-layers">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 col-xs-12">
                            <div className=" sr-col-1">
                                <div className="col-lg-12 col-xs-12">
                                    <div className="cp-head sr-head">
                                        <h3>Your Current plan</h3>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-xs-12">
                                    <div className="cp-box sr-box ppp-box">
                                        <div className="plan-name">
                                            <span>Plan Name</span>
                                            <h3>Standard 1 year</h3>
                                        </div>
                                        <div className="price">
                                            <span><img src={Rupee} /></span><p>1249</p><p>/month</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-xs-12">
                                    <div className="features">
                                        <div className="plan-feature">
                                            <h3>Current Plan Features</h3>
                                            <li><img src={Check} /><p>Create unlimited users</p></li>
                                            <li><img src={Check} /><p>Role based Access </p></li>
                                            <li><img src={Check} /><p>Access to all upcoming features</p></li>
                                            <li><img src={Uncheck} /><p>Huge savings on selected plans</p></li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 col-xs-12">
                                <div className="cp-head sr-head">
                                    <h3>Selected plan</h3>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xs-12">
                                <div className="cp-box sr-box-2 ppp-box">
                                    <div className="plan-name">
                                        <img className="tick" src={Check} />
                                        <h3>Exclusive 3 year</h3>
                                    </div>
                                    <div className="price">
                                        <span><img src={Rupee2} /></span><p>999</p><p>/month</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xs-12">
                                <div className="features">
                                    <div className="plan-feature">
                                        <h3>Plan Features</h3>
                                        <li><img src={Check} /><p>Create unlimited users</p></li>
                                        <li><img src={Check} /><p>Role based Access </p></li>
                                        <li><img src={Check} /><p>Access to all upcoming features</p></li>
                                        <li><img src={Check} /><p>Huge savings on selected plans</p></li>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="back-btn">
                                    <a href="#0"><img src={Revert} />Back</a>
                                </div>
                            </div>
                            <span className="description">Displayed prices are for yearly subscriptions, paid in full at the time of purchase. Plan prices do not include GST.<br></br>
                            The final price can be seen on the purchase page, before payment is completed.</span>
                        </div>
                        <div className="col-lg-5 col-xs-12">
                            <div className="subscriber-details">
                                <div className="pack-info">
                                    <a href="#0">Plan Expired<img src={Sad} /></a>
                                </div>
                                <div className="pack-info">
                                    <p>Expire on</p>
                                    <span>29-December-2019</span>
                                </div>
                                <div className="pack-info f-right">
                                    <p>Subscription valid upto</p>
                                    <span>January 29, 2020</span>
                                </div>
                                <div className="subscription-status border-b">
                                    <h3>Your Subscription has expired</h3>
                                </div>
                                <div className="enterprice-name border-b">
                                    <span>Enterprice Name</span>
                                    <p>Skechers India</p>
                                </div>
                                <div className="invoice-to border-b">
                                    <div className="it-head">
                                        <h4>Invoice to</h4>
                                        <span className="three-dot f-right"></span>
                                    </div>
                                    <div className="it-info">
                                        <span>Full Name</span>
                                        <p>Govind Singh</p>
                                    </div>
                                    <div className="it-info">
                                        <span>Contact</span>
                                        <p>8506920717</p>
                                    </div>
                                    <div className="it-mail">
                                        <span>Email</span>
                                        <p>Govind@turningcloud.com</p>
                                    </div>
                                </div>
                                <div className="sd-base-plan-details border-b">
                                    <div className="sd-base-plan">
                                        <p>Base Plan</p>
                                        <p className="f-right"> <img src={Rupee} /> 11,925</p>
                                    </div>
                                    <div className="sd-gst">
                                        <p>GST</p>
                                        <p className="f-right"><span>(20%)</span> <img src={Rupee} /> 3,000</p>
                                    </div>
                                </div>
                                <div className="sd-grand-total border-b">
                                    <p>Grand Total</p>
                                    <p className="f-right"> <img src={Rupee} /> 15,000</p>
                                </div>
                                <div className="renew-btn">
                                    <a href="#0">Renew Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}