import React, { Component } from 'react';

import Failed from '../../../assets/failed2.svg';
import Rupee from '../../../assets/rupee.svg';

export default class transactionFailed extends Component {
    render(){
        console.log("ina transaction failed")
        return(
            <div className="container-fluid p-lr-0">
                <div className="transaction-successful-pg transaction-failed-pg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-xs-12">
                                <div className="successful-img failed-img">
                                    <img src={Failed} />
                                </div>
                                <div className="ts-head tf-head">
                                    <h2>Your last transaction has failed</h2>
                                </div>
                                <div className="ts-transaction-id tf-transaction-id">
                                    <p>Transaction Reference No # <span>0990028828383</span></p><a href="#0"></a>
                                </div>
                                <span className="tf-note">Please note down transaction reference no for the future reference</span>
                                <div className="ts-login-btn tf-retry-cancel">
                                    <a className="retry" href="/#/subscription/pricingPlans">Retry</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="note-for-the-charge">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <p>If your payment is deducted,  the amount Will be refunded within 7-8 working days. If you haven’t completed payment yet, you can retry payment to complete your order</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}