import React, { Component } from 'react';

import GridShow from '../../../assets/grid-show.svg';
import Group from '../../../assets/group.svg';
import Check from '../../../assets/check.svg';

export default class MultiEnterprisesAaListView extends Component {
    render(){
        console.log("ina multi enterprises aa list view")
        return(
            <div className="enterprises-account-pg">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="ea-head">
                                <h3>List of Enterprises linked with this account</h3>
                                <p>Please choose the one to make as your default choice</p>
                            </div>
                            <div className="view-as">
                                <span>List View</span><span><img src={GridShow} /></span><span><img src={Group} /></span>
                            </div>
                            <div className="ea-form">
                                <form>
                                    <div className="col-lg-10 col-lg-offset-2 col-xs-12">
                                        <div className="ea-form-head">
                                            <input type="text" className="search-enterprices-input" id="search-enterprices" placeholder="Search Enterprises"/>
                                            <button type="search" className="search-enterprices-btn" for="search-enterprices">Search</button>
                                        </div>
                                    </div>
                                    <div className="ea-box">
                                        <div className="row">
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="megashop" name="megashop" type="radio" value="megashop" />
                                                    <label for="megashop">Megashop <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="skechers" name="megashop" type="radio" value="skechers" />
                                                    <label for="skechers">Skechers <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="rs-brother" name="megashop" type="radio" value="rs-brother" />
                                                    <label for="rs-brother">RS Brother <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="cmr" name="megashop" type="radio" value="cmr" />
                                                    <label for="cmr">CMR <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="megashop" name="megashop" type="radio" value="megashop" />
                                                    <label for="megashop">Megashop <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="skechers" name="megashop" type="radio" value="skechers" />
                                                    <label for="skechers">Skechers <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="rs-brother" name="megashop" type="radio" value="rs-brother" />
                                                    <label for="rs-brother">RS Brother <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="cmr" name="megashop" type="radio" value="cmr" />
                                                    <label for="cmr">CMR <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="megashop" name="megashop" type="radio" value="megashop" />
                                                    <label for="megashop">Megashop <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="skechers" name="megashop" type="radio" value="skechers" />
                                                    <label for="skechers">Skechers <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="rs-brother" name="megashop" type="radio" value="rs-brother" />
                                                    <label for="rs-brother">RS Brother <img src={Check} /></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-xs-12">
                                                <div className="ea-select-account">
                                                    <input id="cmr" name="megashop" type="radio" value="cmr" />
                                                    <label for="cmr">CMR <img src={Check}/></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-lg-12">
                                        <div className="continue-btn">
                                            <a className="main-btn" href="#0">Continue</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}