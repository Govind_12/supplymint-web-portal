import React, { Component } from 'react';
import Failed from '../../../assets/failed2.svg';
import Rupee from '../../../assets/rupee.svg';
import Success2 from '../../../assets/success2.svg';

export default class TransactionStatus extends Component {
    constructor(props) {
        super(props)
        this.state ={
            varified_status:null,
            planActivatedDate:""
        }
    }
    componentDidMount() {
        var payload ={
            razorpay_payment_id:this.props.razorpay_payment_id,
            razorpaySubscriptionId:this.props.razorpay_subscription_id,
            razorpaySignature:this.props.razorpay_signature,
        }
        this.props.getPaymentStatusRequest(payload)
    }
    static getDerivedStateFromProps(nextProps){
        if (nextProps.orders.getPaymentStatus.isSuccess) {
            return {
                varified_status:nextProps.orders.getPaymentStatus.data.resource.verified == 1 ? true : false,
                planActivatedDate:nextProps.orders.getPaymentStatus.data.resource.planActivatedDate
            }
             
        }
        return {}
    }
    // componentWillUnmount(){
    //     this.props.history.push('/')
    // }
    
    render() {
        const params = window.location.href.split("?")[1];
        if(this.state.varified_status == true){
            return (
                <div className="transaction-successful-pg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-xs-12">
                                <div className="successful-img">
                                    <img src={Success2} />
                                </div>
                                <div className="ts-head">
                                    <h2>Transation Successful</h2>
                                    <p>Plan Activation Date</p>
                                    <span>{this.state.planActivatedDate}</span>
                                </div>
                                <div className="ts-transaction-id">
                                    <p>Transaction id # {this.props.razorpay_payment_id}<span></span></p>
                                </div>
                                <div className="ts-transaction-id">
                                    <p>Subscription id # {this.props.razorpay_subscription_id}<span></span></p>
                                </div>
                                <div className="ts-login-btn">
                                    <a href="/">Continue to login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }else{
            return(
                <div className="container-fluid p-lr-0">
                    <div className="transaction-successful-pg transaction-failed-pg">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12 col-xs-12">
                                    <div className="successful-img failed-img">
                                        <img src={Failed} />
                                    </div>
                                    <div className="ts-head tf-head">
                                        <h2>Your last transaction has failed</h2>
                                    </div>
                                    <div className="ts-transaction-id tf-transaction-id">
                                        <p>Transaction Reference No # <span> {this.props.razorpay_payment_id}</span></p><a href="#0"></a>
                                    </div>
                                    <span className="tf-note">Please note down transaction reference no for the future reference</span>
                                    <div className="ts-login-btn tf-retry-cancel">
                                        <a className="retry" href="/#/subscription/pricingPlans">Retry</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="note-for-the-charge">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <p>If your payment is deducted,  the amount Will be refunded within 7-8 working days. If you haven’t completed payment yet, you can retry payment to complete your order</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            )
        }
    }
}