import React, { Component } from 'react';

import Check from '../../../assets/check2.svg';
import Rupee from '../../../assets/rupee.svg';
import Uncheck from '../../../assets/uncheck.svg';
import Upgrade from '../../../assets/upgrade.svg';

export default class InvoiceEdit extends Component {
    render(){
        console.log("ina invoice edit")
        return(
            <div className="current-plan-pg invoice-edit-pg bg-layers">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 col-xs-12">
                            <div className="col-lg-12 col-xs-12">
                                <div className="cp-head">
                                    <h3>Your Current plan</h3>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xs-12">
                                <div className="cp-box ppp-box">
                                    <div className="plan-name">
                                        <span>Plan Name</span>
                                        <h3>Standard 1 year</h3>
                                    </div>
                                    <div className="price">
                                        <span><img src={Rupee} /></span><p>1249</p><p>/month</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xs-12">
                                <div className="features">
                                    <div className="plan-feature">
                                        <h3>Current Plan Features</h3>
                                        <li><img src={Check} /><p>Create unlimited users</p></li>
                                        <li><img src={Check} /><p>Role based Access </p></li>
                                        <li><img src={Check} /><p>Access to all upcoming features</p></li>
                                        <li><img src={Uncheck} /><p>Huge savings on selected plans</p></li>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 col-xs-12 p-r-60">
                                <div className="cp-upgrade-plan">
                                    <p>Switch to our 3 year plan & save upto <span><img src={Rupee}></img></span><span>9000</span></p>
                                    <a href="#0"><img src={Upgrade} />Upgrade Plan</a>
                                </div>
                                <span className="description">Displayed prices are for yearly subscriptions, paid in full at the time of purchase. Plan prices do not include GST.
                                The final price can be seen on the purchase page, before payment is completed.</span>
                            </div>
                        </div>
                        <div className="col-lg-5 col-xs-12">
                            <div className="invoice-details-head invoice-edit-form">
                                <h1>Invoice Details</h1>
                            </div>
                            <div className="invoice-details-form invoice-edit-form">
                                <form>
                                    <div className="row">
                                        <div className="col-lg-6 col-xs-12">
                                            <label className="name-detail" for="firstname">First Name</label>
                                            <input type="text" id="firstname" placeholder="First Name" />
                                        </div>
                                        <div className="col-lg-6 col-xs-12">
                                            <label className="name-detail" for="lastname">Last Name</label>
                                            <input type="text" id="lastname" placeholder="Last Name" />
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" for="email">Email</label>
                                            <input type="text" id="email" placeholder="Email" />
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" for="contact-number">Contact Number</label>
                                            <input className="contact-num" type="text" id="contact-number" />
                                            <span className="cntry-code">+91</span>
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" for="stateId">State</label>
                                            <select name="state" className="states" id="stateId" onchange='selct_district(this.value)'>
                                            </select>
                                        </div>
                                        <div className="col-lg-6 col-xs-12 m-top-30">
                                            <label className="name-detail" for="cityId">City</label>
                                            <select name="city" className="cities" id="cityId">
                                                <option value="">Select City</option>
                                            </select>
                                        </div>
                                        <div className="col-lg-4 col-xs-12 m-top-30">
                                            <label className="name-detail" for="postal-code">Postal Code</label>
                                            <input type="text" id="postal-code" />
                                        </div>
                                        <div className="col-lg-8 col-xs-12 m-top-30">
                                            <label className="name-detail" for="address-line-1">Address line 1</label>
                                            <input type="text" id="address-line-1" />
                                        </div>
                                        <div className="col-lg-12 col-xs-12 m-top-30">
                                            <label className="name-detail" for="address-line-2">Address line 2</label>
                                            <input type="text" id="address-line-2" />
                                        </div>
                                        <div className="col-lg-12 col-xs-12 m-top-30">
                                            <button type="submit" className="submit-btn save-changes-btn">Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}