import React from 'react';
import ColoumSetting from '../../replenishment/coloumSetting';
import Pagination from '../../pagination';
import AssignToRetailer from './assignModal';
import VendorFilter from '../../../components/vendorPortal/vendorFilter';

class Wishlist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDownloadDrop: false,
            exportToExcel: false,
            showBulkAction: false,
            filter: false,
            filterBar: false,
        }
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        });
    }

    closeFilter(e) {
        this.setState({
            filter: false,
            filterBar: false
        });
    }
    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }
    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }
    openBulkAction(e) {
        e.preventDefault();
        this.setState({
            showBulkAction: !this.state.showBulkAction
        });
    }

    render() {
        return(
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0 m-top-15">
                    <div className="manage-catalogue-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="mcd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                </div>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.showDownloadDrop ? (
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="pi-pdf-download" type="button">
                                                    <span className="pi-pdf-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 23.764 22.348">
                                                            <g id="prefix__files">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.4 20.252V2.095a.7.7 0 0 1 .7-.7h10.471V4.19a1.4 1.4 0 0 0 1.4 1.4h2.793v2.1h1.4V4.888a.7.7 0 0 0-.2-.5L13.765.2a.7.7 0 0 0-.5-.2H2.1A2.1 2.1 0 0 0 0 2.095v18.157a2.1 2.1 0 0 0 2.1 2.1h4.884v-1.4H2.1a.7.7 0 0 1-.7-.7z" data-name="Path 606" />
                                                                        <text font-size="8px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700;fill:#12203c" id="prefix__PDF" transform="translate(7.764 15.414)" >
                                                                            <tspan x="0" y="0">PDF</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>
                                        </ul>
                                    ) : (null)}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className="gvpd-filter-inner" onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.filter && <VendorFilter closeFilter={(e) => this.closeFilter(e)} />}
                                </div>
                                <div className="mcd-retailer-drop">
                                    <p>Retailer</p>
                                    <button type="text" className="">Vmart Pvt Ltd <img src={require('../../../assets/downArrowNew.svg')} /></button>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="mcd-right">
                                <button className="mcd-comment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="0 0 18.99 17.409">
                                        <path id="prefix__iconmonstr-speech-bubble-28" fill="#8b77fa" d="M1.584 7.609a4.505 4.505 0 0 0 1.332 3.12 6.817 6.817 0 0 1-.264 2.519 7.919 7.919 0 0 0 2.38-1.087 11.87 11.87 0 0 0 2.066.389 4.754 4.754 0 0 0 .044 1.6 11.638 11.638 0 0 1-1.844-.3 18.943 18.943 0 0 1-5.261 1.678 10.331 10.331 0 0 0 1.314-4.2A5.831 5.831 0 0 1 0 7.609C0 3.79 3.735 1 7.914 1c4.144 0 7.895 2.757 7.911 6.582a7.64 7.64 0 0 0-1.6-.248c-.181-2.643-2.938-4.751-6.312-4.751-3.49 0-6.33 2.255-6.33 5.026zm16.525 8.047a6.8 6.8 0 0 0 .861 2.753 12.466 12.466 0 0 1-3.444-1.1 7.4 7.4 0 0 1-1.751.217c-3.058 0-5.142-2.091-5.142-4.306 0-2.512 2.461-4.326 5.18-4.326 2.736 0 5.18 1.827 5.18 4.326a3.815 3.815 0 0 1-.884 2.436zm-5.985-2.391a.649.649 0 1 0-.649.649.649.649 0 0 0 .649-.649zm2.374 0a.649.649 0 1 0-.649.649.649.649 0 0 0 .651-.649zm2.374 0a.649.649 0 1 0-.649.649.649.649 0 0 0 .649-.649z" transform="translate(-.002 -1)"/>
                                    </svg>
                                    Comments
                                </button>
                                {/* <div className="bulk-action-dropdown">
                                    <button className="mcd-bulk-action" type="button" onClick={(e) => this.openBulkAction(e)}>Bulk Action <img src={require('../../../assets/downArrowNew.svg')} /></button>
                                    {this.state.showBulkAction &&
                                    <ul className="bad-inner">
                                        <li>
                                            <button type="button">Assign To Retailer</button>
                                        </li>
                                        <li>
                                            <button type="button">Delete</button>
                                        </li>
                                    </ul>}
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 pad-0 p-lr-47">
                    <div className="manage-catalogue-table" >
                        <div className="mct-manage-table" id="scrollDiv">
                            <ColoumSetting />
                            <table className="table mct-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={require('../../../assets/refresh-block.svg')} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        <th className="pl-20">
                                            <label>Item Name</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Material</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Size</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Available Qty</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>MRP</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Division</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Section</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Department</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder"  value={1} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">1045</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <AssignToRetailer /> */}
            </div>
        )
    }
}

export default Wishlist;