import React from 'react';
import { Link } from "react-router-dom";

class Items extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    previewItem = () => {
        this.props.history.push('/productCatalogue/itemPreview')
    }
    render() {
        return(
            <div className="container-fluid p-lr-0">
                <div className="col-md-12 pad-0">
                    <div className="item-catalogue-design p-lr-47 border-btm">
                        <div className="col-md-8 pad-0">
                            <div className="icd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 pad-0">
                            <div className="icd-right">
                                <Link to="/catalogue/retailer/products/wishlist"><button type="button" className="icd-wishlist">Go to Wishlist
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.847" height="10.705" viewBox="0 0 12.847 10.705">
                                        <path d="M.535 4.817h10.679L7.125.922a.535.535 0 1 1 .738-.775l4.67 4.448a1.07 1.07 0 0 1-.009 1.523l-4.661 4.439a.535.535 0 1 1-.738-.775l4.106-3.895H.535a.536.536 0 1 1 0-1.071z" data-name="arrow (3)"/>
                                    </svg>
                                </button></Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 pad-0">
                    <div className="catalogue-filters">
                        <div className="cat-filter-head">
                            <h3>FILTERS</h3>
                            <button type="button" className="cfh-clear-all">Clear all</button>
                        </div>
                        <div className="cat-filter-body">
                            <div className="cfb-inner-filter">
                                <div className="cfb-if-head">
                                    <h3>AGE GROUP</h3>
                                    <button type="button" className="cfbif-view-all">View all</button>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Adults-Men</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Adults-Women</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Kids-Boys</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Kids-Girls</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Adults-Unisex</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Kids-Unisex</span>
                                    </div>
                                </div>
                            </div>
                            <div className="cfb-inner-filter">
                                <div className="cfb-if-head">
                                    <h3>DEPARTMENT</h3>
                                    <button type="button" className="cfbif-view-all">View all</button>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Top</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Jeans</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Jegging</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Jacket</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Kurtas</span>
                                    </div>
                                </div>
                            </div>
                            <div className="cfb-inner-filter">
                                <div className="cfb-if-head">
                                    <h3>CATEGORY</h3>
                                    <button type="button" className="cfbif-view-all">View all</button>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Shirt</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Trouser</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">T-shirt</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Jeans</span>
                                    </div>
                                </div>
                            </div>
                            <div className="cfb-inner-filter">
                                <div className="cfb-if-head">
                                    <h3>VENDORS</h3>
                                    <button type="button" className="cfbif-view-all">View all</button>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Tanvi Creations</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Jagdish Enterprises</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">TD & Sons</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Samaresh Paul</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Deb Ranjan2 Das</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Ruchi Technoplast</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">Toy Zone Impex Pvt Ltd.</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="fil-cat-name">ASIAN AGENCIES</span>
                                    </div>
                                </div>
                            </div>
                            <div className="cfb-inner-filter">
                                <div className="cfb-if-head">
                                    <h3>PRICE</h3>
                                </div>
                                <div className="cfbif-range">
                                    <p>0 <span>- 1,00000</span></p>
                                    <div className="cfbif-range-input">
                                        <input type="range" className="prise-range" min="0" max="100000" /> 
                                    </div>
                                </div>
                            </div>
                            <div className="cfb-inner-filter">
                                <div className="cfb-if-head">
                                    <h3>COLOR</h3>
                                    <button type="button" className="cfbif-view-all">View all</button>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="cfbif-color black"></span>
                                        <span className="fil-color-name">Black</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="cfbif-color white"></span>
                                        <span className="fil-color-name">White</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="cfbif-color red"></span>
                                        <span className="fil-color-name">Red</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="cfbif-color green"></span>
                                        <span className="fil-color-name">Green</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="cfbif-color yellow"></span>
                                        <span className="fil-color-name">Yellow</span>
                                    </div>
                                </div>
                                <div className="cfbif-item">
                                    <div className="cfbif-item-row">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                        <span className="cfbif-color grey"></span>
                                        <span className="fil-color-name">Grey</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="catalogue-item-details">
                        {/* _____________________________Don't Remove This Code___________________________________ */}
{/*                         
                        <div className="cid-filter-apply-row">
                            <button type="button" className="cidfar-item">Puma
                                <img src={require('../../../assets/clearSearch.svg')} />
                            </button>
                            <button type="button" className="cidfar-item">Rs. 0 - Rs. 2999
                                <img src={require('../../../assets/clearSearch.svg')} />
                            </button>
                        </div> */}
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img1.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Peter England</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 899</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                        <span className="cidri-img-new-tag">NEW</span>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Lee Cooper</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description"> 2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img3.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                        <span className="cidri-img-few-tag">FEW</span>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Roadster</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img4.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                        <span className="cidri-img-new-tag">NEW</span>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Peter England</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img5.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img6.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Wrong</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img7.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Hue trap</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img8.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Black Berry</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img9.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img6.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Wrong</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img7.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Hue trap</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img8.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Black Berry</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img9.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img6.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Wrong</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img7.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Hue trap</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img8.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Black Berry</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img9.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev" onClick={this.previewItem}>Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Items;