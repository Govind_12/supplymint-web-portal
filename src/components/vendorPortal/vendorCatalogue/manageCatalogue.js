import React from 'react';
import ColoumSetting from '../../replenishment/coloumSetting';
import Pagination from '../../pagination';

class ManageCatalogue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDownloadDrop: false,
            exportToExcel: false,
            showBulkAction: false
        }
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
    }
    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }
    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }
    openBulkAction(e) {
        e.preventDefault();
        this.setState({
            showBulkAction: !this.state.showBulkAction
        });
    }

    render() {
        return(
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0 m-top-15">
                    <div className="manage-catalogue-design p-lr-47">
                        <div className="col-lg-6 pad-0">
                            <div className="mcd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                </div>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.showDownloadDrop ? (
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="pi-pdf-download" type="button">
                                                    <span className="pi-pdf-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 23.764 22.348">
                                                            <g id="prefix__files">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.4 20.252V2.095a.7.7 0 0 1 .7-.7h10.471V4.19a1.4 1.4 0 0 0 1.4 1.4h2.793v2.1h1.4V4.888a.7.7 0 0 0-.2-.5L13.765.2a.7.7 0 0 0-.5-.2H2.1A2.1 2.1 0 0 0 0 2.095v18.157a2.1 2.1 0 0 0 2.1 2.1h4.884v-1.4H2.1a.7.7 0 0 1-.7-.7z" data-name="Path 606" />
                                                                        <text font-size="8px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700;fill:#12203c" id="prefix__PDF" transform="translate(7.764 15.414)" >
                                                                            <tspan x="0" y="0">PDF</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    PO Pdf
                                                </button>
                                            </li>
                                        </ul>
                                    ) : (null)}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className="gvpd-filter-inner">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                </div>
                                <div className="nph-switch-btn">
                                    <label className="tg-switch" >
                                        <input type="checkbox" />
                                        <span className="tg-slider tg-round"></span>
                                    </label>
                                    <label className="nph-wbtext">Global</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="mcd-right">
                                <button className="mcd-pause">||</button>
                                <button className="mcd-send">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 17.386 17.044">
                                        <g id="prefix__send" transform="rotate(-30 12.193 3.267)">
                                            <path id="prefix__Path_650" fill="#8b77fa" d="M0 12.136l1.508-6.068L0 0l13.069 6.068zm0 0" data-name="Path 650"/>
                                            <path id="prefix__Path_651" fill="#735eeb" d="M0 6.068L13.069 0H1.508zm0 0" data-name="Path 651" transform="translate(0 6.068)"/>
                                        </g>
                                    </svg>
                                </button>
                                <div className="mdc-upload">
                                    <button className="mdcu-btn" type="button" onClick={(e) => this.openExportToExcel(e)}>
                                        <span className="mdcu-add">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.5" height="9.5" viewBox="0 0 9.5 9.5">
                                                <path id="prefix__add" fill="#fff" d="M14.25 10.292h-3.958v3.958H8.708v-3.958H4.75V8.708h3.958V4.75h1.583v3.958h3.959z" transform="translate(-4.75 -4.75)"/>
                                            </svg>
                                        </span>
                                        Upload Product
                                    </button>
                                    {this.state.exportToExcel &&
                                    <ul className="mcd-upload-dropdown">
                                        <li>
                                            <button className="mcdu-menual-entry" type="button">
                                                <span className="ccf-add">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="7" height="7" viewBox="0 0 9.5 9.5">
                                                        <path id="prefix__add" fill="#fff" d="M14.25 10.292h-3.958v3.958H8.708v-3.958H4.75V8.708h3.958V4.75h1.583v3.958h3.959z" transform="translate(-4.75 -4.75)"/>
                                                    </svg>
                                                </span>
                                                Manual Entry
                                            </button>
                                        </li>
                                        <li>
                                            <button className="mcdu-bulk-upload" type="button">
                                                <span className="ccf-add">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 8.627 12.652">
                                                        <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)"/>
                                                    </svg>
                                                </span>
                                                Bulk Upload
                                            </button>
                                        </li>
                                    </ul>}
                                </div>
                                <div className="bulk-action-dropdown">
                                    <button className="mcd-bulk-action" type="button" onClick={(e) => this.openBulkAction(e)}>Bulk Action <img src={require('../../../assets/downArrowNew.svg')} /></button>
                                    {this.state.showBulkAction &&
                                    <ul className="bad-inner">
                                        <li>
                                            <button type="button">Assign To Retailer</button>
                                        </li>
                                        <li>
                                            <button type="button">Delete</button>
                                        </li>
                                    </ul>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 pad-0 p-lr-47">
                    <div className="manage-catalogue-table" >
                        <div className="mct-manage-table" id="scrollDiv">
                            <ColoumSetting />
                            <table className="table mct-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={require('../../../assets/refresh-block.svg')} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        <th className="pl-20">
                                            <label>Item Name</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Material</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Size</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Available Qty</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>MRP</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Division</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Section</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Department</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">Yellow Round Neck TShi…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">Yellow Round Neck TShi…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">Yellow Round Neck TShi…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt.jpg')} /><label className="bold">White cotton Shirt…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                    <tr>
                                        <td className="fix-action-btn">
                                            <ul className="table-item-list">
                                                <li className="til-inner">
                                                    <label className="checkBoxLabel0">
                                                        <input type="checkBox" name="selectEach" />
                                                        <span className="checkmark1"></span>
                                                    </label>
                                                </li>
                                                <li className="til-inner til-edit-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                                        <path fill="#21314b" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" class="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)"/>
                                                        <path fill="#21314b" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" class="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)"/>
                                                        <path fill="#21314b" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" class="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)"/>
                                                        <path fill="#21314b" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" class="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)"/>
                                                    </svg>
                                                    <span className="generic-tooltip">Edit</span>
                                                </li>
                                                <li className="til-inner til-delete-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 15.691 18.83">
                                                        <path fill="#21314b" id="prefix__iconmonstr-trash-can-2" d="M7.492 14.907a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.138 0a.785.785 0 0 1-1.569 0V7.061a.785.785 0 0 1 1.569 0zm3.923-13.338v1.569H2V1.569h4.481C7.187 1.569 7.76.707 7.76 0h4.17c0 .707.573 1.569 1.28 1.569zm-2.353 3.138v12.554H4.354V4.707H2.785V18.83h14.122V4.707z" transform="translate(-2)" />
                                                    </svg>
                                                    <span className="generic-tooltip">Delete</span>
                                                </li>
                                            </ul>
                                        </td>
                                        <td><img src={require('../../../assets/shirt2.jpg')} /><label className="bold">Yellow Round Neck TShi…</label></td>
                                        <td><label>Cotton</label></td>
                                        <td><label>S</label></td>
                                        <td><label>1000</label></td>
                                        <td><label>1999</label></td>
                                        <td><label>Apparels</label></td>
                                        <td><label>Men</label></td>
                                        <td><label>Top</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder"  value={1} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">1045</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ManageCatalogue;