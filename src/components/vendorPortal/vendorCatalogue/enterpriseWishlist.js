import React from 'react';
import { Link } from "react-router-dom";

class EnterpriseWishlist extends React.Component {
    render() {
        return(
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0">
                    <div className="item-catalogue-design p-lr-47 border-btm">
                        <div className="col-md-8 pad-0">
                            <div className="icd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 pad-0">
                            <div className="icd-right">
                                <Link to="/catalogue/retailer/products/view"><button type="button" className="add-more-products-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="10.234" height="10.234" viewBox="0 0 10.234 10.234">
                                        <path id="prefix__add" fill="#8b77fa" d="M14.984 10.72H10.72v4.264H9.014V10.72H4.75V9.014h4.264V4.75h1.706v4.264h4.264z" transform="translate(-4.75 -4.75)"/>
                                    </svg>
                                    Add More Products
                                </button></Link>
                                <button type="button" className="notify-vendors-btn">NOTIFY VENDORS</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 pad-0 p-lr-47">
                    <div className="my-wishlist-all-products">
                        <span className="bold">All Products</span><span><img className="fa" src={require('../../../assets/slash-new.svg')} /></span><span>My Wishlist</span>
                        <p>24 Items</p>
                    </div>
                    <div className="mw-items">
                        <div className="col-lg-6 pad-0">
                            <div className="mwi-inner">
                                <div className="mwii-img">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img10.jpg')} />
                                </div>
                                <div className="mwii-details">
                                    <p>Wrong</p>
                                    <span className="cid-size">2PCS Men’S compression shi… </span>
                                    <div className="cid-item-prise">
                                        <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                        <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                        <span className="cid-off-per">10% Off</span>
                                    </div>
                                    <div className="mwii-size-quantity">
                                        <div className="mwii-size-btn">
                                            <p>Size</p>
                                            <button type="button">S 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                        <div className="mwii-quantity-btn">
                                            <p>Quantity</p>
                                            <button type="button">1 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                    </div>
                                    <button type="button" className="wl-remove-btn">Remove</button>
                                </div>
                            </div>
                            <div className="mwi-inner">
                                <div className="mwii-img">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img3.jpg')} />
                                </div>
                                <div className="mwii-details">
                                    <p>Roadster</p>
                                    <span className="cid-size">Men’s slim fit jeans by Roadster </span>
                                    <div className="cid-item-prise">
                                        <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                        <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                        <span className="cid-off-per">10% Off</span>
                                    </div>
                                    <div className="mwii-size-quantity">
                                        <div className="mwii-size-btn">
                                            <p>Size</p>
                                            <button type="button">S 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                        <div className="mwii-quantity-btn">
                                            <p>Quantity</p>
                                            <button type="button">1 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                    </div>
                                    <button type="button" className="wl-remove-btn">Remove</button>
                                </div>
                            </div>
                            <div className="mwi-inner">
                                <div className="mwii-img">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img5.jpg')} />
                                </div>
                                <div className="mwii-details">
                                    <p>Roadster</p>
                                    <span className="cid-size">Men’s slim fit jeans by Roadster </span>
                                    <div className="cid-item-prise">
                                        <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                        <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                        <span className="cid-off-per">10% Off</span>
                                    </div>
                                    <div className="mwii-size-quantity">
                                        <div className="mwii-size-btn">
                                            <p>Size</p>
                                            <button type="button">S 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                        <div className="mwii-quantity-btn">
                                            <p>Quantity</p>
                                            <button type="button">1 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                    </div>
                                    <button type="button" className="wl-remove-btn">Remove</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="mwi-inner">
                                <div className="mwii-img">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                </div>
                                <div className="mwii-details">
                                    <p>Levis</p>
                                    <span className="cid-size">Men’s Regular fit jogger by Levis </span>
                                    <div className="cid-item-prise">
                                        <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                        <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                        <span className="cid-off-per">10% Off</span>
                                    </div>
                                    <div className="mwii-size-quantity">
                                        <div className="mwii-size-btn">
                                            <p>Size</p>
                                            <button type="button">S 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                        <div className="mwii-quantity-btn">
                                            <p>Quantity</p>
                                            <button type="button">1 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                    </div>
                                    <button type="button" className="wl-remove-btn">Remove</button>
                                </div>
                            </div>
                            <div className="mwi-inner">
                                <div className="mwii-img">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img7.jpg')} />
                                </div>
                                <div className="mwii-details">
                                    <p>Peter England</p>
                                    <span className="cid-size">Men’s slim fit jeans by Roadster </span>
                                    <div className="cid-item-prise">
                                        <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                        <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                        <span className="cid-off-per">10% Off</span>
                                    </div>
                                    <div className="mwii-size-quantity">
                                        <div className="mwii-size-btn">
                                            <p>Size</p>
                                            <button type="button">S 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                        <div className="mwii-quantity-btn">
                                            <p>Quantity</p>
                                            <button type="button">1 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                    </div>
                                    <button type="button" className="wl-remove-btn">Remove</button>
                                </div>
                            </div>
                            <div className="mwi-inner">
                                <div className="mwii-img">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img5.jpg')} />
                                </div>
                                <div className="mwii-details">
                                    <p>Peter England</p>
                                    <span className="cid-size">2PCS Men’S compression shirts Set… </span>
                                    <div className="cid-item-prise">
                                        <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                        <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                        <span className="cid-off-per">10% Off</span>
                                    </div>
                                    <div className="mwii-size-quantity">
                                        <div className="mwii-size-btn">
                                            <p>Size</p>
                                            <button type="button">S 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                        <div className="mwii-quantity-btn">
                                            <p>Quantity</p>
                                            <button type="button">1 
                                                <img src={require('../../../assets/downArrowNew.svg')} />
                                            </button>
                                        </div>
                                    </div>
                                    <button type="button" className="wl-remove-btn">Remove</button>
                                </div>
                            </div>
                        </div>
                        <div className="burm-pegination">
                            <button type="button" className="burmp-prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="5.995" height="9.99" viewBox="0 0 5.995 9.99">
                                    <path id="prefix__Path_520" fill="none" stroke="#12203c" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M0 0l3.581 3.581L7.162 0" data-name="Path 520" transform="rotate(90 1.584 2.998)"/>
                                </svg>
                            </button>
                            <span className="burmp-numb">01/01</span>
                            <button type="button" className="burmp-next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="5.995" height="9.99" viewBox="0 0 5.995 9.99">
                                    <path id="prefix__Path_520" fill="none" stroke="#12203c" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M0 0l3.581 3.581L7.162 0" data-name="Path 520" transform="rotate(90 1.584 2.998)"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="ew-bottom">
                        <h3>YOU MAY ALSO LIKE</h3>
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img1.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Peter England</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 899</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img3.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                        <span className="cidri-img-new-tag">NEW</span>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Lee Cooper</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description"> 2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img11.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                        <span className="cidri-img-few-tag">FEW</span>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Roadster</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img8.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                        <span className="cidri-img-new-tag">NEW</span>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Peter England</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img5.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img12.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default EnterpriseWishlist;