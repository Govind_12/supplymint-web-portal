import React from 'react';

class WishlistEmpty extends React.Component {
    render() {
        return(
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0">
                    <div className="empty-wishlist">
                        <div className="empty-wishlist-inner">
                            <h3>You have Empty Wishlist</h3>
                            <p>Start adding your products</p>
                            <button type="button" className="ew-add-product">
                                <span className="ewap-add">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.5" height="9.5" viewBox="0 0 9.5 9.5">
                                        <path id="prefix__add" fill="#fff" d="M14.25 10.292h-3.958v3.958H8.708v-3.958H4.75V8.708h3.958V4.75h1.583v3.958h3.959z" transform="translate(-4.75 -4.75)"/>
                                    </svg>
                                </span>
                                Add Products
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default WishlistEmpty;