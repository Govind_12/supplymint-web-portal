import React from 'react';
import ImageView from './imageViewModal';
import { Link } from "react-router-dom";

class ItemPreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clicks: 0,
        }
    }
    IncrementItem = () => {
        this.setState({ clicks: this.state.clicks + 1 });
    }
    DecreaseItem = () => {
        this.setState({ clicks: this.state.clicks - 1 });
    }

    changeImage = () => {
        console.log('get Images');
        var expandImg = document.getElementById("expandedImg");
        var imgText = document.getElementById("imgtext");
        expandImg.src = imgs.src;
        imgText.innerHTML = imgs.alt;
        expandImg.parentElement.style.display = "block";
    }
    render() {
        return(
            <div className="container-fluid p-lr-0">
                <div className="col-md-12 pad-0">
                    <div className="item-catalogue-design p-lr-47 border-btm">
                        <div className="col-md-8 pad-0">
                            <div className="icd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 pad-0">
                            <div className="icd-right">
                                <Link to="/catalogue/retailer/products/wishlist"><button type="button" className="icd-wishlist">Go to Wishlist
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.847" height="10.705" viewBox="0 0 12.847 10.705">
                                        <path d="M.535 4.817h10.679L7.125.922a.535.535 0 1 1 .738-.775l4.67 4.448a1.07 1.07 0 0 1-.009 1.523l-4.661 4.439a.535.535 0 1 1-.738-.775l4.106-3.895H.535a.536.536 0 1 1 0-1.071z" data-name="arrow (3)"/>
                                    </svg>
                                </button></Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47">
                    <div className="item-preview-page">
                        <div className="col-lg-5 pad-0">
                            <div className="ipp-image">
                                <div className="ipp-img-top">
                                    <img src={require('../../../assets/vendorCatalogue/cat-img13.jpg')} onclick={this.changeImage} />
                                </div>
                                <div className="ipp-img-bottom">
                                    <button type="button" className="ippimg-prew">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="7.688" height="13.375" viewBox="0 0 7.688 13.375">
                                            <path id="prefix__Path_377" fill="none" stroke="#d6dce6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l5.274 5.274L16.547 9" data-name="Path 377" transform="rotate(90 9.93 5.344)"/>
                                        </svg>
                                    </button>
                                    {/* <div className="ippimg-item">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img13.jpg')} onclick={this.changeImage} />
                                    </div> */}
                                    <div className="ippimg-item">
                                        <img src={require('../../../assets/vendorCatalogue/cat-imgp1.jpg')} onclick={this.changeImage} />
                                    </div>
                                    <div className="ippimg-item">
                                        <img src={require('../../../assets/vendorCatalogue/cat-imgp2.jpg')} onclick={this.changeImage} />
                                    </div>
                                    <div className="ippimg-item">
                                        <img src={require('../../../assets/vendorCatalogue/cat-imgp3.jpg')} />
                                    </div>
                                    <div className="ippimg-item">
                                        <img src={require('../../../assets/vendorCatalogue/cat-imgp4.jpg')} />
                                    </div>
                                    <div className="ippimg-item">
                                        <img src={require('../../../assets/vendorCatalogue/cat-imgp5.jpg')} />
                                    </div>
                                    <button type="button" className="ippimg-next">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="7.688" height="13.375" viewBox="0 0 7.688 13.375">
                                            <path id="prefix__Path_377" fill="none" stroke="#d6dce6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l5.274 5.274L16.547 9" data-name="Path 377" transform="rotate(90 9.93 5.344)"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-7 pad-0">
                            <div className="ipp-details">
                                <p>Peter England</p>
                                <span className="cid-size">Grey striped slim fit jeans by wrong</span>
                                <div className="cid-item-prise">
                                    <span className="cid-dis-rate">Rs. 899</span>
                                    <span className="cid-actual-rate">Rs. 1099</span>
                                    <span className="cid-off-per">10% Off</span>
                                </div>
                                <span className="ippd-about">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
                                <div className="ippd-size">
                                    <button type="button" className="ippds-inner">XS</button>
                                    <button type="button" className="ippds-inner">S</button>
                                    <button type="button" className="ippds-inner">M</button>
                                    <button type="button" className="ippds-inner">L</button>
                                    <button type="button" className="ippds-inner">XL</button>
                                    <button type="button" className="ippds-inner">XXL</button>
                                </div>
                                <div className="ippd-color">
                                    <span className="ippdc-out selected-color"><span className="ippdc-inner gray"></span></span>
                                    <span className="ippdc-out"><span className="ippdc-inner red"></span></span>
                                    <span className="ippdc-out"><span className="ippdc-inner black"></span></span>
                                    <span className="ippdc-out"><span className="ippdc-inner green"></span></span>
                                    <span className="ippdc-out"><span className="ippdc-inner yellow"></span></span>
                                </div>
                                <div className="ippd-count">
                                    <div className="ippdc-inner">
                                        <button type="button" className="ippdc-min" onClick={this.DecreaseItem}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                                <g fill="none" fill-rule="evenodd">
                                                    <rect className="scg-fill1" width="20" height="20" fill="#FFF" rx="40"/>
                                                    <rect className="scg-fill2" width="10" height="2" x="5" y="9" fill="#d6dce6" rx="1"/>
                                                </g>
                                            </svg>
                                        </button>
                                        <span className="ippdc-number">{ this.state.clicks }</span>
                                        <button type="button" className="ippdc-min" onClick={this.IncrementItem}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 10.234 10.234">
                                                <path id="prefix__add" fill="#d6dce6" d="M14.984 10.72H10.72v4.264H9.014V10.72H4.75V9.014h4.264V4.75h1.706v4.264h4.264z" transform="translate(-4.75 -4.75)"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                                <button type="button" className="ippd-add-top-wishlist">ADD TO WISHLIST</button>
                            </div>
                        </div>
                        <div className="col-lg-12 pad-0 m-top-20">
                            <div className="subscription-tab product-details-tab">
                                <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                                    <li className="nav-item active" >
                                        <a className="nav-link st-btn p-l-0" href="#pendingrequest" role="tab" data-toggle="tab">Product Details</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link st-btn" href="#approvedrequest" role="tab" data-toggle="tab">Other details & Discounts <img src={require('../../../assets/discount.svg')} /></a>
                                    </li>
                                </ul>
                            </div>
                            <div className="tab-content">
                                <div className="tab-pane fade in active" id="pendingrequest" role="tabpanel">
                                    <div className="ppid-tab-head">
                                        <h3>Grey striped slim jeans by wrong</h3>
                                    </div>
                                    <div className="item-preview-details-item">
                                        <div className="ipdi-inner">
                                            <h3>Size</h3>
                                            <p>M</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Material</h3>
                                            <p>100% cotton</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Pattern</h3>
                                            <p>Solid</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Colour</h3>
                                            <p>Black</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Neck / Collar</h3>
                                            <p>Mandarion</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Brand</h3>
                                            <p>Zara</p>
                                        </div>
                                    </div>
                                    <div className="item-preview-details-item">
                                        <div className="ipdi-inner">
                                            <h3>Gender</h3>
                                            <p>Male</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>HSN</h3>
                                            <p>567849</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Colour</h3>
                                            <p>Black</p>
                                        </div>
                                        <div className="ipdi-inner">
                                            <h3>Neck / Collar</h3>
                                            <p>Mandarion</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="approvedrequest" role="tabpanel"></div>
                            </div>
                        </div>
                    </div>
                    <div className="ew-bottom">
                        <h3>SIMILAR PRODUCTS</h3>
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img16.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img14.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description"> 2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img15.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img3.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img7.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img11.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="cid-row">
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img6.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description"> 2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img8.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi… </span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="cidr-item">
                                <div className="cidr-item-inner">
                                    <div className="cidri-img">
                                        <img src={require('../../../assets/vendorCatalogue/cat-img2.jpg')} />
                                        <button type="button" className="cidri-prev">Preview</button>
                                        <div className="img-bottom">
                                            <button type="button" className="cidri-add-wishlist">Add to Wishlist</button>
                                        </div>
                                    </div>
                                    <div className="cidrii-details">
                                        <p>Nike</p>
                                        <span className="cid-size">Sizes <span className="cid-size-inner">S M L XL XXL XXXL</span></span>
                                        <span className="cid-description">2PCS Men’S compression shi…</span>
                                        <div className="cid-item-prise">
                                            <span className="cid-dis-rate"><span className="rupee-sign">&#8377; </span> 699</span>
                                            <span className="cid-actual-rate"><span className="rupee-sign">&#8377; </span> 900</span>
                                            <span className="cid-off-per">10% Off</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <ImageView /> */}
            </div>
        )
    }
}
export default ItemPreview;