import React from 'react';

class BulkUpload extends React.Component {
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content bulk-upload-modal">
                    <div className="bum-head">
                        <h3>Upload a file</h3>
                        <p>You can upload only in .xls format and file size could not be more than 10mb</p>
                        <button type="button" className="bum-close-btn" onClick={this.props.CancelBulkUpload}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="bum-body">
                        <div className="bum-file-name">
                            <p>FIle Name.xls</p>
                            <span>Remove</span>
                        </div>
                        <label className="file upload-file">
                            <input type="file" id="fileUpload" multiple="multiple" />
                            <span >Choose file</span>
                            {/* <img src={require('../../../assets/fileupload')} /> */}
                        </label>
                    </div>
                    <div className="bum-footer">
                        <button type="button" className="bumf-down-temp">Download Template</button>
                        <button type="button" className="bumf-upload">Upload
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.486" height="6.532" viewBox="0 0 13.486 6.532">
                                    <g id="prefix__arrow_1_" data-name="arrow (1)" transform="translate(0 -132)">
                                        <g id="prefix__Group_2565" data-name="Group 2565" transform="translate(0 132)">
                                            <path id="prefix__Path_645" fill="#fff" d="M13.332 134.893l-2.753-2.739a.527.527 0 0 0-.743.747l1.848 1.839H.527a.527.527 0 1 0 0 1.054h11.156l-1.848 1.839a.527.527 0 0 0 .743.747l2.753-2.739a.527.527 0 0 0 .001-.748z" data-name="Path 645" transform="translate(0 -132)"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
export default BulkUpload;