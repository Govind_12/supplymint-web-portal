import React from 'react';
import ColoumSetting from '../../replenishment/coloumSetting';
import Pagination from '../../pagination';
import AssignToRetailer from './assignModal';
import VendorFilter from '../../../components/vendorPortal/vendorFilter';

class LotDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDownloadDrop: false,
            exportToExcel: false,
            showBulkAction: false,
            confirmed: "confirm",
            confirmData:[],
            errorData:[],
            filter: false,
            filterBar: false,
            data: {
                type:1,
                pageNo:1,
                productType:"",
                search:"",
                searchBy:"contains",
                submissionId:'',
                filter:{
                    ageGroup:""
                },
                sortedBy:"",
                sortedIn:""
            }
        }
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
    }
    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        });
    }

    closeFilter(e) {
        this.setState({
            filter: false,
            filterBar: false
        });
    }

    componentDidMount(){
        let val = this.state.data;
        let data = {
            type:val.type,
            pageNo:val.pageNo,
            productType:val.productType,
            search:val.search,
            searchBy:val.searchBy,
            submissionId:this.props.location.query.submissionId,
            filter:{
                ageGroup:val.filter.ageGroup
            },
            sortedBy:val.sortedBy,
            sortedIn:val.sortedIn
         }
        this.props.productCatalogueSubmissionHistoryDetailsRequest(data)
    }

    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }
    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    openExportToExcel(e) {
        e.preventDefault();
        this.setState({
            exportToExcel: !this.state.exportToExcel
        });
    }
    openBulkAction(e) {
        e.preventDefault();
        this.setState({
            showBulkAction: !this.state.showBulkAction
        });
    }
    onConfirmed = (e) => {
        this.setState({
            confirmed: this.state.confirmed == "confirm" ? "notConfirm" : "confirm",
        });
    }

    pageHandler = (e) =>{
        let val = this.state.data;
        let data = {
            type:val.type,
            pageNo:val.pageNo,
            productType:val.productType,
            search:val.search,
            searchBy:val.searchBy,
            submissionId:this.props.location.query.submissionId,
            filter:{
                ageGroup:val.filter.ageGroup
            },
            sortedBy:val.sortedBy,
            sortedIn:val.sortedIn
         }        
        if(e.target.id == 'last'){
            data.pageNo = this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.maxPage;
            this.props.productCatalogueSubmissionHistoryDetailsRequest(data)
        }
        if(e.target.id == 'first'){
            if(data.pageNo!=1){
            data.pageNo = 1;
            this.props.productCatalogueSubmissionHistoryDetailsRequest(data)
            }
        }
        if(e.target.id == 'next'){
            data.pageNo = this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.currPage +1;
            this.props.productCatalogueSubmissionHistoryDetailsRequest(data)
        }
        if(e.target.id == 'prev'){
            if(data.pageNo>1){
            data.pageNo = this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.currPage -1;
            this.props.productCatalogueSubmissionHistoryDetailsRequest(data)
            }
        }
    }

    render() {
        let vendorItems = <tr></tr>
        if(this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.isSuccess){
            vendorItems = this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.resource.map(item=>{
                return (<tr>
                            <td className="fix-action-btn">
                                <ul className="table-item-list">
                                    <li className="til-inner">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </li>
                                </ul>
                            </td>
                            {item.imageUrlMain && <td><img src={item.imageUrlMain} style={{height:'30px', width:'30px', marginLeft:'10px', marginRight:'10px'}}/><label className="bold">{item.itemName}</label></td>}
                            {!item.imageUrlMain && <td><img src={item.imageUrlFront} style={{height:'30px', width:'30px', marginLeft:'10px', marginRight:'10px'}} /><label className="bold">{item.itemName}</label></td>}
                            <td><label>{item.material}</label></td>
                            <td><label>{item.size}</label></td>
                            <td><label>{item.availableQty}</label></td>
                            <td><label>{item.mrp}</label></td>
                            <td><label></label></td>
                            <td><label>{item.ageGroup}</label></td>
                            <td><label>{item.department}</label></td>
                        </tr>)
            })
        }
            
        return(            
            <div className="container-fluid p-lr-0">
                <div className="col-md-12 pad-0 m-top-15">
                    <div className="manage-catalogue-design p-lr-47">
                        <div className="col-md-8 pad-0">
                            <div className="mcd-left">
                                <div className="gvpd-search">
                                    <input type="search" placeholder="Type To Search" />
                                    <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                    {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                </div>
                                <div className="gvpd-download-drop">
                                    <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                            <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.showDownloadDrop ? (
                                        <ul className="pi-history-download">
                                            <li>
                                                <button className="export-excel" type="button">
                                                    <span className="pi-export-svg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                            <g id="prefix__files" transform="translate(0 2)">
                                                                <g id="prefix__Group_2456" data-name="Group 2456">
                                                                    <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                        <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                        <text font-size="7px" font-family="ProximaNova-Bold,Proxima Nova" font-weight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                            <tspan x="0" y="0">XLS</tspan>
                                                                        </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Export to Excel
                                                </button>
                                            </li>
                                        </ul>
                                    ) : (null)}
                                </div>
                                <div className="gvpd-filter">
                                    <button type="button" className="gvpd-filter-inner" onClick={(e) => this.openFilter(e)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                            <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                        </svg>
                                    </button>
                                    {this.state.filter && <VendorFilter closeFilter={(e) => this.closeFilter(e)} />}
                                </div>
                                <div className={this.state.confirmed === "confirm" ? "toggle-button-qc toggle-confirm" : "toggle-button-qc toggle-not-confirm"}>
                                    <button type="button" className="tbqc-confirm" onClick={this.onConfirmed} value={this.state.confirmed === "confirm" ? true : false}>UPLOADED PRODUCTS</button>
                                    <button type="button" className="tbqc-not-confirm" onClick={this.onConfirmed} value={this.state.confirmed === "confirm" ? false : true}>ERRORED PRODUCTS</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 pad-0">
                            <div className="mcd-right">
                                {/* <div className="bulk-action-dropdown">
                                    <button className="mcd-bulk-action" type="button" onClick={(e) => this.openBulkAction(e)}>Bulk Action <img src={require('../../../assets/downArrowNew.svg')} /></button>
                                    {this.state.showBulkAction &&
                                    <ul className="bad-inner">
                                        <li>
                                            <button type="button">Assign To Retailer</button>
                                        </li>
                                        <li>
                                            <button type="button">Delete</button>
                                        </li>
                                    </ul>}
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 p-lr-47">
                    <div className="history-lot-details-status">
                        <div className="col-md-9 pad-0">
                            <div className="hlds-left">
                                <div className="hldsl-inner">
                                    <p>Submission ID</p>
                                    <span>{this.props.location.query.submissionId}</span>
                                </div>
                                <div className="hldsl-inner">
                                    <p>Cataloging Status</p>
                                    <span className={"hldsl-complete"}>{this.props.location.query.catalogueStatus}</span>
                                    {/* "hldsl-complete"  this class will be use in case of complete */}
                                    {/* '"hldsl-partial-reject"'  this class will be use in case of partial reject */}
                                </div>
                                <div className="hldsl-inner">
                                    <p>Type</p>
                                    <span>{this.props.location.query.submissionType}</span>
                                </div>
                                <div className="hldsl-inner">
                                    <p>Submitted by</p>
                                    <span>{this.props.location.query.createdBy}</span>
                                </div>
                                <div className="hldsl-inner">
                                    <p>Uploaded on</p>
                                    <span>{this.props.location.query.creationTime}<span className="hldsl-time"></span></span>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3 pad-0">
                            
                        </div>
                    </div>
                </div>

                <div className="col-md-12 pad-0 p-lr-47 m-top-10">
                    <div className="manage-catalogue-table" >
                        <div className="mct-manage-table" id="scrollDiv">
                            <ColoumSetting />
                            <table className="table mct-main-table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn">
                                            <ul className="rab-refresh">
                                                <li className="rab-rinner">
                                                    <span><img src={require('../../../assets/refresh-block.svg')} /></span>
                                                </li>
                                            </ul>
                                        </th>
                                        <th className="pl-20">
                                            <label>Item Name</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Material</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Size</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Available Qty</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>MRP</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Division</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Section</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>
                                        <th>
                                            <label>Department</label>
                                            <img src={require('../../../assets/headerFilter.svg')} />
                                        </th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.confirmed == 'confirm' && vendorItems}                                    
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-12 pad-0" >
                            <div className="new-gen-pagination">
                                <div className="ngp-left">
                                    <div className="table-page-no">
                                        <span>Page :</span><input type="number" className="paginationBorder"  value={this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.currPage} />
                                        <span className="ngp-total-item">Total Items </span> <span className="bold">{this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.totalRecord}</span>
                                    </div>
                                </div>
                                <div className="ngp-right">
                                    <div className="nt-btn">
                                        <Pagination 
                                        page = {(e)=>this.pageHandler(e)}
                                        next = {this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.currPage +1}
                                        prev = {this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.prePage}
                                        maxPage = {this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.maxPage}
                                        current={this.props.vendorProductTypes.productCatalogueSubmissionHistoryDetails.data.currPage}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default LotDetails;