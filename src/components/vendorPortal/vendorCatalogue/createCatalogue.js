import React from 'react';
import BulkUpload from './bulkUpload';
import BulkUploadProcess from './bulkUploadProcess';
import BulkUploadCompleted from './bulkUploadCompleted';
import BulkUploadRejection from './bulkUploadRejection';
import { Link } from "react-router-dom";

class CreateCatalogue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showBulkUpload: false
        }
    }
    openBulkUpload(e) {
        e.preventDefault();
        this.setState({
            showBulkUpload: !this.state.showBulkUpload
        });
    }
    CancelBulkUpload = (e) => {
        this.setState({
            showBulkUpload: false,
        })
    }


    render (){
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="create-catalogue-pg">
                        <h3>Product Catalogue</h3>
                        <p>You dont have any products in your catalogue , start uploading now</p>
                        <div className="ccp-product">
                            <div className="catalogueBg-1">
                                <img src={require('../../../assets/catalogueBg.svg')} />
                                {/* <svg xmlns="http://www.w3.org/2000/svg" width="297.639" height="297.639" viewBox="0 0 297.639 297.639">
                                    <path id="prefix__blob2" fill="#f2f4f8" d="M119.854-38.041c20.776 28.278 27.557 65.5 15.582 90.606-11.975 25.248-42.706 38.378-72.86 53.094S2.7 136.677-21.1 129.608c-23.95-7.07-42.129-37.512-44.87-66.656-2.89-29.288 9.806-56.989 28.27-84.69C-19.229-49.583 5.01-77.14 34.586-81.324s64.492 15.004 85.268 43.283z" transform="rotate(45 -56.67 221.168)"/>
                                </svg> */}
                            </div>
                            <img className="catalogueBg-2" src={require('../../../assets/catalogueBg2.svg')} />
                            {/* <svg xmlns="http://www.w3.org/2000/svg" width="286.493" height="288.502" viewBox="0 0 286.493 288.502">
                                <path id="prefix__blob2" fill="#f2f4f8" d="M119.854-38.041c20.776 28.278 27.557 65.5 15.582 90.606-11.975 25.248-42.706 38.378-72.86 53.094S2.7 136.677-21.1 129.608c-23.95-7.07-42.129-37.512-44.87-66.656-2.89-29.288 9.806-56.989 28.27-84.69C-19.229-49.583 5.01-77.14 34.586-81.324s64.492 15.004 85.268 43.283z" opacity="0.335" transform="rotate(-150 106.377 70.309)"/>
                            </svg> */}
                            <div className="cc-emptybox">
                                <img src={require('../../../assets/empty-box.svg')} />
                                <p>Empty</p>
                            </div>
                        </div>
                        <div className="cc-footer-btn">
                            <Link to="/productCatalogue/addProducts"><button className="ccfb-menual-entry" type="button">
                                <span className="ccf-add">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.5" height="9.5" viewBox="0 0 9.5 9.5">
                                        <path id="prefix__add" fill="#fff" d="M14.25 10.292h-3.958v3.958H8.708v-3.958H4.75V8.708h3.958V4.75h1.583v3.958h3.959z" transform="translate(-4.75 -4.75)"/>
                                    </svg>
                                </span>
                                Manual Entry
                            </button></Link>
                            <button className="ccfb-bulk-upload" type="button" onClick={(e) => this.openBulkUpload(e)}>
                                <span className="ccf-add">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.5" height="9.5" viewBox="0 0 8.627 12.652">
                                        <path id="prefix__iconmonstr-upload-5" fill="#fff" d="M6.355 9.489V4.217h-1.2l2.158-2.641 2.162 2.641h-1.2v5.272zM5.4 10.543h3.831V5.272h2.4L7.313 0 3 5.272h2.4zm5.272-.527V11.6H3.959v-1.584H3v2.636h8.627v-2.636z" transform="translate(-3)"/>
                                    </svg>
                                </span>
                                Bulk Upload
                            </button>
                        </div>
                    </div>
                </div>
                {/* <BulkUploadRejection /> */}
                {/* <BulkUploadCompleted /> */}
                {/* <BulkUploadProcess /> */}
                {this.state.showBulkUpload && <BulkUpload CancelBulkUpload={this.CancelBulkUpload} />}
                {/* <Footer /> */}
            </div>
        )
    }
}
export default CreateCatalogue;