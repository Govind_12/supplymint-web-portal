import React from 'react';

class AssignToRetailer extends React.Component {
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content assign-to-retailer-modal">
                    <div className="atrm-head">
                        <button type="button" className="bum-close-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                        <input type="text" placeholder="Search Retailer" />
                        <img className="search-icon" src={require('../../../assets/searchicon.svg')} />
                    </div>
                    <div className="atrm-body">
                        <p>Retailers List</p>
                        <div className="atrm-body-inner">
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box pad-0">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                        </div>
                        <div className="atrm-body-inner">
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box pad-0">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                        </div>
                        <div className="atrm-body-inner">
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box pad-0">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                        </div>
                        <div className="atrm-body-inner">
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                            <div className="atrmb-box pad-0">
                                <div className="atrmb-box-inner">
                                    <div className="atrmbb-check">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" name="selectEach" />
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>
                                    <span className="atrmbb-text">Vmart Pvt ltd</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="atrm-footer">
                        <button type="button" className="atrm-discard">Discard</button>
                        <button type="button" className="atrm-assign">Assign</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default AssignToRetailer;