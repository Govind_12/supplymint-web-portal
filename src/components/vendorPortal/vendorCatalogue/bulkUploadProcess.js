import React from 'react';

class BulkUploadProcess extends React.Component {
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content bulk-upload-process-modal">
                    <div className="bump-head">
                        <h3>Upload Inprogress</h3>
                        <p>This will usually take 1 minutes to upload , be patience & wait for process to be complete</p>
                        <button type="button" className="bum-close-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="bump-body">
                        <div className="bump-file-name">
                            <p>FIle Name.xls</p>
                        </div>
                        <div className="circle-wrap">
                            <div className="bump-circle">
                                <div className="mask full">
                                    <div className="fill"></div>
                                </div>
                                <div className="mask half">
                                    <div className="fill"></div>
                                </div>
                                <div className="inside-circle"></div>
                            </div>
                        </div>
                        <div className="bump-footer">
                            <span>78% Completed</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default BulkUploadProcess;