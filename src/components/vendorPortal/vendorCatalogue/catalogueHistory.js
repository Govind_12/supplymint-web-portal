import React from 'react';
import ColoumSetting from '../../replenishment/coloumSetting';
import Pagination from '../../pagination';
import { Link } from "react-router-dom";
import Loader from '../../loaders/filterLoader';
import VendorFilter from '../../../components/vendorPortal/vendorFilter';
import HeaderFilter from '../../../assets/headerFilter.svg';

class CatalogueHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDownloadDrop: false,
            showBulkAction: false,
            filter: false,
            filterBar: false,
            history:{
                type:1,
                pageNo:1,
                search:"",
                searchBy:"contains",
                filter:{},
                sortedBy:"creationTime",
                sortedIn:"ASC"
             },
             filterKey: "",
             filterType: "",
             prevFilter: "",
             filterItems: { },
            applyFilter: false,
            showDownloadDrop: false,
            checkedFilters: [],
            filteredValue: [],
            getHeaderConfig: [],
            fixedHeader: [],
            customHeaders: {},
            headerConfigState: {},
            headerConfigDataState: {},
            fixedHeaderData: [],
            customHeadersState: [],
            headerState: {},
            headerSummary: [],
            defaultHeaderMap: [],
            confirmModal: false,
            headerMsg: "",
            paraMsg: "",
            headerCondition: false,
            tableCustomHeader: [],
            tableGetHeaderConfig: [],
            saveState: [],
            dReport: false,
            pushedPo: {},
            lineItemFixedHeaders: {},
            lineItemDefaultHeaders: {},
            lineItemCustomHeaders: {},
        }
        this.showDownloadDrop = this.showDownloadDrop.bind(this);
        this.closeDownloadDrop = this.closeDownloadDrop.bind(this);
    }

    handleCheckedItems = (e, data) => {
        let array = [...this.state.checkedFilters]
        if (this.state.checkedFilters.some((item) => item == data)) {
            array = array.filter((item) => item != data)
            this.setState({ [data]: "" })
        } else {
            array.push(data)
        }
        var check = array.some((data) => this.state[data] == "" || this.state[data] == undefined)
        this.setState({ checkedFilters: array, applyFilter: !check })
    }
    handleInput = (event) => {
        var applyFilter = this.state.applyFilter
        var value = event.target.value
        var name = event.target.dataset.value
        if (/^\s/g.test(value)) {
            value = value.replace(/^\s+/, '');
        }
        this.setState({ [name]: value, applyFilter }, () => {
            if (this.state.checkedFilters.some((hdata) => this.state[hdata] == "" || this.state[hdata] == undefined)) {
                this.setState({ applyFilter: false })
            } else {
                this.setState({ applyFilter: true })
            }
        })
    }

    submitFilter = () => {
        let payload = {}
        this.state.checkedFilters.map((data) => (payload[Object.keys(this.state.filterItems).find(key => this.state.filterItems[key] === data)] = this.state[data]))
        Object.keys(payload).map((data) => (data.includes("creationTime") && (payload[data] = payload[data] == "" ? "" : payload[data])))
        let t = this.state.history;
        let data = {
            type:t.type == 3 ? 3 : 2,
            pageNo:t.pageNo,
            search:t.search,
            searchBy:t.searchBy,
            filter:payload,
            sortedBy:t.sortedBy,
            sortedIn:t.sortedIn
         }
         console.log(data)
        this.props.productCatalogueSubmissionHistoryRequest(data)
        this.setState(prevState =>({
            filter: false,
            filteredValue: payload,
            history: {
                ...prevState.history,
                type: prevState.history.type == 3 ? 3 : 2,
                filter: payload,
            }
        }))
    }

    clearFilter = () => {
        if (this.state.history.type == 3 || this.state.history.type == 2) {
            let t = this.state.history;
            let payload = {
                type:t.type == 3 ? 3 : 1,
                pageNo:t.pageNo,
                search:t.search,
                searchBy:t.searchBy,
                filter:{},
                sortedBy:t.sortedBy,
                sortedIn:t.sortedIn
            }
            this.props.productCatalogueSubmissionHistoryRequest(payload)

        }
        this.setState({
            filteredValue: [],
            history: {
                ...this.state.history,
                filter: {},
                type: this.state.history.type == 3 ? 3 : 1,
            },
            selectAll: false,
        })
        this.state.checkedFilters.map((data) => this.setState({ checkedFilters: [], [data]: "" }))
    }
    closeFilter(e) {
        // e.preventDefault();
        this.setState({
            filter: false,
            filterBar: false
        });
    }

    searchHandler = (e) =>{
        let val = e.target.value;
        this.setState(prevState => ({
            history: {
              ...prevState.history,         
              type: 3,
              search: val
            }
          }))
        console.log(this.state.history)
    }

    searchDataHandler = (e) =>{
        if (e.keyCode == 13) {
            console.log('trigger')
            console.log(this.state.history)
            this.getHistory()
            this.setState(prevState =>({

            }))
        }
    }

    getHistory = () =>{
        let t = this.state.history;
        let data = {
            type:t.type,
            pageNo:t.pageNo,
            search:t.search,
            searchBy:t.searchBy,
            filter:{...t.filter},
            sortedBy:t.sortedBy,
            sortedIn:t.sortedIn
         }
        this.props.productCatalogueSubmissionHistoryRequest(data)
    }

    openFilter(e) {
        e.preventDefault();
        this.setState({
            filter: !this.state.filter,
            filterBar: !this.state.filterBar
        });
    }

    closeFilter(e) {
        this.setState({
            filter: false,
            filterBar: false
        });
    }

    componentDidMount(){
        let t = this.state.history;
        let data = {
            type:t.type,
            pageNo:t.pageNo,
            search:t.search,
            searchBy:t.searchBy,
            filter:{},
            sortedBy:t.sortedBy,
            sortedIn:t.sortedIn
         }
        this.props.productCatalogueSubmissionHistoryRequest(data)
        if (!this.props.vendorProductTypes.productCatalogueHistoryHeader.isSuccess) {
            let payload = {
                enterpriseName: sessionStorage.getItem('userName'),
                attributeType: "ITEM_SUBMISSION_HISTORY",
                displayName: "TABLE HEADER"
            }
            this.props.productCatalogueHistoryHeaderRequest(payload)
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        if (nextProps.vendorProductTypes.productCatalogueHistoryHeader.isSuccess) {

            if (nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource != null) {

                let getHeaderConfig = nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] != undefined ? Object.values(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"]) : []
                let fixedHeader = nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Fixed Headers"] != undefined ? Object.values(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Fixed Headers"]) : []
                let customHeadersState = nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"] != undefined ? Object.values(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"]) : []


                return {
                    customHeaders: prevState.headerCondition ? nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] : nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"],
                    getHeaderConfig,
                    fixedHeader,
                    customHeadersState,
                    filterItems: Object.keys(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"]).length == 0 ? nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] : nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"].PO,
                    // tableCustomHeader: Object.values(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"]),
                    // tableGetHeaderConfig: Object.values(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"]),
                    headerConfigState: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] != undefined ? nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] : {},
                    fixedHeaderData: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Fixed Headers"] != undefined ? nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Fixed Headers"] : {},
                    headerConfigDataState: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] != undefined ? { ...nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] } : {},
                    headerSummary: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"]) : [],
                    defaultHeaderMap: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"]) : [],
                    pushedPo: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"] == undefined ? nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] : nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"],

                    lineItemDefaultHeaders: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"] == undefined ? {} : nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Default Headers"],
                    lineItemFixedHeaders: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Fixed Headers"] == undefined ? {} : nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Fixed Headers"],

                    lineItemCustomHeaders: nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"] == undefined ? {} : nextProps.vendorProductTypes.productCatalogueHistoryHeader.data.resource["Custom Headers"],

                }


            }
        }
    }

    pageHandler = (e) =>{
        let t = this.state.history;
        let data = {
            type:t.type,
            pageNo:this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.currPage,
            search:t.search,
            searchBy:t.searchBy,
            filter:{submissionType:t.filter.submissionType,
                    uploadStatus:t.filter.uploadStatus,
                    submissionId:t.filter.submissionId},
            sortedBy:t.sortedBy,
            sortedIn:t.sortedIn
         }
        if(e.target.id == 'last'){
            data.pageNo = this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.maxPage;
            this.props.productCatalogueSubmissionHistoryRequest(data)
        }
        if(e.target.id == 'first'){
            if(data.pageNo!=1){
            data.pageNo = 1;
            this.props.productCatalogueSubmissionHistoryRequest(data)
            }
        }
        if(e.target.id == 'next'){
            data.pageNo = this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.currPage +1;
            this.props.productCatalogueSubmissionHistoryRequest(data)
        }
        if(e.target.id == 'prev'){
            if(data.pageNo>1){
            data.pageNo = this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.currPage -1;
            this.props.productCatalogueSubmissionHistoryRequest(data)
            }
        }
    }


    showDownloadDrop(event) {
        event.preventDefault();
        this.setState({ showDownloadDrop: true }, () => {
            document.addEventListener('click', this.closeDownloadDrop);
        });
    }
    closeDownloadDrop() {
        this.setState({ showDownloadDrop: false }, () => {
            document.removeEventListener('click', this.closeDownloadDrop);
        });
    }
    openBulkAction(e) {
        e.preventDefault();
        this.setState({
            showBulkAction: !this.state.showBulkAction
        });
    }

    
    getTime = (date) =>{
        var today = new Date(date);
        let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        return time;
    }

    render() {
        console.log(this.state.filterItems)
        let historyData = this.props.vendorProductTypes.productCatalogueSubmissionHistory; 
        let historyItems = <tr><td></td></tr>
        if(historyData.data.resource){
            historyItems = historyData.data.resource.map(item=>{
                return <tr>
                <td className="fix-action-btn">
                    <ul className="table-item-list">
                        <li className="til-inner">
                            <label className="checkBoxLabel0">
                                <input type="checkBox" name="selectEach" />
                                <span className="checkmark1"></span>
                            </label>
                        </li>
                        <li className="til-inner til-edit-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 21.296 19.52">
                                <g id="prefix__server" transform="translate(-8 -8.667)">
                                    <path id="prefix__Path_700" fill="#8b77fa" d="M17.214 25.981a.666.666 0 0 1-.483-.208l-4.215-4.436A.666.666 0 0 1 13 20.212h2v-3.77a1.11 1.11 0 0 1 1.1-1.109h2.219a1.11 1.11 0 0 1 1.109 1.109v3.772h2a.666.666 0 0 1 .483 1.125L17.7 25.774a.664.664 0 0 1-.486.207z" data-name="Path 700" transform="translate(1.434 2.206)"/>
                                    <path id="prefix__Path_701" fill="#a4b9dd" d="M24.569 12.15a6.761 6.761 0 0 0-12.485 1.689 4.508 4.508 0 0 0-.076 8.968 2.448 2.448 0 0 1 2.646-2.161v-2a2.887 2.887 0 0 1 2.884-2.884h2.219a2.887 2.887 0 0 1 2.883 2.884v2a2.466 2.466 0 0 1 2.465 1.477 2.422 2.422 0 0 1 .157.551 5.365 5.365 0 0 0-.693-10.524z" data-name="Path 701"/>
                                </g>
                            </svg>
                            {/* <span className="generic-tooltip">Server</span> */}
                        </li>
                        <Link to={{ pathname: '/productCatalogue/catalogueHistory/lotDetails', query: item }}><li className="til-inner">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="17" viewBox="0 0 15.802 19.28">
                                <g id="prefix__files-and-folders" transform="translate(-3.762 -3.715)">
                                    <g id="prefix__Group_2650" data-name="Group 2650" transform="translate(3.762 3.715)">
                                        <path id="prefix__Path_702" fill="#a4b9dd" d="M114.242 11.116v10.332A1.547 1.547 0 0 1 112.7 23H99.987a1.547 1.547 0 0 1-1.5-1.186l-.006-.884-.036-5.267V5.262a1.547 1.547 0 0 1 1.547-1.547h6.855c.616 0 2.611 1.7 4.375 3.493 1.603 1.635 3.02 3.345 3.02 3.908z" data-name="Path 702" transform="translate(-98.44 -3.715)"/>
                                        <path fill="#cbe2ff" id="prefix__Path_703" d="M382.634 99.008V102.78c-.116-4.644-3.9-4.859-5.137-6.1l2.111-1.584c1.609 1.639 3.026 3.35 3.026 3.912z" class="prefix__cls-2" data-name="Path 703" transform="translate(-366.832 -91.607)"/>
                                        <path fill="#cbe2ff" id="prefix__Path_704" d="M223.01 3.715h-3.772c4.644.116 4.859 3.9 6.1 5.137l1.584-2.111c-1.635-1.609-3.346-3.026-3.908-3.026z" class="prefix__cls-2" data-name="Path 704" transform="translate(-214.609 -3.715)"/>
                                        <path id="prefix__Path_705" fill="#bed8fb" d="M309.2 11.116v.654a2.5 2.5 0 0 0-2.5-2.495h-1.518a1.547 1.547 0 0 1-1.547-1.547V6.21a2.5 2.5 0 0 0-2.495-2.495h.654a4.444 4.444 0 0 1 3.142 1.3l2.958 2.958a4.444 4.444 0 0 1 1.306 3.143z" data-name="Path 705" transform="translate(-293.393 -3.715)"/>
                                    </g>
                                    <g id="prefix__Group_2651" data-name="Group 2651" transform="translate(6.083 7.879)">
                                        <path fill="#e5eef7" id="prefix__Path_706" d="M170.034 225.578h-10.579a.291.291 0 1 1 0-.582h10.579a.291.291 0 0 1 0 .582z" class="prefix__cls-4" data-name="Path 706" transform="translate(-159.164 -220.703)"/>
                                        <path fill="#e5eef7" id="prefix__Path_707" d="M170.033 266.073h-10.578a.291.291 0 1 1 0-.582h10.578a.291.291 0 1 1 0 .582z" class="prefix__cls-4" data-name="Path 707" transform="translate(-159.164 -259.651)"/>
                                        <path fill="#e5eef7" id="prefix__Path_708" d="M170.033 306.568h-10.578a.291.291 0 1 1 0-.582h10.578a.291.291 0 1 1 0 .582z" class="prefix__cls-4" data-name="Path 708" transform="translate(-159.164 -298.598)"/>
                                        <path fill="#e5eef7" id="prefix__Path_709" d="M167.6 347.063h-8.143a.291.291 0 1 1 0-.582h8.143a.291.291 0 0 1 0 .582z" class="prefix__cls-4" data-name="Path 709" transform="translate(-159.164 -337.545)"/>
                                        <path fill="#e5eef7" id="prefix__Path_710" d="M163.34 113.258h-3.886a.291.291 0 0 1 0-.582h3.886a.291.291 0 1 1 0 .582z" class="prefix__cls-4" data-name="Path 710" transform="translate(-159.163 -112.676)"/>
                                    </g>
                                </g>
                            </svg>
                            {/* <span className="generic-tooltip">File</span> */}
                        </li></Link>
                    </ul>
                </td>   
                {this.state.headerSummary.length == 0 ? this.state.defaultHeaderMap.map((hdata, key) => {
                                                if(item[hdata] == 'Verification Pending'){
                                                    return <td className="cat-pending" key={key}><label><img src={require('../../../assets/error1.svg')} />{item[hdata]}</label></td>
                                                }
                                                if(item[hdata] == 'Partial Completed'){
                                                    return <td className="cat-partial-complete" key={key}><label><img src={require('../../../assets/purpleTick.svg')} />{item[hdata]}</label></td>
                                                    
                                                }
                                                if(item[hdata] == 'Catalogue Rejected'){
                                                    return <td className="cat-rejected" key={key}><label><img src={require('../../../assets/cross.svg')} />{item[hdata]}</label></td>
                                                    
                                                }
                                                if(item[hdata] == 'Cataloging Complete'){
                                                    return <td className="cat-completed" key={key}><label><img src={require('../../../assets/tick1.svg')} />{item[hdata]}</label></td>
                                                }
                                                else{
                                                    return <td key={key}><label>{item[hdata]}</label></td>
                                                }
                                            }) : this.state.headerSummary.map((sdata, keyy) => {
                                                if(item[sdata] == 'Verification Pending'){
                                                    return <td className="cat-pending" key={keyy}><label><img src={require('../../../assets/error1.svg')} />{item[sdata]}</label></td>
                                                }
                                                if(item[sdata] == 'Partial Completed'){
                                                    return <td className="cat-partial-complete" key={keyy}><label><img src={require('../../../assets/purpleTick.svg')} />{item[sdata]}</label></td>
                                                    
                                                }
                                                if(item[sdata] == 'Catalogue Rejected'){
                                                    return <td className="cat-rejected" key={keyy}><label><img src={require('../../../assets/cross.svg')} />{item[sdata]}</label></td>
                                                    
                                                }
                                                if(item[sdata] == 'Cataloging Complete'){
                                                    return <td className="cat-completed" key={keyy}><label><img src={require('../../../assets/tick1.svg')} />{item[sdata]}</label></td>
                                                }
                                                else{
                                                    return <td key={keyy}><label>{item[sdata]}</label></td>
                                                }
                                            })}
                {/* <td><label className="bold">{item.submissionId}</label></td>
                <td><label>{item.uploadStatus}</label></td>
                {item.catalogueStatus == 'Verification Pending' && <td className="cat-pending"><label><img src={require('../../../assets/error1.svg')} />{item.catalogueStatus}</label></td>}
                {item.catalogueStatus == 'Partial Completed' && <td className="cat-partial-complete"><label><img src={require('../../../assets/purpleTick.svg')} />{item.catalogueStatus}</label></td>}
                {item.catalogueStatus == 'Catalogue Rejected' && <td className="cat-rejected"><label><img src={require('../../../assets/cross.svg')} />{item.catalogueStatus}</label></td>}
                {item.catalogueStatus == 'Cataloging Complete' && <td className="cat-completed"><label><img src={require('../../../assets/tick1.svg')} />{item.catalogueStatus}</label></td>}
                <td><label>{item.submissionType}</label></td>
                <td><label>{item.createdBy}</label></td>
                <td><label>{item.creationTime} </label></td> */}
            </tr>
            })
        }
        return(
            <div>
                {!this.props.vendorProductTypes.productCatalogueSubmissionHistory.isSuccess && <Loader />}
                <div className="container-fluid p-lr-0">
                    <div className="col-lg-12 pad-0 m-top-15">
                        <div className="manage-catalogue-design p-lr-47">
                            <div className="col-lg-6 pad-0">
                                <div className="mcd-left">
                                    <div className="gvpd-search">
                                        <input type="search" placeholder="Type To Search" onKeyDown={this.searchDataHandler} onChange={this.searchHandler}/>
                                        <img className="search-image" src={require('../../../assets/searchicon.svg')} />
                                        {/* <span className="closeSearch"><img src={require('../../../assets/clearSearch.svg')} /></span> */}
                                    </div>
                                    <div className="gvpd-download-drop">
                                        <button className={this.state.showDownloadDrop === true ? "pi-download pi-download-focus" : "pi-download"} type="button" onClick={this.showDownloadDrop}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 14.801 16.146">
                                                <path fill="#12203c" id="prefix__dowload4" d="M8.4 14.128L3.018 8.073h4.037V0h2.691v8.073h4.037zm6.055-.673V14.8H2.346v-1.345H1v2.691h14.8v-2.691z" transform="translate(-1)" />
                                            </svg>
                                        </button>
                                        {this.state.showDownloadDrop ? (
                                            <ul className="pi-history-download">
                                                <li>
                                                    <button className="export-excel" type="button">
                                                        <span className="pi-export-svg">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 20.765 22.741">
                                                                <g id="prefix__files" transform="translate(0 2)">
                                                                    <g id="prefix__Group_2456" data-name="Group 2456">
                                                                        <g id="prefix__Group_2455" data-name="Group 2455" transform="translate(0 -2)">
                                                                            <path fill="#12203c" id="prefix__Path_606" d="M1.421 20.609V2.132a.711.711 0 0 1 .711-.711h10.66v2.843a1.421 1.421 0 0 0 1.421 1.421h2.843v2.132h1.421V4.974a.711.711 0 0 0-.206-.5L14.007.206A.711.711 0 0 0 13.5 0H2.132A2.132 2.132 0 0 0 0 2.132v18.477a2.132 2.132 0 0 0 2.132 2.132h4.975V21.32H2.132a.711.711 0 0 1-.711-.711z" data-name="Path 606" />
                                                                            <text fontSize="7px" fontFamily="ProximaNova-Bold,Proxima Nova" fontWeight="700" fill="#12203c" id="prefix__XLS" transform="translate(7.765 16.414)">
                                                                                <tspan x="0" y="0">XLS</tspan>
                                                                            </text>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                        Export to Excel
                                                    </button>
                                                </li>
                                            </ul>
                                        ) : (null)}
                                    </div>
                                    <div className="gvpd-filter">
                                        <button type="button" className="gvpd-filter-inner" onClick={(e) => this.openFilter(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                                <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                            </svg>
                                        </button>
                                        {this.state.applyFilter == true && this.state.checkedFilters.length != 0 ? <span className="clr_Filter_shipApp" onClick={(e) => this.clearFilter(e)} >Clear Filter</span> : null}
                                        {this.state.filter && <VendorFilter ref={node => { this.child = node }} {...this.state} {...this.props} saveFilter={this.saveFilter} submitFilter={this.submitFilter} clearFilter={this.clearFilter} handleCheckedItems={(e, data) => this.handleCheckedItems(e, data)} handleInput={this.handleInput} closeFilter={(e) => this.closeFilter(e)} />}
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 pad-0">
                                <div className="mcd-right">
                                    {/* <div className="bulk-action-dropdown">
                                        <button className="mcd-bulk-action" type="button" onClick={(e) => this.openBulkAction(e)}>Bulk Action <img src={require('../../../assets/downArrowNew.svg')} /></button>
                                        {this.state.showBulkAction &&
                                        <ul className="bad-inner">
                                            <li>
                                                <button type="button">Assign To Retailer</button>
                                            </li>
                                            <li>
                                                <button type="button">Delete</button>
                                            </li>
                                        </ul>}
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12 pad-0 p-lr-47">
                        <div className="manage-catalogue-table" >
                            <div className="mct-manage-table" id="scrollDiv">
                                <ColoumSetting />
                                <table className="table mct-main-table">
                                    <thead>
                                        <tr>
                                        <th className="ght-sticky-col">
                                            <label> </label>
                                        </th>
                                        {this.state.customHeadersState.length == 0 ? this.state.getHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                                <img src={HeaderFilter} className="imgHead" onClick={this.filterHeader} data-key={data}/>
                                            </th>
                                        )) : this.state.customHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                                <img src={HeaderFilter} className="imgHead" onClick={this.filterHeader} data-key={data}/>
                                            </th>
                                        ))}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {historyItems}                                    
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-12 pad-0" >
                                <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  value={this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.currPage} />
                                            <span className="ngp-total-item">Total Items </span> <span className="bold">{this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.totalRecord}</span>
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination 
                                            page = {(e)=>this.pageHandler(e)}
                                            next = {this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.currPage +1}
                                            prev = {this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.prePage}
                                            maxPage = {this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.maxPage}
                                            current={this.props.vendorProductTypes.productCatalogueSubmissionHistory.data.currPage}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CatalogueHistory;