import React from 'react';

class BulkUploadCompleted extends React.Component {
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content bulk-upload-process-modal">
                    <div className="bump-head">
                        <button type="button" className="bum-close-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                    </div>
                    <div className="bucm-body">
                        <img src={require('../../../assets/tickNew.svg')} />
                        <span>Upload process completed successfully</span>
                        <p>Total 2746 items uploaded</p>
                        <button type="button" className="bumc-done">Done</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default BulkUploadCompleted;