import React from 'react';
import SuccessMsg from '../../loaders/requestSuccess';
import FilterLoader from '../../loaders/filterLoader';

class AddProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectAgeGroup: false,
            selectDepartment: false,
            selectCategory: false,
            selectSize: false,
            selectBrand: false,
            selectColor: false,
            selectPattern: false,
            selectMaterial: false,
            selectSeason: false,
            selectNeckCollar: false,
            selectOccasion: false,
            selectAdditionalRemark: false,
            selectCatType: false,
            catPageNo:1,
            catType:3,
            catGroupSearch:"",
            catDepartmentSearch:"",
            catCategorySearch:"",
            catSearchBy:"CONTAINS",
            catBasedOn:"category",
            catGroup:'',
            catDepartment:"",
            catCategory:"",
            sizeArray:[],
            sizeChecked:false,
            sizeSearch: '',
            colorSearch: '',
            patternSearch: '',
            seasonSearch: '',
            neckSearch: '',
            fabricSearch: '',
            usagesSearch: '',
            ageSearch:'',
            barCodeSearch:'',
            itemSearch:'',
            mrpSearch:'',
            rateSearch:'',
            minOrderSearch:'',
            availableQtySearch:'',
            descriptionSearch:'',
            brandSearch:'',
            hsnSearch:'',
            departmentSearch:'',
            categorySearch:'',
            catalogueSearch:'',
            uploadImage: false,
            frontImgUrl:null,
            backImgUrl:null,
            sideImgUrl:null,
            detailsImgUrl:null,
            lookImgUrl:null,
            mainImgUrl:null,
            isBasic: true,
            submitSuccess: false,
            categoryErr: false,
            vendorBarErr: false,
            itemErr: false,
            mrpErr: false,
            rateErr: false,
            rateErr1: false,
            sizeErr: false,
            colorErr: false,
            uploadImgErr: false,
        }
    }

    closeSuccessPopup = () =>{
        this.setState({
            submitSuccess: false,
        })
    }

    toggleHandler = () =>{
        this.setState(prevState=>({
            isBasic: !prevState.isBasic,
        }))
    }

    openUploadImage(e) {
        e.preventDefault();
        this.setState({
            uploadImage: !this.state.uploadImage
        });
    }
    ageGroupData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectAgeGroup: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectAgeGroup: false })
        }
    }
    departmentData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectDepartment: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectDepartment: false })
        }
    }
    categoryData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectCategory: true })
            let dataCategory = {
                pageNo:this.state.catPageNo,
                type:this.state.catType,
                groupSearch:this.state.catGroupSearch,
                departmentSearch:this.state.catDepartmentSearch,
                categorySearch:this.state.catCategorySearch,
                searchBy: this.state.catSearchBy,
                basedOn:this.state.catBasedOn,
                group: this.state.catGroup,
                department: this.state.catDepartment,
                category: this.state.catCategory,
             }
            this.props.productCatalogueCategoryRequest(dataCategory )
        } else if (e.keyCode === 27) {
            this.setState({ selectCategory: false })
        }
    }
    sizeData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectSize: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectSize: false })
        }
    }
    brandData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectBrand: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectBrand: false })
        }
    }
    colorData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectColor: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectColor: false })
        }
    }
    patternData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectPattern: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectPattern: false })
        }
    }
    materialData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectMaterial: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectMaterial: false })
        }
    }
    seasonData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectSeason: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectSeason: false })
        }
    }
    neckCollarData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectNeckCollar: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectNeckCollar: false })
        }
    }
    occasionData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectOccasion: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectOccasion: false })
        }
    }
    additionalRemarkData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectAdditionalRemark: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectAdditionalRemark: false })
        }
    }
    catTypeData = (e) => {
        if (e.keyCode == 13) {
            this.setState({ selectCatType: true })
        } else if (e.keyCode === 27) {
            this.setState({ selectCatType: false })
        }
    }
    
    categorySearchHandler = (e) =>{
        this.setState({
            categorySearch: e.target.value,
            catCategorySearch: e.target.value,
        })
    }

    // sizeDropDown search logic
    sizeSearchHandler = (e) =>{
        this.setState({
            sizeSearch: e.target.value,
            selectSize: true,
            sizeCurrentPage: 1,
        })
    } 


    openSizeDropDwon = () =>{
        this.setState({
            selectSize: true,
        })
    }

    // colorDropDown search logic
    colorSearchHandler = (e) =>{
        this.setState({
            colorSearch: e.target.value,
            selectColor: true,
            colorCurrentPage: 1,
        })
    } 


    openColorDropDwon = () =>{
        this.setState({
            selectColor: true,
        })
    }

    // Pattern search logic
    patternSearchHandler = (e) =>{
        this.setState({
            patternSearch: e.target.value,
            selectPattern: true,
            patternCurrentPage: 1,
        })
    }


    openPatternDropDown = () =>{
        this.setState({
            selectPattern: true,
        })
    }

    // season search logic
    seasonSearchHandler = (e) =>{
        this.setState({
            seasonSearch: e.target.value,
            selectSeason: true,
            seasonCurrentPage:1,
        })
    }

    closeSeasonDropDown = () =>{
        this.setState({
            selectSeason: false,
        })
    }

    openSeasonDropDown = () =>{
        this.setState({
            selectSeason: true,
        })
    }

    // Neck search logic
    neckSearchHandler = (e) =>{
        this.setState({
            neckSearch: e.target.value,
            selectNeckCollar: true,
        })
    }

    openNeckDropDown = () =>{
        this.setState({
            selectNeckCollar: true,
        })
    }

    // Fabric Search logic
    fabricSearchHandler = (e) =>{
        this.setState({
            fabricSearch: e.target.value,
            selectMaterial: true,
        })
    }

    openFabricDropDown = () =>{
        this.setState({
            selectMaterial: true,
        })
    }


    // Usages Search logic
    usagesSearchHandler = (e) =>{
        this.setState({
            usagesSearch: e.target.value,
            selectOccasion:true,
        })
    }

    openUsagesDropDown = () =>{
        this.setState({
            selectOccasion: true,
        })
    }


    // data value set for all input
    setColorData = (data) =>{
        console.log('clicked')
        this.setState({
            colorSearch: data,
            selectColor: false,
        })
    }

    setPatternData = (data) =>{
        this.setState({
            patternSearch: data,
            selectPattern: false,
        })
    }

    setNeckData = (data) =>{
        this.setState({
            neckSearch: data,
            selectNeckCollar: false,
        })
    }

    setFabricData = (data) =>{
        this.setState({
            fabricSearch: data,
            selectMaterial: false,
        })
    }

    setUsagesData = (data) =>{
        this.setState({
            usagesSearch: data,
            selectOccasion: false,
        })
    }

    
    setSizeData = (e, data) =>{
        let dataArray = [...this.state.sizeArray, data];
        let filterData = dataArray;
        if(!e.target.checked){
            filterData = dataArray.filter(item=>
                {return item!=data})
        }
        this.setState(prevState=>({
            sizeChecked: true,
            sizeArray: filterData,
            sizeSearch: filterData.toString().replace (/,/g, "|")
        }))
    }

    setCategory = (data) =>{
        this.setState({
            ageSearch: data.group,
            departmentSearch: data.department,
            categorySearch: data.category,
            selectCategory: false,
        })
    }

    setSeasonHandler = (e) =>{
        if(e.target.value == 'Please Select'){
            this.setState({
                seasonSearch: '',
            })
        }
        else{
            this.setState({
                seasonSearch: e.target.value
            })
        }
    }

    setCatalogueHandler = (e) =>{
        if(e.target.value == 'Please Select'){
            this.setState({
                catalogueSearch: '',
            })
        }
        else{
            this.setState({
                catalogueSearch: e.target.value
            })
        }
    }

    componentWillMount(){

        document.addEventListener('mousedown', this.handleClickOutside)
      }
      
      handleClickOutside = (event) =>{
      
        if(event.path[0].id == 'li' || event.path[0].id == 'btn-close' || event.path[0].id == 'prev' || event.path[0].id == 'next' || event.path[0].id == 'dropDown'){
            this.setState({
                selectCategory: true,
            })
            return
        }

        if(event.path[0].id !== 'li' || event.path[0].id !== 'btn-close' || event.path[0].id !== 'prev' || event.path[0].id !== 'next' || event.path[0].id !== 'dropDown'){
            this.setState({
                selectCategory: false,
            })
        }
        if(event.path[0].id == 'sizeLi' || event.path[0].id == 'dropDownSize'){
            this.setState({
                selectSize: true,
            })
            return
        }

        if(event.path[0].id !== 'sizeLi' || event.path[0].id !== 'dropDownSize'){
            this.setState({
                selectSize: false,
            })
        }
        if(event.path[0].id == 'colorLi' || event.path[0].id == 'dropDownColor'){
            this.setState({
                selectColor: true,
            })
            return
        }

        if(event.path[0].id !== 'colorLi' || event.path[0].id !== 'dropDownColor'){
            this.setState({
                selectColor: false,
            })
        }
        if(event.path[0].id == 'patternLi' || event.path[0].id == 'dropDownPattern'){
            this.setState({
                selectPattern: true,
            })
            return
        }

        if(event.path[0].id !== 'patternLi' || event.path[0].id !== 'dropDownPattern'){
            this.setState({
                selectPattern: false,
            })
        }
        if(event.path[0].id == 'fabricLi' || event.path[0].id == 'dropDownFabric'){
            this.setState({
                selectMaterial: true,
            })
            return
        }

        if(event.path[0].id !== 'fabricLi' || event.path[0].id !== 'dropDownFabric'){
            this.setState({
                selectMaterial: false,
            })
        }
        if(event.path[0].id == 'neckLi' || event.path[0].id == 'dropDownNeck'){
            this.setState({
                selectNeckCollar: true,
            })
            return
        }

        if(event.path[0].id !== 'neckLi' || event.path[0].id !== 'dropDownNeck'){
            this.setState({
                selectNeckCollar: false,
            })
        }
        if(event.path[0].id == 'usagesLi' || event.path[0].id == 'dropDownUsages'){
            this.setState({
                selectOccasion: true,
            })
            return
        }

        if(event.path[0].id !== 'usagesLi' || event.path[0].id !== 'dropDownUsages'){
            this.setState({
                selectOccasion: false,
            })
        }
      }

    closeCategoryDropDown = (event) => {
        console.log(!event.currentTarget.contains(event.relatedTarget))
        console.log(event.relatedTarget)
        this.setState({
            selectCategory: false,
        })
    }

    prevCategoryHandler = () =>{
        let dataCategory = {
            pageNo:this.props.vendorProductTypes.productCatalogueCategory.type.currPage -1,
            type:this.state.catType,
            groupSearch:this.state.catGroupSearch,
            departmentSearch:this.state.catDepartmentSearch,
            categorySearch:this.state.catCategorySearch,
            searchBy: this.state.catSearchBy,
            basedOn:this.state.catBasedOn,
            group: this.state.catGroup,
            department: this.state.catDepartment,
            category: this.state.catCategory,
         }
         this.props.productCatalogueCategoryRequest(dataCategory)
    }

    nextCategoryHandler = () =>{
        let dataCategory = {
            pageNo:this.props.vendorProductTypes.productCatalogueCategory.type.currPage +1,
            type:this.state.catType,
            groupSearch:this.state.catGroupSearch,
            departmentSearch:this.state.catDepartmentSearch,
            categorySearch:this.state.catCategorySearch,
            searchBy: this.state.catSearchBy,
            basedOn:this.state.catBasedOn,
            group: this.state.catGroup,
            department: this.state.catDepartment,
            category: this.state.catCategory,
         }
         this.props.productCatalogueCategoryRequest(dataCategory)
    }

    openCategoryDropDown = () =>{
        this.setState({
            selectCategory: true,
        })
    }

    frontImgHandler = (e) => {
        this.setState({
          frontImgUrl: URL.createObjectURL(e.target.files[0])
        })
      }

    removeFrontImage = () =>{
        this.setState({
            frontImgUrl: null,
        })
    }

    backImgHandler = (e) =>{
        this.setState({
            backImgUrl: URL.createObjectURL(e.target.files[0])
        })
    }

    removeBackImage = () =>{
        this.setState({
            backImgUrl: null,
        })
    }

    sideImgHandler = (e) =>{
        this.setState({
            sideImgUrl: URL.createObjectURL(e.target.files[0])
        })
    }

    removeSideImage = () =>{
        this.setState({
            sideImgUrl: null,
        })
    }

    detailsImgHandler = (e) =>{
        this.setState({
            detailsImgUrl: URL.createObjectURL(e.target.files[0])
        })
    }

    removeDetailsImage = () =>{
        this.setState({
            detailsImgUrl: null,
        })
    }

    lookImgHandler = (e) =>{
        this.setState({
            lookImgUrl: URL.createObjectURL(e.target.files[0])
        })
    }

    removeLookImage = () =>{
        this.setState({
            lookImgUrl: null,
        })
    }

    mainImgUrlHandler = (e) =>{
        this.setState({
            mainImgUrl: URL.createObjectURL(e.target.files[0])
        })
    }

    removeMainImage = () =>{
        this.setState({
            mainImgUrl: null,
        })
    }

    barCodeSearchHandler = (e) =>{
        this.setState({
            barCodeSearch: e.target.value,
        })
    }

    itemSearchHandler = (e) =>{
        this.setState({
            itemSearch: e.target.value,
        })
    }

    mrpSearchHandler = (e) =>{
        this.setState({
            mrpSearch: e.target.value,
        })
    }

    rateSearchHandler = (e) =>{
        this.setState({
            rateSearch: e.target.value,
        })
    }

    minOrderSearchHandler = (e) =>{
        this.setState({
            minOrderSearch: e.target.value,
        })
    }

    availableQtySearchHandler = (e) =>{
        this.setState({
            availableQtySearch: e.target.value,
        })
    }

    descriptionSearchHandler = (e) =>{
        this.setState({
            descriptionSearch: e.target.value,
        })
    }

    brandSearchHandler = (e) =>{
        this.setState({
            brandSearch: e.target.value,
        })
    }

    hsnSearchHandler = (e) =>{
        this.setState({
            hsnSearch: e.target.value,
        })
    }

    cancelManualEntry = () =>{
        this.props.history.push('/catalogue/vendor/manage');
    }

    submitHandler = () =>{
        let data = {
            submissionId:"",
            vendorCode:"90048",
            itemId:"150",
            itemCode:"VM001",
            itemName: this.state.itemSearch,
            ageGroup:this.state.ageSearch,
            department:this.state.departmentSearch,
            category:this.state.categorySearch,
            vendorBarcode:this.state.barCodeSearch,
            size:this.state.sizeSearch,
            description:this.state.descriptionSearch,
            brand: this.state.brandSearch,
            availableQty: this.state.availableQtySearch,
            minOrderQty: this.state.minOrderSearch,
            mrp: this.state.mrpSearch,
            rate: this.state.rateSearch,
            color: this.state.colorSearch,
            pattern: this.state.patternSearch,
            material: this.state.fabricSearch,
            season: this.state.seasonSearch,
            neck: this.state.neckSearch,
            occasion: this.state.usagesSearch,
            additionalRemarks:"BUY ONE GET 1",
            imageUrlMain: this.state.mainImgUrl,
            imageUrlFront: this.state.frontImgUrl,
            imageUrlBack: this.state.backImgUrl,
            imageUrlSide: this.state.sideImgUrl,
            imageUrlDetailed: this.state.detailsImgUrl,
            isActive:"YES",
            isStockout:"YES",
            catalogType:"CLOTHS",
            hsn: this.state.hsnSearch,
            udf1:"",
            udf2:"",
            udf3:"",
            udf4:"",
            udf5:"",
            udf6:"",
            udf7:"",
            udf8:"",
            udf9:"",
            udf10:"",
            Status:"SUBMITTED",
            remarks:"NO REMARK",
            isPublished:"YES"
         }
         let t = this.state;
         if(t.categorySearch && t.barCodeSearch && t.itemSearch && t.mrpSearch && t.rateSearch && t.mrpSearch > t.rateSearch && t.sizeSearch && t.colorSearch){
             if(t.mainImgUrl || t.frontImgUrl || t.backImgUrl || t.sideImgUrl || t.lookImgUrl || t.detailsImgUrl){
                this.props.productCatalogueManualRequest(data)
                this.setState({
                    submitSuccess: true,
                    sizeSearch:'',
                    categorySearch:'',
                    ageSearch:'',
                    departmentSearch:'',
                    barCodeSearch:'',
                    itemSearch:'',
                    mrpSearch:'',
                    rateSearch:'',
                    minOrderSearch:'',
                    availableQtySearch:'',
                    colorSearch:'',
                    patternSearch:'',
                    fabricSearch:'',
                    neckSearch:'',
                    usagesSearch:'',
                    brandSearch:'',
                    seasonSearch:'',
                    catalogueSearch:'',
                    hsnSearch:'',
                    mainImgUrl:null,
                    frontImgUrl:null,
                    sideImgUrl:null,
                    backImgUrl:null,
                    detailsImgUrl:null,
                    lookImgUrl:null,
                    categoryErr: false,
                    vendorBarErr: false,
                    itemErr: false,
                    mrpErr: false,
                    rateErr: false,
                    rateErr1: false,
                    sizeErr: false,
                    colorErr: false,
                    uploadImgErr: false,
                })
             }
         }
         this.setState({
             uploadImgErr: true,
         })
         if(t.mainImgUrl || t.frontImgUrl || t.backImgUrl || t.sideImgUrl || t.lookImgUrl || t.detailsImgUrl){
             this.setState({
                uploadImgErr: false
            })
         }
         if(!t.colorSearch){
             this.setState({
                 colorErr: true,
             })
         }
         if(!t.sizeSearch){
             this.setState({
                 sizeErr: true,
             })
         }
         if(!t.rateSearch){
             this.setState({
                 rateErr: true,
             })
         }
         if(t.rateSearch > t.mrpSearch){
             this.setState({
                 rateErr: true,
                 rateErr1: true,
             })
         }
         if(!t.mrpSearch){
            this.setState({
                mrpErr: true,
            })
         }
         if(!t.itemSearch){
             this.setState({
                    itemErr: true,
                })
            }
        if(!t.barCodeSearch){
            this.setState({
                vendorBarErr: true,
                })
            }
        if(!t.categorySearch){
            this.setState({
                categoryErr: true,
            })
        }
    }

    componentDidMount(){
        // let dataDepartment = {
        //     type:'NECK',
        // }
        let dataCategory = {
            pageNo:this.state.catPageNo,
            type:this.state.catType,
            groupSearch:this.state.catGroupSearch,
            departmentSearch:this.state.catDepartmentSearch,
            categorySearch:this.state.catCategorySearch,
            searchBy: this.state.catSearchBy,
            basedOn:this.state.catBasedOn,
            group: this.state.catGroup,
            department: this.state.catDepartment,
            category: this.state.catCategory,
         }
        let dataSize = {
            type: 'SIZE'
        }
        let dataColour = {
            type: 'COLOUR'
        }
        let dataPattern = {
            type: 'PATTERN'
        }
        let dataSeason = {
            type: 'SEASON'
        }
        let dataNeck = {
            type: 'NECK'
        }
        let dataFabric = {
            type: 'FABRIC'
        }
        let dataUsages = {
            type: 'USAGE'
        }
        // this.props.productCatalogueDepartmentRequest(dataDepartment)
        this.props.productCatalogueCategoryRequest(dataCategory)
        this.props.productCatalogueSizeRequest(dataSize)
        this.props.productCatalogueColourRequest(dataColour)
        this.props.productCataloguePatternRequest(dataPattern)
        this.props.productCatalogueSeasonRequest(dataSeason)
        this.props.productCatalogueNeckRequest(dataNeck)
        this.props.productCatalogueFabricRequest(dataFabric)
        this.props.productCatalogueUsagesRequest(dataUsages )
    }

    render() {
        let catSuccess = this.props.vendorProductTypes.productCatalogueCategory.isSuccess;
        let categoryVal = this.props.vendorProductTypes.productCatalogueCategory.type;
        let catCurrentPage = categoryVal.currPage;
        let catMaxPage = categoryVal.maxPage;
        let catPrevPage = categoryVal.prePage;
        let categoryItems = <li>
                                <span className="vendor-details">
                                    <span className="vd-name div-col-3">No data</span>
                                    <span className="vd-name div-col-3">No data</span>
                                    <span className="vd-name div-col-3">No data</span>
                                </span>
                            </li>
        console.log(categoryVal.isSuccess)
        if(catSuccess){
            categoryItems = categoryVal.resource.map((item, index)=>{
                return (
                    <li key={index} onClick={()=>this.setCategory(item)} id='li'>
                        <span className="vendor-details">
                            <span className="vd-name div-col-3" id='li'>{item.group}</span>
                            <span className="vd-name div-col-3" id='li'>{item.department}</span>
                            <span className="vd-name div-col-3" id='li'>{item.category}</span>
                        </span>
                    </li>
                )
                })
        }

// size DropDown Search logic

        let sizeVal = this.props.vendorProductTypes.productCatalogueSize;
        let sizeLowerVal = this.state.sizeSearch.toLowerCase();
        let sizeFilterVal = sizeVal.type;
        if(sizeVal.type !== '' && this.state.sizeSearch !== '' && !this.state.sizeChecked){
            sizeFilterVal = sizeVal.type.filter(item=>{
                return item.name.toLowerCase().includes(sizeLowerVal)
            })
        }
        let sizeItems =  <li>
                            <span className="vendor-details">
                                <span className="vd-name div-col-1" >No data</span>
                            </span>
                        </li>
        if(sizeVal.isSuccess){
            sizeItems = sizeFilterVal.map((item, index)=>{
                return <li id="sizeLi">
                        <label className="checkBoxLabel0" key={index} id="sizeLi">
                            <input type="checkbox" id="sizeLi" onChange={(e)=>this.setSizeData(e, item.name)}/>
                            <span className="checkmark1" id="sizeLi"></span>
                            {item.name}
                        </label>
                        {/* <span className="vendor-details" id="sizeLi">
                            <span className="vd-name div-col-1"  id="sizeLi"></span>
                        </span> */}
                    </li>
                })
        }

// color search logic

        let colorVal = this.props.vendorProductTypes.productCatalogueColour;
        let colorLowerVal = this.state.colorSearch.toLowerCase();
        let colorFilterVal = colorVal.type;
        if(colorVal.type !== '' && this.state.colorSearch !== ''){
            colorFilterVal = colorVal.type.filter(item=>{
                return item.name.toLowerCase().includes(colorLowerVal)
            })
        }
        let colorItems =  <li>
                            <span className="vendor-details">
                                <span className="vd-name div-col-1" >No data</span>
                            </span>
                        </li>
        if(colorVal.isSuccess){
            colorItems = colorFilterVal.map((item, index)=>{
                return <li onClick={()=>this.setColorData(item.name)} id="colorLi" >
                        <span className="vendor-details" id="colorLi">
                            <span className="vd-name div-col-1" key={index} id="colorLi">{item.name}</span>
                        </span>
                    </li>
                })
        }

        // pattern search logic

        let patternVal = this.props.vendorProductTypes.productCataloguePattern;
        let patternLowerVal = this.state.patternSearch.toLowerCase();
        let patternFilterVal = patternVal.type;
        if(patternVal.type !== '' && this.state.patternSearch !== ''){
            patternFilterVal = patternVal.type.filter(item=>{
                return item.name.toLowerCase().includes(patternLowerVal)
            })
        }
        let patternItems =  <li>
                            <span className="vendor-details">
                                <span className="vd-name div-col-1" >No data</span>
                            </span>
                        </li>
        if(patternVal.isSuccess){
            patternItems = patternFilterVal.map((item, index)=>{
                return <li onClick={()=>this.setPatternData(item.name)} id="patternLi">
                        <span className="vendor-details" id="patternLi">
                            <span className="vd-name div-col-1" key={index} id="patternLi">{item.name}</span>
                        </span>
                    </li>
                })
        }

                // season DropDown

                let seasonVal = this.props.vendorProductTypes.productCatalogueSeason;
                let seasonItems =  <option>No data</option>
                if(seasonVal.isSuccess){
                    seasonItems = seasonVal.type.map((item, index)=>{
                        return <option key={index}>{item.name}</option>
                        })
                }

        // neck search logic

        let neckVal = this.props.vendorProductTypes.productCatalogueNeck;
        let neckLowerVal = this.state.neckSearch.toLowerCase();
        let neckFilterVal = neckVal.type;
        if(neckVal.type !== '' && this.state.neckSearch !== ''){
            neckFilterVal = neckVal.type.filter(item=>{
                return item.name.toLowerCase().includes(neckLowerVal)
            })
        }
        let neckItems =  <li>
                            <span className="vendor-details">
                                <span className="vd-name div-col-1" >No data</span>
                            </span>
                        </li>
        if(neckVal.isSuccess){
            neckItems = neckFilterVal.map((item, index)=>{
                return <li onClick={()=>this.setNeckData(item.name)} id="neckLi">
                        <span className="vendor-details" id="neckLi">
                            <span className="vd-name div-col-1" key={index} id="neckLi">{item.name}</span>
                        </span>
                    </li>
                })
        }

               // fabric search logic

               let fabricVal = this.props.vendorProductTypes.productCatalogueFabric;
               let fabricLowerVal = this.state.fabricSearch.toLowerCase();
               let fabricFilterVal = fabricVal.type;
               if(fabricVal.type !== '' && this.state.fabricSearch !== ''){
                   fabricFilterVal = fabricVal.type.filter(item=>{
                       return item.name.toLowerCase().includes(fabricLowerVal)
                   })
               }
               let fabricItems =  <li>
                                   <span className="vendor-details">
                                       <span className="vd-name div-col-1" >No data</span>
                                   </span>
                               </li>
               if(fabricVal.isSuccess){
                   fabricItems = fabricFilterVal.map((item, index)=>{
                       return <li onClick={()=>this.setFabricData(item.name)} id="fabricLi">
                               <span className="vendor-details" id="fabricLi">
                                   <span className="vd-name div-col-1" key={index} id="fabricLi">{item.name}</span>
                               </span>
                           </li>
                       })
               }

            //    Usages Search Logic
            let usagesVal = this.props.vendorProductTypes.productCatalogueUsages;
            let usagesLowerVal = this.state.usagesSearch.toLowerCase();
            let usagesFilterVal = usagesVal.type;
            if(usagesVal.type !== '' && this.state.usagesSearch !== ''){
                usagesFilterVal = usagesVal.type.filter(item=>{
                    return item.name.toLowerCase().includes(usagesLowerVal)
                })
            }
            let usagesItems =  <li>
                                   <span className="vendor-details">
                                       <span className="vd-name div-col-1" >No data</span>
                                   </span>
                               </li>
               if(usagesVal.isSuccess){
                   usagesItems = usagesFilterVal.map((item, index)=>{
                       return <li onClick={()=>this.setUsagesData(item.name)} id="usagesLi">
                               <span className="vendor-details" id="usagesLi">
                                   <span className="vd-name div-col-1" key={index} id="usagesLi">{item.name}</span>
                               </span>
                           </li>
                       })
               }
        return(
            <div>
            {this.state.submitSuccess && this.props.vendorProductTypes.productCatalogueManualSubmit.isSuccess && <SuccessMsg successMessage={this.props.vendorProductTypes.productCatalogueManualSubmit.data.message +'\n'+'Submission Id:'+ this.props.vendorProductTypes.productCatalogueManualSubmit.data.resource.submissionId} closeRequest={(e) => this.closeSuccessPopup(e)}/>}
            {!this.props.vendorProductTypes.productCatalogueCategory.isSuccess && <FilterLoader />} 
 
            <div className="container-fluid p-lr-0">
                <div className="col-lg-12 pad-0">
                    <div className="manage-catalogue-design p-lr-47 border-btm">
                        <div className="col-lg-6 pad-0">
                            <div className="mcd-left">
                                <div className="mcdl-title">
                                    <h3>Create Catalogue</h3>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="mcd-right">
                                <button type="button" className="apt-cancel" onClick={this.cancelManualEntry}>Cancel</button>
                                <button type="button" className="continue-btn" onClick={this.submitHandler}>Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="col-lg-7 pad-0">
                        <div className="col-lg-4 pad-lft-0">
                            <div className="apt-box">
                                <label>Age Group</label>
                                <div className="inputTextKeyFucMain aptb-input">
                                    <input type="text" className="pnl-purchase-read" value={this.state.ageSearch} disabled />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 pad-lft-0">
                            <div className="apt-box">
                                <label>Department</label>
                                <div className="inputTextKeyFucMain aptb-input">
                                    <input type="text" className="onFocus pnl-purchase-read"  value={this.state.departmentSearch} disabled />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 pad-lft-0">
                            <div className="apt-box">
                                <label>Category<span className="mandatory">*</span></label>
                                <div className="inputTextKeyFucMain">
                                    <input id='category' type="text" className={!this.state.categoryErr ? "onFocus pnl-purchase-input" : "onFocus pnl-purchase-input errorBorder"} placeholder="" value={this.state.categorySearch} onKeyDown={this.categoryData} onChange={this.categorySearchHandler} onFocus={this.openCategoryDropDown} />
                                    <span className="modal-search-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                            <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                        </svg>
                                    </span>
                                    {this.state.selectCategory && 
                                    <div className="dropdown-menu-city1 dropdown-menu-vendor header-dropdown" id="dropDown">
                                        <div className="dropdown-modal-header" id="dropDown">
                                            <span className="div-col-3">Age Group</span>
                                            <span className="div-col-3">Department</span>
                                            <span className="div-col-3">Category</span>
                                        </div>
                                        <ul className="dropdown-menu-city-item" id="dropDown">
                                            {categoryItems}
                                        </ul>
                                        <div className="gen-dropdown-pagination">
                                            <div className="page-close">
                                                <button className="btn-close" type="button" id="btn-close" onClick={this.closeCategoryDropDown}>Close</button>
                                            </div>
                                            <div className="page-next-prew-btn">
                                                {
                                                    catPrevPage <= 0 ? <button className="pnpb-prev" type="button" id="prev" disabled>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                            <path id="chevron_1_" fill="#a7b3c1" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                        </svg>
                                                    </button>:
                                                    <button className="pnpb-prev" type="button" id="prev" onClick={this.prevCategoryHandler}>
                                                         <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={this.prevCategoryHandler}>
                                                            <path id="chevron_1_" fill="#21314b" d="M16.318 8.8a.752.752 0 0 1-1.063 0L8.269 1.815 1.283 8.8A.752.752 0 0 1 .22 7.738L7.738.22A.752.752 0 0 1 8.8.22l7.517 7.518a.752.752 0 0 1 .001 1.062z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                        </svg>
                                                    </button>}
                                                <button className="pnpb-no" type="button" id='li'>{catCurrentPage}/{catMaxPage}</button>
                                                {catCurrentPage >= catMaxPage ? <button className="pnpb-next" type="button"  id="next" disabled>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539">
                                                    <path id="chevron_1_" fill="#a7b3c1" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                </svg>
                                                </button>:
                                                <button className="pnpb-next" type="button"  id="next" onClick={this.nextCategoryHandler}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="9.021" height="14" viewBox="0 0 9.021 16.539" onClick={this.nextCategoryHandler}>
                                                        <path id="chevron_1_" fill="#21314b" d="M16.318.22a.752.752 0 0 0-1.063 0L8.269 7.206 1.283.22A.752.752 0 0 0 .22 1.283L7.738 8.8a.752.752 0 0 0 1.062 0l7.517-7.518A.752.752 0 0 0 16.318.22z" data-name="chevron (1)" transform="rotate(-90 8.27 8.27)"/>
                                                    </svg>
                                                </button>}
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                {this.state.categoryErr && <span className="error">Please Enter Category</span>}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="col-lg-12 pad-0">
                        <div className="create-cat-basic-details">
                            <h3>Basic Details</h3>
                            <div className="nph-switch-btn">
                                <label className="tg-switch">
                                    <input type="checkbox" onChange={this.toggleHandler}/>
                                    <span className="tg-slider tg-round"></span>
                                    {this.state.isBasic ? <span className="ccbd-text">Basic</span>:
                                    <span className="ccbd-text advance">Advance</span>}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 pad-0">
                        <div className="col-lg-6 pad-lft-0">
                            <div className="apt-box">
                                <label>Vendor Barcode <span className="mandatory">*</span></label>
                                <input id='vendorBarcode' type="text" className={this.state.vendorBarErr && 'errorBorder'} value={this.state.barCodeSearch} onChange={this.barCodeSearchHandler}/>
                            </div>
                            {this.state.vendorBarErr && <span className="error">Please Enter Vendoer Barcode</span>}
                        </div>
                        <div className="col-lg-6 pad-lft-0">
                            <div className="apt-box">
                                <label>Item Name <span className="mandatory">*</span></label>
                                <input type="text" className={this.state.itemErr && 'errorBorder'} value={this.state.itemSearch} onChange={this.itemSearchHandler} />
                            </div>
                            {this.state.itemErr && <span className="error">Please Enter Item Name</span>}
                        </div>
                    </div>
                    <div className="col-lg-6 pad-0">
                        <div className="col-lg-3 pad-lft-0">
                            <div className="apt-box">
                                <label>MRP <span className="mandatory">*</span></label>
                                <input type="text" className={this.state.mrpErr && 'errorBorder'} value={this.state.mrpSearch} onChange={this.mrpSearchHandler} />
                            </div>
                            {this.state.mrpErr && <span className="error">Please Enter MRP</span>}
                        </div>
                        <div className="col-lg-3 pad-lft-0">
                            <div className="apt-box">
                                <label>RSP / Rate <span className="mandatory">*</span></label>
                                <input type="text" className={this.state.rateErr && 'errorBorder'} value={this.state.rateSearch} onChange={this.rateSearchHandler} />
                            </div>
                            {this.state.rateErr && !this.state.rateErr1 && <span className="error">Please Enter Rate</span>}
                            {this.state.rateErr && this.state.rateErr1 && <span className="error">Rate can't be greater than MRP</span>}
                        </div>
                        <div className="col-lg-3 pad-lft-0">
                            <div className="apt-box">
                                <label>Min Order Qty</label>
                                <input type="text" value={this.state.minOrderSearch} onChange={this.minOrderSearchHandler} />
                            </div>
                            
                        </div>
                        {!this.state.isBasic && <div className="col-lg-3 pad-lft-0">
                            <div className="apt-box">
                                <label>Available Qty </label>
                                <input type="text" value={this.state.availableQtySearch} onChange={this.availableQtySearchHandler} />
                            </div>
                        </div>}
                    </div>
                </div>
                {!this.state.isBasic && <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="col-lg-5 pad-lft-0">
                        <div className="apt-box">
                            <label>Description </label>
                            <textarea type="text" value={this.state.descriptionSearch} onChange={this.descriptionSearchHandler} ></textarea>
                        </div>
                    </div>
                </div>}
                <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="col-lg-12 pad-0">
                        <div className="create-cat-basic-details">
                            <h3>Attributes</h3>
                        </div>
                    </div>
                    <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Size <span className="mandatory">*</span></label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className={this.state.sizeErr ? "onFocus pnl-purchase-input errorBorder" : "onFocus pnl-purchase-input"} placeholder="" value={this.state.sizeSearch} onKeyDown={this.sizeData} onChange={this.sizeSearchHandler} onFocus={this.openSizeDropDwon} />
                                <span className="modal-search-btn" >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {this.state.selectSize && 
                                <div className="dropdown-menu-city1 gen-width-auto" id="dropDownSize">
                                    <ul className="dropdown-menu-city-item" id="dropDownSize">
                                        {sizeItems}
                                    </ul>
                                </div>}
                            </div>
                            {this.state.sizeErr && <span className="error">Please Enter Size</span>}
                        </div>
                    </div>
                    <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Colour <span className="mandatory">*</span></label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className={this.state.colorErr ? "onFocus pnl-purchase-input errorBorder" : "onFocus pnl-purchase-input"} placeholder="" value={this.state.colorSearch} onKeyDown={this.colorData} onChange={this.colorSearchHandler} onFocus={this.openColorDropDwon}/>
                                <span className="modal-search-btn" >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {this.state.selectColor && 
                                <div className="dropdown-menu-city1 gen-width-auto" id="dropDownColor">
                                    <ul className="dropdown-menu-city-item" id="dropDownColor">
                                        {colorItems}
                                    </ul>
                                </div>}
                            </div>
                            {this.state.colorErr && <span className="error">Please Enter Color</span>}
                        </div>
                    </div>
                    {!this.state.isBasic && <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Pattern </label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className="onFocus pnl-purchase-input" placeholder="" value={this.state.patternSearch} onKeyDown={this.patternData} onChange={this.patternSearchHandler} onFocus={this.openPatternDropDown} />
                                <span className="modal-search-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {this.state.selectPattern && 
                                <div className="dropdown-menu-city1 gen-width-auto" id="dropDownPattern">
                                    <ul className="dropdown-menu-city-item" id="dropDownPattern">
                                        {patternItems}
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>}
                    {!this.state.isBasic && <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Fabric</label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className="onFocus pnl-purchase-input" value={this.state.fabricSearch} placeholder="" onKeyDown={this.materialData} onChange={this.fabricSearchHandler} onFocus={this.openFabricDropDown} />
                                <span className="modal-search-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {this.state.selectMaterial && 
                                <div className="dropdown-menu-city1 gen-width-auto" id="dropDownFabric">
                                    <ul className="dropdown-menu-city-item" id="dropDownFabric">
                                        {fabricItems}
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>}
                    {!this.state.isBasic && <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Neck </label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className="onFocus pnl-purchase-input" placeholder="" value={this.state.neckSearch} onKeyDown={this.neckCollarData} onChange={this.neckSearchHandler} onFocus={this.openNeckDropDown}/>
                                <span className="modal-search-btn" >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {this.state.selectNeckCollar && 
                                <div className="dropdown-menu-city1 gen-width-auto" id="dropDownNeck">
                                    <ul className="dropdown-menu-city-item" id="dropDownNeck">
                                        {neckItems}
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>}
                    {!this.state.isBasic && <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Usages</label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className="onFocus pnl-purchase-input" value={this.state.usagesSearch} placeholder="" onKeyDown={this.occasionData} onChange={this.usagesSearchHandler}  onFocus={this.openUsagesDropDown} />
                                <span className="modal-search-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17.231" viewBox="0 0 17.094 17.231">
                                        <path fill="#a4b9dd" id="iconmonstr-magnifier-2" d="M17.094 15.541l-4.455-4.455a6.99 6.99 0 1 0-1.714 1.666l4.475 4.479 1.69-1.69zM2.049 6.986a4.938 4.938 0 1 1 4.938 4.938 4.943 4.943 0 0 1-4.938-4.938z" className="cls-1" />
                                    </svg>
                                </span>
                                {this.state.selectOccasion && 
                                <div className="dropdown-menu-city1 gen-width-auto" id="dropDownUsages">
                                    <ul className="dropdown-menu-city-item" id="dropDownUsages">
                                        {usagesItems}
                                    </ul>
                                </div>}
                            </div>
                        </div>
                    </div>}
                </div>
                {!this.state.isBasic && <div className="col-lg-12 p-lr-47 m-top-30">
                    <div className="col-lg-12 pad-0">
                        <div className="create-cat-basic-details">
                            <h3>Other Information</h3>
                        </div>
                    </div>
                    <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Brand</label>
                            <div className="inputTextKeyFucMain aptb-input">
                                <input type="text" className="onFocus pnl-purchase-input" placeholder="" value={this.state.brandSearch} onChange={this.brandSearchHandler} />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Season</label>
                            <select value={this.state.seasonSearch} onChange={this.setSeasonHandler}>
                                <option>Please Select</option>
                                {seasonItems}
                            </select>
                        </div>
                    </div>
                    <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>Catalog Type </label>
                            <select value={this.state.catalogueSearch} onChange={this.setCatalogueHandler}>
                                <option>Please Select</option>
                                <option>Global</option>
                                <option>Retailer</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-lg-2 pad-lft-0">
                        <div className="apt-box">
                            <label>HSN </label>
                            <input type="text" value={this.state.hsnSearch} onChange={this.hsnSearchHandler} />
                        </div>
                    </div>
                </div>}
                <div className="col-lg-12 p-lr-47 m-top-30 m-bot-30">
                    <div className="col-lg-7 pad-0">
                        <div className="col-lg-8 pad-lft-0">
                            <div className="apt-box">
                                <label>Upload Images <span className="mandatory">*</span></label>
                                {this.state.isBasic &&<label className={this.state.uploadImgErr ? "aptb-button errorBorder" : "aptb-button"}>
                                    <input type="file" id="fileUpload" multiple="multiple" onChange={this.mainImgUrlHandler} />
                                    <span className="">
                                        <img src={require('../../../assets/addImage.svg')} />
                                    </span>
                                    Add Image
                                </label>}
                                {this.state.mainImgUrl && <div className="apb-multi-image-upload apb-multi-image-upload-overwrite">
                                    <div className="amiu-box">
                                        <span className="amiub-uploaded-img">
                                            <img src={this.state.mainImgUrl} />
                                            <img className="amiubui-close" src={require('../../../assets/close-black.svg')} onClick={this.removeMainImage}/>
                                        </span>
                                    </div>
                                </div>}
                                {this.state.isBasic && this.state.uploadImgErr && <span className="error">Please Upload Image</span>}
                                {!this.state.isBasic &&<button type="button" className={this.state.uploadImgErr ? "aptb-button errorBorder" : "aptb-button"} onClick={(e) => this.openUploadImage(e)}>
                                    <span className="">
                                        <img src={require('../../../assets/addImage.svg')} />
                                    </span>
                                    Add Image
                                </button>}
                                {!this.state.isBasic && this.state.uploadImgErr && <span className="error">Please Upload Image</span>}
                                {this.state.uploadImage && !this.state.isBasic && <div className="apb-multi-image-upload">
                                    <div className="amiu-box">
                                        {this.state.frontImgUrl !== null ?
                                        <span className="amiub-uploaded-img">
                                            <img src={this.state.frontImgUrl} />
                                            <img className="amiubui-close" src={require('../../../assets/close-black.svg')} onClick={this.removeFrontImage}/>
                                        </span>:
                                        <label className="amiub-label">
                                            <input type="file" id="fileUpload" multiple="multiple" onChange={this.frontImgHandler}/>
                                            <span className="apc-add-input">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="frame_1_" width="40" height="40" data-name="frame (1)" viewBox="0 0 50.811 50.811">
                                                    <path fill="#b4c5d6" id="Path_862" d="M50.811 0h-3.97v1.588h2.382v3.176h1.588zm-7.939 0H38.9v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.94 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zM3.176 0H0v1.588h3.176zM1.588 4.764H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm3.97 4.764h-3.97v1.588h3.97zm7.939 0h-3.97v1.588H13.5zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm5.557-.794h-1.587v2.382h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587V12.7h1.588z" class="cls-1" data-name="Path 862"/>
                                                    <g id="Group_2862" data-name="Group 2862" transform="translate(17.464 17.464)">
                                                        <path fill="#6a7385" id="Rectangle_665" d="M0 0H2.382V15.886H0z" class="cls-1" data-name="Rectangle 665" transform="translate(6.751)"/>
                                                        <path fill="#6a7385" id="Rectangle_666" d="M0 0H15.886V2.382H0z" class="cls-1" data-name="Rectangle 666" transform="translate(0 6.751)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </label>} 
                                        <span className="amiub-text">Front</span>
                                    </div>
                                    <div className="amiu-box">
                                    {this.state.backImgUrl !== null ?
                                        <span className="amiub-uploaded-img">
                                            <img src={this.state.backImgUrl} />
                                            <img className="amiubui-close" src={require('../../../assets/close-black.svg')} onClick={this.removeBackImage} />
                                        </span>:
                                        <label className="amiub-label">
                                            <input type="file" id="fileUpload" multiple="multiple" onChange={this.backImgHandler}/>
                                            <span className="apc-add-input">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="frame_1_" width="40" height="40" data-name="frame (1)" viewBox="0 0 50.811 50.811">
                                                    <path fill="#b4c5d6" id="Path_862" d="M50.811 0h-3.97v1.588h2.382v3.176h1.588zm-7.939 0H38.9v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.94 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zM3.176 0H0v1.588h3.176zM1.588 4.764H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm3.97 4.764h-3.97v1.588h3.97zm7.939 0h-3.97v1.588H13.5zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm5.557-.794h-1.587v2.382h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587V12.7h1.588z" class="cls-1" data-name="Path 862"/>
                                                    <g id="Group_2862" data-name="Group 2862" transform="translate(17.464 17.464)">
                                                        <path fill="#6a7385" id="Rectangle_665" d="M0 0H2.382V15.886H0z" class="cls-1" data-name="Rectangle 665" transform="translate(6.751)"/>
                                                        <path fill="#6a7385" id="Rectangle_666" d="M0 0H15.886V2.382H0z" class="cls-1" data-name="Rectangle 666" transform="translate(0 6.751)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </label>}
                                        <span className="amiub-text">Back</span>
                                    </div>
                                    <div className="amiu-box">
                                    {this.state.sideImgUrl !== null ?
                                        <span className="amiub-uploaded-img">
                                            <img src={this.state.sideImgUrl} />
                                            <img className="amiubui-close" src={require('../../../assets/close-black.svg')} onClick={this.removeSideImage}/>
                                        </span>:
                                        <label className="amiub-label">
                                            <input type="file" id="fileUpload" multiple="multiple" onChange={this.sideImgHandler}/>
                                            <span className="apc-add-input">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="frame_1_" width="40" height="40" data-name="frame (1)" viewBox="0 0 50.811 50.811">
                                                    <path fill="#b4c5d6" id="Path_862" d="M50.811 0h-3.97v1.588h2.382v3.176h1.588zm-7.939 0H38.9v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.94 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zM3.176 0H0v1.588h3.176zM1.588 4.764H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm3.97 4.764h-3.97v1.588h3.97zm7.939 0h-3.97v1.588H13.5zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm5.557-.794h-1.587v2.382h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587V12.7h1.588z" class="cls-1" data-name="Path 862"/>
                                                    <g id="Group_2862" data-name="Group 2862" transform="translate(17.464 17.464)">
                                                        <path fill="#6a7385" id="Rectangle_665" d="M0 0H2.382V15.886H0z" class="cls-1" data-name="Rectangle 665" transform="translate(6.751)"/>
                                                        <path fill="#6a7385" id="Rectangle_666" d="M0 0H15.886V2.382H0z" class="cls-1" data-name="Rectangle 666" transform="translate(0 6.751)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </label>}
                                        <span className="amiub-text">Side</span>
                                    </div>
                                    <div className="amiu-box">
                                    {this.state.detailsImgUrl !== null ?
                                        <span className="amiub-uploaded-img">
                                            <img src={this.state.detailsImgUrl} />
                                            <img className="amiubui-close" src={require('../../../assets/close-black.svg')} onClick={this.removeDetailsImage}/>
                                        </span>:
                                        <label className="amiub-label">
                                            <input type="file" id="fileUpload" multiple="multiple" onChange={this.detailsImgHandler}/>
                                            <span className="apc-add-input">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="frame_1_" width="40" height="40" data-name="frame (1)" viewBox="0 0 50.811 50.811">
                                                    <path fill="#6a7385" id="Path_862" d="M50.811 0h-3.97v1.588h2.382v3.176h1.588zm-7.939 0H38.9v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.94 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zM3.176 0H0v1.588h3.176zM1.588 4.764H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm3.97 4.764h-3.97v1.588h3.97zm7.939 0h-3.97v1.588H13.5zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm5.557-.794h-1.587v2.382h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587V12.7h1.588z" class="cls-1" data-name="Path 862"/>
                                                    <g id="Group_2862" data-name="Group 2862" transform="translate(17.464 17.464)">
                                                        <path fill="#6a7385" id="Rectangle_665" d="M0 0H2.382V15.886H0z" class="cls-1" data-name="Rectangle 665" transform="translate(6.751)"/>
                                                        <path fill="#6a7385" id="Rectangle_666" d="M0 0H15.886V2.382H0z" class="cls-1" data-name="Rectangle 666" transform="translate(0 6.751)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </label>}
                                        <span className="amiub-text">Details</span>
                                    </div>
                                    <div className="amiu-box mr0">
                                    {this.state.lookImgUrl !== null ?
                                        <span className="amiub-uploaded-img">
                                            <img src={this.state.lookImgUrl} />
                                            <img className="amiubui-close" src={require('../../../assets/close-black.svg')} onClick={this.removeLookImage}/>
                                        </span>:
                                        <label className="amiub-label">
                                            <input type="file" id="fileUpload" multiple="multiple" onChange={this.lookImgHandler}/>
                                            <span className="apc-add-input">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="frame_1_" width="40" height="40" data-name="frame (1)" viewBox="0 0 50.811 50.811">
                                                    <path fill="#b4c5d6" id="Path_862" d="M50.811 0h-3.97v1.588h2.382v3.176h1.588zm-7.939 0H38.9v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.94 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zm-7.939 0h-3.97v1.588h3.97zM3.176 0H0v1.588h3.176zM1.588 4.764H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm0 7.939H0v3.97h1.588zm3.97 4.764h-3.97v1.588h3.97zm7.939 0h-3.97v1.588H13.5zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm7.939 0h-3.97v1.588h3.97zm5.557-.794h-1.587v2.382h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587v3.97h1.588zm0-7.939h-1.587V12.7h1.588z" class="cls-1" data-name="Path 862"/>
                                                    <g id="Group_2862" data-name="Group 2862" transform="translate(17.464 17.464)">
                                                        <path fill="#6a7385" id="Rectangle_665" d="M0 0H2.382V15.886H0z" class="cls-1" data-name="Rectangle 665" transform="translate(6.751)"/>
                                                        <path fill="#6a7385" id="Rectangle_666" d="M0 0H15.886V2.382H0z" class="cls-1" data-name="Rectangle 666" transform="translate(0 6.751)"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </label>}
                                        <span className="amiub-text">Look</span>
                                    </div>
                                </div>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}
export default AddProduct;