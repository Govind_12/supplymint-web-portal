import React from 'react';

class BulkUploadRejection extends React.Component {
    render(){
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content bulk-upload-rejection-modal">
                    <div className="bum-head">
                        <h3>Preview</h3>
                        <button type="button" className="bum-close-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                            </svg>
                        </button>
                        <div className="burmh-bot">
                            <p>Upload process completed with errors and warnings.</p>
                            <button type="button" className="burm-download">
                                <span className="burm-down-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="7.721" height="9.436" viewBox="0 0 7.721 9.436">
                                        <path id="prefix__iconmonstr-upload-5" fill="#ff4602" d="M6 2.831v4.718H4.926L6.86 9.913l1.935-2.364H7.718V2.831zm-.858-.944h3.434v4.718h2.145L6.86 11.323 3 6.605h2.145z" transform="translate(-3 -1.887)"/>
                                    </svg>
                                </span>
                                Download
                            </button>
                        </div>
                    </div>
                    <div className="burm-body">
                        <div className="burm-body-top">
                            <div className="burmbt-left">
                                <img src={require('../../../assets/error.svg')} />
                                <p>Failure</p>
                            </div>
                            <div className="burmbt-right">
                                <h3>10 Errors</h3>
                                <p>Correct all the errors and try uploading again</p>
                            </div>
                        </div>
                        <div className="burmb-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th><label>Name</label></th>
                                        <th><label>Result</label></th>
                                        <th><label>Errors / Warnings</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                    <tr>
                                        <td><label className="burmt-item">White Shirt[XL]…</label></td>
                                        <td><img src={require('../../../assets/closeRed.svg')} /> <label>Failed to upload</label></td>
                                        <td><img src={require('../../../assets/warningNew.svg')} /> <label>Data format invalid</label></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div className="burm-pegination">
                                <button type="button" className="burmp-prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="5.995" height="9.99" viewBox="0 0 5.995 9.99">
                                        <path id="prefix__Path_520" fill="none" stroke="#12203c" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M0 0l3.581 3.581L7.162 0" data-name="Path 520" transform="rotate(90 1.584 2.998)"/>
                                    </svg>
                                </button>
                                <span className="burmp-numb">01/01</span>
                                <button type="button" className="burmp-next">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="5.995" height="9.99" viewBox="0 0 5.995 9.99">
                                        <path id="prefix__Path_520" fill="none" stroke="#12203c" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M0 0l3.581 3.581L7.162 0" data-name="Path 520" transform="rotate(90 1.584 2.998)"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="burm-footer">
                        <button type="button" >Discard</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default BulkUploadRejection;