import React from 'react';

class ImageView extends React.Component {
    render(){
        return(
            <div className="modal-layout">
                <div className="backdrop modal-backdrop-new"></div>
                    <div className="modal-content image-view-modal">
                        <div className="ipp-image">
                            <div className="ipp-img-top">
                                <img src={require('../../../assets/vendorCatalogue/cat-imgm1.jpg')} />
                            </div>
                            <div className="ipp-img-bottom">
                                <button type="button" className="ippimg-prew">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="7.688" height="13.375" viewBox="0 0 7.688 13.375">
                                        <path id="prefix__Path_377" fill="none" stroke="#d6dce6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l5.274 5.274L16.547 9" data-name="Path 377" transform="rotate(90 9.93 5.344)"/>
                                    </svg>
                                </button>
                                <div className="ippimg-item">
                                    <img src={require('../../../assets/vendorCatalogue/cat-imgp1.jpg')} />
                                </div>
                                <div className="ippimg-item">
                                    <img src={require('../../../assets/vendorCatalogue/cat-imgp2.jpg')} />
                                </div>
                                <div className="ippimg-item">
                                    <img src={require('../../../assets/vendorCatalogue/cat-imgp3.jpg')} />
                                </div>
                                <div className="ippimg-item">
                                    <img src={require('../../../assets/vendorCatalogue/cat-imgp4.jpg')} />
                                </div>
                                <div className="ippimg-item">
                                    <img src={require('../../../assets/vendorCatalogue/cat-imgp5.jpg')} />
                                </div>
                                <button type="button" className="ippimg-next">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="7.688" height="13.375" viewBox="0 0 7.688 13.375">
                                        <path id="prefix__Path_377" fill="none" stroke="#d6dce6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l5.274 5.274L16.547 9" data-name="Path 377" transform="rotate(90 9.93 5.344)"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div className="img-view-close">
                            <button className="ivc-btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.047" height="12.047" viewBox="0 0 12.047 12.047">
                                    <path d="M7.127 6.023l4.691-4.691a.78.78 0 1 0-1.1-1.1L6.023 4.92 1.333.229a.78.78 0 1 0-1.1 1.1L4.92 6.023.229 10.714a.78.78 0 1 0 1.1 1.1l4.694-4.687 4.691 4.691a.78.78 0 1 0 1.1-1.1zm0 0" data-name="close (1)"/>
                                </svg>
                            </button>
                        </div>
                    </div>
            </div>
        )
    }
}
export default ImageView;