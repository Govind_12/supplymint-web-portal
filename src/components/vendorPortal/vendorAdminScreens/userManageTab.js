import React from 'react';
import refreshIcon from '../../../assets/refresh-block.svg'
import searchIcon from '../../../assets/clearSearch.svg';
import SearchImage from '../../../assets/searchicon.svg';
import eyeIcon from "../../../assets/actions-buttons.svg";
import VendorAddUser from './vendorAddUser';
import UserManage from '../../administration/user/userManage';
import VendorEditUser from './vendorEditUser';
import ToastLoader from '../../loaders/toastLoader';
import Pagination from '../../pagination';

export default class UserTabVendor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userFilter: false,
      userModal: false,
      filterBar: false,
      userEditModal: false,
      userId: "",
      userState: this.props.userState,
      prev: "",
      current: "",
      next: "",
      maxPage: "",
      no: 1,
      type: 1,
      search: "",
      firstName: "",
      middleName: "",
      lastName: "",
      //partnerEnterpriseName: "",
      userName: "",
      email: "",
      status: "",
      summaryModalAnimation: false,
      toastLoader: false,
      toastMsg: "",
      addUserModal: false,
      editData: [],
      roleData: [],
      editModalOnRoleSuccess: false,
      jumpPage:1,
      mobile: ""
    }
  }
  componentDidMount() {
    let data = {
      type: 1,
      no: 1,
    }
    this.props.vendorUserRequest(data);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.administration.vendorUser.isSuccess) {
      this.setState({
        userState: nextProps.administration.vendorUser.data.resource,
        prev: nextProps.administration.vendorUser.data.prePage,
        current: nextProps.administration.vendorUser.data.currPage,
        next: nextProps.administration.vendorUser.data.currPage + 1,
        maxPage: nextProps.administration.vendorUser.data.maxPage,
        jumpPage: nextProps.administration.vendorUser.data.currPage,
      })
      this.props.vendorUserClear()
    }

    if (nextProps.administration.allRetailer.isSuccess) {
      this.setState({ roleData: nextProps.administration.allRetailer.data.resource.length > 0 ? nextProps.administration.allRetailer.data.resource : [],
                      userEditModal: this.state.editModalOnRoleSuccess ? true : false,
                      userModalAnimation: this.state.editModalOnRoleSuccess ? true : false
          })
      this.props.allRetailerClear()
    }
    if (nextProps.administration.editUser.isSuccess) {
        let data = {
          type: 1,
          no: 1,
        }
        this.props.vendorUserRequest(data);
    }
  }

  handleSearch(e) {
    this.setState({
      search: e.target.value
    })
  }
  onSearch(e) {
    e.preventDefault();
    if (this.state.search == "") {
      this.setState({
        toastMsg: "Enter text on search input ",
        toastLoader: true
      })
      setTimeout(() => {
        this.setState({
          toastLoader: false
        })
      }, 1500);
    } else {
      this.setState({
        type: 3,
        firstName: "",
        middleName: "",
        lastName: "",
        //partnerEnterpriseName: "",
        userName: "",
        email: "",
        status: "",
        userModalAnimation: false
      })
      let data = {
        type: 3,
        no: 1,
        firstName: "",
        middleName: "",
        lastName: "",
        //partnerEnterpriseName: "",
        userName: "",
        email: "",
        status: "",
        search: this.state.search,
      }
      this.props.vendorUserRequest(data)
    }
  }

  updateFilter(data) {
    this.setState({
      no: data.no,
      type: data.type,
      search: data.search,
      firstName: data.firstName,
      middleName: data.middleName,
      lastName: data.lastName,
      //partnerEnterpriseName: data.partnerEnterpriseName,
      userName: data.userName,
      email: data.email,
      status: data.status
    })
  }

  // onModal(e) {
  //   e.preventDefault();
  //   this.setState({
  //     userModal: !this.state.userModal,
  //     summaryModalAnimation: !this.state.summaryModalAnimation

  //   })
  // }
  onFilter(e) {
    e.preventDefault();
    this.setState({
      userFilter: true,
      filterBar: !this.state.filterBar
    })
  }

  onUserEditModal =(data)=> {
    let userState = this.state.userState.filter( item => item.userId == data.userId)
    this.setState({ editModalOnRoleSuccess: !this.state.editModalOnRoleSuccess},()=>{
      if(!this.state.userEditModal){
        this.props.allRetailerRequest()
        this.setState({ userId: data.userId, editData: userState,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          mobile: data.mobileNumber, 
        })
      }else{
        this.setState({
          userEditModal: !this.state.userEditModal,
          userModalAnimation: !this.state.userModalAnimation,
        })
      }
    })
  }

  deleteUser(id) {
    // const idd = id;
    let data = {
      idd: id,
      no: this.state.current,
      type: this.state.type,
      search: this.state.search,
      firstName: this.state.firstName,
      middleName: this.state.middleName,
      lastName: this.state.lastName,
      //partnerEnterpriseName: this.state.partnerEnterpriseName,
      userName: this.state.userName,
      email: this.state.email,
      status: this.state.status
    }
    this.props.deleteUserRequest(data);

  }
  page =(e)=> {

    if (e.target.id == "prev") {

      if (this.state.current == "" || this.state.current == undefined || this.state.current == 1) {

      } else {
        this.setState({
          prev: this.state.prev,
          current: this.state.current,
          next: this.state.current + 1,
          maxPage: this.state.maxPage,
        })
        if (this.state.current != 0) {
          let data = {
            no: this.state.current - 1,
            type: this.state.type,
            search: this.state.search,
            firstName: this.state.firstName,
            middleName: this.state.middleName,
            lastName: this.state.lastName,
            //partnerEnterpriseName: this.state.partnerEnterpriseName,
            userName: this.state.userName,
            email: this.state.email,
            status: this.state.status
          }
          this.props.vendorUserRequest(data)
        }

      }
    } else if (e.target.id == "next") {
      this.setState({
        prev: this.state.prev,
        current: this.state.current,
        next: this.state.current + 1,
        maxPage: this.state.maxPage,
      })
      if (this.state.current != this.state.maxPage) {
        let data = {
          no: this.state.current + 1,
          type: this.state.type,
          search: this.state.search,
          firstName: this.state.firstName,
          middleName: this.state.middleName,
          lastName: this.state.lastName,
          //partnerEnterpriseName: this.state.partnerEnterpriseName,
          userName: this.state.userName,
          email: this.state.email,
          status: this.state.status
        }
        this.props.vendorUserRequest(data)
      }
    }
    else if (e.target.id == "first") {
      if (this.state.current == 1 || this.state.current == "" || this.state.current == undefined) {

      } else {
        this.setState({
          prev: this.state.prev,
          current: this.state.current,
          next: this.state.current + 1,
          maxPage: this.state.maxPage,
        })
        if (this.state.current <= this.state.maxPage) {
          let data = {
            no: 1,
            type: this.state.type,
            search: this.state.search,
            firstName: this.state.firstName,
            middleName: this.state.middleName,
            lastName: this.state.lastName,
            //partnerEnterpriseName: this.state.partnerEnterpriseName,
            userName: this.state.userName,
            email: this.state.email,
            status: this.state.status
          }
          this.props.vendorUserRequest(data)
        }
      }
    }
    else if (e.target.id == "last") {
      if (this.state.current == this.state.maxPage || this.state.current == undefined) {

      } else {
        this.setState({
          prev: this.state.prev,
          current: this.state.current,
          next: this.state.current + 1,
          maxPage: this.state.maxPage,
        })
        if (this.state.current <= this.state.maxPage) {
          let data = {
            no: this.state.maxPage,
            type: this.state.type,
            search: this.state.search,
            firstName: this.state.firstName,
            middleName: this.state.middleName,
            lastName: this.state.lastName,
            //partnerEnterpriseName: this.state.partnerEnterpriseName,
            userName: this.state.userName,
            email: this.state.email,
            status: this.state.status
          }
          this.props.vendorUserRequest(data)
        }
      }
    }
  }

  onActive(id) {
    let user = this.state.userState;
    for (let i = 0; i < user.length; i++) {
      if (user[i].userId == id) {
        user[i].status = user[i].status == "Active" ? "Inactive" : "Active";
        let data = {
          userName: user[i].userName,
          userId: id,
          active: user[i].status == "Active" ? "true" : "false",
        }
        this.props.userStatusRequest(data)
      }
    }
  }

  onClearSearch(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: ""
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.vendorUserRequest(data);
  }

  onClearFilter(e) {
    e.preventDefault();
    this.setState({
      type: 1,
      no: 1,
      search: "",
      firstName: "",
      middleName: "",
      lastName: "",
      //partnerEnterpriseName: "",
      userName: "",
      email: "",
      status: "",
    })
    let data = {
      type: 1,
      no: 1,
      search: ""
    }
    this.props.vendorUserRequest(data);
  }

  onRefresh =()=> {
    let data = {
      type: 1,
      no: 1,
    }
    this.props.vendorUserRequest(data)
  }

  handleAddUserModal = () => {
    this.setState({
      addUserModal: !this.state.addUserModal,
      userEditModal: false,
    })
  }

  getAnyPage = _ => {
    if (_.target.validity.valid) {
        this.setState({ jumpPage: _.target.value })
        if (_.key == "Enter" && _.target.value != this.state.current) {
            if (_.target.value != "") {
                let data = {
                  no: _.target.value,
                  type: this.state.type,
                  search: this.state.search,
                  firstName: this.state.firstName,
                  middleName: this.state.middleName,
                  lastName: this.state.lastName,
                  //partnerEnterpriseName: this.state.partnerEnterpriseName,
                  userName: this.state.userName,
                  email: this.state.email,
                  status: this.state.status
                }
                this.props.vendorUserRequest(data)
            }
            else {
                this.setState({
                    toastMsg: "Page No should not be empty..",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 3000);
            }
        }
    }
  }

  sendEmail =(e)=>{
    this.setState({
        userEditModal: false,
        editModalOnRoleSuccess: false,
    })
    let data = {
        first: this.state.firstName,
        last: this.state.lastName,
        email: this.state.email,
        mobile: this.state.mobile,
        uType: 'VENDOR',
        additionalUrl: "/send/email"
    }
    this.props.addUserRequest(data); 
  }

  render() {
    const { addUserModal, userEditModal } = this.state
    return (
      <div className="container-fluid pad-0">
            <div className="col-lg-12 pad-0">
                <div className="gen-vendor-potal-design p-lr-47">
                    <div className="col-lg-6 pad-0">
                        <div className="gvpd-left">
                            <div className="gvpd-search">
                                <form onSubmit={(e) => this.onSearch(e)}>
                                    <input type="search" onChange={(e) => this.handleSearch(e)} value={this.state.search} placeholder="Type to Search..." className="search_bar" />
                                    <img className="search-image" src={SearchImage} onClick={(e) => this.onSearch(e)} />
                                    {this.state.type != 3 ? null : <span className="closeSearch" onClick={(e) => this.onClearSearch(e)}><img src={searchIcon} /></span>}
                                </form>
                            </div>
                            {/* <div className="gvpd-filter">
                                <button className={this.state.filter === true ? "gvpd-filter-inner gvpd-filter-inner-focus" : "gvpd-filter-inner"} onClick={(e) => this.onFilter(e)} data-toggle="modal" data-target="#myUserManage">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 15.956 17.407">
                                        <path fill="#12203c" id="prefix__iconmonstr-filter-3" d="M1 0l6.527 10.947v4.283l2.9 2.176v-6.459L16.956 0zm13.4 1.451l-2.16 3.626H5.716L3.554 1.451z" transform="translate(-1)" />
                                    </svg>
                                </button>
                                {this.state.type != 2 ? null : <span className="clearFilterBtn" onClick={(e) => this.onClearFilter(e)} >Clear Filter</span>}
                            </div> */}
                        </div>
                    </div>
                    <div className="col-lg-6 pad-0">
                        <div className="gvpd-right">
                            {/* <div className="tooltip" onClick={this.handleAddUserModal}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 35 35">
                                    <g fill="none" fillRule="evenodd">
                                    <circle cx="17.5" cy="17.5" r="17.5" fill="#8b77fa" />
                                    <path fill="#FFF" d="M25.699 18.64v-1.78H18.64V9.8h-1.78v7.059H9.8v1.78h7.059V25.7h1.78V18.64H25.7" />
                                    </g>
                                </svg>
                            </div> */}
                            <div className="tooltip" onClick={this.handleAddUserModal}>
                                <span className="add-btn"><img src={require('../../../assets/add-green.svg')} />
                                    <span className="generic-tooltip">Add User</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-12 pad-0 p-lr-47">
                <div className="vendor-gen-table">
                    <div className="manage-table">
                        <table className="table gen-main-table">
                            <thead>
                                <tr>
                                    <th className="fix-action-btn">
                                        <ul className="rab-refresh">
                                            {/*<li className="rab-rinner">
                                                <label className="width65">Usage Summary</label>
                                            </li>
                                            <li className="rab-rinner">
                                                <label className="width65 pad-lft-5">Action</label>
                                            </li>*/}
                                            <li className="rab-rinner">
                                                <span><img onClick={() => this.onRefresh()} src={refreshIcon} /></span>
                                            </li>
                                        </ul>
                                    </th>
                                    {/*<th className="positionRelative">
                                        <label>Enterprise</label>
                                          </th>*/}
                                    <th className="positionRelative">
                                        <label>Name</label><img src={require('../../../assets/headerFilter.svg')} />
                                    </th>
                                    <th className="positionRelative">
                                        <label>User Name</label><img src={require('../../../assets/headerFilter.svg')} />
                                    </th>
                                    <th className="positionRelative">
                                        <label>Email</label><img src={require('../../../assets/headerFilter.svg')} />
                                    </th>

                                    <th className="positionRelative">
                                        <label> Mobile</label><img src={require('../../../assets/headerFilter.svg')} />
                                    </th>
                                    <th className="positionRelative">
                                        <label> Access Mode</label><img src={require('../../../assets/headerFilter.svg')} />
                                    </th>
                                    <th className="positionRelative">
                                        <label>Status</label><img src={require('../../../assets/headerFilter.svg')} />
                                    </th>

                                </tr>
                            </thead>

                            <tbody>
                                {this.state.userState == null ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.userState.length == 0 ? <tr className="tableNoData"><td colSpan="8"> NO DATA FOUND </td></tr> : this.state.userState.map((data, key) => (
                                <tr key={key}>
                            <td className="fix-action-btn user-fix-action-btn">
                                {/*<li className="til-inner">
                                    <button onClick={(e) => this.onModal(e)} className="btnSummary">View Summary</button>
                                </li>*/}
                                <ul className="table-item-list">
                                    <li className="til-inner til-edit-btn" onClick={() => this.onUserEditModal(data)}>
                                        {/* <button className="edit_button" >
                                            <img src={require('../../../assets/edit.svg')} />
                                        </button> */}
                                        <svg xmlns="http://www.w3.org/2000/svg" id="prefix__edit-tools_2_" width="16" height="16" data-name="edit-tools (2)" viewBox="0 0 16.298 16.211">
                                            <path fill="#a4b9dd" id="prefix__Path_587" d="M15.831 28.215a.427.427 0 0 0-.427.427v3.793a1.283 1.283 0 0 1-1.282 1.282H2.136a1.283 1.283 0 0 1-1.282-1.281V21.3a1.283 1.283 0 0 1 1.282-1.282h3.793a.427.427 0 0 0 0-.854H2.136A2.138 2.138 0 0 0 0 21.3v11.136a2.138 2.138 0 0 0 2.136 2.136h11.986a2.138 2.138 0 0 0 2.136-2.136v-3.793a.427.427 0 0 0-.427-.428zm0 0" className="prefix__cls-1" data-name="Path 587" transform="translate(0 -18.361)" />
                                            <path fill="#a4b9dd" id="prefix__Path_588" d="M156.367 38.566l6.237-6.237 2.012 2.012-6.237 6.237zm0 0" className="prefix__cls-1" data-name="Path 588" transform="translate(-149.688 -30.959)" />
                                            <path fill="#a4b9dd" id="prefix__Path_589" d="M132.543 199.442l2.223-.616-1.607-1.607zm0 0" className="prefix__cls-1" data-name="Path 589" transform="translate(-126.881 -188.806)" />
                                            <path fill="#a4b9dd" id="prefix__Path_590" d="M318.491.581a1.069 1.069 0 0 0-1.51 0l-.453.453 2.012 2.012.453-.453a1.069 1.069 0 0 0 0-1.51zm0 0" className="prefix__cls-1" data-name="Path 590" transform="translate(-303.006 -.269)" />
                                        </svg>
                                        <span className="generic-tooltip">Edit</span>
                                    </li>
                                    <li className={data.status == "Active" ? "addToggleSwitch mainToggle" : "mainToggle"}>
                                    <label className="switchToggle">
                                        <input onClick={(e) => this.onActive(`${data.userId}`)} type="checkbox" id="myDiv" />
                                        <span className="sliderToggle round">
                                        </span>
                                    </label>
                                    {/* <p className="onActive">Active</p>
                                    <p className="onInActive">Inactive</p> */}
                                    </li>
                                </ul>
                            </td>
                            {/*<td>
                                <label>
                                    {data.partnerEnterpriseName}
                              </label>
                            </td>*/}
                            <td>
                                <label>
                                    {data.firstName} {data.middleName} {data.lastName}
                                </label>
                            </td>
                            <td>
                                <label>
                                    {data.userName}
                                </label>
                            </td>

                            <td>
                              <label className="maxwidth200">
                                {data.email}
                              </label>
                            </td>

                            <td>
                                <label>
                                    {data.mobileNumber}
                                </label>
                            </td>
                            <td>
                                <label>
                                    {data.accessMode}
                                </label>
                            </td>
                            <td>
                                <label>
                                    {data.status}
                                </label>
                            </td>
                          </tr>))}
                      </tbody>
                    </table>
                  </div>
              </div>

                <div className="col-md-12 pad-0" >
                    <div className="new-gen-pagination">
                        <div className="ngp-left">
                            <div className="table-page-no">
                                <span>Page :</span><input type="number" className="paginationBorder" max={this.state.maxPage} min="1" onKeyPress={this.getAnyPage} onChange={this.getAnyPage} value={this.state.jumpPage} />
                                {/* <span className="ngp-total-item">Total Items </span> <span className="bold">{this.state.totalPendingPo}</span> */}
                            </div>
                        </div>
                        <div className="ngp-right">
                            <div className="nt-btn">
                                <Pagination {...this.state} {...this.props} page={this.page}
                                    prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {this.state.userFilter ? <UserFilter
                firstName={this.state.firstName}
                middleName={this.state.middleName}
                lastName={this.state.lastName}
                //partnerEnterpriseName={this.state.partnerEnterpriseName}
                userName={this.state.userName}
                email={this.state.email}
                status={this.state.status}
                updateFilter={(e) => this.updateFilter(e)} {...this.props} filterBar={this.state.filterBar} filter={(e) => this.onFilter(e)} /> : null}
            {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            {/* {this.state.userModal ? <UserModal summaryModalAnimation={this.state.summaryModalAnimation} modal={(e) => this.onModal(e)} /> : null} */}
            {addUserModal && <VendorAddUser {...this.props} handleAddUserModal={this.handleAddUserModal} />}
            {userEditModal ? <VendorEditUser {...this.state} {...this.props} userId={this.state.userId} userModalAnimation={this.state.userModalAnimation} userEditModal={(e) => this.onUserEditModal(e)} editData={this.state.editData} roleData={this.state.roleData} sendEmail={this.sendEmail}/> : null}
        </div>
        )
    }
}