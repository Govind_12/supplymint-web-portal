import React from "react";

class VendorEditUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first: this.props.editData.length > 0 ? this.props.editData[0].firstName : "",
            middle: "",
            last: this.props.editData.length > 0 ? this.props.editData[0].lastName : "",
            user: this.props.editData.length > 0 ? this.props.editData[0].userName : "",
            email: this.props.editData.length > 0 ? this.props.editData[0].email : "",
            mobile: this.props.editData.length > 0 ? this.props.editData[0].mobileNumber : "",
            active: this.props.editData.length > 0 ? this.props.editData[0].status : "",
            phone: "",
            access: "",
            address: "",
            userId: this.props.editData.length > 0 ? this.props.editData[0].userId : "",
            partnerEnterpriseName: "",
            partnerEnterpriseId: "",
            firsterr: false,
            middleerr: false,
            lasterr: false,
            usererr: false,
            emailerr: false,
            mobileerr: false,
            activeerr: false,
            phoneerr: false,
            accesserr: false,
            addresserr: false,
            // data: [
            //     { "id": 0, "text": "Administartion" },
            //     { "id": 1, "text": "Manage Role" },
            //     { "id": 2, "text": "Admin" },
            //     { "id": 3, "text": "Manage Site" },
            //     { "id": 4, "text": "Site Mapping" },
            //     { "id": 5, "text": "Site" }
            // ],
            // selected: this.props.editData.length > 0 ? this.props.editData[0].userRoles : [],
            data: this.props.roleData,
            selected: [],
            selectederr: "",
            current: "",
            open: false,
            search: "",
            btnDisable: true

        };
    }

    componentWillMount(){
        this.props.editData.length > 0 && this.props.editData[0].retailers !== null && this.props.editData[0].retailers !== undefined
            && this.props.editData[0].retailers.length > 0 &&
            this.props.editData[0].retailers.split("|").map( data =>{
                data !== "null" && data !== null && this.onnChange(Number(data));
            })  
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.administration.editUser.isSuccess) {
            this.props.userEditModal()
            this.props.editUserClear();
        }
    }

    onClear(e) {
        e.preventDefault();
        this.setState({
            first: "",
            middle: "",
            last: "",
            user: "",
            email: "",
            mobile: "",
            active: "",
            phone: "",
            access: "",
            address: "",
            selected: []
        })
    }

    onDelete =(e)=> {
        let match = this.state.selected.filter( data => data.id == e )
        this.setState(preState=>({
            data: preState.data.concat(match)
        }));

        var array = this.state.selected;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === e) {
                array.splice(i, 1);
            }
        }
        this.setState({
            selected: array,
            //open: false,
        },()=>this.selected())


    }
    Change(e) {
        this.setState({
            search: e.target.value
        })
    }

    onnChange =(e)=> {

        //e.preventDefault();
        let value = e.target !== undefined ? e.target.value : e
        this.setState({
            current: value,
        })
        let match = this.state.data.filter( data => data.id == value).length > 0 
                    ? this.state.data.filter( data => data.id == value)
                    : {};

        if( match[0] !== undefined ){
            let modifiedData = {
                id: match[0].id,
                entorgid: match[0].entorgid,
                name: match[0].name 
            }
            this.setState(prevState=>({
                selected: prevState.selected.concat(modifiedData),
            }),()=> this.selected());
        }
        let idd = this.state.data.filter( data => data.id == value)[0] !== undefined ? this.state.data.filter( data => data.id == value)[0].id
                  : "";
        var array = this.state.data;
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === idd) {
                array.splice(i, 1);
            }
            else {

            }
        }
        this.setState({
            data: array,
            selectederr: false,
            //open: false,
        })

    }

    handleChange(e) {
        e.preventDefault();
        this.setState({
            btnDisable: false
        })
        if (e.target.id == "first") {
            this.setState(
                {
                    first: e.target.value
                },
                () => {
                    this.first();
                }
            );
        } else if (e.target.id == "last") {
            this.setState(
                {
                    last: e.target.value
                },
                () => {
                    this.last();
                }
            );
        } 
        // else if (e.target.id == "middle") {
        //     this.setState(
        //         {
        //             middle: e.target.value
        //         },
        //         () => {
        //             this.middle();
        //         }
        //     );
        // } 
        else if (e.target.id == "email") {
            this.setState(
                {
                    email: e.target.value
                },
                () => {
                    this.email();
                }
            );
        } else if (e.target.id == "mobile") {
            this.setState(
                {
                    mobile: e.target.value
                },
                () => {
                    this.mobile();
                }
            );
        } 
        // else if (e.target.id == "phone") {
        //     this.setState(
        //         {
        //             phone: e.target.value
        //         },
        //         () => {
        //             this.phone();
        //         }
        //     );
        // } 
        // else if (e.target.id == "access") {
        //     this.setState(
        //         {
        //             access: e.target.value
        //         },
        //         () => {
        //             // this.access();
        //         }
        //     );
        // } 
        // else if (e.target.id == "address") {
        //     this.setState(
        //         {
        //             address: e.target.value
        //         },
        //         () => {
        //             // this.address();
        //         }
        //     );
        // } 
        else if (e.target.id == "active") {
            this.setState(
                {
                    active: e.target.value
                },
                () => {
                    this.active();
                }
            );
        } else if (e.target.id == "user") {
            this.setState(
                {
                    user: e.target.value
                },
                () => {
                    this.user();
                }
            );
        }
    }
    first() {
        if (
            this.state.first == "" || !this.state.first.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                firsterr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                firsterr: false
            });
        }
    }
    // middle() {
    //     if (this.state.middle == "") {
    //         this.setState({
    //             middleerr: false
    //         })
    //     } else if (!this.state.middle.match(/^[a-zA-Z ]+$/)) {
    //         this.setState({
    //             middleerr: true
    //         });
    //     } else {
    //         this.setState({
    //             middleerr: false
    //         });
    //     }
    // }
    active() {
        if (
            this.state.active == "") {
            this.setState({
                activeerr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                activeerr: false
            });
        }
    }
    // access() {
    //     if (
    //         this.state.access == "") {
    //         this.setState({
    //             accesserr: true
    //         });
    //     } else {
    //         this.setState({
    //             accesserr: false
    //         });
    //     }
    // }
    email() {
        if (
            this.state.email == "" || !this.state.email.match(
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )) {
            this.setState({
                emailerr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                emailerr: false
            });
        }
    }

    // phone() {
    //     if (this.state.phone == "") {
    //         this.setState({
    //             phoneerr: false
    //         })
    //     }
    //     else if (this.state.phone.length < 10) {
    //         this.setState({
    //             phoneerr: true
    //         });
    //     }
    //     else {
    //         this.setState({
    //             phoneerr: false
    //         });
    //     }
    // }
    mobile() {
        if (
            this.state.mobile == "" || !this.state.mobile.match(/^\d{10}$/) ||
            !this.state.mobile.match(/^[1-9]\d+$/) ||
            !this.state.mobile.match(/^.{10}$/)) {
            this.setState({
                mobileerr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                mobileerr: false
            });
        }
    }
    // address() {
    //     if (
    //         this.state.address == "") {
    //         this.setState({
    //             addresserr: true
    //         });
    //     } else {
    //         this.setState({
    //             addresserr: false
    //         });
    //     }
    // }
    user() {
        if (
            this.state.user == "" || this.state.user.trim() == "" || !this.state.user.match(/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/)) {
            this.setState({
                usererr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                usererr: false
            });
        }
    }
    last() {
        if (
            this.state.last == "" || !this.state.last.match(/^[a-zA-Z ]+$/)) {
            this.setState({
                lasterr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                lasterr: false
            });
        }
    }
    selected() {
        if (this.state.selected.length == 0) {
            this.setState({
                selectederr: true,
                btnDisable: true
            });
        } else {
            this.setState({
                selectederr: false,
                btnDisable: false
            })
        }
    }
    openClose(e) {
        e.preventDefault();
        this.setState({
            open: !this.state.open
        },()=>{
            if(this.state.open)
             document.addEventListener("click", this.closeRoleModal)
            else
              document.removeEventListener("click", this.closeRoleModal)
        })
    }

    closeRoleModal =(e)=>{
        if( e !== null && e.target !== undefined && !e.target.parentElement.className.includes("dropdownDiv")){
            this.setState({ open: false},()=>{
               document.removeEventListener("click", this.closeRoleModal)
            })
        }
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.first();
        this.last();
        // this.middle();
        this.email();
        //this.phone();
        this.mobile();
        // this.access();
        this.active();
        // this.address();
        this.selected();
        //this.user();
        const t = this;
        setTimeout(function () {
            const { userId, selected, first, firsterr, last, lasterr, email, emailerr, mobile, mobileerr, active, activeerr, user, usererr, selectederr } = t.state;
            if (!selectederr && !firsterr && !lasterr && !emailerr && !mobileerr && !activeerr && !usererr) {
                // t.props.saveUser();
                let userData = {
                    //partnerEnterpriseId: partnerEnterpriseId,
                    //partnerEnterpriseNameE: partnerEnterpriseName,
                    userId: userId,
                    firstNameE: first,
                    //middleNameE: middle,
                    lastNameE: last,
                    emailE: email,
                    mobileNumber: mobile,
                    //workPhone: phone,
                    //accessMode: access,
                    active: active,
                    //address: address,
                    userNameE: user,
                    selected: selected,
                    no: t.props.no,
                    type: t.props.type,
                    search: t.props.search,
                    firstName: t.props.firstName,
                   // middleName: t.props.middleName,
                    lastName: t.props.lastName,
                    //partnerEnterpriseName: t.props.partnerEnterpriseName,
                    userName: t.props.userName,
                    email: t.props.email,
                    status: t.props.status,
                    uType:  sessionStorage.getItem('uType'),
                    newUserName: "",
                    newPassword: "",
                    userAsAdmin: "false"
                }
                t.props.editUserRequest(userData);

            }
        }, 100)
    }

    render() {

        $("body").on("keyup", ".numbersOnly", function () {
            if (this.value != this.value.replace(/[^0-9]/g, '')) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        $("body").on("keyup", ".numbersOnlywithhighpn", function () {
            if (this.value != this.value.replace(/[^0-9-]/g, '')) {
                this.value = this.value.replace(/[^0-9-]/g, '');
            }
        });
        const { search, first, firsterr, last, lasterr, email, emailerr, mobile, mobileerr, active, activeerr, user, usererr, selectederr } = this.state;
        return (
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content add-vendor-user vendor-user-edit-modal">
                    <form onSubmit={(e) => this.onFormSubmit(e)}>
                        <div className="avu-head">
                            <div className="avuh-left">
                                <h3>Manage User</h3>
                            </div>
                            <div className="avuh-right">
                                <button className="cancelBtn" type="button" onClick={(e) => this.props.sendEmail(e)}>Send Email</button>
                                <button className="cancelBtn" type="button" onClick={(e) => this.props.userEditModal(e)}>Close</button>
                                {this.state.btnDisable ? <button type="button" className="btnDisabled save-button-vendor" >Update</button> : 
                                <button className="save-button-vendor" type="submit">Update</button>}
                            </div>
                        </div>
                        <div className="avu-body">
                            <div className="col-md-12 pad-0">
                                <ul className="avu-choose-retailer">
                                    <h3>Basic Details</h3>
                                </ul>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">
                                <div className="col-md-4 col-sm-4 col-xs-4 pad-lft-0">
                                    <label>First Name</label>
                                    <input type="text" maxlength="80" className={firsterr ? "form-box errorBorder" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} value={first} id="first" />
                                    {/* {firsterr ? <img src={errorIcon} className="error_icon_purchase" /> : null} */}
                                    {firsterr ? (
                                        <span className="error">
                                            Enter first name
                                            </span>
                                    ) : null}
                                </div>
                                {/* <div className="col-md-3 col-sm-2 col-xs-3 pad-lft-0">
                                    <label className="editModalLabel">
                                        Middle Name
                                    </label>
                                    <input type="text" className={middleerr ? "errorBorder InputBoxEdit" : "InputBoxEdit"} onChange={(e) => this.handleChange(e)} id="middle" value={middle} placeholder="Middle Name" />
                                    {/* {middleerr ? <img src={errorIcon} className="error_icon_purchase" /> : null} 
                                    {middleerr ? (
                                        <span className="error">
                                            Enter middle name
                                        </span>
                                    ) : null}
                                </div> */}
                                <div className="col-md-4 col-sm-4 col-xs-4 pad-lft-0">
                                    <label>
                                        Last Name
                                    </label>
                                    <input type="text" maxlength="80" className={lasterr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} id="last" value={last} placeholder="Last Name" />
                                    {/* {lasterr ? <img src={errorIcon} className="error_icon_purchase" /> : null} */}
                                    {lasterr ? (
                                        <span className="error">
                                            Enter last name
                                            </span>
                                    ) : null}
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-4 pad-lft-0">
                                    <label>
                                        User Name
                                    </label>
                                    <input type="text" maxlength="90" className={usererr ? "errorBorder form-box" : "onFocus form-box"} id="user" onChange={(e) => this.handleChange(e)} value={user} placeholder="Username" disabled />
                                    {/* {usererr ? <img src={errorIcon} className="error_icon_purchase" /> : null} */}
                                    {usererr ? (
                                        <span className="error">
                                            Enter username
                                        </span>
                                    ) : null}
                                </div>

                            </div>

                            <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-30">

                                <div className="col-md-4 col-sm-4 col-xs-4 pad-lft-0">
                                    <label>
                                        Email
                                    </label>
                                    <input type="text" maxlength="200" className={emailerr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} id="email" value={email} placeholder="Email" />
                                    {/* {emailerr ? <img src={errorIcon} className="error_icon_purchase" /> : null} */}
                                    {emailerr ? (
                                        <span className="error">
                                            Enter valid email
                                        </span>
                                    ) : null}
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-4 pad-lft-0">
                                    <label>
                                        Mobile Number
                                    </label>
                                    <input type="text" className={mobileerr ? "errorBorder form-box numbersOnly" : "onFocus form-box numbersOnly"} maxLength="10" onChange={(e) => this.handleChange(e)} id="mobile" value={mobile} placeholder="Mobile Number" />
                                    {/* {mobileerr ? <img src={errorIcon} className="error_icon_purchase" /> : null} */}
                                    {mobileerr ? (
                                        <span className="error">
                                            Enter valid mobile number
                                        </span>
                                    ) : null}
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-4 pad-lft-0">
                                    <label>
                                        Status
                                    </label>
                                    <select className={activeerr ? "errorBorder form-box" : "onFocus form-box"} onChange={(e) => this.handleChange(e)} value={active} id="active" placeholder="Active">
                                        <option value="" >Status</option>
                                        <option value="Active" >Active</option>
                                        <option value="Inactive" >Inactive</option>
                                    </select>
                                    {/* {activeerr ? <img src={errorIcon} className="error_icon_purchase" /> : null} */}
                                    {activeerr ? (
                                        <span className="error">
                                            Select active
                                        </span>
                                    ) : null}
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 pad-0 m-top-20">
                                    <ul className="avu-choose-retailer">
                                        <h3>Choose Retailers</h3>
                                        <p>You can choose multiple retailers</p>
                                    </ul>
                                    <div className="col-md-12 col-sm-12 pad-0">
                                        <ul className="list-inline m-top-10 width_100 m-bot-10">
                                            <li>
                                                {this.state.selected.length == 0 ? null : this.state.selected.map((data, i) => <div key={data.id} className="role-user-name">
                                                    <span className="role-user-name-inner">{data.name}</span>
                                                    <span onClick={(e) => this.onDelete(data.id)} value={data.id} className="roleun-close" aria-hidden="true">&times;</span>
                                                </div>)}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-2 col-xs-12 pad-0 m-rgt-20">
                                        <div className="userModalSelect" onClick={(e) => this.openClose(e)}>
                                            <label>Select Retailer</label>
                                            {selectederr ? <span className="error"> Select Retailer</span> : null}
                                        </div>
                                        {this.state.open ? <div className="dropdownDiv m-top-10">
                                            {/* <input type="search" className="searchFilterModal" value={search} onChange={(e) => this.Change(e)} /> */}
                                            <ul className="dropdownFilterOption">
                                                {this.state.data.map((data, i) => 
                                                    <li value={data.id} key={data.id} onClick={(e) => this.onnChange(e)} >{data.name}</li>)}
                                            </ul>
                                        </div> : null}
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default VendorEditUser;
