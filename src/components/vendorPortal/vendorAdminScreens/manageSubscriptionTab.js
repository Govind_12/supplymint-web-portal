import React, { Component } from 'react'
    // import NewSideBar from '../../components/newSidebar';
import GreenCheck from '../../../assets/green-check.svg';
import FilterIcon from '../../../assets/filter-icon.svg';
import Pagination from '../../pagination';


export default class ManageSubscriptionTab extends Component {
    constructor(props) {
        super(props)
        this.state = {
            subscriptionData: [],
            transactionData: [],
        }
    }

    componentDidMount(){
        this.props.getSubscriptionRequest('asd');
        this.props.getTransactionRequest('asd');
    }

    static getDerivedStateFromProps(nextProps, preState){
        if (nextProps.administration.getSubscription.isSuccess) {
            return {
                subscriptionData: nextProps.administration.getSubscription.data.resource !== null ? nextProps.administration.getSubscription.data.resource[0] : [] 
            }
        }
        if (nextProps.administration.getTransaction.isSuccess) {
            return {
                transactionData: nextProps.administration.getTransaction.data.resource !== null ? nextProps.administration.getTransaction.data.resource[0] : [] 
            }
        }
        return null;
    }

    componentDidUpdate(){

    }

    render() {
        const{ subscriptionData, transactionData} = this.state;
        return (
            <div className="container-fluid pad-0" >
                <div className="container-fluid pad-0">
                    <div className="col-lg-12 pad-0">
                        <div className="subscription-tab">
                            <ul className="nav nav-tabs subscription-tab-list p-lr-47" role="tablist">
                                <li key="setmodalcatdesc" className="nav-item active" >
                                    <a className="nav-link st-btn" href="#setmodalcatdesc" role="tab" data-toggle="tab">Plan</a>
                                </li>
                                <li key="setmodalitemmudf" className="nav-item">
                                    <a className="nav-link st-btn" href="#setmodalitemmudf" role="tab" data-toggle="tab">All Transactions</a>
                                </li>
                            </ul>
                        </div>
                        <div className="tab-content p-lr-47">
                            <div className="tab-pane fade new-generic-table subscription-table in active" id="setmodalcatdesc" role="tabpanel">
                                <div className="m-top-20 zui-wrapper pnt-bxsha">
                                    <table className="table scrollTable zui-table pit-new-design">
                                        <thead>
                                            <tr>
                                                <th><label>Plan</label></th>
                                                <th><label>Activation Date</label></th>
                                                <th><label>Subscription Expiry</label></th>
                                                <th><label>No of Users</label></th>
                                                <th><label>Status</label></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { subscriptionData.length ? subscriptionData.map( (data, key) => <tr key={key}>
                                                <td><label className="one-year-stand-plan">{data.company_name}<img src={GreenCheck} /></label></td>
                                                <td><label>{data.subs_activation_date}</label></td>
                                                <td><label>{data.subs_expiry_date}</label></td>
                                                <td><label>{data.no_of_users_subscribed}</label></td>
                                                <td><label>{data.status}</label></td>
                                                <td>
                                                    <button>Add License</button>
                                                    <button className="renew-subscription-btn">Renew Subscription</button>
                                                    <button className="upgrade-plan-btn">Upgrade Plan</button>
                                                </td>
                                            </tr>) :
                                            <tr><td>No Data Found</td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="tab-pane fade new-generic-table subscription-table" id="setmodalitemmudf" role="tabpanel">
                                <div className="po-new-search">
                                    <div className="pons-right">
                                        {/* <button className="filter-btn"><img src={FilterIcon} /> <span>3</span></button> */}
                                        <input type="search" placeholder="Type To Search" />
                                    </div>
                                </div>
                                <div className="vendor-gen-table">
                                    <div className="manage-table">
                                        <div className="columnFilterGeneric">
                                            <span className="glyphicon glyphicon-menu-left"></span>
                                        </div>
                                        <table className="table gen-main-table">
                                            <thead>
                                                <tr>
                                                    <th><label># Transaction Number</label></th>
                                                    <th><label>Method</label></th>
                                                    <th><label>Transaction Date</label></th>
                                                    <th><label>Amount</label></th>
                                                    <th><label>Status</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                { transactionData.length ? transactionData.map( (data,key) => <tr key={key}>
                                                    <td><label className="bold">{data.payment_id}</label></td>
                                                    <td><label>{data.method}</label></td>
                                                    <td><label>{data.creation_time}</label></td>
                                                    <td><label>{data.amount}</label></td>
                                                    <td><label className="bold">{data.status}</label></td>
                                                </tr>) :
                                                <tr><td>No Data Found</td></tr>}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="col-md-12 pad-0">
                                    <div className="new-gen-pagination">
                                        <div className="ngp-left">
                                            <div className="table-page-no">
                                                <span>Page :</span><label className="paginationBorder">01</label>
                                            </div>
                                        </div>
                                        <div className="ngp-right">
                                            <div className="nt-btn">
                                                <Pagination />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}