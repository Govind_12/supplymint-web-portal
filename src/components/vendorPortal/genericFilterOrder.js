import React from "react";

export default class GenericFilterOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // type: 1,
            // no: 1,
            // poNumber: this.props.poNumber,
            // status: this.props.status,
            // vendorName: this.props.vendorName,
            // vendorId: this.props.vendorId,
            // vendorCity: this.props.vendorCity,
            // vendorState: this.props.vendorState,
            // poDate: this.props.poDate,
            // userType: this.props.userType,
            // validTo: this.props.validTo,
            // siteDetail: this.props.siteDetail,
            count: this.props.filterCount,
            // orderCode : this.props.orderCode
        };
    }
    // clearFilter(count) {
    //     this.setState({
    //         poNumber: "",
    //         status: "",
    //         vendorName: "",
    //         vendorId: "",
    //         vendorCity: "",
    //         vendorState: "",
    //         poDate: "",
    //         validTo: "",
    //         siteDetail: "",
    //         count: 0,
    //         orderCode : ""
    //     })
    //     this.props.filterCount != 0 ? this.props.clearFilter() : null
    // }

    // onSubmit(count) {
    //     // e.preventDefault();
    //     let data = {
    //         no: 1,
    //         type: 2,
    //         poNumber: this.state.poNumber,
    //         status: this.state.status,
    //         vendorName: this.state.vendorName,
    //         vendorId: this.state.vendorId,
    //         vendorCity: this.state.vendorCity,
    //         poDate: this.state.poDate,
    //         vendorState: this.state.vendorState,
    //         validTo: this.state.validTo,
    //         siteDetail: this.state.siteDetail,
    //         userType: this.props.userType,
    //         filterCount: count,
    //         orderCode : this.state.orderCode
    //     }
    //     this.props.EnterpriseApprovedPoRequest(data);
    //     this.props.closeFilter();
    //     this.props.updateFilter(data)
    // }

    render() {
        let count = 0;

        if (this.props.poNumber != "" ) {
            count++;
        }
        if (this.props.orderCode != "") {
            count++
        }
        if (this.props.poQty != "") {
            count++
        }
        if (this.props.totalCancelledQty != "") {
            count++
        }
        if (this.props.totalPendingQty != "") {
            count++;
        }
        if (this.props.shipmentRequestedQty != "") {
            count++;
        }
        if (this.props.shipmentConfirmedQty != "") {
            count++;
        }
        if (this.props.shipmentIntransitQty != "") {
            count++;
        }
        if (this.props.shipmentIntransitQty != "") {
            count++;
        }
        if (this.props.poDate != "") {
            count++;
        }
        return (

            <div className={this.props.filterBar ? "modal fade display_block" : "display_none"} id="myOrganisationModal" role="dialog" data-backdrop="static">
                <div className={this.props.filterBar ? "backdrop display_block" : "display_none"}></div>
                <div className={this.props.filterBar ? "modal-content modal_content_filter vendorFilterShow" : " vendorFilterHide"}>
                    <form onSubmit={() => this.props.onSubmitFilter(count)}>
                        <button type="button" onClick={(e) => this.props.closeFilter(e)} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <div className="col-md-12 col-sm-12 pad-0">
                            <ul className="list-inline">
                                <li>
                                    <label className="filter_modal">
                                        FILTERS
                                     </label>
                                </li>
                                <li>
                                    <label className="filter_text">
                                        {count} Filters applied
                                     </label>
                                </li>
                            </ul>
                        </div>
                        {this.props.processedOrders && <div className="col-md-12 m0">
                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="container_modal">
                                    <ul className="list-inline m-top-20">
                                        <li>
                                            <input type="text" onChange={this.props.handleFilterChange} id="poNumber" name="poNumber" value={this.props.poNumber} placeholder="PO Number" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={this.props.handleFilterChange} id="orderCode" name="orderCode" value={this.props.orderCode} placeholder="Order Code" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={this.props.handleFilterChange} id="poQty" name="poQty" value={this.props.poQty} placeholder="Po Qty" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={this.props.handleFilterChange} id="totalCancelledQty" name="totalCancelledQty" value={this.props.totalCancelledQty} placeholder="Total Cancelled Qty" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={this.props.handleFilterChange} id="totalPendingQty" name="totalPendingQty" value={this.props.totalPendingQty} placeholder="Total Pending Qty" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="date" onChange={(e) => this.props.handleFilterChange(e)} id="poDate" name="poDate" value={this.props.poDate} placeholder={this.props.poDate != "" ? this.props.poDate : "PO Date"} className="organistionFilterModal" />
                                        </li>
                                    </ul></div>
                            </div>

                            <div className="col-md-12 col-sm-12 pad-0">
                                <div className="container_modal">
                                    <ul className="list-inline m-top-20">
                                        <li>
                                            <input type="text" onChange={(e) => this.props.handleFilterChange(e)} id="shipmentRequestedQty" name="shipmentRequestedQty" value={this.props.shipmentRequestedQty} placeholder="Shipment Requested Qty" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={(e) => this.props.handleFilterChange(e)} id="shipmentConfirmedQty" name="shipmentConfirmedQty" value={this.props.shipmentConfirmedQty} placeholder="Shipment Confirmed Qty" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={(e) => this.props.handleFilterChange(e)} id="shipmentIntransitQty" name="shipmentIntransitQty" value={this.props.shipmentIntransitQty} placeholder="Shipment Intransit Qty" className="organistionFilterModal" />
                                        </li>
                                        <li>
                                            <input type="text" onChange={(e) => this.props.handleFilterChange(e)} id="shipmentDeliveredQty" name="shipmentDeliveredQty" value={this.props.shipmentDeliveredQty} placeholder="Shipment Delivered Qty" className="organistionFilterModal" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>}
                        <div className="col-md-12 col-sm-12 pad-0">
                            <div className="col-md-6 float_right pad-0 m-top-20">
                                <ul className="list-inline text_align_right">
                                    <li>
                                        {this.props.shipmentRequestedQty == "" && this.props.orderCode == "" && this.props.poNumber == "" && this.props.shipmentDeliveredQty == "" && this.props.shipmentIntransitQty == "" && this.props.shipmentConfirmedQty == "" && this.props.poDate == "" && this.props.totalPendingQty == "" && this.props.totalCancelledQty == "" && this.props.poQty == "" ? <button type="button" className="modal_clear_btn textDisable pointerNone">CLEAR FILTER</button>
                                            : <button type="button" onClick={(e) => this.props.clearFilter(0)} className="modal_clear_btn">
                                                CLEAR FILTER
                                         </button>}
                                    </li>
                                    <li>
                                        {this.props.shipmentRequestedQty != "" || this.props.orderCode != "" || this.props.poNumber != "" || this.props.shipmentDeliveredQty != "" || this.props.shipmentIntransitQty != "" || this.props.shipmentConfirmedQty != "" || this.props.poDate != "" || this.props.totalPendingQty != "" || this.props.totalCancelledQty != "" || this.props.poQty != ""
                                            ? <button type="submit" className="modal_Apply_btn">
                                                APPLY
                                        </button> : <button type="submit" className="modal_Apply_btn  btnDisabled" disabled>
                                                APPLY
                                        </button>}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        );
    }
}