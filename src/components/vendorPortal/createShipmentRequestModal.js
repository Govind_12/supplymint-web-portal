import React, { Component } from 'react'
import exclaimIcon from './../../assets/exclain.svg';
import moment from 'moment';
import searchIcon from '../../assets/close-recently.svg';
import { genericDateFormat } from '../../helper';

const todayDate = moment(new Date()).format("YYYY-MM-DD");
// const genericDateFormat = "DD-MM-YYYY";
export default class CreateShipmentRequestModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shipmentDetails: this.props.poDetails,
            status: "",
            lineItemArray: [],
            saveShow: true,
            shipmentRequestedDate: "",
            search: "",
            type: 1,
            prev: "",
            current: 0,
            next: "",
            maxPage: 0,
            no: 1,
            emptyShow: true,
            maxValue: "",
            dateerr: "",
            nullShow: false,
            minValue: "",
            qcFromDate: "",
            qcFromDateErr: "",
            qcToDate: "",
            qcToDateErr: "",
            remarks: "",
            formattedDate: "",
            qcFromFormatted: "",
            qcToFormatted: "",
            genericDateFormat: ""
        }
    }

    date() {
        if (this.state.shipmentRequestedDate != "") {
            this.setState({
                dateerr: false
            })
        } else {
            this.setState({
                dateerr: true
            })
        }
    }
    fromDate() {
        if (this.state.qcFromDate != "") {
            this.setState({
                qcFromDateErr: false
            })
        } else {
            this.setState({
                qcFromDateErr: true
            })
        }
    }
    toDate() {
        if (this.state.qcToDate != "") {
            this.setState({
                qcToDateErr: false
            })
        } else {
            this.setState({
                qcToDateErr: true
            })
        }
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.orders.VendorCreateShipmentDetails.isSuccess) {
            var poDetails = nextProps.orders.VendorCreateShipmentDetails.data.resource == null ? [] : nextProps.orders.VendorCreateShipmentDetails.data.resource
            poDetails.length == 0 ? [] : poDetails.poDetails.map((data) => { data.cancelledQty = 0, data.pendingQty = 0, data.requestedQty = poDetails.status == "PARTIAL" ? Number(data.orderQty) - (Number(data.totalRequestedQty) + Number(data.totalCancelledQty)) : parseInt(data.orderQty), data.error = false })
            // this.setState({
            //     poDetails: nextProps.orders.VendorCreateShipmentDetails.data.resource == null ? [] :nextProps.orders.VendorCreateShipmentDetails.data.resource
            // })

            var format = genericDateFormat(nextProps.orders.VendorCreateShipmentDetails.data.resource.dateFormat)
            let maxValue = moment(poDetails.validTo, format).format("YYYY-MM-DD")
            let minValue = moment(poDetails.validFrom, format).format("YYYY-MM-DD")

            this.setState({
                genericDateFormat: format,
                status: poDetails.status,
                shipmentDetails: poDetails,
                lineItemArray: poDetails.poDetails,
                prev: nextProps.orders.VendorCreateShipmentDetails.data.prePage,
                current: nextProps.orders.VendorCreateShipmentDetails.data.currPage,
                next: nextProps.orders.VendorCreateShipmentDetails.data.currPage + 1,
                maxPage: nextProps.orders.VendorCreateShipmentDetails.data.maxPage,
                minValue,
                maxValue
            })
        }
    }
    handleQuantityChange = (event, data) => {
        var lineItemArray = [...this.state.lineItemArray]
        let index = lineItemArray.findIndex((obj => obj.id == data.id));
        if (event.target.id == "requestedQty") {
            // if(lineItemArray[index].excludeQty == 0 ){
            //     lineItemArray[index].requestedQty = lineItemArray[index].totalRequestedQty
            // }else{
            //     lineItemArray[index].requestedQty = lineItemArray[index].excludeQty
            // }
            lineItemArray[index].requestedQty = event.target.value;
            lineItemArray[index].pendingQty = (parseInt(lineItemArray[index].orderQty) - (parseInt(lineItemArray[index].totalRequestedQty) + parseInt(lineItemArray[index].totalCancelledQty))) - (parseInt(event.target.value == "" ? 0 : event.target.value) + parseInt(lineItemArray[index].cancelledQty == "" || undefined || NaN ? 0 : lineItemArray[index].cancelledQty))
            if (Number(lineItemArray[index].requestedQty) > (Number(lineItemArray[index].orderQty) - (Number(lineItemArray[index].totalRequestedQty) + Number(lineItemArray[index].totalCancelledQty)))) {
                lineItemArray[index].error = true
            }
        } else if (event.target.id == "cancelQty") {
            lineItemArray[index].pendingQty = parseInt(lineItemArray[index].orderQty) - ((parseInt(event.target.value == "" ? 0 : event.target.value) + parseInt(lineItemArray[index].requestedQty == "" ? 0 : lineItemArray[index].requestedQty) + parseInt(lineItemArray[index].totalRequestedQty) + parseInt(lineItemArray[index].totalCancelledQty)))
            lineItemArray[index].cancelledQty = event.target.value
            if (Number(lineItemArray[index].cancelledQty) && Number(lineItemArray[index].cancelledQty) > (Number(lineItemArray[index].orderQty) - Number(lineItemArray[index].requestedQty) - Number(lineItemArray[index].totalRequestedQty) - Number(lineItemArray[index].totalCancelledQty))) {
                lineItemArray[index].error = true
            }

        } else if (event.target.id == "pendingQty") {
            lineItemArray[index].pendingQty = event.target.value
        } else if (event.target.id == "newRequestedQty") {
            lineItemArray[index].totalPendingQty = event.target.value
        }

        if (!(Number(lineItemArray[index].requestedQty) > (Number(lineItemArray[index].orderQty) - (Number(lineItemArray[index].totalRequestedQty) + Number(lineItemArray[index].totalCancelledQty)))) && !(Number(lineItemArray[index].cancelledQty) && Number(lineItemArray[index].cancelledQty) > (Number(lineItemArray[index].orderQty) - Number(lineItemArray[index].requestedQty) - Number(lineItemArray[index].totalRequestedQty) - Number(lineItemArray[index].totalCancelledQty)))) {
            lineItemArray[index].error = false
        }

        this.setState({
            lineItemArray,
            saveShow: !lineItemArray.some((data) => data.error == true),
            emptyShow: !lineItemArray.every((item) => ((Number(item.requestedQty) == 0 || "") && (Number(item.cancelledQty) == 0 || ""))),
            nullShow: lineItemArray.every((item) => ((Number(item.requestedQty) == "") || (Number(item.cancelledQty) == "")))
        })
    }

    createFinalShipment() {
        this.date()
        this.fromDate()
        this.toDate()
        setTimeout(() => {
            if (!this.state.dateerr && !this.state.qcFromDateErr && !this.state.qcToDateErr) {
                let shipmentDetails = this.state.shipmentDetails
                var changelineitems = [], totalRequestQty = 0, basicLineAmount = 0, basicAmount = 0, taxAmount = 0, totalOrderQty = 0, totalCancelledQty = 0, totalPendingQty = 0;
                changelineitems = this.state.lineItemArray.map((item) => {
                    totalRequestQty = totalRequestQty + parseInt(item.requestedQty == "" ? 0 : item.requestedQty)
                    totalCancelledQty = totalCancelledQty + parseInt(item.cancelledQty == "" ? 0 : item.cancelledQty)
                    totalOrderQty = totalOrderQty + parseInt(item.orderQty)
                    basicLineAmount = parseInt(item.rate) * parseInt(item.requestedQty == "" ? 0 : item.requestedQty)
                    totalPendingQty = totalPendingQty + parseInt(item.pendingQty),
                        basicAmount = basicAmount + basicLineAmount
                    taxAmount = taxAmount + basicLineAmount * (parseInt(item.gst) / 100)
                    return {
                        poDetailId: item.id,
                        requestedQty: parseInt(item.requestedQty == "" ? 0 : item.requestedQty),
                        totalRequestedQty: item.totalRequestedQty,
                        orderQty: parseInt(item.orderQty),
                        pendingQty: item.pendingQty,
                        totalCancelledQty: item.totalCancelledQty,
                        cancelQty: parseInt(item.cancelledQty == "" ? 0 : item.cancelledQty)
                    }
                })
                let payload = {
                    basicAmount,
                    taxAmount,
                    // poId: shipmentDetails.id,
                    shipmentAdviceCode: shipmentDetails.shipmentAdviceCode,
                    shipmentRequestOnDate: todayDate + "T00:00+05:30",
                    shipmentRequestDate: this.state.shipmentRequestedDate + "T00:00+05:30",
                    shipmentTotalRequestedQty: totalRequestQty,
                    totalPendingQty: totalPendingQty,
                    shipmentTotalCancelledQty: totalCancelledQty,
                    dueInDays: shipmentDetails.dueDays,
                    transporter: shipmentDetails.transporterName,
                    isShipConfirmed: "FALSE",
                    poDetails: changelineitems,
                    orderCode: this.props.orderCode,
                    orderNumber: this.props.orderNumber,
                    qcFromDate: this.state.qcFromDate == "" ? "" : this.state.qcFromDate + "T00:00+05:30",
                    qcToDate: this.state.qcToDate == "" ? "" : this.state.qcToDate + "T00:00+05:30",
                    remarks: this.state.remarks,
                }
                this.props.vendorCreateSrRequest(payload)
            }
        }, 10);
    }
    handleRequestedDate = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format(this.state.genericDateFormat)
        this.setState({ shipmentRequestedDate: e.target.value, formattedDate: formattedDate == "Invalid date" ? "" : formattedDate }, () => {
            this.date()
        }
        )
    }

    onSearch = (e) => {
        this.setState({ search: e.target.value })
        if (e.keyCode == 13) {
            this.setState({ type: 3 })
            let data = {
                type: 3,
                no: 1,
                search: e.target.value,
                status: "APPROVED",
                userType: "vendorpo",
                id: this.props.poId,
                orderCode: this.props.orderCode,
                orderNumber: this.props.orderNumber
            }
            this.props.VendorCreateShipmentDetailsRequest(data)
        }
    }
    searchClear = () => {
        if (this.state.type == 3) {
            let data = {
                type: 1,
                no: 1,
                search: "",
                status: "APPROVED",
                userType: "vendorpo",
                id: this.props.poId,
                orderCode: this.props.orderCode,
                orderNumber: this.props.orderNumber
            }
            this.props.VendorCreateShipmentDetailsRequest(data)
            this.setState({ type: 1, search: "" })
        } else {
            this.setState({ search: "" })
        }
    }
    handleChange = (e) => {
        var formattedDate = moment(e.target.value, "YYYY-MM-DD").format(this.state.genericDateFormat)
        if (e.target.id == "qcFromDate") {
            this.setState({ qcFromDate: e.target.value, qcToDate: "", qcToFormatted: "", qcFromFormatted: formattedDate == "Invalid date" ? "" : formattedDate }, () => { this.fromDate() })
        } else if (e.target.id == "qcToDate") {
            this.setState({ qcToDate: e.target.value, qcToFormatted: formattedDate == "Invalid date" ? "" : formattedDate }, () => { this.toDate() })
        } else if (e.target.id == "remarks") {
            this.setState({ remarks: e.target.value })
        }
    }
    render() {
        const { error, status, search, dateerr, emptyShow, nullShow, qcFromDateErr, qcToDateErr } = this.state
        return (
            <div>
                <div className="modal display_block " id="pocolorModel">
                    <div className="backdrop display_block"></div>
                    <div className="display_block">
                        <div className="col-md-12 col-sm-12 modal-content modalShow pad-0 createShipmentModal">
                            <div className="modal_Color selectVendorPopUp">
                                <div className="modalTop alignMiddle createShipmentTop">
                                    <div className="col-md-6 pad-0">
                                        <h2 className="displayInline m-rgt-20">Shipment Request</h2>
                                        <span className="shipmentNo m-rgt-20">{this.state.shipmentDetails.orderNumber}</span>
                                        {/*<button type="button" className="printbarCode">Print Barcode</button>*/}
                                    </div>
                                    <div className="col-md-6 pad-0">
                                        <div className="float_Right">
                                            <button type="button" className="cancelBtn m-rgt-10" onClick={this.props.handleShipmentModal}>Cancel</button>
                                            {/* <button type="button" className="saveBtnBlue" onClick={this.createFinalShipment}>Save</button> */}
                                            {/* {this.state.lineItemArray.some((data) => data.error)} */}
                                            {!this.state.saveShow || !emptyShow ? <button type="button" className="saveBtnBlue btnDisabled">Save</button>
                                                : <button type="button" className="saveBtnBlue" onClick={(e) => this.createFinalShipment(e)}>Save</button>}
                                        </div>
                                    </div>
                                </div>
                                <div className="modalOpertions">
                                    <h3>PO Details</h3>
                                    <div className="col-md-12 pad-0">
                                        <div className="shipment-col displayFlex">
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Number</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.orderNumber}</h5>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Date</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.length != 0 && this.state.shipmentDetails.poDate}</h5>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Valid From</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.length != 0 && this.state.shipmentDetails.validFrom}</h5>
                                            </div>
                                        </div>
                                        <div className="shipment-col displayFlex">
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>P.O Valid To</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.length != 0 && this.state.shipmentDetails.validTo}</h5>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Vendor Name</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.vendorName}</h5>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Delivery Site</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.siteDetails} </h5>
                                            </div>
                                        </div>
                                        <div className="shipment-col displayFlex">
                                            {/* <div className="col-md-6 m-top-5 pad-0">
                                                <label>Transpoter</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.transporterName}</h5>
                                            </div> */}
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>ASN Number</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{this.state.shipmentDetails.shipmentAdviceCode}</h5>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <label>Requested on</label>
                                            </div>
                                            <div className="col-md-6 m-top-5 pad-0">
                                                <h5>{moment(new Date).format(this.state.genericDateFormat)}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 pad-0">
                                        <div className="shipment-col displayFlex">
                                            {/* <div className="m-top-5 verticalInlineFlex"> */}
                                            <div className="col-md-6 pad-0">
                                                <label className="m-bot-0 m-rgt-10">Expected Delivery Date<span className="mandatory">*</span></label>
                                            </div>
                                            <div className="col-md-6 pad-0 dateFormat">
                                                <input type="date" className="inputDate" min={todayDate} max={this.state.maxValue} data-date={this.state.formattedDate == "" ? "Select Date" : this.state.formattedDate} onChange={this.handleRequestedDate} />
                                                {/* <input type="date" className="inputDate" min={todayDate} max={this.state.maxValue} placeholder={this.state.shipmentRequestedDate == "" ? "Select Date" : this.state.shipmentRequestedDate} value={this.state.shipmentRequestedDate} onChange={this.handleRequestedDate} /> */}

                                                {dateerr ? (
                                                    <span className="error">
                                                        Select Date
                                                </span>
                                                ) : null}
                                            </div>
                                            {/* </div> */}
                                            <div className="col-md-6 pad-0">
                                                <label className="m-bot-0 m-rgt-10">QC from date<span className="mandatory">*</span></label>
                                            </div>
                                            <div className="col-md-6 pad-0 dateFormat">
                                                <input type="date" className="inputDate " id="qcFromDate" data-date={this.state.qcFromFormatted == "" ? "Select Date" : this.state.qcFromFormatted} min={todayDate} max={this.state.maxValue} onChange={this.handleChange} />
                                                {/* <input type="date" className="inputDate" id="qcFromDate" min={todayDate} max={this.state.maxValue} placeholder={this.state.qcFromDate == "" ? "Select Date" : this.state.qcFromDate} value={this.state.qcFromDate} onChange={this.handleChange} /> */}
                                                {qcFromDateErr ? (
                                                    <span className="error">
                                                        Select Date
                                                </span>
                                                ) : null}
                                            </div>
                                            <div className="col-md-6 pad-0">
                                                <label className="m-bot-0 m-rgt-10">QC to date<span className="mandatory">*</span></label>
                                            </div>
                                            <div className="col-md-6 pad-0 dateFormat">
                                                {this.state.qcFromDate == "" ? <input type="date" className="inputDate" disabled value="" data-date="Select Date" /> : <input type="date" className="inputDate" id="qcToDate" min={this.state.qcFromDate} max={this.state.maxValue} data-date={this.state.qcToFormatted == "" ? "Select Date" : this.state.qcToFormatted} onChange={this.handleChange} />}
                                                {/* {this.state.qcFromDate == "" ? <input type="date" className="inputDate" disabled value="" placeholder={this.state.qcToDate == "" ? "Select Date" : this.state.qcToDate} /> : <input type="date" className="inputDate" id="qcToDate" min={this.state.qcFromDate} max={this.state.maxValue} placeholder={this.state.qcToDate == "" ? "Select Date" : this.state.qcToDate} value={this.state.qcToDate} onChange={this.handleChange} />} */}
                                                {qcToDateErr ? (
                                                    <span className="error">
                                                        Select Date
                                                </span>
                                                ) : null}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 pad-0 m-top-10">
                                        <div className="col-md-4 pad-0">
                                            <div className="col-md-5 pad-0">
                                                <label>Remarks</label>
                                            </div>
                                            <div className="col-md-4 pad-0">
                                                <textarea className="inputBox borderRadius4" rows="2" cols="30" id="remarks" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="modalContentMid">
                                    <div className="col-md-12 col-sm-12 pad-0 modalTableNew">
                                        <div className="col-md-6 pad-0">
                                            <h3>Items</h3>
                                        </div>
                                        <div className="col-md-6 pad-0">
                                            <div className="newSearch clearIconPos displayInline posRelative paginationWidth50 floatRight">
                                                <input type="search" className="searchWid width100" value={search} onKeyDown={this.onSearch} onChange={this.onSearch} placeholder="Search" />
                                                {search != "" ? <span className="closeSearch right4"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 pad-0 tableGeneric bordere3e7f3 mainRoleTable vendorTable cancelPoTable shipmentTable m-top-15">
                                            <div className="zui-wrapper">
                                                <div className="scrollableOrgansation scrollableTableFixed maxheight215 heightAuto">
                                                    <table className="table zui-table roleTable border-bot-table">
                                                        <thead>
                                                            <tr>
                                                                <th><label>Order Qty</label></th>
                                                                {status == "PARTIAL" && <th><label> Total Requested Qty</label></th>}
                                                                {status == "PARTIAL" && <th><label> Total Cancelled Qty</label></th>}
                                                                {/* {status == "PARTIAL" && <th><label> New Request Qty</label></th>} */}
                                                                <th><label> {this.state.status == "PARTIAL" ? "New Requested Qty" : "Request Qty"}</label></th>
                                                                <th><label> Cancel Qty</label></th>
                                                                <th><label> Pending Qty<div className="pendingHover displayInline"><img src={exclaimIcon} className="height13 m-lft-5" />
                                                                    <div className="pendingTooltip"><p className="m0">you can cancel or create new
                                                                    request later for these pending items.</p></div></div></label></th>
                                                                <th><label>Item Code</label></th>
                                                                <th><label>Item Name</label></th>
                                                                {/* <th><label>Division</label> </th> */}
                                                                <th> <label>Section</label></th>
                                                                <th> <label>Department</label></th>
                                                                <th> <label>Article</label></th>

                                                                <th><label>PP Qty</label></th>
                                                                <th><label>Set Qty</label></th>
                                                                <th><label> Rate</label></th>
                                                                <th><label> Amount</label></th>

                                                                <th><label> GST %</label></th>
                                                                <th><label> GST Type (IGST/C-SGST)</label></th>
                                                                <th><label> Size</label></th>
                                                                <th><label> Color</label></th>
                                                                <th><label> MRP</label></th>
                                                                <th><label> HSN Code</label></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.lineItemArray.length != 0 ?
                                                                this.state.lineItemArray.map((data, key) => (
                                                                    <tr key={key}>
                                                                        <td><label>{data.orderQty}</label></td>
                                                                        {status == "PARTIAL" && <td><label>{data.totalRequestedQty}</label></td>}
                                                                        {status == "PARTIAL" && <td><label>{data.totalCancelledQty}</label></td>}
                                                                        {this.state.status != "PARTIAL" ? <td><input type="number" id="requestedQty" className={Number(data.requestedQty) > Number(data.orderQty) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.requestedQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].requestedQty} /></td>
                                                                            : <td><input type="number" id="requestedQty" className={Number(data.requestedQty) > (Number(data.orderQty) - (Number(data.totalRequestedQty) + Number(data.totalCancelledQty))) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.requestedQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].requestedQty} /></td>}
                                                                        <td><input type="number" id="cancelQty" className={Number(data.cancelledQty) && Number(data.cancelledQty) > (Number(data.orderQty) - Number(data.requestedQty) - Number(data.totalRequestedQty) - Number(data.totalCancelledQty)) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.cancelledQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].cancelledQty} /></td>
                                                                        <td><label>{this.state.lineItemArray[key].pendingQty}</label></td>
                                                                        {/* <td><input type="text" id="pendingQty" className={(parseInt(this.state.lineItemArray[key].orderQty) - parseInt(this.state.lineItemArray[key].requestedQty))   < (parseInt(this.state.lineItemArray[key].pendingQty)) ? "active errorBorder" : "active"} onChange={(e) => this.handleQuantityChange(e, data)} placeholder={data.pendingQty} value={this.state.lineItemArray[key].id == data.id && this.state.lineItemArray[key].pendingQty} /></td> */}
                                                                        <td><label >{data.itemCode} </label> </td>
                                                                        <td> <label className="textBreak">{data.itemName}</label></td>
                                                                        {/* <td><label>{data.hl1Name}</label> </td> */}
                                                                        <td> <label> {data.hl2Name} </label></td>
                                                                        <td> <label> {data.hl3Name}</label></td>
                                                                        <td> <label> {data.hl4Name}</label></td>

                                                                        <td><label>{data.nbset}</label></td>
                                                                        <td><label>{data.singleSetQty}</label></td>
                                                                        <td><label>{data.rate}</label></td>
                                                                        <td><label>{data.basicAmount}</label></td>

                                                                        <td><label>{data.gst}</label></td>
                                                                        <td><label>{data.gstType}</label></td>
                                                                        <td><label>{data.cat5Name}</label></td>
                                                                        <td><label>{data.cat6Name}</label></td>
                                                                        <td><label>{data.mrp}</label></td>
                                                                        <td><label>{data.hsn}</label></td>
                                                                    </tr>
                                                                ))
                                                                : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-12 pad-0">
                                        <div className="col-md-6">

                                        </div>
                                        {/* <div className="col-md-6">
                                            <div className="pagerDiv newPagination text-right justifyEnd alignMiddle displayFlex">
                                                <Pagination {...this.state} {...this.props} page={this.page}
                                                    prev={this.state.prev} current={this.state.current} maxPage={this.state.maxPage} next={this.state.next} />
                                            </div>
                                        </div> */}
                                    </div>
                                </div>
                                <div className="modalBottom">
                                    <p className="note displayFlex"><img src={exclaimIcon} />Note: Requested quantity can not be more than order quantity</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}
