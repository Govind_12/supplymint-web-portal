import React, { Component } from 'react';
import plusIcon from '../../assets/plus-blue.svg';
import removeIcon from '../../assets/removeIcon.svg';
// import SetLevelConfigHeader from './setLevelConfigHeader';
import AsnLevelConfigHeader from './asnLevelHeaderConfig';
import SetLevelProcessedExpand from './vendor/setLevelProcessedExpand';
import ConfirmationSummaryModal from '../replenishment/confirmationReset';
import AsnLevelConfirmationModal from './asnLevelConfirmationModal';

export default class ExpandProcessOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            actionExpand: false,
            expandedId: 0,
            dropOpen: true,
            itemBaseArray: [],
            prevId: "",
            asnBaseExpandDetails: [],
            asnRequestedQty: "",
            asnBasedArray: [],
            asnLvlGetHeaderConfig: [],
            asnLvlFixedHeader: [],
            asnLvlCustomHeaders: {},
            asnLvlHeaderConfigState: {},
            asnLvlHeaderConfigDataState: {},
            asnLvlFixedHeaderData: [],
            asnLvlCustomHeadersState: [],
            asnLvlHeaderState: {},
            asnLvlHeaderSummary: [],
            asnLvlDefaultHeaderMap: [],
            asnLvlConfirmModal: false,
            asnLvlHeaderMsg: "",
            asnLvlParaMsg: "",
            asnLvlHeaderCondition: false,
            asnLvlSaveState: [],
            asnLvlColoumSetting: false,
            confirmModalHeader: false,
            mandateHeaderAsn: []
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.orders.poCloser.isSuccess) {
            return { itemBaseArray: nextProps.orders.poCloser.data.resource.poDetails }
        }
        if (nextProps.orders.EnterpriseGetPoDetails.isSuccess) {
            return { itemBaseArray: nextProps.orders.EnterpriseGetPoDetails.data.resource }
        }
        /* if (nextProps.replenishment.getHeaderConfig.isSuccess) {
            if (nextProps.replenishment.getHeaderConfig.data.resource != null && nextProps.replenishment.getHeaderConfig.data.basedOn == "ASN") {
                let asnLvlGetHeaderConfig = nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : []
                let asnLvlFixedHeader = nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"]) : []
                let asnLvlCustomHeadersState = nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : []
                return {
                    asnLvlCustomHeaders: prevState.asnLvlHeaderCondition ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != {} ? nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] : {},
                    asnLvlGetHeaderConfig,
                    asnLvlFixedHeader,
                    asnLvlCustomHeadersState,
                    asnLvlHeaderConfigState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] : {},
                    asnLvlFixedHeaderData: nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] != undefined ? nextProps.replenishment.getHeaderConfig.data.resource["Fixed Headers"] : {},
                    asnLvlHeaderConfigDataState: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] ? { ...nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] } : {},
                    asnLvlHeaderSummary: nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Custom Headers"]) : [],
                    asnLvlDefaultHeaderMap: nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"] != undefined ? Object.keys(nextProps.replenishment.getHeaderConfig.data.resource["Default Headers"]) : [],
                    mandateHeaderAsn: Object.values(nextProps.replenishment.getHeaderConfig.data.resource["Mandate Headers"])
                }
            }
        } */
        return {}
    }
    componentDidMount() {
        if (!this.props.replenishment.getSetHeaderConfig.isSuccess) {
            this.props.getSetHeaderConfigRequest(this.props.setHeaderPayload)
        }
    }
    componentDidUpdate(previousProps, previousState) {
        if (this.props.replenishment.createSetHeaderConfig.isSuccess && this.props.replenishment.createSetHeaderConfig.data.basedOn == "ASN") {
            this.props.getSetHeaderConfigRequest(this.state.setHeaderPayload)
        }
    }
    expandColumn(id, e, data) {
        var img = e.target
        if (!this.state.actionExpand || this.state.prevId !== e.target.id) {
            let payload = {
                userType: this.props.userType,
                orderId: data.orderId,
                setHeaderId: "",
                detailType: data.poType == "poicode" ? "item" : "set",
                poType: data.poType,
                shipmentStatus: data.poStatus,
                shipmentId: data.shipmentId
            }
            this.props.getItemHeaderConfigRequest(this.props.itemHeaderPayload)
            this.props.getCompleteDetailShipmentRequest(payload);
            this.setState({ poType: data.poType, actionExpand: true, prevId: e.target.id, expandedId: id, dropOpen: true })
        } else {
            this.setState({ actionExpand: false, expandedId: id, dropOpen: false })
        }
    }

    //DYNAMIC HEADER
    asnLvlOpenColoumSetting(data) {
        if (this.state.asnLvlCustomHeadersState.length == 0) {
            this.setState({
                asnLvlHeaderCondition: true
            })
        }
        if (data == "true") {
            this.setState({
                asnLvlColoumSetting: true
            })
        } else {
            this.setState({
                asnLvlColoumSetting: false
            })
        }
    }
    asnLvlpushColumnData(data) {
        let asnLvlGetHeaderConfig = this.state.asnLvlGetHeaderConfig
        let asnLvlCustomHeadersState = this.state.asnLvlCustomHeadersState
        let asnLvlHeaderConfigDataState = this.state.asnLvlHeaderConfigDataState
        let asnLvlCustomHeaders = this.state.asnLvlCustomHeaders
        let asnLvlSaveState = this.state.asnLvlSaveState
        let asnLvlDefaultHeaderMap = this.state.asnLvlDefaultHeaderMap
        let asnLvlHeaderSummary = this.state.asnLvlHeaderSummary
        let asnLvlFixedHeaderData = this.state.asnLvlFixedHeaderData
        if (this.state.asnLvlHeaderCondition) {

            if (!data.includes(asnLvlGetHeaderConfig) || asnLvlGetHeaderConfig.length == 0) {
                asnLvlGetHeaderConfig.push(data)
                if (!data.includes(Object.values(asnLvlHeaderConfigDataState))) {
                    let invert = _.invert(asnLvlFixedHeaderData)
                    let keyget = invert[data];
                    Object.assign(asnLvlCustomHeaders, { [keyget]: data })
                    asnLvlSaveState.push(keyget)
                }
                if (!Object.keys(asnLvlCustomHeaders).includes(asnLvlDefaultHeaderMap)) {
                    let keygetvalue = (_.invert(asnLvlFixedHeaderData))[data];
                    asnLvlDefaultHeaderMap.push(keygetvalue)
                }
            }
        } else {
            if (!data.includes(asnLvlCustomHeadersState) || asnLvlCustomHeadersState.length == 0) {
                asnLvlCustomHeadersState.push(data)
                if (!asnLvlCustomHeadersState.includes(asnLvlHeaderConfigDataState)) {
                    let keyget = (_.invert(asnLvlFixedHeaderData))[data];
                    Object.assign(asnLvlCustomHeaders, { [keyget]: data })
                    asnLvlSaveState.push(keyget)
                }
                if (!Object.keys(asnLvlCustomHeaders).includes(asnLvlHeaderSummary)) {
                    let keygetvalue = (_.invert(asnLvlFixedHeaderData))[data];
                    asnLvlHeaderSummary.push(keygetvalue)
                }
            }
        }
        this.setState({
            asnLvlGetHeaderConfig,
            asnLvlCustomHeadersState,
            asnLvlCustomHeaders,
            asnLvlSaveState,
            asnLvlDefaultHeaderMap,
            asnLvlHeaderSummary
        })
    }
    asnLvlCloseColumn(data) {
        let asnLvlGetHeaderConfig = this.state.asnLvlGetHeaderConfig
        let asnLvlHeaderConfigState = this.state.asnLvlHeaderConfigState
        let asnLvlCustomHeaders = []
        let asnLvlCustomHeadersState = this.state.asnLvlCustomHeadersState
        let asnLvlFixedHeaderData = this.state.asnLvlFixedHeaderData
        if (!this.state.asnLvlHeaderCondition) {
            for (let j = 0; j < asnLvlCustomHeadersState.length; j++) {
                if (data == asnLvlCustomHeadersState[j]) {
                    asnLvlCustomHeadersState.splice(j, 1)
                }
            }
            for (var key in asnLvlFixedHeaderData) {
                if (!asnLvlCustomHeadersState.includes(asnLvlFixedHeaderData[key])) {
                    asnLvlCustomHeaders.push(key)
                }
            }
            if (this.state.asnLvlCustomHeadersState.length == 0) {
                this.setState({
                    asnLvlHeaderCondition: false
                })
            }
        } else {
            for (var i = 0; i < asnLvlGetHeaderConfig.length; i++) {
                if (data == asnLvlGetHeaderConfig[i]) {
                    asnLvlGetHeaderConfig.splice(i, 1)
                }
            }
            for (var key in asnLvlFixedHeaderData) {
                if (!asnLvlGetHeaderConfig.includes(asnLvlFixedHeaderData[key])) {
                    asnLvlCustomHeaders.push(key)
                }
            }
        }
        asnLvlCustomHeaders.forEach(e => delete asnLvlHeaderConfigState[e]);
        this.setState({
            asnLvlGetHeaderConfig,
            asnLvlCustomHeaders: asnLvlHeaderConfigState,
            asnLvlCustomHeadersState,
        })
        setTimeout(() => {     // minValue,
            // maxValue
            let keygetvalue = (_.invert(this.state.asnLvlFixedHeaderData))[data];     // minValue,
            // maxValue
            let asnLvlSaveState = this.state.asnLvlSaveState
            asnLvlSaveState.push(keygetvalue)
            let asnLvlHeaderSummary = this.state.asnLvlHeaderSummary
            let asnLvlDefaultHeaderMap = this.state.asnLvlDefaultHeaderMap
            if (!this.state.asnLvlHeaderCondition) {
                for (let j = 0; j < asnLvlHeaderSummary.length; j++) {
                    if (keygetvalue == asnLvlHeaderSummary[j]) {
                        asnLvlHeaderSummary.splice(j, 1)
                    }
                }
            } else {
                for (let i = 0; i < asnLvlDefaultHeaderMap.length; i++) {
                    if (keygetvalue == asnLvlDefaultHeaderMap[i]) {
                        asnLvlDefaultHeaderMap.splice(i, 1)
                    }
                }
            }
            this.setState({
                asnLvlHeaderSummary,
                asnLvlDefaultHeaderMap,
                asnLvlSaveState
            })
        }, 100);
    }
    asnLvlsaveColumnSetting(e) {
        this.setState({
            asnLvlColoumSetting: false,
            asnLvlHeaderCondition: false,
            asnLvlSaveState: []
        })
        let payload = {
            basedOn: "ASN",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PROCESSED-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.asn_Display,
            fixedHeaders: this.state.asnLvlFixedHeaderData,
            defaultHeaders: this.state.asnLvlHeaderConfigDataState,
            customHeaders: this.state.asnLvlCustomHeaders,
        }
        this.props.createHeaderConfigRequest(payload)
    }
    asnLvlResetColumnConfirmation() {
        this.setState({
            setLvlHeaderMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
            // asnLvlParaMsg: "Click confirm to continue.",
            asnLvlConfirmModal: true,
        })
    }
    asnLvlResetColumn() {
        let payload = {
            basedOn: "ASN",
            module: "SHIPMENT TRACKING",
            subModule: "ORDERS",
            section: "PROCESSED-ORDERS",
            source: "WEB-APP",
            typeConfig: "PORTAL",
            attributeType: "TABLE HEADER",
            displayName: this.props.asn_Display,
            fixedHeaders: this.state.asnLvlFixedHeaderData,
            defaultHeaders: this.state.asnLvlHeaderConfigDataState,
            customHeaders: this.state.asnLvlHeaderConfigDataState,
        }
        this.props.createHeaderConfigRequest(payload)
        this.setState({
            asnLvlHeaderCondition: true,
            asnLvlColoumSetting: false,
            asnLvlSaveState: []
        })
    }
    // ------------------

    asnLvlCloseConfirmModal(e) {
        this.setState({
            asnLvlConfirmModal: !this.state.asnLvlConfirmModal,
        })
    }

    // resetColumnConfirmation() {
    //     this.setState({
    //         headerMsg: "Are you sure you want to reset the setting? Reset option will cause the default columns list visible in the table",
    //         confirmModalHeader: true,
    //     })
    // }
    status = (data) => {
        return (
            <div className="poStatus"> {data.status == "PARTIAL" ? <span className="partialTable partiaTag labelEvent">P</span> : ""}
                {data.poType == "poicode" ? <span className="partialTable setTag labelEvent">N</span>
                    : <span className="partialTable setTag labelEvent">S</span>}
                <div className="poStatusToolTip">
                    {data.status == "PARTIAL" ? <div className="each-tag"><span className="partialTable partiaTag labelEvent">P</span><label>Partial</label></div> : ""}
                    {data.poType == "poicode" ? <div className="each-tag"><span className="partialTable setTag labelEvent">N</span><label>Non-set</label></div> : <div className="each-tag"><span className="partialTable setTag labelEvent">S</span><label>Set</label></div>}
                </div>
            </div>
        )
    }
    render() {
        return (
            <div>
                <div className="gen-vend-sticky-table">
                    <div className="gvst-expend" id="expandTableMain">
                        {/* <AsnLevelConfigHeader {...this.props} {...this.state} asnLvlColoumSetting={this.state.asnLvlColoumSetting} asnLvlGetHeaderConfig={this.state.asnLvlGetHeaderConfig} asnLvlResetColumnConfirmation={(e) => this.asnLvlResetColumnConfirmation(e)} asnLvlOpenColoumSetting={(e) => this.asnLvlOpenColoumSetting(e)} asnLvlCloseColumn={(e) => this.asnLvlCloseColumn(e)} asnLvlpushColumnData={(e) => this.asnLvlpushColumnData(e)} asnLvlsaveColumnSetting={(e) => this.asnLvlsaveColumnSetting(e)} />
                         */}
                        <div className="gvst-inner" id="expandedTable" ref={node => this.expandTableref = node}>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th className="fix-action-btn"><label></label></th>
                                        {/* {this.state.asnLvlCustomHeadersState.length == 0 ? this.state.asnLvlGetHeaderConfig.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        )) : this.state.asnLvlCustomHeadersState.map((data, key) => (
                                            <th key={key}>
                                                <label>{data}</label>
                                            </th>
                                        ))} */}
                                        {this.props.setCustomHeadersState.length == 0 ? this.props.getSetHeaderConfig.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            )) : this.props.setCustomHeadersState.map((data, key) => (
                                                <th key={key}>
                                                    <label>{data}</label>
                                                </th>
                                            ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.asnBaseArray.length != 0 ? this.props.asnBaseArray.map((data, key) => (
                                        <React.Fragment key={key}>
                                            <tr>
                                                <td className="fix-action-btn">
                                                    <ul className="table-item-list">
                                                        <li className="til-inner" id={data.shipmentId} onClick={(e) => this.expandColumn(data.shipmentId, e, data)}>
                                                            {this.state.dropOpen && this.state.expandedId == data.shipmentId ? 
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                <path fill="#a4b9dd" fill-rule="nonzero" d="M17.266 0H2.734A2.734 2.734 0 0 0 0 2.734v14.532A2.734 2.734 0 0 0 2.734 20h14.532A2.734 2.734 0 0 0 20 17.266V2.734A2.734 2.734 0 0 0 17.266 0zm-3.933 10.833H6.667a.833.833 0 1 1 0-1.666h6.666a.833.833 0 1 1 0 1.666z"/>
                                                            </svg> : 
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 20 20">
                                                                <path fill="#a4b9dd" fill-rule="evenodd" d="M2.503 0h14.994A2.512 2.512 0 0 1 20 2.503v14.994C20 18.86 18.86 20 17.497 20H2.503A2.512 2.512 0 0 1 0 17.497V2.503A2.496 2.496 0 0 1 2.503 0zm6.32 8.823H5.328c-1.536 0-1.536 2.33 0 2.33h3.495v3.519c0 1.511 2.33 1.511 2.33 0v-3.52h3.519c1.511 0 1.511-2.33 0-2.33h-3.52V5.329c0-1.536-2.33-1.536-2.33 0v3.495z"/>
                                                            </svg> }
                                                        </li>
                                                    </ul>
                                                </td>
                                                {/* {this.state.asnLvlHeaderSummary.length == 0 ? this.state.asnLvlDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                        {hdata == "orderNumber" ? <React.Fragment><label className="fontWeig600  whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label>{data[hdata]}</label>}
                                                    </td>
                                                )) : this.state.asnLvlHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        {sdata == "orderNumber" ? <React.Fragment><label className="table-td-text fontWeig600  whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label>{data[sdata]}</label>}
                                                    </td>
                                                ))} */}
                                                {this.props.setHeaderSummary.length == 0 ? this.props.setDefaultHeaderMap.map((hdata, key) => (
                                                    <td key={key} >
                                                    {hdata == "orderNumber" ? <React.Fragment><label className="fontWeig600  whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                        : <label>{data[hdata]}</label>}
                                                </td>
                                                )) : this.props.setHeaderSummary.map((sdata, keyy) => (
                                                    <td key={keyy} >
                                                        {sdata == "orderNumber" ? <React.Fragment><label className="table-td-text fontWeig600  whiteUnset wordBreakInherit">{data["orderNumber"]} </label>{this.status(data)}</React.Fragment>
                                                            : <label>{data[sdata]}</label>}
                                                    </td>
                                                ))} 
                                            </tr>
                                            {this.state.dropOpen && this.state.expandedId == data.shipmentId ? <tr><td colSpan="100%" className="pad-0"> <SetLevelProcessedExpand {...this.state}  {...this.props} item_Display={this.props.userType == "entpo" ? "ENT_PROCESSED_ITEM" : "VENDOR_PROCESSED_ITEM"} set_Display={this.props.userType == "entpo" ? "ENT_PROCESSED_SET" : "VENDOR_PROCESSED_SET"} /></td></tr> : null}
                                        </React.Fragment>)) : <tr className="tableNoData"><td colSpan="100%"> NO DATA FOUND </td></tr>}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {this.state.asnLvlConfirmModal ? <AsnLevelConfirmationModal {...this.state} {...this.props} asnLvlCloseConfirmModal={(e) => this.asnLvlCloseConfirmModal(e)} asnLvlResetColumn={(e) => this.asnLvlResetColumn(e)} /> : null}
            </div>
        )
    }
}