import React from 'react';
import { Link } from 'react-router-dom';
import arrowIcon from '../../assets/arrow-right.svg';


class DropdownPortal extends React.Component {
    render() {
        var Cancelled_Orders = JSON.parse(sessionStorage.getItem('Cancelled Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled Orders')).crud)
        var Asn_Under_Approval = JSON.parse(sessionStorage.getItem('ASN Under Approval')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('ASN Under Approval')).crud)
        var Pending_Orders = JSON.parse(sessionStorage.getItem('Pending Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Pending Orders')).crud)

        var Approved_Asn = JSON.parse(sessionStorage.getItem('Approved ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Approved ASN')).crud)

        var Cancelled_Asn = JSON.parse(sessionStorage.getItem('Cancelled ASN')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Cancelled ASN')).crud)

        var Lr_Processing = JSON.parse(sessionStorage.getItem('LR Processing')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('LR Processing')).crud)

        var Order = JSON.parse(sessionStorage.getItem('Order')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Order')).crud)
        var Goods_Intransit = JSON.parse(sessionStorage.getItem('Goods Intransit')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Intransit')).crud)
        var Goods_Delivered = JSON.parse(sessionStorage.getItem('Goods Delivered')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Goods Delivered')).crud)
        var Logistics = JSON.parse(sessionStorage.getItem('Logistics')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Logistics')).crud)
        var Shipment = JSON.parse(sessionStorage.getItem('Shipment')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Shipment')).crud)
        var Processed_Orders = JSON.parse(sessionStorage.getItem('Processed Orders')) == null ? 0 : Number(JSON.parse(sessionStorage.getItem('Processed Orders')).crud)

        return (
            <label>
                <div className="dropdownPosition" id="dropOrder">
                    <img src={arrowIcon} className="dropdown-toggle" data-toggle="dropdown" />
                    <div className="dropdown-menu">
                        {sessionStorage.getItem("uType") == "VENDOR" && Order > 0 ? <h4>Order</h4> : null}
                        {sessionStorage.getItem("uType") == "VENDOR" && Order > 0 ? <ul className="pad-0">
                            {Pending_Orders > 0 ? <Link to="/vendor/purchaseOrder/pendingOrders">Pending Orders</Link> : null}
                            {Processed_Orders > 0 ? <Link to="/vendor/purchaseOrder/processedOrders">Processed Orders</Link> : null}
                            {Cancelled_Orders > 0 ? <Link to="/vendor/purchaseOrder/cancelledOrders">Cancelled Orders</Link> : null}
                        </ul> : null}

                        {sessionStorage.getItem("uType") == "VENDOR" && Shipment > 0 ? <h4>Shipment</h4> : null}
                        {sessionStorage.getItem("uType") == "VENDOR" && Shipment > 0 ? <ul className="pad-0">
                            {Asn_Under_Approval > 0 ? <Link to="/vendor/shipment/asnUnderApproval">ASN Under Approval</Link> : null}
                            {Approved_Asn > 0 ? <Link to="/vendor/shipment/approvedAsn">Approved ASN</Link> : null}
                            {Cancelled_Asn > 0 ? <Link to="/vendor/shipment/cancelledAsn">Cancelled ASN</Link> : null}
                        </ul> : null}

                        {sessionStorage.getItem("uType") == "VENDOR" && Logistics > 0 ? <h4>Logistic </h4> : null}
                        {sessionStorage.getItem("uType") == "VENDOR" && Logistics > 0 ? <ul className="pad-0">
                            {Lr_Processing > 0 ? <Link to="/vendor/logistics/lrProcessing">LR Processing</Link> : null}
                            {Goods_Intransit > 0 ? <Link to="/vendor/logistics/goodsIntransit">Goods Intransit</Link> : null}
                            {Goods_Delivered > 0 ? <Link to="/vendor/logistics/goodsDelivered">Goods Delivered</Link> : null}
                        </ul> : null}

                        {sessionStorage.getItem("uType") == "ENT" && Order > 0 ? <h4> Order</h4> : null}
                        {sessionStorage.getItem("uType") == "ENT" && Order > 0 ? <ul className="pad-0">
                            {Pending_Orders > 0 ? <Link to="/enterprise/purchaseOrder/pendingOrders">Open PO	</Link> : null}
                            {Processed_Orders > 0 ? <Link to="/enterprise/purchaseOrder/processedOrders" >Pending Delivery</Link> : null}
                            {Cancelled_Orders > 0 ? <Link to="/enterprise/purchaseOrder/cancelledOrders">Cancelled Orders</Link> : null}
                            <Link to="/enterprise/purchaseOrder/closedPo">Closed PO</Link>
                        </ul> : null}

                        {sessionStorage.getItem("uType") == "ENT" && Order > 0 ? <h4> Quality Check</h4> : null}
                        {sessionStorage.getItem("uType") == "ENT" && <ul className="pad-0">
                            <Link to="/enterprise/qualityCheck/pendingQc">Pending QC</Link>
                        </ul>}

                        {sessionStorage.getItem("uType") == "ENT" && Shipment > 0 ? <h4> Shipment</h4> : null}
                        {sessionStorage.getItem("uType") == "ENT" && Shipment > 0 ? <ul className="pad-0">
                            {Asn_Under_Approval > 0 ? <Link to="/enterprise/shipment/asnUnderApproval">ASN Under Approval</Link> : null}
                            {Approved_Asn > 0 ? <Link to="/enterprise/shipment/approvedAsn">Approved ASN</Link> : null}
                            {Cancelled_Asn > 0 ? <Link to="/enterprise/shipment/cancelledAsn">Rejected ASN</Link> : null}
                        </ul> : null}
                        {sessionStorage.getItem("uType") == "ENT" && Logistics > 0 ? <h4>  Logistic </h4> : null}
                        {sessionStorage.getItem("uType") == "ENT" && Logistics > 0 ? <ul className="pad-0">
                            {Lr_Processing > 0 ? <Link to="/enterprise/logistics/lrProcessing">LR Processing</Link> : null}
                            {Goods_Intransit > 0 ? <Link to="/enterprise/logistics/goodsIntransit">Goods Intransit</Link> : null}
                            {Goods_Delivered > 0 ? <Link to="/enterprise/logistics/goodsDelivered">Gate Entry</Link> : null}
                            <Link to="/enterprise/logistics/grcStatus">GRC Status</Link>
                        </ul> : null}
                    </div>
                </div>
            </label>
        );
    }
}

export default DropdownPortal;