import React from 'react';

class VendorDashboardEmailReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checkDrop: false,
            checkDropShip: false,
            checkDropLog: false,
        }
    }
    showCheckDrop(e) {
        e.preventDefault();
        this.setState({
            checkDrop: !this.state.checkDrop
        });
    }
    showCheckDropShip(e) {
        e.preventDefault();
        this.setState({
            checkDropShip: !this.state.checkDropShip
        });
    }
    showCheckDropLog(e) {
        e.preventDefault();
        this.setState({
            checkDropLog: !this.state.checkDropLog
        });
    }
    render() {
        return(
            <div className="modal">
                <div className="backdrop modal-backdrop-new"></div>
                <div className="modal-content email-report-modal">
                    <div className="erm-head">
                        <h3>EMAIL REPORT</h3>
                        <span className="ermh-close">
                            <span className="ermh-esc">Esc</span>
                            <img onClick={this.props.CloseEmailReport} src={require('../../assets/clearSearch.svg')} />
                        </span>
                    </div>
                    <div className="erm-body vder-body">
                        <div className="ermb-input-field">
                            <span className="ermbif-tag"><span className="ermbift-mailid">govind@turningcloud.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                            <span className="ermbif-tag"><span className="ermbift-mailid">amish.verma@vmart.in</span><img src={require('../../assets/clearSearch.svg')} /></span>
                            <span className="ermbif-tag"><span className="ermbift-mailid">sujit.sijwali@gmail.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                        </div>
                        <div className="ermb-input-field">
                            <span className="ermbif-tag"><span className="ermbift-mailid">akash.singh097@gmail.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                            <span className="ermbif-tag"><span className="ermbift-mailid">suresh.singh@gmail.com</span><img src={require('../../assets/clearSearch.svg')} /></span>
                        </div>
                        <div className="ermb-dashdata">
                            <h3>Dashboard Data</h3>
                            <div className="ermbdd-inner">
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />All Data
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />Open Purchase Orders
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />Closed Purchase Orders
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />Awaiting Delivery
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />Shipment Approved
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>
                            {/* Dashboard Accordian Reports */}
                            <div className="check-panel">
                                <div className="cp-checkbox">
                                    <div className="cpc-head" onClick={(e) => this.showCheckDrop(e)}>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Inspection Status
                                            <span className="checkmark1"></span>
                                        </label>
                                        <img className={this.state.checkDrop === false ? "" : "rotate180"} src={require('../../assets/down-arrow-new.svg')} />
                                    </div>
                                    {this.state.checkDrop && <div className="cpc-body">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Inspection Requested
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Inspection Required
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Cancelled Inspection
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>}
                                </div>
                            </div>
                            <div className="check-panel">
                                <div className="cp-checkbox">
                                    <div className="cpc-head" onClick={(e) => this.showCheckDropShip(e)}>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Shipment
                                            <span className="checkmark1"></span>
                                        </label>
                                        <img className={this.state.checkDropShip === false ? "" : "rotate180"} src={require('../../assets/down-arrow-new.svg')} />
                                    </div>
                                    {this.state.checkDropShip && <div className="cpc-body">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Pending ASN Approval
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Approved ASN
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Rejected ASN
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>}
                                </div>
                            </div>
                            <div className="check-panel">
                                <div className="cp-checkbox">
                                    <div className="cpc-head" onClick={(e) => this.showCheckDropLog(e)}>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Logistic
                                            <span className="checkmark1"></span>
                                        </label>
                                        <img className={this.state.checkDropLog === false ? "" : "rotate180"} src={require('../../assets/down-arrow-new.svg')} />
                                    </div>
                                    {this.state.checkDropLog && <div className="cpc-body">
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />LR Processing
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Goods In-Transit
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />Gate Entry
                                            <span className="checkmark1"></span>
                                        </label>
                                        <label className="checkBoxLabel0">
                                            <input type="checkBox" />GRC Status
                                            <span className="checkmark1"></span>
                                        </label>
                                    </div>}
                                </div>
                            </div>
                            {/* Dashboard Accordian Reports End */}
                        </div>
                        <div className="ermb-dashdata">
                            <h3>Attachments</h3>
                            <div className="ermbdd-inner">
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />Has Attachment
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="gen-pi-formate">
                                    <ul className="gpf-radio-list">
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input type="radio" name="searchformate" />
                                                <span className="checkmark"></span>
                                                Line Item level
                                            </label>
                                        </li>
                                        <li>
                                            <label className="gen-radio-btn">
                                                <input type="radio" name="searchformate" />
                                                <span className="checkmark"></span>
                                                Header Level
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="ermb-dashdata">
                            <h3>Send with applied filter</h3>
                            <div className="ermbdd-inner">
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />Yes
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                                <div className="ermbddi-checkbox">
                                    <label className="checkBoxLabel0">
                                        <input type="checkBox" />No
                                        <span className="checkmark1"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="erm-footer">
                        <button type="button" className="ermf-send">Send Now</button>
                        <button type="button" className="ermf-clear">Clear</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default VendorDashboardEmailReport;