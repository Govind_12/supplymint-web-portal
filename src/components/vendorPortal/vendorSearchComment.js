import React from 'react';
import EmailTranscriptModal from './emailTranscriptModal';
import ToastLoader from '../loaders/toastLoader';
import { decodeToken } from "../../helper/index";
import moment from 'moment';
import axios from 'axios';
import { CONFIG } from "../../config/index";
import SmallClose from '../../assets/smallClose.svg';
import ConfirmationSummaryModal from "../replenishment/confirmationReset";
import searchIcon from '../../assets/clearSearch.svg';

class VendorSearchComment extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            allComments: [],
            commentsCount: 0,
            type: 1,
            search: "",
            select: "ALL",
            module: "",
            isUnreadComment: false,

            inputDropdown: false,
            emailTranscript: false,

            showAllComments: [],
            currentCommentCode: "",
            currentModuleName: "",
            currentCommentId: "",

            toastMsg: "",
            loader: false,
            toastLoader: false,
            prefix: undefined,
            file: "",
            allEmails: [],
            filteredEmails: [],

            sendComment: [],
            comment: "",
            selectedTags: [],
            emailAttach: [],
            invoice: [],
            confirmModal: false,

            toEmails: "",
            ccEmails: "",
            headerMsg: "",
            subject: "",
            remark: "",

            errorToast: false,
            successToast: false,
            notification: [],
            vendorCode: "",
        }
    }

    componentDidMount(){
        let payload ={
            type: 1,
            search: "",
            isUnreadComment: false,
            module: ""
        }
        this.props.getAllCommentsRequest(payload);

        sessionStorage.setItem('currentPage', "VDVMAIN")
        sessionStorage.setItem('currentPageName', "Comments")

         //firebase message
         let t = this;
         const messaging = firebase.messaging()
         messaging.onMessage(function (payload) {
            let payloads ={
                type: t.state.search.length || (t.state.select !== "ALL" && t.state.select.length) ? 3 : 1,
                search: t.state.search,
                isUnreadComment: t.state.isUnreadComment,
                module: t.state.module
            }
            t.props.getAllCommentsRequest(payloads);
         });
    }

    static getDerivedStateFromProps(nextProps, preState){
        if (nextProps.orders.getAllComments.isSuccess) {
            return{
                allComments: nextProps.orders.getAllComments.data.resource == null ? [] : nextProps.orders.getAllComments.data.resource.comments,
                commentsCount: nextProps.orders.getAllComments.data.resource == null ? 0 : nextProps.orders.getAllComments.data.resource.unreadCommentCount
            }
        }
        if (nextProps.shipment.getUserEmails.isSuccess) {
            var formatEmails = nextProps.shipment.getUserEmails.data.resource !== null ? nextProps.shipment.getUserEmails.data.resource.map((_) => { return { id: _.id, value: _.userName.concat('(' + _.email + ')'), title: _.email, useName: _.userName } }) : []
            return{
                allEmails: formatEmails,
                filteredEmails: formatEmails
            }
        }
        return null
    }

    componentDidUpdate(){
        if (this.props.orders.getAllComments.isSuccess) {
            this.setState({
                allComments: this.props.orders.getAllComments.data.resource == null ? [] : this.props.orders.getAllComments.data.resource.comments,
                commentsCount: this.props.orders.getAllComments.data.resource == null ? 0 : this.props.orders.getAllComments.data.resource.unreadCommentCount
            })
            this.props.getAllCommentsClear();
        }
        if (this.props.shipment.getAllComment.isSuccess) {
            if (this.props.shipment.getAllComment.data.resource != null) {
                let commentData = []
                let allComment = []
                commentData = this.state.showAllComments.length == 0 ? this.props.shipment.getAllComment.data.resource.COMMENTS : [...allComment.reverse(), ...this.props.shipment.getAllComment.data.resource.COMMENTS]
                allComment = [...commentData.reverse()]
                this.setState({
                    showAllComments: allComment,
                }, () => {
                    document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                })
            }
            this.getEmails();
            this.props.getAllCommentClear();
            //Needs to call for getting update count::
            let payload ={
                type: this.state.search.length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                search: this.state.search,
                isUnreadComment: this.state.isUnreadComment,
                module: this.state.module
            }
            this.props.getAllCommentsRequest(payload);
        }
        if (this.props.shipment.deleteUploads.isSuccess) {
            this.onReload();
            this.props.deleteUploadsClear();
        }
        if (this.props.shipment.getUserEmails.isSuccess) {
            var formatEmails = this.props.shipment.getUserEmails.data.resource !== null ? this.props.shipment.getUserEmails.data.resource.map((_) => { return { id: _.id, value: _.userName.concat('(' + _.email + ')'), title: _.email, useName: _.userName } }) : []
            this.setState({
                allEmails: formatEmails,
                filteredEmails: formatEmails
            }, () => {
                this.tagifyFun()
            })
            this.props.getUserEmailsClear()
        }
        if (this.props.shipment.getUserEmails.isError) {
            this.tagifyFun()
            this.props.getUserEmailsClear()
        }
        if (this.props.shipment.qcAddComment.isSuccess) {
            this.onReload();
            this.props.qcAddCommentClear();
        }

        if (this.props.orders.emailTranscript.isSuccess) {
            this.setState({
                loader: false,
                successToast: true,
                emailTranscript: false,
                subject: "",
                remark: "",    
            })
            setTimeout(() =>{
              this.setState({ successToast: false})
            }, 5000)
            this.props.emailTranscriptClear()
        } else if (this.props.orders.emailTranscript.isError) {
            this.setState({
                errorToast: true,
                errorMessage: this.props.orders.emailTranscript.message.error == undefined ? undefined : this.props.orders.emailTranscript.message.error.errorMessage,
                errorCode: this.props.orders.emailTranscript.message.error == undefined ? undefined : this.props.orders.emailTranscript.message.error.errorCode,
                code: this.props.orders.emailTranscript.message.status,
                alert: true,
                loader: false,
                emailTranscript: false,
                subject: "",
                remark: "",
            })
            setTimeout(() =>{
                this.setState({ errorToast: false})
            }, 5000)
            this.props.emailTranscriptClear()
        }
        if (this.props.orders.commentNotification.isSuccess) {
            if (this.props.orders.commentNotification.data.resource != null) {
                this.setState({ 
                    notification: this.props.orders.commentNotification.data.resource !== null && this.props.orders.commentNotification.data.resource !== "" ? 
                                  this.props.orders.commentNotification.data.resource : []
                })
            }
            this.props.commentNotificationClear()
        }
    }

    onReload =(e)=>{
        Array.prototype.slice.call(document.getElementsByTagName('tags')).forEach(
            function(item) {
              item.remove();
        });
        let payload = {
            module: this.state.module,
            pageNo: 1,
            subModule: "COMMENTS",
            isAllAttachment: "",
            type: 1,
            search: "",
            commentId: this.state.currentCommentId,
            commentCode: this.state.currentCommentCode
        }
        this.props.getAllCommentRequest(payload)
    }

    getEmails = () => {
        const tokenDetails = decodeToken()
        const uType = sessionStorage.getItem('uType')
        var payload = {
            retailerEId: tokenDetails.schemaEntID,
            vendorEId: uType == "VENDOR" ? tokenDetails.eid : "",
            retailerOrgId: tokenDetails.schemaOrgID,
            vendorOrgId: uType == "VENDOR" ? tokenDetails.coreOrgID : "",
            dataRequired: "BOTH",
            userCallFrom: uType == "VENDOR" ? "VENDOR" : "RETAILER"
        }
        this.props.getUserEmailsRequest(payload)
    }

    tagifyFun = () => {
        const t = this
        var allEmails = this.state.allEmails
        var input = document.querySelector('[name=mix]'),
            tagify = new Tagify(input, {
                mode: 'mix',
                //pattern: /@|#/,
                pattern: /@/,
                transformTag: transformaTag,
                dropdown: {
                    enabled: 0,
                    position: "text",
                    classname: "customSuggestionsList",
                    searchKeys: ["value", "title"],
                    highlightFirst: true,  // automatically highlights first sugegstion item in the dropdown
                },
                whitelist: allEmails.map(function (item) { return typeof item == 'string' ? { value: item } : item }),
                callbacks: {
                    add: console.log,  // callback when adding a tag
                    remove: console.log   // callback when removing a tag
                }
            })
        function transformaTag(tagData) {
            tagData.value = tagData.value.replace(/\((.+?)\)/g, "")
        }
        tagify.on('input', function (e) {
            var prefix = e.detail.prefix;
            if (prefix) {
                if (prefix == '@')
                    tagify.settings.whitelist = allEmails;

                if (e.detail.value.length > 1) {
                    tagify.dropdown.show.call(tagify, e.detail.value);
                }
            }
            t.setState({ message: tagify.DOM.input.innerText, })
        })
        tagify.on('add', function (e) {
            t.setState({ selectedTags: tagify.value, prefix: '@' })
        })
        document.addEventListener('keyup', this.enterTextFunction);
    }

    commentsTab =(e, type)=>{
        this.setState({
            isUnreadComment: !this.state.isUnreadComment,
            module: "",
            select: "ALL",
            showAllComments: [],
            currentCommentCode: "",
            currentModuleName: "",
            search: "",

        },()=>{
            let payload ={
                type: this.state.search.length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                search: this.state.search,
                isUnreadComment: this.state.isUnreadComment,
                module: "",
            }
            this.props.getAllCommentsRequest(payload);
        })
    }

    onSearch = (e) => {
        if (e.target.value.trim().length) {
            if (e.target.value != "" && e.keyCode == 13) {
                let payload ={
                    type: e.target.value.trim().length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                    search: e.target.value.trim(),
                    isUnreadComment: this.state.isUnreadComment,
                    module: this.state.module
                }
                this.props.getAllCommentsRequest(payload);
                this.setState({
                    type: e.target.value.trim().length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                })
            }
        }
        this.setState({ search: e.target.value }, () => {
            if (this.state.search == "" ) {
                let payload ={
                    type: this.state.search.trim().length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                    search: this.state.search.trim(),
                    isUnreadComment: this.state.isUnreadComment,
                    module: this.state.module
                }
                this.props.getAllCommentsRequest(payload);
                this.setState({
                    search: "", type: this.state.search.trim().length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                })
            }
        })
    }

    searchClear = () => {
        let payload ={
            type: (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
            search: "",
            isUnreadComment: this.state.isUnreadComment,
            module: this.state.module
        }
        this.props.getAllCommentsRequest(payload);
        this.setState({
            search: "", type: (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
        })
    }

    onSelect =(e)=>{
        this.setState({ select: e.target.innerText, module: e.target.closest("li").getAttribute("value"),        
                showAllComments: [],
                currentCommentCode: "",
                currentModuleName: ""
            }, ()=>{
            let payload ={
                type: this.state.search.length || (this.state.select !== "ALL" && this.state.select.length) ? 3 : 1,
                search: this.state.search,
                isUnreadComment: this.state.isUnreadComment,
                module: this.state.module,
            }
            this.props.getAllCommentsRequest(payload); 
        })
    }

    showAllComments =(e, data)=>{
        Array.prototype.slice.call(document.getElementsByTagName('tags')).forEach(
            function(item) {
              item.remove();
        });
        this.setState({ currentCommentCode: data.commentCode,
                        currentModuleName: data.module == "ORDER" ? "PO Number" : data.module == "SHIPMENT" ? "ASN No" : data.module == "GOODS_IN_TRANSIT" ? "LR No" :
                                           data.module == "GATE_ENTRY" ? "Gate Entry No" : data.module == "GRC_STATUS" && "Grc No",
                        currentCommentId: data.commentId,
                        module: data.module,
                        select: data.module, 
                        vendorCode: data.vendorCode})
        let payload = {
            module: data.module,
            pageNo: 1,
            subModule: "COMMENTS",
            isAllAttachment: "",
            type: 1,
            search: "",
            commentId: data.commentId,
            commentCode: data.commentCode
        }
        this.props.getAllCommentRequest(payload)

        //Comment notification:::
        let commentPayload = {
            module: data.module,
            status: "SEEN",
            orderNumber: "",
            orderId: "",
            shipmentId: "",
            commentId: data.commentId
        }
        this.props.commentNotificationRequest(commentPayload)
    }

    openInputDropdown(e) {
        e.preventDefault();
        this.setState({
            inputDropdown: !this.state.inputDropdown
        }, () => document.addEventListener('click', this.closeInputDropdown));
    }
    closeInputDropdown = () => {
        this.setState({ inputDropdown: false }, () => {
            document.removeEventListener('click', this.closeInputDropdown);
        });
    }

    openEmailTranscript (e) {
        e.preventDefault();
        this.setState({
            emailTranscript: !this.state.emailTranscript
        }, () => document.addEventListener('click', this.closeInputDropdown));
    }
    closeEmailTranscript = () => {
        this.setState({ emailTranscript: false, subject: "",
                        remark: "", }, () => {
            document.removeEventListener('click', this.closeInputDropdown);
        });
    }

    highlighTag = (comment) => {
        if( comment !== null && comment !== undefined ){
            if (comment.match(`@\\w+`) === null) 
            return comment.trim();
            else {
                var finalCommentStr = "";
                var commentArray = comment.split(" ");
                commentArray.forEach( function(word){
                    if( word.match(`@\\w+`)){
                        word.split("@").forEach( function(char, index){
                            if( index == 0)
                            finalCommentStr += char;
                            else
                            finalCommentStr += char.replace(char, `<span class="mention-user"> @${char} </span> `);   
                        })         
                    }else
                        finalCommentStr += word +" ";
                });
                return finalCommentStr.trim();
            }
        }
    }

    deleteUploads(data) {
        let files = []
        let a = {
            fileName: data.fileName,
            isActive: "FALSE"
        }
        files.push(a)
        let payload = {
            shipmentId: data.shipmentId == undefined ? 0 : data.shipmentId,
            module: this.state.module,
            subModule: "COMMENTS",
            files: files,
            commentId: this.state.currentCommentId,
        }
        this.props.deleteUploadsRequest(payload)
    }

    addComment =()=> {
        var parsedMsg = this.convertToMsg()
        if (parsedMsg.trim().length > 0 && this.state.file == "" && this.state.currentCommentCode !== "") {
            let date = new Date()
            let showAllComments = this.state.showAllComments
            let payload = {
                subModule: "COMMENTS",
                commentedBy: sessionStorage.getItem("prn"),
                comments: parsedMsg,
                commentDate: moment(date).format("YYYY-MM-DDTHH:mm:ss") + "+05:30",
                commentType: "TEXT",
                folderDirectory: "",
                folderName: "",
                filePath: "",
                fileName: "",
                fileURL: "",
                urlExpirationTime: "",
                isUploaded: "FALSE",
                uploadedOn: "",
            }
            this.setState({
                sendComment: payload
            })
            showAllComments = [...showAllComments, payload]
            this.setState(
                {
                    showAllComments: showAllComments,
                    //callAddComments: true,
                    comment: ""
                }, () => {
                    this.sendMessage()
                    document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                })
        }
        if (this.state.file != "" && this.state.currentCommentCode !== "") {
            this.handleFile("", "", "send", parsedMsg)
        }
        if( this.state.currentCommentCode === ""){
            this.setState({
                toastMsg: "Please Select PO/ASN",
                toastLoader: true,
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 5000)
        }
    }

    convertToMsg = () => {
        if( document.getElementsByTagName('tags')[0] !== undefined){
            var tag = document.getElementsByTagName('tags')[0].querySelector('span')
            var div = document.getElementsByTagName('tags')[0].querySelector('span').querySelectorAll('div')
            var allTags = tag.querySelectorAll('tag')
            for (let i = 0; i < allTags.length; i++) {
                allTags[i].outerHTML = '@' + allTags[i].getAttribute('value') + ' '
            }
            for (let j = 0; j < div.length; j++) {
                if (div[j].innerHTML.innerText == undefined) {
                    div[j].outerHTML = div[j].innerHTML
                }
            }
            return tag.innerHTML.replace(/\&nbsp;/g, '').replace(/\<br>/g, '')
        }else
            return "";
    }

    sendMessage = () => {
        let userWatcher = "", emailWatcher = ""
        this.state.selectedTags.map((_) => { userWatcher = [...userWatcher, _.useName], emailWatcher = [...emailWatcher, _.title] })
        document.getElementsByTagName('tags')[0].querySelector('span').innerHTML = ''
        let moduleName = this.state.module;
        let payload = {
            shipmentId: "",
            module: moduleName,
            isQCDone: "",
            QCStatus: "",
            isInvoiceUploaded: "",
            orderNumber: "",
            orderId: "",
            documentNumber: "",
            reason: "",
            lgtNumber: "",
            shipmentAdviceCode: "",
            userWatcher: userWatcher.length > 0 && userWatcher.join(','),
            emailWatcher: emailWatcher.length > 0 && emailWatcher.join(','),
            [moduleName]: {
                QC: [],
                COMMENTS: [this.state.sendComment],
                INVOICE: []
            },
            commentId: this.state.currentCommentId,
            commentCode: this.state.currentCommentCode,
            vendorCode: this.state.vendorCode,
        }
        this.setState({ sendComment: [], emailAttach: [] })
        this.props.qcAddCommentRequest(payload)
    }

    enterTextFunction = (e) =>{
        var parsedMsg = this.convertToMsg()
        if( parsedMsg.trim().length > 0 || this.state.file !== ""){
            if( e.key === 'Enter' && !e.altKey && !e.ctrlKey && !e.shiftKey && this.state.prefix == undefined){
                this.addComment();
            }
            if( e.key === 'Enter' && (e.altKey || e.ctrlKey || e.shiftKey) && this.state.prefix == undefined){
                document.getElementsByClassName('tagify__input')[0].innerText+ "\n";
            }
            else{
                this.setState({ prefix: undefined})
            }   
        }        
    }

    handleFile = (e, subModule, type, parsedMsg) => {
        document.getElementById('fileName').innerHTML = type == "add" ? e.target.files[0].name : ""
        if (type == "add") {
            this.setState({ file: e.target.files })
        } else if (type == "delete") {
            this.setState({ file: "" })
            document.getElementById('commentUpload').value = ""
        } else if (type == "send") {
            this.onFileUpload(this.state.file, subModule, parsedMsg)
            document.getElementById('commentUpload').value = ""
        }
    }

    onFileUpload(e, subModule, parsedMsg) {
        let files = ""
        files = Object.values(this.state.file)
        let date = new Date()
        let id = "commentUpload";
        var allPdf = files.some((data) => (data.name.split('.').pop().toLowerCase() != "pdf" && data.name.split('.').pop().toLowerCase() != "png" && data.name.split('.').pop().toLowerCase() != "jpg" && data.name.split('.').pop().toLowerCase() != "svg" && data.name.split('.').pop().toLowerCase() != "bmp" && data.name.split('.').pop().toLowerCase() != "jpeg"))
        var fileSize = files.some((data) => (data.size > 5000000))
        if (!allPdf && id == "commentUpload") {
            if (!fileSize) {
                let uploadNode = {
                    orderId: "",
                    documentNumber: "",
                    shipmentId: "",
                    orderNumber: "",
                    module: this.state.module,
                    subModule: "COMMENTS",
                    comments: parsedMsg,
                    commentId: this.state.currentCommentId,
                    commentCode: this.state.currentCommentCode,
                    vendorCode: this.state.vendorCode,
                }
                const fd = new FormData();
                this.setState({
                    loader: true
                })
                let headers = {
                    'X-Auth-Token': sessionStorage.getItem('token'),
                    'Content-Type': 'multipart/form-data'
                }
                for (var i = 0; i < files.length; i++) {
                    fd.append("files", files[i]);
                }
                fd.append("uploadNode", JSON.stringify(uploadNode))

                axios.post(`${CONFIG.BASE_URL}/vendorportal/comqc/upload`, fd, { headers: headers })
                    .then(res => {
                        this.setState({
                            loader: false
                        })
                        let result = res.data.data.resource
                        document.getElementsByTagName('tags')[0].querySelector('span').innerHTML = ""
                        if (id == "fileUpload") {
                            let invoice = this.state.invoice
                            for (let i = 0; i < result.length; i++) {
                                let payload = {
                                    commentedBy: sessionStorage.getItem("prn"),
                                    comments: parsedMsg,
                                    commentDate: "",
                                    commentType: "URL",
                                    folderDirectory: result[i].folderDirectory,
                                    folderName: result[i].folderName,
                                    filePath: result[i].filePath,
                                    fileName: result[i].fileName,
                                    fileURL: result[i].fileURL,
                                    urlExpirationTime: result[i].urlExpirationTime,
                                    invoiceVersion: "NEW",
                                    uploadedOn: moment(date).format("YYYY-MM-DDTHH:mm:ss") + "+05:30",
                                    submodule: subModule

                                }
                                invoice.push(payload)
                            }
                            this.setState({
                                invoice,
                                loader: false,
                            }, () =>
                                document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                            )
                        } else {
                            let showAllComments = this.state.showAllComments
                            for (let i = 0; i < result.length; i++) {
                                let payload = {
                                    commentedBy: sessionStorage.getItem("prn"),
                                    comments: parsedMsg,
                                    commentDate: "",
                                    commentType: "URL",
                                    folderDirectory: result[i].folderDirectory,
                                    folderName: result[i].folderName,
                                    filePath: result[i].filePath,
                                    fileName: result[i].fileName,
                                    fileURL: result[i].fileURL,
                                    urlExpirationTime: result[i].urlExpirationTime,
                                    isUploaded: "TRUE",
                                    uploadedBy: sessionStorage.getItem('prn'),
                                    commentVersion: "NEW",
                                    uploadedOn: moment(date).format("YYYY-MM-DDTHH:mm:ss") + "+05:30",
                                    submodule: subModule
                                }
                                showAllComments.push(payload)
                            }
                            this.setState({
                                loader: false,
                                showAllComments,
                                //callAddComments: true,
                                file: ""
                            },
                                () => {
                                    document.getElementById('scrollBot').scrollTop = document.getElementById('scrollBot').scrollHeight
                                })
                        }
                        this.onReload();
                    }).catch((error) => {
                        this.setState({
                            loader: false
                        })
                    })
            } else {
                this.setState({
                    toastMsg: "File Size Not More then 5mb ",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 1500);
            }

        } else {
            this.setState({
                toastMsg: "Please Upload PDF and image file type only ",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);
        }
    }

    submitTranscript = () => {
        var toTags = [...document.getElementById("toEmail").querySelectorAll('tag')]
        var ccTags = [...document.getElementById("ccEmail").querySelectorAll('tag')]
        this.state.toEmails = toTags.map((_) => _.getAttribute('value'))
        this.state.ccEmails = ccTags.map((_) => _.getAttribute('value'))
        var subject = this.state.subject
        if (this.state.toEmails.length == 0) {
            // To cannot be empty
            this.setState({
                toastMsg: "Please enter a valid email address.",
                toastLoader: true
            })
            setTimeout(() => {
                this.setState({
                    toastLoader: false
                })
            }, 1500);

        }
        if (this.state.toEmails.length != 0 && (this.state.subject === undefined || subject.length == 0)) {
            this.setState({
                headerMsg: "Are you sure want to send this message without subject ?",
                confirmModal: true,
                subject: ""
            })

        }
        if (this.state.toEmails.length > 0 && this.state.subject !== undefined && this.state.subject.length !== 0) {
            this.resetColumn();
        }

    }

    resetColumn = () => {
        let payload = {
            shipmentId: "",
            orderId: "",
            module: this.state.module,
            subModule: "COMMENTS",
            shipmentAdviceCode: "",
            documentNumber: "",
            logisticNumber: "",
            to: this.state.toEmails.join(','),
            cc: this.state.ccEmails.join(','),
            subject: this.state.subject,
            remarks: this.state.remark,
            commentId: this.state.currentCommentId,
            commentCode: this.state.currentCommentCode

        }
        this.props.emailTranscriptRequest(payload)
    }

    closeConfirmModal = (e) => {
        this.setState({
            confirmModal: !this.state.confirmModal,
        })
    }

    handleDesc = (event, name) => {
        this.setState({
            [name]: event.target.value
        })
    }

    viewAllAttachment =(e)=>{
        let showAllComments = this.state.showAllComments
        showAllComments = showAllComments.filter( data => data.fileURL.length > 0 )
        this.setState({ showAllComments })
    }

    render(){
        return(
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="col-lg-5">
                        <div className="comment-search-section">
                            <div className="css-head">
                                <button type="button" className={!this.state.isUnreadComment ? "cssh-all-comment" : ""} onClick={(e) =>this.commentsTab(e,"ALL")}>All Comments</button>
                                <button type="button" className={this.state.isUnreadComment ? "cssh-all-comment" : ""} onClick={(e) =>this.commentsTab(e,"UNREAD")}>Unread Comments <span>{this.state.commentsCount}</span></button>
                            </div>
                            <div className="css-search-box">
                                <div className="csssb-inner">
                                    <div className="csssbi-search-input">
                                        <div className="csssbisi-flex">
                                            <div className="csssbisi-dropdown">
                                                <button type="button" className="csssbisi-btn" onClick={(e) => this.openInputDropdown(e)}>{this.state.select}<img src={require('../../assets/downArrowNew.svg')} /></button>
                                                {this.state.inputDropdown && 
                                                <div className="csssbisid-inner" onClick={this.onSelect}>
                                                    <ul>
                                                        <li key="ALL" value="">ALL</li>
                                                        <li key="ORDER" value="ORDER">ORDER</li>
                                                        <li key="SHIPMENT" value="SHIPMENT">SHIPMENT</li>
                                                        <li key="GOODS_IN_TRANSIT" value="GOODS_IN_TRANSIT">GOODS IN TRANSIT</li>
                                                        <li key="GATE_ENTRY" value="GATE_ENTRY">GATE ENTRY</li>
                                                        <li key="GRC_STATUS" value="GRC_STATUS">GRC</li>
                                                    </ul>
                                                </div>}
                                            </div>
                                            <span className="csssbisid-search">
                                                <img src={require('../../assets/searchicon.svg')} />
                                            </span>
                                            <div className="gvpd-search">
                                              <input type="search" value={this.state.search} onKeyDown={(e)=> {e.persist(); if(e.keyCode == 13) this.onSearch(e)}} onChange={this.onSearch}  placeholder="Type to search" />
                                              {this.state.search != "" ? <span className="closeSearch"><img src={searchIcon} onClick={this.searchClear} /></span> : null}
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="csssb-search-list">
                                       {this.state.allComments.length ? this.state.allComments.map( (data, key) => <li key={key} onClick={(e) =>this.showAllComments(e, data)}>
                                            <span className="csssbsl-item">
                                                <span className="csssbsli-num">{data.commentCode == null || data.commentCode == "" ? data.commentId : data.commentCode}</span>
                                                {/* <span className="csssbsli-time">28 May 2020	</span> */}
                                            </span>
                                            <div className="csssbsl-right">
                                                <span className="csssbsl-attachement">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="18.672" height="18.672" viewBox="0 0 18.672 18.672">
                                                        <path fill="#98a8bf" d="M3.44 10.318l5.896-5.896a3.474 3.474 0 014.914 4.914l-5.897 5.896a2.085 2.085 0 11-2.948-2.948l4.422-4.422a.695.695 0 01.983.983l-3.93 3.93.982.983 3.93-3.931a2.085 2.085 0 10-2.947-2.948l-4.423 4.422a3.474 3.474 0 004.914 4.914l5.897-5.897a4.864 4.864 0 10-6.88-6.878L2.458 9.336z"/>
                                                    </svg>
                                                    <span className="csssbsl-attack">{data.attachmentCount}</span>
                                                </span>
                                                <span className="csssbsl-messages">
                                                    <img src={require('../../assets/commentNew.svg')} />
                                                    {this.state.notification.length != 0 && this.state.notification.map((nData) => nData.commentId == data.commentId && <span className="csssbsli-msg-count">{nData.totalNotification}</span>)} : <span className="csssbsli-msg-count">{data.notificationCount}</span>
                                                </span>
                                            </div>
                                        </li>) :
                                        <div className="csssb-no-comment-found">
                                            <div className="csssbncf-inner">
                                                <img src={require('../../assets/no-comment.svg')} />
                                                <h3>No comments found</h3>
                                            </div>
                                        </div>}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-7">
                        <div className="search-comment-messages">
                            <div className="table-chat-box">
                                <div className="table-chat-box-inner">
                                    <div className="scm-head">
                                        <div className="scmh-left">
                                            <span className="scmhl-heading">{this.state.currentModuleName}</span>
                                            <span className="scmhl-num">{this.state.currentCommentCode}</span>
                                        </div>
                                        {this.state.showAllComments.some( data => data.fileURL.length > 0 ) ? <button type="button" className="scmh-view-all" onClick={this.viewAllAttachment}>View All Attachments</button> :null}
                                        {/* <span className="scmh-time">28 May 2020</span> */}
                                    </div>
                                    <div className="tcb-body chat-area">
                                        <div className="tcb-messages width100" id="scrollBot">
                                            {this.state.showAllComments.length == 0 && <div className="tcbm-without-messages">
                                                <div className="tcbmwm-inner">
                                                    <img src={require('../../assets/noComments.svg')} />
                                                    <h3>No conversations found</h3>
                                                    <p>Post your first comment by typing below</p>
                                                </div>
                                            </div>}
                                            <div className="msg-reciver">
                                                <div className="messageReceiver ">
                                                {this.state.showAllComments.length != 0 ? this.state.showAllComments.map((data, key) => (
                                                 <div key={key} className={sessionStorage.getItem("prn") == data.commentedBy || sessionStorage.getItem("prn") == data.uploadedBy ? "messageSender " : "messageReceiver "}>
                                                    <div className="each-comment m-top-15">
                                                        <div className="msgBg">
                                                        <span className="msg-user-icon-text">
                                                            {/* {data.commentedBy.charAt(0)} */}
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 21 21">
                                                                    <path fill="#f3f6fb" d="M10.5 0A10.5 10.5 0 1 0 21 10.5 10.5 10.5 0 0 0 10.5 0zm0 19.25a8.736 8.736 0 0 1-6.78-3.229 2.063 2.063 0 0 1 1.634-1.09c1.964-.453 3.9-.858 2.969-2.577-2.762-5.092-.788-7.979 2.177-7.979 2.907 0 4.93 2.78 2.178 7.979-.906 1.708.963 2.114 2.969 2.577a2.075 2.075 0 0 1 1.637 1.086A8.734 8.734 0 0 1 10.5 19.25z"/>
                                                                </svg>
                                                            </span>
                                                            {data.commentType == "TEXT" ? <label>
                                                                    <pre dangerouslySetInnerHTML={{
                                                                        __html: this.highlighTag(data.comments)
                                                                    }} />
                                                                </label> : 
                                                                <div>
                                                                {data.comments != "" && <label>
                                                                    <pre dangerouslySetInnerHTML={{ 
                                                                        __html: this.highlighTag(data.comments) 
                                                                    }} />
                                                                </label>}
                                                                <div className="tcb-file">
                                                                    <span className="msg-name-pdf">PDF</span>
                                                                    <a href={data.fileURL} className="fileNameQc" target="_blank" >{data.fileName}<img href={data.fileURL} className="msg-pdfdownload-link" src={require('../../assets/download2.svg')} /></a>
                                                                    {/* {sessionStorage.getItem("prn") == data.uploadedBy && <span className="msg-clear-msg" onClick={(e) => this.deleteUploads(data)}><img src={require('../../assets/clearSearch.svg')} /></span>} */}
                                                                </div>
                                                            </div>}
                                                        </div>
                                                        <div className="msg-user-detail">
                                                            <span className="msg-user-name">{sessionStorage.getItem("prn") == data.commentedBy || sessionStorage.getItem("prn") == data.uploadedBy ? "You" : sessionStorage.getItem('uType') == "ENT" ? "Vendor" : "Retailer"}</span>
                                                            {data.commentType == "TEXT" ? <span className="dtof-msg">{data.commentDate}</span> : <span className="dtof-msg">{data.uploadedOn == "" ? data.commentDate : data.uploadedOn}</span>}
                                                        </div>
                                                    </div>
                                                </div>)) : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tcb-footer">
                                        <div className="tcb-footer-inner">
                                            <div className="tcb-footer-inner-item">
                                                <div className="tcb-footer-write-comment-field">
                                                    <div id="all-email-container"></div>
                                                    <textarea name='mix' value="" placeholder="Write a message"> </textarea>
                                                    <div className="tcb-footer-attach-file">
                                                        <span className="tcbfaf-jumpnewline">
                                                            <span className="tcbfafj-uper">Jump to new line</span>
                                                            <span className="tcbfafj-lower">Shift + Enter</span>
                                                        </span>
                                                        <span className="tcbfaf-jumpnewline tcbfaf-border">
                                                            <span className="tcbfafj-uper">Send Comment</span>
                                                            <span className="tcbfafj-lower">Enter</span>
                                                        </span>
                                                        <div className="tcb-footer-attach-file-name">
                                                            {this.state.file !== "" && <span className="tcb-fafn-icon" onClick={(e) => this.handleFile("", "", "delete")}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="12.826" height="15.391" viewBox="0 0 12.826 15.391">
                                                                    <path fill="#a4b9dd" id="prefix__iconmonstr-trash-can-2" d="M6.489 12.185a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm2.565 0a.641.641 0 0 1-1.283 0V5.772a.641.641 0 0 1 1.283 0zm3.207-10.9v1.28H2V1.283h3.663c.577 0 1.046-.7 1.046-1.283h3.409c0 .578.468 1.283 1.046 1.283zM12.9 3.848v10.261H3.924V3.848H2.641v11.543h11.544V3.848z" transform="translate(-2)" />
                                                                </svg>
                                                            </span>}
                                                            <p id="fileName"></p>
                                                        </div>
                                                        <div className="writeComment">
                                                            <label className="custom-file-Icon">
                                                                <input type="file" id="commentUpload" onChange={(e) => this.handleFile(e, "COMMENTS", "add")}/>
                                                                <img src={require('../../assets/attach.svg')} />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="tcb-footer-addcomment">
                                                    <button type="button" className="tcb-footer-add-comment" onClick={() => this.addComment()}>Add Comment</button>
                                                    <button type="button" className="tcb-footer-email-tcranscript" onClick={(e) => this.openEmailTranscript(e)}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="17.028" height="17" viewBox="0 0 17.028 17">
                                                            <path fill="#fff" id="prefix__email-transcript" d="M13.48 6.441S9.8 6.2 8.514 7.816A7.4 7.4 0 0 1 13.48 3.6V2.184l3.547 2.838-3.547 2.851V6.441zM.018 17.039h16.99L8.514 8.8l-8.5 8.235zm1.187-9.933l7.3-5.313 2.166 1.574a10.209 10.209 0 0 1 1.386-.748L8.507.039 0 6.229v8.852l5.216-5.057-4.011-2.918zm11.177 3.471l4.646 4.5V6.842l-4.646 3.735z" transform="translate(0 -.039)" />
                                                        </svg>
                                                        Email Transcript
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
                {this.state.emailTranscript && <EmailTranscriptModal {...this.state} {...this.props} submitTranscript={this.submitTranscript} closeEmailTranscript={this.closeEmailTranscript} handleDesc={this.handleDesc}/>}
                {this.state.errorToast && <div className="email-transcript-alert-msg">
                    <div className="etam-inner">
                        <h3>Failed to Send</h3>
                        <span className="etam-status-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" id="prefix__cancel" width="18" height="18" viewBox="0 0 22 22">
                                <g id="prefix__Group_26" data-name="Group 26">
                                    <path fill="#eb0008" id="prefix__Path_11" d="M11 22a11 11 0 1 1 11-11 11.012 11.012 0 0 1-11 11zm0-20.167A9.167 9.167 0 1 0 20.167 11 9.177 9.177 0 0 0 11 1.833zm3.667 13.75a.914.914 0 0 1-.648-.269L6.685 7.981a.917.917 0 0 1 1.3-1.3l7.333 7.333a.917.917 0 0 1-.648 1.565zm0 0a.914.914 0 0 1-.648-.269L6.685 7.981a.917.917 0 0 1 1.3-1.3l7.333 7.333a.917.917 0 0 1-.648 1.565zm-7.333 0a.917.917 0 0 1-.648-1.565l7.333-7.333a.917.917 0 0 1 1.3 1.3l-7.338 7.33a.914.914 0 0 1-.648.269z" data-name="Path 11" />
                                </g>
                            </svg>
                        </span>
                        <p>Please try again.</p>
                        <span className="etam-close" onClick={() => this.setState({ errorToast: false })}>
                            <img src={SmallClose} />
                        </span>
                    </div>
                </div>}
                {this.state.successToast && <div className="email-transcript-alert-msg">
                    <div className="etam-inner">
                        <h3>Transcript Sent</h3>
                        <span className="etam-status-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                                <path fill="#3ee585" id="prefix__success" d="M10.5 0A10.5 10.5 0 1 0 21 10.5 10.5 10.5 0 0 0 10.5 0zM9.406 14.453l-3.937-3.779L6.69 9.418l2.693 2.57 5.342-5.441 1.243 1.233z" />
                            </svg>
                        </span>
                        <p>Please check Your Mailbox or spam.</p>
                        <span className="etam-close" onClick={() => this.setState({ successToast: false })}>
                            <img src={SmallClose} />
                        </span>
                    </div>
                </div>}
                {this.state.confirmModal ? <ConfirmationSummaryModal {...this.state} closeConfirmModal={(e) => this.closeConfirmModal(e)} resetColumn={(e) => this.resetColumn(e)} /> : null}
            </div>
        )
    }
}
export default VendorSearchComment;