import React, { Component } from 'react';
import InfoIcon from '../../assets/info-icon-new.svg';
import Rupee from '../../assets/rupeeNew.svg';
import SmallInfo from '../../assets/small-info.svg';
import Attach from '../../assets/attach.svg';
import InfoWhite from '../../assets/info-white.svg';
import DownArrow from '../../assets/downArrowNew.svg';
import { object } from 'prop-types';
import UnreadComment from './unreadComments';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import VendorDashboardEmailReport from './vendorDashReport';

class EnterpriseDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: "0",
            cardData: [],
            expiredPo: "currentDay",
            bottomCards: [],
            showSearchDrop: false,
            filterData: [],
            updateFilterData: [],
            search: "",
            selectOption: "All Vendor",
            showUnreadComment: false,
            activeStatusButton: false,
            emailReport: false,
            isQcOn: "TRUE",
        }
    }
    openEmailReport(e) {
        e.preventDefault();
        this.setState({
            emailReport: !this.state.emailReport
        },() => document.addEventListener('click', this.closeAllVendorFilter(e)));
    }
    CloseEmailReport = () => {
        this.setState({
            emailReport: false
        });
    }
    showSearchDrop(e) {
        e.preventDefault();
        this.setState({
            showSearchDrop: !this.state.showSearchDrop
        });
    }
    componentDidMount() {
        this.props.dashboardUpperRequest("Asd")
        this.props.dashboardBottomRequest("Asd")

        var orgDetail = JSON.parse(sessionStorage.getItem('orgId-name'))[0];
        let payload = {
            retailerEId: sessionStorage.getItem('partnerEnterpriseId') !== null ? sessionStorage.getItem('partnerEnterpriseId') : "",
            vendorEId: "",
            retailerOrgId: orgDetail != undefined && orgDetail.orgId != undefined ? orgDetail.orgId : "",
            vendorOrgId: "",
            dataRequired: "VENDOR",
            userCallFrom: "RETAILER"
        }
        this.props.vendorDashboardFilterRequest(payload);
        document.addEventListener("keydown", this.escFunction, false);
        document.addEventListener('click', this.closeAllVendorFilter)

        this.props.getStatusButtonActiveRequest('asd');
        sessionStorage.setItem('currentPage', "RDVMAIN")
        sessionStorage.setItem('currentPageName', "Dashboard")
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
        document.removeEventListener('click', this.closeAllVendorFilter)
    }
    closeAllVendorFilter = (e) =>{
        if( e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop") ){
            this.setState({ showSearchDrop: false, showUnreadComment: false, emailReport: false})
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.dashboardUpper.isSuccess) {
            var cardData = nextProps.orders.dashboardUpper.data.resource == null ? [] : nextProps.orders.dashboardUpper.data.resource 
            // cardData.noAction = Number(cardData.approvedCount) -  Number(cardData.partialPOData && cardData.partialPOData.partialCount)
            cardData.noAction = cardData.approvedCount
            this.setState({ cardData })
        }
        if (nextProps.orders.dashboardBottom.isSuccess) {
            this.setState({ bottomCards: nextProps.orders.dashboardBottom.data.resource == null ? [] : nextProps.orders.dashboardBottom.data.resource })
        }
        if (nextProps.orders.vendorDashboardFilter.isSuccess) {
            this.setState({
                filterData: nextProps.orders.vendorDashboardFilter.data.resource == null ? [] : nextProps.orders.vendorDashboardFilter.data.resource,
                updateFilterData: nextProps.orders.vendorDashboardFilter.data.resource == null ? [] : nextProps.orders.vendorDashboardFilter.data.resource,
            }, () => {if( sessionStorage.getItem('vendorCode') !== null ){
                this.state.filterData.map( key =>{
                    var vendorCode = key.userCode.toString();
                    var sessionVendorCode = sessionStorage.getItem('vendorCode').toString();
                    if( vendorCode == sessionVendorCode ){   
                        this.setState({ selectOption: key.userName.toString()});
                    }
                });
            }})
            
        }
        else if (nextProps.orders.vendorDashboardFilter.isError) {
            this.setState({ filterData: [] })
            this.props.vendorDashboardFilterClear();
        }
        if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) {
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage != undefined ){
                this.setState({ activeStatusButton: nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage === 'TRUE' ? true : false,
                                isQcOn: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired
                })
                this.props.getStatusButtonActiveClear()
            }
        }
    }
    changeNumber = (e) => {
        this.setState({ expiredPo: e.target.dataset.show, number: e.target.dataset.value })
    }
    handlechange = (e, userName, userCode) => {
        this.setState({ selectOption: userName == undefined ? "All Vendors" : userName, showSearchDrop: false })
        let payload = {
            vendorCode : userCode == undefined ? "" : userCode
        }
        sessionStorage.setItem('vendorCode', e.target.value);
        this.props.dashboardUpperRequest(payload);
        this.props.dashboardBottomRequest(payload);
    }
    
    onSearch = (e) => {
        this.setState({ search: e.target.value, updateFilterData: [] });
        var filterSearch = [];
        
        this.state.filterData.map( key =>{
            var vendorName = key.name.toString();
            var searchValue = e.target.value.toString();
            if( vendorName.toLowerCase().includes(searchValue.toLowerCase())){   
                filterSearch.push(key);
            }
        });
        this.setState({ updateFilterData: filterSearch});

    }
    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.setState({ showSearchDrop: false, showUnreadComment: false, emailReport: false })
        }
    }
    setStatusPendingQC = () =>{
        sessionStorage.setItem('currentPage', "RDVQCMAIN")
        sessionStorage.setItem('currentPageName', "Pending Inspection")
        sessionStorage.setItem("inspectionStatus", "PENDING_QC")
        sessionStorage.setItem('isDashboardComment', 0);
        this.props.history.push('/enterprise/qualityCheck/pendingQc');
    }
    setStatusPendingQCConfirm = () =>{ 
        sessionStorage.setItem('currentPage', "RDVQCMAIN")
        sessionStorage.setItem('currentPageName', "Pending Inspection")
        sessionStorage.setItem("inspectionStatus", "PENDING_QC_CONFIRM")
        sessionStorage.setItem('isDashboardComment', 0);
        this.props.history.push('/enterprise/qualityCheck/pendingQc');
    }
    openUnreadComment(e) {
        //e.preventDefault();
        e.persist();
        this.setState({
            showUnreadComment: !this.state.showUnreadComment
        },() => document.addEventListener('click', this.closeAllVendorFilter(e)));
    }
    cancelUnreadComment = (e) => {
            this.setState({
                showUnreadComment: false,
            })
    }
    render() {
        const { cardData, expiredPo ,bottomCards, search, selectOption} = this.state
        return (
            
            <div className="container-fluid nc-design">
                <div className="container-fluid ">
                    <div className="row bg-fff ">
                        <div className="col-lg-12 col-sm-12 col-md-12 p-lr-47">
                            <div className="v-dashboard-head">
                                <div className="vdh-lft">
                                    <h3>Summary</h3>
                                </div>
                                <div className="vdh-rgt">
                                    <button type="button" className="" onClick={(e) => this.openEmailReport(e)}><img src={require('../../assets/mail.svg')} /><span className="generic-tooltip">Email Report</span></button>
                                    <button type="button" className="vdh-download">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 21.5 19.926">
                                            <g data-name="download (3)">
                                                <g data-name="Group 2115">
                                                    <path d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424"/>
                                                    <path d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425"/>
                                                </g>
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Download Excel</span></button>
                                    <div className="vd-search-functionality">
                                        <button type="button" onClick={(e) => this.showSearchDrop(e)}><span>{selectOption}</span><img src={DownArrow} /></button>
                                        {this.state.showSearchDrop && <div className="backdrop-transparent"></div>}
                                        {this.state.showSearchDrop ? (
                                            <div className="vdsf-inner">
                                            <div className="vdsfi-search">
                                                <input type="search" value={search} onKeyDown={(e) => {this.onSearch(e)}} onChange={(e) => {this.onSearch(e)}} placeholder="Type To Search" />
                                                <span className="vdsfis-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.892" height="12.995" viewBox="0 0 12.892 12.995">
                                                        <path fill="#a4b9dd" id="prefix__iconmonstr-magnifier-2" d="M12.892 11.721l-3.36-3.36A5.272 5.272 0 1 0 8.24 9.618L11.617 13l1.275-1.275zM1.545 5.269a3.724 3.724 0 1 1 3.724 3.723 3.728 3.728 0 0 1-3.724-3.723z" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <div className="vdsf-all-vendor">
                                                <button onClick={e => {this.handlechange(e)}} className="bold" value="" key="allVendor">All Vendors</button>
                                                {this.state.updateFilterData.length > 0 &&  this.state.updateFilterData.map(i => (
                                                    i.userName !== null && i.userCode !== null && i.userName !== "" && i.userCode !== "" && <li onClick={e => {this.handlechange(e, i.userName, i.userCode)}}>
                                                        <button value={i.userCode} key={i.id}>{i.userName }</button>
                                                <span className="vdsfav-code">{i.userCode}</span>
                                                    </li>
                                                )) }
                                            </div>
                                        </div> ) : (null)  }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-sm-12 col-md-12 p-lr-0">
                            <div className="vendorDashboard-head-item">
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/enterprise/purchaseOrder/pendingOrders'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "RDVORDMAIN"),
                                     sessionStorage.setItem('currentPageName', "Open PO")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-blue"><img src={require('../../assets/orderDash.svg')} /></span>
                                        <span className="sf-size">{cardData.approvedCount}</span>
                                        <p>Open Purchase Orders</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/enterprise/logistics/grcStatus'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "RDVLOGMAIN"),
                                     sessionStorage.setItem('currentPageName', "GRC Status")}}>
                                    <div className="vdh-inner">
                                        {/* <div className="vdhit-left">
                                            <div className="vdhi-label">
                                                <span>100</span>
                                                <span className="label-bar"><span className="label-bar-inner bg-gradent" style={{height: "36%"}}></span></span>
                                                <span>0</span>
                                            </div>
                                            <span className="sf-size">{cardData.processedPOData != undefined && cardData.processedPOData.processedPercentage}%</span>
                                        </div> */}
                                        <span className="vdhi-img bg-yellow"><img src={require('../../assets/closeOrder.svg')} /></span>
                                        <span className="sf-size">{cardData.processedPOData != undefined && cardData.processedPOData.processedCount}</span>
                                        <p>Closed Purchase Orders</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/enterprise/logistics/goodsIntransit'), sessionStorage.setItem('isDashboardComment', 0),sessionStorage.setItem('currentPage', "RDVLOGMAIN"),
                                     sessionStorage.setItem('currentPageName', "Goods In-Transit")}}>
                                    <div className="vdh-inner">
                                        {/* <div className="vdhi-label">
                                            <span>100</span>
                                            <span className="label-bar"><span className="label-bar-inner clr-grad-red" style={{height: "30%"}}></span></span>
                                            <span>0</span>
                                        </div>
                                        <span className="sf-size">{cardData.partialPOData != undefined && cardData.partialPOData.partialPercentage}%</span> */}
                                        <span className="vdhi-img bg-red"><img src={require('../../assets/waitingDelivery.svg')} /></span>
                                        <span className="sf-size">{cardData.partialPOData != undefined && cardData.partialPOData.partialCount}</span>
                                        <p>Awaiting Delivery</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/enterprise/shipment/approvedAsn'), sessionStorage.setItem('isDashboardComment', 0),sessionStorage.setItem('currentPage', "RDVSHIPMAIN"),
                                     sessionStorage.setItem('currentPageName', "Approved ASN")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-pprl"><img src={require('../../assets/shipmentApv.svg')} /></span>
                                        <span className="sf-size">{cardData.shipmentApprovedCount != undefined && cardData.shipmentApprovedCount}</span>
                                        <p>Shipment Approved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="vendor-dashboard-tabs pad-0">
                            <div className="col-lg-12 col-md-12 pad-0">
                                <div className="vendor-tablist-item p-lr-47">
                                    <ul className="nav nav-tabs vd-tab-list" role="tablist">
                                        <li className="nav-item active">
                                            <a className="nav-link vd-tab-btn" href="#edhighlights" role="tab" data-toggle="tab">Action Items</a>
                                        </li>
                                        {/* <li className="nav-item">
                                            <a className="nav-link vd-tab-btn" href="#edanalysis" role="tab" data-toggle="tab">Analysis</a>
                                        </li> */}
                                    </ul>
                                </div>
                                <div className="tab-content vd-tab-inner">
                                    <div role="tabpanel" className="tab-pane fade vdtab-content in active" id="edhighlights">
                                        <div className="col-lg-12 col-md-12 p-lr-32 m-top-30">
                                            {this.state.isQcOn === "TRUE" &&
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content vdhc-insp">
                                                    <div className="ndhv-head">
                                                        <img src={require('../../assets/inspSearch.svg')} />
                                                        <span>Inspection Status</span>
                                                    </div>
                                                    <div className="set-dash-col sdc-first">
                                                        <div className="vd-highlight-inner">
                                                            <p>Pending Inspection</p>
                                                            <span className="vdhi-status">Inspection Requested</span>
                                                            <button type="button" onClick={() => this.setStatusPendingQC()}>Confirm Inspection
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.pendingQC}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Pending Inspection</p>
                                                            <span className="vdhi-status">Inspection Required</span>
                                                            <button type="button" onClick={() => this.setStatusPendingQCConfirm()}>Complete inspection
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.pendingConfirmQC}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Cancelled Inspection</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/qualityCheck/cancelledInspection'),
                                                                sessionStorage.setItem('isDashboardComment', 0)
                                                                sessionStorage.setItem('currentPage', "RDVQCMAIN")
                                                                sessionStorage.setItem('currentPageName', "Cancelled Inspection")
                                                                sessionStorage.setItem("inspectionStatus", "");}}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.inspectionCancelled}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>}
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content vdhc-shipment">
                                                    <div className="ndhv-head">
                                                        <img src={require('../../assets/shipmentTrack.svg')} />
                                                        <span>Shipment</span>
                                                    </div>
                                                    <div className="set-dash-col sdc-first">
                                                        <div className="vd-highlight-inner">
                                                            <p>ASN Under Approval</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/shipment/asnUnderApproval'),
                                                                sessionStorage.setItem('isDashboardComment', 0)
                                                                sessionStorage.setItem('currentPage', "RDVSHIPMAIN")
                                                                sessionStorage.setItem('currentPageName', "Pending ASN Approval")
                                                                sessionStorage.setItem("inspectionStatus", "")}}>Review and Approve
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnApproval}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>ASN Approved and Under Process</p>
                                                            <button type="button" onClick={() => {
                                                                this.props.history.push('/enterprise/shipment/approvedAsn'), 
                                                                sessionStorage.setItem('isDashboardComment', 0)
                                                                sessionStorage.setItem('currentPage', "RDVSHIPMAIN")
                                                                sessionStorage.setItem('currentPageName', "Approved ASN")
                                                                sessionStorage.setItem("inspectionStatus", "")}}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span> 
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnApproved}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Rejected ASN</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/shipment/cancelledAsn'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "RDVSHIPMAIN"),
                                                                sessionStorage.setItem('currentPageName', "Rejected ASN")}}>Review
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnCancalled}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content vdhc-logistic">
                                                    <div className="ndhv-head">
                                                        <img src={require('../../assets/logistic.svg')} />
                                                        <span>Logistic</span>
                                                    </div>
                                                    {this.state.activeStatusButton && <div className="set-dash-col sdc-first">
                                                        <div className="vd-highlight-inner">
                                                            <p>LR Processing</p>
                                                            <span className="vdhi-status">Invoices pending for Approval</span>
                                                            <button type="button" onClick={() => {
                                                                this.props.history.push('/enterprise/logistics/lrProcessing'), 
                                                                sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_INVOICE_REQUESTED")
                                                                sessionStorage.setItem('currentPage', "RDVLOGMAIN")
                                                                sessionStorage.setItem('currentPageName', "LR Processing")
                                                                sessionStorage.setItem("inspectionStatus", "")}}>Approve
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.invoiceApprovalCount}</span>
                                                        </div>
                                                    </div>}
                                                    {this.state.activeStatusButton && <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>LR Processing</p>
                                                            <span className="vdhi-status">Approved Invoices</span>
                                                            <button type="button" onClick={() => {
                                                                this.props.history.push('/enterprise/logistics/lrProcessing'),
                                                                sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_SHIPPED")
                                                                sessionStorage.setItem('currentPage', "RDVLOGMAIN")
                                                                sessionStorage.setItem('currentPageName', "LR Processing")
                                                                sessionStorage.setItem("inspectionStatus", "")}}>Review
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnShipped}</span>
                                                        </div>
                                                    </div>}
                                                    {!this.state.activeStatusButton && <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>LR Processing</p>
                                                            {/* <span className="vdhi-status">Approved Invoices</span> */}
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/lrProcessing'), 
                                                            sessionStorage.setItem('isDashboardComment', 0),
                                                            sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_SHIPPED")
                                                            sessionStorage.setItem('currentPage', "RDVLOGMAIN")
                                                            sessionStorage.setItem('currentPageName', "LR Processing")
                                                            sessionStorage.setItem("inspectionStatus", "")}}>Message to Vendor</button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnShipped}</span>
                                                        </div>
                                                    </div>}
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>On your way for Delivery</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/goodsIntransit'), 
                                                                sessionStorage.setItem('isDashboardComment', 0)
                                                                sessionStorage.setItem('currentPage', "RDVLOGMAIN")
                                                                sessionStorage.setItem('currentPageName', "Goods In-Transit")
                                                                }} >Message to Vendor
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnIntransit}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Gate Entry Status</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/goodsDelivered'),
                                                                sessionStorage.setItem('isDashboardComment', 0)
                                                                sessionStorage.setItem('currentPage', "RDVLOGMAIN")
                                                                sessionStorage.setItem('currentPageName', "Gate Entry")
                                                                }}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnAtDoorstep}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>GRC Status</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/enterprise/logistics/grcStatus'), 
                                                            sessionStorage.setItem('isDashboardComment', 0)
                                                            sessionStorage.setItem('currentPage', "RDVLOGMAIN")
                                                            sessionStorage.setItem('currentPageName', "GRC Status")
                                                            }}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.grcStatus}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content">
                                                    <div className="vd-unread-comment">
                                                        <div className="vduc-top">
                                                            <img src={require('../../assets/dashComment.svg')} />
                                                        </div>
                                                        <div className="vduc-text">
                                                            <p>Total Unread Comments</p>
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.unreadComments}</span>
                                                        </div>
                                                        <button type="button" onClick={(e) => this.openUnreadComment(e)}>Read Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade vdtab-content" id="edanalysis">0</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showUnreadComment && <UnreadComment {...this.props} activeStatusButton={this.state.activeStatusButton} cancelUnreadComment={this.cancelUnreadComment} />}
                {this.state.emailReport && <VendorDashboardEmailReport CloseEmailReport={this.CloseEmailReport} />}
            </div>
        )
    }
}

export function mapStateToProps(state) {
    return {
        //orders: state.orders,
        logistic: state.logistic
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EnterpriseDashboard);