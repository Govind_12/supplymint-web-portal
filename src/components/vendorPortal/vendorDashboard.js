import React, { Component } from 'react';
import SideBar from '../../components/sidebar';
import openRack from '../../assets/open-rack.svg';
import BraedCrumps from "../../components/breadCrumps"
import Footer from '../../components/footer';
import RightSideBar from "../../components/rightSideBar";
import InfoIcon from '../../assets/info-icon-new.svg';
import InfoWhite from '../../assets/info-white.svg';
import Rupee from '../../assets/rupeeNew.svg';
import SmallInfo from '../../assets/small-info.svg';
import Attach from '../../assets/attach.svg';
import UnreadComment from './unreadComments';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "../../redux/actions";
import VendorDashboardEmailReport from './vendorDashReport';

class VendorDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: "0",
            cardData: [],
            expiredPo: "currentDay",
            bottomCards: [],
            showUnreadComment: false,
            isQcOn: "TRUE",
            activeStatusButton: false,
            emailReport: false,
            vendorPOConfirmation: false,
        };
    }
    openEmailReport(e) {
        e.preventDefault();
        this.setState({
            emailReport: !this.state.emailReport
        },()=>  document.addEventListener('click', this.closeAllModal()));
    }
    CloseEmailReport = () => {
        this.setState({
            emailReport: false
        });
    }
    componentDidMount() {
        this.props.dashboardUpperRequest("Asd")
        this.props.dashboardBottomRequest("Asd")
        //this.props.getInspOnOffConfigRequest('asd');
        this.props.getStatusButtonActiveRequest('asd');

        document.addEventListener("keydown", this.escFunction, false);
        document.addEventListener('click', this.closeAllModal)
        sessionStorage.setItem('currentPage', "VDVMAIN")
        sessionStorage.setItem('currentPageName', "Dashboard")
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
        document.removeEventListener('click', this.closeAllModal)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.orders.dashboardUpper.isSuccess) {
            var cardData = nextProps.orders.dashboardUpper.data.resource == null ? [] : nextProps.orders.dashboardUpper.data.resource
            // cardData.noAction = Number(cardData.approvedCount) - Number(cardData.partialPOData && cardData.partialPOData.partialCount)
            cardData.noAction = cardData.approvedCount
            this.setState({ cardData })
        }
        if (nextProps.orders.dashboardBottom.isSuccess) {
            this.setState({ bottomCards: nextProps.orders.dashboardBottom.data.resource == null ? [] : nextProps.orders.dashboardBottom.data.resource })
        }
        if (nextProps.replenishment.inspectionOnOff.isSuccess) {
            if (nextProps.replenishment.inspectionOnOff.data.qc != null && nextProps.replenishment.inspectionOnOff.data.qc != "") {
                 this.setState({
                    isQcOn: nextProps.replenishment.inspectionOnOff.data.qc,  
                 })
            }
        }
        if (nextProps.logistic.getButtonActiveConfig.isSuccess && nextProps.logistic.getButtonActiveConfig.data.resource != null) { 
            if( nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage != undefined || nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired != undefined){
                this.setState({ activeStatusButton: nextProps.logistic.getButtonActiveConfig.data.resource.asnApprovalPage === 'TRUE' ? true : false,
                                vendorPOConfirmation: nextProps.logistic.getButtonActiveConfig.data.resource.vendorPOConfirmation === 'TRUE' ? true : false,
                                isQcOn: nextProps.logistic.getButtonActiveConfig.data.resource.inspectionRequired }, ()=>{
                                    if( this.state.isQcOn != null && this.state.isQcOn == "FALSE")
                                         this.props.getInspOnOffConfigRequest('asd');
                                });
                this.props.getStatusButtonActiveClear()
            }
        } 
    }
    changeNumber = (e) => {
        this.setState({ expiredPo: e.target.dataset.show, number: e.target.dataset.value })
    }
    setStatusPendingQC = () =>{
        sessionStorage.setItem("inspectionStatus", "PENDING_QC")
        sessionStorage.setItem('isDashboardComment', 0);
        this.props.history.push('/vendor/shipment/asnUnderApproval');
        sessionStorage.setItem('currentPage', "VDVSHIPMAIN")
        sessionStorage.setItem('currentPageName', "ASN Under Approval")
    }
    setStatusPendingQCConfirm = () =>{ 
        sessionStorage.setItem("inspectionStatus", "PENDING_QC_CONFIRM")
        sessionStorage.setItem('isDashboardComment', 0);
        this.props.history.push('/vendor/shipment/asnUnderApproval');
        sessionStorage.setItem('currentPage', "VDVSHIPMAIN")
        sessionStorage.setItem('currentPageName', "ASN Under Approval")
    }
    openUnreadComment =(e)=> {
        e.preventDefault();
        this.setState({
            showUnreadComment: !this.state.showUnreadComment
        },()=>  document.addEventListener('click', this.closeAllModal()));
    }
    // clickEventModalClose = (e) =>{
    //     if( e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop") ){
    //         this.setState({ showUnreadComment: false})
    //     }
    // }
    cancelUnreadComment = (e) => {
        this.setState({
            showUnreadComment: false,
        })
    }

    closeAllModal = (e) =>{
        if( e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop") ){
            this.setState({ showUnreadComment: false,emailReport: false })
        }
    }
    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.setState({ showUnreadComment: false,emailReport: false })
        }
    }

    render() {
        const { cardData, expiredPo, bottomCards } = this.state
        return (
            <div className="container-fluid nc-design">
                <div className="container-fluid ">
                    <div className="row bg-fff ">
                        <div className="col-lg-12 col-sm-12 col-md-12 p-lr-47">
                            <div className="v-dashboard-head">
                                <div className="vdh-lft">
                                    <h3>Summary</h3>
                                </div>
                                <div className="vdh-rgt">
                                    <button type="button" className="" onClick={(e) => this.openEmailReport(e)}><img src={require('../../assets/mail.svg')} /><span className="generic-tooltip">Email Report</span></button>
                                    <button type="button" className="vdh-download vdhd-after">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 21.5 19.926">
                                            <g data-name="download (3)">
                                                <g data-name="Group 2115">
                                                    <path d="M20.884 9.714a.613.613 0 0 0-.616.616v5.6a2.764 2.764 0 0 1-2.761 2.761H3.993a2.764 2.764 0 0 1-2.761-2.761v-5.695a.616.616 0 1 0-1.232 0v5.695a4 4 0 0 0 3.993 3.993h13.515a4 4 0 0 0 3.993-3.993v-5.6a.616.616 0 0 0-.617-.616z" data-name="Path 424"/>
                                                    <path d="M10.317 15.035a.62.62 0 0 0 .434.183.6.6 0 0 0 .433-.183l3.915-3.915a.617.617 0 0 0-.872-.872l-2.861 2.866V.614a.616.616 0 0 0-1.232 0v12.5L7.269 10.25a.617.617 0 0 0-.872.872z" data-name="Path 425"/>
                                                </g>
                                            </g>
                                        </svg>
                                        <span className="generic-tooltip">Download Excel</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12 col-sm-12 col-md-12 p-lr-0">
                            {this.state.vendorPOConfirmation ?
                            <div className="vendorDashboard-head-item">
                                <div className="col-lg-7 col-md-7 col-sm-12 pad-0">
                                    {this.state.vendorPOConfirmation && <div className="col-lg-4 col-md-4 col-sm-6" onClick={() => {this.props.history.push('/vendor/purchaseOrder/pendingOrders'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVORDMAIN"),
                                        sessionStorage.setItem('currentPageName', "Pending Orders"), sessionStorage.setItem('vendorPOStatus', "PENDING_APPROVAL")}}>
                                        <div className="vdh-inner">
                                            <span className="vdhi-img bg-blue"><img src={require('../../assets/orderDash.svg')} /></span>
                                            <span className="sf-size">{cardData.awaitingCount}</span>
                                            <p>PO Confirmation Pending</p>
                                        </div>
                                    </div>}
                                    <div className="col-lg-4 col-md-4 col-sm-6" onClick={() => {this.props.history.push('/vendor/purchaseOrder/pendingOrders'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVORDMAIN"),
                                        sessionStorage.setItem('currentPageName', "Pending Orders"), sessionStorage.setItem('vendorPOStatus', "APPROVED")}}>
                                        <div className="vdh-inner">
                                            <span className="vdhi-img bg-blue"><img src={require('../../assets/orderDash.svg')} /></span>
                                            <span className="sf-size">{cardData.approvedCount}</span>
                                            {this.state.vendorPOConfirmation ? <p>Confirmed PO</p> :
                                            <p>Open Purchase Order</p>}
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-sm-6" onClick={() => {this.props.history.push('/vendor/logistics/grcStatus'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                        sessionStorage.setItem('currentPageName', "GRC Status")}}>
                                        <div className="vdh-inner">
                                            <span className="vdhi-img bg-yellow"><img src={require('../../assets/closeOrder.svg')} /></span>
                                            <span className="sf-size">{cardData.processedPOData != undefined && cardData.processedPOData.processedCount}</span>
                                            <p>Closed Purchase Orders</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-5 col-md-5 col-sm-12 pad-0">
                                    <div className="col-lg-6 col-md-6 col-sm-6" onClick={() => {this.props.history.push('/vendor/logistics/goodsIntransit'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                        sessionStorage.setItem('currentPageName', "Goods In-Transit")}}>
                                        <div className="vdh-inner">
                                            <span className="vdhi-img bg-red"><img src={require('../../assets/waitingDelivery.svg')} /></span>
                                            <span className="sf-size">{cardData.partialPOData != undefined && cardData.partialPOData.partialCount}</span>
                                            <p>Awaiting Delivery</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-6" onClick={() => {this.props.history.push('/vendor/shipment/approvedAsn'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVSHIPMAIN"),
                                        sessionStorage.setItem('currentPageName', "Approved ASN")}}>
                                        <div className="vdh-inner">
                                            <span className="vdhi-img bg-pprl"><img src={require('../../assets/shipmentApv.svg')} /></span>
                                            <span className="sf-size">{cardData.shipmentApprovedCount != undefined && cardData.shipmentApprovedCount}</span>
                                            <p>Shipment Approved</p>
                                        </div>
                                    </div>
                                </div>
                            </div>:
                            <div className="vendorDashboard-head-item">
                                {this.state.vendorPOConfirmation && <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/vendor/purchaseOrder/pendingOrders'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVORDMAIN"),
                                    sessionStorage.setItem('currentPageName', "Pending Orders"), sessionStorage.setItem('vendorPOStatus', "PENDING_APPROVAL")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-blue"><img src={require('../../assets/orderDash.svg')} /></span>
                                        <span className="sf-size">{cardData.awaitingCount}</span>
                                        <p>PO Confirmation Pending</p>
                                    </div>
                                </div>}
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/vendor/purchaseOrder/pendingOrders'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVORDMAIN"),
                                    sessionStorage.setItem('currentPageName', "Pending Orders"), sessionStorage.setItem('vendorPOStatus', "APPROVED")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-blue"><img src={require('../../assets/orderDash.svg')} /></span>
                                        <span className="sf-size">{cardData.approvedCount}</span>
                                        {this.state.vendorPOConfirmation ? <p>Confirmed PO</p> :
                                        <p>Open Purchase Order</p>}
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/vendor/logistics/grcStatus'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                    sessionStorage.setItem('currentPageName', "GRC Status")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-yellow"><img src={require('../../assets/closeOrder.svg')} /></span>
                                        <span className="sf-size">{cardData.processedPOData != undefined && cardData.processedPOData.processedCount}</span>
                                        <p>Closed Purchase Orders</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/vendor/logistics/goodsIntransit'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                    sessionStorage.setItem('currentPageName', "Goods In-Transit")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-red"><img src={require('../../assets/waitingDelivery.svg')} /></span>
                                        <span className="sf-size">{cardData.partialPOData != undefined && cardData.partialPOData.partialCount}</span>
                                        <p>Awaiting Delivery</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6" onClick={() => {this.props.history.push('/vendor/shipment/approvedAsn'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('currentPage', "VDVSHIPMAIN"),
                                    sessionStorage.setItem('currentPageName', "Approved ASN")}}>
                                    <div className="vdh-inner">
                                        <span className="vdhi-img bg-pprl"><img src={require('../../assets/shipmentApv.svg')} /></span>
                                        <span className="sf-size">{cardData.shipmentApprovedCount != undefined && cardData.shipmentApprovedCount}</span>
                                        <p>Shipment Approved</p>
                                    </div>
                                </div>
                            </div>}
                        </div>
                        <div className="vendor-dashboard-tabs pad-0">
                            <div className="col-lg-12 col-md-12 pad-0">
                                <div className="vendor-tablist-item p-lr-47">
                                    <ul className="nav nav-tabs vd-tab-list" role="tablist">
                                        <li className="nav-item active">
                                            <a className="nav-link vd-tab-btn" href="#vdhighlights" role="tab" data-toggle="tab">Action Items</a>
                                        </li>
                                        {/* <li className="nav-item">
                                            <a className="nav-link vd-tab-btn vd-tab-dot" href="#vdpaymentStatus" role="tab" data-toggle="tab">Payment Status</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link vd-tab-btn" href="#vdanalysis" role="tab" data-toggle="tab">Analysis</a>
                                        </li> */}
                                    </ul>
                                </div>
                                <div className="tab-content vd-tab-inner">
                                    <div role="tabpanel" className="tab-pane fade vdtab-content in active" id="vdhighlights">
                                        <div className="col-lg-12 col-md-12  p-lr-32 m-top-30">
                                        {this.state.isQcOn === "TRUE" &&
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content vdhc-insp">
                                                    <div className="ndhv-head">
                                                        <img src={require('../../assets/inspSearch.svg')} />
                                                        <span>Inspection Status</span>
                                                    </div>
                                                    {this.state.isQcOn === "TRUE" && <div className="set-dash-col sdc-first">
                                                        <div className="vd-highlight-inner">
                                                            <p>Pending Inspection</p>
                                                            <span className="vdhi-status">Inspection Requested</span>
                                                            <button type="button" onClick={() => this.setStatusPendingQC()}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.pendingQC}</span>
                                                        </div>
                                                    </div> }
                                                    {this.state.isQcOn === "TRUE" && <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Pending Inspection</p>
                                                            <span className="vdhi-status">Inspection Required</span>
                                                            <button type="button" onClick={() => this.setStatusPendingQCConfirm()}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.pendingConfirmQC}</span>
                                                        </div>
                                                    </div>}
                                                    {this.state.isQcOn === "TRUE" && <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Cancelled Inspection</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/cancelledAsn'), sessionStorage.setItem('isDashboardComment', 0), sessionStorage.setItem('cancelledInspection', "SHIPMENT_INSPECTION_CANCELLED"),
                                                                sessionStorage.setItem('currentPage', "VDVSHIPMAIN"),
                                                                sessionStorage.setItem('currentPageName', "Rejected ASN")}}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.inspectionCancelled}</span>
                                                        </div>
                                                    </div> }
                                                </div>
                                            </div>}
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content vdhc-shipment">
                                                    <div className="ndhv-head">
                                                        <img src={require('../../assets/shipmentTrack.svg')} />
                                                        <span>Shipment</span>
                                                    </div>
                                                    <div className="set-dash-col sdc-first">
                                                        <div className="vd-highlight-inner">
                                                            <p>ASN Under Approval </p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/asnUnderApproval'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "VDVSHIPMAIN"),
                                                                sessionStorage.setItem('currentPageName', "ASN Under Approval")}}>Follow up
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnApproval}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>ASN Approved and Under Process</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/approvedAsn'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "VDVSHIPMAIN"),
                                                                sessionStorage.setItem('currentPageName', "Approved ASN")}}>Review and Upload Invoice
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnApproved}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Rejected ASN</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/shipment/cancelledAsn'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "VDVSHIPMAIN"),
                                                                sessionStorage.setItem('currentPageName', "Rejected ASN"),
                                                                sessionStorage.setItem('isDashboardShipment', 0)}}>Review
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnCancalled}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content vdhc-logistic">
                                                    <div className="ndhv-head">
                                                        <img src={require('../../assets/logistic.svg')} />
                                                        <span>Logistic</span>
                                                    </div>
                                                    {this.state.activeStatusButton && <div className="set-dash-col sdc-first">
                                                        <div className="vd-highlight-inner">
                                                            <p>LR Processing</p>
                                                            <span className="vdhi-status">Invoices pending for Approval</span>
                                                            <button type="button"  onClick={() => {this.props.history.push('/vendor/logistics/lrProcessing'), sessionStorage.setItem('isDashboardComment', 0),sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_INVOICE_REQUESTED"),
                                                                sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                                                sessionStorage.setItem('currentPageName', "LR Processing")}}>Message to Vendor
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.invoiceApprovalCount}</span>
                                                        </div>
                                                    </div>}
                                                    {this.state.activeStatusButton && <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>LR Processing</p>
                                                            <span className="vdhi-status">Approved Invoices</span>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/lrProcessing'), sessionStorage.setItem('isDashboardComment', 0),sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_SHIPPED"),
                                                                sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                                                sessionStorage.setItem('currentPageName', "LR Processing")}}>Message to Vendor
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnShipped}</span>
                                                        </div>
                                                    </div>}
                                                    {!this.state.activeStatusButton && <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>LR Processing</p>
                                                            {/* <span className="vdhi-status">Approved Invoices</span> */}
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/lrProcessing'), sessionStorage.setItem('isDashboardComment', 0),sessionStorage.setItem("lrProcessingApprovedStatus", "SHIPMENT_SHIPPED"),
                                                                sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                                                sessionStorage.setItem('currentPageName', "LR Processing")}}>Message to Vendor
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnShipped}</span>
                                                        </div>
                                                    </div>}
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>On your way for Delivery</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/goodsIntransit'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                                                sessionStorage.setItem('currentPageName', "Goods In-Transit")}}>Track Shipment Status
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnIntransit}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>Gate Entry Status</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/goodsDelivered'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                                                sessionStorage.setItem('currentPageName', "Gate Entry")}}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.asnAtDoorstep}</span>
                                                        </div>
                                                    </div>
                                                    <div className="set-dash-col">
                                                        <div className="vd-highlight-inner">
                                                            <p>GRC Status</p>
                                                            <button type="button" onClick={() => {this.props.history.push('/vendor/logistics/grcStatus'), sessionStorage.setItem('isDashboardComment', 0),
                                                                sessionStorage.setItem('currentPage', "VDVLOGMAIN"),
                                                                sessionStorage.setItem('currentPageName', "GRC Status")}}>Review Now
                                                                <span className="vdhi-rgt-arrow">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.44" height="10.88" viewBox="0 0 6.44 10.88">
                                                                        <path id="Path_890" fill="none" stroke="#b4c5d6" stroke-linecap="round" stroke-linejoin="round" stroke-width="2px" d="M6 9l4.026 4.026L14.052 9" data-name="Path 890" transform="rotate(-90 3.94 11.526)"/>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div className="vdhi-num">
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.grcStatus}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div className="col-lg-3 col-md-6 col-sm-12">
                                                <div className="vd-highlight-content">
                                                    <div className="vd-unread-comment">
                                                        <div className="vduc-top">
                                                            <img src={require('../../assets/dashComment.svg')} />
                                                        </div>
                                                        <div className="vduc-text">
                                                            <p>Total Unread Comments</p>
                                                            <span>{bottomCards.highlights != undefined && bottomCards.highlights.unreadComments}</span>
                                                        </div>
                                                        <button type="button" onClick={(e) => this.openUnreadComment(e)}>Read Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade vdtab-content" id="vdpaymentStatus">
                                        <div className="col-lg-3 col-md-3 col-sm-12">
                                            <div className="vd-payment-status">
                                                <div className="vd-ps-head">
                                                    <h3>Total Due payment</h3>
                                                </div>
                                                <div className="vd-ps-body">
                                                    <span className="vd-ps-left">Number of PO</span><span className="vd-ps-right">45</span>
                                                    <span className="vd-ps-left">PO Amount</span><span className="vd-ps-right clr-red f-weight7"><span><img src={Rupee}></img></span>8,24,000</span>
                                                </div>
                                                <div className="vd-ps-bottom">
                                                    <button type="button">Review Now</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-md-3 col-sm-12">
                                            <div className="vd-payment-status">
                                                <div className="vd-ps-head">
                                                    <h3>Payment Overdue</h3>
                                                </div>
                                                <div className="vd-ps-body">
                                                    <span className="vd-ps-left">Number of PO</span><span className="vd-ps-right">23</span>
                                                    <span className="vd-ps-left">PO Amount</span><span className="vd-ps-right f-weight7"><span><img src={Rupee}></img></span>2,34,200</span>
                                                </div>
                                                <div className="vd-ps-bottom">
                                                    <button type="button">Follow up</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-md-3 col-sm-12">
                                            <div className="vd-payment-status">
                                                <div className="vd-ps-head">
                                                    <h3>Payment On-Hold</h3>
                                                </div>
                                                <div className="vd-ps-body">
                                                    <span className="vd-ps-left">Number of PO</span><span className="vd-ps-right">400</span>
                                                    <span className="vd-ps-left">PO Amount</span><span className="vd-ps-right f-weight7"><span><img src={Rupee}></img></span>1,24,000</span>
                                                </div>
                                                <div className="vd-ps-bottom">
                                                    <button type="button">Follow up</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-md-3 col-sm-12">
                                            <div className="vd-payment-status">
                                                <div className="vd-ps-head m-b-8">
                                                    <h3>Supportive Required <span><img src={SmallInfo}></img></span></h3>
                                                    <span>Valid formats : jpg, jpeg, png, pdf</span>
                                                </div>
                                                <div className="vd-ps-body">
                                                    <span className="vd-psb-text">Choose Document to upload</span>
                                                    <input type="text" placeholder="Attach Document" />
                                                    <span className="vd-attach-img" type="file"><img src={Attach}></img></span>
                                                </div>
                                                <div className="vd-ps-bottom">
                                                    <button type="button">Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade vdtab-content" id="vdanalysis">c</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showUnreadComment && <UnreadComment {...this.props} activeStatusButton={this.state.activeStatusButton} cancelUnreadComment={this.cancelUnreadComment} />}
                {this.state.emailReport && <VendorDashboardEmailReport CloseEmailReport={this.CloseEmailReport} />}
            </div>
        )
    }
}
export function mapStateToProps(state) {
    return {
        //orders: state.orders,
        logistic: state.logistic,
        replenishment: state.replenishment
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VendorDashboard);