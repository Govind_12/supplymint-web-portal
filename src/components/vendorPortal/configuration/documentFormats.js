import React from 'react';
import ToastLoader from '../../loaders/toastLoader';
import moment from "moment";

class DocumentFormats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            documentFormatData: this.props.documentFormatData,
            asnPrefixValue: this.props.documentFormatData.asn != null && this.props.documentFormatData.asn !== "" 
                            ? this.props.documentFormatData.asn.prefix : "",
            asnDocValue: this.props.documentFormatData.asn != null && this.props.documentFormatData.asn !== "" 
                           ? this.props.documentFormatData.asn.randomNo : "",
            asnYearValue: this.props.documentFormatData.asn != null && this.props.documentFormatData.asn !== "" 
                           ? this.props.documentFormatData.asn.yearFormat : "",

            lrPrefixValue: this.props.documentFormatData.lr != null && this.props.documentFormatData.lr !== "" 
                           ? this.props.documentFormatData.lr.prefix : "",
            lrDocValue: this.props.documentFormatData.lr != null && this.props.documentFormatData.lr !== "" 
                          ? this.props.documentFormatData.lr.randomNo : "",
            lrYearValue: this.props.documentFormatData.lr != null && this.props.documentFormatData.lr !== "" 
                          ? this.props.documentFormatData.lr.yearFormat : "",
                          
            gateEntryPrefixValue: this.props.documentFormatData.gateEntry != null && this.props.documentFormatData.gateEntry !== "" 
                            ? this.props.documentFormatData.gateEntry.prefix : "",
            gateEntryDocValue: this.props.documentFormatData.gateEntry != null && this.props.documentFormatData.gateEntry !== "" 
                           ? this.props.documentFormatData.gateEntry.randomNo : "",
            gateEntryYearValue: this.props.documentFormatData.gateEntry != null && this.props.documentFormatData.gateEntry !== "" 
                           ? this.props.documentFormatData.gateEntry.yearFormat : "",

            grcPrefixValue: this.props.documentFormatData.grc != null && this.props.documentFormatData.grc !== "" 
                            ? this.props.documentFormatData.grc.prefix : "",
            grcDocValue: this.props.documentFormatData.grc != null && this.props.documentFormatData.grc !== "" 
                           ? this.props.documentFormatData.grc.randomNo : "",
            grcYearValue: this.props.documentFormatData.grc != null && this.props.documentFormatData.grc !== "" 
                           ? this.props.documentFormatData.grc.yearFormat : "",
            asnPrefixErr: false,
            lrPrefixErr: false,
            gateEntryPrefixErr: false,
            grcPrefixErr: false,  
            
            asnYearErr: false,
            lrYearErr: false,
            gateEntryYearErr: false,
            grcYearErr: false,

            asnDocErr: false,
            lrDocErr: false,
            gateEntryDocErr: false,
            grcDocErr: false,

            asnNumberFormat: "",
            lrNumberFormat: "",
            gateEntryNumberFormat:"",
            grcNumberFormat:"",

            toastLoader: false,
            toastMsg: "",

            asnYearFormat: "",
            lrYearFormat: "",
            gateEntryYearFormat:"",
            grcYearFormat: "",

            tab: "asn",
            asnTabSaveEnable: false,
            lrTabSaveEnable: false,
            gateEntryTabSaveEnable: false,
            grcTabSaveEnable: false,

        }
    }

    componentDidMount() {
        this.props.onRef(this)
        this.init();
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    init =()=>{
        this.paddingWithZero(this.state.asnDocValue, "asn")
        this.paddingWithZero(this.state.lrDocValue, "lr")
        this.paddingWithZero(this.state.gateEntryDocValue, "gateEntry")
        this.paddingWithZero(this.state.grcDocValue, 'grc') 

        this.yearFormat(this.state.asnYearValue, "asn")
        this.yearFormat(this.state.lrYearValue, "lr")
        this.yearFormat(this.state.gateEntryYearValue, "gateEntry")
        this.yearFormat(this.state.grcYearValue, 'grc') 

        let asnIndex = 0
        while(asnIndex < document.getElementById("asnYearValue").options.length){
            if(document.getElementById("asnYearValue").options[asnIndex].value === this.state.asnYearValue)
               document.getElementById("asnYearValue").options[asnIndex].selected = true;
            asnIndex++;   
        }
        let lrIndex = 0
        while(lrIndex < document.getElementById("lrYearValue").options.length){
            if(document.getElementById("lrYearValue").options[lrIndex].value === this.state.lrYearValue)
               document.getElementById("lrYearValue").options[lrIndex].selected = true;
               lrIndex++;   
        }
        let gateEntryIndex = 0
        while(gateEntryIndex < document.getElementById("gateEntryYearValue").options.length){
            if(document.getElementById("gateEntryYearValue").options[gateEntryIndex].value === this.state.gateEntryYearValue)
               document.getElementById("gateEntryYearValue").options[gateEntryIndex].selected = true;
            gateEntryIndex++;   
        }
        let grcIndex = 0
        while(grcIndex < document.getElementById("grcYearValue").options.length){
            if(document.getElementById("grcYearValue").options[grcIndex].value === this.state.grcYearValue)
               document.getElementById("grcYearValue").options[grcIndex].selected = true;
            grcIndex++;   
        }
    }

    handleChangeValue =(e)=>{
        // ASN::
        if( e.target.id == "asnPrefixValue"){
            let value = e.target.value;
            let alphaNumPattern = /^[0-9a-zA-Z]+$/;
            if( value.length < 2 || value.length > 6){
                this.setState({
                    toastMsg: "Prefix length should not be greater than 6 and less than 2.",
                    toastLoader: true,
                    asnPrefixErr: true,
                    asnPrefixValue: value.length > 6 ? this.state.asnPrefixValue : value,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 
            }
            else if( value.includes("-") || value.includes("/") || !alphaNumPattern.test(value)){
                    this.setState({ asnPrefixErr: true },()=>{
                        this.setState({ asnPrefixValue: value });
                     })
            }
            else{
               this.setState({ asnPrefixErr: false},()=>{
                  this.setState({ asnPrefixValue: value });
               })
            }
            this.setState({ asnTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())                       
        }
        else if( e.target.id == "asnDocValue"){
            let decimalPattern = /^[-+]?[0-9]+\.[0-9]+$/;
            let numberPattern = /^[0-9]+$/;
            let value = e.target.value;
            if( value == ""){
                this.setState({ asnDocErr: true, asnDocValue: value},()=>{
                    this.paddingWithZero(0, "asn")
                })
            }
            else if( Number(value) <= 0 || value.length > 8){
                this.setState({
                    toastMsg: "Document Serial length should not be greater than 8 and less than 1.",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 

            } 
            else if( decimalPattern.test(value) || !numberPattern.test(value)){
               this.setState({ asnDocErr: true},()=>{
                    this.setState({ asnDocValue: value })
               })
            }
            else{
               this.setState({ asnDocErr: false}, ()=> {
                   this.setState({ asnDocValue: value },()=>{
                     this.paddingWithZero(this.state.asnDocValue, "asn")
                   })
                }) 
            }
            this.setState({ asnTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        } 
        else if( e.target.id == "asnYearValue"){
           this.setState({ asnYearValue: e.target.value },()=>this.asnYearErr())
           this.yearFormat(e.target.value, "asn")
           this.setState({ asnTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }

        // LR::
        else if( e.target.id == "lrPrefixValue"){
            let value = e.target.value;
            let alphaNumPattern = /^[0-9a-zA-Z]+$/;
            if( value.length < 2 || value.length > 6){
                this.setState({
                    toastMsg: "Prefix length should not be greater than 6 and less than 2.",
                    toastLoader: true,
                    lrPrefixErr: true,
                    lrPrefixValue: value.length > 6 ? this.state.lrPrefixValue : value,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 
            }
            else if( value.includes("-") || value.includes("/") || !alphaNumPattern.test(value)){
                    this.setState({ lrPrefixErr: true },()=>{
                        this.setState({ lrPrefixValue: value });
                     })
            }
            else{
               this.setState({ lrPrefixErr: false},()=>{
                  this.setState({ lrPrefixValue: value });
               })
            }
            this.setState({ lrTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }
        else if( e.target.id == "lrDocValue"){
            let decimalPattern = /^[-+]?[0-9]+\.[0-9]+$/;
            let numberPattern = /^[0-9]+$/;
            let value = e.target.value;
            if( value == ""){
                this.setState({ lrDocErr: true, lrDocValue: value},()=>{
                    this.paddingWithZero(0, "lr")
                })
            }
            else if( Number(value) <= 0 || value.length > 8){
                this.setState({
                    toastMsg: "Document Serial length should not be greater than 8 and less than 1.",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 

            } 
            else if( decimalPattern.test(value) || !numberPattern.test(value)){
               this.setState({ lrDocErr: true},()=>{
                    this.setState({ lrDocValue: value })
               })
            }
            else{
               this.setState({ lrDocErr: false}, ()=> {
                   this.setState({ lrDocValue: value },()=>{
                     this.paddingWithZero(this.state.lrDocValue, "lr")
                   })
                }) 
            } 
            this.setState({ lrTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }
        else if( e.target.id == "lrYearValue"){
           this.setState({ lrYearValue: e.target.value },()=>this.lrYearErr())
           this.yearFormat(e.target.value, "lr")
           this.setState({ lrTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }

        // GATE ENTRY:::
        else if( e.target.id == "gateEntryPrefixValue"){
            let value = e.target.value;
            let alphaNumPattern = /^[0-9a-zA-Z]+$/;
            if( value.length < 2 || value.length > 6){
                this.setState({
                    toastMsg: "Prefix length should not be greater than 6 and less than 2.",
                    toastLoader: true,
                    gateEntryPrefixErr: true,
                    gateEntryPrefixValue: value.length > 6 ? this.state.gateEntryPrefixValue : value,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 
            }
            else if( value.includes("-") || value.includes("/") || !alphaNumPattern.test(value)){
                    this.setState({ gateEntryPrefixErr: true },()=>{
                        this.setState({ gateEntryPrefixValue: value });
                     })
            }
            else{
               this.setState({ gateEntryPrefixErr: false},()=>{
                  this.setState({ gateEntryPrefixValue: value });
               })
            }
            this.setState({ gateEntryTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }
        else if( e.target.id == "gateEntryDocValue"){
            let decimalPattern = /^[-+]?[0-9]+\.[0-9]+$/;
            let numberPattern = /^[0-9]+$/;
            let value = e.target.value;
            if( value == ""){
                this.setState({ gateEntryDocErr: true, gateEntryDocValue: value},()=>{
                    this.paddingWithZero(0, "gateEntry")
                })
            }
            else if( Number(value) <= 0 || value.length > 8){
                this.setState({
                    toastMsg: "Document Serial length should not be greater than 8 and less than 1.",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 

            } 
            else if( decimalPattern.test(value) || !numberPattern.test(value)){
               this.setState({ gateEntryDocErr: true},()=>{
                    this.setState({ gateEntryDocValue: value })
               })
            }
            else{
               this.setState({ gateEntryDocErr: false}, ()=> {
                   this.setState({ gateEntryDocValue: value },()=>{
                     this.paddingWithZero(this.state.gateEntryDocValue, "gateEntry")
                   })
                }) 
            }
            this.setState({ gateEntryTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab()) 
        }   
        else if( e.target.id == "gateEntryYearValue"){
           this.setState({ gateEntryYearValue: e.target.value },()=>this.gateEntryYearErr())
           this.yearFormat(e.target.value, "gateEntry")
           this.setState({ gateEntryTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }

        // GRC::
        else if( e.target.id == "grcPrefixValue"){
            let value = e.target.value;
            let alphaNumPattern = /^[0-9a-zA-Z]+$/;
            if( value.length < 2 || value.length > 6){
                this.setState({
                    toastMsg: "Prefix length should not be greater than 6 and less than 2.",
                    toastLoader: true,
                    grcPrefixErr: true,
                    grcPrefixValue: value.length > 6 ? this.state.grcPrefixValue : value,
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 
            }
            else if( value.includes("-") || value.includes("/") || !alphaNumPattern.test(value)){
                    this.setState({ grcPrefixErr: true },()=>{
                        this.setState({ grcPrefixValue: value });
                     })
            }
            else{
               this.setState({ grcPrefixErr: false},()=>{
                  this.setState({ grcPrefixValue: value });
               })
            }
            this.setState({ grcTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }
        else if( e.target.id == "grcDocValue"){
            let decimalPattern = /^[-+]?[0-9]+\.[0-9]+$/;
            let numberPattern = /^[0-9]+$/;
            let value = e.target.value;
            if( value == ""){
                this.setState({ grcDocErr: true, grcDocValue: value},()=>{
                    this.paddingWithZero(0, "grc")
                })
            }
            else if( Number(value) <= 0 || value.length > 8){
                this.setState({
                    toastMsg: "Document Serial length should not be greater than 8 and less than 1.",
                    toastLoader: true
                })
                setTimeout(() => {
                    this.setState({
                        toastLoader: false
                    })
                }, 5000); 

            } 
            else if( decimalPattern.test(value) || !numberPattern.test(value)){
               this.setState({ grcDocErr: true},()=>{
                    this.setState({ grcDocValue: value })
               })
            }
            else{
               this.setState({ grcDocErr: false}, ()=> {
                   this.setState({ grcDocValue: value },()=>{
                     this.paddingWithZero(this.state.grcDocValue, "grc")
                   })
                }) 
            }
            this.setState({ grcTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        } 
        else if( e.target.id == "grcYearValue"){
           this.setState({ grcYearValue: e.target.value },()=>this.grcYearErr())
           this.yearFormat(e.target.value, 'grc') 
           this.setState({ grcTabSaveEnable: true},()=>this.props.enableDisableSaveButtonOnDocTab())
        }
    }

    asnPrefixErr =()=>{
       if( this.state.asnPrefixValue == "")
         this.setState({ asnPrefixErr: true})
       else
         this.setState({ asnPrefixErr: false})     
    }

    lrPrefixErr =()=>{
        if( this.state.lrPrefixValue == "")
           this.setState({ lrPrefixErr: true})
        else
            this.setState({ lrPrefixErr: false})   
    }

    gateEntryPrefixErr =()=>{
        if( this.state.gateEntryPrefixValue == "")
           this.setState({ gateEntryPrefixErr: true})
        else
            this.setState({ gateEntryPrefixErr: false})   
    }

    grcPrefixErr =()=>{
        if( this.state.grcPrefixValue == "")
           this.setState({ grcPrefixErr: true})
        else
            this.setState({ grcPrefixErr: false})    
    }

    asnYearErr =()=>{
        if( this.state.asnYearValue == "" )
           this.setState({ asnYearErr: true })
        else
           this.setState({ asnYearErr: false })   
    }

    lrYearErr =()=>{
        if( this.state.lrYearValue == "" )
           this.setState({ lrYearErr: true })
        else
           this.setState({ lrYearErr: false })
     }

    gateEntryYearErr =()=>{
        if( this.state.gateEntryYearValue == "" )
           this.setState({ gateEntryYearErr: true })
        else
           this.setState({ gateEntryYearErr: false })
    } 

    grcYearErr =()=>{
        if( this.state.grcYearValue == "" )
           this.setState({ grcYearErr: true })
        else
           this.setState({ grcYearErr: false })
    }

    asnDocErr =()=>{
        if( this.state.asnDocValue == "")
           this.setState({ asnDocErr: true})
        else
           this.setState({ asnDocErr: false})   
    }

    lrDocErr =()=>{
        if( this.state.lrDocValue == "")
           this.setState({ lrDocErr: true})
        else
           this.setState({ lrDocErr: false}) 
    }

    gateEntryDocErr =()=>{
        if( this.state.gateEntryDocValue == "")
          this.setState({ gateEntryDocErr: true})
        else
          this.setState({ gateEntryDocErr: false}) 
    }

    grcDocErr =()=>{
        if( this.state.grcDocValue == "")
           this.setState({ grcDocErr: true})
        else
           this.setState({ grcDocErr: false}) 
    }

    paddingWithZero =(value, type)=>{
        let paddingStr = value;  
        while( paddingStr.length < 8 ){
           paddingStr = '0'+ paddingStr;
        }
        if( type == "asn" )
           this.setState({asnNumberFormat : paddingStr})
        if( type == "lr" )
           this.setState({lrNumberFormat : paddingStr}) 
        if( type == "gateEntry" )
           this.setState({gateEntryNumberFormat : paddingStr})            
        if( type == "grc" )
           this.setState({grcNumberFormat : paddingStr}) 
    }

    yearFormat = (year, type)=>{
        let currentYear = "";
        let nextYear = "";
        if( year.includes("-")){
            currentYear = moment(new Date()).format(year.split("-")[0]);
            nextYear = moment(new Date()).add(1,'year').format(year.split("-")[0]);  
        }
        else{
            currentYear = moment(new Date()).format(year);
        }
        if( type == "asn"){
            this.setState({asnYearFormat: nextYear != "" ? currentYear+"-"+nextYear : currentYear})
        }
        if( type == "lr"){
            this.setState({lrYearFormat: nextYear != "" ? currentYear+"-"+nextYear : currentYear})
        }
        if( type == "gateEntry"){
            this.setState({gateEntryYearFormat: nextYear != "" ? currentYear+"-"+nextYear : currentYear})
        }
        if( type == "grc"){
            this.setState({grcYearFormat: nextYear != "" ? currentYear+"-"+nextYear : currentYear})
        }
    }

    changeTabCS = (e) => {
        if( e.target.hash == "#asn")
          this.setState({ tab: "asn" },()=>this.props.enableDisableSaveButtonOnDocTab())
        else if( e.target.hash == "#lr")
          this.setState({ tab: "lr"},()=>this.props.enableDisableSaveButtonOnDocTab()) 
        else if( e.target.hash == "#gateEntry")
          this.setState({ tab: "gateEntry"},()=>this.props.enableDisableSaveButtonOnDocTab())     
        else 
          this.setState({ tab : "grc" },()=>this.props.enableDisableSaveButtonOnDocTab())
    }

    resetAll =()=>{
        this.setState({
            asnPrefixValue: this.props.documentFormatData.asn != null && this.props.documentFormatData.asn !== "" 
                            ? this.props.documentFormatData.asn.prefix : "",
            asnDocValue: this.props.documentFormatData.asn != null && this.props.documentFormatData.asn !== "" 
                        ? this.props.documentFormatData.asn.randomNo : "",
            asnYearValue: this.props.documentFormatData.asn != null && this.props.documentFormatData.asn !== "" 
                        ? this.props.documentFormatData.asn.yearFormat : "",

            lrPrefixValue: this.props.documentFormatData.lr != null && this.props.documentFormatData.lr !== "" 
                        ? this.props.documentFormatData.lr.prefix : "",
            lrDocValue: this.props.documentFormatData.lr != null && this.props.documentFormatData.lr !== "" 
                        ? this.props.documentFormatData.lr.randomNo : "",
            lrYearValue: this.props.documentFormatData.lr != null && this.props.documentFormatData.lr !== "" 
                        ? this.props.documentFormatData.lr.yearFormat : "",
                        
            gateEntryPrefixValue: this.props.documentFormatData.gateEntry != null && this.props.documentFormatData.gateEntry !== "" 
                            ? this.props.documentFormatData.gateEntry.prefix : "",
            gateEntryDocValue: this.props.documentFormatData.gateEntry != null && this.props.documentFormatData.gateEntry !== "" 
                        ? this.props.documentFormatData.gateEntry.randomNo : "",
            gateEntryYearValue: this.props.documentFormatData.gateEntry != null && this.props.documentFormatData.gateEntry !== "" 
                        ? this.props.documentFormatData.gateEntry.yearFormat : "",

            grcPrefixValue: this.props.documentFormatData.grc != null && this.props.documentFormatData.grc !== "" 
                            ? this.props.documentFormatData.grc.prefix : "",
            grcDocValue: this.props.documentFormatData.grc != null && this.props.documentFormatData.grc !== "" 
                        ? this.props.documentFormatData.grc.randomNo : "",
            grcYearValue: this.props.documentFormatData.grc != null && this.props.documentFormatData.grc !== "" 
                        ? this.props.documentFormatData.grc.yearFormat : "",

            asnPrefixErr: false,
            lrPrefixErr: false,
            gateEntryPrefixErr: false,
            grcPrefixErr: false,  
            
            asnYearErr: false,
            lrYearErr: false,
            gateEntryYearErr: false,
            grcYearErr: false,

            asnDocErr: false,
            lrDocErr: false,
            gateEntryDocErr: false,
            grcDocErr: false,

            asnNumberFormat: "",
            lrNumberFormat: "",
            gateEntryNumberFormat:"",
            grcNumberFormat:"",

            toastLoader: false,
            toastMsg: "",

            asnYearFormat: "",
            lrYearFormat: "",
            gateEntryYearFormat:"",
            grcYearFormat: "",
            
            tab: "asn",
            asnTabSaveEnable: false,
            lrTabSaveEnable: false,
            gateEntryTabSaveEnable: false,
            grcTabSaveEnable: false,

        },()=> {
            this.init();
            this.props.enableDisableSaveButtonOnDocTab();
        })

    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="global-search-tab gcl-tab p-lr-47">
                        <ul className="nav nav-tabs gst-inner" role="tablist">
                            <li className="nav-item active" >
                                <a className="nav-link gsti-btn" href="#asn" onClick={this.changeTabCS} role="tab" data-toggle="tab">ASN</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#lr" onClick={this.changeTabCS} role="tab" data-toggle="tab">LR Number</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#gateEntry" onClick={this.changeTabCS} role="tab" data-toggle="tab">Gate Entry</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#grc" onClick={this.changeTabCS} role="tab" data-toggle="tab">GRC</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="asn" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box" name="prefix" id="asnPrefixValue" value={this.state.asnPrefixValue} onChange={this.handleChangeValue}/>
                                                    { this.state.asnPrefixErr && <span className="error">Enter Valid Prefix</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" id="asnDocValue" value={this.state.asnDocValue} onChange={this.handleChangeValue}/>
                                                    { this.state.asnDocErr && <span className="error">Enter Valid Number</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    {/* <input type="text" className="width_100 input-outer-box" name="year" id="asnYearValue" value={this.state.asnYearValue} onChange={this.handleChangeValue}/> */}
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="asnYearValue" className="width_100 input-outer-box" onChange={this.handleChangeValue}>
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    { this.state.asnYearErr && <span className="error">Please Select Correct Format</span>}
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">{this.state.asnPrefixValue}/{this.state.asnNumberFormat}/{this.state.asnYearFormat}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="lr" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box" name="prefix" id="lrPrefixValue" value={this.state.lrPrefixValue} onChange={this.handleChangeValue}/>
                                                    {this.state.lrPrefixErr && <span className="error">Enter Valid Prefix</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" id="lrDocValue" value={this.state.lrDocValue} onChange={this.handleChangeValue}/>
                                                    { this.state.lrDocErr && <span className="error">Enter Valid Number</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    {/* <input type="text" className="width_100 input-outer-box" name="year" id="lrYearValue" value={this.state.lrYearValue} onChange={this.handleChangeValue}/> */}
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="lrYearValue" className="width_100 input-outer-box" onChange={this.handleChangeValue}>
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    { this.state.lrYearErr && <span className="error">Please Select Correct Format</span>}
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">{this.state.lrPrefixValue}/{this.state.lrNumberFormat}/{this.state.lrYearFormat}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="gateEntry" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="input-outer-box" name="prefix" id="gateEntryPrefixValue" value={this.state.gateEntryPrefixValue} onChange={this.handleChangeValue}/>
                                                    {this.state.gateEntryPrefixErr && <span className="error">Enter Valid Prefix</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" id="gateEntryDocValue" value={this.state.gateEntryDocValue} onChange={this.handleChangeValue}/>
                                                    { this.state.gateEntryDocErr && <span className="error">Enter Valid Number</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    {/* <input type="text" className="width_100 input-outer-box" name="year" id="gateEntryYearValue" value={this.state.gateEntryYearValue} onChange={this.handleChangeValue}/> */}
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="gateEntryYearValue" className="width_100 input-outer-box" onChange={this.handleChangeValue}>
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    { this.state.gateEntryYearErr && <span className="error">Please Select Correct Format</span>}
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">{this.state.gateEntryPrefixValue}/{this.state.gateEntryNumberFormat}/{this.state.gateEntryYearFormat}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="grc" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="input-outer-box" name="prefix" id="grcPrefixValue" value={this.state.grcPrefixValue} onChange={this.handleChangeValue}/>
                                                    {this.state.grcPrefixErr && <span className="error">Enter Valid Prefix</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" id="grcDocValue" value={this.state.grcDocValue} onChange={this.handleChangeValue}/>
                                                    { this.state.grcDocErr && <span className="error">Enter Valid Number</span>}
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    {/* <input type="text" className="width_100 input-outer-box" name="year" id="grcYearValue" value={this.state.grcYearValue} onChange={this.handleChangeValue}/> */}
                                                    <div className="samb-asnno">
                                                        <div className="samb-dropdown">
                                                            <select id="grcYearValue" className="width_100 input-outer-box" onChange={this.handleChangeValue}>
                                                                <option value="">Select</option>
                                                                <option value="YY-YY">YY-YY</option>
                                                                <option value="YYYY">YYYY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    { this.state.grcYearErr && <span className="error">Please Select Correct Format</span>}
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Sample : <span className="bold">{this.state.grcPrefixValue}/{this.state.grcNumberFormat}/{this.state.grcYearFormat}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.toastLoader ? <ToastLoader toastMsg={this.state.toastMsg} /> : null}
            </div>
        )
    }
}

export default DocumentFormats;  