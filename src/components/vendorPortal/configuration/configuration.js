import React from 'react';
import TableHeader from './tableHeader';
import DigiVendSetting from './digivendSetting';
import AsnSetting from './asnSetting';

class Configuration extends React.Component {
    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 pad-0">
                    <div className="global-search-tab gcl-tab p-lr-47">
                        <ul className="nav nav-tabs gst-inner" role="tablist">
                            <li className="nav-item active" >
                                <a className="nav-link gsti-btn" href="#asn" role="tab" data-toggle="tab">ASN</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#lrnumber" role="tab" data-toggle="tab">LR Number</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#gateentry" role="tab" data-toggle="tab">Gate Entry</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link gsti-btn" href="#grc" role="tab" data-toggle="tab">GRC</a>
                            </li>
                            {/* <li className="pst-button">
                                <div className="pstb-inner">
                                    <button type="button" className="pst-save">Save</button>
                                    <button type="button">Clear</button>
                                </div>
                            </li> */}
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="asn" role="tabpanel">
                            <AsnSetting {...this.props} {...this.state} />
                        </div>
                        <div className="tab-pane fade" id="lrnumber" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="width_100 input-outer-box" name="prefix"/>
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" />
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <input type="text" className="width_100 input-outer-box" name="year"/>
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Example : <span className="bold">Prefix/Document Serial/YY-YY</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="gateentry" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="input-outer-box" name="prefix"/>
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" />
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <input type="text" className="width_100 input-outer-box" name="year"/>
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Example : <span className="bold">Prefix/Document Serial/YY-YY</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="grc" role="tabpanel">
                            <div className="col-md-12 pad-lr-30">
                                <div className="col-md-6">
                                    <div className="asnSetting m-top-30">
                                        <ul className="asns-inner">
                                            <div className="asnsi-box">
                                                <div className="asn-input-main afterSlash">
                                                    <label>Prefix</label>
                                                    <input type="text" className="input-outer-box" name="prefix"/>
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main">
                                                    <label>Document Serial</label>
                                                    <input type="text" className="input-outer-box" />
                                                </div>
                                            </div>
                                            <div className="asnsi-box">
                                                <div className="asn-input-main beforeSlash">
                                                    <label>Year</label>
                                                    <input type="text" className="width_100 input-outer-box" name="year"/>
                                                </div>
                                            </div>
                                        </ul>
                                        {/* <span className="asns-dot-btn"></span> */}
                                        <div className="asn-prefix-foot">
                                            <span className="asnpf-text">Example : <span className="bold">Prefix/Document Serial/YY-YY</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Configuration;