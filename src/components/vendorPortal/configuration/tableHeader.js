import React from 'react';
import Pagination from '../../pagination';

class TableHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showBulkAction: false,
            getMainHeaderConfig:[],
            mainHeaderConfigDataState:{},
            getSetHeaderConfig:[],
            setHeaderConfigDataState:{},
            getItemHeaderConfig:[],
            itemHeaderConfigDataState:{},
            getAsnHeaderConfig:[],
            asnHeaderConfigDataState:{},
        }
    }

    componentDidMount() {
        document.addEventListener("keydown", this.escFun, false);
        document.addEventListener("click", this.escFun, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFun, false);
        document.removeEventListener("click", this.escFun, false);
    }

    openBulkAction(e) {
        e.preventDefault();
        this.setState({
            showBulkAction: !this.state.showBulkAction
        });
    }
    openConfigurationHeaders = (data) => {
        this.setState({
            showBulkAction: !this.state.showBulkAction,
        })
        this.props.openConfigurationHeadersPageName(data)
    } 

    escFun = (e) =>{  
        if( e.keyCode == 27 || (e != undefined && e.target != null && e.target.className.baseVal == undefined && e.target.className.includes("backdrop-transparent"))){
            this.setState({ showBulkAction: false })
        }
    }

    render() {
        return (
            <div className="container-fluid pad-0">
                <div className="col-lg-12 p-lr-47">
                    <div className="gen-congiguration">
                        <div className="col-lg-6 pad-0">
                            <div className="gc-left">
                                <div className="global-search-tab gcl-tab">
                                    <ul className="nav nav-tabs gst-inner" role="tablist">
                                        <li className={this.props.headerTabVal == 1 ? "nav-item active": "nav-item"} >
                                            <a className="nav-link gsti-btn" href="#searchpages" role="tab" data-toggle="tab" onClick={(e)=>this.props.tableHeaderTabOpen(1)}>Main Table</a>
                                        </li>
                                        {this.props.pageName !== "Ledger Report" && this.props.pageName !== "Outstanding Report" && this.props.pageName !== "Sales Report" &&
                                         this.props.pageName !== "Stock Report" && this.props.pageName !== "Orders Report" && this.props.pageName !== "Inspections Report" &&
                                         this.props.pageName !== "Shipments Report" && this.props.pageName !== "Logistics Report" ?
                                         <li className={this.props.headerTabVal == 2 ? "nav-item active": "nav-item"}>
                                            <a className="nav-link gsti-btn" href="#useractivities" role="tab" data-toggle="tab" onClick={(e)=>this.props.tableHeaderTabOpen(2)}>Set Table</a>
                                         </li> : null}
                                        {this.props.pageName !== "Ledger Report" && this.props.pageName !== "Outstanding Report" && this.props.pageName !== "Sales Report" &&
                                         this.props.pageName !== "Stock Report" && this.props.pageName !== "Orders Report" && this.props.pageName !== "Inspections Report" &&
                                         this.props.pageName !== "Shipments Report" && this.props.pageName !== "Logistics Report" ? 
                                        <li className={this.props.headerTabVal == 3 ? "nav-item active": "nav-item"}>
                                            <a className="nav-link gsti-btn" href="#itemtable" role="tab" data-toggle="tab" onClick={(e)=>this.props.tableHeaderTabOpen(3)}>Item Table</a>
                                        </li> : null}
                                        {this.props.pageName == "Processed Orders" ||
                                         this.props.pageName == "Goods In-Transit"?
                                         <li className={this.props.headerTabVal == 4 ? "nav-item active": "nav-item"}>
                                            <a className="nav-link gsti-btn" href="#itemtable" role="tab" data-toggle="tab" onClick={(e)=>this.props.tableHeaderTabOpen(4)}>ASN Table</a>
                                         </li> : null}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 pad-0">
                            <div className="gc-right">
                                <div className="nph-switch-btn">
                                    <label className="tg-switch" >
                                        <input type="checkbox" checked={this.props.isSet} onChange={(e) => this.props.changesIsSet(e)} />
                                        <span className="tg-slider tg-round"></span>
                                        {this.props.isSet ? <span className="nph-wbtext nph-wbtextset">Vendor</span> : <span className="nph-wbtext">Retailer</span>}
                                    </label>
                                </div>
                                <div className="gcr-dropdown">
                                    <button type="button" className="" onClick={(e) => this.openBulkAction(e)}>{this.props.pageName} <img src={require('../../../assets/downArrowNew.svg')} /></button>
                                    {this.state.showBulkAction && <div className="backdrop-transparent zIndex2"></div>}
                                    {this.state.showBulkAction &&
                                    <ul className="gcrd-inner">
                                        <li>
                                            <span className="gcrdi-head">Orders</span>
                                        </li>
                                        {!this.props.isSet ? <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Open PO")}>
                                            <span>Open PO</span>
                                        </li> :
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Pending Orders")}>
                                            <span>Pending Orders</span>
                                        </li>}
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Processed Orders")}>
                                            <span>Processed Orders</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Cancelled Orders")}>
                                            <span>Cancelled Orders</span>
                                        </li>
                                        {!this.props.isSet && <li>
                                            <span className="gcrdi-head">Quality Check</span>
                                        </li>}
                                        {!this.props.isSet && <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Pending Inspection")}>
                                            <span>Pending Inspection</span>
                                        </li>}
                                        {!this.props.isSet && <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Cancelled Inspection")}>
                                            <span>Cancelled Inspection</span>
                                        </li>}
                                        <li>
                                            <span className="gcrdi-head">Shipment</span>
                                        </li>
                                        {!this.props.isSet ? <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Pending ASN Approval")}>
                                            <span>Pending ASN Approval</span>
                                        </li> :
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("ASN Under Approval")}>
                                            <span>ASN Under Approval</span>
                                        </li>}
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Approved ASN")}>
                                            <span>Approved ASN</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Rejected ASN")}>
                                            <span>Rejected ASN</span>
                                        </li>
                                        <li>
                                            <span className="gcrdi-head">Logistics</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("LR Processing")}>
                                            <span>LR Processing</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Goods In-Transit")}>
                                            <span>Goods In-Transit</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Gate Entry")}>
                                            <span>Gate Entry</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Grc Status")}>
                                            <span>Grc Status</span>
                                        </li>
                                        <li>
                                            <span className="gcrdi-head">Reports</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Ledger Report")}>
                                            <span>Ledger Report</span>
                                        </li>
                                        {!this.props.isSet && <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Outstanding Report")}>
                                            <span>Outstanding Report</span>
                                        </li>}
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Sales Report")}>
                                            <span>Sales Report</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Stock Report")}>
                                            <span>Stock Report</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Orders Report")}>
                                            <span>Orders Report</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Inspections Report")}>
                                            <span>Inspections Report</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Shipments Report")}>
                                            <span>Shipments Report</span>
                                        </li>
                                        <li className="gcrdi-item" onClick={(e)=> this.openConfigurationHeaders("Logistics Report")}>
                                            <span>Logistics Report</span>
                                        </li>
                                    </ul>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-content">
                    { this.props.headerTabVal == 1 ?
                    <div className={this.props.headerTabVal == 1 ? "tab-pane fade in active": "tab-pane fade in"} id="searchpages" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="gen-item-udf-table">
                                <div className="giut-headfix" id="table-scroll">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                    <table className="table gen-main-table">
                                        <thead >
                                            <tr>
                                                <th className="fix-action-btn"></th>
                                                <th>
                                                    <label>Default Header Name</label>
                                                     <img src={require('../../../assets/headerFilter.svg')} /> 
                                                </th>
                                                <th>
                                                    <label>Current Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} /> 
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {/* {
                                        typeof this.props.getMainHeaderConfig == "undefined" ? null :
                                        this.props.getMainHeaderConfig.map((item,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td className="fix-action-btn">
                                                        <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                        </ul> 
                                                    </td>
                                                    <td className="width145 border-lft"><label className="bold">{item}</label></td>
                                                    <td> 
                                                        <input type="text" className="modal_tableBox " onChange={(e)=>this.props.mainHeaderInputChange(e,item)}/>
                                                    </td>
                                                </tr>
                                            )
                                        })} */}
                                        {this.props.getMainHeaderConfig !== undefined && this.props.getMainHeaderConfig !== null && Object.keys(this.props.getMainHeaderConfig).length !== 0 ?
                                            Object.keys(this.props.getMainHeaderConfig).map((key, index) =>
                                                <tr key={index}>
                                                    <td className="fix-action-btn"></td>
                                                    <td className="border-lft"><label className="bold">{this.props.getMainHeaderConfig[key]}</label></td>
                                                    <td><input type="text" className="modal_tableBox " value={this.props.getMainCustomHeader[key] === undefined ? "" : this.props.getMainCustomHeader[key]} onChange={(e)=>this.props.mainHeaderInputChange(e,key)}/></td>
                                                </tr>
                                            ) :
                                            <tr><td><label>No headers found!</label></td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                {/* <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    : null }
                    { this.props.headerTabVal == 2 ?
                    <div className={this.props.headerTabVal == 2 ? "tab-pane fade in active": "tab-pane fade in"} id="useractivities" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="gen-item-udf-table">
                                <div className="giut-headfix" id="table-scroll">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                    <table className="table gen-main-table">
                                        <thead >
                                            <tr>
                                                <th className="fix-action-btn"></th>
                                                <th>
                                                    <label>Default Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} />
                                                </th>
                                                <th>
                                                    <label>Current Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {/* {
                                        typeof this.props.getSetHeaderConfig == "undefined" ? null :
                                        this.props.getSetHeaderConfig.map((item,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td className="fix-action-btn">
                                                         <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td className="width145 border-lft"><label className="bold">{item}</label></td>
                                                    <td> 
                                                        <input type="text" className="modal_tableBox " onChange={(e)=>this.props.setHeaderInputChange(e,item)}/>
                                                    </td>
                                                </tr>
                                            )
                                        })} */}
                                        {this.props.getSetHeaderConfig !== undefined && this.props.getSetHeaderConfig !== null && Object.keys(this.props.getSetHeaderConfig).length !== 0 ?
                                        Object.keys(this.props.getSetHeaderConfig).map((key, index) =>
                                            <tr key={index}>
                                                <td className="fix-action-btn"></td>
                                                <td className="border-lft"><label className="bold">{this.props.getSetHeaderConfig[key]}</label></td>
                                                <td><input type="text" className="modal_tableBox " value={this.props.getSetCustomHeader[key] === undefined ? "" : this.props.getSetCustomHeader[key]} onChange={(e)=>this.props.setHeaderInputChange(e,key)}/></td>
                                            </tr>
                                        ) :
                                        <tr><td><label>No headers found!</label></td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                {/* <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    : null }
                    { this.props.headerTabVal == 3 ?
                    <div className={this.props.headerTabVal == 3 ? "tab-pane fade in active": "tab-pane fade in"} id="itemtable" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="gen-item-udf-table">
                                <div className="giut-headfix" id="table-scroll">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                    <table className="table gen-main-table">
                                        <thead >
                                            <tr>
                                                <th className="fix-action-btn"></th>
                                                <th>
                                                    <label>Default Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} />
                                                </th>
                                                <th>
                                                    <label>Current Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {/* {
                                        typeof this.props.getItemHeaderConfig == "undefined" ? null :
                                        this.props.getItemHeaderConfig.map((item,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td className="fix-action-btn">
                                                         <ul className="table-item-list">
                                                            <li className="til-inner">
                                                                <label className="checkBoxLabel0">
                                                                    <input type="checkBox" />
                                                                    <span className="checkmark1"></span>
                                                                </label>
                                                            </li>
                                                        </ul> 
                                                    </td>
                                                    <td className="width145 border-lft"><label className="bold">{item}</label></td>
                                                    <td> 
                                                        <input type="text" className="modal_tableBox " onChange={(e)=>this.props.itemHeaderInputChange(e,item)}/>
                                                    </td>
                                                </tr>
                                            )
                                        })} */}
                                        {this.props.getItemHeaderConfig !== undefined && this.props.getItemHeaderConfig !== null && Object.keys(this.props.getItemHeaderConfig).length !== 0 ?
                                        Object.keys(this.props.getItemHeaderConfig).map((key, index) =>
                                            <tr key={index}>
                                                <td className="fix-action-btn"></td>
                                                <td className="border-lft"><label className="bold">{this.props.getItemHeaderConfig[key]}</label></td>
                                                <td><input type="text" className="modal_tableBox " value={this.props.getItemCustomHeader[key] === undefined ? "" : this.props.getItemCustomHeader[key]} onChange={(e)=>this.props.itemHeaderInputChange(e,key)}/></td>
                                            </tr>
                                        ) :
                                        <tr><td><label>No headers found!</label></td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                {/* <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    : null }
                    { this.props.headerTabVal == 4 ?
                    <div className={this.props.headerTabVal == 4 ? "tab-pane fade in active": "tab-pane fade in"} id="searchpages" role="tabpanel">
                        <div className="col-md-12 p-lr-47">
                            <div className="gen-item-udf-table">
                                <div className="giut-headfix" id="table-scroll">
                                {/* <div className="columnFilterGeneric">
                                    <span className="glyphicon glyphicon-menu-left"></span>
                                </div> */}
                                    <table className="table gen-main-table">
                                        <thead >
                                            <tr>
                                                <th className="fix-action-btn"></th>
                                                <th>
                                                    <label>Default Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} />
                                                </th>
                                                <th>
                                                    <label>Current Header Name</label>
                                                    <img src={require('../../../assets/headerFilter.svg')} />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {this.props.getMainHeaderConfig !== undefined && this.props.getMainHeaderConfig !== null && Object.keys(this.props.getMainHeaderConfig).length !== 0 ?
                                        Object.keys(this.props.getMainHeaderConfig).map((key, index) =>
                                            <tr key={index}>
                                                <td className="fix-action-btn"></td>
                                                <td className="border-lft"><label className="bold">{this.props.getMainHeaderConfig[key]}</label></td>
                                                <td><input type="text" className="modal_tableBox " value={this.props.getMainCustomHeader[key] === undefined ? "" : this.props.getMainCustomHeader[key]} onChange={(e)=>this.props.mainHeaderInputChange(e,key)}/></td>
                                            </tr>
                                        ) :
                                        <tr><td><label>No headers found!</label></td></tr>}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="col-md-12 pad-0" >
                                {/* <div className="new-gen-pagination">
                                    <div className="ngp-left">
                                        <div className="table-page-no">
                                            <span>Page :</span><input type="number" className="paginationBorder"  min="1" value={1} />
                                        </div>
                                    </div>
                                    <div className="ngp-right">
                                        <div className="nt-btn">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                    : null }
                </div>
            </div>
        )
    }
}

export default TableHeader;