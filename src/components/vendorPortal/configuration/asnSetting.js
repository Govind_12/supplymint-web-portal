import React, { Component } from 'react'

export default class AsnSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {
        this.props.getAsnFormatRequest()
    }
    render() {
        return (
            <div className="col-md-12 pad-lr-30">
                <div className="col-md-6">
                    <div className="asnSetting m-top-30">
                        <ul className="asns-inner">
                            <div className="asnsi-box">
                                <div className="asn-input-main afterSlash">
                                    <label>Prefix</label>
                                    <input type="text" className="width_100 input-outer-box" name="prefix" value={this.props.prefix} onChange={this.props.handleAsnChange} />
                                </div>
                            </div>
                            <div className="asnsi-box">
                                <div className="asn-input-main">
                                    <label>Document Serial</label>
                                    <input type="text" className="width_100 input-outer-box" />
                                </div>
                            </div>
                            <div className="asnsi-box">
                                <div className="asn-input-main beforeSlash">
                                    <label>Year</label>
                                    <input type="text" className="width_100 input-outer-box" name="year"/>
                                    {/* <select className="width_100 input-outer-box" onChange={this.props.handleAsnChange} name="year">
                                        <option value="YY-YY" selected={this.props.valueStr == "YY-YY" ? true : false}>YY-YY</option>
                                        <option value="YYYY" selected={this.props.valueStr == "YYYY" ? true : false}>YYYY</option>
                                        <option value="MM-YYYY" selected={this.props.valueStr == "MM-YYYY" ? true : false}>MM-YYYY</option>
                                    </select> */}
                                </div>
                            </div>
                        </ul>
                        {/* <span className="asns-dot-btn"></span> */}
                        <div className="asn-prefix-foot">
                            <span className="asnpf-text">Example : <span className="bold">Prefix/Document Serial/YY-YY</span></span>
                        </div>
                        {/* <div className="asnFooter displayInline width100">
                            <label className="displayBlock">Preview</label>
                            <span>{this.props.prefix}/{this.props.randomNo}/{this.props.year}</span>
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }
}
